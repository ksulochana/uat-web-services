﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace Test
{
    public class PropertyCopier<TParent, TChild>
        where TParent : class
        where TChild : class
    {
        public static void Copy(TParent parent, TChild child)
        {
            var parentProperties = parent.GetType().GetProperties();
            var childProperties = child.GetType().GetProperties();
            foreach (var parentProperty in parentProperties)
            {
                foreach (var childProperty in childProperties)
                {
                    if (parentProperty.Name == childProperty.Name)
                    {
                        if (parentProperty.PropertyType == childProperty.PropertyType)
                        {
                            childProperty.SetValue(child, parentProperty.GetValue(parent));
                            break;
                        }
                        else
                        {
                            var parentProperties1 = parentProperty.GetType().GetProperties();
                            var childProperties1 = childProperty.GetType().GetProperties();
                            foreach (var parentProperty1 in parentProperties1)
                            {
                                foreach (var childProperty1 in childProperties1)
                                {
                                    if (parentProperty1.Name == childProperty1.Name && parentProperty1.PropertyType == childProperty1.PropertyType)
                                    {
                                        childProperty1.SetValue(child, parentProperty1.GetValue(parent));
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public class PropertyMap
    {
        public PropertyInfo SourceProperty { get; set; }
        public PropertyInfo TargetProperty { get; set; }
    }

    //public static class ObjectExtensionMethods { public static void CopyPropertiesFrom(this object self, object parent) { var fromProperties = parent.GetType().GetProperties(); var toProperties = self.GetType().GetProperties(); foreach (var fromProperty in fromProperties) { foreach (var toProperty in toProperties) { if (fromProperty.Name == toProperty.Name && fromProperty.PropertyType == toProperty.PropertyType) { toProperty.SetValue(self, fromProperty.GetValue(parent)); break; } } } } public static void MatchPropertiesFrom(this object self, object parent) { var childProperties = self.GetType().GetProperties(); foreach (var childProperty in childProperties) { var attributesForProperty = childProperty.GetCustomAttributes(typeof(MatchParentAttribute), true); var isOfTypeMatchParentAttribute = false; MatchParentAttribute currentAttribute = null; foreach (var attribute in attributesForProperty) { if (attribute.GetType() == typeof(MatchParentAttribute)) { isOfTypeMatchParentAttribute = true; currentAttribute = (MatchParentAttribute)attribute; break; } } if (isOfTypeMatchParentAttribute) { var parentProperties = parent.GetType().GetProperties(); object parentPropertyValue = null; foreach (var parentProperty in parentProperties) { if (parentProperty.Name == currentAttribute.ParentPropertyName) { if (parentProperty.PropertyType== childProperty.PropertyType) { parentPropertyValue = parentProperty.GetValue(parent); } } } childProperty.SetValue(self, parentPropertyValue); } } } }


    class Program
    {
        public static IList<PropertyMap> GetMatchingProperties(Type sourceType, Type targetType)
        {
            var sourceProperties = sourceType.GetProperties();
            var targetProperties = targetType.GetProperties();

            var properties = (from s in sourceProperties
                              from t in targetProperties
                              where s.Name == t.Name &&
                                    s.CanRead &&
                                    t.CanWrite &&
                                    s.PropertyType.IsPublic &&
                                    t.PropertyType.IsPublic &&
                                    s.PropertyType == t.PropertyType &&
                                    (
                                      (s.PropertyType.IsValueType &&
                                       t.PropertyType.IsValueType
                                      ) ||
                                      (s.PropertyType == typeof(string) &&
                                       t.PropertyType == typeof(string)
                                      )
                                    )
                              select new PropertyMap
                              {
                                  SourceProperty = s,
                                  TargetProperty = t
                              }).ToList();
            return properties;
        }

        public static void CopyProperties(object source, object target)
        {
            var sourceType = source.GetType();
            var targetType = target.GetType();
            var propMap = GetMatchingProperties(sourceType, targetType);

            for (var i = 0; i < propMap.Count; i++)
            {
                var prop = propMap[i];
                var sourceValue = prop.SourceProperty.GetValue(source,
                                    null);
                prop.TargetProperty.SetValue(target, sourceValue, null);
            }
        }
        static void Main(string[] args)
        {
            new Program().Test();
        }

        public bool FnIsNumericPositive(string input)
        {
            bool isNumeric = false;

            int ivalue = 0;
            if (int.TryParse(input, NumberStyles.None, new CultureInfo("en-US"), out ivalue))
                isNumeric = true;

            long lvalue = 0;
            if (long.TryParse(input, NumberStyles.None, new CultureInfo("en-US"), out lvalue))
                isNumeric = true;

            Decimal dvalue = 0;
            if (Decimal.TryParse(input, NumberStyles.None, new CultureInfo("en-US"), out dvalue))
                isNumeric = true;

            if (!string.IsNullOrEmpty(input) && input[0] == '-')
                isNumeric = false;


            return isNumeric;
        }

        public void Test()
        {
            XDSConnectN.XDSConnectWS xDSConnectWS = new XDSConnectN.XDSConnectWS();
            xDSConnectWS.Url = "https://www.uat.xds.co.za/xdsconnect/xdsconnectws.asmx";

            //XDSConnectN.Zrequest zrequest = new XDSConnectN.Zrequest();
            //zrequest.Username = "ksulochana";
            //zrequest.Password = "Sulo@1987";
            //zrequest.IdNumber = "8608301250181";
            //zrequest.YourReference = "test1";
            //zrequest.SurName = "Kothamasu";
            //zrequest.FirstName = "Siva";

            string XML1 = xDSConnectWS.ConnectCustomVettingAB("ksulochana", "Sulo@1987", "8608301250181", string.Empty,
             "Siva", "Kothamasu", string.Empty, "test1", string.Empty);

            XDSConnectN.Yrequest yrequest = new XDSConnectN.Yrequest();
            yrequest.pUsername = "ksulochana";
            yrequest.pPassword = "cal100";
            yrequest.pIdNumber = "8608301250181";
            yrequest.pYourReference = "test1";
            yrequest.pLastName = "Kothamasu";
            yrequest.pPDFIndicator = "Y";

            string XML = xDSConnectWS.ConnectCustomVettingY(yrequest);

            MTNTest.MHConnect omHConnect = new MTNTest.MHConnect();
            MTNTest.MTNInput oInput2 = new MTNTest.MTNInput();
            omHConnect.Url = "https://www.web.xds.co.za/ADPConnect/mhconnect.asmx";
            oInput2.AddressLine1 = "WELBEDACHT WEST";
            oInput2.AddressLine2 = "WELBEDACHT WEST";
            oInput2.Suburb = "";
            oInput2.BankAccountNumber = "1535055721";
            oInput2.BankAccountType = "SAVINGSACCOUNT";
            oInput2.BankBranchCode = "470010";
            oInput2.BankName = "Bidvest Bank";
            oInput2.BirthDate = "990-12-06T00:00:00Z";
            oInput2.User = "ksulochana";
            oInput2.Password = "cal100";
            oInput2.IDType = "1";
            oInput2.IdentityNumber = "9306070553088";
            oInput2.Surname = "Mzulwini";
            oInput2.FirstName = "Nelisiwe Nomalizo";
            oInput2.GrossMonthlyIncome = "41991";
            oInput2.Gender = "F";
            oInput2.BirthDate = "19761205";
            oInput2.IsExistingClient = "true";
            oInput2.PostalCode = "4092";
            oInput2.HomeTelephoneCode = "27";
            oInput2.Employer = "Thia";
            oInput2.City = "CHATSWORTH";
            oInput2.WorkTelephoneCode = "";
            oInput2.MaritalStatus = "Single";
            oInput2.Occupation = "SELF-EMPLOYED";
            oInput2.NumberOfYearsAtEmployer = "1";
            oInput2.WorkTelephoneNumber = "3146894";
            oInput2.CellNumber = "27635156498";
            oInput2.HighestQualification = "";
            oInput2.IsExistingClient = "F";
            oInput2.Branch = "Online";
            oInput2.Channel = "Store";
            oInput2.Company = "MTN";
            oInput2.RuleSet = "MTN";
            oInput2.DealerCode = "SABC1006";
            oInput2.EmailAddress = "635156498@noemail.co.za";
            oInput2.HomeTelephoneCode = "";
            oInput2.HomeTelephoneNumber = "635156498";
            oInput2.AlternateContactNumber = "";
            oInput2.CustomerName = "Nelisiwe Nomalizo";

            MTNTest.MTNOutput responseSA = omHConnect.SubmitApplicationATelesales(oInput2);

            Console.WriteLine(responseSA.ToString());
            Console.ReadLine();

            // CanvasConnect.MCanvasConnect oCanvas = new CanvasConnect.MCanvasConnect();

            // CanvasConnect.MTNCanvasInput oCInput = new CanvasConnect.MTNCanvasInput();
            // oCInput.AddressLine1 = string.Empty;
            // oCInput.AddressLine2 = string.Empty;
            // oCInput.AlternateContactNumber = string.Empty;
            // oCInput.BankAccountNumber = string.Empty;
            // oCInput.BankAccountType = string.Empty;
            // oCInput.BankBranchCode = string.Empty;
            // oCInput.BankName = string.Empty;
            // oCInput.BirthDate = string.Empty;
            // oCInput.Branch = string.Empty;
            // oCInput.CellNumber = string.Empty;
            // oCInput.Channel = string.Empty;
            // oCInput.City = string.Empty;
            // oCInput.Company= string.Empty;
            // oCInput.CustomerName = string.Empty;
            // oCInput.DealerCode = string.Empty;
            // oCInput.EmailAddress = string.Empty;
            // oCInput.Employer = string.Empty;
            // oCInput.FirstName = string.Empty;
            // oCInput.Gender = string.Empty;
            // oCInput.GrossMonthlyIncome = string.Empty;
            // oCInput.HighestQualification = string.Empty;
            // oCInput.HomeTelephoneCode = string.Empty;
            // oCInput.HomeTelephoneNumber = string.Empty;
            // oCInput.IdentityNumber = string.Empty;
            // oCInput.IDType = string.Empty;
            // oCInput.IsExistingClient = string.Empty;
            // oCInput.MaritalStatus = string.Empty;
            // oCInput.NumberOfYearsAtEmployer = string.Empty;
            // oCInput.Occupation = string.Empty;
            // oCInput.OverwriteAVSResult = false;
            // oCInput.Password = string.Empty;
            // oCInput.PostalCode = string.Empty;
            // oCInput.RuleSet = string.Empty;
            // oCInput.Suburb = string.Empty;
            // oCInput.Surname = string.Empty;
            // oCInput.User = string.Empty;
            // oCInput.WorkTelephoneCode = string.Empty;
            // oCInput.WorkTelephoneNumber = string.Empty;


            // oCInput.IDType = "1";
            // oCInput.IdentityNumber = "8608301250181";
            // oCInput.IsExistingClient = "F";
            // oCInput.User = "ksulochana";
            // oCInput.Password = "cal100";
            // oCInput.OverwriteAVSResult = false;
            // oCInput.CellNumber = "0765413243";
            // oCInput.DealerCode = "456TEST";
            // oCInput.BankName = "test1";
            // oCInput.BankBranchCode = "test1";
            // oCInput.BankAccountType = "test1";
            // oCInput.BankAccountNumber  = "1378462";
            // oCInput.EmailAddress = "skothamasu@xds.co.za";
            // oCInput.GrossMonthlyIncome = "12000";

            // string ticket = oCanvas.Login(oCInput.User, oCInput.Password);
            // CanvasConnect.CanvasVettingResult oResult=  oCanvas.SubmitApplicationCanvas(oCInput);



            // DateTime odateTime = new DateTime();
            // DateTime.TryParse("2019-11-30", out odateTime);

            //MTNTest.MHConnect omHConnect = new MTNTest.MHConnect();
            //MTNTest.MTNInput oInput = new MTNTest.MTNInput();

            //omHConnect.Url = "https://www.uat.xds.co.za/mhconnect/mhconnect.asmx";
            //oInput.Company = "MTNAcquisition";
            //oInput.RuleSet = "MTN";
            //oInput.Branch = "online";
            //oInput.User = "ksulochana";// "ADP_UAT";
            //oInput.Password = "Sulo@123";// "Sulo@1987";// "053pfr9t";
            //oInput.IsExistingClient = "F";
            //oInput.GrossMonthlyIncome = "87400";
            //oInput.Surname = "NYAMAYARO";
            //oInput.FirstName = "PATRICIA";
            //oInput.IDType = "1";
            //oInput.BirthDate = "19690604";
            //oInput.IdentityNumber = "6906041400187";
            //oInput.Gender = "F";
            //oInput.MaritalStatus = "Single";
            //oInput.AddressLine1 = "208 ROSA ROYALE 1 17 OLEA ROAD";
            //oInput.AddressLine2 = "";
            //oInput.Suburb = "VORNA VALLEY";
            //oInput.City = "";
            //oInput.PostalCode = "1686";
            //oInput.HomeTelephoneCode = "";
            //oInput.HomeTelephoneNumber = "";
            //oInput.WorkTelephoneCode = "2711";
            //oInput.WorkTelephoneNumber = "2116002";
            //oInput.CellNumber = "0767565522";
            //oInput.Occupation = "Professional";
            //oInput.Employer = "UMS";
            //oInput.NumberOfYearsAtEmployer = "4";
            //oInput.BankName = "STANDARDBANK";
            //oInput.BankAccountType = "Current";
            //oInput.BankBranchCode = "051001";
            //oInput.BankAccountNumber = "";
            //oInput.HighestQualification = "Postgraduate";
            //oInput.DealerCode = "304457";
            //oInput.CustomerName = "MTN Store - Cresta";
            //oInput.EmailAddress = "";
            //oInput.AlternateContactNumber = "27767565522";
            //oInput.UniqueReferenceNumber = "34867683-TV6123711";
            // oInput.Channel = "test";

            //MTNTest.MTNOutput responseSA = omHConnect.SubmitApplication(oInput);

            // Console.WriteLine(responseSA.ToString());
            // Console.ReadLine();


            // UATXDSConnect.XDSConnectWS oconnect2 = new UATXDSConnect.XDSConnectWS();
            //oconnect2.Url = "https://www.web.xds.co.za/uatxdsconnect/xdsconnectws.asmx";
            //////UATXDSConnect.CompanyInformationRequest oreq = new UATXDSConnect.CompanyInformationRequest();
            //string str = oconnect2.ConnectCustomVettingSResult(0, "ksulochana", "Sulo@1987", "57485954:59475870", string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, "3", "abc", null);

            // //DateTime dt1 = new DateTime(2016,02,01);
            // //DateTime dt2 = new DateTime(2019, 02, 01);

            // //if (dt1 < dt2)
            // //{
            // //    Console.WriteLine("oldest info found");
            // //    Console.ReadLine();
            // //}

            // adpconnect.ADPConnect oconnectx = new adpconnect.ADPConnect();
            // oconnectx.Url = /*"http://localhost:16816/ADPConnect.asmx";*/ "https://www.uat.xds.co.za/MTNConnect/adpconnect.asmx";
            // adpconnect.VettingVRequest nonRegEntityRequest = new adpconnect.VettingVRequest();

            // nonRegEntityRequest.User = "ksulochana";
            // nonRegEntityRequest.Password = "Sulo@1987";

            // adpconnect.PrincipalInfo principalInfo = new adpconnect.PrincipalInfo();
            // principalInfo.IDType = "1";
            // principalInfo.IdentityNumber = "8608301250181";
            // principalInfo.FirstName = "siva";
            // principalInfo.Surname = "kothamasu";
            // principalInfo.Designation = "123";

            // nonRegEntityRequest.PrincipalDetails = new adpconnect.PrincipalInfo[10];
            // nonRegEntityRequest.PrincipalDetails[0] = principalInfo;

            // principalInfo = new adpconnect.PrincipalInfo();
            // principalInfo.IDType = "1";
            // principalInfo.IdentityNumber = "6308305577083";
            // principalInfo.FirstName = "SAKHILE";
            // principalInfo.Surname = "KUNENE";
            // principalInfo.Designation = "123";

            // nonRegEntityRequest.PrincipalDetails[1] = principalInfo;
            // principalInfo = new adpconnect.PrincipalInfo();
            // principalInfo.IDType = "2";
            // principalInfo.IdentityNumber = "6009160136011";
            // principalInfo.FirstName = "Felicia";
            // principalInfo.Surname = "ADKINS";
            // principalInfo.Designation = "123";

            // nonRegEntityRequest.PrincipalDetails[2] = principalInfo;
            // principalInfo = new adpconnect.PrincipalInfo();
            // principalInfo.IDType = "1";
            // principalInfo.IdentityNumber = "5203240071089";
            // principalInfo.FirstName = "heila";
            // principalInfo.Surname = "LOOTS";
            // principalInfo.Designation = "123";

            // nonRegEntityRequest.PrincipalDetails[3] = principalInfo;
            // principalInfo = new adpconnect.PrincipalInfo();
            // principalInfo.IDType = "1";
            // principalInfo.IdentityNumber = "7806020158088";
            // principalInfo.FirstName = "maria";
            // principalInfo.Surname = "BEZUIDENHOUT";
            // principalInfo.Designation = "123";

            // nonRegEntityRequest.PrincipalDetails[4] = principalInfo;
            // principalInfo = new adpconnect.PrincipalInfo();
            // principalInfo.IDType = "1";
            // principalInfo.IdentityNumber = "5911205865085";
            // principalInfo.FirstName = "mandla";
            // principalInfo.Surname = "MAVUSO";
            // principalInfo.Designation = "123";

            // nonRegEntityRequest.PrincipalDetails[5] = principalInfo;
            // nonRegEntityRequest.IsExistingClient = "1";




            // adpconnect.MTNCommercialFinalOutput oOutput = oconnectx.ConnectCustomVettingV(nonRegEntityRequest);

            //// Console.WriteLine(responsev);
            // Console.ReadLine();

            //UATXDSConnect.XDSConnectWS oconnect1 = new UATXDSConnect.XDSConnectWS();
            //UATXDSConnect.BioMetricResultRequest oreq = new UATXDSConnect.BioMetricResultRequest();
            //oconnect1.Url = "https://www.web.xds.co.za/xdsconnect/xdsconnectws.asmx";
            //string val = oconnect1.Login("ksulochana", "cal100");

            //oreq.ConnectTicket = val;//"536B1094870ADC979BEEAB0C059B17CC09B5A7C3857FF6109CCBABCE31F8DA833521B0A3B89BD3B4F5D333D675AF5D9A4BF7C56CD372965D9FE784C6A3F1C7EFD80713634AB3BBAE2AD01BE761E150D252CD3073A3E2A26396979E9A62B68B78F86DE2D4B5C52E107477589642F9CED3249FDA8F";
            //oreq.EnquiryID = 362180963;
            //oreq.EnquiryResultID = 446494172;
            //oreq.Finger1Index = 0;
            //oreq.Finger2Index = 0;
            //oreq.LastUsedTimeStamp = DateTime.Now;
            //oreq.DeviceFirmwareVersion = "djkfb";
            //oreq.DeviceModel = "sdfsdfsdf";
            //oreq.DeviceSerialNumber = "dfdfgdfg";
            //oreq.WorkstationLoggedInUserName = "sdfsdf";
            //oreq.WorkStationName = "dssdf";

            //oreq.Portrait = "/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCAGUAWADASIAAhEBAxEB/8QAHAAAAgMBAQEBAAAAAAAAAAAAAAUDBAYCAQcI/8QAORAAAQQBAwIFAgQEBgEFAAAAAQACAxEEBRIhMUEGEyJRYRQyI1JxgQcVQpEzYnKhscEWJTVTgtH/xAAZAQADAQEBAAAAAAAAAAAAAAAAAgMBBAX/xAAkEQEBAQEAAwEAAgMBAAMAAAAAAQIRAyExEkFRBBMiMjNhkf/aAAwDAQACEQMRAD8A+/oQhACEIQAhCEAIQhACEIQAhCEAIQhACEKKXJihbb3gfCAlQlUussAPlAH5cqz9akLaDmg/CzreU+XJkY3q9o/UrKv1WQkkvJ/dVn5zy693+6Ot/LXuyoGGnSN/blefV4/eVqxoznd3LmXN3CgUdb+G1+rg/wDlavRkQu6SN/usQ3MqvVz+q6GcCfuP7FHWfluA9pNBwP6FdLGx6jsH3c/qrkGrygWJCfglHWflpkJRFrQI/EaD8hMIMyGceh4v2PBR1idCELQEIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIAQhCAEIQgBCEIAXlr1FIAQhRzTRwMLpHABASKjnarj4TSHO3P8AyhJdS8QP9UcPpb0sdVnHzPyHm3ErLTTPTjJ8Q5EziGPAb+UcJdPqM8gDeig8uhyuQDvsnhJdKTK23KAZTiVA/KaLokqGQc3aqyE2Ut0eYTvyiCeTSh+t4O4lQ8nquHRWl/R5hL9aey4OY8HgqExUV4G811Wfpv4TfVl3JJXYyyCKKqtZzwvTEt/TPwuHLcObsqxFmkmuiWtZtFrsPINBb+i3J5FkkOHqKa484cQQTay0U3Ia9xHyrseQ+Mja+wnmk9ZbPF1KRvEh3NTOHJjnHodz7LFw5t0CmMGR5Z3xnlOnY1KEtwtUbM7y5PS/390yQUIQhaAhCEAIQhACEIQAhCEAIQhACEIQAhCEAIQhACEIQAhCXajqbMRhDSC//hAT5mbHiRknl3YLHajq8mRIdz/0Chz9VdKXW4m0lfOHG3dUt0pnK4+a+XH9kMlDvtCp255B7KxG4BTtVmVpt1yuCT7cKSNthePcBxSS1TOULiD1ULw2zwu3O5+Fw82FO1WRA7hyH9OCuttjleiNpHKzp+IBfdGyz0U4i2m12W12R1nFQx90bVYLN3UFAjcDw1b0cV3AgLjcWlW3QOdSgli2nlErLlGHc2pmSkKtxa9DwDQTyp3JxC/c2rpX8ecsIBNhIoJi3lNIHh/I4Vc1DWTZk9u3gcp5p+pte0RymiOhWWhc4Poq6xzgeB+6dKxsgbFhCU6bqG5zYJOvYpstl6WwIQilrAhCEAIQhACEIQAhCEAIQhACEIQAhCEAIQoMrJZiwOkeenQe5QFTVNQGLEWNPrI5PsFhs/UHOc71WrWq57nOc555dys1NMZHlLapmOzMXuK8omiVE00FK13ypqxZj5ACmYwhwPKrxEK40/KS1TMTA0OCueD1KO3yvKSWqyPCzhRmJzj04VtvTou2gk0GhIrIqDHoKVuKSywFebDu/pA+VYayJg5kAPsEN4UnEkvlpR5Jb2TVgkkk236V0/EpwrkIZyFG0giwpGt+Ar0uE5vawq3klp7obx55THCjwqGTiG7BtMaPZevZuZdIHGckhLSoq903yoABY6pY5tGiFspLlwJNoFK7i5BsBUnMC9jdtPWlSaR1loY5bol3KvRzemlno5xXVMMbI3ir5Vppz6yaNmdE8OHNFarBym5UAdfqA5WLLimOl5jsaYc+k9UydjWoXjXB7Q5psEWF6mIEIQgBCEIAQhCAEIQgBCEIAQhCAEIQgPCa5PRZTW9QE0xDHfhs4Cb65nfS4vlsNPf/ALBYLUsv0lrTyeqG5nVHUcoyyEA8BLt9Lh77cbK43jsp1aLAeTxanY031VNjxfVW4zfRJT5W42nqFdjFt5VOPiqV2NpI5UtVfMSNCsxRX1C8haOtK5F6uAFPq+cvBE2tu1DIdp6H+yZQYgdyTwrPktBprQflbxl3J6J3CS6aKXUeObt7U4bjsP3dV2YG1TSAt/Jf9sLI4SHdDSsGJ3ZT/TkEerhSBoHsjjL5P6VCwgc8qCRgI5amRjtV5IiOQixudylrsQO5avDjua3umbYjssBD2EtorOG/U6z2TBxyEpnhLSTS1czWkU4WlORGwEhYfjPSdPZVy75TTJx2kHbSXPhLegTypay9jlrqrkUxa4PaefZKzbTyrEUpCrmufUP2ZIkYD0PsrEc3ylEMgLLvlXIXh3QqkqFjaaHm+dCYnHlvROFh9NndBlsdu4tbdpDmhw6EWE8T1HqEIWlCEIQAhCEAIQhACEIQAhCEAIQosiTycaST8rSUBjtfyxNmyflZ6Qsdmzhzymup5Je5xvklZyZ9u55S6qmY5Is2VG4gCl2CNqickUjuI2r0IKXx3fRMsbpyk1VcRfg4pX2O46KjHx3Vpj+KtQ06cwxhjsAkpnixMApKYpdtWU0xZeOiVS/PRrG0BnRdtolVGyvPsAp4w7qXUqRzazZ9TlrW/K93D4Ubi3oXWug1pTJc/twSB3FLkge4KkMYvhdtYCOixv6kQhxHBaSFG/bu5BV/y6ChmjBF0iwZ3OqRJaeCaXhlNc0pHgAWXcKq+UCqs/sk66MzrjIZIQXNASbItxII5Tx0ttraUsyB6ydhS1XPz2SysIJ6qvI0Bt8pjOACSRSoTvbtNLYyl01EKs1zrVmY2CqZcAVXLn3DHHf6VZx5/LdSVRy0aVhk1lVjnrRQSjgrdaTkDIwGEHlvpK+a4k427bJW38LztfC+O+eqeVLUaFCEJ0whCEAIQhACEIQAhCEAIQhACX61J5elTH3ACYJJ4ol8vSSPzOQHznPkBLqSd/JtXcx2555VF3HQpKrHjiNqgLxa7f8Aaqx5dSzhpfa9BTqpNIAKHKV4wTKI0Qo6dGF+NvClFN56qKP7QVMwWVKryrELwSOE1xpAGjlKmR91egaaA5WelJ00E1igpGTOqlXiiJHurbIeOi2E1yfXYPArqrDC7b0ChjjcOgVtjDQTRDdgbZUrRyhrCT0UgjvuR+iaOfWoHHa3kqrKXOaeKCsmEC6JJ+VG9p2o10YslL/LJbZVOYECxwmUjaHBVCfj3U7Hb472qPmu6Fyo5Ex3VavSQh4JaeVRlgcLJFpV6XTyn3KW5EpukznhJH2kJVkxOBJCaJa6rPeNtKs5dSNcFD6iVWRDVdg+xUsbzupQ9ui7bfVUiGjPFfTgtj4Xl254F8OFLD4z1qdBl8vKjdfNhMnX0RCEKiQQhCAEIQgBCEIAQhCAEIQgBZfxlLWJFHfJsrULGeNH3PG0f0s/7Q2fWAn5eVB25U0vLyo3CwkqkVpfSFXa4+Z0U032qKLqsrZ9MoBYFK6xpB6qpikdlfYO5UrHRmrcdBvJVyAN7pWC9xoHgKw2UQx7nO49lO5UzvjQY0cZAukygxmEggWs1hZwmob9v6p3iZb2ODRz+qX8Hvk7PR7Bit9laGK1KotZjjftlIafa02gyWzt3N6KuZHH5Lv6PIA6LtkdBSrxP+Ubq1yGoPC6RtHsjjOueSFy5tiipEUs4OqUsIqwqkkBcOlpq5gIURhscJLlfHl4SnG7Uo34hrkcJw6HlRT1Gwk0p/l0Z89vxn58Pg8CkmzMUC+E3zNQb5hYOEmzc5jGncQskq93OeyieCr4VN0YHZWZtQikuiqTsuIn7lbMrm3YC34pec9146dlWHWvWO3/AGqkQtT45p3RaLTHlsjDdcrOwtLX8p9gAW2/2TFr6hC7fCx3u0Fdqvgm8KEn8oVhPPiNCEIWgIQhACEIQAhCEAIQhACwvjN4bm1/kC3SwnjSvrf/AKBDZ9YdxBcVG55j5BNqV7mhV5OR8JFFeV24UiJluCCCXUFahjAFlZWz6swMDKpMYYtw9QVXGhcTu7JrCwnkhR1XVjLqLFaR+vZXWaG6dgNUpcSLc8GuFoYGjYPdT6pZJO1n4fDQbIHOsV7FO8bTo9gB4d7pmxu4UQh0O07gE3ahdz4WT6Fjy+t97h3Cu4OO2GPYx5BHYlSF57lDaJtZ+mW3ntcjJIO5SKFjhVEqUEdlSVzant0hCFXJRSEIJW3kjBS8pFil5uFJLY0FoSXWSPp3bTyExmnIBrqlOXUrSHc2pa1HV4MXvWW8vzZNz+SOyTapG98u1raC1jMMB5PZRzaexxJAtLPJHTfFa+f5OnSiIuBNpKdzH7JGkH3X06bTNw6JRmaNB9xZbv0VM+WI78F/hjg5zKBstTHFmvgClYnwBGSWjhUnNdG7hUmuoXNyewRl4Dhym2JbXNBCR6VlEv2PWhjj9QIK2Qr6FpxvAh/0q0qemX/LYP8ASriefE79CEIWsCEIQAhCEAIQhACEIQAsP41bWU0+7AtwsV43H4sRP5ENn188yTTlG020+y6yadKvB7JFHLG7ni1cbtsNULGjqpoWW+0ur6P457MIHbKA6Jvj0+qCTwN3PT3D8rHYHzPA+FzaduTrCxvSOE5x8egOFlXeJIIXAM4HurUvi+DGgEhjlcPeqC3Oal5tW/K1jY6C6LbFL53L/EA5TizFj21ySXJ1pevPnxvNkMle9WE9ln8OaePv8tJJj7hwqr4nxlR4utRzGiQflMRJHM2xylsl+G/7x9L2zlpoq1HMCOCoZ8ewaCqNe6N1FJ2xT853PRyx1qRLoZz7q7G6wrY2594uUlrhzqC6JoKpNLV0t1S5z2unPUEk+0dVWlyNvdLcjODbJcoXTs8fg79XpsobTzyqBn3uoWSqbZpMmSm3SdYOnW0EpPddP/HinarxwvfzStMwz1ITNmMyNvNBRvmgYSA6z8J5jjmv+RdX/kvlxW0kmbi9U3y9YxI3FjrsJRk6riOBNkBH5qmN+v8Apm8+Ahp4SSVjb5HK1k8mPkttjwbSLNxqcSFTNLuT6W4zjHkAjpa12NIHxsPdZRrNsoJWjwLdGwq0rl1OPpemG9Oh/wBKtqpporToB/lVtUnxG/QhCFrAhCEAIQhACEIQAhC8c4NaXOIAHUk9EB6sX454dAf8n/a1cWfiTyGOLIjc8dg5ZH+ILtkWMfewhs9V8+mFyWhoNocQe6lDeEigjFlWYxQsBV2CnK/Aywp6W8cSQ3G0uVOWXJyJ/LiBc5xqlecT9oCsadltwckSGEON9SFHvK6Zm2NN4Z8JRYkDcnPaJZ3chruQ1If4l58hfBpeHGLPXYOpPZbrS9Wx86Fu14a/8pWG/iHpOUzLZqOMXGubHYrozqfnscOs6/VmmFf4P8TY0Tsh+nzMja3cXEjot9/DXWJJY5NPyKNfbaw8/iPxFmRHFkz53RuG0tvstx/DrQ5sd7syVpAri1utfGZxeW342WfpUVHIx2iOVvJA6OVfElcW23r3Ca5kzIoSXEC0pZ9M1vnQS+r+pp7qPkk/Xp0eG245TJjy8KrmQFo3hW8KaPIZbQLHVGY5pZt7rLnuep51c+TnC+E9CmELwOpSyxGvW5QDuqnLxffju/hw5422leXkgWF6c4BhSLUM0hxPZbrfYPB4L3tWMnIaGEl3KQS5DsnJ8tvPKgydQD/Su9Fc2TUWlyTjr7J6azS8ERRBzhymbskQimt5XrdrYxRFUqLp2z5bcaE29x5d2CeTnxx2/wCzXdfHuS/KnadpJd2Y1fPde8WZ2k5jsZ7AK6gFfWMfFZjsppJcerj1K+Yar/JNK1/OOvYrssv+wg9FfOOe6hry99Z9RgMrX8jLzXSFz2RuP2hx4X0Pw74bwvEOkGXHzpWTAeprjYWA8R5ekZeoMfpGI7HiDac0nqVuf4YPyGTP2tOwjlU1ye08d10t1zRNU8MnfL+JAej2lK8TWjkO2O5+V9a8WxR5OmvikANjgFfIDpv0uSSytt9kl/NVz+uSmhYHkEJ7poqNg+Umg/wxac4BLTHyKsIjNPpmEKwoR/lCnUWMQcaPaQRtHRSqs+IX6EIQtYEIQgBCEIAQhCAFhvGeuPin+hicQ0D1V7rcr5lr8X1Oo5E1EguJS71yK+LH6rPR5krH72yODvcFR6jqeXmUJ5nPaOllVXvIyHNqgCo5XWpyrXPpIKJBVwMsAlVcePc4X0TB7drQnRQNFPpMseqpLmkl/KYY/UKWnR4zPGxA91lWH6bZDgF3hdBSdQstosKFdebydqth6eKG0lh9wmb8DKnj2eY2SP2fyiOB1+k0rsMMo/rKMxHy+T/7Jf5AWZLpRgY5JFcDomOPFn40IY3YwewCaBjq9TyuXMb3JKpxz/7v45CTJwZst3407j+i8j0SJrKL3f3TVwa08KvNLQoHlLZP5Wnl3fUVoGN0wO2PJB7Lk5TpHF7lDKHk2eVF676pLr+Ivnxy+79dTTWeqquko9V1IPcqtJMxvASOjMkiV8x29Uoz5rBo2p5MoGwlmXKXA8IjdfCmaRweTanws52NM147FU5309cNO7lVk9OW32+j4GZJquIWQyBr1NjabqmPJvb5bjfXosZo2ovwclr2njuF9GxNTblQBzD1HK2c/lPd1P8AzFhmblMFTYpJHUtKxfirR8bXcnzHB8EgHJLeFtGSO6hy4kLXg+ZGHX8J/wB3+0JM9+PhrfDbI88xvmbsa6t3uvo+i6hpfh/EDI3h7q5ICuZ2l4L3ueIgHfok82DA1hDWhJryW10Z8OeKmu+IZ9UkIjBYxZ50Tmt3EkkpxLiEu4HCifjbWcps0u88+KETiQAmb5fKxAboqi2Pa9T5LXOw+L4VY59Rb0nXcvHyGBs79t9L4X1XFm+oxYpfztBXw2PzI5G7etr6/wCGJnS6HDuNlvC3N/64Tef+enKEIVUQhCEAIQhACEIQEc7tkEjvZpWPnga7ByZTRIBWo1RxZpk7h1DV8zy9fkZDLij+o0o+T7HV/j/KzsrQZXn5VUm3UmTcZ0oPHVVsnT5ofWGnalzqK7zedWcVnAU85HAVfFd+H8rp7iX8qv8ADm/l1H9/KYQjkUqsLQeqYQR+r4Ud10+OG2BJsqwnkOS1vZJsaK6pNIm7QBVrn77df5nPZnDkbyKYrzZHV0VHH4aOFO57uypK4fJmW8kWHSGuqgdIb60vBuqyuHB0hoBb0ucyOJZ+do5KibC97rKtx4fNnqrccIb2RMWmvlzn1kudikdQqGW+OCySAnubK2DHc93AAXzzUNT8/IdTjV8LN556V8Grr3U+XqO521nAVIvc933KKNjpn8WVfhwXcEhStduZaquHUWq00btpI5T5uADztUeRp/o4bSWU3GMyGGzY5VQPLCn+XhFpPCT5GMW3wr5srl8mbKsYjt7gbWn03JkxSC0mu4WNwpCzIDO1rYYzHCNpA4rlZqcGL1qsTPbOwc0Vc82h7hZOMPY7ewlMcfUrO2QJes14ZfcNpDFKCDVpZPhRlxIpTPka/lpVSaR7TwSstPjFinPibLoJVlNLR0TWfIft5KVZExcSHBNms3FBrbdyFdZEHQOafZUyfVav4ztwpdGa49z28x9Ma7DfK5vIK3PhgBumUPdZOPLZHhyw3yTwtX4Xs6cSfdZj/wBt8v8A8Z4hCF0OMIQhACEIQAhCEBS1f/2nJ/0FfJI8GTN1B20d19f1Bu7T8ge8Z/4XzHCzW4WW55HQqXldX+OYY+jvgcN7Ew1DToW6U97mjgKxh+IsGcgTAD5VPxN4gxHYJx8aiXdx2UZn31061ec4xApriB0teONcqNjjyfld7twV/wCHHfqzA+yE1xgS4JRB9ycYYt3Vc/kdfiaHCjto4TSKLmkvwpWsZz1THHmF8lTivkt56XY4wApnANHyomztr5XbSXuVY4r3vaPLLj1U0cAaF2xgapAqZx1HW68DaXSFT1DLbjQOJPNKlsxC5zdXkZ/xNnukP0sR/WkgxtHc91lpsp5i4Zy8kzv7m03bLgYZAkkY0/JXHbdXr05rPin5k7SrB0MtH20msekBtXSuxZuM8fhvaR8LtuS1z9oTzGJ9rn3/AJHmv8cV26c0dgosrTgYyWhNAuZHBrCSeyrfFjiGfNvvWIzcNpJBbykc+mGQkBq1OoSB0hpQY3luPK4+8vp6/wD6z2sTPpUkEm4NT7RcsPAxpR6ugKe5MGO9tOLefdJ5cJsMrZYj0PUKn6/tP8z7FzJgkxzYHpVPzQ599CtJHH9ZpzXEWa5KzGbD9PMQR+iywZ31fjmdVdl2/c5tpbHMQArYzhsAIWH6rTuIsFUJgC1MJpGvFhLpKorYTSjI7a5XcSTiwleQ71H2tW8R+2JxvsujLi8n1G6dzs7YOlr6d4cZt0tvHUr5Xj27MBPcr65orNmlQ/Itbn/0TyescMEIQruYIQhACEIQAhCEBzIwPjcw9HAhfFtVuHOmYOzivta+Q+LcV2NrcwA4LrCTc7FfDeVnvrXRO5tcfV+bLy5Ty4heyyOSlcmNLBICBwp8jou6aAqZvLAqrXnyx7qzC62fKefELfazD1tN8J4DhaSxEh3VMYH0Qo7jq8VaKN44oqcTEOADuUqhm9KniePMslRdMaHGkLgLKawuHBSHFkBAITfEduPKbNc/nz/Jm3kWul43ovV14nI86uXuDGknsspq2U6WYNv0WnGfnNB8tp/VLZceOaM8+pc/l1+ryO//ABvH+Z+tO4ZPKxCW8Gl8p8Tv1TJ1F7mukLQeKX0mKYNJhkPTi1Lj4enOmLpg02p41yr7xOV8z0LUtVxHcySV7OW/0XxG3JcI5mlkg/sU/Ok6XI3dHHH/AGUE2m4EUbiGtDq4oJt+/aONZ/8AN7/+L4zRQoqtlZhLCkcc72vLATQUGfnuYwtabck7qqZ8GJeq+qapDj2ZJAP3WTzfGPl23E9TvdSahoWXq8u+SRzW+ycaD4FwGFsmQ/cR2KfMxPrd78mvnqEWkx65rmSJHySNjtfQhgnF06pDbgOSU8xodO0/HDIgwABZbxNrTGxmKF3LvZZqdpfHu358/utD4deJsFzDyAVT1nTQ510lHg7VHxyeRIeHLb5UTZYSa5rhPJ3PEru+Py9vysHLjeW39EuyH7StBqTdttKz2XGNpIPKlI67Vd2U4CgVDJkW08qrM8g0VA+X2TzKOtJHP3FWBJ5WM4kqkx24rvNJMLY2nkq0jn1fa1p7zLkxj3K+x4TPLwoWezAvjuiM/wDUIIz1Lgvs7BtY0ewpN457T817I6QhCqgEIQgBCEIAQhCAFiPG2nCSaPJA6jlbdKPEUHnacXAXtWX4bN5XyeeQNcGjsq2Q8ObRHKnzY3MyHCu6ibEZKtS57dUvZxTYDtpWYTTeq6ng8uq7r1jWhtd08vpGzldscNwCuxHkUlgJD6V6Jylt0eOmTJCFailBrlLWuU8L7cOVGx05rR4cgACe4LubWYxJOgtaDCNNBtLPo8s7k/a4bbtK9T1VkDTHGfUq+oas2CLy2G3lJGB0z98hslV35Lzkc3g/x/f62JMl73F18qP6mQHqu5iQaDf7KrKx1Xyocd91JFfKyz5gs891HJmNaBy4/uo34zi8ucSuDCK6p+I3X9LDNbmjG1poKzDrj3vp44SWVsbe4RHLE2iXUt4z9VrYsnHezeWpbly41l7BZUWLmY7otu8f3UU+RjMsbgShvUM+rljC0NpLo9dnieQCQCjLmiceBwqDjGVskpLqw1OsZMo/xTRVWT8V++R24/Ko2AeFKHmlv54P30zw8xuLO17eKK+h6Zq7MzEFuF1S+Sueb4TPSdTkx5A2+EvLPcGpnc5W11ZoslZbKNE8prkZvnwA32SDIltxWT2e+pxRyaslUj9yszuvhQBhvqrZjm3pLAzc7leSASZYA5pWMdhDC74VTHZJ9VbWlxJ6BU/hH7Wh0HCLtaxiB1cLX1lYzwvp0vmx5UkZaG+62a3H9p+S/IEIQqJhCEIAQhCAEIQgBcSxNmhfG7o4Uu0ID5hreniPKexzacCkJjdC7pwvpviLSTks+oibbgPUAsPkYrmOILTYSayrjdhJKXyvAo0unR7GXXKYxwgv5CrZzAxppZzjbe0rEnroq7C73S67cr0HPCntfC613HClifTlA0bRyvWnnqp2LSnOLLyE0lzzBCNp5KQ476Cke8yPAJScU/XpfgMmRNvfzabMgcAOFX06K9tjhOJIjs9K3nWXfCyegC0dfdVQ1xPJtXnYcjyT2XrdOeeppbyRne/SXMdxTRz8KtDpOXlu4aWtPdaluHiw051Ocu5MtreIwAPhHsTn8E8PheBg3ZEhJUj/AAzp0vFuCu+aZHWSVM0hwAHVbyG9/wBkbvBcBJMcrgFAPDGOxx8yV1j3WtiDmNS3Ig3ZBcSUWMxrtspHNouKGbRd+6TZegvALo+QtbkxtbHxfCXNyw012WSWGv5v1i5opcc04ELlrjt5WyyMXHzWH0gOWdztOdjEgchNNI6xz3FHgjgoa8tcCCvWsJ+Fy6OjymJ0zizfwqtQTy2OOqpbttUV0+UFtd1n5F29J3u+VM2P0ilxDGXGyrLR6wFSRHdTcR4xNcqtjyvbKHR8FXcoBuKFSxgd9ALbCTT6b4Uyp58UtlO4NHVaNJPC8Hk6S0kUXG07T5npPV7QhCExQhCEAIQhACEIQAhCEAJdmaLiZlks2PPdqYoQGSyfCb2bnwva+u3QrGaxCYnuY4UQvsC+c+OMYRZpc0cPFrLD5vthAKcr0NVwqVbX8q7Dt2j3UNOrC0DxyUN+F4G7uqkazbwEqiaJ1D2VrHIMoJ5VZjQByFJDLUgFd0tN1sNMjc4A0AE6cYoot0hApLdIt+MPdKvE+TLjw7Wk2UZ+E1O65av5OtwMJDCOEvm14vFM4WIM+QSbc6/ZSslywPtK38003mfI1TtRc4WSoxntBHqCSRMzpGl230qjkY2YZCNxC2SC603MGZG4A2CrgzIoxfdfO4ProXUJDXyrhyc8AAHcitlboagxzCd4B9rXEeRG8mzysWZc9sZJbaij1LObYKxvxt8h8QYSSKpZrJliEpLSKSfJ1jMI2hqR5Oq5nmU6MgJ5Opb3xro8todYdSMiRkw5WUxdQ3yDzHFv6rQxZMD2AB4JWawMeVXdi1J6RwocjGcOQFoMWCORo5BXOoYzWRGuqSWw9kvxlgz1lpC9EIcQKKmDLmPCsxQ91aObV9uWMEbFLE2yCvNu51KeFtytYOiZNcGmZGa1jIIy8n2CfaP4OlY9smXTWjnb1JWh0DGbDp7XV6nd02TSdJb7cRRtijbGwU1ooBdoQmKEIQgBCEIAQhCAEIQgBCEIAQhCAFk/G2OJMNkm3kWLWsSrxDAJtJksXt5Q2fXxeUU8g9lPA6qRnQlk7va15ALCjqOrFMY6I5ClA+FDGaClB91JdK3leUWmwF6z3XbjbeFgaTQs15aGEpvqWJFmRgloJCyWlzGCQHstEc7cBRWfrjfxbek2TojXv3MaOF4zCEbdrmpsMijZC7LoZByBaJs/+svhLWDYWcKaPAikyAaFHqpjjMu2uViOIggtpH6bZJEg8M4soD7r4UY0LEiJBbaY48jgACVYc1rxdcqnJY5bvWb7pVHpEDwWlopQZHhSF7C+Pgp40Bg6IkydkZpEk/ll8u7fTFTaFHFbZAA7sluoaKIot5DSCtJqD3zPv5SbOdI9uyzwll9ujWLz2yc+jtc0vAofCpx4k0TqaSRa0oY5rCHcj5VV5YwGgLVP2hfH7N/DwqjMaC913JibYY7hKYcosbYKW6hmOldVpPtN38xZxnb3EhXmtofCo6awbOe6YuoMpVkc+r2qrnBllXNJZ50wcT3S3JcKrunnh+He+Me5WlfSMBuzBiH+VWVzG3ZG1o7Cl0nnxK/QhCFoCEIQAhCEAIQhACEIQAhCEAIQhACgzIvOw5o/zMKnQgPiWeQMmSI/c1xVaH7qV7xhinE1zILOPUSlWJltkNE05T1F8a4cRiwu3AgfCiifYCnJFcqNdMvY4a6ipN9qFwoWFyHm0tNKYQy7VdiyiHcnhKGuU7HlJYpNHzcgOao3zObaVsyCw0SpvqAW9UivVl2eYxdlR/z4sNWbVR7gRVqNmO1z7NFPCW1c/wDKsljtrIyV0PGOW11PgKjjxYQb7r36SEyE0mlid67/APOrPlmJ1qZnjCFzakaQflLP5dH55dtHCiyNNic0k0m7Cz9f1DOTxJjyH01apTamJTYpJX6cGchx4XjRsFJeQ371/K7PkFwPKoOeXd+F6598KF8m0JpCWh8u1pAKXuLnSXame8lRMaHO68qmYjunenghgtW3PtVMd2yELoEkWnRRPb5kwAWx8L44flxtrhvJWSioSdefZfQ/CGK4QPyHjk8BDLWoQhComEIQgBCEIAQhCAEIQgBCEIAQhCAAKFBCEIAQhCA+YfxBgDNW3gfe0FYKSJzTvZwQvo/8RnMORCAfWG8r58UmvVWzOxPg6j/RLw4JxHKHiwVmJod3I4PupsPUZMZwZLy33SWdPnVz9aIutDatRR5Ecse5hBXQcK6qVnHRNdXIoQ82rPk0VRx3bXcnhNsfbLVG1Oq5QuxjVqB0b29injYelqYYcbh0CXp+MyY5OoteiRzOoK0wwWAHgUoHaayYH09FrLCiKVzuB1V1jZCPV0XX8vLXEN6rx0GRG1aVFK4tB5pK8jKLbG5MJopHMO7qlGRjkkrWVA/Jvuq0kw91xM0tNBVS1xJtPJCW10+bngqPeXFeFgvovQygnSvUMr6BF8r3DYXSA9lw9u4q3ijywnnxDd9me4NaAFFkZjIIjzyqWTmtiHXlLDK/Ik3OPCYp3pk5kyBJIeL6L634b1HGmwmwNcGyD+n3XxjDNGgaTn+Zy4UTXxvO9vsVsnfZdXnp9sQsZ4W8ZR6jC2HLcBJ0Dz/2tkDYtaV6hCFoCEIQAhCEAIQhACEIQAhCEAIQhACpanqEenYbpX8ur0t9yrU0rIInSSOpjRZK+e+JNTkyHOmB9A4aPYIDOa7mSZuQ+WV1uJSGrK9ly3TSPu+q8aVPX1fx/Hpaq00d9VeYAQuXxA9VPq1z2KuPJLjGxZZ7JtHltkYCAqMbedtcLyRjsd25otvst7KXlz8N4nlw6p1p72x1ZWbxMmN3G6j7JnFKLoOUd5X8emqbK0jqpo5tvRIcfIqgSrrModOFLjpl6b+bvFXS7hIBouSg5PBAKIcu3U40iFpu6Ju7c0i1y7b0NWqbZwP6lWyc5jOQbKfpasZTG7TwFnsw7XFSZGpyP70qT5vNBLitLapzhtWqXV9dlNM/1kKIODU0Ja6dDTbVd7q4XcsxIq+FTlnA7p8zqetSR6TTuVHPmNij+7lU8jLABo8qpFG+eTc66VpORyW/q+llpfO/e48KyAAOF4yMNC7INJbeqTPI7jl2K2/dJCXe4St7q4TuDyxpV2LpVyhv6paXluxJzZIHYr6r4M8UfXs+iyX/AIg4YT/wvl0WGXwGUc2vNMzpcDU2ODiKKNT+YXN/iv0OhLND1NuqaeyWx5gFO/8A1M1kvTc4EIQtAQhCAEIQgBCEIAQhCAEIVfMymYeM+Z56Dge5QGa8Z6qIIG4jHcnl9f7LB5mpxvwTGXepXPEOoDIme+Q2SbWKmlMmQaPC2Rlrp8oa+rUkTySlsshE9K7ju5U9r+Ixh5KseVuUMPYq9GLpQtdciCODnorLcYOBBCsRRAm1YEJHISWm/LP5WmOjcZIgVDFkyQup9ilrI2NcacLVTO0qJ7C4ABNN99Ut8fPcLodSbY9SvszWkAg8pJLpMrXXGvWYuU3iii5zWTWp9PfrgDwuXZoB3EpOYspvGx39lBI/IbwY3f2R+IL5K0DdVvgOUUmZvPVZ8yyN6NK4+ulYRYIC38M/2/2fl4I5XFt90hfqpoglRfzS+63/AF0v+2HcxjcOSLVJ7gHfdwlkuotP9Sqyag4im2mnjpNeWGU04HFpXkZHJo8qB0sshrlWsPAdK8Of0VPWUe3d9I8TEfkyWQdqbtxfLaAAmGPjMjYA0UvZWAKWt9rox4pmF3l0opDwrUl+ypy2LW5ZucUsh9NUuDlvcBATwVWnBcaAteNhnh2yGN4HvSvn45N/W6wImsxw082Em1fG8mcSMHdX9OzN2I1z+wS3WM7edo6JoStt4I1h0ErA53odwQvqLXBzQ5psEWF8C8O5DxIAHdCvr/h3U/PgEEh9QHpU/lU+w/QhCZgQhCAEIQgBCEIAQhCACQBZNALG+I9VExdGx34benz8p5red9PB5TD63Dn9Fhs8iRjzaGshq0rnucbVXSMBuZlhssmxnclWYsd2oakMe6t1crUeIfDWNo3h4ytlqYtuwU09+i8/lifEkeFi5rYsVwdQ5IVbEddWUoc4ueXOJJ9yr+G88WVPavi+tBAOeCmEAugUtxXgAWm+O5pA91zad+V/FiBPKYsxTXSwq2JGXVSdQRmhYUqrIVvgcw3SgmcdtUtFJBuHRKM2DaCapY2wmBPmfCZ4sLHEENCWkFr0zwpQwhFLIawY0Dj6mA/srX8swpBzAwk/Cihe0m+6tRkA9SslrblENB04Czjsv9FWyNA0x7TeO3+yas3O4BXL2OHUJv1U5J32x2Z4Y0yz+EAk2V4awWAlraW1zWDnhJckijYTTdGvHn+mIydCha4kXSpu06Jh4FrTZgBKTzkNPCtndrn148wu8kNPRW8T7wFA91lW8KMl4J6LdX0zE9nMTAWALiWEBTxkBoAC5lN9VGV0cL5WBL8hvBTOUdUtyeAVTNS3C+BzGZrN/TdyvtONi6HneG27/KLtnPuvheQfWtD4QyXSarFBNO4Qk1RPC6M3ntxX36W3yw4mTNjxn0B3FhKs+MySA8cr674h8MaUdKOSxjGvAvcO6+ax4jZ8ogH0tT3+07PfEnh3DPnWVtMPIdiyNcHEEFI9IxzHk7WjhOJ4SJPZJTx9B0/Mbm4jZWkX0cPlWljfDeY7GyvJefQ/j91skRtCEIWsCEIQAhChkyY4+rrPsEBMquXnQ4jCXG3flCrT5rnAhp2j4We1Kdx4BQ3itqOY/KyHSO79Eny/8NysE+pcZMBOOXrGlmiaDPnZ7p4iRs54STxrnaizKOJkyEsHQLY6H4kg0eGbzGC18+8V6yNZ1J8wbQvhNGX4zyuYrqIVNWMd1EJNfDY+n2NJdJviu5HKQ4ziSE4xXdFz6juxWowJACAtBjN3jjqsxp7rIWnwjQCjV58XBE4deio6hjhzDScR0W9LC4yMZsjDQWWEnk9+2ByInNkNqzidrVnU8V0ch4VTHcWOCD/yatBHLQphNI3uaUcMhIUps9kp08OWL5dR+VbOUxzfU9JZY7NjhQ7Xj+oojLJVzMna5x2lJMqQc2QrE4f+bhK8qPgncmhNFuY8WaKTZBtyYzgcpVM71EUr5jn3UbGF7+E3xodrQqmHDuddJ9BiOLOiN1vjyjYKXkgrqFa8jZ1UE5U+qqEp68JXlmwSmU10lWSTXKrlDfwmn/xEQyvhlD43EOHSkTfeuALK6J8cV+tni6xqeXgthfO57DxtJTfTdEl8vzH2O6oeFcEGMSzH0jsVr5dRjDPKiHRbPZbFTAayGfbXq91czaZI1xB5UWnBr8zfIK57pxn4zJ42llIaVtm8tzXtHRbDS9Wiy4Gte7bIBRvusl9K7btooxTJjzVyAsM+goSDG1F8VDdY9imkOfFIBuO0rWcW0LwODhYII9wvUMK5ciR3BdwqrifdCFjUMjiB1SPMJ3FCFrS9pO9XHi8Yj4QhYGK1DjIeK4NrIZgrIcB7oQgqBSw/cEIRfhsfTbFJJCdY3UIQufTt8Z5iGntpanTyXN5QhQrphzHwBSns7UIQ5tE+rRMMZdXKzX2y8IQldGfkOMRgcOVcLA00EIWKIZGgOPCqycAoQgVSyPtKUZPLShCfJNEmSaulTa0PLg7sLQhXy5dnGkwRvIsLQiNrWEAVSEKO/q+PirL0S+ccFCERtLJhwUpyuAUIVsIeQnn+9SNja10DufWLP9yEIXRPjh19bTT5XtgYwGm0tFpTWl1kA/qhCGVelaPqKAr9E2iAa0DshC2iLIa32CqZUTPuqiPZCFjYgY40FMyR4NWhC2NXo8qaIjY+k7xpXTRbnVfwhCwr/9k=";


            //string reponse = oconnect1.ConnectIDBioMetricResult(oreq);



            //diauthority.FaceServices faceServices = new diauthority.FaceServices();
            //faceServices.Url = "https://diauthority.co.za/DiaFace/FaceService.svc";

            //diauthority.VerifyFacesRequest oFaceRequest = new diauthority.VerifyFacesRequest();
            //oFaceRequest.ImgArray1 = Convert.FromBase64String("");
            //oFaceRequest.ImgArray2 = Convert.FromBase64String("");
            //oFaceRequest.UserName = "XDStest";
            //oFaceRequest.UserPassword = "XDS!test";
            //oFaceRequest.InstName = "XDS";
            //oFaceRequest.InstReference = "1";

            //diauthority.VerifyFacesResponse oResponse = new diauthority.VerifyFacesResponse();

            //oResponse = faceServices.Verifyfaces(oFaceRequest);




            //UATXDSConnect.XDSConnectWS oconnect = new UATXDSConnect.XDSConnectWS();
            //UATXDSConnect.SimRequest simRequest = new UATXDSConnect.SimRequest();
            //simRequest.ConnectTicket = oconnect.Login("ksulochana", "Sulo@1987");
            //simRequest.EnquiryID = 55139782;
            //simRequest.EnquiryResultID = 56889520;
            //simRequest.ProductID = 169;
            //UATXDSConnect.AmountChange amountChange = new UATXDSConnect.AmountChange();
            //amountChange.ConsumerAccountID = 193914516;
            //amountChange.NewAmount = 100;

            //simRequest.AmountChanges = new UATXDSConnect.AmountChange[1];

            //simRequest.AmountChanges[0] = amountChange;

            //string rese =  oconnect.ConnectGetConsumerCreditScore(simRequest);


            //string InputAdd = "115 CARA BLU, 115 MAIN ST, BRYANSTON - 2021";
            //string inputAddressType = string.Empty;

            //string[] streetIdentifier = { "STREET,", "ST,", "STR,", "ST.", "STR.", "STRAAT,", "STREET", "ST", "STR", "ST", "STR", "STRAAT" };
            //string[] roadIdentifier = { "ROAD,", "RD,", "PAD,", "RD.", "PD.", "ROAD", "RD", "PAD", "PD" };
            //string[] avenueIdentifier = { "AVENUE,", "AVE,", "AV,", "AVE.", "AV.", "AVENUE", "AVE", "AV" };
            //string[] laneIdentifier = { "LANE,", "LN,", "LN.", "LAAN,", "LANE", "LN", "LAAN" };

            //string[] c = InputAdd.Split(new char[] { ' ' });

            //foreach (string s in c)
            //{
            //    if (Array.IndexOf(streetIdentifier,s) > -1)
            //    {
            //        inputAddressType = "STREET";
            //        break;
            //    }
            //    else if (Array.IndexOf(roadIdentifier,s) > -1)
            //    {
            //        inputAddressType = "ROAD";
            //        break;
            //    }
            //    else if (Array.IndexOf(avenueIdentifier,s) > -1)
            //    {
            //        inputAddressType = "AVENUE";
            //        break;
            //    }
            //    else if (Array.IndexOf(laneIdentifier,s) > -1)
            //    {
            //        inputAddressType = "LANE";
            //        break;
            //    }
            //}

            //byte[] bytes = System.IO.File.ReadAllBytes(@"C:\file\batch_file.csv");
            //UATXDSConnect.XDSConnectWS oconnect = new UATXDSConnect.XDSConnectWS();
            //oconnect.Url = "https://www.web.xds.co.za/xdsconnect/xdsconnectws.asmx";
            //string ticket1 = oconnect.Login("ksulochana", "cal100");//"a9j8V9q");

            //string Response = oconnect.ConnectBatchReportRequest(ticket1, @"batch_file.CSV", Convert.ToBase64String(bytes), 147);

            //Console.WriteLine(Response);
            //Console.ReadLine();

            ////string s1 = "997";
            ////double d = 0;
            ////bool b = double.TryParse(s1, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out d);
            ////double db = double.Parse(s1, CultureInfo.InvariantCulture);
            //////decimal d = decimal.Parse(db.ToString()+"m");
            ////decimal d1 = 17.8m;
            ////string s = string.Format("{0:N2}", db);
            ////Console.WriteLine(s);
            ////Console.ReadLine();

            //int i1 = 632005;

            //string s1 = "9999999999999999999999999";

            //Console.WriteLine(FnIsNumericPositive(s1).ToString());

            //            string input = "2017-12-21T12:03:54+02:00";
            //            DateTime dt = DateTime.Parse(input, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.AssumeLocal);// DateTime.ParseExact(input, "yyyy-MM-dd'T'hh:mm:ss%K", System.Globalization.CultureInfo.InvariantCulture);
            //            Console.WriteLine(dt.ToString());

            //            dt = DateTime.Parse(input);
            //            Console.WriteLine(input);

            //            Console.WriteLine(dt.ToString());
            //            XDSPortalLibrary.Entity_Layer.ConsumerIDVerificationTrace eoConsumerTraceWseManager = new XDSPortalLibrary.Entity_Layer.ConsumerIDVerificationTrace();
            //;
            //            eoConsumerTraceWseManager.ReferenceNo = "123";
            //            eoConsumerTraceWseManager.ExternalReference = "Ext123";
            //            eoConsumerTraceWseManager.IDno = "8608301250181";
            //            //eoConsumerTraceWseManager.Firstname = "SIVA Naga Sulochana";
            //            //eoConsumerTraceWseManager.Surname = "Kothamasu";
            //            eoConsumerTraceWseManager.Finger1Index = 6;
            //            eoConsumerTraceWseManager.Finger2Index = 1;
            //            eoConsumerTraceWseManager.Finger1WsqImage = "/6D/qAB6TklTVF9DT00gOQpQSVhfV0lEVEggNTEyClBJWF9IRUlHSFQgNTEyClBJWF9ERVBUSCA4ClBQSSA1MDAKTE9TU1kgMQpDT0xPUlNQQUNFIEdSQVkKQ09NUFJFU1NJT04gV1NRCldTUV9CSVRSQVRFIDEuNjAwMDAw/6QAOgkHAAky0yXNAArg8xmaAQpB7/GaAQuOJ2TNAAvheaMzAAku/1YAAQr5M9MzAQvyhyGaAAomd9oz/6UBhQIALATsqQMcZgTsqQMcZgTsqQMcZgTsqQMcZgMdTgMjKwMdhQMjbAMaEQMfRwT28gMdogTtKQMcdgTo0AMb8ATycQMdGAT/NwMeoATu6wMcrAThSwMbCQTkuQMbcgTzyQMdQQMaIgMfXAT2iQMdlgMbRgMgugMcYwMiEQMfgQMlzgMdGgMi7AMekgMkrwMirgMpnQMm9AMuvgMhgwMoNwMm9QMuvwMfWgMlnwMh6QMosQMiagMpTAMhiAMoPQMkewMrxwMtDgM2EQMkkwMr5AMsWwM1OgMbwgMhUAMZ+AMfKgMg0QMnYgMgygMnWQMdtgMjpwMgdgMm9AMh7wMouQMi6AMp4wMkZwMrrgMkwQMsGwMpmAMx6gMq6gMzfwMl5AMteAMlSwMswQMrdQM0JgMqIgMyjwMtZAM2eQMmbAMuHANH9gNWWgMxiAM7cANojQN9dgMnXAMvOwMscQM1VQM+qANLMANS8QNjiAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP+iABEA/wIAAgACXXgEIpICKez/pgDPAAAAAAIMDAsZJyMfBg8AAACztQFlrq+wsbK2t7i5umapqqusrbu8vb6/wA0ODxCkpqeowcLDAgwREhOWm5ydn6ChoqOlxMXGx8jJy8zO0gcLFBUWFxgZG3WAiYyNkJGSk5SVl5iZmp7Kzc/Q0dPU1dfY2dzg4wQFBggaHG14e3x9f4GEhYiKi46P1trd3+Lk5ebn6Onq6+7zCQodbHBxcnN0dnd5en6Cg4aH2+Hs7fD19vj5+vv9/gM/bm/v/B4fLjs8PT5bXV5r3vHy9/+jAAMAKsFWCrBVgqwVXKplVSq5VQqoVYKsFWCrBVgqwVYKsFWCrBVUqiv0+0VSKrFWCrBVgqwVYKsFWCrBVUqd7kFxBnwVJiqRVYqwVYKsFWCrBVgqsVXGCEguYNmDBgr/AISqpVcqwVYKsFWCq5VX+UDFBPQWUHDBlQZ+WVWKqlWCrBVgqwVWKrzwV8FnBdwa8HrBxwOhUsqmVYKsFWCrBVUqvnwYMF1BYwYkFXBnQVPyFVSq5VgqwVYKqFVfzwVkFPBZQUMDPBBQTmsVXKrFWCrBVgqoVTgT4KuCmgzoHeCYgqIKj3SrBVUqwVYKsFVCq3pwQsFPBWwOcEJBQwWv6SrBVUqwVYKsFVSqulBMQQkENBKwU8E1BRvKsFVirBVgqwVWKr9+CDgpYNiCwgnoJWBy+cqqVXKsFWCrBVcqoVMgW4M2DVghoK6CHgSyqZVQqwVYKsFWCrBVYqp14LmDWgtoMKB5+MqsVXKsFWCrBVgqwVYKqlUkQTMFrA7vKrlVirBVgqwVYKsFWCrBVgqsVX+n4f3FVyqxVgqwVYKseYlOMP2woMRgfaRMCWUzJ3ycF4HrlEQ8xJNNpLs+lgP5aqepM1cqSbBvLRhzJXYUzpYQPKPrdZC5qEV6M6vKNs3a4qowZf2T+UbZ1JCQtennbJ/KRTn1vI824nAnylTuTOE0s7hDy0a/WI5q+lrH8xTjnDSdS0N8mYVGu0o+0D5JWBv00Ymp39UXUBweapmahSZV+aOZqdYdoFhvglz8vWiShm+kJevxERQnzF6dd6qrCeNe+rZr0a0S5V74UakmbFOgd8hb8XYloSkfLFTWZmmShjA31UTmIoWPmpjlMcFjvgbcSOeZYearDUw1avtyEEkm/QggD9oVtaKAHzUN3OvgUsN8D9OjcNrJDzUGXglYlgb7T1BNpVnkHywmi1MthmIfvlUxZoKvkZvlSXpUVRgRXmKl1CLero6i/MWArne9h0+YBnuoFN43mjHQFTlL80HakzcK1eaKnjFOkftLS9iF5QLEPPEEhxgXI1yx4iKJXNjUAcEcZLUw0UpDhIchsNRr7WDlLRGcgVwjS2USBbE4hSRsk8V2FM45DTGXESVJMN2vAtWQsUuIhMMaKiR3hWFgFzhQZmMBGyCmcbY0SnxRVLMqZgVEay0xTmKe46HwqTDFfC0JAoa/VEiWgymcXEDHEjKzwkUY004Ijl01hjgtl67ev0CByKHEQTrXjgRgilcr3LwtIlLEOaO/AZmLOlIVuQ1TsVS1ANgZWgoNqaihKfD17ywEcdgInnnfOOKintZuafovYGOCaanNuyhe1X6WKHXn1HWWubOaEkdAXRTn4N6FFgCKRq6gX5kkpVO/YwQhZFbJpTo07srjRtOZo21wkBR6K5zsEyyBaUMyjyplTdhRORuSpDGKk1GMroIjKAiEsw1zAF0eCKyKhMljWLHggDJ6lqTxkqIv/iHngOMHFFNEb+MoarzpBEeE+5sZfXuSvQwOAGBITLvULBquAeGTlzSKQlwGHJbNiuv6e0Bq9NohkAR4c2Z6XH1buGsy7bb65cVboT1yzkeNtT73aDeRhddWu+F96rQy7t9aq5RCq+ZV1Qhah68htepuCVQSCRDbSZpp1y7cjiajhO07bIqnJKwiCbJ8DBTfO9VkW3VoZF11IdJuT34qWWBTKoDjSnQAAYCo0jYWdro2HqPqbBqotR+pWZYb8QGp2GSuWaTYWpsxAUwMdgdjE2dISHowLnmbeaGOBFLWyuQQBy5GkO1lkBAhnDNJWPWmlByxyGtXWdqYXnMthAtA1uNKMim4YY1GYFoGUsjQ68tqZUO02BbWh8tErXspM4AXkKahKjrqOOIjgyhUkZAF8nX1VcEQVEdd/GlTwlrBD0NaQOJUL1NZ5zGgoCBxVEAiuMDSB5yzTUZ0rOItmb4uhz9vZfw3ZrNz0/F3dV5+CFPr+91any6ujmnygRNKFYIq0eVcMnIBh640kMvPy9yi6m2ImqNpGheFz9ruTpU2wxaM8S5dMTm1eVe72C2d1MU+gciqc+Wjewqtj1OvfPKZhJIkJd1JT2mq6HZUbYkYbVvSxK1XDYWTB0+Y8iXzzUK12zzHCwsWGh6Oq0znxytnToaabDMO2GtqdJ6k32w5xFxrVYe9erra1ySg0rYpJMLHKsTR53Xko7DaaWzhizTgRiceafqoBU/WhJJzOdop2YdxqLGtTeVFSxU2d/px+ZVew60YY6IpltCRwDzuPu7oIVJZWskizMmou9XxV5bGCgLSna9dNOll/v73uEpxMIFpC5pXK0vq8nR6TZ0IAbIpSqGOP3+7f0NZBF4kBbpYpCdvSi2Wg1q1ZEMu0MbMWxTpVJ0D5AIbsEi6qT34nGQGUM+GeOUkrzEHgpHV10yJUkg8NIHiWsg8QFrUr/sP/ASAHGYkAHiqIx71iweCJmqBwmWgg8BJEkYd7SJVwBZGRiGimh9KskFTKvJQ47qjwhkiSa60ioxCVjWLyGyIahhh15HHDJER01kTNjNTLiOq0IpesjTmI5ggpYW1qQ86VuYiR4JUu0DGwvSQ5kkUlgokVapiIpzlsYsTXpW1JGw29qSrGiRzJ46VxigFWDTMSutMwy1yQtJfoIey2UEKqTr3Wmv4jX0SMDHQ+9Mce6iTWzm/J4YFTvLAQx19XwRHq0GORf2vS9IqpAwFTL1k/d0u5+uqPFmd+v2P583B+yBU+m/YTVz/AHvUHoMxP5mvnXXlYYFypwY6cXS+X01m7KIu7pzrj1YEvrhZEmt1U4jxbTglodLhRaWMtMqIcAsUgVMVkH7WoqSptcrgS9acmXER6Ys9bqoU5QCQ9SmNW6NqeCsh2tPPO1I8ECCLzAoR4zCB9wP/AAOMIhxRYRhhWHDU+VahYpnCBpASDFteQeAMqzitBDzSMDgDjNlmUdbV5IkuTgKmxUkWLiDbGSY6wajNnOAAQba3OomYKSJI8UMyFAqKg04SNBpEryBheCgUQ6xVQlloGnfqUoUQYQMB1CI2WQhUPZ1HEVRdWaclSRx1n2M8H3q1sxgXK0mi2wqm++fRxS1yTrFD9PwGeFhdTPb3Eq1JwZMm6PSnOzHU3Tw/7ZsvZZsTa+zl3xlrkHHE+3uQNnjxfd5Of2zQzRum5rLCWwJRWP4oGYqfyuP24Y/f+6Vygd/8fe7R8cMHVzP4wK/0wOBVH291F3EVL93ZaxeOE2Xtfw3eZ9XI7/WNhzPev+/l4AsDN4IEzm87m97RocSaJksyrRusVNTr0X40nW5AgGRs+vsddSthoYZJheGQnAaUGSBSFCUDlKJVFPFrS4IQHggxDEbCUNAeGDxAB88QIH/UeMONMKU8QWKnZciAOEDEGokvV7Oq7hJ0XSrmq7u7S3KUqmUaWHqrNeWvJUzDUIkDTuhJIu2LEkSyZUqogwjcPbGbPhYZUsjVtv6UqrYNYtoESRDiNnrpthEdy5iCGM6dr1jZFjLkiWKlTCQ4BLImyxBqIlrAxI61xr2cU1l+bJ4NbWhlQNANnbmR0DYfc9fd3nBIg6HH1drqLUod+tvL3t0nge8EkNP1vejwpHHW78P2anL+jo67npp+mBLfrzPiZj6/Hm+D2/6QJvxbSq1U3/7QKP8AjrXnb9WDX9XXTvJ9f+Wdnez3f/2oTvenrY43d0ZtQlJqCfeKz/JxyKjqaGOy8ZxxMOKBTK8vgZv333sQ0McU0VHTd1oF4r5twPfkJ3lLRaAmHot2/QK3dmBt9KgNm7M9VkTUnxaf1NgaoFHOIdvY5s9sX9Tu6H1/e/FoNdXFcOu0Ks7+/tXVbF2nVlXfY7IUvv3PYDxdlU2nea3gTz9bLjFAgDA4CiVERhA4sDhrWDAH7iiD/wBwIIEHzlMO0QHhilwyC0ieGDknESTg4OCjVBUqwSqfgAs8gKFxFKW5OSFEpGCRyXyhjBbYFZhbFHGhl5pF5KjTSuMqnGE71LC0kb5FKYMM4JEQyGAyhrhlIJBWpIWRTNm7MX+Ux546ZZGqsrvYtPu3o3MJM4ZkuliZYFF/sXZzVIvzKFOftxrWG/EDe/m91uAZJZjU3drpbWeoN+JEfX+kqxydqOuaQz+V9UCYVO4/XMO/m0P/AJzebzf9vyOrdJt+yz1P5/r3JF/bB17Y9S6VX9K72SR68emj6v0/NoTb8T7Ob43w61e96fYm6Pch6WGSIccQJfRuaG5+f6er7Pw5u2y9SZeOJ8HRn1dL2NuTUXu+ji+UzrQGwbf5bntX8zacS/i+B91gWT6nJKwFWCyv/rvJe0MODBcbab2GwNeAqpnA96qTgxNsD/t9W/8AszSKdI1BAG2Ar1NrS3r+tK5aW8AVJe+QBicoeGoUnWCuIJEgJw+4P/QBSvjLPr3qpAHhjo73H+LMkm4a8WFad3p7oSsVlBFzdiBoKm/l+brZ25KockNddJU6B2Kq+xSQ8ZAyAdX2/wA7yrXu+Lp/Rc2teNdted+eBK8Ol61f6Pi/X/7jlmYFlNWf83k/h2PsQ7k7OanRrNabpfT+LOgWtP1dbFh0pk6WjOFgG6O3dwf7+Pd2my64UV6L1WBi8P4vhv8Apa1aoufrTyUNIIWGYdvOp3E8nd7ZJGzSrIqytefs9GXolT73So0MCpI6nDYRK90au768deiTPw1tzWqHHlmkNStA3u1mYNHQqvdq8wMcK4sGGWu54tDuai04RARsA14yTy1al05NdlKVoG3042pO/vZ7lUHdOddld5JpcL33Krx7iBYe2p9GexU0jMVD1gxNpQpXhAiqCTougZI2gBUcZpAkfpVS0g8LQynWL3MmPObZzGiFsGEReow1oZ3upqrSOQDELhTmT8jb7QAOSloLJNJmVfo9O6HAAQUR22hlI8IVYWX9GkFDw0m14VxqHipMK2cYSAHnhSY4cV2l3eZzcWBfDbm4tiBYKxlTHK4QD7P+IHQrPuQOPu9HRVwAN9Ht/wAWQKZUzRgWusgMgU0pZLLc+mBf/CVRKq1BbFCU9NLNB/V2ytdHoaleRivardt3V7Lw14IXCVP2QtRzVXBhN2+ltE/1KulVqueq1P6P7uV8ExNMf16nk6Gj+RkgWFV6sC0VV92jCqq9s9S/ydTVmZYdF9dyBj/h1ZmE7L5/f8f065FWAVyf67sCn4c+adXVRodT1/8ATLRYCOpvZ7nI8UiNOuq9g7fk7bbNzlU/Z80X4BMTQ0+X0K+rIpeOHMj7nr7BU7emWPj97/6Bt6+5WOq7jQj1JLknegVORJhTIqgccY77J3XHd3s+7g7UZgUl1gSCRuxPDPLrdiVKGxJaFgRvqwUtiftaChY6cg2QUzFVRCgMt5kzzHAbIohkYcaNBkxl7wDbAKRkO6N3d+cq5c0YZlW2Jcclx/a+n/HRw4N2RI2hSAmlnKuf3uaHZk9GFtsQvU9lfJi/P7cyE9doZN0Pm1+n+vY3e9GJAyRJdveD/fdzNSRKuCLCZf5rwVLcviLwtolAgcRaSAz/AJgRI8YQaKiBxZac50mMHCF/Xu/DK/CcF8E2lzlc/BLmomkZwNnxlePrmV7X0+4wDBkKHkwJ0DNAs7mn2/Z7r9CZWSzmfD8pWvlc7c8fu7HTcgbYu8nLgZYIxBV30JOxf2UkSNlcs3RK+eIsB88EH/f14/D7fZCYLKmdP6IE/wAMETA1FieT4ell/Ln7Om4LDI+xzP5+4WNLMFXoHz6tC+BufQmyuVdP6vogh1f6wJrlCeOk1pNM8WHrFaoE2BygaPj1EzR3lkHHAg3Jj9v1irhV0qp23v09UxzWQYYH6Hfv6P8AaBT3DxaFOJi8cGmO2OYFBAne/wBZcussFKssVm9eYgYSscdOwGmigbK039ZyWR4WTnJUCZAGwLJrpyVC1t5r1sNjBYEE4pOzKzQrkWKhmZkKvhcJFVJhRoHdC1NpVLtuapoznNrdduu1dtgqvLWlp+9A59StQhbBj0MMlGxAndr8mFmUM61w7D4SphX34cKl5SGJrR0fF6NzZ0HcATYZbu3/AD5Gn3Ly+AA3NbOR+H8fI2kDwmRzV9q/CHEAalEiDzgUtY+cKqUHDirO64E7h4YyYoZ+8qMB4IMpPDs+pz6BQGUII7A/s/AVYKm8fOmyhVOQPB65YGBT9srL9GwvJQqPR7eh5IGsrl4/vwLHoxLyG9jj7LvDAmFW4I3+L935UkC0CR7C5z4f1Fb4GP4u/wCzlwjamXFJ38GrPl9EsPA5emVc/b2UBZlpzff/AK8uu5/OB0eWp9CCAza9u1N1fF+r1e/m4J6IF8rSV06X5uTnBYAI/U+zwZ+HtR7cDL7tZWP+vf1hsLUTZ9P7sRjN3OVmQJ0Dlx+kpdhSdybAVV2fV1dmv8F10KeyiyJ6BmNIH0fMv3vv6JATQwMdKmi5Ox/T5ely4+w6NFUJ7LSOY+HR3PhqvI0ZJUqCwC1RJQpqYc66ozZiTstTKO7SoDQ6TpAQ5iU2Vnz2ouLa7EkZhUxNtcxtQ+uiQGio9c57adYY2uEDzTe31QM0bSVOoZQmN8twfrpIGSIsjbtm3YTFWdVuSpAubd9wn9uxu8jqTLyRjXPR8fpfv5EWbvMyl60WbnQN0EP8cvk1HcAIR1ippXKBSv508S+CKL3I2OpWLOKFDWm88CAPngtg+cITuuXjhw1pkn5t7b2Xjwk6VMNHcowbVK+AlIkZhvElzdJg5IkxVrn1KY/by+dpryBO5UZJUaejX8fiiltiu/flck68Wvg535r6LaCbOExkNUFeazx9zXtCS8AKct7lXdUFpI20ynNrniZKMkumnRy82WywXbejC6VbmMVDreTvThYHPwbvc22KFWuTAbL5V+ZmOKJT5/e6eiSYZSB4/goy+sgd+MvKf9Xr63vTrTKvm+l8HL3alhv16kJVeBjj2upJgQja+Xpen9nKqiXvxvz970P8ep0fn9xy8/F9n497n+HSHfiq5urm63d0Pl62j0uO5pL3dvjYNiuTYlYOtdz+fGfCaghxxxxkV6+hFPTyyVSX5nrCwBAEIzqo5mDF0NcAYQbSlHcbBmavc0fvfMw6QtKXROkbqe/P1c2XT72kvIBSqJPGSU+umb0p2ZIRYuYVP6e92mGN260W13Ku/wA3p/CVQTcOFAoyBNolTrntQKHPQrYpVwBk0P4/LsumTMseCwNKao5hUrhiQ4EYP3B/zEB4wMQsA4gpalLyAPDAjo6ohefiORMgRMRihygILzApLRa5A5ANASJI1K4zqWq2ILByGJW5J1qXkLXI9QoAijzGYpdtJBMsBWZqiOQRVti3QsAiBWwikhbBMbmHeIrXLOIpAbAw1dDXxkqeRBEte5FlU/V2el1424yCLJqEzZlhvxUmjnb1/MlXKxZm73M94kIh9sTzKkwUDgILXoh3vY9/0ewgd+ejqfDF630fm7Rmx97oUM2Lu0Yd+CX/AD7Oj8fM5eLTEj3XPJ0MJrM0mFGCToU3J49YJa5aU2AnZRcU+aSpt9qUOW0LAoZFrEABfdaZ4Ham3QQwIROhc6el9aXWgWGC/HntNHUSrq6fwGtglPYxeDn7J8UoEzK0jaETLKl/H6Hbr0VpI2N4WhPXl/0gW/kiWQUsBishQ4uh8uiy44iVqyhULYQYSYih4IqEQBIM4oAtX/kFsHjAsJDq4oJkO2uZnEfUM0XSvAI5Qgpt7Woo+r9pouCxcIw69fs3Kak5R2JbPXeVvLzYeA9Mz2aZLmXpYOzVkvrihRKnMrWvBrEXbGYJIVqIbl4p/a9W/KNpKVUddinYovawYPW+/mIsgKMDL1VTatnMh7/0dzdnsiZsUmEyJULP63qe3+DqSWBTsQKm3ueqmNDKsz2l3vk+JirMn7uP2eQ0QaPS5eHtz/oS0McNbn9jsfH63H9y5Vvd3wrh7a5B34oCmLqaZVu7+PA7N93TCliZgx1ralNftfBz+lDtd6TWWsQsrvNoi2tTe7DDG1Ya0ntvSo92qaVy4SKUldpaiPolniQq/PgURw2zskPJfTTE69opI8RsiK11QsdK4d3yVgcLQA0zRCFjIrwcjm4AyEvQBtUE/Xid8/X015AEr6e57W9paiQ6MjVZLL/v/o7vL5mldcdCskJz+t/L4q7uiYFBlCY3e6vT686Gr4QhEtzxNxkgRf8AmA8YBUtXnAKiAK+IDDrRjNUvggtSwxgkpbwgUMrTDOYSKyQcYUNBsimGaGUhcwgpRFzHW0LYkcxYMQqWk72KyBU5Ubk32tvUnBmSw6BWqaKkAoAirTD0JfLfECKnkdI0RsCl0VCnyrUgwYTOeAWFYM3uex2bz0sMt8t6bq4BDHE0/W/YykCMRJWfwfp+TnmXjqJmbvWDchW1xGMubmd4KEDv1TUR4fjTzvJ0HEdLVCu6dmOKBiu3Zdvnap13aZFSgsLAkPfYnTwadZLui5oqSNgDgahjYolXnEZKRKrIpI2UZkOZOF2dpyDaFoGbLXJMiTXzp0tVZEDLBCjujxrm702qtixrDOEdeg9XV7mk/IAkNaTYmazxxaGKMcl/Tv7ariYnmbEwMkNmL8s+IWQiI5TIx7R4luQ1XBUyVCGJfKI8ICNNPKwOMOMP/L//pgDVAQACAAMDAwcKEg4kHBcUGwCztQGytgKxtwOwuAQFaa6vuboGBw8QERKsrbu8CAkNDhMUFRYXGBmpqqu9vr/ACgsMGhsdaqSlpqeowcIcPT9AQUJDREZISUtMTU9QUVNVWFlaXF5hYpyeoaKjw8TFx8keHzw+RUdKTlJUVldbXV9gY2SVm52foMbIy8zOICEjOjuAhIiOkZSXmJnKzc/Q1tfY2doiJCUoKzmDi42Qk5aa0dTV3N3l7CYnLC03ZWtsdnp7gYWHj5LS09ve4ePn6O3z+v+jAAMB+mfEqKCvXqYvBJ3iBqEarvPDoKI0pze7dl0tfc0dzTbUTfPoq94nuNwvFbzOs4OZxO+7F4vIQcw6VdGpBefGBsTmTDUvIxvtvIqZG+E5pxu3yVfScy5v7Yl922JjhgKitMX5Kz4Y3unDbuq/Vt8ul+2vnveK25v4eu88MYO0MfzejT3M+hxO73dOuHkrb/u/lP8AQeWfHon8uPe+HN9Yx7n8n+7/AGf9v9eOjb44Py9H5PJft7/N/e/Lu/3fx9GOF9D/AOvl9G3B2z+T/Z/+v/3XWbt9dXR5WuXd+f2/8H/y/ExoVXn64xyn8s/5/T5txSzWdc/XPutr+nhjbJ3XHTwpJvjf3zFs8GjB3m5e2jB4Qbz+g7H4/wB59hseJQyrvsIWYPAQQ1MV31sm7wtgZ33Ah7ksliVyeYauzOkTk80zDnCwDlXNLJBDeJxPO52nNKtNLB2ysRLnUE7axGLzyUw335cNtdGM6l5J06YGm9yp02z2xmOrPfiMbuGn817Y7Zjr833qv49+9jp/Gb+Hv12p06erHlPNPw7ej4f4uHuejre0Y3x6fzfB978vv4jpv5v9P4Ynkvj/AD7en4Pfa3dWnmnr5an0eTzb+jqZZYE5aN+6pndFDG/ucOkxpVTpPgogd2bm+AiSovCeCYoMT4iH+w/cP0Jg9domNMeHEMVDB3m+G1XmKO4uhnUSb8d4xwvbEXxFPcrQojPdRFPPMnDGW6oTDylr2mCZtRXMJTNQuLvcPLpFNi99TyX2+91RpSyXg82m7kOid27x4re44T08N2ddqTdnvgSZx1s6Z+c7UtY6+nr6K33v1mOj88ZvrN/g/wB39H+T+v7/AJTP8G02+f8AF+TbXrVn5/J/L72n+D+bz3v1e9tx5Ph6X1pxmfj9M6fBuvn8F/yzv6eTE7c/9M+jbjOvg9zy0Rg7U403fi8fu/z+XFn+L8uF7c7TP/R/n/8Ax7noRnd+P8Ycmj/J1fyx5s3cm7x+jPlPP+f+/wD+v7/5/eqr3g55M/P/AIt8kOJb92XF54QRpu77NcIZPXqLoeu2p2TjkewkviJdWIe6JCkVBD3WFYBbvOwkMUEL3qyenewd0jOrN6gs8xF8aqCCHmZmrZ48C2wxdg0LUnJnVYtLYalh5L5szhiSjgYa7SbvT5IM2Ene8OmofWZ3f0ff9N78JnTGfv8A9Xv+ndXax4/R0puh3ObP9Xuf0+3p2sdO6vNHj82/p6H4en72Jxg9a7njT+TTpejh5seZ/j3Ly6e/fz7r48/8vwf0cPNfOe0xpv8AzV1eXd+b293o6Y6780/D4+j0+P8AP73v79JvtIeQ08m/2/6Pd2zDFYzeRMeaenzfhcRMBzzXwdG92sMziu44ziqasV4NxiL48TEmgPxKdm82DwGoIrw0th8CEClIdyZ1Tqe4pNiqhg7iTfWFjuEUwQveJhSKpCGDlCFaxUFqg5WoJgapibDyTus1VpJs4eR0ZWxhloijk230cYjBNjTuG3GFjRrHR0ycvUY8vTpU78RnfPTSe3uj8u73Z6vN5d+mj7k+/m9uuvo9v+bp6szpm+38G/0cK7bpB59+2t0YgYzh5NOvh5Nx0SAJz1OjjSN2cLB3KxeZluQ13S1RVZD4WKole+E2F9coP0HZgkP7RrDwpYHwidk7qHsgGs77DCQanuHqhztqyDJ5qSJLIWoOViodRRBQ8jvhgaCBgOWne6Q1YOmk5UZh6cRUVHXA9s0iurHD2/OLnwzje9tg3X6vLissYOm/Knm3um7DdNL4rmdrVaSWJYOVMQ5VE99pIalhh79Qi5PfWGJseufokGycYIfEQ2cV4SGFp8FaiEO+60h7rTE2Mg52By3EV4b5XkLU85DWRkPMxNlgLPcYzvrkFs8pUVC5Ta8nKOLOMFrlzlz3KsLMXuby/JuiulI0MS7fHpD22d3m0KjF6vt8e7fu3nbduDThfDfpznTMzh7R7e382/ae3tvfHV6fgzzO0xwrr978l04Vt8bG/R7eM+nr/Nn5M9PRN9PT6d/Kz+R2+TbPp27ZdMSc2OiXPQvDB3fNuuzO2aYO7OGFhjBJ3ajdfOKYk8MgTB4Vyk9gYNl4foJqPE5HiTjO8Qf2UcnvAQfEdbZO8QWbHdF1uR3EhCE1hysVLkwVDE8tQSrFRNnmKm0sxV4KmeWYro3zEkXY4Tvu8hv6POYtVm2+/Ln1bqjeYpjBOlztlZnjff34pqKiR5J34xnw6NBc5p5rpGM+GZaaE5RZw2LLPOYiiJoGDvM1DBkd5yXI8Q/2H9Fh2Pz7IzD4RhWXxLMNhO/WQEvdIwg4zpjB3N02vaovJ3KumGtImSoeVNIL04Jxg53SCpqbUMHNuzqpgkqako5JqHGmCCMyop7Zwv5vdb3hTSE6fI9u/p8fpyxN5YOrb+Ptle58H4ejQ0b3L/l/B+X71+0HR6SPN7sFtPb0970e/wBR6xG1qDz7d81H8vDh1xh7dzp+++bG/hn1D06Xv2ndjPOd8VXR17d+KA5DT3/TjbfEe+XvjTHLpHRHkm/Vbf5PJ72OYjG7Gd4xB16f4dDmv0afm+Dc9PCtKjbjlcR5d/8A9/JmdRbPu7/e/wA1/d3+e6RXcKl8v55vDD36nfOL2rwCRV7165Bsfmx64wHrkVC1VeEghCF7hCsEVF83uFQKO0xU90ylqEk5ypQrE4HurgoipQp5wtUJkBzM0WGCCCp5b+bPRcsU5U9uWWbUyJoPS8m7rzmNHApPDo657d8cPfrG61Z26NKs9qYmPP7nnIdJ6tMOky+tO/fMdHmhbzJ7/kxy9G2PydXTw8iX823Pfwk7WCN0799aY6T8m4LryBHR5jfMegzqHlMXmurr37fN7u707uuZ5XdHpm+7qvO7GLzzLHX5MdW3dAN9DmN9GM78GruO5NePq6PTjOCKO4V5/fi+Esd6a67zVk8AUyPsmygIfZmpg8F4mK0K8NHEPdYzi9RfdBfukUQxiDOe4aXQpZY07iuRYkzg5mJsVG+BgeUG9iCb3S/KUlVDNRersvbFNz0wMGI3Tnd7RW/h7/UaVFRjb6Y9HpntGKr3P5evokpBnp6eHanhF4rQMG/PM6sYO1PVuMQV8JDWl7Z4PWTHVeMdOe2vJv377xXI3zxEsT7uIrSr1yFVdnN2+fJvLy3jPbFNdO+bFS8jRfBG70benyfDfuDHlK625w27a7jfROjdtb4iucb6fi8+30A6D3Wv9l73yl724z/LXCq8NZ6TdqXxUVOycMn12ER8KTVTjB4TVeA7zBDF4WnuNqyc50k52M0tLNsdxvUwtyondB3JyqL5F+5hcqJk4Q8pV7wwzDLt6Xl026TFWqJnhHWck1v8ke/izN8Rv9+oe0adVNdN5to1u3zB6ze/5vP430/i0ioucJp7e7f+Xe9Hl83uTt3107q83VParq6dM9+PfxHCMebPo3cg9W7r6Yo4dHCGjRe0ycMRoVOhuqKrlod3WXv19GnwRmHM4twv5unq3Rhe5jdE4a3bzT38785PRPjqMCYucyRjF4x0wmdHc2xpL5S9Qndq+701NTSd4xWcop4SnY/sHsstV4pRZH12L2vfwOqZhO7IlgXutmAsxee602dKs6YeYYqqyFOdi6agC+c81EEyQsVjGjyE0Nq1F75nJehwCxTC0cudVFQBDLgp7edaRiiYqd+N0M8k4jg7bFbr6VpGj2iHq8pwo0qpXOeRny43enBBOZXR03iu0qeXy7cZxgjr6d03O3e7vJx0o+j4Eg5t8Z+aaqK8ty5XLOM3q33xppiWtHlN989MysKtd30oVosJPdcTE1AsPdnTF87sK98xwxSD4SZs/Edk+f2Esf3H2E1nieN9dPXVsP8A6CDsCc7Wo8AygQLYg56EeKprmqaRLDBiaOQmSoIJSiHkXfAikKUh2whvnVmKvvKeUmovK1NY0oe5naoIvdquYY301dYxuIHkKL2vkRMZ8+m3fNk1vORMEC2O8kDB66UIeui7N4/QbTefEkNYQ8DbBLT3jUKXnvEjYhdHuOuWydxghhCovJ3GtMmCjduvzFhWomsL3aYVLyaBzFaXmKqpm9zDySQlptMtYk5KtUTeYbb3pvyVHk377437ouxeGB5Jz83wdXDScyo2+70Q9vh6NDyXemMx00qLnbnbu6uFy5jNL1VcjWJu2CY0bncxNqu5xOd6eWtJtoVM7cz4IOZvt6jboXGtvcJ8x5N13fUacHmYnohmYqxPelai/rpabh4SqcReHxBs/CxZg8RZ/siPecgsQeIbL4GyWIO6ayw91riYe8cQap5iErIRIOVHjGAg55KlIoIIrlN+LI2qc2rnbqMEV5KKuQVfeclQ3qpZjSWMc5UKRLWGF50wYrPDeJ52JCGVsyvO4mSeI5yK00jFh75dTJ8I3ZoPXrU+uIbH49lm0+uF9N1eJ4Y81/RXhmeiv5j3I6vLL3vTp+H+v7/w7irncny7ei/j/wA/4uHjxGHmOv8AB6fh/F/u/wDd/R0/09VHO39z/o6f5f8A3/8A06PT797PKWPz9P8Aq/7fwdXvefTx+cg7ZGifij/4f8/+r+L3fPe/R5jtvRW6vN6PJ/3eX8f3uFIPNt3fzdf/APP/AA+/jbv6esxyVPDq3/6PL/FP/wAfe3Jui7y9HDyRu/6/8vt/09O6o8vA7bffmRt9H5v/AB6S2fUckwRe96z8fv8AmLvn38rGJdGsef34m+NHkJ39Rumup8e7ymg8jFO3Tp3mN8533814N/XM+89UBfG3mky3bs3rjyaE81YrzZpw8+38c5rzacM9I3TE/D/jnfv7heWOjb49/wDdx0aHON9NJPHt/wCz/D1T3cYOvSv73/8Af+T/AJOGfeM6z3/g/wD9/wBf8z3hjq/j/q/9l/8AO14Hze/nf+vR8U76L/pVc9gk0w+I6t/XW254Tx9HvePr8z3k3/Bjo9v8v8n92+O63r7/AF11f3/9v598T3JfTjy/B5P6/wAPt7d2Hm3fk6vd83V55/6v9vR5vbuc3Dht0+Dx5/11/i/r97x7eaTzn3vx7/L/ABf91f8Az/1f6v5+o5CPH7n/ANvJw9GP+r/0sGn/AKv9f/j/AN72zSMej/H6PG/0/wB74f8Aw9v/AJv5fHy327Z/qmb/AOr/AJuH/wCf+/8A7P5/heTH4dv9X9/+bp8nwff/ANn+ijPx12y7w93yfj83RW33f9ce3Pw4e3iL+f4ej/l/9unwfg+/5J4dXI7tI27fhn/Ln5evTEbsY5au2v7n4XyePfvuHKHl6cadX5cbejfBTyDp0ebSvMVtgwycjFHDbfzebqroxw6NxynTnOeNDQ3G28HK1fhnG+ccPJ7rmc+HpPPvCPT7+4vzrXUvSff/AOX+P/N+cOcXMz9H+z7+/wAf3zuEm+cf6/8AT5P6o8td3e+9/k/5/wCf/X9/307rE6f7f+jy9XSp3069Jxd8WCJ+0t8T6968fwZ4weCsR8Pv6UUndn2/Hp5dPR5DMe68NODjx4zXvdO7/D/g9Pl6/JHjvg5nPT4d/Uw/BG/oiuZnh19H+Tf1b/J1+9pPM34e5J8HDymnl6875p2yujq9v/N09PXn04HTS7yMx5vc4eeqiXG0OTE8Iz+D/L8L6J03u+J5CNs15fN6fe3RLob5x2xiY3l/Tv4b5gxuO2xNuneTvd5K6S9pbMuNOj4OmeFKcrDpOI83lbxuijl3QInXjhQVfnVxpvN+7E2xyk4q833ROFgnmNsTuid815Zc+dqmpjc7vhmcHPULTvK6OFUc5LuMRNea+/vVMSbb8NPxh3Sppk8u3OHv6FTjEj4Dfnu6vyz4nPfvN3sID/cWT2M8dEaV4t3X0vw43HgK6t3we549L37yfm/w6fx9f4/wXp7uehp/Fv8AJ0/B/P1dxxP8cdR+H044eX8Odcz5d3mY6PP0R7p/T5+fHwdPXu6sZ9fu8Pg2+5u5W89Xv+iffff6893n3755J0/m6oOuL400nbwrTkY9P+Pb6Jzxj3dNOnp258nDPT0P9GHp8medb9Irknde5+DO8dPVfoI06uS+cTXo90m/T1dRpJyFY3Rv0qeGM9G6PJMxXTRPoN182LvItQzv37uvghBzsMudtLlLzFyHdF7ww45i5VZtJFYgeVCM6IxjTbhrnDdFQHDe9EzzkYibcOG6t0nOkGD4fR9736x3hov+D735dvwzD3b1w6v6PTPD8Wj3VuHR0dfl0DwbTTBL4tC94fYfjj7M1PsY3xt61fBWeZ6NvTn30x+T01n8PVvie6MwPnKr4NO6dfojSjhp5fveSeea6oal8n8/DyRTyulEXY04eWvgqZ5Z6Uz03Rtii8S8tYz8m3Tqrf7lflny+97z2zTDw80OfDPH5vx3xPN6HbFYvw2/9Pj2/mxte2z+D+nfu/KXvd/yfyfxe753tmL5/i/D14xppn+H27/1bp5OrOuHT9/73vUNV5t2N52jHVHu+bH93r/n2sdFzpv2xxnneN/4Tb5a3wg9pierp6/Nu9GfkCa0cck4qJtUzebxTysNA2uytS8s1Dho2znu0M+ZjDeL2fb3cJ7nCHhfdW/PTyfel7lFQX9/0fk973vJd5ma2/B7p+eMdPp4T3c/g97fH4fN051Pdrh5Pw6e7+KtNO+7eHm/Hje3HwT07onT2FYP3WT12QWTwMqyYxXfcXhrprdJ3W86BHC2/ulaQw3jrke5U3ry1t0jp6t/VPMxpE6Rp0adee/y1zFWoxuc50z82bzU7tNumNIIrp8257dX3cJulX3334jByGCXFyujfi+k778k9b7vp9+tGp6erdvN/bIn4P5P5/LEs42/l98j3Tt7+F/J/wBPwXqXdE7b+fEnrGI3eW07/HPm3dRt0l7eDTSGfzme6+7O8YO2PmmYOmfJoRuil5HO8b74z6r2aXllgM16b4bTLyDBwinGZG6E5pmt+CLvV6PT0Q8ph31Wg4oz3D3GCeB19J5oxPMj4+Gen4dJxBR3d/8Ai6Y9GZwze6tae9OfRXhnq28Gik8Ilq+I/GH2AIkPCY6tOnHDR8N/J70+fxwvfSo64l8u7umcdWm/g+/O/S/Ovp8e3q4V1MsYOad0Ya6MO/q3bSuVsZ4Okis48/mHkcYPe6ZvXl38NL9HMOnUXznpkz6OiJnkv5a4bd9bTOd9efzdeO21v6NOrbievSbZ+Tov2yN2m6vx6aTp1VppXmg7e7rnovWjGcbfez6Zh7RvryVj3Yzd+PN19Gnkqe0JHX7/AA356Pu9OjbHIw54l4Z+QvFBzLoQXjGLwBzJEzJVWkxzN7OIwVOMJPO9UTE2eglOehMjGJrgc84z33xpvnbfydGOdDG70aX3dd3gc4dXj02t+p4Sd2+PT4+jyTW9nvN583UhnFd9vtm2/wBdxTB9t+IjMPiRnr6ZPAyzHT5s6fBjNvWLB3s+ommDrvjuDnwgtfS/DbXMVPC8aRdL73N5tL42k10ueLu+eWYkXgQ1BmvIRpqJNM3Nvg5HpeHTNENYwXp5Ho/n8mJtdgx0dE8lX8/keimZNsV1eTg9u/V44z+DbeHE7d/Tv8kvrFbtNvVLffnY2tbt52m/VvjHRvqdLs1CckxecadPVnvnrvY5SQmsXggaoeRiSpiW9EXh5Ss8YUhziqDlardRkZx49HnIxpK56Yz09/ec9WwZ4d8Xc3nmPRfPzT5KmvbDnWfJ+S8VeA72M+vo6M8XTvzWfluRvge8yzOIfXYbGx8l9gbaFeGp0dL1Q9+enf57zVRXePf3dcDGdjunT+N37U3XqDnvjP3vJ+Qq/lvFHM39Onl9vPbOgViuZnhnPv8Au7tBmheUxeN/l/mmJ2wEvIML6Nu/ohmL5y8rnUcNK275dHOjkQiaznb1jioxPKJG+joxLNVpDyFRfftjyTa5bccpGZgnx6YwWnlVZq3kiYcdzEIw7S96gTlYKskNhDlKlp4Q5BzkTeAbZwTzzG7OLw1iJR5hCptU6VZ52gtpWOuF7rEyu3/NXU95JvVP5PRvrwKRnfqrwMYg3ODxVjbpj2DI/SfZlb+wZ1ujfXiz6p03l4O/WLVuirz3Qjr/ABVu6NJ3d5weefJfojTgzz1npuo379M+F9w81Rjo8x56c50345iNL6eS/oJz3Yja8pfT8lZ6UU5zfODkl6d7HmzqKqXfpzu7O+/yN6YYJ5GKqY+HdEt4xi/KxdM+G3q8cmHfFcuYmher6YIb09uQCxiZmmmuQsRLBncpxzgSsO4zIYOVEvQCajnwLnEstdwjqWcidu/uNicqp8stHNJneGHh10ZvdGa6tL9G3Pd3SG1b/f8Ac4T4GCam+MHeI37427tM3wMxjfUj4ZNOB8Q2QFMPiYxNPiJqYoh8DhjE4nvpVZu7BVnuGlXxhjhjeQ9yaiovGdXiu7eploZxpI8y4jPOajDjqg5SM5vvx1ea2mkrzCRfbVVC90qJ03wZxWlaPO03muEgxPPKTi8GjBTzsVBhktMSnIGtqAmxzs5CLB32CxZ7xAyTMwJ3mzZsPOaggnCd1bNiGoO4TVmFIp8KwI99ipNK0mfCWzKu+FdN/DHrjvzmviC7HthfYWiTw1QFaJ3y3VVtsr3Vje7r00HcZ0xGcUSjXOTB02lvdiedk6qnqq1ZVzMy8G+Kahg5WKidtVfNJcc46BDirF5nnIwRJFML3avObF1ic4eWkh3uIu00vO2TdSUUS80zkzMEHcF1JCd9FJg1HOwgGfhHXRD3gWFIwL3G0wQzD69I7Z9cnFY8LFYDG7PwrM3s+ENOG3rPXSYT4j+meyfoBFS+Imo3aOHwN48e7zbY0HuF+uL9Hl663Xv3Sd+MdHlvvKnvbwxuq99+JOfOMaU1wi+k929O2nf5y++urR5THXeRqm8UTpyzv033xKRmnRJy7orq3Z3iZYq5TyFX34lvG62KzOYMPCM6lxHmbHbuxeoY34lvV78s784b1T8O83XZTtmLEtsRiKu0c6DtgrDNcqQxLReoCKeczguaVDFPOxNEtZy+BqYozuVpPdBiRF6pedm1b5i809wq06cI8vkrD3XF4nStudeBqOkrSk75U7+jhj2CRrY+tew2C/iIuRpu3J4JxfTTdvzwd8Juz8GefeJxeY08e/yeN5yCKia4QOcPcztRFdab9J563RcYPPeOETylVdhmxGem5eQxBvyCKubTlMZxvvDPCcZue6uQ2281TGKhredEPJnwx0tyYfHjOzyTUu7DO3Tdnub6bXkqNIai9e9HRjPfnXIrOm1vV72nE1yBU4zKqpqKIeYk0GBZiudmZiZZxNl52BIbxujh3GMwCB3Rfec96cDFUR5eE85FebOr8FMPcq+8v11v03TR36weaSKO8w7uui54rxic8euUMOx8fi3oqfERI2lO+EwGT3abtZ2qKe4tyWKxm5hziCl9zGkY5m8MxOZwbs84mMXtPToVa/MEZ0k4Fx3GMaQLGkEwctRcYwziWormBYq8VnuxFyDkUbzTOlQBJyrhgamRqJ5wgakxDau45EVIQ94GBqHwKTabEHcSEgQs87NiBioE7pxDfvAwsXh3vfGJsEHgLXhvjvgrnua8JeNN2KfE4lP0HY+LtmDxF+BpR4sTv8w4vXfMGmcXKl7pjOC+Kvc7y1VgK3nflZZ036S82fDdiUg0zJwco9VypwDGMY0nlrhnNyzOkTm807tMQ5ZjhxzX3lhId8xMHIYnUEUMMJyTUaGN6mkB3GBzxdog7pBTC3YB7tFNRRB3yzYsHfpmGJhe4hkQZHdAgbMVLzusY3Vd7xkRenyd4SanE3inwYmJrdWeDvy1tyfC1nvtPryz7Ke0BtyOx4bJse3IbI2TY7pA7HVEiavRYSE2OCJEs0QwMDsaFNU4lEHKWCHYzIjZsVLBV6gK2NQmRCWoWasVqNjOJBYbUMDehLJ++mts5M0WmgpmjI/hdTrZqJSphIl/gmjJMhhKEsQ2IZ/eqUgRKsUQjA2mr0lB+45HGI2GyDKMw5H7rxsJrSEYnE1Bi7AP2kEyTUUVAVNROJhFgh/bGGXIdY2HJz0m0sM4IIf2kyOyCZA1OAqCJ1UQ/VLFDEtjsGJciEtUZ1qT6hakyLDDqYKsI5EM1ODF6LH1UsLDxJCQjBloFDOJZbP7I2ohszkNmw5BQIyhQftGQkOsSCxYhyqASg0LS/WR4pqCGGwlSwIxJGG9WYE+gJDrNbqE1ENnJNU0yn0kdTNixCcUt2EOzNZ4Q+pRRxkPEGoodQMDCGk2H56WHUeow5VU2IGCmUnErKfPS0sS2Swlk7EpkIwzWV8Qak+YIkJDknqjaaiYRycijKWJr5yca6hybMImsyqYEhmrS/RBNYw6zspDYyWalhmECsn9UyRE1mRZLDagWw8TaSiz8xEhnIYGCwwJBZs5JkkECMp9SUSxBDkMDkkNiHIh7A1NQPzBOKcjW6jUUQ5NhLDeshg+aIzWtlgdYkNh4mbLEtpiiEg+Ylqmp1PGamyJAiNhhYEhIdY/LSEhhhHJgcnWkUNhAKGYpmiH5s4nE4lAosQjAkJk2TJhCiaIbYi6g/LKNJTJghyRsnZqBq1DFTVRehlgs/KRiYKmGas2eNNZxFMvFUxKy/KZhEqbFqLGRxCQVdbLBJUrKMXhgH5BDZYYnF1LMCahOyw2pIKyxuZbHyBFuBUzUwiQ6nI1lkhl1ORRYxGJlfjkEF8N8UwVLZ1uoSxZIWChIZir1Ok1Ew/GAqtvCM9MbmDSZhs6iyOshtLGeMSQzdxV8Zsj8YWrrfFbqqdJQMTk62WyGpliVotjc77hMS/IlIGJi9Y3NXwg5IajUaim6GG9TWc6XiZ+QQxNTK33420IJa6ak4lgWYCKnSYR3OoT9NippvhK028M2s6mEhyYIYYGzOTapiaSDLC2fitpswXjfnpeHOGrwzqLOsgciGr4hvGAgqWamn+42lW8XjS6kN9L2Klh1JrbLYYbxVZ1gYW9CfpywwyYvSVJRWaWEyRLDZ14gEw7oRmqmz8UYrEZ1W7SL71WM8S5FAeqzBBghoasFS5H6Q2d06RnwZxvzrKiL41FFQOQwMEII73UEUENEH/G2q9VnfgTvrow0ReolamKLEOTMDBEoQGC8TNQCSwf2xRveqzjQvpa8Z76vATFblLSIkMMVMSTCWmEcEwlj/jQZ00mcRnTCE76vglJEhFhTjxMXnOCTFsM0in6ZUTjEzpumDOME4M2s8VLWek2EYz0vVjKZpnFRMM0zJ/cSKkbmmN2ixpE3q8w56aXdM2SrEJQxU2aU0sWIqZMn+2wZYic6yJUm07qq8YgCiFiWBikySEmgsQowf2yzUTDV6BomKm1RuxoUIQWFlzqQhiWSkYIwzMB8VJbE0XYGw6AVicTYXUQRisSswjaYqFJEh+KRV4pYGCCxN3DF4vjE4sQQRUXoi6ZUFE0WcRXxkMVmSw3pSKLSNqrPe3ptNRfDdGHKYSKyqyQfFGJMCwOTMwjS1E4TRzWWC2DJckyIJ1vx2zYXIUlYHXoXrViCAhgUoZoYV+aMLJbOKLBBdwt+NhEOKYGYNTk/HK1JYjANltUX1IyxNSQ6hg1JZ+UWSxAQIVBDZiYamGCaSoxAxPqpBD+qMMDAQisg2mxaixORFZPGLY+aQNBUM1EiwgsVNQxJUuohhgyGzDZ+aaiMSQmTDYgSAiYnWELZg+aaiGwlVYs2CGC1EFEJFLTCmQ/VQRSw5NGhkRKkIJMDAwwkPzhhCGpYIRyYvUViFW2iVUEMTDY1Hzy1aiHWNkIm0zBrkrJBsaj57BCDlLCENqgYcTUCzTJUCz9VyYqEyRKDKpgnJIqGi9szFln9cKgkgVGCrKVGGGyQX1DMabpsWT59QWdZYomCakIYS1ETi9BbdWlEw/QSzRKKQQVRFQ1FQJLDeFAb4xedzR89sjFMkTTDkgQQqJMLKEUYjc1X0HIKylqEgLIqJeBFLC2Fzje2F/WEsMVEq2QIYAakLYGw5JembVJ84ggqGJhDIiqs1iZKliVHJMpxfKWv15gKhG1UzLOcTpBKUkDxkVcL1BD+sjFJkyEywNTEsJBiJqYbEJUtXwSwfrDkNIQirDGGhhuzi7ZIWCs9L0XqB+eQJkuRkXSXBkaZ0QikOoxeoH6JlJMTUtQZxiWzU5OL4gbS2nF8XU+kthWoZbmjGJgimJYCpqEiaKyKP2CKibo4hvGKmpCaGalEsZLfF6PqpJGM0ghY0YxdRmKsOosP1whtpfEl4qjSEtVmRhOOUT6ikVDhjE1epNExnRicFQWnA5H2KtTGKjSazwRwtN8SVkjDgbMJD9Z1Gp03YHS9ZtMo2GCEf3AiSJZmHTdpMYlN90q7xD9siS1Bgxer0QRRZWCDJH7chGCMSzpJFTGCoIG0tn7RYJtoU7qakymCjWj+9LEllG9QZC2CiyP3JYIViRtMVKkOQ7GZyoi9EFgew7GhYqbCFTAkGxubFjJgskI7HBRyrW7HBsw9h9oFuv/owADAfaCEJ7BsaD2gEUdjM9x2QLsaX1nZ3vbfVLHYPtmyFfWXYyviJ2PbqON/dF43lYdR9pYNkQ63W2bOx3djO+o8czrf3175Ox9ceo/uvEcmHYzGt/4A+xPG7G8LHeOwQfdfZfuPsvqHz3IsZL+i/Wdj6vGeE42H9tdQ6z2T6h/vvqGs+k+Isdk+k2Mn1X4j9d/4X6a/wBp+uQEBymRxL2Cx9QhMnjYeQ5nI+UZNhg77BB6hDrP1U7DBqYf98yPlBZdZBxnxFsFix8pOJsQlhYO85BE2SBhPluRYs6izZ5FyMlsNmyJ8oh4h43sJZs8TZZyRswUQfMqEMmxxtg7bYyCCCGCD5RZILEBag7BxLqDsBqSzkfpLDUEMGtgsp2mGGyTxMFpLIPyRsw2oghsQ2O0aiR1TDMNiGSfkYgsQxNQ2ewvGOo7A5GRCqfHkxBEwwQazI9QhclhsxcYoIYbHyKaCgdbqH1nibFmy0jCxUSHxzKpHMQg1BYYIOJWrCwQTYsMLY+O2SYwxVlDJ1ORqVyDJibwKLDGZCzY/SNWLimRFWQskOTavVYm0mAsysnxiCGJKbDVppkqxDC8VJrpggiYBi9Lak/SbMGCM2G83UIqoCWELEBk2zCE3hppdAhlqbHx2YqGMOEu4Jl0hGKVsut4ikGM8aTYCwfFBaznO8tQyDQSMw2YIAGzDIEUTciqwiWMn4tE06Q2CDADUVLPGQ01rIlhgC8sGlQoHxluWYZsDF1jCLuLAwKQ62mBi5BMgDCnxQIbyQyzMysEXTBFZGtg1MEMVfocRVRUXmQP0ls7yM7zGmeOGTUGNKlm1wstmwQRNcAvUZtmor4yGAXEl5WeGINLYrfiHcFiLwzBqFcRMb6bkkTLh+MQG6MO8mGYxe/Bxpv36MX1tmikgvEzoXhjFATMxXxXJcr1AkzbdG8rFYqo3OmIvWRMLDUkKXDGBqQCH4pMOkFyFm8sIkaaYq+dxdEiiwpiJauMBwYxgYCH4oEaQZlcKm8VNQuMb9HpzyzkmAyAJnE3YAxarYcn4s5VDN7zhJrOb4rKcXm2LVkzVFYIwxnTe8wDamH4xDCIRpZnG3dO+jduvt0cAEBBAQSVN6h0mSCZnIYP06CJqBrFurquNJ07sXJnEVUNBAwMXIqjQL5yQBavjsAb6kdGt5esYrGZhoglJgA4iwFpCQD5aBG+JKc9N94ondFQXvo4xGIKYYlBi7aduUlUFiH4yFsTM1jbpQUFqtMzciUINUwmoqGYkcQNn5Iit5xNaReNxpDeaIqGoahybENWas4hkWmH5WGM1Mr6MTBMJe5MsBFQ2OMVIuFxLPy2ZvGiRvmkjQ0mCsNIo2NZkQRI2AT5ZBBMOLXXS+L1GCpuoSw5Gp1zAUxKFn9QGMQ5zOeBImaDCg1YOIOMiYGFD5qoXvvla3VfBhLwmthik7DrZgX5wWq9iavExMXahghLMPacixB85ioZMzEURiFh4whPtrRF75xvgmJs3kUtNiGzR9uZMVN5WzFWohh1LxNiD7FKVBiEYTVJrdRk+qfQYVmZiglMkJYlish1PGfWCKxAYoobEVZyVOyaj6xADicgdZqJ5n6yRUAJk2CDJgybEOTYPtBA6niRTUQ/dLAiuquScn91s2Zg1BYs9g2MrZhmCCHI7JD++ORYooLNjWQ2NjM2SB4g7JZ+22Wweo+o7Ghg7TsanICz2HY4r2HsORscH2gEFdj4HtEbCe0Aw4jsczsmx3ON2Ow8Rsmn7ZrIP/MnzR2PTYdb/wARZ+cQJB/wj+0+oeJ/ZETUZIwnxH9g9Ug9g1v1j/ePskCcTDD4ivpNhyIISw5PgSzLY+gQakdRBk+JIPpms1PEJzNn6hDDYgdaWCzCPbON/XTIeIgsWPYbPzzJOMggioIHkIT6qcSFm0sPgTJ+i2OJNZCeu636CahsjYYYTwPqH6rZE7LY7jDZ+yw9ph5x/bIYYdZ2Dvn1HJbIifoth+i5EDYsf2Bs/RdV3JBsbHNhGGE1J3ziH6aZEGx3eNEdjmwmSQQbHIyeN2OZk2ILI8hsaxh2PAkDs/zZAux8PaFo89oBgzUfdO4amHsOx3dj6Gtg2Oy6nJ2Op7QCArxrseCCHY2Gxre87Ih1GxyfUdkI7IBhdj6ux0NnK+EPaAQJ2Ra/rvsvaHY7EHZDY4rqXW7Hpg2Op6rAfND9EOwdk+gup/8AIwv67BY/8rC/sH/nNb8xIqyZNj2DjCGH9VAsk2cgfVPWYINSEP0CxZ9ksQGtgg+Wa3IioNS/2n5zZhyYP+AyYbB+q0q5FiF8JxBCq6j45x1kWD2WxYmG1MEPzCxkQfoPEEFlsfIVBCAybFnlNZ6pDkfJYFISHW+oes9kJIeJg+Q2WyK2IfYAmYIbMFg+SaqQhhIewdtXUARVQ6l+QDBDZYbLCmQd1i5ZaICKiT5DkWLNmoas2Sx2TJhcMKtpCoAs/LYmwZLaoWDkXUQwRJFQEK/HAhqzDWo14gO0GpgsESoQ6j5DAQWCGwSQsHIrlJZIYIpJD5DZYYeNbSRjI9R7DkkEIQtMPxSwWIFgyLNLkZPrOss2SFbHxnJhsEOpgICJUydaQw8S2IGwSfKQsQWciFsush1nEFnsAq/SDI1IWXjeJ7Z9B7K6lhbLxuo7T8w7MmTrLHEZNjU2LPGfrmp7jk8b2T55xmp4n1gg2NZkQ+quxpOyWeM7T+2esw2ks8xrfqPZbFmzYs6kg4n9ksdk4ixDsZWBeyhA8YGRZ1H13sGphyNbCw1EmS/WLPEOoRheMLBBDAfUfWWzZyNbDkGTlWo+uWIIIDsHYNTEjUH1iEhK7TqYIWCCMQEPzzsOphkirMAQZOT2GAgIPrBaYYnW2Cz6rksEP0HidRUENQ2IbPrMEP7Bk2AYbMMngIIa+mwWIaIG0kPqPaTU/TbOSUENQ4Us5H8IQCwyLAWPUeR+owC2WKZke08h9QtQQIRIrWRDrcj7gpSYghbEAQ7GpgteEKqDUcT/AAkExSVFyqdSsOxoWGkLELrdje62y2dRsa3ImAhh1KbG4CQmFyMgXY0kFgAhyDI2OQAORrdjgSAQGp2OYZOTqdjgcZk7HUs7Hw2QC+0YwI+0A/LDs9k4zY/Ox5faAgQ2PJ7QCBntCIYbHo4jY7FkbOx5EYLOt2NhZIYbCajY2mQozBUsuxxK1b5rJLLsbS0sGBHIYdjU2SL1AkFShRBB/AuQQYuqFWDIbH8C5ELU1BIiDYKNjM2FhaggtpOCGamWz9sswwWMBYaLM1BU1J91QSXfglkMSBGkUEYmph+yBDEzQzCxicETi9ELNWlIfsAWqpskmILaS2zxE4maIn95ppiZJiqmWw1NAYlZxNQNj66AZGUyVMU1Ol8rswzViH65EyBJRmRTBFXoi4BG5gdR+2wpN5vmTmUmRMMtgDKpGA/acUQTF5vUCziphtOJqTIMFQOp+m1iAVgrfUzDYvv0G0pYLVNQzgqfpqFpi46JNx4OZe+WkpEwQFq1S/SCteheJvF7yDGZpN2ExC3KJszkg/RGh0iYu0DZxEwkMqMJoGitYlgyT6gQ5ySQxUBd3zZq+DAzfBGiwwfsTMVDvqZvcvLpqpJi+JojATAKQQfUCxBVcGAnMjFVN5wRnFMDKVYoi6ZD891XvncwxppWlMFGd1Aq7dmTTBANpij6JTNElyZphqJYmknU3iTF4KqrwxRDFH6zuxE4oJwSTIEtXmi5UMM75gqaIvRFRX0RmrirG/SCbs3iovKQU2nMbsxNEl9BgyPnIstmiYZN/DHDfU5uMEpUMwIy3xOoCgPnTTOIbSTUwtiDdFMXhvjFgqznoYHNqQr5zNA1GmJjOZYvec5iYmZti+Jlb4zhpRmKCJDHyxhIb1CpebziJluY6sTOcTMTgxe1RLNTGZNAkIfMYZqWYwVMg1WWiwQyM2xgWlzgqbSsw/NSBvUCpBeJqCJgjSi8uCmKir1FQpYghIflsNpxNQsFJFXG188xw30IYWZVtNCVRWR8wtU1YTImt6hndYBXF8Xo0sRWRDFTVn9QLDNFRgq6TVsWCBJu5b1oxaiMWKhuo/qmLt6lXNjEETFQQ2nO8CN2qhCrSYSGBGD5bDDLYgqghzF0rTSAucCahc7YsImUwJD8sGyMJDFBgInOZggYkw4xMsA1F2oRQSx8csiiVZvVgicJeYC1yLyMVAsA2vDZsDZ+OQwWbBBUwLMVYMy16iasUYgGpnFrtpYIfkpMFpYWKJxNSSxVF5IbLM2YKi9MSMEMTQwfJdbk3sMXHETVQDYmZByWFlSTCJavm1YSEsMEVIWmMYWC5RFZJFSLF6gZqH9QqJW6y2YIQbXisjq6nJYWKIqCzZs/MSLrLEpa8BYICKJCGYZhgyUq0wQ3dR8lgKlDJiate03mqgi+c2mpvijJ1VkxOoPlMDkjIkSwTF4STBMOIcECwwcVRLF3UfJSxEossYLTUsVkU1RMaMPFUFQwCTR8t1muUq1WFm8lO+qwwwYyBsthiXIfkMMSwQ5OdlhLXhZXCRjfFQxUOpeI/UTWayESYYRog34dVBUMOjBDrLSln5JrGwkMTUw6pvhLOGhjEFWISxlO/wCgZEBZIGYUIBUxDgYSD1aJhsfLbEKWLBAxeoLC1iCGYMiFMniU+Yak7BBNZVDDpElaqJYXU8S/QdTkwmSMmZYZGywpYgciD9Q1DCQazJtU2nMyYYGEhYYIF+oWWFsQ2LKVIRUTiDJHJLGTZ+g5DqYLEEMMi2ZgYWYRs/UdTAmpNYiwFgYFQsww/ssLCORkOSwBYtIz2HW/OGUYNTZ4kyYCGGFTJ7Z8kg1EGs1jkWLORasmCE1P0Wx3WxBqCCzCaj9syciCHiEfUGxB+yQ6n1hhgXsnrH1GDWQZNnJ5zjfpGRBCMHZCK+6cbBD6jkxo6h/eBhO0vYWHUWdT9EgsWk1moA7TqLP7A2CCHWQ2WH1ntv6xkmQWfCwwENiD55xo+sWdTZ1lmzrPosMHI2OJYYeIgh4n5zkoQeswcS8ZZg+uyah7bkw5IKuU/UMnsr2SDUwjdirH7LqYbOp7BZ1GS1kQZP0yCDUnIwOSNMJABB9Mgg9Q7BrYUGrYvOLr9Q4jU2bPGWNQARMs/sOs8JCQ3vMOIvCfVYYNanE+oQVww4m7Rdg+k6k4zlrOi5DQ1m2PpvZPVYLMDMxotNN6nB9hs+sWDUk1F7sFE2D6b2XsMDYsqaSsmG8Iy/YTW2YIQqwjotUSZwp+4wQWEdQxNTS2wVqqUfsOTA8YwRLiNu4IqGJswJ9l1kJDBDiYm4xJVRJSNj6wlhNbCQMTeKXESQxnKofZNRBYsjMtt8bdw4xVEUh9gsNmEyGJsXvNRKyTcLD++MENpqYKpRghdMYgPslggsRWSAMTYIqKYLL+4ZPE6hIc6dud755mcwr9thhgZYbixcnBM3HJ2Mq2EptNiokWolr98dVCWcsXozkCKFqD7pZLI1OLxjRxWKiZ2NCCZVeKtv3xhpYJNjOwQwUQguMYmZmZ2NKZVYm7otVsbyEuZLDBDEyGxvAnVIWA2Nh6jC5BscV1ntA/47HoSCDY8Nh1I7HN1JrYNj0NmdjikORBRqdjWajUiJCQwbGhNQ60SGcjY2CI2UZsKbGc1iUai0pDkn8I2IGKlIRyKmGxD9xNSZTRQwJaasWQ/fOy5CWKHIcj7ZxmoZRsM0WKg2NBYgorU5SwQFfcIbOQ5EFkhGCrIQ/dOIgSooYu2qSw/vNjiYRGGYZqSJhswftHaR4xGiDEjghP2nje2zRCSw3piYT9t1PIJA2NZdYf2DIyfUHUQI5ENiJx9YcjW5PYShMkyqGUnJ/edaWGDWWYbwjB9tHsEM4HWJA0bGhKlihslmEbUFn672EsIJBAy2EwCDkfPPiCQIwCQRVk+gJA5PabOtgRmpbVMVOS/Rewdl1IwiMMs4KBJEhPnkJBYgyO0iWYGxRUAUMHzUTjRyNZrSBnQtQWEGG1TD+qmoYPVTiGUYmGJwUVLLZIT9VyIMmE7DqIEgYYZwakhKlg+cMJxPK8RU0DBlQwI/NSDKiB4h9USCiwjOJq1DAD8x1ENhh1D2HUiWEiWEFmghgfnOVDqbPZTIyGEpEqoEMj5p4hGasZEVIywQkUHzCzqSBPBgZiiUamKi6yfNmoIKl1PG8Y8RFEUFTNMikPzEcpbGTqfWGylEsFRNWlaIfls5DkI6nW+pgsLMVAizAsT+oVBDDE0EOo7SWSEFCoImZqKCH5QuuiwidssZCQ65hutWJh+UNRUFnJNZ2hNYpAwRSRIQ/LqSiKnKmL2edhGyFZOGQLPyWGCyTqcSNntHEzC1UqFhMn9QMpqgCzZ7wrDLYhyfliKWIqxVlyeyMOpn1jLD8wVbUWMnI7BqbEEIw6wBhsQ/KYYXItOpg50YCzCkEE2LH6ZZh1CkBCGosQ8ZRkoWSKSAkf1HUWGxDYhKhIPUEbENQwuoIX5C5JaSBXIaIMQsPEiQ6lybLAZHx3Js2SFyQag7Sy8bZ1MHymCxDClqErEBZh7AwnGrDY4n9Z4iFgImCXssFidVVDBP0nWwLFVahTUeoFgXUayH5xZ1rarMVUwmt1EESah+mwa3W2ovYHjMjU6h+k5OpggsthYJ4hW02YYas/WdYQ2dSeqDZhh+05FqyPUMpPVdR8w1AwcQsMFheI1HE9h+ecTxHEDFOtgO4/NOwWcnUFq1jxup+ynqFmEeJ4zXX3DiYO6/tEHI8Z3HYyjsbUh7jsfU2NTZ2ejkPqFiDsljY7uSdg+0chsbjwliwQ/beyczrftHstnIQyNjKQ5GRDEpkaj6x3CxqIIYEhh+qZHddbPG0fuPYe2WeKoB+6ajszXELA7Gk7AmtKm0/unGajUHZGasw/YOwQdgTiYSCoX957JDD2TIs/ZNTYNRkxIjqRsy/bbEGsNUp2Sx+6WNbCwQMNg7DCP2GzBMNqGzJRkVYgsjB9oULMEzlKjA8Yn2SzkhYQissTDZP4WzqISAYYQdTrP3mFsQQkDYTjPuq1DYOOvWMkfttQ0TDCkVExUVxGxlaarJYMBCVsayGqikVmG6EDxJ/Cq2vDF4LS9l+62WwDgipoqDsH8CwsBCyjA6h1GxpIU1GRscQLTRsejiTY7ibIJ9oL/g2fZxGx1fVHY5B6xsgTUbGggYbD2n+EH1APVNjOdlyHidjOQ2YeNg2NhkupdZsbGGCDiNbqdjK6jXTqCHWbGYydZDxHYf4HiXIgD1DYytgIkycgDifuMIQ6mmCzauwan7Z2SzBATZ1sGt/aNSwBBZ43sEC/cIVyDsMTsa3Uw1qdZUPqv3DKQgIpbGRrcj7A5OTZycgIeN/dGDIXWQDk2LIQWbH13jDsh2BYYP4VybLC5FmEIYeINT9I7Ew8Y6mwQ6mxkwWProdkCzDYciyQWSGx9IyYHJYWGHIswgw2lhsQln6TDBDqLBqFhIJWwrBDkNj6RBD6i2MiGKsORZsZH2nUENNmJghYLNhyIfonqkBBBDYAhWwMMDCWbH1l7LCtlggs2BYIftEPZAILUISwwDY1H1HIhgg9VYICKqAYSzB9kyYXshTFWFYLIZD+4WCFVsQAQsAsGR9I7JyA1CtWvZCGCGCH6zkdksEEhNqgswKGRY/YMnWalWFYpghYFBQ1P13U2A1ATNkFMiyfVLFiEyCz2abGRCQwQ2fqtkh7LxAMMEJDrmx9M1jBxBEjZhhg1DBrfquRDB2FqBYBs5MFj7TBDqYcjWwEDYh/dE9QsupyWA+6OpNTBqCAsPEa39tHU+q5KQGt+4dhg1GRFQwZGxsYMnUcbqPsPqDA2djaeqQmx8eNU2PTxsGx2SDI2PA7IE2QCbH92PTzntAbo+0CA6bP0g2PhrNjywwlQbHksmx1IeIdjuBqYNjmNlswbHQQgsWdjqWbNk2Nb2WAbAbGwySzseyxDDS5SB+8dtswZBrdjeQFMGVMMT/A9upghYDJsfwPZCVikhbBsby1EhFZLsZxNZDYYeI2NJ2CyBOTCuxnfUIqGmysGxvCEABgNjeCoLFMELscQh1GoNjcZIHEEOxuUCF2OxYVsbH59oL+jY9Gz+NkE7Hd2RTsfjZ9vG8RsdnY/nYPaAXc9oCfnU7Ho1Gx5eI2Qz9h+I6nZCOTsdzU2MnY4kMFiHY4jk5NmyGxwSCB1mRk/rnGd8yON4nY0MNiHJGxsckydRDscXUOpydjgcSfXP8AiNZ9I1v9l1LZ/YfZbGpyD6R/aew6nY4NnJ1P1n2SGzk7Hl2QBkZGx3eI+y+ybOI9c2dxk+s7HB2N77QDHHtAQYe0FQr7QH9Ox+dj+ZPtAP0bH44nY8Gx7NkI7Hl2QLsfGxDsgTY7uyBPaB0D/6E=";
            //            eoConsumerTraceWseManager.Finger2WsqImage = "/6D/qAB6TklTVF9DT00gOQpQSVhfV0lEVEggNTEyClBJWF9IRUlHSFQgNTEyClBJWF9ERVBUSCA4ClBQSSA1MDAKTE9TU1kgMQpDT0xPUlNQQUNFIEdSQVkKQ09NUFJFU1NJT04gV1NRCldTUV9CSVRSQVRFIDEuNjAwMDAw/6QAOgkHAAky0yXNAArg8xmaAQpB7/GaAQuOJ2TNAAvheaMzAAku/1YAAQr5M9MzAQvyhyGaAAomd9oz/6UBhQIALATlagMbiATlagMbiATlagMbiATlagMbiAMdjAMjdQMdNQMjDAT9IgMeYAMZpgMexwTuKgMclATo7QMb9ATpAAMb9gMZ0wMe/QTncgMbxgTXowMZ4ATeLQMaqQTvwgMcxQT6UgMeCgTvcgMcvAMZtwMe2wMciQMiPgMfTQMljwMdPgMjFwMeaQMkfgMjVgMqZwMr3AM0ogMivQMpsAMpZAMxqwMd9gMj8wMg/AMnlQMfJwMlYgMiygMpvwMjYQMqdQMq6gMzfwMl6QMtfgMq3gMzcQMaQgMfggT3oAMdtwMfOQMleAMgKwMmmgMc4AMipgMfXQMlowMhggMoNgMgcgMm7wMiNwMpDgMjKAMqMAMmwgMuggMovwMw5QMjrwMq0gMkwwMsHgMsNwM1DgMsOgM1EgMsSwM1JwMoNQMwPwNDfgNQ/QMuuQM4EQNjpwN3lQMlZAMs3gMrwwM0gwM9fgNJygNUZQNlRgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP+iABEA/wIAAgACXekEIpwCKez/pgDMAAAAAAMJDg4UIC4dCQkAAACys7UBZa+wsba3uLlmqaqrrK2uuru8vb6/wQ4PEaOkpaanqMDCw8TFDA0QEhMUFZScnZ+goaLGx8jKzNECFhcYGRx8f4uOkZKTlZaXmJqbnsnLzc7P0NLT1dbX4wYHCAkKGhsdcHF0ent9foCDhIWIiYqMjY+Z1NjZ2tvc3d7f4eLk5ebo6ezw8vUEBQseaW9zdXd5gYaHkODq6+3u7/Hz9Pb3+fz9/ltsbXJ2eILn+gMvOzw/Wl5rbv+jAAMAOpnUzqZ1M6mdTOmnTzpx1A6odTOpnUzqZ1M6mdTOpnUzqZ1E6b2PpOpHUjqh1M6mdTOpnUzqZ1M6mdQ/v/o9fvP5HUTqR1M6mdTOpnUzqZ1M6idRvvb73I9pPSfcOpHUjqZ1M6mdTOpnUzp/8Xox7ve7Hs57M8g6gdQOpnUzqZ1M6mdSOpUvWj2w93vfT1u85fYdQOpnUzqZ1M6mdQOpea9hPar3i9tvYz17snUjqJ1M6mdTOpnUDp7yc9kvYb2e9XvUD1O8rnTDqB1M6mdTOpnT/wCzzC9uPXb2I9iPXj1U9A/uOoHUzqZ1M6mdQOofhe1Hq16Oej3sd68en+4dSOpHUzqZ1M6idS1HpJ6UehHrJ6perHnnIdUOpHUzqZ1M6kdS671A9qPTD0+9LvSzy/8AKdSOpnUzqZ1M6mdP+16He2no561enHq/0jqJ1A6mdTOpnUzqZ1I6iT2G9UvWD1G8++8dSOpnUzqZ1M6mdTOpnUTpnOer3qR6HrOonUjqZ1M6mdTOpnUzqZ1M6idN95XxHUTqR1Q6mdTOp/dKyLULDxE4YRfoV4Sq5DOggaPgoxDGIXdyTwAojx9Oiu20PARtew6jHbqt4k/dk3IDdC4yDVyo+7JhmNWqaNNYj9xG1rq33wwghw/cTj2N4wQ7ePlUh9wJY7xUN3Y77B+6jC/MbcVq1o8BVF10s4KaPgpqdHQZyvDEgQ0PvIYKvvDhgZUHhiy+S0N8NmDCyOh3hx828/Jbvh4UXTjau4aXwRqklTjhQHgiZctU+GaMPBhhllMtT/hM22okCNYF4MKLsURCHhDO1rnNaPgLmCOKenwxm12lryL8MJ0dF+cPEiXvtk8RY8AvEJQSTqLwiVdtkicfDn1NeeKXxEIV0E31+EsZ5UJllHwVV9TDQqgfCB22iWlo+EtcmSuZy2eCEMC5Ea2sLPAXS5rgohgHwBKsycip1h4KzRmB3iKrhYMBh8IgkkkYovDIRlBegP3xMXFBqyEuIMxr3ADhjNJA1zF8NoN2r7qZWr0iFQxTzxxGXpNStZJwRtdJokmPgKbOUUCnFnkTDSwUG1ZMK9AliBRtFNLjCttopn2itq4gGON9GcQplApAdRGTnV0MsCUrI8i5ACNZo4EYAKyShTLAaucznJRCshzBXKFLluE19JmwiwhzRMq6/JPIZs7GElRNIswSWw2LG5exeNHO0FSMKwIJFgqMNW5ggIqVmHMIhtsZTjndTJcfVFAgw5qETytqjuJgrXuKmBQjmCZgMYxjWEWzr7YtpEcwUxXoxRKxUs1/L2NjpAyymJhlzrakmS3o1rzmKAoEi1EqkMlbnCZdKkqC5firdOxZZ7p53YcEs6n3OLSCEBEynDpEISsiJpkEWkTEDhmSK+GJEkC/8iPFEyzEXjOaMquGSKUtjDiInBy4ahWstFZkE6RUyaWr0tqBvAQJYsFQZ4KtyoLbu4cC38qM9eI2Ml1XhlCCqMs4WbEMLjSoMkkzFaBl2tSSWZMO9IZpZy0dRy46IDIhWhUpZxKgllTTta8gwhJKgrBBMy/jM3afUhblEQ2WyKCJ/HD7U8VBbkZKKwsAZDPGx0WAFVysDOKhb9F1NNsaH5VmVZAwMRFPq4sBq45lzx2RoFrH8Y1wYVsUGFJWFsyFK2UyqJEzMLDMI5pGxTJw6zZEIMIBglDOKQ0cmE1+SNrIErtAbDBhUmbUAxNBdpSyjmIqIMMM0a1ZxAZaFiuB9MeOKbQFplr2aV3jUJ0RIwsLckyLcC9AgZG1lSu0lzC0SFBnbckAmLhMNe2ErIfFRCHFDgCPjD44oa0fGwTd7DfAuE29R8W3B6mwYtJaJO1eves8m6a16S4ctwFcfp48dQ6KKthsF3c7HV9z35BzwY5Ck7+/6xv7drW0BpKVQ434vb9/l9BlpThdiIy8dCeV52C0SRpapMGSlmrKmuXOGlUbmpavDTly1rhVZBOJSJyYzEYYcETprLJLuWgxRNa2S/1fe+Fo2C2y065VFDHvA/VhcLLA5exkRXlnClULIqYwYVibf9qqRQTYS6TUS6kDF5pBQ2aaJ+eBfsTyYrnNYuwlLG7Sjc31f9uhSWLmRDZbFsbBp4+b5fP7OGiC/wBlNko30R7tPG9bu4hFpYbgWBB2E3a5v4tenBrRrK8JWCXJFv0fi/Jp9HXVOMi1DnRmy8r8fkf8UQzJGuUrIkoMXQ9H8vS3gu8iLGI2kod0/U9naoNJzrsibQmYFPYykWUjMWGg6WjEmSpcCBLRJqrxpjQkoeEhrIGsWBcMhJZffH/ITL8cTSqAuIKI0gZA8JhKJqCakeCCmBcTRM3giJCUqo4gHSUtYGVsswgKs8TMcwymIMEkZZ61gtZNcZUrWKLPFqmONEZxTNaWcQRuWQsmEBch8M8ACQmiuOKXDNhtSEtINikrdNPqUQWpkMkUZiylyXdylKV2ExzJnlSSJZEoumogslK3GGxCCibU/fiMywSgjVjNSosfZ2LfZt5eAWYLDQN2kpjHaj09q31+duEvMJE0va6fU5GBhgdSODu8qqxJu3e10kNcbKjH5B2n8e3kttzQVur71/qDe25ruL/DyeaXKnNKQckOHDRLgNl2cDuu5BWFpRSlFCpxmTXCuErImmikYmVsz7YpZCzxw68lEUVDoZo0DnkpjFMNPDJVNwZK0TSSRQ8IiMTZAIPFEFgX4HwC4pGWZYlwyS4OBNwyFwlO6ScOCLElJtRyqLSAozMEz7EEvRJiFoUnckaZmgQsBZmSNYKzFng5S0mBaKL1BhtEshSslsIFqAizhnmjYmRcYpYuXQoMkwrMQziRFLnA5yzDJLLddGSWjnM1opmNa1U8zZ7uVqizRI1JnN2OAEz+OCmdNkA2ih25hqUNLJU0BYGrc5OG51b2KWWqvmYb65RsW9Q6uj5Yqlqbv9L+vlf45lY5oU8mo7kdXwNgh6/JO6vMJ278O5ruzVZP+J6PQZNHRH/Lvu5rMvdiXmCUj/pfxv4Qlu4udvev9R0vmUhmql6vavzzrVVN2dn6eM7qAWaSoNbFLE5ssa59eepBWSdI5Tn4Y0OXWCCz2w123YI1QMBhaAE5cMyjUEKx0lqywNFLOGSzQsJviiQNMP8A7APHTE5zC4gIb2+zQ0eCQRMrp2htxFwRJk0aeV+foJLQEicoNeEJPZUWgJESolV1JDHLoLIjJNOaA2/kYNoBcZZRxNmxheXoSIMS1RMMmK6grTTEmRhlKTBRRS0rIoJosEpnYb0Fu7eCyuVyXNCApZNjLTBUorDN5U808UBNDpe1LH7oKzVUbnZ3JzJAXT9R+u/vdxI5guydnYZOK5YfV60W1+fr/wAmKzXa+9rMNbl7FH/7sL39nlfxpRmC277/AEezyUlzfS1Dsf5f4PLXd35xzQo9OuRg7PkqOywlyDb9sl2Gxqrv+28xcg7QvWhx3MWrUvNEnO0zpzy15XaOrdPj9R/rdOaZVglau/vPP9P0/VhhwwSTgldlUHxeXp/H+rrgtvn/AMehIOcUm/517692EuxyNb5ORONko50v9HJiTenhyTRMtKw295+SXcnthClhZ65ocvVQafK1kmkxwDXgknnXxAMSFr4o/wDwvHExD44MEyl8ISIwVSgAcIhaFUZKSOkLAXC6ZdCeCha2DihpMBFotUBKGimMyytCwBRLfaE8eNRLz1LBgSTIWTX0ptLBLAcZYzuStCrQiDgLgEwcaCkDOGepKxHCic1Rq1OGyt2SRCWGEzWt3r7jDmC7AKGpDbmK6qa9vTW5RzWVoxJrcwJEdz1v3+tt/plaOYxXMOyPJpa/cNNp/U80nb3lL5v0lMGYyD5FvMbyr8kPTuZR5Shy6eVmaNXQ9Lr9D5/Vx5GbPvdHd26vbAs1a8uXvfj7lGH0TrHpTj0PxOZZiv8A4/ki3Lc3sz/t+ePq7V2cbEPI6jzT82zGg3d/r7nqV7u2BWJYuS80+9p5deuSPq/6nUfXDOZ25nmD6ehhnxg+3U6+o4bBJyafI/5005EyjNbqG0MutifXGikwyTqHRBt6a9LLM/OGiQmQqDGwyi4YrVWlXFIR/wDA+OQNYK+IUlKJHMiXwgmVBU/SQFpFfy8nva0+WOWfSX8PH/Vydqe65iJRzwveodM/336OljVE/QorRdunkdePbfyG1MJpF2qsFHv92Rm6hldT7bSovVNreXOmCbX3Ld3ErOan+9BZJn03Dc/r3pGhYBpvLt7r497mjNggKWFdhmDI3nPmpnl3oa+uEMojmjPdljdgTqfqK/Wrav1IXmiMlzDq+2netzuvYJtsisEYd6JiSbkkcxF9+VwWEzPy4rhexWoqopl1CNgWihs/f9qb4EsbXfcA2FslbFi9DewNJDCuztEc1kU0qCQa2PAwSI5tLbAqghkSE2WdyzCFT5WVw3FKXcwV8maRK2SBZGWamRVKPaw9pszRitCyhUzsPnTGjZG5eeDoEL1Pn7XSrKCEc9ceMQcz2erivGaNoWSNim7Jb/8ADpvkBZ4ywlMTpnk65qiOiBT7HP8AYi7tLB4JImYw0aeIQz5Jll4wJV4pCBeMtEe5KoeGuFX9NvnbLOESrp0303nN5K9G43gs1v6PLOn/ANcrL9nWEdGPu+R25nlTvHhzq/1dZOknLdUdo/w8qPJXwx4CtEqqFO9szPPcp3PfeYuk60qe+W1b7n4fr/vynor8nuOHOTNjq2Han7+lyvR9v6d1JWSr9PHFWbb2cQft+f5u4ySzIn+3qsTHNRNL0P8AR5UeTeVLZn53+p1jTNH7Ltl/H3DrZ2k6o0LBfHleWPrffDX/AE7y+dzfT/22GWEq7fb/AM97aor7Os2mOvViWWbNu9z/AK+jpgrllqcyqPWhtvlmEWpH6+v226/z3eziinFP5rdlBSOkmjeW/Zfoukza5eqmw1ERrqIKPlyK143OxRKzRdG/HOja7+0m+xqTMYFkVdQI+zl5+s0J61XaLIhbqHqbV3zvP14Z1mYRZ2GEOxR0fM3us3CwUWiMqjDp/mT8COb6mTaMFocGXqeXc/dH8Dfzc/noLPZg2/d/pBzEUnVO9WYrSlrZ2+Zrdh17j84zNCJeq8wjupWqqdI6MMPfVgNThSBaS58EdMbEM4YrJYiXFHxx++Ky4qpoVGEC4Q028UoSVILhXt5SapJCLSKrq+oPv+c5iIS0Bi9jX/Z59czX4KlFnr7J0/I85HYPzdYoupeQWeHrPKrzqeueeTvf5KrkVK155frPGHm3nPvvMf261CTYICsjLzrr0k9A0d+vzTpmrJg3WqslF1OaeHPGHu3l06r0w7UHtvrErLdnuPMf7/eeb/4ecxdXTpY5RZohP5R1N6leZ3oGhl/p1VY5WsLNIYu/552vynmw66U3Y6+3zTIELAyo62ud4O6/X63ZuGi7FNKisLfwxy9I7y8y9+CtMutifqCwUr9KsvG+HaLejn6rNpJWBmpWhB03X3A6KhwRQyjYKIz+KXJ8capXGrrrgaOaswxQ32alccaUitU2cA7wv67kvk/JCq5INkmIKo1IRfNEFUtTCzlmkSpjYnmBMwzwBaWQLM3ZlO1+uukqwK0IOwg71vO+xqzJIc8gVufhOobX28umfRKOWtlw7kdzyf4hboit3s9z851T4Ot6kCx0RVu+f9W/5vfxjwxNDb1pA8UWSrLiEIcUgTKvxSS3buwCzhqcz0WbQv8ACFePknaPk6c0yuCA8866894jsQRInLRK72/KPJ+k8rcbXvoXoAG5z/deePs7n1dfkmoLPYmPU7eXj/y8/lryYtCVg1vJ39+c8znfDrPu6nTlzh2+bzqmb70OdrQ9A19g6yvWVZL2UYcBvneUPdO6PODzDgf9vfnGwvq6nR7U0OJ5pO0HcsUCY9O5ZVf+gblJtV5QE6sds/VHFP1mjmC3FyJxwIVejt4eXshL0YyzSCTyfNqj3eMd6/q6LH1kudYGbF2vq7Hz9fpPN3HwGivbuSSwRgQqL0tf4v6e3ekWHQBY2ENkNX9Ht18m5QqRzYzLzRWtBtXXw61VFUQSY5ZgzRXFbibCqqYLc82tS5hWBCkKNh2EsEDLhVLXnDJNNPDLT1I9vKqQxZwKhMlD9+/zOPcolEbTSUyB3a+tB2vqc67IvQQb3Decn93mG/f2WDoZYnkznCp5MOsfH0NkNBtuD4TvJvT7mTjGcWgFXwazyl/Nfy8bs0BpJt/JsVatU8fCJiVCkgLiF/6jEfFIX8fubiy4YRdTsX7exGJcFWF3Q1PR9D9FwS0BCYpKb2r1utswDokKkXcu/wBr5/l3WDoEDaNrrXvXR8WQNBakCA3FfD3YuagLQIUSWyA3V9f2fS6K84nLkSlAKEF1e/uSrshESxE1UuTaJmvsPqsuGAqmElOCF08kiF2R3b0chKjU3p9DF8PW+xAZoTantf8AKzTCvf8Anltr6Wn7qc0qn73m9O43Cb1fxfLfZghqrDMZgf51zD21O6v00f8A37fV7jyj2cY5hI6nK73O9d+YNan0dzW/X/nq7pZokAly/cuepy+a1z+HYRfdYZEWG+6TpcunCsik72y2wvZMyL5dn9DPi5tBLdPCuwJS683V9T0Iv1wdRE0kI2sPT+jV/R+fmXvayXmSiWcRbHb41zkQSg8rfPJcjbZFkD/83l72NXWuxfEdV48845yzbHwwvJ8/xyaxn58YZ8Xd437P7T6qtqiGOTQUnu8u7EJNYyYtAlQABivP8AeCSySaUwj+B7Ih8UGBIJFwhM6CVnAAeCLZFyY8DkkOkxU0iglkoEdIJZJTNF0REWeJkpYU4P03XShnkIAKgCSTEmJY2iYYiSZiljfriLOIOABiUxchSgsc4SfcPAaKXOhypWNglTTRysEQcZhp2DZCZDWoFICSwykZxhzBd0dvYpJZMQrndfn4auhMGazDTDjiBwyG63mf182LGqYfvFJtck6Z5PVUYW6nW2fj8j5tUmDmKq9Pu9/yccVGWtCMfQVJeKwyYupuavauVNI1KkJ+KNdjYRDCyO/MF6eZrJeu+vNKKcMMir+LV+DXagU0wlYalu70cKqpmo7iSkRnBc9Pnk5UQwex9g1YSKyPS43oe/Irqtn6Xd9Dv1SFZXseXc9qCpaqMdtMYrziVbO1+X2qH31tmrQQ2lZdTbk2HQtMta9ASMmNyRMMsZaIkLQQpQl4oklZf5D99QjxVTuwr4gmmh1b1tXDXNbn+bDRwB0hC9T0/Q2G5KDDoEZTLe53ZNP2CcWgK5nZOf51f9f3U0Xiz1qopH2rteDZ6CZgzyUiPUwZdPZZ3TWwtOxAWTbv4t/0ON6MHwV2r9wFImQ/18XJ/wB/zeXrpKwWSuNkw13YvbOnfv8A1/t7OEbCosG3fhW/zOTzNd5W9DzX9kM0Z1/aG4YAvaer0un9f269Mo5rObybvH10owc78/p/Ry+jjJBZgm7d3+f5fWuju3/a6/6cmUcDM0jQzcrveZ2FUP8AMXWZA4M5bKlZNmCW/wBVbZaC7KbAuuyqg90ilNgwkrDsMtISEqDNhnUa/E5OdFhkrfBoXsqGDMtdkJ8YxVxv5ab08QVCGcMc7H2IxDzDbkaFlaZfqWWK7Hc6OwPVMNoZosop6MPU5UWnptzxZi4+H4NxF2n4fixDnkGt9l5E0mBew+nQFe5ex67Zsd5Cy0RQkFywSEXCL/sSzD+B7FQFxCMC1zQmHhCpomv3qS4QsgbG6bIJaIr4ErE0PmfLQEQMgJpIZlXsiCtEDAwwymlAKH3DaFg0P48DVtl1qizyIWKLHfXM/XgWvOGWJgypMV+e+psjBssQ5ZSKNOUK6Fkpg5okaOlLVRlGDK1yILNFkM80CyGa5t+n+15L+mSeJdjX+j9OGFJKTQV/H5n7YqAsM8nq+X8cVOAZsFex1/S7Elmb3Il/s57jQSpGVGFpisEiFPIaEEd+acRiTMFgrc0o04KCkREkZXCyyygapEUNCVJllgWFlTTW5TaxLt10iygrIskCNyTSti9mJwmEbJCLlJkqK5vvkAZ5VSy0Y+lSmkcYKMOcCUXpm5YjQxW6lLtCvY+TVMxMEwqAhtEab1u9cRlMoeCQgC5VcAU8IVmWDR8cf+//pgDQAQACAAMDAwcLDxIhGxgMHwCztQGytgKxtwOwuAQFaa6vuboGBw8QERKsrbu8vQgKDhMUFRYXGKmqq76/wAkLDA0ZGhscHUVJaqWmp6jBwx5DREZHSEpLTE1PUFNZXF1eX2BinZ+goaKjpMLExcbHyB8gPT9AQUJOUVJUVVZXWFpbYWNklZmeycrLzCEkPH+GiIqLkZOUl5iazs/Q1NXW4OHp6iMmJ4eWnM3S2Nrc6CkqLT5lZnB0fYKDhYmNj5Cb0dPX3d7f4uTl5+vt9f3/owADAf2T9Jp9gvJJ64Rvo9ZlmbyS94q1LV5nv0w4hvU7nvDJltYL92cGLULNad+ppwQwE8xUVcsKFHMRVRjGIrfuic+fOJpa3xRG7EvIbs4vMYjGOG7TyhyX3zEmZfO9zprTkKI691s+q1Qwcufoz+Ddv4TPkn0eWdu+56pu/wA/6P8AT+PrOGe3z/c6vxejx45N/wDp/wAE5/2ePo29X3Pz9Pm+P/qz5MPn/wDriRY83/X/ANP5/wDF96D1Tf8Ad6f5vi6ODJ8VeP8As/F+ajk3Mxwo2nTp0dVfgvyk5ztzudV+ETGDmac8X3b4vF9Dnb1tm+2rxui/fngm12leFmMdc4jPxNRvZ9oXZOJ7IhPsqIeEoh0mB8LYgfCQRMVpLB3mGTHTpMLzraiCJp9ZCEYOcipghGEOdteCwGCnmRtiKZmd9XeVgbjgjEzjPld0VMN4BvSQ9wTBm54C8YrFHJfTPTOumujhtjSNM3uGM8beHTvio6unz9PS8vR/p/6vJ5/hvjp3/D8fwdfw1XI+UrdOCM4/s/r/AOn4ov3J35/kwkTj8O/f/T/P0Xu8m6o3DF7zRw4PMVUtZ5jdvJzo5tMVGeneqOnqOmxFHfCdNtAvid3DpbPrVGdfpGx9v8hjGnrohgfWGxO/wha7ap76SXjHWl++2JL4pO9UN3S84z4Y2vPNDgrVOfeWGzaW8HMVNmp27sEY5jMlQ0vVsRpy7sbcQ3Kxvl4RpyU11t86d/W9m3hw2wdzs97s3dLNxradnjQ7nZ/l/s+NZ8c7o8m/PqN/cds/7Yxt6vjzx0fH+Lr6o28mmJ8f3fL1+ZvGnX8H6PufBpyV04+Pp7Ou55Ovbv8AveX39O5Xw/l3f5PzbnOPv/eJx+jqO433dmc7etN35f8AZ+j3vh3cj1f7P+Hxe/ppnDf+v7n9nmvzfn/3eed3T1VtK6Ozc8pp6Pvdk+XTdnpEyc6bd+eLVKPeKd+l891ZnhrVPsEFY2Th7Q016xaop9dtTeZ8TCBG7vilRIF68DBZWHumsvhkrHOlk4prnbJUTeBO7NQ8WcEHPeoRU0dsJyMZ75YLsZ9dzSeRjhe17NRm3gO5O+OidCcZ9PX5Pi+DqudwfJwuo6fg/R+D9HvejS/qkXxjTo/Hnn5eHn3dn4PNDyaaZ3v1TGnZt87W/wCHeeqX8+k6XH8nk/rnr4R7/A9Uff0fR79dmnwbjy+O95e5h/v/ANfv37OBkdnUVy15Oz73ZpwjMv5s79PMHZ+HzaaTM7sYl5prTprhpNYKe9XTv3barS9Xe823zUVNHrYd2nskJsvT5BkeyvsswsB4l1ngAkSJnvuQkvgrWWuew5E96RAAk7zRUUk1BBzuohrSM3DzODJbYaKeVvJBATfduSTkAgmJzib+TdS8pneL6byMEypyVwYqsy8HCcYvyNzx7W/T057d3Re/VzHo8/Tu32qNPe8/5XGnJJuZArf016PP1ryMLWKxUEnTJzlpCCzjR7pcwZ7TEHfw3rFYQnvkARUB4QhD5D8g2bxk+0fIYLPhNRkesj7RBY8LrPYeIfE2HwGtyZ52HjGHnYV1MUo8zFWWFxWIOc0si6mYO4xjDFRQumhjlN+lES2d5Sczo1UVLo4nAvLNUoyNX7Ot5nSE1NK87UVZlhap5myPsI2BseCiwV8ij2nZPL7VU+y2A9dykPEWA9ZtVUwr4GGCYp9daZ3T3WywuR3kKhphh7sw2WFi8h3SEBh0g53MCGzBSvKENiyl600eQ6oAhc7h2PCu43Zhm5UdHVO84VyZ9ee5233eXc6VWeHkr8f44ny53vjGMRnE8l8VbSd2m/r02+/0acs+TorF+i+89/HVunGDk7Pi6t/RfDLnUynILg6b4tUG+sPPRME1U9Myd6b3GxEndSMKLB4G2MKteFIU90BHU/SYYPXf0iJ+QnsFnU+Fs+J4nIO862GR8JrbVzuSQWIVe6w5UYmuYxAi4mJiWeWcbd98SJMve0v1XikZaN9PNuxRpbPQ4Z9WHmrGLkJDnGOF3kZ0vEkVBvvdrlQioIZhIrukOQJE+u2DviJDqfA2f1jY+n6QB6ylRiH16lJPEUuGoPAJE4i8nfEh3Vpvja95JYo8t40HuhDOQX8CbpBm2DurDLIUaVzFgeBN6mL45S7niJqiMJXw8DuVBitTvnDp+Ppo9U616eqYxnpnux2fhc+5PT93rOjfDPk833/6/g+K+MeqY6vwf/b9HTv6cQfB/V5OG3dXJc/H+j4YnfjHkNvxe+vczz6umN719WdcLxtrse4Y0rf48bc/J+TaxnnNcm7hXCG+L+b+jb1XpeSenOfIXzj4P8nTU84e/wDf7LxurhpB8LXK7v8AL/e/N8FdlPZn5/Jg5/uRv6d++s74jR73X8HnCJw3e9W3htioFfAlqhh9d2Px8gp9qoa9hguTPicpgm/hEsb9LHfWxG62nibZ4nHgLVYgjdXOwWYSAxPOQ5SUTUVzMLasEq53vzO7QRYkXHVPI7scVY8vXW7Sp9VmXysi3v8Ak/D2Ga8m3y+fx4uRp0X07PwxvT1Tr6NOr39/XbMqOH4dKPVK6ujTqZdsaZ9nl0zrkesnO9dFVundBJyONMX6ujdp05wjMHcfhx0RutXVjfv29GObdPV157rU7fPpv5k0o3URUbuv4cY55gz3m1jThuxPPXR8XwXqhi739+3zTVqgg7pUbsJD4iGSb+0mydIPaZjFesRLpM7nwEEIY6tPAgrjHDQ7xeA34NN/R3wIu1vd22DvXZmc7pu3d6owWCGM3ugkSEVtieeYaq2efTIRPMy2wJ5dNu8o7jjy9RExpW/4ev3q8255Pw6bqIrSo4dH5/g28HuV8Pl26ePS1/K9fR59xte4+92fg/7p0hxG2s846D1W9WmfJuPf3zfqz/LwPVNIrqz04eUJzxt0+GTuTu6du84ff7N2MRwoh7jG3E/FterO92azrlceTfu28B+KOF4ec/HXVGm/h5fJpHTXMV8F404TeY8r15vNO/8AJu4Ft2l6ud0/F5ovtm+k091n4/uTRVXrwN6kYYHwkEEPtOx8f009kqnfL4QtNiD1ppI0PCxeKxODwjF9AnE+Gciau92oFjEXcyucybSwBfnTVTmdVnvBCGIz6MbqOU4XbDt6unzVp5Mcmm7SKbTPSTpu68+4aO3r/FvqzpXC84l7h0G6l6oznBW7rrko2tsYzqKenp34O4aCadmFkiorhzXN0C7o4VbFVzXxmboz814oxXPPo8/TSaefTd1aQc07s71pF+l8m6HmOGIwYis3o8rXMs8Izzqoz37Turt34s2qu859XVeCz4UmoYmfXLD7LsfSsPsFN7yesxJnMHiIZd++vWTTQjFeAimJKZ8UhMO6/fclgm090IIYIrGmOcgLVbEXveee5BAkVVYrmqM4bM2mbryhjIHSMy+6nkKSYbbrnQScjjbuYpMbzGMbty+qR2W02zG6cwDdy408t7u4v0zjfp05zyGOGm5ul5xvq5yrG+Y4AX7Mabfg5a2/ivwxF7iQbvG8u7ye9bOZkp27ccrlpGY2v0z3UZ6ZyI3wPdzd5S2rFd6oaqAA74xuWFh8BDkbLw+Qf8h7T7Dke0QUQ+wYh/THvn6TxHhTUJ4BsWSywcycc0LzkOQWmol58TUwtXqYvRzFXZKL1uq+nMRM1OJim+fdYvMwVLDpt4HMl6mGMXa3Vd5DES3iWpvaZ5SBGGCnDu51Z1JZ71amGEg7pUwOR4ESHJ9k9of03ZONn2ZmA9hxB64zOT6wk3mfXmYxifExWV7j4EIlwxPhZazzCHnLDDUXvPhcqxVVh5imxF86wXOZhrIL3Nrvo5Gp3XYI0rOc+iXkMGHNZ2mN+mdaPcL9Wlrmm9jSr4Ie5OfC944YJLThOXThpPRNprgaETy4L3s1nLN6d3KY30FMT0rF+ZraNi8FSw8oR8G2HRv079/XPMNdMDCQxg5nGNN+Jvd3Tie9czm5Z348FxSbT4VIpr2nZfJ+m+yWPZSv032l9pLFnxCQ2fWSw+J1sPdRGzY8JqFbDykHHIGHlYZwLDOQPK54EsEzoEHMbpTFs63EvONRNZ3vBenlbLiCbwtbdOYzc5BcXIp7uJsW8cVY5qjTfmlmidOd4VF0Ukh7tXYdSPeGGH2Sy+yZPyDY+UeyO6SvXo6uut9x8Fba24353ud9n4vufm8fQbeHgnd93/R/m+L32JnHdY/R/wDT+7/o/wAf6MHCTuz5P5/5ux6/6PQVux3dPR/V/s/1fd8345xMVzsz+H+j/wA//wB/8PX6Phfvdc8+Ovs+/wDo/wCz71/R493Z5Znld3Tt/wD7/wCRLP6fLw9CPdxjHv8A/wCPH/0dl467g8rp+Kfg/wDV/e/m3b/L2efdGhyGe7d6J/8Ad/v/AMnZJJ+Dffm4bp/1f08Pf6WOlzb8pO927fwef4urEYKrlYxu83k9D456WLycg75MfCaTMYi/CTkOqYmtvwY3zO3fMPIdbU4fNRCVi9ctyJ3/AA9O7479L0YObTTGmN9ef/o/PTpc5gLxfH+b7tbdKw85FPR/Sf8AHdF9veI2zP8Ah/x/6/8Af/j33e9McOny/l/r6vNWO+xt/wBX+D7v5vu3zPAmn3P8X/d8e6vWoqfkD7B8gh9k0rbefX0nyefrjfXgKl/r+D79XPA7vydf+X/99kD4J6un9H5f5vJv6tE7rvPP9z/T/wDnp9//ANvkzw93s/H93/s/9P5OH3K+Pz7ucx0/B0+//wDr+/8A0/8Ae+Xp8+nNp6Pweef/AH9f/H/yp35//wDf8P7P8/Rys/h+LHD8v+P/APn/ALP/AEfF/t/0fn7K5Dq7PL2eP/b/AOb/AF/7v5/+f/N/l+57+7lvv8/vHo/u/wDD/j/v+P8Aq/p83xdVcnD/AOPm4eXHj/76/J8WOj7sycmf3Oj+9/f6Z6v92/GD0eXy1yadeL/4Or9H8/xtdnTnfpeSdsB97r/q6azzns005WHhJ8fT1cMyb7fGckm3OfR0xje+Ty9TieRx492fj39GMabuzr3xRybtDo064Omcyt96OW8scOq/v+XSsU1zb99cCduM/wCbM34eYxi/Tj/t9H/P0+fy7e9Nz4//AIbf0fk/o/DGnfqfLv8A8X4L/wA/+78+/vOfw/H/ALf7v9Pk6vwTPeHT+f8A7fyf+utPwr4McPxf175neeIaPrV7RW0fXNN29lmHvldMZ50zB33rLtaXWXuk/c6t9+rdE513mvzRfhX540W/dr3vu+X8vwae/FIvOfF2cM5xG5Ta82L/AI/8/wDn8vmOHVOOrSeaa/D/AF9fU9b8Bff01d7jL5/w/mxHZjhfpqgOSo+D8f8AR715rOs+neUchHZ5Px9MV06Fbs+rODuGkMaQ786YCc+aXrx8enmtiG+DlQjHZwhnfWmbztGcMO4Zg05b3pjfjOaiYJ5yc757tGdWknKdTVN3dMFXTlwVggv+jTLf3ZcyOufGgD3SB9/z6C4g7pvwv9nTGOiHv7pxW7Tp/BpB4MRUT0eaTwN3P8O7qvjxOkeebp64P8Muj7F8ejp23PW4ej+iWKrwGOz7/wCb4jh1+En4PN96/wCj4oxv770X6PycPe+5ux0Hdo3zjdE/n+Lo8i8xi5+Th08Pj+D8PkQ5jpnTHTvjd1aebqw8ppnpffE4rHZ153ucvQ/oqDG6+737z01pyVXv/F/VWM779+lfj8++53Df+To7I6MbeqoPJ5do8hP9fZ5sPVPTui+7hB3GdOnPx54jf6Jz0xu3ct92Z18NN/TfTF3G45KUzm/Sts74xJyuI0zng3vDuw8pFG3SsUsVHSvLw0xfeARiWXlbxnAx05gDzCwxeet34SeeYbG+OHj6MV3iKv8An6uHDq4bczuuen45+90borsrvFHl6I6itOm/fndp6L55+fy0+DTTbptzjgeEnfuZT2B+ueyxVq9gjdjbM+KtMfn0au+ArTOPizox30vHZnt8nl+HsPDPxPw9f5PLpB3XET0Y7IK8fmOdZ8vQ9HR+Ps4V73PXAvwnyeefg6evz78crDUYh/p/Rn7+7o3XOXF9ufRw/Bn/AFfFX5NOucchHTg3V5PHHx//AD6vJ49InuNfm8no36afj/6/J+X/ACdnVjoruV+Hs4X4eUx+b+zo0hjSe5Tu0/w//Ld0fB+D7/DdXTvTkzzry8OG34/yRncrbPKkF+nzebo6Zpc8aHcuUcPMV1abi9Yo5Kit2bFL0wt3mnoGK27/ACcOmKXmd+e5qgrGemc8yzi+6dtR+Sb90c5i/D8p8Hl27/Mc5WfZep6PxSfdze7fze/+Kenpv0VXf0/D2fF/Z5s+rsx4Df2Z9W3ybeujwEVfhUyevM/XH2jTbiH2J4R5dyetpfa9nZL4jfu7Ony9HgKZjduL+Xh3hY3/AAMbvwf2be8xid9vHH5dxp3Si3CmPJv+LdzLgq/Dy3xTOIeUjMvE7zSr9U6c86W3+Xe59cb6l5amQjp6+l6+j0fDiMHcfh/P5KxJV+qo/q+/1Zvqvw7vHpSM8Ons97T0Xo9V3dPj3+Pq334b43cM4rfyG3q/o/15kZ4zoOzdwrkc9AxGeIaI3eWuThu32xpvjqL791acuZV830MVeW9HNi5fEX4FOE5ypx1bXo3ePG6nu7tu4vfpx5ey2K5sV1N58nn6eH4pqDndszjTozh3xLzzjG7G7fVaL3q8nx6YqWYnvlEaeXqhivC2c/kP1mJfZ0nsw+s6ba8e7dJ4uj4evTN3eLd5b6eSOzr3nfq+7oN/o6d0b+/XDSY4dNeXs2z3W+eldm2/m7Pfheab1Od+EVfyRXTzGkbtDSJenr9E9enLN5qGtOG/G7y+P4bnJi+Myq3Rffjh1/Hh5GZ7PLujq3+WZ8t+mKO4unWxeaPNwzp8eK7gR5Jkvun3p6pzvL3Jrb5pjpw49G3G3h5B5GTG7DuzmAw7eVhaeg34ImJieVhZPReFgp5kmGtw3oag5SKQnpOqG8T3bxiLwpeK7pGLwnZteF5709fli/m3m+q6vB6N3j3e+dO3PvGgejzmmN+mDvXw+9G3f07q8IzN9K3HrEZwvsL9ZnQ9pvv0fWd3Xu6d014UxGCMXPDTG34ujybk75W7fnPW+e+O/VXdL9PTu/LL371dxPo8fT6DmGWY24a3/j6Geas8VDpG+NN0fil5cXgiSN2NIn3vI8je8TFbcRu6unG7788u7s6eE1Nb+qYx1fm6aPVYn48V0q9DBV2X1THR5N3DSNI6er4fR73nxyFeTs25+acaMb72xzY6firQdsZxnvna93ednkYpcRhO4XrPLpw2CN9+WoOEVMx2Z2M+YcReMOnjYxMHNO6dAjPTx8Js8xuIM3oz7OoijmI6zefjm/X49Je69fwejS/Dy6fAYe7jy/F463104zjHeNL43acCpx4DLdXyCj2j7B8gzH2Gc5qDxVW9nQ8WM4vWeA8Ct88bZvE94rfG7EXzmQ71z3nTaxpiHvbdvk/R5NL4N8y886dPRPTfdvUDmrdiP6Y6/KEzGA5jyb/LpU7/ABjgk59u+Px7Y0xU1wjQ5TPs0t6Mab1qOE8ybuq9dOG17HKwxu8mJnRiYrvOe2Ki++YzxzOq7jdLiJedSocy4xgp5ViVsVFO9eW+cSuAmQo5i0uI34cr95IqJngUT3SiXh45vOE8Jfo0krw1VXx1fks+AjE5nX0etTpvd8HrG+SvZYv+k/3F9gvjDPrzo+XdmPhrq6ttT0w+Ca9GC4768BfzejeY0eFd4ifyaeb0dPRw7JO81HRIb8Z+SHmJ6KI2xiXp3Jz7vh7M+E7SjPqo5Sr/AAYkNzFReDmueXhnPXjOuzEY38rGe7Pszx1b4xnjFTy1ptz4VfOLk5xgeStMdV53bqZijr5nRjTy7baBjqgOQIaz27t2kFMScpGG/wAO7cFRJE8xFG2S4QZ7+fBBJFVZnM5XC03i8k2O8RJfdexDzLFRu3Z7usFe8xnb4eEbTwNR1Yn32ofDU7tPLw4eJ3ZnW7cHhMdm7fv8h6yxt3+1Q7Hs/Sa9qlPXYQYV8BUMGT33q2zGcpFd4jyVVVNWnv6aMXq+IK7+mZnG2LmKrvX7NPRPWzRGi8zv2ucwzMyHexHk64xGllecudjOMXjGcHKwlZkuZCj3VjqYYICDlbFM0M2HvNQwkV4UhJnW90GdICz3QyLzYA51AVWE7xYhvtAh8RSvrMBC+FhZN/XWPE1n1Yqk8V3E6XfEZZntOP0jY9UT7V6wnrFjcI+BMQ4A8MkyMUh3pDDpUZ5kvdmsVfbUxpZ72+7pQitc5F6ieGikF3nuuPL0xuNDFPPO6TMNrMrzk2vWcEYhF5VlrOKavBDzFwrgQUyQnM2b0RI2rvjk5V3iYNLMUwvMxVWJ1EHPdiYSDI7rMOSZPedTewetU4PE2qFm614CYzqN6eBtpfdgTxNM8NIPFMdfZc9mfsT7JSU+xTjRfFTnDeYHvjPl2/HtjM8Bd3fDFaMp3QqcTTGJx4NMbt/Xuu6Sc5E1jo4dk23Td5mWN/XpwqiJz5ip23gic63dVZ1yjPwm9qSEZg5SPe+CcaLnMpRysHTGNN8xtuMUdxxUYLZpULeuVjPLf6NMzEM08gVOefVwKmztquUtnWhndvbN5iCMdMZ1RGfS8xIxtd+M2SdHlqDE5m83NnnYrHTMsziFOdY22qM7mkHOVG21V410z7+Nw6aeXhFeAYrOJj3oe8xeTqrbtl8OMXMbmvWrCHsn2U9lMEnrBWbA+IOHvtCeCrvmjh0FeFc2ce/8BD4NDbG3dnvHvyJO8x0b791vEtZ7rTm35llm0kGm3fjmYKK0zjSJzrhXLi+k3mpNN3Tt6XHNuoExdUny6c2ZWG+L79zWmM3lNN+c7lQ0ibnLfTaTg6pxu06cYh5L8NufDfn53TbptwzzUrRUxMDNcqWGWciL6cpleL6Z6SBNc0jNZs6FZHNMTkKq0cxntolI6uF9O6Z7tCdNseXfLPdux1Rw8k43ePd36jh52943+/fvm+PRM55t/Di+fV0tS+viXZOPyA9oWD1moJofFvTFsWfBWIwucPgm4G2KqaO9VMOImr4O9LDN8tIO7mMbpsRMvdaqqxqV5ytTBAE92cVii+8MFHfQ0mUxGOYiqibEEEX7pBvhmcQr3liYdY8wgrxnfWB1Bz1kKWYO+WOJ8BlKeuwmLHiYzGg8N1iTaPgbZ4vnGDwzpvqMSPhZu17S7H5T2C9T7De9UJ4WNsN5l8LfS+cEA94gjEb9+jB3iBS83owd4rXOJPDoWrBNd2ahLXtwYe7epc2L0TU8wxNWl3jCc7pYpu2GDmKbJEk2a5qYrGqWSJ7tWIznKcX5mDUyZK8ooxVi0vO2qG+TCd0sMTBk91hibNNnumt0Khe+2XTB4koz0MRPgG8UcM6x4ZwZq2PAGhRFHiIFfdBIVNkCUQbHZsjqEdjm2ZSKHY6s2KgEdjuhiagsmxuMTDFTkYgsjsbWcSwSxLDLCP8AK6iByaiaW+JSrGxrlGDBBionE5Mo/wAowMTiVvUTi02YlKH7TLYKmgTE5MsTViij+MsNprWiQNjDAw/aGxRDZGE0mairs4hF+yWG02KsgQQUxdnBBUGxnKGCiG+/OmSJqD65qbBWQURRUwyxWdQMwj9hswWSKvvvFXqavpON2jqf5RhliUFgEIc98pX2E1kEJAkCxfhNXoEqU+uQ5JBVpwFTK5VegbH2UsVDYipoYb4uwNqnAYl+q6jWQwIN6nGeIqZqYMQMH2Uqw6hmaYqxU4jOnUfuoQQJkwRRDZzMXis8TCN6Kh+oJAwzrGUGGajF6DE2FmpKGD98h1IWCBYKhicF8Z75K3Ymj6qOoZQtMMTJghukFmJwQ/UT0mGxDCQYFvVpS19JhE/aLOpNYwIJkUMs1F9JQIT6b22zDYYKlGGJ0C+JQqggr+RgsIkFBYismCAT7ZxoEN2G4ziQqaH57COpEcm1GtCzlNp0ukU2PpA+oI2WDiKhiaKIZZxehh+cI5MMGVS2SB9RGbTi1XbD7jY9JsWNTaXiLBTNQTvvWekp80sPbqGxkZMwQwkBlpLlOAEfcdY5OsyHUwBDYigYkoicSjD82iDIfUDJmKEMmBgKvFWlsPuDxo2E7SJCMSxNqzqpialmpT23Whka0sWK1lCRWba6RV2rgs0e2JQkPG5N6IZsoEYuljQXIoidLvy0hIZr1WYSHJrJqYwTRpdNM2VghPlMCamBMgdRRkxNdoTFbqwSUVKJ7YiJxNksiMEIkEtlmC9SVE1fS9QfKSEwGJszxjMMMJTMNhRJbrOG7i8CnyRyZcjUiwORZnJGgGDUjDtwQMKfrkJV7MEDY1MuoSoFmJxCUWvjIjdjNomoP1mwSNCIwIIwJrGrqQtTU5xemGWcbowS/rIA5030u1Packs5DBRZI0NvDOxpcgko/XkiqzqTBYQhgtKWbJDYoZlgWNvAScWvN7C/qEUswVLZnElReirJkIi1LVSl1YMWZxdkksv9xqy3wME1AUTpaUcmJQxLDYnEZ0wFFNUBqP1EEz3xKWGC2JBhsQgwRjPSKirYi4szE1fTSYKP1TIaz37qA0z0xMEMDYNdXIKqnSb0SYxEzYrBVj9UhnTbw24IulVdsmuhMpycRQ4b0gOV4oxMNj+4LON3DbSyFXMXjEOs0mCKgqGYwBWm4dLhDi9YKkbH9xtOJikIrNxNiiEstkoKVWkmMUFQNRpd0Uhh/UnCVic5pl0kGCUSjISoKoqoWLzGIvFS1FQNMwn6lCxe7aaxDfEOcIwwJYUyva8wwyMVQRJpCtn9Qbk4qczFTM1VQMqapxNXrImYJihQgtME3buQ/qC4Bs3laanQqChipmFmhs1FNhQsXvLVruR+osTCZOhU1NMEVDAaTkS6lmCyETNaRiaVn2wRkIuw1NFghm+LpRC2mzS3WMYmqjQbVPylmzUK1E0zixWbAwkZsSlMAsNpb5xeAqKPksVkQWm07t8EMIQt7VJNpCEgLKqwzmlH66Jk2SwaTnUiVRDTN3AlMEVCyhBOA30QV+szZbSmUwl985MOBtiCwkMI2xOVRejdjI/WLJkkEEYMmGzK3i6q4hTWSwWptUo/JTUWKgyYS2hBVQy3mJiYclsWYGrwrB8moLMGVWqaIJMmJypKsQaioLDMUFn9hIUtNIlirSxiCyZsGTDxiQJgmfmyDCkMsCRVEEKQ4l32krWWIdUwwwe2zTAwkMwwTOQWmJtcyEbOQ6mpCH5TFSzBEsFpmiCyRTBZgsEMBqNc2H5uLGtqqaqzKTDS1MMMBAw8SGLJY9uaSEIq6X14KmCah0jOxijtsI/PqaXImovUwkSRUTF7EJa8uILGo+kReJCKq1FJoBMOk4m1FjeTmRTxmT8xyos0XgmCAqxeDS7AE1EuK0qSBgYSH3CEyZjEVi1QEw1MxjOEN9wN0bpIWBsQ2fmkUaXGCYMqaiSsApE4vRbe1UEgkP0TKr6Re8Ml5ibIQ6BkzVnfGep1EVqfcHKaqljCQQJA0TA2IxO3TSZhyVPoFi01MTBNyy2YmhqAoYrbpW2NC8VEln6DOqlR0wpaphq7NDDDBG/dppm6GcKBD89swTc0qCmBYvDFAla6vVZ76zjhN7sMHziCphdG4zeoZKbtbliibYvU6Z6TOK3Vhig+ew3SC8wSznTQM0aXoyZqYb6TNTnpi6wPzyGkxhjOKgiRbttN2Iob1MFqZbmK24T6YEbTPGlsVWmq8KYGiBnAs4zad2G+D6TDWLXdImExNLNQmGaLMDfTNWY0vUD9BgIuO/eXhvNpIKMQFQZJi9WMXW1S/SWKmNzisyriF8ZwRV7VBUsFSjZIH9sWK3FaTfE4kb1V2KlkbDBapRE+oyTjfMVjNymKuQlmiWJqCihgf3qIvjfeVvoZTgEsthIxdTF6Ef3Btd3krRRgIbNitTBkyn8AjNVNpZ37dGW01NpswQRUs1/DUSYL1V4XOqlmhs63+MqJjETgLDkkGpMksJ/EVOSJksPbdY/ZqcqsjCQIupgyH7DZLM2CGEdRWoftpZhhEoqxU5Nh2NhBU1EoY1GxuTWJRkmxxIOJINj0zk7Hg2Pp7oIif/+jAAMB90HOTY/uyIdn25Oz5NkS7HF1HpuxzIdj49w2OD6psf02PRkbH42NrDykOxte2bHRyAdRqK/jPTNddtF/hfBUGx8Idj4nKbGk1BrdnK9x9V1FjY+A7HY1mxme4WOMg+w7It2Pr6bxux9NjgbGs5nUcobHA2OT3HY7HcDYztjtGTB9V8R2mx9h8JsdDjcjidZrPtPaLPG8R++6z0nlHJghKTW/NT1k43JcphohmH5rZID2iBs6yCHI+UPEewwliwsGoayPkuR4iHW6mBsMFWSD5SOUw9qeVdbxGtAYIP1ywwAuSQQWe262CxRYSGKhh+WohkHEcb6ZaWzZmCoIEX5TkHaO2emEAwWYllggVP101PqLB6So2YYYSEbUQQCfJTIcjU2LBZ1Oosam0uJiRUtPyioqENaQ+m8ZCWFhYLFAz7aFKCWLNmyQcaGRBqogsQzYiflEJDFFpYIXIsut1BqWSCiyyJFPtsYsFCCOsgEh7TkQGIWbNWRJ+SjJRBliXIhIMi5ZdQWlhRgrKYMj5SMiMgQ8QQ5NMTcggKUuWbF2yQs/rkBQhbScTFWOKoNS1ZsQJYhmloiiD5KENDNxyKSysBYikGzN2JkwtWJAL6j/AMbFCTFMxLDLAwNORDZIoCLybkuXijfRiGAv+wxRCQ4GzVUSsNmzDkw4YqomSJLk3RZwxKfqsVGCZasExVKWYWYXjAmmCYXQi5TCTYg/WRpmcqhyQmbBAQWYapYBqJmQihphc/kkMNSUi2CcMUkEFlszcisnEVRMlQuCHI/UILJPBIqCYlbwiwGLNZUoAJZ0hi5nJTgg/WbTVRgkWBgxiZJIHjLTEzGlSVAOgDo3m7BB+o6pxV5tTBVEw1RLCraoVN04qpKJmYI0jSxggP15lwzDGJblYsrDk6iAjC6QEYszcNBjqiiAg/VCmYInFTMIw6lyYLLAuKgMBiSJjOc4zCEf2KN0DBeEsuRY4wGxeN8ETGJiid0zG4R+ViLxMu8u1BkanjIZFzuNs5xJpMys0vykJxvvFRoxeWCA7ZAWR0Ilk0DGd2m+8D5LYXM0veTfWuTIyYbVEmckaVvJZlmSqvQfKbUbWMROGAgs2r0yalDdMjUSWmEifbSmY0LMBVnUekWMmMOK0nEyDMSEB7bFETjSy5L2my5DBMEXvcaoipggifbImG1TUARNzjeRcAWJhSiJ+Y2voWIpXGK9NCxCQkTGAsyEVCe2wlWImJlLFl4lyeIhtMYkSiJH55qaclbGSxOo4mCzExiSFhPmJRU0MkxT6lMEBY4gSJamj6S5OIE7a6nJ42YYZGH6CRLMSWIdbBZYXtNkh1n7VRSgQwQd14h/cRtPGFmxDktjUQ2RgfqIxRklpstp1LBxPEfvgMIMFiHUQ84fuJkZHGQbGsyQgYIDU1knbPttmHiqGGxqXUn1kGEICGJIeRyPsMJDxlnY2tkYLGTqdjaliZMn0nY1up5WHY3OtfSeI2Mx2w2PxDsdXJyNj4Q+6MAL7oaJHugL6bH0foiP/iT6KIn9tIbH00R9M/5E9w7Z8g/fSCxrR9k1H0hyNQj7A8R9M1Cd91Gp/cRyNQynsmosPzzJIGGE7r6h9QciBESw90yRslh+gQQ2HWnrH2n2jJGyWE9x4n+22HjPmNhtR2x9VsahybS/OETUxUOSd44xyESD2xIDJhsJDrfUGz6j9Bntic4+k2NY5H7AwiZCI/pkNjWe4iMup8DYdQ2B/aPkNh1DqRh+aQjxvsDDrYRPcGzrfWYGWWzAwr+0/wBpLDxPzyER9gREbHGwny0bH/M2MkR+cIjY9dERyNRB85NY5D4DUZNmGD6QiP8AaNbD9ARGxY/5RH9t/wCUs/TETZUCP0xE2P6bHsdj8ibIA2fxknuiUM7IF90BC3Y7kOx0e0cbseXW7Hxdjaeo5OxvOJ4jtOxxfSLOxuPVMnY2ncDY/mRsf3Y0MHcdjiek62DZAzscmSCHUwbG81HpmxyfTNjuQ6zY5Gt4ixscn0ixsZTumx4Mjidj8ZOxlPVdZseXUuTsfWxsbXUajZAux2PSXUbG853Y7Ox3NZDsfF1kLB9k8BrdjuazidjWcYQhkfRP02xZbDBY2NRqdbZyITUfQP8AwEEJ+y/+EyYIYLHtsP8AbWyQHEwe45EB67xMTDkwZHyiz/bCGynEuR+wWYAg4ixzPbqCy5Hy2yZPaPWYIWJCclh9tMn1DuNm1QSkNBBFxYPmUayA9N7ZrJGnWQe46jtnqtiDWQQ2CGrEIe2anJ1vKalJHJsxVQPymxYarUQ85DCmQQxMMLAFn9ggs2aokhgh9NyLESErEisMyEBkfqtqYIqoIUIYOYCYFgshEkSMT8sMmyoZPKQ2pgkaCJAhgg/YJhSSqJyfVkOKlbCwTBS2bPyXVUEwjS2e1PpgTZgIqrNIWflhFIxJYPTbNjUuLEGGsrkmp+UKTDDlTCnG6hyLEywpcnJSx7jAazUuSsBqrjrUVCwtmGCH23tNj0niBbEAwQEDYYIT22Cya1sdsCFswtiEhyYbIHti5Oo9QhsEBABZyCwDlUPtvcA9NbPdLB81OVg1SQTAek+kA/LbMOpswHE2IbVZsemamwfLcjjHtGTqIIIe26psv7Lklk1NmCHmNbYPmORx0r6TCwwekeq+4tkFUsQtiwQqEHfPpVEk2DtAQEB3355DCEVDiFsWWHIGzqf3jWWSJIDjIcjiYYD914whirNgs6wOILPbP2hhIcnips6hs1D9hJJtJAQ6nJ4iZIIEyfpMFlioas2O2dqkIDtHzzXMMMnadT6TQPEEL9FhhjESGE7T6iRUA2agD6ImRcqkaeN9QghCwQr9JtQsETNjJs8jatZJD9AiYBIkMlyeN4zJtUTqT6K01Bal1MHfaGvqBBM1hWGmzqfUIYQhs/TSyEKwQ5Lk8oQlj6ZqaxAEgZLqPUYLH75AZSQHsmT9RqAmKg1nE8T/ACA4gFNbBqfTPsFmJGCzxmxsdUj4H7JZswGs2Oa2Agsg6jY1mpYNZsciGzBCds2NZxtjY8nugIQcRseX3SgRfdAQk90B0TZPGp2PRshXZ7vugQg+6BaJqdj6bHx2QDs/TW7HQ2QjDsezZBPuicYbIBh2PzZ2OyZGs2Oo6kYbJsbTWZORCbGt1uRUNkskOxqbECQHGJsbXVNSEFnY2pkIhkEGohHYzTRBE1kUQGTsZ2JbFYmoMTRdgsCbGcqcZGJYmhuQoGxlBC8UEJicBOCDtD9kKCiL786IKG12ycb9g1YmiLmJwTVEDCQkEFn+JIS01icEXqW1Wb1KcQfWJw3qGM8WW+DFTRBDAZUfZYvhuwTfEDNQMsIkFjJ/gcRdGiTF2SKCwk5TZlD+EyzNLtQMwlgS4wSwTFVZH90CGJI27sdQMN95E2EmGWGbOR+6QFb8EJLF9JZvuGKGKnAyaWYqyfuIRjDuzqLti8XuZ2m2JrOooEqv32oJvOKzhk0zq188QQFjJgoYf3WzUTncxpMYmr7sTgw56AEwlXeJH9tUvMzTG7c4nF8ZtQ3ZogsMSwObkP0mqxowkEMTWbRQ3wMJgEqBlIZqX91YL7ZdAoxuURnEYSocmxBZ/aqy4w0gQ54hzCanGbfEKZFmCiCX6JVEzipmtOom6SxicYJqixoNwS9EOTB88hgvNRJBiJid5U2aK3N8FE0QVNhsQMH0gm9wuYxglnF8BGJb1NRUtVBS5y2JfoBFUTcWYqzmxeKJtTU6DKhUVOk2IxYfpDaqCYvBOk4JLLEzViacZ4SRhP2qkxpppVF73uxnJgDOWMZxNRoTCbWKc6IH6CJMMG68znMm9vpa9qmo0nQiXFxS+IILA/NYCYIC9872MapwKCYLUIxTZi+BvRB9G8YZhGSMdTfererTeKwVeGCGCCCGAfcEwFRTWKpxUTN40z3xMX0YSqswkLEsaRQQ/LLBErLpv0xppjTSLwpNSYGzJVrkVVsSsTDY9wLuAjCRVJYhxBG6jEzgbkgRUlQSwynzZqEgIks4hiZYveGG9lMqzs3WJylsD7YkJN5jS97yRck25tRpeSKlkrNFLBYgtKfOLXqYIb553N2d7zMmeGraJJFS2mxNm7afnEETExM6Mku68hFY0nOaqCKIGgYmEsDqT5Qllg3lytMgvMUFmGDEViwuq5AZTQg+2yDWq7v3uUzeOugJlmMNQRUIEDaYWck+SiROQVNmGK6t8TBN7bSGraEXWSSKtTDYh+SZVdeIvNNRfNrEzBcp3OCp1N4c2FbKQHuGRBpcwBMsCXiYBSovA2qKyWbXgh+WMDJFVFFSEuMVcKlxpFFJCjaoWYYQsntjMOTbQ0bjUDgibVpBVmCEhss5EEI+2JGIGCCisgmZvCQwukss2B1IMK6z5LKzSQUTFF4JzgvmUKWYM9RrmxBa+dML8lLMwJZzlIQcWYzqFYZgDUWEglus1T+wwWMgSJmZrGYZ6kopuJkUWmzEwD85B1zDZ3VTbARhsQwZSwwQDkQWPmiayAoLNJFW0JmXWDqIJYoE+cI2SCEoILTeQuWm8DkkIHEwkHuEGsoWGbVUTTNUxJdtNWdTBZQgh+eZOSo1VFYZ3y2vnqciGCCA4z56ZAiTViokgbuTDqDiB1yw/LdQwWRCSGKgUYLOssZEC2bD9McmSYIq1MEGoswwGo1v0nIggYWqKhxRFULRDUORA2NbA/RLLVgiopyXW6gbHphB801FlYYRstmJVCHU9oh1B8x1MJkQIXhdYWCxD2zU/MLHE2LMo2og1EMNg9QEgP2SGzDAWBgydb3CYeI+oZLFBqbHOpFe48brLOocyCxka3tOTD8sg1EOT22AS02D0w1TxHzTIcnWljUwWNR2xLkHzTjdRZIG1Q5Gp1Opsah+cQnEWIl1jC2LHputhfokOtsX7Qwax1NnUcT7jAamDU630ixk5PaPoJY9Ig1vaRg+ucZxuRYs2mEsmo1BYyPorxvIEGT6RxHzxskOtMg1EMMGTkMDlNj6S2YPTHUwhAWxZh9KfouTxPaSzlUk2ohYYeJPnveMiKmGCGCHYzvbVhL3uwh6S7GdlIXRxeRNZk2fpj2iwa2KN0kALnkWIdR9hySCN7ILi8FFkf4nWwORkRPCCndicM2cjVX2EyYXOKmVlgrIyoGD+I1ENCkZ6A6BxrMNj6idpKyAad8k7YCrA6mf5GJMRRFXLVC3YqYMF4oP4ByTIq9LO2NHdnOIMXYqJsn1RGJqxYnGaxNYZKrO9mZhkh/eNTAahhWqiamc88TFZzZmz/AQQlmSMQ3veZlMYqqjchV2E/hIWAdQ1YhxjDjfE3xV5GD+BszDCQiU1mYaqgIILwjk/VLTFEOVQYY4aRiqi97EiH10sWISEplxpok3m5WmDJ+wFZFQkxV6378aaUsBeKbH2hslqIzLwTdhuYLpL9tkUxcMXqqYQc4pmCE+0WYJqTSMwtnMJMSpsaqvN6q1YqpI00SHYzsEsuImCS9UVTAbGwmKLyTLF5Jhp2NqxN8890Ysaw2NYQDSxMzOx3NTsfTID3QhyNkCWINjswQWNjomQNksQ7HAdTOQ7HBMgbSZMFnY2FDZlstnJ2NxWqghg4g2NCZFZTDkazY1CMJBDAwZMEOxqYGxiTWnE/yJRRqLSwlmdjeapwWYmGJMjUfbIGaMr0WMNjWWPtlCww5SpEjZyD7I8RQwy5A2otTYftoWSJoSWzNggrJ+yw5MGVTYqUnDahgyPrvEziaCkZxeKJg1MH2HiYYZYSrToBi7AsD/ABmSENgSzJhKmEdjMM2OIhhoCKgpPrFjWUQRQiVizcqwFCfwljUwiWLJCFWZWKT7LaiCGGEhGBKiYFEh+2zUORUI2RiYqGaYr7aMCUIyiFQYBMI/wPGwUQQ2ILFEVNr0jA5H1WwwmCwlTiUiiaRiWD7ZAymQ6iichIqTUfykCWZoswWmk1J/EWGGE1URKVLkMDqPpPIWEYYZoRIHWWSz9Bs9x1JQlESwhZiYHU/SHkYLMIlgYKuwKw2mv2X1HUOQkIiM4DEoTCD/ABkIkJCI5CzRMMCln56emPabFkipSEGJUyD6ZYck1iNmEQxLBiRbxUEMBg+elnmchISGUFJGYqCbHziE9R9JLVkQFWGKIoIp1PzCGwnbG1xsQQkJTIYsMAsT9ARLPqmpgbMyzNQxMUTFMD9A9NsayDjIbMIyVAtfTIe+wMClQTEtsI0Qa32yrEEMPK2RgSwwsLJQQ+4mpLHE8RkWq062EUggRXI+UoQUWGzY43iIXI9NiX5rCmTNdt4hMl1EMFQo5TkfLaLCJRxvpFHE2IGskszCD+uQplKJY4jjZSHJhgEF1kDD7bAQWIRLHqJDEwtmyBDDBB85ILIWIdbqckqCHW2YBqKpibHtsiVDAsOsdaJlNjIh1GVQ0FmH9dsiWJCamitaQiQC1qFQ1CFan5LCJqLT2kgSBgqoSCFeJgJX3CwtWcmw5JAWQ1DBDUDYggIbHzGyqhY9N1ldpyIIMmH9lYkagEs5COQ2FWxkFhVsp7i5UhDBBZsZCa2zBDksLCwP0HWLxNhgfSDWRWps2fkmoswZLCmTxUdobGsInWD+ywwQ9pE7TDBDkEOp+ccYem94yNa2T6a2R1Fl1CNn1CDU/NfUIrIMniOY+yekmR6Y9p/bbMGp42D2T5Txp6TqQgbPM9o/ddRkZPfYF+Y63tsNiF1OT/IQ8bqTWQ5PM7IA+ed11GxoT0jtpB4j6C9pgh7bkQ7GhTuLC2dZ9d7pqbMOT9o5iB1DsbniYIo1Lk/VeYsEIWR+05FnicpyLIhkajY0tizBkayqD6x3pgh1rMMH1X0ziCCprISiK/jYIeMscRqZbIaj+Fs8QWByciiCoXjPpup7RZNTY1ia1P3ixBABqMqsw5MIwQkJFZP1FgA4pMpEoYHWwhD9l4jIE4jWwQhR++WYWwZCZOpLIwQwZP7rqeKsmaHtypBBB+8QKGVdqg1DkwI5P1QWBWJyo4nJyTWH1WBoKgl1VMPpNihE/gNTUMDqPTREYCx/AFhqxRxmtbORVh+ytmCsiB2NgQAwMTjU+kakfrpCWZrIobPqP2nWzUpxms2NBZgRitZsbxsRVnUOxxR7RsdkT0jZAOx+fdDf42OZsiTZ/OQ7HB1vG7HEYSDY8Oz6H1XY4kDD4j646nJh5iGH6zBkw6hbOo42BLH2zXNj1RFPriQ6k4jtjCD/ABBBCapGyj6Q6n6pDTZCCGmch7brP4g1sJAVD3z959MYeM9M1D/AEEVk5FkgHY1OoLDDqciz6Z9peJbGT3X98NbZtnDkGxnYIVyIAYbOts9s/cchYB1LBZdbOs9MP3DWZEKkDCQVDk8Z9ZyTImWEgYQ4n+E9IIbMKWoTWfZeNpdQFmJydSnbP4iCFhsZNgs5PIfTSxqAyWAyXYzmpddVBBVk1EFj+QgLATqmbMNn+UgyVbUlhYMmDUfYNTASEXlgbDDY1D9p4qWHtGxqOJglNVJZ9c+oQcQAaqXjYPsBk8TVq1XLOTBY+q6mGAMkLENsMHEwP2CBskItkLSQ5Os4z+FggCQghh4zYzMEENNZBqdbyH0TtEMJIENnUdsscR+ywcTAELDEkGsyPSP2xO2uSkVWRxv1kyOJIYDIMq/lHJNZUA2WAiQPtuodaWog4mxsZ3iGDW5BxGxqSH0lyXY3JkqsBseyHtOxsfUdZyvG/wAjsdnY/kGyNNni7H8Nj8PugMI+6A+z7oDHEOx9U2P7sfzZCGx2IMizsejUbHkhsOx3LB2jY4MVxEOTsb2xDY2O5DYs2NjsMNj0zY2tj1CHY1NiWzD2zY1DDRAcRrdjOZGtrJNRsanJ1gFnY5gsLS5Go2NipqCxDscQs5NQsBscagAADJ2OarSqbG5ghhNjwQwBTFPG7HBYMixZ1OxrICGzCrsdHjLGx7LOxzbANQ7HtAIXY9Puhni+6BA7sd3Y+EJsf01mx7dn6bIl2OibIAhNkc7OxNjwcZxGx0PSNju8Q+mbGs7rs6iH0mxseTJ2PbseDZADqeJNjsanJ1sGxmO0cTsdnJ1GxtdZkmx5O06nY8HEanY8OxteIs7HhyTtmx0YfSNjo9p2PJxMGztf7Rs0mDndj2bHE2Sx7oC0MOx/O2ajW+6Auo+6Ffr7oIGnugIA7Hk2fJ7oCGuT7oFznuhCV/+h";
            //            eoConsumerTraceWseManager.WorkstationLoggedInUserName = "sulo";
            //            eoConsumerTraceWseManager.WorkStationName = "sulo-hp";
            //            eoConsumerTraceWseManager.EnquiryID = 1;
            //            eoConsumerTraceWseManager.EnquiryResultID = 2;
            //            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(string.Empty, string.Empty);
            //            ra.DIAVerification(eoConsumerTraceWseManager);

            //            MTNConnect.MTNConnect oM = new MTNConnect.MTNConnect();
            //            string ticketcanvas = oM.Login("ksulochana", "a9j8V9q");
            //            MTNConnect.CommercialQuestionnaire oQ = new MTNConnect.CommercialQuestionnaire();

            //            //CommercialQuestionnaire olQ = new CommercialQuestionnaire();

            //            string mtnreq = "<oCommercialQuestionnaire><CIPC>     <CompanyInformation>       <RegisteredName>TANDEM CRANE</RegisteredName>       <BusinessRegistrationNumber>199700773807</BusinessRegistrationNumber>       <PreviousBusinessName>prevbusnam</PreviousBusinessName>       <NameChangeDate>20161205</NameChangeDate>       <ConvertedRegNumber />       <RegistrationDate>20170904</RegistrationDate>       <BusinessStartDate>20160704</BusinessStartDate>       <TaxNumber>4670167156</TaxNumber>       <BusinessStatus>In Business,3</BusinessStatus>       <FinancialEffectiveDate>20170401</FinancialEffectiveDate>       <FinancialYearEnd>4</FinancialYearEnd>       <AuthorisedCapitalAmount>314</AuthorisedCapitalAmount>       <IssuedCapitalAmountorMemberContribution>9998</IssuedCapitalAmountorMemberContribution>       <NumberofAuthorisedShares>997.00000000</NumberofAuthorisedShares>       <NumberofIssuedShares>96</NumberofIssuedShares>     </CompanyInformation>     <Auditors>       <AuditorName>pwc</AuditorName>       <ProfessionNumber>88889</ProfessionNumber>       <AuditorStartDate>20160709</AuditorStartDate>       <TimewithAuditors>12</TimewithAuditors>       <AuditorType>Auditor</AuditorType>       <PhysicalAddress />       <PostalAddress />     </Auditors>     <Principals>       <Principals>         <FirstName>prin</FirstName>         <Surname>nam</Surname>         <PreviousSurname />         <PrincipalType />         <IDNumber>6702040128089</IDNumber>         <AppointmentDate>00010101</AppointmentDate>         <YearswithBusiness />         <PhysicalAddress />         <PostalAddress />         <PercentageMemberShareholding />       </Principals>     </Principals>   </CIPC>   <VAT>     <CompanyName>TANDEM CRANE</CompanyName>     <TradingName>TANDEM CRANE</TradingName>     <VatNumber>4670167156</VatNumber>   </VAT>   <CompanyOverview>     <CompanyOverview>       <TypeofStructure>Branches</TypeofStructure>       <NumberofStructure>45</NumberofStructure>       <Description>teststruct</Description>     </CompanyOverview>   </CompanyOverview>   <CompanyStructure>     <CompanyStructure>       <Name>strucname</Name>       <PercentageShareholding>30</PercentageShareholding>       <Type>Subsidiary,4</Type>       <Comments>done</Comments>     </CompanyStructure>   </CompanyStructure>   <Employees>     <NumberofSalariedEmployees>777</NumberofSalariedEmployees>     <NumberofWagedEmployees>780</NumberofWagedEmployees>     <NumberofCasualEmployees>770</NumberofCasualEmployees>     <NumberofContractedEmployees>10</NumberofContractedEmployees>   </Employees>   <CompanyFleet>     <CompanyFleet>       <Typeofvehicle>Light Duty Vehicles (Passenger Cars, Motorcycles, Combi Vans),1</Typeofvehicle>       <Numberofvehicles>6</Numberofvehicles>       <OwnedOrLeased>Owned</OwnedOrLeased>       <Value>4567</Value>       <OutstandingBalance>1235</OutstandingBalance>     </CompanyFleet>   </CompanyFleet>   <OperationAssesment>     <OperationalDetail>tradingcompany</OperationalDetail>     <IndustrySector>Chemicals,7</IndustrySector>     <Customers>dupont</Customers>     <ExportCountries>USA, Europe</ExportCountries>     <ImportCountries>India</ImportCountries>   </OperationAssesment>   <BEE>     <Level>65 - 74.99</Level>     <CertificateAvailable>True</CertificateAvailable>     <Expirydate>20180927</Expirydate>   </BEE>   <ISO>     <ISO>       <Status>True</Status>       <ISOScore>4560</ISOScore>       <CertificateAvailable>True</CertificateAvailable>     </ISO>   </ISO>   <WorkmansCompensation>     <Status>True</Status>     <CertificateAvailable>True</CertificateAvailable>   </WorkmansCompensation>   <TradeReferences>     <TradeReferences>       <Supplier>suppl</Supplier>       <SupplierContact>supplcontac</SupplierContact>     </TradeReferences>   </TradeReferences>   <Financials>     <Currentassets>6666666</Currentassets>     <Inventory>66666</Inventory>     <AccountsReceivable>66667</AccountsReceivable>     <Fixedassets>8888</Fixedassets>     <PlantEquipmentVehicles>11</PlantEquipmentVehicles>     <Property>6787878</Property>     <Currentliabilities>7778999</Currentliabilities>     <AccountsPayable>12121212</AccountsPayable>     <ShortTermsLoans>343412</ShortTermsLoans>     <LongTermLoans>45454545</LongTermLoans>     <Longtermliabilities>767676</Longtermliabilities>     <MortgageBonds>5454545</MortgageBonds>     <PreviousYearTurnover>1111177771111</PreviousYearTurnover>     <CurrentTurnoverpermonth>11111999</CurrentTurnoverpermonth>     <CurrentTurnoverperannum>1111177999</CurrentTurnoverperannum>   </Financials>   <NewBankCodes>     <AccountName>TANDEM CRANE</AccountName>     <Bank>Standard Bank,1</Bank>     <BranchName>PAROW CENTRE</BranchName>     <BranchCode>31110</BranchCode>     <AccountNumber>071684689</AccountNumber>   </NewBankCodes>   <Contracts>     <Contracts>       <ContractValue>45321</ContractValue>       <Remarks>awesome</Remarks>       <MainContractor>mtn</MainContractor>     </Contracts>   </Contracts> </oCommercialQuestionnaire>";

            //            XmlSerializer serializer = new XmlSerializer(typeof(MTNConnect.CommercialQuestionnaire));

            //                    oQ = (MTNConnect.CommercialQuestionnaire)serializer.Deserialize(new StringReader(mtnreq));

            //    using (var reader = XmlReader.Create(new StringReader(mtnreq)))
            //    {
            //        olQ = (CommercialQuestionnaire)serializer.Deserialize(reader);
            //    }

            ////    PropertyCopier<CommercialQuestionnaire, MTNConnect.CommercialQuestionnaire>.Copy(olQ, oQ);
            //    oQ.CopyPropertiesFrom(olQ);

            //  CopyProperties(olQ, oQ);


            //MTNConnect.CIPC ocipc = new MTNConnect.CIPC();
            //MTNConnect.CompanyInformation ocominfo = new MTNConnect.CompanyInformation();
            //ocominfo.BusinessRegistrationNumber = "1234/123456/07";
            //ocominfo.RegisteredName = "xdstest";
            //ocominfo.FinancialYearEnd = "0";
            //ocominfo.RegistrationDate = "20171107";
            //ocominfo.TaxNumber = "4410122958";
            //ocominfo.NumberofAuthorisedShares = "997.00000000";
            //ocominfo.NumberofIssuedShares = "96";
            //ocominfo.IssuedCapitalAmountorMemberContribution = "9998";

            //ocipc.CompanyInformation = ocominfo;
            //oQ.CIPC = ocipc;

            //MTNConnect.Auditors oAuditors = new MTNConnect.Auditors();
            //oAuditors.AuditorName = "Audito1";
            //oAuditors.ProfessionNumber = "88889";
            //oAuditors.AuditorStartDate = "20160709";
            //oAuditors.TimewithAuditors = "12";

            //ocipc.Auditors = oAuditors;
            //oQ.CIPC = ocipc;

            //MTNConnect.Principals oP = new MTNConnect.Principals();
            //oP.FirstName = "Test";
            //oP.Surname = "Testsurname";
            //oP.IDNumber = "jhgs67";


            //MTNConnect.Principals oP1 = new MTNConnect.Principals();
            //oP1.FirstName = "Test";
            //oP1.Surname = "Testsurname";
            //oP1.IDNumber = "jhgs67";
            //oP1.AppointmentDate = "00010101";

            //ocipc.Principals = new MTNConnect.Principals[2];
            //ocipc.Principals[0] = oP; ocipc.Principals[1] = oP1;


            //MTNConnect.VAT oVat= new MTNConnect.VAT();
            //oVat.CompanyName = "xyz";
            //oVat.TradingName = "df";
            //oVat.VatNumber = "4670167156";

            //MTNConnect.CompanyOverview oview = new MTNConnect.CompanyOverview();

            //oview.Description = "sdfsdf";
            //oview.TypeofStructure = "skjdfh";
            //oview.NumberofStructure = "45";

            //oQ.CompanyOverview = new MTNConnect.CompanyOverview[1];
            //oQ.CompanyOverview[0] = oview;

            //MTNConnect.CompanyStructure oS = new MTNConnect.CompanyStructure();
            //oS.Name =

            //MTNConnect.VAT oV = new MTNConnect.VAT();
            //oQ.VAT = oV;


            //string response = oM.CommercialQuestionnaire(ticket, string.Empty,140, oQ);
            //Console.WriteLine(response);
            //Console.ReadLine();


            //string registrationno = "M1956/000518/07";
            //String x = registrationno.Substring(registrationno.IndexOf("/") + 1).Substring(0, registrationno.Substring(registrationno.IndexOf("/") + 1).IndexOf("/"));

            //UATXDSConnect.XDSConnectWS oConnect = new UATXDSConnect.XDSConnectWS();

            //string strLoginTicket = oConnect.Login("oladayo", "oladayo");

            //if (oConnect.IsTicketValid(strLoginTicket))
            //{
            //    //byte[] avsfile = File.ReadAllBytes("D:\\AVS.csv");
            //    //string avsfilestring = System.Convert.ToBase64String(avsfile);

            //    //string resavs = oConnect.ConnectBatchAVSRequest(strLoginTicket, "AVS.csv", avsfilestring);

            //    string repxml = oConnect.ConnectGetHistoryReport(strLoginTicket, 83398432);
            //}
        }
    }
}
