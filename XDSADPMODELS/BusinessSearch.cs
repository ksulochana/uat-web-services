﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDSADPMODELS
{
    public enum BusinessSearchType { NAME, REGNO, /*ITNO, DUNSNO,*/ VATNO/*, BANKACCOUNTNO, TRADINGNO*/ };
    public enum BusinessResponseStatus { SUCCESS, FAILURE };
    public partial class BusinessSearch
    {

        private Search businessSearchField;
        

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("BusinessSearch")]
        public Search BusinessSearch1
        {
            get
            {
                return this.businessSearchField;
            }
            set
            {
                this.businessSearchField = value;
            }
        }

      
    }

  

    /// <remarks/>
    public partial class Search
    {

        private string searchTypeField;

        private string subjectNameField;

        private string registrationNoField;

        private string iTNumberField;

        private string dunsNumberField;

        private string vatNumberField;

        private string bankAccountNumberField;

        private string tradingNumberField;

        private string userField;

        private string passwordField;

        private string clientReferenceField;

        /// <remarks/>
        public string SearchType
        {
            get
            {
                return this.searchTypeField;
            }
            set
            {
                this.searchTypeField = value;
            }
        }

        /// <remarks/>
        public string SubjectName
        {
            get
            {
                return this.subjectNameField;
            }
            set
            {
                this.subjectNameField = value;
            }
        }

        /// <remarks/>
        public string RegistrationNo
        {
            get
            {
                return this.registrationNoField;
            }
            set
            {
                this.registrationNoField = value;
            }
        }

        /// <remarks/>
        //public string ITNumber
        //{
        //    get
        //    {
        //        return this.iTNumberField;
        //    }
        //    set
        //    {
        //        this.iTNumberField = value;
        //    }
        //}

        ///// <remarks/>
        //public string DunsNumber
        //{
        //    get
        //    {
        //        return this.dunsNumberField;
        //    }
        //    set
        //    {
        //        this.dunsNumberField = value;
        //    }
        //}

        /// <remarks/>
        public string VatNumber
        {
            get
            {
                return this.vatNumberField;
            }
            set
            {
                this.vatNumberField = value;
            }
        }

        /// <remarks/>
        public string BankAccountNumber
        {
            get
            {
                return this.bankAccountNumberField;
            }
            set
            {
                this.bankAccountNumberField = value;
            }
        }

        /// <remarks/>
        //public string TradingNumber
        //{
        //    get
        //    {
        //        return this.tradingNumberField;
        //    }
        //    set
        //    {
        //        this.tradingNumberField = value;
        //    }
        //}

        public string User
        {
            get
            {
                return this.userField;
            }
            set
            {
                this.userField = value;
            }
        }

        public string Password
        {
            get
            {
                return this.passwordField;
            }
            set
            {
                this.passwordField = value;
            }
        }

        public string ClientReference
        {
            get
            {
                return this.clientReferenceField;
            }
            set
            {
                this.clientReferenceField = value;
            }
        }


    }
}
