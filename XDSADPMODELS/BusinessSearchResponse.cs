﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace XDSADPMODELS
{
    public partial class BusinessSearchResponse
    {

        private BusinessSearchResult businessSearchResultField;

        /// <remarks/>
        public BusinessSearchResult BusinessSearchResult
        {
            get
            {
                return this.businessSearchResultField;
            }
            set
            {
                this.businessSearchResultField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class BusinessSearchResult
    {


        private string rawDataField;

        private BusinessResponseStatus responseStatusField;

        private DateTime processingStartDateField = DateTime.Now;

        private float processingTimeSecsField;

        private string uniqueRefGuidField;

        private string errorDescriptionField;

        [XmlArray("SearchResponse")]
        [XmlArrayItem("ResponseData")]
        private SearchResponseData[] searchResponseField;

        /// <remarks/>
        public string RawData
        {
            get
            {
                return this.rawDataField;
            }
            set
            {
                this.rawDataField = value;
            }
        }

        /// <remarks/>
        public BusinessResponseStatus ResponseStatus
        {
            get
            {
                return this.responseStatusField;
            }
            set
            {
                this.responseStatusField = value;
            }
        }

        /// <remarks/>
        public DateTime ProcessingStartDate
        {
            get
            {
                return this.processingStartDateField;
            }
            set
            {
                this.processingStartDateField = value;
            }
        }

        /// <remarks/>
        public float ProcessingTimeSecs
        {
            get
            {
                return this.processingTimeSecsField;
            }
            set
            {
                this.processingTimeSecsField = value;
            }
        }

        /// <remarks/>
        public string UniqueRefGuid
        {
            get
            {
                return this.uniqueRefGuidField;
            }
            set
            {
                this.uniqueRefGuidField = value;
            }
        }

        public string ErrorDescription
        {
            get
            {
                return this.errorDescriptionField;
            }
            set
            {
                this.errorDescriptionField = value;
            }
        }


        /// <remarks/>

        public SearchResponseData[] SearchResponse
        {
            get
            {
                return this.searchResponseField;
            }
            set
            {
                this.searchResponseField = value;
            }
        }
    }

    ///// <remarks/>
    ////[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class SearchResponse
    //{

    //    private SearchResponseData[] searchResponseField;
    //    // [System.Xml.Serialization.XmlElement("SearchResponse")]
    //    /// <remarks/>
        
    //    public SearchResponseData[] Response
    //    {
    //        get
    //        {
    //            return this.searchResponseField;
    //        }
    //        set
    //        {
    //            this.searchResponseField = value;
    //        }
    //    }
    //}

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class SearchResponseData
    {

        //private string enquiryIDField;

        //private string enquiryResultIDField;

        private string nameField;

        private string nameTypeField;

        private string businessNameField;

        private string physicalAddressField;

        private string suburbField;

        private string townField;

        private string countryField;

        private string postCodeField;

        private string postalAddressField;

        private string postalSuburbField;

        private string postalTownField;

        private string postalCountryField;

        private string postalPostCodeField;

        private string phoneNoField;

        private string faxNoField;

        private string regNoField;

        private string regStatusField;

        private string regStatusCodeField;

        private string tradingNumberField;

        private string tUNumberField;

        /// <remarks/>
        //public string EnquiryID
        //{
        //    get
        //    {
        //        return this.enquiryIDField;
        //    }
        //    set
        //    {
        //        this.enquiryIDField = value;
        //    }
        //}

        public string BureauNumber
        {
            get
            {
                return this.tUNumberField;
            }
            set
            {
                this.tUNumberField = value;
            }
        }

        /// <remarks/>
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public string NameType
        {
            get
            {
                return this.nameTypeField;
            }
            set
            {
                this.nameTypeField = value;
            }
        }

        /// <remarks/>
        public string BusinessName
        {
            get
            {
                return this.businessNameField;
            }
            set
            {
                this.businessNameField = value;
            }
        }

        /// <remarks/>
        public string PhysicalAddress
        {
            get
            {
                return this.physicalAddressField;
            }
            set
            {
                this.physicalAddressField = value;
            }
        }

        /// <remarks/>
        public string Suburb
        {
            get
            {
                return this.suburbField;
            }
            set
            {
                this.suburbField = value;
            }
        }

        /// <remarks/>
        public string Town
        {
            get
            {
                return this.townField;
            }
            set
            {
                this.townField = value;
            }
        }

        /// <remarks/>
        public string Country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        /// <remarks/>
        public string PostCode
        {
            get
            {
                return this.postCodeField;
            }
            set
            {
                this.postCodeField = value;
            }
        }

        /// <remarks/>
        public string PostalAddress
        {
            get
            {
                return this.postalAddressField;
            }
            set
            {
                this.postalAddressField = value;
            }
        }

        /// <remarks/>
        public string PostalSuburb
        {
            get
            {
                return this.postalSuburbField;
            }
            set
            {
                this.postalSuburbField = value;
            }
        }

        /// <remarks/>
        public string PostalTown
        {
            get
            {
                return this.postalTownField;
            }
            set
            {
                this.postalTownField = value;
            }
        }

        /// <remarks/>
        public string PostalCountry
        {
            get
            {
                return this.postalCountryField;
            }
            set
            {
                this.postalCountryField = value;
            }
        }

        /// <remarks/>
        public string PostalPostCode
        {
            get
            {
                return this.postalPostCodeField;
            }
            set
            {
                this.postalPostCodeField = value;
            }
        }

        /// <remarks/>
        public string PhoneNo
        {
            get
            {
                return this.phoneNoField;
            }
            set
            {
                this.phoneNoField = value;
            }
        }

        /// <remarks/>
        public string FaxNo
        {
            get
            {
                return this.faxNoField;
            }
            set
            {
                this.faxNoField = value;
            }
        }

        /// <remarks/>
        public string RegNo
        {
            get
            {
                return this.regNoField;
            }
            set
            {
                this.regNoField = value;
            }
        }

        /// <remarks/>
        public string RegStatus
        {
            get
            {
                return this.regStatusField;
            }
            set
            {
                this.regStatusField = value;
            }
        }

        /// <remarks/>
        public string RegStatusCode
        {
            get
            {
                return this.regStatusCodeField;
            }
            set
            {
                this.regStatusCodeField = value;
            }
        }

        /// <remarks/>
        public string TradingNumber
        {
            get
            {
                return this.tradingNumberField;
            }
            set
            {
                this.tradingNumberField = value;
            }
        }
    }
}
