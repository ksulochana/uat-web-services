﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace XDSADPMODELS
{
    public partial class ModuleRequest
    {

        private ModuleRequestModuleRequest moduleRequest1Field;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ModuleRequest")]
        public ModuleRequestModuleRequest ModuleRequest1
        {
            get
            {
                return this.moduleRequest1Field;
            }
            set
            {
                this.moduleRequest1Field = value;
            }
        }
    }

    public class DirectorInformation
    {
        private string directoridnumberField;
        private decimal directorincomeField;


        public string ID
        {
            get
            {
                return this.directoridnumberField;
            }
            set
            {
                this.directoridnumberField = value;
            }
        }

        public Decimal Income
        {
            get
            {
                return this.directorincomeField;
            }
            set
            {
                this.directorincomeField = value;
            }
        }
    }

    public class CompanyInformationRequest
    {
        private int employeecountField;
        private decimal turnoverField;
        private string tradeindustryField;
        private DirectorInformation[] directorsField;


        public int EmployeeCount
        {
            get
            {
                return this.employeecountField;
            }
            set
            {
                this.employeecountField = value;
            }
        }

        public Decimal Turnover
        {
            get
            {
                return this.turnoverField;
            }
            set
            {
                this.turnoverField = value;
            }
        }

        public string TradeIndustry
        {
            get
            {
                return this.tradeindustryField;
            }
            set
            {
                this.tradeindustryField = value;
            }
        }

        public DirectorInformation[] Directors
        {
            get
            {
                return this.directorsField;
            }
            set
            {
                this.directorsField = value;
            }
        }
    }

    public partial class ModuleRequestModuleRequest
    {

        public enum enumComEnquiryReason { CreditRisk, CreditLimitManagement, InsuranceApplication, Employment, FraudCorruptionTheftInv, FraudDetectionFraudPrev, SettingLimit, UnclaimedFundsDistribution, AffordabilityAssessment, PrescreeningMarketing, Other, DebtorBookAssessment, TracingByCreditProvider, ModelDevelopment, Contactibility, CreditScoringModel, CreditInformationQuery, DebtCounseling, CreditApplication, UpdateRecords, Supplier, DefaultCheck, FactualCheck, ReferanceCheck, Tracing, Lease, Rental, Surety, InternalEnquiry, Marketing, UnknownCPUtoCPU, InternationalEnquiry, SubscriberConcern, AccessCheck, InsuranceClaim, EmploymentCheck, FraudInvestigation, FraudDetection, ProvisionLimit, CreditScoreDevelopment, Affordability, PropensityToRepay };

        private string userField;
        private string passwordField;

        private string tUNumberField;

        private string[] moduleProductCodesField;

        private string enquiryReasonField;

        private string enquiryAmountField;

        private string termsField;

        private string userOnTicketField;

        private string contactForenameField;

        private string contactSurnameField;

        private string contactPhoneCodeField;

        private string contactPhoneNoField;

        private string contactFaxCodeField;

        private string contactFaxNoField;

        private string additionalClientReferenceField;

        private string subjectPhoneCodeField;

        private string subjectPhoneNoField;

        private string subjectNameOnTicketField;

        private string subjectAddressField;

        private string subjectSuburbField;

        private string subjectCityField;

        private string subjectPostCodeField;

        private string investigateOptionField;

        private string bankAccountNoField;

        private string bankAbbreviationField;

        private string bankBranchField;

        private string bankBranchCodeField;

        private string specialInstructionsField;

        private string bankCreditAmountField;

        private string bankTermsGivenField;

        private string bankAccountHolderField;
      
        private string isExistingClientField;

        public string User
        {
            get
            {
                return this.userField;
            }
            set
            {
                this.userField = value;
            }
        }

        public string Password
        {
            get
            {
                return this.passwordField;
            }
            set
            {
                this.passwordField = value;
            }
        }

        /// <remarks/>
        public string BureauNumber
        {
            get
            {
                return this.tUNumberField;
            }
            set
            {
                this.tUNumberField = value;
            }
        }


        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public string[] ModuleProductCodes
        {
            get
            {
                return this.moduleProductCodesField;
            }
            set
            {
                this.moduleProductCodesField = value;
            }
        }

        /// <remarks/>
        public string EnquiryReason
        {
            get
            {
                return this.enquiryReasonField;
            }
            set
            {
                this.enquiryReasonField = value;
            }
        }

        /// <remarks/>
        public string EnquiryAmount
        {
            get
            {
                return this.enquiryAmountField;
            }
            set
            {
                this.enquiryAmountField = value;
            }
        }

        /// <remarks/>
        public string Terms
        {
            get
            {
                return this.termsField;
            }
            set
            {
                this.termsField = value;
            }
        }

        /// <remarks/>
        public string UserOnTicket
        {
            get
            {
                return this.userOnTicketField;
            }
            set
            {
                this.userOnTicketField = value;
            }
        }

        /// <remarks/>
        public string ContactForename
        {
            get
            {
                return this.contactForenameField;
            }
            set
            {
                this.contactForenameField = value;
            }
        }

        /// <remarks/>
        public string ContactSurname
        {
            get
            {
                return this.contactSurnameField;
            }
            set
            {
                this.contactSurnameField = value;
            }
        }

        /// <remarks/>
        public string ContactPhoneCode
        {
            get
            {
                return this.contactPhoneCodeField;
            }
            set
            {
                this.contactPhoneCodeField = value;
            }
        }

        /// <remarks/>
        public string ContactPhoneNo
        {
            get
            {
                return this.contactPhoneNoField;
            }
            set
            {
                this.contactPhoneNoField = value;
            }
        }

        /// <remarks/>
        public string ContactFaxCode
        {
            get
            {
                return this.contactFaxCodeField;
            }
            set
            {
                this.contactFaxCodeField = value;
            }
        }

        /// <remarks/>
        public string ContactFaxNo
        {
            get
            {
                return this.contactFaxNoField;
            }
            set
            {
                this.contactFaxNoField = value;
            }
        }

        /// <remarks/>
        public string AdditionalClientReference
        {
            get
            {
                return this.additionalClientReferenceField;
            }
            set
            {
                this.additionalClientReferenceField = value;
            }
        }

        /// <remarks/>
        public string SubjectPhoneCode
        {
            get
            {
                return this.subjectPhoneCodeField;
            }
            set
            {
                this.subjectPhoneCodeField = value;
            }
        }

        /// <remarks/>
        public string SubjectPhoneNo
        {
            get
            {
                return this.subjectPhoneNoField;
            }
            set
            {
                this.subjectPhoneNoField = value;
            }
        }

        /// <remarks/>
        public string SubjectNameOnTicket
        {
            get
            {
                return this.subjectNameOnTicketField;
            }
            set
            {
                this.subjectNameOnTicketField = value;
            }
        }

        /// <remarks/>
        public string SubjectAddress
        {
            get
            {
                return this.subjectAddressField;
            }
            set
            {
                this.subjectAddressField = value;
            }
        }

        /// <remarks/>
        public string SubjectSuburb
        {
            get
            {
                return this.subjectSuburbField;
            }
            set
            {
                this.subjectSuburbField = value;
            }
        }

        /// <remarks/>
        public string SubjectCity
        {
            get
            {
                return this.subjectCityField;
            }
            set
            {
                this.subjectCityField = value;
            }
        }

        /// <remarks/>
        public string SubjectPostCode
        {
            get
            {
                return this.subjectPostCodeField;
            }
            set
            {
                this.subjectPostCodeField = value;
            }
        }

        /// <remarks/>
        public string InvestigateOption
        {
            get
            {
                return this.investigateOptionField;
            }
            set
            {
                this.investigateOptionField = value;
            }
        }

        /// <remarks/>
        public string BankAccountNo
        {
            get
            {
                return this.bankAccountNoField;
            }
            set
            {
                this.bankAccountNoField = value;
            }
        }

        /// <remarks/>
        public string BankAbbreviation
        {
            get
            {
                return this.bankAbbreviationField;
            }
            set
            {
                this.bankAbbreviationField = value;
            }
        }

        /// <remarks/>
        public string BankBranch
        {
            get
            {
                return this.bankBranchField;
            }
            set
            {
                this.bankBranchField = value;
            }
        }

        /// <remarks/>
        public string BankBranchCode
        {
            get
            {
                return this.bankBranchCodeField;
            }
            set
            {
                this.bankBranchCodeField = value;
            }
        }

        /// <remarks/>
        public string SpecialInstructions
        {
            get
            {
                return this.specialInstructionsField;
            }
            set
            {
                this.specialInstructionsField = value;
            }
        }

        /// <remarks/>
        public string BankCreditAmount
        {
            get
            {
                return this.bankCreditAmountField;
            }
            set
            {
                this.bankCreditAmountField = value;
            }
        }

        /// <remarks/>
        public string BankTermsGiven
        {
            get
            {
                return this.bankTermsGivenField;
            }
            set
            {
                this.bankTermsGivenField = value;
            }
        }

        /// <remarks/>
        public string BankAccountHolder
        {
            get
            {
                return this.bankAccountHolderField;
            }
            set
            {
                this.bankAccountHolderField = value;
            }
        }
       
        public string IsExistingClient
        {
            get
            {
                return this.isExistingClientField;
            }
            set
            {
                this.isExistingClientField = value;
            }
        }

    }
    public partial class MTNCommercialFinalOutput
    {

        private string statusField;

        private string errorDescriptionField;

        private long applicationIDField;

        private bool applicationIDFieldSpecified;

        private string existingClientField;

        private string accountNumberField;

        private string investigateFlagField;

        private string investigateRecommendationField;

        private string finalOutcomeField;

        private string finalOutcomeReasonField;

        private string matrixBandField;

        private decimal companyScoreField;

        private bool companyScoreFieldSpecified;

        private string companyBandField;

        private int sMEFilter01Field;

        private bool sMEFilter01FieldSpecified;

        private int sMEFilter02Field;

        private bool sMEFilter02FieldSpecified;

        private int sMEFilter03Field;

        private bool sMEFilter03FieldSpecified;

        private int sMEFilter04Field;

        private bool sMEFilter04FieldSpecified;

        private Principal[] principalsField;

        private decimal finalPrincipalScoreField = 0;

        private bool finalPrincipalScoreFieldSpecified;

        private string finalPrincipalBandField;

        private string scoreFlagField;

        private string dateScoredField;

        private decimal behaviouralScoreField;

        private bool behaviouralScoreFieldSpecified;

        private string behaviouralRiskBandField;

        private CommercialResponse bureauDataField;
        private EnterpriseExclusionRule[] enterpriseExclusionRulesField;
        private PrincipalExclusionRule[] principalExclusionRulesField;

        /// <remarks/>

        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>

        public string ErrorDescription
        {
            get
            {
                return this.errorDescriptionField;
            }
            set
            {
                this.errorDescriptionField = value;
            }
        }

        /// <remarks/>
        public long ApplicationID
        {
            get
            {
                return this.applicationIDField;
            }
            set
            {
                this.applicationIDField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlIgnoreAttribute()]
        //public bool ApplicationIDSpecified {
        //    get {
        //        return this.applicationIDFieldSpecified;
        //    }
        //    set {
        //        this.applicationIDFieldSpecified = value;
        //    }
        //}

        /// <remarks/>

        public string ExistingClient
        {
            get
            {
                return this.existingClientField;
            }
            set
            {
                this.existingClientField = value;
            }
        }

        /// <remarks/>

        public string AccountNumber
        {
            get
            {
                return this.accountNumberField;
            }
            set
            {
                this.accountNumberField = value;
            }
        }

        /// <remarks/>

        //public string InvestigateFlag
        //{
        //    get
        //    {
        //        return this.investigateFlagField;
        //    }
        //    set
        //    {
        //        this.investigateFlagField = value;
        //    }
        //}

        ///// <remarks/>

        //public string InvestigateRecommendation
        //{
        //    get
        //    {
        //        return this.investigateRecommendationField;
        //    }
        //    set
        //    {
        //        this.investigateRecommendationField = value;
        //    }
        //}

        /// <remarks/>

        public string FinalOutcome
        {
            get
            {
                return this.finalOutcomeField;
            }
            set
            {
                this.finalOutcomeField = value;
            }
        }

        /// <remarks/>

        public string FinalOutcomeReason
        {
            get
            {
                return this.finalOutcomeReasonField;
            }
            set
            {
                this.finalOutcomeReasonField = value;
            }
        }

        /// <remarks/>

        public string MatrixBand
        {
            get
            {
                return this.matrixBandField;
            }
            set
            {
                this.matrixBandField = value;
            }
        }

        /// <remarks/>
        public decimal CompanyScore
        {
            get
            {
                return this.companyScoreField;
            }
            set
            {
                this.companyScoreField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlIgnoreAttribute()]
        //public bool CompanyScoreSpecified {
        //    get {
        //        return this.companyScoreFieldSpecified;
        //    }
        //    set {
        //        this.companyScoreFieldSpecified = value;
        //    }
        //}

        /// <remarks/>

        public string CompanyBand
        {
            get
            {
                return this.companyBandField;
            }
            set
            {
                this.companyBandField = value;
            }
        }

        /// <remarks/>
        //public int SMEFilter01
        //{
        //    get
        //    {
        //        return this.sMEFilter01Field;
        //    }
        //    set
        //    {
        //        this.sMEFilter01Field = value;
        //    }
        //}

        /////// <remarks/>
        ////[System.Xml.Serialization.XmlIgnoreAttribute()]
        ////public bool SMEFilter01Specified {
        ////    get {
        ////        return this.sMEFilter01FieldSpecified;
        ////    }
        ////    set {
        ////        this.sMEFilter01FieldSpecified = value;
        ////    }
        ////}

        ///// <remarks/>
        //public int SMEFilter02
        //{
        //    get
        //    {
        //        return this.sMEFilter02Field;
        //    }
        //    set
        //    {
        //        this.sMEFilter02Field = value;
        //    }
        //}

        ///// <remarks/>
        ////[System.Xml.Serialization.XmlIgnoreAttribute()]
        ////public bool SMEFilter02Specified {
        ////    get {
        ////        return this.sMEFilter02FieldSpecified;
        ////    }
        ////    set {
        ////        this.sMEFilter02FieldSpecified = value;
        ////    }
        ////}

        ///// <remarks/>
        //public int SMEFilter03
        //{
        //    get
        //    {
        //        return this.sMEFilter03Field;
        //    }
        //    set
        //    {
        //        this.sMEFilter03Field = value;
        //    }
        //}

        ///// <remarks/>
        ////[System.Xml.Serialization.XmlIgnoreAttribute()]
        ////public bool SMEFilter03Specified {
        ////    get {
        ////        return this.sMEFilter03FieldSpecified;
        ////    }
        ////    set {
        ////        this.sMEFilter03FieldSpecified = value;
        ////    }
        ////}

        ///// <remarks/>
        //public int SMEFilter04
        //{
        //    get
        //    {
        //        return this.sMEFilter04Field;
        //    }
        //    set
        //    {
        //        this.sMEFilter04Field = value;
        //    }
        //}

        /// <remarks/>
        //[System.Xml.Serialization.XmlIgnoreAttribute()]
        //public bool SMEFilter04Specified {
        //    get {
        //        return this.sMEFilter04FieldSpecified;
        //    }
        //    set {
        //        this.sMEFilter04FieldSpecified = value;
        //    }
        //}

        /// <remarks/>

        //public Principal[] Principals
        //{
        //    get
        //    {
        //        return this.principalsField;
        //    }
        //    set
        //    {
        //        this.principalsField = value;
        //    }
        //}

        /// <remarks/>

        public decimal FinalPrincipalScore
        {
            get
            {
                return this.finalPrincipalScoreField;
            }
            set
            {
                this.finalPrincipalScoreField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlIgnoreAttribute()]
        //public bool FinalPrincipalScoreSpecified {
        //    get {
        //        return this.finalPrincipalScoreFieldSpecified;
        //    }
        //    set {
        //        this.finalPrincipalScoreFieldSpecified = value;
        //    }
        //}

        /// <remarks/>

        public string FinalPrincipalBand
        {
            get
            {
                return this.finalPrincipalBandField;
            }
            set
            {
                this.finalPrincipalBandField = value;
            }
        }

        /// <remarks/>

        public string ScoreFlag
        {
            get
            {
                return this.scoreFlagField;
            }
            set
            {
                this.scoreFlagField = value;
            }
        }

        /// <remarks/>

        //public string DateScored
        //{
        //    get
        //    {
        //        return this.dateScoredField;
        //    }
        //    set
        //    {
        //        this.dateScoredField = value;
        //    }
        //}

        /// <remarks/>
        public decimal BehaviouralScore
        {
            get
            {
                return this.behaviouralScoreField;
            }
            set
            {
                this.behaviouralScoreField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlIgnoreAttribute()]
        //public bool BehaviouralScoreSpecified {
        //    get {
        //        return this.behaviouralScoreFieldSpecified;
        //    }
        //    set {
        //        this.behaviouralScoreFieldSpecified = value;
        //    }
        //}

        /// <remarks/>

        public string BehaviouralRiskBand
        {
            get
            {
                return this.behaviouralRiskBandField;
            }
            set
            {
                this.behaviouralRiskBandField = value;
            }
        }

        /// <remarks/>

        public CommercialResponse BureauData
        {
            get
            {
                return this.bureauDataField;
            }
            set
            {
                this.bureauDataField = value;
            }
        }

        public EnterpriseExclusionRule[] EnterpriseExclusionRules
        {
            get
            {
                return this.enterpriseExclusionRulesField;
            }
            set
            {
                this.enterpriseExclusionRulesField = value;
            }
        }

        public PrincipalExclusionRule[] PrincipalExclusionRules
        {
            get
            {
                return this.principalExclusionRulesField;
            }
            set
            {
                this.principalExclusionRulesField = value;
            }
        }
    }


    public partial class Principal
    {

        private int principalDateOfBirthField;

        private bool principalDateOfBirthFieldSpecified;

        private int principalFilter01Field;

        private bool principalFilter01FieldSpecified;

        private int principalFilter02Field;

        private bool principalFilter02FieldSpecified;

        private int principalFilter03Field;

        private bool principalFilter03FieldSpecified;

        private int principalFilter04Field;

        private bool principalFilter04FieldSpecified;

        private string principalFirstNameField;

        private string principalIdentityNumberField;

        private decimal principalScoreField;

        private bool principalScoreFieldSpecified;

        private string principalSurnameField;

        private PrincipleCharacterstics[] principleVariablesField;

        /// <remarks/>
        public int PrincipalDateOfBirth
        {
            get
            {
                return this.principalDateOfBirthField;
            }
            set
            {
                this.principalDateOfBirthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PrincipalDateOfBirthSpecified
        {
            get
            {
                return this.principalDateOfBirthFieldSpecified;
            }
            set
            {
                this.principalDateOfBirthFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int PrincipalFilter01
        {
            get
            {
                return this.principalFilter01Field;
            }
            set
            {
                this.principalFilter01Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PrincipalFilter01Specified
        {
            get
            {
                return this.principalFilter01FieldSpecified;
            }
            set
            {
                this.principalFilter01FieldSpecified = value;
            }
        }

        /// <remarks/>
        public int PrincipalFilter02
        {
            get
            {
                return this.principalFilter02Field;
            }
            set
            {
                this.principalFilter02Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PrincipalFilter02Specified
        {
            get
            {
                return this.principalFilter02FieldSpecified;
            }
            set
            {
                this.principalFilter02FieldSpecified = value;
            }
        }

        /// <remarks/>
        public int PrincipalFilter03
        {
            get
            {
                return this.principalFilter03Field;
            }
            set
            {
                this.principalFilter03Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PrincipalFilter03Specified
        {
            get
            {
                return this.principalFilter03FieldSpecified;
            }
            set
            {
                this.principalFilter03FieldSpecified = value;
            }
        }

        /// <remarks/>
        public int PrincipalFilter04
        {
            get
            {
                return this.principalFilter04Field;
            }
            set
            {
                this.principalFilter04Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PrincipalFilter04Specified
        {
            get
            {
                return this.principalFilter04FieldSpecified;
            }
            set
            {
                this.principalFilter04FieldSpecified = value;
            }
        }

        /// <remarks/>

        public string PrincipalFirstName
        {
            get
            {
                return this.principalFirstNameField;
            }
            set
            {
                this.principalFirstNameField = value;
            }
        }

        /// <remarks/>

        public string PrincipalIdentityNumber
        {
            get
            {
                return this.principalIdentityNumberField;
            }
            set
            {
                this.principalIdentityNumberField = value;
            }
        }

        /// <remarks/>
        public decimal PrincipalScore
        {
            get
            {
                return this.principalScoreField;
            }
            set
            {
                this.principalScoreField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PrincipalScoreSpecified
        {
            get
            {
                return this.principalScoreFieldSpecified;
            }
            set
            {
                this.principalScoreFieldSpecified = value;
            }
        }

        /// <remarks/>

        public string PrincipalSurname
        {
            get
            {
                return this.principalSurnameField;
            }
            set
            {
                this.principalSurnameField = value;
            }
        }

        public PrincipleCharacterstics[] PrincipleVariables
        {
            get { return this.principleVariablesField; }
            set { this.principleVariablesField = value; }
        }
    }



    public partial class CommercialResponse
    {

        private AccountVerificationVA[] accountVerificationVAField;

        private AccountVerificationVH[] accountVerificationVHField;

        private Affiliations[] affiliationsField;

        private AggregateCW aggregateCWField;

        private BankCodes[] bankCodesField;

        private BankHistory[] bankHistoryField;

        private BankReport[] bankReportField;

        private BankingDetailSummary[] bankingDetailsSummaryField;

        private Branch[] branchField;

        private BusinessAdverseSummary businessAdverseSummaryField;

        private BusinessDeedsComprehensivePB[] businessDeedsComprehensivePBField;

        private BusinessDeedsComprehensivePW[] businessDeedsComprehensivePWField;

        private BusinessDeedsDI[] businessDeedsDIField;

        private BusinessDeedsSummaryDA[] businessDeedsSummaryDAField;

        private BusinessNotarialBonds[] businessNotarialBondsField;

        private BusinessPlusPrincipalSummary[] businessPlusPrincipalsSummaryField;

        private BusinessRescueDetail businessRescueDetailField;

        private BusinessBenchmarkBB[] businessbenchmarkField;

        private CancelledTicket[] cancelledTicketsField;

        private CapitalEmployed capitalEmployedField;

        private CommercialDisputeResponse commercialDisputeResponseField;

        private ComplianceIndicatorLI[] complianceIndicatorLIField;

        private ConsumerBusinessEnquiry[] consumerBusinessEnquiriesField;

        private ConsumerDefault[] consumerDefaultsField;

        private ConsumerEnquiry[] consumerEnquiriesField;

        private ConsumerHeaderC1[] consumerHeaderC1Field;

        private ConsumerHeader[] consumerHeaderField;

        private ConsumerInfoNO04[] consumerInfoNO04Field;

        private ConsumerJudgement[] consumerJudgementsField;

        private ConsumerNotarialBonds[] consumerNotarialBondsField;

        private ConsumerNotice[] consumerNoticesField;

        private ConsumerTraceAlert[] consumerTraceAlertsField;

        private CourtRecord[] courtRecordsField;

        private CurrentAsset currentAssetField;

        private CurrentLiabilities currentLiabilitiesField;

        private DeedHistory[] deedHistoryField;

        private DeedsMultipleBond[] deedsMultipleBondField;

        private Default[] defaultsField;

        private DynamicRating dynamicRatingField;

        private EmpOfCapital empOfCapitalField;

        private EmpiricaE1[] empiricaE1Field;

        private EmpiricaEM04 empiricaEM04Field;

        private EnquiryHistory[] enquiryHistoryField;

        private EnquirySummary enquirySummaryField;

        private string errorCodeField;

        private string errorMessageField;

        private FinanceDataFE[] financeDataFEField;

        private FinanceDataFF[] financeDataFFField;

        private FinanceData[] financeDataField;

        private FinanceHeader financeHeaderField;

        private FinancialRatios[] financialRatiosField;

        private FirstResponse firstResponseField;

        private GeneralBankingInfo[] generalBankingInfoField;

        private Header headerField;

        private LinkedBusinessHeader[] linkedBusinessHeadersField;

        private MailboxRetrieveList mailboxRetrieveListField;

        private ModuleAvailabilityResponse moduleAvailabilityResponseField;

        private Names namesField;

        private NotarialBond[] notarialBondsField;

        private ObservationCont[] observationContField;

        private Observation[] observationsField;

        private Operation[] operationsField;

        private AdditionalOperations[] operationsHeaderField;

        private OtherOperation[] otherOperationsField;

        private PrincipalArchive[] principalArchivesField;

        private PrincipalArchiveP5[] principalArchivesP5Field;

        private PrincipalClearance[] principalClearancesField;

        private PrincipalDeedsComprehensiveCA[] principalDeedsCAField;

        private PrincipalDeedsComprehensiveCV[] principalDeedsComprehensiveCVField;

        private PrincipalDeedsSummaryCO[] principalDeedsSummaryCOField;

        private PrincipalDeedsSummaryP8[] principalDeedsSummaryP8Field;

        private PrincipalFirstResponse principalFirstResponseField;

        private PrincipalIDV[] principalIDVsField;

        // private PrincipalLink[] principalLinksField;
        private PrincipalLinks[] principalLinksField;

        private PrincipalNotarialBonds[] principalNotarialBondsField;

        private PrincipalDetailEmpirica[] principalsDetailEmpiricaField;

        private Principal1[] principalsField;

        private System.DateTime processingStartDateField;

        private double processingTimeSecsField;

        private string rawDataField;

        private RegisteredPrincipal[] registeredPrincipalsField;

        private RegistrationDetailsExtended registrationDetailsExtendedField;

        private RegistrationDetails registrationDetailsField;

        private ResponseStatus responseStatusField;

        private SMEAssessment sMEAssessmentField;

        private SearchResponse[] searchResponseField;

        private SegmentDescriptions[] segmentDescriptionsField;

        private TicketStatus[] ticketsStatusField;

        private TradeAccountSynopsis[] tradeAccountSynopsisField;

        private TradeAgeAnalysis[] tradeAgeAnalysisField;

        private TradeHistory[] tradeHistoryField;

        private TradePaymentHistory[] tradePaymentHistoryField;

        private TradeReferenceSummary tradeReferenceSummaryField;

        private TradeReference[] tradeReferencesField;

        private Tradex[] tradexField;

        private string uniqueRefGuidField;

        private UnmatchedCourtRecord[] unmatchedCourtRecordsField;

        private UnmatchedDefault[] unmatchedDefaultsField;

        private UnmatchedNotarialBond[] unmatchedNotarialBondsField;

        private Vehicles[] vehiclesField;

        private VeriCheque[] veriChequesField;

        private StandardBatchCharsSB01[] standardBatchCharsSB01Field;

        public StandardBatchCharsSB01[] StandardBatchCharsSB01
        {
            get
            {
                return this.standardBatchCharsSB01Field;
            }
            set
            {
                this.standardBatchCharsSB01Field = value;
            }
        }

        /// <remarks/>

        //public AccountVerificationVA[] accountVerificationVA
        //{
        //    get
        //    {
        //        return this.accountVerificationVAField;
        //    }
        //    set
        //    {
        //        this.accountVerificationVAField = value;
        //    }
        //}

        ///// <remarks/>

        //public AccountVerificationVH[] accountVerificationVH
        //{
        //    get
        //    {
        //        return this.accountVerificationVHField;
        //    }
        //    set
        //    {
        //        this.accountVerificationVHField = value;
        //    }
        //}

        ///// <remarks/>

        //public Affiliations[] affiliations
        //{
        //    get
        //    {
        //        return this.affiliationsField;
        //    }
        //    set
        //    {
        //        this.affiliationsField = value;
        //    }
        //}

        ///// <remarks/>

        //public AggregateCW aggregateCW
        //{
        //    get
        //    {
        //        return this.aggregateCWField;
        //    }
        //    set
        //    {
        //        this.aggregateCWField = value;
        //    }
        //}

        /// <remarks/>

        public BankCodes[] bankCodes
        {
            get
            {
                return this.bankCodesField;
            }
            set
            {
                this.bankCodesField = value;
            }
        }

        /// <remarks/>

        public BankHistory[] bankHistory
        {
            get
            {
                return this.bankHistoryField;
            }
            set
            {
                this.bankHistoryField = value;
            }
        }

        /// <remarks/>

        //public BankReport[] bankReport
        //{
        //    get
        //    {
        //        return this.bankReportField;
        //    }
        //    set
        //    {
        //        this.bankReportField = value;
        //    }
        //}

        ///// <remarks/>

        //public BankingDetailSummary[] bankingDetailsSummary
        //{
        //    get
        //    {
        //        return this.bankingDetailsSummaryField;
        //    }
        //    set
        //    {
        //        this.bankingDetailsSummaryField = value;
        //    }
        //}

        ///// <remarks/>

        //public Branch[] branch
        //{
        //    get
        //    {
        //        return this.branchField;
        //    }
        //    set
        //    {
        //        this.branchField = value;
        //    }
        //}

        ///// <remarks/>

        //public BusinessAdverseSummary businessAdverseSummary
        //{
        //    get
        //    {
        //        return this.businessAdverseSummaryField;
        //    }
        //    set
        //    {
        //        this.businessAdverseSummaryField = value;
        //    }
        //}

        ///// <remarks/>

        //public BusinessDeedsComprehensivePB[] businessDeedsComprehensivePB
        //{
        //    get
        //    {
        //        return this.businessDeedsComprehensivePBField;
        //    }
        //    set
        //    {
        //        this.businessDeedsComprehensivePBField = value;
        //    }
        //}

        ///// <remarks/>

        //public BusinessDeedsComprehensivePW[] businessDeedsComprehensivePW
        //{
        //    get
        //    {
        //        return this.businessDeedsComprehensivePWField;
        //    }
        //    set
        //    {
        //        this.businessDeedsComprehensivePWField = value;
        //    }
        //}

        ///// <remarks/>

        //public BusinessDeedsDI[] businessDeedsDI
        //{
        //    get
        //    {
        //        return this.businessDeedsDIField;
        //    }
        //    set
        //    {
        //        this.businessDeedsDIField = value;
        //    }
        //}

        /// <remarks/>

        public BusinessDeedsSummaryDA[] businessDeedsSummaryDA
        {
            get
            {
                return this.businessDeedsSummaryDAField;
            }
            set
            {
                this.businessDeedsSummaryDAField = value;
            }
        }

        /// <remarks/>

        //public BusinessNotarialBonds[] businessNotarialBonds
        //{
        //    get
        //    {
        //        return this.businessNotarialBondsField;
        //    }
        //    set
        //    {
        //        this.businessNotarialBondsField = value;
        //    }
        //}

        ///// <remarks/>

        //public BusinessPlusPrincipalSummary[] businessPlusPrincipalsSummary
        //{
        //    get
        //    {
        //        return this.businessPlusPrincipalsSummaryField;
        //    }
        //    set
        //    {
        //        this.businessPlusPrincipalsSummaryField = value;
        //    }
        //}

        ///// <remarks/>

        //public BusinessRescueDetail businessRescueDetail
        //{
        //    get
        //    {
        //        return this.businessRescueDetailField;
        //    }
        //    set
        //    {
        //        this.businessRescueDetailField = value;
        //    }
        //}

        ///// <remarks/>

        //public BusinessBenchmarkBB[] businessbenchmark
        //{
        //    get
        //    {
        //        return this.businessbenchmarkField;
        //    }
        //    set
        //    {
        //        this.businessbenchmarkField = value;
        //    }
        //}

        ///// <remarks/>

        //public CancelledTicket[] cancelledTickets
        //{
        //    get
        //    {
        //        return this.cancelledTicketsField;
        //    }
        //    set
        //    {
        //        this.cancelledTicketsField = value;
        //    }
        //}

        ///// <remarks/>

        //public CapitalEmployed capitalEmployed
        //{
        //    get
        //    {
        //        return this.capitalEmployedField;
        //    }
        //    set
        //    {
        //        this.capitalEmployedField = value;
        //    }
        //}

        ///// <remarks/>

        //public CommercialDisputeResponse commercialDisputeResponse
        //{
        //    get
        //    {
        //        return this.commercialDisputeResponseField;
        //    }
        //    set
        //    {
        //        this.commercialDisputeResponseField = value;
        //    }
        //}

        ///// <remarks/>

        //public ComplianceIndicatorLI[] complianceIndicatorLI
        //{
        //    get
        //    {
        //        return this.complianceIndicatorLIField;
        //    }
        //    set
        //    {
        //        this.complianceIndicatorLIField = value;
        //    }
        //}

        ///// <remarks/>

        //public ConsumerBusinessEnquiry[] consumerBusinessEnquiries
        //{
        //    get
        //    {
        //        return this.consumerBusinessEnquiriesField;
        //    }
        //    set
        //    {
        //        this.consumerBusinessEnquiriesField = value;
        //    }
        //}

        /// <remarks/>

        public ConsumerDefault[] consumerDefaults
        {
            get
            {
                return this.consumerDefaultsField;
            }
            set
            {
                this.consumerDefaultsField = value;
            }
        }

        /// <remarks/>

        //public ConsumerEnquiry[] consumerEnquiries
        //{
        //    get
        //    {
        //        return this.consumerEnquiriesField;
        //    }
        //    set
        //    {
        //        this.consumerEnquiriesField = value;
        //    }
        //}

        ///// <remarks/>

        //public ConsumerHeaderC1[] consumerHeaderC1
        //{
        //    get
        //    {
        //        return this.consumerHeaderC1Field;
        //    }
        //    set
        //    {
        //        this.consumerHeaderC1Field = value;
        //    }
        //}

        ///// <remarks/>

        //public ConsumerHeader[] consumerHeader
        //{
        //    get
        //    {
        //        return this.consumerHeaderField;
        //    }
        //    set
        //    {
        //        this.consumerHeaderField = value;
        //    }
        //}

        /// <remarks/>

        public ConsumerInfoNO04[] consumerInfoNO04
        {
            get
            {
                return this.consumerInfoNO04Field;
            }
            set
            {
                this.consumerInfoNO04Field = value;
            }
        }

        /// <remarks/>

        public ConsumerJudgement[] consumerJudgements
        {
            get
            {
                return this.consumerJudgementsField;
            }
            set
            {
                this.consumerJudgementsField = value;
            }
        }

        /// <remarks/>

        //public ConsumerNotarialBonds[] consumerNotarialBonds
        //{
        //    get
        //    {
        //        return this.consumerNotarialBondsField;
        //    }
        //    set
        //    {
        //        this.consumerNotarialBondsField = value;
        //    }
        //}

        /// <remarks/>

        public ConsumerNotice[] consumerNotices
        {
            get
            {
                return this.consumerNoticesField;
            }
            set
            {
                this.consumerNoticesField = value;
            }
        }

        /// <remarks/>

        //public ConsumerTraceAlert[] consumerTraceAlerts
        //{
        //    get
        //    {
        //        return this.consumerTraceAlertsField;
        //    }
        //    set
        //    {
        //        this.consumerTraceAlertsField = value;
        //    }
        //}

        /// <remarks/>

        public CourtRecord[] courtRecords
        {
            get
            {
                return this.courtRecordsField;
            }
            set
            {
                this.courtRecordsField = value;
            }
        }

        /// <remarks/>

        public CurrentAsset currentAsset
        {
            get
            {
                return this.currentAssetField;
            }
            set
            {
                this.currentAssetField = value;
            }
        }

        /// <remarks/>

        public CurrentLiabilities currentLiabilities
        {
            get
            {
                return this.currentLiabilitiesField;
            }
            set
            {
                this.currentLiabilitiesField = value;
            }
        }

        /// <remarks/>

        //public DeedHistory[] deedHistory
        //{
        //    get
        //    {
        //        return this.deedHistoryField;
        //    }
        //    set
        //    {
        //        this.deedHistoryField = value;
        //    }
        //}

        ///// <remarks/>

        //public DeedsMultipleBond[] deedsMultipleBond
        //{
        //    get
        //    {
        //        return this.deedsMultipleBondField;
        //    }
        //    set
        //    {
        //        this.deedsMultipleBondField = value;
        //    }
        //}

        /// <remarks/>

        public Default[] defaults
        {
            get
            {
                return this.defaultsField;
            }
            set
            {
                this.defaultsField = value;
            }
        }

        /// <remarks/>

        //public DynamicRating dynamicRating
        //{
        //    get
        //    {
        //        return this.dynamicRatingField;
        //    }
        //    set
        //    {
        //        this.dynamicRatingField = value;
        //    }
        //}

        ///// <remarks/>

        //public EmpOfCapital empOfCapital
        //{
        //    get
        //    {
        //        return this.empOfCapitalField;
        //    }
        //    set
        //    {
        //        this.empOfCapitalField = value;
        //    }
        //}

        ///// <remarks/>

        //public EmpiricaE1[] empiricaE1
        //{
        //    get
        //    {
        //        return this.empiricaE1Field;
        //    }
        //    set
        //    {
        //        this.empiricaE1Field = value;
        //    }
        //}

        ///// <remarks/>

        //public EmpiricaEM04 empiricaEM04
        //{
        //    get
        //    {
        //        return this.empiricaEM04Field;
        //    }
        //    set
        //    {
        //        this.empiricaEM04Field = value;
        //    }
        //}

        /// <remarks/>

        public EnquiryHistory[] enquiryHistory
        {
            get
            {
                return this.enquiryHistoryField;
            }
            set
            {
                this.enquiryHistoryField = value;
            }
        }

        /// <remarks/>

        //public EnquirySummary enquirySummary
        //{
        //    get
        //    {
        //        return this.enquirySummaryField;
        //    }
        //    set
        //    {
        //        this.enquirySummaryField = value;
        //    }
        //}

        /// <remarks/>

        public string errorCode
        {
            get
            {
                return this.errorCodeField;
            }
            set
            {
                this.errorCodeField = value;
            }
        }

        /// <remarks/>

        public string errorMessage
        {
            get
            {
                return this.errorMessageField;
            }
            set
            {
                this.errorMessageField = value;
            }
        }

        /// <remarks/>

        //public FinanceDataFE[] financeDataFE
        //{
        //    get
        //    {
        //        return this.financeDataFEField;
        //    }
        //    set
        //    {
        //        this.financeDataFEField = value;
        //    }
        //}

        ///// <remarks/>

        //public FinanceDataFF[] financeDataFF
        //{
        //    get
        //    {
        //        return this.financeDataFFField;
        //    }
        //    set
        //    {
        //        this.financeDataFFField = value;
        //    }
        //}

        ///// <remarks/>

        //public FinanceData[] financeData
        //{
        //    get
        //    {
        //        return this.financeDataField;
        //    }
        //    set
        //    {
        //        this.financeDataField = value;
        //    }
        //}

        /// <remarks/>

        public FinanceHeader financeHeader
        {
            get
            {
                return this.financeHeaderField;
            }
            set
            {
                this.financeHeaderField = value;
            }
        }

        /// <remarks/>

        //public FinancialRatios[] financialRatios
        //{
        //    get
        //    {
        //        return this.financialRatiosField;
        //    }
        //    set
        //    {
        //        this.financialRatiosField = value;
        //    }
        //}

        ///// <remarks/>

        //public FirstResponse firstResponse
        //{
        //    get
        //    {
        //        return this.firstResponseField;
        //    }
        //    set
        //    {
        //        this.firstResponseField = value;
        //    }
        //}

        /// <remarks/>

        public GeneralBankingInfo[] generalBankingInfo
        {
            get
            {
                return this.generalBankingInfoField;
            }
            set
            {
                this.generalBankingInfoField = value;
            }
        }

        /// <remarks/>

        public Header header
        {
            get
            {
                return this.headerField;
            }
            set
            {
                this.headerField = value;
            }
        }

        /// <remarks/>

        //public LinkedBusinessHeader[] linkedBusinessHeaders
        //{
        //    get
        //    {
        //        return this.linkedBusinessHeadersField;
        //    }
        //    set
        //    {
        //        this.linkedBusinessHeadersField = value;
        //    }
        //}

        ///// <remarks/>

        //public MailboxRetrieveList mailboxRetrieveList
        //{
        //    get
        //    {
        //        return this.mailboxRetrieveListField;
        //    }
        //    set
        //    {
        //        this.mailboxRetrieveListField = value;
        //    }
        //}

        ///// <remarks/>

        //public ModuleAvailabilityResponse moduleAvailabilityResponse
        //{
        //    get
        //    {
        //        return this.moduleAvailabilityResponseField;
        //    }
        //    set
        //    {
        //        this.moduleAvailabilityResponseField = value;
        //    }
        //}

        /// <remarks/>

        public Names names
        {
            get
            {
                return this.namesField;
            }
            set
            {
                this.namesField = value;
            }
        }

        /// <remarks/>

        //public NotarialBond[] notarialBonds
        //{
        //    get
        //    {
        //        return this.notarialBondsField;
        //    }
        //    set
        //    {
        //        this.notarialBondsField = value;
        //    }
        //}

        ///// <remarks/>

        //public ObservationCont[] observationCont
        //{
        //    get
        //    {
        //        return this.observationContField;
        //    }
        //    set
        //    {
        //        this.observationContField = value;
        //    }
        //}

        ///// <remarks/>

        //public Observation[] observations
        //{
        //    get
        //    {
        //        return this.observationsField;
        //    }
        //    set
        //    {
        //        this.observationsField = value;
        //    }
        //}

        /// <remarks/>

        public Operation[] operations
        {
            get
            {
                return this.operationsField;
            }
            set
            {
                this.operationsField = value;
            }
        }

        /// <remarks/>

        //public AdditionalOperations[] operationsHeader
        //{
        //    get
        //    {
        //        return this.operationsHeaderField;
        //    }
        //    set
        //    {
        //        this.operationsHeaderField = value;
        //    }
        //}

        ///// <remarks/>

        //public OtherOperation[] otherOperations
        //{
        //    get
        //    {
        //        return this.otherOperationsField;
        //    }
        //    set
        //    {
        //        this.otherOperationsField = value;
        //    }
        //}

        ///// <remarks/>

        //public PrincipalArchive[] principalArchives
        //{
        //    get
        //    {
        //        return this.principalArchivesField;
        //    }
        //    set
        //    {
        //        this.principalArchivesField = value;
        //    }
        //}

        ///// <remarks/>

        //public PrincipalArchiveP5[] principalArchivesP5
        //{
        //    get
        //    {
        //        return this.principalArchivesP5Field;
        //    }
        //    set
        //    {
        //        this.principalArchivesP5Field = value;
        //    }
        //}

        ///// <remarks/>

        //public PrincipalClearance[] principalClearances
        //{
        //    get
        //    {
        //        return this.principalClearancesField;
        //    }
        //    set
        //    {
        //        this.principalClearancesField = value;
        //    }
        //}

        ///// <remarks/>

        //public PrincipalDeedsComprehensiveCA[] principalDeedsCA
        //{
        //    get
        //    {
        //        return this.principalDeedsCAField;
        //    }
        //    set
        //    {
        //        this.principalDeedsCAField = value;
        //    }
        //}

        ///// <remarks/>

        //public PrincipalDeedsComprehensiveCV[] principalDeedsComprehensiveCV
        //{
        //    get
        //    {
        //        return this.principalDeedsComprehensiveCVField;
        //    }
        //    set
        //    {
        //        this.principalDeedsComprehensiveCVField = value;
        //    }
        //}

        ///// <remarks/>

        //public PrincipalDeedsSummaryCO[] principalDeedsSummaryCO
        //{
        //    get
        //    {
        //        return this.principalDeedsSummaryCOField;
        //    }
        //    set
        //    {
        //        this.principalDeedsSummaryCOField = value;
        //    }
        //}

        ///// <remarks/>

        //public PrincipalDeedsSummaryP8[] principalDeedsSummaryP8
        //{
        //    get
        //    {
        //        return this.principalDeedsSummaryP8Field;
        //    }
        //    set
        //    {
        //        this.principalDeedsSummaryP8Field = value;
        //    }
        //}

        ///// <remarks/>

        //public PrincipalFirstResponse principalFirstResponse
        //{
        //    get
        //    {
        //        return this.principalFirstResponseField;
        //    }
        //    set
        //    {
        //        this.principalFirstResponseField = value;
        //    }
        //}

        ///// <remarks/>

        //public PrincipalIDV[] principalIDVs
        //{
        //    get
        //    {
        //        return this.principalIDVsField;
        //    }
        //    set
        //    {
        //        this.principalIDVsField = value;
        //    }
        //}

        /// <remarks/>
        [XmlArray("PrincipalLinks")]
        [XmlArrayItem("PrincipalLink")]
        public PrincipalLinks[] principalLinks
        {
            get
            {
                return this.principalLinksField;
            }
            set
            {
                this.principalLinksField = value;
            }
        }

        /// <remarks/>

       // public PrincipalNotarialBonds[] principalNotarialBonds
        //{
        //    get
        //    {
        //        return this.principalNotarialBondsField;
        //    }
        //    set
        //    {
        //        this.principalNotarialBondsField = value;
        //    }
        //}

        ///// <remarks/>

        //public PrincipalDetailEmpirica[] principalsDetailEmpirica
        //{
        //    get
        //    {
        //        return this.principalsDetailEmpiricaField;
        //    }
        //    set
        //    {
        //        this.principalsDetailEmpiricaField = value;
        //    }
        //}

        /// <remarks/>

        public Principal1[] principals
        {
            get
            {
                return this.principalsField;
            }
            set
            {
                this.principalsField = value;
            }
        }

        /// <remarks/>
        public System.DateTime processingStartDate
        {
            get
            {
                return this.processingStartDateField;
            }
            set
            {
                this.processingStartDateField = value;
            }
        }

        /// <remarks/>
        public double processingTimeSecs
        {
            get
            {
                return this.processingTimeSecsField;
            }
            set
            {
                this.processingTimeSecsField = value;
            }
        }

        /// <remarks/>

        //public string rawData
        //{
        //    get
        //    {
        //        return this.rawDataField;
        //    }
        //    set
        //    {
        //        this.rawDataField = value;
        //    }
        //}

        /// <remarks/>

        //public RegisteredPrincipal[] registeredPrincipals
        //{
        //    get
        //    {
        //        return this.registeredPrincipalsField;
        //    }
        //    set
        //    {
        //        this.registeredPrincipalsField = value;
        //    }
        //}

        ///// <remarks/>

        //public RegistrationDetailsExtended registrationDetailsExtended
        //{
        //    get
        //    {
        //        return this.registrationDetailsExtendedField;
        //    }
        //    set
        //    {
        //        this.registrationDetailsExtendedField = value;
        //    }
        //}

        /// <remarks/>

        public RegistrationDetails registrationDetails
        {
            get
            {
                return this.registrationDetailsField;
            }
            set
            {
                this.registrationDetailsField = value;
            }
        }

        /// <remarks/>
        public ResponseStatus responseStatus
        {
            get
            {
                return this.responseStatusField;
            }
            set
            {
                this.responseStatusField = value;
            }
        }

        /// <remarks/>

        //public SMEAssessment sMEAssessment
        //{
        //    get
        //    {
        //        return this.sMEAssessmentField;
        //    }
        //    set
        //    {
        //        this.sMEAssessmentField = value;
        //    }
        //}

        /// <remarks/>

        //public SearchResponse[] searchResponse
        //{
        //    get
        //    {
        //        return this.searchResponseField;
        //    }
        //    set
        //    {
        //        this.searchResponseField = value;
        //    }
        //}

        /// <remarks/>

        //public SegmentDescriptions[] segmentDescriptions
        //{
        //    get
        //    {
        //        return this.segmentDescriptionsField;
        //    }
        //    set
        //    {
        //        this.segmentDescriptionsField = value;
        //    }
        //}

        ///// <remarks/>

        //public TicketStatus[] ticketsStatus
        //{
        //    get
        //    {
        //        return this.ticketsStatusField;
        //    }
        //    set
        //    {
        //        this.ticketsStatusField = value;
        //    }
        //}

        ///// <remarks/>

        //public TradeAccountSynopsis[] tradeAccountSynopsis
        //{
        //    get
        //    {
        //        return this.tradeAccountSynopsisField;
        //    }
        //    set
        //    {
        //        this.tradeAccountSynopsisField = value;
        //    }
        //}

        ///// <remarks/>

        //public TradeAgeAnalysis[] tradeAgeAnalysis
        //{
        //    get
        //    {
        //        return this.tradeAgeAnalysisField;
        //    }
        //    set
        //    {
        //        this.tradeAgeAnalysisField = value;
        //    }
        //}

        /// <remarks/>

        public TradeHistory[] tradeHistory
        {
            get
            {
                return this.tradeHistoryField;
            }
            set
            {
                this.tradeHistoryField = value;
            }
        }

        /// <remarks/>

        //public TradePaymentHistory[] tradePaymentHistory
        //{
        //    get
        //    {
        //        return this.tradePaymentHistoryField;
        //    }
        //    set
        //    {
        //        this.tradePaymentHistoryField = value;
        //    }
        //}

        ///// <remarks/>

        //public TradeReferenceSummary tradeReferenceSummary
        //{
        //    get
        //    {
        //        return this.tradeReferenceSummaryField;
        //    }
        //    set
        //    {
        //        this.tradeReferenceSummaryField = value;
        //    }
        //}

        ///// <remarks/>

        //public TradeReference[] tradeReferences
        //{
        //    get
        //    {
        //        return this.tradeReferencesField;
        //    }
        //    set
        //    {
        //        this.tradeReferencesField = value;
        //    }
        //}

        ///// <remarks/>

        //public Tradex[] tradex
        //{
        //    get
        //    {
        //        return this.tradexField;
        //    }
        //    set
        //    {
        //        this.tradexField = value;
        //    }
        //}

        /// <remarks/>
        public string uniqueRefGuid
        {
            get
            {
                return this.uniqueRefGuidField;
            }
            set
            {
                this.uniqueRefGuidField = value;
            }
        }

        /// <remarks/>

        //public UnmatchedCourtRecord[] unmatchedCourtRecords
        //{
        //    get
        //    {
        //        return this.unmatchedCourtRecordsField;
        //    }
        //    set
        //    {
        //        this.unmatchedCourtRecordsField = value;
        //    }
        //}

        ///// <remarks/>

        //public UnmatchedDefault[] unmatchedDefaults
        //{
        //    get
        //    {
        //        return this.unmatchedDefaultsField;
        //    }
        //    set
        //    {
        //        this.unmatchedDefaultsField = value;
        //    }
        //}

        ///// <remarks/>

        //public UnmatchedNotarialBond[] unmatchedNotarialBonds
        //{
        //    get
        //    {
        //        return this.unmatchedNotarialBondsField;
        //    }
        //    set
        //    {
        //        this.unmatchedNotarialBondsField = value;
        //    }
        //}

        ///// <remarks/>

        //public Vehicles[] vehicles
        //{
        //    get
        //    {
        //        return this.vehiclesField;
        //    }
        //    set
        //    {
        //        this.vehiclesField = value;
        //    }
        //}

        ///// <remarks/>

        //public VeriCheque[] veriCheques
        //{
        //    get
        //    {
        //        return this.veriChequesField;
        //    }
        //    set
        //    {
        //        this.veriChequesField = value;
        //    }
        //}


        //private BCCB701[] bccb701field;
        //private DeedsMultipleOwners[] deedsmultipleownersfield;
        //private DeedsMultipleOwnersDetail[] deedsmultipleownersdetailfield;
        //private ForensicLinkagesHeaderFL[] forensiclinkagesheaderflfield;
        //private ForensicLinkagesHomeTelephoneFH[] forensiclinkageshometelephonefhfield;
        //private ForensicLinkagesWorkTelephoneFW[] forensiclinkagesworktelephonefwfield;
        //private ForensicLinkagesCellphoneNumberFX[] forensiclinkagescellphonenumberfxfield;
        //private ForensicLinkagesDefaultsAssociates[] forensiclinkagesdefaultsassociatesfield;
        //private ForensicLinkagesDefaultsFF[] forensiclinkagesdefaultsfffield;
        //private ForensicLinkagesHomeAddressFZ[] forensiclinkageshomeaddressfzfield;
        //private ForensicLinkagesJudgementsAssociates[] forensiclinkagesjudgementsassociatesfield;
        //private ForensicLinkagesJudgementsFJ[] forensiclinkagesjudgementsfjfield;
        //private HistoricContactDetailsHD[] historiccontactdetailshdfield;
        //private IndividualTrace06[] individualtrace06field;
        //private LinkedBusinessHeaders[] linkedbusinessheadersfield;
        //private LinkagesLK[] linkageslkfield;
        //private NLRCounterSegmentMC01[] nlrcountersegmentmc01field;
        //private NLRCounterSegmentMC50[] nlrcountersegmentmc50field;
        //private NLRSummaryMY50[] nlrsummarymy50field;
        //private PrincipalDeedsCA[] principaldeedscafield;
        //private ScorecardS701[] scorecards701field;
        //private AddressAlertRA[] addressalertrafield;
        //private AllRiskTypesAR[] allrisktypesarfield;
        //private ClaimantHeaderCH[] claimantheaderchfield;
        //private ClaimantHeaderHC[] claimantheaderhcfield;
        //private ClaimantHeaderSH[] claimantheadershfield;
        //private ClaimDetailsCR[] claimdetailscrfield;
        //private ClaimsLinkedCL[] claimslinkedclfield;
        //private ClaimantSummaryCS[] claimantsummarycsfield;
        //private ClaimsCommentSegmentSX[] claimscommentsegmentsxfield;
        //private DriverDetailsDR[] driverdetailsdrfield;
        //private FraudAlertS6[] fraudalerts6field;
        //private HawkAlertHA[] hawkalerthafield;
        //private HomeOwnersAO[] homeownersaofield;
        //private IdentityVerficationAlertHI[] identityverficationalerthifield;
        //private InsuredDetailsIN[] insureddetailsinfield;
        //private PersonalLiabiltyPL[] personalliabiltyplfield;
        //private ServiceProviderSP[] serviceproviderspfield;
        //private ThirdPartyTP[] thirdpartytpfield;
        //private VehicleAlertVA[] vehiclealertvafield;
        //private VehicleDetailsVH[] vehicledetailsvhfield;
        //private NatisEnquiryHistoryUS[] natisenquiryhistoryusfield;
        //private NatisInputVerificationUT[] natisinputverificationutfield;
        //private NatisDetailedVehicleInformationUU[] natisdetailedvehicleinformationuufield;
        //private DriversLicenseInformationUX[] driverslicenseinformationuxfield;
        //private InternalMessageIM[] internalmessageimfield;
        //private IndivualOwnOtherActivePoliciesPI[] indivualownotheractivepoliciespifield;
        //private IndivualOwnOtherClaimDetails0to3yearsI1[] indivualownotherclaimdetails0to3yearsi1field;
        //private IndivualOwnOtherClaimDetails4to7yearsSI[] indivualownotherclaimdetails4to7yearssifield;
        //private IndivualOwnOtherInactivePoliciesP1[] indivualownotherinactivepoliciesp1field;
        //private InputIdentificationIndividualII[] inputidentificationindividualiifield;
        //private InputIdentificationRiskAddressIA[] inputidentificationriskaddressiafield;
        //private InputIdentificationVehicleIV[] inputidentificationvehicleivfield;
        //private InsuranceEnquirySE[] insuranceenquirysefield;
        //private PolicySummaryPS[] policysummarypsfield;
        //private PolicyCommentSegmentSZ[] policycommentsegmentszfield;
        //private RiskAddressOwnOtherActivePoliciesPA[] riskaddressownotheractivepoliciespafield;
        //private RiskAddressOwnOtherClaimDetails0to3yearsI3[] riskaddressownotherclaimdetails0to3yearsi3field;
        //private RiskAddressOwnOtherClaimDetails4to7yearsSR[] riskaddressownotherclaimdetails4to7yearssrfield;
        //private RiskAddressOwnOtherInactivePoliciesP3[] riskaddressownotherinactivepoliciesp3field;
        //private VehicleOwnOtherActivePoliciesPV[] vehicleownotheractivepoliciespvfield;
        //private VehicleOwnOtherClaimDetails0to3yearsI2[] vehicleownotherclaimdetails0to3yearsi2field;
        //private VehicleOwnOtherClaimDetails4to7yearsSV[] vehicleownotherclaimdetails4to7yearssvfield;
        //private VehicleOwnOtherInactivePoliciesP2[] vehicleownotherinactivepoliciesp2field;
        //private VehicleInformationUR[] vehicleinformationurfield;
        //private VehicleOwnership_TitleHolderInformationUW[] vehicleownership_titleholderinformationuwfield;
        //private TUVehicleDetailsVL[] tuvehicledetailsvlfield;
        //private TUVehicleFinanceDetailsVB[] tuvehiclefinancedetailsvbfield;
        //private TUVehiclePreviousRegistrationDetailsVC[] tuvehiclepreviousregistrationdetailsvcfield;
        //private TUVehicleMileageRecordedVD[] tuvehiclemileagerecordedvdfield;
        //private TUVehicleEnquiryHistoryVE[] tuvehicleenquiryhistoryvefield;
        //private TUVehicleEstimateRecordedVF[] tuvehicleestimaterecordedvffield;
        //private TUVehicleAlertRecordedVG[] tuvehiclealertrecordedvgfield;
        //private TUVehicleStolenRecordedVM[] tuvehiclestolenrecordedvmfield;
        //private TUVehicleRegistrationAuthorityHistoryRecordedVI[] tuvehicleregistrationauthorityhistoryrecordedvifield;
        //private TUVehicleInsuranceClaimsRecordedVJ[] tuvehicleinsuranceclaimsrecordedvjfield;
        //private TUVehicleInsurancePoliciesRecordedVK[] tuvehicleinsurancepoliciesrecordedvkfield;
        //private TUInputVerificationUV[] tuinputverificationuvfield;

        //public BCCB701[] BCCB701 { get { return this.bccb701field; } set { this.bccb701field = value; } }
        //public DeedsMultipleOwners[] DeedsMultipleOwners { get { return this.deedsmultipleownersfield; } set { this.deedsmultipleownersfield = value; } }
        //public DeedsMultipleOwnersDetail[] DeedsMultipleOwnersDetail { get { return this.deedsmultipleownersdetailfield; } set { this.deedsmultipleownersdetailfield = value; } }
        //public ForensicLinkagesHeaderFL[] ForensicLinkagesHeaderFL { get { return this.forensiclinkagesheaderflfield; } set { this.forensiclinkagesheaderflfield = value; } }
        //public ForensicLinkagesHomeTelephoneFH[] ForensicLinkagesHomeTelephoneFH { get { return this.forensiclinkageshometelephonefhfield; } set { this.forensiclinkageshometelephonefhfield = value; } }
        //public ForensicLinkagesWorkTelephoneFW[] ForensicLinkagesWorkTelephoneFW { get { return this.forensiclinkagesworktelephonefwfield; } set { this.forensiclinkagesworktelephonefwfield = value; } }
        //public ForensicLinkagesCellphoneNumberFX[] ForensicLinkagesCellphoneNumberFX { get { return this.forensiclinkagescellphonenumberfxfield; } set { this.forensiclinkagescellphonenumberfxfield = value; } }
        //public ForensicLinkagesDefaultsAssociates[] ForensicLinkagesDefaultsAssociates { get { return this.forensiclinkagesdefaultsassociatesfield; } set { this.forensiclinkagesdefaultsassociatesfield = value; } }
        //public ForensicLinkagesDefaultsFF[] ForensicLinkagesDefaultsFF { get { return this.forensiclinkagesdefaultsfffield; } set { this.forensiclinkagesdefaultsfffield = value; } }
        //public ForensicLinkagesHomeAddressFZ[] ForensicLinkagesHomeAddressFZ { get { return this.forensiclinkageshomeaddressfzfield; } set { this.forensiclinkageshomeaddressfzfield = value; } }
        //public ForensicLinkagesJudgementsAssociates[] ForensicLinkagesJudgementsAssociates { get { return this.forensiclinkagesjudgementsassociatesfield; } set { this.forensiclinkagesjudgementsassociatesfield = value; } }
        //public ForensicLinkagesJudgementsFJ[] ForensicLinkagesJudgementsFJ { get { return this.forensiclinkagesjudgementsfjfield; } set { this.forensiclinkagesjudgementsfjfield = value; } }
        //public HistoricContactDetailsHD[] HistoricContactDetailsHD { get { return this.historiccontactdetailshdfield; } set { this.historiccontactdetailshdfield = value; } }
        //public IndividualTrace06[] IndividualTrace06 { get { return this.individualtrace06field; } set { this.individualtrace06field = value; } }
        //public LinkedBusinessHeaders[] LinkedBusinessHeaders { get { return this.linkedbusinessheadersfield; } set { this.linkedbusinessheadersfield = value; } }
        //public LinkagesLK[] LinkagesLK { get { return this.linkageslkfield; } set { this.linkageslkfield = value; } }
        //public NLRCounterSegmentMC01[] NLRCounterSegmentMC01 { get { return this.nlrcountersegmentmc01field; } set { this.nlrcountersegmentmc01field = value; } }
        //public NLRCounterSegmentMC50[] NLRCounterSegmentMC50 { get { return this.nlrcountersegmentmc50field; } set { this.nlrcountersegmentmc50field = value; } }
        //public NLRSummaryMY50[] NLRSummaryMY50 { get { return this.nlrsummarymy50field; } set { this.nlrsummarymy50field = value; } }
        //public PrincipalDeedsCA[] PrincipalDeedsCA { get { return this.principaldeedscafield; } set { this.principaldeedscafield = value; } }
        //public ScorecardS701[] ScorecardS701 { get { return this.scorecards701field; } set { this.scorecards701field = value; } }
        //public AddressAlertRA[] AddressAlertRA { get { return this.addressalertrafield; } set { this.addressalertrafield = value; } }
        //public AllRiskTypesAR[] AllRiskTypesAR { get { return this.allrisktypesarfield; } set { this.allrisktypesarfield = value; } }
        //public ClaimantHeaderCH[] ClaimantHeaderCH { get { return this.claimantheaderchfield; } set { this.claimantheaderchfield = value; } }
        //public ClaimantHeaderHC[] ClaimantHeaderHC { get { return this.claimantheaderhcfield; } set { this.claimantheaderhcfield = value; } }
        //public ClaimantHeaderSH[] ClaimantHeaderSH { get { return this.claimantheadershfield; } set { this.claimantheadershfield = value; } }
        //public ClaimDetailsCR[] ClaimDetailsCR { get { return this.claimdetailscrfield; } set { this.claimdetailscrfield = value; } }
        //public ClaimsLinkedCL[] ClaimsLinkedCL { get { return this.claimslinkedclfield; } set { this.claimslinkedclfield = value; } }
        //public ClaimantSummaryCS[] ClaimantSummaryCS { get { return this.claimantsummarycsfield; } set { this.claimantsummarycsfield = value; } }
        //public ClaimsCommentSegmentSX[] ClaimsCommentSegmentSX { get { return this.claimscommentsegmentsxfield; } set { this.claimscommentsegmentsxfield = value; } }
        //public DriverDetailsDR[] DriverDetailsDR { get { return this.driverdetailsdrfield; } set { this.driverdetailsdrfield = value; } }
        //public FraudAlertS6[] FraudAlertS6 { get { return this.fraudalerts6field; } set { this.fraudalerts6field = value; } }
        //public HawkAlertHA[] HawkAlertHA { get { return this.hawkalerthafield; } set { this.hawkalerthafield = value; } }
        //public HomeOwnersAO[] HomeOwnersAO { get { return this.homeownersaofield; } set { this.homeownersaofield = value; } }
        //public IdentityVerficationAlertHI[] IdentityVerficationAlertHI { get { return this.identityverficationalerthifield; } set { this.identityverficationalerthifield = value; } }
        //public InsuredDetailsIN[] InsuredDetailsIN { get { return this.insureddetailsinfield; } set { this.insureddetailsinfield = value; } }
        //public PersonalLiabiltyPL[] PersonalLiabiltyPL { get { return this.personalliabiltyplfield; } set { this.personalliabiltyplfield = value; } }
        //public ServiceProviderSP[] ServiceProviderSP { get { return this.serviceproviderspfield; } set { this.serviceproviderspfield = value; } }
        //public ThirdPartyTP[] ThirdPartyTP { get { return this.thirdpartytpfield; } set { this.thirdpartytpfield = value; } }
        //public VehicleAlertVA[] VehicleAlertVA { get { return this.vehiclealertvafield; } set { this.vehiclealertvafield = value; } }
        //public VehicleDetailsVH[] VehicleDetailsVH { get { return this.vehicledetailsvhfield; } set { this.vehicledetailsvhfield = value; } }
        //public NatisEnquiryHistoryUS[] NatisEnquiryHistoryUS { get { return this.natisenquiryhistoryusfield; } set { this.natisenquiryhistoryusfield = value; } }
        //public NatisInputVerificationUT[] NatisInputVerificationUT { get { return this.natisinputverificationutfield; } set { this.natisinputverificationutfield = value; } }
        //public NatisDetailedVehicleInformationUU[] NatisDetailedVehicleInformationUU { get { return this.natisdetailedvehicleinformationuufield; } set { this.natisdetailedvehicleinformationuufield = value; } }
        //public DriversLicenseInformationUX[] DriversLicenseInformationUX { get { return this.driverslicenseinformationuxfield; } set { this.driverslicenseinformationuxfield = value; } }
        //public InternalMessageIM[] InternalMessageIM { get { return this.internalmessageimfield; } set { this.internalmessageimfield = value; } }
        //public IndivualOwnOtherActivePoliciesPI[] IndivualOwnOtherActivePoliciesPI { get { return this.indivualownotheractivepoliciespifield; } set { this.indivualownotheractivepoliciespifield = value; } }
        //public IndivualOwnOtherClaimDetails0to3yearsI1[] IndivualOwnOtherClaimDetails0to3yearsI1 { get { return this.indivualownotherclaimdetails0to3yearsi1field; } set { this.indivualownotherclaimdetails0to3yearsi1field = value; } }
        //public IndivualOwnOtherClaimDetails4to7yearsSI[] IndivualOwnOtherClaimDetails4to7yearsSI { get { return this.indivualownotherclaimdetails4to7yearssifield; } set { this.indivualownotherclaimdetails4to7yearssifield = value; } }
        //public IndivualOwnOtherInactivePoliciesP1[] IndivualOwnOtherInactivePoliciesP1 { get { return this.indivualownotherinactivepoliciesp1field; } set { this.indivualownotherinactivepoliciesp1field = value; } }
        //public InputIdentificationIndividualII[] InputIdentificationIndividualII { get { return this.inputidentificationindividualiifield; } set { this.inputidentificationindividualiifield = value; } }
        //public InputIdentificationRiskAddressIA[] InputIdentificationRiskAddressIA { get { return this.inputidentificationriskaddressiafield; } set { this.inputidentificationriskaddressiafield = value; } }
        //public InputIdentificationVehicleIV[] InputIdentificationVehicleIV { get { return this.inputidentificationvehicleivfield; } set { this.inputidentificationvehicleivfield = value; } }
        //public InsuranceEnquirySE[] InsuranceEnquirySE { get { return this.insuranceenquirysefield; } set { this.insuranceenquirysefield = value; } }
        //public PolicySummaryPS[] PolicySummaryPS { get { return this.policysummarypsfield; } set { this.policysummarypsfield = value; } }
        //public PolicyCommentSegmentSZ[] PolicyCommentSegmentSZ { get { return this.policycommentsegmentszfield; } set { this.policycommentsegmentszfield = value; } }
        //public RiskAddressOwnOtherActivePoliciesPA[] RiskAddressOwnOtherActivePoliciesPA { get { return this.riskaddressownotheractivepoliciespafield; } set { this.riskaddressownotheractivepoliciespafield = value; } }
        //public RiskAddressOwnOtherClaimDetails0to3yearsI3[] RiskAddressOwnOtherClaimDetails0to3yearsI3 { get { return this.riskaddressownotherclaimdetails0to3yearsi3field; } set { this.riskaddressownotherclaimdetails0to3yearsi3field = value; } }
        //public RiskAddressOwnOtherClaimDetails4to7yearsSR[] RiskAddressOwnOtherClaimDetails4to7yearsSR { get { return this.riskaddressownotherclaimdetails4to7yearssrfield; } set { this.riskaddressownotherclaimdetails4to7yearssrfield = value; } }
        //public RiskAddressOwnOtherInactivePoliciesP3[] RiskAddressOwnOtherInactivePoliciesP3 { get { return this.riskaddressownotherinactivepoliciesp3field; } set { this.riskaddressownotherinactivepoliciesp3field = value; } }
        //public VehicleOwnOtherActivePoliciesPV[] VehicleOwnOtherActivePoliciesPV { get { return this.vehicleownotheractivepoliciespvfield; } set { this.vehicleownotheractivepoliciespvfield = value; } }
        //public VehicleOwnOtherClaimDetails0to3yearsI2[] VehicleOwnOtherClaimDetails0to3yearsI2 { get { return this.vehicleownotherclaimdetails0to3yearsi2field; } set { this.vehicleownotherclaimdetails0to3yearsi2field = value; } }
        //public VehicleOwnOtherClaimDetails4to7yearsSV[] VehicleOwnOtherClaimDetails4to7yearsSV { get { return this.vehicleownotherclaimdetails4to7yearssvfield; } set { this.vehicleownotherclaimdetails4to7yearssvfield = value; } }
        //public VehicleOwnOtherInactivePoliciesP2[] VehicleOwnOtherInactivePoliciesP2 { get { return this.vehicleownotherinactivepoliciesp2field; } set { this.vehicleownotherinactivepoliciesp2field = value; } }
        //public VehicleInformationUR[] VehicleInformationUR { get { return this.vehicleinformationurfield; } set { this.vehicleinformationurfield = value; } }
        //public VehicleOwnership_TitleHolderInformationUW[] VehicleOwnership_TitleHolderInformationUW { get { return this.vehicleownership_titleholderinformationuwfield; } set { this.vehicleownership_titleholderinformationuwfield = value; } }
        //public TUVehicleDetailsVL[] TUVehicleDetailsVL { get { return this.tuvehicledetailsvlfield; } set { this.tuvehicledetailsvlfield = value; } }
        //public TUVehicleFinanceDetailsVB[] TUVehicleFinanceDetailsVB { get { return this.tuvehiclefinancedetailsvbfield; } set { this.tuvehiclefinancedetailsvbfield = value; } }
        //public TUVehiclePreviousRegistrationDetailsVC[] TUVehiclePreviousRegistrationDetailsVC { get { return this.tuvehiclepreviousregistrationdetailsvcfield; } set { this.tuvehiclepreviousregistrationdetailsvcfield = value; } }
        //public TUVehicleMileageRecordedVD[] TUVehicleMileageRecordedVD { get { return this.tuvehiclemileagerecordedvdfield; } set { this.tuvehiclemileagerecordedvdfield = value; } }
        //public TUVehicleEnquiryHistoryVE[] TUVehicleEnquiryHistoryVE { get { return this.tuvehicleenquiryhistoryvefield; } set { this.tuvehicleenquiryhistoryvefield = value; } }
        //public TUVehicleEstimateRecordedVF[] TUVehicleEstimateRecordedVF { get { return this.tuvehicleestimaterecordedvffield; } set { this.tuvehicleestimaterecordedvffield = value; } }
        //public TUVehicleAlertRecordedVG[] TUVehicleAlertRecordedVG { get { return this.tuvehiclealertrecordedvgfield; } set { this.tuvehiclealertrecordedvgfield = value; } }
        //public TUVehicleStolenRecordedVM[] TUVehicleStolenRecordedVM { get { return this.tuvehiclestolenrecordedvmfield; } set { this.tuvehiclestolenrecordedvmfield = value; } }
        //public TUVehicleRegistrationAuthorityHistoryRecordedVI[] TUVehicleRegistrationAuthorityHistoryRecordedVI { get { return this.tuvehicleregistrationauthorityhistoryrecordedvifield; } set { this.tuvehicleregistrationauthorityhistoryrecordedvifield = value; } }
        //public TUVehicleInsuranceClaimsRecordedVJ[] TUVehicleInsuranceClaimsRecordedVJ { get { return this.tuvehicleinsuranceclaimsrecordedvjfield; } set { this.tuvehicleinsuranceclaimsrecordedvjfield = value; } }
        //public TUVehicleInsurancePoliciesRecordedVK[] TUVehicleInsurancePoliciesRecordedVK { get { return this.tuvehicleinsurancepoliciesrecordedvkfield; } set { this.tuvehicleinsurancepoliciesrecordedvkfield = value; } }
        //public TUInputVerificationUV[] TUInputVerificationUV { get { return this.tuinputverificationuvfield; } set { this.tuinputverificationuvfield = value; } }

    }

    public class StandardBatchCharsSB01
    {
        private string consumerNoField;
        private string nameField;
        private string valueField;
        private string weightingField;

        public string ConsumerNo
        {
            get { return this.consumerNoField; }
            set { this.consumerNoField = value; }
        }

        public string Name
        {
            get { return this.nameField; }
            set { this.nameField = value; }
        }

        public string Value
        {
            get { return this.valueField; }
            set { this.valueField = value; }
        }

        public string Weighting
        {
            get { return this.weightingField; }
            set { this.weightingField = value; }
        }
    }

    public class EnterpriseExclusionRule
    {
       // private string consumerNoField;
        private string codeField;
        private string valueField;
        private string descriptionField;

        //public string ConsumerNo
        //{
        //    get { return this.consumerNoField; }
        //    set { this.consumerNoField = value; }
        //}

        public string Code
        {
            get { return this.codeField; }
            set { this.codeField = value; }
        }

        public string Value
        {
            get { return this.valueField; }
            set { this.valueField = value; }
        }

        public string Description
        {
            get { return this.descriptionField; }
            set { this.descriptionField = value; }
        }
    }

    public class PrincipalExclusionRule
    {
        // private string consumerNoField;
        private string codeField;
        private string valueField;
        private string descriptionField;

        //public string ConsumerNo
        //{
        //    get { return this.consumerNoField; }
        //    set { this.consumerNoField = value; }
        //}

        public string Code
        {
            get { return this.codeField; }
            set { this.codeField = value; }
        }

        public string Value
        {
            get { return this.valueField; }
            set { this.valueField = value; }
        }

        public string Description
        {
            get { return this.descriptionField; }
            set { this.descriptionField = value; }
        }
    }

    public class ExclusionRule
    {
        private string codeField;
        private string valueField;
        private string descriptionField;

        //public string ConsumerNo
        //{
        //    get { return this.consumerNoField; }
        //    set { this.consumerNoField = value; }
        //}

        public string Code
        {
            get { return this.codeField; }
            set { this.codeField = value; }
        }

        public string Value
        {
            get { return this.valueField; }
            set { this.valueField = value; }
        }

        public string Description
        {
            get { return this.descriptionField; }
            set { this.descriptionField = value; }
        }
    }

    public class PrincipleCharacterstics
    {
        private string consumerNoField;
        private string nameField;
        private string valueField;
        private string weightingField;

        public string ConsumerNo
        {
            get { return this.consumerNoField; }
            set { this.consumerNoField = value; }
        }

        public string Name
        {
            get { return this.nameField; }
            set { this.nameField = value; }
        }

        public string Value
        {
            get { return this.valueField; }
            set { this.valueField = value; }
        }

        public string Weighting
        {
            get { return this.weightingField; }
            set { this.weightingField = value; }
        }
    }

    /// <remarks/>



    public enum ResponseStatus
    {

        /// <remarks/>
        Success,

        /// <remarks/>
        Failure,
    }



    /// <remarks/>





    public partial class Vehicle
    {

        private string descriptionField;

        private string infoDateField;

        private string noHPField;

        private string noLeasedField;

        private string noOwnedField;

        private string quantityField;

        /// <remarks/>

        public string description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        /// <remarks/>

        public string infoDate
        {
            get
            {
                return this.infoDateField;
            }
            set
            {
                this.infoDateField = value;
            }
        }

        /// <remarks/>

        public string noHP
        {
            get
            {
                return this.noHPField;
            }
            set
            {
                this.noHPField = value;
            }
        }

        /// <remarks/>

        public string noLeased
        {
            get
            {
                return this.noLeasedField;
            }
            set
            {
                this.noLeasedField = value;
            }
        }

        /// <remarks/>

        public string noOwned
        {
            get
            {
                return this.noOwnedField;
            }
            set
            {
                this.noOwnedField = value;
            }
        }

        /// <remarks/>

        public string quantity
        {
            get
            {
                return this.quantityField;
            }
            set
            {
                this.quantityField = value;
            }
        }
    }

    /// <remarks/>





    public partial class CreditBand
    {

        private string days11to30Field;

        private string days120toPlusField;

        private string days1to10Field;

        private string days31to60Field;

        private string days61to90Field;

        private string days91to120Field;

        private string invoiceAmountField;

        private string noOfInvoicesField;

        private string withinTermsField;

        /// <remarks/>

        public string days11to30
        {
            get
            {
                return this.days11to30Field;
            }
            set
            {
                this.days11to30Field = value;
            }
        }

        /// <remarks/>

        public string days120toPlus
        {
            get
            {
                return this.days120toPlusField;
            }
            set
            {
                this.days120toPlusField = value;
            }
        }

        /// <remarks/>

        public string days1to10
        {
            get
            {
                return this.days1to10Field;
            }
            set
            {
                this.days1to10Field = value;
            }
        }

        /// <remarks/>

        public string days31to60
        {
            get
            {
                return this.days31to60Field;
            }
            set
            {
                this.days31to60Field = value;
            }
        }

        /// <remarks/>

        public string days61to90
        {
            get
            {
                return this.days61to90Field;
            }
            set
            {
                this.days61to90Field = value;
            }
        }

        /// <remarks/>

        public string days91to120
        {
            get
            {
                return this.days91to120Field;
            }
            set
            {
                this.days91to120Field = value;
            }
        }

        /// <remarks/>

        public string invoiceAmount
        {
            get
            {
                return this.invoiceAmountField;
            }
            set
            {
                this.invoiceAmountField = value;
            }
        }

        /// <remarks/>

        public string noOfInvoices
        {
            get
            {
                return this.noOfInvoicesField;
            }
            set
            {
                this.noOfInvoicesField = value;
            }
        }

        /// <remarks/>

        public string withinTerms
        {
            get
            {
                return this.withinTermsField;
            }
            set
            {
                this.withinTermsField = value;
            }
        }
    }

    /// <remarks/>





    public partial class PaymentHistoryDetail
    {

        private string creditLimitField;

        private string days120PercentageField;

        private string days30PercentageField;

        private string days60PercentageField;

        private string days90PercentageField;

        private string numberSuppliersField;

        private string periodField;

        private string totalCreditLimitField;

        private string totalCurrentPercentageField;

        private string totalOverduePercentageField;

        /// <remarks/>

        public string creditLimit
        {
            get
            {
                return this.creditLimitField;
            }
            set
            {
                this.creditLimitField = value;
            }
        }

        /// <remarks/>

        public string days120Percentage
        {
            get
            {
                return this.days120PercentageField;
            }
            set
            {
                this.days120PercentageField = value;
            }
        }

        /// <remarks/>

        public string days30Percentage
        {
            get
            {
                return this.days30PercentageField;
            }
            set
            {
                this.days30PercentageField = value;
            }
        }

        /// <remarks/>

        public string days60Percentage
        {
            get
            {
                return this.days60PercentageField;
            }
            set
            {
                this.days60PercentageField = value;
            }
        }

        /// <remarks/>

        public string days90Percentage
        {
            get
            {
                return this.days90PercentageField;
            }
            set
            {
                this.days90PercentageField = value;
            }
        }

        /// <remarks/>

        public string numberSuppliers
        {
            get
            {
                return this.numberSuppliersField;
            }
            set
            {
                this.numberSuppliersField = value;
            }
        }

        /// <remarks/>

        public string period
        {
            get
            {
                return this.periodField;
            }
            set
            {
                this.periodField = value;
            }
        }

        /// <remarks/>

        public string totalCreditLimit
        {
            get
            {
                return this.totalCreditLimitField;
            }
            set
            {
                this.totalCreditLimitField = value;
            }
        }

        /// <remarks/>

        public string totalCurrentPercentage
        {
            get
            {
                return this.totalCurrentPercentageField;
            }
            set
            {
                this.totalCurrentPercentageField = value;
            }
        }

        /// <remarks/>

        public string totalOverduePercentage
        {
            get
            {
                return this.totalOverduePercentageField;
            }
            set
            {
                this.totalOverduePercentageField = value;
            }
        }
    }

    /// <remarks/>





    public partial class AgeAnalysisDetail
    {

        private string creditLimitField;

        private string days120PercentageField;

        private string days30PercentageField;

        private string days60PercentageField;

        private string days90PercentageField;

        private string supplierField;

        private string termField;

        private string totalCreditLimitField;

        private string totalCurrentPercentageField;

        private string totalOverduePercentageField;

        /// <remarks/>

        public string creditLimit
        {
            get
            {
                return this.creditLimitField;
            }
            set
            {
                this.creditLimitField = value;
            }
        }

        /// <remarks/>

        public string days120Percentage
        {
            get
            {
                return this.days120PercentageField;
            }
            set
            {
                this.days120PercentageField = value;
            }
        }

        /// <remarks/>

        public string days30Percentage
        {
            get
            {
                return this.days30PercentageField;
            }
            set
            {
                this.days30PercentageField = value;
            }
        }

        /// <remarks/>

        public string days60Percentage
        {
            get
            {
                return this.days60PercentageField;
            }
            set
            {
                this.days60PercentageField = value;
            }
        }

        /// <remarks/>

        public string days90Percentage
        {
            get
            {
                return this.days90PercentageField;
            }
            set
            {
                this.days90PercentageField = value;
            }
        }

        /// <remarks/>

        public string supplier
        {
            get
            {
                return this.supplierField;
            }
            set
            {
                this.supplierField = value;
            }
        }

        /// <remarks/>

        public string term
        {
            get
            {
                return this.termField;
            }
            set
            {
                this.termField = value;
            }
        }

        /// <remarks/>

        public string totalCreditLimit
        {
            get
            {
                return this.totalCreditLimitField;
            }
            set
            {
                this.totalCreditLimitField = value;
            }
        }

        /// <remarks/>

        public string totalCurrentPercentage
        {
            get
            {
                return this.totalCurrentPercentageField;
            }
            set
            {
                this.totalCurrentPercentageField = value;
            }
        }

        /// <remarks/>

        public string totalOverduePercentage
        {
            get
            {
                return this.totalOverduePercentageField;
            }
            set
            {
                this.totalOverduePercentageField = value;
            }
        }
    }

    /// <remarks/>





    public partial class AccountSynopsisDetail
    {

        private string accountNumberField;

        private string creditLimitField;

        private string statusField;

        private string supplierField;

        private string termExtField;

        private string termField;

        /// <remarks/>

        public string accountNumber
        {
            get
            {
                return this.accountNumberField;
            }
            set
            {
                this.accountNumberField = value;
            }
        }

        /// <remarks/>

        public string creditLimit
        {
            get
            {
                return this.creditLimitField;
            }
            set
            {
                this.creditLimitField = value;
            }
        }

        /// <remarks/>

        public string status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>

        public string supplier
        {
            get
            {
                return this.supplierField;
            }
            set
            {
                this.supplierField = value;
            }
        }

        /// <remarks/>

        public string termExt
        {
            get
            {
                return this.termExtField;
            }
            set
            {
                this.termExtField = value;
            }
        }

        /// <remarks/>

        public string term
        {
            get
            {
                return this.termField;
            }
            set
            {
                this.termField = value;
            }
        }
    }

    /// <remarks/>





    public partial class SegmentDescription
    {

        private string dateField;

        private string infoDescriptionField;

        private string infoTypeField;

        /// <remarks/>

        public string date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>

        public string infoDescription
        {
            get
            {
                return this.infoDescriptionField;
            }
            set
            {
                this.infoDescriptionField = value;
            }
        }

        /// <remarks/>

        public string infoType
        {
            get
            {
                return this.infoTypeField;
            }
            set
            {
                this.infoTypeField = value;
            }
        }
    }

    /// <remarks/>





    public partial class ShareInfo
    {

        private string shareNumberField;

        private string shareTypeField;

        private string shareValueField;

        /// <remarks/>

        public string shareNumber
        {
            get
            {
                return this.shareNumberField;
            }
            set
            {
                this.shareNumberField = value;
            }
        }

        /// <remarks/>

        public string shareType
        {
            get
            {
                return this.shareTypeField;
            }
            set
            {
                this.shareTypeField = value;
            }
        }

        /// <remarks/>

        public string shareValue
        {
            get
            {
                return this.shareValueField;
            }
            set
            {
                this.shareValueField = value;
            }
        }
    }

    /// <remarks/>





    public partial class CapitalInfo
    {

        private string capitalChangedDateField;

        private string capitalChangedFromField;

        private string capitalChangedToField;

        /// <remarks/>

        public string capitalChangedDate
        {
            get
            {
                return this.capitalChangedDateField;
            }
            set
            {
                this.capitalChangedDateField = value;
            }
        }

        /// <remarks/>

        public string capitalChangedFrom
        {
            get
            {
                return this.capitalChangedFromField;
            }
            set
            {
                this.capitalChangedFromField = value;
            }
        }

        /// <remarks/>

        public string capitalChangedTo
        {
            get
            {
                return this.capitalChangedToField;
            }
            set
            {
                this.capitalChangedToField = value;
            }
        }
    }

    /// <remarks/>





    public partial class SicDetails
    {

        private string sASicCodeField;

        private string sicCodeField;

        private string sicDescriptionField;

        /// <remarks/>

        public string sASicCode
        {
            get
            {
                return this.sASicCodeField;
            }
            set
            {
                this.sASicCodeField = value;
            }
        }

        /// <remarks/>

        public string sicCode
        {
            get
            {
                return this.sicCodeField;
            }
            set
            {
                this.sicCodeField = value;
            }
        }

        /// <remarks/>

        public string sicDescription
        {
            get
            {
                return this.sicDescriptionField;
            }
            set
            {
                this.sicDescriptionField = value;
            }
        }
    }

    /// <remarks/>





    public partial class Module
    {

        private float hoursField;

        private string productCodeField;

        private string productDescField;

        private ProductType productTypeField;

        /// <remarks/>
        public float hours
        {
            get
            {
                return this.hoursField;
            }
            set
            {
                this.hoursField = value;
            }
        }

        /// <remarks/>

        public string productCode
        {
            get
            {
                return this.productCodeField;
            }
            set
            {
                this.productCodeField = value;
            }
        }

        /// <remarks/>

        public string productDesc
        {
            get
            {
                return this.productDescField;
            }
            set
            {
                this.productDescField = value;
            }
        }

        /// <remarks/>
        public ProductType productType
        {
            get
            {
                return this.productTypeField;
            }
            set
            {
                this.productTypeField = value;
            }
        }
    }

    /// <remarks/>



    public enum ProductType
    {

        /// <remarks/>
        View,

        /// <remarks/>
        Investigate,
    }

    /// <remarks/>





    public partial class DebtorInfo
    {

        private string debtorDescriptionField;

        private string debtorNameField;

        /// <remarks/>

        public string debtorDescription
        {
            get
            {
                return this.debtorDescriptionField;
            }
            set
            {
                this.debtorDescriptionField = value;
            }
        }

        /// <remarks/>

        public string debtorName
        {
            get
            {
                return this.debtorNameField;
            }
            set
            {
                this.debtorNameField = value;
            }
        }
    }

    /// <remarks/>





    public partial class FinanceAmounts
    {

        private string amountBeforeField;

        private string amountCurrentField;

        /// <remarks/>

        public string amountBefore
        {
            get
            {
                return this.amountBeforeField;
            }
            set
            {
                this.amountBeforeField = value;
            }
        }

        /// <remarks/>

        public string amountCurrent
        {
            get
            {
                return this.amountCurrentField;
            }
            set
            {
                this.amountCurrentField = value;
            }
        }
    }

    /// <remarks/>





    public partial class BusinessBenchmarkDetail
    {

        private string bCA_CC_ValField;

        private string bCA_CategoryField;

        private string bCA_Ltd_ValField;

        private string bCA_Other_valField;

        private string bCA_PtyLtd_ValField;

        private string bCA_Subj_valField;

        private string bCA_Var_IdField;

        /// <remarks/>

        public string bCA_CC_Val
        {
            get
            {
                return this.bCA_CC_ValField;
            }
            set
            {
                this.bCA_CC_ValField = value;
            }
        }

        /// <remarks/>

        public string bCA_Category
        {
            get
            {
                return this.bCA_CategoryField;
            }
            set
            {
                this.bCA_CategoryField = value;
            }
        }

        /// <remarks/>

        public string bCA_Ltd_Val
        {
            get
            {
                return this.bCA_Ltd_ValField;
            }
            set
            {
                this.bCA_Ltd_ValField = value;
            }
        }

        /// <remarks/>

        public string bCA_Other_val
        {
            get
            {
                return this.bCA_Other_valField;
            }
            set
            {
                this.bCA_Other_valField = value;
            }
        }

        /// <remarks/>

        public string bCA_PtyLtd_Val
        {
            get
            {
                return this.bCA_PtyLtd_ValField;
            }
            set
            {
                this.bCA_PtyLtd_ValField = value;
            }
        }

        /// <remarks/>

        public string bCA_Subj_val
        {
            get
            {
                return this.bCA_Subj_valField;
            }
            set
            {
                this.bCA_Subj_valField = value;
            }
        }

        /// <remarks/>

        public string bCA_Var_Id
        {
            get
            {
                return this.bCA_Var_IdField;
            }
            set
            {
                this.bCA_Var_IdField = value;
            }
        }
    }

    /// <remarks/>





    public partial class Aggregate
    {

        private string nameField;

        private string valueField;

        /// <remarks/>

        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>

        public string value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(VeriCheque))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Vehicles))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(UnmatchedNotarialBond))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(UnmatchedDefault))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(UnmatchedCourtRecord))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Tradex))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(TradeReference))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(TradeReferenceSummary))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(TradePaymentHistory))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(TradeHistory))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(TradeAgeAnalysis))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(TradeAccountSynopsis))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(TicketStatus))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(SegmentDescriptions))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(SearchResponse))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(SMEAssessment))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(RegistrationDetails))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(RegistrationDetailsExtended))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(RegisteredPrincipal))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Principal1))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PrincipalDetailEmpirica))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PrincipalNotarialBonds))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(LinkedCompany))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(LinkedBusinessDefault))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PrincipalLinks))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PrincipalIDV))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PrincipalFirstResponse))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PrincipalDeedsSummaryP8))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PrincipalDeedsSummaryCO))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PrincipalDeedsComprehensiveCV))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PrincipalDeedsComprehensiveCA))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PrincipalClearance))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PrincipalArchiveP5))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PrincipalArchive))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(OtherOperation))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(AdditionalOperations))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Operation))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Observation))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ObservationCont))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(NotarialBond))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Names))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ModuleAvailabilityResponse))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(MailboxRetrieveList))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(LinkedBusinessHeader))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Header))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(GeneralBankingInfo))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(FirstResponse))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(FinancialRatios))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(FinanceHeader))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(FinanceData))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(FinanceDataFF))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(FinanceDataFE))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(EnquirySummary))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(EnquiryHistory))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Empirica))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(EmpiricaEM04))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(EmpiricaE1))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(EmpOfCapital))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(DynamicRating))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Default))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(DeedsMultipleBond))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(DeedHistory))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(CurrentLiabilities))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(CurrentAsset))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(CourtRecord))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ConsumerTraceAlert))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ConsumerNotice))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ConsumerNotarialBonds))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ConsumerJudgement))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ConsumerInfoNO04))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ConsumerHeader))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ConsumerHeaderC1))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ConsumerEnquiry))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ConsumerDefault))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ConsumerBusinessEnquiry))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ComplianceIndicatorLI))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(CommercialDisputeResponse))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(CapitalEmployed))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(CancelledTicket))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BusinessBenchmarkBB))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BusinessRescueDetail))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BusinessPlusPrincipalSummary))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BusinessNotarialBonds))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BusinessDeedsSummaryDA))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BusinessDeedsDI))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BusinessDeedsComprehensivePW))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BusinessDeedsComprehensivePB))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BusinessAdverseSummary))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Branch))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BankingDetailSummary))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BankReport))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BankHistory))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BankCodes))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(AggregateCW))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Affiliations))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(AccountVerificationVH))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(AccountVerificationVA))]





    public partial class BureauSegment
    {
    }

    /// <remarks/>





    public partial class VeriCheque : BureauSegment
    {

        private string addressField;

        private string cityField;

        private string countryField;

        private string majorProductField;

        private string messageField;

        private string numberFoundField;

        private string postCodeField;

        private string reasonField;

        private string suburbField;

        private string veriAmountField;

        private string veriDateField;

        private string veriRegisterNameField;

        /// <remarks/>

        public string address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>

        public string city
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>

        public string country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }

        /// <remarks/>

        public string numberFound
        {
            get
            {
                return this.numberFoundField;
            }
            set
            {
                this.numberFoundField = value;
            }
        }

        /// <remarks/>

        public string postCode
        {
            get
            {
                return this.postCodeField;
            }
            set
            {
                this.postCodeField = value;
            }
        }

        /// <remarks/>

        public string reason
        {
            get
            {
                return this.reasonField;
            }
            set
            {
                this.reasonField = value;
            }
        }

        /// <remarks/>

        public string suburb
        {
            get
            {
                return this.suburbField;
            }
            set
            {
                this.suburbField = value;
            }
        }

        /// <remarks/>

        public string veriAmount
        {
            get
            {
                return this.veriAmountField;
            }
            set
            {
                this.veriAmountField = value;
            }
        }

        /// <remarks/>

        public string veriDate
        {
            get
            {
                return this.veriDateField;
            }
            set
            {
                this.veriDateField = value;
            }
        }

        /// <remarks/>

        public string veriRegisterName
        {
            get
            {
                return this.veriRegisterNameField;
            }
            set
            {
                this.veriRegisterNameField = value;
            }
        }
    }

    /// <remarks/>





    public partial class Vehicles : BureauSegment
    {

        private string commentField;

        private string dateInfoField;

        private string majorProductField;

        private Vehicle[] vehicleListField;

        /// <remarks/>

        public string comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>

        public string dateInfo
        {
            get
            {
                return this.dateInfoField;
            }
            set
            {
                this.dateInfoField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public Vehicle[] vehicleList
        {
            get
            {
                return this.vehicleListField;
            }
            set
            {
                this.vehicleListField = value;
            }
        }
    }

    /// <remarks/>





    public partial class UnmatchedNotarialBond : BureauSegment
    {

        private string addressField;

        private string bondAmountField;

        private string bondDateField;

        private string bondDistrictField;

        private string bondNumberField;

        private string bondPercentField;

        private string cityField;

        private string countryField;

        private string dateCancelledField;

        private string majorProductField;

        private string messageField;

        private string mortgageeField;

        private string mortgagorDistrictField;

        private string mortgagorField;

        private string numberFoundField;

        private string postCodeField;

        private string serialNumberField;

        private string suburbField;

        private string tradeStyleField;

        /// <remarks/>

        public string address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>

        public string bondAmount
        {
            get
            {
                return this.bondAmountField;
            }
            set
            {
                this.bondAmountField = value;
            }
        }

        /// <remarks/>

        public string bondDate
        {
            get
            {
                return this.bondDateField;
            }
            set
            {
                this.bondDateField = value;
            }
        }

        /// <remarks/>

        public string bondDistrict
        {
            get
            {
                return this.bondDistrictField;
            }
            set
            {
                this.bondDistrictField = value;
            }
        }

        /// <remarks/>

        public string bondNumber
        {
            get
            {
                return this.bondNumberField;
            }
            set
            {
                this.bondNumberField = value;
            }
        }

        /// <remarks/>

        public string bondPercent
        {
            get
            {
                return this.bondPercentField;
            }
            set
            {
                this.bondPercentField = value;
            }
        }

        /// <remarks/>

        public string city
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>

        public string country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        /// <remarks/>

        public string dateCancelled
        {
            get
            {
                return this.dateCancelledField;
            }
            set
            {
                this.dateCancelledField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }

        /// <remarks/>

        public string mortgagee
        {
            get
            {
                return this.mortgageeField;
            }
            set
            {
                this.mortgageeField = value;
            }
        }

        /// <remarks/>

        public string mortgagorDistrict
        {
            get
            {
                return this.mortgagorDistrictField;
            }
            set
            {
                this.mortgagorDistrictField = value;
            }
        }

        /// <remarks/>

        public string mortgagor
        {
            get
            {
                return this.mortgagorField;
            }
            set
            {
                this.mortgagorField = value;
            }
        }

        /// <remarks/>

        public string numberFound
        {
            get
            {
                return this.numberFoundField;
            }
            set
            {
                this.numberFoundField = value;
            }
        }

        /// <remarks/>

        public string postCode
        {
            get
            {
                return this.postCodeField;
            }
            set
            {
                this.postCodeField = value;
            }
        }

        /// <remarks/>

        public string serialNumber
        {
            get
            {
                return this.serialNumberField;
            }
            set
            {
                this.serialNumberField = value;
            }
        }

        /// <remarks/>

        public string suburb
        {
            get
            {
                return this.suburbField;
            }
            set
            {
                this.suburbField = value;
            }
        }

        /// <remarks/>

        public string tradeStyle
        {
            get
            {
                return this.tradeStyleField;
            }
            set
            {
                this.tradeStyleField = value;
            }
        }
    }

    /// <remarks/>





    public partial class UnmatchedDefault : BureauSegment
    {

        private string amountField;

        private string cityField;

        private string commentField;

        private string countryField;

        private string defaultAddressField;

        private string defaultDateField;

        private string defaultNameField;

        private string defaultTradeStyleField;

        private string majorProductField;

        private string messageField;

        private string numberFoundField;

        private string onBehalfOfField;

        private string postCodeField;

        private string serialNoField;

        private string statusField;

        private string subscriberNameField;

        private string suburbField;

        private string supplierNameField;

        /// <remarks/>

        public string amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>

        public string city
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>

        public string comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>

        public string country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        /// <remarks/>

        public string defaultAddress
        {
            get
            {
                return this.defaultAddressField;
            }
            set
            {
                this.defaultAddressField = value;
            }
        }

        /// <remarks/>

        public string defaultDate
        {
            get
            {
                return this.defaultDateField;
            }
            set
            {
                this.defaultDateField = value;
            }
        }

        /// <remarks/>

        public string defaultName
        {
            get
            {
                return this.defaultNameField;
            }
            set
            {
                this.defaultNameField = value;
            }
        }

        /// <remarks/>

        public string defaultTradeStyle
        {
            get
            {
                return this.defaultTradeStyleField;
            }
            set
            {
                this.defaultTradeStyleField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }

        /// <remarks/>

        public string numberFound
        {
            get
            {
                return this.numberFoundField;
            }
            set
            {
                this.numberFoundField = value;
            }
        }

        /// <remarks/>

        public string onBehalfOf
        {
            get
            {
                return this.onBehalfOfField;
            }
            set
            {
                this.onBehalfOfField = value;
            }
        }

        /// <remarks/>

        public string postCode
        {
            get
            {
                return this.postCodeField;
            }
            set
            {
                this.postCodeField = value;
            }
        }

        /// <remarks/>

        public string serialNo
        {
            get
            {
                return this.serialNoField;
            }
            set
            {
                this.serialNoField = value;
            }
        }

        /// <remarks/>

        public string status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>

        public string subscriberName
        {
            get
            {
                return this.subscriberNameField;
            }
            set
            {
                this.subscriberNameField = value;
            }
        }

        /// <remarks/>

        public string suburb
        {
            get
            {
                return this.suburbField;
            }
            set
            {
                this.suburbField = value;
            }
        }

        /// <remarks/>

        public string supplierName
        {
            get
            {
                return this.supplierNameField;
            }
            set
            {
                this.supplierNameField = value;
            }
        }
    }

    /// <remarks/>





    public partial class UnmatchedCourtRecord : BureauSegment
    {

        private string abandonDateField;

        private string actionDateField;

        private string attorneyField;

        private string caseNumberField;

        private string cityField;

        private string claimAmountField;

        private string commentField;

        private string countryField;

        private string courtDistrictField;

        private string courtRecordAddressField;

        private string courtTypeField;

        private string defendantDistrictField;

        private string defendantNameField;

        private string defendantTradeStyleField;

        private string majorProductField;

        private string messageField;

        private string natureOfDebtField;

        private string numberFoundField;

        private string plaintiffNameField;

        private string postCodeField;

        private string returnDateField;

        private string serialNumberField;

        private string suburbField;

        private string typeCodeField;

        private string typeDescField;

        /// <remarks/>

        public string abandonDate
        {
            get
            {
                return this.abandonDateField;
            }
            set
            {
                this.abandonDateField = value;
            }
        }

        /// <remarks/>

        public string actionDate
        {
            get
            {
                return this.actionDateField;
            }
            set
            {
                this.actionDateField = value;
            }
        }

        /// <remarks/>

        public string attorney
        {
            get
            {
                return this.attorneyField;
            }
            set
            {
                this.attorneyField = value;
            }
        }

        /// <remarks/>

        public string caseNumber
        {
            get
            {
                return this.caseNumberField;
            }
            set
            {
                this.caseNumberField = value;
            }
        }

        /// <remarks/>

        public string city
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>

        public string claimAmount
        {
            get
            {
                return this.claimAmountField;
            }
            set
            {
                this.claimAmountField = value;
            }
        }

        /// <remarks/>

        public string comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>

        public string country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        /// <remarks/>

        public string courtDistrict
        {
            get
            {
                return this.courtDistrictField;
            }
            set
            {
                this.courtDistrictField = value;
            }
        }

        /// <remarks/>

        public string courtRecordAddress
        {
            get
            {
                return this.courtRecordAddressField;
            }
            set
            {
                this.courtRecordAddressField = value;
            }
        }

        /// <remarks/>

        public string courtType
        {
            get
            {
                return this.courtTypeField;
            }
            set
            {
                this.courtTypeField = value;
            }
        }

        /// <remarks/>

        public string defendantDistrict
        {
            get
            {
                return this.defendantDistrictField;
            }
            set
            {
                this.defendantDistrictField = value;
            }
        }

        /// <remarks/>

        public string defendantName
        {
            get
            {
                return this.defendantNameField;
            }
            set
            {
                this.defendantNameField = value;
            }
        }

        /// <remarks/>

        public string defendantTradeStyle
        {
            get
            {
                return this.defendantTradeStyleField;
            }
            set
            {
                this.defendantTradeStyleField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }

        /// <remarks/>

        public string natureOfDebt
        {
            get
            {
                return this.natureOfDebtField;
            }
            set
            {
                this.natureOfDebtField = value;
            }
        }

        /// <remarks/>

        public string numberFound
        {
            get
            {
                return this.numberFoundField;
            }
            set
            {
                this.numberFoundField = value;
            }
        }

        /// <remarks/>

        public string plaintiffName
        {
            get
            {
                return this.plaintiffNameField;
            }
            set
            {
                this.plaintiffNameField = value;
            }
        }

        /// <remarks/>

        public string postCode
        {
            get
            {
                return this.postCodeField;
            }
            set
            {
                this.postCodeField = value;
            }
        }

        /// <remarks/>

        public string returnDate
        {
            get
            {
                return this.returnDateField;
            }
            set
            {
                this.returnDateField = value;
            }
        }

        /// <remarks/>

        public string serialNumber
        {
            get
            {
                return this.serialNumberField;
            }
            set
            {
                this.serialNumberField = value;
            }
        }

        /// <remarks/>

        public string suburb
        {
            get
            {
                return this.suburbField;
            }
            set
            {
                this.suburbField = value;
            }
        }

        /// <remarks/>

        public string typeCode
        {
            get
            {
                return this.typeCodeField;
            }
            set
            {
                this.typeCodeField = value;
            }
        }

        /// <remarks/>

        public string typeDesc
        {
            get
            {
                return this.typeDescField;
            }
            set
            {
                this.typeDescField = value;
            }
        }
    }

    /// <remarks/>





    public partial class Tradex : BureauSegment
    {

        private string averageDaysEarlyLateField;

        private string calculationPeriodField;

        private CreditBand[] creditBandsField;

        private string dateCreatedField;

        private string debtorNameField;

        private string globalScoreField;

        private string majorProductField;

        private string scoreCriteriaField;

        private string totalInvoiceAmountField;

        private string totalInvoicesField;

        private string totalOverdueAmountField;

        private string totalOverdueInvoicesField;

        /// <remarks/>

        public string averageDaysEarlyLate
        {
            get
            {
                return this.averageDaysEarlyLateField;
            }
            set
            {
                this.averageDaysEarlyLateField = value;
            }
        }

        /// <remarks/>

        public string calculationPeriod
        {
            get
            {
                return this.calculationPeriodField;
            }
            set
            {
                this.calculationPeriodField = value;
            }
        }

        /// <remarks/>

        public CreditBand[] creditBands
        {
            get
            {
                return this.creditBandsField;
            }
            set
            {
                this.creditBandsField = value;
            }
        }

        /// <remarks/>

        public string dateCreated
        {
            get
            {
                return this.dateCreatedField;
            }
            set
            {
                this.dateCreatedField = value;
            }
        }

        /// <remarks/>

        public string debtorName
        {
            get
            {
                return this.debtorNameField;
            }
            set
            {
                this.debtorNameField = value;
            }
        }

        /// <remarks/>

        public string globalScore
        {
            get
            {
                return this.globalScoreField;
            }
            set
            {
                this.globalScoreField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string scoreCriteria
        {
            get
            {
                return this.scoreCriteriaField;
            }
            set
            {
                this.scoreCriteriaField = value;
            }
        }

        /// <remarks/>

        public string totalInvoiceAmount
        {
            get
            {
                return this.totalInvoiceAmountField;
            }
            set
            {
                this.totalInvoiceAmountField = value;
            }
        }

        /// <remarks/>

        public string totalInvoices
        {
            get
            {
                return this.totalInvoicesField;
            }
            set
            {
                this.totalInvoicesField = value;
            }
        }

        /// <remarks/>

        public string totalOverdueAmount
        {
            get
            {
                return this.totalOverdueAmountField;
            }
            set
            {
                this.totalOverdueAmountField = value;
            }
        }

        /// <remarks/>

        public string totalOverdueInvoices
        {
            get
            {
                return this.totalOverdueInvoicesField;
            }
            set
            {
                this.totalOverdueInvoicesField = value;
            }
        }
    }

    /// <remarks/>





    public partial class TradeReference : BureauSegment
    {

        private string commentField;

        private string creditLimitField;

        private string dateOfRefField;

        private string discountField;

        private string insuranceDescField;

        private string insuranceField;

        private string majorProductField;

        private string monthsKnownField;

        private string obtainedField;

        private string purchasesField;

        private string referenceNameField;

        private string termsGivenField;

        private string termsTakenField;

        private string tradeAccountNumberField;

        private string unlimitedField;

        private string yearsKnownField;

        /// <remarks/>

        public string comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>

        public string creditLimit
        {
            get
            {
                return this.creditLimitField;
            }
            set
            {
                this.creditLimitField = value;
            }
        }

        /// <remarks/>

        public string dateOfRef
        {
            get
            {
                return this.dateOfRefField;
            }
            set
            {
                this.dateOfRefField = value;
            }
        }

        /// <remarks/>

        public string discount
        {
            get
            {
                return this.discountField;
            }
            set
            {
                this.discountField = value;
            }
        }

        /// <remarks/>

        public string insuranceDesc
        {
            get
            {
                return this.insuranceDescField;
            }
            set
            {
                this.insuranceDescField = value;
            }
        }

        /// <remarks/>

        public string insurance
        {
            get
            {
                return this.insuranceField;
            }
            set
            {
                this.insuranceField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string monthsKnown
        {
            get
            {
                return this.monthsKnownField;
            }
            set
            {
                this.monthsKnownField = value;
            }
        }

        /// <remarks/>

        public string obtained
        {
            get
            {
                return this.obtainedField;
            }
            set
            {
                this.obtainedField = value;
            }
        }

        /// <remarks/>

        public string purchases
        {
            get
            {
                return this.purchasesField;
            }
            set
            {
                this.purchasesField = value;
            }
        }

        /// <remarks/>

        public string referenceName
        {
            get
            {
                return this.referenceNameField;
            }
            set
            {
                this.referenceNameField = value;
            }
        }

        /// <remarks/>

        public string termsGiven
        {
            get
            {
                return this.termsGivenField;
            }
            set
            {
                this.termsGivenField = value;
            }
        }

        /// <remarks/>

        public string termsTaken
        {
            get
            {
                return this.termsTakenField;
            }
            set
            {
                this.termsTakenField = value;
            }
        }

        /// <remarks/>

        public string tradeAccountNumber
        {
            get
            {
                return this.tradeAccountNumberField;
            }
            set
            {
                this.tradeAccountNumberField = value;
            }
        }

        /// <remarks/>

        public string unlimited
        {
            get
            {
                return this.unlimitedField;
            }
            set
            {
                this.unlimitedField = value;
            }
        }

        /// <remarks/>

        public string yearsKnown
        {
            get
            {
                return this.yearsKnownField;
            }
            set
            {
                this.yearsKnownField = value;
            }
        }
    }

    /// <remarks/>





    public partial class TradeReferenceSummary : BureauSegment
    {

        private string majorProductField;

        private string sixMonthAverageField;

        private string sixMonthNumberOutsideTermsField;

        private string sixMonthNumberWithinTermsField;

        private string sixMonthTotalField;

        private string sixMonthWeightedAverageField;

        private string threeMonthAverageField;

        private string threeMonthNumberOutsideTermsField;

        private string threeMonthNumberWithinTermsField;

        private string threeMonthTotalField;

        private string threeMonthWeightedAverageField;

        private string twelveMonthAverageField;

        private string twelveMonthNumberOutsideTermsField;

        private string twelveMonthNumberWithinTermsField;

        private string twelveMonthTotalField;

        private string twelveMonthWeightedAverageField;

        private string twentyFourMonthAverageField;

        private string twentyFourMonthNumberOutsideTermsField;

        private string twentyFourMonthNumberWithinTermsField;

        private string twentyFourMonthTotalField;

        private string twentyFourMonthWeightedAverageField;

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string sixMonthAverage
        {
            get
            {
                return this.sixMonthAverageField;
            }
            set
            {
                this.sixMonthAverageField = value;
            }
        }

        /// <remarks/>

        public string sixMonthNumberOutsideTerms
        {
            get
            {
                return this.sixMonthNumberOutsideTermsField;
            }
            set
            {
                this.sixMonthNumberOutsideTermsField = value;
            }
        }

        /// <remarks/>

        public string sixMonthNumberWithinTerms
        {
            get
            {
                return this.sixMonthNumberWithinTermsField;
            }
            set
            {
                this.sixMonthNumberWithinTermsField = value;
            }
        }

        /// <remarks/>

        public string sixMonthTotal
        {
            get
            {
                return this.sixMonthTotalField;
            }
            set
            {
                this.sixMonthTotalField = value;
            }
        }

        /// <remarks/>

        public string sixMonthWeightedAverage
        {
            get
            {
                return this.sixMonthWeightedAverageField;
            }
            set
            {
                this.sixMonthWeightedAverageField = value;
            }
        }

        /// <remarks/>

        public string threeMonthAverage
        {
            get
            {
                return this.threeMonthAverageField;
            }
            set
            {
                this.threeMonthAverageField = value;
            }
        }

        /// <remarks/>

        public string threeMonthNumberOutsideTerms
        {
            get
            {
                return this.threeMonthNumberOutsideTermsField;
            }
            set
            {
                this.threeMonthNumberOutsideTermsField = value;
            }
        }

        /// <remarks/>

        public string threeMonthNumberWithinTerms
        {
            get
            {
                return this.threeMonthNumberWithinTermsField;
            }
            set
            {
                this.threeMonthNumberWithinTermsField = value;
            }
        }

        /// <remarks/>

        public string threeMonthTotal
        {
            get
            {
                return this.threeMonthTotalField;
            }
            set
            {
                this.threeMonthTotalField = value;
            }
        }

        /// <remarks/>

        public string threeMonthWeightedAverage
        {
            get
            {
                return this.threeMonthWeightedAverageField;
            }
            set
            {
                this.threeMonthWeightedAverageField = value;
            }
        }

        /// <remarks/>

        public string twelveMonthAverage
        {
            get
            {
                return this.twelveMonthAverageField;
            }
            set
            {
                this.twelveMonthAverageField = value;
            }
        }

        /// <remarks/>

        public string twelveMonthNumberOutsideTerms
        {
            get
            {
                return this.twelveMonthNumberOutsideTermsField;
            }
            set
            {
                this.twelveMonthNumberOutsideTermsField = value;
            }
        }

        /// <remarks/>

        public string twelveMonthNumberWithinTerms
        {
            get
            {
                return this.twelveMonthNumberWithinTermsField;
            }
            set
            {
                this.twelveMonthNumberWithinTermsField = value;
            }
        }

        /// <remarks/>

        public string twelveMonthTotal
        {
            get
            {
                return this.twelveMonthTotalField;
            }
            set
            {
                this.twelveMonthTotalField = value;
            }
        }

        /// <remarks/>

        public string twelveMonthWeightedAverage
        {
            get
            {
                return this.twelveMonthWeightedAverageField;
            }
            set
            {
                this.twelveMonthWeightedAverageField = value;
            }
        }

        /// <remarks/>

        public string twentyFourMonthAverage
        {
            get
            {
                return this.twentyFourMonthAverageField;
            }
            set
            {
                this.twentyFourMonthAverageField = value;
            }
        }

        /// <remarks/>

        public string twentyFourMonthNumberOutsideTerms
        {
            get
            {
                return this.twentyFourMonthNumberOutsideTermsField;
            }
            set
            {
                this.twentyFourMonthNumberOutsideTermsField = value;
            }
        }

        /// <remarks/>

        public string twentyFourMonthNumberWithinTerms
        {
            get
            {
                return this.twentyFourMonthNumberWithinTermsField;
            }
            set
            {
                this.twentyFourMonthNumberWithinTermsField = value;
            }
        }

        /// <remarks/>

        public string twentyFourMonthTotal
        {
            get
            {
                return this.twentyFourMonthTotalField;
            }
            set
            {
                this.twentyFourMonthTotalField = value;
            }
        }

        /// <remarks/>

        public string twentyFourMonthWeightedAverage
        {
            get
            {
                return this.twentyFourMonthWeightedAverageField;
            }
            set
            {
                this.twentyFourMonthWeightedAverageField = value;
            }
        }
    }

    /// <remarks/>





    public partial class TradePaymentHistory : BureauSegment
    {

        private string majorProductField;

        private string messageField;

        private PaymentHistoryDetail[] paymentHistoryDetailsField;

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }

        /// <remarks/>

        public PaymentHistoryDetail[] paymentHistoryDetails
        {
            get
            {
                return this.paymentHistoryDetailsField;
            }
            set
            {
                this.paymentHistoryDetailsField = value;
            }
        }
    }

    /// <remarks/>





    public partial class TradeHistory : BureauSegment
    {

        private decimal assuredValueField;

        private string commentField;

        private decimal creditLimitField;

        private DateTime? dateOfRefField;

        private string discountField;

        private string insuranceDescField;

        private string insuranceField;

        private int majorProductField;

        private string monthsKnownField;

        private string obtainedField;

        private decimal purchasesField;

        private string referenceNameField;

        private string termsGivenField;

        private decimal termsTakenField;

        private string unlimitedField;

        private string yearsKnownField;

        /// <remarks/>

        public decimal assuredValue
        {
            get
            {
                return this.assuredValueField;
            }
            set
            {
                this.assuredValueField = value;
            }
        }

        /// <remarks/>

        public string comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>

        public decimal creditLimit
        {
            get
            {
                return this.creditLimitField;
            }
            set
            {
                this.creditLimitField = value;
            }
        }

        /// <remarks/>

        public DateTime? dateOfRef
        {
            get
            {
                return this.dateOfRefField;
            }
            set
            {
                this.dateOfRefField = value;
            }
        }

        /// <remarks/>

        public string discount
        {
            get
            {
                return this.discountField;
            }
            set
            {
                this.discountField = value;
            }
        }

        /// <remarks/>

        public string insuranceDesc
        {
            get
            {
                return this.insuranceDescField;
            }
            set
            {
                this.insuranceDescField = value;
            }
        }

        /// <remarks/>

        public string insurance
        {
            get
            {
                return this.insuranceField;
            }
            set
            {
                this.insuranceField = value;
            }
        }

        /// <remarks/>

        public int majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string monthsKnown
        {
            get
            {
                return this.monthsKnownField;
            }
            set
            {
                this.monthsKnownField = value;
            }
        }

        /// <remarks/>

        public string obtained
        {
            get
            {
                return this.obtainedField;
            }
            set
            {
                this.obtainedField = value;
            }
        }

        /// <remarks/>

        public decimal purchases
        {
            get
            {
                return this.purchasesField;
            }
            set
            {
                this.purchasesField = value;
            }
        }

        /// <remarks/>

        public string referenceName
        {
            get
            {
                return this.referenceNameField;
            }
            set
            {
                this.referenceNameField = value;
            }
        }

        /// <remarks/>

        public string termsGiven
        {
            get
            {
                return this.termsGivenField;
            }
            set
            {
                this.termsGivenField = value;
            }
        }

        /// <remarks/>

        public decimal termsTaken
        {
            get
            {
                return this.termsTakenField;
            }
            set
            {
                this.termsTakenField = value;
            }
        }

        /// <remarks/>

        public string unlimited
        {
            get
            {
                return this.unlimitedField;
            }
            set
            {
                this.unlimitedField = value;
            }
        }

        /// <remarks/>

        public string yearsKnown
        {
            get
            {
                return this.yearsKnownField;
            }
            set
            {
                this.yearsKnownField = value;
            }
        }
    }

    /// <remarks/>





    public partial class TradeAgeAnalysis : BureauSegment
    {

        private AgeAnalysisDetail[] ageAnalysisDetailsField;

        private string dateField;

        private string majorProductField;

        private string messageField;

        /// <remarks/>

        public AgeAnalysisDetail[] ageAnalysisDetails
        {
            get
            {
                return this.ageAnalysisDetailsField;
            }
            set
            {
                this.ageAnalysisDetailsField = value;
            }
        }

        /// <remarks/>

        public string date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }
    }

    /// <remarks/>





    public partial class TradeAccountSynopsis : BureauSegment
    {

        private AccountSynopsisDetail[] accountSynopsisDetailsField;

        private string dateField;

        private string majorProductField;

        private string messageField;

        /// <remarks/>

        public AccountSynopsisDetail[] accountSynopsisDetails
        {
            get
            {
                return this.accountSynopsisDetailsField;
            }
            set
            {
                this.accountSynopsisDetailsField = value;
            }
        }

        /// <remarks/>

        public string date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }
    }

    /// <remarks/>





    public partial class TicketStatus : BureauSegment
    {

        private string[] statusField;

        private string[] subjectNameField;

        private string[] ticketField;

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] subjectName
        {
            get
            {
                return this.subjectNameField;
            }
            set
            {
                this.subjectNameField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] ticket
        {
            get
            {
                return this.ticketField;
            }
            set
            {
                this.ticketField = value;
            }
        }
    }

    /// <remarks/>





    public partial class SegmentDescriptions : BureauSegment
    {

        private SegmentDescription[] segmentInfoDescriptionsField;

        /// <remarks/>

        public SegmentDescription[] segmentInfoDescriptions
        {
            get
            {
                return this.segmentInfoDescriptionsField;
            }
            set
            {
                this.segmentInfoDescriptionsField = value;
            }
        }
    }

    /// <remarks/>





    public partial class SearchResponse : BureauSegment
    {

        private string businessNameField;

        private string countryField;

        private string faxNoField;

        private string iTNumberField;

        private string nameField;

        private string nameTypeField;

        private string phoneNoField;

        private string physicalAddressField;

        private string postCodeField;

        private string postalAddressField;

        private string postalCountryField;

        private string postalPostCodeField;

        private string postalSuburbField;

        private string postalTownField;

        private string regNoField;

        private string regStatusCodeField;

        private string regStatusField;

        private string suburbField;

        private string townField;

        private string tradingNumberField;

        /// <remarks/>

        public string businessName
        {
            get
            {
                return this.businessNameField;
            }
            set
            {
                this.businessNameField = value;
            }
        }

        /// <remarks/>

        public string country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        /// <remarks/>

        public string faxNo
        {
            get
            {
                return this.faxNoField;
            }
            set
            {
                this.faxNoField = value;
            }
        }

        /// <remarks/>

        public string iTNumber
        {
            get
            {
                return this.iTNumberField;
            }
            set
            {
                this.iTNumberField = value;
            }
        }

        /// <remarks/>

        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>

        public string nameType
        {
            get
            {
                return this.nameTypeField;
            }
            set
            {
                this.nameTypeField = value;
            }
        }

        /// <remarks/>

        public string phoneNo
        {
            get
            {
                return this.phoneNoField;
            }
            set
            {
                this.phoneNoField = value;
            }
        }

        /// <remarks/>

        public string physicalAddress
        {
            get
            {
                return this.physicalAddressField;
            }
            set
            {
                this.physicalAddressField = value;
            }
        }

        /// <remarks/>

        public string postCode
        {
            get
            {
                return this.postCodeField;
            }
            set
            {
                this.postCodeField = value;
            }
        }

        /// <remarks/>

        public string postalAddress
        {
            get
            {
                return this.postalAddressField;
            }
            set
            {
                this.postalAddressField = value;
            }
        }

        /// <remarks/>

        public string postalCountry
        {
            get
            {
                return this.postalCountryField;
            }
            set
            {
                this.postalCountryField = value;
            }
        }

        /// <remarks/>

        public string postalPostCode
        {
            get
            {
                return this.postalPostCodeField;
            }
            set
            {
                this.postalPostCodeField = value;
            }
        }

        /// <remarks/>

        public string postalSuburb
        {
            get
            {
                return this.postalSuburbField;
            }
            set
            {
                this.postalSuburbField = value;
            }
        }

        /// <remarks/>

        public string postalTown
        {
            get
            {
                return this.postalTownField;
            }
            set
            {
                this.postalTownField = value;
            }
        }

        /// <remarks/>

        public string regNo
        {
            get
            {
                return this.regNoField;
            }
            set
            {
                this.regNoField = value;
            }
        }

        /// <remarks/>

        public string regStatusCode
        {
            get
            {
                return this.regStatusCodeField;
            }
            set
            {
                this.regStatusCodeField = value;
            }
        }

        /// <remarks/>

        public string regStatus
        {
            get
            {
                return this.regStatusField;
            }
            set
            {
                this.regStatusField = value;
            }
        }

        /// <remarks/>

        public string suburb
        {
            get
            {
                return this.suburbField;
            }
            set
            {
                this.suburbField = value;
            }
        }

        /// <remarks/>

        public string town
        {
            get
            {
                return this.townField;
            }
            set
            {
                this.townField = value;
            }
        }

        /// <remarks/>

        public string tradingNumber
        {
            get
            {
                return this.tradingNumberField;
            }
            set
            {
                this.tradingNumberField = value;
            }
        }
    }

    /// <remarks/>





    public partial class SMEAssessment : BureauSegment
    {

        private string[] averageGraphValueField;

        private string bandDescField;

        private string bandTrendField;

        private string[] commentsField;

        private string failureRateField;

        private string[] historyAverageField;

        private string[] historyDateField;

        private string[] historyScoreField;

        private string[] keyContrCodesField;

        private string majorProductField;

        private string oddsOfFailureField;

        private string[] recomActionCodeField;

        private string sMECommentCodeField;

        private string scoreBandField;

        private string scoreDateField;

        private string scoreField;

        private string sizeField;

        private string[] subjectGraphValueField;

        private string typeOfScoreField;

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] averageGraphValue
        {
            get
            {
                return this.averageGraphValueField;
            }
            set
            {
                this.averageGraphValueField = value;
            }
        }

        /// <remarks/>

        public string bandDesc
        {
            get
            {
                return this.bandDescField;
            }
            set
            {
                this.bandDescField = value;
            }
        }

        /// <remarks/>

        public string bandTrend
        {
            get
            {
                return this.bandTrendField;
            }
            set
            {
                this.bandTrendField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] comments
        {
            get
            {
                return this.commentsField;
            }
            set
            {
                this.commentsField = value;
            }
        }

        /// <remarks/>

        public string failureRate
        {
            get
            {
                return this.failureRateField;
            }
            set
            {
                this.failureRateField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] historyAverage
        {
            get
            {
                return this.historyAverageField;
            }
            set
            {
                this.historyAverageField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] historyDate
        {
            get
            {
                return this.historyDateField;
            }
            set
            {
                this.historyDateField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] historyScore
        {
            get
            {
                return this.historyScoreField;
            }
            set
            {
                this.historyScoreField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] keyContrCodes
        {
            get
            {
                return this.keyContrCodesField;
            }
            set
            {
                this.keyContrCodesField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string oddsOfFailure
        {
            get
            {
                return this.oddsOfFailureField;
            }
            set
            {
                this.oddsOfFailureField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] recomActionCode
        {
            get
            {
                return this.recomActionCodeField;
            }
            set
            {
                this.recomActionCodeField = value;
            }
        }

        /// <remarks/>

        public string sMECommentCode
        {
            get
            {
                return this.sMECommentCodeField;
            }
            set
            {
                this.sMECommentCodeField = value;
            }
        }

        /// <remarks/>

        public string scoreBand
        {
            get
            {
                return this.scoreBandField;
            }
            set
            {
                this.scoreBandField = value;
            }
        }

        /// <remarks/>

        public string scoreDate
        {
            get
            {
                return this.scoreDateField;
            }
            set
            {
                this.scoreDateField = value;
            }
        }

        /// <remarks/>

        public string score
        {
            get
            {
                return this.scoreField;
            }
            set
            {
                this.scoreField = value;
            }
        }

        /// <remarks/>

        public string size
        {
            get
            {
                return this.sizeField;
            }
            set
            {
                this.sizeField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] subjectGraphValue
        {
            get
            {
                return this.subjectGraphValueField;
            }
            set
            {
                this.subjectGraphValueField = value;
            }
        }

        /// <remarks/>

        public string typeOfScore
        {
            get
            {
                return this.typeOfScoreField;
            }
            set
            {
                this.typeOfScoreField = value;
            }
        }
    }

    /// <remarks/>





    public partial class RegistrationDetails : BureauSegment
    {

        private string auditorsField;

        private string authorisedCapitalField;

        private CapitalInfo[] capitalInfoField;

        private string cityField;

        private string companyTypeCodeField;

        private string companyTypeDescField;

        private string countryField;

        private string financialYearEndField;

        private string issuedCapitalField;

        private string liquidationDateField;

        private string majorProductField;

        private string postCodeField;

        private string[] postalAddressField;

        private string postalPostCodeField;

        private string[] previousRegistrationNoField;

        private string reg_Info_DateField;

        private string reg_Status_ReasonField;

        private string registeredAddressField;

        private string registrationCountryField;

        private string registrationDateField;

        private string registrationNoField;

        private string registrationStatusCodeField;

        private string registrationStatusDescField;

        private ShareInfo[] shareInfoField;

        private string statedCapitalField;

        private string suburbField;

        /// <remarks/>

        //public string auditors
        //{
        //    get
        //    {
        //        return this.auditorsField;
        //    }
        //    set
        //    {
        //        this.auditorsField = value;
        //    }
        //}

        /// <remarks/>

        public string authorisedCapital
        {
            get
            {
                return this.authorisedCapitalField;
            }
            set
            {
                this.authorisedCapitalField = value;
            }
        }

        /// <remarks/>

        //public CapitalInfo[] capitalInfo
        //{
        //    get
        //    {
        //        return this.capitalInfoField;
        //    }
        //    set
        //    {
        //        this.capitalInfoField = value;
        //    }
        //}

        ///// <remarks/>

        //public string city
        //{
        //    get
        //    {
        //        return this.cityField;
        //    }
        //    set
        //    {
        //        this.cityField = value;
        //    }
        //}

        ///// <remarks/>

        //public string companyTypeCode
        //{
        //    get
        //    {
        //        return this.companyTypeCodeField;
        //    }
        //    set
        //    {
        //        this.companyTypeCodeField = value;
        //    }
        //}

        /// <remarks/>

        public string companyTypeDesc
        {
            get
            {
                return this.companyTypeDescField;
            }
            set
            {
                this.companyTypeDescField = value;
            }
        }

        /// <remarks/>

        //public string country
        //{
        //    get
        //    {
        //        return this.countryField;
        //    }
        //    set
        //    {
        //        this.countryField = value;
        //    }
        //}

        /// <remarks/>

        public string financialYearEnd
        {
            get
            {
                return this.financialYearEndField;
            }
            set
            {
                this.financialYearEndField = value;
            }
        }

        /// <remarks/>

        public string issuedCapital
        {
            get
            {
                return this.issuedCapitalField;
            }
            set
            {
                this.issuedCapitalField = value;
            }
        }

        /// <remarks/>

        //public string liquidationDate
        //{
        //    get
        //    {
        //        return this.liquidationDateField;
        //    }
        //    set
        //    {
        //        this.liquidationDateField = value;
        //    }
        //}

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string postCode
        {
            get
            {
                return this.postCodeField;
            }
            set
            {
                this.postCodeField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] postalAddress
        {
            get
            {
                return this.postalAddressField;
            }
            set
            {
                this.postalAddressField = value;
            }
        }

        /// <remarks/>

        public string postalPostCode
        {
            get
            {
                return this.postalPostCodeField;
            }
            set
            {
                this.postalPostCodeField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] previousRegistrationNo
        {
            get
            {
                return this.previousRegistrationNoField;
            }
            set
            {
                this.previousRegistrationNoField = value;
            }
        }

        /// <remarks/>

        public string reg_Info_Date
        {
            get
            {
                return this.reg_Info_DateField;
            }
            set
            {
                this.reg_Info_DateField = value;
            }
        }

        /// <remarks/>

        public string reg_Status_Reason
        {
            get
            {
                return this.reg_Status_ReasonField;
            }
            set
            {
                this.reg_Status_ReasonField = value;
            }
        }

        /// <remarks/>

        public string registeredAddress
        {
            get
            {
                return this.registeredAddressField;
            }
            set
            {
                this.registeredAddressField = value;
            }
        }

        /// <remarks/>

        //public string registrationCountry
        //{
        //    get
        //    {
        //        return this.registrationCountryField;
        //    }
        //    set
        //    {
        //        this.registrationCountryField = value;
        //    }
        //}

        /// <remarks/>

        public string registrationDate
        {
            get
            {
                return this.registrationDateField;
            }
            set
            {
                this.registrationDateField = value;
            }
        }

        /// <remarks/>

        public string registrationNo
        {
            get
            {
                return this.registrationNoField;
            }
            set
            {
                this.registrationNoField = value;
            }
        }

        /// <remarks/>

        public string registrationStatusCode
        {
            get
            {
                return this.registrationStatusCodeField;
            }
            set
            {
                this.registrationStatusCodeField = value;
            }
        }

        /// <remarks/>

        public string registrationStatusDesc
        {
            get
            {
                return this.registrationStatusDescField;
            }
            set
            {
                this.registrationStatusDescField = value;
            }
        }

        /// <remarks/>

        //public ShareInfo[] shareInfo
        //{
        //    get
        //    {
        //        return this.shareInfoField;
        //    }
        //    set
        //    {
        //        this.shareInfoField = value;
        //    }
        //}

        ///// <remarks/>

        //public string statedCapital
        //{
        //    get
        //    {
        //        return this.statedCapitalField;
        //    }
        //    set
        //    {
        //        this.statedCapitalField = value;
        //    }
        //}

        ///// <remarks/>

        //public string suburb
        //{
        //    get
        //    {
        //        return this.suburbField;
        //    }
        //    set
        //    {
        //        this.suburbField = value;
        //    }
        //}
    }

    /// <remarks/>





    public partial class RegistrationDetailsExtended : BureauSegment
    {

        private string businessRescueCommentField;

        private string businessRescueDateField;

        private string majorProductField;

        /// <remarks/>

        public string businessRescueComment
        {
            get
            {
                return this.businessRescueCommentField;
            }
            set
            {
                this.businessRescueCommentField = value;
            }
        }

        /// <remarks/>

        public string businessRescueDate
        {
            get
            {
                return this.businessRescueDateField;
            }
            set
            {
                this.businessRescueDateField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }
    }

    /// <remarks/>





    public partial class RegisteredPrincipal : BureauSegment
    {

        private string iDNumberField;

        private string initialsField;

        private string majorProductField;

        private string memberContributionField;

        private string memberContributionPercField;

        private string rPDateField;

        private string surnameField;

        /// <remarks/>

        public string iDNumber
        {
            get
            {
                return this.iDNumberField;
            }
            set
            {
                this.iDNumberField = value;
            }
        }

        /// <remarks/>

        public string initials
        {
            get
            {
                return this.initialsField;
            }
            set
            {
                this.initialsField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string memberContribution
        {
            get
            {
                return this.memberContributionField;
            }
            set
            {
                this.memberContributionField = value;
            }
        }

        /// <remarks/>

        public string memberContributionPerc
        {
            get
            {
                return this.memberContributionPercField;
            }
            set
            {
                this.memberContributionPercField = value;
            }
        }

        /// <remarks/>

        public string rPDate
        {
            get
            {
                return this.rPDateField;
            }
            set
            {
                this.rPDateField = value;
            }
        }

        /// <remarks/>

        public string surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }
    }

    /// <remarks/>




    //[System.Xml.Serialization.XmlTypeAttribute(TypeName="Principal", Namespace="http://schemas.datacontract.org/2004/07/MTN_Commercial.CommercialBureauService")]
    public partial class Principal1 : BureauSegment
    {

        private string assetNotarialBondField;

        private string civilCourtRecordField;

        private string commentField;

        private string confirmedByRegistrarField;

        private string dateDisputedField;

        private DateTime dateOfBirthField;

        private string dateStartedField;

        private string debtCouncilDateField;

        private string debtCouncilDescField;

        private string deedsField;

        private string defaultDataField;

        private string empiricaExclusionCodeField;

        private string empiricaExclusionDescriptionField;

        private string[] empiricaReasonCodeField;

        private string[] empiricaReasonDescriptionField;

        private string empiricaScoreField;

        private string forename1Field;

        private string forename2Field;

        private string forename3Field;

        private string iDNumberField;

        private string infoDateField;

        private int majorProductField;

        private string noticesOrNotarialBondsField;

        private string positionField;

        private string surnameField;
        private ExclusionRule[] exclusionRulesField;
        private PrincipleCharacterstics[] principleVariablesField;
        private string principalScoreField;
        private double affordabilityField;
        private double predictedIncomeField;
        private int idMatchField;
        private int consumerNoField;

        /// <remarks/>

        //public string assetNotarialBond
        //{
        //    get
        //    {
        //        return this.assetNotarialBondField;
        //    }
        //    set
        //    {
        //        this.assetNotarialBondField = value;
        //    }
        //}

        ///// <remarks/>

        //public string civilCourtRecord
        //{
        //    get
        //    {
        //        return this.civilCourtRecordField;
        //    }
        //    set
        //    {
        //        this.civilCourtRecordField = value;
        //    }
        //}

        ///// <remarks/>

        //public string comment
        //{
        //    get
        //    {
        //        return this.commentField;
        //    }
        //    set
        //    {
        //        this.commentField = value;
        //    }
        //}

        ///// <remarks/>

        //public string confirmedByRegistrar
        //{
        //    get
        //    {
        //        return this.confirmedByRegistrarField;
        //    }
        //    set
        //    {
        //        this.confirmedByRegistrarField = value;
        //    }
        //}

        ///// <remarks/>

        //public string dateDisputed
        //{
        //    get
        //    {
        //        return this.dateDisputedField;
        //    }
        //    set
        //    {
        //        this.dateDisputedField = value;
        //    }
        //}

        /// <remarks/>

        public DateTime dateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>

        //public string dateStarted
        //{
        //    get
        //    {
        //        return this.dateStartedField;
        //    }
        //    set
        //    {
        //        this.dateStartedField = value;
        //    }
        //}

        ///// <remarks/>

        //public string debtCouncilDate
        //{
        //    get
        //    {
        //        return this.debtCouncilDateField;
        //    }
        //    set
        //    {
        //        this.debtCouncilDateField = value;
        //    }
        //}

        ///// <remarks/>

        //public string debtCouncilDesc
        //{
        //    get
        //    {
        //        return this.debtCouncilDescField;
        //    }
        //    set
        //    {
        //        this.debtCouncilDescField = value;
        //    }
        //}

        ///// <remarks/>

        //public string deeds
        //{
        //    get
        //    {
        //        return this.deedsField;
        //    }
        //    set
        //    {
        //        this.deedsField = value;
        //    }
        //}

        ///// <remarks/>

        //public string defaultData
        //{
        //    get
        //    {
        //        return this.defaultDataField;
        //    }
        //    set
        //    {
        //        this.defaultDataField = value;
        //    }
        //}

        /// <remarks/>

        //public string empiricaExclusionCode
        //{
        //    get
        //    {
        //        return this.empiricaExclusionCodeField;
        //    }
        //    set
        //    {
        //        this.empiricaExclusionCodeField = value;
        //    }
        //}

        /// <remarks/>

        public string empiricaExclusionDescription
        {
            get
            {
                return this.empiricaExclusionDescriptionField;
            }
            set
            {
                this.empiricaExclusionDescriptionField = value;
            }
        }

        /// <remarks/>

        // // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] empiricaReasonCode
        {
            get
            {
                return this.empiricaReasonCodeField;
            }
            set
            {
                this.empiricaReasonCodeField = value;
            }
        }

        /// <remarks/>

        // // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        //public string[] empiricaReasonDescription
        //{
        //    get
        //    {
        //        return this.empiricaReasonDescriptionField;
        //    }
        //    set
        //    {
        //        this.empiricaReasonDescriptionField = value;
        //    }
        //}

        /// <remarks/>

        public string empiricaScore
        {
            get
            {
                return this.empiricaScoreField;
            }
            set
            {
                this.empiricaScoreField = value;
            }
        }

        /// <remarks/>

        public string forename1
        {
            get
            {
                return this.forename1Field;
            }
            set
            {
                this.forename1Field = value;
            }
        }

        /// <remarks/>

        public string forename2
        {
            get
            {
                return this.forename2Field;
            }
            set
            {
                this.forename2Field = value;
            }
        }

        /// <remarks/>

        //public string forename3
        //{
        //    get
        //    {
        //        return this.forename3Field;
        //    }
        //    set
        //    {
        //        this.forename3Field = value;
        //    }
        //}

        /// <remarks/>

        public string iDNumber
        {
            get
            {
                return this.iDNumberField;
            }
            set
            {
                this.iDNumberField = value;
            }
        }

        /// <remarks/>

        //public string infoDate
        //{
        //    get
        //    {
        //        return this.infoDateField;
        //    }
        //    set
        //    {
        //        this.infoDateField = value;
        //    }
        //}

        /// <remarks/>

        public int majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        //public string noticesOrNotarialBonds
        //{
        //    get
        //    {
        //        return this.noticesOrNotarialBondsField;
        //    }
        //    set
        //    {
        //        this.noticesOrNotarialBondsField = value;
        //    }
        //}

        /// <remarks/>

        public string position
        {
            get
            {
                return this.positionField;
            }
            set
            {
                this.positionField = value;
            }
        }

        /// <remarks/>

        public string surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        public string PrincipalScore
        {
            get
            {
                return this.principalScoreField;
            }
            set
            {
                this.principalScoreField = value;
            }
        }

        public PrincipleCharacterstics[] PrincipleVariables
        {
            get { return this.principleVariablesField; }
            set { this.principleVariablesField = value; }
        }

        public double Affordability
        {
            get { return this.affordabilityField; }
            set { this.affordabilityField = value; }
        }

        public double PredictedIncome
        {
            get { return this.predictedIncomeField; }
            set { this.predictedIncomeField = value; }
        }

        public int IDMatch
        {
            get { return this.idMatchField; }
            set { this.idMatchField = value; }
        }

        public ExclusionRule[] ExclusionRules
        {
            get { return this.exclusionRulesField; }
            set { this.exclusionRulesField = value; }
        }

        public int consumerNo
        {
            get
            {
                return this.consumerNoField;
            }
            set
            {
                this.consumerNoField = value;
            }
        }
    }

    /// <remarks/>





    public partial class PrincipalDetailEmpirica : BureauSegment
    {

        private string assetNotarialBondField;

        private string civilCourtRecordField;

        private string commentField;

        private string confirmedByRegistrarField;

        private string dateDisputedField;

        private string dateOfBirthField;

        private string dateStartedField;

        private string debtCouncilDateField;

        private string debtCouncilDescField;

        private string deedsField;

        private string defaultDataField;

        private string empiricaExclusionCodeField;

        private string empiricaExclusionDescriptionField;

        private string[] empiricaReasonCodeField;

        private string[] empiricaReasonDescriptionField;

        private string empiricaScoreField;

        private string forename1Field;

        private string forename2Field;

        private string forename3Field;

        private string iDNumberField;

        private string infoDateField;

        private string majorProductField;

        private string noticesOrNotarialBondsField;

        private string positionField;

        private string surnameField;

        /// <remarks/>

        public string assetNotarialBond
        {
            get
            {
                return this.assetNotarialBondField;
            }
            set
            {
                this.assetNotarialBondField = value;
            }
        }

        /// <remarks/>

        public string civilCourtRecord
        {
            get
            {
                return this.civilCourtRecordField;
            }
            set
            {
                this.civilCourtRecordField = value;
            }
        }

        /// <remarks/>

        public string comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>

        public string confirmedByRegistrar
        {
            get
            {
                return this.confirmedByRegistrarField;
            }
            set
            {
                this.confirmedByRegistrarField = value;
            }
        }

        /// <remarks/>

        public string dateDisputed
        {
            get
            {
                return this.dateDisputedField;
            }
            set
            {
                this.dateDisputedField = value;
            }
        }

        /// <remarks/>

        public string dateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>

        public string dateStarted
        {
            get
            {
                return this.dateStartedField;
            }
            set
            {
                this.dateStartedField = value;
            }
        }

        /// <remarks/>

        public string debtCouncilDate
        {
            get
            {
                return this.debtCouncilDateField;
            }
            set
            {
                this.debtCouncilDateField = value;
            }
        }

        /// <remarks/>

        public string debtCouncilDesc
        {
            get
            {
                return this.debtCouncilDescField;
            }
            set
            {
                this.debtCouncilDescField = value;
            }
        }

        /// <remarks/>

        public string deeds
        {
            get
            {
                return this.deedsField;
            }
            set
            {
                this.deedsField = value;
            }
        }

        /// <remarks/>

        public string defaultData
        {
            get
            {
                return this.defaultDataField;
            }
            set
            {
                this.defaultDataField = value;
            }
        }

        /// <remarks/>

        public string empiricaExclusionCode
        {
            get
            {
                return this.empiricaExclusionCodeField;
            }
            set
            {
                this.empiricaExclusionCodeField = value;
            }
        }

        /// <remarks/>

        public string empiricaExclusionDescription
        {
            get
            {
                return this.empiricaExclusionDescriptionField;
            }
            set
            {
                this.empiricaExclusionDescriptionField = value;
            }
        }

        /// <remarks/>

        //// [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] empiricaReasonCode
        {
            get
            {
                return this.empiricaReasonCodeField;
            }
            set
            {
                this.empiricaReasonCodeField = value;
            }
        }

        /// <remarks/>

        // // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] empiricaReasonDescription
        {
            get
            {
                return this.empiricaReasonDescriptionField;
            }
            set
            {
                this.empiricaReasonDescriptionField = value;
            }
        }

        /// <remarks/>

        public string empiricaScore
        {
            get
            {
                return this.empiricaScoreField;
            }
            set
            {
                this.empiricaScoreField = value;
            }
        }

        /// <remarks/>

        public string forename1
        {
            get
            {
                return this.forename1Field;
            }
            set
            {
                this.forename1Field = value;
            }
        }

        /// <remarks/>

        public string forename2
        {
            get
            {
                return this.forename2Field;
            }
            set
            {
                this.forename2Field = value;
            }
        }

        /// <remarks/>

        public string forename3
        {
            get
            {
                return this.forename3Field;
            }
            set
            {
                this.forename3Field = value;
            }
        }

        /// <remarks/>

        public string iDNumber
        {
            get
            {
                return this.iDNumberField;
            }
            set
            {
                this.iDNumberField = value;
            }
        }

        /// <remarks/>

        public string infoDate
        {
            get
            {
                return this.infoDateField;
            }
            set
            {
                this.infoDateField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string noticesOrNotarialBonds
        {
            get
            {
                return this.noticesOrNotarialBondsField;
            }
            set
            {
                this.noticesOrNotarialBondsField = value;
            }
        }

        /// <remarks/>

        public string position
        {
            get
            {
                return this.positionField;
            }
            set
            {
                this.positionField = value;
            }
        }

        /// <remarks/>

        public string surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }
    }

    /// <remarks/>





    public partial class PrincipalNotarialBonds : BureauSegment
    {

        private string bondAmountField;

        private string bondNumberField;

        private string bondOwnerField;

        private string bondPercentageField;

        private string bondRegDateField;

        private string bondTypeField;

        private string deedsOfficeField;

        private string hOAboveThresholdField;

        private string[] heldOverField;

        private string majorProductField;

        private string messageField;

        private string microfilemRefField;

        private string multipleOwnersField;

        private string ownerIDField;

        private string ownerTypeField;

        private string parentChildIndField;

        /// <remarks/>

        public string bondAmount
        {
            get
            {
                return this.bondAmountField;
            }
            set
            {
                this.bondAmountField = value;
            }
        }

        /// <remarks/>

        public string bondNumber
        {
            get
            {
                return this.bondNumberField;
            }
            set
            {
                this.bondNumberField = value;
            }
        }

        /// <remarks/>

        public string bondOwner
        {
            get
            {
                return this.bondOwnerField;
            }
            set
            {
                this.bondOwnerField = value;
            }
        }

        /// <remarks/>

        public string bondPercentage
        {
            get
            {
                return this.bondPercentageField;
            }
            set
            {
                this.bondPercentageField = value;
            }
        }

        /// <remarks/>

        public string bondRegDate
        {
            get
            {
                return this.bondRegDateField;
            }
            set
            {
                this.bondRegDateField = value;
            }
        }

        /// <remarks/>

        public string bondType
        {
            get
            {
                return this.bondTypeField;
            }
            set
            {
                this.bondTypeField = value;
            }
        }

        /// <remarks/>

        public string deedsOffice
        {
            get
            {
                return this.deedsOfficeField;
            }
            set
            {
                this.deedsOfficeField = value;
            }
        }

        /// <remarks/>

        public string hOAboveThreshold
        {
            get
            {
                return this.hOAboveThresholdField;
            }
            set
            {
                this.hOAboveThresholdField = value;
            }
        }

        /// <remarks/>

        //// [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] heldOver
        {
            get
            {
                return this.heldOverField;
            }
            set
            {
                this.heldOverField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }

        /// <remarks/>

        public string microfilemRef
        {
            get
            {
                return this.microfilemRefField;
            }
            set
            {
                this.microfilemRefField = value;
            }
        }

        /// <remarks/>

        public string multipleOwners
        {
            get
            {
                return this.multipleOwnersField;
            }
            set
            {
                this.multipleOwnersField = value;
            }
        }

        /// <remarks/>

        public string ownerID
        {
            get
            {
                return this.ownerIDField;
            }
            set
            {
                this.ownerIDField = value;
            }
        }

        /// <remarks/>

        public string ownerType
        {
            get
            {
                return this.ownerTypeField;
            }
            set
            {
                this.ownerTypeField = value;
            }
        }

        /// <remarks/>

        public string parentChildInd
        {
            get
            {
                return this.parentChildIndField;
            }
            set
            {
                this.parentChildIndField = value;
            }
        }
    }

    /// <remarks/>





    public partial class LinkedCompany : BureauSegment
    {

        private string[] businessNameField;

        private string[] businessStatusField;

        private DateTime[] registeredDateField;

        /// <remarks/>

        // // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] businessName
        {
            get
            {
                return this.businessNameField;
            }
            set
            {
                this.businessNameField = value;
            }
        }

        /// <remarks/>

        // // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] businessStatus
        {
            get
            {
                return this.businessStatusField;
            }
            set
            {
                this.businessStatusField = value;
            }
        }

        /// <remarks/>

        // // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public DateTime[] registeredDate
        {
            get
            {
                return this.registeredDateField;
            }
            set
            {
                this.registeredDateField = value;
            }
        }
    }

    /// <remarks/>





    public partial class LinkedBusinessDefault : BureauSegment
    {

        private int[] adverseCountField;

        private string[] iTNumberField;

        private int[] judgementCountField;

        private string moduleProductField;

        private string[] registrationNumberField;

        /// <remarks/>

        //    // // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public int[] adverseCount
        {
            get
            {
                return this.adverseCountField;
            }
            set
            {
                this.adverseCountField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] iTNumber
        {
            get
            {
                return this.iTNumberField;
            }
            set
            {
                this.iTNumberField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public int[] judgementCount
        {
            get
            {
                return this.judgementCountField;
            }
            set
            {
                this.judgementCountField = value;
            }
        }

        /// <remarks/>

        public string moduleProduct
        {
            get
            {
                return this.moduleProductField;
            }
            set
            {
                this.moduleProductField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] registrationNumber
        {
            get
            {
                return this.registrationNumberField;
            }
            set
            {
                this.registrationNumberField = value;
            }
        }
    }

    /// <remarks/>





    //public partial class PrincipalLink : BureauSegment
    //{

    //    private string consumerNoField;

    //    private string dateOfBirthField;

    //    private Established establishedField;

    //    private string forename1Field;

    //    private string forename2Field;

    //    private string forename3Field;

    //    private string iDNumberField;

    //    private LinkedBusinessDefault[] linkedBusinessDefaultsField;

    //    private LinkedCompany[] linkedCompaniesField;

    //    private string majorProductField;

    //    private string surnameField;

    //    /// <remarks/>

    //    public string consumerNo
    //    {
    //        get
    //        {
    //            return this.consumerNoField;
    //        }
    //        set
    //        {
    //            this.consumerNoField = value;
    //        }
    //    }

    //    /// <remarks/>

    //    public string dateOfBirth
    //    {
    //        get
    //        {
    //            return this.dateOfBirthField;
    //        }
    //        set
    //        {
    //            this.dateOfBirthField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public Established established
    //    {
    //        get
    //        {
    //            return this.establishedField;
    //        }
    //        set
    //        {
    //            this.establishedField = value;
    //        }
    //    }

    //    /// <remarks/>

    //    public string forename1
    //    {
    //        get
    //        {
    //            return this.forename1Field;
    //        }
    //        set
    //        {
    //            this.forename1Field = value;
    //        }
    //    }

    //    /// <remarks/>

    //    public string forename2
    //    {
    //        get
    //        {
    //            return this.forename2Field;
    //        }
    //        set
    //        {
    //            this.forename2Field = value;
    //        }
    //    }

    //    /// <remarks/>

    //    public string forename3
    //    {
    //        get
    //        {
    //            return this.forename3Field;
    //        }
    //        set
    //        {
    //            this.forename3Field = value;
    //        }
    //    }

    //    /// <remarks/>

    //    public string iDNumber
    //    {
    //        get
    //        {
    //            return this.iDNumberField;
    //        }
    //        set
    //        {
    //            this.iDNumberField = value;
    //        }
    //    }

    //    /// <remarks/>

    //    public LinkedBusinessDefault[] linkedBusinessDefaults
    //    {
    //        get
    //        {
    //            return this.linkedBusinessDefaultsField;
    //        }
    //        set
    //        {
    //            this.linkedBusinessDefaultsField = value;
    //        }
    //    }

    //    /// <remarks/>

    //    public LinkedCompany[] linkedCompanies
    //    {
    //        get
    //        {
    //            return this.linkedCompaniesField;
    //        }
    //        set
    //        {
    //            this.linkedCompaniesField = value;
    //        }
    //    }

    //    /// <remarks/>

    //    public string majorProduct
    //    {
    //        get
    //        {
    //            return this.majorProductField;
    //        }
    //        set
    //        {
    //            this.majorProductField = value;
    //        }
    //    }

    //    /// <remarks/>

    //    public string surname
    //    {
    //        get
    //        {
    //            return this.surnameField;
    //        }
    //        set
    //        {
    //            this.surnameField = value;
    //        }
    //    }
    //}

    /// <remarks/>



    public enum Established
    {

        /// <remarks/>
        No,

        /// <remarks/>
        Yes,
    }

    /// <remarks/>





    public partial class PrincipalIDV : BureauSegment
    {

        private string forename1Field;

        private string forename2Field;

        private string hawkCodeField;

        private string hawkDescriptionField;

        private string hawkNumberField;

        private string iDDescriptionField;

        private string iDNumberField;

        private string iDVerifiedCodeField;

        private string oldIDDescriptionField;

        private string oldIDWarningField;

        private string partField;

        private string partSequenceField;

        private string recordSequenceField;

        private string subscriberNameField;

        private string subscriberNumberField;

        private string subscriberReferenceField;

        private string surnameField;

        private string ticketNumberField;

        /// <remarks/>

        public string forename1
        {
            get
            {
                return this.forename1Field;
            }
            set
            {
                this.forename1Field = value;
            }
        }

        /// <remarks/>

        public string forename2
        {
            get
            {
                return this.forename2Field;
            }
            set
            {
                this.forename2Field = value;
            }
        }

        /// <remarks/>

        public string hawkCode
        {
            get
            {
                return this.hawkCodeField;
            }
            set
            {
                this.hawkCodeField = value;
            }
        }

        /// <remarks/>

        public string hawkDescription
        {
            get
            {
                return this.hawkDescriptionField;
            }
            set
            {
                this.hawkDescriptionField = value;
            }
        }

        /// <remarks/>

        public string hawkNumber
        {
            get
            {
                return this.hawkNumberField;
            }
            set
            {
                this.hawkNumberField = value;
            }
        }

        /// <remarks/>

        public string iDDescription
        {
            get
            {
                return this.iDDescriptionField;
            }
            set
            {
                this.iDDescriptionField = value;
            }
        }

        /// <remarks/>

        public string iDNumber
        {
            get
            {
                return this.iDNumberField;
            }
            set
            {
                this.iDNumberField = value;
            }
        }

        /// <remarks/>

        public string iDVerifiedCode
        {
            get
            {
                return this.iDVerifiedCodeField;
            }
            set
            {
                this.iDVerifiedCodeField = value;
            }
        }

        /// <remarks/>

        public string oldIDDescription
        {
            get
            {
                return this.oldIDDescriptionField;
            }
            set
            {
                this.oldIDDescriptionField = value;
            }
        }

        /// <remarks/>

        public string oldIDWarning
        {
            get
            {
                return this.oldIDWarningField;
            }
            set
            {
                this.oldIDWarningField = value;
            }
        }

        /// <remarks/>

        public string part
        {
            get
            {
                return this.partField;
            }
            set
            {
                this.partField = value;
            }
        }

        /// <remarks/>

        public string partSequence
        {
            get
            {
                return this.partSequenceField;
            }
            set
            {
                this.partSequenceField = value;
            }
        }

        /// <remarks/>

        public string recordSequence
        {
            get
            {
                return this.recordSequenceField;
            }
            set
            {
                this.recordSequenceField = value;
            }
        }

        /// <remarks/>

        public string subscriberName
        {
            get
            {
                return this.subscriberNameField;
            }
            set
            {
                this.subscriberNameField = value;
            }
        }

        /// <remarks/>

        public string subscriberNumber
        {
            get
            {
                return this.subscriberNumberField;
            }
            set
            {
                this.subscriberNumberField = value;
            }
        }

        /// <remarks/>

        public string subscriberReference
        {
            get
            {
                return this.subscriberReferenceField;
            }
            set
            {
                this.subscriberReferenceField = value;
            }
        }

        /// <remarks/>

        public string surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>

        public string ticketNumber
        {
            get
            {
                return this.ticketNumberField;
            }
            set
            {
                this.ticketNumberField = value;
            }
        }
    }

    /// <remarks/>





    public partial class PrincipalFirstResponse : BureauSegment
    {

        private string consumerNumberField;

        private string principalNameField;

        private string ticketNumberField;

        /// <remarks/>

        public string consumerNumber
        {
            get
            {
                return this.consumerNumberField;
            }
            set
            {
                this.consumerNumberField = value;
            }
        }

        /// <remarks/>

        public string principalName
        {
            get
            {
                return this.principalNameField;
            }
            set
            {
                this.principalNameField = value;
            }
        }

        /// <remarks/>

        public string ticketNumber
        {
            get
            {
                return this.ticketNumberField;
            }
            set
            {
                this.ticketNumberField = value;
            }
        }
    }

    /// <remarks/>





    public partial class PrincipalDeedsSummaryP8 : BureauSegment
    {

        private string bondAmountField;

        private string bondFreeAmountField;

        private string businessNameField;

        private string dateField;

        private string dateOfBirthOrIDNumberField;

        private string deedsOfficeField;

        private string majorProductField;

        private string messageField;

        private string numberOfPropertiesField;

        private string principalNameField;

        private string purchasePriceField;

        private string registrationNumberField;

        /// <remarks/>

        public string bondAmount
        {
            get
            {
                return this.bondAmountField;
            }
            set
            {
                this.bondAmountField = value;
            }
        }

        /// <remarks/>

        public string bondFreeAmount
        {
            get
            {
                return this.bondFreeAmountField;
            }
            set
            {
                this.bondFreeAmountField = value;
            }
        }

        /// <remarks/>

        public string businessName
        {
            get
            {
                return this.businessNameField;
            }
            set
            {
                this.businessNameField = value;
            }
        }

        /// <remarks/>

        public string date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>

        public string dateOfBirthOrIDNumber
        {
            get
            {
                return this.dateOfBirthOrIDNumberField;
            }
            set
            {
                this.dateOfBirthOrIDNumberField = value;
            }
        }

        /// <remarks/>

        public string deedsOffice
        {
            get
            {
                return this.deedsOfficeField;
            }
            set
            {
                this.deedsOfficeField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }

        /// <remarks/>

        public string numberOfProperties
        {
            get
            {
                return this.numberOfPropertiesField;
            }
            set
            {
                this.numberOfPropertiesField = value;
            }
        }

        /// <remarks/>

        public string principalName
        {
            get
            {
                return this.principalNameField;
            }
            set
            {
                this.principalNameField = value;
            }
        }

        /// <remarks/>

        public string purchasePrice
        {
            get
            {
                return this.purchasePriceField;
            }
            set
            {
                this.purchasePriceField = value;
            }
        }

        /// <remarks/>

        public string registrationNumber
        {
            get
            {
                return this.registrationNumberField;
            }
            set
            {
                this.registrationNumberField = value;
            }
        }
    }

    /// <remarks/>





    public partial class PrincipalDeedsSummaryCO : BureauSegment
    {

        private string bondAmountField;

        private string bondFreeAmountField;

        private string dateOfBirthOrIDNumberField;

        private string deedsDateField;

        private string deedsOfficeField;

        private string majorProductField;

        private string numberOfPropertiesField;

        private string principalNameField;

        private string principalSurnameField;

        private string purchasePriceField;

        /// <remarks/>

        public string bondAmount
        {
            get
            {
                return this.bondAmountField;
            }
            set
            {
                this.bondAmountField = value;
            }
        }

        /// <remarks/>

        public string bondFreeAmount
        {
            get
            {
                return this.bondFreeAmountField;
            }
            set
            {
                this.bondFreeAmountField = value;
            }
        }

        /// <remarks/>

        public string dateOfBirthOrIDNumber
        {
            get
            {
                return this.dateOfBirthOrIDNumberField;
            }
            set
            {
                this.dateOfBirthOrIDNumberField = value;
            }
        }

        /// <remarks/>

        public string deedsDate
        {
            get
            {
                return this.deedsDateField;
            }
            set
            {
                this.deedsDateField = value;
            }
        }

        /// <remarks/>

        public string deedsOffice
        {
            get
            {
                return this.deedsOfficeField;
            }
            set
            {
                this.deedsOfficeField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string numberOfProperties
        {
            get
            {
                return this.numberOfPropertiesField;
            }
            set
            {
                this.numberOfPropertiesField = value;
            }
        }

        /// <remarks/>

        public string principalName
        {
            get
            {
                return this.principalNameField;
            }
            set
            {
                this.principalNameField = value;
            }
        }

        /// <remarks/>

        public string principalSurname
        {
            get
            {
                return this.principalSurnameField;
            }
            set
            {
                this.principalSurnameField = value;
            }
        }

        /// <remarks/>

        public string purchasePrice
        {
            get
            {
                return this.purchasePriceField;
            }
            set
            {
                this.purchasePriceField = value;
            }
        }
    }

    /// <remarks/>





    public partial class PrincipalDeedsComprehensiveCV : BureauSegment
    {

        private string bondAmountField;

        private string bondDateField;

        private string bondHolderField;

        private string bondNumberField;

        private string commentField;

        private string dateField;

        private string dateOfBirthOrIDNumberField;

        private string deedsOfficeField;

        private string eRFField;

        private string farmField;

        private string majorProductField;

        private string multipleOwnersField;

        private string portionField;

        private string propertyNameField;

        private string propertySizeField;

        private string propertyTypeField;

        private string purchaseDateField;

        private string purchasePriceField;

        private string rowIDField;

        private string schemeNameField;

        private string schemeNumberField;

        private string shareField;

        private string titleField;

        private string townshipField;

        /// <remarks/>

        public string bondAmount
        {
            get
            {
                return this.bondAmountField;
            }
            set
            {
                this.bondAmountField = value;
            }
        }

        /// <remarks/>

        public string bondDate
        {
            get
            {
                return this.bondDateField;
            }
            set
            {
                this.bondDateField = value;
            }
        }

        /// <remarks/>

        public string bondHolder
        {
            get
            {
                return this.bondHolderField;
            }
            set
            {
                this.bondHolderField = value;
            }
        }

        /// <remarks/>

        public string bondNumber
        {
            get
            {
                return this.bondNumberField;
            }
            set
            {
                this.bondNumberField = value;
            }
        }

        /// <remarks/>

        public string comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>

        public string date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>

        public string dateOfBirthOrIDNumber
        {
            get
            {
                return this.dateOfBirthOrIDNumberField;
            }
            set
            {
                this.dateOfBirthOrIDNumberField = value;
            }
        }

        /// <remarks/>

        public string deedsOffice
        {
            get
            {
                return this.deedsOfficeField;
            }
            set
            {
                this.deedsOfficeField = value;
            }
        }

        /// <remarks/>

        public string eRF
        {
            get
            {
                return this.eRFField;
            }
            set
            {
                this.eRFField = value;
            }
        }

        /// <remarks/>

        public string farm
        {
            get
            {
                return this.farmField;
            }
            set
            {
                this.farmField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string multipleOwners
        {
            get
            {
                return this.multipleOwnersField;
            }
            set
            {
                this.multipleOwnersField = value;
            }
        }

        /// <remarks/>

        public string portion
        {
            get
            {
                return this.portionField;
            }
            set
            {
                this.portionField = value;
            }
        }

        /// <remarks/>

        public string propertyName
        {
            get
            {
                return this.propertyNameField;
            }
            set
            {
                this.propertyNameField = value;
            }
        }

        /// <remarks/>

        public string propertySize
        {
            get
            {
                return this.propertySizeField;
            }
            set
            {
                this.propertySizeField = value;
            }
        }

        /// <remarks/>

        public string propertyType
        {
            get
            {
                return this.propertyTypeField;
            }
            set
            {
                this.propertyTypeField = value;
            }
        }

        /// <remarks/>

        public string purchaseDate
        {
            get
            {
                return this.purchaseDateField;
            }
            set
            {
                this.purchaseDateField = value;
            }
        }

        /// <remarks/>

        public string purchasePrice
        {
            get
            {
                return this.purchasePriceField;
            }
            set
            {
                this.purchasePriceField = value;
            }
        }

        /// <remarks/>

        public string rowID
        {
            get
            {
                return this.rowIDField;
            }
            set
            {
                this.rowIDField = value;
            }
        }

        /// <remarks/>

        public string schemeName
        {
            get
            {
                return this.schemeNameField;
            }
            set
            {
                this.schemeNameField = value;
            }
        }

        /// <remarks/>

        public string schemeNumber
        {
            get
            {
                return this.schemeNumberField;
            }
            set
            {
                this.schemeNumberField = value;
            }
        }

        /// <remarks/>

        public string share
        {
            get
            {
                return this.shareField;
            }
            set
            {
                this.shareField = value;
            }
        }

        /// <remarks/>

        public string title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>

        public string township
        {
            get
            {
                return this.townshipField;
            }
            set
            {
                this.townshipField = value;
            }
        }
    }

    /// <remarks/>





    public partial class PrincipalDeedsComprehensiveCA : BureauSegment
    {

        private string bondAmountField;

        private string bondDateField;

        private string bondHolderField;

        private string bondNumberField;

        private string commentField;

        private string dateField;

        private string dateOfBirthOrIDNumberField;

        private string deedsOfficeField;

        private string eRFField;

        private string farmField;

        private string majorProductField;

        private string multipleOwnersField;

        private string portionField;

        private string propertyNameField;

        private string propertySizeField;

        private string propertyTypeField;

        private string provinceField;

        private string purchaseDateField;

        private string purchasePriceField;

        private string rowIDField;

        private string schemeNameField;

        private string schemeNumberField;

        private string shareField;

        private string streetField;

        private string titleField;

        private string townshipField;

        /// <remarks/>

        public string bondAmount
        {
            get
            {
                return this.bondAmountField;
            }
            set
            {
                this.bondAmountField = value;
            }
        }

        /// <remarks/>

        public string bondDate
        {
            get
            {
                return this.bondDateField;
            }
            set
            {
                this.bondDateField = value;
            }
        }

        /// <remarks/>

        public string bondHolder
        {
            get
            {
                return this.bondHolderField;
            }
            set
            {
                this.bondHolderField = value;
            }
        }

        /// <remarks/>

        public string bondNumber
        {
            get
            {
                return this.bondNumberField;
            }
            set
            {
                this.bondNumberField = value;
            }
        }

        /// <remarks/>

        public string comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>

        public string date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>

        public string dateOfBirthOrIDNumber
        {
            get
            {
                return this.dateOfBirthOrIDNumberField;
            }
            set
            {
                this.dateOfBirthOrIDNumberField = value;
            }
        }

        /// <remarks/>

        public string deedsOffice
        {
            get
            {
                return this.deedsOfficeField;
            }
            set
            {
                this.deedsOfficeField = value;
            }
        }

        /// <remarks/>

        public string eRF
        {
            get
            {
                return this.eRFField;
            }
            set
            {
                this.eRFField = value;
            }
        }

        /// <remarks/>

        public string farm
        {
            get
            {
                return this.farmField;
            }
            set
            {
                this.farmField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string multipleOwners
        {
            get
            {
                return this.multipleOwnersField;
            }
            set
            {
                this.multipleOwnersField = value;
            }
        }

        /// <remarks/>

        public string portion
        {
            get
            {
                return this.portionField;
            }
            set
            {
                this.portionField = value;
            }
        }

        /// <remarks/>

        public string propertyName
        {
            get
            {
                return this.propertyNameField;
            }
            set
            {
                this.propertyNameField = value;
            }
        }

        /// <remarks/>

        public string propertySize
        {
            get
            {
                return this.propertySizeField;
            }
            set
            {
                this.propertySizeField = value;
            }
        }

        /// <remarks/>

        public string propertyType
        {
            get
            {
                return this.propertyTypeField;
            }
            set
            {
                this.propertyTypeField = value;
            }
        }

        /// <remarks/>

        public string province
        {
            get
            {
                return this.provinceField;
            }
            set
            {
                this.provinceField = value;
            }
        }

        /// <remarks/>

        public string purchaseDate
        {
            get
            {
                return this.purchaseDateField;
            }
            set
            {
                this.purchaseDateField = value;
            }
        }

        /// <remarks/>

        public string purchasePrice
        {
            get
            {
                return this.purchasePriceField;
            }
            set
            {
                this.purchasePriceField = value;
            }
        }

        /// <remarks/>

        public string rowID
        {
            get
            {
                return this.rowIDField;
            }
            set
            {
                this.rowIDField = value;
            }
        }

        /// <remarks/>

        public string schemeName
        {
            get
            {
                return this.schemeNameField;
            }
            set
            {
                this.schemeNameField = value;
            }
        }

        /// <remarks/>

        public string schemeNumber
        {
            get
            {
                return this.schemeNumberField;
            }
            set
            {
                this.schemeNumberField = value;
            }
        }

        /// <remarks/>

        public string share
        {
            get
            {
                return this.shareField;
            }
            set
            {
                this.shareField = value;
            }
        }

        /// <remarks/>

        public string street
        {
            get
            {
                return this.streetField;
            }
            set
            {
                this.streetField = value;
            }
        }

        /// <remarks/>

        public string title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>

        public string township
        {
            get
            {
                return this.townshipField;
            }
            set
            {
                this.townshipField = value;
            }
        }
    }

    /// <remarks/>





    public partial class PrincipalClearance : BureauSegment
    {

        private string civilCourtCountField;

        private string dateOfBirthField;

        private string debtCouncilDateField;

        private string debtCouncilDescField;

        private string defaultCountField;

        private string disputeDateField;

        private Established establishedField;

        private string forename1Field;

        private string forename2Field;

        private string forename3Field;

        private string iDNumberField;

        private string majorProductField;

        private string noticeNotarialCountField;

        private string surnameField;

        /// <remarks/>

        public string civilCourtCount
        {
            get
            {
                return this.civilCourtCountField;
            }
            set
            {
                this.civilCourtCountField = value;
            }
        }

        /// <remarks/>

        public string dateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>

        public string debtCouncilDate
        {
            get
            {
                return this.debtCouncilDateField;
            }
            set
            {
                this.debtCouncilDateField = value;
            }
        }

        /// <remarks/>

        public string debtCouncilDesc
        {
            get
            {
                return this.debtCouncilDescField;
            }
            set
            {
                this.debtCouncilDescField = value;
            }
        }

        /// <remarks/>

        public string defaultCount
        {
            get
            {
                return this.defaultCountField;
            }
            set
            {
                this.defaultCountField = value;
            }
        }

        /// <remarks/>

        public string disputeDate
        {
            get
            {
                return this.disputeDateField;
            }
            set
            {
                this.disputeDateField = value;
            }
        }

        /// <remarks/>
        public Established established
        {
            get
            {
                return this.establishedField;
            }
            set
            {
                this.establishedField = value;
            }
        }

        /// <remarks/>

        public string forename1
        {
            get
            {
                return this.forename1Field;
            }
            set
            {
                this.forename1Field = value;
            }
        }

        /// <remarks/>

        public string forename2
        {
            get
            {
                return this.forename2Field;
            }
            set
            {
                this.forename2Field = value;
            }
        }

        /// <remarks/>

        public string forename3
        {
            get
            {
                return this.forename3Field;
            }
            set
            {
                this.forename3Field = value;
            }
        }

        /// <remarks/>

        public string iDNumber
        {
            get
            {
                return this.iDNumberField;
            }
            set
            {
                this.iDNumberField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string noticeNotarialCount
        {
            get
            {
                return this.noticeNotarialCountField;
            }
            set
            {
                this.noticeNotarialCountField = value;
            }
        }

        /// <remarks/>

        public string surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }
    }

    /// <remarks/>





    public partial class PrincipalArchiveP5 : BureauSegment
    {

        private string dateOfBirthField;

        private string dateResignedField;

        private string dateStartedField;

        private string iDNumberField;

        private string initialsField;

        private string majorProductField;

        private string positionField;

        private string registrationNumberField;

        private string statusField;

        private string surnameField;

        /// <remarks/>

        public string dateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>

        public string dateResigned
        {
            get
            {
                return this.dateResignedField;
            }
            set
            {
                this.dateResignedField = value;
            }
        }

        /// <remarks/>

        public string dateStarted
        {
            get
            {
                return this.dateStartedField;
            }
            set
            {
                this.dateStartedField = value;
            }
        }

        /// <remarks/>

        public string iDNumber
        {
            get
            {
                return this.iDNumberField;
            }
            set
            {
                this.iDNumberField = value;
            }
        }

        /// <remarks/>

        public string initials
        {
            get
            {
                return this.initialsField;
            }
            set
            {
                this.initialsField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string position
        {
            get
            {
                return this.positionField;
            }
            set
            {
                this.positionField = value;
            }
        }

        /// <remarks/>

        public string registrationNumber
        {
            get
            {
                return this.registrationNumberField;
            }
            set
            {
                this.registrationNumberField = value;
            }
        }

        /// <remarks/>

        public string status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>

        public string surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }
    }

    /// <remarks/>





    public partial class PrincipalArchive : BureauSegment
    {

        private string businessNameField;

        private string dateOfBirthField;

        private string dateResignedField;

        private string dateStartedField;

        private string iDNumberField;

        private string initialsField;

        private string positionField;

        private string registrationNumberField;

        private string surnameField;

        /// <remarks/>

        public string businessName
        {
            get
            {
                return this.businessNameField;
            }
            set
            {
                this.businessNameField = value;
            }
        }

        /// <remarks/>

        public string dateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>

        public string dateResigned
        {
            get
            {
                return this.dateResignedField;
            }
            set
            {
                this.dateResignedField = value;
            }
        }

        /// <remarks/>

        public string dateStarted
        {
            get
            {
                return this.dateStartedField;
            }
            set
            {
                this.dateStartedField = value;
            }
        }

        /// <remarks/>

        public string iDNumber
        {
            get
            {
                return this.iDNumberField;
            }
            set
            {
                this.iDNumberField = value;
            }
        }

        /// <remarks/>

        public string initials
        {
            get
            {
                return this.initialsField;
            }
            set
            {
                this.initialsField = value;
            }
        }

        /// <remarks/>

        public string position
        {
            get
            {
                return this.positionField;
            }
            set
            {
                this.positionField = value;
            }
        }

        /// <remarks/>

        public string registrationNumber
        {
            get
            {
                return this.registrationNumberField;
            }
            set
            {
                this.registrationNumberField = value;
            }
        }

        /// <remarks/>

        public string surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }
    }

    /// <remarks/>





    public partial class OtherOperation : BureauSegment
    {

        private string[] agencyCountryField;

        private string[] agencyNameField;

        private string[] agencyProductField;

        private string[] agentLocationField;

        private string[] agentNumberField;

        private string[] branchLocationField;

        private string[] branchNumberField;

        private string majorProductField;

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] agencyCountry
        {
            get
            {
                return this.agencyCountryField;
            }
            set
            {
                this.agencyCountryField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] agencyName
        {
            get
            {
                return this.agencyNameField;
            }
            set
            {
                this.agencyNameField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] agencyProduct
        {
            get
            {
                return this.agencyProductField;
            }
            set
            {
                this.agencyProductField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] agentLocation
        {
            get
            {
                return this.agentLocationField;
            }
            set
            {
                this.agentLocationField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] agentNumber
        {
            get
            {
                return this.agentNumberField;
            }
            set
            {
                this.agentNumberField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] branchLocation
        {
            get
            {
                return this.branchLocationField;
            }
            set
            {
                this.branchLocationField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] branchNumber
        {
            get
            {
                return this.branchNumberField;
            }
            set
            {
                this.branchNumberField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }
    }

    /// <remarks/>





    public partial class AdditionalOperations : BureauSegment
    {

        private string majorProductField;

        private SicDetails[] sicDetailsField;

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public SicDetails[] sicDetails
        {
            get
            {
                return this.sicDetailsField;
            }
            set
            {
                this.sicDetailsField = value;
            }
        }
    }

    /// <remarks/>





    public partial class Operation : BureauSegment
    {

        private string aveCollPeriodField;

        private string cashSalesField;

        private string cashSalesPercField;

        private string creditSalesField;

        private string creditSalesPercField;

        private DateTime dateField;

        private string divisionalNameField;

        private string exportCommentField;

        private string exportField;

        private string importCommentField;

        private string importField;

        private string leasePeriodFromField;

        private string leasePeriodToField;

        private int majorProductField;

        private decimal numberSalesStaffField;

        private decimal numberWageStaffField;

        private string[] opCommentField;

        private string premisesLessorField;

        private string premisesOwnerField;

        private string premisesRentalField;

        private string premisesSizeField;

        private string sASicCodeField;

        private string sASicDescriptionField;

        private string salesAreaField;

        private string sicCodeField;

        private string sicDescriptionField;

        private string termsFromField;

        private string termsToField;

        private decimal totalNumberStaffField;

        /// <remarks/>

        //public string aveCollPeriod
        //{
        //    get
        //    {
        //        return this.aveCollPeriodField;
        //    }
        //    set
        //    {
        //        this.aveCollPeriodField = value;
        //    }
        //}

        ///// <remarks/>

        //public string cashSales
        //{
        //    get
        //    {
        //        return this.cashSalesField;
        //    }
        //    set
        //    {
        //        this.cashSalesField = value;
        //    }
        //}

        ///// <remarks/>

        //public string cashSalesPerc
        //{
        //    get
        //    {
        //        return this.cashSalesPercField;
        //    }
        //    set
        //    {
        //        this.cashSalesPercField = value;
        //    }
        //}

        ///// <remarks/>

        //public string creditSales
        //{
        //    get
        //    {
        //        return this.creditSalesField;
        //    }
        //    set
        //    {
        //        this.creditSalesField = value;
        //    }
        //}

        ///// <remarks/>

        //public string creditSalesPerc
        //{
        //    get
        //    {
        //        return this.creditSalesPercField;
        //    }
        //    set
        //    {
        //        this.creditSalesPercField = value;
        //    }
        //}

        /// <remarks/>

        public DateTime date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>

        //public string divisionalName
        //{
        //    get
        //    {
        //        return this.divisionalNameField;
        //    }
        //    set
        //    {
        //        this.divisionalNameField = value;
        //    }
        //}

        /// <remarks/>

        public string exportComment
        {
            get
            {
                return this.exportCommentField;
            }
            set
            {
                this.exportCommentField = value;
            }
        }

        /// <remarks/>

        public string export
        {
            get
            {
                return this.exportField;
            }
            set
            {
                this.exportField = value;
            }
        }

        /// <remarks/>

        public string importComment
        {
            get
            {
                return this.importCommentField;
            }
            set
            {
                this.importCommentField = value;
            }
        }

        /// <remarks/>

        public string import
        {
            get
            {
                return this.importField;
            }
            set
            {
                this.importField = value;
            }
        }

        /// <remarks/>

        //public string leasePeriodFrom
        //{
        //    get
        //    {
        //        return this.leasePeriodFromField;
        //    }
        //    set
        //    {
        //        this.leasePeriodFromField = value;
        //    }
        //}

        ///// <remarks/>

        //public string leasePeriodTo
        //{
        //    get
        //    {
        //        return this.leasePeriodToField;
        //    }
        //    set
        //    {
        //        this.leasePeriodToField = value;
        //    }
        //}

        /// <remarks/>

        public int majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public decimal numberSalesStaff
        {
            get
            {
                return this.numberSalesStaffField;
            }
            set
            {
                this.numberSalesStaffField = value;
            }
        }

        /// <remarks/>

        public decimal numberWageStaff
        {
            get
            {
                return this.numberWageStaffField;
            }
            set
            {
                this.numberWageStaffField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        //public string[] opComment
        //{
        //    get
        //    {
        //        return this.opCommentField;
        //    }
        //    set
        //    {
        //        this.opCommentField = value;
        //    }
        //}

        ///// <remarks/>

        //public string premisesLessor
        //{
        //    get
        //    {
        //        return this.premisesLessorField;
        //    }
        //    set
        //    {
        //        this.premisesLessorField = value;
        //    }
        //}

        ///// <remarks/>

        //public string premisesOwner
        //{
        //    get
        //    {
        //        return this.premisesOwnerField;
        //    }
        //    set
        //    {
        //        this.premisesOwnerField = value;
        //    }
        //}

        ///// <remarks/>

        //public string premisesRental
        //{
        //    get
        //    {
        //        return this.premisesRentalField;
        //    }
        //    set
        //    {
        //        this.premisesRentalField = value;
        //    }
        //}

        ///// <remarks/>

        //public string premisesSize
        //{
        //    get
        //    {
        //        return this.premisesSizeField;
        //    }
        //    set
        //    {
        //        this.premisesSizeField = value;
        //    }
        //}

        ///// <remarks/>

        //public string sASicCode
        //{
        //    get
        //    {
        //        return this.sASicCodeField;
        //    }
        //    set
        //    {
        //        this.sASicCodeField = value;
        //    }
        //}

        ///// <remarks/>

        //public string sASicDescription
        //{
        //    get
        //    {
        //        return this.sASicDescriptionField;
        //    }
        //    set
        //    {
        //        this.sASicDescriptionField = value;
        //    }
        //}

        ///// <remarks/>

        //public string salesArea
        //{
        //    get
        //    {
        //        return this.salesAreaField;
        //    }
        //    set
        //    {
        //        this.salesAreaField = value;
        //    }
        //}

        /// <remarks/>

        public string sicCode
        {
            get
            {
                return this.sicCodeField;
            }
            set
            {
                this.sicCodeField = value;
            }
        }

        /// <remarks/>

        public string sicDescription
        {
            get
            {
                return this.sicDescriptionField;
            }
            set
            {
                this.sicDescriptionField = value;
            }
        }

        /// <remarks/>

        //public string termsFrom
        //{
        //    get
        //    {
        //        return this.termsFromField;
        //    }
        //    set
        //    {
        //        this.termsFromField = value;
        //    }
        //}

        ///// <remarks/>

        //public string termsTo
        //{
        //    get
        //    {
        //        return this.termsToField;
        //    }
        //    set
        //    {
        //        this.termsToField = value;
        //    }
        //}

        ///// <remarks/>

        public decimal totalNumberStaff
        {
            get
            {
                return this.totalNumberStaffField;
            }
            set
            {
                this.totalNumberStaffField = value;
            }
        }
    }

    /// <remarks/>





    public partial class Observation : BureauSegment
    {

        private string commentDateField;

        private string[] commentField;

        private string majorProductField;

        /// <remarks/>

        public string commentDate
        {
            get
            {
                return this.commentDateField;
            }
            set
            {
                this.commentDateField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }
    }

    /// <remarks/>





    public partial class ObservationCont : BureauSegment
    {

        private string[] commentField;

        private string majorProductField;

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }
    }

    /// <remarks/>





    public partial class NotarialBond : BureauSegment
    {

        private string addressField;

        private string bondAmountField;

        private string bondDateField;

        private string bondDistrictField;

        private string bondNumberField;

        private string bondPercentField;

        private string cityField;

        private string countryField;

        private string dateCancelledField;

        private string majorProductField;

        private string messageField;

        private string mortgageeField;

        private string mortgagorDistrictField;

        private string mortgagorField;

        private string numberFoundField;

        private string postCodeField;

        private string serialNumberField;

        private string suburbField;

        private string tradeStyleField;

        /// <remarks/>

        public string address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>

        public string bondAmount
        {
            get
            {
                return this.bondAmountField;
            }
            set
            {
                this.bondAmountField = value;
            }
        }

        /// <remarks/>

        public string bondDate
        {
            get
            {
                return this.bondDateField;
            }
            set
            {
                this.bondDateField = value;
            }
        }

        /// <remarks/>

        public string bondDistrict
        {
            get
            {
                return this.bondDistrictField;
            }
            set
            {
                this.bondDistrictField = value;
            }
        }

        /// <remarks/>

        public string bondNumber
        {
            get
            {
                return this.bondNumberField;
            }
            set
            {
                this.bondNumberField = value;
            }
        }

        /// <remarks/>

        public string bondPercent
        {
            get
            {
                return this.bondPercentField;
            }
            set
            {
                this.bondPercentField = value;
            }
        }

        /// <remarks/>

        public string city
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>

        public string country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        /// <remarks/>

        public string dateCancelled
        {
            get
            {
                return this.dateCancelledField;
            }
            set
            {
                this.dateCancelledField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }

        /// <remarks/>

        public string mortgagee
        {
            get
            {
                return this.mortgageeField;
            }
            set
            {
                this.mortgageeField = value;
            }
        }

        /// <remarks/>

        public string mortgagorDistrict
        {
            get
            {
                return this.mortgagorDistrictField;
            }
            set
            {
                this.mortgagorDistrictField = value;
            }
        }

        /// <remarks/>

        public string mortgagor
        {
            get
            {
                return this.mortgagorField;
            }
            set
            {
                this.mortgagorField = value;
            }
        }

        /// <remarks/>

        public string numberFound
        {
            get
            {
                return this.numberFoundField;
            }
            set
            {
                this.numberFoundField = value;
            }
        }

        /// <remarks/>

        public string postCode
        {
            get
            {
                return this.postCodeField;
            }
            set
            {
                this.postCodeField = value;
            }
        }

        /// <remarks/>

        public string serialNumber
        {
            get
            {
                return this.serialNumberField;
            }
            set
            {
                this.serialNumberField = value;
            }
        }

        /// <remarks/>

        public string suburb
        {
            get
            {
                return this.suburbField;
            }
            set
            {
                this.suburbField = value;
            }
        }

        /// <remarks/>

        public string tradeStyle
        {
            get
            {
                return this.tradeStyleField;
            }
            set
            {
                this.tradeStyleField = value;
            }
        }
    }

    /// <remarks/>





    public partial class Names : BureauSegment
    {

        private string aKANameField;

        private string businessNameField;

        private string[] divisionalNamesField;

        private string majorProductField;

        private string[] previousNameDatesField;

        private string[] previousNamesField;

        private string[] tradeStylesField;

        /// <remarks/>

        public string aKAName
        {
            get
            {
                return this.aKANameField;
            }
            set
            {
                this.aKANameField = value;
            }
        }

        /// <remarks/>

        public string businessName
        {
            get
            {
                return this.businessNameField;
            }
            set
            {
                this.businessNameField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        //public string[] divisionalNames
        //{
        //    get
        //    {
        //        return this.divisionalNamesField;
        //    }
        //    set
        //    {
        //        this.divisionalNamesField = value;
        //    }
        //}

        ///// <remarks/>

        //public string majorProduct
        //{
        //    get
        //    {
        //        return this.majorProductField;
        //    }
        //    set
        //    {
        //        this.majorProductField = value;
        //    }
        //}

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] previousNameDates
        {
            get
            {
                return this.previousNameDatesField;
            }
            set
            {
                this.previousNameDatesField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] previousNames
        {
            get
            {
                return this.previousNamesField;
            }
            set
            {
                this.previousNamesField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        //public string[] tradeStyles
        //{
        //    get
        //    {
        //        return this.tradeStylesField;
        //    }
        //    set
        //    {
        //        this.tradeStylesField = value;
        //    }
        //}
    }

    /// <remarks/>





    public partial class ModuleAvailabilityResponse : BureauSegment
    {

        private string iTNumberField;

        private Module[] modulesField;

        /// <remarks/>

        public string iTNumber
        {
            get
            {
                return this.iTNumberField;
            }
            set
            {
                this.iTNumberField = value;
            }
        }

        /// <remarks/>

        public Module[] modules
        {
            get
            {
                return this.modulesField;
            }
            set
            {
                this.modulesField = value;
            }
        }
    }

    /// <remarks/>





    public partial class MailboxRetrieveList : BureauSegment
    {

        private string[] ticketsField;

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] tickets
        {
            get
            {
                return this.ticketsField;
            }
            set
            {
                this.ticketsField = value;
            }
        }
    }

    /// <remarks/>





    public partial class LinkedBusinessHeader : BureauSegment
    {

        private string businessNameField;

        private string consumerNoField;

        private string defaultCounterField;

        private string iTNumberField;

        private string judgementCounterField;

        private string majorProductField;

        private string registeredDateField;

        private string registeredNumberField;

        private string registeredStatusField;

        /// <remarks/>

        public string businessName
        {
            get
            {
                return this.businessNameField;
            }
            set
            {
                this.businessNameField = value;
            }
        }

        /// <remarks/>

        public string consumerNo
        {
            get
            {
                return this.consumerNoField;
            }
            set
            {
                this.consumerNoField = value;
            }
        }

        /// <remarks/>

        public string defaultCounter
        {
            get
            {
                return this.defaultCounterField;
            }
            set
            {
                this.defaultCounterField = value;
            }
        }

        /// <remarks/>

        public string iTNumber
        {
            get
            {
                return this.iTNumberField;
            }
            set
            {
                this.iTNumberField = value;
            }
        }

        /// <remarks/>

        public string judgementCounter
        {
            get
            {
                return this.judgementCounterField;
            }
            set
            {
                this.judgementCounterField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string registeredDate
        {
            get
            {
                return this.registeredDateField;
            }
            set
            {
                this.registeredDateField = value;
            }
        }

        /// <remarks/>

        public string registeredNumber
        {
            get
            {
                return this.registeredNumberField;
            }
            set
            {
                this.registeredNumberField = value;
            }
        }

        /// <remarks/>

        public string registeredStatus
        {
            get
            {
                return this.registeredStatusField;
            }
            set
            {
                this.registeredStatusField = value;
            }
        }
    }

    /// <remarks/>





    public partial class Header : BureauSegment
    {

        private string businessCategoryField;

        private string businessFunctionField;

        private string businessTypeField;

        private string cityField;

        private string countryField;

        private DateTime dateOfHeaderField;

        private string dunsNumberField;

        private string emailField;

        private string faxField;

        private string industryField;

        private int majorProductField;

        private string phoneField;

        private string physicalAddressField;

        private string postCodeField;

        private string postalAddressField;

        private string postalCityField;

        private string postalCountryField;

        private string postalPostCodeField;

        private string postalSuburbField;

        private string purchasePriceField;

        private DateTime startDateField;

        private string startingCapitalField;

        private string suburbField;

        private string taxNumberField;

        private string tradingNumberField;

        private string[] vATNumbersField;

        private string websiteField;

        /// <remarks/>

        //public string businessCategory
        //{
        //    get
        //    {
        //        return this.businessCategoryField;
        //    }
        //    set
        //    {
        //        this.businessCategoryField = value;
        //    }
        //}

        ///// <remarks/>

        //public string businessFunction
        //{
        //    get
        //    {
        //        return this.businessFunctionField;
        //    }
        //    set
        //    {
        //        this.businessFunctionField = value;
        //    }
        //}

        /// <remarks/>

        public string businessType
        {
            get
            {
                return this.businessTypeField;
            }
            set
            {
                this.businessTypeField = value;
            }
        }

        /// <remarks/>

        //public string city
        //{
        //    get
        //    {
        //        return this.cityField;
        //    }
        //    set
        //    {
        //        this.cityField = value;
        //    }
        //}

        ///// <remarks/>

        //public string country
        //{
        //    get
        //    {
        //        return this.countryField;
        //    }
        //    set
        //    {
        //        this.countryField = value;
        //    }
        //}

        /// <remarks/>

        public DateTime dateOfHeader
        {
            get
            {
                return this.dateOfHeaderField;
            }
            set
            {
                this.dateOfHeaderField = value;
            }
        }

        /// <remarks/>

        //public string dunsNumber
        //{
        //    get
        //    {
        //        return this.dunsNumberField;
        //    }
        //    set
        //    {
        //        this.dunsNumberField = value;
        //    }
        //}

        /// <remarks/>

        public string email
        {
            get
            {
                return this.emailField;
            }
            set
            {
                this.emailField = value;
            }
        }

        /// <remarks/>

        public string fax
        {
            get
            {
                return this.faxField;
            }
            set
            {
                this.faxField = value;
            }
        }

        /// <remarks/>

        public string industry
        {
            get
            {
                return this.industryField;
            }
            set
            {
                this.industryField = value;
            }
        }

        /// <remarks/>

        public int majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string phone
        {
            get
            {
                return this.phoneField;
            }
            set
            {
                this.phoneField = value;
            }
        }

        /// <remarks/>

        public string physicalAddress
        {
            get
            {
                return this.physicalAddressField;
            }
            set
            {
                this.physicalAddressField = value;
            }
        }

        /// <remarks/>

        //public string postCode
        //{
        //    get
        //    {
        //        return this.postCodeField;
        //    }
        //    set
        //    {
        //        this.postCodeField = value;
        //    }
        //}

        /// <remarks/>

        public string postalAddress
        {
            get
            {
                return this.postalAddressField;
            }
            set
            {
                this.postalAddressField = value;
            }
        }

        /// <remarks/>

        //public string postalCity
        //{
        //    get
        //    {
        //        return this.postalCityField;
        //    }
        //    set
        //    {
        //        this.postalCityField = value;
        //    }
        //}

        ///// <remarks/>

        //public string postalCountry
        //{
        //    get
        //    {
        //        return this.postalCountryField;
        //    }
        //    set
        //    {
        //        this.postalCountryField = value;
        //    }
        //}

        ///// <remarks/>

        //public string postalPostCode
        //{
        //    get
        //    {
        //        return this.postalPostCodeField;
        //    }
        //    set
        //    {
        //        this.postalPostCodeField = value;
        //    }
        //}

        ///// <remarks/>

        //public string postalSuburb
        //{
        //    get
        //    {
        //        return this.postalSuburbField;
        //    }
        //    set
        //    {
        //        this.postalSuburbField = value;
        //    }
        //}

        ///// <remarks/>

        //public string purchasePrice
        //{
        //    get
        //    {
        //        return this.purchasePriceField;
        //    }
        //    set
        //    {
        //        this.purchasePriceField = value;
        //    }
        //}

        /// <remarks/>

        public DateTime startDate
        {
            get
            {
                return this.startDateField;
            }
            set
            {
                this.startDateField = value;
            }
        }

        /// <remarks/>

        public string startingCapital
        {
            get
            {
                return this.startingCapitalField;
            }
            set
            {
                this.startingCapitalField = value;
            }
        }

        /// <remarks/>

        //public string suburb
        //{
        //    get
        //    {
        //        return this.suburbField;
        //    }
        //    set
        //    {
        //        this.suburbField = value;
        //    }
        //}

        /// <remarks/>

        public string taxNumber
        {
            get
            {
                return this.taxNumberField;
            }
            set
            {
                this.taxNumberField = value;
            }
        }

        /// <remarks/>

        //public string tradingNumber
        //{
        //    get
        //    {
        //        return this.tradingNumberField;
        //    }
        //    set
        //    {
        //        this.tradingNumberField = value;
        //    }
        //}

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] VATNumbers
        {
            get
            {
                return this.vATNumbersField;
            }
            set
            {
                this.vATNumbersField = value;
            }
        }

        /// <remarks/>

        public string website
        {
            get
            {
                return this.websiteField;
            }
            set
            {
                this.websiteField = value;
            }
        }
    }

    /// <remarks/>





    public partial class GeneralBankingInfo : BureauSegment
    {

        private string accountNoField;

        private string bankNameField;

        private string branchField;

        private string[] commentField;

        private DateTime? infoDateField;

        private string majorProductField;

        private DateTime? startDateField;

        /// <remarks/>

        public string accountNo
        {
            get
            {
                return this.accountNoField;
            }
            set
            {
                this.accountNoField = value;
            }
        }

        /// <remarks/>

        public string bankName
        {
            get
            {
                return this.bankNameField;
            }
            set
            {
                this.bankNameField = value;
            }
        }

        /// <remarks/>

        public string branch
        {
            get
            {
                return this.branchField;
            }
            set
            {
                this.branchField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>

        public DateTime? infoDate
        {
            get
            {
                return this.infoDateField;
            }
            set
            {
                this.infoDateField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public DateTime? startDate
        {
            get
            {
                return this.startDateField;
            }
            set
            {
                this.startDateField = value;
            }
        }
    }

    /// <remarks/>





    public partial class FirstResponse : BureauSegment
    {

        private string businessNameField;

        private string dunsNumberField;

        private string iTNumberField;

        private string registrationNumberField;

        private string ticketNumberField;

        /// <remarks/>

        public string businessName
        {
            get
            {
                return this.businessNameField;
            }
            set
            {
                this.businessNameField = value;
            }
        }

        /// <remarks/>

        public string dunsNumber
        {
            get
            {
                return this.dunsNumberField;
            }
            set
            {
                this.dunsNumberField = value;
            }
        }

        /// <remarks/>

        public string iTNumber
        {
            get
            {
                return this.iTNumberField;
            }
            set
            {
                this.iTNumberField = value;
            }
        }

        /// <remarks/>

        public string registrationNumber
        {
            get
            {
                return this.registrationNumberField;
            }
            set
            {
                this.registrationNumberField = value;
            }
        }

        /// <remarks/>

        public string ticketNumber
        {
            get
            {
                return this.ticketNumberField;
            }
            set
            {
                this.ticketNumberField = value;
            }
        }
    }

    /// <remarks/>





    public partial class FinancialRatios : BureauSegment
    {

        private string[] acidTestRatioField;

        private string[] balanceSheetDateField;

        private string[] balanceSheetTypeField;

        private string commentField;

        private string[] currencyField;

        private string[] currentAssetsField;

        private string[] currentLiabilitiesField;

        private string[] currentRatioField;

        private string[] financialYearEndDateField;

        private string[] gearingRatioField;

        private string infoDateField;

        private string[] interestCoverRatioField;

        private string[] interestPaidField;

        private string[] inventoryField;

        private string[] longTermLiabilitiesField;

        private string majorProductField;

        private string[] ownersEquityField;

        private string[] profitBeforeInterestTaxField;

        private string[] profitabilityRatioField;

        private string[] returnOnEquityRatioField;

        private string[] turnoverField;

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] acidTestRatio
        {
            get
            {
                return this.acidTestRatioField;
            }
            set
            {
                this.acidTestRatioField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] balanceSheetDate
        {
            get
            {
                return this.balanceSheetDateField;
            }
            set
            {
                this.balanceSheetDateField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] balanceSheetType
        {
            get
            {
                return this.balanceSheetTypeField;
            }
            set
            {
                this.balanceSheetTypeField = value;
            }
        }

        /// <remarks/>

        public string comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] currency
        {
            get
            {
                return this.currencyField;
            }
            set
            {
                this.currencyField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] currentAssets
        {
            get
            {
                return this.currentAssetsField;
            }
            set
            {
                this.currentAssetsField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] currentLiabilities
        {
            get
            {
                return this.currentLiabilitiesField;
            }
            set
            {
                this.currentLiabilitiesField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] currentRatio
        {
            get
            {
                return this.currentRatioField;
            }
            set
            {
                this.currentRatioField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] financialYearEndDate
        {
            get
            {
                return this.financialYearEndDateField;
            }
            set
            {
                this.financialYearEndDateField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] gearingRatio
        {
            get
            {
                return this.gearingRatioField;
            }
            set
            {
                this.gearingRatioField = value;
            }
        }

        /// <remarks/>

        public string infoDate
        {
            get
            {
                return this.infoDateField;
            }
            set
            {
                this.infoDateField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] interestCoverRatio
        {
            get
            {
                return this.interestCoverRatioField;
            }
            set
            {
                this.interestCoverRatioField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] interestPaid
        {
            get
            {
                return this.interestPaidField;
            }
            set
            {
                this.interestPaidField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] inventory
        {
            get
            {
                return this.inventoryField;
            }
            set
            {
                this.inventoryField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] longTermLiabilities
        {
            get
            {
                return this.longTermLiabilitiesField;
            }
            set
            {
                this.longTermLiabilitiesField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] ownersEquity
        {
            get
            {
                return this.ownersEquityField;
            }
            set
            {
                this.ownersEquityField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] profitBeforeInterestTax
        {
            get
            {
                return this.profitBeforeInterestTaxField;
            }
            set
            {
                this.profitBeforeInterestTaxField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] profitabilityRatio
        {
            get
            {
                return this.profitabilityRatioField;
            }
            set
            {
                this.profitabilityRatioField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] returnOnEquityRatio
        {
            get
            {
                return this.returnOnEquityRatioField;
            }
            set
            {
                this.returnOnEquityRatioField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] turnover
        {
            get
            {
                return this.turnoverField;
            }
            set
            {
                this.turnoverField = value;
            }
        }
    }

    /// <remarks/>





    public partial class FinanceHeader : BureauSegment
    {

        private string balanceSheetDateField;

        private string balanceSheetTypeField;

        private string balanceSheetUnitField;

        private string commentField;

        private string dRCededIndField;

        private string dRCededNameField;

        private string dRFactoredField;

        private string dRFactoredNameField;

        private DateTime dateField;

        private string finYearMonthField;

        private int majorProductField;

        private string sourceField;

        private string sourceTitleField;

        private decimal turnOverAmount1Field;

        private decimal turnOverAmount2Field;

        private string turnOverDateYear1Field;

        private string turnOverDateYear2Field;

        private string turnOverYear1TypeField;

        private string turnOverYear2TypeField;

        /// <remarks/>

        //public string balanceSheetDate
        //{
        //    get
        //    {
        //        return this.balanceSheetDateField;
        //    }
        //    set
        //    {
        //        this.balanceSheetDateField = value;
        //    }
        //}

        ///// <remarks/>

        //public string balanceSheetType
        //{
        //    get
        //    {
        //        return this.balanceSheetTypeField;
        //    }
        //    set
        //    {
        //        this.balanceSheetTypeField = value;
        //    }
        //}

        ///// <remarks/>

        //public string balanceSheetUnit
        //{
        //    get
        //    {
        //        return this.balanceSheetUnitField;
        //    }
        //    set
        //    {
        //        this.balanceSheetUnitField = value;
        //    }
        //}

        ///// <remarks/>

        //public string comment
        //{
        //    get
        //    {
        //        return this.commentField;
        //    }
        //    set
        //    {
        //        this.commentField = value;
        //    }
        //}

        ///// <remarks/>

        //public string dRCededInd
        //{
        //    get
        //    {
        //        return this.dRCededIndField;
        //    }
        //    set
        //    {
        //        this.dRCededIndField = value;
        //    }
        //}

        ///// <remarks/>

        //public string dRCededName
        //{
        //    get
        //    {
        //        return this.dRCededNameField;
        //    }
        //    set
        //    {
        //        this.dRCededNameField = value;
        //    }
        //}

        ///// <remarks/>

        //public string dRFactored
        //{
        //    get
        //    {
        //        return this.dRFactoredField;
        //    }
        //    set
        //    {
        //        this.dRFactoredField = value;
        //    }
        //}

        ///// <remarks/>

        //public string dRFactoredName
        //{
        //    get
        //    {
        //        return this.dRFactoredNameField;
        //    }
        //    set
        //    {
        //        this.dRFactoredNameField = value;
        //    }
        //}

        /// <remarks/>

        public DateTime date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>

        public string finYearMonth
        {
            get
            {
                return this.finYearMonthField;
            }
            set
            {
                this.finYearMonthField = value;
            }
        }

        /// <remarks/>

        public int majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string source
        {
            get
            {
                return this.sourceField;
            }
            set
            {
                this.sourceField = value;
            }
        }

        /// <remarks/>

        //public string sourceTitle
        //{
        //    get
        //    {
        //        return this.sourceTitleField;
        //    }
        //    set
        //    {
        //        this.sourceTitleField = value;
        //    }
        //}

        /// <remarks/>

        public decimal turnOverAmount1
        {
            get
            {
                return this.turnOverAmount1Field;
            }
            set
            {
                this.turnOverAmount1Field = value;
            }
        }

        /// <remarks/>

        public decimal turnOverAmount2
        {
            get
            {
                return this.turnOverAmount2Field;
            }
            set
            {
                this.turnOverAmount2Field = value;
            }
        }

        /// <remarks/>

        //public string turnOverDateYear1
        //{
        //    get
        //    {
        //        return this.turnOverDateYear1Field;
        //    }
        //    set
        //    {
        //        this.turnOverDateYear1Field = value;
        //    }
        //}

        ///// <remarks/>

        //public string turnOverDateYear2
        //{
        //    get
        //    {
        //        return this.turnOverDateYear2Field;
        //    }
        //    set
        //    {
        //        this.turnOverDateYear2Field = value;
        //    }
        //}

        ///// <remarks/>

        //public string turnOverYear1Type
        //{
        //    get
        //    {
        //        return this.turnOverYear1TypeField;
        //    }
        //    set
        //    {
        //        this.turnOverYear1TypeField = value;
        //    }
        //}

        ///// <remarks/>

        //public string turnOverYear2Type
        //{
        //    get
        //    {
        //        return this.turnOverYear2TypeField;
        //    }
        //    set
        //    {
        //        this.turnOverYear2TypeField = value;
        //    }
        //}
    }

    /// <remarks/>





    public partial class FinanceData : BureauSegment
    {

        private string balanceSheetDateBeforeField;

        private string balanceSheetDateCurrentField;

        private string balanceSheetTypeBeforeField;

        private string balanceSheetTypeCurrentField;

        private string commentField;

        private string currencyBeforeField;

        private string currencyCurrentField;

        private string dateField;

        private string finYearMonthBeforeField;

        private string finYearMonthCurrentField;

        private string financialFactorField;

        private string infoSourceField;

        private string majorProductField;

        private string netProfitApproximationBeforeField;

        private string netProfitApproximationCurrentField;

        private string turnoverApproximationBeforeField;

        private string turnoverApproximationCurrentField;

        private string[] turnoverBandBeforeField;

        private string[] turnoverBandCurrentField;

        private string yearBeforeField;

        private string yearCurrentField;

        /// <remarks/>

        public string balanceSheetDateBefore
        {
            get
            {
                return this.balanceSheetDateBeforeField;
            }
            set
            {
                this.balanceSheetDateBeforeField = value;
            }
        }

        /// <remarks/>

        public string balanceSheetDateCurrent
        {
            get
            {
                return this.balanceSheetDateCurrentField;
            }
            set
            {
                this.balanceSheetDateCurrentField = value;
            }
        }

        /// <remarks/>

        public string balanceSheetTypeBefore
        {
            get
            {
                return this.balanceSheetTypeBeforeField;
            }
            set
            {
                this.balanceSheetTypeBeforeField = value;
            }
        }

        /// <remarks/>

        public string balanceSheetTypeCurrent
        {
            get
            {
                return this.balanceSheetTypeCurrentField;
            }
            set
            {
                this.balanceSheetTypeCurrentField = value;
            }
        }

        /// <remarks/>

        public string comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>

        public string currencyBefore
        {
            get
            {
                return this.currencyBeforeField;
            }
            set
            {
                this.currencyBeforeField = value;
            }
        }

        /// <remarks/>

        public string currencyCurrent
        {
            get
            {
                return this.currencyCurrentField;
            }
            set
            {
                this.currencyCurrentField = value;
            }
        }

        /// <remarks/>

        public string date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>

        public string finYearMonthBefore
        {
            get
            {
                return this.finYearMonthBeforeField;
            }
            set
            {
                this.finYearMonthBeforeField = value;
            }
        }

        /// <remarks/>

        public string finYearMonthCurrent
        {
            get
            {
                return this.finYearMonthCurrentField;
            }
            set
            {
                this.finYearMonthCurrentField = value;
            }
        }

        /// <remarks/>

        public string financialFactor
        {
            get
            {
                return this.financialFactorField;
            }
            set
            {
                this.financialFactorField = value;
            }
        }

        /// <remarks/>

        public string infoSource
        {
            get
            {
                return this.infoSourceField;
            }
            set
            {
                this.infoSourceField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string netProfitApproximationBefore
        {
            get
            {
                return this.netProfitApproximationBeforeField;
            }
            set
            {
                this.netProfitApproximationBeforeField = value;
            }
        }

        /// <remarks/>

        public string netProfitApproximationCurrent
        {
            get
            {
                return this.netProfitApproximationCurrentField;
            }
            set
            {
                this.netProfitApproximationCurrentField = value;
            }
        }

        /// <remarks/>

        public string turnoverApproximationBefore
        {
            get
            {
                return this.turnoverApproximationBeforeField;
            }
            set
            {
                this.turnoverApproximationBeforeField = value;
            }
        }

        /// <remarks/>

        public string turnoverApproximationCurrent
        {
            get
            {
                return this.turnoverApproximationCurrentField;
            }
            set
            {
                this.turnoverApproximationCurrentField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] turnoverBandBefore
        {
            get
            {
                return this.turnoverBandBeforeField;
            }
            set
            {
                this.turnoverBandBeforeField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] turnoverBandCurrent
        {
            get
            {
                return this.turnoverBandCurrentField;
            }
            set
            {
                this.turnoverBandCurrentField = value;
            }
        }

        /// <remarks/>

        public string yearBefore
        {
            get
            {
                return this.yearBeforeField;
            }
            set
            {
                this.yearBeforeField = value;
            }
        }

        /// <remarks/>

        public string yearCurrent
        {
            get
            {
                return this.yearCurrentField;
            }
            set
            {
                this.yearCurrentField = value;
            }
        }
    }

    /// <remarks/>





    public partial class FinanceDataFF : BureauSegment
    {

        private string dateField;

        private string debtorDescriptionField;

        private string debtorNameField;

        private DebtorInfo[] debtorsField;

        private string majorProductField;

        /// <remarks/>

        public string date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>

        public string debtorDescription
        {
            get
            {
                return this.debtorDescriptionField;
            }
            set
            {
                this.debtorDescriptionField = value;
            }
        }

        /// <remarks/>

        public string debtorName
        {
            get
            {
                return this.debtorNameField;
            }
            set
            {
                this.debtorNameField = value;
            }
        }

        /// <remarks/>

        public DebtorInfo[] debtors
        {
            get
            {
                return this.debtorsField;
            }
            set
            {
                this.debtorsField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }
    }

    /// <remarks/>





    public partial class FinanceDataFE : BureauSegment
    {

        private FinanceAmounts[] amountsField;

        private string dateField;

        private string majorProductField;

        /// <remarks/>

        public FinanceAmounts[] amounts
        {
            get
            {
                return this.amountsField;
            }
            set
            {
                this.amountsField = value;
            }
        }

        /// <remarks/>

        public string date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }
    }

    /// <remarks/>





    public partial class EnquirySummary : BureauSegment
    {

        private string last12MonthTotalField;

        private string last24MonthTotalField;

        private string last3MonthTotalField;

        private string last6MonthTotalField;

        private string majorProductField;

        /// <remarks/>

        public string last12MonthTotal
        {
            get
            {
                return this.last12MonthTotalField;
            }
            set
            {
                this.last12MonthTotalField = value;
            }
        }

        /// <remarks/>

        public string last24MonthTotal
        {
            get
            {
                return this.last24MonthTotalField;
            }
            set
            {
                this.last24MonthTotalField = value;
            }
        }

        /// <remarks/>

        public string last3MonthTotal
        {
            get
            {
                return this.last3MonthTotalField;
            }
            set
            {
                this.last3MonthTotalField = value;
            }
        }

        /// <remarks/>

        public string last6MonthTotal
        {
            get
            {
                return this.last6MonthTotalField;
            }
            set
            {
                this.last6MonthTotalField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }
    }

    /// <remarks/>





    public partial class EnquiryHistory : BureauSegment
    {

        private string amountField;

        private string contactNameField;

        private string contactPhoneField;

        private DateTime dateOfEnquiryField;

        private int majorProductField;

        private string reasonField;

        private string subscriberField;

        private string subscriberNumberField;

        /// <remarks/>

        //public string amount
        //{
        //    get
        //    {
        //        return this.amountField;
        //    }
        //    set
        //    {
        //        this.amountField = value;
        //    }
        //}

        ///// <remarks/>

        //public string contactName
        //{
        //    get
        //    {
        //        return this.contactNameField;
        //    }
        //    set
        //    {
        //        this.contactNameField = value;
        //    }
        //}

        /// <remarks/>

        public string contactPhone
        {
            get
            {
                return this.contactPhoneField;
            }
            set
            {
                this.contactPhoneField = value;
            }
        }

        /// <remarks/>

        public DateTime dateOfEnquiry
        {
            get
            {
                return this.dateOfEnquiryField;
            }
            set
            {
                this.dateOfEnquiryField = value;
            }
        }

        /// <remarks/>

        public int majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        //public string reason
        //{
        //    get
        //    {
        //        return this.reasonField;
        //    }
        //    set
        //    {
        //        this.reasonField = value;
        //    }
        //}

        /// <remarks/>

        public string subscriber
        {
            get
            {
                return this.subscriberField;
            }
            set
            {
                this.subscriberField = value;
            }
        }

        /// <remarks/>

        //public string subscriberNumber
        //{
        //    get
        //    {
        //        return this.subscriberNumberField;
        //    }
        //    set
        //    {
        //        this.subscriberNumberField = value;
        //    }
        //}
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(EmpiricaEM04))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(EmpiricaE1))]





    public partial class Empirica : BureauSegment
    {
    }

    /// <remarks/>





    public partial class EmpiricaEM04 : Empirica
    {

        private string consumerNoField;

        private string empiricaScoreField;

        private string exclusionCodeDescriptionField;

        private string exclusionCodeField;

        private string expansionScoreDescriptionField;

        private string expansionScoreField;

        private string[] reasonCodeField;

        private string[] reasonDescriptionField;

        /// <remarks/>

        public string consumerNo
        {
            get
            {
                return this.consumerNoField;
            }
            set
            {
                this.consumerNoField = value;
            }
        }

        /// <remarks/>

        public string empiricaScore
        {
            get
            {
                return this.empiricaScoreField;
            }
            set
            {
                this.empiricaScoreField = value;
            }
        }

        /// <remarks/>

        public string exclusionCodeDescription
        {
            get
            {
                return this.exclusionCodeDescriptionField;
            }
            set
            {
                this.exclusionCodeDescriptionField = value;
            }
        }

        /// <remarks/>

        public string exclusionCode
        {
            get
            {
                return this.exclusionCodeField;
            }
            set
            {
                this.exclusionCodeField = value;
            }
        }

        /// <remarks/>

        public string expansionScoreDescription
        {
            get
            {
                return this.expansionScoreDescriptionField;
            }
            set
            {
                this.expansionScoreDescriptionField = value;
            }
        }

        /// <remarks/>

        public string expansionScore
        {
            get
            {
                return this.expansionScoreField;
            }
            set
            {
                this.expansionScoreField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] reasonCode
        {
            get
            {
                return this.reasonCodeField;
            }
            set
            {
                this.reasonCodeField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] reasonDescription
        {
            get
            {
                return this.reasonDescriptionField;
            }
            set
            {
                this.reasonDescriptionField = value;
            }
        }
    }

    /// <remarks/>





    public partial class EmpiricaE1 : Empirica
    {

        private string consumerNoField;

        private string dateOfBirthField;

        private string empiricaScoreField;

        private string errorMessageField;

        private string exclusionCodeDescriptionField;

        private string exclusionCodeField;

        private string expansionCodeField;

        private string forename1Field;

        private string forename2Field;

        private string forename3Field;

        private string identityNoField;

        private string majorProductField;

        private string[] reasonCodeField;

        private string[] reasonDescriptionField;

        private string surnameField;

        /// <remarks/>

        public string consumerNo
        {
            get
            {
                return this.consumerNoField;
            }
            set
            {
                this.consumerNoField = value;
            }
        }

        /// <remarks/>

        public string dateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>

        public string empiricaScore
        {
            get
            {
                return this.empiricaScoreField;
            }
            set
            {
                this.empiricaScoreField = value;
            }
        }

        /// <remarks/>

        public string errorMessage
        {
            get
            {
                return this.errorMessageField;
            }
            set
            {
                this.errorMessageField = value;
            }
        }

        /// <remarks/>

        public string exclusionCodeDescription
        {
            get
            {
                return this.exclusionCodeDescriptionField;
            }
            set
            {
                this.exclusionCodeDescriptionField = value;
            }
        }

        /// <remarks/>

        public string exclusionCode
        {
            get
            {
                return this.exclusionCodeField;
            }
            set
            {
                this.exclusionCodeField = value;
            }
        }

        /// <remarks/>

        public string expansionCode
        {
            get
            {
                return this.expansionCodeField;
            }
            set
            {
                this.expansionCodeField = value;
            }
        }

        /// <remarks/>

        public string forename1
        {
            get
            {
                return this.forename1Field;
            }
            set
            {
                this.forename1Field = value;
            }
        }

        /// <remarks/>

        public string forename2
        {
            get
            {
                return this.forename2Field;
            }
            set
            {
                this.forename2Field = value;
            }
        }

        /// <remarks/>

        public string forename3
        {
            get
            {
                return this.forename3Field;
            }
            set
            {
                this.forename3Field = value;
            }
        }

        /// <remarks/>

        public string identityNo
        {
            get
            {
                return this.identityNoField;
            }
            set
            {
                this.identityNoField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] reasonCode
        {
            get
            {
                return this.reasonCodeField;
            }
            set
            {
                this.reasonCodeField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] reasonDescription
        {
            get
            {
                return this.reasonDescriptionField;
            }
            set
            {
                this.reasonDescriptionField = value;
            }
        }

        /// <remarks/>

        public string surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }
    }

    /// <remarks/>





    public partial class EmpOfCapital : BureauSegment
    {

        private string[] empOfCapAmtField;

        private string[] empOfCapItemField;

        private string empOfCapTotalField;

        private string majorProductField;

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] empOfCapAmt
        {
            get
            {
                return this.empOfCapAmtField;
            }
            set
            {
                this.empOfCapAmtField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] empOfCapItem
        {
            get
            {
                return this.empOfCapItemField;
            }
            set
            {
                this.empOfCapItemField = value;
            }
        }

        /// <remarks/>

        public string empOfCapTotal
        {
            get
            {
                return this.empOfCapTotalField;
            }
            set
            {
                this.empOfCapTotalField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }
    }

    /// <remarks/>





    public partial class DynamicRating : BureauSegment
    {

        private string[] commentField;

        private string conditionField;

        private string majorProductField;

        private string ratingField;

        private string trendField;

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>

        public string condition
        {
            get
            {
                return this.conditionField;
            }
            set
            {
                this.conditionField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string rating
        {
            get
            {
                return this.ratingField;
            }
            set
            {
                this.ratingField = value;
            }
        }

        /// <remarks/>

        public string trend
        {
            get
            {
                return this.trendField;
            }
            set
            {
                this.trendField = value;
            }
        }
    }

    /// <remarks/>





    public partial class Default : BureauSegment
    {

        private decimal amountField;

        private string cityField;

        private string commentField;

        private string countryField;

        private string defaultAddressField;

        private DateTime defaultDateField;

        private string defaultNameField;

        private string defaultTradeStyleField;

        private int majorProductField;

        private string messageField;

        private int numberFoundField;

        private string onBehalfOfField;

        private string postCodeField;

        private string serialNoField;

        private string statusField;

        private string subscriberNameField;

        private string suburbField;

        private string supplierNameField;

        /// <remarks/>

        public decimal amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>

        //public string city
        //{
        //    get
        //    {
        //        return this.cityField;
        //    }
        //    set
        //    {
        //        this.cityField = value;
        //    }
        //}

        /// <remarks/>

        public string comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>

        //public string country
        //{
        //    get
        //    {
        //        return this.countryField;
        //    }
        //    set
        //    {
        //        this.countryField = value;
        //    }
        //}

        ///// <remarks/>

        //public string defaultAddress
        //{
        //    get
        //    {
        //        return this.defaultAddressField;
        //    }
        //    set
        //    {
        //        this.defaultAddressField = value;
        //    }
        //}

        /// <remarks/>

        public DateTime defaultDate
        {
            get
            {
                return this.defaultDateField;
            }
            set
            {
                this.defaultDateField = value;
            }
        }

        /// <remarks/>

        public string defaultName
        {
            get
            {
                return this.defaultNameField;
            }
            set
            {
                this.defaultNameField = value;
            }
        }

        /// <remarks/>

        //public string defaultTradeStyle
        //{
        //    get
        //    {
        //        return this.defaultTradeStyleField;
        //    }
        //    set
        //    {
        //        this.defaultTradeStyleField = value;
        //    }
        //}

        /// <remarks/>

        public int majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        //public string message
        //{
        //    get
        //    {
        //        return this.messageField;
        //    }
        //    set
        //    {
        //        this.messageField = value;
        //    }
        //}

        ///// <remarks/>

        public int numberFound
        {
            get
            {
                return this.numberFoundField;
            }
            set
            {
                this.numberFoundField = value;
            }
        }

        ///// <remarks/>

        //public string onBehalfOf
        //{
        //    get
        //    {
        //        return this.onBehalfOfField;
        //    }
        //    set
        //    {
        //        this.onBehalfOfField = value;
        //    }
        //}

        ///// <remarks/>

        //public string postCode
        //{
        //    get
        //    {
        //        return this.postCodeField;
        //    }
        //    set
        //    {
        //        this.postCodeField = value;
        //    }
        //}

        ///// <remarks/>

        //public string serialNo
        //{
        //    get
        //    {
        //        return this.serialNoField;
        //    }
        //    set
        //    {
        //        this.serialNoField = value;
        //    }
        //}

        /// <remarks/>

        public string status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>

        public string subscriberName
        {
            get
            {
                return this.subscriberNameField;
            }
            set
            {
                this.subscriberNameField = value;
            }
        }

        /// <remarks/>

        //public string suburb
        //{
        //    get
        //    {
        //        return this.suburbField;
        //    }
        //    set
        //    {
        //        this.suburbField = value;
        //    }
        //}

        /// <remarks/>

        public string supplierName
        {
            get
            {
                return this.supplierNameField;
            }
            set
            {
                this.supplierNameField = value;
            }
        }
    }

    /// <remarks/>





    public partial class DeedsMultipleBond : BureauSegment
    {

        private string actionDateField;

        private string bondAmountField;

        private string bondBuyerIDField;

        private string bondBuyerNameField;

        private string bondDateField;

        private string bondHolderField;

        private string bondNumberField;

        private string commentField;

        private string majorProductField;

        private string rowIDField;

        /// <remarks/>

        public string actionDate
        {
            get
            {
                return this.actionDateField;
            }
            set
            {
                this.actionDateField = value;
            }
        }

        /// <remarks/>

        public string bondAmount
        {
            get
            {
                return this.bondAmountField;
            }
            set
            {
                this.bondAmountField = value;
            }
        }

        /// <remarks/>

        public string bondBuyerID
        {
            get
            {
                return this.bondBuyerIDField;
            }
            set
            {
                this.bondBuyerIDField = value;
            }
        }

        /// <remarks/>

        public string bondBuyerName
        {
            get
            {
                return this.bondBuyerNameField;
            }
            set
            {
                this.bondBuyerNameField = value;
            }
        }

        /// <remarks/>

        public string bondDate
        {
            get
            {
                return this.bondDateField;
            }
            set
            {
                this.bondDateField = value;
            }
        }

        /// <remarks/>

        public string bondHolder
        {
            get
            {
                return this.bondHolderField;
            }
            set
            {
                this.bondHolderField = value;
            }
        }

        /// <remarks/>

        public string bondNumber
        {
            get
            {
                return this.bondNumberField;
            }
            set
            {
                this.bondNumberField = value;
            }
        }

        /// <remarks/>

        public string comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string rowID
        {
            get
            {
                return this.rowIDField;
            }
            set
            {
                this.rowIDField = value;
            }
        }
    }

    /// <remarks/>





    public partial class DeedHistory : BureauSegment
    {

        private string bondAmountField;

        private string bondDateField;

        private string bondNumberField;

        private string commentField;

        private string dateField;

        private string dateOfBirthIDField;

        private string deedsOfficeField;

        private string eRFField;

        private string farmField;

        private string majorProductField;

        private string mortgageeField;

        private string portionField;

        private string propertyNameField;

        private string propertySizeField;

        private string purchaseDateField;

        private string purchasePriceField;

        private string rowIDField;

        private string titleField;

        private string townshipField;

        /// <remarks/>

        public string bondAmount
        {
            get
            {
                return this.bondAmountField;
            }
            set
            {
                this.bondAmountField = value;
            }
        }

        /// <remarks/>

        public string bondDate
        {
            get
            {
                return this.bondDateField;
            }
            set
            {
                this.bondDateField = value;
            }
        }

        /// <remarks/>

        public string bondNumber
        {
            get
            {
                return this.bondNumberField;
            }
            set
            {
                this.bondNumberField = value;
            }
        }

        /// <remarks/>

        public string comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>

        public string date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>

        public string dateOfBirthID
        {
            get
            {
                return this.dateOfBirthIDField;
            }
            set
            {
                this.dateOfBirthIDField = value;
            }
        }

        /// <remarks/>

        public string deedsOffice
        {
            get
            {
                return this.deedsOfficeField;
            }
            set
            {
                this.deedsOfficeField = value;
            }
        }

        /// <remarks/>

        public string eRF
        {
            get
            {
                return this.eRFField;
            }
            set
            {
                this.eRFField = value;
            }
        }

        /// <remarks/>

        public string farm
        {
            get
            {
                return this.farmField;
            }
            set
            {
                this.farmField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string mortgagee
        {
            get
            {
                return this.mortgageeField;
            }
            set
            {
                this.mortgageeField = value;
            }
        }

        /// <remarks/>

        public string portion
        {
            get
            {
                return this.portionField;
            }
            set
            {
                this.portionField = value;
            }
        }

        /// <remarks/>

        public string propertyName
        {
            get
            {
                return this.propertyNameField;
            }
            set
            {
                this.propertyNameField = value;
            }
        }

        /// <remarks/>

        public string propertySize
        {
            get
            {
                return this.propertySizeField;
            }
            set
            {
                this.propertySizeField = value;
            }
        }

        /// <remarks/>

        public string purchaseDate
        {
            get
            {
                return this.purchaseDateField;
            }
            set
            {
                this.purchaseDateField = value;
            }
        }

        /// <remarks/>

        public string purchasePrice
        {
            get
            {
                return this.purchasePriceField;
            }
            set
            {
                this.purchasePriceField = value;
            }
        }

        /// <remarks/>

        public string rowID
        {
            get
            {
                return this.rowIDField;
            }
            set
            {
                this.rowIDField = value;
            }
        }

        /// <remarks/>

        public string title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>

        public string township
        {
            get
            {
                return this.townshipField;
            }
            set
            {
                this.townshipField = value;
            }
        }
    }

    /// <remarks/>





    public partial class CurrentLiabilities : BureauSegment
    {

        private string[] currentLiabilityItemAmtField;

        private string[] currentLiabilityItemField;

        private decimal currentLiabilityTotalField;

        private int majorProductField;

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        //public string[] currentLiabilityItemAmt
        //{
        //    get
        //    {
        //        return this.currentLiabilityItemAmtField;
        //    }
        //    set
        //    {
        //        this.currentLiabilityItemAmtField = value;
        //    }
        //}

        ///// <remarks/>

        //// [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        //public string[] currentLiabilityItem
        //{
        //    get
        //    {
        //        return this.currentLiabilityItemField;
        //    }
        //    set
        //    {
        //        this.currentLiabilityItemField = value;
        //    }
        //}

        /// <remarks/>

        public decimal currentLiabilityTotal
        {
            get
            {
                return this.currentLiabilityTotalField;
            }
            set
            {
                this.currentLiabilityTotalField = value;
            }
        }

        /// <remarks/>

        public int majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }
    }

    /// <remarks/>





    public partial class CurrentAsset : BureauSegment
    {

        private string[] currentAssItemAmtField;

        private string[] currentAssItemField;

        private decimal currentAssTotalField;

        private int majorProductField;

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        //public string[] currentAssItemAmt
        //{
        //    get
        //    {
        //        return this.currentAssItemAmtField;
        //    }
        //    set
        //    {
        //        this.currentAssItemAmtField = value;
        //    }
        //}

        ///// <remarks/>

        //// [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        //public string[] currentAssItem
        //{
        //    get
        //    {
        //        return this.currentAssItemField;
        //    }
        //    set
        //    {
        //        this.currentAssItemField = value;
        //    }
        //}

        /// <remarks/>

        public decimal currentAssTotal
        {
            get
            {
                return this.currentAssTotalField;
            }
            set
            {
                this.currentAssTotalField = value;
            }
        }

        /// <remarks/>

        public int majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }
    }

    /// <remarks/>





    public partial class CourtRecord : BureauSegment
    {

        private string abandonDateField;

        private DateTime? actionDateField;

        private string attorneyField;

        private string caseNumberField;

        private string cityField;

        private decimal claimAmountField;

        private string commentField;

        private string countryField;

        private string courtDistrictField;

        private string courtRecordAddressField;

        private string courtTypeField;

        private string defendantDistrictField;

        private string defendantNameField;

        private string defendantTradeStyleField;

        private int majorProductField;

        private string messageField;

        private string natureOfDebtField;

        private string numberFoundField;

        private string plaintiffNameField;

        private string postCodeField;

        private string returnDateField;

        private string serialNumberField;

        private string suburbField;

        private string typeCodeField;

        private string typeDescField;

        /// <remarks/>

        //public string abandonDate
        //{
        //    get
        //    {
        //        return this.abandonDateField;
        //    }
        //    set
        //    {
        //        this.abandonDateField = value;
        //    }
        //}

        /// <remarks/>

        public DateTime? actionDate
        {
            get
            {
                return this.actionDateField;
            }
            set
            {
                this.actionDateField = value;
            }
        }

        /// <remarks/>

        public string attorney
        {
            get
            {
                return this.attorneyField;
            }
            set
            {
                this.attorneyField = value;
            }
        }

        /// <remarks/>

        public string caseNumber
        {
            get
            {
                return this.caseNumberField;
            }
            set
            {
                this.caseNumberField = value;
            }
        }

        /// <remarks/>

        //public string city
        //{
        //    get
        //    {
        //        return this.cityField;
        //    }
        //    set
        //    {
        //        this.cityField = value;
        //    }
        //}

        /// <remarks/>

        public decimal claimAmount
        {
            get
            {
                return this.claimAmountField;
            }
            set
            {
                this.claimAmountField = value;
            }
        }

        /// <remarks/>

        //public string comment
        //{
        //    get
        //    {
        //        return this.commentField;
        //    }
        //    set
        //    {
        //        this.commentField = value;
        //    }
        //}

        ///// <remarks/>

        //public string country
        //{
        //    get
        //    {
        //        return this.countryField;
        //    }
        //    set
        //    {
        //        this.countryField = value;
        //    }
        //}

        ///// <remarks/>

        //public string courtDistrict
        //{
        //    get
        //    {
        //        return this.courtDistrictField;
        //    }
        //    set
        //    {
        //        this.courtDistrictField = value;
        //    }
        //}

        ///// <remarks/>

        //public string courtRecordAddress
        //{
        //    get
        //    {
        //        return this.courtRecordAddressField;
        //    }
        //    set
        //    {
        //        this.courtRecordAddressField = value;
        //    }
        //}

        ///// <remarks/>

        //public string courtType
        //{
        //    get
        //    {
        //        return this.courtTypeField;
        //    }
        //    set
        //    {
        //        this.courtTypeField = value;
        //    }
        //}

        ///// <remarks/>

        //public string defendantDistrict
        //{
        //    get
        //    {
        //        return this.defendantDistrictField;
        //    }
        //    set
        //    {
        //        this.defendantDistrictField = value;
        //    }
        //}

        ///// <remarks/>

        //public string defendantName
        //{
        //    get
        //    {
        //        return this.defendantNameField;
        //    }
        //    set
        //    {
        //        this.defendantNameField = value;
        //    }
        //}

        ///// <remarks/>

        //public string defendantTradeStyle
        //{
        //    get
        //    {
        //        return this.defendantTradeStyleField;
        //    }
        //    set
        //    {
        //        this.defendantTradeStyleField = value;
        //    }
        //}

        /// <remarks/>

        public int majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        //public string message
        //{
        //    get
        //    {
        //        return this.messageField;
        //    }
        //    set
        //    {
        //        this.messageField = value;
        //    }
        //}

        ///// <remarks/>

        //public string natureOfDebt
        //{
        //    get
        //    {
        //        return this.natureOfDebtField;
        //    }
        //    set
        //    {
        //        this.natureOfDebtField = value;
        //    }
        //}

        ///// <remarks/>

        //public string numberFound
        //{
        //    get
        //    {
        //        return this.numberFoundField;
        //    }
        //    set
        //    {
        //        this.numberFoundField = value;
        //    }
        //}

        ///// <remarks/>

        //public string plaintiffName
        //{
        //    get
        //    {
        //        return this.plaintiffNameField;
        //    }
        //    set
        //    {
        //        this.plaintiffNameField = value;
        //    }
        //}

        ///// <remarks/>

        //public string postCode
        //{
        //    get
        //    {
        //        return this.postCodeField;
        //    }
        //    set
        //    {
        //        this.postCodeField = value;
        //    }
        //}

        ///// <remarks/>

        //public string returnDate
        //{
        //    get
        //    {
        //        return this.returnDateField;
        //    }
        //    set
        //    {
        //        this.returnDateField = value;
        //    }
        //}

        ///// <remarks/>

        //public string serialNumber
        //{
        //    get
        //    {
        //        return this.serialNumberField;
        //    }
        //    set
        //    {
        //        this.serialNumberField = value;
        //    }
        //}

        ///// <remarks/>

        //public string suburb
        //{
        //    get
        //    {
        //        return this.suburbField;
        //    }
        //    set
        //    {
        //        this.suburbField = value;
        //    }
        //}

        ///// <remarks/>

        //public string typeCode
        //{
        //    get
        //    {
        //        return this.typeCodeField;
        //    }
        //    set
        //    {
        //        this.typeCodeField = value;
        //    }
        //}

        /// <remarks/>

        public string typeDesc
        {
            get
            {
                return this.typeDescField;
            }
            set
            {
                this.typeDescField = value;
            }
        }
    }

    /// <remarks/>





    public partial class ConsumerTraceAlert : BureauSegment
    {

        private string comment1Field;

        private string comment2Field;

        private string consumerNoField;

        private string contactNameField;

        private string contactPhoneField;

        private string informationDateField;

        private string majorProductField;

        private string messageField;

        private string subscriberNameField;

        private string subscriberNumberField;

        private string traceTypeCodeField;

        private string traceTypeField;

        private string transactionTypeField;

        /// <remarks/>

        public string comment1
        {
            get
            {
                return this.comment1Field;
            }
            set
            {
                this.comment1Field = value;
            }
        }

        /// <remarks/>

        public string comment2
        {
            get
            {
                return this.comment2Field;
            }
            set
            {
                this.comment2Field = value;
            }
        }

        /// <remarks/>

        public string consumerNo
        {
            get
            {
                return this.consumerNoField;
            }
            set
            {
                this.consumerNoField = value;
            }
        }

        /// <remarks/>

        public string contactName
        {
            get
            {
                return this.contactNameField;
            }
            set
            {
                this.contactNameField = value;
            }
        }

        /// <remarks/>

        public string contactPhone
        {
            get
            {
                return this.contactPhoneField;
            }
            set
            {
                this.contactPhoneField = value;
            }
        }

        /// <remarks/>

        public string informationDate
        {
            get
            {
                return this.informationDateField;
            }
            set
            {
                this.informationDateField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }

        /// <remarks/>

        public string subscriberName
        {
            get
            {
                return this.subscriberNameField;
            }
            set
            {
                this.subscriberNameField = value;
            }
        }

        /// <remarks/>

        public string subscriberNumber
        {
            get
            {
                return this.subscriberNumberField;
            }
            set
            {
                this.subscriberNumberField = value;
            }
        }

        /// <remarks/>

        public string traceTypeCode
        {
            get
            {
                return this.traceTypeCodeField;
            }
            set
            {
                this.traceTypeCodeField = value;
            }
        }

        /// <remarks/>

        public string traceType
        {
            get
            {
                return this.traceTypeField;
            }
            set
            {
                this.traceTypeField = value;
            }
        }

        /// <remarks/>

        public string transactionType
        {
            get
            {
                return this.transactionTypeField;
            }
            set
            {
                this.transactionTypeField = value;
            }
        }
    }

    /// <remarks/>





    public partial class ConsumerNotice : BureauSegment
    {

        private string address1Field;

        private decimal amountField;

        private string attorneyNameField;

        private string attorneyPhoneField;

        private string attorneyReferenceField;

        private DateTime captureDateField;

        private string caseNumberField;

        private int consumerNumberField;

        private string courtCodeField;

        private string courtNameField;

        private string courtTypeCodeField;

        private string courtTypeDescField;

        private string dateOfBirthField;

        private string defNoField;

        private string defendantNameField;

        private string iDNoField;

        private int majorProductField;

        private string masterNoField;

        private string messageField;

        private string noticeCodeField;

        private string noticeDateField;

        private string noticeDescriptionField;

        private string plaintiffNameField;

        private string remarksField;

        private string transTypeField;

        /// <remarks/>

        //public string address1
        //{
        //    get
        //    {
        //        return this.address1Field;
        //    }
        //    set
        //    {
        //        this.address1Field = value;
        //    }
        //}

        /// <remarks/>

        public decimal amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>

        public string attorneyName
        {
            get
            {
                return this.attorneyNameField;
            }
            set
            {
                this.attorneyNameField = value;
            }
        }

        /// <remarks/>

        public string attorneyPhone
        {
            get
            {
                return this.attorneyPhoneField;
            }
            set
            {
                this.attorneyPhoneField = value;
            }
        }

        /// <remarks/>

        //public string attorneyReference
        //{
        //    get
        //    {
        //        return this.attorneyReferenceField;
        //    }
        //    set
        //    {
        //        this.attorneyReferenceField = value;
        //    }
        //}

        /// <remarks/>

        public DateTime captureDate
        {
            get
            {
                return this.captureDateField;
            }
            set
            {
                this.captureDateField = value;
            }
        }

        /// <remarks/>

        public string caseNumber
        {
            get
            {
                return this.caseNumberField;
            }
            set
            {
                this.caseNumberField = value;
            }
        }

        /// <remarks/>

        public int consumerNumber
        {
            get
            {
                return this.consumerNumberField;
            }
            set
            {
                this.consumerNumberField = value;
            }
        }

        /// <remarks/>

        //public string courtCode
        //{
        //    get
        //    {
        //        return this.courtCodeField;
        //    }
        //    set
        //    {
        //        this.courtCodeField = value;
        //    }
        //}

        /// <remarks/>

        public string courtName
        {
            get
            {
                return this.courtNameField;
            }
            set
            {
                this.courtNameField = value;
            }
        }

        /// <remarks/>

        //public string courtTypeCode
        //{
        //    get
        //    {
        //        return this.courtTypeCodeField;
        //    }
        //    set
        //    {
        //        this.courtTypeCodeField = value;
        //    }
        //}

        ///// <remarks/>

        //public string courtTypeDesc
        //{
        //    get
        //    {
        //        return this.courtTypeDescField;
        //    }
        //    set
        //    {
        //        this.courtTypeDescField = value;
        //    }
        //}

        ///// <remarks/>

        //public string dateOfBirth
        //{
        //    get
        //    {
        //        return this.dateOfBirthField;
        //    }
        //    set
        //    {
        //        this.dateOfBirthField = value;
        //    }
        //}

        ///// <remarks/>

        //public string defNo
        //{
        //    get
        //    {
        //        return this.defNoField;
        //    }
        //    set
        //    {
        //        this.defNoField = value;
        //    }
        //}

        ///// <remarks/>

        //public string defendantName
        //{
        //    get
        //    {
        //        return this.defendantNameField;
        //    }
        //    set
        //    {
        //        this.defendantNameField = value;
        //    }
        //}

        /// <remarks/>

        public string iDNo
        {
            get
            {
                return this.iDNoField;
            }
            set
            {
                this.iDNoField = value;
            }
        }

        /// <remarks/>

        public int majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        //public string masterNo
        //{
        //    get
        //    {
        //        return this.masterNoField;
        //    }
        //    set
        //    {
        //        this.masterNoField = value;
        //    }
        //}

        ///// <remarks/>

        //public string message
        //{
        //    get
        //    {
        //        return this.messageField;
        //    }
        //    set
        //    {
        //        this.messageField = value;
        //    }
        //}

        ///// <remarks/>

        //public string noticeCode
        //{
        //    get
        //    {
        //        return this.noticeCodeField;
        //    }
        //    set
        //    {
        //        this.noticeCodeField = value;
        //    }
        //}

        ///// <remarks/>

        //public string noticeDate
        //{
        //    get
        //    {
        //        return this.noticeDateField;
        //    }
        //    set
        //    {
        //        this.noticeDateField = value;
        //    }
        //}

        ///// <remarks/>

        //public string noticeDescription
        //{
        //    get
        //    {
        //        return this.noticeDescriptionField;
        //    }
        //    set
        //    {
        //        this.noticeDescriptionField = value;
        //    }
        //}

        /// <remarks/>

        public string plaintiffName
        {
            get
            {
                return this.plaintiffNameField;
            }
            set
            {
                this.plaintiffNameField = value;
            }
        }

        /// <remarks/>

        //public string remarks
        //{
        //    get
        //    {
        //        return this.remarksField;
        //    }
        //    set
        //    {
        //        this.remarksField = value;
        //    }
        //}

        ///// <remarks/>

        //public string transType
        //{
        //    get
        //    {
        //        return this.transTypeField;
        //    }
        //    set
        //    {
        //        this.transTypeField = value;
        //    }
        //}
    }

    /// <remarks/>





    public partial class ConsumerNotarialBonds : BureauSegment
    {

        private string addressField;

        private string amountField;

        private string bondPercField;

        private string captureDateField;

        private string caseNoField;

        private string consumerNumberField;

        private string courtCodeField;

        private string courtDescField;

        private string courtTypeDescField;

        private string courtTypeField;

        private string dateOfBirthField;

        private string defendantNameField;

        private string defendantNumberField;

        private string iDNoField;

        private string judgementCodeField;

        private string judgementDescField;

        private string majorProductField;

        private string masterNoField;

        private string messageField;

        private string notarialDateField;

        private string plaintiffField;

        private string remarksField;

        private string transTypeField;

        /// <remarks/>

        public string address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>

        public string amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>

        public string bondPerc
        {
            get
            {
                return this.bondPercField;
            }
            set
            {
                this.bondPercField = value;
            }
        }

        /// <remarks/>

        public string captureDate
        {
            get
            {
                return this.captureDateField;
            }
            set
            {
                this.captureDateField = value;
            }
        }

        /// <remarks/>

        public string caseNo
        {
            get
            {
                return this.caseNoField;
            }
            set
            {
                this.caseNoField = value;
            }
        }

        /// <remarks/>

        public string consumerNumber
        {
            get
            {
                return this.consumerNumberField;
            }
            set
            {
                this.consumerNumberField = value;
            }
        }

        /// <remarks/>

        public string courtCode
        {
            get
            {
                return this.courtCodeField;
            }
            set
            {
                this.courtCodeField = value;
            }
        }

        /// <remarks/>

        public string courtDesc
        {
            get
            {
                return this.courtDescField;
            }
            set
            {
                this.courtDescField = value;
            }
        }

        /// <remarks/>

        public string courtTypeDesc
        {
            get
            {
                return this.courtTypeDescField;
            }
            set
            {
                this.courtTypeDescField = value;
            }
        }

        /// <remarks/>

        public string courtType
        {
            get
            {
                return this.courtTypeField;
            }
            set
            {
                this.courtTypeField = value;
            }
        }

        /// <remarks/>

        public string dateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>

        public string defendantName
        {
            get
            {
                return this.defendantNameField;
            }
            set
            {
                this.defendantNameField = value;
            }
        }

        /// <remarks/>

        public string defendantNumber
        {
            get
            {
                return this.defendantNumberField;
            }
            set
            {
                this.defendantNumberField = value;
            }
        }

        /// <remarks/>

        public string iDNo
        {
            get
            {
                return this.iDNoField;
            }
            set
            {
                this.iDNoField = value;
            }
        }

        /// <remarks/>

        public string judgementCode
        {
            get
            {
                return this.judgementCodeField;
            }
            set
            {
                this.judgementCodeField = value;
            }
        }

        /// <remarks/>

        public string judgementDesc
        {
            get
            {
                return this.judgementDescField;
            }
            set
            {
                this.judgementDescField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string masterNo
        {
            get
            {
                return this.masterNoField;
            }
            set
            {
                this.masterNoField = value;
            }
        }

        /// <remarks/>

        public string message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }

        /// <remarks/>

        public string notarialDate
        {
            get
            {
                return this.notarialDateField;
            }
            set
            {
                this.notarialDateField = value;
            }
        }

        /// <remarks/>

        public string plaintiff
        {
            get
            {
                return this.plaintiffField;
            }
            set
            {
                this.plaintiffField = value;
            }
        }

        /// <remarks/>

        public string remarks
        {
            get
            {
                return this.remarksField;
            }
            set
            {
                this.remarksField = value;
            }
        }

        /// <remarks/>

        public string transType
        {
            get
            {
                return this.transTypeField;
            }
            set
            {
                this.transTypeField = value;
            }
        }
    }

    /// <remarks/>





    public partial class ConsumerJudgement : BureauSegment
    {

        private string address1Field;

        private decimal adminMonthlyAmountField;

        private string adminNoMonthsField;

        private string adminStartDateField;

        private decimal amountField;

        private string attorneyNameField;

        private string attorneyPhoneField;

        private string attorneyReferenceField;

        private DateTime captureDateField;

        private string caseNumberField;

        private int consumerNumberField;

        private string courtNameCodeField;

        private string courtNameField;

        private string courtTypeCodeField;

        private string courtTypeField;

        private string dateOfBirthField;

        private string debtCodeField;

        private string debtDescriptionField;

        private string defNoField;

        private string defendantNameField;

        private string iDNoField;

        private string judgementCodeField;

        private DateTime judgementDateField;

        private string judgementDescriptionField;

        private int majorProductField;

        private string masterNumberField;

        private string messageField;

        private string plaintiffNameField;

        private string remarksField;

        private string tradeStyleField;

        private string transTypeField;

        /// <remarks/>

        //public string address1
        //{
        //    get
        //    {
        //        return this.address1Field;
        //    }
        //    set
        //    {
        //        this.address1Field = value;
        //    }
        //}

        /// <remarks/>

        public decimal adminMonthlyAmount
        {
            get
            {
                return this.adminMonthlyAmountField;
            }
            set
            {
                this.adminMonthlyAmountField = value;
            }
        }

        /// <remarks/>

        //public string adminNoMonths
        //{
        //    get
        //    {
        //        return this.adminNoMonthsField;
        //    }
        //    set
        //    {
        //        this.adminNoMonthsField = value;
        //    }
        //}

        ///// <remarks/>

        //public string adminStartDate
        //{
        //    get
        //    {
        //        return this.adminStartDateField;
        //    }
        //    set
        //    {
        //        this.adminStartDateField = value;
        //    }
        //}

        /// <remarks/>

        public decimal amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>

        public string attorneyName
        {
            get
            {
                return this.attorneyNameField;
            }
            set
            {
                this.attorneyNameField = value;
            }
        }

        /// <remarks/>

        public string attorneyPhone
        {
            get
            {
                return this.attorneyPhoneField;
            }
            set
            {
                this.attorneyPhoneField = value;
            }
        }

        /// <remarks/>

        //public string attorneyReference
        //{
        //    get
        //    {
        //        return this.attorneyReferenceField;
        //    }
        //    set
        //    {
        //        this.attorneyReferenceField = value;
        //    }
        //}

        /// <remarks/>

        public DateTime captureDate
        {
            get
            {
                return this.captureDateField;
            }
            set
            {
                this.captureDateField = value;
            }
        }

        /// <remarks/>

        public string caseNumber
        {
            get
            {
                return this.caseNumberField;
            }
            set
            {
                this.caseNumberField = value;
            }
        }

        /// <remarks/>

        public int consumerNumber
        {
            get
            {
                return this.consumerNumberField;
            }
            set
            {
                this.consumerNumberField = value;
            }
        }

        /// <remarks/>

        //public string courtNameCode
        //{
        //    get
        //    {
        //        return this.courtNameCodeField;
        //    }
        //    set
        //    {
        //        this.courtNameCodeField = value;
        //    }
        //}

        /// <remarks/>

        public string courtName
        {
            get
            {
                return this.courtNameField;
            }
            set
            {
                this.courtNameField = value;
            }
        }

        /// <remarks/>

        //public string courtTypeCode
        //{
        //    get
        //    {
        //        return this.courtTypeCodeField;
        //    }
        //    set
        //    {
        //        this.courtTypeCodeField = value;
        //    }
        //}

        ///// <remarks/>

        //public string courtType
        //{
        //    get
        //    {
        //        return this.courtTypeField;
        //    }
        //    set
        //    {
        //        this.courtTypeField = value;
        //    }
        //}

        ///// <remarks/>

        //public string dateOfBirth
        //{
        //    get
        //    {
        //        return this.dateOfBirthField;
        //    }
        //    set
        //    {
        //        this.dateOfBirthField = value;
        //    }
        //}

        ///// <remarks/>

        //public string debtCode
        //{
        //    get
        //    {
        //        return this.debtCodeField;
        //    }
        //    set
        //    {
        //        this.debtCodeField = value;
        //    }
        //}

        ///// <remarks/>

        public string debtDescription
        {
            get
            {
                return this.debtDescriptionField;
            }
            set
            {
                this.debtDescriptionField = value;
            }
        }

        ///// <remarks/>

        //public string defNo
        //{
        //    get
        //    {
        //        return this.defNoField;
        //    }
        //    set
        //    {
        //        this.defNoField = value;
        //    }
        //}

        ///// <remarks/>

        //public string defendantName
        //{
        //    get
        //    {
        //        return this.defendantNameField;
        //    }
        //    set
        //    {
        //        this.defendantNameField = value;
        //    }
        //}

        /// <remarks/>

        public string iDNo
        {
            get
            {
                return this.iDNoField;
            }
            set
            {
                this.iDNoField = value;
            }
        }

        /// <remarks/>

        //public string judgementCode
        //{
        //    get
        //    {
        //        return this.judgementCodeField;
        //    }
        //    set
        //    {
        //        this.judgementCodeField = value;
        //    }
        //}

        /// <remarks/>

        public DateTime judgementDate
        {
            get
            {
                return this.judgementDateField;
            }
            set
            {
                this.judgementDateField = value;
            }
        }

        /// <remarks/>

        public string judgementDescription
        {
            get
            {
                return this.judgementDescriptionField;
            }
            set
            {
                this.judgementDescriptionField = value;
            }
        }

        /// <remarks/>

        public int majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        //public string masterNumber
        //{
        //    get
        //    {
        //        return this.masterNumberField;
        //    }
        //    set
        //    {
        //        this.masterNumberField = value;
        //    }
        //}

        ///// <remarks/>

        //public string message
        //{
        //    get
        //    {
        //        return this.messageField;
        //    }
        //    set
        //    {
        //        this.messageField = value;
        //    }
        //}

        ///// <remarks/>

        public string plaintiffName
        {
            get
            {
                return this.plaintiffNameField;
            }
            set
            {
                this.plaintiffNameField = value;
            }
        }

        ///// <remarks/>

        //public string remarks
        //{
        //    get
        //    {
        //        return this.remarksField;
        //    }
        //    set
        //    {
        //        this.remarksField = value;
        //    }
        //}

        ///// <remarks/>

        //public string tradeStyle
        //{
        //    get
        //    {
        //        return this.tradeStyleField;
        //    }
        //    set
        //    {
        //        this.tradeStyleField = value;
        //    }
        //}

        ///// <remarks/>

        //public string transType
        //{
        //    get
        //    {
        //        return this.transTypeField;
        //    }
        //    set
        //    {
        //        this.transTypeField = value;
        //    }
        //}
    }

    /// <remarks/>





    public partial class ConsumerInfoNO04 : BureauSegment
    {

        private int consumerNoField;

        private DateTime dateOfBirthField;

        private DateTime deceasedDateField;

        private string dependantsField;

        private string forename1Field;

        private string forename2Field;

        private string forename3Field;

        private string genderField;

        private string identityNo1Field;

        private string identityNo2Field;

        private string maritalStatusCodeField;

        private string maritalStatusDescField;

        private DateTime nameInfoDateField;

        private string partField;

        private string partSeqField;

        private int recordSeqField;

        private string spouseName1Field;

        private string spouseName2Field;

        private string surnameField;

        private string telephoneNumbersField;

        private string titleField;

        /// <remarks/>

        public int consumerNo
        {
            get
            {
                return this.consumerNoField;
            }
            set
            {
                this.consumerNoField = value;
            }
        }

        /// <remarks/>

        public DateTime dateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>

        public DateTime deceasedDate
        {
            get
            {
                return this.deceasedDateField;
            }
            set
            {
                this.deceasedDateField = value;
            }
        }

        /// <remarks/>

        //public string dependants
        //{
        //    get
        //    {
        //        return this.dependantsField;
        //    }
        //    set
        //    {
        //        this.dependantsField = value;
        //    }
        //}

        /// <remarks/>

        public string forename1
        {
            get
            {
                return this.forename1Field;
            }
            set
            {
                this.forename1Field = value;
            }
        }

        /// <remarks/>

        public string forename2
        {
            get
            {
                return this.forename2Field;
            }
            set
            {
                this.forename2Field = value;
            }
        }

        /// <remarks/>

        //public string forename3
        //{
        //    get
        //    {
        //        return this.forename3Field;
        //    }
        //    set
        //    {
        //        this.forename3Field = value;
        //    }
        //}

        /// <remarks/>

        public string gender
        {
            get
            {
                return this.genderField;
            }
            set
            {
                this.genderField = value;
            }
        }

        /// <remarks/>

        public string identityNo1
        {
            get
            {
                return this.identityNo1Field;
            }
            set
            {
                this.identityNo1Field = value;
            }
        }

        /// <remarks/>

        public string identityNo2
        {
            get
            {
                return this.identityNo2Field;
            }
            set
            {
                this.identityNo2Field = value;
            }
        }

        /// <remarks/>

        //public string maritalStatusCode
        //{
        //    get
        //    {
        //        return this.maritalStatusCodeField;
        //    }
        //    set
        //    {
        //        this.maritalStatusCodeField = value;
        //    }
        //}

        /// <remarks/>

        public string maritalStatusDesc
        {
            get
            {
                return this.maritalStatusDescField;
            }
            set
            {
                this.maritalStatusDescField = value;
            }
        }

        /// <remarks/>

        public DateTime nameInfoDate
        {
            get
            {
                return this.nameInfoDateField;
            }
            set
            {
                this.nameInfoDateField = value;
            }
        }

        /// <remarks/>

        public string part
        {
            get
            {
                return this.partField;
            }
            set
            {
                this.partField = value;
            }
        }

        /// <remarks/>

        public string partSeq
        {
            get
            {
                return this.partSeqField;
            }
            set
            {
                this.partSeqField = value;
            }
        }

        /// <remarks/>

        public int recordSeq
        {
            get
            {
                return this.recordSeqField;
            }
            set
            {
                this.recordSeqField = value;
            }
        }

        /// <remarks/>

        public string spouseName1
        {
            get
            {
                return this.spouseName1Field;
            }
            set
            {
                this.spouseName1Field = value;
            }
        }

        /// <remarks/>

        public string spouseName2
        {
            get
            {
                return this.spouseName2Field;
            }
            set
            {
                this.spouseName2Field = value;
            }
        }

        /// <remarks/>

        public string surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>

        public string telephoneNumbers
        {
            get
            {
                return this.telephoneNumbersField;
            }
            set
            {
                this.telephoneNumbersField = value;
            }
        }

        /// <remarks/>

        public string title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }
    }

    /// <remarks/>





    public partial class ConsumerHeader : BureauSegment
    {

        private string aKADate1Field;

        private string aKADate2Field;

        private string aKADate3Field;

        private string aKAName1Field;

        private string aKAName2Field;

        private string aKAName3Field;

        private string address1Field;

        private string address2Field;

        private string address3Field;

        private string address4Field;

        private string addressDate1Field;

        private string addressDate2Field;

        private string addressDate3Field;

        private string addressDate4Field;

        private string consumerNumberField;

        private string dateOfBirthField;

        private string debtCouncilDateField;

        private string disputeDateField;

        private string employer1Field;

        private string employer2Field;

        private string employer3Field;

        private string employmentDate1Field;

        private string employmentDate2Field;

        private string employmentDate3Field;

        private string forename1Field;

        private string forename2Field;

        private string forename3Field;

        private string homePhoneCodeField;

        private string homePhoneNumberField;

        private string iDNo1Field;

        private string iDNo2Field;

        private string majorProductField;

        private string matchField;

        private string messageField;

        private string occupation1Field;

        private string occupation2Field;

        private string occupation3Field;

        private string remarksField;

        private string spouseForenameField;

        private string surnameField;

        private string ticketNumberField;

        private string workPhoneCodeField;

        private string workPhoneNumberField;

        /// <remarks/>

        public string aKADate1
        {
            get
            {
                return this.aKADate1Field;
            }
            set
            {
                this.aKADate1Field = value;
            }
        }

        /// <remarks/>

        public string aKADate2
        {
            get
            {
                return this.aKADate2Field;
            }
            set
            {
                this.aKADate2Field = value;
            }
        }

        /// <remarks/>

        public string aKADate3
        {
            get
            {
                return this.aKADate3Field;
            }
            set
            {
                this.aKADate3Field = value;
            }
        }

        /// <remarks/>

        public string aKAName1
        {
            get
            {
                return this.aKAName1Field;
            }
            set
            {
                this.aKAName1Field = value;
            }
        }

        /// <remarks/>

        public string aKAName2
        {
            get
            {
                return this.aKAName2Field;
            }
            set
            {
                this.aKAName2Field = value;
            }
        }

        /// <remarks/>

        public string aKAName3
        {
            get
            {
                return this.aKAName3Field;
            }
            set
            {
                this.aKAName3Field = value;
            }
        }

        /// <remarks/>

        public string address1
        {
            get
            {
                return this.address1Field;
            }
            set
            {
                this.address1Field = value;
            }
        }

        /// <remarks/>

        public string address2
        {
            get
            {
                return this.address2Field;
            }
            set
            {
                this.address2Field = value;
            }
        }

        /// <remarks/>

        public string address3
        {
            get
            {
                return this.address3Field;
            }
            set
            {
                this.address3Field = value;
            }
        }

        /// <remarks/>

        public string address4
        {
            get
            {
                return this.address4Field;
            }
            set
            {
                this.address4Field = value;
            }
        }

        /// <remarks/>

        public string addressDate1
        {
            get
            {
                return this.addressDate1Field;
            }
            set
            {
                this.addressDate1Field = value;
            }
        }

        /// <remarks/>

        public string addressDate2
        {
            get
            {
                return this.addressDate2Field;
            }
            set
            {
                this.addressDate2Field = value;
            }
        }

        /// <remarks/>

        public string addressDate3
        {
            get
            {
                return this.addressDate3Field;
            }
            set
            {
                this.addressDate3Field = value;
            }
        }

        /// <remarks/>

        public string addressDate4
        {
            get
            {
                return this.addressDate4Field;
            }
            set
            {
                this.addressDate4Field = value;
            }
        }

        /// <remarks/>

        public string consumerNumber
        {
            get
            {
                return this.consumerNumberField;
            }
            set
            {
                this.consumerNumberField = value;
            }
        }

        /// <remarks/>

        public string dateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>

        public string debtCouncilDate
        {
            get
            {
                return this.debtCouncilDateField;
            }
            set
            {
                this.debtCouncilDateField = value;
            }
        }

        /// <remarks/>

        public string disputeDate
        {
            get
            {
                return this.disputeDateField;
            }
            set
            {
                this.disputeDateField = value;
            }
        }

        /// <remarks/>

        public string employer1
        {
            get
            {
                return this.employer1Field;
            }
            set
            {
                this.employer1Field = value;
            }
        }

        /// <remarks/>

        public string employer2
        {
            get
            {
                return this.employer2Field;
            }
            set
            {
                this.employer2Field = value;
            }
        }

        /// <remarks/>

        public string employer3
        {
            get
            {
                return this.employer3Field;
            }
            set
            {
                this.employer3Field = value;
            }
        }

        /// <remarks/>

        public string employmentDate1
        {
            get
            {
                return this.employmentDate1Field;
            }
            set
            {
                this.employmentDate1Field = value;
            }
        }

        /// <remarks/>

        public string employmentDate2
        {
            get
            {
                return this.employmentDate2Field;
            }
            set
            {
                this.employmentDate2Field = value;
            }
        }

        /// <remarks/>

        public string employmentDate3
        {
            get
            {
                return this.employmentDate3Field;
            }
            set
            {
                this.employmentDate3Field = value;
            }
        }

        /// <remarks/>

        public string forename1
        {
            get
            {
                return this.forename1Field;
            }
            set
            {
                this.forename1Field = value;
            }
        }

        /// <remarks/>

        public string forename2
        {
            get
            {
                return this.forename2Field;
            }
            set
            {
                this.forename2Field = value;
            }
        }

        /// <remarks/>

        public string forename3
        {
            get
            {
                return this.forename3Field;
            }
            set
            {
                this.forename3Field = value;
            }
        }

        /// <remarks/>

        public string homePhoneCode
        {
            get
            {
                return this.homePhoneCodeField;
            }
            set
            {
                this.homePhoneCodeField = value;
            }
        }

        /// <remarks/>

        public string homePhoneNumber
        {
            get
            {
                return this.homePhoneNumberField;
            }
            set
            {
                this.homePhoneNumberField = value;
            }
        }

        /// <remarks/>

        public string iDNo1
        {
            get
            {
                return this.iDNo1Field;
            }
            set
            {
                this.iDNo1Field = value;
            }
        }

        /// <remarks/>

        public string iDNo2
        {
            get
            {
                return this.iDNo2Field;
            }
            set
            {
                this.iDNo2Field = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string match
        {
            get
            {
                return this.matchField;
            }
            set
            {
                this.matchField = value;
            }
        }

        /// <remarks/>

        public string message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }

        /// <remarks/>

        public string occupation1
        {
            get
            {
                return this.occupation1Field;
            }
            set
            {
                this.occupation1Field = value;
            }
        }

        /// <remarks/>

        public string occupation2
        {
            get
            {
                return this.occupation2Field;
            }
            set
            {
                this.occupation2Field = value;
            }
        }

        /// <remarks/>

        public string occupation3
        {
            get
            {
                return this.occupation3Field;
            }
            set
            {
                this.occupation3Field = value;
            }
        }

        /// <remarks/>

        public string remarks
        {
            get
            {
                return this.remarksField;
            }
            set
            {
                this.remarksField = value;
            }
        }

        /// <remarks/>

        public string spouseForename
        {
            get
            {
                return this.spouseForenameField;
            }
            set
            {
                this.spouseForenameField = value;
            }
        }

        /// <remarks/>

        public string surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>

        public string ticketNumber
        {
            get
            {
                return this.ticketNumberField;
            }
            set
            {
                this.ticketNumberField = value;
            }
        }

        /// <remarks/>

        public string workPhoneCode
        {
            get
            {
                return this.workPhoneCodeField;
            }
            set
            {
                this.workPhoneCodeField = value;
            }
        }

        /// <remarks/>

        public string workPhoneNumber
        {
            get
            {
                return this.workPhoneNumberField;
            }
            set
            {
                this.workPhoneNumberField = value;
            }
        }
    }

    /// <remarks/>





    public partial class ConsumerHeaderC1 : BureauSegment
    {

        private string deceasedDateField;

        private string entryDateField;

        private string maritalStatusField;

        private string segmentCodeField;

        private string telHomeCode1Field;

        private string telHomeCode2Field;

        private string telHomeNumber1Field;

        private string telHomeNumber2Field;

        private string telWorkCode1Field;

        private string telWorkCode2Field;

        private string telWorkNumber1Field;

        private string telWorkNumber2Field;

        /// <remarks/>

        public string deceasedDate
        {
            get
            {
                return this.deceasedDateField;
            }
            set
            {
                this.deceasedDateField = value;
            }
        }

        /// <remarks/>

        public string entryDate
        {
            get
            {
                return this.entryDateField;
            }
            set
            {
                this.entryDateField = value;
            }
        }

        /// <remarks/>

        public string maritalStatus
        {
            get
            {
                return this.maritalStatusField;
            }
            set
            {
                this.maritalStatusField = value;
            }
        }

        /// <remarks/>

        public string segmentCode
        {
            get
            {
                return this.segmentCodeField;
            }
            set
            {
                this.segmentCodeField = value;
            }
        }

        /// <remarks/>

        public string telHomeCode1
        {
            get
            {
                return this.telHomeCode1Field;
            }
            set
            {
                this.telHomeCode1Field = value;
            }
        }

        /// <remarks/>

        public string telHomeCode2
        {
            get
            {
                return this.telHomeCode2Field;
            }
            set
            {
                this.telHomeCode2Field = value;
            }
        }

        /// <remarks/>

        public string telHomeNumber1
        {
            get
            {
                return this.telHomeNumber1Field;
            }
            set
            {
                this.telHomeNumber1Field = value;
            }
        }

        /// <remarks/>

        public string telHomeNumber2
        {
            get
            {
                return this.telHomeNumber2Field;
            }
            set
            {
                this.telHomeNumber2Field = value;
            }
        }

        /// <remarks/>

        public string telWorkCode1
        {
            get
            {
                return this.telWorkCode1Field;
            }
            set
            {
                this.telWorkCode1Field = value;
            }
        }

        /// <remarks/>

        public string telWorkCode2
        {
            get
            {
                return this.telWorkCode2Field;
            }
            set
            {
                this.telWorkCode2Field = value;
            }
        }

        /// <remarks/>

        public string telWorkNumber1
        {
            get
            {
                return this.telWorkNumber1Field;
            }
            set
            {
                this.telWorkNumber1Field = value;
            }
        }

        /// <remarks/>

        public string telWorkNumber2
        {
            get
            {
                return this.telWorkNumber2Field;
            }
            set
            {
                this.telWorkNumber2Field = value;
            }
        }
    }

    /// <remarks/>





    public partial class ConsumerEnquiry : BureauSegment
    {

        private string consumerNoField;

        private string contactField;

        private string enquiryDateField;

        private string majorProductField;

        private string subscriberField;

        private string transTypeField;

        private string typeField;

        /// <remarks/>

        public string consumerNo
        {
            get
            {
                return this.consumerNoField;
            }
            set
            {
                this.consumerNoField = value;
            }
        }

        /// <remarks/>

        public string contact
        {
            get
            {
                return this.contactField;
            }
            set
            {
                this.contactField = value;
            }
        }

        /// <remarks/>

        public string enquiryDate
        {
            get
            {
                return this.enquiryDateField;
            }
            set
            {
                this.enquiryDateField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string subscriber
        {
            get
            {
                return this.subscriberField;
            }
            set
            {
                this.subscriberField = value;
            }
        }

        /// <remarks/>

        public string transType
        {
            get
            {
                return this.transTypeField;
            }
            set
            {
                this.transTypeField = value;
            }
        }

        /// <remarks/>

        public string type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }
    }

    /// <remarks/>





    public partial class ConsumerDefault : BureauSegment
    {

        private string accountF700Field;

        private string accountField;

        private string addressField;

        private string amountF700Field;

        private string amountField;

        private string branchCodeField;

        private string consumerNoField;

        private string contactNameField;

        private string contactPhoneCodeField;

        private string contactPhoneNoField;

        private string dateOfBirthField;

        private string debtCodeField;

        private string debtDescField;

        private string defaultDateField;

        private string fnDefaultField;

        private string iDNoField;

        private string majorProductField;

        private string messageField;

        private string nameField;

        private string remarksField;

        private string remarksXField;

        private string remarksYField;

        private string subAccountField;

        private string subscriberNameField;

        private string transTypeField;

        /// <remarks/>

        //public string accountF700
        //{
        //    get
        //    {
        //        return this.accountF700Field;
        //    }
        //    set
        //    {
        //        this.accountF700Field = value;
        //    }
        //}

        /// <remarks/>

        public string account
        {
            get
            {
                return this.accountField;
            }
            set
            {
                this.accountField = value;
            }
        }

        /// <remarks/>

        //public string address
        //{
        //    get
        //    {
        //        return this.addressField;
        //    }
        //    set
        //    {
        //        this.addressField = value;
        //    }
        //}

        ///// <remarks/>

        //public string amountF700
        //{
        //    get
        //    {
        //        return this.amountF700Field;
        //    }
        //    set
        //    {
        //        this.amountF700Field = value;
        //    }
        //}

        /// <remarks/>

        public string amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>

        //public string branchCode
        //{
        //    get
        //    {
        //        return this.branchCodeField;
        //    }
        //    set
        //    {
        //        this.branchCodeField = value;
        //    }
        //}

        ///// <remarks/>

        //public string consumerNo
        //{
        //    get
        //    {
        //        return this.consumerNoField;
        //    }
        //    set
        //    {
        //        this.consumerNoField = value;
        //    }
        //}

        ///// <remarks/>

        //public string contactName
        //{
        //    get
        //    {
        //        return this.contactNameField;
        //    }
        //    set
        //    {
        //        this.contactNameField = value;
        //    }
        //}

        ///// <remarks/>

        //public string contactPhoneCode
        //{
        //    get
        //    {
        //        return this.contactPhoneCodeField;
        //    }
        //    set
        //    {
        //        this.contactPhoneCodeField = value;
        //    }
        //}

        ///// <remarks/>

        //public string contactPhoneNo
        //{
        //    get
        //    {
        //        return this.contactPhoneNoField;
        //    }
        //    set
        //    {
        //        this.contactPhoneNoField = value;
        //    }
        //}

        ///// <remarks/>

        //public string dateOfBirth
        //{
        //    get
        //    {
        //        return this.dateOfBirthField;
        //    }
        //    set
        //    {
        //        this.dateOfBirthField = value;
        //    }
        //}

        ///// <remarks/>

        //public string debtCode
        //{
        //    get
        //    {
        //        return this.debtCodeField;
        //    }
        //    set
        //    {
        //        this.debtCodeField = value;
        //    }
        //}

        /// <remarks/>

        public string debtDesc
        {
            get
            {
                return this.debtDescField;
            }
            set
            {
                this.debtDescField = value;
            }
        }

        /// <remarks/>

        public string defaultDate
        {
            get
            {
                return this.defaultDateField;
            }
            set
            {
                this.defaultDateField = value;
            }
        }

        /// <remarks/>

        //public string fnDefault
        //{
        //    get
        //    {
        //        return this.fnDefaultField;
        //    }
        //    set
        //    {
        //        this.fnDefaultField = value;
        //    }
        //}

        /// <remarks/>

        //public string iDNo
        //{
        //    get
        //    {
        //        return this.iDNoField;
        //    }
        //    set
        //    {
        //        this.iDNoField = value;
        //    }
        //}

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        //public string message
        //{
        //    get
        //    {
        //        return this.messageField;
        //    }
        //    set
        //    {
        //        this.messageField = value;
        //    }
        //}

        /// <remarks/>

        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>

        public string remarks
        {
            get
            {
                return this.remarksField;
            }
            set
            {
                this.remarksField = value;
            }
        }

        /// <remarks/>

        //public string remarksX
        //{
        //    get
        //    {
        //        return this.remarksXField;
        //    }
        //    set
        //    {
        //        this.remarksXField = value;
        //    }
        //}

        ///// <remarks/>

        //public string remarksY
        //{
        //    get
        //    {
        //        return this.remarksYField;
        //    }
        //    set
        //    {
        //        this.remarksYField = value;
        //    }
        //}

        ///// <remarks/>

        //public string subAccount
        //{
        //    get
        //    {
        //        return this.subAccountField;
        //    }
        //    set
        //    {
        //        this.subAccountField = value;
        //    }
        //}

        /// <remarks/>

        public string subscriberName
        {
            get
            {
                return this.subscriberNameField;
            }
            set
            {
                this.subscriberNameField = value;
            }
        }

        /// <remarks/>

        //public string transType
        //{
        //    get
        //    {
        //        return this.transTypeField;
        //    }
        //    set
        //    {
        //        this.transTypeField = value;
        //    }
        //}
    }

    /// <remarks/>





    public partial class ConsumerBusinessEnquiry : BureauSegment
    {

        private string consumerNoField;

        private string contactField;

        private string enquiryDateField;

        private string majorProductField;

        private string subscriberField;

        private string transTypeField;

        private string typeField;

        /// <remarks/>

        public string consumerNo
        {
            get
            {
                return this.consumerNoField;
            }
            set
            {
                this.consumerNoField = value;
            }
        }

        /// <remarks/>

        public string contact
        {
            get
            {
                return this.contactField;
            }
            set
            {
                this.contactField = value;
            }
        }

        /// <remarks/>

        public string enquiryDate
        {
            get
            {
                return this.enquiryDateField;
            }
            set
            {
                this.enquiryDateField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string subscriber
        {
            get
            {
                return this.subscriberField;
            }
            set
            {
                this.subscriberField = value;
            }
        }

        /// <remarks/>

        public string transType
        {
            get
            {
                return this.transTypeField;
            }
            set
            {
                this.transTypeField = value;
            }
        }

        /// <remarks/>

        public string type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }
    }

    /// <remarks/>





    public partial class ComplianceIndicatorLI : BureauSegment
    {

        private string cPAComplianceIndField;

        private string dateField;

        private string iTNumberField;

        private string majorProductField;

        private string nCAComplianceIndField;

        private string segmentTypeField;

        /// <remarks/>

        public string cPAComplianceInd
        {
            get
            {
                return this.cPAComplianceIndField;
            }
            set
            {
                this.cPAComplianceIndField = value;
            }
        }

        /// <remarks/>

        public string date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>

        public string iTNumber
        {
            get
            {
                return this.iTNumberField;
            }
            set
            {
                this.iTNumberField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string nCAComplianceInd
        {
            get
            {
                return this.nCAComplianceIndField;
            }
            set
            {
                this.nCAComplianceIndField = value;
            }
        }

        /// <remarks/>

        public string segmentType
        {
            get
            {
                return this.segmentTypeField;
            }
            set
            {
                this.segmentTypeField = value;
            }
        }
    }

    /// <remarks/>





    public partial class CommercialDisputeResponse : BureauSegment
    {

        private string responseCodeField;

        private string responseDescField;

        /// <remarks/>

        public string responseCode
        {
            get
            {
                return this.responseCodeField;
            }
            set
            {
                this.responseCodeField = value;
            }
        }

        /// <remarks/>

        public string responseDesc
        {
            get
            {
                return this.responseDescField;
            }
            set
            {
                this.responseDescField = value;
            }
        }
    }

    /// <remarks/>





    public partial class CapitalEmployed : BureauSegment
    {

        private string[] capitalEmployedItemAmountField;

        private string[] capitalEmployedItemField;

        private string capitalEmployedTotalField;

        private string majorProductField;

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] capitalEmployedItemAmount
        {
            get
            {
                return this.capitalEmployedItemAmountField;
            }
            set
            {
                this.capitalEmployedItemAmountField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] capitalEmployedItem
        {
            get
            {
                return this.capitalEmployedItemField;
            }
            set
            {
                this.capitalEmployedItemField = value;
            }
        }

        /// <remarks/>

        public string capitalEmployedTotal
        {
            get
            {
                return this.capitalEmployedTotalField;
            }
            set
            {
                this.capitalEmployedTotalField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }
    }

    /// <remarks/>





    public partial class CancelledTicket : BureauSegment
    {

        private string[] cancelledStatusField;

        private string[] statusField;

        private string[] subjectNameField;

        private string[] ticketField;

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] cancelledStatus
        {
            get
            {
                return this.cancelledStatusField;
            }
            set
            {
                this.cancelledStatusField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] subjectName
        {
            get
            {
                return this.subjectNameField;
            }
            set
            {
                this.subjectNameField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] ticket
        {
            get
            {
                return this.ticketField;
            }
            set
            {
                this.ticketField = value;
            }
        }
    }

    /// <remarks/>





    public partial class BusinessBenchmarkBB : BureauSegment
    {

        private string avg_ScoreField;

        private BusinessBenchmarkDetail[] businessBenchmarkDetailField;

        private string majorProductField;

        private string rPT_DateField;

        private string ratingField;

        private string sASicCodeField;

        private string sME_ScoreField;

        private string sicDescField;

        private string statusField;

        private string uSSicCodeField;

        /// <remarks/>

        public string avg_Score
        {
            get
            {
                return this.avg_ScoreField;
            }
            set
            {
                this.avg_ScoreField = value;
            }
        }

        /// <remarks/>

        public BusinessBenchmarkDetail[] businessBenchmarkDetail
        {
            get
            {
                return this.businessBenchmarkDetailField;
            }
            set
            {
                this.businessBenchmarkDetailField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string rPT_Date
        {
            get
            {
                return this.rPT_DateField;
            }
            set
            {
                this.rPT_DateField = value;
            }
        }

        /// <remarks/>

        public string rating
        {
            get
            {
                return this.ratingField;
            }
            set
            {
                this.ratingField = value;
            }
        }

        /// <remarks/>

        public string sASicCode
        {
            get
            {
                return this.sASicCodeField;
            }
            set
            {
                this.sASicCodeField = value;
            }
        }

        /// <remarks/>

        public string sME_Score
        {
            get
            {
                return this.sME_ScoreField;
            }
            set
            {
                this.sME_ScoreField = value;
            }
        }

        /// <remarks/>

        public string sicDesc
        {
            get
            {
                return this.sicDescField;
            }
            set
            {
                this.sicDescField = value;
            }
        }

        /// <remarks/>

        public string status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>

        public string uSSicCode
        {
            get
            {
                return this.uSSicCodeField;
            }
            set
            {
                this.uSSicCodeField = value;
            }
        }
    }

    /// <remarks/>





    public partial class BusinessRescueDetail : BureauSegment
    {

        private string businessRescueCommentField;

        private string businessRescueDateField;

        private string businessRescueTypeField;

        private string endDateField;

        private string majorProductField;

        private string practitionerContactDetailsField;

        private string practitionerDateField;

        private string practitionerNameField;

        private string registrationInfoDateField;

        private string registrationStatusCodeField;

        private string rescuePlanField;

        /// <remarks/>

        public string businessRescueComment
        {
            get
            {
                return this.businessRescueCommentField;
            }
            set
            {
                this.businessRescueCommentField = value;
            }
        }

        /// <remarks/>

        public string businessRescueDate
        {
            get
            {
                return this.businessRescueDateField;
            }
            set
            {
                this.businessRescueDateField = value;
            }
        }

        /// <remarks/>

        public string businessRescueType
        {
            get
            {
                return this.businessRescueTypeField;
            }
            set
            {
                this.businessRescueTypeField = value;
            }
        }

        /// <remarks/>

        public string endDate
        {
            get
            {
                return this.endDateField;
            }
            set
            {
                this.endDateField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string practitionerContactDetails
        {
            get
            {
                return this.practitionerContactDetailsField;
            }
            set
            {
                this.practitionerContactDetailsField = value;
            }
        }

        /// <remarks/>

        public string practitionerDate
        {
            get
            {
                return this.practitionerDateField;
            }
            set
            {
                this.practitionerDateField = value;
            }
        }

        /// <remarks/>

        public string practitionerName
        {
            get
            {
                return this.practitionerNameField;
            }
            set
            {
                this.practitionerNameField = value;
            }
        }

        /// <remarks/>

        public string registrationInfoDate
        {
            get
            {
                return this.registrationInfoDateField;
            }
            set
            {
                this.registrationInfoDateField = value;
            }
        }

        /// <remarks/>

        public string registrationStatusCode
        {
            get
            {
                return this.registrationStatusCodeField;
            }
            set
            {
                this.registrationStatusCodeField = value;
            }
        }

        /// <remarks/>

        public string rescuePlan
        {
            get
            {
                return this.rescuePlanField;
            }
            set
            {
                this.rescuePlanField = value;
            }
        }
    }

    /// <remarks/>





    public partial class BusinessPlusPrincipalSummary : BureauSegment
    {

        private string adverseCountField;

        private string debtCounselingField;

        private string deedsCountField;

        private string forename1Field;

        private string forename2Field;

        private string forename3Field;

        private string iDNumberField;

        private string judgementCountField;

        private string majorProductField;

        private string noticesCountField;

        private string numberOfBusinessesField;

        private string numberOfNonAdverseNotorialBondsField;

        private string surnameField;

        /// <remarks/>

        public string adverseCount
        {
            get
            {
                return this.adverseCountField;
            }
            set
            {
                this.adverseCountField = value;
            }
        }

        /// <remarks/>

        public string debtCounseling
        {
            get
            {
                return this.debtCounselingField;
            }
            set
            {
                this.debtCounselingField = value;
            }
        }

        /// <remarks/>

        public string deedsCount
        {
            get
            {
                return this.deedsCountField;
            }
            set
            {
                this.deedsCountField = value;
            }
        }

        /// <remarks/>

        public string forename1
        {
            get
            {
                return this.forename1Field;
            }
            set
            {
                this.forename1Field = value;
            }
        }

        /// <remarks/>

        public string forename2
        {
            get
            {
                return this.forename2Field;
            }
            set
            {
                this.forename2Field = value;
            }
        }

        /// <remarks/>

        public string forename3
        {
            get
            {
                return this.forename3Field;
            }
            set
            {
                this.forename3Field = value;
            }
        }

        /// <remarks/>

        public string iDNumber
        {
            get
            {
                return this.iDNumberField;
            }
            set
            {
                this.iDNumberField = value;
            }
        }

        /// <remarks/>

        public string judgementCount
        {
            get
            {
                return this.judgementCountField;
            }
            set
            {
                this.judgementCountField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string noticesCount
        {
            get
            {
                return this.noticesCountField;
            }
            set
            {
                this.noticesCountField = value;
            }
        }

        /// <remarks/>

        public string numberOfBusinesses
        {
            get
            {
                return this.numberOfBusinessesField;
            }
            set
            {
                this.numberOfBusinessesField = value;
            }
        }

        /// <remarks/>

        public string numberOfNonAdverseNotorialBonds
        {
            get
            {
                return this.numberOfNonAdverseNotorialBondsField;
            }
            set
            {
                this.numberOfNonAdverseNotorialBondsField = value;
            }
        }

        /// <remarks/>

        public string surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }
    }

    /// <remarks/>





    public partial class BusinessNotarialBonds : BureauSegment
    {

        private string bondAmountField;

        private string bondNumberField;

        private string bondOwnerField;

        private string bondPercentageField;

        private string bondRegDateField;

        private string bondTypeField;

        private string deedsOfficeField;

        private string hOAboveThresholdField;

        private string[] heldOverField;

        private string majorProductField;

        private string messageField;

        private string microfilemRefField;

        private string multipleOwnersField;

        private string ownerIDField;

        private string ownerTypeField;

        private string parentChildIndField;

        /// <remarks/>

        public string bondAmount
        {
            get
            {
                return this.bondAmountField;
            }
            set
            {
                this.bondAmountField = value;
            }
        }

        /// <remarks/>

        public string bondNumber
        {
            get
            {
                return this.bondNumberField;
            }
            set
            {
                this.bondNumberField = value;
            }
        }

        /// <remarks/>

        public string bondOwner
        {
            get
            {
                return this.bondOwnerField;
            }
            set
            {
                this.bondOwnerField = value;
            }
        }

        /// <remarks/>

        public string bondPercentage
        {
            get
            {
                return this.bondPercentageField;
            }
            set
            {
                this.bondPercentageField = value;
            }
        }

        /// <remarks/>

        public string bondRegDate
        {
            get
            {
                return this.bondRegDateField;
            }
            set
            {
                this.bondRegDateField = value;
            }
        }

        /// <remarks/>

        public string bondType
        {
            get
            {
                return this.bondTypeField;
            }
            set
            {
                this.bondTypeField = value;
            }
        }

        /// <remarks/>

        public string deedsOffice
        {
            get
            {
                return this.deedsOfficeField;
            }
            set
            {
                this.deedsOfficeField = value;
            }
        }

        /// <remarks/>

        public string hOAboveThreshold
        {
            get
            {
                return this.hOAboveThresholdField;
            }
            set
            {
                this.hOAboveThresholdField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] heldOver
        {
            get
            {
                return this.heldOverField;
            }
            set
            {
                this.heldOverField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }

        /// <remarks/>

        public string microfilemRef
        {
            get
            {
                return this.microfilemRefField;
            }
            set
            {
                this.microfilemRefField = value;
            }
        }

        /// <remarks/>

        public string multipleOwners
        {
            get
            {
                return this.multipleOwnersField;
            }
            set
            {
                this.multipleOwnersField = value;
            }
        }

        /// <remarks/>

        public string ownerID
        {
            get
            {
                return this.ownerIDField;
            }
            set
            {
                this.ownerIDField = value;
            }
        }

        /// <remarks/>

        public string ownerType
        {
            get
            {
                return this.ownerTypeField;
            }
            set
            {
                this.ownerTypeField = value;
            }
        }

        /// <remarks/>

        public string parentChildInd
        {
            get
            {
                return this.parentChildIndField;
            }
            set
            {
                this.parentChildIndField = value;
            }
        }
    }

    /// <remarks/>





    public partial class BusinessDeedsSummaryDA : BureauSegment
    {

        private string commentField;

        private string deedsOfficeField;

        private int majorProductField;

        private int numberPropertiesField;

        private string oldestPropertyDateField;

        private string oldestPropertyValueField;

        private decimal totalBondField;

        private int totalOwnedField;

        private decimal totalValueField;

        /// <remarks/>

        public string comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>

        public string deedsOffice
        {
            get
            {
                return this.deedsOfficeField;
            }
            set
            {
                this.deedsOfficeField = value;
            }
        }

        /// <remarks/>

        public int majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public int numberProperties
        {
            get
            {
                return this.numberPropertiesField;
            }
            set
            {
                this.numberPropertiesField = value;
            }
        }

        /// <remarks/>

        //public string oldestPropertyDate
        //{
        //    get
        //    {
        //        return this.oldestPropertyDateField;
        //    }
        //    set
        //    {
        //        this.oldestPropertyDateField = value;
        //    }
        //}

        ///// <remarks/>

        //public string oldestPropertyValue
        //{
        //    get
        //    {
        //        return this.oldestPropertyValueField;
        //    }
        //    set
        //    {
        //        this.oldestPropertyValueField = value;
        //    }
        //}

        /// <remarks/>

        public decimal totalBond
        {
            get
            {
                return this.totalBondField;
            }
            set
            {
                this.totalBondField = value;
            }
        }

        /// <remarks/>

        public int totalOwned
        {
            get
            {
                return this.totalOwnedField;
            }
            set
            {
                this.totalOwnedField = value;
            }
        }

        /// <remarks/>

        public decimal totalValue
        {
            get
            {
                return this.totalValueField;
            }
            set
            {
                this.totalValueField = value;
            }
        }
    }

    /// <remarks/>





    public partial class BusinessDeedsDI : BureauSegment
    {

        private string bondAmountField;

        private string bondDateField;

        private string bondHolderField;

        private string bondNumberField;

        private string buyerNameField;

        private string commentField;

        private string dateField;

        private string dateOfBirthOrIDNumberField;

        private string deedsOfficeField;

        private string eRFField;

        private string farmField;

        private string majorProductField;

        private string portionField;

        private string propertySizeField;

        private string purchaseDateField;

        private string purchasePriceField;

        private string rowIDField;

        private string titleField;

        private string townshipField;

        /// <remarks/>

        public string bondAmount
        {
            get
            {
                return this.bondAmountField;
            }
            set
            {
                this.bondAmountField = value;
            }
        }

        /// <remarks/>

        public string bondDate
        {
            get
            {
                return this.bondDateField;
            }
            set
            {
                this.bondDateField = value;
            }
        }

        /// <remarks/>

        public string bondHolder
        {
            get
            {
                return this.bondHolderField;
            }
            set
            {
                this.bondHolderField = value;
            }
        }

        /// <remarks/>

        public string bondNumber
        {
            get
            {
                return this.bondNumberField;
            }
            set
            {
                this.bondNumberField = value;
            }
        }

        /// <remarks/>

        public string buyerName
        {
            get
            {
                return this.buyerNameField;
            }
            set
            {
                this.buyerNameField = value;
            }
        }

        /// <remarks/>

        public string comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>

        public string date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>

        public string dateOfBirthOrIDNumber
        {
            get
            {
                return this.dateOfBirthOrIDNumberField;
            }
            set
            {
                this.dateOfBirthOrIDNumberField = value;
            }
        }

        /// <remarks/>

        public string deedsOffice
        {
            get
            {
                return this.deedsOfficeField;
            }
            set
            {
                this.deedsOfficeField = value;
            }
        }

        /// <remarks/>

        public string eRF
        {
            get
            {
                return this.eRFField;
            }
            set
            {
                this.eRFField = value;
            }
        }

        /// <remarks/>

        public string farm
        {
            get
            {
                return this.farmField;
            }
            set
            {
                this.farmField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string portion
        {
            get
            {
                return this.portionField;
            }
            set
            {
                this.portionField = value;
            }
        }

        /// <remarks/>

        public string propertySize
        {
            get
            {
                return this.propertySizeField;
            }
            set
            {
                this.propertySizeField = value;
            }
        }

        /// <remarks/>

        public string purchaseDate
        {
            get
            {
                return this.purchaseDateField;
            }
            set
            {
                this.purchaseDateField = value;
            }
        }

        /// <remarks/>

        public string purchasePrice
        {
            get
            {
                return this.purchasePriceField;
            }
            set
            {
                this.purchasePriceField = value;
            }
        }

        /// <remarks/>

        public string rowID
        {
            get
            {
                return this.rowIDField;
            }
            set
            {
                this.rowIDField = value;
            }
        }

        /// <remarks/>

        public string title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>

        public string township
        {
            get
            {
                return this.townshipField;
            }
            set
            {
                this.townshipField = value;
            }
        }
    }

    /// <remarks/>





    public partial class BusinessDeedsComprehensivePW : BureauSegment
    {

        private string bondAmountField;

        private string bondDateField;

        private string bondNumberField;

        private string buyerNameField;

        private string commentField;

        private string dateField;

        private string dateOfBirthIDField;

        private string deedsOfficeField;

        private string eRFField;

        private string farmField;

        private string majorProductField;

        private string mortgageeField;

        private string multipleField;

        private string portionField;

        private string propertySizeField;

        private string propertyTypeField;

        private string provinceField;

        private string purchaseDateField;

        private string purchasePriceField;

        private string rowIDField;

        private string schemeNameField;

        private string schemeNumberField;

        private string shareField;

        private string streetField;

        private string titleField;

        private string townshipField;

        /// <remarks/>

        public string bondAmount
        {
            get
            {
                return this.bondAmountField;
            }
            set
            {
                this.bondAmountField = value;
            }
        }

        /// <remarks/>

        public string bondDate
        {
            get
            {
                return this.bondDateField;
            }
            set
            {
                this.bondDateField = value;
            }
        }

        /// <remarks/>

        public string bondNumber
        {
            get
            {
                return this.bondNumberField;
            }
            set
            {
                this.bondNumberField = value;
            }
        }

        /// <remarks/>

        public string buyerName
        {
            get
            {
                return this.buyerNameField;
            }
            set
            {
                this.buyerNameField = value;
            }
        }

        /// <remarks/>

        public string comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>

        public string date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>

        public string dateOfBirthID
        {
            get
            {
                return this.dateOfBirthIDField;
            }
            set
            {
                this.dateOfBirthIDField = value;
            }
        }

        /// <remarks/>

        public string deedsOffice
        {
            get
            {
                return this.deedsOfficeField;
            }
            set
            {
                this.deedsOfficeField = value;
            }
        }

        /// <remarks/>

        public string eRF
        {
            get
            {
                return this.eRFField;
            }
            set
            {
                this.eRFField = value;
            }
        }

        /// <remarks/>

        public string farm
        {
            get
            {
                return this.farmField;
            }
            set
            {
                this.farmField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string mortgagee
        {
            get
            {
                return this.mortgageeField;
            }
            set
            {
                this.mortgageeField = value;
            }
        }

        /// <remarks/>

        public string multiple
        {
            get
            {
                return this.multipleField;
            }
            set
            {
                this.multipleField = value;
            }
        }

        /// <remarks/>

        public string portion
        {
            get
            {
                return this.portionField;
            }
            set
            {
                this.portionField = value;
            }
        }

        /// <remarks/>

        public string propertySize
        {
            get
            {
                return this.propertySizeField;
            }
            set
            {
                this.propertySizeField = value;
            }
        }

        /// <remarks/>

        public string propertyType
        {
            get
            {
                return this.propertyTypeField;
            }
            set
            {
                this.propertyTypeField = value;
            }
        }

        /// <remarks/>

        public string province
        {
            get
            {
                return this.provinceField;
            }
            set
            {
                this.provinceField = value;
            }
        }

        /// <remarks/>

        public string purchaseDate
        {
            get
            {
                return this.purchaseDateField;
            }
            set
            {
                this.purchaseDateField = value;
            }
        }

        /// <remarks/>

        public string purchasePrice
        {
            get
            {
                return this.purchasePriceField;
            }
            set
            {
                this.purchasePriceField = value;
            }
        }

        /// <remarks/>

        public string rowID
        {
            get
            {
                return this.rowIDField;
            }
            set
            {
                this.rowIDField = value;
            }
        }

        /// <remarks/>

        public string schemeName
        {
            get
            {
                return this.schemeNameField;
            }
            set
            {
                this.schemeNameField = value;
            }
        }

        /// <remarks/>

        public string schemeNumber
        {
            get
            {
                return this.schemeNumberField;
            }
            set
            {
                this.schemeNumberField = value;
            }
        }

        /// <remarks/>

        public string share
        {
            get
            {
                return this.shareField;
            }
            set
            {
                this.shareField = value;
            }
        }

        /// <remarks/>

        public string street
        {
            get
            {
                return this.streetField;
            }
            set
            {
                this.streetField = value;
            }
        }

        /// <remarks/>

        public string title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>

        public string township
        {
            get
            {
                return this.townshipField;
            }
            set
            {
                this.townshipField = value;
            }
        }
    }

    /// <remarks/>





    public partial class BusinessDeedsComprehensivePB : BureauSegment
    {

        private string bondAmountField;

        private string bondDateField;

        private string bondHolderField;

        private string bondNumberField;

        private string commentField;

        private string dateField;

        private string deedsOfficeField;

        private string eRFField;

        private string farmField;

        private string majorProductField;

        private string portionField;

        private string propertyNameField;

        private string propertySizeField;

        private string purchaseDateField;

        private string purchasePriceField;

        private string registrationNumberField;

        private string rowIDField;

        private string titleField;

        private string townshipField;

        /// <remarks/>

        public string bondAmount
        {
            get
            {
                return this.bondAmountField;
            }
            set
            {
                this.bondAmountField = value;
            }
        }

        /// <remarks/>

        public string bondDate
        {
            get
            {
                return this.bondDateField;
            }
            set
            {
                this.bondDateField = value;
            }
        }

        /// <remarks/>

        public string bondHolder
        {
            get
            {
                return this.bondHolderField;
            }
            set
            {
                this.bondHolderField = value;
            }
        }

        /// <remarks/>

        public string bondNumber
        {
            get
            {
                return this.bondNumberField;
            }
            set
            {
                this.bondNumberField = value;
            }
        }

        /// <remarks/>

        public string comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>

        public string date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>

        public string deedsOffice
        {
            get
            {
                return this.deedsOfficeField;
            }
            set
            {
                this.deedsOfficeField = value;
            }
        }

        /// <remarks/>

        public string eRF
        {
            get
            {
                return this.eRFField;
            }
            set
            {
                this.eRFField = value;
            }
        }

        /// <remarks/>

        public string farm
        {
            get
            {
                return this.farmField;
            }
            set
            {
                this.farmField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string portion
        {
            get
            {
                return this.portionField;
            }
            set
            {
                this.portionField = value;
            }
        }

        /// <remarks/>

        public string propertyName
        {
            get
            {
                return this.propertyNameField;
            }
            set
            {
                this.propertyNameField = value;
            }
        }

        /// <remarks/>

        public string propertySize
        {
            get
            {
                return this.propertySizeField;
            }
            set
            {
                this.propertySizeField = value;
            }
        }

        /// <remarks/>

        public string purchaseDate
        {
            get
            {
                return this.purchaseDateField;
            }
            set
            {
                this.purchaseDateField = value;
            }
        }

        /// <remarks/>

        public string purchasePrice
        {
            get
            {
                return this.purchasePriceField;
            }
            set
            {
                this.purchasePriceField = value;
            }
        }

        /// <remarks/>

        public string registrationNumber
        {
            get
            {
                return this.registrationNumberField;
            }
            set
            {
                this.registrationNumberField = value;
            }
        }

        /// <remarks/>

        public string rowID
        {
            get
            {
                return this.rowIDField;
            }
            set
            {
                this.rowIDField = value;
            }
        }

        /// <remarks/>

        public string title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>

        public string township
        {
            get
            {
                return this.townshipField;
            }
            set
            {
                this.townshipField = value;
            }
        }
    }

    /// <remarks/>





    public partial class BusinessAdverseSummary : BureauSegment
    {

        private string adverse24MonthsPlusCountField;

        private string adverse24MonthsPlusTotalField;

        private string adverseLast12MonthsCountField;

        private string adverseLast12MonthsTotalField;

        private string adverseLast24MonthsCountField;

        private string adverseLast24MonthsTotalField;

        private string adverseLast6MonthsCountField;

        private string adverseLast6MonthsTotalField;

        private string adverseTotalAmountField;

        private string adverseTotalCountField;

        private string judgement24MonthsPlusCountField;

        private string judgement24MonthsPlusTotalField;

        private string judgementLast12MonthsCountField;

        private string judgementLast12MonthsTotalField;

        private string judgementLast24MonthsCountField;

        private string judgementLast24MonthsTotalField;

        private string judgementLast6MonthsCountField;

        private string judgementLast6MonthsTotalField;

        private string judgementTotalAmountField;

        private string judgementTotalCountField;

        private string majorProductField;

        private string messageField;

        private string notarialBond24MonthsPlusCountField;

        private string notarialBond24MonthsPlusTotalField;

        private string notarialBondLast12MonthsCountField;

        private string notarialBondLast12MonthsTotalField;

        private string notarialBondLast24MonthsCountField;

        private string notarialBondLast24MonthsTotalField;

        private string notarialBondLast6MonthsCountField;

        private string notarialBondLast6MonthsTotalField;

        private string notarialBondTotalAmountField;

        private string notarialBondTotalCountField;

        /// <remarks/>

        public string adverse24MonthsPlusCount
        {
            get
            {
                return this.adverse24MonthsPlusCountField;
            }
            set
            {
                this.adverse24MonthsPlusCountField = value;
            }
        }

        /// <remarks/>

        public string adverse24MonthsPlusTotal
        {
            get
            {
                return this.adverse24MonthsPlusTotalField;
            }
            set
            {
                this.adverse24MonthsPlusTotalField = value;
            }
        }

        /// <remarks/>

        public string adverseLast12MonthsCount
        {
            get
            {
                return this.adverseLast12MonthsCountField;
            }
            set
            {
                this.adverseLast12MonthsCountField = value;
            }
        }

        /// <remarks/>

        public string adverseLast12MonthsTotal
        {
            get
            {
                return this.adverseLast12MonthsTotalField;
            }
            set
            {
                this.adverseLast12MonthsTotalField = value;
            }
        }

        /// <remarks/>

        public string adverseLast24MonthsCount
        {
            get
            {
                return this.adverseLast24MonthsCountField;
            }
            set
            {
                this.adverseLast24MonthsCountField = value;
            }
        }

        /// <remarks/>

        public string adverseLast24MonthsTotal
        {
            get
            {
                return this.adverseLast24MonthsTotalField;
            }
            set
            {
                this.adverseLast24MonthsTotalField = value;
            }
        }

        /// <remarks/>

        public string adverseLast6MonthsCount
        {
            get
            {
                return this.adverseLast6MonthsCountField;
            }
            set
            {
                this.adverseLast6MonthsCountField = value;
            }
        }

        /// <remarks/>

        public string adverseLast6MonthsTotal
        {
            get
            {
                return this.adverseLast6MonthsTotalField;
            }
            set
            {
                this.adverseLast6MonthsTotalField = value;
            }
        }

        /// <remarks/>

        public string adverseTotalAmount
        {
            get
            {
                return this.adverseTotalAmountField;
            }
            set
            {
                this.adverseTotalAmountField = value;
            }
        }

        /// <remarks/>

        public string adverseTotalCount
        {
            get
            {
                return this.adverseTotalCountField;
            }
            set
            {
                this.adverseTotalCountField = value;
            }
        }

        /// <remarks/>

        public string judgement24MonthsPlusCount
        {
            get
            {
                return this.judgement24MonthsPlusCountField;
            }
            set
            {
                this.judgement24MonthsPlusCountField = value;
            }
        }

        /// <remarks/>

        public string judgement24MonthsPlusTotal
        {
            get
            {
                return this.judgement24MonthsPlusTotalField;
            }
            set
            {
                this.judgement24MonthsPlusTotalField = value;
            }
        }

        /// <remarks/>

        public string judgementLast12MonthsCount
        {
            get
            {
                return this.judgementLast12MonthsCountField;
            }
            set
            {
                this.judgementLast12MonthsCountField = value;
            }
        }

        /// <remarks/>

        public string judgementLast12MonthsTotal
        {
            get
            {
                return this.judgementLast12MonthsTotalField;
            }
            set
            {
                this.judgementLast12MonthsTotalField = value;
            }
        }

        /// <remarks/>

        public string judgementLast24MonthsCount
        {
            get
            {
                return this.judgementLast24MonthsCountField;
            }
            set
            {
                this.judgementLast24MonthsCountField = value;
            }
        }

        /// <remarks/>

        public string judgementLast24MonthsTotal
        {
            get
            {
                return this.judgementLast24MonthsTotalField;
            }
            set
            {
                this.judgementLast24MonthsTotalField = value;
            }
        }

        /// <remarks/>

        public string judgementLast6MonthsCount
        {
            get
            {
                return this.judgementLast6MonthsCountField;
            }
            set
            {
                this.judgementLast6MonthsCountField = value;
            }
        }

        /// <remarks/>

        public string judgementLast6MonthsTotal
        {
            get
            {
                return this.judgementLast6MonthsTotalField;
            }
            set
            {
                this.judgementLast6MonthsTotalField = value;
            }
        }

        /// <remarks/>

        public string judgementTotalAmount
        {
            get
            {
                return this.judgementTotalAmountField;
            }
            set
            {
                this.judgementTotalAmountField = value;
            }
        }

        /// <remarks/>

        public string judgementTotalCount
        {
            get
            {
                return this.judgementTotalCountField;
            }
            set
            {
                this.judgementTotalCountField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }

        /// <remarks/>

        public string notarialBond24MonthsPlusCount
        {
            get
            {
                return this.notarialBond24MonthsPlusCountField;
            }
            set
            {
                this.notarialBond24MonthsPlusCountField = value;
            }
        }

        /// <remarks/>

        public string notarialBond24MonthsPlusTotal
        {
            get
            {
                return this.notarialBond24MonthsPlusTotalField;
            }
            set
            {
                this.notarialBond24MonthsPlusTotalField = value;
            }
        }

        /// <remarks/>

        public string notarialBondLast12MonthsCount
        {
            get
            {
                return this.notarialBondLast12MonthsCountField;
            }
            set
            {
                this.notarialBondLast12MonthsCountField = value;
            }
        }

        /// <remarks/>

        public string notarialBondLast12MonthsTotal
        {
            get
            {
                return this.notarialBondLast12MonthsTotalField;
            }
            set
            {
                this.notarialBondLast12MonthsTotalField = value;
            }
        }

        /// <remarks/>

        public string notarialBondLast24MonthsCount
        {
            get
            {
                return this.notarialBondLast24MonthsCountField;
            }
            set
            {
                this.notarialBondLast24MonthsCountField = value;
            }
        }

        /// <remarks/>

        public string notarialBondLast24MonthsTotal
        {
            get
            {
                return this.notarialBondLast24MonthsTotalField;
            }
            set
            {
                this.notarialBondLast24MonthsTotalField = value;
            }
        }

        /// <remarks/>

        public string notarialBondLast6MonthsCount
        {
            get
            {
                return this.notarialBondLast6MonthsCountField;
            }
            set
            {
                this.notarialBondLast6MonthsCountField = value;
            }
        }

        /// <remarks/>

        public string notarialBondLast6MonthsTotal
        {
            get
            {
                return this.notarialBondLast6MonthsTotalField;
            }
            set
            {
                this.notarialBondLast6MonthsTotalField = value;
            }
        }

        /// <remarks/>

        public string notarialBondTotalAmount
        {
            get
            {
                return this.notarialBondTotalAmountField;
            }
            set
            {
                this.notarialBondTotalAmountField = value;
            }
        }

        /// <remarks/>

        public string notarialBondTotalCount
        {
            get
            {
                return this.notarialBondTotalCountField;
            }
            set
            {
                this.notarialBondTotalCountField = value;
            }
        }
    }

    /// <remarks/>





    public partial class Branch : BureauSegment
    {

        private string dunsNoField;

        private string emailField;

        private string faxNumberField;

        private string majorProductField;

        private string phoneNumberField;

        private string physicalAddressField;

        private string physicalCountryField;

        private string physicalPostCodeField;

        private string physicalSuburbField;

        private string physicalTownField;

        private string postBoxField;

        private string sGIndexField;

        private string websiteField;

        /// <remarks/>

        public string dunsNo
        {
            get
            {
                return this.dunsNoField;
            }
            set
            {
                this.dunsNoField = value;
            }
        }

        /// <remarks/>

        public string email
        {
            get
            {
                return this.emailField;
            }
            set
            {
                this.emailField = value;
            }
        }

        /// <remarks/>

        public string faxNumber
        {
            get
            {
                return this.faxNumberField;
            }
            set
            {
                this.faxNumberField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string phoneNumber
        {
            get
            {
                return this.phoneNumberField;
            }
            set
            {
                this.phoneNumberField = value;
            }
        }

        /// <remarks/>

        public string physicalAddress
        {
            get
            {
                return this.physicalAddressField;
            }
            set
            {
                this.physicalAddressField = value;
            }
        }

        /// <remarks/>

        public string physicalCountry
        {
            get
            {
                return this.physicalCountryField;
            }
            set
            {
                this.physicalCountryField = value;
            }
        }

        /// <remarks/>

        public string physicalPostCode
        {
            get
            {
                return this.physicalPostCodeField;
            }
            set
            {
                this.physicalPostCodeField = value;
            }
        }

        /// <remarks/>

        public string physicalSuburb
        {
            get
            {
                return this.physicalSuburbField;
            }
            set
            {
                this.physicalSuburbField = value;
            }
        }

        /// <remarks/>

        public string physicalTown
        {
            get
            {
                return this.physicalTownField;
            }
            set
            {
                this.physicalTownField = value;
            }
        }

        /// <remarks/>

        public string postBox
        {
            get
            {
                return this.postBoxField;
            }
            set
            {
                this.postBoxField = value;
            }
        }

        /// <remarks/>

        public string sGIndex
        {
            get
            {
                return this.sGIndexField;
            }
            set
            {
                this.sGIndexField = value;
            }
        }

        /// <remarks/>

        public string website
        {
            get
            {
                return this.websiteField;
            }
            set
            {
                this.websiteField = value;
            }
        }
    }

    /// <remarks/>





    public partial class BankingDetailSummary : BureauSegment
    {

        private string accountNumberField;

        private string amountField;

        private string bankBranchField;

        private string bankCodeDescriptionField;

        private string bankCodeField;

        private string bankNameField;

        private string dateField;

        private string majorProductField;

        private string startDateField;

        private string termsField;

        /// <remarks/>

        public string accountNumber
        {
            get
            {
                return this.accountNumberField;
            }
            set
            {
                this.accountNumberField = value;
            }
        }

        /// <remarks/>

        public string amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>

        public string bankBranch
        {
            get
            {
                return this.bankBranchField;
            }
            set
            {
                this.bankBranchField = value;
            }
        }

        /// <remarks/>

        public string bankCodeDescription
        {
            get
            {
                return this.bankCodeDescriptionField;
            }
            set
            {
                this.bankCodeDescriptionField = value;
            }
        }

        /// <remarks/>

        public string bankCode
        {
            get
            {
                return this.bankCodeField;
            }
            set
            {
                this.bankCodeField = value;
            }
        }

        /// <remarks/>

        public string bankName
        {
            get
            {
                return this.bankNameField;
            }
            set
            {
                this.bankNameField = value;
            }
        }

        /// <remarks/>

        public string date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string startDate
        {
            get
            {
                return this.startDateField;
            }
            set
            {
                this.startDateField = value;
            }
        }

        /// <remarks/>

        public string terms
        {
            get
            {
                return this.termsField;
            }
            set
            {
                this.termsField = value;
            }
        }
    }

    /// <remarks/>





    public partial class BankReport : BureauSegment
    {

        private string accHolderField;

        private string accountNoField;

        private string amountField;

        private string bankCodeDescField;

        private string bankCodeField;

        private string bankNameField;

        private string branchField;

        private string[] commentField;

        private string dateField;

        private string majorProductField;

        private string startDateField;

        private string termsField;

        /// <remarks/>

        public string accHolder
        {
            get
            {
                return this.accHolderField;
            }
            set
            {
                this.accHolderField = value;
            }
        }

        /// <remarks/>

        public string accountNo
        {
            get
            {
                return this.accountNoField;
            }
            set
            {
                this.accountNoField = value;
            }
        }

        /// <remarks/>

        public string amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>

        public string bankCodeDesc
        {
            get
            {
                return this.bankCodeDescField;
            }
            set
            {
                this.bankCodeDescField = value;
            }
        }

        /// <remarks/>

        public string bankCode
        {
            get
            {
                return this.bankCodeField;
            }
            set
            {
                this.bankCodeField = value;
            }
        }

        /// <remarks/>

        public string bankName
        {
            get
            {
                return this.bankNameField;
            }
            set
            {
                this.bankNameField = value;
            }
        }

        /// <remarks/>

        public string branch
        {
            get
            {
                return this.branchField;
            }
            set
            {
                this.branchField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>

        public string date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string startDate
        {
            get
            {
                return this.startDateField;
            }
            set
            {
                this.startDateField = value;
            }
        }

        /// <remarks/>

        public string terms
        {
            get
            {
                return this.termsField;
            }
            set
            {
                this.termsField = value;
            }
        }
    }

    /// <remarks/>





    public partial class BankHistory : BureauSegment
    {

        private string accountNoField;

        private decimal amountField;

        private string bankCodeDescField;

        private string bankCodeField;

        private string bankNameField;

        private string branchField;

        private string[] commentField;

        private DateTime? dateField;

        private int majorProductField;

        private DateTime? startDateField;

        private string termsField;

        /// <remarks/>

        public string accountNo
        {
            get
            {
                return this.accountNoField;
            }
            set
            {
                this.accountNoField = value;
            }
        }

        /// <remarks/>

        public decimal amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>

        public string bankCodeDesc
        {
            get
            {
                return this.bankCodeDescField;
            }
            set
            {
                this.bankCodeDescField = value;
            }
        }

        /// <remarks/>

        public string bankCode
        {
            get
            {
                return this.bankCodeField;
            }
            set
            {
                this.bankCodeField = value;
            }
        }

        /// <remarks/>

        public string bankName
        {
            get
            {
                return this.bankNameField;
            }
            set
            {
                this.bankNameField = value;
            }
        }

        /// <remarks/>

        public string branch
        {
            get
            {
                return this.branchField;
            }
            set
            {
                this.branchField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>

        public DateTime? date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>

        public int majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public DateTime? startDate
        {
            get
            {
                return this.startDateField;
            }
            set
            {
                this.startDateField = value;
            }
        }

        /// <remarks/>

        public string terms
        {
            get
            {
                return this.termsField;
            }
            set
            {
                this.termsField = value;
            }
        }
    }

    /// <remarks/>





    public partial class BankCodes : BureauSegment
    {

        private string accountNoField;

        private decimal amountField;

        private string bankCodeDescField;

        private string bankCodeField;

        private string bankNameField;

        private string branchField;

        private string[] commentField;

        private DateTime? dateField;

        private string majorProductField;

        private DateTime? startDateField;

        private string termsField;

        /// <remarks/>

        public string accountNo
        {
            get
            {
                return this.accountNoField;
            }
            set
            {
                this.accountNoField = value;
            }
        }

        /// <remarks/>

        public decimal amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>

        public string bankCodeDesc
        {
            get
            {
                return this.bankCodeDescField;
            }
            set
            {
                this.bankCodeDescField = value;
            }
        }

        /// <remarks/>

        public string bankCode
        {
            get
            {
                return this.bankCodeField;
            }
            set
            {
                this.bankCodeField = value;
            }
        }

        /// <remarks/>

        public string bankName
        {
            get
            {
                return this.bankNameField;
            }
            set
            {
                this.bankNameField = value;
            }
        }

        /// <remarks/>

        public string branch
        {
            get
            {
                return this.branchField;
            }
            set
            {
                this.branchField = value;
            }
        }

        /// <remarks/>

        // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
        public string[] comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>

        public DateTime? date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public DateTime? startDate
        {
            get
            {
                return this.startDateField;
            }
            set
            {
                this.startDateField = value;
            }
        }

        /// <remarks/>

        public string terms
        {
            get
            {
                return this.termsField;
            }
            set
            {
                this.termsField = value;
            }
        }
    }

    /// <remarks/>





    public partial class AggregateCW : BureauSegment
    {

        private Aggregate[] aggregatesField;

        private string majorProductField;

        /// <remarks/>

        public Aggregate[] aggregates
        {
            get
            {
                return this.aggregatesField;
            }
            set
            {
                this.aggregatesField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }
    }

    /// <remarks/>





    public partial class Affiliations : BureauSegment
    {

        private string commentField;

        private string dunsNumberField;

        private string endDateField;

        private string iTNumberField;

        private string infoDateField;

        private string majorProductField;

        private string objectCountryField;

        private string objectNameField;

        private string objectTownField;

        private string percentShareField;

        private string registrationNumberField;

        private string relationshipTypeField;

        private string startDateField;

        /// <remarks/>

        public string comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>

        public string dunsNumber
        {
            get
            {
                return this.dunsNumberField;
            }
            set
            {
                this.dunsNumberField = value;
            }
        }

        /// <remarks/>

        public string endDate
        {
            get
            {
                return this.endDateField;
            }
            set
            {
                this.endDateField = value;
            }
        }

        /// <remarks/>

        public string iTNumber
        {
            get
            {
                return this.iTNumberField;
            }
            set
            {
                this.iTNumberField = value;
            }
        }

        /// <remarks/>

        public string infoDate
        {
            get
            {
                return this.infoDateField;
            }
            set
            {
                this.infoDateField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string objectCountry
        {
            get
            {
                return this.objectCountryField;
            }
            set
            {
                this.objectCountryField = value;
            }
        }

        /// <remarks/>

        public string objectName
        {
            get
            {
                return this.objectNameField;
            }
            set
            {
                this.objectNameField = value;
            }
        }

        /// <remarks/>

        public string objectTown
        {
            get
            {
                return this.objectTownField;
            }
            set
            {
                this.objectTownField = value;
            }
        }

        /// <remarks/>

        public string percentShare
        {
            get
            {
                return this.percentShareField;
            }
            set
            {
                this.percentShareField = value;
            }
        }

        /// <remarks/>

        public string registrationNumber
        {
            get
            {
                return this.registrationNumberField;
            }
            set
            {
                this.registrationNumberField = value;
            }
        }

        /// <remarks/>

        public string relationshipType
        {
            get
            {
                return this.relationshipTypeField;
            }
            set
            {
                this.relationshipTypeField = value;
            }
        }

        /// <remarks/>

        public string startDate
        {
            get
            {
                return this.startDateField;
            }
            set
            {
                this.startDateField = value;
            }
        }
    }

    /// <remarks/>





    public partial class AccountVerificationVH : BureauSegment
    {

        private string accountAcceptsCreditsField;

        private string accountAcceptsDebitsField;

        private string accountDormantField;

        private string accountHolderField;

        private string accountNumberField;

        private string accountOpen3MonthsField;

        private string accountTypeField;

        private string bankNameField;

        private string branchCodeField;

        private string branchNameField;

        private string majorProductField;

        private string startDateField;

        private string verifiedDatedField;

        /// <remarks/>

        public string accountAcceptsCredits
        {
            get
            {
                return this.accountAcceptsCreditsField;
            }
            set
            {
                this.accountAcceptsCreditsField = value;
            }
        }

        /// <remarks/>

        public string accountAcceptsDebits
        {
            get
            {
                return this.accountAcceptsDebitsField;
            }
            set
            {
                this.accountAcceptsDebitsField = value;
            }
        }

        /// <remarks/>

        public string accountDormant
        {
            get
            {
                return this.accountDormantField;
            }
            set
            {
                this.accountDormantField = value;
            }
        }

        /// <remarks/>

        public string accountHolder
        {
            get
            {
                return this.accountHolderField;
            }
            set
            {
                this.accountHolderField = value;
            }
        }

        /// <remarks/>

        public string accountNumber
        {
            get
            {
                return this.accountNumberField;
            }
            set
            {
                this.accountNumberField = value;
            }
        }

        /// <remarks/>

        public string accountOpen3Months
        {
            get
            {
                return this.accountOpen3MonthsField;
            }
            set
            {
                this.accountOpen3MonthsField = value;
            }
        }

        /// <remarks/>

        public string accountType
        {
            get
            {
                return this.accountTypeField;
            }
            set
            {
                this.accountTypeField = value;
            }
        }

        /// <remarks/>

        public string bankName
        {
            get
            {
                return this.bankNameField;
            }
            set
            {
                this.bankNameField = value;
            }
        }

        /// <remarks/>

        public string branchCode
        {
            get
            {
                return this.branchCodeField;
            }
            set
            {
                this.branchCodeField = value;
            }
        }

        /// <remarks/>

        public string branchName
        {
            get
            {
                return this.branchNameField;
            }
            set
            {
                this.branchNameField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string startDate
        {
            get
            {
                return this.startDateField;
            }
            set
            {
                this.startDateField = value;
            }
        }

        /// <remarks/>

        public string verifiedDated
        {
            get
            {
                return this.verifiedDatedField;
            }
            set
            {
                this.verifiedDatedField = value;
            }
        }
    }

    /// <remarks/>





    public partial class AccountVerificationVA : BureauSegment
    {

        private string accountAcceptsCreditsField;

        private string accountAcceptsDebitsField;

        private string accountDormantField;

        private string accountFoundField;

        private string accountHolderField;

        private string accountNumberField;

        private string accountOpen3MonthsField;

        private string accountOpenField;

        private string accountTypeField;

        private string bankNameField;

        private string branchCodeField;

        private string branchNameField;

        private string commentField;

        private string errorReasonField;

        private string iDMatchField;

        private string majorProductField;

        private string startDateField;

        private string surnameMatchField;

        private string verifiedDatedField;

        private string verifiedField;

        /// <remarks/>

        public string accountAcceptsCredits
        {
            get
            {
                return this.accountAcceptsCreditsField;
            }
            set
            {
                this.accountAcceptsCreditsField = value;
            }
        }

        /// <remarks/>

        public string accountAcceptsDebits
        {
            get
            {
                return this.accountAcceptsDebitsField;
            }
            set
            {
                this.accountAcceptsDebitsField = value;
            }
        }

        /// <remarks/>

        public string accountDormant
        {
            get
            {
                return this.accountDormantField;
            }
            set
            {
                this.accountDormantField = value;
            }
        }

        /// <remarks/>

        public string accountFound
        {
            get
            {
                return this.accountFoundField;
            }
            set
            {
                this.accountFoundField = value;
            }
        }

        /// <remarks/>

        public string accountHolder
        {
            get
            {
                return this.accountHolderField;
            }
            set
            {
                this.accountHolderField = value;
            }
        }

        /// <remarks/>

        public string accountNumber
        {
            get
            {
                return this.accountNumberField;
            }
            set
            {
                this.accountNumberField = value;
            }
        }

        /// <remarks/>

        public string accountOpen3Months
        {
            get
            {
                return this.accountOpen3MonthsField;
            }
            set
            {
                this.accountOpen3MonthsField = value;
            }
        }

        /// <remarks/>

        public string accountOpen
        {
            get
            {
                return this.accountOpenField;
            }
            set
            {
                this.accountOpenField = value;
            }
        }

        /// <remarks/>

        public string accountType
        {
            get
            {
                return this.accountTypeField;
            }
            set
            {
                this.accountTypeField = value;
            }
        }

        /// <remarks/>

        public string bankName
        {
            get
            {
                return this.bankNameField;
            }
            set
            {
                this.bankNameField = value;
            }
        }

        /// <remarks/>

        public string branchCode
        {
            get
            {
                return this.branchCodeField;
            }
            set
            {
                this.branchCodeField = value;
            }
        }

        /// <remarks/>

        public string branchName
        {
            get
            {
                return this.branchNameField;
            }
            set
            {
                this.branchNameField = value;
            }
        }

        /// <remarks/>

        public string comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>

        public string errorReason
        {
            get
            {
                return this.errorReasonField;
            }
            set
            {
                this.errorReasonField = value;
            }
        }

        /// <remarks/>

        public string iDMatch
        {
            get
            {
                return this.iDMatchField;
            }
            set
            {
                this.iDMatchField = value;
            }
        }

        /// <remarks/>

        public string majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string startDate
        {
            get
            {
                return this.startDateField;
            }
            set
            {
                this.startDateField = value;
            }
        }

        /// <remarks/>

        public string surnameMatch
        {
            get
            {
                return this.surnameMatchField;
            }
            set
            {
                this.surnameMatchField = value;
            }
        }

        /// <remarks/>

        public string verifiedDated
        {
            get
            {
                return this.verifiedDatedField;
            }
            set
            {
                this.verifiedDatedField = value;
            }
        }

        /// <remarks/>

        public string verified
        {
            get
            {
                return this.verifiedField;
            }
            set
            {
                this.verifiedField = value;
            }
        }
    }

    public partial class PrincipalLinks
    {


        private int consumerNoField;

        private DateTime dateOfBirthField;

        private Established establishedField;

        private string forename1Field;

        private string forename2Field;

        private string forename3Field;

        private string iDNumberField;

        private LinkedBusinessDefault[] linkedBusinessDefaultsField;

        private LinkedCompany[] linkedCompaniesField;

        private int majorProductField;

        private string surnameField;

        /// <remarks/>

        public int consumerNo
        {
            get
            {
                return this.consumerNoField;
            }
            set
            {
                this.consumerNoField = value;
            }
        }

        /// <remarks/>

        public DateTime dateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>
        //public Established established
        //{
        //    get
        //    {
        //        return this.establishedField;
        //    }
        //    set
        //    {
        //        this.establishedField = value;
        //    }
        //}

        /// <remarks/>

        public string forename1
        {
            get
            {
                return this.forename1Field;
            }
            set
            {
                this.forename1Field = value;
            }
        }

        /// <remarks/>

        public string forename2
        {
            get
            {
                return this.forename2Field;
            }
            set
            {
                this.forename2Field = value;
            }
        }

        /// <remarks/>

        //public string forename3
        //{
        //    get
        //    {
        //        return this.forename3Field;
        //    }
        //    set
        //    {
        //        this.forename3Field = value;
        //    }
        //}

        /// <remarks/>

        public string iDNumber
        {
            get
            {
                return this.iDNumberField;
            }
            set
            {
                this.iDNumberField = value;
            }
        }

        /// <remarks/>

        public LinkedBusinessDefault[] linkedBusinessDefaults
        {
            get
            {
                return this.linkedBusinessDefaultsField;
            }
            set
            {
                this.linkedBusinessDefaultsField = value;
            }
        }

        /// <remarks/>

        public LinkedCompany[] linkedCompanies
        {
            get
            {
                return this.linkedCompaniesField;
            }
            set
            {
                this.linkedCompaniesField = value;
            }
        }

        /// <remarks/>

        public int majorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>

        public string surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }
    }
}
