﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Threading;
using System.Security.Principal;
using System.Web.Security;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Data;
using System.Net.Mail;
using System.Configuration;
using System.Net;
using System.Text;
using System.Reflection;
//using XDSPortalAuthentication;
//using DevExpress.XtraReports.UI;
//using XdsPortalReports;
using XDSPortalEnquiry.Business;
using System.Diagnostics;

namespace XDSConnect
{
    /// <summary>
    /// Summary description for MTNConnect
    /// </summary>
    [WebService(Namespace = "http://www.web.xds.co.za/XDSConnectWS")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class MCanvasConnect : System.Web.Services.WebService
    {
        private SqlConnection EnquiryCon = new SqlConnection(Properties.Settings.Default.EnquiryConnectionString);
        private SqlConnection AdminCon = new SqlConnection(Properties.Settings.Default.AdminConnectionString);
        private SqlConnection MTNEnquiryCon = new SqlConnection(Properties.Settings.Default.MTNCustomVetting);
        private string strFMPLoginURL = ConfigurationManager.AppSettings["FMPLoginURL"].ToString();
        private string strFMPMatchURL = ConfigurationManager.AppSettings["FMPMatchURL"].ToString();

        [WebMethod(Description = "")]
        public string Login(string strUser, string strPwd)
        {
            LoginUser oLoginUser = AuthenticateUser(strUser, strPwd);

            if (oLoginUser.LoginStatus == LoginUser.LoginUserStatus.Authenticated)
            {

                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                   1,                            // version
                   oLoginUser.LoginSystemUser.SystemUserID.ToString(),                      // user name
                   DateTime.Now,                 // create time
                   DateTime.Now.AddHours(5),  // expire time
                   false,                        // persistent
                   oLoginUser.LoginSystemUser.SubscriberID.ToString());              // user data

                oLoginUser.LoginTicket = FormsAuthentication.Encrypt(ticket);
                //HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, oLoginUser.LoginTicket);
                //Context.Response.Cookies.Add(cookie);
                return oLoginUser.LoginTicket;
            }
            else
                return oLoginUser.LoginStatus.ToString();
        }

        public bool IsTicketValid(string XDSConnectTicket)
        {
            try
            {
                FormsAuthenticationTicket a = FormsAuthentication.Decrypt(XDSConnectTicket);

                bool isValid = true;
                if (a.Expired) isValid = false;
                return isValid;
            }
            catch
            {
                return false;
            }
        }

        private LoginUser AuthenticateUser(string strUser, string strPwd)
        {
            LoginUser oLoginUser = new LoginUser();

            AdminCon.Open();

            if (Membership.ValidateUser(strUser, strPwd))
            {
                XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUser(AdminCon, strUser);

                if (oSystemUser != null)
                {
                    XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
                    XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(AdminCon, oSystemUser.SubscriberID);

                    oLoginUser.LoginStatus = LoginUser.LoginUserStatus.Authenticated;
                    oLoginUser.LoginSystemUser = oSystemUser;

                    if (!oSystemUser.ActiveYN) oLoginUser.LoginStatus = LoginUser.LoginUserStatus.UserDeactivated;
                    if (oSubscriber.StatusInd != "A") oLoginUser.LoginStatus = LoginUser.LoginUserStatus.SubscriberDeactivated;

                }
                else
                {
                    oLoginUser.LoginStatus = LoginUser.LoginUserStatus.UserNotFound;
                }

            }
            else
            {
                oLoginUser.LoginStatus = LoginUser.LoginUserStatus.NotAuthenticated;
            }
            AdminCon.Close();
            return oLoginUser;
            //throw new NotImplementedException();
        }

        [WebMethod(Description = "")]
        // Method for MTN
        public XDSPortalLibrary.Entity_Layer.CanvasVettingResult SubmitApplicationCanvas(XDSPortalLibrary.Entity_Layer.MTNCanvasInput Input)
        {

            Input.AccountHolderName = string.IsNullOrEmpty(Input.AccountHolderName) ? string.Empty : Input.AccountHolderName;
            Input.AlternateContactNumber = string.IsNullOrEmpty(Input.AlternateContactNumber) ? string.Empty : Input.AlternateContactNumber;
            Input.BankAccountNumber = string.IsNullOrEmpty(Input.BankAccountNumber) ? string.Empty : Input.BankAccountNumber;
            Input.BankAccountType = string.IsNullOrEmpty(Input.BankAccountType) ? string.Empty : Input.BankAccountType;
            Input.BankBranchCode = string.IsNullOrEmpty(Input.BankBranchCode) ? string.Empty : Input.BankBranchCode;
            Input.BankName = string.IsNullOrEmpty(Input.BankName) ? string.Empty : Input.BankName;
            Input.BirthDate = string.IsNullOrEmpty(Input.BirthDate) ? string.Empty : Input.BirthDate;
            Input.Branch = string.IsNullOrEmpty(Input.Branch) ? string.Empty : Input.Branch;
            Input.CellNumber = string.IsNullOrEmpty(Input.CellNumber) ? string.Empty : Input.CellNumber;
            Input.Company = string.IsNullOrEmpty(Input.Company) ? string.Empty : Input.Company;
            Input.CustomerName = string.IsNullOrEmpty(Input.CustomerName) ? string.Empty : Input.CustomerName;
            Input.DealerCode = string.IsNullOrEmpty(Input.DealerCode) ? string.Empty : Input.DealerCode;
            Input.EmailAddress = string.IsNullOrEmpty(Input.EmailAddress) ? string.Empty : Input.EmailAddress;
            Input.FirstName = string.IsNullOrEmpty(Input.FirstName) ? string.Empty : Input.FirstName;
            Input.GrossMonthlyIncome = string.IsNullOrEmpty(Input.GrossMonthlyIncome) ? string.Empty : Input.GrossMonthlyIncome;
            Input.IdentityNumber = string.IsNullOrEmpty(Input.IdentityNumber) ? string.Empty : Input.IdentityNumber;
            Input.IDType = string.IsNullOrEmpty(Input.IDType) ? string.Empty : Input.IDType;
            Input.Initials = string.IsNullOrEmpty(Input.Initials) ? string.Empty : Input.Initials;
            Input.IsExistingClient = string.IsNullOrEmpty(Input.IsExistingClient) ? string.Empty : Input.IsExistingClient;
            Input.RuleSet = string.IsNullOrEmpty(Input.RuleSet) ? string.Empty : Input.RuleSet;
            Input.Surname = string.IsNullOrEmpty(Input.Surname) ? string.Empty : Input.Surname;
            Input.HighestQualification = string.IsNullOrEmpty(Input.HighestQualification) ? string.Empty : Input.HighestQualification;
            Input.Occupation = string.IsNullOrEmpty(Input.Occupation) ? string.Empty : Input.Occupation;
            Input.Channel = string.IsNullOrEmpty(Input.Channel) ? string.Empty : Input.Channel;

            Input.OverwriteAVSResult = true;

            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();
            string rXml = string.Empty, ConnectTicket = string.Empty;
            //XDSPortalLibrary.Entity_Layer.CanvasVettingResponse oSResponse = new XDSPortalLibrary.Entity_Layer.CanvasVettingResponse();

            XDSPortalLibrary.Entity_Layer.CanvasVettingResult oSResponse = new XDSPortalLibrary.Entity_Layer.CanvasVettingResult();
            oSResponse.VettingResult = new XDSPortalLibrary.Entity_Layer.CanvasVettingResponse();
            oSResponse.AVSResult = new XDSPortalLibrary.Entity_Layer.AVSResponse();
            int intCustomVettingOinputID = 0;

            try
            {
                oSResponse.VettingResult.IdentityNumber = Input.IdentityNumber;
                oSResponse.VettingResult.Name2 = Input.Surname;
                oSResponse.VettingResult.Name1 = Input.FirstName;
                oSResponse.VettingResult.BirthDate = Input.BirthDate;
                oSResponse.VettingResult.CellularNo = Input.CellNumber;
                oSResponse.VettingResult.AlternateContactNo = Input.AlternateContactNumber;
                oSResponse.VettingResult.EmailAddress = Input.EmailAddress;                
                oSResponse.VettingResult.DealerID = Input.DealerCode;
                
               

                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step 7 \n");
                SqlConnection con = new SqlConnection(EnquiryCon.ConnectionString);
                SqlConnection MTNCon = new SqlConnection(MTNEnquiryCon.ConnectionString);

                XmlSerializer serializerinput = new XmlSerializer(Input.GetType());
                System.IO.StringWriter swinput = new System.IO.StringWriter();
                serializerinput.Serialize(swinput, Input);
                System.IO.StringReader readerin = new System.IO.StringReader(swinput.ToString());

                string strinput = readerin.ReadToEnd();


                SqlCommand cmd = new SqlCommand("Insert into MTNCanavassinginput (Input,CreatedOnDate,CreateByUser) values(@inputXMLstring,getdate(),'canvas')", con);

                //cmd.Parameters.AddWithValue("@inputXMLstring", ("productIndicator:" + ProductIndicator.ToString() + "Company:" + Company + "RuleSet:" + RuleSet + "Branch:" + Branch + "User:" + User + "Password:" + Password + "IsExistingClient:" + IsExistingClient + "GrossMonthlyIncome:" + GrossMonthlyIncome + "Surname:" + Surname + "FirstName:" + FirstName + "IDType:" + IDType + "BirthDate:" + BirthDate + "IdentityNumber:" + IdentityNumber + "Gender:" + Gender + "MaritalStatus:" + MaritalStatus + "AddressLine1:" + AddressLine1 + "AddressLine2:" + AddressLine2 + "Suburb:" + Suburb + "City:" + City + "PostalCode:" + PostalCode + "HomeTelephoneCode:" + HomeTelephoneCode + "HomeTelephoneNumber:" + HomeTelephoneNumber + "WorkTelephoneCode:" + WorkTelephoneCode + "WorkTelephoneNumber:" + WorkTelephoneNumber + "CellNumber:" + CellNumber + "Occupation:" + Occupation + "Employer:" + Employer + "NumberOfYearsAtEmployer:" + NumberOfYearsAtEmployer + "BankName:" + BankName + "BankAccountType:" + BankAccountType + "BankBranchCode:" + BankBranchCode + "BankAccountNumber:" + BankAccountNumber + "HighestQualification:" + HighestQualification));
                cmd.Parameters.AddWithValue("@inputXMLstring", strinput);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                string strSQL = "SELECT @@IDENTITY";
                SqlCommand Imycommand = new SqlCommand(strSQL, con);
                SqlDataReader Idr = Imycommand.ExecuteReader();
                Idr.Read();
                intCustomVettingOinputID = Convert.ToInt32(Idr.GetValue(0));
                Idr.Close();

                con.Close();

                //if (objInput.IDType.ToUpper() == XDSPortalLibrary.Entity_Layer.JDFSPrincipaRequest.IDType.OTHER.ToString())
                //{
                //    objInput.IDType = XDSPortalLibrary.Entity_Layer.JDFSPrincipaRequest.IDType.PASSPORT.ToString();
                //}
                // Validate input

                XDSPortalLibrary.Entity_Layer.Response oResponse = new XDSPortalLibrary.Entity_Layer.Response();
                CustomVetting oCustomvetting = new CustomVetting();
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step ValidateConnectCustomVettingQ \n");
                oResponse = oCustomvetting.ValidateMTNCanVassingInput(Input);
                //  File.AppendAllText(@"C:\Log\JDVet.txt", "Step ValidateConnectCustomVettingQ \n");

                if (oResponse.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                {
                    oSResponse.VettingResult.Status = "Failure";
                    oSResponse.VettingResult.ErrorDescription = oResponse.ResponseData;

                }
                else if (oResponse.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                {
                    // Login

                    ConnectTicket = Login(Input.User, Input.Password);
                    string AVSrespose = string.Empty;

                    if (IsTicketValid(ConnectTicket))
                    {

                        FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);

                        string iDnumber = string.Empty, passportNo = string.Empty;

                        iDnumber = (Input.IDType.ToUpper() == "1") ? Input.IdentityNumber : string.Empty;
                        passportNo = (Input.IDType.ToUpper() == "2") ? Input.IdentityNumber : string.Empty;

                        Input.NumberOfYearsAtEmployer = string.IsNullOrEmpty(Input.NumberOfYearsAtEmployer) ? "0" : Input.NumberOfYearsAtEmployer;

                        


                        try
                        {
                            XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                            XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(AdminCon, int.Parse(oTicket.Name));

                            XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
                            XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(AdminCon, oSystemUser.SubscriberID);

                            XDSPortalEnquiry.Business.CustomVetting dtCustomVetting = new XDSPortalEnquiry.Business.CustomVetting();

                               //File.AppendAllText(@"C:\Log\Response.txt", "EnterSubmitConsumerTrace");
                            oResponse = dtCustomVetting.SubmitConsumerTraceCanvas(EnquiryCon, AdminCon, oSubscriber.SubscriberID, oSystemUser.SystemUserID, 185, oSubscriber.SubscriberName, iDnumber, passportNo, Input.FirstName, Input.Surname, Input.BirthDate,
                                Input.Gender, Input.MaritalStatus, Input.AddressLine1, Input.AddressLine2, Input.Suburb, Input.City, Input.PostalCode, Input.HomeTelephoneCode, Input.HomeTelephoneNumber, Input.WorkTelephoneCode, Input.WorkTelephoneNumber, Input.CellNumber, double.Parse(Input.GrossMonthlyIncome), Input.Occupation, Input.Employer, Input.BankAccountType, Input.BankName,
                                Input.HighestQualification, Input.Branch, Input.BankAccountNumber, int.Parse(Input.NumberOfYearsAtEmployer), string.Empty, true, false, string.Empty);
                             //File.AppendAllText(@"C:\Log\Response.txt", "EndSubmitConsumerTrace");
                             //File.AppendAllText(@"C:\Log\Response.txt", oResponse.ResponseData);
                            switch (oResponse.ResponseStatus)
                            {
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single:
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple:

                                    DataSet dsl = new DataSet();
                                    System.IO.StringReader xmlSR = new System.IO.StringReader(oResponse.ResponseData);
                                    dsl.ReadXml(xmlSR);

                                    //  File.AppendAllText(@"C:\Log\Response.txt", "End xml generate");

                                    if (dsl.Tables[0].Rows.Count == 0 || dsl.Tables[0].Rows.Count == 1)
                                    {
                                        foreach (DataRow dr in dsl.Tables[0].Rows)
                                        {
                                            XDSPortalLibrary.Entity_Layer.CanvasVettingResponse oinputResponse = new XDSPortalLibrary.Entity_Layer.CanvasVettingResponse();
                                            // oResponse = dtCustomVetting.SubmitMulipleCreditEnquiry(con, AdminCon, int.Parse(dr["EnquiryID"].ToString()), int.Parse(dr["EnquiryResultID"].ToString()), false, string.Empty, Unique_ref, QueryNumber.ToString());
                                            oinputResponse = oSResponse.VettingResult;


                                            //   File.AppendAllText(@"C:\Log\Response.txt", "EnterSubmitCustomVettingO");
                                            oResponse = dtCustomVetting.ProcessCanvasVetting(EnquiryCon, AdminCon, int.Parse(dr["EnquiryID"].ToString()), int.Parse(dr["EnquiryResultID"].ToString()), false, string.Empty, oSResponse.VettingResult, Input, out oinputResponse, 0, Input.IsExistingClient);
                                            //  File.AppendAllText(@"C:\Log\Response.txt", "EndSubmitCustomVettingO");

                                            oSResponse.VettingResult = oinputResponse;
                                            oSResponse.VettingResult.ReferenceNo = dr["EnquiryResultID"].ToString();
                                            oSResponse.VettingResult.EnquiryID = dr["EnquiryID"].ToString();

                                            switch (oResponse.ResponseStatus)
                                            {
                                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                                                    oSResponse.VettingResult.Status = "Failure";
                                                    oSResponse.VettingResult.ErrorDescription = oResponse.ResponseData;
                                                    break;
                                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                                                    
                                                    // Trigger AVS
                                                    if (oSResponse.VettingResult.SourceInd != "P")
                                                    {
                                                       

                                                        if (!string.IsNullOrEmpty(Input.BankAccountNumber) && !string.IsNullOrEmpty(Input.BankBranchCode)
                                                            && !string.IsNullOrEmpty(Input.BankAccountType) && !string.IsNullOrEmpty(Input.BankName))
                                                        {
                                                            XDSConnect.XDSConnectWS oWs = new XDSConnectWS();
                                                            string tick = Login("ksulochana", "Sulo@123");
                                                            if (Input.IDType.ToUpper() == "1")
                                                            {
                                                               
                                                                AVSrespose = oWs.ConnectAccountVerificationRealTime(tick, XDSConnectWS.TypeofVerificationenum.Individual, XDSConnectWS.Entity.None, Input.Initials, Input.Surname, Input.IdentityNumber,
                                                                    XDSConnectWS.AVSIDType.SID, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, Input.BankAccountNumber, Input.BankBranchCode, Input.BankAccountType,
                                                                    Input.BankName, string.Empty, string.Empty);
                                                            }
                                                            else if (Input.IDType.ToUpper() == "2")
                                                            {
                                                               
                                                                AVSrespose = oWs.ConnectAccountVerificationRealTime(tick, XDSConnectWS.TypeofVerificationenum.Individual, XDSConnectWS.Entity.None, Input.Initials, Input.Surname, Input.IdentityNumber,
                                                                    XDSConnectWS.AVSIDType.FPP, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, Input.BankAccountNumber, Input.BankBranchCode, Input.BankAccountType,
                                                                    Input.BankName, string.Empty, string.Empty);
                                                            }

                                                            oWs.Dispose();


                                                            if (AVSrespose.ToLower().Contains("error"))
                                                            {
                                                                oSResponse.AVSResult.ErrorCode = "AVS001";
                                                                XmlDocument odoc = new XmlDocument();

                                                                odoc.LoadXml(AVSrespose);
                                                                XmlNodeList onodelist = odoc.GetElementsByTagName("Error");

                                                                oSResponse.AVSResult.ErrorDescription = onodelist[0].InnerText;
                                                            }
                                                            else
                                                            {
                                                                XDSConnect.XDSConnectWS oWsResult = new XDSConnectWS();
                                                                // File.AppendAllText(@"C:\Log\Response.txt", "Begin AVS:" + AVSrespose + "\n");
                                                                XDSPortalLibrary.Entity_Layer.Canvas.AccountVerification oAVSsubmitRsp = new XDSPortalLibrary.Entity_Layer.Canvas.AccountVerification();
                                                                XmlSerializer oXmlSerializer = new XmlSerializer(oAVSsubmitRsp.GetType());
                                                                //The StringReader will be the stream holder for the existing XML file 
                                                                oAVSsubmitRsp = (XDSPortalLibrary.Entity_Layer.Canvas.AccountVerification)oXmlSerializer.Deserialize(new StringReader(AVSrespose));
                                                              
                                                                string AVSResult = string.Empty;
                                                                int loopcount = 0;
                                                                Stopwatch oWatch = new Stopwatch();
                                                                oWatch = Stopwatch.StartNew();
                                                                long noofSeconds = oWatch.ElapsedTicks / Stopwatch.Frequency;
                                                               // File.AppendAllText(@"C:\Log\Response.txt", "outside:noofSeconds" + noofSeconds.ToString() + "\n");
                                                                while (loopcount < 1 && noofSeconds <= 30)
                                                                {
                                                                    
                                                                    AVSResult = oWsResult.ConnectGetAccountVerificationResult(tick, int.Parse(oAVSsubmitRsp.ReferenceNo));

                                                                    if (AVSResult.ToLower().Contains("<noresult>")|| AVSResult.ToLower().Contains("<error>"))
                                                                    {
                                                                        loopcount = 0;
                                                                    }
                                                                    else { break; }
                                                                    noofSeconds = oWatch.ElapsedTicks / Stopwatch.Frequency;
                                                                    Thread.Sleep(600);
                                                                   // File.AppendAllText(@"C:\Log\Response.txt", "in:noofSeconds"+noofSeconds.ToString()+"\n");
                                                                }
                                                                oWatch.Stop();
                                                                oWsResult.Dispose();
                                                              //  File.AppendAllText(@"C:\Log\Response.txt", "Begin AVS:"+oAVSsubmitRsp.ReferenceNo+":"+  AVSResult + "\n");
                                                                if (AVSResult.ToLower().Contains("<noresult>")|| AVSResult.ToLower().Contains("<error>"))
                                                                {
                                                                    oSResponse.AVSResult.ErrorCode = "AVS002";
                                                                    oSResponse.AVSResult.ErrorDescription = "AVS result not available for processing";
                                                                }
                                                                else
                                                                {
                                                                    XDSPortalLibrary.Entity_Layer.Canvas.AccountVerificationResult oResult = new XDSPortalLibrary.Entity_Layer.Canvas.AccountVerificationResult();
                                                                    XmlSerializer oXmlSerializer1 = new XmlSerializer(oResult.GetType());
                                                                    //The StringReader will be the stream holder for the existing XML file 
                                                                    oResult = (XDSPortalLibrary.Entity_Layer.Canvas.AccountVerificationResult)oXmlSerializer1.Deserialize(new StringReader(AVSResult));

                                                                    if (oResult.ResultFile != null)
                                                                    {
                                                                        if (oResult.ResultFile.ACCOUNTFOUND.ToLower() != "yes" ||
                                                                            oResult.ResultFile.IDNUMBERMATCH.ToLower() != "yes" ||
                                                                            // oResult.ResultFile.INITIALSMATCH.ToLower() != "yes" ||
                                                                            oResult.ResultFile.SURNAMEMATCH.ToLower() != "yes")
                                                                        {
                                                                            oSResponse.AVSResult.ErrorCode = "AVS003";
                                                                            oSResponse.AVSResult.ErrorDescription = "Banking details mismatch, please verify that the banking details captured are correct";
                                                                            oSResponse.VettingResult.BureauResponse = "Decline";
                                                                        }
                                                                        else
                                                                        {
                                                                            oSResponse.AVSResult.ResultFile = oResult.ResultFile;
                                                                            oSResponse.VettingResult.BureauResponse = oSResponse.VettingResult.BureauResponse.ToLower() == "approved" ?"Approved":"Declined";
                                                                            oSResponse.AVSResult.ErrorCode = string.Empty;
                                                                            oSResponse.AVSResult.ErrorDescription = string.Empty;

                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        oSResponse.AVSResult.ErrorCode = "AVS002";
                                                                        oSResponse.AVSResult.ErrorDescription = "AVS result not available for processing";
                                                                        oSResponse.VettingResult.BureauResponse = "Approved";
                                                                    }

                                                                }

                                                            }

                                                        }
                                                        else
                                                        {
                                                            oSResponse.AVSResult.ErrorCode = "AVS004";
                                                            oSResponse.AVSResult.ErrorDescription = "No Bank details submitted for processing AVS";
                                                        }
                                                       // File.AppendAllText(@"C:\Log\FMPResponse.txt", "Enter FMP part\n");

                                                        oResponse = dtCustomVetting.SubmitCanvasReport(EnquiryCon, AdminCon, int.Parse(dr["EnquiryID"].ToString()), int.Parse(dr["EnquiryResultID"].ToString()), false, string.Empty, oSResponse, Input, out oSResponse, 0, Input.IsExistingClient, Input.User, Input.Password, strFMPLoginURL, strFMPMatchURL);
                                                        //File.AppendAllText(@"C:\Log\FMPResponse.txt", oResponse.ResponseData+"\n");
                                                        //File.AppendAllText(@"C:\Log\FMPResponse.txt", "Done FMMP\n");

                                                    if (oSResponse.FMPResult != null && oSResponse.FMPResult.FMPScore == "0")
                                                        {
                                                            oSResponse.VettingResult.BureauResponse = oSResponse.VettingResult.BureauResponse.ToLower() == "approved" ? "Approved" : "Declined";
                                                        }
                                                        else
                                                        {
                                                            oSResponse.VettingResult.BureauResponse = "Declined";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        oSResponse.AVSResult.ErrorCode = "AVS005";
                                                        oSResponse.AVSResult.ErrorDescription = "Preapproved customer";
                                                    }


                                                 
                                                    oSResponse.VettingResult.Status = "Success";
                                                    //rsXml.LoadXml(rp.ResponseData);
                                                    break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        oSResponse.VettingResult.Status = "Failure";
                                        oSResponse.VettingResult.ErrorDescription = "Multiple Match Found";

                                    }
                                    break;

                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                                    oSResponse.VettingResult.Status = "Failure";
                                    oSResponse.VettingResult.ErrorDescription = oResponse.ResponseData;
                                    break;
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                                    oSResponse.VettingResult.Status = "Failure";
                                    oSResponse.VettingResult.ErrorDescription = "No Record Found";
                                    break;
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                                    oSResponse.VettingResult.Status = "Success";
                                    oSResponse.VettingResult.ErrorDescription = oResponse.ResponseData;
                                    break;
                            }


                        }
                        catch (Exception e)
                        {
                            oSResponse.VettingResult.Status = "Failure";
                            oSResponse.VettingResult.ErrorDescription = e.Message;
                        }

                    }
                    else
                    {
                        oSResponse.VettingResult.Status = "Failure";
                        oSResponse.VettingResult.ErrorDescription = ConnectTicket;
                    }
                }

                if (oSResponse.VettingResult.BureauResponse.ToUpper() == "DECLINED")
                {
                    oSResponse.VettingResult.CreditLimit = "0";
                    oSResponse.VettingResult.MaxDisposableIncome = "0";
                }


            }
            catch (Exception ex)
            {
                oSResponse.VettingResult.Status = "Failure";
                oSResponse.VettingResult.ErrorDescription = ex.Message;
            }


            //if (oSResponse.VettingResult.Status == "Failure")
            //{
            //    oSResponse.MTNOutput.Outcome = "Decline";
            //    oSResponse.MTNOutput.BureauResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.MTNHResponse.BureauResponse.enumresponseStatus.FAILURE;
            //    oSResponse.MTNOutput.Reason = "Decline";
            //}
            //else if (oSResponse.MTNOutput.Status == "Success")
            //{
            //    oSResponse.MTNOutput.Outcome = "Approve";
            //    oSResponse.MTNOutput.BureauResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.MTNHResponse.BureauResponse.enumresponseStatus.SUCCESS;
            //    oSResponse.MTNOutput.Reason = "Approve";
            //}


            //oSResponse.SubmitResult = oSubmitResponse;
            string response = string.Empty;
            try
            {
                //  File.AppendAllText(@"C:\Log\Response.txt", "Enterdeserialization");
                XmlSerializer serializerout = new XmlSerializer(oSResponse.GetType());
                System.IO.StringWriter swout = new System.IO.StringWriter();
                serializerout.Serialize(swout, oSResponse);
                System.IO.StringReader readerout = new System.IO.StringReader(swout.ToString());

                response = readerout.ReadToEnd();

                SqlConnection con = new SqlConnection(Properties.Settings.Default.EnquiryConnectionString);

                SqlCommand cmd = new SqlCommand("Insert into MTNCanavassingResponse (MTNCanavassinginputID,Response,CreatedOnDate) values(@ID, @inputXMLstring,getdate())", con);
                cmd.Parameters.AddWithValue("@ID", intCustomVettingOinputID);
                cmd.Parameters.AddWithValue("@inputXMLstring", response);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                con.Close();
            }

            catch (Exception ex)
            {
                // File.AppendAllText(@"C:\Log\Response.txt", "EnterException");
                SqlConnection con = new SqlConnection(Properties.Settings.Default.EnquiryConnectionString);

                SqlCommand cmd = new SqlCommand("Insert into MTNCanavassingResponse (MTNCanavassinginputID,Response,CreatedOnDate) values(@ID, @inputXMLstring,getdate())", con);
                cmd.Parameters.AddWithValue("@ID", intCustomVettingOinputID);
                cmd.Parameters.AddWithValue("@inputXMLstring", ex.Message);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                con.Close();

            }

            return oSResponse;

        }


       
    }
}