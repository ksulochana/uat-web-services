﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Threading;
using System.Security.Principal;
using System.Web.Security;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Data;
using System.Net.Mail;
using System.Configuration;
using System.Net;
using System.Text;
using System.Reflection;
//using XDSPortalAuthentication;
//using DevExpress.XtraReports.UI;
//using XdsPortalReports;
using XDSPortalEnquiry.Business;

namespace XDSConnect
{
    /// <summary>
    /// Summary description for MTNConnect
    /// </summary>
    [WebService(Namespace = "http://www.web.xds.co.za/XDSConnectWS")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class MHConnect : System.Web.Services.WebService
    {
        private SqlConnection EnquiryCon = new SqlConnection(Properties.Settings.Default.EnquiryConnectionString);
        private SqlConnection AdminCon = new SqlConnection(Properties.Settings.Default.AdminConnectionString);
        private SqlConnection MTNEnquiryCon = new SqlConnection(Properties.Settings.Default.MTNCustomVetting);

        public string Login(string strUser, string strPwd)
        {
            LoginUser oLoginUser = AuthenticateUser(strUser, strPwd);

            if (oLoginUser.LoginStatus == LoginUser.LoginUserStatus.Authenticated)
            {

                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                   1,                            // version
                   oLoginUser.LoginSystemUser.SystemUserID.ToString(),                      // user name
                   DateTime.Now,                 // create time
                   DateTime.Now.AddHours(5),  // expire time
                   false,                        // persistent
                   oLoginUser.LoginSystemUser.SubscriberID.ToString());              // user data

                oLoginUser.LoginTicket = FormsAuthentication.Encrypt(ticket);
                //HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, oLoginUser.LoginTicket);
                //Context.Response.Cookies.Add(cookie);
                return oLoginUser.LoginTicket;
            }
            else
                return oLoginUser.LoginStatus.ToString();
        }

        public bool IsTicketValid(string XDSConnectTicket)
        {
            try
            {
                FormsAuthenticationTicket a = FormsAuthentication.Decrypt(XDSConnectTicket);

                bool isValid = true;
                if (a.Expired) isValid = false;
                return isValid;
            }
            catch
            {
                return false;
            }
        }

        private LoginUser AuthenticateUser(string strUser, string strPwd)
        {
            LoginUser oLoginUser = new LoginUser();

            AdminCon.Open();

            if (Membership.ValidateUser(strUser, strPwd))
            {
                XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUser(AdminCon, strUser);

                if (oSystemUser != null)
                {
                    XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
                    XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(AdminCon, oSystemUser.SubscriberID);

                    oLoginUser.LoginStatus = LoginUser.LoginUserStatus.Authenticated;
                    oLoginUser.LoginSystemUser = oSystemUser;

                    if (!oSystemUser.ActiveYN) oLoginUser.LoginStatus = LoginUser.LoginUserStatus.UserDeactivated;
                    if (oSubscriber.StatusInd != "A") oLoginUser.LoginStatus = LoginUser.LoginUserStatus.SubscriberDeactivated;

                }
                else
                {
                    oLoginUser.LoginStatus = LoginUser.LoginUserStatus.UserNotFound;
                }

            }
            else
            {
                oLoginUser.LoginStatus = LoginUser.LoginUserStatus.NotAuthenticated;
            }
            AdminCon.Close();
            return oLoginUser;
            //throw new NotImplementedException();
        }

        [WebMethod(Description = "")]
        // Method for MTN
        public XDSPortalLibrary.Entity_Layer.MTNHResponse.MTNOutput SubmitApplication(XDSPortalLibrary.Entity_Layer.MTNHResponse.MTNInput MTNInput)
        {

            // File.AppendAllText(@"C:\Log\JDVet.txt", "Step 1 \n");

            DateTime dtStartdate = DateTime.Now;

            //  File.AppendAllText(@"C:\Log\JDVet.txt", "Step 2 \n");
            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();
            string rXml = string.Empty, ConnectTicket = string.Empty;

            //  File.AppendAllText(@"C:\Log\JDVet.txt", "Step 3 \n");

            XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplicationResponse oSResponse = new XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplicationResponse();
            oSResponse.MTNOutput.BureauResponse = new XDSPortalLibrary.Entity_Layer.MTNHResponse.BureauResponse();

            // File.AppendAllText(@"C:\Log\JDVet.txt", "Step 4 \n");

            XmlSerializer serializer = new XmlSerializer(typeof(XDSPortalLibrary.Entity_Layer.MTNHResponse.MTNInput));
            XDSPortalLibrary.Entity_Layer.MTNHResponse.MTNInput objInput = new XDSPortalLibrary.Entity_Layer.MTNHResponse.MTNInput();

            // File.AppendAllText(@"C:\Log\JDVet.txt", "Step 5 \n");

            int intCustomVettingOinputID = 0;

            // File.AppendAllText(@"C:\Log\JDVet.txt", "Step 6 \n");

            MTNInput.AlternateContactNumber = string.IsNullOrEmpty(MTNInput.AlternateContactNumber) ? string.Empty : MTNInput.AlternateContactNumber;
            MTNInput.EmailAddress  = string.IsNullOrEmpty(MTNInput.EmailAddress) ? string.Empty : MTNInput.EmailAddress;

            try
            {
                MTNInput.Channel = string.IsNullOrEmpty(MTNInput.Channel) ? string.Empty: MTNInput.Channel;

                 //File.AppendAllText(@"C:\Log\JDVet.txt", "Step 7 \n");
                 SqlConnection con = new SqlConnection(EnquiryCon.ConnectionString);
                SqlConnection MTNCon = new SqlConnection(MTNEnquiryCon.ConnectionString);

                XmlSerializer serializerinput = new XmlSerializer(MTNInput.GetType());
                System.IO.StringWriter swinput = new System.IO.StringWriter();
                serializerinput.Serialize(swinput, MTNInput);
                System.IO.StringReader readerin = new System.IO.StringReader(swinput.ToString());

                string strinput = readerin.ReadToEnd();


                SqlCommand cmd = new SqlCommand("Insert into CustomVettingQinput (Input,CreatedOnDate) values(@inputXMLstring,getdate())", con);

                //cmd.Parameters.AddWithValue("@inputXMLstring", ("productIndicator:" + ProductIndicator.ToString() + "Company:" + Company + "RuleSet:" + RuleSet + "Branch:" + Branch + "User:" + User + "Password:" + Password + "IsExistingClient:" + IsExistingClient + "GrossMonthlyIncome:" + GrossMonthlyIncome + "Surname:" + Surname + "FirstName:" + FirstName + "IDType:" + IDType + "BirthDate:" + BirthDate + "IdentityNumber:" + IdentityNumber + "Gender:" + Gender + "MaritalStatus:" + MaritalStatus + "AddressLine1:" + AddressLine1 + "AddressLine2:" + AddressLine2 + "Suburb:" + Suburb + "City:" + City + "PostalCode:" + PostalCode + "HomeTelephoneCode:" + HomeTelephoneCode + "HomeTelephoneNumber:" + HomeTelephoneNumber + "WorkTelephoneCode:" + WorkTelephoneCode + "WorkTelephoneNumber:" + WorkTelephoneNumber + "CellNumber:" + CellNumber + "Occupation:" + Occupation + "Employer:" + Employer + "NumberOfYearsAtEmployer:" + NumberOfYearsAtEmployer + "BankName:" + BankName + "BankAccountType:" + BankAccountType + "BankBranchCode:" + BankBranchCode + "BankAccountNumber:" + BankAccountNumber + "HighestQualification:" + HighestQualification));
                cmd.Parameters.AddWithValue("@inputXMLstring", strinput);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                string strSQL = "SELECT @@IDENTITY";
                SqlCommand Imycommand = new SqlCommand(strSQL, con);
                SqlDataReader Idr = Imycommand.ExecuteReader();
                Idr.Read();
                intCustomVettingOinputID = Convert.ToInt32(Idr.GetValue(0));
                Idr.Close();

                con.Close();

                string strQuery = "Insert into MTNCustomVettingInput (CustomVettingOinputID,MonthlyIncome,Surname,FirstName,IDType,SAIDBirthDate,IdentityNumber,Gender,MaritalStatus,AddressLine1,";
                strQuery += "AddressLine2,Suburb,City,PostalCode,HomeTelephoneCode,HomeTelephoneNumber,WorkTelephoneCode,WorkTelephoneNumber,CellNumber,Occupation,Employer,";
                strQuery += "NumberOfYearsAtEmployer,BankName,BankAccountType,BankBranchCode,BankAccountNumber,HighestQualification,StatusInd,EnquiryDate, Dealercode, CustomerName, Channel, AlternateContactNumber ,EmailAddress ) ";
                strQuery += "values('" + intCustomVettingOinputID.ToString() + "','" + MTNInput.GrossMonthlyIncome + "','" + MTNInput.Surname + "','" + MTNInput.FirstName + "','" + MTNInput.IDType + "','";
                strQuery += MTNInput.BirthDate + "','" + MTNInput.IdentityNumber + "','" + MTNInput.Gender + "','" + MTNInput.MaritalStatus + "','" + MTNInput.AddressLine1 + "','";
                strQuery += MTNInput.AddressLine2 + "','" + MTNInput.Suburb + "','" + MTNInput.City + "','" + MTNInput.PostalCode + "','" + MTNInput.HomeTelephoneCode + "','";
                strQuery += MTNInput.HomeTelephoneNumber + "','" + MTNInput.WorkTelephoneCode + "','" + MTNInput.WorkTelephoneNumber + "','" + MTNInput.CellNumber + "','";
                strQuery += MTNInput.Occupation + "','" + MTNInput.Employer + "','" + MTNInput.NumberOfYearsAtEmployer + "','" + MTNInput.BankName + "','";
                strQuery += MTNInput.BankAccountType + "','" + MTNInput.BankBranchCode + "','" + MTNInput.BankAccountNumber + "','" + MTNInput.HighestQualification + "',";
                strQuery += "1, getdate(),'" + MTNInput.DealerCode + "','" + MTNInput.CustomerName + "','" + MTNInput.Channel + "','" + MTNInput.AlternateContactNumber.Replace("'", "''") + "','" + MTNInput.EmailAddress.Replace("'", "''") + "')";

                cmd = new SqlCommand(strQuery, MTNCon);

                if (MTNCon.State == ConnectionState.Closed)
                    MTNCon.Open();

                cmd.ExecuteNonQuery();

                MTNCon.Close();

                //parse XML


                //  File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx \n");
                //objInput.input = new XDSPortalLibrary.Entity_Layer.SubmitApplicationInput();
                //objInput.input.AddressLine1 = AddressLine1;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 1\n");
                //objInput.input.AddressLine2 = AddressLine2;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 2\n");
                //objInput.input.BankAccountNumber = BankAccountNumber;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 3\n");
                //objInput.input.BankAccountType = BankAccountType;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 4\n");
                //objInput.input.BankBranchCode = BankBranchCode;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 5\n");
                //objInput.input.BankName = BankName;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 6\n");
                //objInput.input.BirthDate = BirthDate;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 7\n");
                //objInput.input.Branch = Branch;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 8\n");
                //objInput.input.CellNumber = CellNumber;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 9\n");
                //objInput.input.City = City;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 10\n");
                //objInput.input.Company = Company;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 11\n");
                //objInput.input.Employer = Employer;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 12\n");
                //objInput.input.FirstName = FirstName;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 13\n");
                //objInput.input.Gender = Gender;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 14\n");
                //objInput.input.GrossMonthlyIncome = GrossMonthlyIncome;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 15\n");
                //objInput.input.HighestQualification = HighestQualification;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 16\n");
                //objInput.input.HomeTelephoneCode = HomeTelephoneCode;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 17\n");
                //objInput.input.HomeTelephoneNumber = HomeTelephoneNumber;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 18\n");
                //objInput.input.IdentityNumber = IdentityNumber;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 19\n");
                //objInput.input.IDType = IDType;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 20\n");
                //objInput.input.IsExistingClient = IsExistingClient;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 21\n");
                //objInput.input.MaritalStatus = MaritalStatus;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 22\n");
                //objInput.input.NumberOfYearsAtEmployer = NumberOfYearsAtEmployer;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 23\n");
                //objInput.input.Occupation = Occupation;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 24\n");
                //objInput.input.Password = Password;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 25\n");
                //objInput.input.PostalCode = PostalCode;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 26\n");
                //objInput.input.RuleSet = RuleSet;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 27\n");
                //objInput.input.Suburb = Suburb;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 28\n");
                //objInput.input.Surname = Surname;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 29\n");
                //objInput.input.User = User;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 30\n");
                //objInput.input.WorkTelephoneCode = WorkTelephoneCode;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 31\n");
                //objInput.input.WorkTelephoneNumber = WorkTelephoneNumber;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 32\n");

                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step yyy \n");
                objInput = MTNInput;// (XDSPortalLibrary.Entity_Layer.MTNInput)serializer.Deserialize(new XmlTextReader(new StringReader(xml)));

                XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplication oAPP = new XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplication();
                oAPP.input = objInput;
                // oSResponse.SubmitResult.ClientReference = objInput.input.ClientReference;

                //if (objInput.IDType.ToUpper() == XDSPortalLibrary.Entity_Layer.JDFSPrincipaRequest.IDType.OTHER.ToString())
                //{
                //    objInput.IDType = XDSPortalLibrary.Entity_Layer.JDFSPrincipaRequest.IDType.PASSPORT.ToString();
                //}
                // Validate input

                XDSPortalLibrary.Entity_Layer.Response oResponse = new XDSPortalLibrary.Entity_Layer.Response();
                CustomVetting oCustomvetting = new CustomVetting();
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step ValidateConnectCustomVettingQ \n");
                oResponse = oCustomvetting.ValidateConnectCustomVettingQ(oAPP);
                //  File.AppendAllText(@"C:\Log\JDVet.txt", "Step ValidateConnectCustomVettingQ \n");

                if (oResponse.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                {
                    oSResponse.MTNOutput.Status = "Failure";
                    oSResponse.MTNOutput.ErrorDescription = oResponse.ResponseData;

                }
                else if (oResponse.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                {
                    // Login

                    ConnectTicket = Login(objInput.User, objInput.Password);


                    if (IsTicketValid(ConnectTicket))
                    {
                        FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);

                        string iDnumber = string.Empty, passportNo = string.Empty;

                        iDnumber = (objInput.IDType.ToUpper() == "1") ? objInput.IdentityNumber : string.Empty;
                        passportNo = (objInput.IDType.ToUpper() == "2") ? objInput.IdentityNumber : string.Empty;

                        objInput.NumberOfYearsAtEmployer = string.IsNullOrEmpty(objInput.NumberOfYearsAtEmployer) ? "0" : objInput.NumberOfYearsAtEmployer;
                        //passportNo = (objInput.Items.IDType.ToUpper() == XDSPortalLibrary.Entity_Layer.JDFSPrincipaRequest.IDType.OTHER.ToString()) ? objInput.Items.IDNumber : string.Empty;
                        
                        try
                        {
                            XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                            XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(AdminCon, int.Parse(oTicket.Name));

                            XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
                            XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(AdminCon, oSystemUser.SubscriberID);

                            XDSPortalEnquiry.Business.CustomVetting dtCustomVetting = new XDSPortalEnquiry.Business.CustomVetting();

                            //   File.AppendAllText(@"C:\Log\Response.txt", "EnterSubmitConsumerTrace");
                            oResponse = dtCustomVetting.SubmitConsumerTraceVettingQ(EnquiryCon, AdminCon, oSubscriber.SubscriberID, oSystemUser.SystemUserID, 137, oSubscriber.SubscriberName, iDnumber, passportNo, objInput.FirstName, objInput.Surname, objInput.BirthDate,
                                objInput.Gender, objInput.MaritalStatus, objInput.AddressLine1, objInput.AddressLine2, objInput.Suburb, objInput.City, objInput.PostalCode, objInput.HomeTelephoneCode, objInput.HomeTelephoneNumber, objInput.WorkTelephoneCode, objInput.WorkTelephoneNumber, objInput.CellNumber, double.Parse(objInput.GrossMonthlyIncome), objInput.Occupation, objInput.Employer, objInput.BankAccountType, objInput.BankName,
                                objInput.HighestQualification, objInput.Branch, objInput.BankAccountNumber, int.Parse(objInput.NumberOfYearsAtEmployer), string.Empty, true, false, string.Empty);
                           //  File.AppendAllText(@"C:\Log\Response.txt", "EndSubmitConsumerTrace");
                           //  File.AppendAllText(@"C:\Log\Response.txt", oResponse.ResponseData);
                            switch (oResponse.ResponseStatus)
                            {
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single:
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple:

                                    DataSet dsl = new DataSet();
                                    System.IO.StringReader xmlSR = new System.IO.StringReader(oResponse.ResponseData);
                                    dsl.ReadXml(xmlSR);

                                    //  File.AppendAllText(@"C:\Log\Response.txt", "End xml generate");

                                    if (dsl.Tables[0].Rows.Count == 0 || dsl.Tables[0].Rows.Count == 1)
                                    {
                                        foreach (DataRow dr in dsl.Tables[0].Rows)
                                        {
                                            XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplicationResponse oinputResponse = new XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplicationResponse();
                                            // oResponse = dtCustomVetting.SubmitMulipleCreditEnquiry(con, AdminCon, int.Parse(dr["EnquiryID"].ToString()), int.Parse(dr["EnquiryResultID"].ToString()), false, string.Empty, Unique_ref, QueryNumber.ToString());
                                            oinputResponse = oSResponse;


                                              // File.AppendAllText(@"C:\Log\Response.txt", "EnterSubmitCustomVettingO");
                                            oResponse = dtCustomVetting.SubmitCustomVettingQ(EnquiryCon, AdminCon, int.Parse(dr["EnquiryID"].ToString()), int.Parse(dr["EnquiryResultID"].ToString()), false, string.Empty, oinputResponse, oAPP, out oSResponse, 0, objInput.IsExistingClient, intCustomVettingOinputID);
                                            //  File.AppendAllText(@"C:\Log\Response.txt", "EndSubmitCustomVettingO");
                                        }
                                        switch (oResponse.ResponseStatus)
                                        {
                                            case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                                                oSResponse.MTNOutput.Status = "Failure";
                                                oSResponse.MTNOutput.ErrorDescription = oResponse.ResponseData;
                                                break;
                                            case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                                                oSResponse.MTNOutput.Status = "Success";
                                                //rsXml.LoadXml(rp.ResponseData);
                                                break;
                                        }
                                    }
                                    else
                                    {
                                        oSResponse.MTNOutput.Status = "Failure";
                                        oSResponse.MTNOutput.ErrorDescription = "Multiple Match Found";

                                    }
                                    break;

                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                                    oSResponse.MTNOutput.Status = "Failure";
                                    oSResponse.MTNOutput.ErrorDescription = oResponse.ResponseData;
                                    break;
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                                    oSResponse.MTNOutput.Status = "Failure";
                                    oSResponse.MTNOutput.ErrorDescription = "No Record Found";
                                    break;
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                                    oSResponse.MTNOutput.Status = "Success";
                                    oSResponse.MTNOutput.ErrorDescription = oResponse.ResponseData;
                                    break;
                            }


                        }
                        catch (Exception e)
                        {
                            oSResponse.MTNOutput.Status = "Failure";
                            oSResponse.MTNOutput.ErrorDescription = e.Message;
                        }

                    }
                    else
                    {
                        oSResponse.MTNOutput.Status = "Failure";
                        oSResponse.MTNOutput.ErrorDescription = ConnectTicket;
                    }
                }

            }
            catch (Exception ex)
            {
                oSResponse.MTNOutput.Status = "Failure";
                oSResponse.MTNOutput.ErrorDescription = ex.Message;
            }

            DateTime dtenddate = DateTime.Now;
            double diff = dtenddate.Subtract(dtStartdate).TotalSeconds;

            oSResponse.MTNOutput.BureauResponse.ProcessingTimeSecs = Math.Round(float.Parse(diff.ToString()), 2);

            if (oSResponse.MTNOutput.Status == "Failure")
            {
                oSResponse.MTNOutput.Outcome = "Decline";
                oSResponse.MTNOutput.BureauResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.MTNHResponse.BureauResponse.enumresponseStatus.FAILURE;
                oSResponse.MTNOutput.Reason = "Decline";
            }
            else if (oSResponse.MTNOutput.Status == "Success")
            {
                oSResponse.MTNOutput.Outcome = "Approve";
                oSResponse.MTNOutput.BureauResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.MTNHResponse.BureauResponse.enumresponseStatus.SUCCESS;
                oSResponse.MTNOutput.Reason = "Approve";
            }

            oSResponse.MTNOutput.BureauResponse.UniqueRefGuid = (Guid.NewGuid()).ToString();
            //oSResponse.SubmitResult = oSubmitResponse;
            string response = string.Empty;
            try
            {
                //  File.AppendAllText(@"C:\Log\Response.txt", "Enterdeserialization");
                XmlSerializer serializerout = new XmlSerializer(oSResponse.GetType());
                System.IO.StringWriter swout = new System.IO.StringWriter();
                serializerout.Serialize(swout, oSResponse);
                System.IO.StringReader readerout = new System.IO.StringReader(swout.ToString());

                response = readerout.ReadToEnd();

                SqlConnection con = new SqlConnection(Properties.Settings.Default.EnquiryConnectionString);

                SqlCommand cmd = new SqlCommand("Insert into CustomVettingQResponse (CustomVettingOinputID,Response,CreatedOnDate) values(@ID, @inputXMLstring,getdate())", con);
                cmd.Parameters.AddWithValue("@ID", intCustomVettingOinputID);
                cmd.Parameters.AddWithValue("@inputXMLstring", response);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                con.Close();
            }

            catch (Exception ex)
            {
                // File.AppendAllText(@"C:\Log\Response.txt", "EnterException");
                SqlConnection con = new SqlConnection(Properties.Settings.Default.EnquiryConnectionString);

                SqlCommand cmd = new SqlCommand("Insert into CustomVettingQResponse (CustomVettingOinputID,Response,CreatedOnDate) values(@ID, @inputXMLstring,getdate())", con);
                cmd.Parameters.AddWithValue("@ID", intCustomVettingOinputID);
                cmd.Parameters.AddWithValue("@inputXMLstring", ex.Message);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                con.Close();

            }

            return oSResponse.MTNOutput;

        }


        [WebMethod(Description = "")]
        // Method for MTN
        public string ConnectCustomVettingQ(int ProductIndicator, string Company, string RuleSet, string Branch, string User, string Password, string IsExistingClient, string GrossMonthlyIncome, string Surname, string FirstName, string IDType, string BirthDate, string IdentityNumber, string Gender, string MaritalStatus, string AddressLine1, string AddressLine2, string Suburb, string City, string PostalCode, string HomeTelephoneCode, string HomeTelephoneNumber, string WorkTelephoneCode, string WorkTelephoneNumber, string CellNumber, string Occupation, string Employer, string NumberOfYearsAtEmployer, string BankName, string BankAccountType, string BankBranchCode, string BankAccountNumber, string HighestQualification)
        {
            Company = string.IsNullOrEmpty(Company) ? string.Empty : Company;
            RuleSet = string.IsNullOrEmpty(RuleSet) ? string.Empty : RuleSet;
            Branch = string.IsNullOrEmpty(Branch) ? string.Empty : Branch;
            User = string.IsNullOrEmpty(User) ? string.Empty : User;
            Password = string.IsNullOrEmpty(Password) ? string.Empty : Password;
            IsExistingClient = string.IsNullOrEmpty(IsExistingClient) ? string.Empty : IsExistingClient;
            GrossMonthlyIncome = string.IsNullOrEmpty(GrossMonthlyIncome) ? string.Empty : GrossMonthlyIncome;
            Surname = string.IsNullOrEmpty(Surname) ? string.Empty : Surname;
            FirstName = string.IsNullOrEmpty(FirstName) ? string.Empty : FirstName;
            IDType = string.IsNullOrEmpty(IDType) ? string.Empty : IDType;
            BirthDate = string.IsNullOrEmpty(BirthDate) ? string.Empty : BirthDate;
            IdentityNumber = string.IsNullOrEmpty(IdentityNumber) ? string.Empty : IdentityNumber;
            Gender = string.IsNullOrEmpty(Gender) ? string.Empty : Gender;
            MaritalStatus = string.IsNullOrEmpty(MaritalStatus) ? string.Empty : MaritalStatus;
            AddressLine1 = string.IsNullOrEmpty(AddressLine1) ? string.Empty : AddressLine1;
            AddressLine2 = string.IsNullOrEmpty(AddressLine2) ? string.Empty : AddressLine2;
            Suburb = string.IsNullOrEmpty(Suburb) ? string.Empty : Suburb;
            City = string.IsNullOrEmpty(City) ? string.Empty : City;
            PostalCode = string.IsNullOrEmpty(PostalCode) ? string.Empty : PostalCode;
            HomeTelephoneCode = string.IsNullOrEmpty(HomeTelephoneCode) ? string.Empty : HomeTelephoneCode;
            HomeTelephoneNumber = string.IsNullOrEmpty(HomeTelephoneNumber) ? string.Empty : HomeTelephoneNumber;
            WorkTelephoneCode = string.IsNullOrEmpty(WorkTelephoneCode) ? string.Empty : WorkTelephoneCode;
            WorkTelephoneNumber = string.IsNullOrEmpty(WorkTelephoneNumber) ? string.Empty : WorkTelephoneNumber;
            CellNumber = string.IsNullOrEmpty(CellNumber) ? string.Empty : CellNumber;
            Occupation = string.IsNullOrEmpty(Occupation) ? string.Empty : Occupation;
            Employer = string.IsNullOrEmpty(Employer) ? string.Empty : Employer;
            NumberOfYearsAtEmployer = string.IsNullOrEmpty(NumberOfYearsAtEmployer) ? "0" : NumberOfYearsAtEmployer;
            BankName = string.IsNullOrEmpty(BankName) ? string.Empty : BankName;
            BankAccountType = string.IsNullOrEmpty(BankAccountType) ? string.Empty : BankAccountType;
            BankBranchCode = string.IsNullOrEmpty(BankBranchCode) ? string.Empty : BankBranchCode;
            BankAccountNumber = string.IsNullOrEmpty(BankAccountNumber) ? string.Empty : BankAccountNumber;
            HighestQualification = string.IsNullOrEmpty(HighestQualification) ? string.Empty : HighestQualification;

            // File.AppendAllText(@"C:\Log\JDVet.txt", "Step 1 \n");

            DateTime dtStartdate = DateTime.Now;

            //  File.AppendAllText(@"C:\Log\JDVet.txt", "Step 2 \n");
            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();
            string rXml = string.Empty, ConnectTicket = string.Empty;

            //  File.AppendAllText(@"C:\Log\JDVet.txt", "Step 3 \n");

            XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplicationResponse oSResponse = new XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplicationResponse();
            oSResponse.MTNOutput.BureauResponse = new XDSPortalLibrary.Entity_Layer.MTNHResponse.BureauResponse();

            // File.AppendAllText(@"C:\Log\JDVet.txt", "Step 4 \n");

            XmlSerializer serializer = new XmlSerializer(typeof(XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplication));
            XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplication objInput = new XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplication();

            // File.AppendAllText(@"C:\Log\JDVet.txt", "Step 5 \n");

            int intCustomVettingOinputID = 0;

            // File.AppendAllText(@"C:\Log\JDVet.txt", "Step 6 \n");

            try
            {

                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step 7 \n");
                SqlConnection con = new SqlConnection(EnquiryCon.ConnectionString);

                SqlCommand cmd = new SqlCommand("Insert into CustomVettingQinput (Input,CreatedOnDate) values(@inputXMLstring,getdate())", con);

                cmd.Parameters.AddWithValue("@inputXMLstring", ("productIndicator:" + ProductIndicator.ToString() + "Company:" + Company + "RuleSet:" + RuleSet + "Branch:" + Branch + "User:" + User + "Password:" + Password + "IsExistingClient:" + IsExistingClient + "GrossMonthlyIncome:" + GrossMonthlyIncome + "Surname:" + Surname + "FirstName:" + FirstName + "IDType:" + IDType + "BirthDate:" + BirthDate + "IdentityNumber:" + IdentityNumber + "Gender:" + Gender + "MaritalStatus:" + MaritalStatus + "AddressLine1:" + AddressLine1 + "AddressLine2:" + AddressLine2 + "Suburb:" + Suburb + "City:" + City + "PostalCode:" + PostalCode + "HomeTelephoneCode:" + HomeTelephoneCode + "HomeTelephoneNumber:" + HomeTelephoneNumber + "WorkTelephoneCode:" + WorkTelephoneCode + "WorkTelephoneNumber:" + WorkTelephoneNumber + "CellNumber:" + CellNumber + "Occupation:" + Occupation + "Employer:" + Employer + "NumberOfYearsAtEmployer:" + NumberOfYearsAtEmployer + "BankName:" + BankName + "BankAccountType:" + BankAccountType + "BankBranchCode:" + BankBranchCode + "BankAccountNumber:" + BankAccountNumber + "HighestQualification:" + HighestQualification));
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                string strSQL = "SELECT @@IDENTITY";
                SqlCommand Imycommand = new SqlCommand(strSQL, con);
                SqlDataReader Idr = Imycommand.ExecuteReader();
                Idr.Read();
                intCustomVettingOinputID = Convert.ToInt32(Idr.GetValue(0));
                Idr.Close();

                strSQL = "Insert into CustomVettingQInputValues (CustomVettingQinputID,Company,RuleSet,Branch,[User],[Password],IsExistingClient,GrossMonthlyIncome,";
                strSQL += "Surname,FirstName,IDType,BirthDate,IdentityNumber,Gender,MaritalStatus,AddressLine1,AddressLine2,Suburb,City,PostalCode,HomeTelephoneCode,";
                strSQL += "HomeTelephoneNumber,WorkTelephoneCode,WorkTelephoneNumber,CellNumber,Occupation,Employer,NumberOfYearsAtEmployer,BankName,BankAccountType,";
                strSQL += "BankBranchCode,BankAccountNumber,HighestQualification,CreatedOnDate) ";
                strSQL += "values(" + intCustomVettingOinputID + ",'" + Company + "','" + RuleSet + "','" + Branch + "','" + User + "','" + Password + "','";
                strSQL += IsExistingClient + "','" + GrossMonthlyIncome + "','" + Surname + "','" + FirstName + "','" + IDType + "','" + BirthDate + "','";
                strSQL += IdentityNumber + "','" + Gender + "','" + MaritalStatus + "','" + AddressLine1 + "','" + AddressLine2 + "','" + Suburb + "','";
                strSQL += City + "','" + PostalCode + "','" + HomeTelephoneCode + "','" + HomeTelephoneNumber + "','" + WorkTelephoneCode + "','";
                strSQL += WorkTelephoneNumber + "','" + CellNumber + "','" + Occupation + "','" + Employer + "','" + NumberOfYearsAtEmployer + "','";
                strSQL += BankName + "','" + BankAccountType + "','" + BankBranchCode + "','" + BankAccountNumber + "','" + HighestQualification + "', GETDATE())";

                cmd = new SqlCommand(strSQL, con);

                cmd.Parameters.AddWithValue("@inputXMLstring", ("productIndicator:" + ProductIndicator.ToString() + "Company:" + Company + "RuleSet:" + RuleSet + "Branch:" + Branch + "User:" + User + "Password:" + Password + "IsExistingClient:" + IsExistingClient + "GrossMonthlyIncome:" + GrossMonthlyIncome + "Surname:" + Surname + "FirstName:" + FirstName + "IDType:" + IDType + "BirthDate:" + BirthDate + "IdentityNumber:" + IdentityNumber + "Gender:" + Gender + "MaritalStatus:" + MaritalStatus + "AddressLine1:" + AddressLine1 + "AddressLine2:" + AddressLine2 + "Suburb:" + Suburb + "City:" + City + "PostalCode:" + PostalCode + "HomeTelephoneCode:" + HomeTelephoneCode + "HomeTelephoneNumber:" + HomeTelephoneNumber + "WorkTelephoneCode:" + WorkTelephoneCode + "WorkTelephoneNumber:" + WorkTelephoneNumber + "CellNumber:" + CellNumber + "Occupation:" + Occupation + "Employer:" + Employer + "NumberOfYearsAtEmployer:" + NumberOfYearsAtEmployer + "BankName:" + BankName + "BankAccountType:" + BankAccountType + "BankBranchCode:" + BankBranchCode + "BankAccountNumber:" + BankAccountNumber + "HighestQualification:" + HighestQualification));
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                con.Close();

                //parse XML

                //xml = xml.Replace("&", string.Empty);
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx \n");
                objInput.input = new XDSPortalLibrary.Entity_Layer.MTNHResponse.MTNInput();
                objInput.input.AddressLine1 = AddressLine1;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 1\n");
                objInput.input.AddressLine2 = AddressLine2;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 2\n");
                objInput.input.BankAccountNumber = BankAccountNumber;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 3\n");
                objInput.input.BankAccountType = BankAccountType;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 4\n");
                objInput.input.BankBranchCode = BankBranchCode;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 5\n");
                objInput.input.BankName = BankName;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 6\n");
                objInput.input.BirthDate = BirthDate;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 7\n");
                objInput.input.Branch = Branch;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 8\n");
                objInput.input.CellNumber = CellNumber;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 9\n");
                objInput.input.City = City;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 10\n");
                objInput.input.Company = Company;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 11\n");
                objInput.input.Employer = Employer;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 12\n");
                objInput.input.FirstName = FirstName;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 13\n");
                objInput.input.Gender = Gender;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 14\n");
                objInput.input.GrossMonthlyIncome = GrossMonthlyIncome;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 15\n");
                objInput.input.HighestQualification = HighestQualification;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 16\n");
                objInput.input.HomeTelephoneCode = HomeTelephoneCode;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 17\n");
                objInput.input.HomeTelephoneNumber = HomeTelephoneNumber;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 18\n");
                objInput.input.IdentityNumber = IdentityNumber;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 19\n");
                objInput.input.IDType = IDType;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 20\n");
                objInput.input.IsExistingClient = IsExistingClient;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 21\n");
                objInput.input.MaritalStatus = MaritalStatus;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 22\n");
                objInput.input.NumberOfYearsAtEmployer = NumberOfYearsAtEmployer;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 23\n");
                objInput.input.Occupation = Occupation;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 24\n");
                objInput.input.Password = Password;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 25\n");
                objInput.input.PostalCode = PostalCode;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 26\n");
                objInput.input.RuleSet = RuleSet;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 27\n");
                objInput.input.Suburb = Suburb;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 28\n");
                objInput.input.Surname = Surname;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 29\n");
                objInput.input.User = User;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 30\n");
                objInput.input.WorkTelephoneCode = WorkTelephoneCode;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 31\n");
                objInput.input.WorkTelephoneNumber = WorkTelephoneNumber;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 32\n");

                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step yyy \n");

                // oSResponse.SubmitResult.ClientReference = objInput.input.ClientReference;

                if (objInput.input.IDType.ToUpper() == XDSPortalLibrary.Entity_Layer.JDFSPrincipaRequest.IDType.OTHER.ToString())
                {
                    objInput.input.IDType = XDSPortalLibrary.Entity_Layer.JDFSPrincipaRequest.IDType.PASSPORT.ToString();
                }
                // Validate input

                XDSPortalLibrary.Entity_Layer.Response oResponse = new XDSPortalLibrary.Entity_Layer.Response();
                CustomVetting oCustomvetting = new CustomVetting();
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step ValidateConnectCustomVettingQ \n");
                oResponse = oCustomvetting.ValidateConnectCustomVettingQSOA(objInput);
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step ValidateConnectCustomVettingQ \n");

                if (oResponse.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                {
                    oSResponse.MTNOutput.Status = "Failure";
                    oSResponse.MTNOutput.ErrorDescription = oResponse.ResponseData;

                }
                else if (oResponse.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                {
                    // Login

                    ConnectTicket = Login(objInput.input.User, objInput.input.Password);


                    if (IsTicketValid(ConnectTicket))
                    {
                        FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);

                        string iDnumber = string.Empty, passportNo = string.Empty;

                        iDnumber = (objInput.input.IDType.ToUpper() == "SAID") ? objInput.input.IdentityNumber : string.Empty;
                        passportNo = (objInput.input.IDType.ToUpper() == "PASSPORT") ? objInput.input.IdentityNumber : string.Empty;
                        //passportNo = (objInput.Items.IDType.ToUpper() == XDSPortalLibrary.Entity_Layer.JDFSPrincipaRequest.IDType.OTHER.ToString()) ? objInput.Items.IDNumber : string.Empty;

                        try
                        {
                            XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                            XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(AdminCon, int.Parse(oTicket.Name));

                            XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
                            XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(AdminCon, oSystemUser.SubscriberID);

                            XDSPortalEnquiry.Business.CustomVetting dtCustomVetting = new XDSPortalEnquiry.Business.CustomVetting();

                            //   File.AppendAllText(@"C:\Log\Response.txt", "EnterSubmitConsumerTrace");
                            oResponse = dtCustomVetting.SubmitConsumerTraceVettingQ(EnquiryCon, AdminCon, oSubscriber.SubscriberID, oSystemUser.SystemUserID, 137, oSubscriber.SubscriberName, iDnumber, passportNo, objInput.input.FirstName, objInput.input.Surname, objInput.input.BirthDate,
                                objInput.input.Gender, objInput.input.MaritalStatus, objInput.input.AddressLine1, objInput.input.AddressLine2, objInput.input.Suburb, objInput.input.City, objInput.input.PostalCode, objInput.input.HomeTelephoneCode, objInput.input.HomeTelephoneNumber, objInput.input.WorkTelephoneCode, objInput.input.WorkTelephoneNumber, objInput.input.CellNumber, double.Parse(objInput.input.GrossMonthlyIncome), objInput.input.Occupation, objInput.input.Employer, objInput.input.BankAccountType, objInput.input.BankName,
                                objInput.input.HighestQualification, objInput.input.Branch, objInput.input.BankAccountNumber, int.Parse(objInput.input.NumberOfYearsAtEmployer), string.Empty, true, false, string.Empty);
                            // File.AppendAllText(@"C:\Log\Response.txt", "EndSubmitConsumerTrace");
                            // File.AppendAllText(@"C:\Log\Response.txt", oResponse.ResponseData);
                            switch (oResponse.ResponseStatus)
                            {
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single:
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple:

                                    DataSet dsl = new DataSet();
                                    System.IO.StringReader xmlSR = new System.IO.StringReader(oResponse.ResponseData);
                                    dsl.ReadXml(xmlSR);

                                    //  File.AppendAllText(@"C:\Log\Response.txt", "End xml generate");

                                    if (dsl.Tables[0].Rows.Count == 0 || dsl.Tables[0].Rows.Count == 1)
                                    {
                                        foreach (DataRow dr in dsl.Tables[0].Rows)
                                        {
                                            XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplicationResponse oinputResponse = new XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplicationResponse();
                                            // oResponse = dtCustomVetting.SubmitMulipleCreditEnquiry(con, AdminCon, int.Parse(dr["EnquiryID"].ToString()), int.Parse(dr["EnquiryResultID"].ToString()), false, string.Empty, Unique_ref, QueryNumber.ToString());
                                            oinputResponse = oSResponse;


                                            //   File.AppendAllText(@"C:\Log\Response.txt", "EnterSubmitCustomVettingO");
                                            oResponse = dtCustomVetting.SubmitCustomVettingQ(EnquiryCon, AdminCon, int.Parse(dr["EnquiryID"].ToString()), int.Parse(dr["EnquiryResultID"].ToString()), false, string.Empty, oinputResponse, objInput, out oSResponse, ProductIndicator, IsExistingClient, intCustomVettingOinputID);
                                            //  File.AppendAllText(@"C:\Log\Response.txt", "EndSubmitCustomVettingO");
                                        }
                                        switch (oResponse.ResponseStatus)
                                        {
                                            case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                                                oSResponse.MTNOutput.Status = "Failure";
                                                oSResponse.MTNOutput.ErrorDescription = oResponse.ResponseData;
                                                break;
                                            case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                                                //rsXml.LoadXml(rp.ResponseData);
                                                break;
                                        }
                                    }
                                    else
                                    {
                                        oSResponse.MTNOutput.Status = "Failure";
                                        oSResponse.MTNOutput.ErrorDescription = "Multiple Match Found";

                                    }
                                    break;

                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                                    oSResponse.MTNOutput.Status = "Failure";
                                    oSResponse.MTNOutput.ErrorDescription = oResponse.ResponseData;
                                    break;
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                                    oSResponse.MTNOutput.Status = "Failure";
                                    oSResponse.MTNOutput.ErrorDescription = "No Record Found";
                                    break;
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                                    oSResponse.MTNOutput.Status = "Success";
                                    oSResponse.MTNOutput.ErrorDescription = oResponse.ResponseData;
                                    break;
                            }


                        }
                        catch (Exception e)
                        {
                            oSResponse.MTNOutput.Status = "Failure";
                            oSResponse.MTNOutput.ErrorDescription = e.Message;
                        }

                    }
                    else
                    {
                        oSResponse.MTNOutput.Status = "Failure";
                        oSResponse.MTNOutput.ErrorDescription = ConnectTicket;
                    }
                }

            }
            catch (Exception ex)
            {
                oSResponse.MTNOutput.Status = "Failure";
                oSResponse.MTNOutput.ErrorDescription = ex.Message;
            }

            DateTime dtenddate = DateTime.Now;
            double diff = dtenddate.Subtract(dtStartdate).TotalSeconds;

            oSResponse.MTNOutput.BureauResponse.ProcessingTimeSecs = Math.Round(float.Parse(diff.ToString()), 2);

            if (oSResponse.MTNOutput.Status == "Failure")
            {
                oSResponse.MTNOutput.Outcome = "Decline";
                oSResponse.MTNOutput.BureauResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.MTNHResponse.BureauResponse.enumresponseStatus.FAILURE;
                oSResponse.MTNOutput.Reason = "Decline";
            }
            else if (oSResponse.MTNOutput.Status == "Success")
            {
                oSResponse.MTNOutput.Outcome = "Approve";
                oSResponse.MTNOutput.BureauResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.MTNHResponse.BureauResponse.enumresponseStatus.SUCCESS;
                oSResponse.MTNOutput.Reason = "Approve";
            }

            oSResponse.MTNOutput.BureauResponse.UniqueRefGuid = (Guid.NewGuid()).ToString();
            //oSResponse.SubmitResult = oSubmitResponse;
            string response = string.Empty;
            try
            {
                //  File.AppendAllText(@"C:\Log\Response.txt", "Enterdeserialization");
                XmlSerializer serializerout = new XmlSerializer(oSResponse.GetType());
                System.IO.StringWriter swout = new System.IO.StringWriter();
                serializerout.Serialize(swout, oSResponse);
                System.IO.StringReader readerout = new System.IO.StringReader(swout.ToString());

                response = readerout.ReadToEnd();

                SqlConnection con = new SqlConnection(Properties.Settings.Default.EnquiryConnectionString);

                SqlCommand cmd = new SqlCommand("Insert into CustomVettingQResponse (CustomVettingOinputID,Response,CreatedOnDate) values(@ID, @inputXMLstring,getdate())", con);
                cmd.Parameters.AddWithValue("@ID", intCustomVettingOinputID);
                cmd.Parameters.AddWithValue("@inputXMLstring", response);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                con.Close();
            }

            catch (Exception ex)
            {
                // File.AppendAllText(@"C:\Log\Response.txt", "EnterException");
                SqlConnection con = new SqlConnection(Properties.Settings.Default.EnquiryConnectionString);

                SqlCommand cmd = new SqlCommand("Insert into CustomVettingQResponse (CustomVettingOinputID,Response,CreatedOnDate) values(@ID, @inputXMLstring,getdate())", con);
                cmd.Parameters.AddWithValue("@ID", intCustomVettingOinputID);
                cmd.Parameters.AddWithValue("@inputXMLstring", ex.Message);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                con.Close();

            }

            return response;

        }

        [WebMethod(Description = "")]
        // Method for MTN
        public XDSPortalLibrary.Entity_Layer.MTNHResponse.MTNOutput SubmitApplicationA(XDSPortalLibrary.Entity_Layer.MTNHResponse.MTNInput MTNInput)
        {

            MTNInput.DealerCode = string.IsNullOrEmpty(MTNInput.DealerCode) ? string.Empty : MTNInput.DealerCode;
            MTNInput.CustomerName = string.IsNullOrEmpty(MTNInput.CustomerName) ? string.Empty : MTNInput.CustomerName;
            MTNInput.Channel = string.IsNullOrEmpty(MTNInput.Channel) ? string.Empty : MTNInput.Channel;
            MTNInput.AlternateContactNumber = string.IsNullOrEmpty(MTNInput.AlternateContactNumber) ? string.Empty : MTNInput.AlternateContactNumber;
            MTNInput.EmailAddress = string.IsNullOrEmpty(MTNInput.EmailAddress) ? string.Empty : MTNInput.EmailAddress;

            // File.AppendAllText(@"C:\Log\JDVet.txt", "Step 1 \n");

            DateTime dtStartdate = DateTime.Now;

            //  File.AppendAllText(@"C:\Log\JDVet.txt", "Step 2 \n");
            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();
            string rXml = string.Empty, ConnectTicket = string.Empty;

            //  File.AppendAllText(@"C:\Log\JDVet.txt", "Step 3 \n");

            XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplicationResponse oSResponse = new XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplicationResponse();
            oSResponse.MTNOutput.BureauResponse = new XDSPortalLibrary.Entity_Layer.MTNHResponse.BureauResponse();

            // File.AppendAllText(@"C:\Log\JDVet.txt", "Step 4 \n");

            XmlSerializer serializer = new XmlSerializer(typeof(XDSPortalLibrary.Entity_Layer.MTNHResponse.MTNInput));
            XDSPortalLibrary.Entity_Layer.MTNHResponse.MTNInput objInput = new XDSPortalLibrary.Entity_Layer.MTNHResponse.MTNInput();

            // File.AppendAllText(@"C:\Log\JDVet.txt", "Step 5 \n");

            int intCustomVettingOinputID = 0;

            // File.AppendAllText(@"C:\Log\JDVet.txt", "Step 6 \n");

            try
            {

                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step 7 \n");
                SqlConnection con = new SqlConnection(EnquiryCon.ConnectionString);
                SqlConnection MTNCon = new SqlConnection(MTNEnquiryCon.ConnectionString);

                XmlSerializer serializerinput = new XmlSerializer(MTNInput.GetType());
                System.IO.StringWriter swinput = new System.IO.StringWriter();
                serializerinput.Serialize(swinput, MTNInput);
                System.IO.StringReader readerin = new System.IO.StringReader(swinput.ToString());

                string strinput = readerin.ReadToEnd();


                SqlCommand cmd = new SqlCommand("Insert into CustomVettingQinput (Input,CreatedOnDate) values(@inputXMLstring,getdate())", con);

                //cmd.Parameters.AddWithValue("@inputXMLstring", ("productIndicator:" + ProductIndicator.ToString() + "Company:" + Company + "RuleSet:" + RuleSet + "Branch:" + Branch + "User:" + User + "Password:" + Password + "IsExistingClient:" + IsExistingClient + "GrossMonthlyIncome:" + GrossMonthlyIncome + "Surname:" + Surname + "FirstName:" + FirstName + "IDType:" + IDType + "BirthDate:" + BirthDate + "IdentityNumber:" + IdentityNumber + "Gender:" + Gender + "MaritalStatus:" + MaritalStatus + "AddressLine1:" + AddressLine1 + "AddressLine2:" + AddressLine2 + "Suburb:" + Suburb + "City:" + City + "PostalCode:" + PostalCode + "HomeTelephoneCode:" + HomeTelephoneCode + "HomeTelephoneNumber:" + HomeTelephoneNumber + "WorkTelephoneCode:" + WorkTelephoneCode + "WorkTelephoneNumber:" + WorkTelephoneNumber + "CellNumber:" + CellNumber + "Occupation:" + Occupation + "Employer:" + Employer + "NumberOfYearsAtEmployer:" + NumberOfYearsAtEmployer + "BankName:" + BankName + "BankAccountType:" + BankAccountType + "BankBranchCode:" + BankBranchCode + "BankAccountNumber:" + BankAccountNumber + "HighestQualification:" + HighestQualification));
                cmd.Parameters.AddWithValue("@inputXMLstring", strinput);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                string strSQL = "SELECT @@IDENTITY";
                SqlCommand Imycommand = new SqlCommand(strSQL, con);
                SqlDataReader Idr = Imycommand.ExecuteReader();
                Idr.Read();
                intCustomVettingOinputID = Convert.ToInt32(Idr.GetValue(0));
                Idr.Close();

                con.Close();

                string strQuery = "Insert into MTNCustomVettingInput (CustomVettingOinputID,MonthlyIncome,Surname,FirstName,IDType,SAIDBirthDate,IdentityNumber,Gender,MaritalStatus,AddressLine1,";
                strQuery += "AddressLine2,Suburb,City,PostalCode,HomeTelephoneCode,HomeTelephoneNumber,WorkTelephoneCode,WorkTelephoneNumber,CellNumber,Occupation,Employer,";
                strQuery += "NumberOfYearsAtEmployer,BankName,BankAccountType,BankBranchCode,BankAccountNumber,HighestQualification,StatusInd,EnquiryDate, Dealercode, CustomerName, Channel, AlternateContactNumber ,EmailAddress ) ";
                strQuery += "values('" + intCustomVettingOinputID.ToString() + "','" + MTNInput.GrossMonthlyIncome + "','" + MTNInput.Surname + "','" + MTNInput.FirstName + "','" + MTNInput.IDType + "','";
                strQuery += MTNInput.BirthDate + "','" + MTNInput.IdentityNumber + "','" + MTNInput.Gender + "','" + MTNInput.MaritalStatus + "','" + MTNInput.AddressLine1 + "','";
                strQuery += MTNInput.AddressLine2 + "','" + MTNInput.Suburb + "','" + MTNInput.City + "','" + MTNInput.PostalCode + "','" + MTNInput.HomeTelephoneCode + "','";
                strQuery += MTNInput.HomeTelephoneNumber + "','" + MTNInput.WorkTelephoneCode + "','" + MTNInput.WorkTelephoneNumber + "','" + MTNInput.CellNumber + "','";
                strQuery += MTNInput.Occupation + "','" + MTNInput.Employer + "','" + MTNInput.NumberOfYearsAtEmployer + "','" + MTNInput.BankName + "','";
                strQuery += MTNInput.BankAccountType + "','" + MTNInput.BankBranchCode + "','" + MTNInput.BankAccountNumber + "','" + MTNInput.HighestQualification + "',";
                strQuery += "1, getdate(),'" + MTNInput.DealerCode + "','" + MTNInput.CustomerName + "','" + MTNInput.Channel + "','" + MTNInput.AlternateContactNumber.Replace("'", "''") + "','" + MTNInput.EmailAddress.Replace("'", "''") + "')";

                cmd = new SqlCommand(strQuery, MTNCon);

                if (MTNCon.State == ConnectionState.Closed)
                    MTNCon.Open();

                cmd.ExecuteNonQuery();

                MTNCon.Close();

                //parse XML


                //  File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx \n");
                //objInput.input = new XDSPortalLibrary.Entity_Layer.SubmitApplicationInput();
                //objInput.input.AddressLine1 = AddressLine1;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 1\n");
                //objInput.input.AddressLine2 = AddressLine2;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 2\n");
                //objInput.input.BankAccountNumber = BankAccountNumber;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 3\n");
                //objInput.input.BankAccountType = BankAccountType;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 4\n");
                //objInput.input.BankBranchCode = BankBranchCode;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 5\n");
                //objInput.input.BankName = BankName;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 6\n");
                //objInput.input.BirthDate = BirthDate;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 7\n");
                //objInput.input.Branch = Branch;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 8\n");
                //objInput.input.CellNumber = CellNumber;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 9\n");
                //objInput.input.City = City;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 10\n");
                //objInput.input.Company = Company;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 11\n");
                //objInput.input.Employer = Employer;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 12\n");
                //objInput.input.FirstName = FirstName;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 13\n");
                //objInput.input.Gender = Gender;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 14\n");
                //objInput.input.GrossMonthlyIncome = GrossMonthlyIncome;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 15\n");
                //objInput.input.HighestQualification = HighestQualification;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 16\n");
                //objInput.input.HomeTelephoneCode = HomeTelephoneCode;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 17\n");
                //objInput.input.HomeTelephoneNumber = HomeTelephoneNumber;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 18\n");
                //objInput.input.IdentityNumber = IdentityNumber;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 19\n");
                //objInput.input.IDType = IDType;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 20\n");
                //objInput.input.IsExistingClient = IsExistingClient;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 21\n");
                //objInput.input.MaritalStatus = MaritalStatus;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 22\n");
                //objInput.input.NumberOfYearsAtEmployer = NumberOfYearsAtEmployer;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 23\n");
                //objInput.input.Occupation = Occupation;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 24\n");
                //objInput.input.Password = Password;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 25\n");
                //objInput.input.PostalCode = PostalCode;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 26\n");
                //objInput.input.RuleSet = RuleSet;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 27\n");
                //objInput.input.Suburb = Suburb;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 28\n");
                //objInput.input.Surname = Surname;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 29\n");
                //objInput.input.User = User;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 30\n");
                //objInput.input.WorkTelephoneCode = WorkTelephoneCode;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 31\n");
                //objInput.input.WorkTelephoneNumber = WorkTelephoneNumber;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 32\n");

                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step yyy \n");
                objInput = MTNInput;// (XDSPortalLibrary.Entity_Layer.MTNInput)serializer.Deserialize(new XmlTextReader(new StringReader(xml)));

                XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplication oAPP = new XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplication();
                oAPP.input = objInput;
                // oSResponse.SubmitResult.ClientReference = objInput.input.ClientReference;

                //if (objInput.IDType.ToUpper() == XDSPortalLibrary.Entity_Layer.JDFSPrincipaRequest.IDType.OTHER.ToString())
                //{
                //    objInput.IDType = XDSPortalLibrary.Entity_Layer.JDFSPrincipaRequest.IDType.PASSPORT.ToString();
                //}
                // Validate input

                XDSPortalLibrary.Entity_Layer.Response oResponse = new XDSPortalLibrary.Entity_Layer.Response();
                CustomVetting oCustomvetting = new CustomVetting();
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step ValidateConnectCustomVettingQ \n");
                oResponse = oCustomvetting.ValidateConnectCustomVettingQA(oAPP);
                //  File.AppendAllText(@"C:\Log\JDVet.txt", "Step ValidateConnectCustomVettingQ \n");

                if (oResponse.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                {
                    oSResponse.MTNOutput.Status = "Failure";
                    oSResponse.MTNOutput.ErrorDescription = oResponse.ResponseData;

                }
                else if (oResponse.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                {
                    // Login

                    ConnectTicket = Login(objInput.User, objInput.Password);


                    if (IsTicketValid(ConnectTicket))
                    {
                        FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);

                        string iDnumber = string.Empty, passportNo = string.Empty;

                        iDnumber = (objInput.IDType.ToUpper() == "1") ? objInput.IdentityNumber : string.Empty;
                        passportNo = (objInput.IDType.ToUpper() == "2") ? objInput.IdentityNumber : string.Empty;

                        objInput.NumberOfYearsAtEmployer = string.IsNullOrEmpty(objInput.NumberOfYearsAtEmployer) ? "0" : objInput.NumberOfYearsAtEmployer;
                        //passportNo = (objInput.Items.IDType.ToUpper() == XDSPortalLibrary.Entity_Layer.JDFSPrincipaRequest.IDType.OTHER.ToString()) ? objInput.Items.IDNumber : string.Empty;

                        try
                        {
                            XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                            XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(AdminCon, int.Parse(oTicket.Name));

                            XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
                            XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(AdminCon, oSystemUser.SubscriberID);

                            XDSPortalEnquiry.Business.CustomVetting dtCustomVetting = new XDSPortalEnquiry.Business.CustomVetting();

                            //   File.AppendAllText(@"C:\Log\Response.txt", "EnterSubmitConsumerTrace");
                            oResponse = dtCustomVetting.SubmitConsumerTraceVettingQ(EnquiryCon, AdminCon, oSubscriber.SubscriberID, oSystemUser.SystemUserID, 137, oSubscriber.SubscriberName, iDnumber, passportNo, objInput.FirstName, objInput.Surname, objInput.BirthDate,
                                objInput.Gender, objInput.MaritalStatus, objInput.AddressLine1, objInput.AddressLine2, objInput.Suburb, objInput.City, objInput.PostalCode, objInput.HomeTelephoneCode, objInput.HomeTelephoneNumber, objInput.WorkTelephoneCode, objInput.WorkTelephoneNumber, objInput.CellNumber, double.Parse(objInput.GrossMonthlyIncome), objInput.Occupation, objInput.Employer, objInput.BankAccountType, objInput.BankName,
                                objInput.HighestQualification, objInput.Branch, objInput.BankAccountNumber, int.Parse(objInput.NumberOfYearsAtEmployer), string.Empty, true, false, string.Empty);
                            // File.AppendAllText(@"C:\Log\Response.txt", "EndSubmitConsumerTrace");
                            // File.AppendAllText(@"C:\Log\Response.txt", oResponse.ResponseData);
                            switch (oResponse.ResponseStatus)
                            {
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single:
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple:

                                    DataSet dsl = new DataSet();
                                    System.IO.StringReader xmlSR = new System.IO.StringReader(oResponse.ResponseData);
                                    dsl.ReadXml(xmlSR);

                                    //  File.AppendAllText(@"C:\Log\Response.txt", "End xml generate");

                                    if (dsl.Tables[0].Rows.Count == 0 || dsl.Tables[0].Rows.Count == 1)
                                    {
                                        foreach (DataRow dr in dsl.Tables[0].Rows)
                                        {
                                            XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplicationResponse oinputResponse = new XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplicationResponse();
                                            // oResponse = dtCustomVetting.SubmitMulipleCreditEnquiry(con, AdminCon, int.Parse(dr["EnquiryID"].ToString()), int.Parse(dr["EnquiryResultID"].ToString()), false, string.Empty, Unique_ref, QueryNumber.ToString());
                                            oinputResponse = oSResponse;


                                           // File.AppendAllText(@"C:\Log\Response.txt", "EnterSubmitCustomVettingO");
                                            oResponse = dtCustomVetting.SubmitCustomVettingQA(EnquiryCon, AdminCon, int.Parse(dr["EnquiryID"].ToString()), int.Parse(dr["EnquiryResultID"].ToString()), false, string.Empty, oinputResponse, oAPP, out oSResponse, 0, objInput.IsExistingClient);
                                            //File.AppendAllText(@"C:\Log\Response.txt", "EndSubmitCustomVettingO");
                                        }
                                        switch (oResponse.ResponseStatus)
                                        {
                                            case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                                                oSResponse.MTNOutput.Status = "Failure";
                                                oSResponse.MTNOutput.ErrorDescription = oResponse.ResponseData;
                                                break;
                                            case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                                                oSResponse.MTNOutput.Status = "Success";
                                                //rsXml.LoadXml(rp.ResponseData);
                                                break;
                                        }
                                    }
                                    else
                                    {
                                        oSResponse.MTNOutput.Status = "Failure";
                                        oSResponse.MTNOutput.ErrorDescription = "Multiple Match Found";

                                    }
                                    break;

                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                                    oSResponse.MTNOutput.Status = "Failure";
                                    oSResponse.MTNOutput.ErrorDescription = oResponse.ResponseData;
                                    break;
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                                    oSResponse.MTNOutput.Status = "Failure";
                                    oSResponse.MTNOutput.ErrorDescription = "No Record Found";
                                    break;
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                                    oSResponse.MTNOutput.Status = "Success";
                                    oSResponse.MTNOutput.ErrorDescription = oResponse.ResponseData;
                                    break;
                            }


                        }
                        catch (Exception e)
                        {
                            oSResponse.MTNOutput.Status = "Failure";
                            oSResponse.MTNOutput.ErrorDescription = e.Message;
                        }

                    }
                    else
                    {
                        oSResponse.MTNOutput.Status = "Failure";
                        oSResponse.MTNOutput.ErrorDescription = ConnectTicket;
                    }
                }

            }
            catch (Exception ex)
            {
                oSResponse.MTNOutput.Status = "Failure";
                oSResponse.MTNOutput.ErrorDescription = ex.Message;
            }

            DateTime dtenddate = DateTime.Now;
            double diff = dtenddate.Subtract(dtStartdate).TotalSeconds;

            oSResponse.MTNOutput.BureauResponse.ProcessingTimeSecs = Math.Round(float.Parse(diff.ToString()), 2);

            if (oSResponse.MTNOutput.Status == "Failure")
            {
                oSResponse.MTNOutput.Outcome = "Decline";
                oSResponse.MTNOutput.BureauResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.MTNHResponse.BureauResponse.enumresponseStatus.FAILURE;
                oSResponse.MTNOutput.Reason = "Decline";
            }
            else if (oSResponse.MTNOutput.Status == "Success")
            {
                oSResponse.MTNOutput.Outcome = "Approve";
                oSResponse.MTNOutput.BureauResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.MTNHResponse.BureauResponse.enumresponseStatus.SUCCESS;
                oSResponse.MTNOutput.Reason = "Approve";
            }

            oSResponse.MTNOutput.BureauResponse.UniqueRefGuid = (Guid.NewGuid()).ToString();
            //oSResponse.SubmitResult = oSubmitResponse;
            string response = string.Empty;
            try
            {
                //  File.AppendAllText(@"C:\Log\Response.txt", "Enterdeserialization");
                XmlSerializer serializerout = new XmlSerializer(oSResponse.GetType());
                System.IO.StringWriter swout = new System.IO.StringWriter();
                serializerout.Serialize(swout, oSResponse);
                System.IO.StringReader readerout = new System.IO.StringReader(swout.ToString());

                response = readerout.ReadToEnd();

                SqlConnection con = new SqlConnection(Properties.Settings.Default.EnquiryConnectionString);

                SqlCommand cmd = new SqlCommand("Insert into CustomVettingQResponse (CustomVettingOinputID,Response,CreatedOnDate) values(@ID, @inputXMLstring,getdate())", con);
                cmd.Parameters.AddWithValue("@ID", intCustomVettingOinputID);
                cmd.Parameters.AddWithValue("@inputXMLstring", response);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                con.Close();
            }

            catch (Exception ex)
            {
                // File.AppendAllText(@"C:\Log\Response.txt", "EnterException");
                SqlConnection con = new SqlConnection(Properties.Settings.Default.EnquiryConnectionString);

                SqlCommand cmd = new SqlCommand("Insert into CustomVettingQResponse (CustomVettingOinputID,Response,CreatedOnDate) values(@ID, @inputXMLstring,getdate())", con);
                cmd.Parameters.AddWithValue("@ID", intCustomVettingOinputID);
                cmd.Parameters.AddWithValue("@inputXMLstring", ex.Message);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                con.Close();

            }

            return oSResponse.MTNOutput;

        }
        [WebMethod(Description = "")]
        public XDSPortalLibrary.Entity_Layer.MTNHResponse.MTNOutput SubmitApplicationATelesales(XDSPortalLibrary.Entity_Layer.MTNHResponse.MTNInput MTNInput)
        {

            MTNInput.DealerCode = string.IsNullOrEmpty(MTNInput.DealerCode) ? string.Empty : MTNInput.DealerCode;
            MTNInput.CustomerName = string.IsNullOrEmpty(MTNInput.CustomerName) ? string.Empty : MTNInput.CustomerName;
            MTNInput.Channel = string.IsNullOrEmpty(MTNInput.Channel) ? string.Empty : MTNInput.Channel;
            MTNInput.AlternateContactNumber = string.IsNullOrEmpty(MTNInput.AlternateContactNumber) ? string.Empty : MTNInput.AlternateContactNumber;
            MTNInput.EmailAddress = string.IsNullOrEmpty(MTNInput.EmailAddress) ? string.Empty : MTNInput.EmailAddress;

            // File.AppendAllText(@"C:\Log\JDVet.txt", "Step 1 \n");

            DateTime dtStartdate = DateTime.Now;

            //  File.AppendAllText(@"C:\Log\JDVet.txt", "Step 2 \n");
            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();
            string rXml = string.Empty, ConnectTicket = string.Empty;

            //  File.AppendAllText(@"C:\Log\JDVet.txt", "Step 3 \n");

            XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplicationResponse oSResponse = new XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplicationResponse();
            oSResponse.MTNOutput.BureauResponse = new XDSPortalLibrary.Entity_Layer.MTNHResponse.BureauResponse();

            // File.AppendAllText(@"C:\Log\JDVet.txt", "Step 4 \n");

            XmlSerializer serializer = new XmlSerializer(typeof(XDSPortalLibrary.Entity_Layer.MTNHResponse.MTNInput));
            XDSPortalLibrary.Entity_Layer.MTNHResponse.MTNInput objInput = new XDSPortalLibrary.Entity_Layer.MTNHResponse.MTNInput();

            // File.AppendAllText(@"C:\Log\JDVet.txt", "Step 5 \n");

            int intCustomVettingOinputID = 0;

            // File.AppendAllText(@"C:\Log\JDVet.txt", "Step 6 \n");

            try
            {

                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step 7 \n");
                SqlConnection con = new SqlConnection(EnquiryCon.ConnectionString);
                SqlConnection MTNCon = new SqlConnection(MTNEnquiryCon.ConnectionString);

                XmlSerializer serializerinput = new XmlSerializer(MTNInput.GetType());
                System.IO.StringWriter swinput = new System.IO.StringWriter();
                serializerinput.Serialize(swinput, MTNInput);
                System.IO.StringReader readerin = new System.IO.StringReader(swinput.ToString());

                string strinput = readerin.ReadToEnd();


                SqlCommand cmd = new SqlCommand("Insert into CustomVettingQinput (Input,CreatedOnDate) values(@inputXMLstring,getdate())", con);

                //cmd.Parameters.AddWithValue("@inputXMLstring", ("productIndicator:" + ProductIndicator.ToString() + "Company:" + Company + "RuleSet:" + RuleSet + "Branch:" + Branch + "User:" + User + "Password:" + Password + "IsExistingClient:" + IsExistingClient + "GrossMonthlyIncome:" + GrossMonthlyIncome + "Surname:" + Surname + "FirstName:" + FirstName + "IDType:" + IDType + "BirthDate:" + BirthDate + "IdentityNumber:" + IdentityNumber + "Gender:" + Gender + "MaritalStatus:" + MaritalStatus + "AddressLine1:" + AddressLine1 + "AddressLine2:" + AddressLine2 + "Suburb:" + Suburb + "City:" + City + "PostalCode:" + PostalCode + "HomeTelephoneCode:" + HomeTelephoneCode + "HomeTelephoneNumber:" + HomeTelephoneNumber + "WorkTelephoneCode:" + WorkTelephoneCode + "WorkTelephoneNumber:" + WorkTelephoneNumber + "CellNumber:" + CellNumber + "Occupation:" + Occupation + "Employer:" + Employer + "NumberOfYearsAtEmployer:" + NumberOfYearsAtEmployer + "BankName:" + BankName + "BankAccountType:" + BankAccountType + "BankBranchCode:" + BankBranchCode + "BankAccountNumber:" + BankAccountNumber + "HighestQualification:" + HighestQualification));
                cmd.Parameters.AddWithValue("@inputXMLstring", strinput);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                string strSQL = "SELECT @@IDENTITY";
                SqlCommand Imycommand = new SqlCommand(strSQL, con);
                SqlDataReader Idr = Imycommand.ExecuteReader();
                Idr.Read();
                intCustomVettingOinputID = Convert.ToInt32(Idr.GetValue(0));
                Idr.Close();

                con.Close();

                string strQuery = "Insert into MTNCustomVettingInput (CustomVettingOinputID,MonthlyIncome,Surname,FirstName,IDType,SAIDBirthDate,IdentityNumber,Gender,MaritalStatus,AddressLine1,";
                strQuery += "AddressLine2,Suburb,City,PostalCode,HomeTelephoneCode,HomeTelephoneNumber,WorkTelephoneCode,WorkTelephoneNumber,CellNumber,Occupation,Employer,";
                strQuery += "NumberOfYearsAtEmployer,BankName,BankAccountType,BankBranchCode,BankAccountNumber,HighestQualification,StatusInd,EnquiryDate, Dealercode, CustomerName, Channel, AlternateContactNumber ,EmailAddress ) ";
                strQuery += "values('" + intCustomVettingOinputID.ToString() + "','" + MTNInput.GrossMonthlyIncome + "','" + MTNInput.Surname + "','" + MTNInput.FirstName + "','" + MTNInput.IDType + "','";
                strQuery += MTNInput.BirthDate + "','" + MTNInput.IdentityNumber + "','" + MTNInput.Gender + "','" + MTNInput.MaritalStatus + "','" + MTNInput.AddressLine1 + "','";
                strQuery += MTNInput.AddressLine2 + "','" + MTNInput.Suburb + "','" + MTNInput.City + "','" + MTNInput.PostalCode + "','" + MTNInput.HomeTelephoneCode + "','";
                strQuery += MTNInput.HomeTelephoneNumber + "','" + MTNInput.WorkTelephoneCode + "','" + MTNInput.WorkTelephoneNumber + "','" + MTNInput.CellNumber + "','";
                strQuery += MTNInput.Occupation + "','" + MTNInput.Employer + "','" + MTNInput.NumberOfYearsAtEmployer + "','" + MTNInput.BankName + "','";
                strQuery += MTNInput.BankAccountType + "','" + MTNInput.BankBranchCode + "','" + MTNInput.BankAccountNumber + "','" + MTNInput.HighestQualification + "',";
                strQuery += "1, getdate(),'" + MTNInput.DealerCode + "','" + MTNInput.CustomerName + "','" + MTNInput.Channel + "','" + MTNInput.AlternateContactNumber.Replace("'", "''") + "','" + MTNInput.EmailAddress.Replace("'", "''") + "')";

                cmd = new SqlCommand(strQuery, MTNCon);

                if (MTNCon.State == ConnectionState.Closed)
                    MTNCon.Open();

                cmd.ExecuteNonQuery();

                MTNCon.Close();

                //parse XML


                //  File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx \n");
                //objInput.input = new XDSPortalLibrary.Entity_Layer.SubmitApplicationInput();
                //objInput.input.AddressLine1 = AddressLine1;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 1\n");
                //objInput.input.AddressLine2 = AddressLine2;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 2\n");
                //objInput.input.BankAccountNumber = BankAccountNumber;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 3\n");
                //objInput.input.BankAccountType = BankAccountType;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 4\n");
                //objInput.input.BankBranchCode = BankBranchCode;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 5\n");
                //objInput.input.BankName = BankName;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 6\n");
                //objInput.input.BirthDate = BirthDate;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 7\n");
                //objInput.input.Branch = Branch;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 8\n");
                //objInput.input.CellNumber = CellNumber;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 9\n");
                //objInput.input.City = City;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 10\n");
                //objInput.input.Company = Company;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 11\n");
                //objInput.input.Employer = Employer;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 12\n");
                //objInput.input.FirstName = FirstName;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 13\n");
                //objInput.input.Gender = Gender;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 14\n");
                //objInput.input.GrossMonthlyIncome = GrossMonthlyIncome;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 15\n");
                //objInput.input.HighestQualification = HighestQualification;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 16\n");
                //objInput.input.HomeTelephoneCode = HomeTelephoneCode;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 17\n");
                //objInput.input.HomeTelephoneNumber = HomeTelephoneNumber;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 18\n");
                //objInput.input.IdentityNumber = IdentityNumber;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 19\n");
                //objInput.input.IDType = IDType;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 20\n");
                //objInput.input.IsExistingClient = IsExistingClient;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 21\n");
                //objInput.input.MaritalStatus = MaritalStatus;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 22\n");
                //objInput.input.NumberOfYearsAtEmployer = NumberOfYearsAtEmployer;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 23\n");
                //objInput.input.Occupation = Occupation;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 24\n");
                //objInput.input.Password = Password;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 25\n");
                //objInput.input.PostalCode = PostalCode;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 26\n");
                //objInput.input.RuleSet = RuleSet;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 27\n");
                //objInput.input.Suburb = Suburb;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 28\n");
                //objInput.input.Surname = Surname;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 29\n");
                //objInput.input.User = User;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 30\n");
                //objInput.input.WorkTelephoneCode = WorkTelephoneCode;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 31\n");
                //objInput.input.WorkTelephoneNumber = WorkTelephoneNumber;
                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 32\n");

                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step yyy \n");
                objInput = MTNInput;// (XDSPortalLibrary.Entity_Layer.MTNInput)serializer.Deserialize(new XmlTextReader(new StringReader(xml)));

                XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplication oAPP = new XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplication();
                oAPP.input = objInput;
                // oSResponse.SubmitResult.ClientReference = objInput.input.ClientReference;

                //if (objInput.IDType.ToUpper() == XDSPortalLibrary.Entity_Layer.JDFSPrincipaRequest.IDType.OTHER.ToString())
                //{
                //    objInput.IDType = XDSPortalLibrary.Entity_Layer.JDFSPrincipaRequest.IDType.PASSPORT.ToString();
                //}
                // Validate input

                XDSPortalLibrary.Entity_Layer.Response oResponse = new XDSPortalLibrary.Entity_Layer.Response();
                CustomVetting oCustomvetting = new CustomVetting();
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step ValidateConnectCustomVettingQ \n");
                oResponse = oCustomvetting.ValidateConnectCustomVettingQATeleSales(oAPP);
                //  File.AppendAllText(@"C:\Log\JDVet.txt", "Step ValidateConnectCustomVettingQ \n");

                if (oResponse.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                {
                    oSResponse.MTNOutput.Status = "Failure";
                    oSResponse.MTNOutput.ErrorDescription = oResponse.ResponseData;

                }
                else if (oResponse.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                {
                    // Login

                    ConnectTicket = Login(objInput.User, objInput.Password);


                    if (IsTicketValid(ConnectTicket))
                    {
                        FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);

                        string iDnumber = string.Empty, passportNo = string.Empty;

                        iDnumber = (objInput.IDType.ToUpper() == "1") ? objInput.IdentityNumber : string.Empty;
                        passportNo = (objInput.IDType.ToUpper() == "2") ? objInput.IdentityNumber : string.Empty;

                        objInput.NumberOfYearsAtEmployer = string.IsNullOrEmpty(objInput.NumberOfYearsAtEmployer) ? "0" : objInput.NumberOfYearsAtEmployer;
                        //passportNo = (objInput.Items.IDType.ToUpper() == XDSPortalLibrary.Entity_Layer.JDFSPrincipaRequest.IDType.OTHER.ToString()) ? objInput.Items.IDNumber : string.Empty;

                        try
                        {
                            XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                            XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(AdminCon, int.Parse(oTicket.Name));

                            XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
                            XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(AdminCon, oSystemUser.SubscriberID);

                            XDSPortalEnquiry.Business.CustomVetting dtCustomVetting = new XDSPortalEnquiry.Business.CustomVetting();

                            //   File.AppendAllText(@"C:\Log\Response.txt", "EnterSubmitConsumerTrace");
                            oResponse = dtCustomVetting.SubmitConsumerTraceVettingQ(EnquiryCon, AdminCon, oSubscriber.SubscriberID, oSystemUser.SystemUserID, 137, oSubscriber.SubscriberName, iDnumber, passportNo, objInput.FirstName, objInput.Surname, objInput.BirthDate,
                                objInput.Gender, objInput.MaritalStatus, objInput.AddressLine1, objInput.AddressLine2, objInput.Suburb, objInput.City, objInput.PostalCode, objInput.HomeTelephoneCode, objInput.HomeTelephoneNumber, objInput.WorkTelephoneCode, objInput.WorkTelephoneNumber, objInput.CellNumber, double.Parse(objInput.GrossMonthlyIncome), objInput.Occupation, objInput.Employer, objInput.BankAccountType, objInput.BankName,
                                objInput.HighestQualification, objInput.Branch, objInput.BankAccountNumber, int.Parse(objInput.NumberOfYearsAtEmployer), string.Empty, true, false, string.Empty);
                            // File.AppendAllText(@"C:\Log\Response.txt", "EndSubmitConsumerTrace");
                            // File.AppendAllText(@"C:\Log\Response.txt", oResponse.ResponseData);
                            switch (oResponse.ResponseStatus)
                            {
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single:
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple:

                                    DataSet dsl = new DataSet();
                                    System.IO.StringReader xmlSR = new System.IO.StringReader(oResponse.ResponseData);
                                    dsl.ReadXml(xmlSR);

                                    //  File.AppendAllText(@"C:\Log\Response.txt", "End xml generate");

                                    if (dsl.Tables[0].Rows.Count == 0 || dsl.Tables[0].Rows.Count == 1)
                                    {
                                        foreach (DataRow dr in dsl.Tables[0].Rows)
                                        {
                                            XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplicationResponse oinputResponse = new XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplicationResponse();
                                            // oResponse = dtCustomVetting.SubmitMulipleCreditEnquiry(con, AdminCon, int.Parse(dr["EnquiryID"].ToString()), int.Parse(dr["EnquiryResultID"].ToString()), false, string.Empty, Unique_ref, QueryNumber.ToString());
                                            oinputResponse = oSResponse;


                                            // File.AppendAllText(@"C:\Log\Response.txt", "EnterSubmitCustomVettingO");
                                            oResponse = dtCustomVetting.SubmitCustomVettingQA(EnquiryCon, AdminCon, int.Parse(dr["EnquiryID"].ToString()), int.Parse(dr["EnquiryResultID"].ToString()), false, string.Empty, oinputResponse, oAPP, out oSResponse, 0, objInput.IsExistingClient);
                                            //File.AppendAllText(@"C:\Log\Response.txt", "EndSubmitCustomVettingO");
                                        }
                                        switch (oResponse.ResponseStatus)
                                        {
                                            case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                                                oSResponse.MTNOutput.Status = "Failure";
                                                oSResponse.MTNOutput.ErrorDescription = oResponse.ResponseData;
                                                break;
                                            case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                                                oSResponse.MTNOutput.Status = "Success";
                                                //rsXml.LoadXml(rp.ResponseData);
                                                break;
                                        }
                                    }
                                    else
                                    {
                                        oSResponse.MTNOutput.Status = "Failure";
                                        oSResponse.MTNOutput.ErrorDescription = "Multiple Match Found";

                                    }
                                    break;

                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                                    oSResponse.MTNOutput.Status = "Failure";
                                    oSResponse.MTNOutput.ErrorDescription = oResponse.ResponseData;
                                    break;
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                                    oSResponse.MTNOutput.Status = "Failure";
                                    oSResponse.MTNOutput.ErrorDescription = "No Record Found";
                                    break;
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                                    oSResponse.MTNOutput.Status = "Success";
                                    oSResponse.MTNOutput.ErrorDescription = oResponse.ResponseData;
                                    break;
                            }


                        }
                        catch (Exception e)
                        {
                            oSResponse.MTNOutput.Status = "Failure";
                            oSResponse.MTNOutput.ErrorDescription = e.Message;
                        }

                    }
                    else
                    {
                        oSResponse.MTNOutput.Status = "Failure";
                        oSResponse.MTNOutput.ErrorDescription = ConnectTicket;
                    }
                }

            }
            catch (Exception ex)
            {
                oSResponse.MTNOutput.Status = "Failure";
                oSResponse.MTNOutput.ErrorDescription = ex.Message;
            }

            DateTime dtenddate = DateTime.Now;
            double diff = dtenddate.Subtract(dtStartdate).TotalSeconds;

            oSResponse.MTNOutput.BureauResponse.ProcessingTimeSecs = Math.Round(float.Parse(diff.ToString()), 2);

            if (oSResponse.MTNOutput.Status == "Failure")
            {
                oSResponse.MTNOutput.Outcome = "Decline";
                oSResponse.MTNOutput.BureauResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.MTNHResponse.BureauResponse.enumresponseStatus.FAILURE;
                oSResponse.MTNOutput.Reason = "Decline";
            }
            else if (oSResponse.MTNOutput.Status == "Success")
            {
                oSResponse.MTNOutput.Outcome = "Approve";
                oSResponse.MTNOutput.BureauResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.MTNHResponse.BureauResponse.enumresponseStatus.SUCCESS;
                oSResponse.MTNOutput.Reason = "Approve";
            }

            oSResponse.MTNOutput.BureauResponse.UniqueRefGuid = (Guid.NewGuid()).ToString();
            //oSResponse.SubmitResult = oSubmitResponse;
            string response = string.Empty;
            try
            {
                //  File.AppendAllText(@"C:\Log\Response.txt", "Enterdeserialization");
                XmlSerializer serializerout = new XmlSerializer(oSResponse.GetType());
                System.IO.StringWriter swout = new System.IO.StringWriter();
                serializerout.Serialize(swout, oSResponse);
                System.IO.StringReader readerout = new System.IO.StringReader(swout.ToString());

                response = readerout.ReadToEnd();

                SqlConnection con = new SqlConnection(Properties.Settings.Default.EnquiryConnectionString);

                SqlCommand cmd = new SqlCommand("Insert into CustomVettingQResponse (CustomVettingOinputID,Response,CreatedOnDate) values(@ID, @inputXMLstring,getdate())", con);
                cmd.Parameters.AddWithValue("@ID", intCustomVettingOinputID);
                cmd.Parameters.AddWithValue("@inputXMLstring", response);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                con.Close();
            }

            catch (Exception ex)
            {
                // File.AppendAllText(@"C:\Log\Response.txt", "EnterException");
                SqlConnection con = new SqlConnection(Properties.Settings.Default.EnquiryConnectionString);

                SqlCommand cmd = new SqlCommand("Insert into CustomVettingQResponse (CustomVettingOinputID,Response,CreatedOnDate) values(@ID, @inputXMLstring,getdate())", con);
                cmd.Parameters.AddWithValue("@ID", intCustomVettingOinputID);
                cmd.Parameters.AddWithValue("@inputXMLstring", ex.Message);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                con.Close();

            }

            return oSResponse.MTNOutput;

        }


        [WebMethod(Description = "")]
        // Method for MTN
        public string ConnectCustomVettingQA(int ProductIndicator, string Company, string RuleSet, string Branch, string User, string Password, string IsExistingClient, string GrossMonthlyIncome, string Surname, string FirstName, string IDType, string BirthDate, string IdentityNumber, string Gender, string MaritalStatus, string AddressLine1, string AddressLine2, string Suburb, string City, string PostalCode, string HomeTelephoneCode, string HomeTelephoneNumber, string WorkTelephoneCode, string WorkTelephoneNumber, string CellNumber, string Occupation, string Employer, string NumberOfYearsAtEmployer, string BankName, string BankAccountType, string BankBranchCode, string BankAccountNumber, string HighestQualification)
        {
            Company = string.IsNullOrEmpty(Company) ? string.Empty : Company;
            RuleSet = string.IsNullOrEmpty(RuleSet) ? string.Empty : RuleSet;
            Branch = string.IsNullOrEmpty(Branch) ? string.Empty : Branch;
            User = string.IsNullOrEmpty(User) ? string.Empty : User;
            Password = string.IsNullOrEmpty(Password) ? string.Empty : Password;
            IsExistingClient = string.IsNullOrEmpty(IsExistingClient) ? string.Empty : IsExistingClient;
            GrossMonthlyIncome = string.IsNullOrEmpty(GrossMonthlyIncome) ? string.Empty : GrossMonthlyIncome;
            Surname = string.IsNullOrEmpty(Surname) ? string.Empty : Surname;
            FirstName = string.IsNullOrEmpty(FirstName) ? string.Empty : FirstName;
            IDType = string.IsNullOrEmpty(IDType) ? string.Empty : IDType;
            BirthDate = string.IsNullOrEmpty(BirthDate) ? string.Empty : BirthDate;
            IdentityNumber = string.IsNullOrEmpty(IdentityNumber) ? string.Empty : IdentityNumber;
            Gender = string.IsNullOrEmpty(Gender) ? string.Empty : Gender;
            MaritalStatus = string.IsNullOrEmpty(MaritalStatus) ? string.Empty : MaritalStatus;
            AddressLine1 = string.IsNullOrEmpty(AddressLine1) ? string.Empty : AddressLine1;
            AddressLine2 = string.IsNullOrEmpty(AddressLine2) ? string.Empty : AddressLine2;
            Suburb = string.IsNullOrEmpty(Suburb) ? string.Empty : Suburb;
            City = string.IsNullOrEmpty(City) ? string.Empty : City;
            PostalCode = string.IsNullOrEmpty(PostalCode) ? string.Empty : PostalCode;
            HomeTelephoneCode = string.IsNullOrEmpty(HomeTelephoneCode) ? string.Empty : HomeTelephoneCode;
            HomeTelephoneNumber = string.IsNullOrEmpty(HomeTelephoneNumber) ? string.Empty : HomeTelephoneNumber;
            WorkTelephoneCode = string.IsNullOrEmpty(WorkTelephoneCode) ? string.Empty : WorkTelephoneCode;
            WorkTelephoneNumber = string.IsNullOrEmpty(WorkTelephoneNumber) ? string.Empty : WorkTelephoneNumber;
            CellNumber = string.IsNullOrEmpty(CellNumber) ? string.Empty : CellNumber;
            Occupation = string.IsNullOrEmpty(Occupation) ? string.Empty : Occupation;
            Employer = string.IsNullOrEmpty(Employer) ? string.Empty : Employer;
            NumberOfYearsAtEmployer = string.IsNullOrEmpty(NumberOfYearsAtEmployer) ? "0" : NumberOfYearsAtEmployer;
            BankName = string.IsNullOrEmpty(BankName) ? string.Empty : BankName;
            BankAccountType = string.IsNullOrEmpty(BankAccountType) ? string.Empty : BankAccountType;
            BankBranchCode = string.IsNullOrEmpty(BankBranchCode) ? string.Empty : BankBranchCode;
            BankAccountNumber = string.IsNullOrEmpty(BankAccountNumber) ? string.Empty : BankAccountNumber;
            HighestQualification = string.IsNullOrEmpty(HighestQualification) ? string.Empty : HighestQualification;

            // File.AppendAllText(@"C:\Log\JDVet.txt", "Step 1 \n");

            DateTime dtStartdate = DateTime.Now;

            //  File.AppendAllText(@"C:\Log\JDVet.txt", "Step 2 \n");
            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();
            string rXml = string.Empty, ConnectTicket = string.Empty;

            //  File.AppendAllText(@"C:\Log\JDVet.txt", "Step 3 \n");

            XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplicationResponse oSResponse = new XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplicationResponse();
            oSResponse.MTNOutput.BureauResponse = new XDSPortalLibrary.Entity_Layer.MTNHResponse.BureauResponse();

            // File.AppendAllText(@"C:\Log\JDVet.txt", "Step 4 \n");

            XmlSerializer serializer = new XmlSerializer(typeof(XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplication));
            XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplication objInput = new XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplication();

            // File.AppendAllText(@"C:\Log\JDVet.txt", "Step 5 \n");

            int intCustomVettingOinputID = 0;

            // File.AppendAllText(@"C:\Log\JDVet.txt", "Step 6 \n");

            try
            {

                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step 7 \n");
                SqlConnection con = new SqlConnection(EnquiryCon.ConnectionString);

                SqlCommand cmd = new SqlCommand("Insert into CustomVettingQinput (Input,CreatedOnDate) values(@inputXMLstring,getdate())", con);

                cmd.Parameters.AddWithValue("@inputXMLstring", ("productIndicator:" + ProductIndicator.ToString() + "Company:" + Company + "RuleSet:" + RuleSet + "Branch:" + Branch + "User:" + User + "Password:" + Password + "IsExistingClient:" + IsExistingClient + "GrossMonthlyIncome:" + GrossMonthlyIncome + "Surname:" + Surname + "FirstName:" + FirstName + "IDType:" + IDType + "BirthDate:" + BirthDate + "IdentityNumber:" + IdentityNumber + "Gender:" + Gender + "MaritalStatus:" + MaritalStatus + "AddressLine1:" + AddressLine1 + "AddressLine2:" + AddressLine2 + "Suburb:" + Suburb + "City:" + City + "PostalCode:" + PostalCode + "HomeTelephoneCode:" + HomeTelephoneCode + "HomeTelephoneNumber:" + HomeTelephoneNumber + "WorkTelephoneCode:" + WorkTelephoneCode + "WorkTelephoneNumber:" + WorkTelephoneNumber + "CellNumber:" + CellNumber + "Occupation:" + Occupation + "Employer:" + Employer + "NumberOfYearsAtEmployer:" + NumberOfYearsAtEmployer + "BankName:" + BankName + "BankAccountType:" + BankAccountType + "BankBranchCode:" + BankBranchCode + "BankAccountNumber:" + BankAccountNumber + "HighestQualification:" + HighestQualification));
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                string strSQL = "SELECT @@IDENTITY";
                SqlCommand Imycommand = new SqlCommand(strSQL, con);
                SqlDataReader Idr = Imycommand.ExecuteReader();
                Idr.Read();
                intCustomVettingOinputID = Convert.ToInt32(Idr.GetValue(0));
                Idr.Close();

                con.Close();

                //parse XML

                //xml = xml.Replace("&", string.Empty);
               // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx \n");
                objInput.input = new XDSPortalLibrary.Entity_Layer.MTNHResponse.MTNInput();
                objInput.input.AddressLine1 = AddressLine1;
               // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 1\n");
                objInput.input.AddressLine2 = AddressLine2;
               // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 2\n");
                objInput.input.BankAccountNumber = BankAccountNumber;
               // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 3\n");
                objInput.input.BankAccountType = BankAccountType;
               // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 4\n");
                objInput.input.BankBranchCode = BankBranchCode;
               // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 5\n");
                objInput.input.BankName = BankName;
               // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 6\n");
                objInput.input.BirthDate = BirthDate;
               // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 7\n");
                objInput.input.Branch = Branch;
               // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 8\n");
                objInput.input.CellNumber = CellNumber;
               // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 9\n");
                objInput.input.City = City;
               // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 10\n");
                objInput.input.Company = Company;
               // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 11\n");
                objInput.input.Employer = Employer;
               // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 12\n");
                objInput.input.FirstName = FirstName;
               // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 13\n");
                objInput.input.Gender = Gender;
               // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 14\n");
                objInput.input.GrossMonthlyIncome = GrossMonthlyIncome;
               // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 15\n");
                objInput.input.HighestQualification = HighestQualification;
               // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 16\n");
                objInput.input.HomeTelephoneCode = HomeTelephoneCode;
               // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 17\n");
                objInput.input.HomeTelephoneNumber = HomeTelephoneNumber;
               // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 18\n");
                objInput.input.IdentityNumber = IdentityNumber;
               // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 19\n");
                objInput.input.IDType = IDType;
               // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 20\n");
                objInput.input.IsExistingClient = IsExistingClient;
               // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 21\n");
                objInput.input.MaritalStatus = MaritalStatus;
               // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 22\n");
                objInput.input.NumberOfYearsAtEmployer = NumberOfYearsAtEmployer;
               // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 23\n");
                objInput.input.Occupation = Occupation;
               // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 24\n");
                objInput.input.Password = Password;
               // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 25\n");
                objInput.input.PostalCode = PostalCode;
               // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 26\n");
                objInput.input.RuleSet = RuleSet;
               // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 27\n");
                objInput.input.Suburb = Suburb;
               // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 28\n");
                objInput.input.Surname = Surname;
               // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 29\n");
                objInput.input.User = User;
               // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 30\n");
                objInput.input.WorkTelephoneCode = WorkTelephoneCode;
               // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 31\n");
                objInput.input.WorkTelephoneNumber = WorkTelephoneNumber;
               // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 32\n");

               // File.AppendAllText(@"C:\Log\JDVet.txt", "Step yyy \n");

                // oSResponse.SubmitResult.ClientReference = objInput.input.ClientReference;

                if (objInput.input.IDType.ToUpper() == XDSPortalLibrary.Entity_Layer.JDFSPrincipaRequest.IDType.OTHER.ToString())
                {
                    objInput.input.IDType = XDSPortalLibrary.Entity_Layer.JDFSPrincipaRequest.IDType.PASSPORT.ToString();
                }
                // Validate input

                XDSPortalLibrary.Entity_Layer.Response oResponse = new XDSPortalLibrary.Entity_Layer.Response();
                CustomVetting oCustomvetting = new CustomVetting();
               // File.AppendAllText(@"C:\Log\JDVet.txt", "Step ValidateConnectCustomVettingQ \n");
                oResponse = oCustomvetting.ValidateConnectCustomVettingQSOA(objInput);
               // File.AppendAllText(@"C:\Log\JDVet.txt", "Step ValidateConnectCustomVettingQ \n");
               // File.AppendAllText(@"C:\Log\Response.txt", oResponse.ResponseStatus.ToString());
               // File.AppendAllText(@"C:\Log\Response.txt", oResponse.ResponseData.ToString());
                if (oResponse.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                {
                    oSResponse.MTNOutput.Status = "Failure";
                    oSResponse.MTNOutput.ErrorDescription = oResponse.ResponseData;

                }
                else if (oResponse.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                {
                    // Login

                    ConnectTicket = Login(objInput.input.User, objInput.input.Password);


                    if (IsTicketValid(ConnectTicket))
                    {
                        FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);

                        string iDnumber = string.Empty, passportNo = string.Empty;

                        iDnumber = (objInput.input.IDType.ToUpper() == "SAID") ? objInput.input.IdentityNumber : string.Empty;
                        passportNo = (objInput.input.IDType.ToUpper() == "PASSPORT") ? objInput.input.IdentityNumber : string.Empty;
                        //passportNo = (objInput.Items.IDType.ToUpper() == XDSPortalLibrary.Entity_Layer.JDFSPrincipaRequest.IDType.OTHER.ToString()) ? objInput.Items.IDNumber : string.Empty;

                        try
                        {
                            XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                            XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(AdminCon, int.Parse(oTicket.Name));

                            XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
                            XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(AdminCon, oSystemUser.SubscriberID);

                            XDSPortalEnquiry.Business.CustomVetting dtCustomVetting = new XDSPortalEnquiry.Business.CustomVetting();

                              // File.AppendAllText(@"C:\Log\Response.txt", "EnterSubmitConsumerTrace");
                            oResponse = dtCustomVetting.SubmitConsumerTraceVettingQ(EnquiryCon, AdminCon, oSubscriber.SubscriberID, oSystemUser.SystemUserID, 137, oSubscriber.SubscriberName, iDnumber, passportNo, objInput.input.FirstName, objInput.input.Surname, objInput.input.BirthDate,
                                objInput.input.Gender, objInput.input.MaritalStatus, objInput.input.AddressLine1, objInput.input.AddressLine2, objInput.input.Suburb, objInput.input.City, objInput.input.PostalCode, objInput.input.HomeTelephoneCode, objInput.input.HomeTelephoneNumber, objInput.input.WorkTelephoneCode, objInput.input.WorkTelephoneNumber, objInput.input.CellNumber, double.Parse(objInput.input.GrossMonthlyIncome), objInput.input.Occupation, objInput.input.Employer, objInput.input.BankAccountType, objInput.input.BankName,
                                objInput.input.HighestQualification, objInput.input.Branch, objInput.input.BankAccountNumber, int.Parse(objInput.input.NumberOfYearsAtEmployer), string.Empty, true, false, string.Empty);
                            // File.AppendAllText(@"C:\Log\Response.txt", "EndSubmitConsumerTrace");
                            // File.AppendAllText(@"C:\Log\Response.txt", oResponse.ResponseData);
                            switch (oResponse.ResponseStatus)
                            {
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single:
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple:

                                    DataSet dsl = new DataSet();
                                    System.IO.StringReader xmlSR = new System.IO.StringReader(oResponse.ResponseData);
                                    dsl.ReadXml(xmlSR);

                                     // File.AppendAllText(@"C:\Log\Response.txt", "End xml generate");

                                    if (dsl.Tables[0].Rows.Count == 0 || dsl.Tables[0].Rows.Count == 1)
                                    {
                                        foreach (DataRow dr in dsl.Tables[0].Rows)
                                        {
                                            XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplicationResponse oinputResponse = new XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplicationResponse();
                                            // oResponse = dtCustomVetting.SubmitMulipleCreditEnquiry(con, AdminCon, int.Parse(dr["EnquiryID"].ToString()), int.Parse(dr["EnquiryResultID"].ToString()), false, string.Empty, Unique_ref, QueryNumber.ToString());
                                            oinputResponse = oSResponse;


                                            //   File.AppendAllText(@"C:\Log\Response.txt", "EnterSubmitCustomVettingO");
                                            oResponse = dtCustomVetting.SubmitCustomVettingQA(EnquiryCon, AdminCon, int.Parse(dr["EnquiryID"].ToString()), int.Parse(dr["EnquiryResultID"].ToString()), false, string.Empty, oinputResponse, objInput, out oSResponse, ProductIndicator, IsExistingClient);
                                            //  File.AppendAllText(@"C:\Log\Response.txt", "EndSubmitCustomVettingO");
                                        }
                                        switch (oResponse.ResponseStatus)
                                        {
                                            case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                                                oSResponse.MTNOutput.Status = "Failure";
                                                oSResponse.MTNOutput.ErrorDescription = oResponse.ResponseData;
                                                break;
                                            case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                                                //rsXml.LoadXml(rp.ResponseData);
                                                break;
                                        }
                                    }
                                    else
                                    {
                                        oSResponse.MTNOutput.Status = "Failure";
                                        oSResponse.MTNOutput.ErrorDescription = "Multiple Match Found";

                                    }
                                    break;

                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                                    oSResponse.MTNOutput.Status = "Failure";
                                    oSResponse.MTNOutput.ErrorDescription = oResponse.ResponseData;
                                    break;
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                                    oSResponse.MTNOutput.Status = "Failure";
                                    oSResponse.MTNOutput.ErrorDescription = "No Record Found";
                                    break;
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                                    oSResponse.MTNOutput.Status = "Success";
                                    oSResponse.MTNOutput.ErrorDescription = oResponse.ResponseData;
                                    break;
                            }


                        }
                        catch (Exception e)
                        {
                            oSResponse.MTNOutput.Status = "Failure";
                            oSResponse.MTNOutput.ErrorDescription = e.Message;
                        }

                    }
                    else
                    {
                        oSResponse.MTNOutput.Status = "Failure";
                        oSResponse.MTNOutput.ErrorDescription = ConnectTicket;
                    }
                }

            }
            catch (Exception ex)
            {
                oSResponse.MTNOutput.Status = "Failure";
                oSResponse.MTNOutput.ErrorDescription = ex.Message;
            }

            DateTime dtenddate = DateTime.Now;
            double diff = dtenddate.Subtract(dtStartdate).TotalSeconds;

            oSResponse.MTNOutput.BureauResponse.ProcessingTimeSecs = Math.Round(float.Parse(diff.ToString()), 2);

            if (oSResponse.MTNOutput.Status == "Failure")
            {
                oSResponse.MTNOutput.Outcome = "Decline";
                oSResponse.MTNOutput.BureauResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.MTNHResponse.BureauResponse.enumresponseStatus.FAILURE;
                oSResponse.MTNOutput.Reason = "Decline";
            }
            else if (oSResponse.MTNOutput.Status == "Success")
            {
                oSResponse.MTNOutput.Outcome = "Approve";
                oSResponse.MTNOutput.BureauResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.MTNHResponse.BureauResponse.enumresponseStatus.SUCCESS;
                oSResponse.MTNOutput.Reason = "Approve";
            }

            oSResponse.MTNOutput.BureauResponse.UniqueRefGuid = (Guid.NewGuid()).ToString();
            //oSResponse.SubmitResult = oSubmitResponse;
            string response = string.Empty;
            try
            {
                //  File.AppendAllText(@"C:\Log\Response.txt", "Enterdeserialization");
                XmlSerializer serializerout = new XmlSerializer(oSResponse.GetType());
                System.IO.StringWriter swout = new System.IO.StringWriter();
                serializerout.Serialize(swout, oSResponse);
                System.IO.StringReader readerout = new System.IO.StringReader(swout.ToString());

                response = readerout.ReadToEnd();

                SqlConnection con = new SqlConnection(Properties.Settings.Default.EnquiryConnectionString);

                SqlCommand cmd = new SqlCommand("Insert into CustomVettingQResponse (CustomVettingOinputID,Response,CreatedOnDate) values(@ID, @inputXMLstring,getdate())", con);
                cmd.Parameters.AddWithValue("@ID", intCustomVettingOinputID);
                cmd.Parameters.AddWithValue("@inputXMLstring", response);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                con.Close();
            }

            catch (Exception ex)
            {
                // File.AppendAllText(@"C:\Log\Response.txt", "EnterException");
                SqlConnection con = new SqlConnection(Properties.Settings.Default.EnquiryConnectionString);

                SqlCommand cmd = new SqlCommand("Insert into CustomVettingQResponse (CustomVettingOinputID,Response,CreatedOnDate) values(@ID, @inputXMLstring,getdate())", con);
                cmd.Parameters.AddWithValue("@ID", intCustomVettingOinputID);
                cmd.Parameters.AddWithValue("@inputXMLstring", ex.Message);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                con.Close();

            }

            return response;

        }

        [WebMethod(Description = "")]
        // Method for MTN
        public XDSPortalLibrary.Entity_Layer.MTNHResponse.MTNOutput SubmitApplicationTeleSales(XDSPortalLibrary.Entity_Layer.MTNHResponse.MTNInput MTNInput)
        {

            // File.AppendAllText(@"C:\Log\JDVet.txt", "Step 1 \n");

            DateTime dtStartdate = DateTime.Now;

            //  File.AppendAllText(@"C:\Log\JDVet.txt", "Step 2 \n");
            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();
            string rXml = string.Empty, ConnectTicket = string.Empty;

            //  File.AppendAllText(@"C:\Log\JDVet.txt", "Step 3 \n");

            XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplicationResponse oSResponse = new XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplicationResponse();
            oSResponse.MTNOutput.BureauResponse = new XDSPortalLibrary.Entity_Layer.MTNHResponse.BureauResponse();

            // File.AppendAllText(@"C:\Log\JDVet.txt", "Step 4 \n");

            XmlSerializer serializer = new XmlSerializer(typeof(XDSPortalLibrary.Entity_Layer.MTNHResponse.MTNInput));
            XDSPortalLibrary.Entity_Layer.MTNHResponse.MTNInput objInput = new XDSPortalLibrary.Entity_Layer.MTNHResponse.MTNInput();

            // File.AppendAllText(@"C:\Log\JDVet.txt", "Step 5 \n");

            int intCustomVettingOinputID = 0;

            // File.AppendAllText(@"C:\Log\JDVet.txt", "Step 6 \n");

            try
            {

                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step 7 \n");
                SqlConnection con = new SqlConnection(EnquiryCon.ConnectionString);
                SqlConnection MTNCon = new SqlConnection(MTNEnquiryCon.ConnectionString);

                XmlSerializer serializerinput = new XmlSerializer(MTNInput.GetType());
                System.IO.StringWriter swinput = new System.IO.StringWriter();
                serializerinput.Serialize(swinput, MTNInput);
                System.IO.StringReader readerin = new System.IO.StringReader(swinput.ToString());

                string strinput = readerin.ReadToEnd();


                SqlCommand cmd = new SqlCommand("Insert into CustomVettingQinput (Input,CreatedOnDate) values(@inputXMLstring,getdate())", con);

                //cmd.Parameters.AddWithValue("@inputXMLstring", ("productIndicator:" + ProductIndicator.ToString() + "Company:" + Company + "RuleSet:" + RuleSet + "Branch:" + Branch + "User:" + User + "Password:" + Password + "IsExistingClient:" + IsExistingClient + "GrossMonthlyIncome:" + GrossMonthlyIncome + "Surname:" + Surname + "FirstName:" + FirstName + "IDType:" + IDType + "BirthDate:" + BirthDate + "IdentityNumber:" + IdentityNumber + "Gender:" + Gender + "MaritalStatus:" + MaritalStatus + "AddressLine1:" + AddressLine1 + "AddressLine2:" + AddressLine2 + "Suburb:" + Suburb + "City:" + City + "PostalCode:" + PostalCode + "HomeTelephoneCode:" + HomeTelephoneCode + "HomeTelephoneNumber:" + HomeTelephoneNumber + "WorkTelephoneCode:" + WorkTelephoneCode + "WorkTelephoneNumber:" + WorkTelephoneNumber + "CellNumber:" + CellNumber + "Occupation:" + Occupation + "Employer:" + Employer + "NumberOfYearsAtEmployer:" + NumberOfYearsAtEmployer + "BankName:" + BankName + "BankAccountType:" + BankAccountType + "BankBranchCode:" + BankBranchCode + "BankAccountNumber:" + BankAccountNumber + "HighestQualification:" + HighestQualification));
                cmd.Parameters.AddWithValue("@inputXMLstring", strinput);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                string strSQL = "SELECT @@IDENTITY";
                SqlCommand Imycommand = new SqlCommand(strSQL, con);
                SqlDataReader Idr = Imycommand.ExecuteReader();
                Idr.Read();
                intCustomVettingOinputID = Convert.ToInt32(Idr.GetValue(0));
                Idr.Close();

                con.Close();

                string strQuery = "Insert into MTNCustomVettingInput (CustomVettingOinputID,MonthlyIncome,Surname,FirstName,IDType,SAIDBirthDate,IdentityNumber,Gender,MaritalStatus,AddressLine1,";
                strQuery += "AddressLine2,Suburb,City,PostalCode,HomeTelephoneCode,HomeTelephoneNumber,WorkTelephoneCode,WorkTelephoneNumber,CellNumber,Occupation,Employer,";
                strQuery += "NumberOfYearsAtEmployer,BankName,BankAccountType,BankBranchCode,BankAccountNumber,HighestQualification,StatusInd,EnquiryDate, DealerCode, CustomerName, Channel) ";
                strQuery += "values('" + intCustomVettingOinputID.ToString() + "','" + MTNInput.GrossMonthlyIncome + "','" + MTNInput.Surname + "','" + MTNInput.FirstName + "','" + MTNInput.IDType + "','";
                strQuery += MTNInput.BirthDate + "','" + MTNInput.IdentityNumber + "','" + MTNInput.Gender + "','" + MTNInput.MaritalStatus + "','" + MTNInput.AddressLine1 + "','";
                strQuery += MTNInput.AddressLine2 + "','" + MTNInput.Suburb + "','" + MTNInput.City + "','" + MTNInput.PostalCode + "','" + MTNInput.HomeTelephoneCode + "','";
                strQuery += MTNInput.HomeTelephoneNumber + "','" + MTNInput.WorkTelephoneCode + "','" + MTNInput.WorkTelephoneNumber + "','" + MTNInput.CellNumber + "','";
                strQuery += MTNInput.Occupation + "','" + MTNInput.Employer + "','" + MTNInput.NumberOfYearsAtEmployer + "','" + MTNInput.BankName + "','";
                strQuery += MTNInput.BankAccountType + "','" + MTNInput.BankBranchCode + "','" + MTNInput.BankAccountNumber + "','" + MTNInput.HighestQualification + "',";
                strQuery += "1, getdate(), '" + MTNInput.DealerCode + "', '" + MTNInput.CustomerName + "', '" + MTNInput.Channel + "')";

                cmd = new SqlCommand(strQuery, MTNCon);

                if (MTNCon.State == ConnectionState.Closed)
                    MTNCon.Open();

                cmd.ExecuteNonQuery();

                MTNCon.Close();


                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step yyy \n");
                objInput = MTNInput;// (XDSPortalLibrary.Entity_Layer.MTNInput)serializer.Deserialize(new XmlTextReader(new StringReader(xml)));

                XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplication oAPP = new XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplication();
                oAPP.input = objInput;
                // oSResponse.SubmitResult.ClientReference = objInput.input.ClientReference;

                //if (objInput.IDType.ToUpper() == XDSPortalLibrary.Entity_Layer.JDFSPrincipaRequest.IDType.OTHER.ToString())
                //{
                //    objInput.IDType = XDSPortalLibrary.Entity_Layer.JDFSPrincipaRequest.IDType.PASSPORT.ToString();
                //}
                // Validate input

                XDSPortalLibrary.Entity_Layer.Response oResponse = new XDSPortalLibrary.Entity_Layer.Response();
                CustomVetting oCustomvetting = new CustomVetting();
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step ValidateConnectCustomVettingQ \n");
                oResponse = oCustomvetting.ValidateConnectCustomVettingQTeleSales(oAPP);
                //  File.AppendAllText(@"C:\Log\JDVet.txt", "Step ValidateConnectCustomVettingQ \n");

                if (oResponse.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                {
                    oSResponse.MTNOutput.Status = "Failure";
                    oSResponse.MTNOutput.ErrorDescription = oResponse.ResponseData;

                }
                else if (oResponse.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                {
                    // Login

                    ConnectTicket = Login(objInput.User, objInput.Password);


                    if (IsTicketValid(ConnectTicket))
                    {
                        FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);

                        string iDnumber = string.Empty, passportNo = string.Empty;

                        iDnumber = (objInput.IDType.ToUpper() == "1") ? objInput.IdentityNumber : string.Empty;
                        passportNo = (objInput.IDType.ToUpper() == "2") ? objInput.IdentityNumber : string.Empty;

                        objInput.NumberOfYearsAtEmployer = string.IsNullOrEmpty(objInput.NumberOfYearsAtEmployer) ? "0" : objInput.NumberOfYearsAtEmployer;
                        //passportNo = (objInput.Items.IDType.ToUpper() == XDSPortalLibrary.Entity_Layer.JDFSPrincipaRequest.IDType.OTHER.ToString()) ? objInput.Items.IDNumber : string.Empty;

                        try
                        {
                            XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                            XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(AdminCon, int.Parse(oTicket.Name));

                            XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
                            XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(AdminCon, oSystemUser.SubscriberID);

                            XDSPortalEnquiry.Business.CustomVetting dtCustomVetting = new XDSPortalEnquiry.Business.CustomVetting();

                            //   File.AppendAllText(@"C:\Log\Response.txt", "EnterSubmitConsumerTrace");
                            oResponse = dtCustomVetting.SubmitConsumerTraceVettingQ(EnquiryCon, AdminCon, oSubscriber.SubscriberID, oSystemUser.SystemUserID, 137, oSubscriber.SubscriberName, iDnumber, passportNo, objInput.FirstName, objInput.Surname, objInput.BirthDate,
                                objInput.Gender, objInput.MaritalStatus, objInput.AddressLine1, objInput.AddressLine2, objInput.Suburb, objInput.City, objInput.PostalCode, objInput.HomeTelephoneCode, objInput.HomeTelephoneNumber, objInput.WorkTelephoneCode, objInput.WorkTelephoneNumber, objInput.CellNumber, double.Parse(objInput.GrossMonthlyIncome), objInput.Occupation, objInput.Employer, objInput.BankAccountType, objInput.BankName,
                                objInput.HighestQualification, objInput.Branch, objInput.BankAccountNumber, int.Parse(objInput.NumberOfYearsAtEmployer), string.Empty, true, false, string.Empty);
                            // File.AppendAllText(@"C:\Log\Response.txt", "EndSubmitConsumerTrace");
                            // File.AppendAllText(@"C:\Log\Response.txt", oResponse.ResponseData);
                            switch (oResponse.ResponseStatus)
                            {
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single:
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple:

                                    DataSet dsl = new DataSet();
                                    System.IO.StringReader xmlSR = new System.IO.StringReader(oResponse.ResponseData);
                                    dsl.ReadXml(xmlSR);

                                    //  File.AppendAllText(@"C:\Log\Response.txt", "End xml generate");

                                    if (dsl.Tables[0].Rows.Count == 0 || dsl.Tables[0].Rows.Count == 1)
                                    {
                                        foreach (DataRow dr in dsl.Tables[0].Rows)
                                        {
                                            XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplicationResponse oinputResponse = new XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplicationResponse();
                                            // oResponse = dtCustomVetting.SubmitMulipleCreditEnquiry(con, AdminCon, int.Parse(dr["EnquiryID"].ToString()), int.Parse(dr["EnquiryResultID"].ToString()), false, string.Empty, Unique_ref, QueryNumber.ToString());
                                            oinputResponse = oSResponse;


                                            //   File.AppendAllText(@"C:\Log\Response.txt", "EnterSubmitCustomVettingO");
                                            oResponse = dtCustomVetting.SubmitCustomVettingQTeleSales(EnquiryCon, AdminCon, int.Parse(dr["EnquiryID"].ToString()), int.Parse(dr["EnquiryResultID"].ToString()), false, string.Empty, oinputResponse, oAPP, out oSResponse, 0, objInput.IsExistingClient);
                                            //  File.AppendAllText(@"C:\Log\Response.txt", "EndSubmitCustomVettingO");
                                        }
                                        switch (oResponse.ResponseStatus)
                                        {
                                            case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                                                oSResponse.MTNOutput.Status = "Failure";
                                                oSResponse.MTNOutput.ErrorDescription = oResponse.ResponseData;
                                                break;
                                            case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                                                oSResponse.MTNOutput.Status = "Success";
                                                //rsXml.LoadXml(rp.ResponseData);
                                                break;
                                        }
                                    }
                                    else
                                    {
                                        oSResponse.MTNOutput.Status = "Failure";
                                        oSResponse.MTNOutput.ErrorDescription = "Multiple Match Found";

                                    }
                                    break;

                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                                    oSResponse.MTNOutput.Status = "Failure";
                                    oSResponse.MTNOutput.ErrorDescription = oResponse.ResponseData;
                                    break;
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                                    oSResponse.MTNOutput.Status = "Failure";
                                    oSResponse.MTNOutput.ErrorDescription = "No Record Found";
                                    break;
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                                    oSResponse.MTNOutput.Status = "Success";
                                    oSResponse.MTNOutput.ErrorDescription = oResponse.ResponseData;
                                    break;
                            }


                        }
                        catch (Exception e)
                        {
                            oSResponse.MTNOutput.Status = "Failure";
                            oSResponse.MTNOutput.ErrorDescription = e.Message;
                        }

                    }
                    else
                    {
                        oSResponse.MTNOutput.Status = "Failure";
                        oSResponse.MTNOutput.ErrorDescription = ConnectTicket;
                    }
                }

            }
            catch (Exception ex)
            {
                oSResponse.MTNOutput.Status = "Failure";
                oSResponse.MTNOutput.ErrorDescription = ex.Message;
            }

            DateTime dtenddate = DateTime.Now;
            double diff = dtenddate.Subtract(dtStartdate).TotalSeconds;

            oSResponse.MTNOutput.BureauResponse.ProcessingTimeSecs = Math.Round(float.Parse(diff.ToString()), 2);

            if (oSResponse.MTNOutput.Status == "Failure")
            {
                oSResponse.MTNOutput.Outcome = "Decline";
                oSResponse.MTNOutput.BureauResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.MTNHResponse.BureauResponse.enumresponseStatus.FAILURE;
                oSResponse.MTNOutput.Reason = "Decline";
            }
            else if (oSResponse.MTNOutput.Status == "Success")
            {
                oSResponse.MTNOutput.Outcome = "Approve";
                oSResponse.MTNOutput.BureauResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.MTNHResponse.BureauResponse.enumresponseStatus.SUCCESS;
                oSResponse.MTNOutput.Reason = "Approve";
            }

            oSResponse.MTNOutput.BureauResponse.UniqueRefGuid = (Guid.NewGuid()).ToString();
            //oSResponse.SubmitResult = oSubmitResponse;
            string response = string.Empty;
            try
            {
                //  File.AppendAllText(@"C:\Log\Response.txt", "Enterdeserialization");
                XmlSerializer serializerout = new XmlSerializer(oSResponse.GetType());
                System.IO.StringWriter swout = new System.IO.StringWriter();
                serializerout.Serialize(swout, oSResponse);
                System.IO.StringReader readerout = new System.IO.StringReader(swout.ToString());

                response = readerout.ReadToEnd();

                SqlConnection con = new SqlConnection(Properties.Settings.Default.EnquiryConnectionString);

                SqlCommand cmd = new SqlCommand("Insert into CustomVettingQResponse (CustomVettingOinputID,Response,CreatedOnDate) values(@ID, @inputXMLstring,getdate())", con);
                cmd.Parameters.AddWithValue("@ID", intCustomVettingOinputID);
                cmd.Parameters.AddWithValue("@inputXMLstring", response);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                con.Close();
            }

            catch (Exception ex)
            {
                // File.AppendAllText(@"C:\Log\Response.txt", "EnterException");
                SqlConnection con = new SqlConnection(Properties.Settings.Default.EnquiryConnectionString);

                SqlCommand cmd = new SqlCommand("Insert into CustomVettingQResponse (CustomVettingOinputID,Response,CreatedOnDate) values(@ID, @inputXMLstring,getdate())", con);
                cmd.Parameters.AddWithValue("@ID", intCustomVettingOinputID);
                cmd.Parameters.AddWithValue("@inputXMLstring", ex.Message);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                con.Close();

            }

            return oSResponse.MTNOutput;

        }
    }
}