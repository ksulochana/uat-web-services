﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Threading;
using System.Security.Principal;
using System.Web.Security;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Data;
using System.Net.Mail;
using System.Configuration;
using System.Net;
using System.Text;
using System.Reflection;
using XDSPortalAuthentication;
using DevExpress.XtraReports.UI;
using XdsPortalReports;
using XDSPortalEnquiry.Business;
using XDSPortalLibrary.Entity_Layer;

namespace XDSConnect
{
    /// <summary>
    /// Summary description for MTNConnect
    /// </summary>
    [WebService(Namespace = "http://www.web.xds.co.za/XDSConnectWS")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class MTNConnect : System.Web.Services.WebService
    {
        private SqlConnection EnquiryCon = new SqlConnection(Properties.Settings.Default.EnquiryConnectionString);
        private SqlConnection AdminCon = new SqlConnection(Properties.Settings.Default.AdminConnectionString);
        private SqlConnection XDSLogCon = new SqlConnection(Properties.Settings.Default.XDSLog);
        private SqlConnection MTNCommercialQuestionnaireCon = new SqlConnection(Properties.Settings.Default.MTNCommercialQuestionnaire);
        private string strSmtpServerName = ConfigurationManager.AppSettings["XDSSmtp"].ToString();
        private string strSmtpUserLoginID = ConfigurationManager.AppSettings["XDSSmtpUserLoginID"].ToString();
        private string strSmtpUserPassword = ConfigurationManager.AppSettings["XDSSmtpUserPassword"].ToString();
        private int intport = Convert.ToInt16(ConfigurationManager.AppSettings["XDSSmtpPort"].ToString());
        private string strVendorID = ConfigurationManager.AppSettings["HansVendorID"].ToString();
        private string strIvsURL = ConfigurationManager.AppSettings["IvsURL"].ToString();
        private string strHanisURL1 = ConfigurationManager.AppSettings["HanisURL1"].ToString();
        private string strHanisURL2 = ConfigurationManager.AppSettings["HanisURL2"].ToString();
        private string strDIAURL = ConfigurationManager.AppSettings["DIAURL"].ToString();
        private string strHanissiteID = ConfigurationManager.AppSettings["HanisSiteID"].ToString();
        private string strHanisWorkStationID = ConfigurationManager.AppSettings["HanisWorkStationID"].ToString();
        private string strHanisType = ConfigurationManager.AppSettings["HanisType"].ToString();
        private string strJ2KURL = ConfigurationManager.AppSettings["J2KURL"].ToString();
        private string strDHAURL = ConfigurationManager.AppSettings["DHAURL"].ToString();
        private string DIABiometricuRL = ConfigurationManager.AppSettings["DIABiometricURL"].ToString();
        private string DIABiometricUsername = ConfigurationManager.AppSettings["DIABiometricUsername"].ToString();
        private string DIABiometricPassword = ConfigurationManager.AppSettings["DIABiometricPassword"].ToString();

        [WebMethod(Description = "Log in XDS Connect Web Service and returns a Ticket")]
        public string Login(string strUser, string strPwd)
        {
            LoginUser oLoginUser = AuthenticateUser(strUser, strPwd);

            if (oLoginUser.LoginStatus == LoginUser.LoginUserStatus.Authenticated)
            {

                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                   1,                            // version
                   oLoginUser.LoginSystemUser.SystemUserID.ToString(),                      // user name
                   DateTime.Now,                 // create time
                   DateTime.Now.AddHours(5),  // expire time
                   false,                        // persistent
                   oLoginUser.LoginSystemUser.SubscriberID.ToString());              // user data

                oLoginUser.LoginTicket = FormsAuthentication.Encrypt(ticket);
                //HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, oLoginUser.LoginTicket);
                //Context.Response.Cookies.Add(cookie);
                return oLoginUser.LoginTicket;
            }
            else
                return oLoginUser.LoginStatus.ToString();
        }

        [WebMethod(Description = "Checks Validity of your XDS Connect Ticket")]
        public bool IsTicketValid(string XDSConnectTicket)
        {
            try
            {
                FormsAuthenticationTicket a = FormsAuthentication.Decrypt(XDSConnectTicket);

                bool isValid = true;
                if (a.Expired) isValid = false;
                return isValid;
            }
            catch
            {
                return false;
            }
        }

        private LoginUser AuthenticateUser(string strUser, string strPwd)
        {
            LoginUser oLoginUser = new LoginUser();

            AdminCon.Open();

            if (Membership.ValidateUser(strUser, strPwd))
            {
                XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUser(AdminCon, strUser);

                if (oSystemUser != null)
                {
                    XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
                    XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(AdminCon, oSystemUser.SubscriberID);

                    oLoginUser.LoginStatus = LoginUser.LoginUserStatus.Authenticated;
                    oLoginUser.LoginSystemUser = oSystemUser;

                    if (!oSystemUser.ActiveYN) oLoginUser.LoginStatus = LoginUser.LoginUserStatus.UserDeactivated;
                    if (oSubscriber.StatusInd != "A") oLoginUser.LoginStatus = LoginUser.LoginUserStatus.SubscriberDeactivated;

                }
                else
                {
                    oLoginUser.LoginStatus = LoginUser.LoginUserStatus.UserNotFound;
                }

            }
            else
            {
                oLoginUser.LoginStatus = LoginUser.LoginUserStatus.NotAuthenticated;
            }
            AdminCon.Close();
            return oLoginUser;
            //throw new NotImplementedException();
        }

        [WebMethod(Description = "")]
        // Method for MTN
        public XDSPortalLibrary.Entity_Layer.MTN.SubmitResponse ConnectCustomVettingQ(int ProductIndicator, string Company, string RuleSet, string Branch, string User, string Password, string IsExistingClient, string GrossMonthlyIncome, string Surname, string FirstName, string IDType, string BirthDate, string IdentityNumber, string Gender, string MaritalStatus, string AddressLine1, string AddressLine2, string Suburb, string City, string PostalCode, string HomeTelephoneCode, string HomeTelephoneNumber, string WorkTelephoneCode, string WorkTelephoneNumber, string CellNumber, string Occupation, string Employer, string NumberOfYearsAtEmployer, string BankName, string BankAccountType, string BankBranchCode, string BankAccountNumber, string HighestQualification, string UniqueReference)
        {
            Company = string.IsNullOrEmpty(Company) ? string.Empty : Company;
            RuleSet = string.IsNullOrEmpty(RuleSet) ? string.Empty : RuleSet;
            Branch = string.IsNullOrEmpty(Branch) ? string.Empty : Branch;
            User = string.IsNullOrEmpty(User) ? string.Empty : User;
            Password = string.IsNullOrEmpty(Password) ? string.Empty : Password;
            IsExistingClient = string.IsNullOrEmpty(IsExistingClient) ? string.Empty : IsExistingClient;
            GrossMonthlyIncome = string.IsNullOrEmpty(GrossMonthlyIncome) ? string.Empty : GrossMonthlyIncome;
            Surname = string.IsNullOrEmpty(Surname) ? string.Empty : Surname;
            FirstName = string.IsNullOrEmpty(FirstName) ? string.Empty : FirstName;
            IDType = string.IsNullOrEmpty(IDType) ? string.Empty : IDType;
            BirthDate = string.IsNullOrEmpty(BirthDate) ? string.Empty : BirthDate;
            IdentityNumber = string.IsNullOrEmpty(IdentityNumber) ? string.Empty : IdentityNumber;
            Gender = string.IsNullOrEmpty(Gender) ? string.Empty : Gender;
            MaritalStatus = string.IsNullOrEmpty(MaritalStatus) ? string.Empty : MaritalStatus;
            AddressLine1 = string.IsNullOrEmpty(AddressLine1) ? string.Empty : AddressLine1;
            AddressLine2 = string.IsNullOrEmpty(AddressLine2) ? string.Empty : AddressLine2;
            Suburb = string.IsNullOrEmpty(Suburb) ? string.Empty : Suburb;
            City = string.IsNullOrEmpty(City) ? string.Empty : City;
            PostalCode = string.IsNullOrEmpty(PostalCode) ? string.Empty : PostalCode;
            HomeTelephoneCode = string.IsNullOrEmpty(HomeTelephoneCode) ? string.Empty : HomeTelephoneCode;
            HomeTelephoneNumber = string.IsNullOrEmpty(HomeTelephoneNumber) ? string.Empty : HomeTelephoneNumber;
            WorkTelephoneCode = string.IsNullOrEmpty(WorkTelephoneCode) ? string.Empty : WorkTelephoneCode;
            WorkTelephoneNumber = string.IsNullOrEmpty(WorkTelephoneNumber) ? string.Empty : WorkTelephoneNumber;
            CellNumber = string.IsNullOrEmpty(CellNumber) ? string.Empty : CellNumber;
            Occupation = string.IsNullOrEmpty(Occupation) ? string.Empty : Occupation;
            Employer = string.IsNullOrEmpty(Employer) ? string.Empty : Employer;
            NumberOfYearsAtEmployer = string.IsNullOrEmpty(NumberOfYearsAtEmployer) ? "0" : NumberOfYearsAtEmployer;
            BankName = string.IsNullOrEmpty(BankName) ? string.Empty : BankName;
            BankAccountType = string.IsNullOrEmpty(BankAccountType) ? string.Empty : BankAccountType;
            BankBranchCode = string.IsNullOrEmpty(BankBranchCode) ? string.Empty : BankBranchCode;
            BankAccountNumber = string.IsNullOrEmpty(BankAccountNumber) ? string.Empty : BankAccountNumber;
            HighestQualification = string.IsNullOrEmpty(HighestQualification) ? string.Empty : HighestQualification;

            // File.AppendAllText(@"C:\Log\JDVet.txt", "Step 1 \n");

            DateTime dtStartdate = DateTime.Now;

            //  File.AppendAllText(@"C:\Log\JDVet.txt", "Step 2 \n");
            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();
            string rXml = string.Empty, ConnectTicket = string.Empty;

            //  File.AppendAllText(@"C:\Log\JDVet.txt", "Step 3 \n");

            XDSPortalLibrary.Entity_Layer.MTN.SubmitResponse oSResponse = new XDSPortalLibrary.Entity_Layer.MTN.SubmitResponse();
            oSResponse.SubmitResult.BureauResponse = new XDSPortalLibrary.Entity_Layer.MTN.BureauResponse();

            // File.AppendAllText(@"C:\Log\JDVet.txt", "Step 4 \n");

            XmlSerializer serializer = new XmlSerializer(typeof(XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplication));
            XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplication objInput = new XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplication();

            // File.AppendAllText(@"C:\Log\JDVet.txt", "Step 5 \n");

            int intCustomVettingOinputID = 0;

            // File.AppendAllText(@"C:\Log\JDVet.txt", "Step 6 \n");

            try
            {

                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step 7 \n");
                SqlConnection con = new SqlConnection(EnquiryCon.ConnectionString);

                SqlCommand cmd = new SqlCommand("Insert into CustomVettingQinput (Input,CreatedOnDate) values(@inputXMLstring,getdate())", con);

                cmd.Parameters.AddWithValue("@inputXMLstring", ("productIndicator:" + ProductIndicator.ToString() + "Company:" + Company + "RuleSet:" + RuleSet + "Branch:" + Branch + "User:" + User + "Password:" + Password + "IsExistingClient:" + IsExistingClient + "GrossMonthlyIncome:" + GrossMonthlyIncome + "Surname:" + Surname + "FirstName:" + FirstName + "IDType:" + IDType + "BirthDate:" + BirthDate + "IdentityNumber:" + IdentityNumber + "Gender:" + Gender + "MaritalStatus:" + MaritalStatus + "AddressLine1:" + AddressLine1 + "AddressLine2:" + AddressLine2 + "Suburb:" + Suburb + "City:" + City + "PostalCode:" + PostalCode + "HomeTelephoneCode:" + HomeTelephoneCode + "HomeTelephoneNumber:" + HomeTelephoneNumber + "WorkTelephoneCode:" + WorkTelephoneCode + "WorkTelephoneNumber:" + WorkTelephoneNumber + "CellNumber:" + CellNumber + "Occupation:" + Occupation + "Employer:" + Employer + "NumberOfYearsAtEmployer:" + NumberOfYearsAtEmployer + "BankName:" + BankName + "BankAccountType:" + BankAccountType + "BankBranchCode:" + BankBranchCode + "BankAccountNumber:" + BankAccountNumber + "HighestQualification:" + HighestQualification + "UniqueReference:" + UniqueReference));
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                string strSQL = "SELECT @@IDENTITY";
                SqlCommand Imycommand = new SqlCommand(strSQL, con);
                SqlDataReader Idr = Imycommand.ExecuteReader();
                Idr.Read();
                intCustomVettingOinputID = Convert.ToInt32(Idr.GetValue(0));
                Idr.Close();

                con.Close();

                //parse XML

                //xml = xml.Replace("&", string.Empty);
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx \n");
                objInput.input = new XDSPortalLibrary.Entity_Layer.MTNHResponse.MTNInput();
                objInput.input.AddressLine1 = AddressLine1;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 1\n");
                objInput.input.AddressLine2 = AddressLine2;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 2\n");
                objInput.input.BankAccountNumber = BankAccountNumber;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 3\n");
                objInput.input.BankAccountType = BankAccountType;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 4\n");
                objInput.input.BankBranchCode = BankBranchCode;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 5\n");
                objInput.input.BankName = BankName;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 6\n");
                objInput.input.BirthDate = BirthDate;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 7\n");
                objInput.input.Branch = Branch;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 8\n");
                objInput.input.CellNumber = CellNumber;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 9\n");
                objInput.input.City = City;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 10\n");
                objInput.input.Company = Company;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 11\n");
                objInput.input.Employer = Employer;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 12\n");
                objInput.input.FirstName = FirstName;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 13\n");
                objInput.input.Gender = Gender;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 14\n");
                objInput.input.GrossMonthlyIncome = GrossMonthlyIncome;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 15\n");
                objInput.input.HighestQualification = HighestQualification;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 16\n");
                objInput.input.HomeTelephoneCode = HomeTelephoneCode;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 17\n");
                objInput.input.HomeTelephoneNumber = HomeTelephoneNumber;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 18\n");
                objInput.input.IdentityNumber = IdentityNumber;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 19\n");
                objInput.input.IDType = IDType;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 20\n");
                objInput.input.IsExistingClient = IsExistingClient;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 21\n");
                objInput.input.MaritalStatus = MaritalStatus;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 22\n");
                objInput.input.NumberOfYearsAtEmployer = NumberOfYearsAtEmployer;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 23\n");
                objInput.input.Occupation = Occupation;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 24\n");
                objInput.input.Password = Password;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 25\n");
                objInput.input.PostalCode = PostalCode;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 26\n");
                objInput.input.RuleSet = RuleSet;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 27\n");
                objInput.input.Suburb = Suburb;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 28\n");
                objInput.input.Surname = Surname;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 29\n");
                objInput.input.User = User;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 30\n");
                objInput.input.WorkTelephoneCode = WorkTelephoneCode;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 31\n");
                objInput.input.WorkTelephoneNumber = WorkTelephoneNumber;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 32\n");

                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step yyy \n");

                // oSResponse.SubmitResult.ClientReference = objInput.input.ClientReference;

                if (objInput.input.IDType.ToUpper() == XDSPortalLibrary.Entity_Layer.JDFSPrincipaRequest.IDType.OTHER.ToString())
                {
                    objInput.input.IDType = XDSPortalLibrary.Entity_Layer.JDFSPrincipaRequest.IDType.PASSPORT.ToString();
                }
                // Validate input

                XDSPortalLibrary.Entity_Layer.Response oResponse = new XDSPortalLibrary.Entity_Layer.Response();
                CustomVetting oCustomvetting = new CustomVetting();
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step ValidateConnectCustomVettingQ \n");
                oResponse = oCustomvetting.ValidateConnectCustomVettingQSOA(objInput);
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step ValidateConnectCustomVettingQ \n");

                if (oResponse.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                {
                    oSResponse.SubmitResult.Status = "Failure";
                    oSResponse.SubmitResult.ErrorDescription = oResponse.ResponseData;

                }
                else if (oResponse.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                {
                    // Login

                    ConnectTicket = Login(objInput.input.User, objInput.input.Password);


                    if (IsTicketValid(ConnectTicket))
                    {
                        FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);

                        string iDnumber = string.Empty, passportNo = string.Empty;

                        iDnumber = (objInput.input.IDType.ToUpper() == "SAID") ? objInput.input.IdentityNumber : string.Empty;
                        passportNo = (objInput.input.IDType.ToUpper() == "PASSPORT") ? objInput.input.IdentityNumber : string.Empty;
                        //passportNo = (objInput.Items.IDType.ToUpper() == XDSPortalLibrary.Entity_Layer.JDFSPrincipaRequest.IDType.OTHER.ToString()) ? objInput.Items.IDNumber : string.Empty;

                        try
                        {
                            XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                            XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(AdminCon, int.Parse(oTicket.Name));

                            XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
                            XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(AdminCon, oSystemUser.SubscriberID);

                            XDSPortalEnquiry.Business.CustomVetting dtCustomVetting = new XDSPortalEnquiry.Business.CustomVetting();

                            //   File.AppendAllText(@"C:\Log\Response.txt", "EnterSubmitConsumerTrace");
                            oResponse = dtCustomVetting.SubmitConsumerTraceVettingQ(EnquiryCon, AdminCon, oSubscriber.SubscriberID, oSystemUser.SystemUserID, 137, oSubscriber.SubscriberName, iDnumber, passportNo, objInput.input.FirstName, objInput.input.Surname, objInput.input.BirthDate,
                                objInput.input.Gender, objInput.input.MaritalStatus, objInput.input.AddressLine1, objInput.input.AddressLine2, objInput.input.Suburb, objInput.input.City, objInput.input.PostalCode, objInput.input.HomeTelephoneCode, objInput.input.HomeTelephoneNumber, objInput.input.WorkTelephoneCode, objInput.input.WorkTelephoneNumber, objInput.input.CellNumber, double.Parse(objInput.input.GrossMonthlyIncome), objInput.input.Occupation, objInput.input.Employer, objInput.input.BankAccountType, objInput.input.BankName,
                                objInput.input.HighestQualification, objInput.input.Branch, objInput.input.BankAccountNumber, int.Parse(objInput.input.NumberOfYearsAtEmployer), string.Empty, true, false, string.Empty);
                            // File.AppendAllText(@"C:\Log\Response.txt", "EndSubmitConsumerTrace");
                            // File.AppendAllText(@"C:\Log\Response.txt", oResponse.ResponseData);
                            switch (oResponse.ResponseStatus)
                            {
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single:
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple:

                                    DataSet dsl = new DataSet();
                                    System.IO.StringReader xmlSR = new System.IO.StringReader(oResponse.ResponseData);
                                    dsl.ReadXml(xmlSR);

                                    //  File.AppendAllText(@"C:\Log\Response.txt", "End xml generate");

                                    if (dsl.Tables[0].Rows.Count == 0 || dsl.Tables[0].Rows.Count == 1)
                                    {
                                        foreach (DataRow dr in dsl.Tables[0].Rows)
                                        {
                                            XDSPortalLibrary.Entity_Layer.MTN.SubmitResponse oinputResponse = new XDSPortalLibrary.Entity_Layer.MTN.SubmitResponse();
                                            // oResponse = dtCustomVetting.SubmitMulipleCreditEnquiry(con, AdminCon, int.Parse(dr["EnquiryID"].ToString()), int.Parse(dr["EnquiryResultID"].ToString()), false, string.Empty, Unique_ref, QueryNumber.ToString());
                                            oinputResponse = oSResponse;


                                            //   File.AppendAllText(@"C:\Log\Response.txt", "EnterSubmitCustomVettingO");
                                            oResponse = dtCustomVetting.SubmitCustomVettingQSOA(EnquiryCon, AdminCon, int.Parse(dr["EnquiryID"].ToString()), int.Parse(dr["EnquiryResultID"].ToString()), false, string.Empty, oinputResponse, objInput, out oSResponse, ProductIndicator, IsExistingClient);
                                            //  File.AppendAllText(@"C:\Log\Response.txt", "EndSubmitCustomVettingO");
                                        }
                                        switch (oResponse.ResponseStatus)
                                        {
                                            case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                                                oSResponse.SubmitResult.Status = "Failure";
                                                oSResponse.SubmitResult.ErrorDescription = oResponse.ResponseData;
                                                break;
                                            case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                                                oSResponse.SubmitResult.Status = "Success";
                                                oSResponse.SubmitResult.ErrorDescription = string.Empty;
                                                break;
                                        }
                                    }
                                    else
                                    {
                                        oSResponse.SubmitResult.Status = "Failure";
                                        oSResponse.SubmitResult.ErrorDescription = "Multiple Match Found";

                                    }
                                    break;

                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                                    oSResponse.SubmitResult.Status = "Failure";
                                    oSResponse.SubmitResult.ErrorDescription = oResponse.ResponseData;
                                    break;
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                                    oSResponse.SubmitResult.Status = "Failure";
                                    oSResponse.SubmitResult.ErrorDescription = "No Record Found";
                                    break;
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                                    oSResponse.SubmitResult.Status = "Success";
                                    oSResponse.SubmitResult.ErrorDescription = string.Empty;
                                    break;
                            }


                        }
                        catch (Exception e)
                        {
                            oSResponse.SubmitResult.Status = "Failure";
                            oSResponse.SubmitResult.ErrorDescription = e.Message;
                        }

                    }
                    else
                    {
                        oSResponse.SubmitResult.Status = "Failure";
                        oSResponse.SubmitResult.ErrorDescription = ConnectTicket;
                    }
                }

            }
            catch (Exception ex)
            {
                oSResponse.SubmitResult.Status = "Failure";
                oSResponse.SubmitResult.ErrorDescription = ex.Message;
            }

            DateTime dtenddate = DateTime.Now;
            double diff = dtenddate.Subtract(dtStartdate).TotalSeconds;

            oSResponse.SubmitResult.BureauResponse.ProcessingTimeSecs = Math.Round(float.Parse(diff.ToString()), 2);

            if (oSResponse.SubmitResult.Status == "Failure")
            {
                oSResponse.SubmitResult.Outcome = "Decline";
                oSResponse.SubmitResult.BureauResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.MTN.ResponseStatus.Failure; ;
                oSResponse.SubmitResult.Reason = "Decline";
            }
            else if (oSResponse.SubmitResult.Status == "Success")
            {
                oSResponse.SubmitResult.Outcome = "Approve";
                oSResponse.SubmitResult.BureauResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.MTN.ResponseStatus.Success;
                oSResponse.SubmitResult.Reason = "Approve";
            }

            oSResponse.SubmitResult.BureauResponse.UniqueRefGuid = Guid.NewGuid();
            //oSResponse.SubmitResult = oSubmitResponse;
            string response = string.Empty;
            try
            {
                //  File.AppendAllText(@"C:\Log\Response.txt", "Enterdeserialization");
                XmlSerializer serializerout = new XmlSerializer(oSResponse.GetType());
                System.IO.StringWriter swout = new System.IO.StringWriter();
                serializerout.Serialize(swout, oSResponse);
                System.IO.StringReader readerout = new System.IO.StringReader(swout.ToString());

                response = readerout.ReadToEnd();

                SqlConnection con = new SqlConnection(Properties.Settings.Default.EnquiryConnectionString);

                SqlCommand cmd = new SqlCommand("Insert into CustomVettingQResponse (CustomVettingOinputID,Response,CreatedOnDate) values(@ID, @inputXMLstring,getdate())", con);
                cmd.Parameters.AddWithValue("@ID", intCustomVettingOinputID);
                cmd.Parameters.AddWithValue("@inputXMLstring", response);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                con.Close();
            }

            catch (Exception ex)
            {
                // File.AppendAllText(@"C:\Log\Response.txt", "EnterException");
                SqlConnection con = new SqlConnection(Properties.Settings.Default.EnquiryConnectionString);

                SqlCommand cmd = new SqlCommand("Insert into CustomVettingQResponse (CustomVettingOinputID,Response,CreatedOnDate) values(@ID, @inputXMLstring,getdate())", con);
                cmd.Parameters.AddWithValue("@ID", intCustomVettingOinputID);
                cmd.Parameters.AddWithValue("@inputXMLstring", ex.Message);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                con.Close();

            }

            return oSResponse;

        }

        [WebMethod(Description = "")]
        public string CommercialQuestionnaire(string ConnectTicket, string SubscriberReference, int ProductID, XDSPortalLibrary.Entity_Layer.CommercialQuestionnaire oCommercialQuestionnaire)
        {
            string rXml = "";
            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();

            CommercialQuestionnaireResult oResult = new CommercialQuestionnaireResult();
            int intCustomVettingOinputID = 0;

            if (IsTicketValid(ConnectTicket))
            {
                FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);

                try
                {
                    SqlConnection con = new SqlConnection(EnquiryCon.ConnectionString);

                    XmlSerializer serializer = new XmlSerializer(oCommercialQuestionnaire.GetType());
                    System.IO.StringWriter sw = new System.IO.StringWriter();
                    serializer.Serialize(sw, oCommercialQuestionnaire);
                    System.IO.StringReader reader = new System.IO.StringReader(sw.ToString());

                    SqlCommand cmd = new SqlCommand("Insert into CommercialQuestionnaireinput (Input,CreatedOnDate) values(@inputXMLstring,getdate())", con);

                    cmd.Parameters.AddWithValue("@inputXMLstring", reader.ReadToEnd());
                    if (con.State == ConnectionState.Closed)
                        con.Open();

                    cmd.ExecuteNonQuery();

                    string strSQL = "SELECT @@IDENTITY";
                    SqlCommand Imycommand = new SqlCommand(strSQL, con);
                    SqlDataReader Idr = Imycommand.ExecuteReader();
                    Idr.Read();
                    intCustomVettingOinputID = Convert.ToInt32(Idr.GetValue(0));
                    Idr.Close();

                    con.Close();


                    XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                    XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(Properties.Settings.Default.AdminConnectionString, int.Parse(oTicket.Name));


                    XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();


                    // File.AppendAllText(@"C:\Log\History.txt", "Here1" + "\n");
                    XDSPortalEnquiry.Business.CommercialEnquiry_BusinessInvestigativeEnquiry dtCommercial = new XDSPortalEnquiry.Business.CommercialEnquiry_BusinessInvestigativeEnquiry();
                    oResult = dtCommercial.SubmitCommercialQuestionnaire(EnquiryCon, AdminCon, MTNCommercialQuestionnaireCon, oCommercialQuestionnaire, SubscriberReference, oSystemUser.SubscriberID,
                         oSystemUser.SystemUserID, ProductID, string.Empty);

                    XmlSerializer serializer2 = new XmlSerializer(oResult.GetType());
                    System.IO.StringWriter sw2 = new System.IO.StringWriter();
                    serializer2.Serialize(sw2, oResult);
                    System.IO.StringReader reader2 = new System.IO.StringReader(sw2.ToString());

                    rXml = reader2.ReadToEnd(); ;

                    rsXml.LoadXml(rXml);

                    if (rXml.Contains("Information successfully saved"))
                    {
                        string emailResponse = SendEmail(oCommercialQuestionnaire.CIPC.CompanyInformation.BusinessRegistrationNumber,
                                  oCommercialQuestionnaire.CIPC.CompanyInformation.RegisteredName, SubscriberReference);

                        if (!string.IsNullOrEmpty(emailResponse))
                        {
                            throw new Exception(emailResponse);
                        }
                    }
                    //  File.AppendAllText(@"C:\Log\History.txt", rp.ResponseData + "\n");

                    //switch (rp.ResponseStatus)
                    //{
                    //    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single:
                    //    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple:
                    //    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                    //        rsXml.LoadXml(rp.ResponseData);
                    //        break;

                    //    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                    //        rXml = "<NoResult><Error>" + rp.ResponseData + "</Error></NoResult>";
                    //        rsXml.LoadXml(rXml);
                    //        break;
                    //    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                    //        rXml = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                    //        rsXml.LoadXml(rXml);
                    //        break;
                    //}
                }

                catch (Exception oException)
                {
                    oResult.Result = string.Empty;
                    oResult.XDSReferenceNo = "0";
                    oResult.Error = "(.NetException) " + oException.Message + "";

                    XmlSerializer serializer = new XmlSerializer(oResult.GetType());
                    System.IO.StringWriter sw = new System.IO.StringWriter();
                    serializer.Serialize(sw, oResult);
                    System.IO.StringReader reader = new System.IO.StringReader(sw.ToString());

                    rXml = reader.ReadToEnd(); ;
                    rsXml.LoadXml(rXml);
                }

            }
            else
            {
                oResult.Result = string.Empty;
                oResult.XDSReferenceNo = "0";
                oResult.Error = "Invalid Ticket";

                XmlSerializer serializer = new XmlSerializer(oResult.GetType());
                System.IO.StringWriter sw = new System.IO.StringWriter();
                serializer.Serialize(sw, oResult);
                System.IO.StringReader reader = new System.IO.StringReader(sw.ToString());

                rXml = reader.ReadToEnd(); ;

                // rXml = "<NoResult><Error>Invalid Ticket</Error></NoResult>";
                rsXml.LoadXml(rXml);
            }

            try
            {
                //  File.AppendAllText(@"C:\Log\Response.txt", "Enterdeserialization");
                XmlSerializer serializerout = new XmlSerializer(oResult.GetType());
                System.IO.StringWriter swout = new System.IO.StringWriter();
                serializerout.Serialize(swout, oResult);
                System.IO.StringReader readerout = new System.IO.StringReader(swout.ToString());

                string response = readerout.ReadToEnd();

                SqlConnection con = new SqlConnection(Properties.Settings.Default.EnquiryConnectionString);

                SqlCommand cmd = new SqlCommand("Insert into CommercialQuestionnaireResponse (inputID,Response,CreatedOnDate) values(@ID, @inputXMLstring,getdate())", con);
                cmd.Parameters.AddWithValue("@ID", intCustomVettingOinputID);
                cmd.Parameters.AddWithValue("@inputXMLstring", response);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                con.Close();
            }

            catch (Exception ex)
            {
                // File.AppendAllText(@"C:\Log\Response.txt", "EnterException");
                SqlConnection con = new SqlConnection(Properties.Settings.Default.EnquiryConnectionString);

                SqlCommand cmd = new SqlCommand("Insert into CommercialQuestionnaireResponse (inputID,Response,CreatedOnDate) values(@ID, @inputXMLstring,getdate())", con);
                cmd.Parameters.AddWithValue("@ID", intCustomVettingOinputID);
                cmd.Parameters.AddWithValue("@inputXMLstring", ex.Message);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                con.Close();

            }
            return rsXml.OuterXml;
        }

        private string SendEmail(string RegistrationNumber, string BusinessName, string SubscriberReference)
        {
            string rxml = string.Empty;
            bool EnableSSL = bool.Parse(ConfigurationManager.AppSettings["EnableSSL"].ToString());
            try
            {

                string DirPath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                SmtpClient oSmtp = new SmtpClient(strSmtpServerName, intport);
                oSmtp.Credentials = new NetworkCredential(strSmtpUserLoginID, strSmtpUserPassword);

                oSmtp.EnableSsl = EnableSSL;

                MailMessage xmail = new MailMessage();

                TemplateParser.Parser xparser = null;

                xmail.To.Add(ConfigurationManager.AppSettings["XDSSmtpBusinessInvestigativeEmailBCC"].ToString());

                xmail.From = new MailAddress(ConfigurationManager.AppSettings["XDSSmtpBusinessInvestigativeEmailFrom"].ToString(), "MTN New Request");

                System.Collections.Hashtable template = new System.Collections.Hashtable();

                template.Add("Reference", SubscriberReference);
                template.Add("RegistrationNumber", RegistrationNumber);
                template.Add("BusinessName", BusinessName);

                xmail.Subject = "XDS Business Investigative Enquiry - MTN Ref#: " + SubscriberReference;

                xparser = new TemplateParser.Parser(DirPath + "EmailTemplates\\BusinessInvestigativeMTN.htm", template);

                xmail.IsBodyHtml = true;
                xmail.BodyEncoding = Encoding.UTF8;
                xmail.Body = xparser.Parse();

                oSmtp.Send(xmail);

            }
            catch (Exception ex)
            {
                rxml = ex.Message;
            }
            return rxml;

        }

        [WebMethod(Description = "")]
        public string PostCommercialReport(string TransactionID, string SubscriberReference, string Status, string StatusReason, string Report)
        {
            string strResponse = string.Empty, strResult = string.Empty;
            int statusCode = 0;

            try
            {
                string message = @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:hel=""http://esm.mtn.co.za/data/commonbusiness/HeliosCommercialReport"" xmlns:v1=""http://esm.mtn.co.za/data/common/header/v1"">";
                message += "<soapenv:Header/>";
                message += "<soapenv:Body>";
                message += "<hel:HeliosCommercialRequest>";
                message += "<hel:ESFHeaderV1Helios>";
                message += "<v1:ClientID/>";
                message += "<v1:ServiceInterface/>";
                message += "<v1:RequiredVersion/>";
                message += "<v1:ServiceOperation></v1:ServiceOperation>";
                message += "<v1:TransactionID>" + TransactionID + "</v1:TransactionID>";
                message += "<v1:CorrelationID/>";
                message += "<v1:ReceivedTimeStamp/>";
                message += "<v1:ReponseTimeStamp/>";
                message += "<v1:ClientIPAddress/>";
                message += "<v1:InternalMessageID/>";
                message += "<v1:Extensions name=\"?\"/>";
                message += "</hel:ESFHeaderV1Helios>";
                message += "<hel:getHeliosCommercialReport>";
                message += "<hel:ProspectId>" + SubscriberReference + "</hel:ProspectId>";
                message += "<hel:Status>" + Status + "</hel:Status>";
                message += "<hel:StatusReason>" + StatusReason + "</hel:StatusReason>";
                message += "<hel:CommercialReport>" + Report + "</hel:CommercialReport>";
                message += "</hel:getHeliosCommercialReport>";
                message += "</hel:HeliosCommercialRequest>";
                message += "</soapenv:Body>";
                message += "</soapenv:Envelope>";

                XmlDocument xmlDoc = new XmlDocument();

                xmlDoc.LoadXml(message);

                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
                HttpWebRequest ws = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["MTNQuestionnaireURL"].ToString());
                ws.Headers.Add("SOAPAction", "\"http://esm.mtn.co.za/data/commonbusiness/HeliosCommercialReport/HeliosCommercialReportCallback\"");
                ws.ContentType = "text/xml; encoding='utf-8'";
                ws.Method = "POST";

                Stream stm = ws.GetRequestStream();
                xmlDoc.Save(stm);
                stm.Close();

                using (HttpWebResponse webResponse = (HttpWebResponse)ws.GetResponse())
                {
                    using (Stream responseStream = webResponse.GetResponseStream())
                    {
                        using (StreamReader responseStreamReader = new StreamReader(responseStream, true))
                        {
                            strResult = responseStreamReader.ReadToEnd();
                            responseStreamReader.Close();
                            strResponse = "Success";
                        }

                        responseStream.Close();
                    }
                    webResponse.Close();
                }

                if (strResult != "")
                {
                    XmlDocument xmlReturnDoc = new XmlDocument();
                    xmlReturnDoc.LoadXml(strResult);

                    XmlNamespaceManager manager = new XmlNamespaceManager(xmlReturnDoc.NameTable);
                    manager.AddNamespace("soapenv", "http://schemas.xmlsoap.org/soap/envelope/");
                    manager.AddNamespace("s", "http://schemas.xmlsoap.org/soap/envelope/");
                    manager.AddNamespace("ns2", "http://esm.mtn.co.za/data/commonbusiness/HeliosCommercialReport");

                    XmlNode xnResult = xmlReturnDoc.SelectSingleNode("/soapenv:Envelope/s:Body/ns2:HeliosCommercialResponse", manager);
                    strResponse = xnResult["ns2:CommercialReportCallbackResult"].InnerText;
                }
            }
            catch (WebException oWebException)
            {

                switch (oWebException.Status)
                {
                    case WebExceptionStatus.ProtocolError:
                        HttpWebResponse oHttpResponse = (HttpWebResponse)oWebException.Response;
                        statusCode = (int)oHttpResponse.StatusCode;
                        strResponse = statusCode.ToString();
                        break;
                    case WebExceptionStatus.ConnectionClosed:
                        strResponse = "ConnectionClosed";
                        break;
                    case WebExceptionStatus.ConnectFailure:
                        strResponse = "ConnectFailure";
                        break;
                    case WebExceptionStatus.Timeout:
                        strResponse = "Timeout";
                        break;
                    case WebExceptionStatus.TrustFailure:
                        strResponse = "TrustFailure";
                        break;
                    case WebExceptionStatus.MessageLengthLimitExceeded:
                        strResponse = "MessageLengthLimitExceeded";
                        break;
                    case WebExceptionStatus.PipelineFailure:
                        strResponse = "PipelineFailure";
                        break;
                    case WebExceptionStatus.ServerProtocolViolation:
                        strResponse = "ServerProtocolViolation";
                        break;
                    case WebExceptionStatus.RequestCanceled:
                        strResponse = "RequestCanceled";
                        break;
                    case WebExceptionStatus.SendFailure:
                        strResponse = "SendFailure";
                        break;
                    case WebExceptionStatus.UnknownError:
                        strResponse = "UnknownError";
                        break;
                    default:
                        strResponse = oWebException.Message;
                        break;
                }
            }
            catch (Exception exception)
            {
                strResponse = exception.Message;
            }

            return strResponse;
        }

        [WebMethod(Description = "")]
        // Method for MTN
        public XDSPortalLibrary.Entity_Layer.MTN.SubmitResponse ConnectCustomVettingQA(int ProductIndicator, string Company, string RuleSet, string Branch, string User, string Password, string IsExistingClient, string GrossMonthlyIncome, string Surname, string FirstName, string IDType, string BirthDate, string IdentityNumber, string Gender, string MaritalStatus, string AddressLine1, string AddressLine2, string Suburb, string City, string PostalCode, string HomeTelephoneCode, string HomeTelephoneNumber, string WorkTelephoneCode, string WorkTelephoneNumber, string CellNumber, string Occupation, string Employer, string NumberOfYearsAtEmployer, string BankName, string BankAccountType, string BankBranchCode, string BankAccountNumber, string HighestQualification)
        {
            Company = string.IsNullOrEmpty(Company) ? string.Empty : Company;
            RuleSet = string.IsNullOrEmpty(RuleSet) ? string.Empty : RuleSet;
            Branch = string.IsNullOrEmpty(Branch) ? string.Empty : Branch;
            User = string.IsNullOrEmpty(User) ? string.Empty : User;
            Password = string.IsNullOrEmpty(Password) ? string.Empty : Password;
            IsExistingClient = string.IsNullOrEmpty(IsExistingClient) ? string.Empty : IsExistingClient;
            GrossMonthlyIncome = string.IsNullOrEmpty(GrossMonthlyIncome) ? string.Empty : GrossMonthlyIncome;
            Surname = string.IsNullOrEmpty(Surname) ? string.Empty : Surname;
            FirstName = string.IsNullOrEmpty(FirstName) ? string.Empty : FirstName;
            IDType = string.IsNullOrEmpty(IDType) ? string.Empty : IDType;
            BirthDate = string.IsNullOrEmpty(BirthDate) ? string.Empty : BirthDate;
            IdentityNumber = string.IsNullOrEmpty(IdentityNumber) ? string.Empty : IdentityNumber;
            Gender = string.IsNullOrEmpty(Gender) ? string.Empty : Gender;
            MaritalStatus = string.IsNullOrEmpty(MaritalStatus) ? string.Empty : MaritalStatus;
            AddressLine1 = string.IsNullOrEmpty(AddressLine1) ? string.Empty : AddressLine1;
            AddressLine2 = string.IsNullOrEmpty(AddressLine2) ? string.Empty : AddressLine2;
            Suburb = string.IsNullOrEmpty(Suburb) ? string.Empty : Suburb;
            City = string.IsNullOrEmpty(City) ? string.Empty : City;
            PostalCode = string.IsNullOrEmpty(PostalCode) ? string.Empty : PostalCode;
            HomeTelephoneCode = string.IsNullOrEmpty(HomeTelephoneCode) ? string.Empty : HomeTelephoneCode;
            HomeTelephoneNumber = string.IsNullOrEmpty(HomeTelephoneNumber) ? string.Empty : HomeTelephoneNumber;
            WorkTelephoneCode = string.IsNullOrEmpty(WorkTelephoneCode) ? string.Empty : WorkTelephoneCode;
            WorkTelephoneNumber = string.IsNullOrEmpty(WorkTelephoneNumber) ? string.Empty : WorkTelephoneNumber;
            CellNumber = string.IsNullOrEmpty(CellNumber) ? string.Empty : CellNumber;
            Occupation = string.IsNullOrEmpty(Occupation) ? string.Empty : Occupation;
            Employer = string.IsNullOrEmpty(Employer) ? string.Empty : Employer;
            NumberOfYearsAtEmployer = string.IsNullOrEmpty(NumberOfYearsAtEmployer) ? "0" : NumberOfYearsAtEmployer;
            BankName = string.IsNullOrEmpty(BankName) ? string.Empty : BankName;
            BankAccountType = string.IsNullOrEmpty(BankAccountType) ? string.Empty : BankAccountType;
            BankBranchCode = string.IsNullOrEmpty(BankBranchCode) ? string.Empty : BankBranchCode;
            BankAccountNumber = string.IsNullOrEmpty(BankAccountNumber) ? string.Empty : BankAccountNumber;
            HighestQualification = string.IsNullOrEmpty(HighestQualification) ? string.Empty : HighestQualification;

            // File.AppendAllText(@"C:\Log\JDVet.txt", "Step 1 \n");

            DateTime dtStartdate = DateTime.Now;

            //  File.AppendAllText(@"C:\Log\JDVet.txt", "Step 2 \n");
            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();
            string rXml = string.Empty, ConnectTicket = string.Empty;

            //  File.AppendAllText(@"C:\Log\JDVet.txt", "Step 3 \n");

            XDSPortalLibrary.Entity_Layer.MTN.SubmitResponse oSResponse = new XDSPortalLibrary.Entity_Layer.MTN.SubmitResponse();
            oSResponse.SubmitResult.BureauResponse = new XDSPortalLibrary.Entity_Layer.MTN.BureauResponse();

            // File.AppendAllText(@"C:\Log\JDVet.txt", "Step 4 \n");

            XmlSerializer serializer = new XmlSerializer(typeof(XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplication));
            XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplication objInput = new XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplication();

            // File.AppendAllText(@"C:\Log\JDVet.txt", "Step 5 \n");

            int intCustomVettingOinputID = 0;

            // File.AppendAllText(@"C:\Log\JDVet.txt", "Step 6 \n");

            try
            {

                //File.AppendAllText(@"C:\Log\JDVet.txt", "Step 7 \n");
                SqlConnection con = new SqlConnection(EnquiryCon.ConnectionString);

                SqlCommand cmd = new SqlCommand("Insert into CustomVettingQinput (Input,CreatedOnDate) values(@inputXMLstring,getdate())", con);

                cmd.Parameters.AddWithValue("@inputXMLstring", ("productIndicator:" + ProductIndicator.ToString() + "Company:" + Company + "RuleSet:" + RuleSet + "Branch:" + Branch + "User:" + User + "Password:" + Password + "IsExistingClient:" + IsExistingClient + "GrossMonthlyIncome:" + GrossMonthlyIncome + "Surname:" + Surname + "FirstName:" + FirstName + "IDType:" + IDType + "BirthDate:" + BirthDate + "IdentityNumber:" + IdentityNumber + "Gender:" + Gender + "MaritalStatus:" + MaritalStatus + "AddressLine1:" + AddressLine1 + "AddressLine2:" + AddressLine2 + "Suburb:" + Suburb + "City:" + City + "PostalCode:" + PostalCode + "HomeTelephoneCode:" + HomeTelephoneCode + "HomeTelephoneNumber:" + HomeTelephoneNumber + "WorkTelephoneCode:" + WorkTelephoneCode + "WorkTelephoneNumber:" + WorkTelephoneNumber + "CellNumber:" + CellNumber + "Occupation:" + Occupation + "Employer:" + Employer + "NumberOfYearsAtEmployer:" + NumberOfYearsAtEmployer + "BankName:" + BankName + "BankAccountType:" + BankAccountType + "BankBranchCode:" + BankBranchCode + "BankAccountNumber:" + BankAccountNumber + "HighestQualification:" + HighestQualification));
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                string strSQL = "SELECT @@IDENTITY";
                SqlCommand Imycommand = new SqlCommand(strSQL, con);
                SqlDataReader Idr = Imycommand.ExecuteReader();
                Idr.Read();
                intCustomVettingOinputID = Convert.ToInt32(Idr.GetValue(0));
                Idr.Close();

                con.Close();

                //parse XML

                //xml = xml.Replace("&", string.Empty);
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx \n");
                objInput.input = new XDSPortalLibrary.Entity_Layer.MTNHResponse.MTNInput();
                objInput.input.AddressLine1 = AddressLine1;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 1\n");
                objInput.input.AddressLine2 = AddressLine2;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 2\n");
                objInput.input.BankAccountNumber = BankAccountNumber;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 3\n");
                objInput.input.BankAccountType = BankAccountType;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 4\n");
                objInput.input.BankBranchCode = BankBranchCode;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 5\n");
                objInput.input.BankName = BankName;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 6\n");
                objInput.input.BirthDate = BirthDate;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 7\n");
                objInput.input.Branch = Branch;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 8\n");
                objInput.input.CellNumber = CellNumber;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 9\n");
                objInput.input.City = City;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 10\n");
                objInput.input.Company = Company;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 11\n");
                objInput.input.Employer = Employer;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 12\n");
                objInput.input.FirstName = FirstName;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 13\n");
                objInput.input.Gender = Gender;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 14\n");
                objInput.input.GrossMonthlyIncome = GrossMonthlyIncome;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 15\n");
                objInput.input.HighestQualification = HighestQualification;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 16\n");
                objInput.input.HomeTelephoneCode = HomeTelephoneCode;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 17\n");
                objInput.input.HomeTelephoneNumber = HomeTelephoneNumber;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 18\n");
                objInput.input.IdentityNumber = IdentityNumber;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 19\n");
                objInput.input.IDType = IDType;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 20\n");
                objInput.input.IsExistingClient = IsExistingClient;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 21\n");
                objInput.input.MaritalStatus = MaritalStatus;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 22\n");
                objInput.input.NumberOfYearsAtEmployer = NumberOfYearsAtEmployer;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 23\n");
                objInput.input.Occupation = Occupation;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 24\n");
                objInput.input.Password = Password;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 25\n");
                objInput.input.PostalCode = PostalCode;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 26\n");
                objInput.input.RuleSet = RuleSet;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 27\n");
                objInput.input.Suburb = Suburb;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 28\n");
                objInput.input.Surname = Surname;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 29\n");
                objInput.input.User = User;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 30\n");
                objInput.input.WorkTelephoneCode = WorkTelephoneCode;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 31\n");
                objInput.input.WorkTelephoneNumber = WorkTelephoneNumber;
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step xxx 32\n");

                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step yyy \n");

                // oSResponse.SubmitResult.ClientReference = objInput.input.ClientReference;

                if (objInput.input.IDType.ToUpper() == XDSPortalLibrary.Entity_Layer.JDFSPrincipaRequest.IDType.OTHER.ToString())
                {
                    objInput.input.IDType = XDSPortalLibrary.Entity_Layer.JDFSPrincipaRequest.IDType.PASSPORT.ToString();
                }
                // Validate input

                XDSPortalLibrary.Entity_Layer.Response oResponse = new XDSPortalLibrary.Entity_Layer.Response();
                CustomVetting oCustomvetting = new CustomVetting();
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step ValidateConnectCustomVettingQ \n");
                oResponse = oCustomvetting.ValidateConnectCustomVettingQSOA(objInput);
                // File.AppendAllText(@"C:\Log\JDVet.txt", "Step ValidateConnectCustomVettingQ \n");

                if (oResponse.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                {
                    oSResponse.SubmitResult.Status = "Failure";
                    oSResponse.SubmitResult.ErrorDescription = oResponse.ResponseData;

                }
                else if (oResponse.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                {
                    // Login

                    ConnectTicket = Login(objInput.input.User, objInput.input.Password);


                    if (IsTicketValid(ConnectTicket))
                    {
                        FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);

                        string iDnumber = string.Empty, passportNo = string.Empty;

                        iDnumber = (objInput.input.IDType.ToUpper() == "SAID") ? objInput.input.IdentityNumber : string.Empty;
                        passportNo = (objInput.input.IDType.ToUpper() == "PASSPORT") ? objInput.input.IdentityNumber : string.Empty;
                        //passportNo = (objInput.Items.IDType.ToUpper() == XDSPortalLibrary.Entity_Layer.JDFSPrincipaRequest.IDType.OTHER.ToString()) ? objInput.Items.IDNumber : string.Empty;

                        try
                        {
                            XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                            XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(AdminCon, int.Parse(oTicket.Name));

                            XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
                            XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(AdminCon, oSystemUser.SubscriberID);

                            XDSPortalEnquiry.Business.CustomVetting dtCustomVetting = new XDSPortalEnquiry.Business.CustomVetting();

                            //   File.AppendAllText(@"C:\Log\Response.txt", "EnterSubmitConsumerTrace");
                            oResponse = dtCustomVetting.SubmitConsumerTraceVettingQ(EnquiryCon, AdminCon, oSubscriber.SubscriberID, oSystemUser.SystemUserID, 137, oSubscriber.SubscriberName, iDnumber, passportNo, objInput.input.FirstName, objInput.input.Surname, objInput.input.BirthDate,
                                objInput.input.Gender, objInput.input.MaritalStatus, objInput.input.AddressLine1, objInput.input.AddressLine2, objInput.input.Suburb, objInput.input.City, objInput.input.PostalCode, objInput.input.HomeTelephoneCode, objInput.input.HomeTelephoneNumber, objInput.input.WorkTelephoneCode, objInput.input.WorkTelephoneNumber, objInput.input.CellNumber, double.Parse(objInput.input.GrossMonthlyIncome), objInput.input.Occupation, objInput.input.Employer, objInput.input.BankAccountType, objInput.input.BankName,
                                objInput.input.HighestQualification, objInput.input.Branch, objInput.input.BankAccountNumber, int.Parse(objInput.input.NumberOfYearsAtEmployer), string.Empty, true, false, string.Empty);
                            // File.AppendAllText(@"C:\Log\Response.txt", "EndSubmitConsumerTrace");
                            // File.AppendAllText(@"C:\Log\Response.txt", oResponse.ResponseData);
                            switch (oResponse.ResponseStatus)
                            {
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single:
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple:

                                    DataSet dsl = new DataSet();
                                    System.IO.StringReader xmlSR = new System.IO.StringReader(oResponse.ResponseData);
                                    dsl.ReadXml(xmlSR);

                                    //  File.AppendAllText(@"C:\Log\Response.txt", "End xml generate");

                                    if (dsl.Tables[0].Rows.Count == 0 || dsl.Tables[0].Rows.Count == 1)
                                    {
                                        foreach (DataRow dr in dsl.Tables[0].Rows)
                                        {
                                            XDSPortalLibrary.Entity_Layer.MTN.SubmitResponse oinputResponse = new XDSPortalLibrary.Entity_Layer.MTN.SubmitResponse();
                                            // oResponse = dtCustomVetting.SubmitMulipleCreditEnquiry(con, AdminCon, int.Parse(dr["EnquiryID"].ToString()), int.Parse(dr["EnquiryResultID"].ToString()), false, string.Empty, Unique_ref, QueryNumber.ToString());
                                            oinputResponse = oSResponse;


                                            //   File.AppendAllText(@"C:\Log\Response.txt", "EnterSubmitCustomVettingO");
                                            oResponse = dtCustomVetting.SubmitCustomVettingQSOAA(EnquiryCon, AdminCon, int.Parse(dr["EnquiryID"].ToString()), int.Parse(dr["EnquiryResultID"].ToString()), false, string.Empty, oinputResponse, objInput, out oSResponse, ProductIndicator, IsExistingClient);
                                            //  File.AppendAllText(@"C:\Log\Response.txt", "EndSubmitCustomVettingO");
                                        }
                                        switch (oResponse.ResponseStatus)
                                        {
                                            case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                                                oSResponse.SubmitResult.Status = "Failure";
                                                oSResponse.SubmitResult.ErrorDescription = oResponse.ResponseData;
                                                break;
                                            case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                                                oSResponse.SubmitResult.Status = "Success";
                                                oSResponse.SubmitResult.ErrorDescription = string.Empty;
                                                break;
                                        }
                                    }
                                    else
                                    {
                                        oSResponse.SubmitResult.Status = "Failure";
                                        oSResponse.SubmitResult.ErrorDescription = "Multiple Match Found";

                                    }
                                    break;

                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                                    oSResponse.SubmitResult.Status = "Failure";
                                    oSResponse.SubmitResult.ErrorDescription = oResponse.ResponseData;
                                    break;
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                                    oSResponse.SubmitResult.Status = "Failure";
                                    oSResponse.SubmitResult.ErrorDescription = "No Record Found";
                                    break;
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                                    oSResponse.SubmitResult.Status = "Success";
                                    oSResponse.SubmitResult.ErrorDescription = string.Empty;
                                    break;
                            }


                        }
                        catch (Exception e)
                        {
                            oSResponse.SubmitResult.Status = "Failure";
                            oSResponse.SubmitResult.ErrorDescription = e.Message;
                        }

                    }
                    else
                    {
                        oSResponse.SubmitResult.Status = "Failure";
                        oSResponse.SubmitResult.ErrorDescription = ConnectTicket;
                    }
                }

            }
            catch (Exception ex)
            {
                oSResponse.SubmitResult.Status = "Failure";
                oSResponse.SubmitResult.ErrorDescription = ex.Message;
            }

            DateTime dtenddate = DateTime.Now;
            double diff = dtenddate.Subtract(dtStartdate).TotalSeconds;

            oSResponse.SubmitResult.BureauResponse.ProcessingTimeSecs = Math.Round(float.Parse(diff.ToString()), 2);

            if (oSResponse.SubmitResult.Status == "Failure")
            {
                oSResponse.SubmitResult.Outcome = "Decline";
                oSResponse.SubmitResult.BureauResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.MTN.ResponseStatus.Failure; ;
                oSResponse.SubmitResult.Reason = "Decline";
            }
            else if (oSResponse.SubmitResult.Status == "Success")
            {
                oSResponse.SubmitResult.Outcome = "Approve";
                oSResponse.SubmitResult.BureauResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.MTN.ResponseStatus.Success;
                oSResponse.SubmitResult.Reason = "Approve";
            }

            oSResponse.SubmitResult.BureauResponse.UniqueRefGuid = Guid.NewGuid();
            //oSResponse.SubmitResult = oSubmitResponse;
            string response = string.Empty;
            try
            {
                //  File.AppendAllText(@"C:\Log\Response.txt", "Enterdeserialization");
                XmlSerializer serializerout = new XmlSerializer(oSResponse.GetType());
                System.IO.StringWriter swout = new System.IO.StringWriter();
                serializerout.Serialize(swout, oSResponse);
                System.IO.StringReader readerout = new System.IO.StringReader(swout.ToString());

                response = readerout.ReadToEnd();

                SqlConnection con = new SqlConnection(Properties.Settings.Default.EnquiryConnectionString);

                SqlCommand cmd = new SqlCommand("Insert into CustomVettingQResponse (CustomVettingOinputID,Response,CreatedOnDate) values(@ID, @inputXMLstring,getdate())", con);
                cmd.Parameters.AddWithValue("@ID", intCustomVettingOinputID);
                cmd.Parameters.AddWithValue("@inputXMLstring", response);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                con.Close();
            }

            catch (Exception ex)
            {
                // File.AppendAllText(@"C:\Log\Response.txt", "EnterException");
                SqlConnection con = new SqlConnection(Properties.Settings.Default.EnquiryConnectionString);

                SqlCommand cmd = new SqlCommand("Insert into CustomVettingQResponse (CustomVettingOinputID,Response,CreatedOnDate) values(@ID, @inputXMLstring,getdate())", con);
                cmd.Parameters.AddWithValue("@ID", intCustomVettingOinputID);
                cmd.Parameters.AddWithValue("@inputXMLstring", ex.Message);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                con.Close();

            }

            return oSResponse;

        }

        [WebMethod(Description = "Process a Consumer Verification based on ID No and returns Photo and Finger print match output")]
        public string ConnectIDBioMetricVerification(string ConnectTicket, string IdNumber, string FirstName, string Surname, string YourReference, string VoucherCode)
        {
            string rXml = "";
            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();

            if (IsTicketValid(ConnectTicket))
            {
                FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);

                try
                {
                    XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                    XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(AdminCon.ConnectionString, int.Parse(oTicket.Name));

                    XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
                    XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(AdminCon.ConnectionString, oSystemUser.SubscriberID);
                    XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
                    DateTime dDate = DateTime.Now;

                    XDSPortalEnquiry.Business.ConsumerTrace_IdentityVerification dtConsumertrace = new XDSPortalEnquiry.Business.ConsumerTrace_IdentityVerification();
                    rp = dtConsumertrace.SubmitIdentityVerificationBioMetric(EnquiryCon, AdminCon, oSubscriber.SubscriberID, oSystemUser.SystemUserID, 139, oSubscriber.SubscriberName, IdNumber, Surname, FirstName, "", dDate, YourReference, true, true, VoucherCode, strDHAURL);

                    switch (rp.ResponseStatus)
                    {
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single:
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple:
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                            rsXml.LoadXml(rp.ResponseData);
                            break;

                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                            rXml = "<NoResult><Error>" + rp.ResponseData + "</Error></NoResult>";
                            rsXml.LoadXml(rXml);
                            break;
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                            rXml = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                            rsXml.LoadXml(rXml);
                            break;
                    }

                }

                catch (Exception oException)
                {
                    rXml = "<NoResult><Error>" + oException.Message + "</Error></NoResult>";
                    rsXml.LoadXml(rXml);
                }
            }
            else
            {
                rXml = "<NoResult><Error>Invalid Ticket</Error></NoResult>";
                rsXml.LoadXml(rXml);
            }

            return rsXml.OuterXml;

        }

        [WebMethod(Description = "Process a Consumer Verification based on ID No and returns Photo and Finger print match output")]
        public string ConnectIDBioMetricVerificationResult(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string Errors, string Finger1WsqImage, string Finger2WsqImage, bool HasErrors, int Finger1Index, int Finger2Index, string Finger1Name, string Finger2Name, string DeviceSerialNumber, string DeviceFirmwareVersion, string DeviceModel, string WorkStationName, string WorkstationLoggedInUserName, DateTime LastUsedTimeStamp, string Finger1NameString, string Finger2NameString, string BonusXML)
        {
            string rXml = "";
            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();

            if (IsTicketValid(ConnectTicket))
            {
                FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);

                try
                {
                    XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                    XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(AdminCon.ConnectionString, int.Parse(oTicket.Name));

                    XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
                    XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(AdminCon.ConnectionString, oSystemUser.SubscriberID);
                    XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();


                    XDSPortalEnquiry.Data.SubscriberEnquiry odSubscriberEnquiry = new XDSPortalEnquiry.Data.SubscriberEnquiry();
                    XDSPortalEnquiry.Entity.SubscriberEnquiry oSubscriberEnquiry = odSubscriberEnquiry.GetSubscriberEnquiryObject(EnquiryCon, EnquiryID);


                    XDSPortalEnquiry.Data.SubscriberEnquiryResult odSubscriberEnquiryResult = new XDSPortalEnquiry.Data.SubscriberEnquiryResult();
                    XDSPortalEnquiry.Entity.SubscriberEnquiryResult oSubscriberEnquiryResult = odSubscriberEnquiryResult.GetSubscriberEnquiryResultObject(EnquiryCon, EnquiryResultID);

                    XDSPortalEnquiry.Data.SubscriberEnquiryResultBonus odSubscriberEnquiryResultBonus = new XDSPortalEnquiry.Data.SubscriberEnquiryResultBonus();


                    DataSet ds = new DataSet();

                    DataSet dsDataSegments = odSubscriberEnquiryResultBonus.GetSubscriberEnquiryResultBonusDataSet(EnquiryCon, EnquiryResultID);

                    if (!string.IsNullOrEmpty(BonusXML))
                    {
                        try
                        {
                            System.IO.StringReader xmlSR = new System.IO.StringReader(BonusXML);
                            ds.ReadXml(xmlSR);
                            if (ds.Tables.Count < 1)
                            {
                                ds = null;
                            }
                            else if (ds.Tables[0].Rows.Count < 1)
                            {
                                ds = null;
                            }
                            else
                            {

                                if (ds.Tables[0].Columns.Contains("DataSegmentID") && ds.Tables[0].Columns.Contains("BonusViewed"))
                                {
                                    foreach (DataRow dr in ds.Tables[0].Rows)
                                    {
                                        if (dr["BonusViewed"].ToString().ToLower() == "true")
                                        {
                                            foreach (DataRow r in dsDataSegments.Tables[0].Rows)
                                            {
                                                if (dr["DataSegmentID"].ToString() == r["DataSegmentID"].ToString())
                                                {
                                                    dr["BonusPrice"] = r["BillingPrice"].ToString();
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    rXml = "<Error>" + "Invalid Trace Plus XML Supplied" + "</Error>";
                                    rsXml.LoadXml(rXml);
                                    return rsXml.OuterXml;

                                }
                            }


                        }
                        catch
                        {
                            rXml = "<Error>" + "Invalid Trace Plus XML Supplied" + "</Error>";
                            rsXml.LoadXml(rXml);
                            return rsXml.OuterXml;

                        }
                    }
                    else
                    {
                        ds = null;
                    }
                    dsDataSegments.Clear();
                    dsDataSegments = ds;

                    switch (oSubscriberEnquiry.ProductID)
                    {
                        case 139:
                            XDSPortalEnquiry.Business.MultipleTrace_IdentityVerification dtMultipleIDPhoto = new XDSPortalEnquiry.Business.MultipleTrace_IdentityVerification();
                            rp = dtMultipleIDPhoto.SubmitMultipleBIoMetricVerification(EnquiryCon, AdminCon, XDSLogCon, oSubscriberEnquiryResult, oSubscriberEnquiry,
                                dsDataSegments, true, oSubscriberEnquiryResult.VoucherCode,
                                strVendorID, Errors, Finger1WsqImage, Finger2WsqImage, HasErrors, Finger1Index, Finger2Index, Finger1Name, Finger2Name, DeviceSerialNumber, DeviceFirmwareVersion, DeviceModel, WorkStationName, WorkstationLoggedInUserName, LastUsedTimeStamp, Finger1NameString, Finger2NameString, strIvsURL, strHanisURL1, strHanisURL2, strHanissiteID, strHanisWorkStationID, strHanisType, strJ2KURL,DIABiometricuRL,DIABiometricUsername,DIABiometricPassword);
                            rsXml.LoadXml(rp.ResponseData);
                            break;
                        default:
                            break;
                    }


                    switch (rp.ResponseStatus)
                    {
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single:
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple:
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                            rsXml.LoadXml(rp.ResponseData);
                            break;

                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:

                            if (oSubscriberEnquiry.ProductID == 139)
                            {
                                if (Uri.IsWellFormedUriString(strDIAURL, UriKind.Absolute))
                                {
                                    XDSPortalEnquiry.Business.MultipleTrace_IdentityVerification dtMultipleIDPhoto = new XDSPortalEnquiry.Business.MultipleTrace_IdentityVerification();
                                    rp = dtMultipleIDPhoto.SubmitMultipleDIABIoMetricVerification(EnquiryCon, AdminCon, XDSLogCon,
                                        dsDataSegments, true, oSubscriberEnquiryResult.VoucherCode,
                                        Errors, Finger1WsqImage, Finger2WsqImage, HasErrors, Finger1Index, Finger2Index, Finger1Name, Finger2Name, DeviceSerialNumber, DeviceFirmwareVersion, DeviceModel, WorkStationName, WorkstationLoggedInUserName, LastUsedTimeStamp, Finger1NameString, Finger2NameString, strDIAURL, 148, oSubscriberEnquiry.SubscriberEnquiryID, oSubscriberEnquiryResult.SubscriberEnquiryResultID);
                                }
                            }
                            else
                            {
                                rXml = "<NoResult><Error>" + rp.ResponseData + "</Error></NoResult>";
                                rsXml.LoadXml(rXml);
                            }
                            break;
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                            rXml = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                            rsXml.LoadXml(rXml);
                            break;
                    }

                }

                catch (Exception oException)
                {
                    rXml = "<NoResult><Error>" + oException.Message + "</Error></NoResult>";
                    rsXml.LoadXml(rXml);
                }
            }
            else
            {
                rXml = "<NoResult><Error>Invalid Ticket</Error></NoResult>";
                rsXml.LoadXml(rXml);
            }

            string finalXML = rsXml.OuterXml;

            if (finalXML.Contains("TmStamp"))
            {
                XmlNode node = rsXml.SelectSingleNode("HomeAffairs/BioMetricVerificationResult/TmStamp");
                node.LastChild.Value = node.LastChild.Value.Replace("-", "/");
            }

            return rsXml.OuterXml;
        }

        [WebMethod(Description = "Refer an ID no to fraud for a specific Product")]
        public string ConnectIDBioMetricVerificationReferToFraud(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string comments, string ExternalRef)
        {
            string rXml = "";
            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();

            if (IsTicketValid(ConnectTicket))
            {
                FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);

                try
                {
                    XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                    XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(AdminCon.ConnectionString, int.Parse(oTicket.Name));

                    XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
                    XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(AdminCon.ConnectionString, oSystemUser.SubscriberID);
                    XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();


                    XDSPortalEnquiry.Data.SubscriberEnquiry odSubscriberEnquiry = new XDSPortalEnquiry.Data.SubscriberEnquiry();
                    XDSPortalEnquiry.Entity.SubscriberEnquiry oSubscriberEnquiry = odSubscriberEnquiry.GetSubscriberEnquiryObject(EnquiryCon, EnquiryID);


                    XDSPortalEnquiry.Data.SubscriberEnquiryResult odSubscriberEnquiryResult = new XDSPortalEnquiry.Data.SubscriberEnquiryResult();
                    XDSPortalEnquiry.Entity.SubscriberEnquiryResult oSubscriberEnquiryResult = odSubscriberEnquiryResult.GetSubscriberEnquiryResultObject(EnquiryCon, EnquiryResultID);

                    if (oSubscriberEnquiryResult.EnquiryResult.ToUpper() != "R")
                    {
                        throw new Exception("Please view the report before refering to fraud");
                    }


                    switch (ProductID)
                    {
                        case 139:
                            XDSPortalEnquiry.Business.MultipleTrace_IdentityVerification dtMultipleIDPhoto = new XDSPortalEnquiry.Business.MultipleTrace_IdentityVerification();
                            rp = dtMultipleIDPhoto.UpdateReferToFraud(EnquiryCon, AdminCon, XDSLogCon, EnquiryID, EnquiryResultID, comments, ExternalRef);
                            break;
                        default:
                            break;
                    }


                    switch (rp.ResponseStatus)
                    {
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single:
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple:
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                            rsXml.LoadXml(rp.ResponseData);
                            break;

                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                            rXml = "<NoResult><Error>" + rp.ResponseData + "</Error></NoResult>";
                            rsXml.LoadXml(rXml);
                            break;
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                            rXml = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                            rsXml.LoadXml(rXml);
                            break;
                    }

                }

                catch (Exception oException)
                {
                    rXml = "<NoResult><Error>" + oException.Message + "</Error></NoResult>";
                    rsXml.LoadXml(rXml);
                }
            }
            else
            {
                rXml = "<NoResult><Error>Invalid Ticket</Error></NoResult>";
                rsXml.LoadXml(rXml);
            }

            return rsXml.OuterXml;

        }



    }
}