﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using XDSPortalEnquiry;

namespace XDSConnect
{
    public class LoginUser
    {

        public enum LoginUserStatus
            {
                Authenticated = 1,
                NotAuthenticated = 2,
                UserDeactivated = 3,
                SubscriberDeactivated = 4,
                UserNotFound = 5,
                IPNotAllowed=6
            }

        XDSPortalEnquiry.Entity.SystemUser _LoginSystemUser;
        LoginUserStatus _LoginStatus = LoginUserStatus.NotAuthenticated;
        string _LoginTicket;

        public XDSPortalEnquiry.Entity.SystemUser LoginSystemUser
        {
            get
            {
                return _LoginSystemUser;
            }
            set
            {
                this._LoginSystemUser = value;
            }
        }

        public LoginUserStatus LoginStatus
        {
            get
            {
                return _LoginStatus;
            }
            set
            {
                this._LoginStatus = value;
            }
        }

        public string LoginTicket
        {
            get
            {
                return this._LoginTicket;
            }
            set
            {
                this._LoginTicket = value;
            }
        }

    }

    

  

}
