﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Threading;
using System.Security.Principal;
using System.Web.Security;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Data;
using System.Net.Mail;
using System.Configuration;
using System.Net;
using System.Text;
using System.Reflection;
using XDSPortalAuthentication;
using DevExpress.XtraReports.UI;
using XdsPortalReports;
using XDSPortalEnquiry.Business;
//using XDSPortalLibrary.Entity_Layer;

namespace XDSConnect
{
    /// <summary>
    /// Summary description for MTNConnect
    /// </summary>
    [WebService(Namespace = "http://www.web.xds.co.za/XDSConnectWS")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class ADPConnect : System.Web.Services.WebService
    {
        private SqlConnection EnquiryCon = new SqlConnection(Properties.Settings.Default.EnquiryConnectionString);
        private SqlConnection AdminCon = new SqlConnection(Properties.Settings.Default.AdminConnectionString);
        private SqlConnection XDSLogCon = new SqlConnection(Properties.Settings.Default.XDSLog);
        private SqlConnection MTNCommercialQuestionnaireCon = new SqlConnection(Properties.Settings.Default.MTNCommercialQuestionnaire);
        private string strSmtpServerName = ConfigurationManager.AppSettings["XDSSmtp"].ToString();
        private string strSmtpUserLoginID = ConfigurationManager.AppSettings["XDSSmtpUserLoginID"].ToString();
        private string strSmtpUserPassword = ConfigurationManager.AppSettings["XDSSmtpUserPassword"].ToString();
        private int intport = Convert.ToInt16(ConfigurationManager.AppSettings["XDSSmtpPort"].ToString());
        private string strVendorID = ConfigurationManager.AppSettings["HansVendorID"].ToString();
        private string strIvsURL = ConfigurationManager.AppSettings["IvsURL"].ToString();
        private string strHanisURL1 = ConfigurationManager.AppSettings["HanisURL1"].ToString();
        private string strHanisURL2 = ConfigurationManager.AppSettings["HanisURL2"].ToString();
        private string strDIAURL = ConfigurationManager.AppSettings["DIAURL"].ToString();
        private string strHanissiteID = ConfigurationManager.AppSettings["HanisSiteID"].ToString();
        private string strHanisWorkStationID = ConfigurationManager.AppSettings["HanisWorkStationID"].ToString();
        private string strHanisType = ConfigurationManager.AppSettings["HanisType"].ToString();
        private string strJ2KURL = ConfigurationManager.AppSettings["J2KURL"].ToString();

        [WebMethod(Description = "Log in XDS Connect Web Service and returns a Ticket")]
        public string Login(string strUser, string strPwd)
        {
            LoginUser oLoginUser = AuthenticateUser(strUser, strPwd);

            if (oLoginUser.LoginStatus == LoginUser.LoginUserStatus.Authenticated)
            {

                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                   1,                            // version
                   oLoginUser.LoginSystemUser.SystemUserID.ToString(),                      // user name
                   DateTime.Now,                 // create time
                   DateTime.Now.AddHours(5),  // expire time
                   false,                        // persistent
                   oLoginUser.LoginSystemUser.SubscriberID.ToString());              // user data

                oLoginUser.LoginTicket = FormsAuthentication.Encrypt(ticket);
                //HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, oLoginUser.LoginTicket);
                //Context.Response.Cookies.Add(cookie);
                return oLoginUser.LoginTicket;
            }
            else
                return oLoginUser.LoginStatus.ToString();
        }

        [WebMethod(Description = "Checks Validity of your XDS Connect Ticket")]
        public bool IsTicketValid(string XDSConnectTicket)
        {
            try
            {
                FormsAuthenticationTicket a = FormsAuthentication.Decrypt(XDSConnectTicket);

                bool isValid = true;
                if (a.Expired) isValid = false;
                return isValid;
            }
            catch
            {
                return false;
            }
        }

        private LoginUser AuthenticateUser(string strUser, string strPwd)
        {
            LoginUser oLoginUser = new LoginUser();

            AdminCon.Open();

            if (Membership.ValidateUser(strUser, strPwd))
            {
                XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUser(AdminCon, strUser);

                if (oSystemUser != null)
                {
                    XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
                    XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(AdminCon, oSystemUser.SubscriberID);

                    oLoginUser.LoginStatus = LoginUser.LoginUserStatus.Authenticated;
                    oLoginUser.LoginSystemUser = oSystemUser;

                    if (!oSystemUser.ActiveYN) oLoginUser.LoginStatus = LoginUser.LoginUserStatus.UserDeactivated;
                    if (oSubscriber.StatusInd != "A") oLoginUser.LoginStatus = LoginUser.LoginUserStatus.SubscriberDeactivated;

                }
                else
                {
                    oLoginUser.LoginStatus = LoginUser.LoginUserStatus.UserNotFound;
                }

            }
            else
            {
                oLoginUser.LoginStatus = LoginUser.LoginUserStatus.NotAuthenticated;
            }
            AdminCon.Close();
            return oLoginUser;
            //throw new NotImplementedException();
        }

        

        [WebMethod(Description = "")]
        public XDSADPMODELS.MTNCommercialFinalOutput ConnectCustomVettingSResultA(int ProductCode, string User, string Password, string BureauNumber, string EnquiryReason, string EnquiryAmount, string Terms, string UserOnTicket, string ContactForename, string ContactSurname, string ContactPhoneCode, string ContactPhoneNo, string ContactFaxCode, string ContactFaxNo, string AdditionalClientReference, string SubjectPhoneCode, string SubjectPhoneNo, string SubjectNameOnTicket, string SubjectAddress, string SubjectSuburb, string SubjectCity, string SubjectPostCode, string InvestigateOption, string BankAccountNo, string BankAbbreviation, string BankBranch, string BankBranchCode, string SpecialInstructions, string BankCreditAmount, string BankTermsGiven, string BankAccountHolder, string IsExistingClient)
        {
            User = string.IsNullOrEmpty(User) ? string.Empty : User;
            Password = string.IsNullOrEmpty(Password) ? string.Empty : Password;
            BureauNumber = string.IsNullOrEmpty(BureauNumber) ? string.Empty : BureauNumber;
            EnquiryReason = string.IsNullOrEmpty(EnquiryReason) ? string.Empty : EnquiryReason;
            EnquiryAmount = string.IsNullOrEmpty(EnquiryAmount) ? string.Empty : EnquiryAmount;
            Terms = string.IsNullOrEmpty(Terms) ? string.Empty : Terms;
            UserOnTicket = string.IsNullOrEmpty(UserOnTicket) ? string.Empty : UserOnTicket;
            ContactForename = string.IsNullOrEmpty(ContactForename) ? string.Empty : ContactForename;
            ContactSurname = string.IsNullOrEmpty(ContactSurname) ? string.Empty : ContactSurname;
            ContactPhoneCode = string.IsNullOrEmpty(ContactPhoneCode) ? string.Empty : ContactPhoneCode;
            ContactPhoneNo = string.IsNullOrEmpty(ContactPhoneNo) ? string.Empty : ContactPhoneNo;
            ContactFaxCode = string.IsNullOrEmpty(ContactFaxCode) ? string.Empty : ContactFaxCode;
            ContactFaxNo = string.IsNullOrEmpty(ContactFaxNo) ? string.Empty : ContactFaxNo;
            AdditionalClientReference = string.IsNullOrEmpty(AdditionalClientReference) ? string.Empty : AdditionalClientReference;
            SubjectPhoneCode = string.IsNullOrEmpty(SubjectPhoneCode) ? string.Empty : SubjectPhoneCode;
            SubjectPhoneNo = string.IsNullOrEmpty(SubjectPhoneNo) ? string.Empty : SubjectPhoneNo;
            SubjectNameOnTicket = string.IsNullOrEmpty(SubjectNameOnTicket) ? string.Empty : SubjectNameOnTicket;
            SubjectAddress = string.IsNullOrEmpty(SubjectAddress) ? string.Empty : SubjectAddress;
            SubjectSuburb = string.IsNullOrEmpty(SubjectSuburb) ? string.Empty : SubjectSuburb;
            SubjectCity = string.IsNullOrEmpty(SubjectCity) ? string.Empty : SubjectCity;
            SubjectPostCode = string.IsNullOrEmpty(SubjectPostCode) ? string.Empty : SubjectPostCode;
            InvestigateOption = string.IsNullOrEmpty(InvestigateOption) ? string.Empty : InvestigateOption;
            BankAccountNo = string.IsNullOrEmpty(BankAccountNo) ? string.Empty : BankAccountNo;
            BankAbbreviation = string.IsNullOrEmpty(BankAbbreviation) ? string.Empty : BankAbbreviation;
            BankBranch = string.IsNullOrEmpty(BankBranch) ? string.Empty : BankBranch;
            BankBranchCode = string.IsNullOrEmpty(BankBranchCode) ? string.Empty : BankBranchCode;
            SpecialInstructions = string.IsNullOrEmpty(SpecialInstructions) ? string.Empty : SpecialInstructions;
            BankCreditAmount = string.IsNullOrEmpty(BankCreditAmount) ? string.Empty : BankCreditAmount;
            BankTermsGiven = string.IsNullOrEmpty(BankTermsGiven) ? string.Empty : BankTermsGiven;
            BankAccountHolder = string.IsNullOrEmpty(BankAccountHolder) ? string.Empty : BankAccountHolder;
            IsExistingClient = string.IsNullOrEmpty(IsExistingClient) ? string.Empty : IsExistingClient;

            DateTime dtStartdate = DateTime.Now;
            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();
            string rXml = string.Empty, ConnectTicket = string.Empty;

            XDSADPMODELS.MTNCommercialFinalOutput oBResponse = new XDSADPMODELS.MTNCommercialFinalOutput();

            oBResponse.BureauData = new XDSADPMODELS.CommercialResponse();
            oBResponse.BureauData.processingStartDate = DateTime.Now;

            XmlSerializer serializer = new XmlSerializer(typeof(XDSPortalLibrary.Entity_Layer.ModuleRequest));
            XDSADPMODELS.ModuleRequest objInput = new XDSADPMODELS.ModuleRequest();

            int intCustomVettingRinputID = 0;
            int EnquiryID = 0, EnquiryResultID = 0;

            try
            {
                //switch (productID)
                //{
                //    case 138:
                SqlConnection con = new SqlConnection(EnquiryCon.ConnectionString);

                SqlCommand cmd = new SqlCommand("Insert into CustomVettingSReportAinput (Input,CreatedOnDate) values(@inputXMLstring,getdate())", con);

                cmd.Parameters.AddWithValue("@inputXMLstring", ("User:" + User + ";Password:" + Password + ";ITNumber:" + BureauNumber + ";ProductCode:" + ProductCode.ToString() + ";EnquiryReason:" + EnquiryReason + ";EnquiryAmount:" + EnquiryAmount + ";Terms:" + Terms + ";UserOnTicket:" + UserOnTicket + ";ContactForename:" + ContactForename + ";ContactSurname:" + ContactSurname + ";ContactPhoneCode:" + ContactPhoneCode + ";ContactPhoneNo:" + ContactPhoneNo + ";ContactFaxCode:" + ContactFaxCode + ";ContactFaxNo:" + ContactFaxNo + ";AdditionalClientReference:" + AdditionalClientReference + ";SubjectPhoneCode:" + SubjectPhoneCode + ";SubjectPhoneNo:" + SubjectPhoneNo + ";SubjectNameOnTicket:" + SubjectNameOnTicket + ";SubjectAddress:" + SubjectAddress + ";SubjectSuburb:" + SubjectSuburb + ";SubjectCity:" + SubjectCity + ";SubjectPostCode:" + SubjectPostCode + ";InvestigateOption:" + InvestigateOption + ";BankAccountNo:" + BankAccountNo + ";BankAbbreviation:" + BankAbbreviation + ";BankBranch:" + BankBranch + ";BankBranchCode:" + BankBranchCode + ";SpecialInstructions:" + SpecialInstructions + ";BankCreditAmount:" + BankCreditAmount + ";BankTermsGiven:" + BankTermsGiven + ";BankAccountHolder:" + BankAccountHolder + ";IsExistingClient:" + IsExistingClient));

                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                string strSQL = "SELECT @@IDENTITY";
                SqlCommand Imycommand = new SqlCommand(strSQL, con);
                SqlDataReader Idr = Imycommand.ExecuteReader();
                Idr.Read();
                intCustomVettingRinputID = Convert.ToInt32(Idr.GetValue(0));
                Idr.Close();

                con.Close();

                //parse XML

                //xml = xml.Replace("&", string.Empty);

                //objInput = (XDSPortalLibrary.Entity_Layer.ModuleRequest)serializer.Deserialize(new XmlTextReader(new StringReader(xml)));

                objInput.ModuleRequest1 = new XDSADPMODELS.ModuleRequestModuleRequest();

                objInput.ModuleRequest1.AdditionalClientReference = AdditionalClientReference;
                objInput.ModuleRequest1.BankAbbreviation = BankAbbreviation;
                objInput.ModuleRequest1.BankAccountHolder = BankAccountHolder;
                objInput.ModuleRequest1.BankAccountNo = BankAccountNo;
                objInput.ModuleRequest1.BankBranch = BankBranch;
                objInput.ModuleRequest1.BankBranchCode = BankBranchCode;
                objInput.ModuleRequest1.BankCreditAmount = BankCreditAmount;
                objInput.ModuleRequest1.BankTermsGiven = BankTermsGiven;
                objInput.ModuleRequest1.ContactFaxCode = ContactFaxCode;
                objInput.ModuleRequest1.ContactFaxNo = ContactFaxNo;
                objInput.ModuleRequest1.ContactForename = ContactForename;
                objInput.ModuleRequest1.ContactPhoneCode = ContactPhoneCode;
                objInput.ModuleRequest1.ContactPhoneNo = ContactPhoneNo;
                objInput.ModuleRequest1.ContactSurname = ContactSurname;
                objInput.ModuleRequest1.EnquiryAmount = EnquiryAmount;
                objInput.ModuleRequest1.BureauNumber = BureauNumber;
                objInput.ModuleRequest1.EnquiryReason = EnquiryReason;
                //objInput.ModuleRequest1.EnquiryResultID = EnquiryResultID;
                objInput.ModuleRequest1.InvestigateOption = InvestigateOption;
                // objInput.ModuleRequest1.ModuleProductCodes = moduleProductCodes;
                objInput.ModuleRequest1.Password = Password;
                objInput.ModuleRequest1.SpecialInstructions = SpecialInstructions;
                objInput.ModuleRequest1.SubjectAddress = SubjectAddress;
                objInput.ModuleRequest1.SubjectCity = SubjectCity;
                objInput.ModuleRequest1.SubjectNameOnTicket = SubjectNameOnTicket;
                objInput.ModuleRequest1.SubjectPhoneCode = SubjectPhoneCode;
                objInput.ModuleRequest1.SubjectPhoneNo = SubjectPhoneNo;
                objInput.ModuleRequest1.SubjectPostCode = SubjectPostCode;
                objInput.ModuleRequest1.SubjectSuburb = SubjectSuburb;
                objInput.ModuleRequest1.Terms = Terms;
                objInput.ModuleRequest1.User = User;
                objInput.ModuleRequest1.UserOnTicket = UserOnTicket;
                objInput.ModuleRequest1.IsExistingClient = IsExistingClient;

                XDSPortalLibrary.Entity_Layer.Response oResponse = new XDSPortalLibrary.Entity_Layer.Response();
                MultipleCommercial_BusinessEnquiry oCommercial = new MultipleCommercial_BusinessEnquiry();
                oResponse = oCommercial.ValidateModuleRequestA(objInput);

                EnquiryID = int.Parse(objInput.ModuleRequest1.BureauNumber.Substring(0, objInput.ModuleRequest1.BureauNumber.IndexOf(":")));
                EnquiryResultID = int.Parse(objInput.ModuleRequest1.BureauNumber.Substring(objInput.ModuleRequest1.BureauNumber.IndexOf(":") + 1));
                oBResponse.BureauData.uniqueRefGuid = (Guid.NewGuid()).ToString();
                oBResponse.ExistingClient = IsExistingClient;
                oBResponse.AccountNumber = BankAccountNo;

                if (oResponse.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                {
                    oBResponse.BureauData.responseStatus = XDSADPMODELS.ResponseStatus.Failure;
                    oBResponse.BureauData.errorMessage = oResponse.ResponseData;

                    oBResponse.Status = XDSADPMODELS.ResponseStatus.Failure.ToString();
                    oBResponse.ErrorDescription = oResponse.ResponseData;

                }
                else if (oResponse.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                {
                    ConnectTicket = string.Empty;

                    ConnectTicket = Login(objInput.ModuleRequest1.User, objInput.ModuleRequest1.Password);

                    // File.AppendAllText(@"C:\Log\mtnvet.txt", ConnectTicket);

                    if (IsTicketValid(ConnectTicket))
                    {
                        FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);

                        try
                        {
                            XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                            XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(Properties.Settings.Default.AdminConnectionString, int.Parse(oTicket.Name));

                            XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
                            XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(Properties.Settings.Default.AdminConnectionString, oSystemUser.SubscriberID);
                            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

                            XDSPortalEnquiry.Data.SubscriberEnquiry odSubscriberEnquiry = new XDSPortalEnquiry.Data.SubscriberEnquiry();
                            XDSPortalEnquiry.Entity.SubscriberEnquiry oSubscriberEnquiry = odSubscriberEnquiry.GetSubscriberEnquiryObject(EnquiryCon, EnquiryID);

                            XDSPortalEnquiry.Data.SubscriberEnquiryResult odSubscriberEnquiryResult = new XDSPortalEnquiry.Data.SubscriberEnquiryResult();
                            XDSPortalEnquiry.Entity.SubscriberEnquiryResult oSubscriberEnquiryResult = odSubscriberEnquiryResult.GetSubscriberEnquiryResultObject(EnquiryCon, EnquiryResultID);

                            XDSPortalEnquiry.Data.SubscriberEnquiryResultBonus odSubscriberEnquiryResultBonus = new XDSPortalEnquiry.Data.SubscriberEnquiryResultBonus();

                            XDSPortalEnquiry.Business.MultipleCommercial_BusinessEnquiry dtCommercial = new XDSPortalEnquiry.Business.MultipleCommercial_BusinessEnquiry();

                            //  string IsExistingClient = string.Empty;
                            // File.AppendAllText(@"C:\Log\mtnvet.txt", "stepx");

                            switch (oSubscriberEnquiry.ProductID)
                            {
                                case 138:
                                    XDSADPMODELS.MTNCommercialFinalOutput oinputResponse = new XDSADPMODELS.MTNCommercialFinalOutput();
                                    oinputResponse = oBResponse;
                                    // File.AppendAllText(@"C:\Log\mtnvet.txt", "step1");
                                    oResponse = dtCommercial.SubmitCustomVettingRA(EnquiryCon, AdminCon, EnquiryID, EnquiryResultID, 138, null, false, string.Empty, oinputResponse, objInput, out oBResponse, ProductCode, IsExistingClient, dtStartdate);
                                    //File.AppendAllText(@"C:\Log\mtnvet1.txt","ws"+ oBResponse.FinalPrincipalScore.ToString());

                                    //File.AppendAllText(@"C:\Log\mtnvet.txt", "step2");
                                    break;
                                default:
                                    break;
                            }

                          


                            switch (oResponse.ResponseStatus)
                            {
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                                    oBResponse.BureauData.responseStatus = XDSADPMODELS.ResponseStatus.Failure;
                                    oBResponse.BureauData.errorMessage = oResponse.ResponseData;

                                    oBResponse.Status = XDSADPMODELS.ResponseStatus.Failure.ToString();
                                    oBResponse.ErrorDescription = oResponse.ResponseData;
                                    break;
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                                    oBResponse.BureauData.responseStatus = XDSADPMODELS.ResponseStatus.Failure;
                                    oBResponse.BureauData.errorMessage = "No Record Found";

                                    oBResponse.Status = XDSADPMODELS.ResponseStatus.Failure.ToString();
                                    oBResponse.ErrorDescription = "No Record Found";
                                    break;
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                                    oBResponse.BureauData.responseStatus = XDSADPMODELS.ResponseStatus.Success;

                                    oBResponse.Status = XDSADPMODELS.ResponseStatus.Success.ToString();
                                    break;
                            }

                        }
                        catch (Exception e)
                        {
                            oBResponse.BureauData.responseStatus = XDSADPMODELS.ResponseStatus.Failure;
                            oBResponse.BureauData.errorMessage = e.Message;

                            oBResponse.Status = XDSADPMODELS.ResponseStatus.Failure.ToString();
                            oBResponse.ErrorDescription = e.Message;
                        }
                    }
                    else
                    {
                        oBResponse.BureauData.responseStatus = XDSADPMODELS.ResponseStatus.Failure;
                        oBResponse.BureauData.errorMessage = ConnectTicket;

                        oBResponse.Status = XDSADPMODELS.ResponseStatus.Failure.ToString();
                        oBResponse.ErrorDescription = ConnectTicket;
                    }
                }

                //        break;
                //    default:
                //        oBResponse.ModuleRequestResult.ResponseStatus = XDSPortalLibrary.Entity_Layer.ModuleRequestResponseModuleRequestResult.enumRResultResponseStatus.FAILURE.ToString();
                //        oBResponse.ModuleRequestResult.ErrorMessage = "Invalid Product Supplied";
                //        break;
                //}

            }
            catch (Exception ex)
            {
                oBResponse.BureauData.responseStatus = XDSADPMODELS.ResponseStatus.Failure;
                oBResponse.BureauData.errorMessage = ex.Message;

                oBResponse.Status = XDSADPMODELS.ResponseStatus.Failure.ToString();
                oBResponse.ErrorDescription = ex.Message;
            }

            string response = string.Empty;

            try
            {
                //  File.AppendAllText(@"C:\Log\Response.txt", "Enterdeserialization");
                XmlSerializer serializerout = new XmlSerializer(oBResponse.GetType());
                System.IO.StringWriter swout = new System.IO.StringWriter();
                serializerout.Serialize(swout, oBResponse);
                System.IO.StringReader readerout = new System.IO.StringReader(swout.ToString());

                response = readerout.ReadToEnd();

                //File.AppendAllText(@"C:\Log\mtnvet1.txt", "ws" + oBResponse.FinalPrincipalScore.ToString());
                //File.AppendAllText(@"C:\Log\mtnvet1.txt", "response" + response);

                SqlConnection con = new SqlConnection(Properties.Settings.Default.EnquiryConnectionString);

                SqlCommand cmd = new SqlCommand("Insert into CustomVettingSReportAResponse (CustomVettingSReportAinputID,Response,CreatedOnDate) values(@ID, @inputXMLstring,getdate())", con);
                cmd.Parameters.AddWithValue("@ID", intCustomVettingRinputID);
                cmd.Parameters.AddWithValue("@inputXMLstring", response);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                con.Close();
            }

            catch (Exception ex)
            {
                // File.AppendAllText(@"C:\Log\Response.txt", "EnterException");
                SqlConnection con = new SqlConnection(Properties.Settings.Default.EnquiryConnectionString);

                SqlCommand cmd = new SqlCommand("Insert into CustomVettingRReportResponse (CustomVettingOinputID,Response,CreatedOnDate) values(@ID, @inputXMLstring,getdate())", con);
                cmd.Parameters.AddWithValue("@ID", intCustomVettingRinputID);
                cmd.Parameters.AddWithValue("@inputXMLstring", ex.Message);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                con.Close();

            }
            File.AppendAllText(@"C:\Log\R3MTN.txt", response);
            return oBResponse;
        }

        [WebMethod(Description = "Vetting match  for non registered enterprises")]
        public XDSADPMODELS.MTNCommercialFinalOutput ConnectCustomVettingV(XDSPortalLibrary.Entity.VettingVRequest VettingRequestDetails)
        {
            DateTime dtStartdate = DateTime.Now;
            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();
            string rXml = string.Empty, ConnectTicket = string.Empty;

            XDSADPMODELS.MTNCommercialFinalOutput oBResponse = new XDSADPMODELS.MTNCommercialFinalOutput();
            oBResponse.BureauData = new XDSADPMODELS.CommercialResponse();
            oBResponse.BureauData.processingStartDate = DateTime.Now;


            int intCustomVettingNRinputID = 0;
            int EnquiryID = 0;
            int EnquiryResultID = 0;

            try
            {
                SqlConnection con = new SqlConnection(EnquiryCon.ConnectionString);

                XmlSerializer serializerinput = new XmlSerializer(VettingRequestDetails.GetType());
                System.IO.StringWriter swinput = new System.IO.StringWriter();
                serializerinput.Serialize(swinput, VettingRequestDetails);
                System.IO.StringReader readerin = new System.IO.StringReader(swinput.ToString());

                string strinput = readerin.ReadToEnd();


                SqlCommand cmd = new SqlCommand("Insert into CustomVettingSSolePropinput (Input,CreatedOnDate) values(@inputXMLstring,getdate())", con);

                cmd.Parameters.AddWithValue("@inputXMLstring", strinput);

                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                string strSQL = "SELECT @@IDENTITY";
                SqlCommand Imycommand = new SqlCommand(strSQL, con);
                SqlDataReader Idr = Imycommand.ExecuteReader();
                Idr.Read();
                intCustomVettingNRinputID = Convert.ToInt32(Idr.GetValue(0));
                Idr.Close();

                con.Close();


                XDSPortalLibrary.Entity_Layer.Response oResponse = new XDSPortalLibrary.Entity_Layer.Response();
                CommercialEnquiry_BusinessEnquiry oCommercial = new CommercialEnquiry_BusinessEnquiry();

                oResponse = oCommercial.ValidateModuleRequestNR(VettingRequestDetails);

                oBResponse.BureauData.uniqueRefGuid = (Guid.NewGuid()).ToString();

                if (oResponse.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                {
                    oBResponse.BureauData.responseStatus = XDSADPMODELS.ResponseStatus.Failure;
                    oBResponse.BureauData.errorMessage = oResponse.ResponseData;

                    oBResponse.Status = XDSPortalLibrary.Entity_Layer.MTNSOA.ResponseStatus.Failure.ToString();
                    oBResponse.ErrorDescription = oResponse.ResponseData;

                }
                else if (oResponse.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                {
                    ConnectTicket = string.Empty;

                    ConnectTicket = Login(VettingRequestDetails.User, VettingRequestDetails.Password);

                    if (IsTicketValid(ConnectTicket))
                    {

                        try
                        {
                            FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);

                            XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                            XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(AdminCon, int.Parse(oTicket.Name));

                            XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
                            XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(AdminCon, oSystemUser.SubscriberID);

                            XDSPortalEnquiry.Business.CustomVetting dtCustomVetting = new XDSPortalEnquiry.Business.CustomVetting();

                            oResponse = dtCustomVetting.SubmitConsumerTraceVettingV(EnquiryCon, AdminCon, oSystemUser.SubscriberID, oSystemUser.SystemUserID, 180, oSubscriber.SubscriberName, VettingRequestDetails.PrincipalDetails, string.Empty, false, false, string.Empty);

                            switch (oResponse.ResponseStatus)
                            {
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single:
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple:
                                    XDSPortalEnquiry.Business.MultipleCommercial_BusinessEnquiry dtCommercial = new XDSPortalEnquiry.Business.MultipleCommercial_BusinessEnquiry();
                                    XDSADPMODELS.MTNCommercialFinalOutput oinputResponse = new XDSADPMODELS.MTNCommercialFinalOutput();
                                    oinputResponse = oBResponse;

                                    oResponse = dtCommercial.SubmitCustomVettingV(EnquiryCon, AdminCon, oResponse.EnquiryID, oResponse.EnquiryResultID, 180, null, false, string.Empty, oinputResponse, VettingRequestDetails, out oBResponse, dtStartdate, oResponse.ResponseData);

                                    switch (oResponse.ResponseStatus)
                                    {
                                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                                            oBResponse.BureauData.responseStatus = XDSADPMODELS.ResponseStatus.Failure;
                                            oBResponse.ErrorDescription = oResponse.ResponseData;
                                            break;
                                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                                            oBResponse.Status = XDSPortalLibrary.Entity_Layer.MTNSOA.ResponseStatus.Success.ToString();
                                            //rsXml.LoadXml(rp.ResponseData);
                                            break;
                                    }

                                    break;
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                                    oBResponse.BureauData.responseStatus = XDSADPMODELS.ResponseStatus.Failure;
                                    oBResponse.BureauData.errorMessage = oResponse.ResponseData;

                                    oBResponse.Status = XDSPortalLibrary.Entity_Layer.MTNSOA.ResponseStatus.Failure.ToString();
                                    oBResponse.ErrorDescription = oResponse.ResponseData;
                                    break;
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                                    oBResponse.BureauData.responseStatus = XDSADPMODELS.ResponseStatus.Failure;
                                    oBResponse.BureauData.errorMessage = "No Record Found";

                                    oBResponse.Status = XDSPortalLibrary.Entity_Layer.MTNSOA.ResponseStatus.Failure.ToString();
                                    oBResponse.ErrorDescription = "No Record Found";
                                    break;
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                                    oBResponse.BureauData.responseStatus = XDSADPMODELS.ResponseStatus.Success;

                                    oBResponse.Status = XDSPortalLibrary.Entity_Layer.MTNSOA.ResponseStatus.Success.ToString();
                                    break;
                            }



                        }
                        catch (Exception e)
                        {
                            oBResponse.BureauData.responseStatus = XDSADPMODELS.ResponseStatus.Failure;
                            oBResponse.BureauData.errorMessage = e.Message;
                            oBResponse.Status = XDSPortalLibrary.Entity_Layer.MTNSOA.ResponseStatus.Failure.ToString();
                            oBResponse.ErrorDescription = e.Message;
                        }
                    }
                    else
                    {
                        oBResponse.BureauData.responseStatus = XDSADPMODELS.ResponseStatus.Failure;
                        oBResponse.BureauData.errorMessage = ConnectTicket;

                        oBResponse.Status = XDSPortalLibrary.Entity_Layer.MTNSOA.ResponseStatus.Failure.ToString();
                        oBResponse.ErrorDescription = ConnectTicket;
                    }
                }
            }


            catch (Exception ex)
            {
                oBResponse.BureauData.responseStatus = XDSADPMODELS.ResponseStatus.Failure;
                oBResponse.BureauData.errorMessage = ex.Message;

                oBResponse.Status = XDSPortalLibrary.Entity_Layer.MTNSOA.ResponseStatus.Failure.ToString();
                oBResponse.ErrorDescription = ex.Message;
            }

            string response = string.Empty;

            try
            {
                //  File.AppendAllText(@"C:\Log\Response.txt", "Enterdeserialization");
                XmlSerializer serializerout = new XmlSerializer(oBResponse.GetType());
                System.IO.StringWriter swout = new System.IO.StringWriter();
                serializerout.Serialize(swout, oBResponse);
                System.IO.StringReader readerout = new System.IO.StringReader(swout.ToString());

                response = readerout.ReadToEnd();

                SqlConnection con = new SqlConnection(Properties.Settings.Default.EnquiryConnectionString);

                SqlCommand cmd = new SqlCommand("Insert into CustomVettingSSolePropResponse (CustomVettingSSolepropinputID,Response,CreatedOnDate) values(@ID, @inputXMLstring,getdate())", con);
                cmd.Parameters.AddWithValue("@ID", intCustomVettingNRinputID);
                cmd.Parameters.AddWithValue("@inputXMLstring", response);
                if (con.State == ConnectionState.Closed)
                    con.Open();
                cmd.ExecuteNonQuery();

                con.Close();
            }

            catch (Exception ex)
            {
                // File.AppendAllText(@"C:\Log\Response.txt", "EnterException");
                SqlConnection con = new SqlConnection(Properties.Settings.Default.EnquiryConnectionString);

                SqlCommand cmd = new SqlCommand("Insert into CustomVettingSSolePropResponse (CustomVettingSSolepropinputID,Response,CreatedOnDate) values(@ID, @inputXMLstring,getdate())", con);
                cmd.Parameters.AddWithValue("@ID", intCustomVettingNRinputID);
                cmd.Parameters.AddWithValue("@inputXMLstring", ex.Message);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                con.Close();

            }

            return oBResponse;
        }

        [WebMethod(Description = "")]
        public XDSADPMODELS.BusinessSearchResponse ConnectCustomVettingSMatchA(string SearchType, string SubjectName, string RegistrationNo, string VatNumber, string BankAccountNumber,  string User, string Password, string ClientReference)
        {

            SearchType = string.IsNullOrEmpty(SearchType) ? string.Empty : SearchType;
            SubjectName = string.IsNullOrEmpty(SubjectName) ? string.Empty : SubjectName;
            RegistrationNo = string.IsNullOrEmpty(RegistrationNo) ? string.Empty : RegistrationNo;
           // ITNumber = string.IsNullOrEmpty(ITNumber) ? string.Empty : ITNumber;
           // DunsNumber = string.IsNullOrEmpty(DunsNumber) ? string.Empty : DunsNumber;
            VatNumber = string.IsNullOrEmpty(VatNumber) ? string.Empty : VatNumber;
            BankAccountNumber = string.IsNullOrEmpty(BankAccountNumber) ? string.Empty : BankAccountNumber;
           // TradingNumber = string.IsNullOrEmpty(TradingNumber) ? string.Empty : TradingNumber;
            User = string.IsNullOrEmpty(User) ? string.Empty : User;
            Password = string.IsNullOrEmpty(Password) ? string.Empty : Password;
            ClientReference = string.IsNullOrEmpty(ClientReference) ? string.Empty : ClientReference;


            DateTime dtStartdate = DateTime.Now;
            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();
            string rXml = string.Empty, ConnectTicket = string.Empty;

            XDSADPMODELS.BusinessSearchResponse oBResponse = new XDSADPMODELS.BusinessSearchResponse();
            oBResponse.BusinessSearchResult = new XDSADPMODELS.BusinessSearchResult();
            //oBResponse.BusinessSearchResult.SearchResponse = new XDSADPMODELS.SearchResponseData[];

            XmlSerializer serializer = new XmlSerializer(typeof(XDSADPMODELS.BusinessSearch));
            XDSADPMODELS.BusinessSearch objInput = new XDSADPMODELS.BusinessSearch();

            int intCustomVettingRinputID = 0;

            try
            {
                //switch (productID)
                //{
                //case 138:
                SqlConnection con = new SqlConnection(EnquiryCon.ConnectionString);

                SqlCommand cmd = new SqlCommand("Insert into CustomVettingSAMatchinput (Input,CreatedOnDate) values(@inputXMLstring,getdate())", con);

                //                cmd.Parameters.AddWithValue("@inputXMLstring", (";SearchType:" + SearchType + ";SubjectName:" + SubjectName + ";RegistrationNo:" + RegistrationNo + ";ITNumber:" + ITNumber + ";DunsNumber:" + DunsNumber + ";VatNumber:" + VatNumber + ";BankAccountNumber:" + BankAccountNumber + ";TradingNumber:" + TradingNumber + ";User:" + User + ";Password:" + Password + ";ClientReference:" + ClientReference));

                cmd.Parameters.AddWithValue("@inputXMLstring", (";SearchType:" + SearchType + ";SubjectName:" + SubjectName + ";RegistrationNo:" + RegistrationNo + ";VatNumber:" + VatNumber + ";BankAccountNumber:" + BankAccountNumber +  ";User:" + User + ";Password:" + Password + ";ClientReference:" + ClientReference));
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                string strSQL = "SELECT @@IDENTITY";
                SqlCommand Imycommand = new SqlCommand(strSQL, con);
                SqlDataReader Idr = Imycommand.ExecuteReader();
                Idr.Read();
                intCustomVettingRinputID = Convert.ToInt32(Idr.GetValue(0));
                Idr.Close();

                con.Close();

                //parse XML

                // xml = xml.Replace("&", string.Empty);

                // objInput = (XDSPortalLibrary.Entity_Layer.BusinessSearch)serializer.Deserialize(new XmlTextReader(new StringReader(xml)));

                //if (objInput.BusinessSearch1.SearchType.ToUpper() == XDSPortalLibrary.Entity_Layer.Business.BusinessSearchType.NAME.ToString())
                //{
                //    objInput.BusinessSearch1.RegistrationNo = string.Empty;
                //    objInput.BusinessSearch1.
                //}

                objInput.BusinessSearch1 = new XDSADPMODELS.Search();

                objInput.BusinessSearch1.BankAccountNumber = BankAccountNumber;
                objInput.BusinessSearch1.ClientReference = ClientReference;
               // objInput.BusinessSearch1.DunsNumber = DunsNumber;
               // objInput.BusinessSearch1.ITNumber = ITNumber;
                objInput.BusinessSearch1.Password = Password;
                objInput.BusinessSearch1.RegistrationNo = RegistrationNo;
                objInput.BusinessSearch1.SearchType = SearchType;
                objInput.BusinessSearch1.SubjectName = SubjectName;
//objInput.BusinessSearch1.TradingNumber = TradingNumber;
                objInput.BusinessSearch1.User = User;
                objInput.BusinessSearch1.VatNumber = VatNumber;


                XDSPortalLibrary.Entity_Layer.Response oResponse = new XDSPortalLibrary.Entity_Layer.Response();
                CommercialEnquiry_BusinessEnquiry oCommercial = new CommercialEnquiry_BusinessEnquiry();
                oResponse = oCommercial.ValidateBusinessSearchRA(objInput);

                if (oResponse.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                {
                    oBResponse.BusinessSearchResult.ResponseStatus = XDSADPMODELS.BusinessResponseStatus.FAILURE; 
                    oBResponse.BusinessSearchResult.ErrorDescription = oResponse.ResponseData;

                }
                else if (oResponse.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                {
                    ConnectTicket = Login(objInput.BusinessSearch1.User, objInput.BusinessSearch1.Password);
                    if (IsTicketValid(ConnectTicket))
                    {
                        FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);

                        string strRegNo1 = string.Empty, strRegNo2 = string.Empty, strRegNo3 = string.Empty;

                        string registrationno = string.Empty;
                        try
                        {
                            XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                            XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(AdminCon, int.Parse(oTicket.Name));

                            XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
                            XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(AdminCon, oSystemUser.SubscriberID);

                            registrationno = objInput.BusinessSearch1.RegistrationNo;

                            if (!string.IsNullOrEmpty(registrationno))
                            {
                                if (registrationno.Length == 12)
                                {
                                    strRegNo1 = registrationno.Substring(0, 4);
                                    strRegNo2 = registrationno.Substring(4, 6);
                                    strRegNo3 = registrationno.Substring(10, 2);
                                }
                                else
                                {
                                    strRegNo1 = registrationno.Substring(0, registrationno.IndexOf("/"));
                                    strRegNo2 = registrationno.Substring(registrationno.IndexOf("/") + 1, (registrationno.LastIndexOf("/") - (registrationno.IndexOf("/") + 1)));
                                    strRegNo3 = registrationno.Substring(registrationno.LastIndexOf("/") + 1);
                                }
                            }


                            XDSPortalEnquiry.Business.CommercialEnquiry_BusinessEnquiry dtCommercial = new XDSPortalEnquiry.Business.CommercialEnquiry_BusinessEnquiry();

                            XDSADPMODELS.BusinessSearchResponse oinputResponse = new XDSADPMODELS.BusinessSearchResponse();
                            oinputResponse = oBResponse;

                            oResponse = dtCommercial.SubmitBusinessEnquiryCustomA(EnquiryCon, AdminCon, oSubscriber.SubscriberID, oSystemUser.SystemUserID, 138, oSubscriber.SubscriberName, strRegNo1, strRegNo2, strRegNo3, objInput.BusinessSearch1.SubjectName, objInput.BusinessSearch1.VatNumber, string.Empty, string.Empty, objInput.BusinessSearch1.ClientReference, true, true, string.Empty, string.Empty, oinputResponse, out oBResponse);

                            switch (oResponse.ResponseStatus)
                            {
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                                    oBResponse.BusinessSearchResult.ResponseStatus = XDSADPMODELS.BusinessResponseStatus.FAILURE;
                                    oBResponse.BusinessSearchResult.ErrorDescription = oResponse.ResponseData;
                                    break;
                                case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                                    oBResponse.BusinessSearchResult.ResponseStatus = XDSADPMODELS.BusinessResponseStatus.FAILURE;
                                    oBResponse.BusinessSearchResult.ErrorDescription = "No Record Found";
                                    break;
                            }

                        }
                        catch (Exception e)
                        {
                            oBResponse.BusinessSearchResult.ResponseStatus = XDSADPMODELS.BusinessResponseStatus.FAILURE;
                            oBResponse.BusinessSearchResult.ErrorDescription = e.Message;
                        }
                    }
                    else
                    {
                        oBResponse.BusinessSearchResult.ResponseStatus = XDSADPMODELS.BusinessResponseStatus.FAILURE;
                        oBResponse.BusinessSearchResult.ErrorDescription = ConnectTicket;
                    }
                }

                //        break;
                //    default:
                //        oBResponse.BusinessSearchResult.ResponseStatus = XDSPortalLibrary.Entity_Layer.Business.BusinessResponseStatus.FAILURE;
                //        oBResponse.BusinessSearchResult.ErrorDescription = "Invalid Product Supplied";
                //        break;
                //}

            }
            catch (Exception ex)
            {
                oBResponse.BusinessSearchResult.ResponseStatus = XDSADPMODELS.BusinessResponseStatus.FAILURE;
                oBResponse.BusinessSearchResult.ErrorDescription = ex.Message;
            }

            DateTime dtenddate = DateTime.Now;
            double diff = dtenddate.Subtract(dtStartdate).TotalSeconds;

            oBResponse.BusinessSearchResult.ProcessingTimeSecs = float.Parse(Math.Round(diff, 2).ToString());

            oBResponse.BusinessSearchResult.UniqueRefGuid = (Guid.NewGuid()).ToString();

            string response = string.Empty;

            try
            {
                //  File.AppendAllText(@"C:\Log\Response.txt", "Enterdeserialization");
                XmlSerializer serializerout = new XmlSerializer(oBResponse.GetType());
                System.IO.StringWriter swout = new System.IO.StringWriter();
                serializerout.Serialize(swout, oBResponse);
                System.IO.StringReader readerout = new System.IO.StringReader(swout.ToString());

                response = readerout.ReadToEnd();

                SqlConnection con = new SqlConnection(Properties.Settings.Default.EnquiryConnectionString);

                SqlCommand cmd = new SqlCommand("Insert into CustomVettingSAResponse (CustomVettingSAMatchinputID,Response,CreatedOnDate) values(@ID, @inputXMLstring,getdate())", con);
                cmd.Parameters.AddWithValue("@ID", intCustomVettingRinputID);
                cmd.Parameters.AddWithValue("@inputXMLstring", response);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                con.Close();
            }

            catch (Exception ex)
            {
                // File.AppendAllText(@"C:\Log\Response.txt", "EnterException");
                SqlConnection con = new SqlConnection(Properties.Settings.Default.EnquiryConnectionString);

                SqlCommand cmd = new SqlCommand("Insert into CustomVettingOResponse (CustomVettingOinputID,Response,CreatedOnDate) values(@ID, @inputXMLstring,getdate())", con);
                cmd.Parameters.AddWithValue("@ID", intCustomVettingRinputID);
                cmd.Parameters.AddWithValue("@inputXMLstring", ex.Message);
                if (con.State == ConnectionState.Closed)
                    con.Open();

                cmd.ExecuteNonQuery();

                con.Close();

            }

            return oBResponse;
        }

      

    }
}