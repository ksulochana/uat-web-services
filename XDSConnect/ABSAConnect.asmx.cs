﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Threading;
using System.Security.Principal;
using System.Web.Security;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Data;
using System.Net.Mail;
using System.Configuration;
using System.Net;
using System.Text;
using System.Reflection;
using XDSPortalAuthentication;
using DevExpress.XtraReports.UI;
using XdsPortalReports;

namespace XDSConnect
{
    /// <summary>
    /// Summary description for ABSAConnect
    /// </summary>
    [WebService(Namespace = "http://www.web.xds.co.za/XDSConnectWS")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class ABSAConnect : System.Web.Services.WebService
    {
        #region [ Declarations ]
        private SqlConnection EnquiryCon = new SqlConnection(Properties.Settings.Default.EnquiryConnectionString);
        private SqlConnection AdminCon = new SqlConnection(Properties.Settings.Default.AdminConnectionString);
        private SqlConnection AuthCon = new SqlConnection(Properties.Settings.Default.AuthConnectionstring);
        private SqlConnection AuthEnquiryCon = new SqlConnection(Properties.Settings.Default.AuthEnquiryConnection);
        private SqlConnection AVSCon = new SqlConnection(Properties.Settings.Default.AVSConnection);
        private SqlConnection BCCOn = new SqlConnection(Properties.Settings.Default.BCConnection);
        private SqlConnection AVSEnquiryCon = new SqlConnection(Properties.Settings.Default.AVSConnectionEnquiryDB);
        private SqlConnection BCEnquiryCOn = new SqlConnection(Properties.Settings.Default.BCConnectionEnquiryDB);
        private SqlConnection BIVEnquiryCOn = new SqlConnection(Properties.Settings.Default.BIVConnectionEnquiryDB);
        private string strSmtpServerName = ConfigurationManager.AppSettings["XDSSmtp"].ToString();
        private string strSmtpUserLoginID = ConfigurationManager.AppSettings["XDSSmtpUserLoginID"].ToString();
        private string strSmtpUserPassword = ConfigurationManager.AppSettings["XDSSmtpUserPassword"].ToString();
        private int intport = Convert.ToInt16(ConfigurationManager.AppSettings["XDSSmtpPort"].ToString());
        private SqlConnection SMSCon = new SqlConnection(Properties.Settings.Default.SMSDBConnectionString);
        private SqlConnection IdenticateCon = new SqlConnection(Properties.Settings.Default.IdenticateConnection);
        private string CVetUserName = ConfigurationManager.AppSettings["CustomVettingLogUserName"].ToString();
        private string CVetPassword = ConfigurationManager.AppSettings["CustomVettingLogPassword"].ToString();
        private string CVetUrL = ConfigurationManager.AppSettings["CustomVettingLogURL"].ToString();
        private bool UseCustomVettingLog = bool.Parse(ConfigurationManager.AppSettings["UseCustomvettingLog"].ToString());
        private string sCredit4LifeDirectory = ConfigurationManager.AppSettings["Credit4LifeDirectory"].ToString();
        private string sCredit4LifeReportURL = ConfigurationManager.AppSettings["Credit4LifeReportURL"].ToString();
        private string strPBSAUsername = ConfigurationManager.AppSettings["PBSAUsername"].ToString();
        private string strPBSAPassword = ConfigurationManager.AppSettings["PBSAPassword"].ToString();
        private string strPBSAMemberKey = ConfigurationManager.AppSettings["PBSAMemberKey"].ToString();
        private string strPBSATerminalID = ConfigurationManager.AppSettings["PBSATerminalID"].ToString();
        private string strPBSAURL = ConfigurationManager.AppSettings["PBSAURL"].ToString();
        private string AuthSyncServer = ConfigurationManager.AppSettings["AuthSyncServer"].ToString();
        #endregion

        #region [ Enums ]
        public enum EnquiryReason
        {
            CreditAssessment = 1,
            DebtReview = 2
        }

        public enum AuthenticationProcessAction
        {
            Authenticate = 1,
            Void = 2,
            ReferToFraud = 3
        } 
        #endregion

        [WebMethod(Description = "Log in XDS Connect Web Service and returns a Ticket")]
        public string Login(string strUser, string strPwd)
        {
            LoginUser oLoginUser = AuthenticateUser(strUser, strPwd);

            if (oLoginUser.LoginStatus == LoginUser.LoginUserStatus.Authenticated)
            {

                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                   1,                            // version
                   oLoginUser.LoginSystemUser.SystemUserID.ToString(),                      // user name
                   DateTime.Now,                 // create time
                   DateTime.Now.AddHours(5),  // expire time
                   false,                        // persistent
                   oLoginUser.LoginSystemUser.SubscriberID.ToString());              // user data

                oLoginUser.LoginTicket = FormsAuthentication.Encrypt(ticket);
                //HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, oLoginUser.LoginTicket);
                //Context.Response.Cookies.Add(cookie);
                return oLoginUser.LoginTicket;
            }
            else
                return oLoginUser.LoginStatus.ToString();
        }

        [WebMethod(Description = "Checks Validity of your XDS Connect Ticket")]
        public bool IsTicketValid(string XDSConnectTicket)
        {
            try
            {
                FormsAuthenticationTicket a = FormsAuthentication.Decrypt(XDSConnectTicket);

                bool isValid = true;
                if (a.Expired) isValid = false;
                return isValid;
            }
            catch
            {
                return false;
            }
        }

        private LoginUser AuthenticateUser(string strUser, string strPwd)
        {
            LoginUser oLoginUser = new LoginUser();

            AdminCon.Open();

            if (Membership.ValidateUser(strUser, strPwd))
            {
                XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUser(AdminCon, strUser);

                if (oSystemUser != null)
                {
                    XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
                    XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(AdminCon, oSystemUser.SubscriberID);

                    oLoginUser.LoginStatus = LoginUser.LoginUserStatus.Authenticated;
                    oLoginUser.LoginSystemUser = oSystemUser;

                    if (!oSystemUser.ActiveYN) oLoginUser.LoginStatus = LoginUser.LoginUserStatus.UserDeactivated;
                    if (oSubscriber.StatusInd != "A") oLoginUser.LoginStatus = LoginUser.LoginUserStatus.SubscriberDeactivated;

                }
                else
                {
                    oLoginUser.LoginStatus = LoginUser.LoginUserStatus.UserNotFound;
                }

            }
            else
            {
                oLoginUser.LoginStatus = LoginUser.LoginUserStatus.NotAuthenticated;
            }
            AdminCon.Close();
            return oLoginUser;
            //throw new NotImplementedException();
        }

        //[WebMethod(Description = "Log out of XDS Connect Web Service")]
        public void LogOut()
        {
            // Deprive client of the authentication key
            FormsAuthentication.SignOut();
        }

        [WebMethod(Description = "Process a Consumer match based on ProductID and returns matched Consumers")]
        public string ConnectConsumerMatch(string ConnectTicket, string EnquiryReason, int ProductId, string IdNumber,
            string PassportNo, string FirstName, string Surname, string BirthDate, string YourReference, string VoucherCode)
        {
            string rXml = "";
            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();

            if (IsTicketValid(ConnectTicket))
            {
                FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);

                try
                {
                    XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                    XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(AdminCon.ConnectionString, int.Parse(oTicket.Name));

                    XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
                    XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(AdminCon.ConnectionString, oSystemUser.SubscriberID);
                    XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
                    DateTime dDate = DateTime.Now;

                    if (!string.IsNullOrEmpty(BirthDate)) dDate = DateTime.Parse(BirthDate);

                    switch (ProductId)
                    {
                        case 1:
                        case 2:
                        case 39:
                        case 53:
                        case 80:
                        case 81:
                        case 83:
                        case 87:
                        case 92:
                        case 93:
                        case 94:
                        case 95:
                        case 96:
                        case 98:
                        case 99:
                            XDSPortalEnquiry.Business.ConsumerTrace_ConsumerTrace dtConsumertraceMS = new XDSPortalEnquiry.Business.ConsumerTrace_ConsumerTrace();
                            rp = dtConsumertraceMS.SubmitConsumerTrace(EnquiryCon, AdminCon, oSubscriber.SubscriberID, oSystemUser.SystemUserID, ProductId, oSubscriber.SubscriberName, IdNumber, PassportNo, Surname, "", FirstName, "", "", "", dDate, XDSPortalEnquiry.Entity.SubscriberEnquiry.GenderInd.M, YourReference, true, true, VoucherCode);
                            break;
                        case 33:
                        case 34:
                        case 35:
                        case 36:
                        case 37:
                        case 38:
                            XDSPortalEnquiry.Business.ConsumerTrace_PublicDomainEnquiry dtConsumerDEBTRW = new XDSPortalEnquiry.Business.ConsumerTrace_PublicDomainEnquiry();
                            rp = dtConsumerDEBTRW.SubmitPublicDomainEnquiry(EnquiryCon, AdminCon, oSubscriber.SubscriberID, oSystemUser.SystemUserID, ProductId, oSubscriber.SubscriberName, IdNumber, PassportNo, Surname, "", FirstName, "", "", "", dDate, XDSPortalEnquiry.Entity.SubscriberEnquiry.GenderInd.M, YourReference, true, true, VoucherCode);
                            break;
                        case 15:
                            XDSPortalEnquiry.Business.ConsumerCreditGrantor_CreditEnquiry dtConsumerCredit = new XDSPortalEnquiry.Business.ConsumerCreditGrantor_CreditEnquiry();
                            rp = dtConsumerCredit.SubmitCreditEnquiry(EnquiryCon, AdminCon, oSubscriber.SubscriberID, oSystemUser.SystemUserID, ProductId, oSubscriber.SubscriberName, IdNumber, PassportNo, Surname, "", FirstName, "", "", "", dDate, XDSPortalEnquiry.Entity.SubscriberEnquiry.GenderInd.M, YourReference, EnquiryReason, true, 0, 0, 0, true, 0, 0, 0, 0, true, true, VoucherCode);
                            break;
                        default:
                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Invalid Product Supplied";
                            break;
                    }

                    switch (rp.ResponseStatus)
                    {
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single:
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple:
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                            rsXml.LoadXml(rp.ResponseData);
                            break;

                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                            rXml = "<NoResult><Error>" + rp.ResponseData + "</Error></NoResult>";
                            rsXml.LoadXml(rXml);
                            break;
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                            rXml = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                            rsXml.LoadXml(rXml);
                            break;
                    }

                }

                catch (Exception oException)
                {
                    rXml = "<NoResult><Error>" + oException.Message + "</Error></NoResult>";
                    rsXml.LoadXml(rXml);
                }
            }
            else
            {
                rXml = "<NoResult><Error>Invalid Ticket</Error></NoResult>";
                rsXml.LoadXml(rXml);
            }

            return rsXml.OuterXml;

        }

        [WebMethod(Description = "Process a Consumer match based on ProductID and returns matched Consumers")]
        public string ConnectConsumerMatchKYC(string ConnectTicket, string EnquiryReason, int ProductId, string IdNumber, string PassportNo, string Surname, string YourReference, string VoucherCode)
        {
            string rXml = "";
            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();

            if (IsTicketValid(ConnectTicket))
            {
                FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);

                try
                {
                    XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                    XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(AdminCon.ConnectionString, int.Parse(oTicket.Name));

                    XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
                    XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(AdminCon.ConnectionString, oSystemUser.SubscriberID);
                    XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
                    DateTime dDate = DateTime.Now;

                    switch (ProductId)
                    {
                        case 91:
                            XDSPortalEnquiry.Business.KYCEnquiry dtKYC = new XDSPortalEnquiry.Business.KYCEnquiry();
                            XDSPortalEnquiry.Business.ConsumerTrace_ConsumerTrace dtConsumertraceMS = new XDSPortalEnquiry.Business.ConsumerTrace_ConsumerTrace();
                            rp = dtKYC.SubmitConsumerTrace(EnquiryCon, AdminCon, oSubscriber.SubscriberID, oSystemUser.SystemUserID, ProductId,
                                oSubscriber.SubscriberName, IdNumber, PassportNo, Surname, "", "", "", "", "", dDate,
                                XDSPortalEnquiry.Entity.SubscriberEnquiry.GenderInd.M, YourReference, true, true, VoucherCode);
                            break;
                        default:
                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Invalid Product Supplied";
                            break;
                    }

                    switch (rp.ResponseStatus)
                    {
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single:
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple:
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                            rsXml.LoadXml(rp.ResponseData);
                            break;

                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                            rXml = "<NoResult><Error>" + rp.ResponseData + "</Error></NoResult>";
                            rsXml.LoadXml(rXml);
                            break;
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                            rXml = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                            rsXml.LoadXml(rXml);
                            break;
                    }

                }

                catch (Exception oException)
                {
                    rXml = "<NoResult><Error>" + oException.Message + "</Error></NoResult>";
                    rsXml.LoadXml(rXml);
                }
            }
            else
            {
                rXml = "<NoResult><Error>Invalid Ticket</Error></NoResult>";
                rsXml.LoadXml(rXml);
            }

            return rsXml.OuterXml;
        }

        [WebMethod(Description = "Process a Consumer match based on AccountNo and returns matched Consumers")]
        public string ConnectAccountMatch(string ConnectTicket, string AccountNo, string SubAccountNo, string Surname, string YourReference, string VoucherCode)
        {
            string rXml = "";
            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();

            if (IsTicketValid(ConnectTicket))
            {
                FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);

                try
                {
                    XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                    XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(AdminCon.ConnectionString, int.Parse(oTicket.Name));

                    XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
                    XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(AdminCon.ConnectionString, oSystemUser.SubscriberID);
                    XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

                    XDSPortalEnquiry.Business.ConsumerTrace_AccountTrace dtConsumertrace = new XDSPortalEnquiry.Business.ConsumerTrace_AccountTrace();
                    rp = dtConsumertrace.SubmitAccountTrace(EnquiryCon, AdminCon, oSubscriber.SubscriberID, oSystemUser.SystemUserID, 5, oSubscriber.SubscriberName, AccountNo, SubAccountNo, YourReference, true, true, VoucherCode, Surname);

                    switch (rp.ResponseStatus)
                    {
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single:
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple:
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                            rsXml.LoadXml(rp.ResponseData);
                            break;

                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                            rXml = "<NoResult><Error>" + rp.ResponseData + "</Error></NoResult>";
                            rsXml.LoadXml(rXml);
                            break;
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                            rXml = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                            rsXml.LoadXml(rXml);
                            break;
                    }

                }

                catch (Exception oException)
                {
                    rXml = "<NoResult><Error>" + oException.Message + "</Error></NoResult>";
                    rsXml.LoadXml(rXml);
                }
            }
            else
            {
                rXml = "<NoResult><Error>Invalid Ticket</Error></NoResult>";
                rsXml.LoadXml(rXml);
            }

            return rsXml.OuterXml;

        }

        [WebMethod(Description = "Process a Consumer match based on Address and returns matched Consumers")]
        public string ConnectAddressMatch(string ConnectTicket, string Province, string City, string Suburb, bool PostalMatch, string StreetName_PostalNo, string PostalCode, string StreetNo, string Surname, string YourReference, string VoucherCode)
        {
            string rXml = "";
            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();

            if (IsTicketValid(ConnectTicket))
            {
                FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);

                try
                {
                    XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                    XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(AdminCon.ConnectionString, int.Parse(oTicket.Name));

                    XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
                    XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(AdminCon.ConnectionString, oSystemUser.SubscriberID);
                    XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

                    XDSPortalEnquiry.Business.ConsumerTrace_AddressTrace dtConsumertrace = new XDSPortalEnquiry.Business.ConsumerTrace_AddressTrace();
                    rp = dtConsumertrace.SubmitAddressTrace(EnquiryCon, AdminCon, oSubscriber.SubscriberID, oSystemUser.SystemUserID, 3, oSubscriber.SubscriberName, Surname, StreetNo, StreetName_PostalNo, Suburb, PostalCode, Province, City, "South Africa", YourReference, true, true, VoucherCode);

                    switch (rp.ResponseStatus)
                    {
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single:
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple:
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                            rsXml.LoadXml(rp.ResponseData);
                            break;

                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                            rXml = "<NoResult><Error>" + rp.ResponseData + "</Error></NoResult>";
                            rsXml.LoadXml(rXml);
                            break;
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                            rXml = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                            rsXml.LoadXml(rXml);
                            break; ;
                    }

                }

                catch (Exception oException)
                {
                    rXml = "<NoResult><Error>" + oException.Message + "</Error></NoResult>";
                    rsXml.LoadXml(rXml);
                }
            }
            else
            {
                rXml = "<NoResult><Error>Invalid Ticket</Error></NoResult>";
                rsXml.LoadXml(rXml);
            }

            return rsXml.OuterXml;

        }

        [WebMethod(Description = "Process a Consumer match based on TelephoneNo and returns matched Consumers")]
        public string ConnectTelephoneMatch(string ConnectTicket, string TelephoneCode, string TelephoneNo, string Surname,
            string YourReference, string VoucherCode)
        {
            string rXml = "";
            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();

            if (IsTicketValid(ConnectTicket))
            {
                FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);

                try
                {
                    XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                    XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(AdminCon.ConnectionString, int.Parse(oTicket.Name));

                    XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
                    XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(AdminCon.ConnectionString, oSystemUser.SubscriberID);
                    XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
                    DateTime dDate = DateTime.Now;

                    XDSPortalEnquiry.Business.ConsumerTrace_TelephoneTrace dtConsumertrace = new XDSPortalEnquiry.Business.ConsumerTrace_TelephoneTrace();
                    rp = dtConsumertrace.SubmitTelephoneTrace(EnquiryCon, AdminCon, oSubscriber.SubscriberID, oSystemUser.SystemUserID, 4, oSubscriber.SubscriberName, TelephoneCode, TelephoneNo, "", "", XDSPortalEnquiry.Entity.SubscriberEnquiry.TelephoneTypeInd.T, YourReference, true, true, VoucherCode, Surname);

                    switch (rp.ResponseStatus)
                    {
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single:
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple:
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                            rsXml.LoadXml(rp.ResponseData);
                            break;

                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                            rXml = "<NoResult><Error>" + rp.ResponseData + "</Error></NoResult>";
                            rsXml.LoadXml(rXml);
                            break;
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                            rXml = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                            rsXml.LoadXml(rXml);
                            break;
                    }

                }

                catch (Exception oException)
                {
                    rXml = "<NoResult><Error>" + oException.Message + "</Error></NoResult>";
                    rsXml.LoadXml(rXml);
                }
            }
            else
            {
                rXml = "<NoResult><Error>Invalid Ticket</Error></NoResult>";
                rsXml.LoadXml(rXml);
            }

            return rsXml.OuterXml;

        }

        [WebMethod(Description = "Process a Consumer match based on ConsumerAddressID and returns matched Consumers")]
        public string ConnectAddressIDMatch(string ConnectTicket, int ConsumerAddressID, string YourReference, string VoucherCode)
        {
            string rXml = "";
            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();

            if (IsTicketValid(ConnectTicket))
            {
                FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);

                try
                {
                    XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                    XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(AdminCon.ConnectionString, int.Parse(oTicket.Name));

                    XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
                    XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(AdminCon.ConnectionString, oSystemUser.SubscriberID);
                    XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

                    XDSPortalEnquiry.Business.ConsumerTrace_AddressTrace dtConsumertrace = new XDSPortalEnquiry.Business.ConsumerTrace_AddressTrace();

                    string Province = "", City = "", Suburb = "", StreetName_PostalNo = "", PostalCode = "", StreetNo = "", Surname = "";
                    bool PostalMatch;
                    DataSet ds = new DataSet();
                    ds = dtConsumertrace.SubmitAddressIDTrace(AdminCon, ConsumerAddressID);

                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow dr = ds.Tables[0].Rows[0];
                        Province = "GAUTENG";
                        City = dr["Town"].ToString() != "" ? dr["Town"].ToString() : dr["Address1"].ToString();
                        Suburb = dr["Suburb"].ToString() != "" ? dr["Suburb"].ToString() : dr["Address2"].ToString();
                        PostalMatch = false;
                        StreetName_PostalNo = dr["Address2"].ToString() != "" ? dr["Address2"].ToString() : dr["Address3"].ToString();
                        StreetNo = dr["Address1"].ToString() != "" ? dr["Address1"].ToString() : dr["Address4"].ToString();
                        PostalCode = dr["PostalCode"].ToString();
                        Surname = "";
                    }

                    rp = dtConsumertrace.SubmitAddressTrace(EnquiryCon, AdminCon, oSubscriber.SubscriberID, oSystemUser.SystemUserID, 3, oSubscriber.SubscriberName, Surname, StreetNo, StreetName_PostalNo, Suburb, PostalCode, Province, City, ConsumerAddressID.ToString(), YourReference, true, true, VoucherCode);

                    switch (rp.ResponseStatus)
                    {
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single:
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple:
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                            rsXml.LoadXml(rp.ResponseData);
                            break;

                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                            rXml = "<NoResult><Error>" + rp.ResponseData + "</Error></NoResult>";
                            rsXml.LoadXml(rXml);
                            break;
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                            rXml = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                            rsXml.LoadXml(rXml);
                            break; ;
                    }

                }

                catch (Exception oException)
                {
                    rXml = "<NoResult><Error>" + oException.Message + "</Error></NoResult>";
                    rsXml.LoadXml(rXml);
                }
            }
            else
            {
                rXml = "<NoResult><Error>Invalid Ticket</Error></NoResult>";
                rsXml.LoadXml(rXml);
            }

            return rsXml.OuterXml;

        }

        [WebMethod(Description = "Process a Consumer match based on ConsumerTelephoneID and returns matched Consumers")]
        public string ConnectTelephoneIDMatch(string ConnectTicket, int ConsumerTelephoneID, string YourReference, string VoucherCode)
        {
            string rXml = "";
            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();

            if (IsTicketValid(ConnectTicket))
            {
                FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);

                try
                {
                    XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                    XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(AdminCon.ConnectionString, int.Parse(oTicket.Name));

                    XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
                    XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(AdminCon.ConnectionString, oSystemUser.SubscriberID);
                    XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
                    DateTime dDate = DateTime.Now;

                    XDSPortalEnquiry.Business.ConsumerTrace_TelephoneTrace dtConsumertrace = new XDSPortalEnquiry.Business.ConsumerTrace_TelephoneTrace();

                    string TelephoneCode = "", TelephoneNo = "", Surname = "";
                    DataSet ds = new DataSet();
                    ds = dtConsumertrace.SubmitTelephoneIDTrace(AdminCon, ConsumerTelephoneID);

                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        DataRow dr = ds.Tables[0].Rows[0];
                        TelephoneCode = dr["TelephoneCode"].ToString();
                        TelephoneNo = dr["TelephoneNo"].ToString();
                        Surname = "";
                    }

                    rp = dtConsumertrace.SubmitTelephoneTrace(EnquiryCon, AdminCon, oSubscriber.SubscriberID, oSystemUser.SystemUserID, 4, oSubscriber.SubscriberName, TelephoneCode, TelephoneNo, "", "", XDSPortalEnquiry.Entity.SubscriberEnquiry.TelephoneTypeInd.T, YourReference, true, true, VoucherCode, Surname);

                    switch (rp.ResponseStatus)
                    {
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single:
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple:
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                            rsXml.LoadXml(rp.ResponseData);
                            break;

                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                            rXml = "<NoResult><Error>" + rp.ResponseData + "</Error></NoResult>";
                            rsXml.LoadXml(rXml);
                            break;
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                            rXml = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                            rsXml.LoadXml(rXml);
                            break;
                    }

                }

                catch (Exception oException)
                {
                    rXml = "<NoResult><Error>" + oException.Message + "</Error></NoResult>";
                    rsXml.LoadXml(rXml);
                }
            }
            else
            {
                rXml = "<NoResult><Error>Invalid Ticket</Error></NoResult>";
                rsXml.LoadXml(rXml);
            }

            return rsXml.OuterXml;

        }

        [WebMethod(Description = "Process a Consumer match based on Easy Trace and returns matched Consumers")]
        public string ConnectEasyMatch(string ConnectTicket, int ProductId, string SearchType, string Surname, string FirstName, int Age, int Year, int Deviation, string YourReference, string VoucherCode)
        {
            string rXml = "";
            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();

            if (IsTicketValid(ConnectTicket))
            {
                FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);

                try
                {
                    XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                    XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(AdminCon.ConnectionString, int.Parse(oTicket.Name));

                    XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
                    XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(AdminCon.ConnectionString, oSystemUser.SubscriberID);
                    XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
                    DateTime dDate = DateTime.Now;

                    XDSPortalEnquiry.Business.ConsumerTrace_EasyTrace dtConsumertrace = new XDSPortalEnquiry.Business.ConsumerTrace_EasyTrace();
                    rp = dtConsumertrace.SubmitEasyTrace(EnquiryCon, AdminCon, oSubscriber.SubscriberID, oSystemUser.SystemUserID, 7, oSubscriber.SubscriberName, Surname, FirstName, "", "", "", XDSPortalEnquiry.Entity.SubscriberEnquiry.GenderInd.M, SearchType, Age, Year, Deviation, YourReference, true, true, VoucherCode);

                    switch (rp.ResponseStatus)
                    {
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single:
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple:
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                            rsXml.LoadXml(rp.ResponseData);
                            break;

                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                            rXml = "<NoResult><Error>" + rp.ResponseData + "</Error></NoResult>";
                            rsXml.LoadXml(rXml);
                            break;
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                            rXml = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                            rsXml.LoadXml(rXml);
                            break;
                    }

                }

                catch (Exception oException)
                {
                    rXml = "<NoResult><Error>" + oException.Message + "</Error></NoResult>";
                    rsXml.LoadXml(rXml);
                }
            }
            else
            {
                rXml = "<NoResult><Error>Invalid Ticket</Error></NoResult>";
                rsXml.LoadXml(rXml);
            }

            return rsXml.OuterXml;

        }

        [WebMethod(Description = "Process a Consumer Verification based on ID No and returns matched Consumers")]
        public string ConnectIDVerification(string ConnectTicket, string IdNumber, string FirstName, string Surname, string YourReference, string VoucherCode)
        {
            string rXml = "";
            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();

            if (IsTicketValid(ConnectTicket))
            {
                FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);

                try
                {
                    XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                    XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(AdminCon.ConnectionString, int.Parse(oTicket.Name));

                    XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
                    XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(AdminCon.ConnectionString, oSystemUser.SubscriberID);
                    XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
                    DateTime dDate = DateTime.Now;

                    XDSPortalEnquiry.Business.ConsumerTrace_IdentityVerification dtConsumertrace = new XDSPortalEnquiry.Business.ConsumerTrace_IdentityVerification();
                    rp = dtConsumertrace.SubmitIdentityVerification(EnquiryCon, AdminCon, oSubscriber.SubscriberID, oSystemUser.SystemUserID, 9, oSubscriber.SubscriberName, IdNumber, Surname, FirstName, "", dDate, YourReference, true, true, VoucherCode);

                    switch (rp.ResponseStatus)
                    {
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single:
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple:
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                            rsXml.LoadXml(rp.ResponseData);
                            break;

                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                            rXml = "<NoResult><Error>" + rp.ResponseData + "</Error></NoResult>";
                            rsXml.LoadXml(rXml);
                            break;
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                            rXml = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                            rsXml.LoadXml(rXml);
                            break;
                    }

                }

                catch (Exception oException)
                {
                    rXml = "<NoResult><Error>" + oException.Message + "</Error></NoResult>";
                    rsXml.LoadXml(rXml);
                }
            }
            else
            {
                rXml = "<NoResult><Error>Invalid Ticket</Error></NoResult>";
                rsXml.LoadXml(rXml);
            }

            return rsXml.OuterXml;

        }

        [WebMethod(Description = "Returns the result of selected matched Consumer based on a ProductID")]
        public string ConnectGetResult(string ConnectTicket, int EnquiryID, int EnquiryResultID, int ProductID, string BonusXML)
        {
            string rXml = "";
            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();

            if (IsTicketValid(ConnectTicket))
            {
                FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);

                try
                {
                    XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                    XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(Properties.Settings.Default.AdminConnectionString, int.Parse(oTicket.Name));

                    XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
                    XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(Properties.Settings.Default.AdminConnectionString, oSystemUser.SubscriberID);
                    XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

                    XDSPortalEnquiry.Data.SubscriberEnquiry odSubscriberEnquiry = new XDSPortalEnquiry.Data.SubscriberEnquiry();
                    XDSPortalEnquiry.Entity.SubscriberEnquiry oSubscriberEnquiry = odSubscriberEnquiry.GetSubscriberEnquiryObject(EnquiryCon, EnquiryID);

                    XDSPortalEnquiry.Data.SubscriberEnquiryResult odSubscriberEnquiryResult = new XDSPortalEnquiry.Data.SubscriberEnquiryResult();
                    XDSPortalEnquiry.Entity.SubscriberEnquiryResult oSubscriberEnquiryResult = odSubscriberEnquiryResult.GetSubscriberEnquiryResultObject(EnquiryCon, EnquiryResultID);

                    XDSPortalEnquiry.Data.SubscriberEnquiryResultBonus odSubscriberEnquiryResultBonus = new XDSPortalEnquiry.Data.SubscriberEnquiryResultBonus();


                    DataSet ds = new DataSet();

                    DataSet dsDataSegments = odSubscriberEnquiryResultBonus.GetSubscriberEnquiryResultBonusDataSet(EnquiryCon, EnquiryResultID);

                    if (!string.IsNullOrEmpty(BonusXML))
                    {
                        try
                        {
                            System.IO.StringReader xmlSR = new System.IO.StringReader(BonusXML);
                            ds.ReadXml(xmlSR);
                            if (ds.Tables.Count < 1)
                            {
                                ds = null;
                            }
                            else if (ds.Tables[0].Rows.Count < 1)
                            {
                                ds = null;
                            }
                            else
                            {

                                if (ds.Tables[0].Columns.Contains("DataSegmentID") && ds.Tables[0].Columns.Contains("BonusViewed"))
                                {
                                    foreach (DataRow dr in ds.Tables[0].Rows)
                                    {
                                        if (dr["BonusViewed"].ToString().ToLower() == "true")
                                        {
                                            foreach (DataRow r in dsDataSegments.Tables[0].Rows)
                                            {
                                                if (dr["DataSegmentID"].ToString() == r["DataSegmentID"].ToString())
                                                {
                                                    dr["BonusPrice"] = r["BillingPrice"].ToString();
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    rXml = "<Error>" + "Invalid Trace Plus XML Supplied" + "</Error>";
                                    rsXml.LoadXml(rXml);
                                    return rsXml.OuterXml;

                                }
                            }


                        }
                        catch
                        {
                            rXml = "<Error>" + "Invalid Trace Plus XML Supplied" + "</Error>";
                            rsXml.LoadXml(rXml);
                            return rsXml.OuterXml;

                        }
                    }
                    else
                    {
                        ds = null;
                    }
                    dsDataSegments.Clear();
                    dsDataSegments = ds;


                    switch (oSubscriberEnquiry.ProductID)
                    {
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 7:
                        case 39:
                            XDSPortalEnquiry.Business.MultipleTrace_ConsumerTrace dtMultipleTrace = new XDSPortalEnquiry.Business.MultipleTrace_ConsumerTrace();
                            rp = dtMultipleTrace.SubmitMultipleConsumerTrace(EnquiryCon, AdminCon, EnquiryID, EnquiryResultID, dsDataSegments, true, oSubscriberEnquiryResult.VoucherCode);
                            break;
                        case 9:
                            XDSPortalEnquiry.Business.MultipleTrace_IdentityVerification dtMultipleID = new XDSPortalEnquiry.Business.MultipleTrace_IdentityVerification();
                            rp = dtMultipleID.SubmitMultipleIdentityVerification(EnquiryCon, AdminCon, EnquiryID, EnquiryResultID, dsDataSegments, true, oSubscriberEnquiryResult.VoucherCode);
                            break;
                        case 12:
                            if (ProductID == 0)
                            {
                                throw new Exception("InValid ProductID Supplied");
                            }
                            XDSPortalEnquiry.Business.MultipleCommercial_BusinessEnquiry dtMultipleCommercial = new XDSPortalEnquiry.Business.MultipleCommercial_BusinessEnquiry();
                            rp = dtMultipleCommercial.SubmitMulipleBusinessEnquiry(EnquiryCon, AdminCon, EnquiryID, EnquiryResultID, ProductID, dsDataSegments, true, oSubscriberEnquiryResult.VoucherCode);
                            break;
                        case 14:
                            XDSPortalEnquiry.Business.MultipleCommercial_DirectorEnquiry dtMultipleDirectorTrace = new XDSPortalEnquiry.Business.MultipleCommercial_DirectorEnquiry();
                            rp = dtMultipleDirectorTrace.SubmitMulipleDirectorEnquiry(EnquiryCon, AdminCon, EnquiryID, EnquiryResultID, dsDataSegments, true, oSubscriberEnquiryResult.VoucherCode);
                            break;
                        case 15:
                        case 31:
                            XDSPortalEnquiry.Business.MultipleCreditGrantor_CreditEnquiry dtMultipleCredit = new XDSPortalEnquiry.Business.MultipleCreditGrantor_CreditEnquiry();
                            rp = dtMultipleCredit.SubmitMulipleCreditEnquiry(EnquiryCon, AdminCon, EnquiryID, EnquiryResultID, dsDataSegments, true, oSubscriberEnquiryResult.VoucherCode);
                            break;

                        case 33:
                        case 34:
                        case 35:
                        case 36:
                        case 37:
                        case 38:
                            XDSPortalEnquiry.Business.MultipleTrace_PublicDomainEnquiry dtMultiplePublicDomain = new XDSPortalEnquiry.Business.MultipleTrace_PublicDomainEnquiry();
                            rp = dtMultiplePublicDomain.SubmitMultiplePublicDomainEnquiry(EnquiryCon, AdminCon, EnquiryID, EnquiryResultID, dsDataSegments, true, oSubscriberEnquiryResult.VoucherCode);
                            break;
                        case 42:
                            XDSPortalEnquiry.Business.MultipleDeeds_DeedsEnquiry dtMultipleDeeds = new XDSPortalEnquiry.Business.MultipleDeeds_DeedsEnquiry();
                            rp = dtMultipleDeeds.SubmitMultipleDeedsEnquiry(EnquiryCon, AdminCon, EnquiryID, EnquiryResultID, dsDataSegments, true, oSubscriberEnquiryResult.VoucherCode);
                            break;

                        case 51:
                            XDSPortalEnquiry.Business.CustomVetting dtCustomVetting = new XDSPortalEnquiry.Business.CustomVetting();
                            rp = dtCustomVetting.SubmitCustomVetingC(EnquiryCon, AdminCon, EnquiryID, EnquiryResultID, true, oSubscriberEnquiryResult.VoucherCode);
                            break;
                        case 80:
                        case 81:
                            XDSPortalEnquiry.Business.MultipleTrace_CollectionsSnapshot dtCollections = new XDSPortalEnquiry.Business.MultipleTrace_CollectionsSnapshot();
                            rp = dtCollections.SubmitMultipleCollectionTrace(EnquiryCon, AdminCon, EnquiryID, EnquiryResultID, dsDataSegments, true, oSubscriberEnquiryResult.VoucherCode);
                            break;
                        case 87:
                            XDSPortalEnquiry.Business.MultipleCreditGrantor_CreditEnquiry dtACFECreditEnquiry = new XDSPortalEnquiry.Business.MultipleCreditGrantor_CreditEnquiry();
                            dtACFECreditEnquiry.AuthSyncServersconnection = AuthSyncServer;
                            rp = dtACFECreditEnquiry.SubmitACFEMulipleCreditEnquiry(EnquiryCon, AdminCon, EnquiryID, EnquiryResultID, dsDataSegments, true, oSubscriberEnquiryResult.VoucherCode);
                            break;
                        case 91:
                            XDSPortalEnquiry.Business.KYCEnquiry dtKYC = new XDSPortalEnquiry.Business.KYCEnquiry();
                            rp = dtKYC.SubmitKYCEnquiry(EnquiryCon, AdminCon, EnquiryID, EnquiryResultID, dsDataSegments, true, oSubscriberEnquiryResult.VoucherCode);
                            break;
                        case 94:
                            XDSPortalEnquiry.Business.MultipleCreditGrantor_CreditEnquiry dtConsumerICheck = new XDSPortalEnquiry.Business.MultipleCreditGrantor_CreditEnquiry();
                            rp = dtConsumerICheck.SubmitConsumerICheckEnquiry(EnquiryCon, AdminCon, EnquiryID, EnquiryResultID, dsDataSegments, true, oSubscriberEnquiryResult.VoucherCode);
                            break;
                        case 95:
                            XDSPortalEnquiry.Business.MultipleCreditGrantor_CreditEnquiry dtConsumerScoreIndicator = new XDSPortalEnquiry.Business.MultipleCreditGrantor_CreditEnquiry();
                            rp = dtConsumerScoreIndicator.SubmitConsumerScoreIndicator(EnquiryCon, AdminCon, EnquiryID, EnquiryResultID, dsDataSegments, true, oSubscriberEnquiryResult.VoucherCode);
                            break;
                        case 96:
                            XDSPortalEnquiry.Business.ContactTrace_ConsumerTrace dtContactTrace = new XDSPortalEnquiry.Business.ContactTrace_ConsumerTrace();
                            rp = dtContactTrace.SubmitMultipleContactTrace(EnquiryCon, AdminCon, EnquiryID, EnquiryResultID, dsDataSegments, true, oSubscriberEnquiryResult.VoucherCode);
                            break;
                        case 101:
                            XDSPortalEnquiry.Business.MultipleTrace_IdentityVerification dtMultipleIDPhoto = new XDSPortalEnquiry.Business.MultipleTrace_IdentityVerification();
                            rp = dtMultipleIDPhoto.SubmitMultipleIdPhotoVerification(EnquiryCon, AdminCon, oSubscriberEnquiryResult,
                                dsDataSegments, true, oSubscriberEnquiryResult.VoucherCode,
                                strPBSAUsername, strPBSAPassword, strPBSAMemberKey, strPBSATerminalID, strPBSAURL);
                            break;
                        default:
                            break;
                    }
                    switch (rp.ResponseStatus)
                    {
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single:
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple:
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                            rsXml.LoadXml(rp.ResponseData);
                            break;

                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                            rXml = "<NoResult><Error>" + rp.ResponseData + "</Error></NoResult>";
                            rsXml.LoadXml(rXml);
                            break;
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                            rXml = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                            rsXml.LoadXml(rXml);
                            break;
                    }
                }

                catch (Exception oException)
                {
                    rXml = "<Error>(.NetException) " + oException.Message + "</Error>";
                    rsXml.LoadXml(rXml);
                }

            }
            else
            {
                rXml = "<NoResult><Error>Invalid Ticket</Error></NoResult>";
                rsXml.LoadXml(rXml);
            }

            return rsXml.OuterXml;

        }

        [WebMethod(Description = "Returns a list of Credit Enquiry Reasons")]
        public string ConnectGetEnquiryReasons(string ConnectTicket)
        {
            string rXml = "";
            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();

            if (IsTicketValid(ConnectTicket))
            {
                try
                {
                    rXml = "<CreditEnquiryReasons>";
                    rXml = rXml + "<EnquiryReason>Credit assessment</EnquiryReason>";
                    rXml = rXml + "<EnquiryReason>Investigation into Fraud Corruption or Theft by SAPS or State Agency</EnquiryReason>";
                    rXml = rXml + "<EnquiryReason>Application for employment</EnquiryReason>";
                    rXml = rXml + "<EnquiryReason>Account limit determination - Supply of goods, services or utilities</EnquiryReason>";
                    rXml = rXml + "<EnquiryReason>Assessing an application for insurance</EnquiryReason>";
                    rXml = rXml + "<EnquiryReason>Employment or qualifications verification</EnquiryReason>";
                    rXml = rXml + "<EnquiryReason>Affordability Assessment</EnquiryReason>";
                    rXml = rXml + "<EnquiryReason>Assessing a Debt Review Application</EnquiryReason>";
                    rXml = rXml + "<EnquiryReason>Fraud Detection and Prevention</EnquiryReason>";
                    rXml = rXml + "<EnquiryReason>Assessment of a debtor book</EnquiryReason>";
                    rXml = rXml + "<EnquiryReason>Tracing a consumer to distribute unclaimed funds</EnquiryReason>";
                    rXml = rXml + "<EnquiryReason>Tracing a consumer on a credit agreement</EnquiryReason>";
                    rXml = rXml + "<EnquiryReason>Developing a Credit Scoring System</EnquiryReason>";
                    rXml = rXml + "</CreditEnquiryReasons>";
                }

                catch (Exception oException)
                {
                    rXml = "<Error>" + oException.Message + "</Error>";
                }
            }
            else
            {
                rXml = "<NoResult><Error>Invalid Ticket</Error></NoResult>";
            }

            //rXml = "<Tickets><Ruddy>a" + ConnectTicket + "</Ruddy></Tickets>";
            return rXml;

        }

        [WebMethod(Description = "Submit a Request to get the Subscriber Profile ")]
        public string ConnectFraudGetProfile(string ConnectTicket)
        {
            string rXml = "";
            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();
            DataSet dsresult = new DataSet("Profile");

            if (IsTicketValid(ConnectTicket))
            {
                FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);

                try
                {
                    XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                    XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(AdminCon.ConnectionString, int.Parse(oTicket.Name));

                    XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
                    XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(AdminCon.ConnectionString, oSystemUser.SubscriberID);
                    XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

                    DataTable dBranches = new DataTable();
                    dBranches = odSubscriber.GetBranches(AuthCon.ConnectionString, oSystemUser.SubscriberID).Tables[0].Copy();
                    dBranches.TableName = "Branches";
                    DataTable dSubscriberProfile = new DataTable();
                    dSubscriberProfile = odSubscriber.GetSubscriberProfile(AuthCon.ConnectionString, oSystemUser.SubscriberID).Tables[0].Copy();
                    dSubscriberProfile.TableName = "SubscriberProfile";
                    DataTable dPurposes = new DataTable();
                    dPurposes = odSubscriber.GetPurposes(AuthCon.ConnectionString, oSystemUser.SubscriberID).Tables[0].Copy();
                    dPurposes.TableName = "Purposes";

                    dsresult.Tables.Add(dBranches);
                    dsresult.Tables.Add(dSubscriberProfile);
                    dsresult.Tables.Add(dPurposes);

                    rXml = dsresult.GetXml();
                    rsXml.LoadXml(rXml);
                }

                catch (Exception oException)
                {
                    rXml = "<NoResult><Error>" + oException.Message + "</Error></NoResult>";
                    rsXml.LoadXml(rXml);
                }
            }
            else
            {
                rXml = "<NoResult><Error>Invalid Ticket</Error></NoResult>";
                rsXml.LoadXml(rXml);
            }

            return rsXml.OuterXml;

        }

        [WebMethod(Description = "Process a Consumer match based on ID Number/Passport or Telephone No or Account No and returns matched Consumers")]
        public string ConnectFraudConsumerMatch(string ConnectTicket, AuthenticationSearchType SearchType, string BranchCode,
            int PurposeID, string IdNumber, string PassportNo, string CellularCode, string CellularNo, string AccountNo,
            string SubaccountNo, string YourReference, string VoucherCode)
        {
            string rXml = "";
            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();

            if (IsTicketValid(ConnectTicket))
            {
                FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);

                try
                {
                    XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                    XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(AdminCon.ConnectionString, int.Parse(oTicket.Name));

                    XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
                    XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(AdminCon.ConnectionString, oSystemUser.SubscriberID);
                    XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

                    XDSPortalAuthentication.AuthenticationConsumer dtConsumertraceMS = new XDSPortalAuthentication.AuthenticationConsumer();
                    //rp = dtConsumertraceMS.SubmitConsumerTrace(AuthEnquiryCon, AuthCon, oSubscriber.SubscriberID, oSystemUser.SystemUserID, 1, oSubscriber.SubscriberName, SearchType, BranchCode, PurposeID, IdNumber, PassportNo, CellularCode, CellularNo, AccountNo, SubaccountNo, OverrideOTP, OverrideOTPReason, YourReference, true, VoucherCode);
                    rp = dtConsumertraceMS.SubmitConsumerTrace(AuthEnquiryCon, AuthCon, oSubscriber.SubscriberID, oSystemUser.SystemUserID, 1, oSubscriber.SubscriberName, SearchType, BranchCode, PurposeID, IdNumber, PassportNo, CellularCode, CellularNo, AccountNo, SubaccountNo, false, "", "", YourReference, true, VoucherCode);

                    switch (rp.ResponseStatus)
                    {
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single:
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple:
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                            rsXml.LoadXml(rp.ResponseData);
                            break;

                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                            rXml = "<NoResult><Error>" + rp.ResponseData + "</Error></NoResult>";
                            rsXml.LoadXml(rXml);
                            break;
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                            rXml = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                            rsXml.LoadXml(rXml);
                            break;
                    }

                }

                catch (Exception oException)
                {
                    rXml = "<NoResult><Error>" + oException.Message + "</Error></NoResult>";
                    rsXml.LoadXml(rXml);
                }
            }
            else
            {
                rXml = "<NoResult><Error>Invalid Ticket</Error></NoResult>";
                rsXml.LoadXml(rXml);
            }

            return rsXml.OuterXml;

        }

        [WebMethod(Description = "Submit a Request to get the authentication questions")]
        public AuthenticationProcess ConnectFraudGetQuestions(string ConnectTicket, int EnquiryID, int EnquiryResultID)
        {
            //string rXml = "";
            //System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();
            //AuthenticationProcess moMyAuthenticationManager = null;

            //if (IsTicketValid(ConnectTicket))
            //{
            //    FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);

            //    try
            //    {

            //        XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
            //        XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(AdminCon.ConnectionString, int.Parse(oTicket.Name));

            //        XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
            //        XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(AdminCon.ConnectionString, oSystemUser.SubscriberID);
            //        XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            //        XDSPortalEnquiry.Data.SubscriberEnquiry dse = new XDSPortalEnquiry.Data.SubscriberEnquiry();
            //        XDSPortalEnquiry.Entity.SubscriberEnquiry se = new XDSPortalEnquiry.Entity.SubscriberEnquiry();
            //        XDSPortalEnquiry.Data.SubscriberEnquiryResult dsec = new XDSPortalEnquiry.Data.SubscriberEnquiryResult();
            //        XDSPortalEnquiry.Data.SubscriberEnquiryResultBonus dsecb = new XDSPortalEnquiry.Data.SubscriberEnquiryResultBonus();

            //        XDSPortalEnquiry.Entity.SubscriberEnquiryResult sec = dsec.GetSubscriberEnquiryResultObject(AuthEnquiryCon, EnquiryResultID);

            //        //Get the Subscriber Enquiry Record Details
            //        se = dse.GetSubscriberEnquiryObject(EnquiryCon, sec.SubscriberEnquiryID);


            //        if (AdminCon.State == ConnectionState.Closed)
            //            AdminCon.Open();

            //        string strSQL = "SELECT Connectionstring from Authenticationserver";
            //        SqlCommand Imycommand = new SqlCommand(strSQL, AdminCon);
            //        SqlDataReader Idr = Imycommand.ExecuteReader();
            //        Idr.Read();
            //        string authadmincon = Idr.GetValue(0).ToString();
            //        Idr.Close();
            //        AdminCon.Close();

            //        moMyAuthenticationManager = new AuthenticationProcess(oSystemUser.Username, se.BranchCode, se.PurposeID, Properties.Settings.Default.AuthConnectionstring.ToString(), Properties.Settings.Default.AuthEnquiryConnection.ToString(), authadmincon);

            //        moMyAuthenticationManager.NewAuthentication(se, sec);


            //    }

            //    catch (Exception oException)
            //    {

            //    }
            //}
            //else
            //{
            //    moMyAuthenticationManager = null;
            //}

            //return moMyAuthenticationManager;

            AuthenticationProcess moMyAuthenticationManager = new AuthenticationProcess();

            try
            {

                if (IsTicketValid(ConnectTicket))
                {
                    FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);

                    try
                    {

                        XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                        XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(AdminCon.ConnectionString, int.Parse(oTicket.Name));

                        XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
                        XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(AdminCon.ConnectionString, oSystemUser.SubscriberID);

                        XDSPortalEnquiry.Business.FraudEnquiry oFraudEnquiry = new XDSPortalEnquiry.Business.FraudEnquiry(AdminCon);

                        XDSPortalEnquiry.Data.SubscriberEnquiryResult dsec = new XDSPortalEnquiry.Data.SubscriberEnquiryResult();
                        XDSPortalEnquiry.Entity.SubscriberEnquiryResult sec = dsec.GetSubscriberEnquiryResultObject(AuthEnquiryCon, EnquiryResultID);


                        moMyAuthenticationManager = oFraudEnquiry.GetQuestions(EnquiryCon, AdminCon, SMSCon, sec.SubscriberEnquiryID, EnquiryResultID, strSmtpServerName, strSmtpUserLoginID, strSmtpUserPassword, intport);


                    }

                    catch (Exception oException)
                    {

                        //XDSPortalEnquiry.Data.Authentication dAuthentication = new XDSPortalEnquiry.Data.Authentication();
                        //dAuthentication.AuthenticationError(AuthCon, 0, 0, EnquiryID, EnquiryResultID, oException.Message, "ConnectFraudGetQuestions1");
                        moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.ErrorMessage = oException.Message;
                    }
                }
                else
                {
                    throw new Exception("Invalid ticket");
                }
            }
            catch (Exception ex)
            {
                moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.ErrorMessage = ex.Message;
            }

            return moMyAuthenticationManager;

        }

        [WebMethod(Description = "Submit a Request to Complete or Cancel an authentication")]
        public AuthenticationProcess ConnectFraudProcess(string ConnectTicket, AuthenticationProcessAction ProcesAction, AuthenticationProcess MyAuthenticationProcess, string Comment)
        {
            //    string rXml = "";
            //    System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();

            //    if (IsTicketValid(ConnectTicket))
            //    {
            //        FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);

            //        XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
            //        XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(AdminCon.ConnectionString, int.Parse(oTicket.Name));

            //        if (ProcesAction == AuthenticationProcessAction.Authenticate)
            //        {
            //            MyAuthenticationProcess.Authenticate(Comment, oSystemUser.Username, AuthCon.ConnectionString, AuthEnquiryCon.ConnectionString);
            //        }
            //        else
            //        {
            //            MyAuthenticationProcess.VoidAuthentication(oSystemUser.Username, AuthCon.ConnectionString, AuthEnquiryCon.ConnectionString);
            //        }

            //    }
            //    else
            //    {
            //        MyAuthenticationProcess = null;
            //    }

            //    return MyAuthenticationProcess;

            #region[ Write To File ]
            if (IsTicketValid(ConnectTicket))
            {
                FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);

                XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(AdminCon.ConnectionString, int.Parse(oTicket.Name));

                if (oSystemUser.SubscriberID == 11651)
                {
                    DataSet dsAnswers = new DataSet();

                    XmlSerializer serializer1 = new XmlSerializer(MyAuthenticationProcess.GetType());
                    System.IO.StringWriter sw1 = new System.IO.StringWriter();
                    serializer1.Serialize(sw1, MyAuthenticationProcess);
                    System.IO.StringReader reader1 = new System.IO.StringReader(sw1.ToString());
                    dsAnswers.ReadXml(reader1);

                    File.WriteAllText("C:\\AuthLogFiles\\AuthLogFile_" + oSystemUser.SubscriberID + "_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".txt",
                        dsAnswers.GetXml());

                    sw1.Close();
                    sw1.Dispose();

                    reader1.Close();
                    reader1.Dispose();
                }
            }
            #endregion


            XDSPortalEnquiry.Business.FraudEnquiry oFraudEnquiry = new XDSPortalEnquiry.Business.FraudEnquiry(AdminCon);

            try
            {

                if (IsTicketValid(ConnectTicket))
                {
                    FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);

                    if (ProcesAction == AuthenticationProcessAction.Authenticate)
                    {
                        MyAuthenticationProcess = oFraudEnquiry.AuthenticationProcess(EnquiryCon, AdminCon, SMSCon, MyAuthenticationProcess, strSmtpServerName, strSmtpUserLoginID, strSmtpUserPassword, intport);
                    }
                    else if (ProcesAction == AuthenticationProcessAction.Void)
                    {
                        MyAuthenticationProcess = oFraudEnquiry.AuthenticationProcessVoid(EnquiryCon, AdminCon, MyAuthenticationProcess, Comment, strSmtpServerName, strSmtpUserLoginID, strSmtpUserPassword, intport);
                    }
                    else if (ProcesAction == AuthenticationProcessAction.ReferToFraud)
                    {
                        MyAuthenticationProcess = oFraudEnquiry.AuthenticationProcessReferToFraud(EnquiryCon, AdminCon, MyAuthenticationProcess, Comment, strSmtpServerName, strSmtpUserLoginID, strSmtpUserPassword, intport);
                    }

                }
                else
                {
                    throw new Exception("Invalid ticket");
                }
            }
            catch (Exception ex)
            {
                MyAuthenticationProcess.CurrentObjectState.AuthenticationDocument.ErrorMessage = ex.Message;
            }

            return MyAuthenticationProcess;

        }

        [WebMethod(Description = "Submit a Request to Process the Authentication")]
        public string ConnectFraudSavePersonalQuestions(string ConnectTicket, AuthenticationProcess MyAuthenticationProcess, string Comment)
        {
            string rXml = "";
            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();

            XDSPortalEnquiry.Business.FraudEnquiry oFraudEnquiry = new XDSPortalEnquiry.Business.FraudEnquiry(AdminCon);
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                if (IsTicketValid(ConnectTicket))
                {
                    try
                    {
                        FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);
                        rp = oFraudEnquiry.AuthenticationProcessSavePersonalQuestions(EnquiryCon, AdminCon, MyAuthenticationProcess);
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            rXml = "<NoResult>" + rp.ResponseData + "</NoResult>";
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            rXml = "<PersonalQuestions><Result>" + rp.ResponseData + "</Result></PersonalQuestions>";
                        }
                    }
                    catch (Exception ex)
                    {
                        rXml = "<NoResult><Error>" + ex.Message + "</Error></NoResult>";
                    }
                }
                else
                {
                    throw new Exception("Invalid ticket");
                }
            }
            catch (Exception ex)
            {
                rXml = "<NoResult><Error>" + ex.Message + "</Error></NoResult>";
            }
            rsXml.LoadXml(rXml);
            return rsXml.OuterXml;

        }

        [WebMethod(Description = "Submit a Request for getting the Blocked consumers")]
        public string ConnectFraudGetBlockedConsumers(string ConnectTicket)
        {
            string rXml = "";
            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            XDSPortalEnquiry.Business.FraudEnquiry oFraudEnquiry = new XDSPortalEnquiry.Business.FraudEnquiry(AdminCon);
            try
            {

                if (IsTicketValid(ConnectTicket))
                {
                    FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);
                    try
                    {
                        rp = oFraudEnquiry.GetBlockedConsumers(AdminCon, int.Parse(oTicket.Name));
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        { rXml = "<NoResult>" + rp.ResponseData + "</NoResult>"; }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        { rXml = rp.ResponseData; }

                    }
                    catch (Exception ex)
                    {
                        rXml = "<NoResult><Error>" + ex.Message + "</Error></NoResult>";
                    }

                }
                else
                {
                    throw new Exception("Invalid ticket");
                }
            }
            catch (Exception ex)
            {
                rXml = "<NoResult><Error>" + ex.Message + "</Error></NoResult>";
            }
            rsXml.LoadXml(rXml);
            return rsXml.OuterXml;
        }

        [WebMethod(Description = "Submit a Request for getting the Blocked consumers")]
        public string ConnectFraudUnBlockConsumer(string ConnectTicket, int BlockID, string Comment)
        {
            string rXml = "";
            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            XDSPortalEnquiry.Business.FraudEnquiry oFraudEnquiry = new XDSPortalEnquiry.Business.FraudEnquiry(AdminCon);

            try
            {

                if (IsTicketValid(ConnectTicket))
                {
                    FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);
                    try
                    {
                        rp = oFraudEnquiry.UnblockConsumer(AdminCon, BlockID, int.Parse(oTicket.Name));
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        { rXml = "<NoResult>" + rp.ResponseData + "</NoResult>"; }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        { rXml = "<UnBlockConsumer><Result>" + rp.ResponseData + "</Result></UnBlockConsumer>"; }

                    }
                    catch (Exception ex)
                    {
                        rXml = "<NoResult><Error>" + ex.Message + "</Error></NoResult>";
                    }

                }
            }
            catch (Exception ex)
            {
                rXml = "<NoResult><Error>" + ex.Message + "</Error></NoResult>";
            }
            rsXml.LoadXml(rXml);
            return rsXml.OuterXml;
        }

    }
}