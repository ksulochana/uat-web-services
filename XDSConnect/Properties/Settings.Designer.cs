﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace XDSConnect.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "17.9.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=192.168.9.67;Initial Catalog=Rental_Genie_Online;User Id=onlineaccess" +
            ";password=0nl1n34cc355@xd5")]
        public string RentalGenie {
            get {
                return ((string)(this["RentalGenie"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=192.168.9.67;User ID=onlineaccess;Password=0nl1n34cc355@xd5;database=" +
            "BankStatement;")]
        public string BankStatment {
            get {
                return ((string)(this["BankStatment"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=192.168.9.67;Initial Catalog=XX_AdminDB;User Id=onlineaccess;password" +
            "=0nl1n34cc355@xd5")]
        public string RealTimeConnection {
            get {
                return ((string)(this["RealTimeConnection"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=192.168.9.67;Initial Catalog=XX_AdminDB;User Id=onlineaccess;password" +
            "=0nl1n34cc355@xd5")]
        public string AdminConnectionString {
            get {
                return ((string)(this["AdminConnectionString"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=192.168.9.67;Initial Catalog=XX_EnquiryDB;User Id=onlineaccess;passwo" +
            "rd=0nl1n34cc355@xd5")]
        public string EnquiryConnectionString {
            get {
                return ((string)(this["EnquiryConnectionString"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=192.168.9.67;Initial Catalog=XX_AdminDB;User Id=onlineaccess;password" +
            "=0nl1n34cc355@xd5")]
        public string AuthConnectionstring {
            get {
                return ((string)(this["AuthConnectionstring"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=192.168.9.67;Initial Catalog=XX_EnquiryDB;User Id=onlineaccess;passwo" +
            "rd=0nl1n34cc355@xd5")]
        public string AuthEnquiryConnection {
            get {
                return ((string)(this["AuthEnquiryConnection"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=192.168.9.67;Initial Catalog=XX_SMScity;User Id=onlineaccess;password" +
            "=0nl1n34cc355@xd5")]
        public string SMSDBConnectionString {
            get {
                return ((string)(this["SMSDBConnectionString"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=192.168.9.67;Initial Catalog=XX_AdminDB;User Id=onlineaccess;password" +
            "=0nl1n34cc355@xd5")]
        public string AuthAdminConnection {
            get {
                return ((string)(this["AuthAdminConnection"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=192.168.9.67;Initial Catalog=XX_AccountverificationService;User Id=on" +
            "lineaccess;password=0nl1n34cc355@xd5")]
        public string AVSConnection {
            get {
                return ((string)(this["AVSConnection"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=192.168.9.67;Initial Catalog=XX_BankCodes;User Id=onlineaccess;passwo" +
            "rd=0nl1n34cc355@xd5")]
        public string BCConnection {
            get {
                return ((string)(this["BCConnection"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=192.168.9.67;Initial Catalog=XDSTracker;User Id=onlineaccess;password" +
            "=0nl1n34cc355@xd5")]
        public string TrackerConnectionString {
            get {
                return ((string)(this["TrackerConnectionString"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=192.168.9.67;Initial Catalog=Identicate;User Id=onlineaccess;password" +
            "=0nl1n34cc355@xd5")]
        public string IdenticateConnection {
            get {
                return ((string)(this["IdenticateConnection"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=192.168.9.67;Initial Catalog=XX_EnquiryLog;User Id=onlineaccess;passw" +
            "ord=0nl1n34cc355@xd5")]
        public string AVSConnectionEnquiryDB {
            get {
                return ((string)(this["AVSConnectionEnquiryDB"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=192.168.9.67;Initial Catalog=XX_EnquiryLog;User Id=onlineaccess;passw" +
            "ord=0nl1n34cc355@xd5")]
        public string BCConnectionEnquiryDB {
            get {
                return ((string)(this["BCConnectionEnquiryDB"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=192.168.9.67;Initial Catalog=XX_EnquiryLog;User Id=onlineaccess;passw" +
            "ord=0nl1n34cc355@xd5")]
        public string BIVConnectionEnquiryDB {
            get {
                return ((string)(this["BIVConnectionEnquiryDB"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=XDS-DEVSVR;Initial Catalog=BatchBankingEnquiries;User Id=onlineaccess" +
            ";Password=P@ssw0rd")]
        public string BatchAVS {
            get {
                return ((string)(this["BatchAVS"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=192.168.9.67;Initial Catalog=XX_XDSLog;User ID=onlineaccess;Password=" +
            "0nl1n34cc355@xd5")]
        public string XDSLog {
            get {
                return ((string)(this["XDSLog"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=192.168.9.67;Initial Catalog=XX_SABULKREPORT;User ID=onlineaccess;Pas" +
            "sword=0nl1n34cc355@xd5")]
        public string BatchReport {
            get {
                return ((string)(this["BatchReport"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=192.168.9.67;Initial Catalog=XDSCommercialBL;User ID=onlineaccess;Pas" +
            "sword=0nl1n34cc355@xd5")]
        public string MTNCommercialQuestionnaire {
            get {
                return ((string)(this["MTNCommercialQuestionnaire"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=192.168.9.67;Initial Catalog=XX_EnquiryDB;User Id=onlineaccess;passwo" +
            "rd=0nl1n34cc355@xd5")]
        public string MTNCustomVetting {
            get {
                return ((string)(this["MTNCustomVetting"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=192.168.9.67;Initial Catalog=IDM_ONLINE;User Id=onlineaccess;password" +
            "=0nl1n34cc355@xd5")]
        public string IDMONLINE {
            get {
                return ((string)(this["IDMONLINE"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=192.168.9.49;Initial Catalog=APPLICATIONLOG;User Id=onlineaccess;pass" +
            "word=0nl1n34cc355@xd5")]
        public string IPLogConnection {
            get {
                return ((string)(this["IPLogConnection"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=192.168.9.67;Initial Catalog=BatchCommercialScore;User Id=onlineacces" +
            "s;Password=0nl1n34cc355@xd5")]
        public string BatchCommercialScore {
            get {
                return ((string)(this["BatchCommercialScore"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.SpecialSettingAttribute(global::System.Configuration.SpecialSetting.ConnectionString)]
        [global::System.Configuration.DefaultSettingValueAttribute("Data Source=192.168.9.67;Initial Catalog=XX_AdminDB;User Id=onlineaccess;password" +
            "=0nl1n34cc355@xd5")]
        public string IDVConnectionString {
            get {
                return ((string)(this["IDVConnectionString"]));
            }
        }
    }
}
