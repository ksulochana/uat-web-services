﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace XDSConnect
{
    public class RequestFile
    {
        private string _FileName;

        private byte[] _ReportFile;

        public RequestFile()
        {
          
        }

        public string FileName
        {
            get
            {
                return this._FileName;
            }
            set
            {
                this._FileName = value;
            }
        }

        public byte[] ReportFile
        {
            get
            {
                return this._ReportFile;
            }
            set
            {
                this._ReportFile = value;
            }
        }
    }
}
