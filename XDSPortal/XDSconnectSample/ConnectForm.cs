﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Xml;
using XDSconnectSample.xdsconnect;
using System.Xml.Serialization;
using System.Data.SqlClient;
using System.IO;
using System.Collections;
using Microsoft.Reporting.WinForms;
using System.Text.RegularExpressions;

namespace XDSconnectSample
{
    public partial class ConnectForm : Form
    {
        public string strToken = string.Empty;
        public string Username =ConfigurationManager.AppSettings["UserName"].ToString();
        public string Password = ConfigurationManager.AppSettings["Password"].ToString();
        public XDSconnectSample.xdsconnect.XDSConnectWS aXDSConnectWS;
        public XDSconnectSample.AuthWS.XDSConnectAuthWS aAuthConnectWS;
        public AuthWS.AuthenticationProcess moMyAuthenticationManager = new AuthWS.AuthenticationProcess();
        public AuthWS.AuthenticationSearchType searchtype = AuthWS.AuthenticationSearchType.ConsumerIdentity;
        public string Authenticationprocessaction = string.Empty;
        AuthWS.AuthenticationDocument odco = new AuthWS.AuthenticationDocument();
        
        protected bool bOverrideOTP;

        public ConnectForm()
        {
            InitializeComponent();
        }


        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                
                    if (treeView1.SelectedNode.GetNodeCount(true) == 0)
                    {
                        tabBlockedConsumers.SelectedIndex = int.Parse(treeView1.SelectedNode.Name);
                       
                    }
                
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

       // Get the Enquiry Reasons for Credit Enquiry on Form Load
        private void Form1_Load(object sender, EventArgs e)
        {

            //DataSet dsDataSegment = new DataSet();
            // dsDataSegment.ReadXml(new StringReader("<DataSegments>  <Segments>    <ConsumerID>0</ConsumerID>    <CommercialID>0</CommercialID>    <DirectorID>0</DirectorID>    <HomeAffairsID>0</HomeAffairsID>    <ExternalReferenceNo />    <AssociationCode>SCORE</AssociationCode>    <DataSegmentID>0</DataSegmentID>    <DataSegmentName />    <DataSegmentDisplayText />    <Active>false</Active>    <Limit>0</Limit>    <ReportID>98</ReportID>    <ReportName>JD Customvetting Report</ReportName>    <BonusPrice>0</BonusPrice>    <BonusViewed>false</BonusViewed>    <PayAsYouGo>false</PayAsYouGo>    <PayAsYouGoEnquiryLimit>88</PayAsYouGoEnquiryLimit>  </Segments></DataSegments>"));


            try
            {

                string resultString = Regex.Match("1478.0SQM", @"\d+").Value;

              
                    aXDSConnectWS = new XDSconnectSample.xdsconnect.XDSConnectWS();
                aAuthConnectWS = new AuthWS.XDSConnectAuthWS();
                txtUrl.Text = ConfigurationManager.AppSettings["URL"].ToString();
                    txtUserName.Text = ConfigurationManager.AppSettings["UserName"].ToString();
                    txtPassword.Text = ConfigurationManager.AppSettings["Password"].ToString();
                    lblStatus.Text = string.Empty;
             
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
          //  tabBlockedConsumers.SelectedTab = CreditEnquiry;


            //xdsconnect.QuestionDocument oq = new QuestionDocument();
            //XmlSerializer s = new XmlSerializer(typeof(xdsconnect.QuestionDocument));


            //string xml = "<?xml version=\"1.0\" encoding=\"utf-16\"?> <AuthenticationProcess  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"><CurrentObjectState><AuthenticationDocument><SubscriberID>12509</SubscriberID><SubscriberAuthenticationID>172108</SubscriberAuthenticationID><AuthenticationDate>2016-07-05T09:11:31.905+02:00</AuthenticationDate><AuthenticationStatusInd>A</AuthenticationStatusInd><TotalQuestionPointValue>0</TotalQuestionPointValue><RequiredAuthenticatedPerc>0.0</RequiredAuthenticatedPerc><AuthenticatedPerc>0.0</AuthenticatedPerc><ActionedBySystemUserID>0</ActionedBySystemUserID><SAFPSIndicator>false</SAFPSIndicator><IsUserBlocked>false</IsUserBlocked><PersonalQuestionsenabled>false</PersonalQuestionsenabled><BlockingEnabledFlag>false</BlockingEnabledFlag><ReferToFraud>false</ReferToFraud><blockid>0</blockid><BlockingReason /><QuestionTimeout>0</QuestionTimeout><ErrorMessage /><VoidorFraudReason /><NoofAttemptsRemaining>0</NoofAttemptsRemaining><OTPStatus>false</OTPStatus><OTPEnabled>false</OTPEnabled><OTPNotGeneratedReason /><OTPValue /><AuthenticationStatusReason>Authenticated</AuthenticationStatusReason><SAFPSMessage /><Questions><QuestionDocument><ProductAuthenticationQuestionID>5003</ProductAuthenticationQuestionID><Question>Do you receive statements at 22 SHAMROCK RD, -, KEMPTON PARK, GAUTENG, 1619?</Question><QuestionPointValue>0</QuestionPointValue><RequiredNoOfAnswers>0</RequiredNoOfAnswers><Answers><AnswerDocument><AnswerID>1195461</AnswerID><Answer>Yes</Answer><IsEnteredAnswerYN>true</IsEnteredAnswerYN></AnswerDocument></Answers></QuestionDocument><QuestionDocument><ProductAuthenticationQuestionID>8071</ProductAuthenticationQuestionID><Question>Are you married to NOMASOMI ANGELA?</Question><QuestionPointValue>0</QuestionPointValue><RequiredNoOfAnswers>0</RequiredNoOfAnswers><Answers><AnswerDocument><AnswerID>1195473</AnswerID><Answer>Yes</Answer><IsEnteredAnswerYN>true</IsEnteredAnswerYN></AnswerDocument></Answers></QuestionDocument><QuestionDocument><ProductAuthenticationQuestionID>8060</ProductAuthenticationQuestionID><Question>In what year did you purchase the property at 460, SHAMROCK, BREDELL AH EXT 2, BREDELL AH, KEMPTON PARK?</Question><QuestionPointValue>0</QuestionPointValue><RequiredNoOfAnswers>0</RequiredNoOfAnswers><Answers><AnswerDocument><AnswerID>1195463</AnswerID><Answer>1989</Answer><IsEnteredAnswerYN>true</IsEnteredAnswerYN></AnswerDocument></Answers></QuestionDocument><QuestionDocument><ProductAuthenticationQuestionID>8084</ProductAuthenticationQuestionID><Question>Name one of your exisiting or previous cell phone numbers?</Question><QuestionPointValue>0</QuestionPointValue><RequiredNoOfAnswers>0</RequiredNoOfAnswers><Answers><AnswerDocument><AnswerID>1195468</AnswerID><Answer>0836448898</Answer><IsEnteredAnswerYN>true</IsEnteredAnswerYN></AnswerDocument></Answers></QuestionDocument><QuestionDocument><ProductAuthenticationQuestionID>1014</ProductAuthenticationQuestionID><Question>Name your credit card accounts</Question><QuestionPointValue>0</QuestionPointValue><RequiredNoOfAnswers>0</RequiredNoOfAnswers><Answers><AnswerDocument><AnswerID>1195475</AnswerID><Answer>Absa Credit Card</Answer><IsEnteredAnswerYN>true</IsEnteredAnswerYN></AnswerDocument></Answers></QuestionDocument></Questions></AuthenticationDocument><CurrentQuestionNum>0</CurrentQuestionNum></CurrentObjectState></AuthenticationProcess>";

            ////string xml = @"<AuthenticationProcess><CurrentObjectState><AuthenticationDocument><SubscriberID>6696</SubscriberID><SubscriberAuthenticationID>172126</SubscriberAuthenticationID><AuthenticationDate>2016-07-05T10:01:30</AuthenticationDate><AuthenticationStatusInd>P</AuthenticationStatusInd><AuthenticationTypeInd>S</AuthenticationTypeInd><ReferenceNo>A172126</ReferenceNo><TotalQuestionPointValue>155</TotalQuestionPointValue><RequiredAuthenticatedPerc>80.0000</RequiredAuthenticatedPerc><AuthenticatedPerc>0.0000</AuthenticatedPerc><RepeatAuthenticationMessage>MR JOHN DOE has failed authentication on the 2016/06/02 at the branch: XDS Development</RepeatAuthenticationMessage><ActionedBySystemUserID>18947</ActionedBySystemUserID><FirstName>JOHN</FirstName><Surname>DOE</Surname><IDNo>0000000000000</IDNo><BirthDate>1980/01/01</BirthDate><Gender>M</Gender><Ha_IDIssuedDate/><ConsumerAccountAgeMessage>Consumer's Vodacom account is 2 year(s) and 11 month(s) old Consumer's Multichoice account is 4 year(s) and 4 month(s) old </ConsumerAccountAgeMessage><EncryptedReferenceNo/><AuthenticationComment/><SAFPSIndicator>false</SAFPSIndicator><IsUserBlocked>false</IsUserBlocked><PersonalQuestionsenabled>false</PersonalQuestionsenabled><BlockingEnabledFlag>false</BlockingEnabledFlag><ReferToFraud>false</ReferToFraud><blockid>0</blockid><BlockingReason/><QuestionTimeout>180</QuestionTimeout><ErrorMessage/><VoidorFraudReason/><NoofAttemptsRemaining>0</NoofAttemptsRemaining><OTPStatus>false</OTPStatus><OTPEnabled>false</OTPEnabled><OTPNotGeneratedReason/><OTPValue/><AuthenticationStatusReason>NotProcessed</AuthenticationStatusReason><SAFPSMessage>Not Listed on SAFPS</SAFPSMessage><Questions><QuestionDocument><ProductAuthenticationQuestionID>26005</ProductAuthenticationQuestionID><Question>Name one of your work phone numbers?</Question><AnswerStatusInd>P</AnswerStatusInd><QuestionPointValue>86</QuestionPointValue><RequiredNoOfAnswers>1</RequiredNoOfAnswers><Answers><AnswerDocument><AnswerID>1195846</AnswerID><Answer>0114657323</Answer><IsEnteredAnswerYN>false</IsEnteredAnswerYN></AnswerDocument><AnswerDocument><AnswerID>1195847</AnswerID><Answer>0118070998</Answer><IsEnteredAnswerYN>true</IsEnteredAnswerYN></AnswerDocument><AnswerDocument><AnswerID>1195848</AnswerID><Answer>0118860592</Answer><IsEnteredAnswerYN>false</IsEnteredAnswerYN></AnswerDocument><AnswerDocument><AnswerID>1195849</AnswerID><Answer>0357896918</Answer><IsEnteredAnswerYN>false</IsEnteredAnswerYN></AnswerDocument><AnswerDocument><AnswerID>1195850</AnswerID><Answer>None of these</Answer><IsEnteredAnswerYN>false</IsEnteredAnswerYN></AnswerDocument></Answers></QuestionDocument><QuestionDocument><ProductAuthenticationQuestionID>28011</ProductAuthenticationQuestionID><Question>Were you born in 1980?</Question><AnswerStatusInd>P</AnswerStatusInd><QuestionPointValue>69</QuestionPointValue><RequiredNoOfAnswers>1</RequiredNoOfAnswers><Answers><AnswerDocument><AnswerID>1195844</AnswerID><Answer>Yes</Answer><IsEnteredAnswerYN>true</IsEnteredAnswerYN></AnswerDocument><AnswerDocument><AnswerID>1195845</AnswerID><Answer>No</Answer><IsEnteredAnswerYN>false</IsEnteredAnswerYN></AnswerDocument></Answers></QuestionDocument></Questions><AuthenticationHistory><AuthenticationHistoryDocument><BureauDate>2016/07/01 04:09:13 PM</BureauDate><SubscriberName>Zailab</SubscriberName><cellularNumber/><AuthenticationStatus>Pending</AuthenticationStatus><AuthenticationPerc>0.0000</AuthenticationPerc><EmailAddress/></AuthenticationHistoryDocument><AuthenticationHistoryDocument><BureauDate>2016/06/28 11:22:03 AM</BureauDate><SubscriberName>XDS Call Centre V2.2</SubscriberName><cellularNumber>0727950315</cellularNumber><AuthenticationStatus>Pending</AuthenticationStatus><AuthenticationPerc>0.0000</AuthenticationPerc><EmailAddress/></AuthenticationHistoryDocument><AuthenticationHistoryDocument><BureauDate>2016/06/28 11:21:18 AM</BureauDate><SubscriberName>XDS Call Centre V2.2</SubscriberName><cellularNumber>0727950315</cellularNumber><AuthenticationStatus>Not Authenticated</AuthenticationStatus><AuthenticationPerc>35.0877</AuthenticationPerc><EmailAddress/></AuthenticationHistoryDocument><AuthenticationHistoryDocument><BureauDate>2016/06/28 11:19:39 AM</BureauDate><SubscriberName>XDS Call Centre V2.2</SubscriberName><cellularNumber>0727950315</cellularNumber><AuthenticationStatus>Pending</AuthenticationStatus><AuthenticationPerc>0.0000</AuthenticationPerc><EmailAddress/></AuthenticationHistoryDocument><AuthenticationHistoryDocument><BureauDate>2016/06/28 11:14:40 AM</BureauDate><SubscriberName>XDS Call Centre V2.2</SubscriberName><cellularNumber>0817108152</cellularNumber><AuthenticationStatus>Pending</AuthenticationStatus><AuthenticationPerc>0.0000</AuthenticationPerc><EmailAddress/></AuthenticationHistoryDocument></AuthenticationHistory><VoidReasons/></AuthenticationDocument><CurrentQuestionNum>0</CurrentQuestionNum></CurrentObjectState></AuthenticationProcess>";

            //// oq = (QuestionDocument)s.Deserialize(new XmlTextReader(new StringReader(xml)));
            //XmlSerializer serializer = new XmlSerializer(typeof(xdsconnect.AuthenticationProcess));

            ////string xml = @"<AuthenticationProcess><CurrentObjectState><AuthenticationDocument><SubscriberID>6696</SubscriberID><SubscriberAuthenticationID>172126</SubscriberAuthenticationID></AuthenticationDocument><CurrentQuestionNum>0</CurrentQuestionNum></CurrentObjectState></AuthenticationProcess>";

            ////StringReader sr = new StringReader(xml);
            ////XmlTextReader xr = new XmlTextReader(sr);


            ////moMyAuthenticationManager = (xdsconnect.AuthenticationProcess)serializer.Deserialize(xr);

            
            ////odco.SubscriberID = 12509;
            ////odco.SubscriberAuthenticationID = 172108;

            ////ArrayList oQuestions = new ArrayList();

            ////QuestionDocument oQuestionDocument = new QuestionDocument();

            ////oQuestionDocument.ProductAuthenticationQuestionID = 5003;
            ////oQuestionDocument.Question = "Do you receive statements at 22 SHAMROCK RD, -, KEMPTON PARK, GAUTENG, 1619?";
            ////oQuestionDocument.QuestionPointValue = 0;
            ////oQuestionDocument.RequiredNoOfAnswers = 0;

            ////ArrayList oAnswers = new ArrayList();
            ////AnswerDocument oAnswerDocument = new AnswerDocument();


            ////oAnswerDocument.Answer = "Yes";
            ////oAnswerDocument.AnswerID = 1195461;
            ////oAnswerDocument.IsEnteredAnswerYN = true;
            ////oAnswers.Add(oAnswerDocument);

            ////oQuestionDocument.Answers = oAnswers.ToArray(typeof(AnswerDocument)) as AnswerDocument[];

            ////oQuestions.Add(oQuestionDocument);

            ////QuestionDocument oQuestionDocument1 = new QuestionDocument();

            ////oQuestionDocument1.ProductAuthenticationQuestionID = 8071;
            ////oQuestionDocument1.Question = "Are you married to NOMASOMI ANGELA?";
            ////oQuestionDocument1.QuestionPointValue = 0;
            ////oQuestionDocument1.RequiredNoOfAnswers = 0;

            ////ArrayList oAnswers1 = new ArrayList();
            ////AnswerDocument oAnswerDocument1 = new AnswerDocument();


            ////oAnswerDocument1.Answer = "Yes";
            ////oAnswerDocument1.AnswerID = 1195473;
            ////oAnswerDocument1.IsEnteredAnswerYN = true;
            ////oAnswers1.Add(oAnswerDocument1);

            ////oQuestionDocument1.Answers = oAnswers1.ToArray(typeof(AnswerDocument)) as AnswerDocument[];

            ////oQuestions.Add(oQuestionDocument1);


            ////QuestionDocument oQuestionDocument2 = new QuestionDocument();

            ////oQuestionDocument2.ProductAuthenticationQuestionID = 8060;
            ////oQuestionDocument2.Question = "In what year did you purchase the property at 460, SHAMROCK, BREDELL AH EXT 2, BREDELL AH, KEMPTON PARK?";
            ////oQuestionDocument2.QuestionPointValue = 0;
            ////oQuestionDocument2.RequiredNoOfAnswers = 0;

            ////ArrayList oAnswers2 = new ArrayList();
            ////AnswerDocument oAnswerDocument2 = new AnswerDocument();


            ////oAnswerDocument2.Answer = "1989";
            ////oAnswerDocument2.AnswerID = 1195463;
            ////oAnswerDocument2.IsEnteredAnswerYN = true;
            ////oAnswers2.Add(oAnswerDocument2);

            ////oQuestionDocument2.Answers = oAnswers2.ToArray(typeof(AnswerDocument)) as AnswerDocument[];

            ////oQuestions.Add(oQuestionDocument2);

            ////QuestionDocument oQuestionDocument3 = new QuestionDocument();

            ////oQuestionDocument3.ProductAuthenticationQuestionID = 8084;
            ////oQuestionDocument3.Question = "Name one of your exisiting or previous cell phone numbers?";
            ////oQuestionDocument3.QuestionPointValue = 0;
            ////oQuestionDocument3.RequiredNoOfAnswers = 0;

            ////ArrayList oAnswers3 = new ArrayList();
            ////AnswerDocument oAnswerDocument3 = new AnswerDocument();


            ////oAnswerDocument3.Answer = "0836448898";
            ////oAnswerDocument3.AnswerID = 1195468;
            ////oAnswerDocument3.IsEnteredAnswerYN = true;
            ////oAnswers3.Add(oAnswerDocument3);

            ////oQuestionDocument3.Answers = oAnswers3.ToArray(typeof(AnswerDocument)) as AnswerDocument[];

            ////oQuestions.Add(oQuestionDocument3);

            ////QuestionDocument oQuestionDocument4 = new QuestionDocument();

            ////oQuestionDocument4.ProductAuthenticationQuestionID = 1014;
            ////oQuestionDocument4.Question = "Name your credit card accounts";
            ////oQuestionDocument4.QuestionPointValue = 0;
            ////oQuestionDocument4.RequiredNoOfAnswers = 0;

            ////ArrayList oAnswers4 = new ArrayList();
            ////AnswerDocument oAnswerDocument4 = new AnswerDocument();


            ////oAnswerDocument4.Answer = "Absa Credit Card";
            ////oAnswerDocument4.AnswerID = 1195475;
            ////oAnswerDocument4.IsEnteredAnswerYN = true;
            ////oAnswers4.Add(oAnswerDocument4);

            ////oQuestionDocument4.Answers = oAnswers4.ToArray(typeof(AnswerDocument)) as AnswerDocument[];

            ////oQuestions.Add(oQuestionDocument4);

            ////odco.Questions = oQuestions.ToArray(typeof(QuestionDocument)) as QuestionDocument[];




            //this.reportViewer1.RefreshReport();
        }

        #region Credit Enquiry

        // Get the Credit Enquiry Reasons
        private void cboEnquiryReason_Click(object sender, EventArgs e)
        {
            try
            {
                if (cboEnquiryReason.Items.Count == 0)
                {
                    aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                    strToken = aXDSConnectWS.Login(Username, Password);
                    
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        string axml = aXDSConnectWS.ConnectGetEnquiryReasons(strToken);
                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(axml);
                        ds.ReadXml(xmlSR);
                        cboEnquiryReason.DataSource = ds.Tables[0];
                        cboEnquiryReason.DisplayMember = ds.Tables[0].Columns[0].ToString();
                        cboEnquiryReason.ValueMember = ds.Tables[0].Columns[0].ToString();
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }
        //Submit the consumer details for search
        private void submit0_Click(object sender, EventArgs e)
        {
            try
            {
                
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    txtmatch0.Text = aXDSConnectWS.ConnectConsumerMatch(strToken, cboEnquiryReason.Text, txtProductIDCr.Text == "" ? 15 : Int32.Parse(txtProductIDCr.Text), txtIDNo.Text, txtPassportNo.Text, txtFirstName.Text, txtSurname.Text, dtBirthDate.Value.ToShortDateString(), txtExtRef.Text, txtVoucherCode.Text);

                    DataSet ds = new DataSet();
                    System.IO.StringReader xmlSR = new System.IO.StringReader(txtmatch0.Text);
                    ds.ReadXml(xmlSR);
                    grid0.DataSource = ds.Tables[0];
                    
                }
                else
                {
                    txtmatch0.Text = aXDSConnectWS.ConnectConsumerMatch(strToken, cboEnquiryReason.Text, txtProductIDCr.Text == "" ? 15 : Int32.Parse(txtProductIDCr.Text), txtIDNo.Text, txtPassportNo.Text, txtFirstName.Text, txtSurname.Text, dtBirthDate.Value.ToShortDateString(), txtExtRef.Text, txtVoucherCode.Text);

                    DataSet ds = new DataSet();
                    System.IO.StringReader xmlSR = new System.IO.StringReader(txtmatch0.Text);
                    ds.ReadXml(xmlSR);
                    grid0.DataSource = ds.Tables[0];
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        // Request the Report XML
      private void view0_Click(object sender, EventArgs e)
        {
            try
            {
                int intEnquiryID = int.Parse(txtEnquiryID.Text);
                int intEnquiryResultID = int.Parse(txtEnquiryRID.Text);
                string strBonusXML = txtBonusXML.Text;
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    txtReportXML0.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0,strBonusXML);
                }
                else
                {
                    txtReportXML0.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0,strBonusXML);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        //Clear the data in fields
        private void clear0_Click(object sender, EventArgs e)
        {
            txtIDNo.Text = string.Empty;
            txtPassportNo.Text = string.Empty;
            txtSurname.Text = string.Empty;
            txtFirstName.Text = string.Empty;
            dtBirthDate.Text = string.Empty;
            txtExtRef.Text = string.Empty;
        }
#endregion Credit Enquiry

        #region Consumer Trace
        //Submit the consumer details for search
        private void btnSubmitct_Click(object sender, EventArgs e)
        {
            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtXMLMatchResultct.Text = aXDSConnectWS.ConnectConsumerMatch(strToken, null, 2, txtIDNoct.Text, txtPPNoct.Text, txtFirstNamect.Text, txtSurNamect.Text, dtpbirthDatect.Value.ToShortDateString(), txtXternalRefct.Text, txtVoucherCodect.Text);

                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMLMatchResultct.Text);
                        ds.ReadXml(xmlSR);
                        grdvwMatchResultct.DataSource = ds.Tables[0];
                    }
                    else
                    {
                        if (!(strToken==string.Empty))
                        {
                        MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtXMLMatchResultct.Text = aXDSConnectWS.ConnectConsumerMatch(strToken, null, 2, txtIDNoct.Text, txtPPNoct.Text, txtFirstNamect.Text, txtSurNamect.Text, dtpbirthDatect.Value.ToShortDateString(), txtXternalRefct.Text,txtVoucherCodect.Text);

                    DataSet ds = new DataSet();
                    System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMLMatchResultct.Text);
                    ds.ReadXml(xmlSR);
                    grdvwMatchResultct.DataSource = ds.Tables[0];
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }

        }
        //Request the Report XML
        private void btnviewct_Click(object sender, EventArgs e)
        {
            try
            {
                int intEnquiryID = int.Parse(txtEnquiryIDct.Text);
                int intEnquiryResultID = int.Parse(txtEnquiryResultIDct.Text);
                string strBonusXML = txtTraceplusXMLct.Text;
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
//                        txtReportXMLct.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0, strBonusXML);

                        byte[] obyte = aXDSConnectWS.ConnectGetResultBinaryReport(strToken, intEnquiryID, intEnquiryResultID, 0, strBonusXML,"html");
                        File.WriteAllBytes(@"C:\Users\skothamasu\Documents\DevX\htmlreport.html", obyte);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    //txtReportXMLct.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0,strBonusXML);

                    byte[] obyte = aXDSConnectWS.ConnectGetResultBinaryReport(strToken, intEnquiryID, intEnquiryResultID, 0, strBonusXML, "html");

                    
                    
                    string s = Convert.ToBase64String(obyte);

                    //ReportParameter  oparamter = new ReportParameter();

                    //oparamter.Name = "data";
                    //oparamter.Values.Add(s);

                    //List<ReportParameter> olist = new List<ReportParameter>();
                    //olist.Add(oparamter);

                    Stream sr = new MemoryStream(obyte);

                    reportViewer1.LocalReport.LoadReportDefinition(sr);

                    //reportViewer1.LocalReport.SetParameters(olist);

                    Warning[] warnings;
                    string[] streamIds;
                    string mimeType = string.Empty;
                    string encoding = string.Empty;
                    string extension = string.Empty;

                    LocalReport a = new LocalReport();
                    RenderingExtension[] x = a.ListRenderingExtensions();
                    


                    reportViewer1.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

                    
                    //reportViewer1.RefreshReport();

                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        //Clear the data in fields
        private void btnClearct_Click(object sender, EventArgs e)
        {
            txtIDNoct.Text = string.Empty;
            txtPPNoct.Text = string.Empty;
            txtFirstNamect.Text = string.Empty;
            txtSurNamect.Text = string.Empty;
            dtpbirthDatect.Text = string.Empty;
            txtXternalRefct.Text = string.Empty;
        }

        private void btnTracePlusct_Click(object sender, EventArgs e)
        {
            try
            {
                int intEnquiryID = int.Parse(txtEnquiryIDct.Text);
                int intEnquiryResultID = int.Parse(txtEnquiryResultIDct.Text);
                
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtTraceplusXMLct.Text = aXDSConnectWS.ConnectGetBonusSegments(strToken, intEnquiryResultID);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtTraceplusXMLct.Text = aXDSConnectWS.ConnectGetBonusSegments(strToken, intEnquiryResultID);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }


        #endregion Consumer Trace

        #region Account Trace
        //Submit the Account details for search
        private void btnSubmitAT_Click(object sender, EventArgs e)
        {
            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtXMLMatchResultAT.Text = aXDSConnectWS.ConnectAccountMatch(strToken, txtAccNoAT.Text, txtSubAccNoAT.Text, txtSurNameAT.Text, txtXternalRefNoAT.Text, txtVoucherCodeAT.Text);

                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMLMatchResultAT.Text);
                        ds.ReadXml(xmlSR);
                        grdvwMatchResultAT.DataSource = ds.Tables[0];
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtXMLMatchResultAT.Text = aXDSConnectWS.ConnectAccountMatch(strToken, txtAccNoAT.Text, txtSubAccNoAT.Text, txtSurNameAT.Text, txtXternalRefNoAT.Text,txtVoucherCodeAT.Text);

                    DataSet ds = new DataSet();
                    System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMLMatchResultAT.Text);
                    ds.ReadXml(xmlSR);
                    grdvwMatchResultAT.DataSource = ds.Tables[0];
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }
        //Request the Report XML
        private void btnViewAT_Click(object sender, EventArgs e)
        {
            try
            {
                int intEnquiryID = int.Parse(txtEnquiryIDAT.Text);
                int intEnquiryResultID = int.Parse(txtEnquiryResultIDAT.Text);
                string strBonusXML = txtTracePlusXMLAT.Text;
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                    txtReportXMLAT.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0,strBonusXML);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtReportXMLAT.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0,strBonusXML);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        //Clears the data in fields
        private void btnClearAT_Click(object sender, EventArgs e)
        {
            txtAccNoAT.Text = string.Empty;
            txtSubAccNoAT.Text = string.Empty;
            txtSurNameAT.Text = string.Empty;
            txtXternalRefNoAT.Text = string.Empty;
        }

        private void btnTracePlusAT_Click(object sender, EventArgs e)
        {
            try
            {
                int intEnquiryID = int.Parse(txtEnquiryIDAT.Text);
                int intEnquiryResultID = int.Parse(txtEnquiryResultIDAT.Text);
                
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtTracePlusXMLAT.Text = aXDSConnectWS.ConnectGetBonusSegments(strToken, intEnquiryResultID);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtTracePlusXMLAT.Text = aXDSConnectWS.ConnectGetBonusSegments(strToken, intEnquiryResultID);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }


        #endregion Account trace

        #region Address Trace

        //Submit the Address details for search
        private void btnSubmitAddT_Click(object sender, EventArgs e)
        {
            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                bool IsPostalMatch = false;

                if (rblAddressType_Physical.Checked == true)
                {
                    IsPostalMatch = false;
                }
                else if (rblAddressType_Postal.Checked == true)
                {
                    IsPostalMatch = true;
                }

                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtXMLMatchREsultAddT.Text = aXDSConnectWS.ConnectAddressMatch(strToken, txtprovince.Text, txtcity.Text, txtSuburb.Text, IsPostalMatch, txtStreetName.Text, txtPostalCode.Text, txtStreetNo.Text, txtSurNameAddT.Text, txtXternalRefAddT.Text, txtVoucherCodeAddT.Text);


                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMLMatchREsultAddT.Text);
                        ds.ReadXml(xmlSR);
                        grdvwMatchResultAddT.DataSource = ds.Tables[0];
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtXMLMatchREsultAddT.Text = aXDSConnectWS.ConnectAddressMatch(strToken, txtprovince.Text, txtcity.Text, txtSuburb.Text, IsPostalMatch, txtStreetName.Text, txtPostalCode.Text, txtStreetNo.Text, txtSurNameAddT.Text, txtXternalRefAddT.Text,txtVoucherCodeAddT.Text);

                    DataSet ds = new DataSet();
                    System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMLMatchREsultAddT.Text);
                    ds.ReadXml(xmlSR);
                    grdvwMatchResultAddT.DataSource = ds.Tables[0];
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        // Request the Report XML
        private void btnViewAddT_Click(object sender, EventArgs e)
        {
            try
            {
                int intEnquiryID = int.Parse(txtEnquiryIDAddT.Text);
                int intEnquiryResultID = int.Parse(txtEnquiryResultIDAddT.Text);
                string strBonusXML = txtTracePlusXMLAddT.Text;
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtReportXMLAddT.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0, strBonusXML);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtReportXMLAddT.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0,strBonusXML);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }

        }

        //Clears the data in fields
        private void btnClearAddT_Click(object sender, EventArgs e)
        {
            rblAddressType_Physical.Checked = false;
            rblAddressType_Postal.Checked = false;
            txtStreetName.Text = string.Empty;
            txtStreetNo.Text = string.Empty;
            txtSuburb.Text = string.Empty;
            txtcity.Text = string.Empty;
            txtprovince.Text = string.Empty;
            txtPostalCode.Text = string.Empty;
            txtSurNameAddT.Text = string.Empty;
            txtXternalRefAddT.Text = string.Empty;
        }
        private void btnTracePlusAddT_Click(object sender, EventArgs e)
        {
            try
            {
                int intEnquiryID = int.Parse(txtEnquiryIDAddT.Text);
                int intEnquiryResultID = int.Parse(txtEnquiryResultIDAddT.Text);

                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtTracePlusXMLAddT.Text = aXDSConnectWS.ConnectGetBonusSegments(strToken, intEnquiryResultID);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtTracePlusXMLAddT.Text = aXDSConnectWS.ConnectGetBonusSegments(strToken, intEnquiryResultID);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        #endregion Address Trace

        #region Telephone Trace

        //Submit the Telephone details for search
        private void btnSubmitTelT_Click(object sender, EventArgs e)
        {
            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtXMLMatchResultTelT.Text = aXDSConnectWS.ConnectTelephoneMatch(strToken, txtTelephoneCode.Text, txtTelephoneNo.Text, txtSurNameTelT.Text, txtXternalRefNoTelT.Text, txtVoucherCodeTelT.Text);


                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMLMatchResultTelT.Text);
                        ds.ReadXml(xmlSR);
                        grdvwMatchResultTelT.DataSource = ds.Tables[0];
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtXMLMatchResultTelT.Text = aXDSConnectWS.ConnectTelephoneMatch(strToken, txtTelephoneCode.Text, txtTelephoneNo.Text, txtSurNameTelT.Text, txtXternalRefNoTelT.Text,txtVoucherCodeTelT.Text);

                    DataSet ds = new DataSet();
                    System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMLMatchResultTelT.Text);
                    ds.ReadXml(xmlSR);
                    grdvwMatchResultTelT.DataSource = ds.Tables[0];
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        //Request the Report XML
        private void btnViewTelT_Click(object sender, EventArgs e)
        {
            try
            {
                int intEnquiryID = int.Parse(txtEnquiryIDTelT.Text);
                int intEnquiryResultID = int.Parse(txtEnquiryResultIDTelT.Text);
                string strBonusXML = txtTracePlusXMLTelT.Text;
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtReportXMLTelT.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0, strBonusXML);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtReportXMLTelT.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0,strBonusXML);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }

        }

        // Clears the data
        private void btnClearTelT_Click(object sender, EventArgs e)
        {
            txtTelephoneCode.Text = string.Empty;
            txtTelephoneNo.Text = string.Empty;
            txtSurNameTelT.Text = string.Empty;
            txtXternalRefNoTelT.Text = string.Empty;
        }

        private void btnTracePlusTelT_Click(object sender, EventArgs e)
        {
            try
            {
                int intEnquiryID = int.Parse(txtEnquiryIDTelT.Text);
                int intEnquiryResultID = int.Parse(txtEnquiryResultIDTelT.Text);

                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtTracePlusXMLTelT.Text = aXDSConnectWS.ConnectGetBonusSegments(strToken, intEnquiryResultID);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtTracePlusXMLTelT.Text = aXDSConnectWS.ConnectGetBonusSegments(strToken, intEnquiryResultID);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        #endregion Telephone Trace

        #region Easy Trace

        //Submit the consumer name and age details for search
        private void btnSubmitEasyT_Click(object sender, EventArgs e)
        {
            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                string strSearchtype = string.Empty;

                    if (rblExact.Checked == true)
                    {
                        strSearchtype = "E";
                    }
                    else if (rblDeviation.Checked == true)
                    {
                        strSearchtype = "D";
                    }

                    if (!aXDSConnectWS.IsTicketValid(strToken))
                    {
                        strToken = aXDSConnectWS.Login(Username, Password);
                        if (aXDSConnectWS.IsTicketValid(strToken))
                        {
                            txtXMlMatchResultEasyT.Text = aXDSConnectWS.ConnectEasyMatch(strToken, 0, strSearchtype, txtSurNameEasyT.Text, txtFirstNameEasyT.Text, Convert.ToInt16(txtAge.Text), Convert.ToInt16(txtBirthYear.Text), Convert.ToInt16(txtAgeDeviation.Text), txtXternalRefNoEasyT.Text, txtVoucherCodeEasYT.Text);


                            DataSet ds = new DataSet();
                            System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMlMatchResultEasyT.Text);
                            ds.ReadXml(xmlSR);
                            grdvwMatchResultEasyT.DataSource = ds.Tables[0];
                        }
                        else
                        {
                            if (!(strToken == string.Empty))
                            {
                                MessageBox.Show(strToken);
                            }
                        }
                    }
                    else
                    {
                        txtXMlMatchResultEasyT.Text = aXDSConnectWS.ConnectEasyMatch(strToken, 0, strSearchtype, txtSurNameEasyT.Text, txtFirstNameEasyT.Text, Convert.ToInt16(txtAge.Text), Convert.ToInt16(txtBirthYear.Text), Convert.ToInt16(txtAgeDeviation.Text), txtXternalRefNoEasyT.Text,txtVoucherCodeEasYT.Text);

                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMlMatchResultEasyT.Text);
                        ds.ReadXml(xmlSR);
                        grdvwMatchResultEasyT.DataSource = ds.Tables[0];
                    }
              
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }
        //Request the REPORT XML
        private void btnViewEasyT_Click(object sender, EventArgs e)
        {
            try
            {
                int intEnquiryID = int.Parse(txtEnquiryIDEasyT.Text);
                int intEnquiryResultID = int.Parse(txtEnquiryResultIDEasyT.Text);
                string strBonusXML = txtTracePlusXMLEasyT.Text;
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtReportXMLEasyT.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0, strBonusXML);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtReportXMLEasyT.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0,strBonusXML);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        //Clears the data in the input fields
        private void btnclearEasyT_Click(object sender, EventArgs e)
        {
            rblExact.Checked = false;
            rblDeviation.Checked = false;
            txtSurNameEasyT.Text = string.Empty;
            txtFirstNameEasyT.Text = string.Empty;
            txtAge.Text = string.Empty;
            txtBirthYear.Text = string.Empty;
            txtAgeDeviation.Text = string.Empty;
            txtXternalRefNoEasyT.Text = string.Empty;
        }
        private void btnTraceplusEasyT_Click(object sender, EventArgs e)
        {
            try
            {
                int intEnquiryID = int.Parse(txtEnquiryIDEasyT.Text);
                int intEnquiryResultID = int.Parse(txtEnquiryResultIDEasyT.Text);

                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtTracePlusXMLEasyT.Text = aXDSConnectWS.ConnectGetBonusSegments(strToken, intEnquiryResultID);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtTracePlusXMLEasyT.Text = aXDSConnectWS.ConnectGetBonusSegments(strToken, intEnquiryResultID);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

 
        #endregion Easy Trace

        #region Adverse Enquiry
        //Submit the consumer details for search
        private void btnSubmitAdvE_Click(object sender, EventArgs e)
        {
            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtXMLMatchResultAdvE.Text = aXDSConnectWS.ConnectConsumerMatch(strToken, null, 34, txtIDNoAdvE.Text, txtPPNoAdvE.Text, txtFirstNameAdvE.Text, txtSurNameAdvE.Text, dtpbirthdateAdvE.Value.ToShortDateString(), txtXternalRefNoAdvE.Text, txtVoucherCodeAdvE.Text);

                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMLMatchResultAdvE.Text);
                        ds.ReadXml(xmlSR);
                        grdvwMatchResultAdvE.DataSource = ds.Tables[0];
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtXMLMatchResultAdvE.Text = aXDSConnectWS.ConnectConsumerMatch(strToken, null, 34, txtIDNoAdvE.Text, txtPPNoAdvE.Text, txtFirstNameAdvE.Text, txtSurNameAdvE.Text, dtpbirthdateAdvE.Value.ToShortDateString(), txtXternalRefNoAdvE.Text,txtVoucherCodeAdvE.Text);

                    DataSet ds = new DataSet();
                    System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMLMatchResultAdvE.Text);
                    ds.ReadXml(xmlSR);
                    grdvwMatchResultAdvE.DataSource = ds.Tables[0];
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        //Request the Report XML
        private void btnViewAdvE_Click(object sender, EventArgs e)
        {
            try
            {
                int intEnquiryID = int.Parse(txtEnquiryIDAdvE.Text);
                int intEnquiryResultID = int.Parse(txtEnquiryResultIDAdvE.Text);
                string strBonusXML = txtTracePlusXMLAdvE.Text;
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtReportXMLAdvE.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0, strBonusXML);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtReportXMLAdvE.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0,strBonusXML);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }
        //Clears the data in the input fields
        private void btnClearADVE_Click(object sender, EventArgs e)
        {
            txtIDNoAdvE.Text = string.Empty;
            txtFirstNameAdvE.Text = string.Empty;
            txtSurNameAdvE.Text = string.Empty;
            txtPPNoAdvE.Text = string.Empty;
            dtpbirthdateAdvE.Text = string.Empty;
            txtXternalRefNoAdvE.Text = string.Empty;
        }
        #endregion Adverse Enquiry

        #region Judgment Enquiry

        //Submit the consumer details for search
        private void btnSubmitJudgE_Click(object sender, EventArgs e)
        {
            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtXMLMatchResultJudgE.Text = aXDSConnectWS.ConnectConsumerMatch(strToken, null, 33, txtIDNoJudgE.Text, txtPPNoJudgE.Text, txtFirstNameJudgE.Text, txtSurNamejudgE.Text, dtpBirthdateJudgE.Value.ToShortDateString(), txtXternalRefJudgE.Text, txtVoucherCodejudgE.Text);

                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMLMatchResultJudgE.Text);
                        ds.ReadXml(xmlSR);
                        grdvwMatchResultjudgE.DataSource = ds.Tables[0];
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtXMLMatchResultJudgE.Text = aXDSConnectWS.ConnectConsumerMatch(strToken, null, 33, txtIDNoJudgE.Text, txtPPNoJudgE.Text, txtFirstNameJudgE.Text, txtSurNamejudgE.Text, dtpBirthdateJudgE.Value.ToShortDateString(), txtXternalRefJudgE.Text,txtVoucherCodejudgE.Text);

                    DataSet ds = new DataSet();
                    System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMLMatchResultJudgE.Text);
                    ds.ReadXml(xmlSR);
                    grdvwMatchResultjudgE.DataSource = ds.Tables[0];
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        // Request the report XML
        private void btnViewJudgE_Click(object sender, EventArgs e)
        {
            try
            {
                int intEnquiryID = int.Parse(txtEnquiryIDJudgE.Text);
                int intEnquiryResultID = int.Parse(txtEnquiryResultIDJudgE.Text);
                string strBonusXML = txtTracePlusXMLJudgE.Text;
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtReportXMlJudgE.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0, strBonusXML);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtReportXMlJudgE.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0,strBonusXML);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }
        //Clears the data in the input fields
        private void btnClearJudgE_Click(object sender, EventArgs e)
        {
            txtIDNoJudgE.Text = string.Empty;
            txtFirstNameJudgE.Text = string.Empty;
            txtSurNamejudgE.Text = string.Empty;
            txtPPNoJudgE.Text = string.Empty;
            dtpBirthdateJudgE.Text = string.Empty;
            txtXternalRefJudgE.Text = string.Empty;
        }
        #endregion Judgment Enquiry

        #region Admin Order Enquiry

        //Submit the consumer details for search
        private void btnSubmitAOE_Click(object sender, EventArgs e)
        {
            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtXMLMatchResultAOE.Text = aXDSConnectWS.ConnectConsumerMatch(strToken, null, 35, txtIDNoAOE.Text, txtPPNoAOE.Text, txtFirstNameAOE.Text, txtSurNAmeAOE.Text, dtpBirthDateAOE.Value.ToShortDateString(), txtXternalRefnoAOE.Text, txtVoucherCodeAOE.Text);

                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMLMatchResultAOE.Text);
                        ds.ReadXml(xmlSR);
                        grdvwMatchResultAOE.DataSource = ds.Tables[0];
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtXMLMatchResultAOE.Text = aXDSConnectWS.ConnectConsumerMatch(strToken, null, 35, txtIDNoAOE.Text, txtPPNoAOE.Text, txtFirstNameAOE.Text, txtSurNAmeAOE.Text, dtpBirthDateAOE.Value.ToShortDateString(), txtXternalRefnoAOE.Text,txtVoucherCodeAOE.Text);

                    DataSet ds = new DataSet();
                    System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMLMatchResultAOE.Text);
                    ds.ReadXml(xmlSR);
                    grdvwMatchResultAOE.DataSource = ds.Tables[0];
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        // Request the Report XML
        private void btnViewAOE_Click(object sender, EventArgs e)
        {
            try
            {
                int intEnquiryID = int.Parse(txtEnquiryIDAOE.Text);
                int intEnquiryResultID = int.Parse(txtEnquiryResultIDAOE.Text);
                string strBonusXML = txtTracePlusXMLAOE.Text;
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtReportXMLAOE.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0, strBonusXML);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtReportXMLAOE.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0,strBonusXML);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        //Clears the Data in the input fields
        private void btnClearAOE_Click(object sender, EventArgs e)
        {
            txtIDNoAOE.Text = string.Empty;
            txtPPNoAOE.Text = string.Empty;
            txtFirstNameAOE.Text = string.Empty;
            txtSurNAmeAOE.Text = string.Empty;
            dtpBirthDateAOE.Text = string.Empty;
            txtXternalRefnoAOE.Text = string.Empty;
        }
        #endregion Admin Order Enquiry

        #region Sequestration Enquiry

        //Submit the consumer details for search
        private void btnSubmitSeqE_Click(object sender, EventArgs e)
        {

            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtXMLMatchResultSeqE.Text = aXDSConnectWS.ConnectConsumerMatch(strToken, null, 36, txtIDNoSeqE.Text, txtPPNoSeqE.Text, txtFirstNameSeqE.Text, txtSurNameSeqE.Text, dtpBirthdateSeqE.Value.ToShortDateString(), txtXternalRefNoSeqE.Text,txtVoucherCodeSeqE.Text);

                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMLMatchResultSeqE.Text);
                        ds.ReadXml(xmlSR);
                        grdvwMatchResultSeqE.DataSource = ds.Tables[0];
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtXMLMatchResultSeqE.Text = aXDSConnectWS.ConnectConsumerMatch(strToken, null, 36, txtIDNoSeqE.Text, txtPPNoSeqE.Text, txtFirstNameSeqE.Text, txtSurNameSeqE.Text, dtpBirthdateSeqE.Value.ToShortDateString(), txtXternalRefNoSeqE.Text,txtVoucherCodeSeqE.Text);

                    DataSet ds = new DataSet();
                    System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMLMatchResultSeqE.Text);
                    ds.ReadXml(xmlSR);
                    grdvwMatchResultSeqE.DataSource = ds.Tables[0];
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        //Request the Report XML
        private void btnViewSeqE_Click(object sender, EventArgs e)
        {
            try
            {
                int intEnquiryID = int.Parse(txtEnquiryIDSeqE.Text);
                int intEnquiryResultID = int.Parse(txtEnquiryResultIDSeqE.Text);
                string strBonusXML = txtTracePlusXMLSeqE.Text;
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtReportXMLSeqE.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0, strBonusXML);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtReportXMLSeqE.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0,strBonusXML);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        //Clears the Data in the input fields
        private void btnClearSeqE_Click(object sender, EventArgs e)
        {
            txtIDNoSeqE.Text = string.Empty;
            txtPPNoSeqE.Text = string.Empty;
            txtFirstNameSeqE.Text = string.Empty;
            txtSurNameSeqE.Text = string.Empty;
            dtpBirthdateSeqE.Text = string.Empty;
            txtXternalRefNoSeqE.Text = string.Empty;
        }
        #endregion Sequestration Enquiry

        #region Debt Review Enquiry
        //Submit the consumer details for search
        private void btnSubmitDebtRE_Click(object sender, EventArgs e)
        {
            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtXMlMatchResultDebtRE.Text = aXDSConnectWS.ConnectConsumerMatch(strToken, null, 38, txtIDnoDebtRE.Text, txtPPNoDebtRE.Text, txtFirstNameDebtRE.Text, txtSurNameDebtRE.Text, dtpBirthDateDebtRE.Value.ToShortDateString(), txtXternalRefNoDebtRE.Text, txtVoucherCodeDebtRE.Text);

                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMlMatchResultDebtRE.Text);
                        ds.ReadXml(xmlSR);
                        grdvwMatchResultDebtRE.DataSource = ds.Tables[0];
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtXMlMatchResultDebtRE.Text = aXDSConnectWS.ConnectConsumerMatch(strToken, null, 38, txtIDnoDebtRE.Text, txtPPNoDebtRE.Text, txtFirstNameDebtRE.Text, txtSurNameDebtRE.Text, dtpBirthDateDebtRE.Value.ToShortDateString(), txtXternalRefNoDebtRE.Text,txtVoucherCodeDebtRE.Text);

                    DataSet ds = new DataSet();
                    System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMlMatchResultDebtRE.Text);
                    ds.ReadXml(xmlSR);
                    grdvwMatchResultDebtRE.DataSource = ds.Tables[0];
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }
        //Request the Report XML
        private void btnViewDebtRE_Click(object sender, EventArgs e)
        {
            try
            {
                int intEnquiryID = int.Parse(txtEnquiryIDDebtRE.Text);
                int intEnquiryResultID = int.Parse(txtEnquiryResultIDDebtRE.Text);
                string strBonusXML = txtTracePlusXMLDebtRE.Text;
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtReportXMLDebtRE.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0, strBonusXML);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtReportXMLDebtRE.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0,strBonusXML);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }
        //Clears the data in the input fields
        private void btnClearDebtRE_Click(object sender, EventArgs e)
        {
            txtIDnoDebtRE.Text = string.Empty;
            txtPPNoDebtRE.Text = string.Empty;
            txtFirstNameDebtRE.Text = string.Empty;
            txtSurNameDebtRE.Text = string.Empty;
            dtpBirthDateDebtRE.Text = string.Empty;
            txtXternalRefNoDebtRE.Text = string.Empty;

        }
        #endregion Debt Review Enquiry

        #region Full Public Domain Enquiry
        //Submit the consumer details for search
        private void btnSubmitFPDE_Click(object sender, EventArgs e)
        {

            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtXMLMatchResultFPDE.Text = aXDSConnectWS.ConnectConsumerMatch(strToken, null, 37, txtIDNoFPDE.Text, txtPpnoFPDE.Text, txtFirstNameFPDE.Text, txtSurNameFPDE.Text, dtpBirthdateFPDE.Value.ToShortDateString(), txtXternalRefnoFPDE.Text, txtVoucherCodeFPDE.Text);

                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMLMatchResultFPDE.Text);
                        ds.ReadXml(xmlSR);
                        grdvwMatchResultFPDE.DataSource = ds.Tables[0];
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtXMLMatchResultFPDE.Text = aXDSConnectWS.ConnectConsumerMatch(strToken, null, 37, txtIDNoFPDE.Text, txtPpnoFPDE.Text, txtFirstNameFPDE.Text, txtSurNameFPDE.Text, dtpBirthdateFPDE.Value.ToShortDateString(), txtXternalRefnoFPDE.Text,txtVoucherCodeFPDE.Text);

                    DataSet ds = new DataSet();
                    System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMLMatchResultFPDE.Text);
                    ds.ReadXml(xmlSR);
                    grdvwMatchResultFPDE.DataSource = ds.Tables[0];
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }

        }
        //Request the Report XML
        private void btnViewFPDE_Click(object sender, EventArgs e)
        {
            try
            {
                int intEnquiryID = int.Parse(txtEnquiryIDFPDE.Text);
                int intEnquiryResultID = int.Parse(txtEnquiryResultIDFPDE.Text);
                string strBonusXML = txtTraceplusXMLFPDE.Text;
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtReportXMLFPDE.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0, strBonusXML);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtReportXMLFPDE.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0,strBonusXML);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }
        //Clears the data in the input fields
        private void btnClearFPDE_Click(object sender, EventArgs e)
        {
            txtIDNoFPDE.Text = string.Empty;
            txtPpnoFPDE.Text = string.Empty;
            txtFirstNameFPDE.Text = string.Empty;
            txtSurNameFPDE.Text = string.Empty;
            dtpBirthdateFPDE.Text = string.Empty;
            txtXternalRefnoFPDE.Text = string.Empty;
        }
        #endregion Full Public Domain Enquiry

        #region Business Enquiry
        //Submit the Commercial details for search
        private void btnSubmitBusE_Click(object sender, EventArgs e)
        {
            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtXMlMatchResultBusE.Text = aXDSConnectWS.ConnectBusinessMatch(strToken, txtRegNo1.Text, txtRegNo2.Text, txtRegNo3.Text, txtBusinessName.Text, txtVatNo.Text,txtSolePropIDNo.Text,txtXternalRefnoBusE.Text, txtVoucherCodeBusE.Text);

                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMlMatchResultBusE.Text);
                        ds.ReadXml(xmlSR);
                        grdvwMatchResultBusE.DataSource = ds.Tables[0];
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtXMlMatchResultBusE.Text = aXDSConnectWS.ConnectBusinessMatch(strToken, txtRegNo1.Text, txtRegNo2.Text, txtRegNo3.Text, txtBusinessName.Text, txtVatNo.Text,txtSolePropIDNo.Text,txtXternalRefnoBusE.Text,txtVoucherCodeBusE.Text);

                    DataSet ds = new DataSet();
                    System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMlMatchResultBusE.Text);
                    ds.ReadXml(xmlSR);
                    grdvwMatchResultBusE.DataSource = ds.Tables[0];
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }
        //Request the Report XML
        private void btnViewBusE_Click(object sender, EventArgs e)
        {
            try
            {
                int intEnquiryID = int.Parse(txtEnquiryIDBusE.Text);
                int intEnquiryResultID = int.Parse(txtEnquiryResultIDBusE.Text);
                string strBonusXML = txtTraceplusXMLBusE.Text;
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtReportXMLBusE.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, Convert.ToInt16(cmbProductID.SelectedItem), strBonusXML);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtReportXMLBusE.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, Convert.ToInt16(cmbProductID.SelectedItem), strBonusXML);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        //Clears the data in input fields

        private void btnClearBusE_Click(object sender, EventArgs e)
        {
            txtRegNo1.Text = string.Empty;
            txtRegNo2.Text = string.Empty;
            txtRegNo3.Text = string.Empty;
            txtBusinessName.Text = string.Empty;
            txtVatNo.Text = string.Empty;
            txtXternalRefnoBusE.Text = string.Empty;
        }
        #endregion Business Enquiry

        #region Director Enquiry
        //Submit the Director details for search
        private void btnSubmitDirE_Click(object sender, EventArgs e)
        {
            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtXMLMatchResultDirE.Text = aXDSConnectWS.ConnectDirectorMatch(strToken, txtIDNoDirE.Text, txtFirstNameDirE.Text, txtSurNameDirE.Text, txtXternalRefnoDirE.Text, txtVoucherCodeDirE.Text);

                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMLMatchResultDirE.Text);
                        ds.ReadXml(xmlSR);
                        grdvwMatchResultDirE.DataSource = ds.Tables[0];
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtXMLMatchResultDirE.Text = aXDSConnectWS.ConnectDirectorMatch(strToken, txtIDNoDirE.Text, txtFirstNameDirE.Text, txtSurNameDirE.Text, txtXternalRefnoDirE.Text,txtVoucherCodeDirE.Text);

                    DataSet ds = new DataSet();
                    System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMLMatchResultDirE.Text);
                    ds.ReadXml(xmlSR);
                    grdvwMatchResultDirE.DataSource = ds.Tables[0];
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }
        // Request Report XML
        private void btnViewDirE_Click(object sender, EventArgs e)
        {
            try
            {
                int intEnquiryID = int.Parse(txtEnquiryIDDirE.Text);
                int intEnquiryResultID = int.Parse(txtEnquiryResultIDDirE.Text);
                string strBonusXML = txtTracePlusXMLDirE.Text;
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtReportXMLDirE.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0, strBonusXML);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtReportXMLDirE.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0,strBonusXML);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }
        //Clears the data in all input fields
        private void btnClearDirE_Click(object sender, EventArgs e)
        {
            txtIDNoDirE.Text = string.Empty;
            txtPPNoDirE.Text = string.Empty;
            txtFirstNameDirE.Text = string.Empty;
            txtSurNameDirE.Text = string.Empty;
            txtXternalRefnoDirE.Text = string.Empty;
        }
        #endregion Director Enquiry

        #region Identity Verification
        //Submit the consumer details for search
        private void btnSubmitIDV_Click(object sender, EventArgs e)
        {
            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtXMLMatchResultIDV.Text = aXDSConnectWS.ConnectIDVerification(strToken, txtIDNoIDV.Text, txtFirstNameIDV.Text, txtSurNameIDV.Text, txtXternalRefNoIDV.Text, txtVoucherCodeIDV.Text);

                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMLMatchResultIDV.Text);
                        ds.ReadXml(xmlSR);
                        grdvwMatchResultIDV.DataSource = ds.Tables[0];
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtXMLMatchResultIDV.Text = aXDSConnectWS.ConnectIDVerification(strToken, txtIDNoIDV.Text, txtFirstNameIDV.Text, txtSurNameIDV.Text, txtXternalRefNoIDV.Text,txtVoucherCodeIDV.Text);

                    DataSet ds = new DataSet();
                    System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMLMatchResultIDV.Text);
                    ds.ReadXml(xmlSR);
                    grdvwMatchResultIDV.DataSource = ds.Tables[0];
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }
        // Request the Report XML
        private void btnViewIDV_Click(object sender, EventArgs e)
        {
            try
            {
                int intEnquiryID = int.Parse(txtEnquiryIDIDV.Text);
                int intEnquiryResultID = int.Parse(txtEnquiryResultIDIDV.Text);
                string strBonusXML = txtTracePlusXMLIDV.Text;
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtReportXMLIDV.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0, strBonusXML);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtReportXMLIDV.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0,strBonusXML);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }
        //Clears the data in input fields
        private void btnClearIDV_Click(object sender, EventArgs e)
        {
            txtIDNoIDV.Text = string.Empty;
            txtFirstNameIDV.Text = string.Empty;
            txtSurNameIDV.Text = string.Empty;
            txtXternalRefNoIDV.Text = string.Empty;
        }
        #endregion Identity Verification

        #region Marital Status Enquiry
        //Submit the consumer details for search
        private void btnSubmitMSV_Click(object sender, EventArgs e)
        {
            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtXMLMatchResultMSV.Text = aXDSConnectWS.ConnectConsumerMatch(strToken, null, 39, txtIDNoMSV.Text, txtPPNoMSV.Text, txtFirstNameMSV.Text, txtSurnameMSV.Text, dtpBirthDateMSV.Text, txtXternalRefNoMSV.Text, txtVoucherCodeMSV.Text);

                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMLMatchResultMSV.Text);
                        ds.ReadXml(xmlSR);
                        grdvwMatchResultMSV.DataSource = ds.Tables[0];
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtXMLMatchResultMSV.Text = aXDSConnectWS.ConnectConsumerMatch(strToken, null, 39, txtIDNoMSV.Text, txtPPNoMSV.Text, txtFirstNameMSV.Text, txtSurnameMSV.Text, dtpBirthDateMSV.Text, txtXternalRefNoMSV.Text,txtVoucherCodeMSV.Text);

                    DataSet ds = new DataSet();
                    System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMLMatchResultMSV.Text);
                    ds.ReadXml(xmlSR);
                    grdvwMatchResultMSV.DataSource = ds.Tables[0];
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }
        //Request Report XML
        private void btnViewMSV_Click(object sender, EventArgs e)
        {
            try
            {
                int intEnquiryID = int.Parse(txtEnquiryIDMSV.Text);
                int intEnquiryResultID = int.Parse(txtEnquiryResultIDMSV.Text);
                string strBonusXML = txtTracePlusXMLMSV.Text;
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtReportXMLMSV.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0, strBonusXML);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtReportXMLMSV.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0,strBonusXML);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }
        //Clears the data in input fields
        private void btnClearMSV_Click(object sender, EventArgs e)
        {
            txtIDNoMSV.Text = string.Empty;
            txtPPNoMSV.Text = string.Empty;
            txtFirstNameMSV.Text = string.Empty;
            txtSurnameMSV.Text = string.Empty;
            dtpBirthDateMSV.Text = string.Empty;
            txtXternalRefNoMSV.Text = string.Empty;
        }

        #endregion Marital Status Enquiry

        #region Business Investigative Enquiry

        private void cmbCountry_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbCountry.Items.Count == 0)
                {
                    aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                    strToken = aXDSConnectWS.Login(Username, Password);
                    string axml = aXDSConnectWS.ConnectGetCountryCodes(strToken);

                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(axml);
                        ds.ReadXml(xmlSR);
                        cmbCountry.DataSource = ds.Tables[0];
                        cmbCountry.DisplayMember = ds.Tables[0].Columns[1].ToString();
                        cmbCountry.ValueMember = ds.Tables[0].Columns[0].ToString();
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }
      

        private void cmbReportType_Click(object sender, EventArgs e)
        {
            try
            {
                    aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                    strToken = aXDSConnectWS.Login(Username, Password);
                    string axml = aXDSConnectWS.ConnectGetReportTypes(strToken,int.Parse(cmbCountry.SelectedValue.ToString()));

                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(axml);
                        ds.ReadXml(xmlSR);
                        cmbReportType.DataSource = ds.Tables[0];
                        cmbReportType.DisplayMember = ds.Tables[0].Columns[1].ToString();
                        cmbReportType.ValueMember = ds.Tables[0].Columns[0].ToString();
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
              
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }

        }
        private void cmbrptTimeFrame_Click(object sender, EventArgs e)
        {
            try
            {
                                   aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                    strToken = aXDSConnectWS.Login(Username, Password);
                    string axml = aXDSConnectWS.ConnectGetReportTimeFrames(strToken,int.Parse(cmbReportType.SelectedValue.ToString()));

                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(axml);
                        ds.ReadXml(xmlSR);
                        cmbrptTimeFrame.DataSource = ds.Tables[0];
                        cmbrptTimeFrame.DisplayMember = ds.Tables[0].Columns[1].ToString();
                        cmbrptTimeFrame.ValueMember = ds.Tables[0].Columns[0].ToString();
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
              
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }


        }

        private void cmbBIVTerms_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbBIVTerms.Items.Count == 0)
                {
                    aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                    strToken = aXDSConnectWS.Login(Username, Password);
                    string axml = aXDSConnectWS.ConnectGetTerms(strToken,43);

                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(axml);
                        ds.ReadXml(xmlSR);
                        cmbBIVTerms.DataSource = ds.Tables[0];
                        cmbBIVTerms.DisplayMember = ds.Tables[0].Columns[0].ToString();
                        cmbBIVTerms.ValueMember = ds.Tables[0].Columns[0].ToString();
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }

        }

        private void btnSubmitBIE_Click(object sender, EventArgs e)
        {
            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {                        
                        if (txtBIProductID.Text == "43")
                            txtXMLMatchResultBIE.Text = aXDSConnectWS.ConnectBusinessInvestigativeEnquiry(strToken, cmbCountry.SelectedText.ToString(), cmbReportType.SelectedText.ToString(), cmbrptTimeFrame.SelectedText.ToString(), txtRegNo1BIE.Text, txtRegNo2BIE.Text, txtRegNo3BIE.Text, txtBusNameBIE.Text, txtIDNoBIE.Text.ToString(), txtOtherBIE.Text.ToString(), txtCompanyContactNo.Text.ToString(), cmbBIVTerms.SelectedValue.ToString(), Convert.ToDouble(txtAmount.Text.ToString()), txtFirstNameBIE.Text, txtSurNameBIE.Text, txtContactNoBIE.Text, txtEmailAddressBIE.Text, txtCompanyNameBIE.Text, true, txtVoucherCodeBIE.Text, txtxternalRefNoBIE.Text);
                        else
                            txtXMLMatchResultBIE.Text = aXDSConnectWS.ConnectCustomBusinessInvestigativeEnquiry(strToken, cmbCountry.Text, cmbReportType.Text, cmbrptTimeFrame.Text, txtRegNo1BIE.Text, txtRegNo2BIE.Text, txtRegNo3BIE.Text, txtBusNameBIE.Text, txtIDNoBIE.Text.ToString(), txtOtherBIE.Text.ToString(), txtCompanyContactNo.Text.ToString(), cmbBIVTerms.SelectedValue.ToString(), Convert.ToDouble(txtAmount.Text.ToString()), txtFirstNameBIE.Text, txtSurNameBIE.Text, txtContactNoBIE.Text, txtEmailAddressBIE.Text, txtCompanyNameBIE.Text, true, Convert.ToInt32(txtBIProductID.Text), txtVoucherCodeBIE.Text, txtxternalRefNoBIE.Text);

                        //DataSet ds = new DataSet();
                        //System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMLMatchResultMSV.Text);
                        //ds.ReadXml(xmlSR);
                        //grdvwMatchResultMSV.DataSource = ds.Tables[0];
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    if (txtBIProductID.Text == "43")
                        txtXMLMatchResultBIE.Text = aXDSConnectWS.ConnectBusinessInvestigativeEnquiry(strToken, cmbCountry.SelectedText.ToString(), cmbReportType.SelectedText.ToString(), cmbrptTimeFrame.SelectedText.ToString(), txtRegNo1BIE.Text, txtRegNo2BIE.Text, txtRegNo3BIE.Text, txtBusNameBIE.Text, txtIDNoBIE.Text.ToString(), txtOtherBIE.Text.ToString(), txtCompanyContactNo.Text.ToString(), cmbBIVTerms.SelectedValue.ToString(), Convert.ToDouble(txtAmount.Text.ToString()), txtFirstNameBIE.Text, txtSurNameBIE.Text, txtContactNoBIE.Text, txtEmailAddressBIE.Text, txtCompanyNameBIE.Text, true, txtVoucherCodeBIE.Text, txtxternalRefNoBIE.Text);
                    else
                        txtXMLMatchResultBIE.Text = aXDSConnectWS.ConnectCustomBusinessInvestigativeEnquiry(strToken, cmbCountry.Text, cmbReportType.Text, cmbrptTimeFrame.Text, txtRegNo1BIE.Text, txtRegNo2BIE.Text, txtRegNo3BIE.Text, txtBusNameBIE.Text, txtIDNoBIE.Text.ToString(), txtOtherBIE.Text.ToString(), txtCompanyContactNo.Text.ToString(), cmbBIVTerms.SelectedValue.ToString(), Convert.ToDouble(txtAmount.Text.ToString()), txtFirstNameBIE.Text, txtSurNameBIE.Text, txtContactNoBIE.Text, txtEmailAddressBIE.Text, txtCompanyNameBIE.Text, true, Convert.ToInt32(txtBIProductID.Text), txtVoucherCodeBIE.Text, txtxternalRefNoBIE.Text);
                                        
                    //DataSet ds = new DataSet();
                    //System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMLMatchResultMSV.Text);
                    //ds.ReadXml(xmlSR);
                    //grdvwMatchResultMSV.DataSource = ds.Tables[0];
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        private void btnCleatBIE_Click(object sender, EventArgs e)
        {
            txtRegNo1BIE.Text = string.Empty;
            txtRegNo2BIE.Text = string.Empty;
            txtRegNo3BIE.Text = string.Empty;
            txtBusNameBIE.Text = string.Empty;
            txtFirstNameBIE.Text = string.Empty;
            txtSurNameBIE.Text = string.Empty;
            txtContactNoBIE.Text = string.Empty;
            txtEmailAddressBIE.Text = string.Empty;
            txtCompanyNameBIE.Text = string.Empty;
            txtxternalRefNoBIE.Text = string.Empty;
            txtVoucherCodeBIE.Text = string.Empty;
        }
        #endregion Business Investigative Enquiry

        #region ERF Search

        private void btnsubmitERF_Click(object sender, EventArgs e)
        {
            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtXMLMatchResultERF.Text = aXDSConnectWS.ConnectDeedsMatch(strToken, XDSconnectSample.xdsconnect.SearchtypeInd.ErfNo, cmbDeedsOfficeERF.Text, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, txtTownshipNameERF.Text, txtErfNoERF.Text, txtPortionNoERF.Text, string.Empty, string.Empty, txtXternalRefERF.Text, txtVoucherCodeERF.Text);

                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMLMatchResultERF.Text);
                        ds.ReadXml(xmlSR);
                        grdvwXMlMatchResulterf.DataSource = ds.Tables[0];
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtXMLMatchResultERF.Text = aXDSConnectWS.ConnectDeedsMatch(strToken, XDSconnectSample.xdsconnect.SearchtypeInd.ErfNo, cmbDeedsOfficeERF.Text, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, txtTownshipNameERF.Text, txtErfNoERF.Text, txtPortionNoERF.Text, string.Empty, string.Empty, txtXternalRefERF.Text, txtVoucherCodeERF.Text);

                    DataSet ds = new DataSet();
                    System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMLMatchResultERF.Text);
                    ds.ReadXml(xmlSR);
                    grdvwXMlMatchResulterf.DataSource = ds.Tables[0];
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        private void btnViewERF_Click(object sender, EventArgs e)
        {
            try
            {
                int intEnquiryID = int.Parse(txtEnquiryIDERF.Text);
                int intEnquiryResultID = int.Parse(txtEnquiryResultIDERF.Text);
                string strBonusXML = txtTracePlusXMLERF.Text;
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtReportXMLERF.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0, strBonusXML);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtReportXMLERF.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0, strBonusXML);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        private void btnClearERF_Click(object sender, EventArgs e)
        {
            cmbDeedsOfficeERF.Text = string.Empty;
            txtErfNoERF.Text = string.Empty;
            txtTownshipNameERF.Text = string.Empty;
            txtXternalRefERF.Text = string.Empty;
            txtVoucherCodeERF.Text = string.Empty;
            txtPortionNoERF.Text = string.Empty;
        }

        #endregion ERF Search

        #region Individual Search

        private void btnSubmitInd_Click(object sender, EventArgs e)
        {
            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtXMlMatchResultInd.Text = aXDSConnectWS.ConnectDeedsMatch(strToken, XDSconnectSample.xdsconnect.SearchtypeInd.IDNo, cmbDeedsOfficeInd.Text, txtIDNOInd.Text, txtFirstNameInd.Text, txtSurNameInd.Text, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, txtXternalRefInd.Text, txtVoucherCodeInd.Text);

                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMlMatchResultInd.Text);
                        ds.ReadXml(xmlSR);
                        grdvwXMLMatchResultInd.DataSource = ds.Tables[0];
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtXMlMatchResultInd.Text = aXDSConnectWS.ConnectDeedsMatch(strToken, XDSconnectSample.xdsconnect.SearchtypeInd.IDNo, cmbDeedsOfficeInd.Text, txtIDNOInd.Text, txtFirstNameInd.Text, txtSurNameInd.Text, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, txtXternalRefInd.Text, txtVoucherCodeInd.Text);

                    DataSet ds = new DataSet();
                    System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMlMatchResultInd.Text);
                    ds.ReadXml(xmlSR);
                    grdvwXMLMatchResultInd.DataSource = ds.Tables[0];
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        private void btnViewInd_Click(object sender, EventArgs e)
        {
            try
            {
                int intEnquiryID = int.Parse(txtEnquiryIDInd.Text);
                int intEnquiryResultID = int.Parse(txtEnquiryResultIDInd.Text);
                string strBonusXML = txtTracePlusXMLInd.Text;
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtReportXMLInd.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0, strBonusXML);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtReportXMLInd.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0, strBonusXML);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }
        private void btnClearInd_Click(object sender, EventArgs e)
        {
            cmbDeedsOfficeInd.Text = string.Empty;
            txtIDNOInd.Text = string.Empty;
            txtFirstNameInd.Text = string.Empty;
            txtSurNameInd.Text = string.Empty;
            txtXternalRefInd.Text = string.Empty;
            txtVoucherCodeInd.Text = string.Empty;
        }

#endregion Individual Search

        #region Company Search
        private void btnSubmitCom_Click(object sender, EventArgs e)
        {
            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtXMlMatchResultCom.Text = aXDSConnectWS.ConnectDeedsMatch(strToken, XDSconnectSample.xdsconnect.SearchtypeInd.BusRegistrationNO, cmbDeedsOfficeCOM.Text, string.Empty, string.Empty, string.Empty, txtRegNo1Com.Text, txtRegNo2Com.Text, txtRegNo3Com.Text, txtBusinessNameCom.Text, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, txtXternalRefCom.Text, txtVoucherCodeCom.Text);

                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMlMatchResultCom.Text);
                        ds.ReadXml(xmlSR);
                        grdvwXMLMatchResultCom.DataSource = ds.Tables[0];
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtXMlMatchResultCom.Text = aXDSConnectWS.ConnectDeedsMatch(strToken, XDSconnectSample.xdsconnect.SearchtypeInd.BusRegistrationNO, cmbDeedsOfficeCOM.Text, string.Empty, string.Empty, string.Empty, txtRegNo1Com.Text, txtRegNo2Com.Text, txtRegNo3Com.Text, txtBusinessNameCom.Text, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, txtXternalRefCom.Text, txtVoucherCodeCom.Text);

                    DataSet ds = new DataSet();
                    System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMlMatchResultCom.Text);
                    ds.ReadXml(xmlSR);
                    grdvwXMLMatchResultCom.DataSource = ds.Tables[0];
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        private void btnViewCom_Click(object sender, EventArgs e)
        {
            try
            {
                int intEnquiryID = int.Parse(txtEnquiryIDCom.Text);
                int intEnquiryResultID = int.Parse(txtEnquiryResultIDCom.Text);
                string strBonusXML = txtTracePlusXMLCom.Text;
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtReportXMLCom.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0, strBonusXML);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtReportXMLCom.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0, strBonusXML);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }
        private void btnClearCom_Click(object sender, EventArgs e)
        {
            cmbDeedsOfficeCOM.Text = string.Empty;
            txtRegNo1Com.Text = string.Empty;
            txtRegNo2Com.Text = string.Empty;
            txtRegNo3Com.Text = string.Empty;
            txtBusinessNameCom.Text = string.Empty;
            txtXternalRefCom.Text = string.Empty;
            txtVoucherCodeCom.Text = string.Empty;

        }
#endregion Company Search

        #region Title Deed Search

        private void btnSubmitTDS_Click(object sender, EventArgs e)
        {
            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtXMLMatchResultTDS.Text = aXDSConnectWS.ConnectDeedsMatch(strToken, XDSconnectSample.xdsconnect.SearchtypeInd.TitleDeedNo, cmbDeedsOfficeTDS.Text, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, txtTitleDeedNoTDS.Text, txtBondAccNoTDS.Text, txtXternalRefTDS.Text, txtVoucherCodeTDS.Text);

                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMLMatchResultTDS.Text);
                        ds.ReadXml(xmlSR);
                        grdvwXMLMatchResultTDS.DataSource = ds.Tables[0];
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtXMLMatchResultTDS.Text = aXDSConnectWS.ConnectDeedsMatch(strToken, XDSconnectSample.xdsconnect.SearchtypeInd.TitleDeedNo, cmbDeedsOfficeTDS.Text, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, txtTitleDeedNoTDS.Text, txtBondAccNoTDS.Text, txtXternalRefTDS.Text, txtVoucherCodeTDS.Text);

                    DataSet ds = new DataSet();
                    System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMLMatchResultTDS.Text);
                    ds.ReadXml(xmlSR);
                    grdvwXMLMatchResultTDS.DataSource = ds.Tables[0];
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        private void btnViewTDS_Click(object sender, EventArgs e)
        {
            try
            {
                int intEnquiryID = int.Parse(txtEnquiryIDTDS.Text);
                int intEnquiryResultID = int.Parse(txtEnquiryResultIDTDS.Text);
                string strBonusXML = txtTracePlusXMLTDS.Text;
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtReportXMLTDS.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0, strBonusXML);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtReportXMLTDS.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 0, strBonusXML);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        private void btnClearTDS_Click(object sender, EventArgs e)
        {
            cmbDeedsOfficeTDS.Text = string.Empty;
            txtTitleDeedNoTDS.Text = string.Empty;
            txtBondAccNoTDS.Text = string.Empty;
            txtXternalRefTDS.Text = string.Empty;
            txtVoucherCodeTDS.Text = string.Empty;
        }
        #endregion Title Deed Search       

        #region Configuration
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtUrl.Text == string.Empty || txtUserName.Text == string.Empty || txtPassword.Text == string.Empty)
                {
                    throw new Exception("URL, UserName and Password are mandatory");
                }

                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.AppSettings.Settings.Remove("URL");
                config.AppSettings.Settings.Remove("UserName");
                config.AppSettings.Settings.Remove("Password");
                config.AppSettings.Settings.Add("URL", txtUrl.Text);
                config.AppSettings.Settings.Add("UserName", txtUserName.Text);
                config.AppSettings.Settings.Add("Password", txtPassword.Text);
                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("appSettings");
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                Username = ConfigurationManager.AppSettings["UserName"].ToString();
                Password = ConfigurationManager.AppSettings["Password"].ToString();
                strToken = aXDSConnectWS.Login(Username, Password);
                lblStatus.Text = "Configuration details Updated Successfully";
            }
            catch (Exception ex)
            {
                lblStatus.Text = ex.Message;
            }
        }
        #endregion Configuration

        #region Account Verification
        private void cmbVerificationtype_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbVerificationtype.Text.ToString() == "Individual")
            {
                txtInitials.Enabled = true;
                txtSurnameAccv.Enabled = true;
                txtIDNoAccv.Enabled = true;
                txtBusinessnameAccv.Enabled = false;
                txtReg1Accv.Enabled = false;
                txtReg2Accv.Enabled = false;
                txtReg3Accv.Enabled = false;
                txtTrustNoAccV.Enabled = false;
                cmbEntityAccv.Enabled = false;
            }
            else
            {
                txtInitials.Enabled = false;
                txtSurnameAccv.Enabled = false;
                txtIDNoAccv.Enabled = false;
                cmbEntityAccv.Enabled = true;
                //txtBusinessnameAccv.Enabled = true;
                //txtReg1Accv.Enabled = true;
                //txtReg2Accv.Enabled = true;
                //txtReg3Accv.Enabled = true;
            }
        }
        private void cmbEntityAccv_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbEntityAccv.Text.ToString() == "Registered Company")
            {
                txtInitials.Enabled = false;
                txtSurnameAccv.Enabled = false;
                txtIDNoAccv.Enabled = false;
                txtBusinessnameAccv.Enabled = true;
                txtReg1Accv.Enabled = true;
                txtReg2Accv.Enabled = true;
                txtReg3Accv.Enabled = true;
                txtTrustNoAccV.Enabled = false;
                cmbEntityAccv.Enabled = true;
            }
            else if (cmbEntityAccv.Text.ToString() == "Sole Proprieter")
            {
                txtInitials.Enabled = false;
                txtSurnameAccv.Enabled = false;
                txtIDNoAccv.Enabled = true;
                txtBusinessnameAccv.Enabled = true;
                txtReg1Accv.Enabled = false;
                txtReg2Accv.Enabled = false;
                txtReg3Accv.Enabled = false;
                txtTrustNoAccV.Enabled = false;
                cmbEntityAccv.Enabled = true;
            }
            else if (cmbEntityAccv.Text.ToString() == "Trust")
            {
                txtInitials.Enabled = false;
                txtSurnameAccv.Enabled = false;
                txtIDNoAccv.Enabled = false;
                txtBusinessnameAccv.Enabled = true;
                txtReg1Accv.Enabled = false;
                txtReg2Accv.Enabled = false;
                txtReg3Accv.Enabled = false;
                txtTrustNoAccV.Enabled = true;
                cmbEntityAccv.Enabled = true;
            }

        }

        private void cmbAccTypeAccv_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbAccTypeAccv.Items.Count == 0)
                {
                    aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                    strToken = aXDSConnectWS.Login(Username, Password);
                    string axml = aXDSConnectWS.ConnectGetAccountTypes(strToken);

                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(axml);
                        ds.ReadXml(xmlSR);
                        cmbAccTypeAccv.DataSource = ds.Tables[0];
                        cmbAccTypeAccv.DisplayMember = ds.Tables[0].Columns[0].ToString();
                        cmbAccTypeAccv.ValueMember = ds.Tables[0].Columns[1].ToString();
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }

        }

        private void cmbBankNameAccv_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbBankNameAccv.Items.Count == 0)
                {
                    aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                    strToken = aXDSConnectWS.Login(Username, Password);
                    string axml = aXDSConnectWS.ConnectGetBankNames(strToken);

                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(axml);
                        ds.ReadXml(xmlSR);
                        cmbBankNameAccv.DataSource = ds.Tables[0];
                        cmbBankNameAccv.DisplayMember = ds.Tables[0].Columns[0].ToString();
                        cmbBankNameAccv.ValueMember = ds.Tables[0].Columns[1].ToString();
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        private void btnSubmittAccv_Click(object sender, EventArgs e)
        {
            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        XDSconnectSample.xdsconnect.AVSIDType idType = AVSIDType.SID;

                        switch (comboBoxavsidtype.Text)
                        {
                            case "South African ID":
                                idType = AVSIDType.SID;
                                break;
                            case "South African Passport":
                                idType = AVSIDType.SPP;
                                break;
                            case "Foreign Passport":
                                idType = AVSIDType.FPP;
                                break;
                            case "Company Registration Number":
                                idType = AVSIDType.SBR;
                                break;
                        }

                        if (cmbVerificationtype.Text.ToString() == "Individual")
                        {
                            if (treeView1.SelectedNode.Text == "Account Verification Real Time")
                            {
                                txtXMLMatchResultAccv.Text = aXDSConnectWS.ConnectAccountVerificationRealTime(strToken,
                                    XDSconnectSample.xdsconnect.TypeofVerificationenum.Individual, Entity.None, txtInitials.Text,
                                    txtSurnameAccv.Text, txtIDNoAccv.Text, idType,
                                    txtBusinessnameAccv.Text, txtReg1Accv.Text, txtReg2Accv.Text, txtReg3Accv.Text, txtTrustNoAccV.Text,
                                    txtAccNoAccv.Text, txtBranchCodeAccv.Text, cmbAccTypeAccv.SelectedValue.ToString(),
                                    cmbBankNameAccv.SelectedValue.ToString(), txtVoucherCodeAccv.Text, txtExternalRefAccv.Text);
                            }
                            else
                                txtXMLMatchResultAccv.Text = aXDSConnectWS.ConnectAccountVerification(strToken, 
                                    XDSconnectSample.xdsconnect.TypeofVerificationenum.Individual, Entity.None, txtInitials.Text, 
                                    txtSurnameAccv.Text, txtIDNoAccv.Text, txtBusinessnameAccv.Text, txtReg1Accv.Text, 
                                    txtReg2Accv.Text, txtReg3Accv.Text, txtTrustNoAccV.Text, txtAccNoAccv.Text, txtBranchCodeAccv.Text, 
                                    cmbAccTypeAccv.SelectedValue.ToString(), cmbBankNameAccv.SelectedValue.ToString(), txtCFirstNameAccv.Text, 
                                    txtCSurnameAccv.Text, txtCEmailAddAccv.Text, true, txtVoucherCodeAccv.Text, txtExternalRefAccv.Text);
                        }
                        else
                        {
                            if (treeView1.SelectedNode.Text == "Account Verification Real Time")
                            {
                                txtXMLMatchResultAccv.Text = aXDSConnectWS.ConnectAccountVerificationRealTime(strToken,
                                    XDSconnectSample.xdsconnect.TypeofVerificationenum.Company,
                                     (cmbEntityAccv.Text == "Registered Company" ? Entity.RegisteredCompany :
                                    (cmbEntityAccv.Text == "Sole Proprieter" ? Entity.SoleProp :
                                    (cmbEntityAccv.Text == "Trust" ? Entity.Trust : Entity.None))), txtInitials.Text,
                                    txtSurnameAccv.Text, txtIDNoAccv.Text, idType,
                                    txtBusinessnameAccv.Text, txtReg1Accv.Text, txtReg2Accv.Text, txtReg3Accv.Text, txtTrustNoAccV.Text,
                                    txtAccNoAccv.Text, txtBranchCodeAccv.Text, cmbAccTypeAccv.SelectedValue.ToString(),
                                    cmbBankNameAccv.SelectedValue.ToString(), txtVoucherCodeAccv.Text, txtExternalRefAccv.Text);
                            }
                            else
                                txtXMLMatchResultAccv.Text = aXDSConnectWS.ConnectAccountVerification(strToken, 
                                    XDSconnectSample.xdsconnect.TypeofVerificationenum.Company, 
                                    (cmbEntityAccv.Text == "Registered Company" ? Entity.RegisteredCompany : 
                                    (cmbEntityAccv.Text == "Sole Proprieter" ? Entity.SoleProp : 
                                    (cmbEntityAccv.Text == "Trust" ? 
                                    Entity.Trust : Entity.None))), 
                                    txtInitials.Text, txtSurnameAccv.Text, txtIDNoAccv.Text, txtBusinessnameAccv.Text, txtReg1Accv.Text, 
                                    txtReg2Accv.Text, txtReg3Accv.Text, txtTrustNoAccV.Text, txtAccNoAccv.Text, txtBranchCodeAccv.Text, 
                                    cmbAccTypeAccv.SelectedValue.ToString(), cmbBankNameAccv.SelectedValue.ToString(), txtCFirstNameAccv.Text, 
                                    txtCSurnameAccv.Text, txtCEmailAddAccv.Text, true, txtVoucherCodeAccv.Text, txtExternalRefAccv.Text);
                        }
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    XDSconnectSample.xdsconnect.AVSIDType idType = AVSIDType.SID;

                    switch (comboBoxavsidtype.Text)
                    {
                        case "South African ID":
                            idType = AVSIDType.SID;
                            break;
                        case "South African Passport":
                            idType = AVSIDType.SPP;
                            break;
                        case "Foreign Passport":
                            idType = AVSIDType.FPP;
                            break;
                        case "Company Registration Number":
                            idType = AVSIDType.SBR;
                            break;
                    }

                    if (cmbVerificationtype.Text.ToString() == "Individual")
                    {
                        if (treeView1.SelectedNode.Text == "Account Verification Real Time")
                        {
                            txtXMLMatchResultAccv.Text = aXDSConnectWS.ConnectAccountVerificationRealTime(strToken,
                                XDSconnectSample.xdsconnect.TypeofVerificationenum.Individual, Entity.None, txtInitials.Text,
                                txtSurnameAccv.Text, txtIDNoAccv.Text, idType,
                                txtBusinessnameAccv.Text, txtReg1Accv.Text, txtReg2Accv.Text, txtReg3Accv.Text, txtTrustNoAccV.Text,
                                txtAccNoAccv.Text, txtBranchCodeAccv.Text, cmbAccTypeAccv.SelectedValue.ToString(),
                                cmbBankNameAccv.SelectedValue.ToString(), txtVoucherCodeAccv.Text, txtExternalRefAccv.Text);
                        }
                        else
                            txtXMLMatchResultAccv.Text = aXDSConnectWS.ConnectAccountVerification(strToken, XDSconnectSample.xdsconnect.TypeofVerificationenum.Individual, Entity.None, txtInitials.Text, txtSurnameAccv.Text, txtIDNoAccv.Text, txtBusinessnameAccv.Text, txtReg1Accv.Text, txtReg2Accv.Text, txtReg3Accv.Text, txtTrustNoAccV.Text, txtAccNoAccv.Text, txtBranchCodeAccv.Text, cmbAccTypeAccv.SelectedValue.ToString(), cmbBankNameAccv.SelectedValue.ToString(), txtCFirstNameAccv.Text, txtCSurnameAccv.Text, txtCEmailAddAccv.Text, true, txtVoucherCodeAccv.Text, txtExternalRefAccv.Text);
                    }
                    else
                    {
                        if (treeView1.SelectedNode.Text == "Account Verification Real Time")
                        {
                            txtXMLMatchResultAccv.Text = aXDSConnectWS.ConnectAccountVerificationRealTime(strToken,
                                XDSconnectSample.xdsconnect.TypeofVerificationenum.Company,
                                 (cmbEntityAccv.Text == "Registered Company" ? Entity.RegisteredCompany :
                                (cmbEntityAccv.Text == "Sole Proprieter" ? Entity.SoleProp :
                                (cmbEntityAccv.Text == "Trust" ? Entity.Trust : Entity.None))), txtInitials.Text,
                                txtSurnameAccv.Text, txtIDNoAccv.Text, idType,
                                txtBusinessnameAccv.Text, txtReg1Accv.Text, txtReg2Accv.Text, txtReg3Accv.Text, txtTrustNoAccV.Text,
                                txtAccNoAccv.Text, txtBranchCodeAccv.Text, cmbAccTypeAccv.SelectedValue.ToString(),
                                cmbBankNameAccv.SelectedValue.ToString(), txtVoucherCodeAccv.Text, txtExternalRefAccv.Text);
                        }
                        else
                            txtXMLMatchResultAccv.Text = aXDSConnectWS.ConnectAccountVerification(strToken, XDSconnectSample.xdsconnect.TypeofVerificationenum.Company, (cmbEntityAccv.Text == "Registered Company" ? Entity.RegisteredCompany : (cmbEntityAccv.Text == "Sole Proprieter" ? Entity.SoleProp : (cmbEntityAccv.Text == "Trust" ? Entity.Trust : Entity.None))), txtInitials.Text, txtSurnameAccv.Text, txtIDNoAccv.Text, txtBusinessnameAccv.Text, txtReg1Accv.Text, txtReg2Accv.Text, txtReg3Accv.Text, txtTrustNoAccV.Text, txtAccNoAccv.Text, txtBranchCodeAccv.Text, cmbAccTypeAccv.SelectedValue.ToString(), cmbBankNameAccv.SelectedValue.ToString(), txtCFirstNameAccv.Text, txtCSurnameAccv.Text, txtCEmailAddAccv.Text, true, txtVoucherCodeAccv.Text, txtExternalRefAccv.Text);
                    }

                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        private void btnClearAccv_Click(object sender, EventArgs e)
        {
            txtInitials.Text = string.Empty;
            txtSurnameAccv.Text = string.Empty;
            txtIDNoAccv.Text = string.Empty;
            txtBusinessnameAccv.Text = string.Empty;
            txtReg1Accv.Text = string.Empty;
            txtReg2Accv.Text = string.Empty;
            txtReg3Accv.Text = string.Empty;
            txtAccNoAccv.Text = string.Empty;
            txtBranchCodeAccv.Text = string.Empty;
            txtCFirstNameAccv.Text = string.Empty;
            txtCSurnameAccv.Text = string.Empty;
            txtCEmailAddAccv.Text = string.Empty;
            txtVoucherCodeAccv.Text = string.Empty;
            txtExternalRefAccv.Text = string.Empty;
         }

        #endregion Account Verification

        #region Bank Code Request

        private void cmbverificationtypeBCR_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbverificationtypeBCR.Text.ToString() == "Individual")
            {
                
                txtIDNoBCR.Enabled = true;
                txtReg1BCR.Enabled = false;
                txtReg2BCR.Enabled = false;
                txtReg3BCR.Enabled = false;
                txtTrustNoBCR.Enabled = false;
                cmbEntityBCR.Enabled = false;
            }
            else
            {               
                txtIDNoBCR.Enabled = false;
                cmbEntityBCR.Enabled = true;                
            }
        }

        private void cmbEntityBCR_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbEntityBCR.Text.ToString() == "Registered Company")
            {                
                txtIDNoBCR.Enabled = false;                
                txtReg1BCR.Enabled = true;
                txtReg2BCR.Enabled = true;
                txtReg3BCR.Enabled = true;
                txtTrustNoBCR.Enabled = false;
                cmbEntityBCR.Enabled = true;
            }
            else if (cmbEntityBCR.Text.ToString() == "Sole Proprieter")
            {
                txtIDNoBCR.Enabled = true;                
                txtReg1BCR.Enabled = false;
                txtReg2BCR.Enabled = false;
                txtReg3BCR.Enabled = false;
                txtTrustNoBCR.Enabled = false;
                cmbEntityBCR.Enabled = true;
            }
            else if (cmbEntityBCR.Text.ToString() == "Trust")
            {
                txtIDNoBCR.Enabled = false;
                txtReg1BCR.Enabled = false;
                txtReg2BCR.Enabled = false;
                txtReg3BCR.Enabled = false;
                txtTrustNoBCR.Enabled = true;
                cmbEntityBCR.Enabled = true;
            }
        }

        private void cmbRequestTypeBCR_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbRequestTypeBCR.Items.Count == 0)
                {
                    aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                    strToken = aXDSConnectWS.Login(Username, Password);
                    string axml = aXDSConnectWS.ConnectGetBankCodeRequestType(strToken);

                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(axml);
                        ds.ReadXml(xmlSR);
                        cmbRequestTypeBCR.DataSource = ds.Tables[0];
                        cmbRequestTypeBCR.DisplayMember = ds.Tables[0].Columns[0].ToString();
                        cmbRequestTypeBCR.ValueMember = ds.Tables[0].Columns[1].ToString();
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }

        }

        private void cmbBankNameBCR_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbBankNameBCR.Items.Count == 0)
                {
                    aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                    strToken = aXDSConnectWS.Login(Username, Password);
                    string axml = aXDSConnectWS.ConnectBCGetBankNames(strToken);

                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(axml);
                        ds.ReadXml(xmlSR);
                        cmbBankNameBCR.DataSource = ds.Tables[0];
                        cmbBankNameBCR.DisplayMember = ds.Tables[0].Columns[0].ToString();
                        cmbBankNameBCR.ValueMember = ds.Tables[0].Columns[1].ToString();
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }

        }        


        private void cmbTermsBCR_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbTermsBCR.Items.Count == 0)
                {
                    aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                    strToken = aXDSConnectWS.Login(Username, Password);
                    string axml = aXDSConnectWS.ConnectGetTerms(strToken,44);

                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(axml);
                        ds.ReadXml(xmlSR);
                        cmbTermsBCR.DataSource = ds.Tables[0];
                        cmbTermsBCR.DisplayMember = ds.Tables[0].Columns[0].ToString();
                        cmbTermsBCR.ValueMember = ds.Tables[0].Columns[0].ToString();
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }

        }

        private void btnSubmittBCR_Click(object sender, EventArgs e)
        {
            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        if (cmbverificationtypeBCR.Text.ToString() == "Individual")
                        {
                            txtXMLMatchResultBCR.Text = aXDSConnectWS.ConnectBankCodesRequest(strToken, cmbRequestTypeBCR.SelectedValue.ToString(), TypeofVerificationenum.Individual, Entity.None, txtIDNoBCR.Text, txtReg1BCR.Text, txtReg2BCR.Text, txtReg3BCR.Text, txtTrustNoBCR.Text, cmbBankNameBCR.SelectedValue.ToString(), txtBranchCodeBCR.Text, txtBranchCodeBCR.Text, txtAccNoBCR.Text, txtAccholderBCR.Text, Convert.ToDouble(txtAmountBCR.Text), cmbTermsBCR.SelectedValue.ToString(), XDSconnectSample.xdsconnect.Region.Gauteng, CountryCode.ZAF, txtAdditionalInfoBCR.Text, txtFirstNameBCR.Text, txtSurnameBCR.Text, txtCompanyBCR.Text, txtContactNoBCR.Text, txtEmailAddBCR.Text, true, txtVoucherCodeBCR.Text, txtExternalRefBCR.Text);
                        }
                        else
                        {
                            txtXMLMatchResultBCR.Text = aXDSConnectWS.ConnectBankCodesRequest(strToken, cmbRequestTypeBCR.SelectedValue.ToString(), TypeofVerificationenum.Company, (cmbEntityBCR.Text == "Registered Company" ? Entity.RegisteredCompany : (cmbEntityBCR.Text == "Sole Proprieter" ? Entity.SoleProp : (cmbEntityBCR.Text == "Trust" ? Entity.Trust : Entity.None))), txtIDNoBCR.Text, txtReg1BCR.Text, txtReg2BCR.Text, txtReg3BCR.Text, txtTrustNoBCR.Text, cmbBankNameBCR.SelectedValue.ToString(), txtBranchCodeBCR.Text, txtBranchCodeBCR.Text, txtAccNoBCR.Text, txtAccholderBCR.Text, Convert.ToDouble(txtAmountBCR.Text), cmbTermsBCR.SelectedValue.ToString(), XDSconnectSample.xdsconnect.Region.Gauteng, CountryCode.ZAF, txtAdditionalInfoBCR.Text, txtFirstNameBCR.Text, txtSurnameBCR.Text, txtCompanyBCR.Text, txtContactNoBCR.Text, txtEmailAddBCR.Text, true, txtVoucherCodeBCR.Text, txtExternalRefBCR.Text);
                        }
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    if (cmbverificationtypeBCR.Text.ToString() == "Individual")
                    {
                        txtXMLMatchResultBCR.Text = aXDSConnectWS.ConnectBankCodesRequest(strToken, cmbRequestTypeBCR.SelectedValue.ToString(), TypeofVerificationenum.Individual, Entity.None, txtIDNoBCR.Text, txtReg1BCR.Text, txtReg2BCR.Text, txtReg3BCR.Text, txtTrustNoBCR.Text, cmbBankNameBCR.SelectedValue.ToString(), txtBranchCodeBCR.Text, txtBranchCodeBCR.Text, txtAccNoBCR.Text, txtAccholderBCR.Text, Convert.ToDouble(txtAmountBCR.Text), cmbTermsBCR.SelectedValue.ToString(), XDSconnectSample.xdsconnect.Region.Gauteng, CountryCode.ZAF, txtAdditionalInfoBCR.Text, txtFirstNameBCR.Text, txtSurnameBCR.Text, txtCompanyBCR.Text, txtContactNoBCR.Text, txtEmailAddBCR.Text, true, txtVoucherCodeBCR.Text, txtExternalRefBCR.Text);
                    }
                    else
                    {
                        txtXMLMatchResultBCR.Text = aXDSConnectWS.ConnectBankCodesRequest(strToken, cmbRequestTypeBCR.SelectedValue.ToString(), TypeofVerificationenum.Company, (cmbEntityBCR.Text == "Registered Company" ? Entity.RegisteredCompany : (cmbEntityBCR.Text == "Sole Proprieter" ? Entity.SoleProp : (cmbEntityBCR.Text == "Trust" ? Entity.Trust : Entity.None))), txtIDNoBCR.Text, txtReg1BCR.Text, txtReg2BCR.Text, txtReg3BCR.Text, txtTrustNoBCR.Text, cmbBankNameBCR.SelectedValue.ToString(), txtBranchCodeBCR.Text, txtBranchCodeBCR.Text, txtAccNoBCR.Text, txtAccholderBCR.Text, Convert.ToDouble(txtAmountBCR.Text), cmbTermsBCR.SelectedValue.ToString(), XDSconnectSample.xdsconnect.Region.Gauteng, CountryCode.ZAF, txtAdditionalInfoBCR.Text, txtFirstNameBCR.Text, txtSurnameBCR.Text, txtCompanyBCR.Text, txtContactNoBCR.Text, txtEmailAddBCR.Text, true, txtVoucherCodeBCR.Text, txtExternalRefBCR.Text);
                    }

                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        private void btnClearBCR_Click(object sender, EventArgs e)
        {
            
            txtBranchNameBCR.Text = string.Empty;
            txtBranchCodeBCR.Text = string.Empty;
            txtAccNoBCR.Text = string.Empty;
            txtAccholderBCR.Text = string.Empty;
            txtAmountBCR.Text = string.Empty;
            txtAdditionalInfoBCR.Text = string.Empty;
            txtFirstNameBCR.Text = string.Empty;
            txtSurnameBCR.Text = string.Empty;
            txtCompanyBCR.Text = string.Empty;
            txtContactNoBCR.Text = string.Empty;
            txtEmailAddBCR.Text = string.Empty;
            txtVoucherCodeBCR.Text = string.Empty;
            txtExternalRefBCR.Text = string.Empty;
        }

        #endregion Bank Code Request

        #region GetAccountVerificationResult
        
        private void btnviewGetAccR_Click(object sender, EventArgs e)
        {
            try
            {
                int intEnquiryLogID = int.Parse(txtEnquiryLogID.Text);
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtXMLReportGetAccR.Text = aXDSConnectWS.ConnectGetAccountVerificationResult(strToken, intEnquiryLogID);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtXMLReportGetAccR.Text = aXDSConnectWS.ConnectGetAccountVerificationResult(strToken, intEnquiryLogID);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }

        }
        #endregion GetAccountVerificationResult

        #region Verify ID

        private void btnSubmitVID_Click(object sender, EventArgs e)
        {
            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {

                        txtXMLReportResultVID.Text = aXDSConnectWS.ConnectHomeAffairsIDVerification(strToken,46,txtIDNoVID.Text,txtFirstNameVID.Text,txtSurnameVID.Text,txtExternalRefVID.Text,txtVouchercodeVID.Text );
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {

                    txtXMLReportResultVID.Text = aXDSConnectWS.ConnectHomeAffairsIDVerification(strToken, 46, txtIDNoVID.Text, txtFirstNameVID.Text, txtSurnameVID.Text, txtExternalRefVID.Text, txtVouchercodeVID.Text);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        private void btnClearVID_Click(object sender, EventArgs e)
        {
            txtIDNoVID.Text = string.Empty;
            txtFirstNameVID.Text = string.Empty;
            txtSurnameVID.Text = string.Empty;
            txtVouchercodeVID.Text = string.Empty;
            txtExternalRefVID.Text = string.Empty;
        }


        #endregion Verify ID

        #region Profile ID 

        private void btnSubmitPID_Click(object sender, EventArgs e)
        {

            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {

                        txtXMLReportResultPID.Text = aXDSConnectWS.ConnectHomeAffairsIDVerification(strToken, 47, txtIDNoPID.Text, txtFirstnamePID.Text, txtSurnamePID.Text, txtExternalRefPID.Text, txtVouchercodePID.Text);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {

                    txtXMLReportResultPID.Text = aXDSConnectWS.ConnectHomeAffairsIDVerification(strToken, 47, txtIDNoPID.Text, txtFirstnamePID.Text, txtSurnamePID.Text, txtExternalRefPID.Text, txtVouchercodePID.Text);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        private void btnClearPID_Click(object sender, EventArgs e)
        {
            txtIDNoPID.Text = string.Empty;
            txtFirstnamePID.Text = string.Empty;
            txtSurnamePID.Text = string.Empty;
            txtVouchercodePID.Text = string.Empty;
            txtExternalRefPID.Text = string.Empty;
        }

        #endregion Profile ID

        #region Individual file

        private void btnSubmitIndF_Click(object sender, EventArgs e)
        {

            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {

                        txtXMLReportResultIndF.Text = aXDSConnectWS.ConnectHomeAffairsIDVerification(strToken, 48, txtIDNoIndF.Text, txtFirstnameIndF.Text, txtSurnameIndF.Text, txtExternalRefIndF.Text, txtVouchercodeIndF.Text);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {

                    txtXMLReportResultIndF.Text = aXDSConnectWS.ConnectHomeAffairsIDVerification(strToken, 48, txtIDNoIndF.Text, txtFirstnameIndF.Text, txtSurnameIndF.Text, txtExternalRefIndF.Text, txtVouchercodeIndF.Text);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        private void btnClearIndF_Click(object sender, EventArgs e)
        {
            txtIDNoIndF.Text = string.Empty;
            txtSurnameIndF.Text = string.Empty;
            txtFirstnameIndF.Text = string.Empty;
            txtVouchercodeIndF.Text = string.Empty;
            txtExternalRefIndF.Text = string.Empty;
        }

        #endregion

        #region Request Birth Certificate

        private void btnSubmitRBC_Click(object sender, EventArgs e)
        {

            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {

                        txtXMLReportresultRBC.Text = aXDSConnectWS.ConnectRequestBirthorDeathCertificate(strToken, 49, txtIDNoRBC.Text, txtFirstnameRBC.Text, txtSurnameRBC.Text,txtCFirstNameRBC.Text,txtCSurnameRBC.Text,txtCCompanyRBC.Text,txtCContactNumberRBC.Text,txtCEmailAddressRBC.Text,txtVoucherCodeRBC.Text, txtExternalRefRBC.Text);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {

                    txtXMLReportresultRBC.Text = aXDSConnectWS.ConnectRequestBirthorDeathCertificate(strToken, 49, txtIDNoRBC.Text, txtFirstnameRBC.Text, txtSurnameRBC.Text, txtCFirstNameRBC.Text, txtCSurnameRBC.Text, txtCCompanyRBC.Text, txtCContactNumberRBC.Text, txtCEmailAddressRBC.Text, txtVoucherCodeRBC.Text, txtExternalRefRBC.Text);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        private void btnClearRBC_Click(object sender, EventArgs e)
        {
            txtIDNoRBC.Text = string.Empty;
            txtFirstnameRBC.Text = string.Empty;
            txtSurnameRBC.Text = string.Empty;
            txtCFirstNameRBC.Text = string.Empty;
            txtCSurnameRBC.Text = string.Empty;
            txtCCompanyRBC.Text = string.Empty;
            txtCContactNumberRBC.Text = string.Empty;
            txtCEmailAddressRBC.Text = string.Empty;
            txtVoucherCodeRBC.Text = string.Empty;
            txtExternalRefRBC.Text = string.Empty;
        }

        #endregion Request Birth Certificate

        #region Request Death Certificate

        private void btnSubmitRDC_Click(object sender, EventArgs e)
        {

            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {

                        txtXMLReportResultRDC.Text = aXDSConnectWS.ConnectRequestBirthorDeathCertificate(strToken, 49, txtIDNumberRDC.Text, txtFirstnameRDC.Text, txtSurnameRDC.Text, txtCFirstnameRDC.Text, txtCSurnameRDC.Text, txtCCompanyRDC.Text, txtCContactNoRDC.Text, txtCEmailAddressRDC.Text, txtVoucherCodeRDC.Text, txtExternalRefRDC.Text);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {

                    txtXMLReportResultRDC.Text = aXDSConnectWS.ConnectRequestBirthorDeathCertificate(strToken, 49, txtIDNumberRDC.Text, txtFirstnameRDC.Text, txtSurnameRDC.Text, txtCFirstnameRDC.Text, txtCSurnameRDC.Text, txtCCompanyRDC.Text, txtCContactNoRDC.Text, txtCEmailAddressRDC.Text, txtVoucherCodeRDC.Text, txtExternalRefRDC.Text);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        private void btnClearRDC_Click(object sender, EventArgs e)
        {

            txtIDNumberRDC.Text = string.Empty;
            txtFirstnameRDC.Text = string.Empty;
            txtSurnameRDC.Text = string.Empty;
            txtCFirstnameRDC.Text = string.Empty;
            txtCSurnameRDC.Text = string.Empty;
            txtCCompanyRDC.Text = string.Empty;
            txtCContactNoRDC.Text = string.Empty;
            txtCEmailAddressRDC.Text = string.Empty;
            txtVoucherCodeRDC.Text = string.Empty;
            txtExternalRefRDC.Text = string.Empty;
        }

        #endregion Request Death Certificate

        #region Authentication

        private void btnsearchM_Click(object sender, EventArgs e)
        {
            try
            {
                ClearInfo();
                grdvwMatchresultM.DataSource = null;
                string XMlMatchResult = string.Empty, XMLprofile = string.Empty;
               // aAuthConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aAuthConnectWS.IsTicketValid(strToken))
                {
                    strToken = aAuthConnectWS.Login(Username, Password);
                    if (aAuthConnectWS.IsTicketValid(strToken))
                    {
                        //XMlMatchResult = aXDSConnectWS.ConnectFraudConsumerMatch(strToken, searchtype, cmbBranch.SelectedValue.ToString(), int.Parse(cmbpurpose.SelectedValue.ToString()), txtIDNoM.Text.ToString(), txtPassportNoM.Text.ToString(), string.Empty, txtCellNumberM.Text, string.Empty, string.Empty, chkOverOTP.Checked, txtOverOTPReason.Text, string.Empty, string.Empty);
                        XMlMatchResult = aAuthConnectWS.ConnectFraudConsumerMatch(strToken, searchtype, cmbBranch.SelectedValue.ToString(), int.Parse(cmbpurpose.SelectedValue.ToString()), txtIDNoM.Text.ToString(), txtPassportNoM.Text.ToString(), string.Empty, txtCellNumberM.Text, string.Empty, string.Empty, string.Empty, string.Empty);
                        
                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(XMlMatchResult);
                        ds.ReadXml(xmlSR);
                        grdvwMatchresultM.DataSource = ds.Tables[0];
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    XMlMatchResult = aAuthConnectWS.ConnectFraudConsumerMatch(strToken, searchtype, cmbBranch.SelectedValue.ToString(), int.Parse(cmbpurpose.SelectedValue.ToString()), txtIDNoM.Text.ToString(), txtPassportNoM.Text.ToString(), string.Empty, txtCellNumberM.Text, string.Empty, string.Empty, string.Empty, string.Empty);

                    DataSet ds = new DataSet();
                    System.IO.StringReader xmlSR = new System.IO.StringReader(XMlMatchResult);
                    ds.ReadXml(xmlSR);
                    grdvwMatchresultM.DataSource = ds.Tables[0];
                }
                
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }

        }

        private void btnsearchgetQ_Click(object sender, EventArgs e)
        {
            try{
                ClearInfo();
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                pnlAuthQuestions.Visible = true;
                //Application.DoEvents();            
                
                if (!aAuthConnectWS.IsTicketValid(strToken))
                {
                    strToken = aAuthConnectWS.Login(Username, Password);
                    if (aAuthConnectWS.IsTicketValid(strToken))
                    {

                        moMyAuthenticationManager = aAuthConnectWS.ConnectFraudGetQuestions(strToken, 0, int.Parse(txtSubEnConsID.Text));
                       

                        txtRefNoM.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.ReferenceNo;
                        txtAuthTypeM.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.AuthenticationTypeInd;
                        txtAuthStatusM.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.AuthenticationStatusInd;
                        txtAuthMsgM.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.RepeatAuthenticationMessage;

                        txtFNameMP.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.FirstName;
                        txtSNameMP.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.SecondName;
                        txtSurNameMP.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.Surname;
                        txtIDNoMP.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.IDNo;
                        txtGenderMP.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.Gender;

                        txtFNameMH.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.Ha_FirstName;
                        txtSNameMH.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.Ha_SecondName;
                        txtSurnameMH.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.Ha_Surname;
                        txtIdNoMH.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.Ha_IDNo;
                        txtDeceasedStatusMH.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.Ha_DeaseasedStatus;
                        txtDeceasedDateMH.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.Ha_DeaseasedDate;
                        txtCauseOfDeathMH.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.Ha_CauseOfDeath;

                        txtAccountAgeDetails.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.ConsumerAccountAgeMessage;
                        txtErrorMessageM.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.ErrorMessage;
                        txtSAFPSM.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.SAFPSIndicator ? "ID Number Listed on SAFPS Database" : "ID Number not listed on SAFPS Database";

                        if (!(moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.Questions == null))
                        {
                            foreach (AuthWS.QuestionDocument oQuestionStructure in moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.Questions)
                            {
                                ListViewItem oListViewItem = new ListViewItem(oQuestionStructure.Question);
                                oListViewItem.Tag = oQuestionStructure.ProductAuthenticationQuestionID;
                                lstQuestions.Items.Add(oListViewItem);
                            }
                        }


                        lstQuestions.Enabled = true;
                        cmbAnswersM.Enabled = true;
                        btnAuthenticateM.Enabled = true;
                        btnVoidM.Enabled = true;
                        btnReferToFraud.Enabled = true;
                        btnAuthHisM.Enabled = true;

                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {

                    moMyAuthenticationManager = aAuthConnectWS.ConnectFraudGetQuestions(strToken, 0, int.Parse(txtSubEnConsID.Text));
                    

                    txtRefNoM.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.ReferenceNo;
                    txtAuthTypeM.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.AuthenticationTypeInd;
                    txtAuthStatusM.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.AuthenticationStatusInd;
                    txtAuthMsgM.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.RepeatAuthenticationMessage;

                    txtFNameMP.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.FirstName;
                    txtSNameMP.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.SecondName;
                    txtSurNameMP.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.Surname;
                    txtIDNoMP.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.IDNo;
                    txtGenderMP.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.Gender;

                    txtFNameMH.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.Ha_FirstName;
                    txtSNameMH.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.Ha_SecondName;
                    txtSurnameMH.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.Ha_Surname;
                    txtIdNoMH.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.Ha_IDNo;
                    txtDeceasedStatusMH.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.Ha_DeaseasedStatus;
                    txtDeceasedDateMH.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.Ha_DeaseasedDate;
                    txtCauseOfDeathMH.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.Ha_CauseOfDeath;

                    txtAccountAgeDetails.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.ConsumerAccountAgeMessage;
                    txtErrorMessageM.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.ErrorMessage;
                    txtSAFPSM.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.SAFPSIndicator ? "ID Number Listed on SAFPS Database" : "ID Number not listed on SAFPS Database";

                    if (!(moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.Questions == null))
                    {
                        foreach (AuthWS.QuestionDocument oQuestionStructure in moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.Questions)
                        {
                            ListViewItem oListViewItem = new ListViewItem(oQuestionStructure.Question);
                            oListViewItem.Tag = oQuestionStructure.ProductAuthenticationQuestionID;
                            lstQuestions.Items.Add(oListViewItem);
                        }
                    }

                   dgAuthHisM.DataSource = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.AuthenticationHistory;

                    lstQuestions.Enabled = true;
                    cmbAnswersM.Enabled = true;
                    btnAuthenticateM.Enabled = true;
                    btnVoidM.Enabled = true;
                    pnlfinalresult.Visible = false;
                    btnReferToFraud.Enabled = true;
                    btnAuthHisM.Enabled = true;
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        private void lstQuestions_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (lstQuestions.SelectedItems.Count > 0)
                {

                    long lQuestionID = System.Convert.ToInt64(lstQuestions.SelectedItems[0].Tag);
                    AuthWS.QuestionDocument oQuestionStructure = GetQuestionStructure(lQuestionID);
                    PopulateAnswerDropDown(oQuestionStructure);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }

        }

        private AuthWS.QuestionDocument GetQuestionStructure(long QuestionID)
        {
            AuthWS.QuestionDocument[] oQuestionDocument = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.Questions;
            
            foreach (AuthWS.QuestionDocument oQuestionDoc in oQuestionDocument)
            {
                if (oQuestionDoc.ProductAuthenticationQuestionID == QuestionID)
                {
                    return oQuestionDoc;
                }
            }
            return null;
        }

        private void cmbAnswersM_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstQuestions.SelectedItems.Count > 0 && cmbAnswersM.SelectedItem != null)
            {
                long lQuestionID = System.Convert.ToInt64(lstQuestions.SelectedItems[0].Tag);

                AuthWS.QuestionDocument oQuestionDocument = GetQuestionStructure(lQuestionID);
                foreach (AuthWS.AnswerDocument oAnswerDocument in oQuestionDocument.Answers)
                {
                    oAnswerDocument.IsEnteredAnswerYN = false;
                    if (oAnswerDocument.Answer == cmbAnswersM.SelectedItem.ToString())
                    {
                        oAnswerDocument.IsEnteredAnswerYN = true;
                    }

                }
            }
        }

        private void PopulateAnswerDropDown(AuthWS.QuestionDocument aQuestionStructure)
        {

            cmbAnswersM.Items.Clear();
            cmbAnswersM.Items.Add("");
            foreach (AuthWS.AnswerDocument oAnswerStructure in aQuestionStructure.Answers)
            {
                cmbAnswersM.Items.Add(oAnswerStructure.Answer);
                if (oAnswerStructure.IsEnteredAnswerYN)
                {
                    cmbAnswersM.SelectedItem = oAnswerStructure.Answer;
                }
            }
        }

        private void btnAuthenticateM_Click(object sender, EventArgs e)
        {
            try
            {
                string strXMLResult = string.Empty;
                moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.OTPValue = txtOTP.Text;
                
                if (!aAuthConnectWS.IsTicketValid(strToken))
                {
                    strToken = aAuthConnectWS.Login(Username, Password);
                
                    if (aAuthConnectWS.IsTicketValid(strToken))
                    {                     

                        moMyAuthenticationManager = aAuthConnectWS.ConnectFraudProcess(strToken, AuthWS.AuthenticationProcessAction.Authenticate, moMyAuthenticationManager, string.Empty.ToString());
                        
                    }
                    if (!(strToken == string.Empty))
                    {
                        MessageBox.Show(strToken);
                    }

                }
                else
                {

                    //string xml = @"<?xml version=""1.0"" encoding=""utf-16""?> <AuthenticationProcess><CurrentObjectState><AuthenticationDocument><SubscriberID>12509</SubscriberID><SubscriberAuthenticationID>172108</SubscriberAuthenticationID><AuthenticationDate>2016-07-05T09:11:31.905+02:00</AuthenticationDate><AuthenticationStatusInd>A</AuthenticationStatusInd><TotalQuestionPointValue>0</TotalQuestionPointValue><RequiredAuthenticatedPerc>0.0</RequiredAuthenticatedPerc><AuthenticatedPerc>0.0</AuthenticatedPerc><ActionedBySystemUserID>0</ActionedBySystemUserID><SAFPSIndicator>false</SAFPSIndicator><IsUserBlocked>false</IsUserBlocked><PersonalQuestionsenabled>false</PersonalQuestionsenabled><BlockingEnabledFlag>false</BlockingEnabledFlag><ReferToFraud>false</ReferToFraud><blockid>0</blockid><BlockingReason /><QuestionTimeout>0</QuestionTimeout><ErrorMessage /><VoidorFraudReason /><NoofAttemptsRemaining>0</NoofAttemptsRemaining><OTPStatus>false</OTPStatus><OTPEnabled>false</OTPEnabled><OTPNotGeneratedReason /><OTPValue /><AuthenticationStatusReason>Authenticated</AuthenticationStatusReason><SAFPSMessage /><Questions><QuestionDocument><ProductAuthenticationQuestionID>5003</ProductAuthenticationQuestionID><Question>Do you receive statements at 22 SHAMROCK RD, -, KEMPTON PARK, GAUTENG, 1619?</Question><QuestionPointValue>0</QuestionPointValue><RequiredNoOfAnswers>0</RequiredNoOfAnswers><Answers><AnswerDocument><AnswerID>1195461</AnswerID><Answer>Yes</Answer><IsEnteredAnswerYN>true</IsEnteredAnswerYN></AnswerDocument></Answers></QuestionDocument><QuestionDocument><ProductAuthenticationQuestionID>8071</ProductAuthenticationQuestionID><Question>Are you married to NOMASOMI ANGELA?</Question><QuestionPointValue>0</QuestionPointValue><RequiredNoOfAnswers>0</RequiredNoOfAnswers><Answers><AnswerDocument><AnswerID>1195473</AnswerID><Answer>Yes</Answer><IsEnteredAnswerYN>true</IsEnteredAnswerYN></AnswerDocument></Answers></QuestionDocument><QuestionDocument><ProductAuthenticationQuestionID>8060</ProductAuthenticationQuestionID><Question>In what year did you purchase the property at 460, SHAMROCK, BREDELL AH EXT 2, BREDELL AH, KEMPTON PARK?</Question><QuestionPointValue>0</QuestionPointValue><RequiredNoOfAnswers>0</RequiredNoOfAnswers><Answers><AnswerDocument><AnswerID>1195463</AnswerID><Answer>1989</Answer><IsEnteredAnswerYN>true</IsEnteredAnswerYN></AnswerDocument></Answers></QuestionDocument><QuestionDocument><ProductAuthenticationQuestionID>8084</ProductAuthenticationQuestionID><Question>Name one of your exisiting or previous cell phone numbers?</Question><QuestionPointValue>0</QuestionPointValue><RequiredNoOfAnswers>0</RequiredNoOfAnswers><Answers><AnswerDocument><AnswerID>1195468</AnswerID><Answer>0836448898</Answer><IsEnteredAnswerYN>true</IsEnteredAnswerYN></AnswerDocument></Answers></QuestionDocument><QuestionDocument><ProductAuthenticationQuestionID>1014</ProductAuthenticationQuestionID><Question>Name your credit card accounts</Question><QuestionPointValue>0</QuestionPointValue><RequiredNoOfAnswers>0</RequiredNoOfAnswers><Answers><AnswerDocument><AnswerID>1195475</AnswerID><Answer>Absa Credit Card</Answer><IsEnteredAnswerYN>true</IsEnteredAnswerYN></AnswerDocument></Answers></QuestionDocument></Questions></AuthenticationDocument><CurrentQuestionNum>0</CurrentQuestionNum></CurrentObjectState></AuthenticationProcess>";                    
                    //XmlSerializer serializer = new XmlSerializer(typeof(XDSconnectSample.xdsconnect.AuthenticationProcess));

//                    moMyAuthenticationManager = (XDSconnectSample.xdsconnect.AuthenticationProcess)serializer.Deserialize(new XmlTextReader(new StringReader(xml)));

                    //moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument = odco;
                  moMyAuthenticationManager = aAuthConnectWS.ConnectFraudProcess(strToken, AuthWS.AuthenticationProcessAction.Authenticate, moMyAuthenticationManager,string.Empty.ToString());                 
                    
                  
                   
                }
                if (string.IsNullOrEmpty(moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.ErrorMessage))
                {
                    if (moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.PersonalQuestions != null)
                    {
                        if (moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.PersonalQuestions.Length > 0)
                        {
                            dgPersonalQuestions.DataSource = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.PersonalQuestions;

                            btnSavePQA.Enabled = true;
                        }
                        else
                        {
                            btnSavePQA.Enabled = false;
                            dgPersonalQuestions.DataSource = null;
                            txtPersonalQuestionsresult.Text = string.Empty;
                        }
                    }
                    else
                    {
                        btnSavePQA.Enabled = false;
                        dgPersonalQuestions.DataSource = null;
                        txtPersonalQuestionsresult.Text = string.Empty;
                    }
                    pnlAuthQuestions.Visible = false;
                    pnlfinalresult.Visible = true;
                    this.pnlfinalresult.Location = new System.Drawing.Point(7, 159);
                    txtminAuthPercM.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.RequiredAuthenticatedPerc.ToString();
                    txtAuthpercM.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.AuthenticatedPerc.ToString();
                    btnAuthHisM.Enabled = false;
                    btnVoidM.Enabled = false;
                    btnAuthenticateM.Enabled = false;
                    btnReferToFraud.Enabled = false;
                }
                else
                {
                    txtErrorMessageM.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.ErrorMessage;
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
            
        }

        private void ClearInfo()
        {

            txtAccountAgeDetails.Text = "";
            lstQuestions.Items.Clear();
            cmbAnswersM.Items.Clear();
            txtRefNoM.Text = "";
            txtAuthTypeM.Text = "";
            txtAuthStatusM.Text = "";
            txtAuthMsgM.Text = "";
            txtAuthpercM.Text = "";
            txtFNameMH.Text = "";
            txtSurnameMH.Text = "";
            txtSNameMH.Text = "";
            txtDOBMH.Text = "";
            txtDeceasedDateMH.Text = "";
            txtDeceasedStatusMH.Text = "";
            txtCauseOfDeathMH.Text = "";
            txtIdNoMH.Text = "";
            txtFNameMP.Text = "";
            txtSNameMP.Text = "";
            txtSurNameMP.Text = "";
            txtIDNoMP.Text = "";
            txtPassportnoMP.Text = "";
            txtGenderMP.Text = "";
            txtDOBMP.Text = "";
            txtReasonDescM.Text = string.Empty;

            lstQuestions.Enabled = false;
            cmbAnswersM.Enabled = false;
            btnAuthenticateM.Enabled = false;
            btnVoidM.Enabled = false;
            btnReferToFraud.Enabled = false;
            btnAuthHisM.Enabled = false;

            pnlAuthQuestions.Visible = false;
            pnlfinalresult.Visible = false;
            this.pnlfinalresult.Location = new System.Drawing.Point(3, 666);
            pnlReasonsM.Visible = false;
            pnlAuthHisM.Visible = false;
        }

        private void cmbBranch_Click(object sender, EventArgs e)
        {

            if (cmbBranch.Items.Count == 0)
            {
 
                   //// aAuthConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                    if (!aAuthConnectWS.IsTicketValid(strToken))
                    {
                        strToken = aAuthConnectWS.Login(Username, Password);
                    }
 
                if (aAuthConnectWS.IsTicketValid(strToken))
                {
                    string XMLprofile;

                    XMLprofile = aAuthConnectWS.ConnectFraudGetProfile(strToken);
                    DataSet dsprofile = new DataSet();
                    System.IO.StringReader xmlSRprofile = new System.IO.StringReader(XMLprofile);
                    dsprofile.ReadXml(xmlSRprofile);

                    cmbBranch.DataSource = dsprofile.Tables["Branches"];
                    cmbBranch.DisplayMember = "BranchName";
                    cmbBranch.ValueMember = "BranchCode";


                    if (Convert.ToBoolean(dsprofile.Tables["SubscriberProfile"].Rows[0].Field<string>("AuthenticationSearchOnIDNoYN").ToString()) == true)
                    {
                        searchtype = AuthWS.AuthenticationSearchType.ConsumerIdentity;
                    }
                    else if (Convert.ToBoolean(dsprofile.Tables["SubscriberProfile"].Rows[0].Field<string>("AuthenticationSearchOnCellPhoneNoYN").ToString()) == true)
                    {
                        searchtype = AuthWS.AuthenticationSearchType.ConsumerTelephone;
                    }
                    else if (Convert.ToBoolean(dsprofile.Tables["SubscriberProfile"].Rows[0].Field<string>("AuthenticationSearchOnAccountNoYN").ToString()) == true)
                    {
                        searchtype = AuthWS.AuthenticationSearchType.ConsumerAccount;
                    }
                }
                else
                {
                    if (!(strToken == string.Empty))
                    {
                        MessageBox.Show(strToken);
                    }
                }
            }
        }

        private void cmbpurpose_Click(object sender, EventArgs e)
        {
            if (cmbpurpose.Items.Count == 0)
            {

                   // aAuthConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                    if (!aAuthConnectWS.IsTicketValid(strToken))
                    {
                        strToken = aAuthConnectWS.Login(Username, Password);
                    }

                if (aAuthConnectWS.IsTicketValid(strToken))
                {
                    string XMLprofile;
                    XMLprofile = aAuthConnectWS.ConnectFraudGetProfile(strToken);
                    DataSet dsprofile = new DataSet();
                    System.IO.StringReader xmlSRprofile = new System.IO.StringReader(XMLprofile);
                    dsprofile.ReadXml(xmlSRprofile);

                    cmbpurpose.DataSource = dsprofile.Tables["Purposes"];
                    cmbpurpose.DisplayMember = "Purpose";
                    cmbpurpose.ValueMember = "PurposeID";


                    if (Convert.ToBoolean(dsprofile.Tables["SubscriberProfile"].Rows[0].Field<string>("AuthenticationSearchOnIDNoYN").ToString()) == true)
                    {
                        searchtype = AuthWS.AuthenticationSearchType.ConsumerIdentity;
                    }
                    else if (Convert.ToBoolean(dsprofile.Tables["SubscriberProfile"].Rows[0].Field<string>("AuthenticationSearchOnCellPhoneNoYN").ToString()) == true)
                    {
                        searchtype = AuthWS.AuthenticationSearchType.ConsumerTelephone;
                    }
                    else if (Convert.ToBoolean(dsprofile.Tables["SubscriberProfile"].Rows[0].Field<string>("AuthenticationSearchOnAccountNoYN").ToString()) == true)
                    {
                        searchtype = AuthWS.AuthenticationSearchType.ConsumerAccount;
                    }
                }
                else
                {
                    if (!(strToken == string.Empty))
                    {
                        MessageBox.Show(strToken);
                    }
                }
            }
        
        }

        private void btnVoidM_Click(object sender, EventArgs e)
        {
            pnlReasonsM.Visible = true;
            pnlReasonsM.Location = new System.Drawing.Point(7, 159);
            pnlAuthQuestions.Visible = false;
            Authenticationprocessaction = "VOID";
            cmbReasonsM.DataSource = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.VoidReasons;
            cmbReasonsM.DisplayMember = "VoidReason";
            btnAuthenticateM.Enabled = false;
            btnVoidM.Enabled = false;
            btnReferToFraud.Enabled = false;
            btnAuthHisM.Enabled = false;
            txtReasonDescM.Enabled = true;
            btnProcessM.Enabled = true;
            btnbacktoAuthQM.Enabled = true;
        }

        private void btnReferToFraud_Click(object sender, EventArgs e)
        {
            pnlReasonsM.Visible = true;
            pnlReasonsM.Location = new System.Drawing.Point(7, 159);
            pnlAuthQuestions.Visible = false;
            Authenticationprocessaction = "REFERTOFRAUD";
            cmbReasonsM.DataSource = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.FraudReasons;
            cmbReasonsM.DisplayMember = "FraudReason";
            btnAuthenticateM.Enabled = false;
            btnVoidM.Enabled = false;
            btnReferToFraud.Enabled = false;
            btnAuthHisM.Enabled = false;
            txtReasonDescM.Enabled = true;
            btnProcessM.Enabled = true;
            btnbacktoAuthQM.Enabled = true;
        }

        private void bthAuthHisM_Click(object sender, EventArgs e)
        {
            pnlAuthHisM.Visible = true;
            this.pnlAuthHisM.Location = new System.Drawing.Point(7, 159);
            pnlAuthQuestions.Visible = false;
            btnAuthenticateM.Enabled = false;
            btnVoidM.Enabled = false;
            btnReferToFraud.Enabled = false;
            btnAuthHisM.Enabled = false;
        }
        private void bthBack_Click(object sender, EventArgs e)
        {
            pnlAuthHisM.Visible = false;
            pnlAuthQuestions.Visible = true;
            btnAuthenticateM.Enabled = true;
            btnVoidM.Enabled = true;
            btnReferToFraud.Enabled = true;
            btnAuthHisM.Enabled = true;
        }
        private void btnbacktoAuthQM_Click(object sender, EventArgs e)
        {
            pnlReasonsM.Visible = false;
            pnlAuthQuestions.Visible = true;
            btnAuthenticateM.Enabled = true;
            btnVoidM.Enabled = true;
            btnReferToFraud.Enabled = true;
            btnAuthHisM.Enabled = true;
        }

        private void btnProcessM_Click(object sender, EventArgs e)
        {
            btnbacktoAuthQM.Enabled = false;

            if (Authenticationprocessaction.ToLower() == "void")
            {
                try
                {
                    string strXMLResult = string.Empty;

                    if (!aAuthConnectWS.IsTicketValid(strToken))
                    {
                        strToken = aAuthConnectWS.Login(Username, Password);
                        if (aAuthConnectWS.IsTicketValid(strToken))
                        {
                            if (moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.VoidReasons != null)
                            {
                                foreach (AuthWS.VoidReasonsDocument oVrDocument in moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.VoidReasons)
                                {
                                    if (cmbReasonsM.Text == oVrDocument.VoidReason)
                                    {
                                        oVrDocument.IsEnteredReasonYN = true;
                                        break;
                                    }
                                }
                            }

                            moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.VoidorFraudReason = txtReasonDescM.Text;

                            moMyAuthenticationManager = aAuthConnectWS.ConnectFraudProcess(strToken, AuthWS.AuthenticationProcessAction.Void, moMyAuthenticationManager, string.Empty.ToString());
                        }
                        else if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }

                    }
                    else
                    {
                        if (moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.VoidReasons != null)
                        {
                            foreach (AuthWS.VoidReasonsDocument oVrDocument in moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.VoidReasons)
                            {
                                if (cmbReasonsM.Text == oVrDocument.VoidReason)
                                {
                                    oVrDocument.IsEnteredReasonYN = true;
                                    break;
                                }
                            }
                        }

                        moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.VoidorFraudReason = txtReasonDescM.Text;

                        moMyAuthenticationManager = aAuthConnectWS.ConnectFraudProcess(strToken, AuthWS.AuthenticationProcessAction.Void, moMyAuthenticationManager, string.Empty.ToString());
                    }

                    txtAuthTypeM.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.AuthenticationTypeInd;
                    txtAuthStatusM.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.AuthenticationStatusInd;
                    txtErrorMessageM.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.ErrorMessage;

                }
                catch (Exception oException)
                {
                    MessageBox.Show(oException.Message);
                }
            }
            else if (Authenticationprocessaction.ToLower() == "refertofraud")
            {
                try
                {
                    string strXMLResult = string.Empty;

                    if (!aAuthConnectWS.IsTicketValid(strToken))
                    {
                        strToken = aAuthConnectWS.Login(Username, Password);
                        if (aAuthConnectWS.IsTicketValid(strToken))
                        {
                            if (moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.FraudReasons != null)
                            {
                                foreach (AuthWS.FraudReasonsDocument oFrDocument in moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.FraudReasons)
                                {
                                     if (cmbReasonsM.Text == oFrDocument.FraudReason)
                                        {
                                            oFrDocument.IsEnteredReasonYN = true;
                                            break;
                                        }
                                }
                            }
                            moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.VoidorFraudReason = txtReasonDescM.Text;

                            moMyAuthenticationManager = aAuthConnectWS.ConnectFraudProcess(strToken, AuthWS.AuthenticationProcessAction.ReferToFraud, moMyAuthenticationManager, string.Empty.ToString());
                        }
                        else if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }

                    }
                    else
                    {
                        if (moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.FraudReasons != null)
                        {
                            foreach (AuthWS.FraudReasonsDocument oFrDocument in moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.FraudReasons)
                            {
                                 if (cmbReasonsM.Text == oFrDocument.FraudReason)
                                    {
                                        oFrDocument.IsEnteredReasonYN = true;
                                        break;
                                    }
                            }
                        }

                        moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.VoidorFraudReason = txtReasonDescM.Text;

                        moMyAuthenticationManager = aAuthConnectWS.ConnectFraudProcess(strToken, AuthWS.AuthenticationProcessAction.ReferToFraud, moMyAuthenticationManager, string.Empty.ToString());
                    }

                    txtAuthTypeM.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.AuthenticationTypeInd;
                    txtAuthStatusM.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.AuthenticationStatusInd;
                    txtErrorMessageM.Text = moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.ErrorMessage;

                }
                catch (Exception oException)
                {
                    MessageBox.Show(oException.Message);
                }
            }
            pnlAuthQuestions.Visible = true;
            pnlReasonsM.Visible = false;
            
            if (!string.IsNullOrEmpty(txtErrorMessageM.Text))
            {
                btnAuthenticateM.Enabled = true;
                btnVoidM.Enabled = true;
                btnReferToFraud.Enabled = true;
                btnAuthHisM.Enabled = true;
            }
        }

        private void btnSavePQA_Click(object sender, EventArgs e)
        {
            object ds = dgPersonalQuestions.DataSource;
            IEnumerable<xdsconnect.PersonalQuestionsDocument> List = ds as IEnumerable<xdsconnect.PersonalQuestionsDocument>;

            foreach (xdsconnect.PersonalQuestionsDocument opQDocument in List)
            {
                foreach (AuthWS.PersonalQuestionsDocument oAuthDocument in moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.PersonalQuestions)
                {
                    if (opQDocument.PQuestionID == oAuthDocument.PQuestionID)
                    {
                        oAuthDocument.PAnswer = opQDocument.PAnswer;
                    }
                }
            }
           
            string strXMLResult = string.Empty;

            try
            {
                
                if (!aAuthConnectWS.IsTicketValid(strToken))
                {
                    strToken = aAuthConnectWS.Login(Username, Password);
                    if (aAuthConnectWS.IsTicketValid(strToken))
                    {

                        strXMLResult = aAuthConnectWS.ConnectFraudSavePersonalQuestions(strToken,  moMyAuthenticationManager, "");
                    }
                    else  if (!(strToken == string.Empty))
                    {
                        MessageBox.Show(strToken);
                    }

                }
                else
                {
                    strXMLResult = aAuthConnectWS.ConnectFraudSavePersonalQuestions(strToken, moMyAuthenticationManager, "");
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
            if (strXMLResult.ToLower().Contains("error"))
            {
                btnSavePQA.Enabled = true;
            }
            else
            {
                btnSavePQA.Enabled = false;
            }
            txtPersonalQuestionsresult.Text = strXMLResult;
        }

        private void btnGetBlockedConsumers_Click(object sender, EventArgs e)
        {
            string strXMLResult = string.Empty;
            try
            {

                if (!aAuthConnectWS.IsTicketValid(strToken))
                {
                    strToken = aAuthConnectWS.Login(Username, Password);
                    if (aAuthConnectWS.IsTicketValid(strToken))
                    {

                        strXMLResult = aAuthConnectWS.ConnectFraudGetBlockedConsumers(strToken);
                    }
                    else if (!(strToken == string.Empty))
                    {
                        MessageBox.Show(strToken);
                    }

                }
                else
                {
                    strXMLResult = aAuthConnectWS.ConnectFraudGetBlockedConsumers(strToken);
                }

                txtBConsumers.Text = strXMLResult;
                DataSet ds = new DataSet();
                System.IO.StringReader xmlSR = new System.IO.StringReader(strXMLResult);
                ds.ReadXml(xmlSR);
                dgBConsumers.DataSource = null;
                if (ds.Tables.Count > 0)
                {
                    dgBConsumers.DataSource = ds.Tables[0];
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }

        }

        private void btnUnblockConsumer_Click(object sender, EventArgs e)
        {
            string strXMLResult = string.Empty;
            int BlockID = 0;
            try
            {
                if (string.IsNullOrEmpty(txtBlockID.Text))
                {
                    throw new Exception("Please enter a valid value for Block ID");
                }
                else if (!int.TryParse(txtBlockID.Text, out BlockID))
                {
                    throw new Exception("Please enter a valid value for Block ID");
                }

                if (!aAuthConnectWS.IsTicketValid(strToken))
                {
                    strToken = aAuthConnectWS.Login(Username, Password);
                    if (aAuthConnectWS.IsTicketValid(strToken))
                    {

                        strXMLResult = aAuthConnectWS.ConnectFraudUnBlockConsumer(strToken, BlockID,string.Empty);
                    }
                    else if (!(strToken == string.Empty))
                    {
                        MessageBox.Show(strToken);
                    }

                }
                else
                {
                    strXMLResult = aAuthConnectWS.ConnectFraudUnBlockConsumer(strToken, BlockID,string.Empty);
                }

                txtUnblockResult.Text = strXMLResult;
               
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }

        }


        #endregion Authentication

        #region listings and Delistings

        #region Listings

        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Node.GetNodeCount(true) == 0)
            {

                if (int.Parse(e.Node.Name) == 31)
                {
                    pnlCTrace.Visible = true;
                    pnlListConsumer.Visible = false;
                    this.pnlCTrace.Location = new System.Drawing.Point(3, 4);
                }
                else if (int.Parse(e.Node.Name) == 32)
                {
                    pnlBEnq.Visible = true;
                    pnlListComm.Visible = false;
                    this.pnlBEnq.Location = new System.Drawing.Point(3, 4);
                }
            }
        }


        #region Consumer listings 

        private void cmbAccTypeLCon_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbAccTypeLCon.Items.Count == 0)
                {
                    aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                    strToken = aXDSConnectWS.Login(Username, Password);
                    string axml = aXDSConnectWS.ConnectGetDefaultAlertAccountTypes(strToken, Listingtype.Consumer);

                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(axml);
                        ds.ReadXml(xmlSR);
                        cmbAccTypeLCon.DataSource = ds.Tables[0];
                        cmbAccTypeLCon.DisplayMember = ds.Tables[0].Columns[0].ToString();
                        cmbAccTypeLCon.ValueMember = ds.Tables[0].Columns[2].ToString();
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        private void cmbSCodeLCon_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbSCodeLCon.Items.Count == 0)
                {
                    aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                    strToken = aXDSConnectWS.Login(Username, Password);
                    string axml = aXDSConnectWS.ConnectGetDefaultAlertStatusCode(strToken, Listingtype.Consumer);

                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(axml);
                        ds.ReadXml(xmlSR);
                        cmbSCodeLCon.DataSource = ds.Tables[0];
                        cmbSCodeLCon.DisplayMember = ds.Tables[0].Columns[0].ToString();
                        cmbSCodeLCon.ValueMember = ds.Tables[0].Columns[2].ToString();
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }    

        private void btnSubmittListCon_Click(object sender, EventArgs e)
        {
            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtXMLResultListCon.Text = aXDSConnectWS.ConnectConsumerMatch(strToken, null, 53, txtIDNoLisCon.Text, txtPPNoLisCon.Text, txtFNameLisCon.Text, txtsnameListCon.Text, dtpBDateLicon.Value.ToShortDateString(), txtExtRefListCon.Text, txtVoucherListCon.Text);

                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMLResultListCon.Text);
                        ds.ReadXml(xmlSR);
                        grdXMLResultListCon.DataSource = ds.Tables[0];
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtXMLResultListCon.Text = aXDSConnectWS.ConnectConsumerMatch(strToken, null, 53, txtIDNoLisCon.Text, txtPPNoLisCon.Text, txtFNameLisCon.Text, txtsnameListCon.Text, dtpBDateLicon.Value.ToShortDateString(), txtExtRefListCon.Text, txtVoucherListCon.Text);

                    DataSet ds = new DataSet();
                    System.IO.StringReader xmlSR = new System.IO.StringReader(txtXMLResultListCon.Text);
                    ds.ReadXml(xmlSR);
                    grdXMLResultListCon.DataSource = ds.Tables[0];
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        private void btnClearListCon_Click(object sender, EventArgs e)
        {
            txtIDNoLisCon.Text = string.Empty;
            txtPPNoLisCon.Text = string.Empty;
            txtFNameLisCon.Text = string.Empty;
            txtsnameListCon.Text = string.Empty;
            dtpBDateLicon.Text = string.Empty;
            txtExtRefListCon.Text = string.Empty;
        }

        private void btnListCon_Click(object sender, EventArgs e)
        {
            try
            {
                if ((!(string.IsNullOrEmpty(txtEnqIDListCon.Text)) && (!string.IsNullOrEmpty(txtEnqRIDListCon.Text))))
                {
                    ClearConsumerListing();
                    pnlCTrace.Visible = false;
                    pnlListConsumer.Visible = true;
                    this.pnlListConsumer.Location = new System.Drawing.Point(3, 4);

                    DataTable dt = (DataTable)(grdXMLResultListCon.DataSource);
                    if (dt != null)
                    {
                        for (int i=0; i< dt.Rows.Count ; i++)
                        {
                            if (dt.Rows[i]["EnquiryID"].ToString() == txtEnqIDListCon.Text && dt.Rows[i]["EnquiryResultID"].ToString() == txtEnqRIDListCon.Text)
                            {
                                if (dt.Columns.Contains("IDNo"))
                                    txtIDLCon.Text = dt.Rows[i]["IDNo"].ToString();
                                if (dt.Columns.Contains("PassportNo"))
                                    txtPPLCon.Text = dt.Rows[i]["PassportNo"].ToString();
                                if (dt.Columns.Contains("FirstName"))
                                    txtFNameLCon.Text = dt.Rows[i]["FirstName"].ToString();
                                if (dt.Columns.Contains("Surname"))
                                    txtSurNameLCon.Text = dt.Rows[i]["Surname"].ToString();
                                if (dt.Columns.Contains("BirthDate"))
                                    txtBDateLCon.Text = dt.Rows[i]["BirthDate"].ToString();
                            }
                        }
                    }
                }
                else
                {
                    throw new Exception("Please enter valid Enqiry and Enquiry Result ID");
                }
            }
            catch (Exception a)
            {
                MessageBox.Show(a.Message);
            }
        }

        private void chkSMSLCon_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSMSLCon.Checked)
            {
                lblMobileLCon.Enabled = true;
                txtMobileLCon.Enabled = true;
            }
            else
            {
                lblMobileLCon.Enabled = false;
                txtMobileLCon.Enabled = false;
            }
        }

        private void chkEmailLCon_CheckedChanged(object sender, EventArgs e)
        {
            if (chkEmailLCon.Checked)
            {
                lblEmailLCon.Enabled = true;
                txtEmailLCon.Enabled = true;
            }
            else
            {
                lblEmailLCon.Enabled = false;
                txtEmailLCon.Enabled = false;
            }
        }

        private void rdbClientResponsibleLCon_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbClientResponsibleLCon.Checked == true)
            {
                chkEmailLCon.Checked = false;
                txtEmailLCon.Text = string.Empty;
                chkSMSLCon.Checked = false;                
                txtMobileLCon.Text = string.Empty;

                chkEmailLCon.Enabled = false;
                chkSMSLCon.Enabled = false;
                
            }
            else if (rdbClientResponsibleLCon.Checked == false)
            {
                chkEmailLCon.Enabled = true;
                chkSMSLCon.Enabled = true;                
            }
        }

        private void rdbXDSNotifyLCon_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbXDSNotifyLCon.Checked == true)
            {
                chkEmailLCon.Enabled = true;
                chkSMSLCon.Enabled = true;                
            }
            else if (rdbXDSNotifyLCon.Checked == false)
            {
                chkEmailLCon.Checked = false;
                txtEmailLCon.Text = string.Empty;
                chkSMSLCon.Checked = false;
                txtMobileLCon.Text = string.Empty;

                chkEmailLCon.Enabled = false;
                chkSMSLCon.Enabled = false;
            }
        }

        private void btnSubmitLCon_Click(object sender, EventArgs e)
        {
            try
            {
                
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);

                    
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        string EnquiryXML = aXDSConnectWS.AdminEnquiryHistory(strToken, int.Parse(txtEnqIDListCon.Text), 53, true);
                        System.IO.StringReader xmlSR = new System.IO.StringReader(EnquiryXML);
                        DataSet dsEnquiry = new DataSet("Enquiry");

                        dsEnquiry.ReadXml(xmlSR);

                        string Subscribername = dsEnquiry.Tables[0].Rows[0].Field<string>("Subscribername").ToString();

                        txtResponseLCon.Text = aXDSConnectWS.ConnectLoadConsumerDefaultAlert(strToken, int.Parse(txtEnqIDListCon.Text), int.Parse(txtEnqRIDListCon.Text),Subscribername ,txtAccNoLCon.Text, txtSubAccNoLCon.Text, double.Parse(txtAmountLCon.Text), cmbSCodeLCon.SelectedValue.ToString(), cmbAccTypeLCon.SelectedValue.ToString(), txtAdd1LCon.Text, txtAdd2LCon.Text, txtAdd3LCon.Text, txtAdd4LCon.Text, txtPostalCodeLCon.Text, txtWPhoneLCon.Text, txtHPhoneLCon.Text, txtMobileLCon.Text, txtEmailLCon.Text, dtpEffDLCon.Text.ToString(), txtCommentLCon.Text, chkSMSLCon.Checked, chkEmailLCon.Checked,rdbClientResponsibleLCon.Checked, txtContactLCon.Text, (cmbListTypeLCon.SelectedItem == "Payment Notification" ? (chkDefaultMoveLCon.Checked?AlertType.PDefaultMovein20D: AlertType.P) : AlertType.D), chkConsumerNotifiedLCon.Checked);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    
                        string EnquiryXML = aXDSConnectWS.AdminEnquiryHistory(strToken, int.Parse(txtEnqIDListCon.Text), 53, true);
                        System.IO.StringReader xmlSR = new System.IO.StringReader(EnquiryXML);
                        DataSet dsEnquiry = new DataSet("Enquiry");

                        dsEnquiry.ReadXml(xmlSR);

                        string Subscribername = dsEnquiry.Tables[0].Rows[0].Field<string>("Subscribername").ToString();
                        txtResponseLCon.Text = aXDSConnectWS.ConnectLoadConsumerDefaultAlert(strToken, int.Parse(txtEnqIDListCon.Text), int.Parse(txtEnqRIDListCon.Text), Subscribername, txtAccNoLCon.Text, txtSubAccNoLCon.Text, double.Parse(txtAmountLCon.Text), cmbSCodeLCon.SelectedValue.ToString(), cmbAccTypeLCon.SelectedValue.ToString(), txtAdd1LCon.Text, txtAdd2LCon.Text, txtAdd3LCon.Text, txtAdd4LCon.Text, txtPostalCodeLCon.Text, txtWPhoneLCon.Text, txtHPhoneLCon.Text, txtMobileLCon.Text, txtEmailLCon.Text, dtpEffDLCon.Text.ToString(), txtCommentLCon.Text, chkSMSLCon.Checked, chkEmailLCon.Checked, rdbClientResponsibleLCon.Checked, txtContactLCon.Text, (cmbListTypeLCon.SelectedItem == "Payment Notification" ? (chkDefaultMoveLCon.Checked ? AlertType.PDefaultMovein20D : AlertType.P) : AlertType.D), chkConsumerNotifiedLCon.Checked);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        private void btnClearLCon_Click(object sender, EventArgs e)
        {
            ClearConsumerListing();
        }

        private void ClearConsumerListing()
        {
            txtIDLCon.Text = string.Empty;
            txtFNameLCon.Text = string.Empty;
            txtPPLCon.Text = string.Empty;
            txtSurNameLCon.Text = string.Empty;
            txtSNameLCon.Text = string.Empty;
            txtBDateLCon.Text = string.Empty;
            txtAccNoLCon.Text = string.Empty;
            txtSubAccNoLCon.Text = string.Empty;
            txtAmountLCon.Text = string.Empty;
            cmbSCodeLCon.SelectedText = string.Empty;
            cmbAccTypeAccv.SelectedText = string.Empty;
            txtAdd1LCon.Text = string.Empty;
            txtAdd2LCon.Text = string.Empty;
            txtAdd3LCon.Text = string.Empty;
            txtAdd4LCon.Text = string.Empty;
            txtPostalCodeLCon.Text = string.Empty;
            txtHPhoneLCon.Text = string.Empty;
            txtWPhoneLCon.Text = string.Empty;
            cmbGenderLCon.SelectedText = string.Empty;
            chkEmailLCon.Checked = false;
            chkSMSLCon.Checked = false;
            txtCommentLCon.Text = string.Empty;
            txtContactLCon.Text = string.Empty;
            txtMobileLCon.Text = string.Empty;
            txtEmailLCon.Text = string.Empty;
            cmbListTypeLCon.Text = string.Empty;
            chkConsumerNotifiedLCon.Checked = false;
        }

        #endregion Consumer listings

       
        #region Commercial listings
        private void btnSubmitListComm_Click(object sender, EventArgs e)
        {
            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtXmlResultListComm.Text = aXDSConnectWS.ConnectBusinessMatchTracePlus(strToken, txtReg1ListComm.Text, txtReg2ListComm.Text, txtReg3ListComm.Text, txtBusNameListComm.Text, txtVatListComm.Text, txtSolepropIDListComm.Text, txtExtRefListComm.Text, txtVoucherListComm.Text,54);

                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(txtXmlResultListComm.Text);
                        ds.ReadXml(xmlSR);
                        grdXMlResultListComm.DataSource = ds.Tables[0];
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtXmlResultListComm.Text = aXDSConnectWS.ConnectBusinessMatchTracePlus(strToken, txtReg1ListComm.Text, txtReg2ListComm.Text, txtReg3ListComm.Text, txtBusNameListComm.Text, txtVatListComm.Text, txtSolepropIDListComm.Text, txtExtRefListComm.Text, txtVoucherListComm.Text,54);

                    DataSet ds = new DataSet();
                    System.IO.StringReader xmlSR = new System.IO.StringReader(txtXmlResultListComm.Text);
                    ds.ReadXml(xmlSR);
                    grdXMlResultListComm.DataSource = ds.Tables[0];
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        private void btnClearListComm_Click(object sender, EventArgs e)
        {
            txtReg1ListComm.Text = string.Empty;
            txtReg2ListComm.Text = string.Empty;
            txtReg3ListComm.Text = string.Empty;
            txtBusNameListComm.Text = string.Empty;
            txtVatListComm.Text = string.Empty;
            txtExtRefListComm.Text = string.Empty;
            txtVoucherListComm.Text = string.Empty;
        }

        private void btnlistComm_Click(object sender, EventArgs e)
        {
            try
            {
                if ((!(string.IsNullOrEmpty(txtEnqIDListComm.Text)) && (!string.IsNullOrEmpty(txtEnqRIDListComm.Text))))
                {
                    ClearCommercialListing();
                    pnlBEnq.Visible = false;
                    pnlListComm.Visible = true;
                    this.pnlListComm.Location = new System.Drawing.Point(3, 4);

                    DataTable dt = (DataTable)(grdXMlResultListComm.DataSource);
                    if (dt != null)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            if (dt.Rows[i]["EnquiryID"].ToString() == txtEnqIDListComm.Text && dt.Rows[i]["EnquiryResultID"].ToString() == txtEnqRIDListComm.Text)
                            {
                                if (dt.Columns.Contains("RegistrationNo"))
                                {
                                    string RegNo = dt.Rows[i]["RegistrationNo"].ToString();
                                    txtReg1LCom.Text = RegNo.Substring(0, 4);
                                    txtReg2LCom.Text = RegNo.Substring(5, 6);
                                    txtReg3LCom.Text = RegNo.Substring(12, 2);
                                }
                                if (dt.Columns.Contains("Businessname"))
                                    txtBusNameLCom.Text = dt.Rows[i]["Businessname"].ToString();
                            }
                        }
                    }
                }
                else
                {
                    throw new Exception("Please enter valid Enqiry and Enquiry Result ID");
                }
            }
            catch (Exception a)
            {
                MessageBox.Show(a.Message);
            }
        }

        private void cmbSCodeLCom_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbSCodeLCom.Items.Count == 0)
                {
                    aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                    strToken = aXDSConnectWS.Login(Username, Password);
                    string axml = aXDSConnectWS.ConnectGetDefaultAlertStatusCode(strToken, Listingtype.Commercial);

                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(axml);
                        ds.ReadXml(xmlSR);
                        cmbSCodeLCom.DataSource = ds.Tables[0];
                        cmbSCodeLCom.DisplayMember = ds.Tables[0].Columns[0].ToString();
                        cmbSCodeLCom.ValueMember = ds.Tables[0].Columns[2].ToString();
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        private void cmbAccTypeLCom_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbAccTypeLCom.Items.Count == 0)
                {
                    aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                    strToken = aXDSConnectWS.Login(Username, Password);
                    string axml = aXDSConnectWS.ConnectGetDefaultAlertAccountTypes(strToken, Listingtype.Commercial);

                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(axml);
                        ds.ReadXml(xmlSR);
                        cmbAccTypeLCom.DataSource = ds.Tables[0];
                        cmbAccTypeLCom.DisplayMember = ds.Tables[0].Columns[0].ToString();
                        cmbAccTypeLCom.ValueMember = ds.Tables[0].Columns[2].ToString();
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        private void ClearCommercialListing()
        {
            txtReg1LCom.Text = string.Empty;
            txtReg2LCom.Text = string.Empty;
            txtReg3LCom.Text = string.Empty;
            txtVatLCom.Text = string.Empty;
            txtBusNameLCom.Text = string.Empty;
            txtAccNoLCom.Text = string.Empty;
            txtSubAccNoLCom.Text = string.Empty;
            txtAmountLCom.Text = string.Empty;
            txtIdLCom.Text = string.Empty;
            txtPPNoLCom.Text = string.Empty;
            txtFNameLCom.Text = string.Empty;
            txtSurNameLCom.Text = string.Empty;
            txtSNameLCom.Text = string.Empty;
            txtThirdNameLCom.Text = string.Empty;
            dtpBDateLCom.Text = string.Empty;
            txtAdd1LCom.Text = string.Empty;
            txtAdd2LCom.Text = string.Empty;
            txtAdd3LCom.Text = string.Empty;
            txtAdd4LCom.Text = string.Empty;
            txtPostalCodeLCom.Text = string.Empty;
            txtHPhoneLCom.Text = string.Empty;
            txtWPhoneLCom.Text = string.Empty;
            chkEmailLCom.Checked = false;
            chkSMSLCom.Checked = false;
            txtEmailLCom.Text = string.Empty;
            txtMobileLCom.Text = string.Empty;
            txtCommentLCom.Text = string.Empty;
            dtpEffDateLCom.Text = string.Empty;
            txtContactLCom.Text = string.Empty;
        }

        private void chkSMSLCom_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSMSLCom.Checked)
            {
                lblMobileLCom.Enabled = true;
                txtMobileLCom.Enabled = true;
            }
            else
            {
                lblMobileLCom.Enabled = false;
                txtMobileLCom.Enabled = false;
            }

        }

        private void chkEmailLCom_CheckedChanged(object sender, EventArgs e)
        {
            if (chkEmailLCom.Checked)
            {
                lblEmailLCom.Enabled = true;
                txtEmailLCom.Enabled = true;
            }
            else
            {
                lblEmailLCom.Enabled = false;
                txtEmailLCom.Enabled = false;
            }

        }

        private void rdbClientNotifyLCom_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbClientResponsibleLCom.Checked == true)
            {
                chkEmailLCom.Checked = false;
                txtEmailLCom.Text = string.Empty;
                chkSMSLCom.Checked = false;
                txtMobileLCom.Text = string.Empty;

                chkEmailLCom.Enabled = false;
                chkSMSLCom.Enabled = false;                
            }
            else if (rdbClientResponsibleLCom.Checked == false)
            {
                chkEmailLCom.Enabled = true;
                chkSMSLCom.Enabled = true;                
            }
        }


        private void rdbXDSNotifyLCom_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbXDSNotifyLCom.Checked == true)
            {
                chkEmailLCom.Enabled = true;
                chkSMSLCom.Enabled = true; 
            }
            else if (rdbXDSNotifyLCom.Checked == false)
            {
                chkEmailLCom.Checked = false;
                txtEmailLCom.Text = string.Empty;
                chkSMSLCom.Checked = false;
                txtMobileLCom.Text = string.Empty;

                chkEmailLCom.Enabled = false;
                chkSMSLCom.Enabled = false; 
            }

        }

        private void btnClearLCom_Click(object sender, EventArgs e)
        {
            ClearCommercialListing();
        }

        private void btnSubmitLCom_Click(object sender, EventArgs e)
        {
            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        string EnquiryXML = aXDSConnectWS.AdminEnquiryHistory(strToken, int.Parse(txtEnqIDListComm.Text), 53, true);
                        System.IO.StringReader xmlSR = new System.IO.StringReader(EnquiryXML);
                        DataSet dsEnquiry = new DataSet("Enquiry");

                        dsEnquiry.ReadXml(xmlSR);

                        string Subscribername = dsEnquiry.Tables[0].Rows[0].Field<string>("Subscribername").ToString();

                        txtResponseLCom.Text = aXDSConnectWS.ConnectLoadCommercialDefaultAlert(strToken, int.Parse(txtEnqIDListComm.Text), int.Parse(txtEnqRIDListComm.Text),Subscribername, txtVatLCom.Text, txtIdLCom.Text, txtPPNoLCom.Text, txtSurNameLCom.Text, txtFNameLCom.Text, txtSNameLCom.Text, txtThirdNameLCom.Text, cmbGenderLCom.SelectedText.ToString(), dtpBDateLCom.Text.ToString(), txtAccNoLCom.Text, txtSubAccNoLCom.Text, double.Parse(txtAmountLCom.Text), cmbSCodeLCom.SelectedValue.ToString(), cmbAccTypeLCom.SelectedValue.ToString(), txtAdd1LCom.Text, txtAdd2LCom.Text, txtAdd3LCom.Text, txtAdd4LCom.Text, txtPostalCodeLCom.Text, txtHPhoneLCom.Text, txtWPhoneLCom.Text, txtMobileLCom.Text, txtEmailLCom.Text, txtCommentLCom.Text, dtpEffDateLCom.Text.ToString(), chkSMSLCom.Checked, chkEmailLCom.Checked, rdbClientResponsibleLCom.Checked, txtContactLCom.Text, (cmbListingtypeLcom.SelectedItem.ToString() == "Payment Notification" ? (chkDefaultMoveLcom.Checked?AlertType.PDefaultMovein20D:AlertType.P) : AlertType.D), chkConsumerNotifiedLcom.Checked);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    string EnquiryXML = aXDSConnectWS.AdminEnquiryHistory(strToken, int.Parse(txtEnqIDListComm.Text), 53, true);
                    System.IO.StringReader xmlSR = new System.IO.StringReader(EnquiryXML);
                    DataSet dsEnquiry = new DataSet("Enquiry");

                    dsEnquiry.ReadXml(xmlSR);

                    string Subscribername = dsEnquiry.Tables[0].Rows[0].Field<string>("Subscribername").ToString();

                    txtResponseLCom.Text = aXDSConnectWS.ConnectLoadCommercialDefaultAlert(strToken, int.Parse(txtEnqIDListComm.Text), int.Parse(txtEnqRIDListComm.Text), Subscribername, txtVatLCom.Text, txtIdLCom.Text, txtPPNoLCom.Text, txtSurNameLCom.Text, txtFNameLCom.Text, txtSNameLCom.Text, txtThirdNameLCom.Text, cmbGenderLCom.SelectedText.ToString(), dtpBDateLCom.Text.ToString(), txtAccNoLCom.Text, txtSubAccNoLCom.Text, double.Parse(txtAmountLCom.Text), cmbSCodeLCom.SelectedValue.ToString(), cmbAccTypeLCom.SelectedValue.ToString(), txtAdd1LCom.Text, txtAdd2LCom.Text, txtAdd3LCom.Text, txtAdd4LCom.Text, txtPostalCodeLCom.Text, txtHPhoneLCom.Text, txtWPhoneLCom.Text, txtMobileLCom.Text, txtEmailLCom.Text, txtCommentLCom.Text, dtpEffDateLCom.Text.ToString(), chkSMSLCom.Checked, chkEmailLCom.Checked, rdbClientResponsibleLCom.Checked, txtContactLCom.Text, (cmbListingtypeLcom.SelectedItem.ToString() == "Payment Notification" ? (chkDefaultMoveLcom.Checked ? AlertType.PDefaultMovein20D : AlertType.P) : AlertType.D), chkConsumerNotifiedLcom.Checked);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }


        #endregion Commercial Listings

        #endregion Listings

        #region Delistings

        private void cmbEntities_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbEntities.SelectedItem.ToString().ToLower() == "consumer")
            {
                pnlConsumerD.Visible = true;
                pnlCommercialD.Visible = false;
                this.pnlConsumerD.Location = new System.Drawing.Point(16, 59);
            }
            else if (cmbEntities.SelectedItem.ToString().ToLower() == "commercial")
            {
                pnlConsumerD.Visible = false;
                pnlCommercialD.Visible = true;
                this.pnlCommercialD.Location = new System.Drawing.Point(16,59);
            }

        }

        #region Consumer

        private void btnGetHistoryCon_Click(object sender, EventArgs e)
        {
            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtDHistoryCon.Text = aXDSConnectWS.ConnectGetConsumerDefaultAlertHistory(strToken);

                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(txtDHistoryCon.Text);
                        ds.ReadXml(xmlSR);
                        grdDHistoryCon.DataSource = ds.Tables[0];
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtDHistoryCon.Text = aXDSConnectWS.ConnectGetConsumerDefaultAlertHistory(strToken);

                    DataSet ds = new DataSet();
                    System.IO.StringReader xmlSR = new System.IO.StringReader(txtDHistoryCon.Text);
                    ds.ReadXml(xmlSR);
                    grdDHistoryCon.DataSource = ds.Tables[0];
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        private void btnDelistcon_Click(object sender, EventArgs e)
        {
            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtResponseDCon.Text = aXDSConnectWS.ConnectRemoveConsumerDefaultAlert(strToken, int.Parse(txtAlertID.Text),txtMobileCon.Text,txtEmailCon.Text,chkSMSCon.Checked,chkEmailCon.Checked,rdbClientResponsibleCon.Checked, txtContactDetialsCon.Text);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtResponseDCon.Text = aXDSConnectWS.ConnectRemoveConsumerDefaultAlert(strToken, int.Parse(txtAlertID.Text), txtMobileCon.Text, txtEmailCon.Text, chkSMSCon.Checked, chkEmailCon.Checked,rdbClientResponsibleCon.Checked, txtContactDetialsCon.Text);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        private void btnMoveCon_Click(object sender, EventArgs e)
        {
            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtResponseDCon.Text = aXDSConnectWS.ConnectMoveConsumerDefaultAlert(strToken, int.Parse(txtAlertID.Text), txtMobileCon.Text, txtEmailCon.Text, chkSMSCon.Checked, chkEmailCon.Checked,rdbClientResponsibleCon.Checked, txtContactDetialsCon.Text);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtResponseDCon.Text = aXDSConnectWS.ConnectMoveConsumerDefaultAlert(strToken, int.Parse(txtAlertID.Text), txtMobileCon.Text, txtEmailCon.Text, chkSMSCon.Checked, chkEmailCon.Checked,rdbClientResponsibleCon.Checked, txtContactDetialsCon.Text);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        private void btnClearDCon_Click(object sender, EventArgs e)
        {
            txtAlertID.Text = string.Empty;
            grdDHistoryCon.DataSource = null;
            txtDHistoryCon.Text = string.Empty;
            txtResponseDCon.Text = string.Empty;
            txtContactDetialsCon.Text = string.Empty;
            txtMobileCon.Text = string.Empty;
            txtEmailCon.Text = string.Empty;
            chkEmailCon.Checked = false;
            chkSMSCon.Checked = false;
        }

        private void chkSMSCon_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSMSCon.Checked)
            {
                lblMobileCOn.Enabled = true;
                txtMobileCon.Enabled = true;
            }
            else
            {
                lblMobileCOn.Enabled = false;
                txtMobileCon.Enabled = false;
            }
    
        }
        private void chkEmailCon_CheckedChanged(object sender, EventArgs e)
        {
            if (chkEmailCon.Checked)
            {
                lblEmailCon.Enabled = true;
                txtEmailCon.Enabled = true;
            }
            else
            {
                lblEmailCon.Enabled = false;
                txtEmailCon.Enabled = false;
            }
        }

        private void rdbClientResponsibleCon_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbClientResponsibleCon.Checked == true)
            {
                chkEmailCon.Checked = false;
                txtEmailCon.Text = string.Empty;
                chkSMSCon.Checked = false;
                txtMobileCon.Text = string.Empty;


                chkEmailCon.Enabled = false;
                chkSMSCon.Enabled = false;
            }
            else if (rdbClientResponsibleCon.Checked == false)
            {
                chkEmailCon.Enabled = true;
                chkSMSCon.Enabled = true;                
            }

        }

        private void cmbListTypeLCon_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            if (cmbListTypeLCon.SelectedItem == "Payment Notification")
            {
                chkDefaultMoveLCon.Enabled = true;
                chkDefaultMoveLCon.Checked = false;
            }
            else
            {
                chkDefaultMoveLCon.Enabled = false;
                chkDefaultMoveLCon.Enabled = false;
            }
        }



        private void rdbXDSNotifyCon_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbXDSNotifyCon.Checked == true)
            {
                chkEmailCon.Enabled = true;
                chkSMSCon.Enabled = true; 
            }
            else if (rdbXDSNotifyCon.Checked == false)
            {
                chkEmailCon.Checked = false;
                txtEmailCon.Text = string.Empty;
                chkSMSCon.Checked = false;
                txtMobileCon.Text = string.Empty;


                chkEmailCon.Enabled = false;
                chkSMSCon.Enabled = false;
            }
        }
    
        
        #endregion consumer

        #region Commercial

        private void btnGetDHistoryCom_Click(object sender, EventArgs e)
        {
            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtDHistoryCom.Text = aXDSConnectWS.ConnectGetCommercialDefaultAlertHistory(strToken);

                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(txtDHistoryCom.Text);
                        ds.ReadXml(xmlSR);
                        grdDHistoryCom.DataSource = ds.Tables[0];
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtDHistoryCom.Text = aXDSConnectWS.ConnectGetCommercialDefaultAlertHistory(strToken);

                    DataSet ds = new DataSet();
                    System.IO.StringReader xmlSR = new System.IO.StringReader(txtDHistoryCom.Text);
                    ds.ReadXml(xmlSR);
                    grdDHistoryCom.DataSource = ds.Tables[0];
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        private void btnDelistcom_Click(object sender, EventArgs e)
        {
            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtResponseDCom.Text = aXDSConnectWS.ConnectRemoveCommercialDefaultAlert(strToken, int.Parse(txtAlertIDcom.Text),txtMobileCom.Text,txtEmailCom.Text,chkSMSCom.Checked,chkEmailCom.Checked, rdbClientResponsibleCom.Checked, txtContactDetailsCom.Text);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtResponseDCom.Text = aXDSConnectWS.ConnectRemoveCommercialDefaultAlert(strToken, int.Parse(txtAlertIDcom.Text), txtMobileCom.Text, txtEmailCom.Text, chkSMSCom.Checked, chkEmailCom.Checked, rdbClientResponsibleCom.Checked, txtContactDetailsCom.Text);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

      

        private void btnMoveCom_Click(object sender, EventArgs e)
        {
            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtResponseDCom.Text = aXDSConnectWS.ConnectMoveCommercialDefaultAlert(strToken, int.Parse(txtAlertIDcom.Text), txtMobileCom.Text, txtEmailCom.Text, chkSMSCom.Checked, chkEmailCom.Checked,rdbClientResponsibleCom.Checked, txtContactDetailsCom.Text);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtResponseDCom.Text = aXDSConnectWS.ConnectMoveCommercialDefaultAlert(strToken, int.Parse(txtAlertIDcom.Text), txtMobileCom.Text, txtEmailCom.Text, chkSMSCom.Checked, chkEmailCom.Checked, rdbClientResponsibleCom.Checked, txtContactDetailsCom.Text);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }


        private void btnClearDCom_Click(object sender, EventArgs e)
        {
            txtAlertIDcom.Text = string.Empty;
            grdDHistoryCom.DataSource = null;
            txtDHistoryCom.Text = string.Empty;
            txtResponseDCom.Text = string.Empty;
        }

        private void chkEmailCom_CheckedChanged(object sender, EventArgs e)
        {
            if (chkEmailCom.Checked)
            {
                lblEmailCom.Enabled = true;
                txtEmailCom.Enabled = true;
            }
            else
            {
                lblEmailCom.Enabled = false;
                txtEmailCom.Enabled = false;
            }

        }

        private void chkSMSCom_CheckedChanged(object sender, EventArgs e)
        {
           
            if (chkSMSCom.Checked)
            {
                lblMobileCom.Enabled = true;
                txtMobileCom.Enabled = true;
            }
            else
            {
                lblMobileCom.Enabled = false;
                txtMobileCom.Enabled = false;
            }
        }

        private void rdbClientResponsibleCom_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbClientResponsibleCom.Checked == true)
            {
                chkEmailCom.Checked = false;
                txtEmailCom.Text = string.Empty;
                chkSMSCom.Checked = false;
                txtMobileLCom.Text = string.Empty;

                chkEmailCom.Enabled = false;
                chkSMSCom.Enabled = false;                
            }
            else if (rdbClientResponsibleCom.Checked == false)
            {
                chkEmailCom.Enabled = true;
                chkSMSCom.Enabled = true;                
            }
        }
        private void rdbXDSNotifyCom_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbXDSNotifyCom.Checked == true)
            {
                chkEmailCom.Enabled = true;
                chkSMSCom.Enabled = true;
            }
            else if (rdbXDSNotifyCom.Checked == false)
            {
                chkEmailCom.Checked = false;
                txtEmailCom.Text = string.Empty;
                chkSMSCom.Checked = false;
                txtMobileLCom.Text = string.Empty;

                chkEmailCom.Enabled = false;
                chkSMSCom.Enabled = false; 
            }

        }

        private void cmbListingtypeLcom_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbListingtypeLcom.SelectedItem.ToString() == "Payment Notification")
            {
                chkDefaultMoveLcom.Enabled = true;
                chkDefaultMoveLcom.Checked = false;
            }
            else
            {
                chkDefaultMoveLcom.Enabled = false;
                chkDefaultMoveLcom.Checked = false;
            }

        }

       

        #endregion Commercial

        #endregion Delistings
        
        #endregion listings and Delistings

        #region Credit Assesment

        private void btnC4LSubmit_Click(object sender, EventArgs e)
        {
            //string strXMLResult = string.Empty;
            //try
            //{
            //    if (string.IsNullOrEmpty(txtC4LID.Text) && string.IsNullOrEmpty(txtC4LPpt.Text))
            //    {
            //        throw new Exception("Please enter either ID or Passport");
            //    }

            //    if (!aXDSConnectWS.IsTicketValid(strToken))
            //    {
            //        strToken = aXDSConnectWS.Login(Username, Password);
            //        if (aXDSConnectWS.IsTicketValid(strToken))
            //        {

            //            strXMLResult = aXDSConnectWS.ConnectGetConsumerCreditReport(strToken, txtC4LID.Text, txtC4LPpt.Text);
            //        }
            //        else if (!(strToken == string.Empty))
            //        {
            //            MessageBox.Show(strToken);
            //        }

            //    }
            //    else
            //    {
            //        strXMLResult = aXDSConnectWS.ConnectGetConsumerCreditReport(strToken, txtC4LID.Text, txtC4LPpt.Text);
            //    }

            //    txtC4LResult.Text = strXMLResult;

            //}
            //catch (Exception oException)
            //{
            //    MessageBox.Show(oException.Message);
            //}
        }


        private void cmbEnqCA_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbEnqCA.Items.Count == 0)
                {
                    aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                    strToken = aXDSConnectWS.Login(Username, Password);

                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        string axml = aXDSConnectWS.ConnectGetEnquiryReasons(strToken);
                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(axml);
                        ds.ReadXml(xmlSR);
                        cmbEnqCA.DataSource = ds.Tables[0];
                        cmbEnqCA.DisplayMember = ds.Tables[0].Columns[0].ToString();
                        cmbEnqCA.ValueMember = ds.Tables[0].Columns[0].ToString();
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }

        }

        private void button24_Click(object sender, EventArgs e)
        {
            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    textBox102.Text = aXDSConnectWS.ConnectConsumerMatchCreditAssesment(strToken, cmbEnqCA.Text, 31, txtIDNOCA.Text, txtPassCA.Text, textBox97.Text, textBox98.Text, dateTimePicker3.Value.ToShortDateString(), textBox105.Text, textBox103.Text, textBox116.Text, textBox115.Text, textBox114.Text, textBox113.Text, textBox112.Text, textBox111.Text, textBox110.Text, textBox99.Text, textBox123.Text, textBox122.Text, textBox121.Text, textBox120.Text, textBox119.Text, textBox118.Text, textBox117.Text, textBox92.Text, textBox130.Text, textBox129.Text, textBox128.Text, textBox127.Text, textBox126.Text, textBox125.Text, textBox124.Text);

                    DataSet ds = new DataSet();
                    System.IO.StringReader xmlSR = new System.IO.StringReader(txtmatch0.Text);
                    ds.ReadXml(xmlSR);
                    dataGridView6.DataSource = ds.Tables[0];
                }
                else
                {
                    textBox102.Text = aXDSConnectWS.ConnectConsumerMatchCreditAssesment(strToken, cmbEnqCA.Text, 31, txtIDNOCA.Text, txtPassCA.Text, textBox97.Text, textBox98.Text, dateTimePicker3.Value.ToShortDateString(), textBox105.Text, textBox103.Text, textBox116.Text, textBox115.Text, textBox114.Text, textBox113.Text, textBox112.Text, textBox111.Text, textBox110.Text, textBox99.Text, textBox123.Text, textBox122.Text, textBox121.Text, textBox120.Text, textBox119.Text, textBox118.Text, textBox117.Text, textBox92.Text, textBox130.Text, textBox129.Text, textBox128.Text, textBox127.Text, textBox126.Text, textBox125.Text, textBox124.Text);

                    DataSet ds = new DataSet();
                    System.IO.StringReader xmlSR = new System.IO.StringReader(textBox102.Text);
                    ds.ReadXml(xmlSR);
                    dataGridView6.DataSource = ds.Tables[0];
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }

        }

        private void button23_Click(object sender, EventArgs e)
        {
            txtIDNOCA.Text = string.Empty;
            txtPassCA.Text = string.Empty;
            textBox97.Text = string.Empty;
            textBox98.Text = string.Empty;
            dateTimePicker3.Text = string.Empty;
            textBox105.Text = string.Empty;
            textBox103.Text = string.Empty;
            textBox116.Text = string.Empty;
            textBox115.Text = string.Empty;
            textBox114.Text = string.Empty;
            textBox113.Text = string.Empty;
            textBox112.Text = string.Empty;
            textBox111.Text = string.Empty;
            textBox110.Text = string.Empty;
            textBox99.Text = string.Empty;
            textBox123.Text = string.Empty;
            textBox122.Text = string.Empty;
            textBox121.Text = string.Empty;
            textBox120.Text = string.Empty;
            textBox119.Text = string.Empty;
            textBox118.Text = string.Empty;
            textBox117.Text = string.Empty;
            textBox92.Text = string.Empty;
            textBox130.Text = string.Empty;
            textBox129.Text = string.Empty;
            textBox128.Text = string.Empty;
            textBox127.Text = string.Empty;
            textBox126.Text = string.Empty;
            textBox125.Text = string.Empty;
            textBox124.Text = string.Empty;
        }

        private void button22_Click(object sender, EventArgs e)
        {
            try
            {
                int intEnquiryID = int.Parse(textBox94.Text);
                int intEnquiryResultID = int.Parse(textBox93.Text);
                string strBonusXML = textBox95.Text;
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    textBox101.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 31, strBonusXML);
                }
                else
                {
                    textBox101.Text = aXDSConnectWS.ConnectGetResult(strToken, intEnquiryID, intEnquiryResultID, 31, strBonusXML);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }
        private void button25_Click(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            System.IO.StringReader xmlSR = new System.IO.StringReader(textBox101.Text);
            ds.ReadXml(xmlSR);
            SqlCommand cmd = new SqlCommand();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["FinChoiceConnectionString"].ConnectionString);
            cmd.Connection = con;
            cmd.CommandTimeout = 0;

            if (ds.Tables.Count > 0)
            {
                if (ds.Tables.Contains("ConsumerDetail") && ds.Tables["ConsumerDetail"].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables["ConsumerDetail"].Rows[0];

                    string BirthDate1 = "01/01/1900", LastUpdatedDate1 = "01/01/1900", FirstReportedDate1 = "01/01/1900";
                    if (!String.IsNullOrEmpty(dr["BirthDate"].ToString()))
                        BirthDate1 = Convert.ToDateTime(dr["BirthDate"]).ToShortDateString();
                    if (!String.IsNullOrEmpty(dr["LastUpdatedDate"].ToString()))
                        LastUpdatedDate1 = Convert.ToDateTime(dr["LastUpdatedDate"]).ToShortDateString();
                    if (!String.IsNullOrEmpty(dr["FirstReportedDate"].ToString()))
                        FirstReportedDate1 = Convert.ToDateTime(dr["FirstReportedDate"]).ToShortDateString();

                    cmd = new SqlCommand("Insert into ConsumerDetail(ReferenceNo,ExternalReference,ConsumerID,Initials,FirstName,SecondName,ThirdName,Surname,IDNo,PassportNo,BirthDate,Gender,TitleDesc,MaritalStatusDesc,PrivacyStatus,ResidentialAddress,PostalAddress,HomeTelephoneNo,WorkTelephoneNo,CellularNo,EmailAddress,EmployerDetail,LastUpdatedDate,FirstReportedDate) values ('" + dr["ReferenceNo"].ToString() + "','" + dr["ExternalReference"].ToString() + "'," + dr["ConsumerID"].ToString() + ",'" + dr["Initials"].ToString() + "','" + dr["FirstName"].ToString() + "','" + dr["SecondName"].ToString() + "','" + dr["ThirdName"].ToString() + "','" + dr["Surname"].ToString() + "','" + dr["IDNo"].ToString() + "','" + dr["PassportNo"].ToString() + "','" + BirthDate1 + "','" + dr["Gender"].ToString() + "','" + dr["TitleDesc"].ToString() + "','" + dr["MaritalStatusDesc"].ToString() + "','" + dr["PrivacyStatus"].ToString() + "','" + dr["ResidentialAddress"].ToString() + "','" + dr["PostalAddress"].ToString() + "','" + dr["HomeTelephoneNo"].ToString() + "','" + dr["WorkTelephoneNo"].ToString() + "','" + dr["CellularNo"].ToString() + "','" + dr["EmailAddress"].ToString() + "','" + dr["EmployerDetail"].ToString() + "','" + LastUpdatedDate1 + "','" + FirstReportedDate1 + "')", con);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                    if (ds.Tables.Contains("ReportInformation") && ds.Tables["ReportInformation"].Rows.Count > 0)
                    {
                        foreach (DataRow dRow in ds.Tables["ReportInformation"].Rows)
                        {
                            cmd = new SqlCommand("Insert into ReportInformation(ReferenceNo,ExternalReference,ReportID,ReportName) values ('" + dr["ReferenceNo"].ToString() + "','" + dr["ExternalReference"].ToString() + "'," + dRow["ReportID"].ToString() + ",'" + dRow["ReportName"].ToString() + "')", con);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                    if (ds.Tables.Contains("SubscriberInputDetails") && ds.Tables["SubscriberInputDetails"].Rows.Count > 0)
                    {
                        foreach (DataRow dRow in ds.Tables["SubscriberInputDetails"].Rows)
                        {
                            string EnquiryDate = "01/01/1900";
                            if (!String.IsNullOrEmpty(dRow["EnquiryDate"].ToString()))
                                EnquiryDate = Convert.ToDateTime(dRow["EnquiryDate"]).ToShortDateString();

                            cmd = new SqlCommand("Insert into SubscriberInputDetails(ReferenceNo,ExternalReference,EnquiryDate,EnquiryType,SubscriberName,SubscriberUserName,EnquiryInput,EnquiryReason) values ('" + dr["ReferenceNo"].ToString() + "','" + dr["ExternalReference"].ToString() + "','" + EnquiryDate + "','" + dRow["EnquiryType"].ToString() + "','" + dRow["SubscriberName"].ToString() + "','" + dRow["SubscriberUserName"].ToString() + "','" + dRow["EnquiryInput"].ToString() + "','" + dRow["EnquiryReason"].ToString() + "')", con);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                    if (ds.Tables.Contains("ConsumerAddressHistory") && ds.Tables["ConsumerAddressHistory"].Rows.Count > 0)
                    {
                        foreach (DataRow dRow in ds.Tables["ConsumerAddressHistory"].Rows)
                        {
                            string LastUpdatedDate = "01/01/1900", FirstReportedDate = "01/01/1900";
                            if (!String.IsNullOrEmpty(dRow["LastUpdatedDate"].ToString()))
                                LastUpdatedDate = Convert.ToDateTime(dRow["LastUpdatedDate"]).ToShortDateString();
                            if (!String.IsNullOrEmpty(dRow["FirstReportedDate"].ToString()))
                                FirstReportedDate = Convert.ToDateTime(dRow["FirstReportedDate"]).ToShortDateString();

                            cmd = new SqlCommand("Insert into ConsumerAddressHistory(ReferenceNo,ExternalReference,ConsumerAddressID,AddressType,AddressTypeInd,Address1,Address2,Address3,Address4,PostalCode,Address,LastUpdatedDate,FirstReportedDate,OwnerTenant) values ('" + dr["ReferenceNo"].ToString() + "','" + dr["ExternalReference"].ToString() + "'," + dRow["ConsumerAddressID"].ToString() + ",'" + dRow["AddressType"].ToString() + "','" + dRow["AddressTypeInd"].ToString() + "','" + dRow["Address1"].ToString() + "','" + dRow["Address2"].ToString() + "','" + dRow["Address3"].ToString() + "','" + dRow["Address4"].ToString() + "','" + dRow["PostalCode"].ToString() + "','" + dRow["Address"].ToString() + "','" + LastUpdatedDate + "','" + FirstReportedDate + "','" + dRow["OwnerTenant"].ToString() + "')", con);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                    if (ds.Tables.Contains("ConsumerTelephoneHistory") && ds.Tables["ConsumerTelephoneHistory"].Rows.Count > 0)
                    {
                        foreach (DataRow dRow in ds.Tables["ConsumerTelephoneHistory"].Rows)
                        {
                            string LastUpdatedDate = "01/01/1900", FirstReportedDate = "01/01/1900";
                            if (!String.IsNullOrEmpty(dRow["LastUpdatedDate"].ToString()))
                                LastUpdatedDate = Convert.ToDateTime(dRow["LastUpdatedDate"]).ToShortDateString();
                            if (!String.IsNullOrEmpty(dRow["FirstReportedDate"].ToString()))
                                FirstReportedDate = Convert.ToDateTime(dRow["FirstReportedDate"]).ToShortDateString();

                            cmd = new SqlCommand("Insert into ConsumerTelephoneHistory(ReferenceNo,ExternalReference,ConsumerTelephoneID,TelephoneType,TelephoneTypeInd,TelCode,TelNo,TelephoneNo,EmailAddress,LastUpdatedDate,FirstReportedDate) values ('" + dr["ReferenceNo"].ToString() + "','" + dr["ExternalReference"].ToString() + "'," + dRow["ConsumerTelephoneID"].ToString() + ",'" + dRow["TelephoneType"].ToString() + "','" + dRow["TelephoneTypeInd"].ToString() + "','" + dRow["TelCode"].ToString() + "','" + dRow["TelNo"].ToString() + "','" + dRow["TelephoneNo"].ToString() + "','" + dRow["EmailAddress"].ToString() + "','" + LastUpdatedDate + "','" + FirstReportedDate + "')", con);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                    if (ds.Tables.Contains("ConsumerEmploymentHistory") && ds.Tables["ConsumerEmploymentHistory"].Rows.Count > 0)
                    {
                        foreach (DataRow dRow in ds.Tables["ConsumerEmploymentHistory"].Rows)
                        {
                            string LastUpdatedDate = "01/01/1900", FirstReportedDate = "01/01/1900";
                            if (!String.IsNullOrEmpty(dRow["LastUpdatedDate"].ToString()))
                                LastUpdatedDate = Convert.ToDateTime(dRow["LastUpdatedDate"]).ToShortDateString();
                            if (!String.IsNullOrEmpty(dRow["FirstReportedDate"].ToString()))
                                FirstReportedDate = Convert.ToDateTime(dRow["FirstReportedDate"]).ToShortDateString();

                            cmd = new SqlCommand("Insert into ConsumerEmploymentHistory(ReferenceNo,ExternalReference,EmployerDetail,Designation,LastUpdatedDate,FirstReportedDate) values ('" + dr["ReferenceNo"].ToString() + "','" + dr["ExternalReference"].ToString() + "','" + dRow["EmployerDetail"].ToString() + "','" + dRow["Designation"].ToString() + "','" + LastUpdatedDate + "','" + FirstReportedDate + "')", con);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                    if (ds.Tables.Contains("ConsumerEmailHistory") && ds.Tables["ConsumerEmailHistory"].Rows.Count > 0)
                    {
                        foreach (DataRow dRow in ds.Tables["ConsumerEmailHistory"].Rows)
                        {
                            string LastUpdatedDate = "01/01/1900";
                            if (!String.IsNullOrEmpty(dRow["LastUpdatedDate"].ToString()))
                                LastUpdatedDate = Convert.ToDateTime(dRow["LastUpdatedDate"]).ToShortDateString();

                            cmd = new SqlCommand("Insert into ConsumerEmailHistory(ReferenceNo,ExternalReference,EmailAddress,LastUpdatedDate) values ('" + dr["ReferenceNo"].ToString() + "','" + dr["ExternalReference"].ToString() + "','" + dRow["EmailAddress"].ToString() + "','" + LastUpdatedDate + "')", con);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                    if (ds.Tables.Contains("ConsumerFraudIndicatorsSummary") && ds.Tables["ConsumerFraudIndicatorsSummary"].Rows.Count > 0)
                    {
                        foreach (DataRow dRow in ds.Tables["ConsumerFraudIndicatorsSummary"].Rows)
                        {
                            string HADDate = "01/01/1900";
                            if(!String.IsNullOrEmpty(dRow["HomeAffairsDeceasedDate"].ToString()))
                                HADDate = Convert.ToDateTime(dRow["HomeAffairsDeceasedDate"]).ToShortDateString();

                            cmd = new SqlCommand("Insert into ConsumerFraudIndicatorsSummary(ReferenceNo,ExternalReference,SAFPSListingYN,HomeAffairsVerificationYN,HomeAffairsDeceasedStatus,EmployerFraudVerificationYN,ProtectiveVerificationYN,HomeAffairsDeceasedDate,HomeAffairsFirstName,HomeAffairsSurname,HomeAffairsSecondName,HomeAffairsCauseOfDeath) values ('" + dr["ReferenceNo"].ToString() + "','" + dr["ExternalReference"].ToString() + "','" + dRow["SAFPSListingYN"].ToString() + "','" + dRow["HomeAffairsVerificationYN"].ToString() + "','" + dRow["HomeAffairsDeceasedStatus"].ToString() + "','" + dRow["EmployerFraudVerificationYN"].ToString() + "','" + dRow["ProtectiveVerificationYN"].ToString() + "','" + HADDate + "','" + dRow["HomeAffairsFirstName"].ToString() + "','" + dRow["HomeAffairsSurname"].ToString() + "','" + dRow["HomeAffairsSecondName"].ToString() + "','" + dRow["HomeAffairsCauseOfDeath"].ToString() + "')", con);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                    if (ds.Tables.Contains("ConsumerScoring") && ds.Tables["ConsumerScoring"].Rows.Count > 0)
                    {
                        foreach (DataRow dRow in ds.Tables["ConsumerScoring"].Rows)
                        {
                            string ScoreDate = "01/01/1900";
                            if (!String.IsNullOrEmpty(dRow["ScoreDate"].ToString()))
                                ScoreDate = Convert.ToDateTime(dRow["ScoreDate"]).ToShortDateString();

                            cmd = new SqlCommand("Insert into ConsumerScoring(ReferenceNo,ExternalReference,SubscriberID,Unique_Identifier,ScoreDate,FinalScore,Exception_Code,ReasonCode1,ReasonCode2,ReasonCode3,ModelID,Classification,Description,RiskCategory) values ('" + dr["ReferenceNo"].ToString() + "','" + dr["ExternalReference"].ToString() + "'," + dRow["SubscriberID"].ToString() + "," + dRow["Unique_Identifier"].ToString() + ",'" + ScoreDate + "'," + Convert.ToDecimal(dRow["FinalScore"]) + ",'" + dRow["Exception_Code"].ToString() + "','" + dRow["ReasonCode1"].ToString() + "','" + dRow["ReasonCode2"].ToString() + "','" + dRow["ReasonCode3"].ToString() + "','" + dRow["ModelID"].ToString() + "','" + dRow["Classification"].ToString() + "','" + dRow["Description"].ToString() + "','" + dRow["RiskCategory"].ToString() + "')", con);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                    if (ds.Tables.Contains("ConsumerPropertyInformationSummary") && ds.Tables["ConsumerPropertyInformationSummary"].Rows.Count > 0)
                    {
                        foreach (DataRow dRow in ds.Tables["ConsumerPropertyInformationSummary"].Rows)
                        {
                            cmd = new SqlCommand("Insert into ConsumerPropertyInformationSummary(ReferenceNo,ExternalReference,TotalProperty,PurchasePrice) values ('" + dr["ReferenceNo"].ToString() + "','" + dr["ExternalReference"].ToString() + "'," + dRow["TotalProperty"].ToString() + "," + Convert.ToDecimal(dRow["PurchasePrice"]) + ")", con);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                    if (ds.Tables.Contains("ConsumerOtherContactInfoAddress") && ds.Tables["ConsumerOtherContactInfoAddress"].Rows.Count > 0)
                    {
                        foreach (DataRow dRow in ds.Tables["ConsumerOtherContactInfoAddress"].Rows)
                        {
                            string LastUpdatedDate = "01/01/1900";
                            if (!String.IsNullOrEmpty(dRow["LastUpdatedDate"].ToString()))
                                LastUpdatedDate = Convert.ToDateTime(dRow["LastUpdatedDate"]).ToShortDateString();

                            cmd = new SqlCommand("Insert into ConsumerOtherContactInfoAddress(ReferenceNo,ExternalReference,Address,AddressTypeInd,LastUpdatedDate) values ('" + dr["ReferenceNo"].ToString() + "','" + dr["ExternalReference"].ToString() + "','" + dRow["Address"].ToString() + "','" + dRow["AddressTypeInd"].ToString() + "','" + LastUpdatedDate + "')", con);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                    if (ds.Tables.Contains("ConsumerOtherContactInfoTelephone") && ds.Tables["ConsumerOtherContactInfoTelephone"].Rows.Count > 0)
                    {
                        foreach (DataRow dRow in ds.Tables["ConsumerOtherContactInfoTelephone"].Rows)
                        {
                            string LastUpdatedDate = "01/01/1900";
                            if (!String.IsNullOrEmpty(dRow["LastUpdatedDate"].ToString()))
                                LastUpdatedDate = Convert.ToDateTime(dRow["LastUpdatedDate"]).ToShortDateString();

                            cmd = new SqlCommand("Insert into ConsumerOtherContactInfoTelephone(ReferenceNo,ExternalReference,TelephoneNumber,TelephoneTypeInd,LastUpdatedDate) values ('" + dr["ReferenceNo"].ToString() + "','" + dr["ExternalReference"].ToString() + "','" + dRow["TelephoneNumber"].ToString() + "','" + dRow["TelephoneTypeInd"].ToString() + "','" + LastUpdatedDate + "')", con);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                    if (ds.Tables.Contains("XDSPaymentNotification") && ds.Tables["XDSPaymentNotification"].Rows.Count > 0)
                    {
                        foreach (DataRow dRow in ds.Tables["XDSPaymentNotification"].Rows)
                        {
                            string DateLoaded = "01/01/1900", StatusDate = "01/01/1900";
                            if (!String.IsNullOrEmpty(dRow["DateLoaded"].ToString()))
                                DateLoaded = Convert.ToDateTime(dRow["DateLoaded"]).ToShortDateString();
                            if (!String.IsNullOrEmpty(dRow["StatusDate"].ToString()))
                                StatusDate = Convert.ToDateTime(dRow["StatusDate"]).ToShortDateString();
                            
                            cmd = new SqlCommand("Insert into XDSPaymentNotification(ReferenceNo,ExternalReference,SubscriberID,Company,AccountNo,Statuscode,Amount,Comments,DateLoaded,StatusDate) values ('" + dr["ReferenceNo"].ToString() + "','" + dr["ExternalReference"].ToString() + "'," + dRow["SubscriberID"].ToString() + ",'" + dRow["Company"].ToString() + "','" + dRow["AccountNo"].ToString() + "','" + dRow["Statuscode"].ToString() + "'," + Convert.ToDecimal(dRow["Amount"]) + ",'" + dRow["Comments"].ToString() + "','" + DateLoaded + "','" + StatusDate + "')", con);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                    if (ds.Tables.Contains("AccountTypeLegend") && ds.Tables["AccountTypeLegend"].Rows.Count > 0)
                    {
                        foreach (DataRow dRow in ds.Tables["AccountTypeLegend"].Rows)
                        {
                            cmd = new SqlCommand("Insert into AccountTypeLegend(ReferenceNo,ExternalReference,AccountTypeCode,AccountTypeDesc) values ('" + dr["ReferenceNo"].ToString() + "','" + dr["ExternalReference"].ToString() + "','" + dRow["AccountTypeCode"].ToString() + "','" + dRow["AccountTypeDesc"].ToString() + "')", con);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                    if (ds.Tables.Contains("ConsumerAccountStatus") && ds.Tables["ConsumerAccountStatus"].Rows.Count > 0)
                    {
                        foreach (DataRow dRow in ds.Tables["ConsumerAccountStatus"].Rows)
                        {
                            string AccountOpenedDate = "01/01/1900", LastPaymentDate = "01/01/1900";
                            if (!String.IsNullOrEmpty(dRow["AccountOpenedDate"].ToString()))
                                AccountOpenedDate = Convert.ToDateTime(dRow["AccountOpenedDate"]).ToShortDateString();
                            if (!String.IsNullOrEmpty(dRow["LastPaymentDate"].ToString()))
                                LastPaymentDate = Convert.ToDateTime(dRow["LastPaymentDate"]).ToShortDateString();
                            
                            cmd = new SqlCommand("Insert into ConsumerAccountStatus(ReferenceNo,ExternalReference,ConsumerAccountID,SubscriberID,AccountNo,SubAccountNo,AccountOpenedDate,SubscriberName,CreditLimitAmt,CurrentBalanceAmt,MonthlyInstalmentAmt,ArrearsAmt,Industry,InstallmentTerms,ArrearsTypeInd,AccountType,LastPaymentDate,StatusCodeDesc) values ('" + dr["ReferenceNo"].ToString() + "','" + dr["ExternalReference"].ToString() + "'," + dRow["ConsumerAccountID"].ToString() + "," + dRow["SubscriberID"].ToString() + ",'" + dRow["AccountNo"].ToString() + "','" + dRow["SubAccountNo"].ToString() + "','" + AccountOpenedDate + "','" + dRow["SubscriberName"].ToString() + "'," + Convert.ToDecimal(dRow["CreditLimitAmt"]) + "," + Convert.ToDecimal(dRow["CurrentBalanceAmt"]) + "," + Convert.ToDecimal(dRow["MonthlyInstalmentAmt"]) + "," + Convert.ToDecimal(dRow["ArrearsAmt"]) + ",'" + dRow["Industry"].ToString() + "','" + dRow["InstallmentTerms"].ToString() + "','" + dRow["ArrearsTypeInd"].ToString() + "','" + dRow["AccountType"].ToString() + "','" + LastPaymentDate + "','" + dRow["StatusCodeDesc"].ToString() + "')", con);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                    if (ds.Tables.Contains("Consumer24MonthlyPayment") && ds.Tables["Consumer24MonthlyPayment"].Rows.Count > 0)
                    {
                        foreach (DataRow dRow in ds.Tables["Consumer24MonthlyPayment"].Rows)
                        {
                            string AccountOpenedDate = "01/01/1900";
                            if (!String.IsNullOrEmpty(dRow["AccountOpenedDate"].ToString()))
                                AccountOpenedDate = Convert.ToDateTime(dRow["AccountOpenedDate"]).ToShortDateString();

                            cmd = new SqlCommand("Insert into Consumer24MonthlyPayment(ReferenceNo,ExternalReference,AccountOpenedDate,SubscriberName,AccountNo,SubAccountNo,M24,M23,M22,M21,M20,M19,M18,M17,M16,M15,M14,M13,M12,M11,M10,M09,M08,M07,M06,M05,M04,M03,M02,M01) values ('" + dr["ReferenceNo"].ToString() + "','" + dr["ExternalReference"].ToString() + "','" + AccountOpenedDate + "','" + dRow["SubscriberName"].ToString() + "','" + dRow["AccountNo"].ToString() + "','" + dRow["SubAccountNo"].ToString() + "','" + dRow["M24"].ToString() + "','" + dRow["M23"].ToString() + "','" + dRow["M22"].ToString() + "','" + dRow["M21"].ToString() + "','" + dRow["M20"].ToString() + "','" + dRow["M19"].ToString() + "','" + dRow["M18"].ToString() + "','" + dRow["M17"].ToString() + "','" + dRow["M16"].ToString() + "','" + dRow["M15"].ToString() + "','" + dRow["M14"].ToString() + "','" + dRow["M13"].ToString() + "','" + dRow["M12"].ToString() + "','" + dRow["M11"].ToString() + "','" + dRow["M10"].ToString() + "','" + dRow["M09"].ToString() + "','" + dRow["M08"].ToString() + "','" + dRow["M07"].ToString() + "','" + dRow["M06"].ToString() + "','" + dRow["M05"].ToString() + "','" + dRow["M04"].ToString() + "','" + dRow["M03"].ToString() + "','" + dRow["M02"].ToString() + "','" + dRow["M01"].ToString() + "')", con);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                    if (ds.Tables.Contains("ConsumerDefinition") && ds.Tables["ConsumerDefinition"].Rows.Count > 0)
                    {
                        foreach (DataRow dRow in ds.Tables["ConsumerDefinition"].Rows)
                        {
                            cmd = new SqlCommand("Insert into ConsumerDefinition(ReferenceNo,ExternalReference,DefinitionCode ,DefinitionDesc) values ('" + dr["ReferenceNo"].ToString() + "','" + dr["ExternalReference"].ToString() + "','" + dRow["DefinitionCode"].ToString() + "','" + dRow["DefinitionDesc"].ToString().Replace("'","''") + "')", con);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                    if (ds.Tables.Contains("NLRAccountTypeLegend") && ds.Tables["NLRAccountTypeLegend"].Rows.Count > 0)
                    {
                        foreach (DataRow dRow in ds.Tables["NLRAccountTypeLegend"].Rows)
                        {
                            cmd = new SqlCommand("Insert into NLRAccountTypeLegend(ReferenceNo,ExternalReference,AccountTypeCode,AccountTypeDesc) values ('" + dr["ReferenceNo"].ToString() + "','" + dr["ExternalReference"].ToString() + "','" + dRow["AccountTypeCode"].ToString() + "','" + dRow["AccountTypeDesc"].ToString() + "')", con);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                    if (ds.Tables.Contains("ConsumerNLRAccountStatus") && ds.Tables["ConsumerNLRAccountStatus"].Rows.Count > 0)
                    {
                        foreach (DataRow dRow in ds.Tables["ConsumerNLRAccountStatus"].Rows)
                        {
                            string LastPayDate = "01/01/1900", LastUpdDate = "01/01/1900", StatusDate = "01/01/1900";
                            if (!String.IsNullOrEmpty(dRow["LastPaymentDate"].ToString()))
                                LastPayDate = Convert.ToDateTime(dRow["LastPaymentDate"]).ToShortDateString();
                            if (!String.IsNullOrEmpty(dRow["LastUpdatedDate"].ToString()))
                                LastUpdDate = Convert.ToDateTime(dRow["LastUpdatedDate"]).ToShortDateString();
                            if (!String.IsNullOrEmpty(dRow["StatusDate"].ToString()))
                                StatusDate = Convert.ToDateTime(dRow["StatusDate"]).ToShortDateString();

                            cmd = new SqlCommand("Insert into ConsumerNLRAccountStatus(ReferenceNo,ExternalReference,ConsumerAccountID,SubscriberID ,AccountNo,SubAccountNo,AccountOpenedDate,SubscriberName,CreditLimitAmt,CurrentBalanceAmt,MonthlyInstalmentAmt,ArrearsAmt,ArrearsTypeInd,AccountType,LastPaymentDate,StatusCode,StatusCodeDesc,BranchCode,SubscriberCode,TotalAmtRepayable,RandValueOfInterestCharges,RandValueOfTotalChargeOfCredit,SettlementAmt,AnnualRateForTotalChargeOfCredit,LoanRegNo,LoanType,LastUpdatedDate,StatusDate,LoanEndUseCode,InterestRateType) values ('" + dr["ReferenceNo"].ToString() + "','" + dr["ExternalReference"].ToString() + "'," + dRow["ConsumerAccountID"].ToString() + "," + dRow["SubscriberID"].ToString() + ",'" + dRow["AccountNo"].ToString() + "','" + dRow["SubAccountNo"].ToString() + "','" + Convert.ToDateTime(dRow["AccountOpenedDate"]).ToShortDateString() + "','" + dRow["SubscriberName"].ToString() + "'," + Convert.ToDecimal(dRow["CreditLimitAmt"]) + "," + Convert.ToDecimal(dRow["CurrentBalanceAmt"]) + "," + Convert.ToDecimal(dRow["MonthlyInstalmentAmt"]) + "," + Convert.ToDecimal(dRow["ArrearsAmt"]) + ",'" + dRow["ArrearsTypeInd"].ToString() + "','" + dRow["AccountType"].ToString() + "','" + LastPayDate + "','" + dRow["StatusCode"].ToString() + "','" + dRow["StatusCodeDesc"].ToString() + "','" + dRow["BranchCode"].ToString() + "','" + dRow["SubscriberCode"].ToString() + "'," + Convert.ToDecimal(dRow["TotalAmtRepayable"]) + "," + Convert.ToDecimal(dRow["RandValueOfInterestCharges"]) + "," + Convert.ToDecimal(dRow["RandValueOfTotalChargeOfCredit"]) + "," + Convert.ToDecimal(dRow["SettlementAmt"]) + "," + Convert.ToDecimal(dRow["AnnualRateForTotalChargeOfCredit"]) + ",'" + dRow["LoanRegNo"].ToString() + "','" + dRow["LoanType"].ToString() + "','" + LastUpdDate + "','" + StatusDate + "','" + dRow["LoanEndUseCode"].ToString() + "','" + dRow["InterestRateType"].ToString() + "')", con);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                    if (ds.Tables.Contains("ConsumerNLR24MonthlyPayment") && ds.Tables["ConsumerNLR24MonthlyPayment"].Rows.Count > 0)
                    {
                        foreach (DataRow dRow in ds.Tables["ConsumerNLR24MonthlyPayment"].Rows)
                        {
                            string AccountOpenedDate = "01/01/1900";
                            if (!String.IsNullOrEmpty(dRow["AccountOpenedDate"].ToString()))
                                AccountOpenedDate = Convert.ToDateTime(dRow["AccountOpenedDate"]).ToShortDateString();

                            cmd = new SqlCommand("Insert into ConsumerNLR24MonthlyPayment(ReferenceNo,ExternalReference,AccountOpenedDate,SubscriberName,AccountNo,SubAccountNo,M24,M23,M22,M21,M20,M19,M18,M17,M16,M15,M14,M13,M12,M11,M10,M09,M08,M07,M06,M05,M04,M03,M02,M01) values ('" + dr["ReferenceNo"].ToString() + "','" + dr["ExternalReference"].ToString() + "','" + AccountOpenedDate + "','" + dRow["SubscriberName"].ToString() + "','" + dRow["AccountNo"].ToString() + "','" + dRow["SubAccountNo"].ToString() + "','" + dRow["M24"].ToString() + "','" + dRow["M23"].ToString() + "','" + dRow["M22"].ToString() + "','" + dRow["M21"].ToString() + "','" + dRow["M20"].ToString() + "','" + dRow["M19"].ToString() + "','" + dRow["M18"].ToString() + "','" + dRow["M17"].ToString() + "','" + dRow["M16"].ToString() + "','" + dRow["M15"].ToString() + "','" + dRow["M14"].ToString() + "','" + dRow["M13"].ToString() + "','" + dRow["M12"].ToString() + "','" + dRow["M11"].ToString() + "','" + dRow["M10"].ToString() + "','" + dRow["M09"].ToString() + "','" + dRow["M08"].ToString() + "','" + dRow["M07"].ToString() + "','" + dRow["M06"].ToString() + "','" + dRow["M05"].ToString() + "','" + dRow["M04"].ToString() + "','" + dRow["M03"].ToString() + "','" + dRow["M02"].ToString() + "','" + dRow["M01"].ToString() + "')", con);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                    if (ds.Tables.Contains("ConsumerNLRDefinition") && ds.Tables["ConsumerNLRDefinition"].Rows.Count > 0)
                    {
                        foreach (DataRow dRow in ds.Tables["ConsumerNLRDefinition"].Rows)
                        {
                            cmd = new SqlCommand("Insert into ConsumerNLRDefinition(ReferenceNo,ExternalReference,DefinitionCode,DefinitionDesc) values ('" + dr["ReferenceNo"].ToString() + "','" + dr["ExternalReference"].ToString() + "','" + dRow["DefinitionCode"].ToString() + "','" + dRow["DefinitionDesc"].ToString().Replace("'","''") + "')", con);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                    if (ds.Tables.Contains("ConsumerCPANLRDebtSummary") && ds.Tables["ConsumerCPANLRDebtSummary"].Rows.Count > 0)
                    {
                        foreach (DataRow dRow in ds.Tables["ConsumerCPANLRDebtSummary"].Rows)
                        {
                            string MostRecentJudgmentDate = "01/01/1900", MostRecentCourtNoticeDate = "01/01/1900", MostRecentAdverseDate = "01/01/1900";
                            string RecentDefaultListingDate = "01/01/1900", MostRecentEnqDateLast24Months = "01/01/1900";
                            int HighestMonthsInArrears = 0;
                            if (!String.IsNullOrEmpty(dRow["MostRecentJudgmentDate"].ToString()))
                                MostRecentJudgmentDate = Convert.ToDateTime(dRow["MostRecentJudgmentDate"]).ToShortDateString();
                            if (!String.IsNullOrEmpty(dRow["MostRecentCourtNoticeDate"].ToString()))
                                MostRecentCourtNoticeDate = Convert.ToDateTime(dRow["MostRecentCourtNoticeDate"]).ToShortDateString();
                            if (!String.IsNullOrEmpty(dRow["MostRecentAdverseDate"].ToString()))
                                MostRecentAdverseDate = Convert.ToDateTime(dRow["MostRecentAdverseDate"]).ToShortDateString();
                            if (!String.IsNullOrEmpty(dRow["RecentDefaultListingDate"].ToString()))
                                RecentDefaultListingDate = Convert.ToDateTime(dRow["RecentDefaultListingDate"]).ToShortDateString();
                            if (!String.IsNullOrEmpty(dRow["MostRecentEnqDateLast24Months"].ToString()))
                                MostRecentEnqDateLast24Months = Convert.ToDateTime(dRow["MostRecentEnqDateLast24Months"]).ToShortDateString();
                            if (!String.IsNullOrEmpty(dRow["HighestMonthsInArrears"].ToString()))
                                HighestMonthsInArrears = Convert.ToInt32(dRow["HighestMonthsInArrears"]); 

                            cmd = new SqlCommand("Insert into ConsumerCPANLRDebtSummary(ReferenceNo,ExternalReference,TotalMonthlyInstallmentCPA,TotalOutStandingDebtCPA,NoOFActiveAccountsCPA,NoOfAccountInGoodStandingCPA,NoOfAccountInBadStandingCPA,TotalArrearAmountCPA,TotalAdverseAmountCPA,NoOfAccountsOpenedinLast45DaysCPA,NoOfPaidUpOrClosedAccountsCPA,NoOfEnquiriesLast90DaysOWNCPA,NoOfEnquiriesLast90DaysOTHCPA,HighestMonthsinArrearsCPA,TotalMonthlyInstallmentNLR,TotalOutStandingDebtNLR,NoOFActiveAccountsNLR,NoOfAccountInGoodStandingNLR ,NoOfAccountInBadStandingNLR ,TotalArrearAmountNLR,TotalAdverseAmountNLR,NoOfAccountsOpenedinLast45DaysNLR,NoOfPaidUpOrClosedAccountsNLR,NoOfEnquiriesLast90DaysOWNNLR,NoOfEnquiriesLast90DaysOTHNLR,HighestMonthsinArrearsNLR,TotalMonthlyInstallment,TotalOutStandingDebt,NoOFActiveAccounts,NoOfAccountInGoodStanding,NoOfAccountInBadStanding,TotalArrearAmount,HighestMonthsInArrears,NoOfAccountsOpenedinLast45Days ,NoOfPaidUpOrClosedAccounts,NoOfEnquiriesLast90DaysOWN,NoOfEnquiriesLast90DaysOTH,JudgementCount,TotalJudgmentAmt,MostRecentJudgmentDate,CourtNoticeCount,TotalCourtNoticeAmt,MostRecentCourtNoticeDate,NoofAccountdefaults,TotalAdverseAmt,MostRecentAdverseDate,DefaultListingCount,DefaultListingAmt,RecentDefaultListingDate,NoOfEnqinLast24Months,MostRecentEnqDateLast24Months,DebtReviewStatus,DisputeMessage) values ('" + dr["ReferenceNo"].ToString() + "','" + dr["ExternalReference"].ToString() + "'," + Convert.ToDecimal(dRow["TotalMonthlyInstallmentCPA"]) + "," + Convert.ToDecimal(dRow["TotalOutStandingDebtCPA"]) + "," + dRow["NoOFActiveAccountsCPA"].ToString() + "," + dRow["NoOfAccountInGoodStandingCPA"].ToString() + "," + dRow["NoOfAccountInBadStandingCPA"].ToString() + "," + Convert.ToDecimal(dRow["TotalArrearAmountCPA"]) + "," + Convert.ToDecimal(dRow["TotalAdverseAmountCPA"]) + "," + dRow["NoOfAccountsOpenedinLast45DaysCPA"].ToString() + "," + dRow["NoOfPaidUpOrClosedAccountsCPA"].ToString() + "," + dRow["NoOfEnquiriesLast90DaysOWNCPA"].ToString() + "," + dRow["NoOfEnquiriesLast90DaysOTHCPA"].ToString() + "," + dRow["HighestMonthsinArrearsCPA"].ToString() + "," + Convert.ToDecimal(dRow["TotalMonthlyInstallmentNLR"]) + "," + Convert.ToDecimal(dRow["TotalOutStandingDebtNLR"]) + "," + dRow["NoOFActiveAccountsNLR"].ToString() + "," + dRow["NoOfAccountInGoodStandingNLR"].ToString() + "," + dRow["NoOfAccountInBadStandingNLR"].ToString() + "," + Convert.ToDecimal(dRow["TotalArrearAmountNLR"]) + "," + Convert.ToDecimal(dRow["TotalAdverseAmountNLR"]) + "," + dRow["NoOfAccountsOpenedinLast45DaysNLR"].ToString() + "," + dRow["NoOfPaidUpOrClosedAccountsNLR"].ToString() + "," + dRow["NoOfEnquiriesLast90DaysOWNNLR"].ToString() + "," + dRow["NoOfEnquiriesLast90DaysOTHNLR"].ToString() + "," + dRow["HighestMonthsinArrearsNLR"].ToString() + "," + Convert.ToDecimal(dRow["TotalMonthlyInstallment"]) + "," + Convert.ToDecimal(dRow["TotalOutStandingDebt"]) + "," + dRow["NoOFActiveAccounts"].ToString() + "," + dRow["NoOfAccountInGoodStanding"].ToString() + "," + dRow["NoOfAccountInBadStanding"].ToString() + "," + Convert.ToDecimal(dRow["TotalArrearAmount"]) + "," + HighestMonthsInArrears + "," + dRow["NoOfAccountsOpenedinLast45Days"].ToString() + "," + dRow["NoOfPaidUpOrClosedAccounts"].ToString() + "," + dRow["NoOfEnquiriesLast90DaysOWN"].ToString() + "," + dRow["NoOfEnquiriesLast90DaysOTH"].ToString() + "," + dRow["JudgementCount"].ToString() + "," + Convert.ToDecimal(dRow["TotalJudgmentAmt"]) + ",'" + MostRecentJudgmentDate + "'," + dRow["CourtNoticeCount"].ToString() + "," + Convert.ToDecimal(dRow["TotalCourtNoticeAmt"]) + ",'" + MostRecentCourtNoticeDate + "'," + dRow["NoofAccountdefaults"].ToString() + "," + Convert.ToDecimal(dRow["TotalAdverseAmt"]) + ",'" + MostRecentAdverseDate + "'," + dRow["DefaultListingCount"].ToString() + "," + Convert.ToDecimal(dRow["DefaultListingAmt"]) + ",'" + RecentDefaultListingDate + "'," + dRow["NoOfEnqinLast24Months"].ToString() + ",'" + MostRecentEnqDateLast24Months + "','" + dRow["DebtReviewStatus"].ToString() + "','" + dRow["DisputeMessage"].ToString() + "')", con);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                    if (ds.Tables.Contains("ConsumerAdverseInfo") && ds.Tables["ConsumerAdverseInfo"].Rows.Count > 0)
                    {
                        foreach (DataRow dRow in ds.Tables["ConsumerAdverseInfo"].Rows)
                        {
                            string ActionDate = "01/01/1900";
                            if (!String.IsNullOrEmpty(dRow["ActionDate"].ToString()))
                                ActionDate = Convert.ToDateTime(dRow["ActionDate"]).ToShortDateString();

                            cmd = new SqlCommand("Insert into ConsumerAdverseInfo(ReferenceNo,ExternalReference,ConsumerID,AccountNo,SubAccountNo,PeriodNum,SubscriberID,SubscriberName,ActionDate,CurrentBalanceAmt,DataStatus,StatusCode,StatusCodeDescription,Comments) values ('" + dr["ReferenceNo"].ToString() + "','" + dr["ExternalReference"].ToString() + "'," + dRow["ConsumerID"].ToString() + ",'" + dRow["AccountNo"].ToString() + "','" + dRow["SubAccountNo"].ToString() + "'," + dRow["PeriodNum"].ToString() + "," + dRow["SubscriberID"].ToString() + ",'" + dRow["SubscriberName"].ToString() + "','" + ActionDate + "'," + Convert.ToDecimal(dRow["CurrentBalanceAmt"]) + ",'" + dRow["DataStatus"].ToString() + "','" + dRow["StatusCode"].ToString() + "','" + dRow["StatusCodeDescription"].ToString() + "','" + dRow["Comments"].ToString() + "')", con);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                    if (ds.Tables.Contains("ConsumerJudgement") && ds.Tables["ConsumerJudgement"].Rows.Count > 0)
                    {
                        foreach (DataRow dRow in ds.Tables["ConsumerJudgement"].Rows)
                        {
                            string CaseFilingDate = "01/01/1900", LastUpdatedDate = "01/01/1900";
                            if (!String.IsNullOrEmpty(dRow["CaseFilingDate"].ToString()))
                                CaseFilingDate = Convert.ToDateTime(dRow["CaseFilingDate"]).ToShortDateString();
                            if (!String.IsNullOrEmpty(dRow["LastUpdatedDate"].ToString()))
                                LastUpdatedDate = Convert.ToDateTime(dRow["LastUpdatedDate"]).ToShortDateString();

                            cmd = new SqlCommand("Insert into ConsumerJudgement(ReferenceNo,ExternalReference,CaseFilingDate,CaseType,LastUpdatedDate,PlaintiffName,DisputeAmt,CaseReason,CourtName,CaseNumber,AttorneyName,TelephoneNo,Comments) values ('" + dr["ReferenceNo"].ToString() + "','" + dr["ExternalReference"].ToString() + "','" + CaseFilingDate + "','" + dRow["CaseType"].ToString() + "','" + LastUpdatedDate + "','" + dRow["PlaintiffName"].ToString() + "'," + Convert.ToDecimal(dRow["DisputeAmt"]) + ",'" + dRow["CaseReason"].ToString() + "','" + dRow["CourtName"].ToString() + "','" + dRow["CaseNumber"].ToString() + "','" + dRow["AttorneyName"].ToString() + "','" + dRow["TelephoneNo"].ToString() + "','" + dRow["Comments"].ToString() + "')", con);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                    if (ds.Tables.Contains("ConsumerAdminOrder") && ds.Tables["ConsumerAdminOrder"].Rows.Count > 0)
                    {
                        foreach (DataRow dRow in ds.Tables["ConsumerAdminOrder"].Rows)
                        {
                            string CaseFilingDate = "01/01/1900", LastUpdatedDate = "01/01/1900";
                            if (!String.IsNullOrEmpty(dRow["CaseFilingDate"].ToString()))
                                CaseFilingDate = Convert.ToDateTime(dRow["CaseFilingDate"]).ToShortDateString();
                            if (!String.IsNullOrEmpty(dRow["LastUpdatedDate"].ToString()))
                                LastUpdatedDate = Convert.ToDateTime(dRow["LastUpdatedDate"]).ToShortDateString();

                            cmd = new SqlCommand("Insert into ConsumerAdminOrder(ReferenceNo,ExternalReference,CaseFilingDate,CaseType,LastUpdatedDate,PlaintiffName,DisputeAmt,CaseReason,CourtName,CaseNumber,AttorneyName,TelephoneNo,Comments) values ('" + dr["ReferenceNo"].ToString() + "','" + dr["ExternalReference"].ToString() + "','" + CaseFilingDate + "','" + dRow["CaseType"].ToString() + "','" + LastUpdatedDate + "','" + dRow["PlaintiffName"].ToString() + "'," + Convert.ToDecimal(dRow["DisputeAmt"]) + ",'" + dRow["CaseReason"].ToString() + "','" + dRow["CourtName"].ToString() + "','" + dRow["CaseNumber"].ToString() + "','" + dRow["AttorneyName"].ToString() + "','" + dRow["TelephoneNo"].ToString() + "','" + dRow["Comments"].ToString() + "')", con);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                    if (ds.Tables.Contains("ConsumerSequestration") && ds.Tables["ConsumerSequestration"].Rows.Count > 0)
                    {
                        foreach (DataRow dRow in ds.Tables["ConsumerSequestration"].Rows)
                        {
                            string CaseFilingDate = "01/01/1900", LastUpdatedDate = "01/01/1900";
                            if (!String.IsNullOrEmpty(dRow["CaseFilingDate"].ToString()))
                                CaseFilingDate = Convert.ToDateTime(dRow["CaseFilingDate"]).ToShortDateString();
                            if (!String.IsNullOrEmpty(dRow["LastUpdatedDate"].ToString()))
                                LastUpdatedDate = Convert.ToDateTime(dRow["LastUpdatedDate"]).ToShortDateString();

                            cmd = new SqlCommand("Insert into ConsumerSequestration(ReferenceNo,ExternalReference,CaseFilingDate,CaseType,LastUpdatedDate,PlaintiffName,DisputeAmt,CaseReason,CourtName,CaseNumber,AttorneyName,TelephoneNo,Comments) values ('" + dr["ReferenceNo"].ToString() + "','" + dr["ExternalReference"].ToString() + "','" + CaseFilingDate + "','" + dRow["CaseType"].ToString() + "','" + LastUpdatedDate + "','" + dRow["PlaintiffName"].ToString() + "'," + Convert.ToDecimal(dRow["DisputeAmt"]) + ",'" + dRow["CaseReason"].ToString() + "','" + dRow["CourtName"].ToString() + "','" + dRow["CaseNumber"].ToString() + "','" + dRow["AttorneyName"].ToString() + "','" + dRow["TelephoneNo"].ToString() + "','" + dRow["Comments"].ToString() + "')", con);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                    if (ds.Tables.Contains("ConsumerDefaultAlert") && ds.Tables["ConsumerDefaultAlert"].Rows.Count > 0)
                    {
                        foreach (DataRow dRow in ds.Tables["ConsumerDefaultAlert"].Rows)
                        {
                            string DateLoaded = "01/01/1900", StatusDate = "01/01/1900";
                            if (!String.IsNullOrEmpty(dRow["DateLoaded"].ToString()))
                                DateLoaded = Convert.ToDateTime(dRow["DateLoaded"]).ToShortDateString();
                            if (!String.IsNullOrEmpty(dRow["StatusDate"].ToString()))
                                StatusDate = Convert.ToDateTime(dRow["StatusDate"]).ToShortDateString();
                            
                            cmd = new SqlCommand("Insert into ConsumerDefaultAlert(ReferenceNo,ExternalReference,SubscriberID,Company,AccountNo,Statuscode,Amount,Comments,DateLoaded,StatusDate) values ('" + dr["ReferenceNo"].ToString() + "','" + dr["ExternalReference"].ToString() + "'," + dRow["SubscriberID"].ToString() + ",'" + dRow["Company"].ToString() + "','" + dRow["AccountNo"].ToString() + "','" + dRow["Statuscode"].ToString() + "'," + Convert.ToDecimal(dRow["Amount"]) + ",'" + dRow["Comments"].ToString() + "','" + DateLoaded + "','" + StatusDate + "')", con);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                    if (ds.Tables.Contains("ConsumerEnquiryHistory") && ds.Tables["ConsumerEnquiryHistory"].Rows.Count > 0)
                    {
                        foreach (DataRow dRow in ds.Tables["ConsumerEnquiryHistory"].Rows)
                        {
                            string EnquiryDate = "01/01/1900";
                            if (!String.IsNullOrEmpty(dRow["EnquiryDate"].ToString()))
                                EnquiryDate = Convert.ToDateTime(dRow["EnquiryDate"]).ToShortDateString();

                            cmd = new SqlCommand("Insert into ConsumerEnquiryHistory(ReferenceNo,ExternalReference,EnquiryDate,SubscriberName,SubscriberBusinessTypeDesc,CreditGrantorEnquiryReasonDesc,SubscriberContact) values ('" + dr["ReferenceNo"].ToString() + "','" + dr["ExternalReference"].ToString() + "','" + EnquiryDate + "','" + dRow["SubscriberName"].ToString() + "','" + dRow["SubscriberBusinessTypeDesc"].ToString() + "','" + dRow["CreditGrantorEnquiryReasonDesc"].ToString() + "','" + dRow["SubscriberContact"].ToString() + "')", con);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                    if (ds.Tables.Contains("ConsumerDebtReviewStatus") && ds.Tables["ConsumerDebtReviewStatus"].Rows.Count > 0)
                    {
                        foreach (DataRow dRow in ds.Tables["ConsumerDebtReviewStatus"].Rows)
                        {
                            string BirthDate = "01/01/1900", DebtReviewStatusDate = "01/01/1900";
                            if (!String.IsNullOrEmpty(dRow["BirthDate"].ToString()))
                                BirthDate = Convert.ToDateTime(dRow["BirthDate"]).ToShortDateString();
                            if (!String.IsNullOrEmpty(dRow["DebtReviewStatusDate"].ToString()))
                                DebtReviewStatusDate = Convert.ToDateTime(dRow["DebtReviewStatusDate"]).ToShortDateString();
                            
                            cmd = new SqlCommand("Insert into ConsumerDebtReviewStatus(ReferenceNo,ExternalReference,IDno,FirstName,SurName,BirthDate,DebtReviewStatusDate,DebtCounsellorName,DebtCounsellorFirstName,DebtCounsellorSurName,DebtCounsellorTelephoneNo,DebtReviewStatusCode,HomeTelephoneNo,WorkTelephoneNo,CellularNo,DebtReviewStatus,DebtCounsellorRegistrationNo) values ('" + dr["ReferenceNo"].ToString() + "','" + dr["ExternalReference"].ToString() + "','" + dRow["IDno"].ToString() + "','" + dRow["FirstName"].ToString() + "','" + dRow["SurName"].ToString() + "','" + BirthDate + "','" + DebtReviewStatusDate + "','" + dRow["DebtCounsellorName"].ToString() + "','" + dRow["DebtCounsellorFirstName"].ToString() + "','" + dRow["DebtCounsellorSurName"].ToString() + "','" + dRow["DebtCounsellorTelephoneNo"].ToString().Replace("'","") + "','" + dRow["DebtReviewStatusCode"].ToString() + "','" + dRow["HomeTelephoneNo"].ToString() + "','" + dRow["WorkTelephoneNo"].ToString() + "','" + dRow["CellularNo"].ToString() + "','" + dRow["DebtReviewStatus"].ToString() + "','" + dRow["DebtCounsellorRegistrationNo"] + "')", con);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                    if (ds.Tables.Contains("ConsumerDirectorShipLink") && ds.Tables["ConsumerDirectorShipLink"].Rows.Count > 0)
                    {
                        foreach (DataRow dRow in ds.Tables["ConsumerDirectorShipLink"].Rows)
                        {
                            string AppointmentDate = "01/01/1900";
                            if (!String.IsNullOrEmpty(dRow["AppointmentDate"].ToString()))
                                AppointmentDate = Convert.ToDateTime(dRow["AppointmentDate"]).ToShortDateString();
                            
                            cmd = new SqlCommand("Insert into ConsumerDirectorShipLink(ReferenceNo,ExternalReference,DirectorDesignationDesc,AppointmentDate,CommercialName,RegistrationNo,PhysicalAddress,TelephoneNo,SICDesc,DirectorStatus) values ('" + dr["ReferenceNo"].ToString() + "','" + dr["ExternalReference"].ToString() + "','" + dRow["DirectorDesignationDesc"].ToString() + "','" + AppointmentDate + "','" + dRow["CommercialName"].ToString() + "','" + dRow["RegistrationNo"].ToString() + "','" + dRow["PhysicalAddress"].ToString() + "','" + dRow["TelephoneNo"].ToString() + "','" + dRow["SICDesc"].ToString() + "','" + dRow["DirectorStatus"].ToString() + "')", con);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                    if (ds.Tables.Contains("ConsumerPropertyInformation") && ds.Tables["ConsumerPropertyInformation"].Rows.Count > 0)
                    {
                        foreach (DataRow dRow in ds.Tables["ConsumerPropertyInformation"].Rows)
                        {
                            string TransferDate = "01/01/1900", PurchaseDate = "01/01/1900";
                            if (!String.IsNullOrEmpty(dRow["TransferDate"].ToString()))
                                TransferDate = Convert.ToDateTime(dRow["TransferDate"]).ToShortDateString();
                            if (!String.IsNullOrEmpty(dRow["PurchaseDate"].ToString()))
                                PurchaseDate = Convert.ToDateTime(dRow["PurchaseDate"]).ToShortDateString();
                            
                            cmd = new SqlCommand("Insert into ConsumerPropertyInformation(ReferenceNo,ExternalReference,AuthorityName,TownshipName,StandNo,PortionNo,TitleDeedNo,BuyerName,BuyerTypeCode,BuyerIDNo,BuyerMaritalStatusCode,SellerName,SellerTypeCode,SellerIDNo,SellerMaritalStatusCode,TransferDate,RegistrarName,OldTitleDeedNo,AttorneyFirmNo,AttorneyFileNo,TitleDeedFeeAmt,PropertyTypeCode,StreetNo,StreetName,SuburbName,CityName,TransferID,ErfNo,DeedsOffice,PhysicalAddress,PropertyTypeDesc,ErfSize,PurchaseDate,PurchasePriceAmt,BuyerSharePerc,BondHolderName,BondAccountNo,BondAmt) values ('" + dr["ReferenceNo"].ToString() + "','" + dr["ExternalReference"].ToString() + "','" + dRow["AuthorityName"].ToString() + "','" + dRow["TownshipName"].ToString() + "','" + dRow["StandNo"].ToString() + "','" + dRow["PortionNo"].ToString() + "','" + dRow["TitleDeedNo"].ToString() + "','" + dRow["BuyerName"].ToString() + "','" + dRow["BuyerTypeCode"].ToString() + "','" + dRow["BuyerIDNo"].ToString() + "','" + dRow["BuyerMaritalStatusCode"].ToString() + "','" + dRow["SellerName"].ToString() + "','" + dRow["SellerTypeCode"].ToString() + "','" + dRow["SellerIDNo"].ToString() + "','" + dRow["SellerMaritalStatusCode"].ToString() + "','" + TransferDate + "','" + dRow["RegistrarName"].ToString() + "','" + dRow["OldTitleDeedNo"].ToString() + "','" + dRow["AttorneyFirmNo"].ToString() + "','" + dRow["AttorneyFileNo"].ToString() + "','" + Convert.ToDecimal(dRow["TitleDeedFeeAmt"]) + "','" + dRow["PropertyTypeCode"].ToString() + "','" + dRow["StreetNo"].ToString() + "','" + dRow["StreetName"].ToString() + "','" + dRow["SuburbName"].ToString() + "','" + dRow["CityName"].ToString() + "'," + dRow["TransferID"].ToString() + ",'" + dRow["ErfNo"].ToString() + "','" + dRow["DeedsOffice"].ToString() + "','" + dRow["PhysicalAddress"].ToString() + "','" + dRow["PropertyTypeDesc"].ToString() + "','" + dRow["ErfSize"].ToString() + "','" + PurchaseDate + "'," + Convert.ToDecimal(dRow["PurchasePriceAmt"]) + "," + Convert.ToDecimal(dRow["BuyerSharePerc"]) + ",'" + dRow["BondHolderName"].ToString() + "','" + dRow["BondAccountNo"].ToString() + "'," + Convert.ToDecimal(dRow["BondAmt"]) + ")", con);
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }
                }
            }
        }

        #endregion Credit Assesment

        #region [MPOWA]
        private void btnMSubmit_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
            //    if (!aXDSConnectWS.IsTicketValid(strToken))
            //    {
            //        strToken = aXDSConnectWS.Login(Username, Password);
            //        if (aXDSConnectWS.IsTicketValid(strToken))
            //        {
            //            txtMReport.Text = aXDSConnectWS.ConnectCustomVettingH(Username, Password, txtMFName.Text, txtMSName.Text, txtMSuName.Text, txtMID.Text, txtMPpt.Text, txtMDOB.Value.ToShortDateString(), Convert.ToDouble(txtMGross.Text));
            //        }
            //        else
            //        {
            //            if (!(strToken == string.Empty))
            //            {
            //                MessageBox.Show(strToken);
            //            }
            //        }
            //    }
            //    else
            //    {
            //        txtMReport.Text = aXDSConnectWS.ConnectCustomVettingH(Username, Password, txtMFName.Text, txtMSName.Text, txtMSuName.Text, txtMID.Text, txtMPpt.Text, txtMDOB.Value.ToShortDateString(), Convert.ToDouble(txtMGross.Text));
            //    }
            //}
            //catch (Exception oException)
            //{
            //    MessageBox.Show(oException.Message);
            //}
        }
        #endregion

        private void button26_Click(object sender, EventArgs e)
        {
            try
            {
                int intRefNo = int.Parse(txtBIRefNo.Text);
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        txtBIResult.Text = aXDSConnectWS.ConnectGetBusinessInvestigativeEnquiryResult(strToken, intRefNo);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    txtBIResult.Text = aXDSConnectWS.ConnectGetBusinessInvestigativeEnquiryResult(strToken, intRefNo);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        private void button30_Click(object sender, EventArgs e)
        {
            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        textBox134.Text = aXDSConnectWS.ConnectConsumerMatch(strToken, null, 83, textBox132.Text, textBox107.Text, textBox108.Text, textBox109.Text, dateTimePicker4.Value.ToShortDateString(), textBox131.Text, textBox96.Text);

                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(textBox134.Text);
                        ds.ReadXml(xmlSR);
                        dataGridView7.DataSource = ds.Tables[0];
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    textBox134.Text = aXDSConnectWS.ConnectConsumerMatch(strToken, null, 83, textBox132.Text, textBox107.Text, textBox108.Text, textBox109.Text, dateTimePicker4.Value.ToShortDateString(), textBox131.Text, textBox96.Text);

                    DataSet ds = new DataSet();
                    System.IO.StringReader xmlSR = new System.IO.StringReader(textBox134.Text);
                    ds.ReadXml(xmlSR);
                    dataGridView7.DataSource = ds.Tables[0];
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }

        }

        private void button29_Click(object sender, EventArgs e)
        {
            textBox132.Text = string.Empty;
            textBox107.Text = string.Empty;
            textBox108.Text = string.Empty;
            textBox109.Text = string.Empty;
            textBox131.Text = string.Empty;
            textBox96.Text = string.Empty;
        }

        private void button27_Click(object sender, EventArgs e)
        {

        }

        private void button28_Click(object sender, EventArgs e)
        {
            try
            {
                int intEnquiryID = int.Parse(textBox104.Text);
                int intEnquiryResultID = int.Parse(textBox100.Text);
                string strBonusXML = textBox106.Text;
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        textBox133.Text = aXDSConnectWS.ConnectCustomVettingH(strToken, intEnquiryID, intEnquiryResultID);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    textBox133.Text = aXDSConnectWS.ConnectCustomVettingH(strToken, intEnquiryID, intEnquiryResultID);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        private void button34_Click(object sender, EventArgs e)
        {
            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        textBox145.Text = aXDSConnectWS.ConnectConsumerMatch(strToken, null, 92, textBox143.Text, textBox139.Text, textBox140.Text, textBox141.Text, dateTimePicker5.Value.ToShortDateString(), textBox142.Text, textBox135.Text);

                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(textBox145.Text);
                        ds.ReadXml(xmlSR);
                        dataGridView8.DataSource = ds.Tables[0];
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    textBox145.Text = aXDSConnectWS.ConnectConsumerMatch(strToken, null, 92, textBox143.Text, textBox139.Text, textBox140.Text, textBox141.Text, dateTimePicker5.Value.ToShortDateString(), textBox142.Text, textBox135.Text);

                    DataSet ds = new DataSet();
                    System.IO.StringReader xmlSR = new System.IO.StringReader(textBox145.Text);
                    ds.ReadXml(xmlSR);
                    dataGridView8.DataSource = ds.Tables[0];
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        private void button33_Click(object sender, EventArgs e)
        {
            textBox135.Text = string.Empty;
            textBox139.Text = string.Empty;
            textBox140.Text = string.Empty;
            textBox141.Text = string.Empty;
            textBox143.Text = string.Empty;
            textBox142.Text = string.Empty;
        }

        private void button32_Click(object sender, EventArgs e)
        {
            try
            {
                int intEnquiryID = int.Parse(textBox137.Text);
                int intEnquiryResultID = int.Parse(textBox136.Text);
                string strBonusXML = textBox138.Text;
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        textBox144.Text = aXDSConnectWS.ConnectCustomVettingI(strToken, intEnquiryID, intEnquiryResultID);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    textBox144.Text = aXDSConnectWS.ConnectCustomVettingI(strToken, intEnquiryID, intEnquiryResultID);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        private void button42_Click(object sender, EventArgs e)
        {
            try
            {
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        textBox167.Text = aXDSConnectWS.ConnectConsumerMatch(strToken, null, 93, textBox165.Text, textBox161.Text, textBox162.Text, textBox163.Text, dateTimePicker7.Value.ToShortDateString(), textBox164.Text, textBox157.Text);

                        DataSet ds = new DataSet();
                        System.IO.StringReader xmlSR = new System.IO.StringReader(textBox167.Text);
                        ds.ReadXml(xmlSR);
                        dataGridView10.DataSource = ds.Tables[0];
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    textBox167.Text = aXDSConnectWS.ConnectConsumerMatch(strToken, null, 93, textBox165.Text, textBox161.Text, textBox162.Text, textBox163.Text, dateTimePicker7.Value.ToShortDateString(), textBox164.Text, textBox157.Text);

                    DataSet ds = new DataSet();
                    System.IO.StringReader xmlSR = new System.IO.StringReader(textBox167.Text);
                    ds.ReadXml(xmlSR);
                    dataGridView10.DataSource = ds.Tables[0];
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        private void button41_Click(object sender, EventArgs e)
        {
            textBox165.Text = string.Empty;
            textBox161.Text = string.Empty;
            textBox162.Text = string.Empty;
            textBox163.Text = string.Empty;
            textBox164.Text = string.Empty;
            textBox157.Text = string.Empty;
        }

        private void button40_Click(object sender, EventArgs e)
        {
            try
            {
                int intEnquiryID = int.Parse(textBox159.Text);
                int intEnquiryResultID = int.Parse(textBox158.Text);
                string strBonusXML = textBox160.Text;
                aXDSConnectWS.Url = ConfigurationSettings.AppSettings["URL"].ToString();
                if (!aXDSConnectWS.IsTicketValid(strToken))
                {
                    strToken = aXDSConnectWS.Login(Username, Password);
                    if (aXDSConnectWS.IsTicketValid(strToken))
                    {
                        textBox166.Text = aXDSConnectWS.ConnectCustomVettingJ(strToken, intEnquiryID, intEnquiryResultID);
                    }
                    else
                    {
                        if (!(strToken == string.Empty))
                        {
                            MessageBox.Show(strToken);
                        }
                    }
                }
                else
                {
                    textBox166.Text = aXDSConnectWS.ConnectCustomVettingJ(strToken, intEnquiryID, intEnquiryResultID);
                }
            }
            catch (Exception oException)
            {
                MessageBox.Show(oException.Message);
            }
        }

        private void button39_Click(object sender, EventArgs e)
        {

        }
    }
}
