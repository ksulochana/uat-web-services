using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using XDSPortalAuthentication;
using System.Data.SqlClient;

namespace XDSConsumerAuthenticationBBTestApp {
  public class AuthenticationTest1 : System.Windows.Forms.Form {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.Container components = null;
    private System.Windows.Forms.GroupBox grpQuestions;
    private System.Windows.Forms.ListView lstQuestions;
    private System.Windows.Forms.GroupBox grpAnswers;
    private System.Windows.Forms.ComboBox cboAnswer;
    private System.Windows.Forms.GroupBox grpSearchCriteria;
    private System.Windows.Forms.TextBox txtIDNo;
    private System.Windows.Forms.Label lblIDNo;
    private System.Windows.Forms.Button btnSearch;
    private System.Windows.Forms.GroupBox grpAuthenticationDetails;
    private System.Windows.Forms.TextBox txtReferenceNo;
    private System.Windows.Forms.Label lblReferenceNo;
    private System.Windows.Forms.TextBox txtAuthenticationType;
    private System.Windows.Forms.Label lblAuthenticationType;
    private System.Windows.Forms.TextBox txtAuthenticationStatus;
    private System.Windows.Forms.Label lblAuthenticationStatus;
    private System.Windows.Forms.GroupBox grpAuthenticationResults;
    private System.Windows.Forms.Label lblRepeatAuthenticationMessage;
    private System.Windows.Forms.TextBox txtAuthenticatedPerc;
    private System.Windows.Forms.Label lblAuthenticatedPerc;
    private System.Windows.Forms.Button btnAuthenticate;
    private System.Windows.Forms.TextBox txtRepeatAuthenticationMessage;
    private System.Windows.Forms.TextBox txtPassportNo;
    private System.Windows.Forms.Label lblPassportNo;
    private System.Windows.Forms.Button btnVoid;
		private System.Windows.Forms.GroupBox grpPersonalDetails;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox txtSurname;
		private System.Windows.Forms.TextBox txtResultIDNo;
		private System.Windows.Forms.TextBox txtFirstName;
		private System.Windows.Forms.TextBox txtSecondName;
        private TextBox textBox7;
        private Label label11;
        private TextBox txtBirthDate;
        private Label label10;
        private TextBox textBox5;
        private Label label9;
        private GroupBox groupBox1;
        private TextBox txtHASecondName;
        private TextBox txtHaFirstName;
        private TextBox txtHAIdNo;
        private TextBox txtHaSurname;
        private Label label5;
        private Label label6;
        private Label label7;
        private Label label8;
        private TextBox txtHaDeceasedDate;
        private Label label14;
        private TextBox txtHADeceasedStatus;
        private Label label13;
        private TextBox txtHaBirthDate;
        private TextBox txtHaCauseOfDeath;
        private Label label12;
        private TextBox textBox9;
        private Label label17;
        private Label label16;
        private GroupBox groupBox2;
        private TextBox txtAccountAgeDetails;
        private DataGridView dataGridView1;
        private GroupBox groupBox3;
        private Button button1;
        private TextBox txtSubEnConsID;
        private Label label18;

    // Use My Inherited Authentication Manager Object
    //private MyAuthenticationManager moMyAuthenticationManager = new MyAuthenticationManager();
        private AuthenticationProcess moMyAuthenticationManager;
     
    public AuthenticationTest1() {
      //
      // Required for Windows Form Designer support
      //
      InitializeComponent();

      ClearInfo();
      
    }

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    protected override void Dispose( bool disposing ) {
      if( disposing ) {
        if (components != null) {
          components.Dispose();
        }
      }
      base.Dispose( disposing );
    }

    #region Windows Form Designer generated code
    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
        this.grpQuestions = new System.Windows.Forms.GroupBox();
        this.lstQuestions = new System.Windows.Forms.ListView();
        this.grpAnswers = new System.Windows.Forms.GroupBox();
        this.cboAnswer = new System.Windows.Forms.ComboBox();
        this.grpSearchCriteria = new System.Windows.Forms.GroupBox();
        this.txtPassportNo = new System.Windows.Forms.TextBox();
        this.lblPassportNo = new System.Windows.Forms.Label();
        this.btnSearch = new System.Windows.Forms.Button();
        this.txtIDNo = new System.Windows.Forms.TextBox();
        this.lblIDNo = new System.Windows.Forms.Label();
        this.grpAuthenticationDetails = new System.Windows.Forms.GroupBox();
        this.txtRepeatAuthenticationMessage = new System.Windows.Forms.TextBox();
        this.lblRepeatAuthenticationMessage = new System.Windows.Forms.Label();
        this.txtAuthenticationStatus = new System.Windows.Forms.TextBox();
        this.lblAuthenticationStatus = new System.Windows.Forms.Label();
        this.txtAuthenticationType = new System.Windows.Forms.TextBox();
        this.lblAuthenticationType = new System.Windows.Forms.Label();
        this.txtReferenceNo = new System.Windows.Forms.TextBox();
        this.lblReferenceNo = new System.Windows.Forms.Label();
        this.grpAuthenticationResults = new System.Windows.Forms.GroupBox();
        this.textBox9 = new System.Windows.Forms.TextBox();
        this.label17 = new System.Windows.Forms.Label();
        this.txtAuthenticatedPerc = new System.Windows.Forms.TextBox();
        this.lblAuthenticatedPerc = new System.Windows.Forms.Label();
        this.btnAuthenticate = new System.Windows.Forms.Button();
        this.btnVoid = new System.Windows.Forms.Button();
        this.grpPersonalDetails = new System.Windows.Forms.GroupBox();
        this.textBox7 = new System.Windows.Forms.TextBox();
        this.label11 = new System.Windows.Forms.Label();
        this.txtBirthDate = new System.Windows.Forms.TextBox();
        this.label10 = new System.Windows.Forms.Label();
        this.textBox5 = new System.Windows.Forms.TextBox();
        this.label9 = new System.Windows.Forms.Label();
        this.txtSecondName = new System.Windows.Forms.TextBox();
        this.txtFirstName = new System.Windows.Forms.TextBox();
        this.txtResultIDNo = new System.Windows.Forms.TextBox();
        this.txtSurname = new System.Windows.Forms.TextBox();
        this.label4 = new System.Windows.Forms.Label();
        this.label3 = new System.Windows.Forms.Label();
        this.label2 = new System.Windows.Forms.Label();
        this.label1 = new System.Windows.Forms.Label();
        this.groupBox1 = new System.Windows.Forms.GroupBox();
        this.label16 = new System.Windows.Forms.Label();
        this.txtHaDeceasedDate = new System.Windows.Forms.TextBox();
        this.label14 = new System.Windows.Forms.Label();
        this.txtHADeceasedStatus = new System.Windows.Forms.TextBox();
        this.label13 = new System.Windows.Forms.Label();
        this.txtHaBirthDate = new System.Windows.Forms.TextBox();
        this.txtHaCauseOfDeath = new System.Windows.Forms.TextBox();
        this.label12 = new System.Windows.Forms.Label();
        this.txtHASecondName = new System.Windows.Forms.TextBox();
        this.txtHaFirstName = new System.Windows.Forms.TextBox();
        this.txtHAIdNo = new System.Windows.Forms.TextBox();
        this.txtHaSurname = new System.Windows.Forms.TextBox();
        this.label5 = new System.Windows.Forms.Label();
        this.label6 = new System.Windows.Forms.Label();
        this.label7 = new System.Windows.Forms.Label();
        this.label8 = new System.Windows.Forms.Label();
        this.groupBox2 = new System.Windows.Forms.GroupBox();
        this.txtAccountAgeDetails = new System.Windows.Forms.TextBox();
        this.dataGridView1 = new System.Windows.Forms.DataGridView();
        this.groupBox3 = new System.Windows.Forms.GroupBox();
        this.button1 = new System.Windows.Forms.Button();
        this.txtSubEnConsID = new System.Windows.Forms.TextBox();
        this.label18 = new System.Windows.Forms.Label();
        this.grpQuestions.SuspendLayout();
        this.grpAnswers.SuspendLayout();
        this.grpSearchCriteria.SuspendLayout();
        this.grpAuthenticationDetails.SuspendLayout();
        this.grpAuthenticationResults.SuspendLayout();
        this.grpPersonalDetails.SuspendLayout();
        this.groupBox1.SuspendLayout();
        this.groupBox2.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
        this.groupBox3.SuspendLayout();
        this.SuspendLayout();
        // 
        // grpQuestions
        // 
        this.grpQuestions.Controls.Add(this.lstQuestions);
        this.grpQuestions.Controls.Add(this.grpAnswers);
        this.grpQuestions.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.grpQuestions.Location = new System.Drawing.Point(8, 127);
        this.grpQuestions.Name = "grpQuestions";
        this.grpQuestions.Size = new System.Drawing.Size(432, 185);
        this.grpQuestions.TabIndex = 6;
        this.grpQuestions.TabStop = false;
        this.grpQuestions.Text = "Questions";
        // 
        // lstQuestions
        // 
        this.lstQuestions.FullRowSelect = true;
        this.lstQuestions.Location = new System.Drawing.Point(8, 16);
        this.lstQuestions.Name = "lstQuestions";
        this.lstQuestions.Size = new System.Drawing.Size(416, 102);
        this.lstQuestions.TabIndex = 3;
        this.lstQuestions.UseCompatibleStateImageBehavior = false;
        this.lstQuestions.View = System.Windows.Forms.View.List;
        this.lstQuestions.SelectedIndexChanged += new System.EventHandler(this.lstQuestions_SelectedIndexChanged);
        // 
        // grpAnswers
        // 
        this.grpAnswers.Controls.Add(this.cboAnswer);
        this.grpAnswers.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.grpAnswers.Location = new System.Drawing.Point(8, 126);
        this.grpAnswers.Name = "grpAnswers";
        this.grpAnswers.Size = new System.Drawing.Size(416, 48);
        this.grpAnswers.TabIndex = 7;
        this.grpAnswers.TabStop = false;
        this.grpAnswers.Text = "Answers";
        // 
        // cboAnswer
        // 
        this.cboAnswer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.cboAnswer.Location = new System.Drawing.Point(8, 16);
        this.cboAnswer.Name = "cboAnswer";
        this.cboAnswer.Size = new System.Drawing.Size(400, 21);
        this.cboAnswer.TabIndex = 4;
        this.cboAnswer.SelectedIndexChanged += new System.EventHandler(this.cboAnswer_SelectedIndexChanged);
        // 
        // grpSearchCriteria
        // 
        this.grpSearchCriteria.Controls.Add(this.txtPassportNo);
        this.grpSearchCriteria.Controls.Add(this.lblPassportNo);
        this.grpSearchCriteria.Controls.Add(this.btnSearch);
        this.grpSearchCriteria.Controls.Add(this.txtIDNo);
        this.grpSearchCriteria.Controls.Add(this.lblIDNo);
        this.grpSearchCriteria.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.grpSearchCriteria.Location = new System.Drawing.Point(8, 8);
        this.grpSearchCriteria.Name = "grpSearchCriteria";
        this.grpSearchCriteria.Size = new System.Drawing.Size(432, 42);
        this.grpSearchCriteria.TabIndex = 8;
        this.grpSearchCriteria.TabStop = false;
        this.grpSearchCriteria.Text = "Search Criteria";
        // 
        // txtPassportNo
        // 
        this.txtPassportNo.Location = new System.Drawing.Point(257, 16);
        this.txtPassportNo.MaxLength = 13;
        this.txtPassportNo.Name = "txtPassportNo";
        this.txtPassportNo.Size = new System.Drawing.Size(88, 20);
        this.txtPassportNo.TabIndex = 1;
        // 
        // lblPassportNo
        // 
        this.lblPassportNo.Location = new System.Drawing.Point(185, 16);
        this.lblPassportNo.Name = "lblPassportNo";
        this.lblPassportNo.Size = new System.Drawing.Size(72, 23);
        this.lblPassportNo.TabIndex = 7;
        this.lblPassportNo.Text = "Passport No";
        // 
        // btnSearch
        // 
        this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.btnSearch.Location = new System.Drawing.Point(361, 16);
        this.btnSearch.Name = "btnSearch";
        this.btnSearch.Size = new System.Drawing.Size(64, 23);
        this.btnSearch.TabIndex = 2;
        this.btnSearch.Text = "Search";
        this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
        // 
        // txtIDNo
        // 
        this.txtIDNo.Location = new System.Drawing.Point(81, 16);
        this.txtIDNo.MaxLength = 13;
        this.txtIDNo.Name = "txtIDNo";
        this.txtIDNo.Size = new System.Drawing.Size(88, 20);
        this.txtIDNo.TabIndex = 0;
        // 
        // lblIDNo
        // 
        this.lblIDNo.Location = new System.Drawing.Point(9, 16);
        this.lblIDNo.Name = "lblIDNo";
        this.lblIDNo.Size = new System.Drawing.Size(72, 23);
        this.lblIDNo.TabIndex = 5;
        this.lblIDNo.Text = "ID No";
        // 
        // grpAuthenticationDetails
        // 
        this.grpAuthenticationDetails.Controls.Add(this.txtRepeatAuthenticationMessage);
        this.grpAuthenticationDetails.Controls.Add(this.lblRepeatAuthenticationMessage);
        this.grpAuthenticationDetails.Controls.Add(this.txtAuthenticationStatus);
        this.grpAuthenticationDetails.Controls.Add(this.lblAuthenticationStatus);
        this.grpAuthenticationDetails.Controls.Add(this.txtAuthenticationType);
        this.grpAuthenticationDetails.Controls.Add(this.lblAuthenticationType);
        this.grpAuthenticationDetails.Controls.Add(this.txtReferenceNo);
        this.grpAuthenticationDetails.Controls.Add(this.lblReferenceNo);
        this.grpAuthenticationDetails.Location = new System.Drawing.Point(448, 127);
        this.grpAuthenticationDetails.Name = "grpAuthenticationDetails";
        this.grpAuthenticationDetails.Size = new System.Drawing.Size(523, 124);
        this.grpAuthenticationDetails.TabIndex = 9;
        this.grpAuthenticationDetails.TabStop = false;
        this.grpAuthenticationDetails.Text = "Authentication Details";
        // 
        // txtRepeatAuthenticationMessage
        // 
        this.txtRepeatAuthenticationMessage.Location = new System.Drawing.Point(144, 90);
        this.txtRepeatAuthenticationMessage.Multiline = true;
        this.txtRepeatAuthenticationMessage.Name = "txtRepeatAuthenticationMessage";
        this.txtRepeatAuthenticationMessage.ReadOnly = true;
        this.txtRepeatAuthenticationMessage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
        this.txtRepeatAuthenticationMessage.Size = new System.Drawing.Size(359, 33);
        this.txtRepeatAuthenticationMessage.TabIndex = 14;
        this.txtRepeatAuthenticationMessage.TabStop = false;
        // 
        // lblRepeatAuthenticationMessage
        // 
        this.lblRepeatAuthenticationMessage.Location = new System.Drawing.Point(8, 83);
        this.lblRepeatAuthenticationMessage.Name = "lblRepeatAuthenticationMessage";
        this.lblRepeatAuthenticationMessage.Size = new System.Drawing.Size(120, 40);
        this.lblRepeatAuthenticationMessage.TabIndex = 13;
        this.lblRepeatAuthenticationMessage.Text = "Repeat Authentication Message";
        // 
        // txtAuthenticationStatus
        // 
        this.txtAuthenticationStatus.Location = new System.Drawing.Point(144, 63);
        this.txtAuthenticationStatus.Name = "txtAuthenticationStatus";
        this.txtAuthenticationStatus.ReadOnly = true;
        this.txtAuthenticationStatus.Size = new System.Drawing.Size(120, 20);
        this.txtAuthenticationStatus.TabIndex = 12;
        this.txtAuthenticationStatus.TabStop = false;
        // 
        // lblAuthenticationStatus
        // 
        this.lblAuthenticationStatus.Location = new System.Drawing.Point(8, 63);
        this.lblAuthenticationStatus.Name = "lblAuthenticationStatus";
        this.lblAuthenticationStatus.Size = new System.Drawing.Size(120, 23);
        this.lblAuthenticationStatus.TabIndex = 11;
        this.lblAuthenticationStatus.Text = "Authentication Status";
        // 
        // txtAuthenticationType
        // 
        this.txtAuthenticationType.Location = new System.Drawing.Point(144, 39);
        this.txtAuthenticationType.Name = "txtAuthenticationType";
        this.txtAuthenticationType.ReadOnly = true;
        this.txtAuthenticationType.Size = new System.Drawing.Size(120, 20);
        this.txtAuthenticationType.TabIndex = 10;
        this.txtAuthenticationType.TabStop = false;
        // 
        // lblAuthenticationType
        // 
        this.lblAuthenticationType.Location = new System.Drawing.Point(8, 39);
        this.lblAuthenticationType.Name = "lblAuthenticationType";
        this.lblAuthenticationType.Size = new System.Drawing.Size(120, 23);
        this.lblAuthenticationType.TabIndex = 9;
        this.lblAuthenticationType.Text = "Authentication Type";
        // 
        // txtReferenceNo
        // 
        this.txtReferenceNo.Location = new System.Drawing.Point(144, 15);
        this.txtReferenceNo.Name = "txtReferenceNo";
        this.txtReferenceNo.ReadOnly = true;
        this.txtReferenceNo.Size = new System.Drawing.Size(120, 20);
        this.txtReferenceNo.TabIndex = 8;
        this.txtReferenceNo.TabStop = false;
        // 
        // lblReferenceNo
        // 
        this.lblReferenceNo.Location = new System.Drawing.Point(8, 15);
        this.lblReferenceNo.Name = "lblReferenceNo";
        this.lblReferenceNo.Size = new System.Drawing.Size(120, 23);
        this.lblReferenceNo.TabIndex = 7;
        this.lblReferenceNo.Text = "Reference No";
        // 
        // grpAuthenticationResults
        // 
        this.grpAuthenticationResults.Controls.Add(this.textBox9);
        this.grpAuthenticationResults.Controls.Add(this.label17);
        this.grpAuthenticationResults.Controls.Add(this.txtAuthenticatedPerc);
        this.grpAuthenticationResults.Controls.Add(this.lblAuthenticatedPerc);
        this.grpAuthenticationResults.Location = new System.Drawing.Point(448, 257);
        this.grpAuthenticationResults.Name = "grpAuthenticationResults";
        this.grpAuthenticationResults.Size = new System.Drawing.Size(523, 56);
        this.grpAuthenticationResults.TabIndex = 10;
        this.grpAuthenticationResults.TabStop = false;
        this.grpAuthenticationResults.Text = "Authentication Results";
        // 
        // textBox9
        // 
        this.textBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.textBox9.Location = new System.Drawing.Point(413, 24);
        this.textBox9.Name = "textBox9";
        this.textBox9.ReadOnly = true;
        this.textBox9.Size = new System.Drawing.Size(64, 20);
        this.textBox9.TabIndex = 12;
        this.textBox9.TabStop = false;
        // 
        // label17
        // 
        this.label17.Location = new System.Drawing.Point(249, 24);
        this.label17.Name = "label17";
        this.label17.Size = new System.Drawing.Size(139, 23);
        this.label17.TabIndex = 11;
        this.label17.Text = "Minimum Authentication %";
        // 
        // txtAuthenticatedPerc
        // 
        this.txtAuthenticatedPerc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.txtAuthenticatedPerc.Location = new System.Drawing.Point(144, 24);
        this.txtAuthenticatedPerc.Name = "txtAuthenticatedPerc";
        this.txtAuthenticatedPerc.ReadOnly = true;
        this.txtAuthenticatedPerc.Size = new System.Drawing.Size(64, 20);
        this.txtAuthenticatedPerc.TabIndex = 10;
        this.txtAuthenticatedPerc.TabStop = false;
        // 
        // lblAuthenticatedPerc
        // 
        this.lblAuthenticatedPerc.Location = new System.Drawing.Point(8, 24);
        this.lblAuthenticatedPerc.Name = "lblAuthenticatedPerc";
        this.lblAuthenticatedPerc.Size = new System.Drawing.Size(120, 23);
        this.lblAuthenticatedPerc.TabIndex = 9;
        this.lblAuthenticatedPerc.Text = "Authenticated %";
        // 
        // btnAuthenticate
        // 
        this.btnAuthenticate.Enabled = false;
        this.btnAuthenticate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.btnAuthenticate.Location = new System.Drawing.Point(288, 676);
        this.btnAuthenticate.Name = "btnAuthenticate";
        this.btnAuthenticate.Size = new System.Drawing.Size(152, 30);
        this.btnAuthenticate.TabIndex = 5;
        this.btnAuthenticate.Text = "Authenticate";
        this.btnAuthenticate.Click += new System.EventHandler(this.btnAuthenticate_Click);
        // 
        // btnVoid
        // 
        this.btnVoid.Enabled = false;
        this.btnVoid.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.btnVoid.Location = new System.Drawing.Point(448, 676);
        this.btnVoid.Name = "btnVoid";
        this.btnVoid.Size = new System.Drawing.Size(152, 30);
        this.btnVoid.TabIndex = 11;
        this.btnVoid.Text = "Void";
        this.btnVoid.Click += new System.EventHandler(this.btnVoid_Click);
        // 
        // grpPersonalDetails
        // 
        this.grpPersonalDetails.Controls.Add(this.textBox7);
        this.grpPersonalDetails.Controls.Add(this.label11);
        this.grpPersonalDetails.Controls.Add(this.txtBirthDate);
        this.grpPersonalDetails.Controls.Add(this.label10);
        this.grpPersonalDetails.Controls.Add(this.textBox5);
        this.grpPersonalDetails.Controls.Add(this.label9);
        this.grpPersonalDetails.Controls.Add(this.txtSecondName);
        this.grpPersonalDetails.Controls.Add(this.txtFirstName);
        this.grpPersonalDetails.Controls.Add(this.txtResultIDNo);
        this.grpPersonalDetails.Controls.Add(this.txtSurname);
        this.grpPersonalDetails.Controls.Add(this.label4);
        this.grpPersonalDetails.Controls.Add(this.label3);
        this.grpPersonalDetails.Controls.Add(this.label2);
        this.grpPersonalDetails.Controls.Add(this.label1);
        this.grpPersonalDetails.Location = new System.Drawing.Point(8, 410);
        this.grpPersonalDetails.Name = "grpPersonalDetails";
        this.grpPersonalDetails.Size = new System.Drawing.Size(963, 128);
        this.grpPersonalDetails.TabIndex = 12;
        this.grpPersonalDetails.TabStop = false;
        this.grpPersonalDetails.Text = "Personal Details";
        // 
        // textBox7
        // 
        this.textBox7.Location = new System.Drawing.Point(808, 24);
        this.textBox7.Name = "textBox7";
        this.textBox7.ReadOnly = true;
        this.textBox7.Size = new System.Drawing.Size(109, 20);
        this.textBox7.TabIndex = 15;
        // 
        // label11
        // 
        this.label11.Location = new System.Drawing.Point(742, 24);
        this.label11.Name = "label11";
        this.label11.Size = new System.Drawing.Size(72, 23);
        this.label11.TabIndex = 14;
        this.label11.Text = "Gender";
        // 
        // txtBirthDate
        // 
        this.txtBirthDate.Location = new System.Drawing.Point(112, 89);
        this.txtBirthDate.Name = "txtBirthDate";
        this.txtBirthDate.ReadOnly = true;
        this.txtBirthDate.Size = new System.Drawing.Size(232, 20);
        this.txtBirthDate.TabIndex = 13;
        // 
        // label10
        // 
        this.label10.Location = new System.Drawing.Point(8, 86);
        this.label10.Name = "label10";
        this.label10.Size = new System.Drawing.Size(72, 23);
        this.label10.TabIndex = 12;
        this.label10.Text = "Date of Birth";
        // 
        // textBox5
        // 
        this.textBox5.Location = new System.Drawing.Point(464, 92);
        this.textBox5.Name = "textBox5";
        this.textBox5.ReadOnly = true;
        this.textBox5.Size = new System.Drawing.Size(232, 20);
        this.textBox5.TabIndex = 11;
        // 
        // label9
        // 
        this.label9.Location = new System.Drawing.Point(384, 92);
        this.label9.Name = "label9";
        this.label9.Size = new System.Drawing.Size(72, 23);
        this.label9.TabIndex = 10;
        this.label9.Text = "PassportNo";
        // 
        // txtSecondName
        // 
        this.txtSecondName.Location = new System.Drawing.Point(112, 56);
        this.txtSecondName.Name = "txtSecondName";
        this.txtSecondName.ReadOnly = true;
        this.txtSecondName.Size = new System.Drawing.Size(232, 20);
        this.txtSecondName.TabIndex = 9;
        // 
        // txtFirstName
        // 
        this.txtFirstName.Location = new System.Drawing.Point(112, 24);
        this.txtFirstName.Name = "txtFirstName";
        this.txtFirstName.ReadOnly = true;
        this.txtFirstName.Size = new System.Drawing.Size(232, 20);
        this.txtFirstName.TabIndex = 8;
        // 
        // txtResultIDNo
        // 
        this.txtResultIDNo.Location = new System.Drawing.Point(464, 56);
        this.txtResultIDNo.Name = "txtResultIDNo";
        this.txtResultIDNo.ReadOnly = true;
        this.txtResultIDNo.Size = new System.Drawing.Size(232, 20);
        this.txtResultIDNo.TabIndex = 7;
        // 
        // txtSurname
        // 
        this.txtSurname.Location = new System.Drawing.Point(464, 24);
        this.txtSurname.Name = "txtSurname";
        this.txtSurname.ReadOnly = true;
        this.txtSurname.Size = new System.Drawing.Size(232, 20);
        this.txtSurname.TabIndex = 6;
        // 
        // label4
        // 
        this.label4.Location = new System.Drawing.Point(8, 56);
        this.label4.Name = "label4";
        this.label4.Size = new System.Drawing.Size(96, 23);
        this.label4.TabIndex = 4;
        this.label4.Text = "Second Name";
        // 
        // label3
        // 
        this.label3.Location = new System.Drawing.Point(384, 56);
        this.label3.Name = "label3";
        this.label3.Size = new System.Drawing.Size(72, 23);
        this.label3.TabIndex = 3;
        this.label3.Text = "ID Number";
        // 
        // label2
        // 
        this.label2.Location = new System.Drawing.Point(384, 24);
        this.label2.Name = "label2";
        this.label2.Size = new System.Drawing.Size(72, 23);
        this.label2.TabIndex = 2;
        this.label2.Text = "Surname ";
        // 
        // label1
        // 
        this.label1.Location = new System.Drawing.Point(8, 24);
        this.label1.Name = "label1";
        this.label1.Size = new System.Drawing.Size(96, 23);
        this.label1.TabIndex = 1;
        this.label1.Text = "First Name";
        // 
        // groupBox1
        // 
        this.groupBox1.Controls.Add(this.label16);
        this.groupBox1.Controls.Add(this.txtHaDeceasedDate);
        this.groupBox1.Controls.Add(this.label14);
        this.groupBox1.Controls.Add(this.txtHADeceasedStatus);
        this.groupBox1.Controls.Add(this.label13);
        this.groupBox1.Controls.Add(this.txtHaBirthDate);
        this.groupBox1.Controls.Add(this.txtHaCauseOfDeath);
        this.groupBox1.Controls.Add(this.label12);
        this.groupBox1.Controls.Add(this.txtHASecondName);
        this.groupBox1.Controls.Add(this.txtHaFirstName);
        this.groupBox1.Controls.Add(this.txtHAIdNo);
        this.groupBox1.Controls.Add(this.txtHaSurname);
        this.groupBox1.Controls.Add(this.label5);
        this.groupBox1.Controls.Add(this.label6);
        this.groupBox1.Controls.Add(this.label7);
        this.groupBox1.Controls.Add(this.label8);
        this.groupBox1.Location = new System.Drawing.Point(8, 544);
        this.groupBox1.Name = "groupBox1";
        this.groupBox1.Size = new System.Drawing.Size(963, 125);
        this.groupBox1.TabIndex = 13;
        this.groupBox1.TabStop = false;
        this.groupBox1.Text = "Home Affairs Details";
        // 
        // label16
        // 
        this.label16.Location = new System.Drawing.Point(8, 85);
        this.label16.Name = "label16";
        this.label16.Size = new System.Drawing.Size(96, 23);
        this.label16.TabIndex = 23;
        this.label16.Text = "Cause of Death";
        // 
        // txtHaDeceasedDate
        // 
        this.txtHaDeceasedDate.Location = new System.Drawing.Point(834, 82);
        this.txtHaDeceasedDate.Name = "txtHaDeceasedDate";
        this.txtHaDeceasedDate.ReadOnly = true;
        this.txtHaDeceasedDate.Size = new System.Drawing.Size(109, 20);
        this.txtHaDeceasedDate.TabIndex = 21;
        // 
        // label14
        // 
        this.label14.Location = new System.Drawing.Point(730, 79);
        this.label14.Name = "label14";
        this.label14.Size = new System.Drawing.Size(98, 23);
        this.label14.TabIndex = 20;
        this.label14.Text = "Deceased Date";
        // 
        // txtHADeceasedStatus
        // 
        this.txtHADeceasedStatus.Location = new System.Drawing.Point(834, 51);
        this.txtHADeceasedStatus.Name = "txtHADeceasedStatus";
        this.txtHADeceasedStatus.ReadOnly = true;
        this.txtHADeceasedStatus.Size = new System.Drawing.Size(109, 20);
        this.txtHADeceasedStatus.TabIndex = 19;
        // 
        // label13
        // 
        this.label13.Location = new System.Drawing.Point(728, 51);
        this.label13.Name = "label13";
        this.label13.Size = new System.Drawing.Size(100, 23);
        this.label13.TabIndex = 18;
        this.label13.Text = "Deceased Status";
        // 
        // txtHaBirthDate
        // 
        this.txtHaBirthDate.Location = new System.Drawing.Point(834, 21);
        this.txtHaBirthDate.Name = "txtHaBirthDate";
        this.txtHaBirthDate.ReadOnly = true;
        this.txtHaBirthDate.Size = new System.Drawing.Size(109, 20);
        this.txtHaBirthDate.TabIndex = 17;
        // 
        // txtHaCauseOfDeath
        // 
        this.txtHaCauseOfDeath.Location = new System.Drawing.Point(112, 88);
        this.txtHaCauseOfDeath.Name = "txtHaCauseOfDeath";
        this.txtHaCauseOfDeath.ReadOnly = true;
        this.txtHaCauseOfDeath.Size = new System.Drawing.Size(232, 20);
        this.txtHaCauseOfDeath.TabIndex = 16;
        // 
        // label12
        // 
        this.label12.Location = new System.Drawing.Point(730, 24);
        this.label12.Name = "label12";
        this.label12.Size = new System.Drawing.Size(72, 23);
        this.label12.TabIndex = 15;
        this.label12.Text = "Date of Birth";
        // 
        // txtHASecondName
        // 
        this.txtHASecondName.Location = new System.Drawing.Point(112, 56);
        this.txtHASecondName.Name = "txtHASecondName";
        this.txtHASecondName.ReadOnly = true;
        this.txtHASecondName.Size = new System.Drawing.Size(232, 20);
        this.txtHASecondName.TabIndex = 9;
        // 
        // txtHaFirstName
        // 
        this.txtHaFirstName.Location = new System.Drawing.Point(112, 24);
        this.txtHaFirstName.Name = "txtHaFirstName";
        this.txtHaFirstName.ReadOnly = true;
        this.txtHaFirstName.Size = new System.Drawing.Size(232, 20);
        this.txtHaFirstName.TabIndex = 8;
        // 
        // txtHAIdNo
        // 
        this.txtHAIdNo.Location = new System.Drawing.Point(464, 56);
        this.txtHAIdNo.Name = "txtHAIdNo";
        this.txtHAIdNo.ReadOnly = true;
        this.txtHAIdNo.Size = new System.Drawing.Size(232, 20);
        this.txtHAIdNo.TabIndex = 7;
        // 
        // txtHaSurname
        // 
        this.txtHaSurname.Location = new System.Drawing.Point(464, 24);
        this.txtHaSurname.Name = "txtHaSurname";
        this.txtHaSurname.ReadOnly = true;
        this.txtHaSurname.Size = new System.Drawing.Size(232, 20);
        this.txtHaSurname.TabIndex = 6;
        // 
        // label5
        // 
        this.label5.Location = new System.Drawing.Point(8, 56);
        this.label5.Name = "label5";
        this.label5.Size = new System.Drawing.Size(96, 23);
        this.label5.TabIndex = 4;
        this.label5.Text = "Second Name";
        // 
        // label6
        // 
        this.label6.Location = new System.Drawing.Point(384, 56);
        this.label6.Name = "label6";
        this.label6.Size = new System.Drawing.Size(72, 23);
        this.label6.TabIndex = 3;
        this.label6.Text = "ID Number";
        // 
        // label7
        // 
        this.label7.Location = new System.Drawing.Point(384, 24);
        this.label7.Name = "label7";
        this.label7.Size = new System.Drawing.Size(72, 23);
        this.label7.TabIndex = 2;
        this.label7.Text = "Surname ";
        // 
        // label8
        // 
        this.label8.Location = new System.Drawing.Point(8, 24);
        this.label8.Name = "label8";
        this.label8.Size = new System.Drawing.Size(96, 23);
        this.label8.TabIndex = 1;
        this.label8.Text = "First Name";
        // 
        // groupBox2
        // 
        this.groupBox2.Controls.Add(this.txtAccountAgeDetails);
        this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.groupBox2.Location = new System.Drawing.Point(8, 318);
        this.groupBox2.Name = "groupBox2";
        this.groupBox2.Size = new System.Drawing.Size(963, 89);
        this.groupBox2.TabIndex = 14;
        this.groupBox2.TabStop = false;
        this.groupBox2.Text = "Consumer Account Age Details";
        // 
        // txtAccountAgeDetails
        // 
        this.txtAccountAgeDetails.BackColor = System.Drawing.Color.White;
        this.txtAccountAgeDetails.Location = new System.Drawing.Point(16, 19);
        this.txtAccountAgeDetails.Multiline = true;
        this.txtAccountAgeDetails.Name = "txtAccountAgeDetails";
        this.txtAccountAgeDetails.ReadOnly = true;
        this.txtAccountAgeDetails.Size = new System.Drawing.Size(914, 64);
        this.txtAccountAgeDetails.TabIndex = 9;
        // 
        // dataGridView1
        // 
        this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        this.dataGridView1.Location = new System.Drawing.Point(8, 53);
        this.dataGridView1.Name = "dataGridView1";
        this.dataGridView1.Size = new System.Drawing.Size(816, 68);
        this.dataGridView1.TabIndex = 17;
        // 
        // groupBox3
        // 
        this.groupBox3.Controls.Add(this.button1);
        this.groupBox3.Controls.Add(this.txtSubEnConsID);
        this.groupBox3.Controls.Add(this.label18);
        this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.groupBox3.Location = new System.Drawing.Point(446, 8);
        this.groupBox3.Name = "groupBox3";
        this.groupBox3.Size = new System.Drawing.Size(378, 42);
        this.groupBox3.TabIndex = 18;
        this.groupBox3.TabStop = false;
        this.groupBox3.Text = "Search Criteria";
        // 
        // button1
        // 
        this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.button1.Location = new System.Drawing.Point(300, 11);
        this.button1.Name = "button1";
        this.button1.Size = new System.Drawing.Size(64, 23);
        this.button1.TabIndex = 2;
        this.button1.Text = "Search";
        this.button1.Click += new System.EventHandler(this.button1_Click_1);
        // 
        // txtSubEnConsID
        // 
        this.txtSubEnConsID.Location = new System.Drawing.Point(81, 16);
        this.txtSubEnConsID.MaxLength = 13;
        this.txtSubEnConsID.Name = "txtSubEnConsID";
        this.txtSubEnConsID.Size = new System.Drawing.Size(88, 20);
        this.txtSubEnConsID.TabIndex = 0;
        // 
        // label18
        // 
        this.label18.Location = new System.Drawing.Point(9, 16);
        this.label18.Name = "label18";
        this.label18.Size = new System.Drawing.Size(72, 23);
        this.label18.TabIndex = 5;
        this.label18.Text = "SubEnquiryID";
        // 
        // AuthenticationTest1
        // 
        this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
        this.ClientSize = new System.Drawing.Size(990, 720);
        this.Controls.Add(this.groupBox3);
        this.Controls.Add(this.dataGridView1);
        this.Controls.Add(this.groupBox2);
        this.Controls.Add(this.groupBox1);
        this.Controls.Add(this.grpPersonalDetails);
        this.Controls.Add(this.btnVoid);
        this.Controls.Add(this.btnAuthenticate);
        this.Controls.Add(this.grpAuthenticationResults);
        this.Controls.Add(this.grpAuthenticationDetails);
        this.Controls.Add(this.grpSearchCriteria);
        this.Controls.Add(this.grpQuestions);
        this.Name = "AuthenticationTest1";
        this.Text = "XDS Consumer Authentication Black Box Test Application";
        this.grpQuestions.ResumeLayout(false);
        this.grpAnswers.ResumeLayout(false);
        this.grpSearchCriteria.ResumeLayout(false);
        this.grpSearchCriteria.PerformLayout();
        this.grpAuthenticationDetails.ResumeLayout(false);
        this.grpAuthenticationDetails.PerformLayout();
        this.grpAuthenticationResults.ResumeLayout(false);
        this.grpAuthenticationResults.PerformLayout();
        this.grpPersonalDetails.ResumeLayout(false);
        this.grpPersonalDetails.PerformLayout();
        this.groupBox1.ResumeLayout(false);
        this.groupBox1.PerformLayout();
        this.groupBox2.ResumeLayout(false);
        this.groupBox2.PerformLayout();
        ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
        this.groupBox3.ResumeLayout(false);
        this.groupBox3.PerformLayout();
        this.ResumeLayout(false);

		}
    #endregion

    private void btnSearch_Click(object sender, System.EventArgs e) {
      try {
        ClearInfo();

        //Declare Variables
        SqlConnection con = new SqlConnection(Properties.Settings.Default.CreditEnquiryDB);
        SqlConnection Acon = new SqlConnection(Properties.Settings.Default.AdminDB);
        XDSPortalAuthentication.AuthenticationConsumer dt = new XDSPortalAuthentication.AuthenticationConsumer();
        XDSPortalEnquiry.Data.SubscriberEnquiry dse = new XDSPortalEnquiry.Data.SubscriberEnquiry();
        XDSPortalEnquiry.Entity.SubscriberEnquiry se = new XDSPortalEnquiry.Entity.SubscriberEnquiry();
        XDSPortalEnquiry.Data.SubscriberEnquiryResult dsec = new XDSPortalEnquiry.Data.SubscriberEnquiryResult();
        XDSPortalEnquiry.Data.SubscriberEnquiryResultBonus dsecb = new XDSPortalEnquiry.Data.SubscriberEnquiryResultBonus();

        //Submit enquiry and get SubscriberEnquiryID

        XDSPortalLibrary.Entity_Layer.Response rp = dt.SubmitConsumerTrace(con, Acon, 6161, 12856, 2, "John Lombela", AuthenticationSearchType.ConsumerIdentity, "XDS DEVELOPMENT", 4, txtIDNo.Text, txtPassportNo.Text, "", "", "", "", false, "", "pgummala@xds.co.za", "", true, "");

        //Get the Subscriber Enquiry Record Details
        se = dse.GetSubscriberEnquiryObject(con, rp.EnquiryID);

        DataSet multipleset = dsec.GetSubscriberEnquiryResultDataSet(con, rp.EnquiryID);
        dataGridView1.DataSource = multipleset.Tables[0];
        
      }
      catch (Exception oException) {
        MessageBox.Show(oException.Message);
      }
    }

    private void btnAuthenticate_Click(object sender, System.EventArgs e) {
      try {
        
        
        //moMyAuthenticationManager.Authenticate();
        moMyAuthenticationManager.Authenticate("");


        txtAuthenticationStatus.Text = moMyAuthenticationManager.AuthenticationStatus;
        txtAuthenticatedPerc.Text = moMyAuthenticationManager.AuthenticatedPerc.ToString();
        txtRepeatAuthenticationMessage.Text = moMyAuthenticationManager.AuthenticationComment.ToString();
         
        lstQuestions.Enabled = false;
        cboAnswer.Enabled = false;
        btnAuthenticate.Enabled = false;
        btnVoid.Enabled = false;
      }
      catch (Exception oException) {
        MessageBox.Show(oException.Message);
      }
    }

    private void btnVoid_Click(object sender, System.EventArgs e) {
      try {
        moMyAuthenticationManager.VoidAuthentication();

        txtAuthenticationStatus.Text = moMyAuthenticationManager.AuthenticationStatus;
        txtAuthenticatedPerc.Text = moMyAuthenticationManager.AuthenticatedPerc.ToString();
         
        lstQuestions.Enabled = false;
        cboAnswer.Enabled = false;
        btnAuthenticate.Enabled = false;
        btnVoid.Enabled = false;
      }
      catch (Exception oException) {
        MessageBox.Show(oException.Message);
      }
    }
  
    private void lstQuestions_SelectedIndexChanged(object sender, System.EventArgs e) {
      try {
        if (lstQuestions.SelectedItems.Count > 0) {
          long lQuestionID = System.Convert.ToInt64(lstQuestions.SelectedItems[0].Tag);
          PopulateAnswerDropDown(moMyAuthenticationManager.Question(lQuestionID));
        }
      }
      catch (Exception oException) {
        MessageBox.Show(oException.Message);
      }
    }

    private void cboAnswer_SelectedIndexChanged(object sender, System.EventArgs e) {
      if (lstQuestions.SelectedItems.Count > 0 && cboAnswer.SelectedItem!=null) {
        long lQuestionID = System.Convert.ToInt64(lstQuestions.SelectedItems[0].Tag);
          
        QuestionStructure oQuestionStructure = moMyAuthenticationManager.Question(lQuestionID);
        foreach (AnswerStructure oAnswerStructure in oQuestionStructure.Answers)
        {
            oAnswerStructure.IsEnteredAnswer = false;
            if (oAnswerStructure.Answer == cboAnswer.SelectedItem.ToString())
            {
                oAnswerStructure.IsEnteredAnswer = true;
            }

          
          AnswerStructure oAnswerS = oQuestionStructure.Answers[0];
          oAnswerS.IsEnteredAnswer = true;

        }
      }
    }

    private void ClearInfo() {

      txtAccountAgeDetails.Text = "";
      lstQuestions.Items.Clear();
      cboAnswer.Items.Clear();
      txtReferenceNo.Text = "";
      txtAuthenticationType.Text = "";
      txtAuthenticationStatus.Text = "";
      txtRepeatAuthenticationMessage.Text = "";
      txtAuthenticatedPerc.Text = "";

      lstQuestions.Enabled = false;
      cboAnswer.Enabled = false;
      btnAuthenticate.Enabled = false;
      btnVoid.Enabled = false;
    }
    
    private void PopulateAnswerDropDown(QuestionStructure QuestionStructure) {
      cboAnswer.Items.Clear();
      cboAnswer.Items.Add("");
      foreach (AnswerStructure oAnswerStructure in QuestionStructure.Answers) {
        cboAnswer.Items.Add(oAnswerStructure.Answer);
        if (oAnswerStructure.IsEnteredAnswer) {
          cboAnswer.SelectedItem = oAnswerStructure.Answer;
        }
      }
    }

    private void button1_Click(object sender, EventArgs e)
    {
        
    }

    private void button1_Click_1(object sender, EventArgs e)
    {
        ClearInfo();

        //Declare Variables
        string con = Properties.Settings.Default.CreditEnquiryDB;
        string Acon = Properties.Settings.Default.AdminDB;
        //string XMLData = "";
        DataSet ds = new DataSet();
        XDSPortalEnquiry.Business.MultipleTrace_ConsumerTrace dt = new XDSPortalEnquiry.Business.MultipleTrace_ConsumerTrace();
        XDSPortalEnquiry.Data.SubscriberEnquiry dse = new XDSPortalEnquiry.Data.SubscriberEnquiry();
        XDSPortalEnquiry.Entity.SubscriberEnquiry se = new XDSPortalEnquiry.Entity.SubscriberEnquiry();
        XDSPortalEnquiry.Data.SubscriberEnquiryResult dsec = new XDSPortalEnquiry.Data.SubscriberEnquiryResult();
        XDSPortalEnquiry.Data.SubscriberEnquiryResultBonus dsecb = new XDSPortalEnquiry.Data.SubscriberEnquiryResultBonus();

        //Get the Subscriber Enquiry Record Details
        int intSubscriberEnquiryResultID = int.Parse(txtSubEnConsID.Text);
        XDSPortalEnquiry.Entity.SubscriberEnquiryResult sec = dsec.GetSubscriberEnquiryResultObject(con, intSubscriberEnquiryResultID);

        //Get the Subscriber Enquiry Record Details
        se = dse.GetSubscriberEnquiryObject(con, sec.SubscriberEnquiryID);


        moMyAuthenticationManager = new AuthenticationProcess("xds", se.BranchCode, se.PurposeID,Acon,con);


        moMyAuthenticationManager.NewAuthentication(se, sec);
        
        txtReferenceNo.Text = moMyAuthenticationManager.ReferenceNo;
        txtAuthenticationType.Text = moMyAuthenticationManager.AuthenticationType;
        txtAuthenticationStatus.Text = moMyAuthenticationManager.AuthenticationStatus;
        txtRepeatAuthenticationMessage.Text = moMyAuthenticationManager.RepeatAuthenticationMessage;

        txtFirstName.Text = moMyAuthenticationManager.FirstName;
        txtSecondName.Text = moMyAuthenticationManager.SecondName;
        txtSurname.Text = moMyAuthenticationManager.Surname;
        txtResultIDNo.Text = moMyAuthenticationManager.IDNo;
        txtBirthDate.Text = moMyAuthenticationManager.BirthDate;

        txtHaFirstName.Text = moMyAuthenticationManager.Ha_FirstName;
        txtHASecondName.Text = moMyAuthenticationManager.Ha_SecondName;
        txtHaSurname.Text = moMyAuthenticationManager.Ha_Surname;
        txtHAIdNo.Text = moMyAuthenticationManager.Ha_IDNo;
        txtHADeceasedStatus.Text = moMyAuthenticationManager.Ha_DeaseasedStatus;
        txtHaDeceasedDate.Text = moMyAuthenticationManager.Ha_DeaseasedDate;
        txtHaCauseOfDeath.Text = moMyAuthenticationManager.Ha_CauseOfDeath;


        txtAccountAgeDetails.Text = moMyAuthenticationManager.ConsumerAccountAgeMessage;

        foreach (QuestionStructure oQuestionStructure in moMyAuthenticationManager.Questions)
        {
            ListViewItem oListViewItem = new ListViewItem(oQuestionStructure.Question);
            oListViewItem.Tag = oQuestionStructure.QuestionID;
            lstQuestions.Items.Add(oListViewItem);
        }

        lstQuestions.Enabled = true;
        cboAnswer.Enabled = true;
        btnAuthenticate.Enabled = true;
        btnVoid.Enabled = true;
    }
  }
}
