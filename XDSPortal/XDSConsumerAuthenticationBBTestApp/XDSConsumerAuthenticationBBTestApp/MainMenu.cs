using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace XDSConsumerAuthenticationBBTestApp {
  /// <summary>
  /// Summary description for Form1.
  /// </summary>
  public class MainMenu : System.Windows.Forms.Form {
    private System.Windows.Forms.Button btnAuthenticationTest2;
    private System.Windows.Forms.Button btnAuthenticationTest1;
    private System.Windows.Forms.Button btnConfigurationSetup;
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.Container components = null;

    public MainMenu() {
      //
      // Required for Windows Form Designer support
      //
      InitializeComponent();

      //
      // TODO: Add any constructor code after InitializeComponent call
      //
    }

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    protected override void Dispose( bool disposing ) {
      if( disposing ) {
        if(components != null) {
          components.Dispose();
        }
      }
      base.Dispose( disposing );
    }

    #region Windows Form Designer generated code
    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
      this.btnAuthenticationTest2 = new System.Windows.Forms.Button();
      this.btnAuthenticationTest1 = new System.Windows.Forms.Button();
      this.btnConfigurationSetup = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // btnAuthenticationTest2
      // 
      this.btnAuthenticationTest2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
      this.btnAuthenticationTest2.Location = new System.Drawing.Point(232, 8);
      this.btnAuthenticationTest2.Name = "btnAuthenticationTest2";
      this.btnAuthenticationTest2.Size = new System.Drawing.Size(208, 32);
      this.btnAuthenticationTest2.TabIndex = 14;
      this.btnAuthenticationTest2.Text = "Single Question Test";
      this.btnAuthenticationTest2.Click += new System.EventHandler(this.btnAuthenticationTest2_Click);
      // 
      // btnAuthenticationTest1
      // 
      this.btnAuthenticationTest1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
      this.btnAuthenticationTest1.Location = new System.Drawing.Point(8, 8);
      this.btnAuthenticationTest1.Name = "btnAuthenticationTest1";
      this.btnAuthenticationTest1.Size = new System.Drawing.Size(208, 32);
      this.btnAuthenticationTest1.TabIndex = 16;
      this.btnAuthenticationTest1.Text = "Multiple Question Test";
      this.btnAuthenticationTest1.Click += new System.EventHandler(this.btnAuthenticationTest1_Click);
      // 
      // btnConfigurationSetup
      // 
      this.btnConfigurationSetup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
      this.btnConfigurationSetup.Location = new System.Drawing.Point(344, 72);
      this.btnConfigurationSetup.Name = "btnConfigurationSetup";
      this.btnConfigurationSetup.Size = new System.Drawing.Size(96, 32);
      this.btnConfigurationSetup.TabIndex = 17;
      this.btnConfigurationSetup.Text = "Configuration Setup";
      this.btnConfigurationSetup.Click += new System.EventHandler(this.btnConfigurationSetup_Click);
      // 
      // MainMenu
      // 
      this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
      this.ClientSize = new System.Drawing.Size(448, 108);
      this.Controls.Add(this.btnConfigurationSetup);
      this.Controls.Add(this.btnAuthenticationTest1);
      this.Controls.Add(this.btnAuthenticationTest2);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
      this.Name = "MainMenu";
      this.Text = "XDS Consumer Authentication Black Box Test Application";
      this.ResumeLayout(false);

    }
    #endregion

    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main() {
      Application.Run(new MainMenu());
    }

    private void btnAuthenticationTest1_Click(object sender, System.EventArgs e) {
      AuthenticationTest1 oAuthenticationTest1 = new AuthenticationTest1();
      
      oAuthenticationTest1.Show();
    }

    private void btnAuthenticationTest2_Click(object sender, System.EventArgs e) {
      AuthenticationTest2 oAuthenticationTest2 = new AuthenticationTest2();
      
      oAuthenticationTest2.Show();
    }

    private void btnConfigurationSetup_Click(object sender, System.EventArgs e) {
      
    }
  }
}
