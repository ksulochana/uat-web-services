using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using XDSPortalAuthentication;

namespace XDSConsumerAuthenticationBBTestApp {
  public class AuthenticationTest2 : System.Windows.Forms.Form {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.Container components = null;
    private System.Windows.Forms.GroupBox grpAnswers;
    private System.Windows.Forms.ComboBox cboAnswer;
    private System.Windows.Forms.GroupBox grpSearchCriteria;
    private System.Windows.Forms.TextBox txtIDNo;
    private System.Windows.Forms.Label lblIDNo;
    private System.Windows.Forms.Button btnSearch;
    private System.Windows.Forms.GroupBox grpAuthenticationDetails;
    private System.Windows.Forms.TextBox txtReferenceNo;
    private System.Windows.Forms.Label lblReferenceNo;
    private System.Windows.Forms.TextBox txtAuthenticationType;
    private System.Windows.Forms.Label lblAuthenticationType;
    private System.Windows.Forms.TextBox txtAuthenticationStatus;
    private System.Windows.Forms.Label lblAuthenticationStatus;
    private System.Windows.Forms.GroupBox grpAuthenticationResults;
    private System.Windows.Forms.Label lblRepeatAuthenticationMessage;
    private System.Windows.Forms.TextBox txtAuthenticatedPerc;
    private System.Windows.Forms.Label lblAuthenticatedPerc;
    private System.Windows.Forms.Button btnAuthenticate;
    private System.Windows.Forms.TextBox txtRepeatAuthenticationMessage;
    private System.Windows.Forms.TextBox txtPassportNo;
    private System.Windows.Forms.Label lblPassportNo;
    private System.Windows.Forms.Button btnVoid;
    private System.Windows.Forms.GroupBox grpQuestion;
    private System.Windows.Forms.TextBox txtQuestion;
    private System.Windows.Forms.Button btnNextQuestion;
    private System.Windows.Forms.Button btnPreviousQuestion;

    // Use Standard Authentication Manager Object
    private AuthenticationProcess moAuthenticationManager;
    
    public AuthenticationTest2() {
      //
      // Required for Windows Form Designer support
      //
      InitializeComponent();

      ClearInfo();
      
    }

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    protected override void Dispose( bool disposing ) {
      if( disposing ) {
        if (components != null) {
          components.Dispose();
        }
      }
      base.Dispose( disposing );
    }

    #region Windows Form Designer generated code
    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
      this.grpQuestion = new System.Windows.Forms.GroupBox();
      this.txtQuestion = new System.Windows.Forms.TextBox();
      this.grpAnswers = new System.Windows.Forms.GroupBox();
      this.cboAnswer = new System.Windows.Forms.ComboBox();
      this.grpSearchCriteria = new System.Windows.Forms.GroupBox();
      this.txtPassportNo = new System.Windows.Forms.TextBox();
      this.lblPassportNo = new System.Windows.Forms.Label();
      this.btnSearch = new System.Windows.Forms.Button();
      this.txtIDNo = new System.Windows.Forms.TextBox();
      this.lblIDNo = new System.Windows.Forms.Label();
      this.grpAuthenticationDetails = new System.Windows.Forms.GroupBox();
      this.txtRepeatAuthenticationMessage = new System.Windows.Forms.TextBox();
      this.lblRepeatAuthenticationMessage = new System.Windows.Forms.Label();
      this.txtAuthenticationStatus = new System.Windows.Forms.TextBox();
      this.lblAuthenticationStatus = new System.Windows.Forms.Label();
      this.txtAuthenticationType = new System.Windows.Forms.TextBox();
      this.lblAuthenticationType = new System.Windows.Forms.Label();
      this.txtReferenceNo = new System.Windows.Forms.TextBox();
      this.lblReferenceNo = new System.Windows.Forms.Label();
      this.grpAuthenticationResults = new System.Windows.Forms.GroupBox();
      this.txtAuthenticatedPerc = new System.Windows.Forms.TextBox();
      this.lblAuthenticatedPerc = new System.Windows.Forms.Label();
      this.btnAuthenticate = new System.Windows.Forms.Button();
      this.btnVoid = new System.Windows.Forms.Button();
      this.btnNextQuestion = new System.Windows.Forms.Button();
      this.btnPreviousQuestion = new System.Windows.Forms.Button();
      this.grpQuestion.SuspendLayout();
      this.grpAnswers.SuspendLayout();
      this.grpSearchCriteria.SuspendLayout();
      this.grpAuthenticationDetails.SuspendLayout();
      this.grpAuthenticationResults.SuspendLayout();
      this.SuspendLayout();
      // 
      // grpQuestion
      // 
      this.grpQuestion.Controls.Add(this.txtQuestion);
      this.grpQuestion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
      this.grpQuestion.Location = new System.Drawing.Point(8, 72);
      this.grpQuestion.Name = "grpQuestion";
      this.grpQuestion.Size = new System.Drawing.Size(432, 48);
      this.grpQuestion.TabIndex = 6;
      this.grpQuestion.TabStop = false;
      this.grpQuestion.Text = "Question";
      // 
      // txtQuestion
      // 
      this.txtQuestion.Location = new System.Drawing.Point(8, 16);
      this.txtQuestion.MaxLength = 13;
      this.txtQuestion.Name = "txtQuestion";
      this.txtQuestion.ReadOnly = true;
      this.txtQuestion.Size = new System.Drawing.Size(416, 20);
      this.txtQuestion.TabIndex = 2;
      this.txtQuestion.Text = "";
      // 
      // grpAnswers
      // 
      this.grpAnswers.Controls.Add(this.cboAnswer);
      this.grpAnswers.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
      this.grpAnswers.Location = new System.Drawing.Point(8, 128);
      this.grpAnswers.Name = "grpAnswers";
      this.grpAnswers.Size = new System.Drawing.Size(432, 48);
      this.grpAnswers.TabIndex = 7;
      this.grpAnswers.TabStop = false;
      this.grpAnswers.Text = "Answers";
      // 
      // cboAnswer
      // 
      this.cboAnswer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cboAnswer.Location = new System.Drawing.Point(8, 16);
      this.cboAnswer.Name = "cboAnswer";
      this.cboAnswer.Size = new System.Drawing.Size(416, 21);
      this.cboAnswer.TabIndex = 4;
      this.cboAnswer.SelectedIndexChanged += new System.EventHandler(this.cboAnswer_SelectedIndexChanged);
      // 
      // grpSearchCriteria
      // 
      this.grpSearchCriteria.Controls.Add(this.txtPassportNo);
      this.grpSearchCriteria.Controls.Add(this.lblPassportNo);
      this.grpSearchCriteria.Controls.Add(this.btnSearch);
      this.grpSearchCriteria.Controls.Add(this.txtIDNo);
      this.grpSearchCriteria.Controls.Add(this.lblIDNo);
      this.grpSearchCriteria.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
      this.grpSearchCriteria.Location = new System.Drawing.Point(8, 8);
      this.grpSearchCriteria.Name = "grpSearchCriteria";
      this.grpSearchCriteria.Size = new System.Drawing.Size(432, 56);
      this.grpSearchCriteria.TabIndex = 8;
      this.grpSearchCriteria.TabStop = false;
      this.grpSearchCriteria.Text = "Search Criteria";
      // 
      // txtPassportNo
      // 
      this.txtPassportNo.Location = new System.Drawing.Point(256, 24);
      this.txtPassportNo.MaxLength = 13;
      this.txtPassportNo.Name = "txtPassportNo";
      this.txtPassportNo.Size = new System.Drawing.Size(88, 20);
      this.txtPassportNo.TabIndex = 1;
      this.txtPassportNo.Text = "";
      // 
      // lblPassportNo
      // 
      this.lblPassportNo.Location = new System.Drawing.Point(184, 24);
      this.lblPassportNo.Name = "lblPassportNo";
      this.lblPassportNo.Size = new System.Drawing.Size(72, 23);
      this.lblPassportNo.TabIndex = 7;
      this.lblPassportNo.Text = "Passport No";
      // 
      // btnSearch
      // 
      this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
      this.btnSearch.Location = new System.Drawing.Point(360, 24);
      this.btnSearch.Name = "btnSearch";
      this.btnSearch.Size = new System.Drawing.Size(64, 23);
      this.btnSearch.TabIndex = 2;
      this.btnSearch.Text = "Search";
      this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
      // 
      // txtIDNo
      // 
      this.txtIDNo.Location = new System.Drawing.Point(80, 24);
      this.txtIDNo.MaxLength = 13;
      this.txtIDNo.Name = "txtIDNo";
      this.txtIDNo.Size = new System.Drawing.Size(88, 20);
      this.txtIDNo.TabIndex = 0;
      this.txtIDNo.Text = "";
      // 
      // lblIDNo
      // 
      this.lblIDNo.Location = new System.Drawing.Point(8, 24);
      this.lblIDNo.Name = "lblIDNo";
      this.lblIDNo.Size = new System.Drawing.Size(72, 23);
      this.lblIDNo.TabIndex = 5;
      this.lblIDNo.Text = "ID No";
      // 
      // grpAuthenticationDetails
      // 
      this.grpAuthenticationDetails.Controls.Add(this.txtRepeatAuthenticationMessage);
      this.grpAuthenticationDetails.Controls.Add(this.lblRepeatAuthenticationMessage);
      this.grpAuthenticationDetails.Controls.Add(this.txtAuthenticationStatus);
      this.grpAuthenticationDetails.Controls.Add(this.lblAuthenticationStatus);
      this.grpAuthenticationDetails.Controls.Add(this.txtAuthenticationType);
      this.grpAuthenticationDetails.Controls.Add(this.lblAuthenticationType);
      this.grpAuthenticationDetails.Controls.Add(this.txtReferenceNo);
      this.grpAuthenticationDetails.Controls.Add(this.lblReferenceNo);
      this.grpAuthenticationDetails.Location = new System.Drawing.Point(448, 8);
      this.grpAuthenticationDetails.Name = "grpAuthenticationDetails";
      this.grpAuthenticationDetails.Size = new System.Drawing.Size(400, 208);
      this.grpAuthenticationDetails.TabIndex = 9;
      this.grpAuthenticationDetails.TabStop = false;
      this.grpAuthenticationDetails.Text = "Authentication Details";
      // 
      // txtRepeatAuthenticationMessage
      // 
      this.txtRepeatAuthenticationMessage.Location = new System.Drawing.Point(144, 96);
      this.txtRepeatAuthenticationMessage.Multiline = true;
      this.txtRepeatAuthenticationMessage.Name = "txtRepeatAuthenticationMessage";
      this.txtRepeatAuthenticationMessage.ReadOnly = true;
      this.txtRepeatAuthenticationMessage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.txtRepeatAuthenticationMessage.Size = new System.Drawing.Size(248, 104);
      this.txtRepeatAuthenticationMessage.TabIndex = 14;
      this.txtRepeatAuthenticationMessage.TabStop = false;
      this.txtRepeatAuthenticationMessage.Text = "";
      // 
      // lblRepeatAuthenticationMessage
      // 
      this.lblRepeatAuthenticationMessage.Location = new System.Drawing.Point(8, 96);
      this.lblRepeatAuthenticationMessage.Name = "lblRepeatAuthenticationMessage";
      this.lblRepeatAuthenticationMessage.Size = new System.Drawing.Size(120, 40);
      this.lblRepeatAuthenticationMessage.TabIndex = 13;
      this.lblRepeatAuthenticationMessage.Text = "Repeat Authentication Message";
      // 
      // txtAuthenticationStatus
      // 
      this.txtAuthenticationStatus.Location = new System.Drawing.Point(144, 72);
      this.txtAuthenticationStatus.Name = "txtAuthenticationStatus";
      this.txtAuthenticationStatus.ReadOnly = true;
      this.txtAuthenticationStatus.Size = new System.Drawing.Size(120, 20);
      this.txtAuthenticationStatus.TabIndex = 12;
      this.txtAuthenticationStatus.TabStop = false;
      this.txtAuthenticationStatus.Text = "";
      // 
      // lblAuthenticationStatus
      // 
      this.lblAuthenticationStatus.Location = new System.Drawing.Point(8, 72);
      this.lblAuthenticationStatus.Name = "lblAuthenticationStatus";
      this.lblAuthenticationStatus.Size = new System.Drawing.Size(120, 23);
      this.lblAuthenticationStatus.TabIndex = 11;
      this.lblAuthenticationStatus.Text = "Authentication Status";
      // 
      // txtAuthenticationType
      // 
      this.txtAuthenticationType.Location = new System.Drawing.Point(144, 48);
      this.txtAuthenticationType.Name = "txtAuthenticationType";
      this.txtAuthenticationType.ReadOnly = true;
      this.txtAuthenticationType.Size = new System.Drawing.Size(120, 20);
      this.txtAuthenticationType.TabIndex = 10;
      this.txtAuthenticationType.TabStop = false;
      this.txtAuthenticationType.Text = "";
      // 
      // lblAuthenticationType
      // 
      this.lblAuthenticationType.Location = new System.Drawing.Point(8, 48);
      this.lblAuthenticationType.Name = "lblAuthenticationType";
      this.lblAuthenticationType.Size = new System.Drawing.Size(120, 23);
      this.lblAuthenticationType.TabIndex = 9;
      this.lblAuthenticationType.Text = "Authentication Type";
      // 
      // txtReferenceNo
      // 
      this.txtReferenceNo.Location = new System.Drawing.Point(144, 24);
      this.txtReferenceNo.Name = "txtReferenceNo";
      this.txtReferenceNo.ReadOnly = true;
      this.txtReferenceNo.Size = new System.Drawing.Size(120, 20);
      this.txtReferenceNo.TabIndex = 8;
      this.txtReferenceNo.TabStop = false;
      this.txtReferenceNo.Text = "";
      // 
      // lblReferenceNo
      // 
      this.lblReferenceNo.Location = new System.Drawing.Point(8, 24);
      this.lblReferenceNo.Name = "lblReferenceNo";
      this.lblReferenceNo.Size = new System.Drawing.Size(120, 23);
      this.lblReferenceNo.TabIndex = 7;
      this.lblReferenceNo.Text = "Reference No";
      // 
      // grpAuthenticationResults
      // 
      this.grpAuthenticationResults.Controls.Add(this.txtAuthenticatedPerc);
      this.grpAuthenticationResults.Controls.Add(this.lblAuthenticatedPerc);
      this.grpAuthenticationResults.Location = new System.Drawing.Point(448, 224);
      this.grpAuthenticationResults.Name = "grpAuthenticationResults";
      this.grpAuthenticationResults.Size = new System.Drawing.Size(400, 56);
      this.grpAuthenticationResults.TabIndex = 10;
      this.grpAuthenticationResults.TabStop = false;
      this.grpAuthenticationResults.Text = "Authentication Results";
      // 
      // txtAuthenticatedPerc
      // 
      this.txtAuthenticatedPerc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
      this.txtAuthenticatedPerc.Location = new System.Drawing.Point(144, 24);
      this.txtAuthenticatedPerc.Name = "txtAuthenticatedPerc";
      this.txtAuthenticatedPerc.ReadOnly = true;
      this.txtAuthenticatedPerc.Size = new System.Drawing.Size(64, 20);
      this.txtAuthenticatedPerc.TabIndex = 10;
      this.txtAuthenticatedPerc.TabStop = false;
      this.txtAuthenticatedPerc.Text = "";
      // 
      // lblAuthenticatedPerc
      // 
      this.lblAuthenticatedPerc.Location = new System.Drawing.Point(8, 24);
      this.lblAuthenticatedPerc.Name = "lblAuthenticatedPerc";
      this.lblAuthenticatedPerc.Size = new System.Drawing.Size(120, 23);
      this.lblAuthenticatedPerc.TabIndex = 9;
      this.lblAuthenticatedPerc.Text = "Authenticated %";
      // 
      // btnAuthenticate
      // 
      this.btnAuthenticate.Enabled = false;
      this.btnAuthenticate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
      this.btnAuthenticate.Location = new System.Drawing.Point(272, 288);
      this.btnAuthenticate.Name = "btnAuthenticate";
      this.btnAuthenticate.Size = new System.Drawing.Size(152, 32);
      this.btnAuthenticate.TabIndex = 5;
      this.btnAuthenticate.Text = "Authenticate";
      this.btnAuthenticate.Click += new System.EventHandler(this.btnAuthenticate_Click);
      // 
      // btnVoid
      // 
      this.btnVoid.Enabled = false;
      this.btnVoid.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
      this.btnVoid.Location = new System.Drawing.Point(432, 288);
      this.btnVoid.Name = "btnVoid";
      this.btnVoid.Size = new System.Drawing.Size(152, 32);
      this.btnVoid.TabIndex = 11;
      this.btnVoid.Text = "Void";
      this.btnVoid.Click += new System.EventHandler(this.btnVoid_Click);
      // 
      // btnNextQuestion
      // 
      this.btnNextQuestion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
      this.btnNextQuestion.Location = new System.Drawing.Point(232, 184);
      this.btnNextQuestion.Name = "btnNextQuestion";
      this.btnNextQuestion.Size = new System.Drawing.Size(120, 23);
      this.btnNextQuestion.TabIndex = 13;
      this.btnNextQuestion.Text = "Next Question";
      this.btnNextQuestion.Click += new System.EventHandler(this.btnNextQuestion_Click);
      // 
      // btnPreviousQuestion
      // 
      this.btnPreviousQuestion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
      this.btnPreviousQuestion.Location = new System.Drawing.Point(96, 184);
      this.btnPreviousQuestion.Name = "btnPreviousQuestion";
      this.btnPreviousQuestion.Size = new System.Drawing.Size(120, 23);
      this.btnPreviousQuestion.TabIndex = 14;
      this.btnPreviousQuestion.Text = "Previous Question";
      this.btnPreviousQuestion.Click += new System.EventHandler(this.btnPreviousQuestion_Click);
      // 
      // AuthenticationTest2
      // 
      this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
      this.ClientSize = new System.Drawing.Size(856, 326);
      this.Controls.Add(this.btnPreviousQuestion);
      this.Controls.Add(this.btnNextQuestion);
      this.Controls.Add(this.btnVoid);
      this.Controls.Add(this.btnAuthenticate);
      this.Controls.Add(this.grpAuthenticationResults);
      this.Controls.Add(this.grpAuthenticationDetails);
      this.Controls.Add(this.grpSearchCriteria);
      this.Controls.Add(this.grpAnswers);
      this.Controls.Add(this.grpQuestion);
      this.Name = "AuthenticationTest2";
      this.Text = "XDS Consumer Authentication Black Box Test Application";
      this.grpQuestion.ResumeLayout(false);
      this.grpAnswers.ResumeLayout(false);
      this.grpSearchCriteria.ResumeLayout(false);
      this.grpAuthenticationDetails.ResumeLayout(false);
      this.grpAuthenticationResults.ResumeLayout(false);
      this.ResumeLayout(false);

    }
    #endregion

    private void btnSearch_Click(object sender, System.EventArgs e) {
      try {
        ClearInfo();
        moAuthenticationManager = new AuthenticationProcess("Ruddy", "XDS DEVELOPMENT", 4,"","");
        moAuthenticationManager.NewAuthentication(txtIDNo.Text, txtPassportNo.Text, "", "", "", "","","");

        txtReferenceNo.Text = moAuthenticationManager.ReferenceNo;
        txtAuthenticationType.Text = moAuthenticationManager.AuthenticationType;
        txtAuthenticationStatus.Text = moAuthenticationManager.AuthenticationStatus;
        txtRepeatAuthenticationMessage.Text = moAuthenticationManager.RepeatAuthenticationMessage;

        QuestionStructure oQuestionStructure = moAuthenticationManager.CurrentQuestion;

        txtQuestion.Text = oQuestionStructure.Question;
        PopulateAnswerDropDown(oQuestionStructure);

        cboAnswer.Enabled = true;
        btnAuthenticate.Enabled = true;
        btnVoid.Enabled = true;
        btnPreviousQuestion.Enabled = true;
        btnNextQuestion.Enabled = true;
      }
      catch (Exception oException) {
        MessageBox.Show(oException.Message);
      }
    }

    private void btnPreviousQuestion_Click(object sender, System.EventArgs e) {
      QuestionStructure oQuestionStructure = moAuthenticationManager.PreviousQuestion;

      if (oQuestionStructure==null) {
        oQuestionStructure = moAuthenticationManager.LastQuestion;
      }
      txtQuestion.Text = oQuestionStructure.Question;
      PopulateAnswerDropDown(oQuestionStructure);
    }

    private void btnNextQuestion_Click(object sender, System.EventArgs e) {
      QuestionStructure oQuestionStructure = moAuthenticationManager.NextQuestion;

      if (oQuestionStructure==null) {
        oQuestionStructure = moAuthenticationManager.FirstQuestion;
      }
      txtQuestion.Text = oQuestionStructure.Question;
      PopulateAnswerDropDown(oQuestionStructure);
    }

    private void btnAuthenticate_Click(object sender, System.EventArgs e) {
      try {
        moAuthenticationManager.Authenticate();

        txtAuthenticationStatus.Text = moAuthenticationManager.AuthenticationStatus;
        txtAuthenticatedPerc.Text = moAuthenticationManager.AuthenticatedPerc.ToString();

        cboAnswer.Enabled = false;
        btnAuthenticate.Enabled = false;
        btnVoid.Enabled = false;
        btnPreviousQuestion.Enabled = false;
        btnNextQuestion.Enabled = false;
      }
      catch (Exception oException) {
        MessageBox.Show(oException.Message);
      }
    }

    private void btnVoid_Click(object sender, System.EventArgs e) {
      try {
        moAuthenticationManager.VoidAuthentication();

        txtAuthenticationStatus.Text = moAuthenticationManager.AuthenticationStatus;
        txtAuthenticatedPerc.Text = moAuthenticationManager.AuthenticatedPerc.ToString();

        cboAnswer.Enabled = false;
        btnAuthenticate.Enabled = false;
        btnVoid.Enabled = false;
        btnPreviousQuestion.Enabled = false;
        btnNextQuestion.Enabled = false;
      }
      catch (Exception oException) {
        MessageBox.Show(oException.Message);
      }
    }

    private void cboAnswer_SelectedIndexChanged(object sender, System.EventArgs e) {
      if (cboAnswer.SelectedItem!=null) {
        foreach (AnswerStructure oAnswerStructure in moAuthenticationManager.CurrentQuestion.Answers) {
          oAnswerStructure.IsEnteredAnswer = false;
          if (oAnswerStructure.Answer==cboAnswer.SelectedItem.ToString()) {
            oAnswerStructure.IsEnteredAnswer = true;
          }
        }
      }
    }

    private void ClearInfo() {
      cboAnswer.Items.Clear();
      txtReferenceNo.Text = "";
      txtAuthenticationType.Text = "";
      txtAuthenticationStatus.Text = "";
      txtRepeatAuthenticationMessage.Text = "";
      txtAuthenticatedPerc.Text = "";

      cboAnswer.Enabled = false;
      btnAuthenticate.Enabled = false;
      btnVoid.Enabled = false;
      btnPreviousQuestion.Enabled = false;
      btnNextQuestion.Enabled = false;
    }

    private void PopulateAnswerDropDown(QuestionStructure QuestionStructure) {
      cboAnswer.Items.Clear();
      cboAnswer.Items.Add("");
      foreach (AnswerStructure oAnswerStructure in QuestionStructure.Answers) {
        cboAnswer.Items.Add(oAnswerStructure.Answer);
        if (oAnswerStructure.IsEnteredAnswer) {
          cboAnswer.SelectedItem = oAnswerStructure.Answer;
        }
      }
    }
  }
}
