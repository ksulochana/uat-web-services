﻿using System;

using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace XDSPortalEnquiry.Business
{
    public class Consumer_SimSwap
    {

        private XDSPortalLibrary.Business_Layer.ConsumerTrace moConsumerTraceWseManager;
        private XDSPortalLibrary.Entity_Layer.ConsumerTrace eoConsumerTraceWseManager;


        public Consumer_SimSwap()
        {
            moConsumerTraceWseManager = new XDSPortalLibrary.Business_Layer.ConsumerTrace();
            eoConsumerTraceWseManager = new XDSPortalLibrary.Entity_Layer.ConsumerTrace();
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitSimSwapRequest(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string strIDNo, string strCellNo, string strExtRef, string strVoucherCode)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bool bBonusChecking = false;
            string rXml = "";
            int MatchResult = 0;
            int intSubscriberEnquiryResultID = 0;
            DataSet ds = new DataSet();
            Boolean boolSubmitEnquiry = true;
            string strValidationStatus = "";


            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);


            try
            {
                // Check if this subscriber is authorized to use the current product

                if (spr.ReportID > 0)
                {
                    // Check the PayAsYouGo settings for the subscriber
                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {

                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;

                        eSe.IDNo = strIDNo;
                        if (strCellNo.Length == 10 && strCellNo.Substring(0, 1) == "0")
                        {
                            strCellNo = "27" + strCellNo.Substring(1, 9);
                        }
                        eSe.TelephoneNo = strCellNo;
                        eSe.ProductID = intProductId;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = strExtRef;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";
                        

                        //Generate serach input string

                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.FirstName))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.FirstName;
                        }
                        if (!string.IsNullOrEmpty(eSe.Surname))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.Surname;
                        }
                        if (!string.IsNullOrEmpty(eSe.TelephoneNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.TelephoneNo;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table

                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);

                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        SqlConnection objConstring = new SqlConnection(eSe.ExtraVarInput1.ToString());

                        eoConsumerTraceWseManager.ConsumerID = eSC.KeyID;
                        eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                        eoConsumerTraceWseManager.ProductID = eSe.ProductID;
                        eoConsumerTraceWseManager.ReferenceNo = eSC.Reference;
                        eoConsumerTraceWseManager.subscriberID = eSe.SubscriberID;
                        eoConsumerTraceWseManager.TmpReference = eSC.ExtraVarOutput1;
                        moConsumerTraceWseManager.ConnectionString = objConstring;
                        eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;
                        eoConsumerTraceWseManager.AssociationTypeCode = sub.AssociationTypeCode;
                        eoConsumerTraceWseManager.IDno = strIDNo;
                        eoConsumerTraceWseManager.HomePhoneNumber = eSe.TelephoneNo;

                        XDSPortalEnquiry.Data.Consumer oEnquiryConsumer = new XDSPortalEnquiry.Data.Consumer();
                        oEnquiryConsumer.CheckBlockedStatus(AdminConnection, int.Parse(eSe.SubscriberID.ToString()), int.Parse(eSC.KeyID.ToString()), eSe.ProductID, sub.SubscriberAssociationCode, eSe.ProductID);

                        XDSPortalEnquiry.Entity.SimSwap sso = dSe.GetSimSwapObject(con, strIDNo, eSe.TelephoneNo);

                        if (sso.SimSwapResult != null)
                        {
                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.ConsumerTraceMatchSimSwap(eoConsumerTraceWseManager, spr.ReportID.ToString(), "Sim Swap Report");

                            //If error raise one last call with a different connection
                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.ConsumerTraceMatchSimSwap(eoConsumerTraceWseManager, spr.ReportID.ToString(), "Sim Swap Report");
                            }

                            rXml = rp.ResponseData;

                            System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                            {
                                eSC.SearchOutput = "";
                                eSC.IDNo = "";
                                eSC.PassportNo = "";
                                eSC.Surname = "";
                                eSC.FirstName = "";
                                eSC.BirthDate = DateTime.Parse("1900/01/01");
                                eSC.Gender = "";
                                eSC.DetailsViewedYN = false;
                                eSC.Billable = false;
                                eSC.EnquiryResult = XDSPortalEnquiry.Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();

                                dSC.UpdateSubscriberEnquiryResult(con, eSC);
                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSC.EnquiryResult = "E";
                                eSe.ErrorDescription = rp.ResponseData.ToString();
                                dSe.UpdateSubscriberEnquiryError(con, eSe);
                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report ||
                                        rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single ||
                                        rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                            {
                                ds.ReadXml(xmlSR);
                                if (ds.Tables.Contains("ConsumerDetails"))
                                {
                                    XDSPortalEnquiry.Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                    XDSPortalEnquiry.Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, eSe.ProductID);

                                    DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                    dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                    dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                    dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                    dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                    dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                    DataRow drSubscriberInput;
                                    drSubscriberInput = dtSubscriberInput.NewRow();

                                    drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                    drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                    drSubscriberInput["SubscriberName"] = eSe.SubscriberName;
                                    drSubscriberInput["SubscriberUserName"] = eSe.SystemUser;
                                    drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();

                                    dtSubscriberInput.Rows.Add(drSubscriberInput);

                                    ds.Tables.Add(dtSubscriberInput);

                                    DataTable dtSimSwapResult = new DataTable("SimSwapDetails");
                                    dtSimSwapResult.Columns.Add("CellNumber", typeof(String));
                                    dtSimSwapResult.Columns.Add("SimSwapResult", typeof(String));
                                    dtSimSwapResult.Columns.Add("RiskFlag", typeof(String));

                                    DataRow drSimSwapResult;
                                    drSimSwapResult = dtSimSwapResult.NewRow();

                                    drSimSwapResult["CellNumber"] = sso.CellphoneNumber;
                                    drSimSwapResult["SimSwapResult"] = sso.SimSwapResult;
                                    drSimSwapResult["RiskFlag"] = sso.RiskFlag;

                                    dtSimSwapResult.Rows.Add(drSimSwapResult);

                                    ds.Tables.Add(dtSimSwapResult);

                                    rXml = ds.GetXml();
                                    rp.ResponseData = rXml;

                                    foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                    {
                                        eSC.SearchOutput = "";
                                        eSC.IDNo = "";
                                        eSC.PassportNo = "";
                                        eSC.Surname = "";
                                        eSC.FirstName = "";
                                        eSC.BirthDate = DateTime.Parse("1900/01/01");
                                        eSC.Gender = "";
                                        eSC.SearchOutput = "";

                                        if (r.Table.Columns.Contains("IDNo"))
                                        {
                                            if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                            {
                                                eSC.IDNo = r["IDNo"].ToString();
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                            }
                                        }
                                        if (r.Table.Columns.Contains("PassportNo"))
                                        {
                                            if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                            {
                                                eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                            }
                                        }
                                        if (r.Table.Columns.Contains("Surname"))
                                        {
                                            if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                            {
                                                eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                            }
                                        }

                                        if (r.Table.Columns.Contains("FirstName"))
                                        {
                                            if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                            {
                                                eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                            }
                                        }

                                        if (eSC.SearchOutput.Length > 0)
                                        {
                                            eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                        }

                                        eSC.CreatedByUser = eSe.CreatedByUser;
                                        eSC.CreatedOnDate = DateTime.Now;
                                        eSC.KeyID = 0;
                                        eSC.KeyType = "D";
                                        eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                        eSC.ExtraVarOutput1 = "";
                                        eSC.ProductID = intProductId;
                                        eSC.VoucherCode = strVoucherCode;

                                        eSC.BillingTypeID = spr.BillingTypeID;
                                        eSC.BillingPrice = spr.UnitPrice;
                                        eSC.DetailsViewedDate = DateTime.Now;
                                        eSC.DetailsViewedYN = true;
                                        eSC.Billable = true;
                                        eSC.XMLData = rXml.Replace("'", "''");
                                        eSC.EnquiryResult = XDSPortalEnquiry.Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();

                                        //dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                    }
                                }
                            }

                            rp.EnquiryID = eSe.SubscriberEnquiryID;
                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                            rp.ResponseData = "Submitted Successfully";
                        }
                        else
                        {
                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.SubmitSimSwapRequest(eoConsumerTraceWseManager);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.SubmitSimSwapRequest(eoConsumerTraceWseManager);
                            }

                            //rp = moConsumerTraceWseManager.search(eoConsumerTraceWseManager);
                            rp.EnquiryID = intSubscriberEnquiryID;

                            // Get Response data

                            rXml = rp.ResponseData;

                            System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);


                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                            {
                                eSC.SearchOutput = "";
                                eSC.IDNo = "";
                                eSC.PassportNo = "";
                                eSC.Surname = "";
                                eSC.FirstName = "";
                                eSC.BirthDate = DateTime.Parse("1900/01/01");
                                eSC.Gender = "";
                                eSC.DetailsViewedYN = false;
                                eSC.Billable = false;
                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                                eSC.CreatedByUser = eSe.CreatedByUser;
                                eSC.CreatedOnDate = DateTime.Now;

                                rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                rp.EnquiryID = eSC.SubscriberEnquiryID;
                            }

                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSC.EnquiryResult = "E";
                                eSe.ErrorDescription = rp.ResponseData.ToString();
                                dSe.UpdateSubscriberEnquiryError(con, eSe);
                            }

                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report ||
                                     rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single ||
                                     rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                            {
                                dSe.UpdateSubscriberEnquiryBranchCode(con, eSe.SubscriberEnquiryID, rp.ResponseData.ToString());

                                eSC.SearchOutput = "";
                                eSC.IDNo = "";
                                eSC.PassportNo = "";
                                eSC.Surname = "";
                                eSC.FirstName = "";
                                eSC.BirthDate = DateTime.Parse("1900/01/01");
                                eSC.Gender = "";
                                eSC.SearchOutput = "";

                                eSC.IDNo = strIDNo;
                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;

                                if (eSC.SearchOutput.Length > 0)
                                {
                                    eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                }

                                eSC.DetailsViewedYN = false;
                                eSC.Billable = false;
                                eSC.CreatedByUser = eSe.CreatedByUser;
                                eSC.CreatedOnDate = DateTime.Now;
                                eSC.KeyID = 0;
                                eSC.KeyType = "D";

                                eSC.BillingTypeID = spr.BillingTypeID;
                                eSC.BillingPrice = spr.UnitPrice;

                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                eSC.ExtraVarOutput1 = "";
                                eSC.ProductID = intProductId;
                                eSC.VoucherCode = strVoucherCode;

                                rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                intSubscriberEnquiryResultID = rp.EnquiryResultID;
                                rp.EnquiryID = eSC.SubscriberEnquiryID;

                                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);
                                // Change the PayAsYouGoEnquiryLimit  after viewing the report

                                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                                rp.ResponseData = "Submitted Successfully";

                            }
                        }
                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }
            catch (Exception oException)
            {

                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);

                MatchResult = eSe.SubscriberEnquiryID;
                rp.EnquiryID = eSe.SubscriberEnquiryID;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (oException.Message.Contains("A .NET Framework"))
                {
                    rp.ResponseData = "Error: The System is unable to process this request. Please contact XDS Support";
                }

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            ds.Dispose();
            return rp;
        }

  
    }
}
