﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace XDSPortalEnquiry.Business
{
    public class NLR_Registrations
    {
        XDSPortalLibrary.Entity_Layer.NLRDailyRegistrations eoNLRDailyRegistrations;


        public NLR_Registrations()
        {
            eoNLRDailyRegistrations = new XDSPortalLibrary.Entity_Layer.NLRDailyRegistrations();
        }

      
        public XDSPortalLibrary.Entity_Layer.Response InsertNLRRegistrations(SqlConnection con, SqlConnection AdminConnection, int intEnqSubscriberID, int intProductID, int intSystemUserID, string strSubscriberCode, string strSupplierRefNo, string strEnquiryRefNo, string strConsumerIdNo, string strPassportNo, string strConsumerBirthdate, string strSurname, string strForename1, string strForename2, string strForename3, string strMaidenName, string strAliasName, string strGender, string strMaritalStatus, string strSpouseCurrentSurname, string strSpouseForename1, string strSpouseForename2, string strResAddress1, string strResAddress2, string strResAddress3, string strResAddress4, string strResPostalCode, string strPeriodAtCurretAddress, string strOthAddress1, string strOthAddress2, string strOthAddress3, string strOthAddress4, string strOthPostalCode, string strOwnerTenant, string strOccupation, string strEmployerType, string strEmployer, string strEmployeeNo, string strPaySlipRefNo, string strHomeTelCode, string strHomeTelNo, string strWorkTelCode, string strWorkTelNo, string strCellNo, string strMonthlySalary, string strSalaryFrequncy, string strBranchCode, string strAccno, string strSubAccno, string strDateDisbursed, string strLoantype, string strLoadInd, string strLoanAmount, string strLoanAmtBalInd, string strCurrentBalance, string strcurrbalanceInd, string strInstallmentAmt, string strRepaymentPeriod,  string strLoanPurpose, string strTotalAmountRepayable, string strInterestType, string strAnnualRateForTotalChargeOfCredit, string strRandValueOfInterestCharges, string strRandValueOFTotalChargeOFCredit, string strSettlementAmount, string strVoucherCode,string strExternalReference)
        {
            string strAdminCon = AdminConnection.ConnectionString;
            string strValidationStatus = string.Empty;
            double Totalcost = 0;
            string rXml = "";
            DataSet ds = new DataSet();


            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            Data.NLR dNLR = new XDSPortalEnquiry.Data.NLR();


            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();


            try
            {
               
                
                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intEnqSubscriberID, intProductID);               

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intEnqSubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

                

                //Data.DefaultAlerts oDefaultAlerts = new XDSPortalEnquiry.Data.DefaultAlerts();

                
                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {

                    // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML 
                    // use this XML data for generating the Report 

                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {

                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intEnqSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1)
                    {
                        //Calculate The Total cost of the Report , including Bonus Segments

                        Totalcost = spr.UnitPrice;


                    }


                    if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                    {

                        eoNLRDailyRegistrations.SubscriberCode = strSubscriberCode;
                        eoNLRDailyRegistrations.SupplierRefNo = strSupplierRefNo;
                        eoNLRDailyRegistrations.EnquiryRefNo = strEnquiryRefNo;
                        eoNLRDailyRegistrations.ConsSurname = strSurname;
                        eoNLRDailyRegistrations.Forename1 = strForename1;
                        eoNLRDailyRegistrations.Forename2 = strForename2;
                        eoNLRDailyRegistrations.Forename3 = strForename3;
                        eoNLRDailyRegistrations.SpouseCurrSurname = strSpouseCurrentSurname;
                        eoNLRDailyRegistrations.SpouseForename1 = strSpouseForename1;
                        eoNLRDailyRegistrations.SpouseForename2 = strSpouseForename2;
                        eoNLRDailyRegistrations.ConsBirthDate = strConsumerBirthdate;
                        eoNLRDailyRegistrations.ConsIdentityNo = strConsumerIdNo;
                        eoNLRDailyRegistrations.CurrAddrLn1 = strResAddress1;
                        eoNLRDailyRegistrations.CurrAddrLn2 = strResAddress2;
                        eoNLRDailyRegistrations.CurrAddrLn3 = strResAddress3;
                        eoNLRDailyRegistrations.CurrAddrLn4 = strResAddress4;
                        eoNLRDailyRegistrations.CurrAddressPostalCode = strResPostalCode;
                        eoNLRDailyRegistrations.PeriodAtCurrAddress = strPeriodAtCurretAddress;
                        eoNLRDailyRegistrations.OtherAddrLn1 = strOthAddress1;
                        eoNLRDailyRegistrations.OtherAddrLn2 = strOthAddress2;
                        eoNLRDailyRegistrations.OtherAddrLn3 = strOthAddress3;
                        eoNLRDailyRegistrations.OtherAddrLn4 = strOthAddress4;
                        eoNLRDailyRegistrations.OtherAddressPostalCode = strOthPostalCode;
                        eoNLRDailyRegistrations.OwnerTenantAtCurrAddress = strOwnerTenant;
                        eoNLRDailyRegistrations.ConsOccupation = strOccupation;
                        eoNLRDailyRegistrations.ConsEmployer = strEmployer;
                        eoNLRDailyRegistrations.ConsMaidenName = strMaidenName;
                        eoNLRDailyRegistrations.ConsAliasName = strAliasName;
                        eoNLRDailyRegistrations.Gender = strGender;
                        eoNLRDailyRegistrations.ConsMaritalStatus = strMaritalStatus;
                        eoNLRDailyRegistrations.ConsHomeTelDiallingCode = strHomeTelCode;
                        eoNLRDailyRegistrations.ConsHomeTelNo = strHomeTelNo;
                        eoNLRDailyRegistrations.ConsWorkTelDiallingCode = strWorkTelCode;
                        eoNLRDailyRegistrations.ConsWorkTelNo = strWorkTelNo;
                        eoNLRDailyRegistrations.ConsCellularTelNo = strCellNo;
                        eoNLRDailyRegistrations.InstalmentAmt = strInstallmentAmt;
                        eoNLRDailyRegistrations.MonthlySalary = strMonthlySalary;
                        eoNLRDailyRegistrations.SalaryFrequency = strSalaryFrequncy;
                        eoNLRDailyRegistrations.BranchCode = strBranchCode;
                        eoNLRDailyRegistrations.AccountNo = strAccno;
                        eoNLRDailyRegistrations.SubAccountNo = strSubAccno;
                        eoNLRDailyRegistrations.LoanType = strLoantype;
                        eoNLRDailyRegistrations.DateLoanDisbursed = strDateDisbursed;
                        eoNLRDailyRegistrations.LoanAmt2 = strLoanAmount;
                        eoNLRDailyRegistrations.LoanAmtBalanceIndicator = strLoanAmtBalInd;
                        eoNLRDailyRegistrations.CurrBalance = strCurrentBalance;
                        eoNLRDailyRegistrations.CurrBalanceIndicator = strcurrbalanceInd;
                        eoNLRDailyRegistrations.MonthlyInstalment = strInstallmentAmt;
                        eoNLRDailyRegistrations.LoadIndicator = strLoadInd;
                        eoNLRDailyRegistrations.RepaymentPeriod2 = strRepaymentPeriod;
                        eoNLRDailyRegistrations.LoanPurpose = strLoanPurpose;
                        eoNLRDailyRegistrations.TotalAmtRepayable = strTotalAmountRepayable;
                        eoNLRDailyRegistrations.InterestType = strInterestType;
                        eoNLRDailyRegistrations.AnnualRateForTotalChargeOfCredit = strAnnualRateForTotalChargeOfCredit;
                        eoNLRDailyRegistrations.RandValueOfInterestCharges = strRandValueOfInterestCharges;
                        eoNLRDailyRegistrations.RandValueOfTotalChargeOfCredit = strRandValueOFTotalChargeOFCredit;
                        eoNLRDailyRegistrations.SettlementAmt = strSettlementAmount;
                        eoNLRDailyRegistrations.PassportNo = strPassportNo;
                        eoNLRDailyRegistrations.EnquirySubscriberID = intEnqSubscriberID;
                        eoNLRDailyRegistrations.Username = sys.SystemUserFullName;
                        eoNLRDailyRegistrations.SubscriberName = sub.SubscriberName;
                        eoNLRDailyRegistrations.SystemUserID = sys.SystemUserID;
                        eoNLRDailyRegistrations.ChangedByUser = sys.SystemUserFullName;
                        eoNLRDailyRegistrations.StatusInd = XDSPortalLibrary.Entity_Layer.NLRDailyRegistrations.EnquiryStatusInd.P.ToString();
                        eoNLRDailyRegistrations.VoucherCode = strVoucherCode;
                        eoNLRDailyRegistrations.SubscriberReference = strExternalReference;
                        eoNLRDailyRegistrations.Billable = false;
                        eoNLRDailyRegistrations.Payasyougo = sub.PayAsYouGo;
                        eoNLRDailyRegistrations.productID = intProductID;
                        eoNLRDailyRegistrations.BillingtypeId = spr.BillingTypeID;
                        eoNLRDailyRegistrations.subscriberEnquiryDate = DateTime.Now;
                        eoNLRDailyRegistrations.CreatedOnDate = DateTime.Now;
                        eoNLRDailyRegistrations.SubscriberID = intEnqSubscriberID;


                        int EnquiryReferenceNo = 0;
                        if (!string.IsNullOrEmpty(strEnquiryRefNo) && int.TryParse(strEnquiryRefNo, out EnquiryReferenceNo))
                        {
                            eSe = dSe.GetSubscriberEnquiryObject(con, EnquiryReferenceNo);
                        }
                        eoNLRDailyRegistrations.DateEnquirySent = eSe.SubscriberEnquiryDate.ToString();
                        eoNLRDailyRegistrations.TimeEnquirySent = eSe.SubscriberEnquiryDate.ToString();

                        eoNLRDailyRegistrations.NLRDailyRegistrationsLogID = dNLR.InsertLog(con, eoNLRDailyRegistrations);

                        string constr = dNLR.GetNLRDailyRegistrationsLogObject(con, eoNLRDailyRegistrations.NLRDailyRegistrationsLogID);
                        SqlConnection objConstring = new SqlConnection(constr);


                        dNLR.ExecValidations(objConstring, eoNLRDailyRegistrations);

                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, constr);
                        rp = ra.NLRRegistration(eoNLRDailyRegistrations);


                        // Get Response data
                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            eoNLRDailyRegistrations.StatusInd = "F";
                            eoNLRDailyRegistrations.ErrorDescription = rp.ResponseData.ToString();
                            eoNLRDailyRegistrations.ChangedByUser = sys.Username;
                            eoNLRDailyRegistrations.ChangedOnDate = DateTime.Now;
                            dNLR.UpdateError(con, eoNLRDailyRegistrations);
                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            eoNLRDailyRegistrations.StatusInd = "C";
                            eoNLRDailyRegistrations.NLRDailyRegistrationsID = rp.ResponseKey;
                            eoNLRDailyRegistrations.Billable = false;
                            eoNLRDailyRegistrations.DetailsViewedYN = true;
                            eoNLRDailyRegistrations.XMLData = rp.ResponseData;
                            eoNLRDailyRegistrations.ChangedOnDate = DateTime.Now;
                            eoNLRDailyRegistrations.ChangedByUser = sys.Username;
                            eoNLRDailyRegistrations.Detailsvieweddate = DateTime.Now;

                            dNLR.UpdateResult(con, eoNLRDailyRegistrations);

                            if (!(string.IsNullOrEmpty(strVoucherCode)))
                            {
                                dSV.UpdateSubscriberVoucher(AdminConnection, strVoucherCode, eoNLRDailyRegistrations.CreatedByUser);
                            }
                        }

                    }
                    else
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";

                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception ex)
            {
                eoNLRDailyRegistrations.StatusInd = "F";
                eoNLRDailyRegistrations.ErrorDescription = ex.Message;                
                eoNLRDailyRegistrations.NLRDailyRegistrationsLogID = dNLR.UpdateError(con,eoNLRDailyRegistrations);

                rp.EnquiryID = eoNLRDailyRegistrations.NLRDailyRegistrationsLogID;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = ex.Message;

                if (ex.Message.Contains("A .NET Framework"))
                {
                    rp.ResponseData = "Error: The System is unable to process this request. Please contact XDS Support";
                }

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }

            return rp;
        }
    }
}
