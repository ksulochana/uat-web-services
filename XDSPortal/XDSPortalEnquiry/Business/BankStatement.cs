﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using XDSDataLibrary;


namespace XDSPortalEnquiry.Business
{
    public class BankStatement
    {
        private XDSPortalLibrary.Business_Layer.ConsumerIDVerificationTrace moConsumerTraceWseManager;
        private XDSPortalLibrary.Entity_Layer.ConsumerIDVerificationTrace eoConsumerTraceWseManager;


        public BankStatement()
        {
            moConsumerTraceWseManager = new XDSPortalLibrary.Business_Layer.ConsumerIDVerificationTrace();
            eoConsumerTraceWseManager = new XDSPortalLibrary.Entity_Layer.ConsumerIDVerificationTrace();
        }

        public object GetTruidBankNames(string adminCon)
        {
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();
            ReportAccess reportAccess = new ReportAccess(adminCon, eSe.ExtraVarInput1);
            return reportAccess.GetTruidBankNames();
        }

        public XDSPortalLibrary.Entity_Layer.Response PostTruidBankStatementRequest(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID,
            int intSystemUserID, int intProductId, string strSubscriberName, string strVoucherCode, string mobile, string idNumber, string bank, string accountName,
            string strBankStatementConnString)
        {

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            //bBonusChecking = false;
            string rXml = "";
            int MatchResult = 0;
            DataSet ds = new DataSet();
            int intSubscriberEnquiryResultID = 0;
            Boolean boolSubmitEnquiry = true;
            string strValidationStatus = "";
            string strExtRef = null;

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);

            string strAdminCon = AdminConnection.ConnectionString;

            bool bBonusChecking = false;

            try
            {
                if (spr.ReportID > 0)
                {
                    // Check the PayAsYouGo settings for the subscriber
                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }

                    if (boolSubmitEnquiry)
                    {
                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;
                        // eSe.BirthDate = dtBirthDate;
                        // eSe.FirstName = strFirstName;
                        eSe.IDNo = idNumber;
                        eSe.ProductID = intProductId;
                        //eSe.SecondName = strSecondName;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = strExtRef;
                        // eSe.Surname = strSurname;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";

                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.FirstName))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.FirstName;
                        }
                        if (!string.IsNullOrEmpty(eSe.Surname))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.Surname;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);
                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoConsumerTraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoConsumerTraceWseManager.IDno = eSe.IDNo;
                        eoConsumerTraceWseManager.BonusCheck = bBonusChecking;
                        eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;
                        eoConsumerTraceWseManager.subscriberID = sub.SubscriberID;
                        //eoConsumerTraceWseManager.DHAURl = strDHAURL;

                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.PostTruidBankStatementRequest(eoConsumerTraceWseManager, mobile, idNumber, bank);

                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.PostTruidBankStatementRequest(eoConsumerTraceWseManager, mobile, idNumber, bank);
                        }

                        rp.EnquiryID = intSubscriberEnquiryID;

                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            eSC.SearchOutput = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.DetailsViewedYN = false;
                            eSC.Billable = false;
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;

                            rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                            rp.EnquiryID = eSC.SubscriberEnquiryID;
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);
                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            //eSC.KeyID = intSubscriberEnquiryID;
                            eSC.KeyType = "R";
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                            eSC.BillingTypeID = spr.BillingTypeID;
                            eSC.BillingPrice = spr.UnitPrice;
                            //eSC.Reference = "R-" + intSubscriberEnquiryID.ToString();
                            eSC.ProductID = intProductId;
                            rXml = ds.GetXml();
                            //rp.ResponseData = rXml;
                            eSC.IDNo = rp.ResponseData;// quid???
                            eSC.XMLData = rXml.Replace("'", "''");
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();

                            rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                            rp.EnquiryID = eSC.SubscriberEnquiryID;
                            int RequestID = SaveRequestRequestID(strBankStatementConnString, AdminConnection.ConnectionString, idNumber, accountName, mobile, bank,
                                sys.SystemUserFullName, rp.ResponseData, intSubscriberEnquiryID.ToString());
                            rp.ResponseKey = RequestID;
                            dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                        }
                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }
                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;


                MatchResult = eSe.SubscriberEnquiryID;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public int SaveRequestRequestID(string strBankStatementConnString, string adminCon, string iDNoOROtherID, string accountName, string mobileNumber, string bankNameValue, string displayName, string transactionId, string subscriberID)
        {
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();
            ReportAccess reportAccess = new ReportAccess(adminCon, eSe.ExtraVarInput1);
            return reportAccess.SaveRequestRequestID(strBankStatementConnString, iDNoOROtherID, accountName, mobileNumber, bankNameValue, displayName, transactionId, subscriberID);
        }

        public object GetTruidBankStatementStatus(string adminCon, string transactionID)
        {
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();
            ReportAccess reportAccess = new ReportAccess(adminCon, eSe.ExtraVarInput1);
            //if there is no status code saved yet, save it first then move on to the next
            object result = reportAccess.GetTruidBankStatementStatus(transactionID);
            return result;
        }

        public byte[] GetTruidBankStatement(string adminCon, string transactionID)
        {
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();
            ReportAccess reportAccess = new ReportAccess(adminCon, eSe.ExtraVarInput1);

            return reportAccess.GetTruidBankStatement(transactionID);
        }

        public string GetBankStatementRequestIDByTrueIDTransactionID(string adminCon, string TransactionID)
        {
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();
            ReportAccess reportAccess = new ReportAccess(adminCon, eSe.ExtraVarInput1);

            return reportAccess.GetBankStatementRequestIDByTrueIDTransactionID(adminCon, TransactionID);
        }
        public string IsAnyTrueIDResponseSaved(string adminCon, string RequestID)
        {
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();
            ReportAccess reportAccess = new ReportAccess(adminCon, eSe.ExtraVarInput1);

            return reportAccess.IsTrueIDResponseSaved(adminCon, RequestID);
        }

        public void SaveResponses(string adminCon, string reference, string description, string date, string createdbyUser, string createdDate, string requestID)
        {
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();
            ReportAccess reportAccess = new ReportAccess(adminCon, eSe.ExtraVarInput1);
            reportAccess.SaveResponses(reference, description, date, createdbyUser, createdDate, requestID);
        }


        public object GetEnquiryStatus(string adminCon, string iDNoOROtherIDOrTransactionID)
        {
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();
            ReportAccess reportAccess = new ReportAccess(adminCon, eSe.ExtraVarInput1);

            object responseFromTrudID = reportAccess.GetEnquiryStatus(iDNoOROtherIDOrTransactionID);

            return responseFromTrudID;
        }

        public byte[] GetBankStatementByte(string strBankStatementConnString, string adminCon, string requestID)
        {
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();
            ReportAccess reportAccess = new ReportAccess(adminCon, eSe.ExtraVarInput1);
            return reportAccess.GetBankStatementByte(strBankStatementConnString, requestID);
        }

        //public bool IsDuplicateEnquiry(string strBankStatementConnString, string adminCon, string iDNoOROtherID)
        //{
        //    Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();
        //    ReportAccess reportAccess = new ReportAccess(adminCon, eSe.ExtraVarInput1);
        //    return reportAccess.IsDuplicateEnquiry(strBankStatementConnString, iDNoOROtherID);
        //}
    }
}
