﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace XDSPortalEnquiry.Business
{
    public class MultipleBanking_AccountVerificationNed
    {
        private XDSPortalLibrary.Business_Layer.AccountVerificationNed moAccountVerificationWseManager;
        private XDSPortalLibrary.Entity_Layer.AccountVerificationNed eoAccountVerificationWseManager;


        public MultipleBanking_AccountVerificationNed()
        {
            moAccountVerificationWseManager = new XDSPortalLibrary.Business_Layer.AccountVerificationNed();
            eoAccountVerificationWseManager = new XDSPortalLibrary.Entity_Layer.AccountVerificationNed();
        }
        public XDSPortalLibrary.Entity_Layer.Response GetAccountVerificationResult(SqlConnection con, SqlConnection AdminConnection,SqlConnection AVSConnection, int intProductId, int intSubscriberEnquiryID, int intSubscriberEnquiryLogID)
        {

            string strUserName = string.Empty, strPassword = string.Empty, strAccRef = string.Empty;
            if (con.State == ConnectionState.Closed)
                con.Open();

            string rXml = "";
            double Totalcost = 0;

            string sessionID = string.Empty;

            DataSet ds = new DataSet();
            DataSet oEnquiryDS = new DataSet();
            DataSet dsLogon = new DataSet();
            DataSet dsLogo = new DataSet();

            Data.IdecoLogonDetails dlogin = new XDSPortalEnquiry.Data.IdecoLogonDetails();
            dsLogon = dlogin.GetIdecoLogonDetailsDataSet(AdminConnection);

            strUserName = dsLogon.Tables[0].Rows[0].Field<string>("Username").ToString();
            strPassword = dsLogon.Tables[0].Rows[0].Field<string>("Password").ToString();
            strAccRef = dsLogon.Tables[0].Rows[0].Field<string>("AccRef").ToString();



            Entity.SubscriberEnquiryLog sel = new XDSPortalEnquiry.Entity.SubscriberEnquiryLog();
            Data.SubscriberEnquiryLog dsel = new XDSPortalEnquiry.Data.SubscriberEnquiryLog();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            Data.IdecoreturnCodes Icode = new XDSPortalEnquiry.Data.IdecoreturnCodes();



            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {

                sel = dsel.GetSubscriberEnquiryLogObject(con, intSubscriberEnquiryLogID);

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, sel.SubscriberID, intProductId);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, sel.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, sel.SystemUserID);

                if (spr.ReportID > 0)
                {

                    // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML 
                    // use this XML data for generating the Report 

                    //if (string.IsNullOrEmpty(sel.XMLData) || sel.XMLData == string.Empty)
                    //{
                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(sel.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, sel.SubscriberID, sel.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }
                        else if (sub.PayAsYouGo == 1)
                        {
                            //Calculate The Total cost of the Report , including Bonus Segments

                            Totalcost = spr.UnitPrice;
                        }

                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {
                            sel.SubscriberEnquiryID = intSubscriberEnquiryID;
                            sel.SubscriberEnquiryLogID = intSubscriberEnquiryLogID;

                            eoAccountVerificationWseManager.Key = sel.KeyID;
                            moAccountVerificationWseManager.ConnectionString = AVSConnection;

                            rp = moAccountVerificationWseManager.GetData(eoAccountVerificationWseManager);

                            rXml = rp.ResponseData;

                            System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);


                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {

                                sel.EnquiryResult = "N";
                                sel.EnquiryStatus = "P";
                                sel.ErrorDescription = rp.ResponseData.ToString();
                                dsel.UpdateSubscriberEnquiryLogErrorAccv(con, sel);


                            }

                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                            {
                                ds.ReadXml(xmlSR);
                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, sel.ProductID);
                                dsLogo = dsel.GetSubscriberLogo(AdminConnection, intProductId, spr.ReportID, sel.SubscriberID);
                                string status = string.Empty;
                                if (ds.Tables.Contains("AccountVerificationResponse"))
                                {
                                    status = ds.Tables["AccountVerificationResponse"].Rows[0].Field<string>("Status").ToString();
                                    if (ds.Tables["AccountVerificationResponse"].Rows[0].Field<string>("Status").ToString() == "P" ||
                                        ds.Tables["AccountVerificationResponse"].Rows[0].Field<string>("Status").ToString() == "F")
                                    {

                                        DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                        dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                        dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                        dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                        dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                        dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                        dtSubscriberInput.Columns.Add("EnquiryStatus", typeof(String));
                                        dtSubscriberInput.Columns.Add("XDsRefNo", typeof(String));
                                        dtSubscriberInput.Columns.Add("ExternalRef", typeof(String));
                                        dtSubscriberInput.Columns.Add("IDNo", typeof(String));
                                        dtSubscriberInput.Columns.Add("SurName", typeof(String));
                                        dtSubscriberInput.Columns.Add("Initials", typeof(String));
                                        dtSubscriberInput.Columns.Add("AccountNo", typeof(String));
                                        dtSubscriberInput.Columns.Add("BankName", typeof(String));
                                        dtSubscriberInput.Columns.Add("BranchCode", typeof(String));
                                        dtSubscriberInput.Columns.Add("AccountType", typeof(String));
                                        dtSubscriberInput.Columns.Add("EmailAddress", typeof(String));
                                        dtSubscriberInput.Columns.Add("RequesterFirstName", typeof(String));
                                        dtSubscriberInput.Columns.Add("RequesterSurName", typeof(String));
                                        dtSubscriberInput.Columns.Add("refnumber", typeof(String));

                                        DataRow drSubscriberInput;
                                        drSubscriberInput = dtSubscriberInput.NewRow();

                                        DataTable dtSubscriberLogoInput = new DataTable("SubscriberLogo");
                                        if (dsLogo.Tables.Count > 0 && dsLogo.Tables[0].Rows.Count > 0)
                                        { 
                                            dtSubscriberLogoInput.Columns.Add("SubscriberID", typeof(int));
                                            dtSubscriberLogoInput.Columns.Add("ProductID", typeof(int));
                                            dtSubscriberLogoInput.Columns.Add("ReportID", typeof(int));
                                            dtSubscriberLogoInput.Columns.Add("LogoImage", typeof(byte[]));

                                            DataRow drSubscriberLogoInput;
                                            drSubscriberLogoInput = dtSubscriberLogoInput.NewRow();

                                            foreach (DataRow dr in dsLogo.Tables[0].Rows)
                                            {
                                                drSubscriberLogoInput["LogoImage"] = dr["LogoImage"];
                                                drSubscriberLogoInput["SubscriberID"] = dr["SubscriberID"];
                                                drSubscriberLogoInput["ProductID"] = dr["ProductID"];
                                                drSubscriberLogoInput["ReportID"] = dr["ReportID"];
                                            }

                                            dtSubscriberLogoInput.Rows.Add(drSubscriberLogoInput);
                                        } 

                                        drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                        drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                        drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                        drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                        drSubscriberInput["EnquiryInput"] = sel.Searchinput.Trim();
                                        if (!(String.IsNullOrEmpty(sel.IDNo.Trim())))
                                        {
                                            drSubscriberInput["IDNo"] = sel.IDNo.Trim();
                                        }
                                        else
                                        {
                                            drSubscriberInput["IDNo"] = sel.BusRegistrationNo.Trim();
                                        }
                                        drSubscriberInput["SurName"] = sel.SurName.Trim();
                                        drSubscriberInput["Initials"] = sel.FirstName.Trim();
                                        drSubscriberInput["AccountNo"] = sel.AccountNo.Trim();
                                        drSubscriberInput["BankName"] = sel.BankName.Trim();
                                        drSubscriberInput["BranchCode"] = sel.BranchCode.Trim();
                                        drSubscriberInput["AccountType"] = sel.BranchName.Trim();
                                        drSubscriberInput["EnquiryStatus"] = "Processed";
                                        drSubscriberInput["XDSRefNo"] = sel.Reference.ToString();
                                        drSubscriberInput["ExternalRef"] = sel.SubscriberReference.ToString();
                                        drSubscriberInput["EmailAddress"] = sel.EmailAddress.ToString();
                                        drSubscriberInput["RequesterFirstName"] = sel.AccHolder.ToString();
                                        drSubscriberInput["RequesterSurName"] = sel.BusBusinessName.ToString();
                                        drSubscriberInput["refnumber"] = intSubscriberEnquiryLogID;


                                        dtSubscriberInput.Rows.Add(drSubscriberInput);

                                        string resultXml = ds.Tables["AccountVerificationResponse"].Rows[0].Field<string>("ResultXML").ToString();

                                        DataSet dsResult = new DataSet();
                                        System.IO.StringReader xmlSR1 = new System.IO.StringReader(resultXml);
                                        dsResult.ReadXml(xmlSR1);

                                        dsResult.Tables.Add(dtSubscriberInput);
                                        if(dtSubscriberLogoInput.Rows.Count > 0)
                                            dsResult.Tables.Add(dtSubscriberLogoInput);

                                        rXml = dsResult.GetXml();
                                        rp.ResponseData = rXml;
                                        rp.EnquiryLogID = sel.SubscriberEnquiryLogID;
                                        rp.EnquiryID = sel.SubscriberEnquiryID;

                                        sel.SearchOutput = "";

                                        sel.DetailsViewedDate = DateTime.Now;
                                        sel.DetailsViewedYN = true;
                                        if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(sel.VoucherCode)))
                                        {
                                            sel.Billable = false;
                                        }
                                        else
                                        {
                                            sel.Billable = true;
                                        }
                                        sel.ChangedByUser = sel.CreatedByUser;
                                        sel.ChangedOnDate = DateTime.Now;
                                        sel.SearchOutput = "";

                                        sel.KeyID = rp.ResponseKey;
                                        sel.KeyType = rp.ResponseKeyType;

                                        sel.BillingTypeID = spr.BillingTypeID;
                                        sel.BillingPrice = spr.UnitPrice;

                                        sel.XMLData = rXml;
                                        sel.EnquiryResult = Entity.SubscriberEnquiryLog.EnquiryResultInd.R.ToString();
                                        sel.EnquiryStatus = Entity.SubscriberEnquiryLog.EnquiryStatusInd.C.ToString();
                                        sel.ProductID = intProductId;

                                        //XdsPortalReports.AccountVerificationReport oAccountVerificationReport = new XdsPortalReports.AccountVerificationReport(rp.ResponseData);
                                        //Stream oStream = null;
                                        //oAccountVerificationReport.ExportToPdf(oStream);

                                        //Byte[] bytearr = new byte[oStream.Length];
                                        //oStream.Read(bytearr, 0, bytearr.Length);

                                        //sel.ReportFile = bytearr;
                                        //sel.FileName = oAccountVerificationReport.Name;


                                        //                                    dsel.UpdateSubscriberEnquiryLog(con, sel);


                                        // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost

                                        if (sub.PayAsYouGo == 1)
                                        {
                                            dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, sel.SubscriberID, Totalcost);
                                        }
                                        if (!(string.IsNullOrEmpty(sel.VoucherCode)))
                                        {
                                            dSV.UpdateSubscriberVoucher(AdminConnection, sel.VoucherCode, sel.CreatedByUser);
                                        }
                                        rp.ResponseData = dsResult.GetXml();
                                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                                    } 
                                    else
                                    {

                                        DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                        dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                        dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                        dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                        dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                        dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                        dtSubscriberInput.Columns.Add("EnquiryStatus", typeof(String));
                                        dtSubscriberInput.Columns.Add("XDsRefNo", typeof(String));
                                        dtSubscriberInput.Columns.Add("ExternalRef", typeof(String));
                                        dtSubscriberInput.Columns.Add("IDNo", typeof(String));
                                        dtSubscriberInput.Columns.Add("SurName", typeof(String));
                                        dtSubscriberInput.Columns.Add("Initials", typeof(String));
                                        dtSubscriberInput.Columns.Add("AccountNo", typeof(String));
                                        dtSubscriberInput.Columns.Add("BankName", typeof(String));
                                        dtSubscriberInput.Columns.Add("BranchCode", typeof(String));
                                        dtSubscriberInput.Columns.Add("AccountType", typeof(String));
                                        dtSubscriberInput.Columns.Add("EmailAddress", typeof(String));
                                        dtSubscriberInput.Columns.Add("refnumber", typeof(String));

                                        DataRow drSubscriberInput;
                                        drSubscriberInput = dtSubscriberInput.NewRow();

                                        DataTable dtSubscriberLogoInput = new DataTable("SubscriberLogo");
                                        if (dsLogo.Tables.Count > 0 && dsLogo.Tables[0].Rows.Count > 0)
                                        {
                                            dtSubscriberLogoInput.Columns.Add("SubscriberID", typeof(int));
                                            dtSubscriberLogoInput.Columns.Add("ProductID", typeof(int));
                                            dtSubscriberLogoInput.Columns.Add("ReportID", typeof(int));
                                            dtSubscriberLogoInput.Columns.Add("LogoImage", typeof(byte[]));

                                            DataRow drSubscriberLogoInput;
                                            drSubscriberLogoInput = dtSubscriberLogoInput.NewRow();

                                            foreach (DataRow dr in dsLogo.Tables[0].Rows)
                                            {
                                                drSubscriberLogoInput["LogoImage"] = dr["LogoImage"];
                                                drSubscriberLogoInput["SubscriberID"] = dr["SubscriberID"];
                                                drSubscriberLogoInput["ProductID"] = dr["ProductID"];
                                                drSubscriberLogoInput["ReportID"] = dr["ReportID"];
                                            }

                                            dtSubscriberLogoInput.Rows.Add(drSubscriberLogoInput);
                                        }

                                        drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                        drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                        drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                        drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                        drSubscriberInput["EnquiryInput"] = sel.Searchinput.Trim();
                                        if (!(String.IsNullOrEmpty(sel.IDNo.Trim())))
                                        {
                                            drSubscriberInput["IDNo"] = sel.IDNo.Trim();
                                        }
                                        else
                                        {
                                            drSubscriberInput["IDNo"] = sel.BusRegistrationNo.Trim();
                                        }
                                        drSubscriberInput["SurName"] = sel.SurName.Trim();
                                        drSubscriberInput["Initials"] = sel.FirstName.Trim();
                                        drSubscriberInput["AccountNo"] = sel.AccountNo.Trim();
                                        drSubscriberInput["BankName"] = sel.BankName.Trim();
                                        drSubscriberInput["BranchCode"] = sel.BranchCode.Trim();
                                        drSubscriberInput["AccountType"] = sel.BranchName.Trim();
                                        drSubscriberInput["EnquiryStatus"] = status == "H" ? "On Hold" : (status == "N" ? "Processing" : "Unknown");
                                        drSubscriberInput["XDsRefNo"] = sel.Reference.ToString();
                                        drSubscriberInput["ExternalRef"] = sel.SubscriberReference.ToString();
                                        drSubscriberInput["EmailAddress"] = sel.EmailAddress.ToString();
                                        drSubscriberInput["refnumber"] = intSubscriberEnquiryLogID;


                                        dtSubscriberInput.Rows.Add(drSubscriberInput);

                                        ds.Tables.Add(dtSubscriberInput);
                                        if (dtSubscriberLogoInput.Rows.Count > 0)
                                            ds.Tables.Add(dtSubscriberLogoInput);

                                        rXml = ds.GetXml();
                                        rp.ResponseData = rXml;
                                        rp.EnquiryLogID = sel.SubscriberEnquiryLogID;
                                        rp.EnquiryID = sel.SubscriberEnquiryID;

                                        sel.SearchOutput = "";

                                        sel.DetailsViewedYN = false;
                                        sel.Billable = false;

                                        sel.ChangedByUser = sel.CreatedByUser;
                                        sel.ChangedOnDate = DateTime.Now;
                                        sel.SearchOutput = "";

                                        sel.KeyID = rp.ResponseKey;
                                        sel.KeyType = rp.ResponseKeyType;

                                        sel.BillingTypeID = spr.BillingTypeID;
                                        sel.BillingPrice = spr.UnitPrice;

                                        sel.XMLData = rXml;
                                        sel.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                                        sel.SubscriberEnquiryID = intSubscriberEnquiryID;
                                        sel.ProductID = intProductId;

                                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;

                                    }

                                    XdsPortalReports.AccountVerificationReport oAccountVerificationReport = new XdsPortalReports.AccountVerificationReport(rp.ResponseData);
                                    MemoryStream oStream = new MemoryStream();
                                    oAccountVerificationReport.ExportToPdf(oStream);

                                    oStream.Position = 0;

                                    sel.ReportFile = oStream.ToArray();
                                    sel.FileName = oAccountVerificationReport.Name;

                                }
                                else
                                {
                                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                                }

                                dsel.UpdateSubscriberEnquiryLog(con, sel);

                                rp.ResponseKey = intSubscriberEnquiryLogID;
                            }
                            else
                            {
                                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                                rp.ResponseKey = 0;
                            }

                        }

                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.
                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                        }                 
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
                
            }

            catch (Exception oException)
            {
                sel.EnquiryResult = "E";
                sel.ErrorDescription = oException.Message;
                sel.SubscriberEnquiryID = dsel.UpdateSubscriberEnquiryLogError(con, sel);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            oEnquiryDS.Dispose();
            dsLogon.Dispose();
            return rp;
        }
    }
}
