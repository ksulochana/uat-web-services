using System;

using System.Collections;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;

namespace XDSPortalEnquiry.Business
{
    public class ConsumerTrace_IdentityVerification
    {

        private XDSPortalLibrary.Business_Layer.ConsumerIDVerificationTrace moConsumerTraceWseManager;
        private XDSPortalLibrary.Entity_Layer.ConsumerIDVerificationTrace eoConsumerTraceWseManager;


        public ConsumerTrace_IdentityVerification()
        {
            moConsumerTraceWseManager = new XDSPortalLibrary.Business_Layer.ConsumerIDVerificationTrace();
            eoConsumerTraceWseManager = new XDSPortalLibrary.Entity_Layer.ConsumerIDVerificationTrace();
        }


        public XDSPortalLibrary.Entity_Layer.Response SubmitIdentityVerification(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string strIDNo, string strSurname, string strFirstName, string strSecondName, DateTime dtBirthDate, string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            string rXml = "";
            int MatchResult = 0;
            DataSet ds = new DataSet();
            int intSubscriberEnquiryResultID = 0;
            Boolean boolSubmitEnquiry = true;
            string strValidationStatus = "";

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);

            try
            {
                 if (spr.ReportID > 0)
                {
                    
                    // Check the PayAsYouGo settings for the subscriber
                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {
                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;
                        eSe.BirthDate = dtBirthDate;
                        eSe.FirstName = strFirstName;
                        eSe.IDNo = strIDNo;
                        eSe.ProductID = intProductId;
                        eSe.SecondName = strSecondName;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = strExtRef;
                        eSe.Surname = strSurname;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";

                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.Surname))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.Surname;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);
                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                        
                        eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoConsumerTraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoConsumerTraceWseManager.ConfirmationChkBox = bConfirmationChkBox;
                        eoConsumerTraceWseManager.DOB = dtBirthDate;
                        eoConsumerTraceWseManager.ExternalReference = strExtRef;
                        eoConsumerTraceWseManager.Firstname = strFirstName;
                        eoConsumerTraceWseManager.IDno = strIDNo;
                        eoConsumerTraceWseManager.ProductID = intProductId;
                        eoConsumerTraceWseManager.SecondName = strSecondName;
                        eoConsumerTraceWseManager.subscriberID = intSubscriberID;
                        eoConsumerTraceWseManager.Surname = strSurname;
                        eoConsumerTraceWseManager.BonusCheck = bBonusChecking;
                        eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;

                        

                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.ConsumerIDVerificationMatch(eoConsumerTraceWseManager);
                        
                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.ConsumerIDVerificationMatch(eoConsumerTraceWseManager);
                        }

                        rp.EnquiryID = intSubscriberEnquiryID;

                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);
                        
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            eSC.SearchOutput = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.DetailsViewedYN = false;
                            eSC.Billable = false;
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;

                            rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                            rp.EnquiryID = eSC.SubscriberEnquiryID;
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            
                                eSC.EnquiryResult = "E";
                                eSe.ErrorDescription = rp.ResponseData.ToString();
                                dSe.UpdateSubscriberEnquiryError(con, eSe);
                            

                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            ds.ReadXml(xmlSR);
                            if (ds.Tables.Contains("ConsumerDetail"))
                            {
                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, intProductId);

                                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);
                                rXml = ds.GetXml();
                                rp.ResponseData = rXml;

                                foreach (DataRow r in ds.Tables["ConsumerDetail"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    eSC.Billable = true;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();

                                    rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                                }
                            }
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {

                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }
                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;                                    
                                    if (int.Parse(r["HomeAffairsID"].ToString()) > 0)
                                    {
                                        eSC.KeyID = int.Parse(r["HomeAffairsID"].ToString());
                                    }
                                    else
                                    {
                                        eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                    }
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {
                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");


                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);

                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;
                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);
                                            }
                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }

                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    if (int.Parse(r["HomeAffairsID"].ToString()) > 0)
                                    {
                                        eSC.KeyID = int.Parse(r["HomeAffairsID"].ToString());
                                    }
                                    else
                                    {
                                        eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                    }
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {

                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");

                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;

                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;

                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);

                                            }

                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }


                        }
                       
                    }

                }
                 else
                 {
                     rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                     rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                 }
                 con.Close();
                 AdminConnection.Close();
                 SqlConnection.ClearPool(con);
                 SqlConnection.ClearPool(AdminConnection);

            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;


                MatchResult = eSe.SubscriberEnquiryID;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitIdentityVerificationConvertResponseidv(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string strIDNo, string strSurname, string strFirstName, string strSecondName, DateTime dtBirthDate, string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode, string strDHAURL)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            string rXml = "";
            int MatchResult = 0;
            DataSet ds = new DataSet();
            int intSubscriberEnquiryResultID = 0;
            Boolean boolSubmitEnquiry = true;
            string strValidationStatus = "";

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);

            try
            {
                if (spr.ReportID > 0)
                {

                    // Check the PayAsYouGo settings for the subscriber
                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {

                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;
                        eSe.BirthDate = dtBirthDate;
                        eSe.FirstName = strFirstName;
                        eSe.IDNo = strIDNo;
                        eSe.ProductID = intProductId;
                        eSe.SecondName = strSecondName;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = strExtRef;
                        eSe.Surname = strSurname;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";

                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.Surname))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.Surname;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);
                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        rp = SubmitRealTimeIdentityVerificationCustom9(con, AdminConnection, intSubscriberID, intSystemUserID, 182, strSubscriberName, strIDNo,
                          strSurname, strFirstName, strSecondName, dtBirthDate, strExtRef, bConfirmationChkBox, bBonusChecking,
                          strVoucherCode, strDHAURL);

                        rp.EnquiryID = intSubscriberEnquiryID;

                        rXml = rp.ResponseData;

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            if (!rXml.Contains("RealTimeIDV"))
                            {
                                // File.AppendAllText(@"C:\Log\voom.txt", "Enter5");

                                eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                                eoConsumerTraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                                eoConsumerTraceWseManager.ConfirmationChkBox = bConfirmationChkBox;
                                eoConsumerTraceWseManager.DOB = dtBirthDate;
                                eoConsumerTraceWseManager.ExternalReference = strExtRef;
                                eoConsumerTraceWseManager.Firstname = strFirstName;
                                eoConsumerTraceWseManager.IDno = strIDNo;
                                eoConsumerTraceWseManager.ProductID = intProductId;
                                eoConsumerTraceWseManager.SecondName = strSecondName;
                                eoConsumerTraceWseManager.subscriberID = intSubscriberID;
                                eoConsumerTraceWseManager.Surname = strSurname;
                                eoConsumerTraceWseManager.BonusCheck = bBonusChecking;
                                eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;

                                // File.AppendAllText(@"C:\Log\voom.txt", "Enter10");

                                XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.ConsumerIDVerificationMatch(eoConsumerTraceWseManager);

                                //If error raise one last call with a different connection
                                if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                                {
                                    eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                    ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                    rp = ra.ConsumerIDVerificationMatch(eoConsumerTraceWseManager);
                                }

                                rp.EnquiryID = intSubscriberEnquiryID;

                                rXml = rp.ResponseData;


                            }
                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            // File.AppendAllText(@"C:\Log\voom.txt", "Enter1");
                            DataSet dsT = new DataSet();
                            // File.AppendAllText(@"C:\Log\voom.txt", "Enter2");
                            System.IO.StringReader xmlSRt = new System.IO.StringReader(rXml);
                            // File.AppendAllText(@"C:\Log\voom.txt", "Enter3");
                            dsT.ReadXml(xmlSRt);
                            // File.AppendAllText(@"C:\Log\voom.txt", "Enter4");
                            // File.AppendAllText(@"C:\Log\voom.txt", rXml);
                            if (!dsT.Tables.Contains("RealTimeIDV"))
                            {
                                // File.AppendAllText(@"C:\Log\voom.txt", "Enter5");

                                eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                                eoConsumerTraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                                eoConsumerTraceWseManager.ConfirmationChkBox = bConfirmationChkBox;
                                eoConsumerTraceWseManager.DOB = dtBirthDate;
                                eoConsumerTraceWseManager.ExternalReference = strExtRef;
                                eoConsumerTraceWseManager.Firstname = strFirstName;
                                eoConsumerTraceWseManager.IDno = strIDNo;
                                eoConsumerTraceWseManager.ProductID = intProductId;
                                eoConsumerTraceWseManager.SecondName = strSecondName;
                                eoConsumerTraceWseManager.subscriberID = intSubscriberID;
                                eoConsumerTraceWseManager.Surname = strSurname;
                                eoConsumerTraceWseManager.BonusCheck = bBonusChecking;
                                eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;

                                // File.AppendAllText(@"C:\Log\voom.txt", "Enter10");

                                XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.ConsumerIDVerificationMatch(eoConsumerTraceWseManager);

                                //If error raise one last call with a different connection
                                if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                                {
                                    eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                    ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                    rp = ra.ConsumerIDVerificationMatch(eoConsumerTraceWseManager);
                                }

                                rp.EnquiryID = intSubscriberEnquiryID;

                                rXml = rp.ResponseData;


                            }
                        }
                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            eSC.SearchOutput = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.DetailsViewedYN = false;
                            eSC.Billable = false;
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;

                            rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                            rp.EnquiryID = eSC.SubscriberEnquiryID;
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);


                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            // File.AppendAllText(@"C:\Log\voom.txt", "Enter here 1");
                            ds.ReadXml(xmlSR);
                            // File.AppendAllText(@"C:\Log\voom.txt", "Enter here");
                            if (ds.Tables.Contains("RealTimeIDV"))
                            {
                                // File.AppendAllText(@"C:\Log\voom.txt", "inside");

                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, intProductId);



                                DataSet dsFinal = new DataSet();
                                foreach (DataRow r in ds.Tables["RealTimeIDV"].Rows)
                                {
                                    var haerror = r["HAErrorDescription"];
                                    // File.AppendAllText(@"C:\Log\voom.txt", haerror.ToString());
                                    if (haerror != null && haerror.ToString() != string.Empty && r["HAErrorDescription"].ToString() == "Input ID does not exist on Home Affairs")
                                    {

                                        eSC.SearchOutput = "";
                                        eSC.IDNo = "";
                                        eSC.PassportNo = "";
                                        eSC.Surname = "";
                                        eSC.FirstName = "";
                                        eSC.BirthDate = DateTime.Parse("1900/01/01");
                                        eSC.Gender = "";
                                        eSC.DetailsViewedYN = false;
                                        eSC.Billable = false;
                                        eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                        eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                                        eSC.CreatedByUser = eSe.CreatedByUser;
                                        eSC.CreatedOnDate = DateTime.Now;

                                        rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                                        //  File.AppendAllText(@"C:\Log\voom.txt", "Done");
                                    }
                                    else
                                    {
                                        DataTable dtCosnumerDetails = new DataTable("ConsumerDetails");
                                        dtCosnumerDetails.Columns.Add("ConsumerID", typeof(String));
                                        dtCosnumerDetails.Columns.Add("HomeAffairsID", typeof(String));
                                        dtCosnumerDetails.Columns.Add("Idno", typeof(String));
                                        dtCosnumerDetails.Columns.Add("FirstName", typeof(String));
                                        dtCosnumerDetails.Columns.Add("SecondName", typeof(String));
                                        dtCosnumerDetails.Columns.Add("Surname", typeof(String));
                                        dtCosnumerDetails.Columns.Add("DeceasedStatus", typeof(String));
                                        dtCosnumerDetails.Columns.Add("DeceasedDate", typeof(DateTime));
                                        dtCosnumerDetails.Columns.Add("IDIssuedDate", typeof(DateTime));
                                        dtCosnumerDetails.Columns.Add("CauseOfDeath", typeof(String));
                                        dtCosnumerDetails.Columns.Add("BonusXML", typeof(String));
                                        dtCosnumerDetails.Columns.Add("TempReference", typeof(String));
                                        dtCosnumerDetails.Columns.Add("EnquiryID", typeof(String));
                                        dtCosnumerDetails.Columns.Add("EnquiryResultID", typeof(String));
                                        dtCosnumerDetails.Columns.Add("Reference", typeof(String));

                                        DataRow drCosnumerDetails;
                                        drCosnumerDetails = dtCosnumerDetails.NewRow();
                                        drCosnumerDetails["BonusXML"] = string.Empty;
                                        drCosnumerDetails["TempReference"] = string.Empty;
                                        drCosnumerDetails["ConsumerID"] = "0";
                                        drCosnumerDetails["HomeAffairsID"] = intSubscriberEnquiryID;
                                        drCosnumerDetails["Idno"] = r["HAIDNO"].ToString();
                                        drCosnumerDetails["FirstName"] = string.Empty;
                                        drCosnumerDetails["SecondName"] = string.Empty;
                                        if (!string.IsNullOrEmpty(r["HANames"].ToString()))
                                        {
                                            int i = 0;
                                            foreach (string s in r["HANames"].ToString().Split(new char[] { ' ' }))
                                            {
                                                if (!string.IsNullOrEmpty(s.Trim()))
                                                {
                                                    if (i == 0)
                                                    {
                                                        drCosnumerDetails["FirstName"] = s;
                                                    }
                                                    else if (i == 1)
                                                    {
                                                        drCosnumerDetails["SecondName"] = s;
                                                    }

                                                    i++;
                                                }
                                            }

                                            drCosnumerDetails["SecondName"] = drCosnumerDetails["SecondName"].ToString().Trim();
                                            drCosnumerDetails["FirstName"] = drCosnumerDetails["FirstName"].ToString().Trim();
                                        }


                                        drCosnumerDetails["Surname"] = r["HASurname"].ToString();
                                        drCosnumerDetails["DeceasedStatus"] = r["HADeceasedStatus"].ToString().ToLower() == "alive" ? "Active" : r["HADeceasedStatus"].ToString();
                                        if (!string.IsNullOrEmpty(r["HADeceasedDate"].ToString()))
                                        {
                                            drCosnumerDetails["DeceasedDate"] = r["HADeceasedDate"].ToString();
                                        }
                                        if (!string.IsNullOrEmpty(r["HAIDBookIssuedDate"].ToString()))
                                        {
                                            drCosnumerDetails["IDIssuedDate"] = r["HAIDBookIssuedDate"].ToString();
                                        }
                                        drCosnumerDetails["CauseOfDeath"] = string.Empty;
                                        drCosnumerDetails["EnquiryID"] = rp.EnquiryID;
                                        drCosnumerDetails["EnquiryResultID"] = rp.EnquiryResultID;
                                        drCosnumerDetails["Reference"] = "H" + intSubscriberEnquiryID.ToString() + "-" + intSubscriberEnquiryID.ToString();

                                        dtCosnumerDetails.Rows.Add(drCosnumerDetails);


                                        dsFinal = new DataSet("ListOfConsumers");
                                        dsFinal.Tables.Add(dtCosnumerDetails);

                                        rXml = dsFinal.GetXml();
                                        rp.ResponseData = rXml;

                                        eSC.SearchOutput = "";
                                        eSC.IDNo = "";
                                        eSC.PassportNo = "";
                                        eSC.Surname = "";
                                        eSC.FirstName = "";
                                        eSC.BirthDate = DateTime.Parse("1900/01/01");
                                        eSC.Gender = "";
                                        eSC.DetailsViewedDate = DateTime.Now;
                                        eSC.DetailsViewedYN = true;
                                        eSC.Billable = true;
                                        eSC.CreatedByUser = eSe.CreatedByUser;
                                        eSC.CreatedOnDate = DateTime.Now;
                                        eSC.SearchOutput = "";

                                        if (r.Table.Columns.Contains("HAIDNO"))
                                        {
                                            if (!string.IsNullOrEmpty(r["HAIDNO"].ToString()))
                                            {
                                                eSC.IDNo = r["HAIDNO"].ToString();
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                            }
                                        }

                                        if (r.Table.Columns.Contains("HANames"))
                                        {
                                            if (!string.IsNullOrEmpty(r["HANames"].ToString()))
                                            {
                                                eSC.FirstName = r["HANames"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                            }
                                        }

                                        if (r.Table.Columns.Contains("HASurname"))
                                        {
                                            if (!string.IsNullOrEmpty(r["HASurname"].ToString()))
                                            {
                                                eSC.Surname = r["HASurname"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                            }
                                        }

                                        if (eSC.SearchOutput.Length > 0)
                                        {
                                            eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                        }

                                        //eSC.KeyID = intSubscriberEnquiryID;
                                        eSC.KeyType = "R";
                                        eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                        eSC.BillingTypeID = spr.BillingTypeID;
                                        eSC.BillingPrice = spr.UnitPrice;
                                        //eSC.Reference = "R-" + intSubscriberEnquiryID.ToString();
                                        eSC.ProductID = intProductId;

                                        eSC.XMLData = rXml.Replace("'", "''");
                                        eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();

                                        rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                                        dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);

                                        dsFinal.Tables[0].Rows[0]["EnquiryResultID"] = rp.EnquiryResultID;
                                        rXml = dsFinal.GetXml();
                                        rp.ResponseData = rXml;



                                    }


                                }
                                rp.ResponseData = dsFinal.GetXml();
                            }



                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {

                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }
                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    if (int.Parse(r["HomeAffairsID"].ToString()) > 0)
                                    {
                                        eSC.KeyID = int.Parse(r["HomeAffairsID"].ToString());
                                    }
                                    else
                                    {
                                        eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                    }
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;
                                    eSC.DetailsViewedDate = DateTime.Now;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {
                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");


                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);

                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;
                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);
                                            }
                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;
                                    eSC.XMLData = rp.ResponseData;
                                    eSC.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;

                                    // eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    dSC.UpdateSubscriberEnquiryResult(con, eSC);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }

                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    if (int.Parse(r["HomeAffairsID"].ToString()) > 0)
                                    {
                                        eSC.KeyID = int.Parse(r["HomeAffairsID"].ToString());
                                    }
                                    else
                                    {
                                        eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                    }
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {

                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");

                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;

                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;

                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);

                                            }

                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }


                        }

                    }

                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }
                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.StackTrace.ToLower().Substring(oException.StackTrace.ToLower().IndexOf("xdsportalenquiry"), 100) + oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;


                MatchResult = eSe.SubscriberEnquiryID;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitRealTimeIdentityVerificationCustom9(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string strIDNo, string strSurname, string strFirstName, string strSecondName, DateTime dtBirthDate, string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode, string strDHAURL)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            string rXml = "";
            int MatchResult = 0;
            DataSet ds = new DataSet();
            int intSubscriberEnquiryResultID = 0;
            Boolean boolSubmitEnquiry = true;
            string strValidationStatus = "";

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);

            try
            {
                if (spr.ReportID > 0)
                {

                    // Check the PayAsYouGo settings for the subscriber
                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {
                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;
                        eSe.BirthDate = dtBirthDate;
                        eSe.FirstName = strFirstName;
                        eSe.IDNo = strIDNo;
                        eSe.ProductID = intProductId;
                        eSe.SecondName = strSecondName;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = strExtRef;
                        eSe.Surname = strSurname;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";

                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.FirstName))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.FirstName;
                        }
                        if (!string.IsNullOrEmpty(eSe.Surname))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.Surname;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);
                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoConsumerTraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoConsumerTraceWseManager.IDno = eSe.IDNo;
                        eoConsumerTraceWseManager.BonusCheck = bBonusChecking;
                        eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;
                        eoConsumerTraceWseManager.subscriberID = sub.SubscriberID;
                        eoConsumerTraceWseManager.DHAURl = strDHAURL;

                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.GetDHAIDReport(eoConsumerTraceWseManager);

                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.GetDHAIDReport(eoConsumerTraceWseManager);
                        }

                        rp.EnquiryID = intSubscriberEnquiryID;

                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            eSC.SearchOutput = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.DetailsViewedYN = false;
                            eSC.Billable = false;
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;

                            rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                            rp.EnquiryID = eSC.SubscriberEnquiryID;
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);


                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            ds.ReadXml(xmlSR);
                            if (ds.Tables.Contains("RealTimeIDV"))
                            {
                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, intProductId);

                                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryReference", typeof(String));
                                dtSubscriberInput.Columns.Add("YourReference", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();
                                drSubscriberInput["EnquiryReference"] = "R-" + intSubscriberEnquiryID.ToString();
                                drSubscriberInput["YourReference"] = strExtRef;

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);
                                rXml = ds.GetXml();
                                rp.ResponseData = rXml;

                                foreach (DataRow r in ds.Tables["RealTimeIDV"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("HAIDNO"))
                                    {
                                        if (!string.IsNullOrEmpty(r["HAIDNO"].ToString()))
                                        {
                                            eSC.IDNo = r["HAIDNO"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("HANames"))
                                    {
                                        if (!string.IsNullOrEmpty(r["HANames"].ToString()))
                                        {
                                            eSC.FirstName = r["HANames"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("HASurname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["HASurname"].ToString()))
                                        {
                                            eSC.Surname = r["HASurname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    //eSC.KeyID = intSubscriberEnquiryID;
                                    eSC.KeyType = "R";
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;
                                    //eSC.Reference = "R-" + intSubscriberEnquiryID.ToString();
                                    eSC.ProductID = intProductId;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();

                                    rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                                }
                            }
                        }

                    }

                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }
                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.StackTrace.ToLower().Substring(oException.StackTrace.ToLower().IndexOf("xdsportalenquiry"), 100) + oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;


                MatchResult = eSe.SubscriberEnquiryID;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitRealTimeIdentityVerification(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string strIDNo, string strSurname, string strFirstName, string strSecondName, DateTime dtBirthDate, string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode, string strDHAURL)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            string rXml = "";
            int MatchResult = 0;
            DataSet ds = new DataSet();
            int intSubscriberEnquiryResultID = 0;
            Boolean boolSubmitEnquiry = true;
            string strValidationStatus = "";

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);

            try
            {
                if (spr.ReportID > 0)
                {

                    // Check the PayAsYouGo settings for the subscriber
                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {
                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;
                        eSe.BirthDate = dtBirthDate;
                        eSe.FirstName = strFirstName;
                        eSe.IDNo = strIDNo;
                        eSe.ProductID = intProductId;
                        eSe.SecondName = strSecondName;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = strExtRef;
                        eSe.Surname = strSurname;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";

                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.FirstName))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.FirstName;
                        }
                        if (!string.IsNullOrEmpty(eSe.Surname))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.Surname;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);
                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;
                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoConsumerTraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoConsumerTraceWseManager.IDno = eSe.IDNo;
                        eoConsumerTraceWseManager.BonusCheck = bBonusChecking;
                        eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;
                        eoConsumerTraceWseManager.subscriberID = sub.SubscriberID;
                        eoConsumerTraceWseManager.DHAURl = strDHAURL;
                        eoConsumerTraceWseManager.ProductID = eSe.ProductID;

                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.GetDHAIDReport(eoConsumerTraceWseManager);

                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.GetDHAIDReport(eoConsumerTraceWseManager);
                        }

                        rp.EnquiryID = intSubscriberEnquiryID;

                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                        //Start of Change: Call internal Home Affairs DB
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            ds.ReadXml(xmlSR);

                            if (!ds.Tables.Contains("RealTimeIDV"))
                            {
                                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                                rp.ResponseData = "DHA link down.";
                            }

                            if (ds.Tables.Contains("RealTimeIDV") && string.IsNullOrEmpty(ds.Tables["RealTimeIDV"].Rows[0]["InputIDNO"].ToString()))
                            {
                                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                                rp.ResponseData = "DHA link down.";
                            }
                        }
                        //End of Change

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            eSC.SearchOutput = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.DetailsViewedYN = false;
                            eSC.Billable = false;
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;

                            rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                            rp.EnquiryID = eSC.SubscriberEnquiryID;
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);

                            //Start of Change: Call internal Home Affairs DB
                            if (sub.RealTimeIDVOverride)
                            {
                                System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();
                                rp = SubmitIdentityVerification(con, AdminConnection, sub.SubscriberID, sys.SystemUserID, 9, sub.SubscriberName, strIDNo, "", "", "", DateTime.Now, strExtRef, true, true, strVoucherCode);
                                rXml = rp.ResponseData;
                                xmlSR = new System.IO.StringReader(rXml);

                                switch (rp.ResponseStatus)
                                {
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single:
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple:
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                                        ds.ReadXml(xmlSR);
                                        if (ds.Tables.Contains("ConsumerDetails"))
                                        {
                                            //foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                            //{
                                            string EnquiryID, EnquiryResultID;

                                            EnquiryID = ds.Tables["ConsumerDetails"].Rows[0]["EnquiryID"].ToString();
                                            EnquiryResultID = ds.Tables["ConsumerDetails"].Rows[0]["EnquiryResultID"].ToString();

                                            XDSPortalEnquiry.Business.MultipleTrace_IdentityVerification dtMultipleID = new XDSPortalEnquiry.Business.MultipleTrace_IdentityVerification();
                                            rp = dtMultipleID.SubmitMultipleIdentityVerification(con, AdminConnection, int.Parse(EnquiryID), int.Parse(EnquiryResultID), null, true, strVoucherCode);

                                            rXml = rp.ResponseData;
                                            xmlSR = new System.IO.StringReader(rXml);
                                            DataSet ds1 = new DataSet();
                                            ds1.ReadXml(xmlSR);

                                            if (ds1.Tables.Contains("ConsumerDetail"))
                                            {
                                                DataSet dsDataSegment = new DataSet();
                                                dsDataSegment.ReadXml(new StringReader(eSe.ExtraVarInput2));
                                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, eSe.ProductID);

                                                DataSet dsReport = new DataSet("HomeAffairs");

                                                DataTable dtReportInformation = new DataTable("ReportInformation");
                                                dtReportInformation.Columns.Add("ReportID", typeof(String));
                                                dtReportInformation.Columns.Add("ReportName", typeof(String));
                                                DataRow drReportInformation;
                                                drReportInformation = dtReportInformation.NewRow();

                                                drReportInformation["ReportID"] = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();
                                                drReportInformation["ReportName"] = dsDataSegment.Tables[0].Rows[0]["ReportName"].ToString();

                                                dtReportInformation.Rows.Add(drReportInformation);

                                                dsReport.Tables.Add(dtReportInformation);

                                                DataTable dtRealTimeIDV = new DataTable("RealTimeIDV");
                                                dtRealTimeIDV.Columns.Add("InputIDNO", typeof(String));
                                                dtRealTimeIDV.Columns.Add("HAIDNO", typeof(String));
                                                dtRealTimeIDV.Columns.Add("IDNOMatchStatus", typeof(String));
                                                dtRealTimeIDV.Columns.Add("HANames", typeof(String));
                                                dtRealTimeIDV.Columns.Add("HASurname", typeof(String));
                                                dtRealTimeIDV.Columns.Add("HADateOfBirth", typeof(DateTime));
                                                dtRealTimeIDV.Columns.Add("HADeceasedStatus", typeof(String));
                                                dtRealTimeIDV.Columns.Add("HADeceasedDate", typeof(String));
                                                dtRealTimeIDV.Columns.Add("HAIDBookIssuedDate", typeof(String));
                                                dtRealTimeIDV.Columns.Add("HAErrorDescription", typeof(String));
                                                dtRealTimeIDV.Columns.Add("IdCardInd", typeof(String));
                                                dtRealTimeIDV.Columns.Add("IdCardDate", typeof(String));
                                                dtRealTimeIDV.Columns.Add("IdBlocked", typeof(String));
                                                dtRealTimeIDV.Columns.Add("DeathPlace", typeof(String));
                                                dtRealTimeIDV.Columns.Add("CauseOfDeath", typeof(String));
                                                DataRow drRealTimeIDV;
                                                drRealTimeIDV = dtRealTimeIDV.NewRow();


                                                drRealTimeIDV["InputIDNO"] = ds1.Tables["ConsumerDetail"].Rows[0]["IDNo"].ToString();
                                                drRealTimeIDV["HAIDNO"] = ds1.Tables["ConsumerDetail"].Rows[0]["IDNo"].ToString();
                                                drRealTimeIDV["IDNOMatchStatus"] = "Matched";
                                                string Name = ds1.Tables["ConsumerDetail"].Rows[0]["FirstName"].ToString();
                                                if (ds1.Tables["ConsumerDetail"].Columns.Contains("SecondName"))
                                                {
                                                    Name = Name + " " + ds1.Tables["ConsumerDetail"].Rows[0]["SecondName"].ToString();
                                                }
                                                if (ds1.Tables["ConsumerDetail"].Columns.Contains("ThirdName"))
                                                {
                                                    Name = Name + " " + ds1.Tables["ConsumerDetail"].Rows[0]["ThirdName"].ToString();
                                                }
                                                drRealTimeIDV["HANames"] = Name;
                                                drRealTimeIDV["HASurname"] = ds1.Tables["ConsumerDetail"].Rows[0]["Surname"].ToString();
                                                drRealTimeIDV["HADateOfBirth"] = ds1.Tables["ConsumerDetail"].Rows[0]["BirthDate"].ToString();
                                                if (ds1.Tables["ConsumerDetail"].Rows[0]["XDSDeceasedStatus"].ToString() == "Active")
                                                {
                                                    drRealTimeIDV["HADeceasedStatus"] = "Alive";
                                                }
                                                else
                                                {
                                                    drRealTimeIDV["HADeceasedStatus"] = "Deceased";
                                                }
                                                drRealTimeIDV["HADeceasedDate"] = ds1.Tables["ConsumerDetail"].Rows[0]["XDSDeceasedDate"].ToString();
                                                drRealTimeIDV["HAIDBookIssuedDate"] = "";
                                                drRealTimeIDV["HAErrorDescription"] = "";
                                                drRealTimeIDV["IdCardInd"] = "";
                                                drRealTimeIDV["IdCardDate"] = "";
                                                drRealTimeIDV["IdBlocked"] = "false"; 
                                                drRealTimeIDV["DeathPlace"] = "";
                                                drRealTimeIDV["CauseOfDeath"] = "";

                                                dtRealTimeIDV.Rows.Add(drRealTimeIDV);

                                                dsReport.Tables.Add(dtRealTimeIDV);

                                                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                                dtSubscriberInput.Columns.Add("EnquiryReference", typeof(String));
                                                dtSubscriberInput.Columns.Add("YourReference", typeof(String));
                                                DataRow drSubscriberInput;
                                                drSubscriberInput = dtSubscriberInput.NewRow();

                                                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                                drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();
                                                drSubscriberInput["EnquiryReference"] = ds1.Tables["ConsumerDetail"].Rows[0]["ReferenceNo"].ToString();
                                                drSubscriberInput["YourReference"] = strExtRef;

                                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                                dsReport.Tables.Add(dtSubscriberInput);

                                                rXml = dsReport.GetXml();
                                                rp.ResponseData = rXml;
                                                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                                            }
                                            //}
                                        }
                                        break;
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                                        rXml = "<NoResult><Error>DHA link is down</Error></NoResult>";
                                        rsXml.LoadXml(rXml);
                                        break;
                                }
                            }
                            //End of Change
                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            //ds.ReadXml(xmlSR);
                            if (ds.Tables.Contains("RealTimeIDV"))
                            {
                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, intProductId);

                                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryReference", typeof(String));
                                dtSubscriberInput.Columns.Add("YourReference", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();
                                drSubscriberInput["EnquiryReference"] = "R-" + intSubscriberEnquiryID.ToString();
                                drSubscriberInput["YourReference"] = strExtRef;

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);
                                rXml = ds.GetXml();
                                rp.ResponseData = rXml;

                                foreach (DataRow r in ds.Tables["RealTimeIDV"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    eSC.Billable = true;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("HAIDNO"))
                                    {
                                        if (!string.IsNullOrEmpty(r["HAIDNO"].ToString()))
                                        {
                                            eSC.IDNo = r["HAIDNO"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("HANames"))
                                    {
                                        if (!string.IsNullOrEmpty(r["HANames"].ToString()))
                                        {
                                            eSC.FirstName = r["HANames"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("HASurname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["HASurname"].ToString()))
                                        {
                                            eSC.Surname = r["HASurname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                 

                                    //eSC.KeyID = intSubscriberEnquiryID;
                                    eSC.KeyType = "R";
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;
                                    //eSC.Reference = "R-" + intSubscriberEnquiryID.ToString();
                                    eSC.ProductID = intProductId;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();

                                    rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                                }
                            }
                        }

                    }

                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }
                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;


                MatchResult = eSe.SubscriberEnquiryID;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitRealTimeIdentityVerification(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string strIDNo, string strSurname, string strFirstName, string strSecondName, DateTime dtBirthDate, string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode, string strDHAURL, string SolutionTransactionID)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            string rXml = "";
            int MatchResult = 0;
            DataSet ds = new DataSet();
            int intSubscriberEnquiryResultID = 0;
            Boolean boolSubmitEnquiry = true;
            string strValidationStatus = "";

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);

            try
            {
                if (spr.ReportID > 0)
                {

                    // Check the PayAsYouGo settings for the subscriber
                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {
                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;
                        eSe.BirthDate = dtBirthDate;
                        eSe.FirstName = strFirstName;
                        eSe.IDNo = strIDNo;
                        eSe.ProductID = intProductId;
                        eSe.SecondName = strSecondName;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = strExtRef;
                        eSe.Surname = strSurname;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";

                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.FirstName))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.FirstName;
                        }
                        if (!string.IsNullOrEmpty(eSe.Surname))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.Surname;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);
                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        //Submit to MDP
                        MDPEnquiryLog mdpLog = new MDPEnquiryLog();
                        XDSPortalLibrary.Entity_Layer.MDPResponse mdpResp;

                        if (string.IsNullOrEmpty(SolutionTransactionID))
                        {
                            mdpResp = mdpLog.SubmitToMDP(eSe.ProductID.ToString(), ConfigurationManager.AppSettings["MDPIDVProductName"].ToString(), eSe.SubscriberID.ToString(), eSe.SubscriberName, intSubscriberEnquiryID.ToString());
                        }
                        else
                        {
                            mdpResp = mdpLog.SubmitToMDP(eSe.ProductID.ToString(), ConfigurationManager.AppSettings["MDPIDVProductName"].ToString(), eSe.SubscriberID.ToString(), eSe.SubscriberName, intSubscriberEnquiryID.ToString(), SolutionTransactionID);
                        }

                        if (mdpResp.response.ProductTransactionId != "")
                        {
                            XDSPortalEnquiry.Data.SubscriberEnquiry se = new Data.SubscriberEnquiry();
                            se.InsertSubscriberEnquiryMDP(con, intSubscriberEnquiryID, mdpResp.response.ProductTransactionId, mdpResp.response.Message, eSe.CreatedByUser, DateTime.Now);
                        }

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoConsumerTraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoConsumerTraceWseManager.IDno = eSe.IDNo;
                        eoConsumerTraceWseManager.BonusCheck = bBonusChecking;
                        eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;
                        eoConsumerTraceWseManager.subscriberID = sub.SubscriberID;
                        eoConsumerTraceWseManager.DHAURl = strDHAURL;
                        eoConsumerTraceWseManager.ProductID = eSe.ProductID;

                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.GetDHAIDReport(eoConsumerTraceWseManager);

                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.GetDHAIDReport(eoConsumerTraceWseManager);
                        }

                        rp.EnquiryID = intSubscriberEnquiryID;

                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            eSC.SearchOutput = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.DetailsViewedYN = false;
                            eSC.Billable = false;
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;

                            rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                            rp.EnquiryID = eSC.SubscriberEnquiryID;
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);

                            rp.ResponseData = "DHA link is down";
                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            ds.ReadXml(xmlSR);
                            if (ds.Tables.Contains("RealTimeIDV"))
                            {
                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, intProductId);

                                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryReference", typeof(String));
                                dtSubscriberInput.Columns.Add("YourReference", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();
                                drSubscriberInput["EnquiryReference"] = "R-" + intSubscriberEnquiryID.ToString();
                                drSubscriberInput["YourReference"] = strExtRef;

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);
                                rXml = ds.GetXml();
                                rp.ResponseData = rXml;

                                foreach (DataRow r in ds.Tables["RealTimeIDV"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    eSC.Billable = true;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("HAIDNO"))
                                    {
                                        if (!string.IsNullOrEmpty(r["HAIDNO"].ToString()))
                                        {
                                            eSC.IDNo = r["HAIDNO"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("HANames"))
                                    {
                                        if (!string.IsNullOrEmpty(r["HANames"].ToString()))
                                        {
                                            eSC.FirstName = r["HANames"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("HASurname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["HASurname"].ToString()))
                                        {
                                            eSC.Surname = r["HASurname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }



                                    //eSC.KeyID = intSubscriberEnquiryID;
                                    eSC.KeyType = "R";
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;
                                    //eSC.Reference = "R-" + intSubscriberEnquiryID.ToString();
                                    eSC.ProductID = intProductId;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();

                                    rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                                }
                            }
                        }

                    }

                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }
                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;


                MatchResult = eSe.SubscriberEnquiryID;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitRealTimeHistory(SqlConnection con, SqlConnection AdminConnection, int intSubscriberEnquiryResultID)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                eSC = dSC.GetSubscriberEnquiryResultObject(con, intSubscriberEnquiryResultID);

                if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                    rp.ResponseData = eSC.XMLData;
                    rp.EnquiryID = eSC.SubscriberEnquiryID;
                    rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }

            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitRealTimeMaritalVerification(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string strIDNo, string strSurname, string strFirstName, string strSecondName, DateTime dtBirthDate, string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode, string strDHAURL)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            string rXml = "";
            int MatchResult = 0;
            DataSet ds = new DataSet();
            int intSubscriberEnquiryResultID = 0;
            Boolean boolSubmitEnquiry = true;
            string strValidationStatus = "";

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);

            try
            {
                if (spr.ReportID > 0)
                {

                    // Check the PayAsYouGo settings for the subscriber
                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {
                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;
                        eSe.BirthDate = dtBirthDate;
                        eSe.FirstName = strFirstName;
                        eSe.IDNo = strIDNo;
                        eSe.ProductID = intProductId;
                        eSe.SecondName = strSecondName;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = strExtRef;
                        eSe.Surname = strSurname;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";

                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.FirstName))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.FirstName;
                        }
                        if (!string.IsNullOrEmpty(eSe.Surname))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.Surname;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);
                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoConsumerTraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoConsumerTraceWseManager.IDno = eSe.IDNo;
                        eoConsumerTraceWseManager.BonusCheck = bBonusChecking;
                        eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;
                        eoConsumerTraceWseManager.subscriberID = sub.SubscriberID;
                        eoConsumerTraceWseManager.DHAURl = strDHAURL;
                        eoConsumerTraceWseManager.ProductID = eSe.ProductID;

                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.GetDHAMaritalReport(eoConsumerTraceWseManager);

                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.GetDHAMaritalReport(eoConsumerTraceWseManager);
                        }

                        rp.EnquiryID = intSubscriberEnquiryID;

                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                        //Start of Change: Call internal Home Affairs DB
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            ds.ReadXml(xmlSR);

                            if (!ds.Tables.Contains("RealTimeMaritalCheck"))
                            {
                                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                                rp.ResponseData = "DHA link down.";
                            }

                            if (ds.Tables.Contains("RealTimeMaritalCheck") && string.IsNullOrEmpty(ds.Tables["RealTimeMaritalCheck"].Rows[0]["InputIDNO"].ToString()))
                            {
                                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                                rp.ResponseData = "DHA link down.";
                            }
                        }
                        //End of Change

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            eSC.SearchOutput = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.DetailsViewedYN = false;
                            eSC.Billable = false;
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;

                            rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                            rp.EnquiryID = eSC.SubscriberEnquiryID;
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);

                            //Start of Change: Call internal Home Affairs DB
                            if (sub.RealTimeIDVOverride)
                            {
                                System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();
                                rp = SubmitIdentityVerification(con, AdminConnection, sub.SubscriberID, sys.SystemUserID, 9, sub.SubscriberName, strIDNo, "", "", "", DateTime.Now, strExtRef, true, true, strVoucherCode);
                                rXml = rp.ResponseData;
                                xmlSR = new System.IO.StringReader(rXml);

                                switch (rp.ResponseStatus)
                                {
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single:
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple:
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                                        ds.ReadXml(xmlSR);
                                        if (ds.Tables.Contains("ConsumerDetails"))
                                        {
                                            //foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                            //{
                                            string EnquiryID, EnquiryResultID;

                                            EnquiryID = ds.Tables["ConsumerDetails"].Rows[0]["EnquiryID"].ToString();
                                            EnquiryResultID = ds.Tables["ConsumerDetails"].Rows[0]["EnquiryResultID"].ToString();

                                            XDSPortalEnquiry.Business.MultipleTrace_IdentityVerification dtMultipleID = new XDSPortalEnquiry.Business.MultipleTrace_IdentityVerification();
                                            rp = dtMultipleID.SubmitMultipleIdentityVerification(con, AdminConnection, int.Parse(EnquiryID), int.Parse(EnquiryResultID), null, true, strVoucherCode);

                                            rXml = rp.ResponseData;
                                            xmlSR = new System.IO.StringReader(rXml);
                                            DataSet ds1 = new DataSet();
                                            ds1.ReadXml(xmlSR);

                                            if (ds1.Tables.Contains("ConsumerDetail"))
                                            {
                                                DataSet dsDataSegment = new DataSet();
                                                dsDataSegment.ReadXml(new StringReader(eSe.ExtraVarInput2));
                                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, eSe.ProductID);

                                                DataSet dsReport = new DataSet("HomeAffairs");

                                                DataTable dtReportInformation = new DataTable("ReportInformation");
                                                dtReportInformation.Columns.Add("ReportID", typeof(String));
                                                dtReportInformation.Columns.Add("ReportName", typeof(String));
                                                DataRow drReportInformation;
                                                drReportInformation = dtReportInformation.NewRow();

                                                drReportInformation["ReportID"] = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();
                                                drReportInformation["ReportName"] = dsDataSegment.Tables[0].Rows[0]["ReportName"].ToString();

                                                dtReportInformation.Rows.Add(drReportInformation);

                                                dsReport.Tables.Add(dtReportInformation);

                                                DataTable dtRealTimeIDV = new DataTable("RealTimeMaritalCheck");
                                                dtRealTimeIDV.Columns.Add("InputIDNO", typeof(String));
                                                dtRealTimeIDV.Columns.Add("HAIDNO", typeof(String));
                                                dtRealTimeIDV.Columns.Add("IDNOMatchStatus", typeof(String));
                                                dtRealTimeIDV.Columns.Add("HANames", typeof(String));
                                                dtRealTimeIDV.Columns.Add("HASurname", typeof(String));
                                                dtRealTimeIDV.Columns.Add("HADateOfBirth", typeof(DateTime));
                                                dtRealTimeIDV.Columns.Add("HAMaritalStatus", typeof(String));
                                                dtRealTimeIDV.Columns.Add("HAMarriageDate", typeof(String));
                                                dtRealTimeIDV.Columns.Add("HAErrorDescription", typeof(String));
                                                DataRow drRealTimeIDV;
                                                drRealTimeIDV = dtRealTimeIDV.NewRow();

                                                drRealTimeIDV["InputIDNO"] = ds1.Tables["ConsumerDetail"].Rows[0]["IDNo"].ToString();
                                                drRealTimeIDV["HAIDNO"] = ds1.Tables["ConsumerDetail"].Rows[0]["IDNo"].ToString();
                                                drRealTimeIDV["IDNOMatchStatus"] = "Matched";
                                                string Name = ds1.Tables["ConsumerDetail"].Rows[0]["FirstName"].ToString();
                                                if (ds1.Tables["ConsumerDetail"].Columns.Contains("SecondName"))
                                                {
                                                    Name = Name + " " + ds1.Tables["ConsumerDetail"].Rows[0]["SecondName"].ToString();
                                                }
                                                if (ds1.Tables["ConsumerDetail"].Columns.Contains("ThirdName"))
                                                {
                                                    Name = Name + " " + ds1.Tables["ConsumerDetail"].Rows[0]["ThirdName"].ToString();
                                                }
                                                drRealTimeIDV["HANames"] = Name;
                                                drRealTimeIDV["HASurname"] = ds1.Tables["ConsumerDetail"].Rows[0]["Surname"].ToString();
                                                drRealTimeIDV["HADateOfBirth"] = ds1.Tables["ConsumerDetail"].Rows[0]["BirthDate"].ToString();
                                                drRealTimeIDV["HAMaritalStatus"] = ds1.Tables["ConsumerDetail"].Rows[0]["MaritalStatusDesc"].ToString().ToUpper();
                                                drRealTimeIDV["HAMarriageDate"] = "";
                                                drRealTimeIDV["HAErrorDescription"] = "";

                                                dtRealTimeIDV.Rows.Add(drRealTimeIDV);

                                                dsReport.Tables.Add(dtRealTimeIDV);

                                                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                                dtSubscriberInput.Columns.Add("EnquiryReference", typeof(String));
                                                dtSubscriberInput.Columns.Add("YourReference", typeof(String));
                                                DataRow drSubscriberInput;
                                                drSubscriberInput = dtSubscriberInput.NewRow();

                                                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                                drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();
                                                drSubscriberInput["EnquiryReference"] = ds1.Tables["ConsumerDetail"].Rows[0]["ReferenceNo"].ToString();
                                                drSubscriberInput["YourReference"] = strExtRef;

                                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                                dsReport.Tables.Add(dtSubscriberInput);

                                                rXml = dsReport.GetXml();
                                                rp.ResponseData = rXml;
                                                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                                            }
                                            //}
                                        }
                                        break;
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                                        rXml = "<NoResult><Error>DHA link is down</Error></NoResult>";
                                        rsXml.LoadXml(rXml);
                                        break;
                                }
                            }
                            //End of Change
                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            //ds.ReadXml(xmlSR);
                            if (ds.Tables.Contains("RealTimeMaritalCheck"))
                            {
                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, intProductId);

                                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryReference", typeof(String));
                                dtSubscriberInput.Columns.Add("YourReference", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();
                                drSubscriberInput["EnquiryReference"] = "R-" + intSubscriberEnquiryID.ToString();
                                drSubscriberInput["YourReference"] = strExtRef;

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);
                                rXml = ds.GetXml();
                                rp.ResponseData = rXml;

                                foreach (DataRow r in ds.Tables["RealTimeMaritalCheck"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    eSC.Billable = true;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("HAIDNO"))
                                    {
                                        if (!string.IsNullOrEmpty(r["HAIDNO"].ToString()))
                                        {
                                            eSC.IDNo = r["HAIDNO"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("HANames"))
                                    {
                                        if (!string.IsNullOrEmpty(r["HANames"].ToString()))
                                        {
                                            eSC.FirstName = r["HANames"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("HASurname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["HASurname"].ToString()))
                                        {
                                            eSC.Surname = r["HASurname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    //eSC.KeyID = intSubscriberEnquiryID;
                                    eSC.KeyType = "R";
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;
                                    //eSC.Reference = "R-" + intSubscriberEnquiryID.ToString();
                                    eSC.ProductID = intProductId;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();

                                    rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                                }
                            }
                        }

                    }

                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }
                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;


                MatchResult = eSe.SubscriberEnquiryID;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitRealTimeIdentityMaritalVerification(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string strIDNo, string strSurname, string strFirstName, string strSecondName, DateTime dtBirthDate, string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode, string strDHAURL)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            string rXml = "";
            int MatchResult = 0;
            DataSet ds = new DataSet();
            int intSubscriberEnquiryResultID = 0;
            Boolean boolSubmitEnquiry = true;
            string strValidationStatus = "";

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);

            try
            {
                if (spr.ReportID > 0)
                {

                    // Check the PayAsYouGo settings for the subscriber
                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {
                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;
                        eSe.BirthDate = dtBirthDate;
                        eSe.FirstName = strFirstName;
                        eSe.IDNo = strIDNo;
                        eSe.ProductID = intProductId;
                        eSe.SecondName = strSecondName;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = strExtRef;
                        eSe.Surname = strSurname;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";

                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.FirstName))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.FirstName;
                        }
                        if (!string.IsNullOrEmpty(eSe.Surname))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.Surname;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);
                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoConsumerTraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoConsumerTraceWseManager.IDno = eSe.IDNo;
                        eoConsumerTraceWseManager.BonusCheck = bBonusChecking;
                        eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;
                        eoConsumerTraceWseManager.subscriberID = sub.SubscriberID;
                        eoConsumerTraceWseManager.DHAURl = strDHAURL;
                        eoConsumerTraceWseManager.ProductID = eSe.ProductID;

                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.GetDHAIDMaritalReport(eoConsumerTraceWseManager);

                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.GetDHAIDMaritalReport(eoConsumerTraceWseManager);
                        }

                        rp.EnquiryID = intSubscriberEnquiryID;

                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                        //Start of Change: Call internal Home Affairs DB
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            ds.ReadXml(xmlSR);

                            if (!ds.Tables.Contains("RealTimeIDVAndMaritalCheck"))
                            {
                                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                                rp.ResponseData = "DHA link down.";
                            }

                            if (ds.Tables.Contains("RealTimeIDVAndMaritalCheck") && string.IsNullOrEmpty(ds.Tables["RealTimeIDVAndMaritalCheck"].Rows[0]["InputIDNO"].ToString()))
                            {
                                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                                rp.ResponseData = "DHA link down.";
                            }
                        }
                        //End of Change

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            eSC.SearchOutput = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.DetailsViewedYN = false;
                            eSC.Billable = false;
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;

                            rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                            rp.EnquiryID = eSC.SubscriberEnquiryID;
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);

                            //Start of Change: Call internal Home Affairs DB
                            if (sub.RealTimeIDVOverride)
                            {
                                System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();
                                rp = SubmitIdentityVerification(con, AdminConnection, sub.SubscriberID, sys.SystemUserID, 9, sub.SubscriberName, strIDNo, "", "", "", DateTime.Now, strExtRef, true, true, strVoucherCode);
                                rXml = rp.ResponseData;
                                xmlSR = new System.IO.StringReader(rXml);

                                switch (rp.ResponseStatus)
                                {
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single:
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple:
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                                        ds.ReadXml(xmlSR);
                                        if (ds.Tables.Contains("ConsumerDetails"))
                                        {
                                            //foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                            //{
                                            string EnquiryID, EnquiryResultID;

                                            EnquiryID = ds.Tables["ConsumerDetails"].Rows[0]["EnquiryID"].ToString();
                                            EnquiryResultID = ds.Tables["ConsumerDetails"].Rows[0]["EnquiryResultID"].ToString();

                                            XDSPortalEnquiry.Business.MultipleTrace_IdentityVerification dtMultipleID = new XDSPortalEnquiry.Business.MultipleTrace_IdentityVerification();
                                            rp = dtMultipleID.SubmitMultipleIdentityVerification(con, AdminConnection, int.Parse(EnquiryID), int.Parse(EnquiryResultID), null, true, strVoucherCode);

                                            rXml = rp.ResponseData;
                                            xmlSR = new System.IO.StringReader(rXml);
                                            DataSet ds1 = new DataSet();
                                            ds1.ReadXml(xmlSR);

                                            if (ds1.Tables.Contains("ConsumerDetail"))
                                            {
                                                DataSet dsDataSegment = new DataSet();
                                                dsDataSegment.ReadXml(new StringReader(eSe.ExtraVarInput2));
                                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, eSe.ProductID);

                                                DataSet dsReport = new DataSet("HomeAffairs");

                                                DataTable dtReportInformation = new DataTable("ReportInformation");
                                                dtReportInformation.Columns.Add("ReportID", typeof(String));
                                                dtReportInformation.Columns.Add("ReportName", typeof(String));
                                                DataRow drReportInformation;
                                                drReportInformation = dtReportInformation.NewRow();

                                                drReportInformation["ReportID"] = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();
                                                drReportInformation["ReportName"] = dsDataSegment.Tables[0].Rows[0]["ReportName"].ToString();

                                                dtReportInformation.Rows.Add(drReportInformation);

                                                dsReport.Tables.Add(dtReportInformation);

                                                DataTable dtRealTimeIDV = new DataTable("RealTimeIDVAndMaritalCheck");
                                                dtRealTimeIDV.Columns.Add("InputIDNO", typeof(String));
                                                dtRealTimeIDV.Columns.Add("HAIDNO", typeof(String));
                                                dtRealTimeIDV.Columns.Add("IDNOMatchStatus", typeof(String));
                                                dtRealTimeIDV.Columns.Add("HANames", typeof(String));
                                                dtRealTimeIDV.Columns.Add("HASurname", typeof(String));
                                                dtRealTimeIDV.Columns.Add("HADateOfBirth", typeof(DateTime));
                                                dtRealTimeIDV.Columns.Add("HADeceasedStatus", typeof(String));
                                                dtRealTimeIDV.Columns.Add("HADeceasedDate", typeof(String));
                                                dtRealTimeIDV.Columns.Add("HAIDBookIssuedDate", typeof(String));
                                                dtRealTimeIDV.Columns.Add("HAMaritalStatus", typeof(String));
                                                dtRealTimeIDV.Columns.Add("HAMarriageDate", typeof(String));
                                                dtRealTimeIDV.Columns.Add("HAErrorDescription", typeof(String));
                                                dtRealTimeIDV.Columns.Add("IdCardInd", typeof(String));
                                                dtRealTimeIDV.Columns.Add("IdCardDate", typeof(String));
                                                if (spr.CountryofBirthActive)
                                                {
                                                    dtRealTimeIDV.Columns.Add("CountryofBirth", typeof(String));
                                                    dtRealTimeIDV.Columns.Add("IDBlocked", typeof(String));
                                                    dtRealTimeIDV.Columns.Add("Age", typeof(String));
                                                    dtRealTimeIDV.Columns.Add("Gender", typeof(String));
                                                    dtRealTimeIDV.Columns.Add("FirstName", typeof(String));
                                                    dtRealTimeIDV.Columns.Add("DeathPlace", typeof(String));
                                                    dtRealTimeIDV.Columns.Add("CauseOfDeath", typeof(String));
                                                }
                                                DataRow drRealTimeIDV;
                                                drRealTimeIDV = dtRealTimeIDV.NewRow();

                                                drRealTimeIDV["InputIDNO"] = ds1.Tables["ConsumerDetail"].Rows[0]["IDNo"].ToString();
                                                drRealTimeIDV["HAIDNO"] = ds1.Tables["ConsumerDetail"].Rows[0]["IDNo"].ToString();
                                                drRealTimeIDV["IDNOMatchStatus"] = "Matched";
                                                string Name = ds1.Tables["ConsumerDetail"].Rows[0]["FirstName"].ToString();
                                                if (ds1.Tables["ConsumerDetail"].Columns.Contains("SecondName"))
                                                {
                                                    Name = Name + " " + ds1.Tables["ConsumerDetail"].Rows[0]["SecondName"].ToString();
                                                }
                                                if (ds1.Tables["ConsumerDetail"].Columns.Contains("ThirdName"))
                                                {
                                                    Name = Name + " " + ds1.Tables["ConsumerDetail"].Rows[0]["ThirdName"].ToString();
                                                }
                                                drRealTimeIDV["HANames"] = Name;
                                                drRealTimeIDV["HASurname"] = ds1.Tables["ConsumerDetail"].Rows[0]["Surname"].ToString();
                                                drRealTimeIDV["HADateOfBirth"] = ds1.Tables["ConsumerDetail"].Rows[0]["BirthDate"].ToString();
                                                if (ds1.Tables["ConsumerDetail"].Rows[0]["XDSDeceasedStatus"].ToString() == "Active")
                                                {
                                                    drRealTimeIDV["HADeceasedStatus"] = "Alive";
                                                }
                                                else
                                                {
                                                    drRealTimeIDV["HADeceasedStatus"] = "Deceased";
                                                }
                                                drRealTimeIDV["HADeceasedDate"] = ds1.Tables["ConsumerDetail"].Rows[0]["XDSDeceasedDate"].ToString();
                                                drRealTimeIDV["HAIDBookIssuedDate"] = "";
                                                drRealTimeIDV["HAMaritalStatus"] = ds1.Tables["ConsumerDetail"].Rows[0]["MaritalStatusDesc"].ToString().ToUpper();
                                                drRealTimeIDV["HAMarriageDate"] = "";
                                                drRealTimeIDV["HAErrorDescription"] = "";
                                                drRealTimeIDV["IdCardInd"] = "";
                                                drRealTimeIDV["IdCardDate"] = "";

                                                if (spr.CountryofBirthActive)
                                                {
                                                    drRealTimeIDV["CountryofBirth"] = string.Empty;// ds1.Tables["ConsumerDetail"].Rows[0]["HAIDNO"].ToString().Substring(10, 1) == "0" ? "South African" : "Foreign National";
                                                    drRealTimeIDV["IDBlocked"] = string.Empty;
                                                    drRealTimeIDV["DeathPlace"] = string.Empty;
                                                    drRealTimeIDV["CauseOfDeath"] = string.Empty;
                                                    drRealTimeIDV["FirstName"] = ds1.Tables["ConsumerDetail"].Rows[0]["FirstName"].ToString();
                                                    drRealTimeIDV["Gender"] = ds1.Tables["ConsumerDetail"].Rows[0]["Gender"].ToString();

                                                    string birthdate = ds1.Tables["ConsumerDetail"].Rows[0]["BirthDate"].ToString();

                                                    DateTime dtBirthdate = new DateTime();

                                                    if (DateTime.TryParse(birthdate, out dtBirthdate))
                                                    {
                                                        drRealTimeIDV["Age"] = Math.Round(DateTime.Now.Subtract(dtBirthdate).Days/(365.25));
                                                    }
                                                    else
                                                    {
                                                        drRealTimeIDV["Age"] = "";
                                                    }
                                                }

                                                    dtRealTimeIDV.Rows.Add(drRealTimeIDV);

                                                dsReport.Tables.Add(dtRealTimeIDV);

                                                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                                dtSubscriberInput.Columns.Add("EnquiryReference", typeof(String));
                                                dtSubscriberInput.Columns.Add("YourReference", typeof(String));
                                                DataRow drSubscriberInput;
                                                drSubscriberInput = dtSubscriberInput.NewRow();

                                                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                                drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();
                                                drSubscriberInput["EnquiryReference"] = ds1.Tables["ConsumerDetail"].Rows[0]["ReferenceNo"].ToString();
                                                drSubscriberInput["YourReference"] = strExtRef;

                                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                                dsReport.Tables.Add(dtSubscriberInput);

                                                rXml = dsReport.GetXml();
                                                rp.ResponseData = rXml;
                                                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                                            }
                                            //}
                                        }
                                        break;
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                                        rXml = "<NoResult><Error>DHA link is down</Error></NoResult>";
                                        rsXml.LoadXml(rXml);
                                        break;
                                }
                            }
                            //End of Change
                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            //ds.ReadXml(xmlSR);
                            if (ds.Tables.Contains("RealTimeIDVAndMaritalCheck"))
                            {
                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, intProductId);

                                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryReference", typeof(String));
                                dtSubscriberInput.Columns.Add("YourReference", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();
                                drSubscriberInput["EnquiryReference"] = "R-" + intSubscriberEnquiryID.ToString();
                                drSubscriberInput["YourReference"] = strExtRef;

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);

                                if (spr.CountryofBirthActive)
                                {

                                    //DataColumn clCountryofBirth = new DataColumn("CountryofBirth", typeof(System.String));
                                    //clCountryofBirth.DefaultValue = string.Empty;// ds.Tables["RealTimeIDVAndMaritalCheck"].Rows[0]["HAIDNO"].ToString().Substring(10,1) == "0"? "South African": "Foreign National";
                                    //ds.Tables["RealTimeIDVAndMaritalCheck"].Columns.Add(clCountryofBirth);

                                    DataColumn clFirstName = new DataColumn("FirstName", typeof(System.String));

                                    string[] Names = ds.Tables["RealTimeIDVAndMaritalCheck"].Rows[0]["HANames"].ToString().Split(new char[] { ' ' });
                                    foreach (string name in Names)
                                    {
                                        clFirstName.DefaultValue = name.Trim();
                                        break;
                                    }
                                    ds.Tables["RealTimeIDVAndMaritalCheck"].Columns.Add(clFirstName);

                                    DataColumn clAge = new DataColumn("Age", typeof(System.String));

                                    string birthdate = ds.Tables["RealTimeIDVAndMaritalCheck"].Rows[0]["HADateOfBirth"].ToString();

                                    DateTime dtBirthdate = new DateTime();

                                    if (DateTime.TryParse(birthdate, out dtBirthdate))
                                    {
                                        clAge.DefaultValue = Math.Round(DateTime.Now.Subtract(dtBirthdate).Days / (365.25)).ToString();
                                    }
                                    else
                                    {

                                        clAge.DefaultValue = "";
                                    }
                                    ds.Tables["RealTimeIDVAndMaritalCheck"].Columns.Add(clAge);

                                    DataColumn clGender = new DataColumn("Gender", typeof(System.String));
                                    clGender.DefaultValue = int.Parse(ds.Tables["RealTimeIDVAndMaritalCheck"].Rows[0]["HAIDNO"].ToString().Substring(6, 1)) >= 5 ? "Male" : "Female";
                                    ds.Tables["RealTimeIDVAndMaritalCheck"].Columns.Add(clGender);
                                }
                                else
                                {
                                    ds.Tables["RealTimeIDVAndMaritalCheck"].Columns.Remove("CountryofBirth");
                                    ds.Tables["RealTimeIDVAndMaritalCheck"].Columns.Remove("IDBlocked");
                                    ds.Tables["RealTimeIDVAndMaritalCheck"].Columns.Remove("DeathPlace");
                                    ds.Tables["RealTimeIDVAndMaritalCheck"].Columns.Remove("CauseOfDeath");
                                }

                                rXml = ds.GetXml();
                                rp.ResponseData = rXml;

                                foreach (DataRow r in ds.Tables["RealTimeIDVAndMaritalCheck"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    eSC.Billable = true;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("HAIDNO"))
                                    {
                                        if (!string.IsNullOrEmpty(r["HAIDNO"].ToString()))
                                        {
                                            eSC.IDNo = r["HAIDNO"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("HANames"))
                                    {
                                        if (!string.IsNullOrEmpty(r["HANames"].ToString()))
                                        {
                                            eSC.FirstName = r["HANames"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("HASurname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["HASurname"].ToString()))
                                        {
                                            eSC.Surname = r["HASurname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    //eSC.KeyID = intSubscriberEnquiryID;
                                    eSC.KeyType = "R";
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;
                                    //eSC.Reference = "R-" + intSubscriberEnquiryID.ToString();
                                    eSC.ProductID = intProductId;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();

                                    rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                                }
                            }
                        }

                    }

                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }
                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;


                MatchResult = eSe.SubscriberEnquiryID;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitIdentityVerificationPlus(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string strIDNo, string strSurname, string strFirstName, string strSecondName, DateTime dtBirthDate, string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode, string strIDIssuedDate)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            string rXml = "";
            int MatchResult = 0;
            DataSet ds = new DataSet();
            int intSubscriberEnquiryResultID = 0;
            Boolean boolSubmitEnquiry = true;
            string strValidationStatus = "";

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);

            try
            {
                if (spr.ReportID > 0)
                {

                    // Check the PayAsYouGo settings for the subscriber
                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {
                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;
                        eSe.BirthDate = dtBirthDate;
                        eSe.FirstName = strFirstName;
                        eSe.IDNo = strIDNo;
                        eSe.ProductID = intProductId;
                        eSe.SecondName = strSecondName;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = strExtRef;
                        eSe.Surname = strSurname;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";
                        eSe.AccountNo = strIDIssuedDate;

                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.Surname))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.Surname;
                        }
                        if (!string.IsNullOrEmpty(eSe.FirstName))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.FirstName;
                        }
                        if (!string.IsNullOrEmpty(eSe.AccountNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.AccountNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.SubscriberReference))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.SubscriberReference;
                        }

                        if (eSe.BirthDate != DateTime.MinValue && eSe.BirthDate.ToString("yyyy/MM/dd") != DateTime.Now.ToString("yyyy/MM/dd"))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.BirthDate.ToString("yyyy/MM/dd");
                        }

                        if (!string.IsNullOrEmpty(strVoucherCode))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + strVoucherCode;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);
                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoConsumerTraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoConsumerTraceWseManager.ConfirmationChkBox = bConfirmationChkBox;
                        eoConsumerTraceWseManager.DOB = dtBirthDate;
                        eoConsumerTraceWseManager.ExternalReference = strExtRef;
                        eoConsumerTraceWseManager.Firstname = strFirstName;
                        eoConsumerTraceWseManager.IDno = strIDNo;
                        eoConsumerTraceWseManager.ProductID = intProductId;
                        eoConsumerTraceWseManager.SecondName = strSecondName;
                        eoConsumerTraceWseManager.subscriberID = intSubscriberID;
                        eoConsumerTraceWseManager.Surname = strSurname;
                        eoConsumerTraceWseManager.BonusCheck = bBonusChecking;
                        eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;
                        eoConsumerTraceWseManager.IDIssuedDate = strIDIssuedDate;

                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.ConsumerIDVerificationMatch(eoConsumerTraceWseManager);

                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.ConsumerIDVerificationMatch(eoConsumerTraceWseManager);
                        }

                        rp.EnquiryID = intSubscriberEnquiryID;

                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            eSC.SearchOutput = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.DetailsViewedYN = false;
                            eSC.Billable = false;
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;

                            rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                            rp.EnquiryID = eSC.SubscriberEnquiryID;
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);


                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            ds.ReadXml(xmlSR);
                            if (ds.Tables.Contains("ConsumerDetail"))
                            {
                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, intProductId);

                                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);
                                rXml = ds.GetXml();
                                rp.ResponseData = rXml;

                                foreach (DataRow r in ds.Tables["ConsumerDetail"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    eSC.Billable = true;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();

                                    rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                                }
                            }
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {

                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }
                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    if (int.Parse(r["HomeAffairsID"].ToString()) > 0)
                                    {
                                        eSC.KeyID = int.Parse(r["HomeAffairsID"].ToString());
                                    }
                                    else
                                    {
                                        eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                    }
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {
                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");


                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);

                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;
                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);
                                            }
                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }

                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    if (int.Parse(r["HomeAffairsID"].ToString()) > 0)
                                    {
                                        eSC.KeyID = int.Parse(r["HomeAffairsID"].ToString());
                                    }
                                    else
                                    {
                                        eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                    }
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {

                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");

                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;

                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;

                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);

                                            }

                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }


                        }

                    }

                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }
                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;


                MatchResult = eSe.SubscriberEnquiryID;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitIdentityVerificationBioMetric(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string strIDNo, string strSurname, string strFirstName, string strSecondName, DateTime dtBirthDate, string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode, string strDHAURL)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            string rXml = "";
            int MatchResult = 0;
            DataSet ds = new DataSet();
            int intSubscriberEnquiryResultID = 0;
            Boolean boolSubmitEnquiry = true;
            string strValidationStatus = "";

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);

            try
            {
                if (spr.ReportID > 0)
                {
                    XDSPortalEnquiry.Data.IndemnityContent oContent = new XDSPortalEnquiry.Data.IndemnityContent();


                    if (!oContent.CheckIndemnityAccepted(strAdminCon, intSubscriberID, intSystemUserID, intProductId))
                    {
                        throw new Exception("Please accept the Terms of use for accessing this product");
                    }

                    // Check the PayAsYouGo settings for the subscriber
                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {
                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;
                        eSe.BirthDate = dtBirthDate;
                        eSe.FirstName = strFirstName;
                        eSe.IDNo = strIDNo;
                        eSe.ProductID = intProductId;
                        eSe.SecondName = strSecondName;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = strExtRef;
                        eSe.Surname = strSurname;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";

                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.Surname))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.Surname;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);
                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoConsumerTraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoConsumerTraceWseManager.ConfirmationChkBox = bConfirmationChkBox;
                        eoConsumerTraceWseManager.DOB = dtBirthDate;
                        eoConsumerTraceWseManager.ExternalReference = strExtRef;
                        eoConsumerTraceWseManager.Firstname = strFirstName;
                        eoConsumerTraceWseManager.IDno = strIDNo;
                        eoConsumerTraceWseManager.ProductID = intProductId;
                        eoConsumerTraceWseManager.SecondName = strSecondName;
                        eoConsumerTraceWseManager.subscriberID = intSubscriberID;
                        eoConsumerTraceWseManager.Surname = strSurname;
                        eoConsumerTraceWseManager.BonusCheck = bBonusChecking;
                        eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;
                        eoConsumerTraceWseManager.DHAURl = strDHAURL;

                        if (intProductId == 236)
                        {
                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        }
                        else
                        {
                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.GetDHAIDReport(eoConsumerTraceWseManager);

                            //If error raise one last call with a different connection
                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.GetDHAIDReport(eoConsumerTraceWseManager);
                            }
                        }

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report && rp.ResponseData.Contains("<IDNOMatchStatus>Not Matched</IDNOMatchStatus>"))
                        {
                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                        }

                        rp.EnquiryID = intSubscriberEnquiryID;

                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                        bool NormalRun = true;
                        int HomeAffairsID = 0;

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            int intRunMode = dSe.GetDHARunMode(con);

                            if (intRunMode == 2 || intRunMode == 3 || intProductId == 236)
                            {
                                NormalRun = false;
                                System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();
                                XDSPortalEnquiry.Business.ConsumerTrace_IdentityVerification dtConsumertrace = new XDSPortalEnquiry.Business.ConsumerTrace_IdentityVerification();
                                rp = dtConsumertrace.SubmitIdentityVerification(con, AdminConnection, sub.SubscriberID, sys.SystemUserID, 9, sub.SubscriberName, strIDNo, "", "", "", DateTime.Now, strExtRef, true, true, strVoucherCode);
                                rXml = rp.ResponseData;
                                xmlSR = new System.IO.StringReader(rXml);

                                switch (rp.ResponseStatus)
                                {
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single:
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple:
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                                        ds.ReadXml(xmlSR);
                                        if (ds.Tables.Contains("ConsumerDetails"))
                                        {
                                            string EnquiryID, EnquiryResultID;

                                            EnquiryID = ds.Tables["ConsumerDetails"].Rows[0]["EnquiryID"].ToString();
                                            EnquiryResultID = ds.Tables["ConsumerDetails"].Rows[0]["EnquiryResultID"].ToString();
                                            HomeAffairsID = int.Parse(ds.Tables["ConsumerDetails"].Rows[0]["HomeAffairsID"].ToString());

                                            XDSPortalEnquiry.Business.MultipleTrace_IdentityVerification dtMultipleID = new XDSPortalEnquiry.Business.MultipleTrace_IdentityVerification();
                                            rp = dtMultipleID.SubmitMultipleIdentityVerificationFailover(con, AdminConnection, int.Parse(EnquiryID), int.Parse(EnquiryResultID), null, true, strVoucherCode);

                                            rXml = rp.ResponseData;
                                            xmlSR = new System.IO.StringReader(rXml);
                                            DataSet ds1 = new DataSet();
                                            ds1.ReadXml(xmlSR);

                                            if (ds1.Tables.Contains("ConsumerDetail"))
                                            {
                                                DataSet dsDataSegment = new DataSet();
                                                dsDataSegment.ReadXml(new System.IO.StringReader(eSe.ExtraVarInput2));
                                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, eSe.ProductID);

                                                DataSet dsReport = new DataSet("HomeAffairs");

                                                DataTable dtReportInformation = new DataTable("ReportInformation");
                                                dtReportInformation.Columns.Add("ReportID", typeof(String));
                                                dtReportInformation.Columns.Add("ReportName", typeof(String));
                                                DataRow drReportInformation;
                                                drReportInformation = dtReportInformation.NewRow();

                                                drReportInformation["ReportID"] = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();
                                                drReportInformation["ReportName"] = dsDataSegment.Tables[0].Rows[0]["ReportName"].ToString();

                                                dtReportInformation.Rows.Add(drReportInformation);

                                                dsReport.Tables.Add(dtReportInformation);

                                                DataTable dtRealTimeIDV = new DataTable("RealTimeIDV");
                                                dtRealTimeIDV.Columns.Add("InputIDNO", typeof(String));
                                                dtRealTimeIDV.Columns.Add("HAIDNO", typeof(String));
                                                dtRealTimeIDV.Columns.Add("IDNOMatchStatus", typeof(String));
                                                dtRealTimeIDV.Columns.Add("HANames", typeof(String));
                                                dtRealTimeIDV.Columns.Add("HASurname", typeof(String));
                                                dtRealTimeIDV.Columns.Add("HADateOfBirth", typeof(DateTime));
                                                dtRealTimeIDV.Columns.Add("HADeceasedStatus", typeof(String));
                                                dtRealTimeIDV.Columns.Add("HADeceasedDate", typeof(String));
                                                dtRealTimeIDV.Columns.Add("HAIDBookIssuedDate", typeof(String));
                                                dtRealTimeIDV.Columns.Add("HAMaritalStatus", typeof(String));
                                                dtRealTimeIDV.Columns.Add("HAMarriageDate", typeof(String));
                                                dtRealTimeIDV.Columns.Add("HAErrorDescription", typeof(String));
                                                dtRealTimeIDV.Columns.Add("IdCardInd", typeof(String));
                                                dtRealTimeIDV.Columns.Add("IdCardDate", typeof(String));
                                                dtRealTimeIDV.Columns.Add("CountryofBirth", typeof(String));
                                                dtRealTimeIDV.Columns.Add("IDBlocked", typeof(String));
                                                dtRealTimeIDV.Columns.Add("DeathPlace", typeof(String));
                                                dtRealTimeIDV.Columns.Add("CauseOfDeath", typeof(String));

                                                DataRow drRealTimeIDV;
                                                drRealTimeIDV = dtRealTimeIDV.NewRow();

                                                drRealTimeIDV["InputIDNO"] = ds1.Tables["ConsumerDetail"].Rows[0]["IDNo"].ToString();
                                                drRealTimeIDV["HAIDNO"] = ds1.Tables["ConsumerDetail"].Rows[0]["IDNo"].ToString();
                                                drRealTimeIDV["IDNOMatchStatus"] = "Matched";
                                                string Name = ds1.Tables["ConsumerDetail"].Rows[0]["FirstName"].ToString();
                                                if (ds1.Tables["ConsumerDetail"].Columns.Contains("SecondName"))
                                                {
                                                    Name = Name + " " + ds1.Tables["ConsumerDetail"].Rows[0]["SecondName"].ToString();
                                                }
                                                if (ds1.Tables["ConsumerDetail"].Columns.Contains("ThirdName"))
                                                {
                                                    Name = Name + " " + ds1.Tables["ConsumerDetail"].Rows[0]["ThirdName"].ToString();
                                                }
                                                drRealTimeIDV["HANames"] = Name;
                                                drRealTimeIDV["HASurname"] = ds1.Tables["ConsumerDetail"].Rows[0]["Surname"].ToString();
                                                drRealTimeIDV["HADateOfBirth"] = ds1.Tables["ConsumerDetail"].Rows[0]["BirthDate"].ToString();
                                                drRealTimeIDV["HADeceasedStatus"] = "";
                                                drRealTimeIDV["HADeceasedDate"] = "";
                                                drRealTimeIDV["HAIDBookIssuedDate"] = "";
                                                drRealTimeIDV["HAMaritalStatus"] = ds1.Tables["ConsumerDetail"].Rows[0]["MaritalStatusDesc"].ToString().ToUpper();
                                                drRealTimeIDV["HAMarriageDate"] = "";
                                                drRealTimeIDV["HAErrorDescription"] = "";
                                                drRealTimeIDV["IdCardInd"] = "";
                                                drRealTimeIDV["IdCardDate"] = "";
                                                drRealTimeIDV["CountryofBirth"] = "";
                                                drRealTimeIDV["IDBlocked"] = "";
                                                drRealTimeIDV["DeathPlace"] = "";
                                                drRealTimeIDV["CauseOfDeath"] = "";


                                                dtRealTimeIDV.Rows.Add(drRealTimeIDV);

                                                dsReport.Tables.Add(dtRealTimeIDV);

                                                rXml = dsReport.GetXml();
                                                rp.ResponseData = rXml;
                                                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                                                dSe.InsertDHAFailedLog(con, intSubscriberEnquiryID, int.Parse(EnquiryID), sys.Username, DateTime.Now);
                                                xmlSR = new System.IO.StringReader(rXml);
                                                ds = new DataSet();
                                            }
                                        }
                                        break;
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                        rp = ra.GetDHAIDReport(eoConsumerTraceWseManager);

                                        //If error raise one last call with a different connection
                                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                                        {
                                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                            rp = ra.GetDHAIDReport(eoConsumerTraceWseManager);
                                        }

                                        rXml = rp.ResponseData;
                                        xmlSR = new System.IO.StringReader(rXml);
                                        break;
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                                        rXml = "<NoResult><Error>DHA link is down</Error></NoResult>";
                                        rsXml.LoadXml(rXml);
                                        break;
                                }
                            }
                        }

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            eSC.SearchOutput = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.DetailsViewedYN = false;
                            eSC.Billable = false;
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;

                            rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                            rp.EnquiryID = eSC.SubscriberEnquiryID;
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            //rp.ResponseData = "Input ID does not exist on Home Affairs";
                            dSe.UpdateSubscriberEnquiryError(con, eSe);
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            ds.ReadXml(xmlSR);
                            if (ds.Tables.Contains("RealTimeIDV"))
                            {
                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, intProductId);

                                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);
                                rXml = ds.GetXml();
                                rp.ResponseData = rXml;

                                foreach (DataRow r in ds.Tables["RealTimeIDV"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    eSC.Billable = true;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("HAIDNO"))
                                    {
                                        if (!string.IsNullOrEmpty(r["HAIDNO"].ToString()))
                                        {
                                            eSC.IDNo = r["HAIDNO"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("HASurname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["HASurname"].ToString()))
                                        {
                                            eSC.Surname = r["HASurname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("HANames"))
                                    {
                                        if (!string.IsNullOrEmpty(r["HANames"].ToString()))
                                        {
                                            eSC.FirstName = r["HANames"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    //eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = "R";
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();

                                    rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                    eSC = dSC.GetSubscriberEnquiryResultObjectBySubscriberEnquiryID(con, eSC.SubscriberEnquiryID);
                                    rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);

                                    intSubscriberEnquiryResultID = rp.EnquiryResultID;

                                    //Build list 
                                    ds = new DataSet("ListOfConsumers");

                                    DataTable dtSubscriberInput1 = new DataTable("ConsumerDetails");
                                    dtSubscriberInput1.Columns.Add("ConsumerID", typeof(Int32));
                                    dtSubscriberInput1.Columns.Add("FirstName", typeof(String));
                                    dtSubscriberInput1.Columns.Add("SecondName", typeof(String));
                                    dtSubscriberInput1.Columns.Add("ThirdName", typeof(String));
                                    dtSubscriberInput1.Columns.Add("Surname", typeof(String));
                                    dtSubscriberInput1.Columns.Add("Idno", typeof(String));
                                    dtSubscriberInput1.Columns.Add("PassportNo", typeof(String));
                                    dtSubscriberInput1.Columns.Add("BirthDate", typeof(DateTime));
                                    dtSubscriberInput1.Columns.Add("GenderInd", typeof(String));
                                    dtSubscriberInput1.Columns.Add("BonusXML", typeof(String));
                                    dtSubscriberInput1.Columns.Add("TempReference", typeof(String));
                                    dtSubscriberInput1.Columns.Add("EnquiryID", typeof(Int32));
                                    dtSubscriberInput1.Columns.Add("EnquiryResultID", typeof(Int32));
                                    dtSubscriberInput1.Columns.Add("Reference", typeof(String));
                                    DataRow drSubscriberInput1;
                                    drSubscriberInput1 = dtSubscriberInput1.NewRow();

                                    string[] Names = r["HANames"].ToString().Split(' ');

                                    drSubscriberInput1["ConsumerID"] = intSubscriberEnquiryResultID; // 0;
                                    drSubscriberInput1["FirstName"] = Names[0].ToString();
                                    drSubscriberInput1["SecondName"] = "";
                                    drSubscriberInput1["ThirdName"] = "";
                                    drSubscriberInput1["Surname"] = r["HASurname"].ToString().Replace("'", "''");
                                    drSubscriberInput1["Idno"] = strIDNo;
                                    drSubscriberInput1["PassportNo"] = "";
                                    drSubscriberInput1["BirthDate"] = dtBirthDate;
                                    drSubscriberInput1["GenderInd"] = "";
                                    drSubscriberInput1["BonusXML"] = "";
                                    drSubscriberInput1["TempReference"] = "";
                                    drSubscriberInput1["EnquiryID"] = intSubscriberEnquiryID;
                                    drSubscriberInput1["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    drSubscriberInput1["Reference"] = "R" + intSubscriberEnquiryID.ToString() + "-0";

                                    dtSubscriberInput1.Rows.Add(drSubscriberInput1);

                                    ds.Tables.Add(dtSubscriberInput1);

                                    rp.ResponseData = ds.GetXml();
                                }
                            }
                        }
                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }
                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.StackTrace.ToLower().Substring(oException.StackTrace.ToLower().IndexOf("xdsportalenquiry"), 100) + oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;


                MatchResult = eSe.SubscriberEnquiryID;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitRealTimeIDMApplication(SqlConnection IDMOnline, SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string strIDNo, string strVoucherCode)
        {
            string strAdminCon = AdminConnection.ConnectionString;
            string strIDMCon = IDMOnline.ConnectionString;


            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            string rXml = "";

            int MatchResult = 0;
            DataSet ds = new DataSet();

            Boolean boolSubmitEnquiry = true;
            string strValidationStatus = "";

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);

            try
            {
                if (spr.ReportID > 0)
                {
                    // Check the PayAsYouGo settings for the subscriber
                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }
                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }


                    if (boolSubmitEnquiry)
                    {
                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;

                        eSe.IDNo = strIDNo;
                        eSe.ProductID = intProductId;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        //  eSe.SubscriberReference = strExtRef;

                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";

                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);
                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoConsumerTraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoConsumerTraceWseManager.IDno = eSe.IDNo;
                        // eoConsumerTraceWseManager.BonusCheck = bBonusChecking;
                        eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;
                        eoConsumerTraceWseManager.subscriberID = sub.SubscriberID;

                        // Submit data for matching 

                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, strIDMCon);

                        rp = ra.ConsumerIDMIDVerificationMatch(eoConsumerTraceWseManager);

                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.ConsumerIDMIDVerificationMatch(eoConsumerTraceWseManager);
                        }


                        rp.EnquiryID = intSubscriberEnquiryID;

                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            eSC.SearchOutput = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.DetailsViewedYN = false;
                            eSC.Billable = false;
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;

                            rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                            rp.EnquiryID = eSC.SubscriberEnquiryID;
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);


                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            ds.ReadXml(xmlSR);
                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, intProductId);

                                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryReference", typeof(String));
                                dtSubscriberInput.Columns.Add("YourReference", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();
                                drSubscriberInput["EnquiryReference"] = "R-" + intSubscriberEnquiryID.ToString();
                                //drSubscriberInput["YourReference"] = strExtRef;

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);
                                rXml = ds.GetXml();
                                rp.ResponseData = rXml;

                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    eSC.Billable = true;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("IDNumber"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNumber"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNumber"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    //eSC.KeyID = intSubscriberEnquiryID;
                                    eSC.KeyType = "R";
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;
                                    //eSC.Reference = "R-" + intSubscriberEnquiryID.ToString();
                                    eSC.ProductID = intProductId;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();

                                    rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                                }
                            }

                        }

                    }
                }

                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }
                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;


                MatchResult = eSe.SubscriberEnquiryID;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;

        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitIdentityVerificationV1(SqlConnection con, SqlConnection AdminConnection, SqlConnection IDVConnection, string strUser, int intProductId, string strIDNo, string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode)
        {
            string strIDVCon = IDVConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            string rXml = "";
            int MatchResult = 0;
            DataSet ds = new DataSet();
            int intSubscriberEnquiryResultID = 0;
            Boolean boolSubmitEnquiry = true;
            string strValidationStatus = "";

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUser(AdminConnection, strUser);

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, sys.SubscriberID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, sys.SubscriberID, intProductId);

            try
            {
                if (spr.ReportID > 0)
                {

                    // Check the PayAsYouGo settings for the subscriber
                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, sys.SubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {
                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = sys.SubscriberID;
                        eSe.IDNo = strIDNo;
                        eSe.ProductID = intProductId;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = sub.SubscriberName;
                        eSe.SubscriberReference = strExtRef;
                        eSe.SystemUserID = sys.SystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";

                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);
                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoConsumerTraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoConsumerTraceWseManager.ConfirmationChkBox = bConfirmationChkBox;
                        eoConsumerTraceWseManager.ExternalReference = strExtRef;
                        eoConsumerTraceWseManager.IDno = strIDNo;
                        eoConsumerTraceWseManager.ProductID = intProductId;
                        eoConsumerTraceWseManager.subscriberID = sys.SubscriberID;
                        eoConsumerTraceWseManager.BonusCheck = bBonusChecking;
                        eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;

                        if (string.IsNullOrEmpty(strIDNo))
                        {
                            throw new Exception("ID No. is Mandatory");
                        }

                        if (!FnValidateIDNo(strIDNo))
                        {
                            throw new Exception("Invalid ID No.");
                        }

                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strIDVCon, eSe.ExtraVarInput1);
                        rp = ra.GetIDVerificationReportV1(eoConsumerTraceWseManager);

                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strIDVCon, eSe.ExtraVarInput1);
                            rp = ra.GetIDVerificationReportV1(eoConsumerTraceWseManager);
                        }

                        rp.EnquiryID = intSubscriberEnquiryID;

                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            eSC.SearchOutput = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.DetailsViewedYN = false;
                            eSC.Billable = false;
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;

                            rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                            rp.EnquiryID = eSC.SubscriberEnquiryID;
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            ds.ReadXml(xmlSR);
                            if (ds.Tables.Contains("ConsumerDetail"))
                            {
                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, intProductId);

                                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryReference", typeof(String));
                                dtSubscriberInput.Columns.Add("YourReference", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();
                                drSubscriberInput["EnquiryReference"] = eSC.Reference;
                                drSubscriberInput["YourReference"] = eSe.SubscriberReference;

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);
                                rXml = ds.GetXml();
                                rp.ResponseData = rXml;

                                foreach (DataRow r in ds.Tables["ConsumerDetail"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    eSC.Billable = true;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();

                                    rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, sys.SubscriberID, spr.UnitPrice);
                                }
                            }
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {

                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }
                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    if (int.Parse(r["HomeAffairsID"].ToString()) > 0)
                                    {
                                        eSC.KeyID = int.Parse(r["HomeAffairsID"].ToString());
                                    }
                                    else
                                    {
                                        eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                    }
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {
                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");


                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);

                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;
                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);
                                            }
                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }

                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    if (int.Parse(r["HomeAffairsID"].ToString()) > 0)
                                    {
                                        eSC.KeyID = int.Parse(r["HomeAffairsID"].ToString());
                                    }
                                    else
                                    {
                                        eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                    }
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {

                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");

                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;

                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;

                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);

                                            }
                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }
                        }
                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }
                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.StackTrace.ToLower().Substring(oException.StackTrace.ToLower().IndexOf("xdsportalenquiry"), 100) + oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                MatchResult = eSe.SubscriberEnquiryID;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public bool FnValidateIDNo(string input)
        {
            bool IsValid = true;

            try
            {
                double Product = 0, ProductSum = 0, Factor = 1;

                if (input.Trim() == string.Empty) return false;
                if (!FnIsNumeric(input)) return false;
                if (input.Trim().Length != 13) return false;
                if (int.Parse(input.Substring(0, 6)) == 0) return false;
                if (int.Parse(input.Substring(input.Length - 7)) == 0) return false;
                if (!FnIsDate("19" + input.Substring(0, 6)) && !FnIsDate("20" + input.Substring(0, 6))) return false;
                if (input.Trim().Length == 13 && int.Parse(input.Substring(10, 1)) > 2) return false;

                if (IsValid)
                {
                    char[] idchar = input.Substring(0, 12).ToArray();

                    foreach (char c in idchar)
                    {
                        Product = int.Parse(c.ToString()) * Factor;
                        ProductSum = ProductSum + (Product > 9 ? ((Product - 10.0) + 1.0) : Product);
                        Factor = Factor == 1 ? 2.0 : 1.0;
                    }

                    ProductSum = ProductSum + int.Parse(input.Substring(12, 1));

                    IsValid = (ProductSum / 10.0) - Math.Floor(ProductSum / 10.0) == 0.0 ? true : false;
                }
            }
            catch (Exception ex)
            {
                IsValid = false;
            }

            return IsValid;
        }

        public bool FnIsNumeric(string input)
        {
            bool isNumeric = false;

            int ivalue = 0;
            if (int.TryParse(input, NumberStyles.Any, new CultureInfo("en-US"), out ivalue))
                isNumeric = true;

            long lvalue = 0;
            if (long.TryParse(input, NumberStyles.Any, new CultureInfo("en-US"), out lvalue))
                isNumeric = true;

            Decimal dvalue = 0;
            if (Decimal.TryParse(input, NumberStyles.Any, new CultureInfo("en-US"), out dvalue))
                isNumeric = true;

            return isNumeric;
        }

        public bool FnIsDate(string input)
        {
            bool result = false;
            DateTime dtParse = new DateTime();
            if (DateTime.TryParseExact(input, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtParse))
            {
                if (dtParse.Year >= 1753) { result = true; } else { result = false; }
            }
            return result;
        }
    }
}
