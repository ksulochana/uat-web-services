using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using XDSPortalLibrary;


namespace XDSPortalEnquiry.Business
{
    public class SubscriberEnquiry
    {
        Data.XDSSettings oXDSSettings = new XDSPortalEnquiry.Data.XDSSettings();

        public SubscriberEnquiry()
        {
        }

        public XDSPortalLibrary.Entity_Layer.Response GetSubscriberEnquirySearchHistory(SqlConnection con, SqlConnection AdminConnection, int intProductID, int intSubscriberID, int intSystemUserID)
        {
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            DataSet ds = new DataSet("SearchHistory");

            try
            {
                Data.SubscriberEnquiry dSubscriberEnquiry = new XDSPortalEnquiry.Data.SubscriberEnquiry();

                ds = dSubscriberEnquiry.GetSubscriberEnquirySearchHistory(con, intSubscriberID, intProductID, intSystemUserID);

                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                rp.ResponseData = ds.GetXml();
            }
            catch (Exception oException)
            {
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }

            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response GetSubscriberEnquiryKYCSearchHistory(SqlConnection con, SqlConnection AdminConnection, int intProductID, int intSubscriberID, string SystemUserFullName, string SystemUser, string StartDate, string EndDate)
        {
            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);



            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            DataSet ds = new DataSet();

            try
            {
                Data.SubscriberEnquiry dSubscriberEnquiry = new XDSPortalEnquiry.Data.SubscriberEnquiry();

                ds = dSubscriberEnquiry.GetSubscriberEnquiryKYCReport(con, intProductID, SystemUser, StartDate, EndDate);

                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;

                if (ds.Tables.Contains("ReportItem"))
                {
                    Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                    Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, intProductID);

                    DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                    dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                    dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                    dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                    dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                    dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                    DataRow drSubscriberInput;
                    drSubscriberInput = dtSubscriberInput.NewRow();

                    drSubscriberInput["EnquiryDate"] = DateTime.Now;
                    drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                    drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                    drSubscriberInput["SubscriberUserName"] = SystemUserFullName;
                    drSubscriberInput["EnquiryInput"] = StartDate + "|" + EndDate;

                    dtSubscriberInput.Rows.Add(drSubscriberInput);

                    ds.Tables.Add(dtSubscriberInput);

                    string tXML = ds.GetXml();

                    tXML = tXML.Replace("<ReportItem>", "").Replace("</ReportItem>", "").Replace("<XMLData>", "").Replace("</XMLData>", "");
                    tXML = tXML.Replace("&lt;", "<");
                    tXML = tXML.Replace("&gt;", ">");

                    rp.ResponseData = tXML;
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "No records found!";
                }
            }
            catch (Exception oException)
            {
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }

            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response GetSubscriberEnquiryReport(SqlConnection con, int SubscriberEnquiryResultID)
        {
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            DataSet ds = new DataSet("Report");

            try
            {
                Data.SubscriberEnquiry dSubscriberEnquiry = new XDSPortalEnquiry.Data.SubscriberEnquiry();

                ds = dSubscriberEnquiry.GetSubscriberEnquiryReport(con, SubscriberEnquiryResultID);

                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;

                    if (ds.Tables[0].Rows[0]["ProductID"].ToString() == "1")
                    {
                        string fraudscoreXML = ds.Tables[0].Rows[0]["ExtraVarOutput1"] as string;

                        if (!string.IsNullOrEmpty(fraudscoreXML))
                        {
                            string authXML = ds.Tables[0].Rows[0]["XMLData"] as string;
                            rp.ResponseData = "<AuthenticationResponse>" + authXML.Replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>",string.Empty) + fraudscoreXML + "</AuthenticationResponse>";
                            
                        }
                        else
                        {
                            rp.ResponseData = ds.Tables[0].Rows[0]["XMLData"] as string;
                        }
                    }
                    else
                    {
                        rp.ResponseData = ds.Tables[0].Rows[0]["XMLData"] as string;
                    }
                    //rp.ResponseData =  ds.GetXml();
                }
                else
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            }
            catch (Exception oException)
            {
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();

                SqlConnection.ClearPool(con);
            }

            ds.Dispose();
            return rp;
        }
    }
}