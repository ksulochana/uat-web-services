﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using Newtonsoft.Json;
using System.Configuration;

namespace XDSPortalEnquiry.Business
{
    public class MDPEnquiryLog
    {
        private XDSPortalLibrary.Entity_Layer.MDPInput input;
        private XDSPortalLibrary.Entity_Layer.MDPResponse mdpResponse;

        public MDPEnquiryLog()
        {
            input = new XDSPortalLibrary.Entity_Layer.MDPInput();
            mdpResponse = new XDSPortalLibrary.Entity_Layer.MDPResponse();
        }

        public XDSPortalLibrary.Entity_Layer.MDPResponse SubmitToMDP(string ProductID, string ProductName, string SubscriberID, string SubscriberName, string SubscriberEnquiryID)
        {
            try
            {
                string MDPURL = ConfigurationManager.AppSettings["MDPURL"].ToString();
                string MDPUsername = ConfigurationManager.AppSettings["MDPUsername"].ToString();
                string MDPPassword = ConfigurationManager.AppSettings["MDPPassword"].ToString();

                input.ProductId = ProductID;
                input.ProductName = ProductName;
                input.EnquiryId = SubscriberEnquiryID;
                input.SubscriberId = SubscriberID;
                input.SubscriberName = SubscriberName;

                string credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes(MDPUsername + ":" + MDPPassword));
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

                using (WebClient client = new WebClient())
                {
                    client.Headers[HttpRequestHeader.ContentType] = "application/json";
                    client.Headers[HttpRequestHeader.Authorization] = "Basic " + credentials;
                    var jsonObj = JsonConvert.SerializeObject(input);
                    var dataString = client.UploadString(MDPURL, "POST", jsonObj);
                    mdpResponse = JsonConvert.DeserializeObject<XDSPortalLibrary.Entity_Layer.MDPResponse>(dataString);
                }
            }
            catch(Exception ex)
            {
                mdpResponse.response.ProductTransactionId = "";
                mdpResponse.response.Message = ex.Message;
            }

            return mdpResponse;
        }

        public XDSPortalLibrary.Entity_Layer.MDPResponse SubmitToMDP(string ProductID, string ProductName, string SubscriberID, string SubscriberName, string SubscriberEnquiryID, string SolutionTransactionID)
        {
            try
            {
                string MDPURL = ConfigurationManager.AppSettings["MDPURL"].ToString();
                string MDPUsername = ConfigurationManager.AppSettings["MDPUsername"].ToString();
                string MDPPassword = ConfigurationManager.AppSettings["MDPPassword"].ToString();

                input.ProductId = ProductID;
                input.ProductName = ProductName;
                input.EnquiryId = SubscriberEnquiryID;
                input.SubscriberId = SubscriberID;
                input.SubscriberName = SubscriberName;
                input.SolutionTransactionId = SolutionTransactionID;

                string credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes(MDPUsername + ":" + MDPPassword));
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

                using (WebClient client = new WebClient())
                {
                    client.Headers[HttpRequestHeader.ContentType] = "application/json";
                    client.Headers[HttpRequestHeader.Authorization] = "Basic " + credentials;
                    var jsonObj = JsonConvert.SerializeObject(input);
                    var dataString = client.UploadString(MDPURL, "POST", jsonObj);
                    mdpResponse = JsonConvert.DeserializeObject<XDSPortalLibrary.Entity_Layer.MDPResponse>(dataString);
                }
            }
            catch (Exception ex)
            {
                mdpResponse.response.ProductTransactionId = "";
                mdpResponse.response.Message = ex.Message;
            }

            return mdpResponse;
        }
    }
}
