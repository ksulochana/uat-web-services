using System;

using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace XDSPortalEnquiry.Business
{
    public class ConsumerTrace_DocumentVerification
    {

        private XDSPortalLibrary.Business_Layer.ConsumerDocumentVerificationTraceco moConsumerTraceWseManager;
        private XDSPortalLibrary.Entity_Layer.ConsumerDocumentVerificationTrace eoConsumerTraceWseManager;


        public ConsumerTrace_DocumentVerification()
        {
            moConsumerTraceWseManager = new XDSPortalLibrary.Business_Layer.ConsumerDocumentVerificationTraceco();
            eoConsumerTraceWseManager = new XDSPortalLibrary.Entity_Layer.ConsumerDocumentVerificationTrace();
        }

        public void ValidateInput(Entity.SubscriberEnquiryLog Validate)
        {
           
            if (!string.IsNullOrEmpty(Validate.EntityType.Trim()))
            {              
                
                if (!string.IsNullOrEmpty(Validate.FirstName.Trim()) && Validate.FirstName.Trim().Length > 3)
                {
                    throw new Exception("Invalid Initials. Cannot be more than 3 characters");
                }
                if (!string.IsNullOrEmpty(Validate.FirstName.Trim()) &&  !Regex.IsMatch(Validate.FirstName.Trim(), @"^[a-zA-Z]+$"))
                {
                    throw new Exception("Initials are invalid. Cannot include special characters or numbers");
                }
                
                if (Validate.SurName.Trim().Length < 3 || Validate.SurName.Trim().Length > 30)
                {
                    throw new Exception("Surname is invalid. Should be between 3 and 30 characters");
                }
                if (!string.IsNullOrEmpty(Validate.IDNo.Trim()))
                {
                    long idNo = 0;
                    long.TryParse(Validate.IDNo.Trim(), out idNo);

                    if (idNo < 1 || (Validate.IDNo.Trim().Length != 13))
                        throw new Exception("ID Number is invalid. Cannot be 0 and should be 13 digits long");
                }
            }
        }




          public XDSPortalLibrary.Entity_Layer.Response SubmitDocumentsVerification(SqlConnection con, SqlConnection AdminConnection,
            SqlConnection AVSConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName,           
            string strIDNo, string strSurname, string strFirstName,string strEmailAddress, bool bConfirmationChkBox, string strExtRef, bool bBonusChecking,
            string strVoucherCode,string strContactNo)
          {


         if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            int intSubscriberEnquiryLogID = 0;
            
            int MatchResult = 0;
            DataSet ds = new DataSet();
            string strValidationStatus = "";
            Boolean boolSubmitEnquiry = true;

            
            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            Entity.SubscriberEnquiryLog sel = new XDSPortalEnquiry.Entity.SubscriberEnquiryLog();
            Data.SubscriberEnquiryLog dsel = new XDSPortalEnquiry.Data.SubscriberEnquiryLog();

            
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);


            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            try
            {

                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {

                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }
                        

                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {                        

                       
                        sel.FirstName = strFirstName ;
                        sel.SurName = strSurname;
                       
                        sel.ContactNo = strContactNo;
                        sel.EmailAddress = strEmailAddress;
                        sel.VoucherCode = strVoucherCode;
                        sel.SubscriberReference = strExtRef;
                        sel.SubscriberID = intSubscriberID;
                        sel.SubscriberName = sub.SubscriberName;
                        sel.SystemUserID = intSystemUserID;
                        sel.SystemUserName = sys.SystemUserFullName;
                        sel.CreatedByUser = sys.Username;
                        sel.CreatedOnDate = DateTime.Now;
                        sel.SubscriberEnquiryDate = DateTime.Now;
                        sel.EnquiryStatus = XDSPortalEnquiry.Entity.SubscriberEnquiryLog.EnquiryStatusInd.P.ToString();
                         sel.IDNo = strIDNo;                      
                        sel.BillingTypeID = spr.BillingTypeID;
                        


                        ValidateInput(sel);

                      
                        sel.KeyType = "B";
                        sel.ProductID = intProductId;
                        sel.BillingPrice = spr.UnitPrice;

                        if (sub.PayAsYouGo == 1 || !(strVoucherCode == string.Empty))
                        {
                            sel.Billable = false;
                        }
                        else
                        {
                            sel.Billable = true;
                        }

                        intSubscriberEnquiryLogID = dsel.InsertSubscriberEnquiryLog(con, sel);

                      
                        if (sub.PayAsYouGo == 1)
                        {
                            dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                        }
                        if (!(strVoucherCode == string.Empty))
                        {
                            dSV.UpdateSubscriberVoucher(AdminConnection, strVoucherCode, sys.Username);
                        }

                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = "Enquiry Submitted successfully";
                        rp.EnquiryLogID = intSubscriberEnquiryLogID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }

            catch (Exception oException)
            {
                sel.EnquiryResult = "E";
                sel.ErrorDescription = oException.Message;
                sel.SubscriberEnquiryLogID = dsel.UpdateSubscriberEnquiryLogError(con, sel);

                MatchResult = sel.SubscriberEnquiryLogID;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;
            }
            
            ds.Dispose();
            return rp;
        }

        }
    }

    