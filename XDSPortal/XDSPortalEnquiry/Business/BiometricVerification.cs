﻿using AfiswitchAfis.Simple;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using BiometricProxy.AfiswitchService;
using BiometricProxy;
using System.IO;
using System.Xml.Serialization;


namespace XDSPortalEnquiry.Business
{
   public class BiometricVerification
    {
        public int PrintMatchThreshold = 0;//=> Convert.ToInt32(ConfigurationManager.AppSettings["PrintMatchThreshold"]);
       public BiometricVerification() { }


        public Entity.Biometricmodel.VerifyUserResult VerifyOnlineUser(int SubscriberID,Entity.Biometricmodel.VerifyUserInfo userInfo, Entity.Biometricmodel.VerifyUserInfo applicantInfo)
        {
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            Entity.Biometricmodel.VerifyUserResult result = new Entity.Biometricmodel.VerifyUserResult();


            result = VerifyOnlineUser(userInfo, applicantInfo);

            if (result.Message.Contains("not found"))
            {
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
            }
            else
            {
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
            }


            //XmlSerializer serializer1 = new XmlSerializer(result.GetType());
            //System.IO.StringWriter sw1 = new System.IO.StringWriter();
            //serializer1.Serialize(sw1, result);
            //System.IO.StringReader reader1 = new System.IO.StringReader(sw1.ToString());

            //rp.ResponseData = reader1.ReadToEnd();


            return result;
        }

        public Entity.Biometricmodel.VerifyUserResult VerifyOnlineUser(Entity.Biometricmodel.VerifyUserInfo userInfo, Entity.Biometricmodel.VerifyUserInfo applicantInfo)
       {
            BiometricServices.Models.VerifyUserResult oResult = new BiometricServices.Models.VerifyUserResult();
            Entity.Biometricmodel.VerifyUserResult oclientResult = new Entity.Biometricmodel.VerifyUserResult();

            var tempUserString = userInfo.IsoTemplateString.Remove(userInfo.IsoTemplateString.Length - 1).Split(',');
            var userIsoTemplate = tempUserString.Select(s => Convert.ToByte(s)).ToArray();

            var mappedUser = new BiometricServices.Models.VerifyUserInfo
           {
               IsoTemplate = userIsoTemplate,
               FingerNo = userInfo.FingerIndex,
               UserLogin = userInfo.UserId
           };

            var tempApplicantString = applicantInfo.IsoTemplateString.Remove(applicantInfo.IsoTemplateString.Length - 1).Split(',');
            var applicantIsoTempalte = tempApplicantString.Select(s => Convert.ToByte(s)).ToArray();

            var mappedApplicant = new BiometricServices.Models.VerifyUserInfo
            {
               IsoTemplate = applicantIsoTempalte,
               FingerNo = applicantInfo.FingerIndex,
               UserLogin = applicantInfo.UserId
           };

            var afiswitchRepository = new BiometricServices.Repository.AfiswitchRepository();

            oResult = afiswitchRepository.VerifyOnlineUser(mappedApplicant, mappedUser);

            oclientResult.Message = oResult.Message;
            oclientResult.ResultCode = oResult.ResultCode;

           return oclientResult;
       }

        public XDSPortalLibrary.Entity_Layer.Response VerifyPrintsSequence(List<Entity.Biometricmodel.FingerPrintSequenceInfo> referenceFingerInfo)
        {
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                if (referenceFingerInfo != null)
                {
                  //  File.AppendAllText(@"C:\Log\BIO.txt","Step1\n");

                    var afisRepository = new BiometricServices.Repository.AfiswitchRepository();

                  //  File.AppendAllText(@"C:\Log\BIO.txt", "Step2\n");
                    var mappedFingerInfo = MapFingerInfo(referenceFingerInfo);
                  //  File.AppendAllText(@"C:\Log\BIO.txt", mappedFingerInfo.ToString());
                    afisRepository.VerifyCaptureSequence(mappedFingerInfo);
                  //  File.AppendAllText(@"C:\Log\BIO.txt", "Step3");
                }


                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                rp.ResponseData = "Success";
            }
            catch (Exception exception)
            {
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = exception.Message + exception.StackTrace;
            }

            return rp;
        }

        private List<BiometricServices.Models.VerifyUserInfo> MapFingerInfo(List<Entity.Biometricmodel.FingerPrintSequenceInfo> referenceFingersInfo)
        {
            return (from e in referenceFingersInfo
                    where e != null
                    select new BiometricServices.Models.VerifyUserInfo {
                        FingerNo = e.FingerIndex,
                        IsoTemplate = e.IsoTemplateString.Remove(e.IsoTemplateString.Length - 1).Split(',').Select(s => Convert.ToByte(s)).ToArray()
                    }).ToList();
        }

        //  public void VerifyCaptureSequence(List<Entity.Biometricmodel.VerifyUserInfo> referenceFingersInfo)
        //{
        //    var validPrints = referenceFingersInfo.Where(fp => fp != null).ToList();
        //    if (validPrints.Count < 2) return;

        //    var lastCapturedFingerIndex = validPrints.Count - 1;
        //    var lastCapturePerson = new Person(
        //        new Fingerprint
        //        {
        //            AsIsoTemplate = validPrints[lastCapturedFingerIndex].IsoTemplate,
        //        });
        //    var lastCaptureFingerName = MapFingerFromIndex(validPrints[lastCapturedFingerIndex].FingerNo).ToString();
        //    if (lastCaptureFingerName == "Any") lastCaptureFingerName = "Last captured";

        //    var engine = new AfisEngine();

        //    for (int i = 0; i < lastCapturedFingerIndex; i++)
        //    {
        //        var capturePersonToValidate = new Person(
        //            new Fingerprint
        //            {
        //                AsIsoTemplate = validPrints[i].IsoTemplate,
        //            });

        //        var verficationScore = engine.Verify(lastCapturePerson, capturePersonToValidate);
        //        if (verficationScore > PrintMatchThreshold)
        //        {
        //            var fingerName = MapFingerFromIndex(validPrints[i].FingerNo).ToString();
        //            if (fingerName == "Any") fingerName = "User fingerprint";
        //            throw new Exception($"{lastCaptureFingerName} matches {fingerName}, Please recapture...");
        //        }
        //    }
        //}

        public XDSPortalLibrary.Entity_Layer.Response EnrollApplicant(List<Entity.Biometricmodel.VerifyUserInfo> enrollmentInfo, string firstName, string lastName, string cellphoneNumber, string idNumber, string faceImageString)
        {
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                if (enrollmentInfo == null || enrollmentInfo.Count <= 1)
                    throw new Exception("Please capture atleast one finger with the user's finger to enroll applicant");

                var mappedEnrolmentData = (from e in enrollmentInfo
                                           where e != null
                                           select new BiometricServices.Models.VerifyUserInfo
                                           {
                                               FingerNo = e.FingerIndex,
                                               IsoTemplate = e.IsoTemplateString.Remove(e.IsoTemplateString.Length - 1).Split(',').Select(s => Convert.ToByte(s)).ToArray(),
                                               WsqBuffer = e.WsqBufferString.Remove(e.WsqBufferString.Length - 1).Split(',').Select(s => Convert.ToByte(s)).ToArray(),
                                               UserLogin = e.UserId
                                           }).ToList();
                var afiswitchRepository = new BiometricServices.Repository.AfiswitchRepository();
                byte[] faceImageBytes = new byte[0];
                if (!string.IsNullOrEmpty(faceImageString))
                {
                    var normalizedFaceString = faceImageString.Contains("base64") ? faceImageString.Remove(0, faceImageString.IndexOf(',') + 1) : faceImageString;
                    faceImageBytes = Convert.FromBase64String(normalizedFaceString);
                }
                string result = afiswitchRepository.EnrollApplicant(mappedEnrolmentData, firstName, lastName, cellphoneNumber, idNumber, faceImageBytes);
                rp.ResponseData = result;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
            }
            catch (Exception exception)
            {
                rp.ResponseData= exception.Message;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            }

            return rp;
        }


        public XDSPortalLibrary.Entity_Layer.Response EnrollUser(List<Entity.Biometricmodel.VerifyUserInfo> enrollmentInfo, string firstName, string lastName, string cellphoneNumber, string idNumber, string faceImageString)
        {
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                if (enrollmentInfo == null || enrollmentInfo.Count <= 1)
                    throw new Exception("Please capture atleast one finger with the user's finger to enroll applicant");

                var mappedEnrolmentData = (from e in enrollmentInfo
                                           where e != null
                                           select new BiometricServices.Models.VerifyUserInfo
                                           {
                                               FingerNo = e.FingerIndex,
                                               IsoTemplate = e.IsoTemplateString.Remove(e.IsoTemplateString.Length - 1).Split(',').Select(s => Convert.ToByte(s)).ToArray(),
                                               WsqBuffer = e.WsqBufferString.Remove(e.WsqBufferString.Length - 1).Split(',').Select(s => Convert.ToByte(s)).ToArray(),
                                               UserLogin = e.UserId
                                           }).ToList();
                var afiswitchRepository = new BiometricServices.Repository.AfiswitchRepository();
                byte[] faceImageBytes = new byte[0];
                if (!string.IsNullOrEmpty(faceImageString))
                {
                    var normalizedFaceString = faceImageString.Contains("base64") ? faceImageString.Remove(0, faceImageString.IndexOf(',') + 1) : faceImageString;
                    faceImageBytes = Convert.FromBase64String(normalizedFaceString);
                }
                string result = "";//afiswitchRepository.EnrollUser(mappedEnrolmentData, firstName, lastName, cellphoneNumber, idNumber, faceImageBytes);

                rp.ResponseData = result;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
            }
            catch (Exception exception)
            {
                rp.ResponseData = exception.Message;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            }

            return rp;
        }


        public XDSPortalLibrary.Entity_Layer.Response VerifySingleUser(Entity.Biometricmodel.VerifyUserInfo userInfo)
        {
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                var afiswitchRepository = new BiometricServices.Repository.AfiswitchRepository();
                var tempString = userInfo.IsoTemplateString.Remove(userInfo.IsoTemplateString.Length - 1).Split(',');
                var isoTemplate = tempString.Select(s => Convert.ToByte(s)).ToArray();
                var mappedUserInfo = new BiometricServices.Models.VerifyUserInfo
                {
                    UserLogin = userInfo.UserId,
                    FingerNo = userInfo.FingerIndex,
                    IsoTemplate = isoTemplate
                };
                var result = afiswitchRepository.VerifyUser(mappedUserInfo);


                rp.ResponseData = result;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
            }
            catch (Exception exception)
            {
                rp.ResponseData = exception.Message;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                
            }

            return rp;
        }


        public void CheckAvailability()
        {
            try
            {
                ServiceWrapper.AfiswitchServiceClient.GetVersion();
            }
            catch
            {
                throw new Exception("Could not connect to Afiswitch Service");
            }
        }

        private static long GenerateMemberNo()
        {
            return Convert.ToInt64(DateTime.Now.ToString("yyyyMMddhhmmssffff"));
        }

        private static Finger MapFingerFromIndex(int fingerIndex)
        {
            Finger afisFinger;

            switch (fingerIndex)
            {
                case 1: afisFinger = Finger.RightThumb; break;
                case 2: afisFinger = Finger.RightIndex; break;
                case 3: afisFinger = Finger.RightMiddle; break;
                case 4: afisFinger = Finger.RightRing; break;
                case 5: afisFinger = Finger.RightLittle; break;
                case 6: afisFinger = Finger.LeftThumb; break;
                case 7: afisFinger = Finger.LeftIndex; break;
                case 8: afisFinger = Finger.LeftMiddle; break;
                case 9: afisFinger = Finger.LeftRing; break;
                case 10: afisFinger = Finger.LeftLittle; break;
                default: afisFinger = Finger.Any; break;
            }

            return afisFinger;
        }


    }
}
