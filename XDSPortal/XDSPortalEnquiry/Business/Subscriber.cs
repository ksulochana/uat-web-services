using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using XDSPortalLibrary;


namespace XDSPortalEnquiry.Business
{
    public class Subscriber
    {
        public Subscriber()
        {
        }

        public XDSPortalLibrary.Entity_Layer.Response GetSubscriberProductList(SqlConnection AdminConnection, int intSubscriberID)
        {
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            DataSet ds = new DataSet("Products");

            try
            {
                Data.Subscriber dSubscriberEnquiry = new XDSPortalEnquiry.Data.Subscriber();

                ds = dSubscriberEnquiry.GetSubscriberProductList(AdminConnection, intSubscriberID);

                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                rp.ResponseData = ds.GetXml();
            }
            catch (Exception oException)
            {
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;
 
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close(); 

                SqlConnection.ClearPool(AdminConnection);
            }

            ds.Dispose();
            return rp;
        }
    }
}