﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;


namespace XDSPortalEnquiry.Business
{
    public class ConsumerTrace_IndividualFile
    {
        private XDSPortalLibrary.Business_Layer.IDVerificationIdeco moIDVerificationIdecoWseManager;
        private XDSPortalLibrary.Entity_Layer.IDVerificationIdeco eoIDVerificationIdecoWseManager;

        public ConsumerTrace_IndividualFile()
        {
            moIDVerificationIdecoWseManager = new XDSPortalLibrary.Business_Layer.IDVerificationIdeco();
            eoIDVerificationIdecoWseManager = new XDSPortalLibrary.Entity_Layer.IDVerificationIdeco();
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitIndividualFileEnquiry(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string strIDNo, string strSurName,  string strFirstName, bool bConfirmationChkBox, string strExtRef, bool bBonusChecking, string strVoucherCode)
        {

            string strUserName = string.Empty, strPassword = string.Empty, strAccRef = string.Empty;
            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            string rXml = "";
            int MatchResult = 0;
            DataSet ds = new DataSet();
            string strValidationStatus = "";
            Boolean boolSubmitEnquiry = true;
            DataSet dsLogon = new DataSet();
            DataSet dsBankName = new DataSet();
            DataSet dsAcctype = new DataSet();
            double Totalcost = 0;

            Data.IdecoLogonDetails dlogin = new XDSPortalEnquiry.Data.IdecoLogonDetails();
            dsLogon = dlogin.GetIdecoLogonDetailsDataSet(AdminConnection);

            strUserName = dsLogon.Tables[0].Rows[0].Field<string>("Username").ToString();
            strPassword = dsLogon.Tables[0].Rows[0].Field<string>("Password").ToString();
            strAccRef = dsLogon.Tables[0].Rows[0].Field<string>("AccRef").ToString();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();

            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();


            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);


            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();


                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {


                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }


                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {

                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;
                        eSe.FirstName = strFirstName;
                        eSe.IDNo = strIDNo;
                        eSe.ProductID = intProductId;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = strExtRef;
                        eSe.Surname = strSurName;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";

                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.Surname))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.Surname;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);
                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;


                        eoIDVerificationIdecoWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoIDVerificationIdecoWseManager.ExternalReference = strExtRef;
                        eoIDVerificationIdecoWseManager.Firstname = strFirstName;
                        eoIDVerificationIdecoWseManager.IDno = strIDNo;
                        eoIDVerificationIdecoWseManager.Surname = strSurName;
                        eoIDVerificationIdecoWseManager.UserName = strUserName;
                        eoIDVerificationIdecoWseManager.Password = strPassword;
                        eoIDVerificationIdecoWseManager.AccountRef = strAccRef;


                        // Submit data for matching 
                        rp = moIDVerificationIdecoWseManager.IndividualID(eoIDVerificationIdecoWseManager);

                        // Get Response data
                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);


                        eSC.SearchOutput = "";
                        eSC.IDNo = "";
                        eSC.PassportNo = "";
                        eSC.Surname = "";
                        eSC.FirstName = "";
                        eSC.BirthDate = DateTime.Parse("1900/01/01");
                        eSC.Gender = "";
                        eSC.DetailsViewedYN = false;
                        eSC.Billable = false;
                        eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                        eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                        eSC.CreatedByUser = eSe.CreatedByUser;
                        eSC.CreatedOnDate = DateTime.Now;
                        eSC.KeyType = rp.ResponseKeyType;

                        rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                        eSC = dSC.GetSubscriberEnquiryResultObject(con, rp.EnquiryResultID);


                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");

                            Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                            Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, eSe.ProductID);

                            DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                            dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                            dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                            dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                            dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                            dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                            dtSubscriberInput.Columns.Add("Result", typeof(String));
                            dtSubscriberInput.Columns.Add("XDsRefNo", typeof(String));
                            dtSubscriberInput.Columns.Add("ExternalRef", typeof(String));
                            dtSubscriberInput.Columns.Add("IDno", typeof(String));

                            DataRow drSubscriberInput;
                            drSubscriberInput = dtSubscriberInput.NewRow();

                            drSubscriberInput["EnquiryDate"] = DateTime.Now;
                            drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                            drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                            drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                            drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();
                            drSubscriberInput["XDSRefNo"] = eSC.Reference.ToString();
                            drSubscriberInput["ExternalRef"] = strExtRef;
                            drSubscriberInput["Result"] = "ID number not found";
                            drSubscriberInput["IDno"] = strIDNo;



                            if (eSC.BonusIncluded == true)
                            {
                                Data.SubscriberEnquiryResultBonus oSubscriberEnquiryResultBonus = new XDSPortalEnquiry.Data.SubscriberEnquiryResultBonus();
                                DataSet dsBonus = oSubscriberEnquiryResultBonus.GetSelectedSubscriberEnquiryResultBonus(con, eSC.SubscriberEnquiryResultID);

                                DataTable dtBonusSelected = new DataTable("ConsumerBonusSelected");

                                dtBonusSelected.Columns.Add("DataSegmentID", typeof(String));
                                dtBonusSelected.Columns.Add("DataSegmentName", typeof(String));
                                dtBonusSelected.Columns.Add("DataSegmentDisplayText", typeof(String));

                                DataRow drBonusSelected;
                                if (dsBonus.Tables[0].Rows.Count > 0)
                                {
                                    foreach (DataRow r in dsBonus.Tables[0].Rows)
                                    {
                                        drBonusSelected = dtBonusSelected.NewRow();
                                        drBonusSelected["DataSegmentID"] = r["DataSegmentID"];
                                        drBonusSelected["DataSegmentName"] = r["DataSegmentName"];
                                        drBonusSelected["DataSegmentDisplayText"] = r["DataSegmentDisplayText"];
                                        dtBonusSelected.Rows.Add(drBonusSelected);
                                    }
                                    ds.Tables.Add(dtBonusSelected);
                                }
                            }

                            rXml = ds.GetXml();
                            rp.ResponseData = rXml;

                            eSC.DetailsViewedDate = DateTime.Now;
                            eSC.DetailsViewedYN = true;
                            if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                            {
                                eSC.Billable = false;
                            }
                            else
                            {
                                eSC.Billable = true;
                            }
                            eSC.ChangedByUser = eSe.CreatedByUser;
                            eSC.ChangedOnDate = DateTime.Now;
                            eSC.SearchOutput = "";

                            eSC.KeyID = rp.ResponseKey;
                            eSC.KeyType = rp.ResponseKeyType;
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                            eSC.BillingTypeID = spr.BillingTypeID;
                            eSC.BillingPrice = spr.UnitPrice;

                            eSC.XMLData = rXml.Replace("'", "''");
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                            eSC.ProductID = eSe.ProductID;


                            if (ds.Tables.Contains("return"))
                            {
                                drSubscriberInput["Result"] = "ID number found";

                                foreach (DataRow r in ds.Tables["return"].Rows)
                                {
                                    if (r.Table.Columns.Contains("id_number"))
                                    {
                                        if (!string.IsNullOrEmpty(r["id_number"].ToString()))
                                        {
                                            eSC.Surname = r["id_number"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["surname"].ToString()))
                                        {
                                            eSC.Surname = r["surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("firstname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["firstname"].ToString()))
                                        {
                                            eSC.FirstName = r["firstname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    if (r.Table.Columns.Contains("gender"))
                                    {
                                        if (r["gender"].ToString() == "1")
                                        {
                                            r["gender"] = "Male";
                                        }
                                        else if (r["gender"].ToString() == "0")
                                        {
                                            r["gender"] = "Female";
                                        }
                                    }
                                    if (r.Table.Columns.Contains("status"))
                                    {
                                        if (r["status"].ToString() == "1")
                                        {
                                            r["status"] = "Alive";
                                        }
                                        else if (r["status"].ToString() == "0")
                                        {
                                            r["status"] = "Dead";
                                        }
                                    }

                                    r.Table.AcceptChanges(); 
                                }
                            }
                            dtSubscriberInput.Rows.Add(drSubscriberInput);
                            ds.Tables.Add(dtSubscriberInput);

                            rp.ResponseData = ds.GetXml();

                            dSC.UpdateSubscriberEnquiryResult(con, eSC);

                        }



                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            eSe.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);
                        }

                        if (sub.PayAsYouGo == 1)
                        {
                            dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                        }
                        if (!(strVoucherCode == string.Empty))
                        {
                            dSV.UpdateSubscriberVoucher(AdminConnection, strVoucherCode, sys.Username);
                        }

                    }



                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();


            }

            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;


                MatchResult = eSe.SubscriberEnquiryID;
                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
            }

            ds.Dispose();

            return rp;
        }
    }
}

