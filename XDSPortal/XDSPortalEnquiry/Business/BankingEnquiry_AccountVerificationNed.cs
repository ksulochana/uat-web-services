﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Configuration;

namespace XDSPortalEnquiry.Business
{
    public class BankingEnquiry_AccountVerificationNed
    {
        private XDSPortalLibrary.Business_Layer.AccountVerificationNed moAccountVerificationWseManager;
        private XDSPortalLibrary.Entity_Layer.AccountVerificationNed eoAccountVerificationWseManager;

        public BankingEnquiry_AccountVerificationNed()
        {
            moAccountVerificationWseManager = new XDSPortalLibrary.Business_Layer.AccountVerificationNed();
            eoAccountVerificationWseManager = new XDSPortalLibrary.Entity_Layer.AccountVerificationNed();
        }

        public int[] GetSubscriberVendorCode(SqlConnection AVSConnection, int SubscriberID)
        {
            moAccountVerificationWseManager.ConnectionString = AVSConnection;

            return moAccountVerificationWseManager.GetSubscriberVendorCode(SubscriberID);
        }

        public void ValidateInput(Entity.SubscriberEnquiryLog Validate)
        {
            //string.IsNullOrEmpty(Validate.BankName) || string.IsNullOrEmpty(Validate.BranchName) || 
            if (string.IsNullOrEmpty(Validate.BranchCode.Trim()) || string.IsNullOrEmpty(Validate.AccountNo.Trim()) ||
                string.IsNullOrEmpty(Validate.BranchName.Trim()) || string.IsNullOrEmpty(Validate.EntityType.Trim()))
            {
                throw new Exception("Please enter data to all mandatory fields");
            }

            else if (!string.IsNullOrEmpty(Validate.EntityType.Trim()))
            {
                //|| string.IsNullOrEmpty(Validate.SurName))
                if (Validate.EntityType.Trim().ToLower().Contains("company") && string.IsNullOrEmpty(Validate.BusRegistrationNo.Trim().Replace("/", "")))
                {
                    //throw new Exception("Registration no and name are mandatory for the selected entity");
                    throw new Exception("Registration no is mandatory for the selected entity");
                }
                else if (Validate.EntityType.Trim().ToLower().Contains("sole") && string.IsNullOrEmpty(Validate.IDNo.Trim()))
                {
                    throw new Exception("ID no is mandatory for the selected entity");
                }
                else if (Validate.EntityType.Trim().ToLower().Contains("trust") && string.IsNullOrEmpty(Validate.Trust.Trim()))
                {
                    throw new Exception("Trust no is mandatory for the selected entity");
                }
                else if (Validate.EntityType.Trim().ToLower().Contains("individual") && string.IsNullOrEmpty(Validate.IDNo.Trim()))
                {
                    throw new Exception("IDno is mandatory for the selected entity");
                }
                else if (string.IsNullOrEmpty(Validate.AccHolder.Trim()) ||
                    string.IsNullOrEmpty(Validate.BusBusinessName.Trim()) || string.IsNullOrEmpty(Validate.EmailAddress.Trim()))
                {
                    throw new Exception("FirstName, SurName, Email Address are Mandatory");
                }


                if (!string.IsNullOrEmpty(Validate.BranchCode.Trim()) && !Regex.IsMatch(Validate.BranchCode.Trim(), "^[0-9]*$"))
                {
                    throw new Exception("Branch Number is invalid. Should only contain numbers");
                }
                if (!string.IsNullOrEmpty(Validate.BranchCode.Trim()))
                {
                    long branchCode = 0;
                    long.TryParse(Validate.BranchCode.Trim(), out branchCode);

                    if (branchCode < 1 || (Validate.BranchCode.Trim().Length < 4 || Validate.BranchCode.Trim().Length > 6))
                        throw new Exception("Branch Number is invalid. Cannot be 0 and has to be 4 to 6 digits long");
                }
                if (!string.IsNullOrEmpty(Validate.BusRegistrationNo.Trim()) && Validate.EntityType.Trim().ToLower().Contains("company"))
                {
                    long regNo = 0;
                    long.TryParse(Validate.BusRegistrationNo.Trim().Replace("/", ""), out regNo);

                    if (regNo == 0 || Validate.BusRegistrationNo.Trim().Replace("/", "").Length > 13 ||
                        !Regex.IsMatch(Validate.BusRegistrationNo.Trim().Replace("/", ""), "^[0-9]*$"))
                        throw new Exception("Registration Number is invalid. Cannot be 0 and should not be more than 13 digits long");
                }

                if (!string.IsNullOrEmpty(Validate.FirstName.Trim()) &&
                    Validate.FirstName.Trim().Length > 3 && !Validate.EntityType.Trim().ToLower().Contains("company"))
                {
                    throw new Exception("Invalid Initials. Cannot be more than 3 characters");
                }
                if (!string.IsNullOrEmpty(Validate.FirstName.Trim()) && !Validate.EntityType.Trim().ToLower().Contains("company") &&
                 !Regex.IsMatch(Validate.FirstName.Trim(), @"^[a-zA-Z]+$"))
                {
                    throw new Exception("Initials are invalid. Cannot include special characters or numbers");
                }
                if (!string.IsNullOrEmpty(Validate.AccountNo.Trim()) && 
                    (Validate.AccountNo.Trim().Length > 13 || !Regex.IsMatch(Validate.AccountNo.Trim(), @"^[a-zA-Z0-9]+$")))
                {
                    throw new Exception("Account Number is invalid. Cannot be more than 13 digits long and cannot include special characters");
                } 
                if (Validate.SurName.Trim().Length < 2 || Validate.SurName.Trim().Length > 30)
                {
                    throw new Exception("Surname / Company Name is invalid. Should be between 2 and 30 characters");
                }
                if (!string.IsNullOrEmpty(Validate.IDNo.Trim()) && !Validate.EntityType.Trim().ToLower().Contains("company"))
                {
                    long idNo = 0;
                    long.TryParse(Validate.IDNo.Trim(), out idNo);

                    if (idNo < 1 || (Validate.IDNo.Trim().Length != 13))
                        throw new Exception("ID Number is invalid. Cannot be 0 and should be 13 digits long");
                }
            }
        }

        public void ValidateRealTimeInput(Entity.SubscriberEnquiryLog Validate)
        {
            if (string.IsNullOrEmpty(Validate.BranchCode.Trim()) || string.IsNullOrEmpty(Validate.AccountNo.Trim()) ||
                string.IsNullOrEmpty(Validate.BranchName.Trim()) || string.IsNullOrEmpty(Validate.EntityType.Trim()))
            {
                throw new Exception("Please enter data to all mandatory fields - Branch Code, Account Number, Account Type,Verification Type");
            }
            else if (!string.IsNullOrEmpty(Validate.EntityType.Trim()))
            {
                //|| string.IsNullOrEmpty(Validate.SurName))
                if (Validate.EntityType.Trim().ToLower().Contains("company") && string.IsNullOrEmpty(Validate.BusRegistrationNo.Trim().Replace("/", "")))
                {
                    //throw new Exception("Registration no and name are mandatory for the selected entity");
                    throw new Exception("Registration no is mandatory for the selected entity");
                }
                else if (Validate.EntityType.Trim().ToLower().Contains("sole") && string.IsNullOrEmpty(Validate.IDNo.Trim()))
                {
                    throw new Exception("ID no is mandatory for the selected entity");
                }
                else if (Validate.EntityType.Trim().ToLower().Contains("trust") && string.IsNullOrEmpty(Validate.Trust.Trim()))
                {
                    throw new Exception("Trust no is mandatory for the selected entity");
                }
                else if (Validate.EntityType.Trim().ToLower().Contains("individual") && string.IsNullOrEmpty(Validate.IDNo.Trim()))
                {
                    throw new Exception("IDno is mandatory for the selected entity");
                }



                if (!string.IsNullOrEmpty(Validate.BranchCode.Trim()) && !Regex.IsMatch(Validate.BranchCode.Trim(), "^[0-9]*$"))
                {
                    throw new Exception("Branch Number is invalid. Should only contain numbers");
                }
                if (!string.IsNullOrEmpty(Validate.BranchCode.Trim()))
                {
                    long branchCode = 0;
                    long.TryParse(Validate.BranchCode.Trim(), out branchCode);

                    if (branchCode < 1 || (Validate.BranchCode.Trim().Length < 4 || Validate.BranchCode.Trim().Length > 6))
                        throw new Exception("Branch code is invalid. Cannot be 0 and has to be 4 to 6 digits long");
                }
                if (!string.IsNullOrEmpty(Validate.AccountNo.Trim()) && !Regex.IsMatch(Validate.AccountNo.Trim(), "^[0-9]*$"))
                {
                    throw new Exception("Account Number is invalid. Should only contain numbers");
                }
                if (!string.IsNullOrEmpty(Validate.AccountNo.Trim()))
                {
                    long accountNo = 0;
                    long.TryParse(Validate.AccountNo.Trim(), out accountNo);

                    if (accountNo < 1 || (Validate.AccountNo.Length > 13))
                        throw new Exception("Account Number is invalid. Cannot be 0 and cannot be more than 13 digits long");
                }
                if (!string.IsNullOrEmpty(Validate.IDNo.Trim().Trim()) && Validate.ExtraVarInput2.Trim() == "SID") 
                {
                    long idNo = 0;
                    long.TryParse(Validate.IDNo.Trim(), out idNo);

                    if (idNo < 1 || (Validate.IDNo.Trim().Length != 13))
                        throw new Exception("ID Number is invalid. Cannot be 0 and should be 13 digits long");
                }
                if (!string.IsNullOrEmpty(Validate.BusRegistrationNo.Trim()) && Validate.EntityType.Trim().ToLower().Contains("company"))
                {
                    long regNo = 0;
                    long.TryParse(Validate.BusRegistrationNo.Trim().Replace("/", ""), out regNo);

                    if (regNo == 0 || (Validate.BusRegistrationNo.Trim().Replace("/", "").Length != 12))
                        throw new Exception("Registration Number is invalid. Cannot be 0 and should be 12 digits long");
                }
                if (Validate.SurName.Trim().Length < 2 || Validate.SurName.Trim().Length > 60)
                {
                    throw new Exception("Surname / Company Name is invalid. Should be between 2 and 60 characters");
                }
                if (string.IsNullOrEmpty(Validate.FirstName.Trim()) && !Validate.EntityType.Trim().ToLower().Contains("company") && Validate.ExtraVarInput2.ToLower() == "sid")
                {
                    throw new Exception("Initials are required");
                }
                if (!string.IsNullOrEmpty(Validate.FirstName.Trim()) && !Validate.EntityType.Trim().ToLower().Contains("company") &&
                    !Regex.IsMatch(Validate.FirstName.Trim(), @"^[a-zA-Z]+$"))
                {
                    throw new Exception("Initials are invalid. Cannot include special characters or numbers");
                }
                if (!string.IsNullOrEmpty(Validate.FirstName.Trim()) &&
                    Validate.ExtraVarInput2.Trim() == "SID" && !Validate.EntityType.Trim().ToLower().Contains("company"))
                {
                    if (Validate.FirstName.Trim().Length > 3)
                        throw new Exception("Invalid Initials. Cannot be more than 3 characters");
                }
            }
        }

        public void ValidateRealTimeInputValues(Entity.SubscriberEnquiryLog Validate)
        {
            if (string.IsNullOrEmpty(Validate.BranchCode.Trim()) || string.IsNullOrEmpty(Validate.AccountNo.Trim()) ||
                string.IsNullOrEmpty(Validate.EntityType.Trim()))
            {
                throw new Exception("Please enter data to all mandatory fields - Branch Code, Account Number,Verification Type");
            }
            else if (!string.IsNullOrEmpty(Validate.EntityType.Trim()))
            {
                //|| string.IsNullOrEmpty(Validate.SurName))
                if (Validate.EntityType.Trim().ToLower().Contains("company") && string.IsNullOrEmpty(Validate.BusRegistrationNo.Trim().Replace("/", "")))
                {
                    //throw new Exception("Registration no and name are mandatory for the selected entity");
                    throw new Exception("Registration no is mandatory for the selected entity");
                }
                else if (Validate.EntityType.Trim().ToLower().Contains("sole") && string.IsNullOrEmpty(Validate.IDNo.Trim()))
                {
                    throw new Exception("ID no is mandatory for the selected entity");
                }
                else if (Validate.EntityType.Trim().ToLower().Contains("trust") && string.IsNullOrEmpty(Validate.Trust.Trim()))
                {
                    throw new Exception("Trust no is mandatory for the selected entity");
                }
                else if (Validate.EntityType.Trim().ToLower().Contains("individual") && string.IsNullOrEmpty(Validate.IDNo.Trim()))
                {
                    throw new Exception("IDno is mandatory for the selected entity");
                }



                if (!string.IsNullOrEmpty(Validate.BranchCode.Trim()) && !Regex.IsMatch(Validate.BranchCode.Trim(), "^[0-9]*$"))
                {
                    throw new Exception("Branch Number is invalid. Should only contain numbers");
                }
                if (!string.IsNullOrEmpty(Validate.BranchCode.Trim()))
                {
                    long branchCode = 0;
                    long.TryParse(Validate.BranchCode.Trim(), out branchCode);

                    if (branchCode < 1 || (Validate.BranchCode.Trim().Length < 4 || Validate.BranchCode.Trim().Length > 6))
                        throw new Exception("Branch code is invalid. Cannot be 0 and has to be 4 to 6 digits long");
                }
                if (!string.IsNullOrEmpty(Validate.AccountNo.Trim()) && !Regex.IsMatch(Validate.AccountNo.Trim(), "^[0-9]*$"))
                {
                    throw new Exception("Account Number is invalid. Should only contain numbers");
                }
                if (!string.IsNullOrEmpty(Validate.AccountNo.Trim()))
                {
                    long accountNo = 0;
                    long.TryParse(Validate.AccountNo.Trim(), out accountNo);

                    if (accountNo < 1 || (Validate.AccountNo.Length > 13))
                        throw new Exception("Account Number is invalid. Cannot be 0 and cannot be more than 13 digits long");
                }
                if (!string.IsNullOrEmpty(Validate.IDNo.Trim().Trim()) && Validate.ExtraVarInput2.Trim() == "SID")
                {
                    long idNo = 0;
                    long.TryParse(Validate.IDNo.Trim(), out idNo);

                    if (idNo < 1 || (Validate.IDNo.Trim().Length != 13))
                        throw new Exception("ID Number is invalid. Cannot be 0 and should be 13 digits long");
                }
                if (!string.IsNullOrEmpty(Validate.BusRegistrationNo.Trim()) && Validate.EntityType.Trim().ToLower().Contains("company"))
                {
                    long regNo = 0;
                    long.TryParse(Validate.BusRegistrationNo.Trim().Replace("/", ""), out regNo);

                    if (regNo == 0 || (Validate.BusRegistrationNo.Trim().Replace("/", "").Length != 12))
                        throw new Exception("Registration Number is invalid. Cannot be 0 and should be 12 digits long");
                }
                //if (Validate.SurName.Trim().Length < 3 || Validate.SurName.Trim().Length > 60)
                //{
                //    throw new Exception("Surname / Company Name is invalid. Should be between 3 and 60 characters");
                //}
                //if (string.IsNullOrEmpty(Validate.FirstName.Trim()) && !Validate.EntityType.Trim().ToLower().Contains("company"))
                //{
                //    throw new Exception("Initials are required");
                //}
                if (!string.IsNullOrEmpty(Validate.FirstName.Trim()) && !Validate.EntityType.Trim().ToLower().Contains("company") &&
                    !Regex.IsMatch(Validate.FirstName.Trim(), @"^[a-zA-Z]+$"))
                {
                    throw new Exception("Initials are invalid. Cannot include special characters or numbers");
                }
                if (!string.IsNullOrEmpty(Validate.FirstName.Trim()) &&
                    Validate.ExtraVarInput2.Trim() == "SID" && !Validate.EntityType.Trim().ToLower().Contains("company"))
                {
                    if (Validate.FirstName.Trim().Length > 3)
                        throw new Exception("Invalid Initials. Cannot be more than 3 characters");
                }
            }
        }

        public void ValidateRealTimePlusInput(Entity.SubscriberEnquiryLog Validate)
        {
            if (string.IsNullOrEmpty(Validate.BranchCode.Trim()) || string.IsNullOrEmpty(Validate.AccountNo.Trim()) ||
                string.IsNullOrEmpty(Validate.BranchName.Trim()) || string.IsNullOrEmpty(Validate.EntityType.Trim()))
            {
                throw new Exception("Please enter data to all mandatory fields - Branch Code, Account Number, Account Type,Verification Type");
            }
            else if (!string.IsNullOrEmpty(Validate.EntityType.Trim()))
            {
                //|| string.IsNullOrEmpty(Validate.SurName))
                if (Validate.EntityType.Trim().ToLower().Contains("company") && string.IsNullOrEmpty(Validate.BusRegistrationNo.Trim().Replace("/", "")))
                {
                    //throw new Exception("Registration no and name are mandatory for the selected entity");
                    throw new Exception("Registration no is mandatory for the selected entity");
                }
                else if (Validate.EntityType.Trim().ToLower().Contains("sole") && string.IsNullOrEmpty(Validate.IDNo.Trim()))
                {
                    throw new Exception("ID no is mandatory for the selected entity");
                }
                else if (Validate.EntityType.Trim().ToLower().Contains("trust") && string.IsNullOrEmpty(Validate.Trust.Trim()))
                {
                    throw new Exception("Trust no is mandatory for the selected entity");
                }
                else if (Validate.EntityType.Trim().ToLower().Contains("individual") && string.IsNullOrEmpty(Validate.IDNo.Trim()))
                {
                    throw new Exception("IDno is mandatory for the selected entity");
                }
                else if (string.IsNullOrEmpty(Validate.ProductID.ToString()))
                {
                    throw new Exception("ProductID is mandatory field");
                }



                if (!string.IsNullOrEmpty(Validate.BranchCode.Trim()) && !Regex.IsMatch(Validate.BranchCode.Trim(), "^[0-9]*$"))
                {
                    throw new Exception("Branch Number is invalid. Should only contain numbers");
                }
                if (!string.IsNullOrEmpty(Validate.BranchCode.Trim()))
                {
                    long branchCode = 0;
                    long.TryParse(Validate.BranchCode.Trim(), out branchCode);

                    if (branchCode < 1 || (Validate.BranchCode.Trim().Length < 4 || Validate.BranchCode.Trim().Length > 6))
                        throw new Exception("Branch code is invalid. Cannot be 0 and has to be 4 to 6 digits long");
                }
                if (!string.IsNullOrEmpty(Validate.AccountNo.Trim()) && !Regex.IsMatch(Validate.AccountNo.Trim(), "^[0-9]*$"))
                {
                    throw new Exception("Account Number is invalid. Should only contain numbers");
                }
                if (!string.IsNullOrEmpty(Validate.AccountNo.Trim()))
                {
                    long accountNo = 0;
                    long.TryParse(Validate.AccountNo.Trim(), out accountNo);

                    if (accountNo < 1 || (Validate.AccountNo.Length > 13))
                        throw new Exception("Account Number is invalid. Cannot be 0 and cannot be more than 13 digits long");
                }
                if (!string.IsNullOrEmpty(Validate.IDNo.Trim().Trim()) && Validate.ExtraVarInput2.Trim() == "SID")
                {
                    long idNo = 0;
                    long.TryParse(Validate.IDNo.Trim(), out idNo);

                    if (idNo < 1 || (Validate.IDNo.Trim().Length != 13))
                        throw new Exception("ID Number is invalid. Cannot be 0 and should be 13 digits long");
                }
                if (!string.IsNullOrEmpty(Validate.BusRegistrationNo.Trim()) && Validate.EntityType.Trim().ToLower().Contains("company"))
                {
                    long regNo = 0;
                    long.TryParse(Validate.BusRegistrationNo.Trim().Replace("/", ""), out regNo);

                    if (regNo == 0 || (Validate.BusRegistrationNo.Trim().Replace("/", "").Length != 12))
                        throw new Exception("Registration Number is invalid. Cannot be 0 and should be 12 digits long");
                }
                if (Validate.SurName.Trim().Length < 2 || Validate.SurName.Trim().Length > 60)
                {
                    throw new Exception("Surname / Company Name is invalid. Should be between 2 and 60 characters");
                }
                if (string.IsNullOrEmpty(Validate.FirstName.Trim()) && !Validate.EntityType.Trim().ToLower().Contains("company"))
                {
                    throw new Exception("Initials are required");
                }
                if (!string.IsNullOrEmpty(Validate.FirstName.Trim()) && !Validate.EntityType.Trim().ToLower().Contains("company") &&
                    !Regex.IsMatch(Validate.FirstName.Trim(), @"^[a-zA-Z]+$"))
                {
                    throw new Exception("Initials are invalid. Cannot include special characters or numbers");
                }
                if (!string.IsNullOrEmpty(Validate.FirstName.Trim()) &&
                    Validate.ExtraVarInput2.Trim() == "SID" && !Validate.EntityType.Trim().ToLower().Contains("company"))
                {
                    if (Validate.FirstName.Trim().Length > 3)
                        throw new Exception("Invalid Initials. Cannot be more than 3 characters");
                }
            }
        }


        public XDSPortalLibrary.Entity_Layer.Response SubmitAccountVerification(SqlConnection con, SqlConnection AdminConnection,
            SqlConnection AVSConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName,
            string TypeOfVerification, string strEntityType, string strRegNo1, string strRegNo2, string strRegNo3, string Trust, 
            string strIDNo, string strSurName, string strInitials, string strAccNo, string strBranchCode, string strAcctype, string strBankName, 
            string ClientFirstName, string ClientSurName, string strEmailAddress, bool bConfirmationChkBox, string strExtRef, bool bBonusChecking, 
            string strVoucherCode, string source)
        { 
            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            int intSubscriberEnquiryLogID = 0;
            string rXml = "";
            int MatchResult = 0;
            DataSet ds = new DataSet();
            string strValidationStatus = "";
            Boolean boolSubmitEnquiry = true;
            DataSet dsLogon = new DataSet();
            DataSet dsBankName = new DataSet();
            DataSet dsAcctype = new DataSet();

           


            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            Entity.SubscriberEnquiryLog sel = new XDSPortalEnquiry.Entity.SubscriberEnquiryLog();
            Data.SubscriberEnquiryLog dsel = new XDSPortalEnquiry.Data.SubscriberEnquiryLog();


            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            try
            {
                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                { 
                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }


                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {
                        strEntityType = TypeOfVerification.ToLower() == "individual" ? "Individual" : ((TypeOfVerification.ToLower() == "company" && strEntityType.ToLower() == "none") ? "" : strEntityType);

                        sel.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        sel.SubscriberID = intSubscriberID;
                        sel.ProductID = intProductId;
                        sel.SubscriberEnquiryDate = DateTime.Now;
                        sel.SubscriberName = strSubscriberName;
                        sel.SubscriberReference = strExtRef;
                        sel.SystemUserID = intSystemUserID;
                        sel.BusRegistrationNo = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        sel.IDNo = strIDNo;
                        sel.ExtraVarInput3 = source; 
                        sel.BankName = strBankName;
                        sel.AccountNo = strAccNo.Replace(" ", "").Trim();
                        sel.BranchCode = strBranchCode.Replace(" ", "").Trim();
                        sel.FirstName = strInitials.Replace(" ", "").Trim();
                        sel.SurName = strSurName.Trim();
                        sel.EmailAddress = strEmailAddress.Replace(" ", "").Trim(); 
                        sel.BranchName = strAcctype;
                        sel.SystemUserID = intSystemUserID;
                        sel.SystemUserName = sys.SystemUserFullName;
                        sel.CreatedByUser = sys.Username;
                        sel.CreatedOnDate = DateTime.Now;
                        sel.Searchinput = "";
                        sel.KeyType = "F";
                        sel.AccHolder = ClientFirstName;
                        sel.BusBusinessName = ClientSurName;
                        sel.Trust = Trust;
                        sel.EntityType = strEntityType;
                        
                        //sel.KeyType = 

                        ValidateInput(sel);

                        if (!string.IsNullOrEmpty(sel.IDNo))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.IDNo;
                        }
                        if (!string.IsNullOrEmpty(sel.BusRegistrationNo))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        }
                        if (!string.IsNullOrEmpty(sel.AccountNo))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.AccountNo;
                        }
                        if (!string.IsNullOrEmpty(sel.SurName))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.SurName;
                        }
                        if (!string.IsNullOrEmpty(sel.BranchCode))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.BranchCode;
                        }
                        if (!string.IsNullOrEmpty(sel.Trust))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.Trust;
                        }

                        if (sel.Searchinput.Length > 0)
                        {
                            sel.Searchinput = sel.Searchinput.Substring(3, sel.Searchinput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table
                        intSubscriberEnquiryLogID = dsel.InsertSubscriberEnquiryLog(con, sel);
                        sel.SubscriberEnquiryLogID = intSubscriberEnquiryLogID;

                        if (strEntityType == "Individual")
                            eoAccountVerificationWseManager.IDNo = strIDNo;
                        else
                            eoAccountVerificationWseManager.IDNo = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;  

                        moAccountVerificationWseManager.ConnectionString = AVSConnection;
                        eoAccountVerificationWseManager.ExternalReference = strExtRef;
                        eoAccountVerificationWseManager.Initials = strInitials.Replace(" ", "").Trim();
                        eoAccountVerificationWseManager.AccNo = strAccNo.Replace(" ", "").Trim();
                        eoAccountVerificationWseManager.Acctype = strAcctype;
                        eoAccountVerificationWseManager.BankName = strBankName;
                        eoAccountVerificationWseManager.BranchCode = strBranchCode.Replace(" ", "").Trim();
                        eoAccountVerificationWseManager.SurName = strSurName.Trim();
                        eoAccountVerificationWseManager.EmailAddress = strEmailAddress.Replace(" ", "").Trim();
                        eoAccountVerificationWseManager.RequesterFirstName = ClientFirstName.Trim();
                        eoAccountVerificationWseManager.RequesterSurName = ClientSurName.Trim();
                        eoAccountVerificationWseManager.IDType = TypeOfVerification == "Individual" ? "SID" : (TypeOfVerification == "Company" ? "SBR" : "Unknown");
                        eoAccountVerificationWseManager.SubscriberID = intSubscriberID; 
                        // Submit data for matching 
                        rp = moAccountVerificationWseManager.search(eoAccountVerificationWseManager);

                        // Get Response data
                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                        sel.KeyID = rp.ResponseKey;
                        sel.KeyType = rp.ResponseKeyType;
                        sel.ExtraVarInput1 = rp.TmpReference;
                        sel.IsAHVRequest = true;
                        rp.EnquiryLogID = intSubscriberEnquiryLogID;

                        dsel.UpdateSubscriberEnquiryLog(con, sel);

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            sel.EnquiryResult = "E";
                            sel.ErrorDescription = rp.ResponseData.ToString();
                            dsel.UpdateSubscriberEnquiryLogError(con, sel);
                        }

                        if (sub.PayAsYouGo == 1)
                        {
                            dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                        }
                        if (!(strVoucherCode == string.Empty))
                        {
                            dSV.UpdateSubscriberVoucher(AdminConnection, strVoucherCode, sys.Username);
                        }

                    }



                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }

            catch (Exception oException)
            {
                sel.EnquiryResult = "E";
                sel.ErrorDescription = oException.Message;
                sel.SubscriberEnquiryLogID = dsel.UpdateSubscriberEnquiryLogError(con, sel);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;


                MatchResult = sel.SubscriberEnquiryLogID;
                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }

            ds.Dispose();
            dsLogon.Dispose();
            dsBankName.Dispose();
            dsAcctype.Dispose();

            return rp;
        }


        public XDSPortalLibrary.Entity_Layer.Response SubmitAccountVerificationRealTime(SqlConnection con, SqlConnection AdminConnection,
           SqlConnection AVSConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName,
           string TypeOfVerification, string strEntityType, string strRegNo1, string strRegNo2, string strRegNo3, string Trust,
           string strIDNo, string strIdType, string strSurName, string strInitials, string strAccNo, string strBranchCode, string strAcctype, string strBankName,
           string strExtRef, string strVoucherCode, string source)
        {


            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            int intSubscriberEnquiryLogID = 0;
            string rXml = "";
            int MatchResult = 0;
            DataSet ds = new DataSet();
            string strValidationStatus = "";
            Boolean boolSubmitEnquiry = true;
            DataSet dsLogon = new DataSet();
            DataSet dsBankName = new DataSet();
            DataSet dsAcctype = new DataSet();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            Entity.SubscriberEnquiryLog sel = new XDSPortalEnquiry.Entity.SubscriberEnquiryLog();
            Data.SubscriberEnquiryLog dsel = new XDSPortalEnquiry.Data.SubscriberEnquiryLog();


            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            try
            {
                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {


                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }


                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {
                        strEntityType = TypeOfVerification.ToLower() == "individual" ? "Individual" :
                            ((TypeOfVerification.ToLower() == "company" && strEntityType.ToLower() == "none") ? "" : strEntityType);

                        sel.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        sel.SubscriberID = intSubscriberID;
                        sel.ProductID = intProductId;
                        sel.SubscriberEnquiryDate = DateTime.Now;
                        sel.SubscriberName = strSubscriberName;
                        sel.SubscriberReference = strExtRef;
                        sel.SystemUserID = intSystemUserID;
                        sel.BusRegistrationNo = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        sel.IDNo = strIDNo;
                        sel.ExtraVarInput2 = strIdType;
                        sel.ExtraVarInput3 = source;
                        sel.ExtraintInput1 = 3;
                        sel.BankName = strBankName;
                        sel.AccountNo = strAccNo.Replace(" ", "").Trim();
                        sel.BranchCode = strBranchCode.Replace(" ", "").Trim();
                        sel.FirstName = strInitials.Replace(" ", "").Trim();
                        sel.SurName = strSurName.Trim();
                        sel.BranchName = strAcctype;
                        sel.SystemUserID = intSystemUserID;
                        sel.SystemUserName = sys.SystemUserFullName;
                        sel.CreatedByUser = sys.Username;
                        sel.CreatedOnDate = DateTime.Now;
                        sel.Searchinput = "";
                        sel.KeyType = "F";
                        sel.Trust = Trust;
                        sel.EntityType = strEntityType;

                        //sel.KeyType = 

                        ValidateRealTimeInput(sel);

                        if (!string.IsNullOrEmpty(sel.IDNo))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.IDNo;
                        }
                        if (!string.IsNullOrEmpty(sel.BusRegistrationNo))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        }
                        if (!string.IsNullOrEmpty(sel.AccountNo))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.AccountNo;
                        }
                        if (!string.IsNullOrEmpty(sel.SurName))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.SurName;
                        }
                        if (!string.IsNullOrEmpty(sel.BranchCode))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.BranchCode;
                        }
                        if (!string.IsNullOrEmpty(sel.Trust))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.Trust;
                        }

                        if (sel.Searchinput.Length > 0)
                        {
                            sel.Searchinput = sel.Searchinput.Substring(3, sel.Searchinput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table
                        intSubscriberEnquiryLogID = dsel.InsertSubscriberEnquiryLogAVSRealTime(con, sel);
                        sel.SubscriberEnquiryLogID = intSubscriberEnquiryLogID;

                        if (strEntityType == "Individual")
                            eoAccountVerificationWseManager.IDNo = strIDNo;
                        else if (strEntityType == "Trust")
                            eoAccountVerificationWseManager.IDNo = Trust;
                        else
                            eoAccountVerificationWseManager.IDNo = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;

                        moAccountVerificationWseManager.ConnectionString = AVSConnection;
                        eoAccountVerificationWseManager.ExternalReference = strExtRef;
                        eoAccountVerificationWseManager.Initials = strInitials.Replace(" ", "").Trim();
                        eoAccountVerificationWseManager.AccNo = strAccNo.Replace(" ", "").Trim();
                        eoAccountVerificationWseManager.Acctype = strAcctype;
                        eoAccountVerificationWseManager.BankName = strBankName;
                        eoAccountVerificationWseManager.BranchCode = strBranchCode.Replace(" ", "").Trim();
                        eoAccountVerificationWseManager.SurName = strSurName.Trim();
                        eoAccountVerificationWseManager.IDType = strIdType;
                        eoAccountVerificationWseManager.SubscriberID = intSubscriberID;
                        // Submit data for matching 
                        rp = moAccountVerificationWseManager.InsertAVSRealTime(eoAccountVerificationWseManager);

                        // Get Response data
                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                        sel.KeyID = rp.ResponseKey;
                        sel.KeyType = rp.ResponseKeyType;
                        sel.ExtraVarInput1 = rp.TmpReference;
                        sel.IsAHVRequest = true;
                        rp.EnquiryLogID = intSubscriberEnquiryLogID;

                        dsel.UpdateSubscriberEnquiryLog(con, sel);

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            sel.EnquiryResult = "E";
                            sel.ErrorDescription = rp.ResponseData.ToString();
                            dsel.UpdateSubscriberEnquiryLogError(con, sel);
                        }

                        if (sub.PayAsYouGo == 1)
                        {
                            dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                        }
                        if (!(strVoucherCode == string.Empty))
                        {
                            dSV.UpdateSubscriberVoucher(AdminConnection, strVoucherCode, sys.Username);
                        }

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }

            catch (Exception oException)
            {
                sel.EnquiryResult = "E";
                sel.ErrorDescription = oException.Message;
                sel.SubscriberEnquiryLogID = dsel.UpdateSubscriberEnquiryLogError(con, sel);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;


                MatchResult = sel.SubscriberEnquiryLogID;
                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }

            ds.Dispose();
            dsLogon.Dispose();
            dsBankName.Dispose();
            dsAcctype.Dispose();

            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitAccountVerificationRealTime(SqlConnection con, SqlConnection AdminConnection,
           SqlConnection AVSConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName,
           string TypeOfVerification, string strEntityType, string strRegNo1, string strRegNo2, string strRegNo3, string Trust,
           string strIDNo, string strIdType, string strSurName, string strInitials, string strAccNo, string strBranchCode, string strAcctype, string strBankName,
           string strExtRef, string strVoucherCode, string source, string SolutionTransactionID)
        {


            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            int intSubscriberEnquiryLogID = 0;
            string rXml = "";
            int MatchResult = 0;
            DataSet ds = new DataSet();
            string strValidationStatus = "";
            Boolean boolSubmitEnquiry = true;
            DataSet dsLogon = new DataSet();
            DataSet dsBankName = new DataSet();
            DataSet dsAcctype = new DataSet();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            Entity.SubscriberEnquiryLog sel = new XDSPortalEnquiry.Entity.SubscriberEnquiryLog();
            Data.SubscriberEnquiryLog dsel = new XDSPortalEnquiry.Data.SubscriberEnquiryLog();


            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            try
            {
                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {


                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }


                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {
                        strEntityType = TypeOfVerification.ToLower() == "individual" ? "Individual" :
                            ((TypeOfVerification.ToLower() == "company" && strEntityType.ToLower() == "none") ? "" : strEntityType);

                        sel.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        sel.SubscriberID = intSubscriberID;
                        sel.ProductID = intProductId;
                        sel.SubscriberEnquiryDate = DateTime.Now;
                        sel.SubscriberName = strSubscriberName;
                        sel.SubscriberReference = strExtRef;
                        sel.SystemUserID = intSystemUserID;
                        sel.BusRegistrationNo = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        sel.IDNo = strIDNo;
                        sel.ExtraVarInput2 = strIdType;
                        sel.ExtraVarInput3 = source;
                        sel.ExtraintInput1 = 3;
                        sel.BankName = strBankName;
                        sel.AccountNo = strAccNo.Replace(" ", "").Trim();
                        sel.BranchCode = strBranchCode.Replace(" ", "").Trim();
                        sel.FirstName = strInitials.Replace(" ", "").Trim();
                        sel.SurName = strSurName.Trim();
                        sel.BranchName = strAcctype;
                        sel.SystemUserID = intSystemUserID;
                        sel.SystemUserName = sys.SystemUserFullName;
                        sel.CreatedByUser = sys.Username;
                        sel.CreatedOnDate = DateTime.Now;
                        sel.Searchinput = "";
                        sel.KeyType = "F";
                        sel.Trust = Trust;
                        sel.EntityType = strEntityType;

                        //sel.KeyType = 

                        ValidateRealTimeInput(sel);

                        if (!string.IsNullOrEmpty(sel.IDNo))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.IDNo;
                        }
                        if (!string.IsNullOrEmpty(sel.BusRegistrationNo))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        }
                        if (!string.IsNullOrEmpty(sel.AccountNo))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.AccountNo;
                        }
                        if (!string.IsNullOrEmpty(sel.SurName))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.SurName;
                        }
                        if (!string.IsNullOrEmpty(sel.BranchCode))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.BranchCode;
                        }
                        if (!string.IsNullOrEmpty(sel.Trust))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.Trust;
                        }

                        if (sel.Searchinput.Length > 0)
                        {
                            sel.Searchinput = sel.Searchinput.Substring(3, sel.Searchinput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table
                        intSubscriberEnquiryLogID = dsel.InsertSubscriberEnquiryLogAVSRealTime(con, sel);
                        sel.SubscriberEnquiryLogID = intSubscriberEnquiryLogID;

                        //Submit to MDP
                        MDPEnquiryLog mdpLog = new MDPEnquiryLog();
                        XDSPortalLibrary.Entity_Layer.MDPResponse mdpResp;

                        if (string.IsNullOrEmpty(SolutionTransactionID))
                        {
                            mdpResp = mdpLog.SubmitToMDP(sel.ProductID.ToString(), ConfigurationManager.AppSettings["MDPAVSProductName"].ToString(), sel.SubscriberID.ToString(), sel.SubscriberName, intSubscriberEnquiryLogID.ToString());
                        }
                        else
                        {
                            mdpResp = mdpLog.SubmitToMDP(sel.ProductID.ToString(), ConfigurationManager.AppSettings["MDPAVSProductName"].ToString(), sel.SubscriberID.ToString(), sel.SubscriberName, intSubscriberEnquiryLogID.ToString(), SolutionTransactionID);
                        }

                        if (mdpResp.response.ProductTransactionId != "")
                        {
                            XDSPortalEnquiry.Data.SubscriberEnquiry se = new Data.SubscriberEnquiry();
                            se.InsertSubscriberEnquiryMDP(con, intSubscriberEnquiryLogID, mdpResp.response.ProductTransactionId, mdpResp.response.Message, sel.CreatedByUser, DateTime.Now);
                        }

                        if (strEntityType == "Individual")
                            eoAccountVerificationWseManager.IDNo = strIDNo;
                        else if (strEntityType == "Trust")
                            eoAccountVerificationWseManager.IDNo = Trust;
                        else
                            eoAccountVerificationWseManager.IDNo = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;

                        moAccountVerificationWseManager.ConnectionString = AVSConnection;
                        eoAccountVerificationWseManager.ExternalReference = strExtRef;
                        eoAccountVerificationWseManager.Initials = strInitials.Replace(" ", "").Trim();
                        eoAccountVerificationWseManager.AccNo = strAccNo.Replace(" ", "").Trim();
                        eoAccountVerificationWseManager.Acctype = strAcctype;
                        eoAccountVerificationWseManager.BankName = strBankName;
                        eoAccountVerificationWseManager.BranchCode = strBranchCode.Replace(" ", "").Trim();
                        eoAccountVerificationWseManager.SurName = strSurName.Trim();
                        eoAccountVerificationWseManager.IDType = strIdType;
                        eoAccountVerificationWseManager.SubscriberID = intSubscriberID;
                        // Submit data for matching 
                        rp = moAccountVerificationWseManager.InsertAVSRealTime(eoAccountVerificationWseManager);

                        // Get Response data
                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                        sel.KeyID = rp.ResponseKey;
                        sel.KeyType = rp.ResponseKeyType;
                        sel.ExtraVarInput1 = rp.TmpReference;
                        sel.IsAHVRequest = true;
                        rp.EnquiryLogID = intSubscriberEnquiryLogID;

                        dsel.UpdateSubscriberEnquiryLog(con, sel);

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            sel.EnquiryResult = "E";
                            sel.ErrorDescription = rp.ResponseData.ToString();
                            dsel.UpdateSubscriberEnquiryLogError(con, sel);
                        }

                        if (sub.PayAsYouGo == 1)
                        {
                            dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                        }
                        if (!(strVoucherCode == string.Empty))
                        {
                            dSV.UpdateSubscriberVoucher(AdminConnection, strVoucherCode, sys.Username);
                        }

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }

            catch (Exception oException)
            {
                sel.EnquiryResult = "E";
                sel.ErrorDescription = oException.Message;
                sel.SubscriberEnquiryLogID = dsel.UpdateSubscriberEnquiryLogError(con, sel);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;


                MatchResult = sel.SubscriberEnquiryLogID;
                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }

            ds.Dispose();
            dsLogon.Dispose();
            dsBankName.Dispose();
            dsAcctype.Dispose();

            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitAccountVerificationRealTime(SqlConnection con, SqlConnection AdminConnection,
           SqlConnection AVSConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName,
           string TypeOfVerification, string strEntityType, string strRegNo1, string strRegNo2, string strRegNo3, string Trust,
           string strIDNo, string strIdType, string strSurName, string strInitials, string strAccNo, string strBranchCode, string strAcctype, string strBankName,
           string strContactNumber, string strEmailAddress, string strExtRef, string strVoucherCode, string source)
        {


            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            int intSubscriberEnquiryLogID = 0;
            string rXml = "";
            int MatchResult = 0;
            DataSet ds = new DataSet();
            string strValidationStatus = "";
            Boolean boolSubmitEnquiry = true;
            DataSet dsLogon = new DataSet();
            DataSet dsBankName = new DataSet();
            DataSet dsAcctype = new DataSet();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            Entity.SubscriberEnquiryLog sel = new XDSPortalEnquiry.Entity.SubscriberEnquiryLog();
            Data.SubscriberEnquiryLog dsel = new XDSPortalEnquiry.Data.SubscriberEnquiryLog();


            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            try
            {
                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {


                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }


                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {
                        strEntityType = TypeOfVerification.ToLower() == "individual" ? "Individual" :
                            ((TypeOfVerification.ToLower() == "company" && strEntityType.ToLower() == "none") ? "" : strEntityType);

                        sel.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        sel.SubscriberID = intSubscriberID;
                        sel.ProductID = intProductId;
                        sel.SubscriberEnquiryDate = DateTime.Now;
                        sel.SubscriberName = strSubscriberName;
                        sel.SubscriberReference = strExtRef;
                        sel.SystemUserID = intSystemUserID;
                        sel.BusRegistrationNo = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        sel.IDNo = strIDNo;
                        sel.ExtraVarInput2 = strIdType;
                        sel.ExtraVarInput3 = source;
                        sel.ExtraintInput1 = 3;
                        sel.BankName = strBankName;
                        sel.AccountNo = strAccNo.Replace(" ", "").Trim();
                        sel.BranchCode = strBranchCode.Replace(" ", "").Trim();
                        sel.FirstName = strInitials.Replace(" ", "").Trim();
                        sel.SurName = strSurName.Trim();
                        sel.BranchName = strAcctype;
                        sel.ContactNo = strContactNumber;
                        sel.EmailAddress = strEmailAddress;
                        sel.SystemUserID = intSystemUserID;
                        sel.SystemUserName = sys.SystemUserFullName;
                        sel.CreatedByUser = sys.Username;
                        sel.CreatedOnDate = DateTime.Now;
                        sel.Searchinput = "";
                        sel.KeyType = "F";
                        sel.Trust = Trust;
                        sel.EntityType = strEntityType;

                        //sel.KeyType = 

                        ValidateRealTimeInput(sel);

                        if (!string.IsNullOrEmpty(sel.IDNo))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.IDNo;
                        }
                        if (!string.IsNullOrEmpty(sel.BusRegistrationNo))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        }
                        if (!string.IsNullOrEmpty(sel.AccountNo))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.AccountNo;
                        }
                        if (!string.IsNullOrEmpty(sel.SurName))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.SurName;
                        }
                        if (!string.IsNullOrEmpty(sel.BranchCode))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.BranchCode;
                        }
                        if (!string.IsNullOrEmpty(sel.Trust))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.Trust;
                        }

                        if (sel.Searchinput.Length > 0)
                        {
                            sel.Searchinput = sel.Searchinput.Substring(3, sel.Searchinput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table
                        intSubscriberEnquiryLogID = dsel.InsertSubscriberEnquiryLogAVSRealTime(con, sel);
                        sel.SubscriberEnquiryLogID = intSubscriberEnquiryLogID;

                        if (strEntityType == "Individual")
                            eoAccountVerificationWseManager.IDNo = strIDNo;
                        else if (strEntityType == "Trust")
                            eoAccountVerificationWseManager.IDNo = Trust;
                        else
                            eoAccountVerificationWseManager.IDNo = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;

                        moAccountVerificationWseManager.ConnectionString = AVSConnection;
                        eoAccountVerificationWseManager.ExternalReference = strExtRef;
                        eoAccountVerificationWseManager.Initials = strInitials.Replace(" ", "").Trim();
                        eoAccountVerificationWseManager.AccNo = strAccNo.Replace(" ", "").Trim();
                        eoAccountVerificationWseManager.Acctype = strAcctype;
                        eoAccountVerificationWseManager.BankName = strBankName;
                        eoAccountVerificationWseManager.BranchCode = strBranchCode.Replace(" ", "").Trim();
                        eoAccountVerificationWseManager.SurName = strSurName.Trim();
                        eoAccountVerificationWseManager.IDType = strIdType;
                        eoAccountVerificationWseManager.SubscriberID = intSubscriberID;
                        eoAccountVerificationWseManager.ContactNumber = strContactNumber;
                        eoAccountVerificationWseManager.EmailAddress = strEmailAddress;
                        // Submit data for matching 
                        rp = moAccountVerificationWseManager.InsertAVSRealTimeWithContacts(eoAccountVerificationWseManager);

                        // Get Response data
                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                        sel.KeyID = rp.ResponseKey;
                        sel.KeyType = rp.ResponseKeyType;
                        sel.ExtraVarInput1 = rp.TmpReference;
                        sel.IsAHVRequest = true;
                        rp.EnquiryLogID = intSubscriberEnquiryLogID;

                        dsel.UpdateSubscriberEnquiryLog(con, sel);

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            sel.EnquiryResult = "E";
                            sel.ErrorDescription = rp.ResponseData.ToString();
                            dsel.UpdateSubscriberEnquiryLogError(con, sel);
                        }

                        if (sub.PayAsYouGo == 1)
                        {
                            dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                        }
                        if (!(strVoucherCode == string.Empty))
                        {
                            dSV.UpdateSubscriberVoucher(AdminConnection, strVoucherCode, sys.Username);
                        }

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }

            catch (Exception oException)
            {
                sel.EnquiryResult = "E";
                sel.ErrorDescription = oException.Message;
                sel.SubscriberEnquiryLogID = dsel.UpdateSubscriberEnquiryLogError(con, sel);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;


                MatchResult = sel.SubscriberEnquiryLogID;
                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }

            ds.Dispose();
            dsLogon.Dispose();
            dsBankName.Dispose();
            dsAcctype.Dispose();

            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitAccountVerificationRealTime(SqlConnection con, SqlConnection AdminConnection,
           SqlConnection AVSConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName,
           string TypeOfVerification, string strEntityType, string strRegNo1, string strRegNo2, string strRegNo3, string Trust,
           string strIDNo, string strIdType, string strSurName, string strInitials, string strAccNo, string strBranchCode, string strAcctype, string strBankName,
           string strContactNumber, string strEmailAddress, string strExtRef, string strVoucherCode, string source, string validation)
        {


            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            int intSubscriberEnquiryLogID = 0;
            string rXml = "";
            int MatchResult = 0;
            DataSet ds = new DataSet();
            string strValidationStatus = "";
            Boolean boolSubmitEnquiry = true;
            DataSet dsLogon = new DataSet();
            DataSet dsBankName = new DataSet();
            DataSet dsAcctype = new DataSet();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            Entity.SubscriberEnquiryLog sel = new XDSPortalEnquiry.Entity.SubscriberEnquiryLog();
            Data.SubscriberEnquiryLog dsel = new XDSPortalEnquiry.Data.SubscriberEnquiryLog();


            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            try
            {
                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {


                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }


                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {
                        strEntityType = TypeOfVerification.ToLower() == "individual" ? "Individual" :
                            ((TypeOfVerification.ToLower() == "company" && strEntityType.ToLower() == "none") ? "" : strEntityType);

                        sel.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        sel.SubscriberID = intSubscriberID;
                        sel.ProductID = intProductId;
                        sel.SubscriberEnquiryDate = DateTime.Now;
                        sel.SubscriberName = strSubscriberName;
                        sel.SubscriberReference = strExtRef;
                        sel.SystemUserID = intSystemUserID;
                        sel.BusRegistrationNo = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        sel.IDNo = strIDNo;
                        sel.ExtraVarInput2 = strIdType;
                        sel.ExtraVarInput3 = source;
                        sel.ExtraintInput1 = 3;
                        sel.BankName = strBankName;
                        sel.AccountNo = strAccNo.Replace(" ", "").Trim();
                        sel.BranchCode = strBranchCode.Replace(" ", "").Trim();
                        sel.FirstName = strInitials.Replace(" ", "").Trim();
                        sel.SurName = strSurName.Trim();
                        sel.BranchName = strAcctype;
                        sel.ContactNo = strContactNumber;
                        sel.EmailAddress = strEmailAddress;
                        sel.SystemUserID = intSystemUserID;
                        sel.SystemUserName = sys.SystemUserFullName;
                        sel.CreatedByUser = sys.Username;
                        sel.CreatedOnDate = DateTime.Now;
                        sel.Searchinput = "";
                        sel.KeyType = "F";
                        sel.Trust = Trust;
                        sel.EntityType = strEntityType;
                        //sel.KeyType = 

                        //   ValidateRealTimeInput(sel);

                        if (validation == "true")
                        {
                            ValidateRealTimeInputValues(sel);
                        }
                        else
                        {
                            ValidateRealTimeInput(sel);
                        }

                        if (!string.IsNullOrEmpty(sel.IDNo))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.IDNo;
                        }
                        if (!string.IsNullOrEmpty(sel.BusRegistrationNo))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        }
                        if (!string.IsNullOrEmpty(sel.AccountNo))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.AccountNo;
                        }
                        if (!string.IsNullOrEmpty(sel.SurName))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.SurName;
                        }
                        if (!string.IsNullOrEmpty(sel.BranchCode))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.BranchCode;
                        }
                        if (!string.IsNullOrEmpty(sel.Trust))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.Trust;
                        }

                        if (sel.Searchinput.Length > 0)
                        {
                            sel.Searchinput = sel.Searchinput.Substring(3, sel.Searchinput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table
                        intSubscriberEnquiryLogID = dsel.InsertSubscriberEnquiryLogAVSRealTime(con, sel);
                        sel.SubscriberEnquiryLogID = intSubscriberEnquiryLogID;

                        if (strEntityType == "Individual")
                            eoAccountVerificationWseManager.IDNo = strIDNo;
                        else
                            eoAccountVerificationWseManager.IDNo = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;

                        moAccountVerificationWseManager.ConnectionString = AVSConnection;
                        eoAccountVerificationWseManager.ExternalReference = strExtRef;
                        eoAccountVerificationWseManager.Initials = strInitials.Replace(" ", "").Trim();
                        eoAccountVerificationWseManager.AccNo = strAccNo.Replace(" ", "").Trim();
                        eoAccountVerificationWseManager.Acctype = strAcctype;
                        eoAccountVerificationWseManager.BankName = strBankName;
                        eoAccountVerificationWseManager.BranchCode = strBranchCode.Replace(" ", "").Trim();
                        eoAccountVerificationWseManager.SurName = strSurName.Trim();
                        eoAccountVerificationWseManager.IDType = strIdType;
                        eoAccountVerificationWseManager.SubscriberID = intSubscriberID;
                        eoAccountVerificationWseManager.ContactNumber = strContactNumber;
                        eoAccountVerificationWseManager.EmailAddress = strEmailAddress;
                        // Submit data for matching 
                        rp = moAccountVerificationWseManager.InsertAVSRealTimeWithContacts(eoAccountVerificationWseManager);

                        // Get Response data
                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                        sel.KeyID = rp.ResponseKey;
                        sel.KeyType = rp.ResponseKeyType;
                        sel.ExtraVarInput1 = rp.TmpReference;
                        sel.IsAHVRequest = true;
                        rp.EnquiryLogID = intSubscriberEnquiryLogID;

                        dsel.UpdateSubscriberEnquiryLog(con, sel);

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            sel.EnquiryResult = "E";
                            sel.ErrorDescription = rp.ResponseData.ToString();
                            dsel.UpdateSubscriberEnquiryLogError(con, sel);
                        }

                        if (sub.PayAsYouGo == 1)
                        {
                            dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                        }
                        if (!(strVoucherCode == string.Empty))
                        {
                            dSV.UpdateSubscriberVoucher(AdminConnection, strVoucherCode, sys.Username);
                        }

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }

            catch (Exception oException)
            {
                sel.EnquiryResult = "E";
                sel.ErrorDescription = oException.Message;
                sel.SubscriberEnquiryLogID = dsel.UpdateSubscriberEnquiryLogError(con, sel);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;


                MatchResult = sel.SubscriberEnquiryLogID;
                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }

            ds.Dispose();
            dsLogon.Dispose();
            dsBankName.Dispose();
            dsAcctype.Dispose();

            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitAccountVerificationRealTimePlus(SqlConnection con, SqlConnection AdminConnection,
          SqlConnection AVSConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName,
          string TypeOfVerification, string strEntityType, string strRegNo1, string strRegNo2, string strRegNo3, string Trust,
          string strIDNo, string strIdType, string strSurName, string strInitials, string strAccNo, string strBranchCode, string strAcctype, string strBankName,
          string strContactNumber, string strEmailAddress, string strExtRef, string strVoucherCode, string source, SqlConnection EnquiryCon, SqlConnection AdminCon)
        {


            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            int intSubscriberEnquiryLogID = 0;
            string rXml = "";
            int MatchResult = 0;
            DataSet ds = new DataSet();
            string strValidationStatus = "";
            Boolean boolSubmitEnquiry = true;
            DataSet dsLogon = new DataSet();
            DataSet dsBankName = new DataSet();
            DataSet dsAcctype = new DataSet();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            Entity.SubscriberEnquiryLog sel = new XDSPortalEnquiry.Entity.SubscriberEnquiryLog();
            Data.SubscriberEnquiryLog dsel = new XDSPortalEnquiry.Data.SubscriberEnquiryLog();


            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            try
            {
                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {


                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }


                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {
                        strEntityType = TypeOfVerification.ToLower() == "individual" ? "Individual" :
                            ((TypeOfVerification.ToLower() == "company" && strEntityType.ToLower() == "none") ? "" : strEntityType);

                        sel.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        sel.SubscriberID = intSubscriberID;
                        sel.ProductID = intProductId;
                        sel.SubscriberEnquiryDate = DateTime.Now;
                        sel.SubscriberName = strSubscriberName;
                        sel.SubscriberReference = strExtRef;
                        sel.SystemUserID = intSystemUserID;
                        sel.BusRegistrationNo = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        sel.IDNo = strIDNo;
                        sel.ExtraVarInput2 = strIdType;
                        sel.ExtraVarInput3 = source;
                        sel.ExtraintInput1 = 3;
                        sel.BankName = strBankName;
                        sel.AccountNo = strAccNo.Replace(" ", "").Trim();
                        sel.BranchCode = strBranchCode.Replace(" ", "").Trim();
                        sel.FirstName = strInitials.Replace(" ", "").Trim();
                        sel.SurName = strSurName.Trim();
                        sel.BranchName = strAcctype;
                        sel.ContactNo = strContactNumber;
                        sel.EmailAddress = strEmailAddress;
                        sel.SystemUserID = intSystemUserID;
                        sel.SystemUserName = sys.SystemUserFullName;
                        sel.CreatedByUser = sys.Username;
                        sel.CreatedOnDate = DateTime.Now;
                        sel.Searchinput = "";
                        sel.KeyType = "F";
                        sel.Trust = Trust;
                        sel.EntityType = strEntityType;

                        //sel.KeyType = 

                        ValidateRealTimePlusInput(sel);

                        if (!string.IsNullOrEmpty(sel.IDNo))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.IDNo;
                        }
                        if (!string.IsNullOrEmpty(sel.BusRegistrationNo))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        }
                        if (!string.IsNullOrEmpty(sel.AccountNo))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.AccountNo;
                        }
                        if (!string.IsNullOrEmpty(sel.SurName))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.SurName;
                        }
                        if (!string.IsNullOrEmpty(sel.BranchCode))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.BranchCode;
                        }
                        if (!string.IsNullOrEmpty(sel.Trust))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.Trust;
                        }

                        if (sel.Searchinput.Length > 0)
                        {
                            sel.Searchinput = sel.Searchinput.Substring(3, sel.Searchinput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table
                        intSubscriberEnquiryLogID = dsel.InsertSubscriberEnquiryLogAVSRealTime(con, sel);
                        sel.SubscriberEnquiryLogID = intSubscriberEnquiryLogID;

                        //Call Realtime IDV
                        if (intProductId == 202)
                        {

                            string strDHAURL = System.Configuration.ConfigurationSettings.AppSettings["DHAURL"].ToString();

                            XDSPortalEnquiry.Business.ConsumerTrace_IdentityVerification dtConsumertrace = new XDSPortalEnquiry.Business.ConsumerTrace_IdentityVerification();
                            rp = dtConsumertrace.SubmitRealTimeIdentityMaritalVerification(EnquiryCon, AdminCon, sub.SubscriberID, intSystemUserID, 155, sub.SubscriberName, strIDNo, "", "", "", DateTime.Now, strExtRef, true, true, strVoucherCode, strDHAURL);
           
                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                throw new Exception("DHA link is down");
                            }
                        }

                        if (strEntityType == "Individual")
                            eoAccountVerificationWseManager.IDNo = strIDNo;
                        else
                            eoAccountVerificationWseManager.IDNo = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;

                        moAccountVerificationWseManager.ConnectionString = AVSConnection;
                        eoAccountVerificationWseManager.ExternalReference = strExtRef;
                        eoAccountVerificationWseManager.Initials = strInitials.Replace(" ", "").Trim();
                        eoAccountVerificationWseManager.AccNo = strAccNo.Replace(" ", "").Trim();
                        eoAccountVerificationWseManager.Acctype = strAcctype;
                        eoAccountVerificationWseManager.BankName = strBankName;
                        eoAccountVerificationWseManager.BranchCode = strBranchCode.Replace(" ", "").Trim();
                        eoAccountVerificationWseManager.SurName = strSurName.Trim();
                        eoAccountVerificationWseManager.IDType = strIdType;
                        eoAccountVerificationWseManager.SubscriberID = intSubscriberID;
                        eoAccountVerificationWseManager.ContactNumber = strContactNumber;
                        eoAccountVerificationWseManager.EmailAddress = strEmailAddress;
                        // Submit data for matching 
                        rp = moAccountVerificationWseManager.InsertAVSRealTimeWithContacts(eoAccountVerificationWseManager);

                        // Get Response data
                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                        sel.KeyID = rp.ResponseKey;
                        sel.KeyType = rp.ResponseKeyType;
                        sel.ExtraVarInput1 = rp.TmpReference;
                        sel.IsAHVRequest = true;
                        rp.EnquiryLogID = intSubscriberEnquiryLogID;

                        dsel.UpdateSubscriberEnquiryLog(con, sel);

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            sel.EnquiryResult = "E";
                            sel.ErrorDescription = rp.ResponseData.ToString();
                            dsel.UpdateSubscriberEnquiryLogError(con, sel);
                        }

                        if (sub.PayAsYouGo == 1)
                        {
                            dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                        }
                        if (!(strVoucherCode == string.Empty))
                        {
                            dSV.UpdateSubscriberVoucher(AdminConnection, strVoucherCode, sys.Username);
                        }

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }

            catch (Exception oException)
            {
                sel.EnquiryResult = "E";
                sel.ErrorDescription = oException.Message;
                sel.SubscriberEnquiryLogID = dsel.UpdateSubscriberEnquiryLogError(con, sel);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;


                MatchResult = sel.SubscriberEnquiryLogID;
                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }

            ds.Dispose();
            dsLogon.Dispose();
            dsBankName.Dispose();
            dsAcctype.Dispose();

            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response GetAccountVerificationRequestHistory(SqlConnection con, string Status, int SystemUserID, int SubscriberID, bool IsRealTime)
        {
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            XDSPortalEnquiry.Data.SubscriberEnquiryLog dSubscriberEnquiryLog = new XDSPortalEnquiry.Data.SubscriberEnquiryLog();
            DataSet ds = new DataSet("RequestHistory");

            try
            {
                if (IsRealTime)
                {
                    if (Status == "All")
                        ds = dSubscriberEnquiryLog.GetRealTimeSubscriberEnquiryLogDataSet(con, SystemUserID, "100", "", SubscriberID);
                    else
                        ds = dSubscriberEnquiryLog.GetRealTimeSubscriberEnquiryLogDataSet(con, SystemUserID, "100", Status.Substring(0, 1));
                }
                else
                {
                    if (Status == "All")
                        ds = dSubscriberEnquiryLog.GetSubscriberEnquiryLogDataSetpersubscriber(con, SystemUserID, "45", "", SubscriberID);
                    else
                        ds = dSubscriberEnquiryLog.GetSubscriberEnquiryLogDataSetpersubscriber(con, SystemUserID, "45", Status.Substring(0, 1));
                }

                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                rp.ResponseData = ds.GetXml();
            }
            catch (Exception oException)
            {
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();

                SqlConnection.ClearPool(con);
            }

            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response GetAccountVerificationPlusRequestHistory(SqlConnection con, string Status, int SystemUserID, int SubscriberID)
        {
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            XDSPortalEnquiry.Data.SubscriberEnquiryLog dSubscriberEnquiryLog = new XDSPortalEnquiry.Data.SubscriberEnquiryLog();
            DataSet ds = new DataSet("RequestHistory");
            DataSet ds1 = new DataSet();
            try
            {

                //if (Status == "All")
                //    ds = dSubscriberEnquiryLog.GetRealTimeSubscriberEnquiryLogDataSet(con, SystemUserID, "195", "", SubscriberID);
                //else
                //    ds = dSubscriberEnquiryLog.GetRealTimeSubscriberEnquiryLogDataSet(con, SystemUserID, "195", Status.Substring(0, 1));

                if (Status == "All")
                {
                    ds = dSubscriberEnquiryLog.GetRealTimeSubscriberEnquiryLogDataSet(con, SystemUserID, "195", "", SubscriberID);
                    ds1 = dSubscriberEnquiryLog.GetRealTimeSubscriberEnquiryLogDataSet(con, SystemUserID, "202", "", SubscriberID);
                }
                else
                {
                    ds = dSubscriberEnquiryLog.GetRealTimeSubscriberEnquiryLogDataSet(con, SystemUserID, "195", Status.Substring(0, 1));
                    ds1 = dSubscriberEnquiryLog.GetRealTimeSubscriberEnquiryLogDataSet(con, SystemUserID, "202", Status.Substring(0, 1));
                }

                ds.Merge(ds1);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                rp.ResponseData = ds.GetXml();
            }
            catch (Exception oException)
            {
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();

                SqlConnection.ClearPool(con);
            }

            ds.Dispose();
            return rp;
        }


        public XDSPortalLibrary.Entity_Layer.Response GetAccountVerificationRequestHistory(SqlConnection con, string Status, int SystemUserID, int SubscriberID, bool IsRealTime, int EnquiryLogID)
        {
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            XDSPortalEnquiry.Data.SubscriberEnquiryLog dSubscriberEnquiryLog = new XDSPortalEnquiry.Data.SubscriberEnquiryLog();
            DataSet ds = new DataSet("RequestHistory");

            try
            {
                if (IsRealTime)
                {
                    if (Status == "All")
                        ds = dSubscriberEnquiryLog.GetRealTimeSubscriberEnquiryLogDataSet(con, SystemUserID, "100", "", SubscriberID, EnquiryLogID);
                    else
                        ds = dSubscriberEnquiryLog.GetRealTimeSubscriberEnquiryLogDataSet(con, SystemUserID, "100", Status.Substring(0, 1), EnquiryLogID);
                }
                else
                {
                    if (Status == "All")
                        ds = dSubscriberEnquiryLog.GetSubscriberEnquiryLogDataSetpersubscriber(con, SystemUserID, "45", "", SubscriberID, EnquiryLogID);
                    else
                        ds = dSubscriberEnquiryLog.GetSubscriberEnquiryLogDataSetpersubscriber(con, SystemUserID, "45", Status.Substring(0, 1), EnquiryLogID);
                }

                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                rp.ResponseData = ds.GetXml();
            }
            catch (Exception oException)
            {
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();

                SqlConnection.ClearPool(con);
            }

            ds.Dispose();
            return rp;
        }
    }
}
