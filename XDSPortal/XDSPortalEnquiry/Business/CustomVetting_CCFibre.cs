﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using XDSPortalLibrary.Entity_Layer;
using XDSPortalLibrary.Entity_Layer.VettingAA;
using XDSPortalEnquiry.Helpers;
using System.Web.UI.WebControls;
using System.Globalization;
using DevExpress.XtraBars;
using System.Linq;

namespace XDSPortalEnquiry.Business
{
    public class CustomVetting_CCFibre
    {
        readonly string _voucherCode;
        readonly string _dHaurl;
        readonly int _productId;
        readonly VettingAARequest _oRequest;
        VettingAAResponse _vettingAAResponse;
        readonly XDSPortalEnquiry.Data.SubscriberChecks _subscriberChecks;
        readonly DataSet _bonusSegments;

        readonly Data.SystemUser _duser;
        readonly Data.Subscriber _dsubscriber;
        readonly Data.SubscriberEnquiry _dSe;
        readonly Data.SubscriberEnquiryResult _dSC;

        readonly Entity.SystemUser _systemUser;
        readonly Entity.Subscriber _subscriber;
        Entity.SubscriberProductReports _subscriberProductReports;
        readonly SqlConnection _adminConnection;
        readonly SqlConnection _enquirynConnection;
        Entity.SubscriberEnquiry _subscriberEnquiry;
        Entity.SubscriberEnquiryResult _subscriberEnquiryResult;
        XDSDataLibrary.ResponseInformation _responseInformation;

        Response rp;

        public CustomVetting_CCFibre(VettingAARequest iRequest, string VoucherCode, int productId, SqlConnection adminConnection, SqlConnection enquirynConnection,string Dhaurl)
        {
            _oRequest = new VettingAARequest(iRequest);
            _voucherCode = VoucherCode;
            _productId = productId;
            _bonusSegments = null;
            _adminConnection = adminConnection;
            _enquirynConnection = enquirynConnection;

            _vettingAAResponse = new VettingAAResponse();
            _duser = new Data.SystemUser();
            _dsubscriber = new Data.Subscriber();
            _dSe = new Data.SubscriberEnquiry();
            _dSC = new Data.SubscriberEnquiryResult();
            _subscriberEnquiry = new Entity.SubscriberEnquiry();
            _subscriberEnquiryResult = new Entity.SubscriberEnquiryResult();
            _responseInformation = new XDSDataLibrary.ResponseInformation();
            rp = new Response();

            _systemUser = _duser.GetSystemUser(_adminConnection, _oRequest.Username);
            _subscriber = _dsubscriber.GetSubscriberRecord(adminConnection, _systemUser.SubscriberID);

            xdsBilling xb = new xdsBilling();
            _subscriberProductReports = xb.GetPrice(_adminConnection, _subscriber.SubscriberID, productId);

            _subscriberChecks = new Data.SubscriberChecks(_systemUser, _subscriber, _subscriberProductReports, _adminConnection);

            _dHaurl = Dhaurl;
        }

        public VettingAAResponse ProcessVetting()
        {
            Response rp;
            List<string> errorMessages = _oRequest.Validate();

            try
            {
                if (errorMessages.Count > 0)
                {

                    _vettingAAResponse.GetErrorResponse(string.Join(",", errorMessages.Select(s => s)));
                }
                else if (!(_subscriberChecks.IsProfileActive() &&
                        _subscriberChecks.IsValidVoucher(_voucherCode) &&
                        _subscriberChecks.Hasproductaccess(_productId) &&
                        _subscriberChecks.GotEnoughCredit(_bonusSegments)))
                {
                    _vettingAAResponse.GetErrorResponse(_subscriberChecks.Response.ResponseData);
                }
                else
                {
                    _subscriberEnquiry = InsertSubscriberEnquiry();
                    rp = ConsumerMatch();

                    if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                    {
                        Logerror(rp.ResponseData);
                        _vettingAAResponse.GetErrorResponse(rp.ResponseData);
                    }
                    else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                    {
                        Lognone();
                        _vettingAAResponse.GetErrorResponse("No match found");
                    }
                    else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                    {
                        System.IO.StringReader xmlSR = new System.IO.StringReader(rp.ResponseData);
                        DataSet ds = new DataSet();

                        ds.ReadXml(xmlSR);
                        ds.Tables[0].Columns.Add("EnquiryID");
                        ds.Tables[0].Columns.Add("EnquiryResultID");
                        ds.Tables[0].Columns.Add("Reference");

                        DataSet dsMatchData = LogMultiple(ds, rp.ResponseKeyType);

                        if (dsMatchData.Tables[0].Rows.Count > 1)
                        {
                            _vettingAAResponse.GetErrorResponse("Multiple match found");
                        }
                        else
                        {
                            foreach (DataRow dr in dsMatchData.Tables[0].Rows)
                            {
                                _vettingAAResponse.BureauMatched = true;
                                // trigger vetting
                                rp = GetVettingResult();

                                if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                                {
                                    Logerror(rp.ResponseData);
                                    _vettingAAResponse.GetErrorResponse(rp.ResponseData);
                                }
                                else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                                {
                                    _subscriberEnquiryResult.Billable = true;
                                    _subscriberEnquiryResult.EnquiryResult = "R";
                                    _subscriberEnquiryResult.DetailsViewedDate = DateTime.Now;
                                    _subscriberEnquiryResult.XMLData = Serialization.Serialize(_vettingAAResponse);

                                    _dSC.UpdateSubscriberEnquiryResult(_enquirynConnection, _subscriberEnquiryResult);
                                }
                            }
                        }
                    }

                }
            }
            catch (Exception e)
            {
                _vettingAAResponse.GetErrorResponse(e.Message);
            }
            FillResponseobject(_vettingAAResponse);

            ProcessPolicyRules();
            return _vettingAAResponse;
        }

        private Entity.SubscriberEnquiry InsertSubscriberEnquiry()
        {
            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
            eSe.SubscriberID = _subscriber.SubscriberID;


            DateTime dDate = DateTime.Now;

           if(!DateTime.TryParse(_oRequest.Dateofbirth.ToString(), out dDate))
            {
                dDate = DateTime.Now;
            }

            eSe.BirthDate = dDate;
            eSe.FirstName = _oRequest.Firstname;
            eSe.IDNo = _oRequest.Idtype == eIdType.Said ? _oRequest.Idorpassportnumber : string.Empty;
            eSe.PassportNo = _oRequest.Idtype == eIdType.Passport ? _oRequest.Idorpassportnumber : string.Empty;
            eSe.ProductID = _productId;
            eSe.SubscriberEnquiryDate = DateTime.Now;
            eSe.SubscriberName = _subscriber.SubscriberName;
            eSe.SubscriberReference = _oRequest.Transactionnumber.ToString();
            eSe.Surname = _oRequest.Surname;
            eSe.SystemUserID = _systemUser.SystemUserID;
            eSe.SystemUser = _systemUser.SystemUserFullName;
            eSe.CreatedByUser = _systemUser.Username;
            eSe.CreatedOnDate = DateTime.Now;
            eSe.SearchInput = "";
            eSe.NetMonthlyIncomeAmt = decimal.Parse(_oRequest.Grossmonthlyincome.ToString());

            //Generate serach input string

            if (!string.IsNullOrEmpty(eSe.IDNo))
            {
                eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
            }
            if (!string.IsNullOrEmpty(eSe.PassportNo))
            {
                eSe.SearchInput = eSe.SearchInput + " | " + eSe.PassportNo;
            }
            if (!string.IsNullOrEmpty(eSe.Surname))
            {
                eSe.SearchInput = eSe.SearchInput + " | " + eSe.Surname;
            }

            if (eSe.SearchInput.Length > 0)
            {
                eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
            }

            // Log the Current Enquiry to SubscriberEnquiry Table

            return dSe.GetSubscriberEnquiryObject(_enquirynConnection, dSe.InsertSubscriberEnquiry(_enquirynConnection, eSe));
        }

        private Response ConsumerMatch()
        {
            Response rp;
            ConsumerTrace eoConsumerTraceWseManager = GenerateConsumerTraceManager();

            string adminConnectionstring = _adminConnection.ConnectionString;
            string reportConnectionstring = _subscriberEnquiry.ExtraVarInput1;

            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(adminConnectionstring, reportConnectionstring);
            rp = ra.ConsumerTraceMatch(eoConsumerTraceWseManager);

            //If error raise one last call with a different connection
            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
            {
                _subscriberEnquiry = _dSe.ErrorGetSubscriberEnquiryObject(_enquirynConnection, _subscriberEnquiry.SubscriberEnquiryID);
                if (reportConnectionstring != _subscriberEnquiry.ExtraVarInput1)
                {
                    ra = new XDSDataLibrary.ReportAccess(adminConnectionstring, _subscriberEnquiry.ExtraVarInput1);
                    rp = ra.ConsumerTraceMatch(eoConsumerTraceWseManager);
                }
            }

            return rp;

        }

        private ConsumerTrace GenerateConsumerTraceManager()
        {
            return new ConsumerTrace()
            {
                DataSegments = _subscriberEnquiry.ExtraVarInput2,
                ReferenceNo = _subscriberEnquiry.SubscriberEnquiryID.ToString(),
                DOB = _subscriberEnquiry.BirthDate,
                ExternalReference = _subscriberEnquiry.SubscriberReference,
                FirstName = _subscriberEnquiry.FirstName,
                IDno = _subscriberEnquiry.IDNo,
                Passportno = _subscriberEnquiry.PassportNo,
                ProductID = _productId,
                subscriberID = _subscriber.SubscriberID,
                Surname = _subscriberEnquiry.Surname
            };
        }

        private void Logerror(string message)
        {
            _subscriberEnquiry.ErrorDescription = message;
            _dSe.UpdateSubscriberEnquiryError(_enquirynConnection, _subscriberEnquiry);
        }
        private void Lognone()
        {
            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            _dSC.InsertSubscriberEnquiryResult(_enquirynConnection, new Entity.SubscriberEnquiryResult()
            {
                SearchOutput = "",
                IDNo = "",
                PassportNo = "",
                Surname = "",
                FirstName = "",
                BirthDate = new DateTime(1990, 1, 1, 00, 00, 0, DateTimeKind.Local),
                Gender = "",
                DetailsViewedYN = false,
                Billable = false,
                SubscriberEnquiryID = _subscriberEnquiry.SubscriberEnquiryID,
                EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString(),
                CreatedByUser = _subscriberEnquiry.CreatedByUser,
                CreatedOnDate = DateTime.Now
            });

        }
        private DataSet LogMultiple(DataSet ds, string responsekeytype)
        {
            int subscriberEnquiryResultID = 0;

            _subscriberEnquiryResult = new Entity.SubscriberEnquiryResult();

            if (ds.Tables.Contains("ConsumerDetails"))
            {
                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                {
                    GenerateSubscriberEnquiryResultObject(r);
                    _subscriberEnquiryResult.KeyType = responsekeytype;
                    string BonusXML = string.Empty;
                    BonusXML = r["BonusXML"].ToString();
                    if (BonusXML != string.Empty)
                    {
                        DataSet dsBonus = new DataSet();
                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                        dsBonus.ReadXml(BonusxmlSR);
                        if (dsBonus.Tables.Contains("Segments"))
                        {
                            _subscriberEnquiryResult.BonusIncluded = true;
                            _subscriberEnquiryResult.XMLBonus = BonusXML.Replace("'", "''");

                            Int64 intSubscriberEnquiryResultID = _dSC.InsertSubscriberEnquiryResult(_enquirynConnection, _subscriberEnquiryResult);

                            LogBonusSegments(intSubscriberEnquiryResultID, dsBonus.Tables[0]);

                        }
                        dsBonus.Dispose();
                    }
                    else
                    {

                        // Insert the match results to SubscriberEnquiryResult Table
                        subscriberEnquiryResultID = _dSC.InsertSubscriberEnquiryResult(_enquirynConnection, _subscriberEnquiryResult);

                    }
                    _subscriberEnquiryResult = _dSC.GetSubscriberEnquiryResultObject(_enquirynConnection, _subscriberEnquiry.SubscriberID, _subscriberEnquiry.CreatedByUser, _subscriberEnquiry.SubscriberEnquiryID, subscriberEnquiryResultID);

                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = _subscriberEnquiryResult.SubscriberEnquiryID;
                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = subscriberEnquiryResultID;
                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = _subscriberEnquiryResult.Reference;

                }
            }
            return ds;
        }
        private void LogBonusSegments(Int64 subscriberEnquiryResultID, DataTable dtBonus)
        {
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            foreach (DataRow dr in dtBonus.Rows)
            {
                eSB.SubscriberEnquiryResultID = int.Parse(subscriberEnquiryResultID.ToString());
                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                eSB.Billable = false;
                eSB.CreatedByUser = _subscriberEnquiry.CreatedByUser;
                eSB.CreatedOnDate = DateTime.Now;

                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                dSB.InsertSubscriberEnquiryResultBonus(_enquirynConnection, eSB);
            }
        }

        private void GenerateSubscriberEnquiryResultObject(DataRow r)
        {
            _subscriberEnquiryResult.SearchOutput = "";
            _subscriberEnquiryResult.IDNo = "";
            _subscriberEnquiryResult.PassportNo = "";
            _subscriberEnquiryResult.Surname = "";
            _subscriberEnquiryResult.FirstName = "";
            _subscriberEnquiryResult.BirthDate = new DateTime(1990, 1, 1, 00, 00, 0, DateTimeKind.Local);
            _subscriberEnquiryResult.Gender = "";

            if (r.Table.Columns.Contains("IDNo") && !string.IsNullOrEmpty(r["IDNo"].ToString()))
            {
                _subscriberEnquiryResult.IDNo = r["IDNo"].ToString();
                _subscriberEnquiryResult.SearchOutput = _subscriberEnquiryResult.SearchOutput + " | " + _subscriberEnquiryResult.IDNo;
            }
            if (r.Table.Columns.Contains("PassportNo") && !string.IsNullOrEmpty(r["PassportNo"].ToString()))
            {
                _subscriberEnquiryResult.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                _subscriberEnquiryResult.SearchOutput = _subscriberEnquiryResult.SearchOutput + " | " + _subscriberEnquiryResult.PassportNo;
            }
            if (r.Table.Columns.Contains("Surname") && !string.IsNullOrEmpty(r["Surname"].ToString()))
            {
                _subscriberEnquiryResult.Surname = r["Surname"].ToString().Replace("'", "''");
                _subscriberEnquiryResult.SearchOutput = _subscriberEnquiryResult.SearchOutput + " | " + _subscriberEnquiryResult.Surname;
            }

            if (r.Table.Columns.Contains("FirstName") && !string.IsNullOrEmpty(r["FirstName"].ToString()))
            {
                _subscriberEnquiryResult.FirstName = r["FirstName"].ToString().Replace("'", "''");
                _subscriberEnquiryResult.SearchOutput = _subscriberEnquiryResult.SearchOutput + " | " + _subscriberEnquiryResult.FirstName;
            }
            if (r.Table.Columns.Contains("SecondName") && !string.IsNullOrEmpty(r["SecondName"].ToString()))
            {
                _subscriberEnquiryResult.SecondName = r["SecondName"].ToString().Replace("'", "''");
                _subscriberEnquiryResult.SearchOutput = _subscriberEnquiryResult.SearchOutput + " | " + _subscriberEnquiryResult.SecondName;
            }
            if (r.Table.Columns.Contains("BirthDate") && !string.IsNullOrEmpty(r["BirthDate"].ToString()))
            {
                _subscriberEnquiryResult.BirthDate = Convert.ToDateTime(r["BirthDate"]);
                _subscriberEnquiryResult.SearchOutput = _subscriberEnquiryResult.SearchOutput + " | " + Convert.ToDateTime(r["BirthDate"]).ToShortDateString();
            }
            if (r.Table.Columns.Contains("GenderInd") && !string.IsNullOrEmpty(r["GenderInd"].ToString()))
            {
                _subscriberEnquiryResult.Gender = r["GenderInd"].ToString().Replace("'", "''");
                _subscriberEnquiryResult.SearchOutput = _subscriberEnquiryResult.SearchOutput + " | " + _subscriberEnquiryResult.Gender;
            }

            if (_subscriberEnquiryResult.SearchOutput.Length > 0)
            {
                _subscriberEnquiryResult.SearchOutput = _subscriberEnquiryResult.SearchOutput.Substring(3, _subscriberEnquiryResult.SearchOutput.Length - 3).ToUpper();
            }
            _subscriberEnquiryResult.DetailsViewedYN = false;
            _subscriberEnquiryResult.Billable = false;
            _subscriberEnquiryResult.CreatedByUser = _subscriberEnquiry.CreatedByUser;
            _subscriberEnquiryResult.CreatedOnDate = DateTime.Now;
            _subscriberEnquiryResult.KeyID = int.Parse(r["ConsumerID"].ToString());


            _subscriberEnquiryResult.BillingTypeID = _subscriberProductReports.BillingTypeID;
            _subscriberEnquiryResult.BillingPrice = _subscriberProductReports.UnitPrice;

            _subscriberEnquiryResult.SubscriberEnquiryID = _subscriberEnquiry.SubscriberEnquiryID;
            _subscriberEnquiryResult.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
            _subscriberEnquiryResult.ExtraVarOutput1 = "";
            _subscriberEnquiryResult.ProductID = _productId;
            _subscriberEnquiryResult.VoucherCode = _voucherCode;
        }

        private void FillResponseobject(VettingAAResponse aResponse)
        {
            if (aResponse == null)
            {
                aResponse = new VettingAAResponse();
            }
            aResponse.Agilitytransactionnumber = _oRequest.Transactionnumber;
            aResponse.AgilityCustomertype = _oRequest.Customertype;
            aResponse.AgilityExistingcustomer = _oRequest.Existingcustomer;
            aResponse.Agilitytransactiondatetime = _oRequest.Transactiondatetime;
            aResponse.XDStransactiondatetime = DateTime.Now;
            aResponse.XDStransactionnumber =  _subscriberEnquiryResult.SubscriberEnquiryResultID.ToString();
            aResponse.XDSconsumernumber = _subscriberEnquiryResult.KeyID;
            aResponse.Numberofservicesrequested = _oRequest.Numberofservicesappliedfor;
            aResponse.IDtype = _oRequest.Idtype;
            aResponse.IDorpassportnumber = _oRequest.Idorpassportnumber;
            aResponse.Dateofbirth = _oRequest.Dateofbirth;

        }

        private ConsumerTrace GenerateSearchManager()
        {
            return new ConsumerTrace()
            {
                DataSegments = _subscriberEnquiry.ExtraVarInput2,
                ReferenceNo = _subscriberEnquiry.SubscriberEnquiryID.ToString(),
                DOB = _subscriberEnquiry.BirthDate,
                ExternalReference = _subscriberEnquiry.SubscriberReference,
                FirstName = _subscriberEnquiry.FirstName,
                IDno = _subscriberEnquiry.IDNo,
                Passportno = _subscriberEnquiry.PassportNo,
                ProductID = _productId,
                subscriberID = _subscriber.SubscriberID,
                Surname = _subscriberEnquiry.Surname,
                GrossMonthlyIncome = double.Parse(_subscriberEnquiry.NetMonthlyIncomeAmt.ToString()),
                ConsumerID = _subscriberEnquiryResult.KeyID,
                EnquiryID = _subscriberEnquiry.SubscriberEnquiryID,
                EnquiryResultID = _subscriberEnquiryResult.SubscriberEnquiryResultID,
                SubscriberAssociationCode = _subscriber.SubscriberAssociationCode,
                AssociationTypeCode = _subscriber.AssociationTypeCode
            };
        }

        private Response GetVettingResult()
        {
            ConsumerTrace eoConsumerTraceWseManager = GenerateSearchManager();

            string adminConnectionstring = _adminConnection.ConnectionString;
            string reportConnectionstring = _subscriberEnquiry.ExtraVarInput1;

            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(adminConnectionstring, reportConnectionstring);


            ProcessRIDV();

            rp = ra.ProcessAAvetting(_subscriberProductReports.ReportID, eoConsumerTraceWseManager, _vettingAAResponse, out _vettingAAResponse, out _responseInformation);

            //If error raise one last call with a different connection
            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
            {
                _subscriberEnquiry = _dSe.ErrorGetSubscriberEnquiryObject(_enquirynConnection, _subscriberEnquiry.SubscriberEnquiryID);
                if (reportConnectionstring != _subscriberEnquiry.ExtraVarInput1)
                {
                    ra = new XDSDataLibrary.ReportAccess(adminConnectionstring, _subscriberEnquiry.ExtraVarInput1);
                    rp = ra.ProcessAAvetting(_subscriberProductReports.ReportID, eoConsumerTraceWseManager, _vettingAAResponse, out _vettingAAResponse, out _responseInformation);
                }
            }

            return rp;

        }

        private void ProcessRIDV()
        {
            DateTime date = new DateTime();

            XDSPortalEnquiry.Business.ConsumerTrace_IdentityVerification dtConsumertrace = new XDSPortalEnquiry.Business.ConsumerTrace_IdentityVerification();
            rp = dtConsumertrace.SubmitRealTimeIdentityMaritalVerification(_enquirynConnection, _adminConnection, _subscriber.SubscriberID, _systemUser.SystemUserID, 155, _subscriber.SubscriberName,
                _oRequest.Idorpassportnumber, _oRequest.Surname, _oRequest.Firstname, string.Empty, _oRequest.Dateofbirth, _oRequest.Transactionnumber.ToString(), false, false, _voucherCode, _dHaurl);

            if (rp.ResponseStatus == Response.ResponseStatusEnum.Report)
            {
                System.IO.StringReader xmlSR = new System.IO.StringReader(rp.ResponseData);
                DataSet ds = new DataSet();
                ds.ReadXml(xmlSR);
                if (ds.Tables.Contains("RealTimeIDVAndMaritalCheck"))
                {
                    // pull the values and fill the response object
                    _vettingAAResponse.IDverified = !string.IsNullOrEmpty(ds.Tables["RealTimeIDVAndMaritalCheck"].Rows[0]["HAIDNO"].ToString());
                    _vettingAAResponse.Deceased = (ds.Tables["RealTimeIDVAndMaritalCheck"].Rows[0]["HADeceasedStatus"].ToString().ToLower() == "deceased");
                    _vettingAAResponse.IDissued = DateTime.TryParseExact(ds.Tables["RealTimeIDVAndMaritalCheck"].Rows[0]["IdCardDate"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out date) ||
                        DateTime.TryParseExact(ds.Tables["RealTimeIDVAndMaritalCheck"].Rows[0]["HAIDBookIssuedDate"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out date);

                }

            }

        }

        private PolicyRules GetPolicyRule(string rulenumber, string decision)
        {
            return new PolicyRules { Rulenumber = rulenumber, Decision = decision };
        }

        private int CalculateAge(DateTime birthDate, DateTime now)
        {
            int age = now.Year - birthDate.Year;

            if (now.Month < birthDate.Month || (now.Month == birthDate.Month && now.Day < birthDate.Day))
                age--;

            return age;
        }

        private void ProcessPolicyRules()
        {
            if (_vettingAAResponse == null) { _vettingAAResponse = new VettingAAResponse(); }
            if (_vettingAAResponse.Policyrulestriggered == null)
            {
                _vettingAAResponse.Policyrulestriggered = new List<PolicyRules>();
            }
            if (!_vettingAAResponse.BureauMatched) { _vettingAAResponse.Policyrulestriggered.Add(GetPolicyRule("P0001", "Refer")); }
            else { _vettingAAResponse.Policyrulestriggered.Add(GetPolicyRule("P0001", "")); }
            if (!_vettingAAResponse.IDissued ||
                !_vettingAAResponse.IDverified) { _vettingAAResponse.Policyrulestriggered.Add(GetPolicyRule("P0002", "Refer")); }
            else { _vettingAAResponse.Policyrulestriggered.Add(GetPolicyRule("P0002", "")); }
            if (_responseInformation.SafpsFlag) { _vettingAAResponse.Policyrulestriggered.Add(GetPolicyRule("P0003", "Refer")); }
            else { _vettingAAResponse.Policyrulestriggered.Add(GetPolicyRule("P0003", "")); }
            if (_vettingAAResponse.Totalownenquiriesinlast90days >= 1) { _vettingAAResponse.Policyrulestriggered.Add(GetPolicyRule("P0004", "Refer")); }
            else { _vettingAAResponse.Policyrulestriggered.Add(GetPolicyRule("P0004", "")); }

            if (_vettingAAResponse.Numberofservicesrequested > 2) { _vettingAAResponse.Policyrulestriggered.Add(GetPolicyRule("P0005", "Refer")); }
            else { _vettingAAResponse.Policyrulestriggered.Add(GetPolicyRule("P0005", "")); }

            if (_vettingAAResponse.IDtype == eIdType.Passport) { _vettingAAResponse.Policyrulestriggered.Add(GetPolicyRule("P0006", "Refer")); }
            else { _vettingAAResponse.Policyrulestriggered.Add(GetPolicyRule("P0006", "")); }

            if (_vettingAAResponse.Deceased) { _vettingAAResponse.Policyrulestriggered.Add(GetPolicyRule("D0001", "Decline")); }
            else { _vettingAAResponse.Policyrulestriggered.Add(GetPolicyRule("D0001", "")); }

            if (_vettingAAResponse.UnderDebtReview) { _vettingAAResponse.Policyrulestriggered.Add(GetPolicyRule("D0002", "Decline")); }
            else { _vettingAAResponse.Policyrulestriggered.Add(GetPolicyRule("D0002", "")); }

            if (CalculateAge(_vettingAAResponse.Dateofbirth, DateTime.Now) < 18) { _vettingAAResponse.Policyrulestriggered.Add(GetPolicyRule("D0003", "Decline")); }
            else { _vettingAAResponse.Policyrulestriggered.Add(GetPolicyRule("D0003", "")); }

            if (_vettingAAResponse.RiskGrade == 0) { _vettingAAResponse.Policyrulestriggered.Add(GetPolicyRule("D0004", "Decline")); }
            else { _vettingAAResponse.Policyrulestriggered.Add(GetPolicyRule("D0004", "")); }

            if (_vettingAAResponse.RiskGrade == 1 &&
                _oRequest.Grossmonthlyincome > 0 && _oRequest.Grossmonthlyincome < 4000 &&
                 _responseInformation.PredictedIncome > 0 && _responseInformation.PredictedIncome < 4000
                ) { _vettingAAResponse.Policyrulestriggered.Add(GetPolicyRule("D0005", "Decline")); }
            else { _vettingAAResponse.Policyrulestriggered.Add(GetPolicyRule("D0005", "")); }

            if (_responseInformation.isLatestStatusonCPANegative || _responseInformation.isLatestStatusonNLRNegative ) { _vettingAAResponse.Policyrulestriggered.Add(GetPolicyRule("D0007", "Decline")); }
            else { _vettingAAResponse.Policyrulestriggered.Add(GetPolicyRule("D0007", "")); }

            if (_vettingAAResponse.Numberofaccounts3plusinarrears >= 1) { _vettingAAResponse.Policyrulestriggered.Add(GetPolicyRule("D0008", "Decline")); }
            else { _vettingAAResponse.Policyrulestriggered.Add(GetPolicyRule("D0008", "")); }

            if (_responseInformation.NumberofNLR3plusinarrears >= 1) { _vettingAAResponse.Policyrulestriggered.Add(GetPolicyRule("D0009", "Decline")); }
            else { _vettingAAResponse.Policyrulestriggered.Add(GetPolicyRule("D0009", "")); }

            if (_vettingAAResponse.Numberofcourtnotices > 1) { _vettingAAResponse.Policyrulestriggered.Add(GetPolicyRule("D00010", "Decline")); }
            else { _vettingAAResponse.Policyrulestriggered.Add(GetPolicyRule("D0010", "")); }

            if (_vettingAAResponse.FraudDatabaseMatch ) { _vettingAAResponse.Policyrulestriggered.Add(GetPolicyRule("D00011", "Decline")); }
            else { _vettingAAResponse.Policyrulestriggered.Add(GetPolicyRule("D0011", "")); }
        }

    }
}
