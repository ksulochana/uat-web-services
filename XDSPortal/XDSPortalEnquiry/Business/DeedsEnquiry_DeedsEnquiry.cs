﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace XDSPortalEnquiry.Business
{
    public enum SearchtypeInd
    {
        IDNo=1,
        BusRegistrationNO=2,
        BusinessName=3,
        BondAccNo =4,
        TitleDeedNo=5,
        ErfNo=6
    }
    public class DeedsEnquiry_DeedsEnquiry
    {
        private XDSPortalLibrary.Business_Layer.DeedsEnquiry moDeedsEnquiryWseManager;
        private XDSPortalLibrary.Entity_Layer.DeedsEnquiry eoDeedsEnquiryWseManager;

        public DeedsEnquiry_DeedsEnquiry()
        {
            moDeedsEnquiryWseManager = new XDSPortalLibrary.Business_Layer.DeedsEnquiry();
            eoDeedsEnquiryWseManager = new XDSPortalLibrary.Entity_Layer.DeedsEnquiry();
        }
        public XDSPortalLibrary.Entity_Layer.Response SubmitDeedsEnquiry(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, SearchtypeInd intSearchTypeInd, string strDeedsOffice, string strIDNo, string strFirstname, string strSurName, string strRegNo1, string strRegNo2, string strRegNo3, string strBusinessname, string strTownshipName, string strErfNo, string strPortionNo, string strTitleDeedNo, string strBondAccNo, string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            string rXml = "";
            int MatchResult = 0;
            int intSubscriberEnquiryResultID = 0;
            DataSet ds = new DataSet();
            Boolean boolSubmitEnquiry = true;
            string strValidationStatus = "";

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);


            try
            {
                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {
                    // Check the PayAsYouGo settings for the subscriber
                    // Check the PayAsYouGo settings for the subscriber
                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {

                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;
                        
                        eSe.PassportNo = strDeedsOffice;
                        eSe.IDNo = strIDNo;
                        eSe.FirstName = strFirstname;
                        eSe.Surname = strSurName;
                        eSe.ProductID = intProductId;
                        eSe.BusRegistrationNo = strRegNo1 + strRegNo2 + strRegNo3;
                        eSe.BusBusinessName = strBusinessname;
                        eSe.MaidenName = strTownshipName;
                        eSe.TelephoneNo = strErfNo;
                        eSe.TelephoneCode = strPortionNo;
                        eSe.AccountNo = strBondAccNo;
                        eSe.SubAccountNo = strTitleDeedNo;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = strExtRef;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";

                        //Generate serach input string

                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.FirstName))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.FirstName;
                        }
                        if (!string.IsNullOrEmpty(eSe.Surname))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.Surname;
                        }
                        
                        if (!string.IsNullOrEmpty(eSe.BusRegistrationNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        }
                        if (!string.IsNullOrEmpty(eSe.BusBusinessName))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.BusBusinessName;
                        }
                        if (!string.IsNullOrEmpty(strTownshipName))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + strTownshipName;
                        }
                        if (!string.IsNullOrEmpty(strErfNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + strErfNo;
                        }
                        if (!string.IsNullOrEmpty(strPortionNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + strPortionNo;
                        }
                        if (!string.IsNullOrEmpty(strTitleDeedNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + strTitleDeedNo;
                        }
                        if (!string.IsNullOrEmpty(strBondAccNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + strBondAccNo;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table

                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);
                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                        
                        eoDeedsEnquiryWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoDeedsEnquiryWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoDeedsEnquiryWseManager.ConfirmationChkBox = bConfirmationChkBox;
                        eoDeedsEnquiryWseManager.ExternalReference = strExtRef;
                        if (!String.IsNullOrEmpty(strIDNo))
                        {
                            eoDeedsEnquiryWseManager.IDno = strIDNo;
                        }
                        else if (!String.IsNullOrEmpty(strRegNo1))
                        {
                            eoDeedsEnquiryWseManager.IDno = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        }
                        eoDeedsEnquiryWseManager.ProductID = intProductId;
                        eoDeedsEnquiryWseManager.subscriberID = intSubscriberID;
                        eoDeedsEnquiryWseManager.TownshipName = strTownshipName;
                        eoDeedsEnquiryWseManager.ErfNo = strErfNo;
                        eoDeedsEnquiryWseManager.BondAccNo = strBondAccNo;
                        eoDeedsEnquiryWseManager.TitleDeedNo = strTitleDeedNo;
                        eoDeedsEnquiryWseManager.DeedsOffice = strDeedsOffice;
                        eoDeedsEnquiryWseManager.FirstName = strFirstname;
                        eoDeedsEnquiryWseManager.SurName = strSurName;
                        eoDeedsEnquiryWseManager.BusinessName = strBusinessname;
                        eoDeedsEnquiryWseManager.PortionNo = strPortionNo;
                        eoDeedsEnquiryWseManager.SearchTypeInd = Convert.ToInt16(intSearchTypeInd);
                        eoDeedsEnquiryWseManager.BonusCheck = bBonusChecking;


                        // Submit data for matching 

                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.DeedsEnquiryMatch(eoDeedsEnquiryWseManager);

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.DeedsEnquiryMatch(eoDeedsEnquiryWseManager);
                        }

                        rp.EnquiryID = intSubscriberEnquiryID;

                        // Get Response data

                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);


                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            eSC.SearchOutput = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.DetailsViewedDate = DateTime.Now;
                            eSC.DetailsViewedYN = false;
                            eSC.Billable = false;
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;
                            eSC.SearchOutput = "";
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();

                            rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                            rp.EnquiryID = eSC.SubscriberEnquiryID;
                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);


                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            ds.ReadXml(xmlSR);
                            if (ds.Tables.Contains("PropertyInformation"))
                            {
                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, intProductId);

                                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);
                                rXml = ds.GetXml();
                                rp.ResponseData = rXml;

                                foreach (DataRow r in ds.Tables["PropertyInformation"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    eSC.Billable = true;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;

                                    if (r.Table.Columns.Contains("PropertyType"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PropertyType"].ToString()))
                                        {
                                            eSC.Ha_FirstName = r["PropertyType"].ToString();

                                        }
                                    }

                                    if (r.Table.Columns.Contains("StandNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["StandNo"].ToString()))
                                        {
                                            eSC.Ha_SecondName = r["StandNo"].ToString();

                                        }
                                    }

                                    if (r.Table.Columns.Contains("TownshipName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["TownshipName"].ToString()))
                                        {
                                            eSC.Ha_CauseOfDeath = r["TownshipName"].ToString().Replace("'", "''");

                                        }
                                    }
                                    if (r.Table.Columns.Contains("Province"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Province"].ToString()))
                                        {
                                            eSC.Ha_DeceasedStatus = r["Province"].ToString().Replace("'", "''");

                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }


                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                    eSC.ExtraVarOutput1 = rp.TmpReference;

                                    rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    // Change the PayAsYouGoEnquiryLimit  after viewing the report

                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                                }
                            }
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("PropertyDetails"))
                            {
                                foreach (DataRow r in ds.Tables["PropertyDetails"].Rows)
                                {

                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    
                                    if (r.Table.Columns.Contains("TitleDeedNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["TitleDeedNo"].ToString()))
                                        {
                                            eSC.IDNo = r["TitleDeedNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + r["TitleDeedNo"].ToString();;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PropertyTypeDesc"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PropertyTypeDesc"].ToString()))
                                        {
                                            eSC.Ha_FirstName = r["PropertyTypeDesc"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Ha_FirstName;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("BuyerName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["BuyerName"].ToString()))
                                        {
                                            eSC.FirstName = r["BuyerName"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("SellerName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["SellerName"].ToString()))
                                        {
                                            eSC.SecondName = r["SellerName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.SecondName;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PortionNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PortionNo"].ToString()))
                                        {
                                            eSC.Surname = r["PortionNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("StandNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["StandNo"].ToString()))
                                        {
                                            eSC.BusRegistrationNo = r["StandNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusRegistrationNo;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }
                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.KeyID = int.Parse(r["PropertyDeedID"].ToString());
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;


                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {
                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");


                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);

                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;
                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;

                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);
                                            }



                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {

                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();

                                }
                            }

                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("PropertyDetails"))
                            {
                                foreach (DataRow r in ds.Tables["PropertyDetails"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("TitleDeedNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["TitleDeedNo"].ToString()))
                                        {
                                            eSC.IDNo = r["TitleDeedNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + r["TitleDeedNo"].ToString(); ;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("PropertyTypeDesc"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PropertyTypeDesc"].ToString()))
                                        {
                                            eSC.Ha_FirstName = r["PropertyTypeDesc"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Ha_FirstName;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("BuyerName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["BuyerName"].ToString()))
                                        {
                                            eSC.FirstName = r["BuyerName"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("SellerName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["SellerName"].ToString()))
                                        {
                                            eSC.SecondName = r["SellerName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.SecondName;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PortionNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PortionNo"].ToString()))
                                        {
                                            eSC.Surname = r["PortionNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("StandNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["StandNo"].ToString()))
                                        {
                                            eSC.BusRegistrationNo = r["StandNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusRegistrationNo;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.KeyID = int.Parse(r["PropertyDeedID"].ToString());
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {


                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");

                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;

                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;

                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);

                                            }

                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {

                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }


                        }

                    }

                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {

                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);

                MatchResult = eSe.SubscriberEnquiryID;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitIndividualDeedsEnquiry(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, SearchtypeInd intSearchTypeInd, string strDeedsOffice, string strIDNo, string strFirstname, string strSurName, string strRegNo1, string strRegNo2, string strRegNo3, string strBusinessname, string strTownshipName, string strErfNo, string strPortionNo, string strTitleDeedNo, string strBondAccNo, string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            string rXml = "";
            int MatchResult = 0;
            int intSubscriberEnquiryResultID = 0;
            DataSet ds = new DataSet();
            Boolean boolSubmitEnquiry = true;
            string strValidationStatus = "";

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);


            try
            {
                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {
                    // Check the PayAsYouGo settings for the subscriber
                    // Check the PayAsYouGo settings for the subscriber
                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {

                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;

                        eSe.PassportNo = strDeedsOffice;
                        eSe.IDNo = strIDNo;
                        eSe.FirstName = strFirstname;
                        eSe.Surname = strSurName;
                        eSe.ProductID = intProductId;
                        eSe.BusRegistrationNo = strRegNo1 + strRegNo2 + strRegNo3;
                        eSe.BusBusinessName = strBusinessname;
                        eSe.MaidenName = strTownshipName;
                        eSe.TelephoneNo = strErfNo;
                        eSe.TelephoneCode = strPortionNo;
                        eSe.AccountNo = strBondAccNo;
                        eSe.SubAccountNo = strTitleDeedNo;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = strExtRef;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";

                        //Generate serach input string

                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.FirstName))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.FirstName;
                        }
                        if (!string.IsNullOrEmpty(eSe.Surname))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.Surname;
                        }

                        if (!string.IsNullOrEmpty(eSe.BusRegistrationNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        }
                        if (!string.IsNullOrEmpty(eSe.BusBusinessName))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.BusBusinessName;
                        }
                        if (!string.IsNullOrEmpty(strTownshipName))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + strTownshipName;
                        }
                        if (!string.IsNullOrEmpty(strErfNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + strErfNo;
                        }
                        if (!string.IsNullOrEmpty(strPortionNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + strPortionNo;
                        }
                        if (!string.IsNullOrEmpty(strTitleDeedNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + strTitleDeedNo;
                        }
                        if (!string.IsNullOrEmpty(strBondAccNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + strBondAccNo;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table

                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);
                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        eoDeedsEnquiryWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoDeedsEnquiryWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoDeedsEnquiryWseManager.ConfirmationChkBox = bConfirmationChkBox;
                        eoDeedsEnquiryWseManager.ExternalReference = strExtRef;
                        if (!String.IsNullOrEmpty(strIDNo))
                        {
                            eoDeedsEnquiryWseManager.IDno = strIDNo;
                        }
                        else if (!String.IsNullOrEmpty(strRegNo1))
                        {
                            eoDeedsEnquiryWseManager.IDno = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        }
                        eoDeedsEnquiryWseManager.ProductID = intProductId;
                        eoDeedsEnquiryWseManager.subscriberID = intSubscriberID;
                        eoDeedsEnquiryWseManager.TownshipName = strTownshipName;
                        eoDeedsEnquiryWseManager.ErfNo = strErfNo;
                        eoDeedsEnquiryWseManager.BondAccNo = strBondAccNo;
                        eoDeedsEnquiryWseManager.TitleDeedNo = strTitleDeedNo;
                        eoDeedsEnquiryWseManager.DeedsOffice = strDeedsOffice;
                        eoDeedsEnquiryWseManager.FirstName = strFirstname;
                        eoDeedsEnquiryWseManager.SurName = strSurName;
                        eoDeedsEnquiryWseManager.BusinessName = strBusinessname;
                        eoDeedsEnquiryWseManager.PortionNo = strPortionNo;
                        eoDeedsEnquiryWseManager.SearchTypeInd = Convert.ToInt16(intSearchTypeInd);
                        eoDeedsEnquiryWseManager.BonusCheck = bBonusChecking;


                        // Submit data for matching 

                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.IndividualDeedsEnquiryMatch(eoDeedsEnquiryWseManager);

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.IndividualDeedsEnquiryMatch(eoDeedsEnquiryWseManager);
                        }

                        rp.EnquiryID = intSubscriberEnquiryID;

                        // Get Response data

                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);


                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            eSC.SearchOutput = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.DetailsViewedDate = DateTime.Now;
                            eSC.DetailsViewedYN = true;
                            eSC.Billable = true;
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;
                            eSC.SearchOutput = "";

                            rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                            rp.EnquiryID = eSC.SubscriberEnquiryID;
                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);


                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            ds.ReadXml(xmlSR);
                            if (ds.Tables.Contains("PropertyInformation"))
                            {
                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, intProductId);

                                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);
                                rXml = ds.GetXml();
                                rp.ResponseData = rXml;

                                foreach (DataRow r in ds.Tables["PropertyInformation"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    eSC.Billable = true;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;

                                    if (r.Table.Columns.Contains("PropertyType"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PropertyType"].ToString()))
                                        {
                                            eSC.Ha_FirstName = r["PropertyType"].ToString();

                                        }
                                    }

                                    if (r.Table.Columns.Contains("StandNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["StandNo"].ToString()))
                                        {
                                            eSC.Ha_SecondName = r["StandNo"].ToString();

                                        }
                                    }

                                    if (r.Table.Columns.Contains("TownshipName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["TownshipName"].ToString()))
                                        {
                                            eSC.Ha_CauseOfDeath = r["TownshipName"].ToString().Replace("'", "''");

                                        }
                                    }
                                    if (r.Table.Columns.Contains("Province"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Province"].ToString()))
                                        {
                                            eSC.Ha_DeceasedStatus = r["Province"].ToString().Replace("'", "''");

                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }


                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                    eSC.ExtraVarOutput1 = rp.TmpReference;

                                    rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    // Change the PayAsYouGoEnquiryLimit  after viewing the report

                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                                }
                            }
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("PropertyDetails"))
                            {
                                foreach (DataRow r in ds.Tables["PropertyDetails"].Rows)
                                {

                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("BuyerIDNO"))
                                    {
                                        if (!string.IsNullOrEmpty(r["BuyerIDNO"].ToString()))
                                        {
                                            eSC.FirstName = r["BuyerIDNO"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }


                                    if (r.Table.Columns.Contains("BuyerName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["BuyerName"].ToString()))
                                        {
                                            eSC.FirstName = r["BuyerName"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }



                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }
                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    // eSC.KeyID = int.Parse(r["PropertyDeedID"].ToString());
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;


                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {
                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");


                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);

                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;
                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;

                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);
                                            }



                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {

                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();

                                }
                            }

                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("PropertyDetails"))
                            {
                                foreach (DataRow r in ds.Tables["PropertyDetails"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("TitleDeedNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["TitleDeedNo"].ToString()))
                                        {
                                            eSC.IDNo = r["TitleDeedNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + r["TitleDeedNo"].ToString(); ;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("PropertyTypeDesc"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PropertyTypeDesc"].ToString()))
                                        {
                                            eSC.Ha_FirstName = r["PropertyTypeDesc"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Ha_FirstName;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("BuyerName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["BuyerName"].ToString()))
                                        {
                                            eSC.FirstName = r["BuyerName"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("SellerName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["SellerName"].ToString()))
                                        {
                                            eSC.SecondName = r["SellerName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.SecondName;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PortionNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PortionNo"].ToString()))
                                        {
                                            eSC.Surname = r["PortionNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("StandNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["StandNo"].ToString()))
                                        {
                                            eSC.BusRegistrationNo = r["StandNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusRegistrationNo;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.KeyID = int.Parse(r["PropertyDeedID"].ToString());
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {


                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");

                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;

                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;

                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);

                                            }

                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {

                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }


                        }

                    }

                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {

                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);

                MatchResult = eSe.SubscriberEnquiryID;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

    }
}
