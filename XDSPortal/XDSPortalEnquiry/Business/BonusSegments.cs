using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using XDSPortalLibrary;


namespace XDSPortalEnquiry.Business
{
    public class BonusSegments
    {

        private XDSPortalLibrary.Business_Layer.GetBonusSegments moBonusWseManager;
        private XDSPortalLibrary.Entity_Layer.GetBonusSegments eoBonusWseManager;

        public BonusSegments()
        {
            moBonusWseManager = new XDSPortalLibrary.Business_Layer.GetBonusSegments();
            eoBonusWseManager = new XDSPortalLibrary.Entity_Layer.GetBonusSegments();
        }


        public XDSPortalLibrary.Entity_Layer.Response GetBonusSegments(SqlConnection con, SqlConnection AdminConnection, int intSubscriberEnquiryResultID)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            string rXml = "";
            int MatchResult = 0;

            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();


            moBonusWseManager.ConnectionString = AdminConnection;
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            eSC = dSC.GetSubscriberEnquiryResultObject(con, intSubscriberEnquiryResultID);
            eSe = dSe.GetSubscriberEnquiryObject(con, eSC.SubscriberEnquiryID);
            DataSet  dsBonus = new DataSet();

            dsBonus = dSB.GetSubscriberEnquiryResultBonusDS(con,intSubscriberEnquiryResultID);


            try
            {
                if (dsBonus.Tables[0].Rows.Count == 0)
                {

                    eoBonusWseManager.SubscriberID = eSe.SubscriberID;
                    eoBonusWseManager.ProductID = eSC.ProductID;
                    eoBonusWseManager.KeyID = eSC.KeyID;
                    eoBonusWseManager.KeyType = eSC.KeyType;
                    eoBonusWseManager.ExternalReference = eSe.SubscriberReference;
                    eoBonusWseManager.DataSegments = eSe.ExtraVarInput2;

                    // Submit data for matching 

                    XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                    string strBonus = ra.GetTracePlusResult(eoBonusWseManager.KeyID.ToString(), eSe.ExtraVarInput2);

                    rp.ResponseData = strBonus;

                    // Get Response data

                    rXml = rp.ResponseData;

                    System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);


                    if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                    {

                        eSC.EnquiryResult = "E";
                        eSe.ErrorDescription = rp.ResponseData.ToString();
                        dSe.UpdateSubscriberEnquiryError(con, eSe);


                    }
                    else
                    {
                        ds.ReadXml(xmlSR);
                        rp.ResponseData = strBonus;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = intSubscriberEnquiryResultID;

                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count == 0)
                            {
                                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                            }
                            else
                            {
                                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Bonus;
                            }
                        }
                        else
                        {
                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                        }
                        

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Bonus)
                        {
                            if (ds.Tables.Contains("Segments"))
                            {
                                eSC.BonusIncluded = true;
                                eSC.XMLBonus = rp.ResponseData.Replace("'", "''");
                                eSC.ExtraVarOutput1 = rp.TmpReference;

                                foreach (DataRow dr in ds.Tables["Segments"].Rows)
                                {
                                    eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                    eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                    eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                    eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                    eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                    eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                    eSB.Billable = false;
                                    eSB.CreatedByUser = eSe.CreatedByUser;
                                    eSB.CreatedOnDate = DateTime.Now;

                                    // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                    dSB.InsertSubscriberEnquiryResultBonus(con, eSB);

                                }
                                dSC.UpdateMulipleSubscriberEnquiryResult(con, eSC);

                                rp.ResponseData = ds.GetXml();

                            }
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            rp.ResponseData = ds.GetXml();
                        }
                    }
                    

                    con.Close();
                    AdminConnection.Close();
                    SqlConnection.ClearPool(con);
                    SqlConnection.ClearPool(AdminConnection);
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Bonus;
                    rp.ResponseData = dsBonus.GetXml();
                    rp.EnquiryID = eSC.SubscriberEnquiryID;
                    rp.EnquiryResultID = intSubscriberEnquiryResultID;
                }

                }
           
            catch (Exception oException)
            {

                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);

                MatchResult = eSe.SubscriberEnquiryID;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            ds.Dispose();
            return rp;
        }



    }
}
