using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using XDSPortalLibrary;


namespace XDSPortalEnquiry.Business
{
    public class ConsumerTrace_ConsumerTrace
    {

        private XDSPortalLibrary.Business_Layer.ConsumerTrace moConsumerTraceWseManager;
        private XDSPortalLibrary.Entity_Layer.ConsumerTrace eoConsumerTraceWseManager;


        public ConsumerTrace_ConsumerTrace()
        {
            moConsumerTraceWseManager = new XDSPortalLibrary.Business_Layer.ConsumerTrace();
            eoConsumerTraceWseManager = new XDSPortalLibrary.Entity_Layer.ConsumerTrace();
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitConsumerTrace(SqlConnection con, SqlConnection AdminConnection, 
            int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string strIDNo, string strPassportNo, 
            string strSurname, string strMaidenName, string strFirstName, string strSecondName, string strFirstInitial,
            string strSecondInitial, DateTime dtBirthDate, Entity.SubscriberEnquiry.GenderInd rblGender, string strExtRef, 
            bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode, string SolutionTransactionID)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            string sGender = "";
            string rXml = "";
            int MatchResult = 0;
            int intSubscriberEnquiryResultID = 0;
            DataSet ds = new DataSet();
            Boolean boolSubmitEnquiry = true;
            string strValidationStatus = "";


            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);


            if (rblGender == Entity.SubscriberEnquiry.GenderInd.M)
            {
                sGender = "M";
            }
            if (rblGender == Entity.SubscriberEnquiry.GenderInd.F)
            {
                sGender = "F";
            }



            try
            {
                // Check if this subscriber is authorized to use the current product

                if (spr.ReportID > 0)
                {
                    // Check the PayAsYouGo settings for the subscriber
                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {

                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;

                        eSe.BirthDate = dtBirthDate;
                        eSe.FirstInitial = strFirstInitial;
                        eSe.FirstName = strFirstName;
                        eSe.Gender = rblGender.ToString();
                        eSe.IDNo = strIDNo;
                        eSe.MaidenName = strMaidenName;
                        eSe.PassportNo = strPassportNo;
                        eSe.ProductID = intProductId;
                        eSe.SecondInitial = strSecondInitial;
                        eSe.SecondName = strSecondName;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = strExtRef;
                        eSe.Surname = strSurname;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";

                        //Generate serach input string

                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.PassportNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.PassportNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.Surname))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.Surname;
                        }
                        if (!string.IsNullOrEmpty(eSe.FirstInitial))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.FirstName;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table

                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);

                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        //Submit to MDP
                        if (eSe.ProductID == 152)
                        {
                            MDPEnquiryLog mdpLog = new MDPEnquiryLog();
                            XDSPortalLibrary.Entity_Layer.MDPResponse mdpResp;

                            if (string.IsNullOrEmpty(SolutionTransactionID))
                            {
                                mdpResp = mdpLog.SubmitToMDP(eSe.ProductID.ToString(), ConfigurationManager.AppSettings["MDPKYCProductName"].ToString(), eSe.SubscriberID.ToString(), eSe.SubscriberName, intSubscriberEnquiryID.ToString());
                            }
                            else
                            {
                                mdpResp = mdpLog.SubmitToMDP(eSe.ProductID.ToString(), ConfigurationManager.AppSettings["MDPKYCProductName"].ToString(), eSe.SubscriberID.ToString(), eSe.SubscriberName, intSubscriberEnquiryID.ToString(), SolutionTransactionID);
                            }

                            if (mdpResp.response.ProductTransactionId != "")
                            {
                                XDSPortalEnquiry.Data.SubscriberEnquiry se = new Data.SubscriberEnquiry();
                                se.InsertSubscriberEnquiryMDP(con, intSubscriberEnquiryID, mdpResp.response.ProductTransactionId, mdpResp.response.Message, eSe.CreatedByUser, DateTime.Now);
                            }
                        }

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        //SqlConnection MatchingConstring = new SqlConnection(eSe.ExtraVarInput1.ToString());

                        //moConsumerTraceWseManager.ConnectionString = MatchingConstring;
                        eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoConsumerTraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoConsumerTraceWseManager.ConfirmationChkBox = bConfirmationChkBox;
                        eoConsumerTraceWseManager.DOB = dtBirthDate;
                        eoConsumerTraceWseManager.ExternalReference = strExtRef;
                        eoConsumerTraceWseManager.FirstInitial = strFirstInitial;
                        eoConsumerTraceWseManager.FirstName = strFirstName;
                        eoConsumerTraceWseManager.Gender = sGender;
                        eoConsumerTraceWseManager.IDno = strIDNo;
                        eoConsumerTraceWseManager.MaidenName = strMaidenName;
                        eoConsumerTraceWseManager.Passportno = strPassportNo;
                        eoConsumerTraceWseManager.ProductID = intProductId;
                        eoConsumerTraceWseManager.SecondInitial = strSecondInitial;
                        eoConsumerTraceWseManager.SecondName = strSecondName;
                        eoConsumerTraceWseManager.subscriberID = intSubscriberID;
                        eoConsumerTraceWseManager.Surname = strSurname;
                        eoConsumerTraceWseManager.BonusCheck = bBonusChecking;

                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.ConsumerTraceMatch(eoConsumerTraceWseManager);

                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.ConsumerTraceMatch(eoConsumerTraceWseManager);
                        }

                        //rp = moConsumerTraceWseManager.search(eoConsumerTraceWseManager);
                        rp.EnquiryID = intSubscriberEnquiryID;

                        // Get Response data

                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);


                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            eSC.SearchOutput = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.DetailsViewedYN = false;
                            eSC.Billable = false;
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;

                            rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                            rp.EnquiryID = eSC.SubscriberEnquiryID;
                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);


                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            ds.ReadXml(xmlSR);
                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, intProductId);

                                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);
                                rXml = ds.GetXml();
                                rp.ResponseData = rXml;

                                foreach (DataRow r in ds.Tables["ConsumerDetail"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    eSC.Billable = true;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                    eSC.ExtraVarOutput1 = rp.TmpReference;

                                    rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    // Change the PayAsYouGoEnquiryLimit  after viewing the report

                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                                }
                            }
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {

                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("SecondName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["SecondName"].ToString()))
                                        {
                                            eSC.SecondName = r["SecondName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.SecondName;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("BirthDate"))
                                    {
                                        if (!string.IsNullOrEmpty(r["BirthDate"].ToString()))
                                        {
                                            eSC.BirthDate = Convert.ToDateTime(r["BirthDate"]);
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + Convert.ToDateTime(r["BirthDate"]).ToShortDateString();
                                        }
                                    }
                                    if (r.Table.Columns.Contains("GenderInd"))
                                    {
                                        if (!string.IsNullOrEmpty(r["GenderInd"].ToString()))
                                        {
                                            eSC.Gender = r["GenderInd"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Gender;
                                        }
                                    }


                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }
                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;


                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {
                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");


                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);

                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;
                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;

                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);
                                            }



                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {

                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();

                                }
                            }

                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {


                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");

                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;

                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;

                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);

                                            }

                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {

                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }


                        }

                    }

                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }
            catch (Exception oException)
            {

                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);

                MatchResult = eSe.SubscriberEnquiryID;
                rp.EnquiryID = eSe.SubscriberEnquiryID;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (oException.Message.Contains("A .NET Framework"))
                {
                    rp.ResponseData = "Error: The System is unable to process this request. Please contact XDS Support";
                }

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitConsumerTrace(SqlConnection con, SqlConnection AdminConnection,
            int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string strIDNo, string strPassportNo,
            string strSurname, string strMaidenName, string strFirstName, string strSecondName, string strFirstInitial,
            string strSecondInitial, DateTime dtBirthDate, Entity.SubscriberEnquiry.GenderInd rblGender, string strExtRef,
            bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            string sGender = "";
            string rXml = "";
            int MatchResult = 0;
            int intSubscriberEnquiryResultID = 0;
            DataSet ds = new DataSet();
            Boolean boolSubmitEnquiry = true;
            string strValidationStatus = "";


            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);


            if (rblGender == Entity.SubscriberEnquiry.GenderInd.M)
            {
                sGender = "M";
            }
            if (rblGender == Entity.SubscriberEnquiry.GenderInd.F)
            {
                sGender = "F";
            }



            try
            {
                // Check if this subscriber is authorized to use the current product

                if (spr.ReportID > 0)
                {
                    // Check the PayAsYouGo settings for the subscriber
                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {

                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;

                        eSe.BirthDate = dtBirthDate;
                        eSe.FirstInitial = strFirstInitial;
                        eSe.FirstName = strFirstName;
                        eSe.Gender = rblGender.ToString();
                        eSe.IDNo = strIDNo;
                        eSe.MaidenName = strMaidenName;
                        eSe.PassportNo = strPassportNo;
                        eSe.ProductID = intProductId;
                        eSe.SecondInitial = strSecondInitial;
                        eSe.SecondName = strSecondName;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = strExtRef;
                        eSe.Surname = strSurname;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";

                        //Generate serach input string

                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.PassportNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.PassportNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.Surname))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.Surname;
                        }
                        if (!string.IsNullOrEmpty(eSe.FirstInitial))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.FirstName;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table

                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);

                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;
                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        //SqlConnection MatchingConstring = new SqlConnection(eSe.ExtraVarInput1.ToString());

                        //moConsumerTraceWseManager.ConnectionString = MatchingConstring;
                        eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoConsumerTraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoConsumerTraceWseManager.ConfirmationChkBox = bConfirmationChkBox;
                        eoConsumerTraceWseManager.DOB = dtBirthDate;
                        eoConsumerTraceWseManager.ExternalReference = strExtRef;
                        eoConsumerTraceWseManager.FirstInitial = strFirstInitial;
                        eoConsumerTraceWseManager.FirstName = strFirstName;
                        eoConsumerTraceWseManager.Gender = sGender;
                        eoConsumerTraceWseManager.IDno = strIDNo;
                        eoConsumerTraceWseManager.MaidenName = strMaidenName;
                        eoConsumerTraceWseManager.Passportno = strPassportNo;
                        eoConsumerTraceWseManager.ProductID = intProductId;
                        eoConsumerTraceWseManager.SecondInitial = strSecondInitial;
                        eoConsumerTraceWseManager.SecondName = strSecondName;
                        eoConsumerTraceWseManager.subscriberID = intSubscriberID;
                        eoConsumerTraceWseManager.Surname = strSurname;
                        eoConsumerTraceWseManager.BonusCheck = bBonusChecking;

                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.ConsumerTraceMatch(eoConsumerTraceWseManager);

                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.ConsumerTraceMatch(eoConsumerTraceWseManager);
                        }

                        //rp = moConsumerTraceWseManager.search(eoConsumerTraceWseManager);
                        rp.EnquiryID = intSubscriberEnquiryID;

                        // Get Response data

                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);


                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            eSC.SearchOutput = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.DetailsViewedYN = false;
                            eSC.Billable = false;
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;

                            rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                            rp.EnquiryID = eSC.SubscriberEnquiryID;
                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);


                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            ds.ReadXml(xmlSR);
                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, intProductId);

                                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);
                                rXml = ds.GetXml();
                                rp.ResponseData = rXml;

                                foreach (DataRow r in ds.Tables["ConsumerDetail"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    eSC.Billable = true;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                    eSC.ExtraVarOutput1 = rp.TmpReference;

                                    rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    // Change the PayAsYouGoEnquiryLimit  after viewing the report

                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                                }
                            }
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {

                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("SecondName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["SecondName"].ToString()))
                                        {
                                            eSC.SecondName = r["SecondName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.SecondName;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("BirthDate"))
                                    {
                                        if (!string.IsNullOrEmpty(r["BirthDate"].ToString()))
                                        {
                                            eSC.BirthDate = Convert.ToDateTime(r["BirthDate"]);
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + Convert.ToDateTime(r["BirthDate"]).ToShortDateString();
                                        }
                                    }
                                    if (r.Table.Columns.Contains("GenderInd"))
                                    {
                                        if (!string.IsNullOrEmpty(r["GenderInd"].ToString()))
                                        {
                                            eSC.Gender = r["GenderInd"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Gender;
                                        }
                                    }


                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }
                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;


                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {
                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");


                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);

                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;
                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;

                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);
                                            }



                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {

                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();

                                }
                            }

                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {


                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");

                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;

                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;

                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);

                                            }

                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {

                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }


                        }

                    }

                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }
            catch (Exception oException)
            {

                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);

                MatchResult = eSe.SubscriberEnquiryID;
                rp.EnquiryID = eSe.SubscriberEnquiryID;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (oException.Message.Contains("A .NET Framework"))
                {
                    rp.ResponseData = "Error: The System is unable to process this request. Please contact XDS Support";
                }

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitConsumerTraceLite(SqlConnection con, SqlConnection AdminConnection,
       int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string strIDNo, string strPassportNo,
       string strSurname, string strMaidenName, string strFirstName, string strSecondName, string strFirstInitial,
       string strSecondInitial, string TelephoneNumber, DateTime dtBirthDate, Entity.SubscriberEnquiry.GenderInd rblGender, string strExtRef,
       bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            string sGender = "";
            string rXml = "";
            int MatchResult = 0;
            int intSubscriberEnquiryResultID = 0;
            DataSet ds = new DataSet();
            Boolean boolSubmitEnquiry = true;
            string strValidationStatus = "";


            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);


            if (rblGender == Entity.SubscriberEnquiry.GenderInd.M)
            {
                sGender = "M";
            }
            if (rblGender == Entity.SubscriberEnquiry.GenderInd.F)
            {
                sGender = "F";
            }



            try
            {
                // Check if this subscriber is authorized to use the current product

                if (spr.ReportID > 0)
                {
                    // Check the PayAsYouGo settings for the subscriber
                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {

                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;

                        eSe.BirthDate = dtBirthDate;
                        eSe.FirstInitial = strFirstInitial;
                        eSe.FirstName = strFirstName;
                        eSe.Gender = rblGender.ToString();
                        eSe.IDNo = strIDNo;
                        eSe.MaidenName = strMaidenName;
                        eSe.PassportNo = strPassportNo;
                        eSe.ProductID = intProductId;
                        eSe.SecondInitial = strSecondInitial;
                        eSe.SecondName = strSecondName;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = strExtRef;
                        eSe.Surname = strSurname;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";

                        //Generate serach input string

                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.PassportNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.PassportNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.Surname))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.Surname;
                        }
                        if (!string.IsNullOrEmpty(eSe.FirstInitial))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.FirstName;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table

                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);

                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        //SqlConnection MatchingConstring = new SqlConnection(eSe.ExtraVarInput1.ToString());

                        //moConsumerTraceWseManager.ConnectionString = MatchingConstring;
                        eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoConsumerTraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoConsumerTraceWseManager.ConfirmationChkBox = bConfirmationChkBox;
                        eoConsumerTraceWseManager.DOB = dtBirthDate;
                        eoConsumerTraceWseManager.ExternalReference = strExtRef;
                        eoConsumerTraceWseManager.FirstInitial = strFirstInitial;
                        eoConsumerTraceWseManager.FirstName = strFirstName;
                        eoConsumerTraceWseManager.Gender = sGender;
                        eoConsumerTraceWseManager.IDno = strIDNo;
                        eoConsumerTraceWseManager.MaidenName = strMaidenName;
                        eoConsumerTraceWseManager.Passportno = strPassportNo;
                        eoConsumerTraceWseManager.ProductID = intProductId;
                        eoConsumerTraceWseManager.SecondInitial = strSecondInitial;
                        eoConsumerTraceWseManager.SecondName = strSecondName;
                        eoConsumerTraceWseManager.subscriberID = intSubscriberID;
                        eoConsumerTraceWseManager.Surname = strSurname;
                        eoConsumerTraceWseManager.BonusCheck = bBonusChecking;

                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.ConsumerTraceMatchTraceLite(eoConsumerTraceWseManager);

                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.ConsumerTraceMatchTraceLite(eoConsumerTraceWseManager);
                        }

                        //rp = moConsumerTraceWseManager.search(eoConsumerTraceWseManager);
                        rp.EnquiryID = intSubscriberEnquiryID;

                        // Get Response data

                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);


                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            eSC.SearchOutput = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.DetailsViewedYN = false;
                            eSC.Billable = false;
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;

                            rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                            rp.EnquiryID = eSC.SubscriberEnquiryID;
                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);


                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            ds.ReadXml(xmlSR);
                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, intProductId);

                                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);
                                rXml = ds.GetXml();
                                rp.ResponseData = rXml;

                                foreach (DataRow r in ds.Tables["ConsumerDetail"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    eSC.Billable = true;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                    eSC.ExtraVarOutput1 = rp.TmpReference;

                                    rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    // Change the PayAsYouGoEnquiryLimit  after viewing the report

                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                                }
                            }
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {

                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("SecondName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["SecondName"].ToString()))
                                        {
                                            eSC.SecondName = r["SecondName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.SecondName;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("BirthDate"))
                                    {
                                        if (!string.IsNullOrEmpty(r["BirthDate"].ToString()))
                                        {
                                            eSC.BirthDate = Convert.ToDateTime(r["BirthDate"]);
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + Convert.ToDateTime(r["BirthDate"]).ToShortDateString();
                                        }
                                    }
                                    if (r.Table.Columns.Contains("GenderInd"))
                                    {
                                        if (!string.IsNullOrEmpty(r["GenderInd"].ToString()))
                                        {
                                            eSC.Gender = r["GenderInd"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Gender;
                                        }
                                    }


                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }
                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;


                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {
                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");


                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);

                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;
                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;

                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);
                                            }



                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {

                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();

                                }
                            }

                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {


                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");

                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;

                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;

                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);

                                            }

                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {

                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }


                        }

                    }

                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }
            catch (Exception oException)
            {

                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);

                MatchResult = eSe.SubscriberEnquiryID;
                rp.EnquiryID = eSe.SubscriberEnquiryID;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (oException.Message.Contains("A .NET Framework"))
                {
                    rp.ResponseData = "Error: The System is unable to process this request. Please contact XDS Support";
                }

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            ds.Dispose();
            return rp;
        }



        public XDSPortalLibrary.Entity_Layer.Response SubmitSnapshotConsumerTrace(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string strIDNo, string strPassportNo, string strSurname, string strMaidenName, string strFirstName, string strSecondName, string strFirstInitial, string strSecondInitial, DateTime dtBirthDate, double dbMonthlySalary, string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            string sGender = "";
            string rXml = "";
            int MatchResult = 0;
            int intSubscriberEnquiryResultID = 0;
            DataSet ds = new DataSet();
            Boolean boolSubmitEnquiry = true;
            string strValidationStatus = "";


            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);


            //if (rblGender == Entity.SubscriberEnquiry.GenderInd.M)
            //{
            //    sGender = "M";
            //}
            //if (rblGender == Entity.SubscriberEnquiry.GenderInd.F)
            //{
            //    sGender = "F";
            //}



            try
            {
                // Check if this subscriber is authorized to use the current product

                if (spr.ReportID > 0)
                {
                    // Check the PayAsYouGo settings for the subscriber
                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {

                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;

                        eSe.BirthDate = dtBirthDate;
                        eSe.FirstInitial = strFirstInitial;
                        eSe.FirstName = strFirstName;
                        //eSe.Gender = rblGender.ToString();
                        eSe.IDNo = strIDNo;
                        eSe.MaidenName = strMaidenName;
                        eSe.PassportNo = strPassportNo;
                        eSe.ProductID = intProductId;
                        eSe.SecondInitial = strSecondInitial;
                        eSe.SecondName = strSecondName;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = strExtRef;
                        eSe.Surname = strSurname;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.NetMonthlyIncomeAmt = decimal.Parse(dbMonthlySalary.ToString());
                        eSe.SearchInput = "";

                        //Generate serach input string

                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.PassportNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.PassportNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.Surname))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.Surname;
                        }
                        if (!string.IsNullOrEmpty(eSe.FirstInitial))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.FirstName;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table

                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);

                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        //SqlConnection MatchingConstring = new SqlConnection(eSe.ExtraVarInput1.ToString());

                        //moConsumerTraceWseManager.ConnectionString = MatchingConstring;
                        eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoConsumerTraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoConsumerTraceWseManager.ConfirmationChkBox = bConfirmationChkBox;
                        eoConsumerTraceWseManager.DOB = dtBirthDate;
                        eoConsumerTraceWseManager.ExternalReference = strExtRef;
                        eoConsumerTraceWseManager.FirstInitial = strFirstInitial;
                        eoConsumerTraceWseManager.FirstName = strFirstName;
                        eoConsumerTraceWseManager.Gender = sGender;
                        eoConsumerTraceWseManager.IDno = strIDNo;
                        eoConsumerTraceWseManager.MaidenName = strMaidenName;
                        eoConsumerTraceWseManager.Passportno = strPassportNo;
                        eoConsumerTraceWseManager.ProductID = intProductId;
                        eoConsumerTraceWseManager.SecondInitial = strSecondInitial;
                        eoConsumerTraceWseManager.SecondName = strSecondName;
                        eoConsumerTraceWseManager.subscriberID = intSubscriberID;
                        eoConsumerTraceWseManager.Surname = strSurname;
                        eoConsumerTraceWseManager.BonusCheck = bBonusChecking;
                        eoConsumerTraceWseManager.UserName = sys.Username;
                        eoConsumerTraceWseManager.AssociationTypeCode = sub.AssociationTypeCode;
                        eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;

                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.ConsumerMicroLoanConsumerMatch(eoConsumerTraceWseManager);

                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.ConsumerMicroLoanConsumerMatch(eoConsumerTraceWseManager);
                        }

                        //rp = moConsumerTraceWseManager.search(eoConsumerTraceWseManager);
                        rp.EnquiryID = intSubscriberEnquiryID;

                        // Get Response data

                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);


                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            eSC.SearchOutput = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.DetailsViewedYN = false;
                            eSC.Billable = false;
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;

                            rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                            rp.EnquiryID = eSC.SubscriberEnquiryID;
                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);


                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            ds.ReadXml(xmlSR);
                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, intProductId);

                                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);
                                rXml = ds.GetXml();
                                rp.ResponseData = rXml;

                                foreach (DataRow r in ds.Tables["ConsumerDetail"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    eSC.Billable = true;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                    eSC.ExtraVarOutput1 = rp.TmpReference;

                                    rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    // Change the PayAsYouGoEnquiryLimit  after viewing the report

                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                                }
                            }
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                if (ds.Tables.Contains("ConsumerNLRScore"))
                                {
                                    foreach (DataRow dr in ds.Tables["ConsumerNLRScore"].Rows)
                                    {
                                        eSC.ExtraVarOutput1 = dr["FinalScore"].ToString();
                                        eSC.ExtraVarOutput2 = dr["ExceptionCode"].ToString();
                                        eSC.ExtraVarOutput3 = dr["RiskCategory"].ToString();
                                    }

                                }

                                ds.Tables.Remove("ConsumerNLRScore");

                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {

                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("SecondName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["SecondName"].ToString()))
                                        {
                                            eSC.SecondName = r["SecondName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.SecondName;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("BirthDate"))
                                    {
                                        if (!string.IsNullOrEmpty(r["BirthDate"].ToString()))
                                        {
                                            eSC.BirthDate = Convert.ToDateTime(r["BirthDate"]);
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + Convert.ToDateTime(r["BirthDate"]).ToShortDateString();
                                        }
                                    }
                                    if (r.Table.Columns.Contains("GenderInd"))
                                    {
                                        if (!string.IsNullOrEmpty(r["GenderInd"].ToString()))
                                        {
                                            eSC.Gender = r["GenderInd"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Gender;
                                        }
                                    }


                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }
                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                    //eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;


                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {
                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");


                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);

                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;
                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;

                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);
                                            }



                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {

                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();

                                }
                            }

                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {


                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");

                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;

                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;

                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);

                                            }

                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {

                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }


                        }

                    }

                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }
            catch (Exception oException)
            {

                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);

                MatchResult = eSe.SubscriberEnquiryID;
                rp.EnquiryID = eSe.SubscriberEnquiryID;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (oException.Message.Contains("A .NET Framework"))
                {
                    rp.ResponseData = "Error: The System is unable to process this request. Please contact XDS Support";
                }

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitConsumerTraceCompliance(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string strIDNo, string strPassportNo, string strSurname, string strMaidenName, string strFirstName, string strSecondName, string strFirstInitial, string strSecondInitial, DateTime dtBirthDate, Entity.SubscriberEnquiry.GenderInd rblGender, string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode, string strDHAURL, string strClientID, string strUserID, string strPassword, string strURL)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            string sGender = "";
            string rXml = "";
            string rXmlMatch = "";
            int MatchResult = 0;
            int intSubscriberEnquiryResultID = 0;
            DataSet ds = new DataSet();
            Boolean boolSubmitEnquiry = true;
            string strValidationStatus = "";
            double Totalcost = 0;

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();


            if (rblGender == Entity.SubscriberEnquiry.GenderInd.M)
            {
                sGender = "M";
            }
            if (rblGender == Entity.SubscriberEnquiry.GenderInd.F)
            {
                sGender = "F";
            }

            try
            {
                // Check if this subscriber is authorized to use the current product

                if (spr.ReportID > 0)
                {
                    // Check the PayAsYouGo settings for the subscriber
                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1)
                    {
                        //Calculate The Total cost of the Report , including Bonus Segments

                        Totalcost = spr.UnitPrice;

                        if (eoConsumerTraceWseManager.BonusSegments != null)
                        {

                            foreach (DataRow dr in eoConsumerTraceWseManager.BonusSegments.Tables[0].Rows)
                            {
                                if (dr["BonusViewed"].ToString().ToLower() == "true")
                                {
                                    Totalcost = Totalcost + Convert.ToInt16(dr["BonusPrice"].ToString());
                                }
                            }
                        }
                    }

                    if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {

                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;

                        eSe.BirthDate = dtBirthDate;
                        eSe.FirstInitial = strFirstInitial;
                        eSe.FirstName = strFirstName;
                        eSe.Gender = rblGender.ToString();
                        eSe.IDNo = strIDNo;
                        eSe.MaidenName = strMaidenName;
                        eSe.PassportNo = strPassportNo;
                        eSe.ProductID = intProductId;
                        eSe.SecondInitial = strSecondInitial;
                        eSe.SecondName = strSecondName;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = strExtRef;
                        eSe.Surname = strSurname;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";

                        //Generate serach input string

                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.PassportNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.PassportNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.Surname))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.Surname;
                        }
                        if (!string.IsNullOrEmpty(eSe.FirstInitial))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.FirstName;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table

                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);

                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        //SqlConnection MatchingConstring = new SqlConnection(eSe.ExtraVarInput1.ToString());

                        //moConsumerTraceWseManager.ConnectionString = MatchingConstring;
                        eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoConsumerTraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoConsumerTraceWseManager.ConfirmationChkBox = bConfirmationChkBox;
                        eoConsumerTraceWseManager.DOB = dtBirthDate;
                        eoConsumerTraceWseManager.ExternalReference = strExtRef;
                        eoConsumerTraceWseManager.FirstInitial = strFirstInitial;
                        eoConsumerTraceWseManager.FirstName = strFirstName;
                        eoConsumerTraceWseManager.Gender = sGender;
                        eoConsumerTraceWseManager.IDno = strIDNo;
                        eoConsumerTraceWseManager.MaidenName = strMaidenName;
                        eoConsumerTraceWseManager.Passportno = strPassportNo;
                        eoConsumerTraceWseManager.ProductID = intProductId;
                        eoConsumerTraceWseManager.SecondInitial = strSecondInitial;
                        eoConsumerTraceWseManager.SecondName = strSecondName;
                        eoConsumerTraceWseManager.subscriberID = intSubscriberID;
                        eoConsumerTraceWseManager.Surname = strSurname;
                        eoConsumerTraceWseManager.BonusCheck = bBonusChecking;

                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.ConsumerTraceMatchComplianceExposure(eoConsumerTraceWseManager);

                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.ConsumerTraceMatchComplianceExposure(eoConsumerTraceWseManager);
                        }


                        //rp = moConsumerTraceWseManager.search(eoConsumerTraceWseManager);
                        rp.EnquiryID = intSubscriberEnquiryID;

                        // Get Response data

                        rXmlMatch = rp.ResponseData;
                        System.IO.StringReader xmlSRMatch = new System.IO.StringReader(rXmlMatch);

                        bool ConsumerExists = true;

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None ||
                            rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report ||
                            rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple ||
                            rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                        {
                            ds.ReadXml(xmlSRMatch);

                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                if (ds.Tables["ConsumerDetails"].Rows.Count > 1)
                                {
                                    for (int i = 0; i < ds.Tables["ConsumerDetails"].Rows.Count; i++)
                                    {
                                        if (i > 0)
                                        {
                                            ds.Tables["ConsumerDetails"].Rows[i].Delete();
                                        }
                                    }
                                }

                                ds.Tables[0].Columns.Add("EnquiryID");
                                ds.Tables[0].Columns.Add("EnquiryResultID");
                                ds.Tables[0].Columns.Add("Reference");

                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {

                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("SecondName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["SecondName"].ToString()))
                                        {
                                            eSC.SecondName = r["SecondName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.SecondName;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("BirthDate"))
                                    {
                                        if (!string.IsNullOrEmpty(r["BirthDate"].ToString()))
                                        {
                                            eSC.BirthDate = Convert.ToDateTime(r["BirthDate"]);
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + Convert.ToDateTime(r["BirthDate"]).ToShortDateString();
                                        }
                                    }
                                    if (r.Table.Columns.Contains("GenderInd"))
                                    {
                                        if (!string.IsNullOrEmpty(r["GenderInd"].ToString()))
                                        {
                                            eSC.Gender = r["GenderInd"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Gender;
                                        }
                                    }


                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }
                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {
                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");


                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);

                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;
                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;

                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);
                                            }



                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {
                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    }

                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }
                            else
                            {
                                eSC.SearchOutput = "";
                                eSC.IDNo = "";
                                eSC.PassportNo = "";
                                eSC.Surname = "";
                                eSC.FirstName = "";
                                eSC.BirthDate = DateTime.Parse("1900/01/01");
                                eSC.Gender = "";

                                if (!string.IsNullOrEmpty(strIDNo))
                                {
                                    eSC.IDNo = strIDNo;
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                }

                                if (!string.IsNullOrEmpty(strPassportNo))
                                {
                                    eSC.PassportNo = strPassportNo.Replace("'", "''");
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                }

                                if (!string.IsNullOrEmpty(strSurname))
                                {
                                    eSC.Surname = strSurname.Replace("'", "''");
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                }

                                if (!string.IsNullOrEmpty(strFirstName))
                                {
                                    eSC.FirstName = strFirstName.Replace("'", "''");
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                }

                                if (!string.IsNullOrEmpty(strSecondName))
                                {
                                    eSC.SecondName = strSecondName.Replace("'", "''");
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.SecondName;
                                }

                                if (!string.IsNullOrEmpty(dtBirthDate.ToString()))
                                {
                                    eSC.BirthDate = dtBirthDate;
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + Convert.ToDateTime(dtBirthDate.ToShortDateString());
                                }

                                if (!string.IsNullOrEmpty(sGender))
                                {
                                    eSC.Gender = sGender.Replace("'", "''");
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Gender;
                                }

                                if (eSC.SearchOutput.Length > 0)
                                {
                                    eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                }
                                eSC.DetailsViewedYN = false;
                                eSC.Billable = false;
                                eSC.CreatedByUser = eSe.CreatedByUser;
                                eSC.CreatedOnDate = DateTime.Now;
                                eSC.KeyID = 0;
                                eSC.KeyType = rp.ResponseKeyType;

                                eSC.BillingTypeID = spr.BillingTypeID;
                                eSC.BillingPrice = spr.UnitPrice;

                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                eSC.ExtraVarOutput1 = "";
                                eSC.ProductID = intProductId;
                                eSC.VoucherCode = strVoucherCode;

                                string BonusXML = string.Empty;

                                intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);

                                //Build list 
                                ds = new DataSet("ListOfConsumers");

                                DataTable dtSubscriberInput = new DataTable("ConsumerDetails");
                                dtSubscriberInput.Columns.Add("ConsumerID", typeof(Int32));
                                dtSubscriberInput.Columns.Add("FirstName", typeof(String));
                                dtSubscriberInput.Columns.Add("SecondName", typeof(String));
                                dtSubscriberInput.Columns.Add("ThirdName", typeof(String));
                                dtSubscriberInput.Columns.Add("Surname", typeof(String));
                                dtSubscriberInput.Columns.Add("IDNo", typeof(String));
                                dtSubscriberInput.Columns.Add("PassportNo", typeof(String));
                                dtSubscriberInput.Columns.Add("BirthDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("GenderInd", typeof(String));
                                dtSubscriberInput.Columns.Add("BonusXML", typeof(String));
                                dtSubscriberInput.Columns.Add("TempReference", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryID", typeof(Int32));
                                dtSubscriberInput.Columns.Add("EnquiryResultID", typeof(Int32));
                                dtSubscriberInput.Columns.Add("Reference", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["ConsumerID"] = 0;
                                drSubscriberInput["FirstName"] = strFirstName;
                                drSubscriberInput["SecondName"] = "";
                                drSubscriberInput["ThirdName"] = "";
                                drSubscriberInput["Surname"] = strSurname;
                                drSubscriberInput["IDNo"] = strIDNo;
                                drSubscriberInput["PassportNo"] = strPassportNo;
                                drSubscriberInput["BirthDate"] = dtBirthDate;
                                drSubscriberInput["GenderInd"] = sGender;
                                drSubscriberInput["BonusXML"] = "";
                                drSubscriberInput["TempReference"] = "";
                                drSubscriberInput["EnquiryID"] = intSubscriberEnquiryID;
                                drSubscriberInput["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                drSubscriberInput["Reference"] = strExtRef;

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);

                                rp.ResponseData = ds.GetXml();
                                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                                ConsumerExists = false;
                            }

                            //string realTime = ConnectGetRealTimeIDVerification(clientID, ConnectTicket, "153", oSubscriberEnquiry.IDNo, "", "", oSubscriberEnquiry.SubscriberReference, "");
                            //System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();
                            //XDSPortalLibrary.Entity_Layer.Response rp1 = new XDSPortalLibrary.Entity_Layer.Response();

                            //XDSPortalEnquiry.Business.ConsumerTrace_IdentityVerification dtConsumertrace = new XDSPortalEnquiry.Business.ConsumerTrace_IdentityVerification();
                            //rp1 = dtConsumertrace.SubmitRealTimeIdentityVerification(con, AdminConnection, sub.SubscriberID, intSystemUserID, 153, sub.SubscriberName, strIDNo, strSurname, strFirstName, "", dtBirthDate, strExtRef, true, true, strVoucherCode, strDHAURL);

                            //switch (rp1.ResponseStatus)
                            //{
                            //    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single:
                            //    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple:
                            //    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                            //        rsXml.LoadXml(rp1.ResponseData);
                            //        break;
                            //    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                            //        rXml = "<NoResult><Error>" + rp.ResponseData + "</Error></NoResult>";
                            //        rsXml.LoadXml(rXml);
                            //        break;
                            //    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                            //        rXml = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                            //        rsXml.LoadXml(rXml);
                            //        break;
                            //}

                            //string realTime = rsXml.OuterXml;

                            //Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                            //Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                            //DataSet dsDataSegments = null;
                            //XDSPortalLibrary.Entity_Layer.Response rp2 = new XDSPortalLibrary.Entity_Layer.Response();
                            //XDSPortalEnquiry.Business.LexisNexisData lexisNexisData = new XDSPortalEnquiry.Business.LexisNexisData();

                            //rp2 = lexisNexisData.RenderLexisNexisData(con, AdminConnection, intSubscriberEnquiryID, intSubscriberEnquiryResultID, dsDataSegments, realTime, strClientID, strUserID, strPassword, strURL, 176, strFirstName, strSurname);

                            //////rXml = rp2.ResponseData;

                            //////// Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                            //////if (sub.PayAsYouGo == 1)
                            //////{
                            //////    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                            //////}
                            //////if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                            //////{
                            //////    dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                            //////}

                            //////if (String.IsNullOrEmpty(eSC.VoucherCode))
                            //////{
                            //////    //Log FootPrint

                            //////    eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                            //////    eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                            //////    eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                            //////    eSubscriberFootPrint.KeyID = eSC.KeyID;
                            //////    eSubscriberFootPrint.KeyType = eSC.KeyType;
                            //////    eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                            //////    eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                            //////    if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                            //////    {
                            //////        eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                            //////    }
                            //////    eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                            //////    eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                            //////    eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                            //////    eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                            //////    eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                            //////    eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                            //////    eSubscriberFootPrint.CreatedByUser = sys.Username;
                            //////    eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                            //////    eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                            //////    eSubscriberFootPrint.ProductID = eSe.ProductID;
                            //////    eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                            //////    int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                            //////}

                            //Check if data returned by Bureau and LN, if not, return nothing
                            //if (ConsumerExists == false && rp2.ResponseData.Contains("SupplierData") == false)
                            //{
                            //    rp.ResponseData = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                            //}
                            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();
                            string realTime = string.Empty;
                            if (!string.IsNullOrEmpty(strIDNo))
                            {
                                XDSPortalLibrary.Entity_Layer.Response rp1 = new XDSPortalLibrary.Entity_Layer.Response();

                                //  XDSPortalEnquiry.Business.ConsumerTrace_IdentityVerification dtConsumertrace = new XDSPortalEnquiry.Business.ConsumerTrace_IdentityVerification();
                                XDSPortalEnquiry.Business.ConsumerTrace_IdentityVerification dtConsumertrace = new XDSPortalEnquiry.Business.ConsumerTrace_IdentityVerification();
                                //   rp1 = dtConsumertrace.SubmitRealTimeIdentityVerification(con, AdminConnection, sub.SubscriberID, intSystemUserID, 153, sub.SubscriberName, strIDNo, strSurname, strFirstName, "", dtBirthDate, strExtRef, true, true, strVoucherCode, strDHAURL);
                                //   rp1 = dtConsumertrace.SubmitRealTimeIdentityVerification(con, AdminConnection, sub.SubscriberID, intSystemUserID, 153, sub.SubscriberName, strIDNo, strSurname, strFirstName, "", dtBirthDate, strExtRef, true, true, strVoucherCode, strDHAURL, strClientIPAddress, "ConnectGetRealTimeIDVerification");
                                rp1= dtConsumertrace.SubmitRealTimeIdentityVerification(con, AdminConnection, sub.SubscriberID, intSystemUserID, 153, sub.SubscriberName, strIDNo, strSurname, strFirstName, "", dtBirthDate, strExtRef, true, true, strVoucherCode, strDHAURL);
                                switch (rp1.ResponseStatus)
                                {
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single:
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple:
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                                        rsXml.LoadXml(rp1.ResponseData);
                                        break;
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                                        rXml = "<NoResult><Error>" + rp1.ResponseData + "</Error></NoResult>";
                                        rsXml.LoadXml(rXml);
                                        break;
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                                        rXml = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                                        rsXml.LoadXml(rXml);
                                        break;
                                }

                                realTime = rsXml.OuterXml;
                                System.IO.StringReader xmlSRTIDV = new System.IO.StringReader(realTime);
                                DataSet ds2 = new DataSet();
                                ds2.ReadXml(xmlSRTIDV);

                                if (ds2.Tables.Contains("RealTimeIDV"))
                                {
                                    if (ds2.Tables["RealTimeIDV"].Rows.Count > 1)
                                    {
                                        for (int i = 0; i < ds2.Tables["RealTimeIDV"].Rows.Count; i++)
                                        {
                                            if (i > 0)
                                            {
                                                ds2.Tables["RealTimeIDV"].Rows[i].Delete();
                                            }
                                        }
                                    }
                                }
                                string IDFirstname = string.Empty;
                                string IDSurname = string.Empty;

                                foreach (DataRow r in ds2.Tables["RealTimeIDV"].Rows)
                                {



                                    if (r.Table.Columns.Contains("HASurname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["HASurname"].ToString()))
                                        {
                                            IDSurname = r["HASurname"].ToString();
                                            //  eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("HANames"))
                                    {
                                        if (!string.IsNullOrEmpty(r["HANames"].ToString()))
                                        {
                                            IDFirstname = r["HANames"].ToString().Replace("'", "''");
                                            //  eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }
                                }


                                //MultipleTrace_IdentityVerification Mt = new MultipleTrace_IdentityVerification();
                                //DataSet ds1 = new DataSet();
                                //XDSPortalLibrary.Entity_Layer.Response rp_Buerau = new XDSPortalLibrary.Entity_Layer.Response();
                                //rp_Buerau = Mt.GetBuerauDetails(con, AdminConnection, eSC.SubscriberEnquiryID, eSC.SubscriberEnquiryResultID, ds1, false, "");
                                //if (rp_Buerau.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                                //{
                                //    System.Xml.XmlDocument rsXmlB = new System.Xml.XmlDocument();
                                //    if (!string.IsNullOrEmpty(rp_Buerau.ResponseData))
                                //    {
                                //        rsXmlB.LoadXml(rp_Buerau.ResponseData);
                                //        string ConsumerDetails = rsXmlB.OuterXml;
                                //        System.IO.StringReader xmlSRTIDVB = new System.IO.StringReader(ConsumerDetails);
                                //        DataSet consumerds = new DataSet();
                                //        consumerds.ReadXml(xmlSRTIDVB);
                                //        if (consumerds.Tables.Contains("ConsumerDetail"))
                                //        {
                                //            // ds2.Tables.Add(consumerds.Tables["ConsumerDetail"].ToString().Copy());
                                //            ds2.Tables.Add(consumerds.Tables["ConsumerDetail"].Copy());

                                //        }
                                //    }
                                //    string XML = ds2.GetXml();
                                //    rsXml.LoadXml(XML);
                                //    realTime = rsXml.OuterXml;
                                //}
                                if (string.IsNullOrEmpty(realTime) || realTime.Contains("NoResult"))
                                {
                                    DataSet ds3 = new DataSet("HomeAffairs");
                                    string XML = ds3.GetXml();
                                    rsXml.LoadXml(XML);
                                    realTime = rsXml.OuterXml;
                                }
                                Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                DataSet dsDataSegments = null;
                                XDSPortalLibrary.Entity_Layer.Response rp2 = new XDSPortalLibrary.Entity_Layer.Response();
                                XDSPortalEnquiry.Business.LexisNexisData lexisNexisData = new XDSPortalEnquiry.Business.LexisNexisData();

                                rp2 = lexisNexisData.RenderLexisNexisData(con, AdminConnection, intSubscriberEnquiryID, intSubscriberEnquiryResultID, dsDataSegments, realTime, strClientID, strUserID, strPassword, strURL, 176, IDFirstname, IDSurname, Convert.ToDateTime("1900-01-01"), "", "", "", "", "", "", "", "", "");
                                if (ConsumerExists == false && rp2.ResponseData.Contains("SupplierData") == false)
                                {
                                    rp.ResponseData = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                                }
                            }
                            else if (!string.IsNullOrEmpty(strPassportNo))
                            {
                                DataSet ds3 = new DataSet("HomeAffairs");
                                DataSet dsempty = new DataSet();
                                //MultipleTrace_IdentityVerification Mt = new MultipleTrace_IdentityVerification();

                                //XDSPortalLibrary.Entity_Layer.Response rp_Buerau = new XDSPortalLibrary.Entity_Layer.Response();
                                //rp_Buerau = Mt.GetBuerauDetails(con, AdminConnection, eSC.SubscriberEnquiryID, eSC.SubscriberEnquiryResultID, dsempty, false, "");
                                //if (rp_Buerau.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                                //{
                                //    System.Xml.XmlDocument rsXmlB = new System.Xml.XmlDocument();
                                //    if (!string.IsNullOrEmpty(rp_Buerau.ResponseData))
                                //    {
                                //        rsXmlB.LoadXml(rp_Buerau.ResponseData);
                                //        string ConsumerDetails = rsXmlB.OuterXml;
                                //        System.IO.StringReader xmlSRTIDVB = new System.IO.StringReader(ConsumerDetails);
                                //        DataSet consumerds = new DataSet();
                                //        consumerds.ReadXml(xmlSRTIDVB);
                                //        if (consumerds.Tables.Contains("ConsumerDetail"))
                                //        {
                                //            // ds2.Tables.Add(consumerds.Tables["ConsumerDetail"].ToString().Copy());
                                //            ds3.Tables.Add(consumerds.Tables["ConsumerDetail"].Copy());
                                //            string XML = ds3.GetXml();
                                //            rsXml.LoadXml(XML);
                                //            realTime = rsXml.OuterXml;
                                //        }

                                //    }


                                //}
                                if(string.IsNullOrEmpty(realTime))
                                {
                                    string XML = ds3.GetXml();
                                    rsXml.LoadXml(XML);
                                    realTime = rsXml.OuterXml;
                                }
                               
                                Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                DataSet dsDataSegments = null;
                                XDSPortalLibrary.Entity_Layer.Response rp2 = new XDSPortalLibrary.Entity_Layer.Response();
                                XDSPortalEnquiry.Business.LexisNexisData lexisNexisData = new XDSPortalEnquiry.Business.LexisNexisData();

                                rp2 = lexisNexisData.RenderLexisNexisData(con, AdminConnection, intSubscriberEnquiryID, intSubscriberEnquiryResultID, dsDataSegments, realTime, strClientID, strUserID, strPassword, strURL, 176, strFirstName, strSurname, Convert.ToDateTime("1900-01-01"), "", "", "", "", "", "", "", "", "");
                                if (ConsumerExists == false && rp2.ResponseData.Contains("SupplierData") == false)
                                {
                                    rp.ResponseData = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                                }
                            }
                            else
                            {
                                DataSet dsDataSegments = null;
                                XDSPortalLibrary.Entity_Layer.Response rp2 = new XDSPortalLibrary.Entity_Layer.Response();
                                XDSPortalEnquiry.Business.LexisNexisData lexisNexisData = new XDSPortalEnquiry.Business.LexisNexisData();

                                rp2 = lexisNexisData.RenderLexisNexisData(con, AdminConnection, intSubscriberEnquiryID, intSubscriberEnquiryResultID, dsDataSegments, realTime, strClientID, strUserID, strPassword, strURL, 176, strFirstName, strSurname, Convert.ToDateTime("1900-01-01"), "", "", "", "", "", "", "", "", "");
                                if (ConsumerExists == false && rp2.ResponseData.Contains("SupplierData") == false)
                                {
                                    rp.ResponseData = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                                }
                            }

                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);


                        }

                    }

                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }
            catch (Exception oException)
            {

                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);

                MatchResult = eSe.SubscriberEnquiryID;
                rp.EnquiryID = eSe.SubscriberEnquiryID;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (oException.Message.Contains("A .NET Framework"))
                {
                    rp.ResponseData = "Error: The System is unable to process this request. Please contact XDS Support";
                }

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitConsumerTraceCompliance(SqlConnection con, SqlConnection AdminConnection, 
            int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string strIDNo, string strPassportNo, 
            string strSurname, string strMaidenName, string strFirstName, string strSecondName, string strFirstInitial, 
            string strSecondInitial, string strBirthDate, string strGender, string strAddressLine1, string strAddressLine2, 
            string strCity, string strProvince, string strPostalCode, string strCountry, string strCitizenship, string strCellNumber, 
            string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode, string strDHAURL, string strClientID, 
            string strUserID, string strPassword, string strURL)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            string sGender = "";
            string rXml = "";
            string rXmlMatch = "";
            int MatchResult = 0;
            int intSubscriberEnquiryResultID = 0;
            DataSet ds = new DataSet();
            Boolean boolSubmitEnquiry = true;
            string strValidationStatus = "";
            double Totalcost = 0;

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);

            DateTime dtBirthDate = Convert.ToDateTime("1900-01-01");

            if (string.IsNullOrEmpty(strFirstName))
            {
                throw new Exception("Your search input must have the firstname");
            }
            if (string.IsNullOrEmpty(strSurname))
            {
                throw new Exception("Your search input must have the surname");
            }
            if (!string.IsNullOrEmpty(strGender))
            {
                System.Collections.Generic.List<string> possibleGender = new System.Collections.Generic.List<string>();

                // Adding elements to List
                possibleGender.Add("M");
                possibleGender.Add("F");
                possibleGender.Add("MALE");
                possibleGender.Add("FEMALE");

                if (!possibleGender.Contains(strGender.ToUpper()))
                    throw new Exception("Invalid gender");

                if (strGender.ToUpper() == "M")
                    strGender = "Male";

                if (strGender.ToUpper() == "F")
                    strGender = "Female";
            }
            if (!string.IsNullOrEmpty(strCellNumber))
            {
                if (strCellNumber.Length == 10 && strCellNumber.StartsWith("0"))
                    strCellNumber = "+27" + strCellNumber.Substring(1, 9);
            }
            if (!string.IsNullOrEmpty(strBirthDate))
            {
                if (!DateTime.TryParse(strBirthDate, out dtBirthDate))
                    throw new Exception("Invalid birthdate");
            }
            if (!string.IsNullOrEmpty(strIDNo) && string.IsNullOrEmpty(strGender))
            {
                if (FnValidateIDNo(strIDNo))
                {
                    string IDSubstring = strIDNo.Substring(6, 4);
                    int iGender = int.Parse(IDSubstring);

                    if (iGender < 5000)
                        strGender = "Female";
                    else
                        strGender = "Male";
                }
            }

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            try
            {
                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {
                    // Check the PayAsYouGo settings for the subscriber
                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1)
                    {
                        //Calculate The Total cost of the Report , including Bonus Segments

                        Totalcost = spr.UnitPrice;

                        if (eoConsumerTraceWseManager.BonusSegments != null)
                        {

                            foreach (DataRow dr in eoConsumerTraceWseManager.BonusSegments.Tables[0].Rows)
                            {
                                if (dr["BonusViewed"].ToString().ToLower() == "true")
                                {
                                    Totalcost = Totalcost + Convert.ToInt16(dr["BonusPrice"].ToString());
                                }
                            }
                        }
                    }

                    if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {
                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;

                        eSe.BirthDate = dtBirthDate;
                        eSe.FirstInitial = strFirstInitial;
                        eSe.FirstName = strFirstName;
                        eSe.Gender = strGender;
                        eSe.IDNo = strIDNo;
                        eSe.MaidenName = strMaidenName;
                        eSe.PassportNo = strPassportNo;
                        eSe.ProductID = intProductId;
                        eSe.SecondInitial = strSecondInitial;
                        eSe.SecondName = strSecondName;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = strExtRef;
                        eSe.Surname = strSurname;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";

                        //Generate serach input string

                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.PassportNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.PassportNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.Surname))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.Surname;
                        }
                        if (!string.IsNullOrEmpty(eSe.FirstName))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.FirstName;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table

                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);
                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;
                        dSe.InsertSubscriberConsumerComplianceEnquiry(con, intSubscriberEnquiryID.ToString(), sys.SystemUserFullName, strIDNo, 
                            strPassportNo, strFirstName, strSurname, dtBirthDate, strGender, strAddressLine1, strAddressLine2, strCity, 
                            strProvince, strPostalCode, strCountry, strCitizenship, strCellNumber);
                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        //SqlConnection MatchingConstring = new SqlConnection(eSe.ExtraVarInput1.ToString());

                        //moConsumerTraceWseManager.ConnectionString = MatchingConstring;
                        eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoConsumerTraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoConsumerTraceWseManager.ConfirmationChkBox = bConfirmationChkBox;
                        eoConsumerTraceWseManager.DOB = dtBirthDate;
                        eoConsumerTraceWseManager.ExternalReference = strExtRef;
                        eoConsumerTraceWseManager.FirstInitial = strFirstInitial;
                        eoConsumerTraceWseManager.FirstName = strFirstName;
                        eoConsumerTraceWseManager.Gender = sGender;
                        eoConsumerTraceWseManager.IDno = strIDNo;
                        eoConsumerTraceWseManager.MaidenName = strMaidenName;
                        eoConsumerTraceWseManager.Passportno = strPassportNo;
                        eoConsumerTraceWseManager.ProductID = intProductId;
                        eoConsumerTraceWseManager.SecondInitial = strSecondInitial;
                        eoConsumerTraceWseManager.SecondName = strSecondName;
                        eoConsumerTraceWseManager.subscriberID = intSubscriberID;
                        eoConsumerTraceWseManager.Surname = strSurname;
                        eoConsumerTraceWseManager.BonusCheck = bBonusChecking;

                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.ConsumerTraceMatchComplianceExposure(eoConsumerTraceWseManager);

                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.ConsumerTraceMatchComplianceExposure(eoConsumerTraceWseManager);
                        }

                        //rp = moConsumerTraceWseManager.search(eoConsumerTraceWseManager);
                        rp.EnquiryID = intSubscriberEnquiryID;

                        // Get Response data
                        rXmlMatch = rp.ResponseData;
                        System.IO.StringReader xmlSRMatch = new System.IO.StringReader(rXmlMatch);

                        bool ConsumerExists = true;

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None ||
                            rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report ||
                            rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple ||
                            rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                        {
                            ds.ReadXml(xmlSRMatch);

                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                if (ds.Tables["ConsumerDetails"].Rows.Count > 1)
                                {
                                    for (int i = 0; i < ds.Tables["ConsumerDetails"].Rows.Count; i++)
                                    {
                                        if (i > 0)
                                        {
                                            ds.Tables["ConsumerDetails"].Rows[i].Delete();
                                        }
                                    }
                                }

                                ds.Tables[0].Columns.Add("EnquiryID");
                                ds.Tables[0].Columns.Add("EnquiryResultID");
                                ds.Tables[0].Columns.Add("Reference");

                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("SecondName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["SecondName"].ToString()))
                                        {
                                            eSC.SecondName = r["SecondName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.SecondName;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("BirthDate"))
                                    {
                                        if (!string.IsNullOrEmpty(r["BirthDate"].ToString()))
                                        {
                                            eSC.BirthDate = Convert.ToDateTime(r["BirthDate"]);
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + Convert.ToDateTime(r["BirthDate"]).ToShortDateString();
                                        }
                                    }
                                    if (r.Table.Columns.Contains("GenderInd"))
                                    {
                                        if (!string.IsNullOrEmpty(r["GenderInd"].ToString()))
                                        {
                                            eSC.Gender = r["GenderInd"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Gender;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }
                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {
                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");


                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);

                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;
                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;

                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);
                                            }
                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {
                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    }

                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }
                            else
                            {
                                eSC.SearchOutput = "";
                                eSC.IDNo = "";
                                eSC.PassportNo = "";
                                eSC.Surname = "";
                                eSC.FirstName = "";
                                eSC.BirthDate = DateTime.Parse("1900/01/01");
                                eSC.Gender = "";

                                if (!string.IsNullOrEmpty(strIDNo))
                                {
                                    eSC.IDNo = strIDNo;
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                }

                                if (!string.IsNullOrEmpty(strPassportNo))
                                {
                                    eSC.PassportNo = strPassportNo.Replace("'", "''");
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                }

                                if (!string.IsNullOrEmpty(strSurname))
                                {
                                    eSC.Surname = strSurname.Replace("'", "''");
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                }

                                if (!string.IsNullOrEmpty(strFirstName))
                                {
                                    eSC.FirstName = strFirstName.Replace("'", "''");
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                }

                                if (!string.IsNullOrEmpty(strSecondName))
                                {
                                    eSC.SecondName = strSecondName.Replace("'", "''");
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.SecondName;
                                }

                                if (!string.IsNullOrEmpty(dtBirthDate.ToString()))
                                {
                                    eSC.BirthDate = dtBirthDate;
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + Convert.ToDateTime(dtBirthDate.ToShortDateString());
                                }

                                if (!string.IsNullOrEmpty(sGender))
                                {
                                    eSC.Gender = sGender.Replace("'", "''");
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Gender;
                                }

                                if (eSC.SearchOutput.Length > 0)
                                {
                                    eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                }
                                eSC.DetailsViewedYN = false;
                                eSC.Billable = false;
                                eSC.CreatedByUser = eSe.CreatedByUser;
                                eSC.CreatedOnDate = DateTime.Now;
                                eSC.KeyID = 0;
                                eSC.KeyType = rp.ResponseKeyType;

                                eSC.BillingTypeID = spr.BillingTypeID;
                                eSC.BillingPrice = spr.UnitPrice;

                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                eSC.ExtraVarOutput1 = "";
                                eSC.ProductID = intProductId;
                                eSC.VoucherCode = strVoucherCode;

                                string BonusXML = string.Empty;

                                intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);

                                //Build list 
                                ds = new DataSet("ListOfConsumers");

                                DataTable dtSubscriberInput = new DataTable("ConsumerDetails");
                                dtSubscriberInput.Columns.Add("ConsumerID", typeof(Int32));
                                dtSubscriberInput.Columns.Add("FirstName", typeof(String));
                                dtSubscriberInput.Columns.Add("SecondName", typeof(String));
                                dtSubscriberInput.Columns.Add("ThirdName", typeof(String));
                                dtSubscriberInput.Columns.Add("Surname", typeof(String));
                                dtSubscriberInput.Columns.Add("IDNo", typeof(String));
                                dtSubscriberInput.Columns.Add("PassportNo", typeof(String));
                                dtSubscriberInput.Columns.Add("BirthDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("GenderInd", typeof(String));
                                dtSubscriberInput.Columns.Add("BonusXML", typeof(String));
                                dtSubscriberInput.Columns.Add("TempReference", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryID", typeof(Int32));
                                dtSubscriberInput.Columns.Add("EnquiryResultID", typeof(Int32));
                                dtSubscriberInput.Columns.Add("Reference", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["ConsumerID"] = 0;
                                drSubscriberInput["FirstName"] = strFirstName;
                                drSubscriberInput["SecondName"] = "";
                                drSubscriberInput["ThirdName"] = "";
                                drSubscriberInput["Surname"] = strSurname;
                                drSubscriberInput["IDNo"] = strIDNo;
                                drSubscriberInput["PassportNo"] = strPassportNo;
                                drSubscriberInput["BirthDate"] = dtBirthDate;
                                drSubscriberInput["GenderInd"] = sGender;
                                drSubscriberInput["BonusXML"] = "";
                                drSubscriberInput["TempReference"] = "";
                                drSubscriberInput["EnquiryID"] = intSubscriberEnquiryID;
                                drSubscriberInput["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                drSubscriberInput["Reference"] = strExtRef;

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);

                                rp.ResponseData = ds.GetXml();
                                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                                ConsumerExists = false;
                            }
                            
                            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();
                            string realTime = string.Empty;

                            if (!string.IsNullOrEmpty(strIDNo))
                            {
                                XDSPortalLibrary.Entity_Layer.Response rp1 = new XDSPortalLibrary.Entity_Layer.Response();

                                XDSPortalEnquiry.Business.ConsumerTrace_IdentityVerification dtConsumertrace = new XDSPortalEnquiry.Business.ConsumerTrace_IdentityVerification();

                                rp1 = dtConsumertrace.SubmitRealTimeIdentityVerification(con, AdminConnection, sub.SubscriberID, intSystemUserID, 153, sub.SubscriberName, strIDNo, strSurname, strFirstName, "", dtBirthDate, strExtRef, true, true, strVoucherCode, strDHAURL);
                                switch (rp1.ResponseStatus)
                                {
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single:
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple:
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                                        rsXml.LoadXml(rp1.ResponseData);
                                        break;
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                                        rXml = "<NoResult><Error>" + rp1.ResponseData + "</Error></NoResult>";
                                        rsXml.LoadXml(rXml);
                                        break;
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                                        rXml = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                                        rsXml.LoadXml(rXml);
                                        break;
                                }

                                realTime = rsXml.OuterXml;
                                System.IO.StringReader xmlSRTIDV = new System.IO.StringReader(realTime);
                                DataSet ds2 = new DataSet();
                                ds2.ReadXml(xmlSRTIDV);

                                if (ds2.Tables.Contains("RealTimeIDV"))
                                {
                                    if (ds2.Tables["RealTimeIDV"].Rows.Count > 1)
                                    {
                                        for (int i = 0; i < ds2.Tables["RealTimeIDV"].Rows.Count; i++)
                                        {
                                            if (i > 0)
                                            {
                                                ds2.Tables["RealTimeIDV"].Rows[i].Delete();
                                            }
                                        }
                                    }
                                }

                                string IDFirstname = string.Empty;
                                string IDSurname = string.Empty;

                                if (ds2.Tables.Contains("RealTimeIDV"))
                                { 
                                    foreach (DataRow r in ds2.Tables["RealTimeIDV"].Rows)
                                    {
                                        if (r.Table.Columns.Contains("HASurname"))
                                        {
                                            if (!string.IsNullOrEmpty(r["HASurname"].ToString()))
                                            {
                                                IDSurname = r["HASurname"].ToString();
                                                //  eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                            }
                                        }
                                        if (r.Table.Columns.Contains("HANames"))
                                        {
                                            if (!string.IsNullOrEmpty(r["HANames"].ToString()))
                                            {
                                                IDFirstname = r["HANames"].ToString().Replace("'", "''");
                                                //  eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                            }
                                        }
                                    }
                                }
                                
                                if (string.IsNullOrEmpty(IDFirstname) && string.IsNullOrEmpty(IDSurname))
                                {
                                    IDFirstname = strFirstName;
                                    IDSurname = strSurname;

                                    DataSet ds3 = new DataSet("HomeAffairs");
                                    string XML = ds3.GetXml();
                                    rsXml.LoadXml(XML);
                                    realTime = rsXml.OuterXml;
                                }

                                Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                DataSet dsDataSegments = null;
                                XDSPortalLibrary.Entity_Layer.Response rp2 = new XDSPortalLibrary.Entity_Layer.Response();
                                XDSPortalEnquiry.Business.LexisNexisData lexisNexisData = new XDSPortalEnquiry.Business.LexisNexisData();

                                rp2 = lexisNexisData.RenderLexisNexisData(con, AdminConnection, intSubscriberEnquiryID, intSubscriberEnquiryResultID, dsDataSegments, 
                                    realTime, strClientID, strUserID, strPassword, strURL, intProductId, IDFirstname, IDSurname, dtBirthDate, strGender, 
                                    strAddressLine1, strAddressLine2, strCity, strProvince, strPostalCode, strCountry, strCitizenship, strCellNumber);
                                if (ConsumerExists == false && rp2.ResponseData.Contains("SupplierData") == false)
                                {
                                    rp.ResponseData = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                                }
                            }
                            else if (!string.IsNullOrEmpty(strPassportNo))
                            {
                                DataSet ds3 = new DataSet("HomeAffairs");
                                DataSet dsempty = new DataSet();
                                
                                if (string.IsNullOrEmpty(realTime))
                                {
                                    string XML = ds3.GetXml();
                                    rsXml.LoadXml(XML);
                                    realTime = rsXml.OuterXml;
                                }

                                Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                DataSet dsDataSegments = null;
                                XDSPortalLibrary.Entity_Layer.Response rp2 = new XDSPortalLibrary.Entity_Layer.Response();
                                XDSPortalEnquiry.Business.LexisNexisData lexisNexisData = new XDSPortalEnquiry.Business.LexisNexisData();

                                rp2 = lexisNexisData.RenderLexisNexisData(con, AdminConnection, intSubscriberEnquiryID, intSubscriberEnquiryResultID, dsDataSegments, 
                                    realTime, strClientID, strUserID, strPassword, strURL, intProductId, strFirstName, strSurname, dtBirthDate, strGender,
                                    strAddressLine1, strAddressLine2, strCity, strProvince, strPostalCode, strCountry, strCitizenship, strCellNumber);
                                if (ConsumerExists == false && rp2.ResponseData.Contains("SupplierData") == false)
                                {
                                    rp.ResponseData = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                                }
                            }
                            else
                            {
                                DataSet dsDataSegments = null;
                                XDSPortalLibrary.Entity_Layer.Response rp2 = new XDSPortalLibrary.Entity_Layer.Response();
                                XDSPortalEnquiry.Business.LexisNexisData lexisNexisData = new XDSPortalEnquiry.Business.LexisNexisData();

                                rp2 = lexisNexisData.RenderLexisNexisData(con, AdminConnection, intSubscriberEnquiryID, intSubscriberEnquiryResultID, dsDataSegments, 
                                    realTime, strClientID, strUserID, strPassword, strURL, intProductId, strFirstName, strSurname, dtBirthDate, strGender,
                                    strAddressLine1, strAddressLine2, strCity, strProvince, strPostalCode, strCountry, strCitizenship, strCellNumber);
                                if (ConsumerExists == false && rp2.ResponseData.Contains("SupplierData") == false)
                                {
                                    rp.ResponseData = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                                }
                            }
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);
                        }
                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);

                MatchResult = eSe.SubscriberEnquiryID;
                rp.EnquiryID = eSe.SubscriberEnquiryID;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (oException.Message.Contains("A .NET Framework"))
                {
                    rp.ResponseData = "Error: The System is unable to process this request. Please contact XDS Support";
                }

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitConsumerTraceLiteCompliance(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string strIDNo, string strPassportNo, string strSurname, string strMaidenName, string strFirstName, string strSecondName, string strFirstInitial, string strSecondInitial, DateTime dtBirthDate, Entity.SubscriberEnquiry.GenderInd rblGender, string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode, string strDHAURL, string strClientID, string strUserID, string strPassword, string strURL)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            string sGender = "";
            string rXml = "";
            string rXmlMatch = "";
            int MatchResult = 0;
            int intSubscriberEnquiryResultID = 0;
            DataSet ds = new DataSet();
            Boolean boolSubmitEnquiry = true;
            string strValidationStatus = "";
            double Totalcost = 0;

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();


            if (rblGender == Entity.SubscriberEnquiry.GenderInd.M)
            {
                sGender = "M";
            }
            if (rblGender == Entity.SubscriberEnquiry.GenderInd.F)
            {
                sGender = "F";
            }

            try
            {
                // Check if this subscriber is authorized to use the current product

                if (spr.ReportID > 0)
                {
                    // Check the PayAsYouGo settings for the subscriber
                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1)
                    {
                        //Calculate The Total cost of the Report , including Bonus Segments

                        Totalcost = spr.UnitPrice;

                        if (eoConsumerTraceWseManager.BonusSegments != null)
                        {

                            foreach (DataRow dr in eoConsumerTraceWseManager.BonusSegments.Tables[0].Rows)
                            {
                                if (dr["BonusViewed"].ToString().ToLower() == "true")
                                {
                                    Totalcost = Totalcost + Convert.ToInt16(dr["BonusPrice"].ToString());
                                }
                            }
                        }
                    }

                    if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {

                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;

                        eSe.BirthDate = dtBirthDate;
                        eSe.FirstInitial = strFirstInitial;
                        eSe.FirstName = strFirstName;
                        eSe.Gender = rblGender.ToString();
                        eSe.IDNo = strIDNo;
                        eSe.MaidenName = strMaidenName;
                        eSe.PassportNo = strPassportNo;
                        eSe.ProductID = intProductId;
                        eSe.SecondInitial = strSecondInitial;
                        eSe.SecondName = strSecondName;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = strExtRef;
                        eSe.Surname = strSurname;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";

                        //Generate serach input string

                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.PassportNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.PassportNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.Surname))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.Surname;
                        }
                        if (!string.IsNullOrEmpty(eSe.FirstInitial))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.FirstName;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table

                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);

                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        //SqlConnection MatchingConstring = new SqlConnection(eSe.ExtraVarInput1.ToString());

                        //moConsumerTraceWseManager.ConnectionString = MatchingConstring;
                        eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoConsumerTraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoConsumerTraceWseManager.ConfirmationChkBox = bConfirmationChkBox;
                        eoConsumerTraceWseManager.DOB = dtBirthDate;
                        eoConsumerTraceWseManager.ExternalReference = strExtRef;
                        eoConsumerTraceWseManager.FirstInitial = strFirstInitial;
                        eoConsumerTraceWseManager.FirstName = strFirstName;
                        eoConsumerTraceWseManager.Gender = sGender;
                        eoConsumerTraceWseManager.IDno = strIDNo;
                        eoConsumerTraceWseManager.MaidenName = strMaidenName;
                        eoConsumerTraceWseManager.Passportno = strPassportNo;
                        eoConsumerTraceWseManager.ProductID = intProductId;
                        eoConsumerTraceWseManager.SecondInitial = strSecondInitial;
                        eoConsumerTraceWseManager.SecondName = strSecondName;
                        eoConsumerTraceWseManager.subscriberID = intSubscriberID;
                        eoConsumerTraceWseManager.Surname = strSurname;
                        eoConsumerTraceWseManager.BonusCheck = bBonusChecking;

                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.ConsumerTraceMatchLiteComplianceExposure(eoConsumerTraceWseManager);

                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.ConsumerTraceMatchLiteComplianceExposure(eoConsumerTraceWseManager);
                        }


                        //rp = moConsumerTraceWseManager.search(eoConsumerTraceWseManager);
                        rp.EnquiryID = intSubscriberEnquiryID;

                        // Get Response data

                        rXmlMatch = rp.ResponseData;
                        System.IO.StringReader xmlSRMatch = new System.IO.StringReader(rXmlMatch);

                        bool ConsumerExists = true;

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None ||
                            rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report ||
                            rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple ||
                            rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                        {
                            ds.ReadXml(xmlSRMatch);

                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                if (ds.Tables["ConsumerDetails"].Rows.Count > 1)
                                {
                                    for (int i = 0; i < ds.Tables["ConsumerDetails"].Rows.Count; i++)
                                    {
                                        if (i > 0)
                                        {
                                            ds.Tables["ConsumerDetails"].Rows[i].Delete();
                                        }
                                    }
                                }

                                ds.Tables[0].Columns.Add("EnquiryID");
                                ds.Tables[0].Columns.Add("EnquiryResultID");
                                ds.Tables[0].Columns.Add("Reference");

                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {

                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("SecondName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["SecondName"].ToString()))
                                        {
                                            eSC.SecondName = r["SecondName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.SecondName;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("BirthDate"))
                                    {
                                        if (!string.IsNullOrEmpty(r["BirthDate"].ToString()))
                                        {
                                            eSC.BirthDate = Convert.ToDateTime(r["BirthDate"]);
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + Convert.ToDateTime(r["BirthDate"]).ToShortDateString();
                                        }
                                    }
                                    if (r.Table.Columns.Contains("GenderInd"))
                                    {
                                        if (!string.IsNullOrEmpty(r["GenderInd"].ToString()))
                                        {
                                            eSC.Gender = r["GenderInd"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Gender;
                                        }
                                    }


                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }
                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {
                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");


                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);

                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;
                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;

                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);
                                            }



                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {
                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    }

                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }
                            else
                            {
                                eSC.SearchOutput = "";
                                eSC.IDNo = "";
                                eSC.PassportNo = "";
                                eSC.Surname = "";
                                eSC.FirstName = "";
                                eSC.BirthDate = DateTime.Parse("1900/01/01");
                                eSC.Gender = "";

                                if (!string.IsNullOrEmpty(strIDNo))
                                {
                                    eSC.IDNo = strIDNo;
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                }

                                if (!string.IsNullOrEmpty(strPassportNo))
                                {
                                    eSC.PassportNo = strPassportNo.Replace("'", "''");
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                }

                                if (!string.IsNullOrEmpty(strSurname))
                                {
                                    eSC.Surname = strSurname.Replace("'", "''");
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                }

                                if (!string.IsNullOrEmpty(strFirstName))
                                {
                                    eSC.FirstName = strFirstName.Replace("'", "''");
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                }

                                if (!string.IsNullOrEmpty(strSecondName))
                                {
                                    eSC.SecondName = strSecondName.Replace("'", "''");
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.SecondName;
                                }

                                if (!string.IsNullOrEmpty(dtBirthDate.ToString()))
                                {
                                    eSC.BirthDate = dtBirthDate;
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + Convert.ToDateTime(dtBirthDate.ToShortDateString());
                                }

                                if (!string.IsNullOrEmpty(sGender))
                                {
                                    eSC.Gender = sGender.Replace("'", "''");
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Gender;
                                }

                                if (eSC.SearchOutput.Length > 0)
                                {
                                    eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                }
                                eSC.DetailsViewedYN = false;
                                eSC.Billable = false;
                                eSC.CreatedByUser = eSe.CreatedByUser;
                                eSC.CreatedOnDate = DateTime.Now;
                                eSC.KeyID = 0;
                                eSC.KeyType = rp.ResponseKeyType;

                                eSC.BillingTypeID = spr.BillingTypeID;
                                eSC.BillingPrice = spr.UnitPrice;

                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                eSC.ExtraVarOutput1 = "";
                                eSC.ProductID = intProductId;
                                eSC.VoucherCode = strVoucherCode;

                                string BonusXML = string.Empty;

                                intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);

                                //Build list 
                                ds = new DataSet("ListOfConsumers");

                                DataTable dtSubscriberInput = new DataTable("ConsumerDetails");
                                dtSubscriberInput.Columns.Add("ConsumerID", typeof(Int32));
                                dtSubscriberInput.Columns.Add("FirstName", typeof(String));
                                dtSubscriberInput.Columns.Add("SecondName", typeof(String));
                                dtSubscriberInput.Columns.Add("ThirdName", typeof(String));
                                dtSubscriberInput.Columns.Add("Surname", typeof(String));
                                dtSubscriberInput.Columns.Add("IDNo", typeof(String));
                                dtSubscriberInput.Columns.Add("PassportNo", typeof(String));
                                dtSubscriberInput.Columns.Add("BirthDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("GenderInd", typeof(String));
                                dtSubscriberInput.Columns.Add("BonusXML", typeof(String));
                                dtSubscriberInput.Columns.Add("TempReference", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryID", typeof(Int32));
                                dtSubscriberInput.Columns.Add("EnquiryResultID", typeof(Int32));
                                dtSubscriberInput.Columns.Add("Reference", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["ConsumerID"] = 0;
                                drSubscriberInput["FirstName"] = strFirstName;
                                drSubscriberInput["SecondName"] = "";
                                drSubscriberInput["ThirdName"] = "";
                                drSubscriberInput["Surname"] = strSurname;
                                drSubscriberInput["IDNo"] = strIDNo;
                                drSubscriberInput["PassportNo"] = strPassportNo;
                                drSubscriberInput["BirthDate"] = dtBirthDate;
                                drSubscriberInput["GenderInd"] = sGender;
                                drSubscriberInput["BonusXML"] = "";
                                drSubscriberInput["TempReference"] = "";
                                drSubscriberInput["EnquiryID"] = intSubscriberEnquiryID;
                                drSubscriberInput["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                drSubscriberInput["Reference"] = strExtRef;

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);

                                rp.ResponseData = ds.GetXml();
                                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                                ConsumerExists = false;
                            }

                            //string realTime = ConnectGetRealTimeIDVerification(clientID, ConnectTicket, "153", oSubscriberEnquiry.IDNo, "", "", oSubscriberEnquiry.SubscriberReference, "");
                            //System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();
                            //XDSPortalLibrary.Entity_Layer.Response rp1 = new XDSPortalLibrary.Entity_Layer.Response();

                            //XDSPortalEnquiry.Business.ConsumerTrace_IdentityVerification dtConsumertrace = new XDSPortalEnquiry.Business.ConsumerTrace_IdentityVerification();
                            //rp1 = dtConsumertrace.SubmitRealTimeIdentityVerification(con, AdminConnection, sub.SubscriberID, intSystemUserID, 153, sub.SubscriberName, strIDNo, strSurname, strFirstName, "", dtBirthDate, strExtRef, true, true, strVoucherCode, strDHAURL);

                            //switch (rp1.ResponseStatus)
                            //{
                            //    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single:
                            //    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple:
                            //    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                            //        rsXml.LoadXml(rp1.ResponseData);
                            //        break;
                            //    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                            //        rXml = "<NoResult><Error>" + rp.ResponseData + "</Error></NoResult>";
                            //        rsXml.LoadXml(rXml);
                            //        break;
                            //    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                            //        rXml = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                            //        rsXml.LoadXml(rXml);
                            //        break;
                            //}

                            //string realTime = rsXml.OuterXml;

                            //Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                            //Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                            //DataSet dsDataSegments = null;
                            //XDSPortalLibrary.Entity_Layer.Response rp2 = new XDSPortalLibrary.Entity_Layer.Response();
                            //XDSPortalEnquiry.Business.LexisNexisData lexisNexisData = new XDSPortalEnquiry.Business.LexisNexisData();

                            //rp2 = lexisNexisData.RenderLexisNexisData(con, AdminConnection, intSubscriberEnquiryID, intSubscriberEnquiryResultID, dsDataSegments, realTime, strClientID, strUserID, strPassword, strURL, 176, strFirstName, strSurname);

                            //////rXml = rp2.ResponseData;

                            //////// Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                            //////if (sub.PayAsYouGo == 1)
                            //////{
                            //////    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                            //////}
                            //////if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                            //////{
                            //////    dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                            //////}

                            //////if (String.IsNullOrEmpty(eSC.VoucherCode))
                            //////{
                            //////    //Log FootPrint

                            //////    eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                            //////    eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                            //////    eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                            //////    eSubscriberFootPrint.KeyID = eSC.KeyID;
                            //////    eSubscriberFootPrint.KeyType = eSC.KeyType;
                            //////    eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                            //////    eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                            //////    if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                            //////    {
                            //////        eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                            //////    }
                            //////    eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                            //////    eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                            //////    eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                            //////    eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                            //////    eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                            //////    eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                            //////    eSubscriberFootPrint.CreatedByUser = sys.Username;
                            //////    eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                            //////    eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                            //////    eSubscriberFootPrint.ProductID = eSe.ProductID;
                            //////    eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                            //////    int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                            //////}

                            //Check if data returned by Bureau and LN, if not, return nothing
                            //if (ConsumerExists == false && rp2.ResponseData.Contains("SupplierData") == false)
                            //{
                            //    rp.ResponseData = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                            //}
                            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();
                            string realTime = string.Empty;
                            if (!string.IsNullOrEmpty(strIDNo))
                            {
                                XDSPortalLibrary.Entity_Layer.Response rp1 = new XDSPortalLibrary.Entity_Layer.Response();

                                //  XDSPortalEnquiry.Business.ConsumerTrace_IdentityVerification dtConsumertrace = new XDSPortalEnquiry.Business.ConsumerTrace_IdentityVerification();
                                XDSPortalEnquiry.Business.ConsumerTrace_IdentityVerification dtConsumertrace = new XDSPortalEnquiry.Business.ConsumerTrace_IdentityVerification();
                                //   rp1 = dtConsumertrace.SubmitRealTimeIdentityVerification(con, AdminConnection, sub.SubscriberID, intSystemUserID, 153, sub.SubscriberName, strIDNo, strSurname, strFirstName, "", dtBirthDate, strExtRef, true, true, strVoucherCode, strDHAURL);
                                //   rp1 = dtConsumertrace.SubmitRealTimeIdentityVerification(con, AdminConnection, sub.SubscriberID, intSystemUserID, 153, sub.SubscriberName, strIDNo, strSurname, strFirstName, "", dtBirthDate, strExtRef, true, true, strVoucherCode, strDHAURL, strClientIPAddress, "ConnectGetRealTimeIDVerification");
                                rp1= dtConsumertrace.SubmitRealTimeIdentityVerification(con, AdminConnection, sub.SubscriberID, intSystemUserID, 153, sub.SubscriberName, strIDNo, strSurname, strFirstName, "", dtBirthDate, strExtRef, true, true, strVoucherCode, strDHAURL);
                                switch (rp1.ResponseStatus)
                                {
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single:
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple:
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                                        rsXml.LoadXml(rp1.ResponseData);
                                        break;
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                                        rXml = "<NoResult><Error>" + rp1.ResponseData + "</Error></NoResult>";
                                        rsXml.LoadXml(rXml);
                                        break;
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                                        rXml = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                                        rsXml.LoadXml(rXml);
                                        break;
                                }

                                realTime = rsXml.OuterXml;
                                System.IO.StringReader xmlSRTIDV = new System.IO.StringReader(realTime);
                                DataSet ds2 = new DataSet();
                                ds2.ReadXml(xmlSRTIDV);

                                if (ds2.Tables.Contains("RealTimeIDV"))
                                {
                                    if (ds2.Tables["RealTimeIDV"].Rows.Count > 1)
                                    {
                                        for (int i = 0; i < ds2.Tables["RealTimeIDV"].Rows.Count; i++)
                                        {
                                            if (i > 0)
                                            {
                                                ds2.Tables["RealTimeIDV"].Rows[i].Delete();
                                            }
                                        }
                                    }
                                }
                                string IDFirstname = string.Empty;
                                string IDSurname = string.Empty;

                                foreach (DataRow r in ds2.Tables["RealTimeIDV"].Rows)
                                {



                                    if (r.Table.Columns.Contains("HASurname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["HASurname"].ToString()))
                                        {
                                            IDSurname = r["HASurname"].ToString();
                                            //  eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("HANames"))
                                    {
                                        if (!string.IsNullOrEmpty(r["HANames"].ToString()))
                                        {
                                            IDFirstname = r["HANames"].ToString().Replace("'", "''");
                                            //  eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }
                                }


                                //MultipleTrace_IdentityVerification Mt = new MultipleTrace_IdentityVerification();
                                //DataSet ds1 = new DataSet();
                                //XDSPortalLibrary.Entity_Layer.Response rp_Buerau = new XDSPortalLibrary.Entity_Layer.Response();
                                //rp_Buerau = Mt.GetBuerauDetails(con, AdminConnection, eSC.SubscriberEnquiryID, eSC.SubscriberEnquiryResultID, ds1, false, "");
                                //if (rp_Buerau.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                                //{
                                //    System.Xml.XmlDocument rsXmlB = new System.Xml.XmlDocument();
                                //    if (!string.IsNullOrEmpty(rp_Buerau.ResponseData))
                                //    {
                                //        rsXmlB.LoadXml(rp_Buerau.ResponseData);
                                //        string ConsumerDetails = rsXmlB.OuterXml;
                                //        System.IO.StringReader xmlSRTIDVB = new System.IO.StringReader(ConsumerDetails);
                                //        DataSet consumerds = new DataSet();
                                //        consumerds.ReadXml(xmlSRTIDVB);
                                //        if (consumerds.Tables.Contains("ConsumerDetail"))
                                //        {
                                //            // ds2.Tables.Add(consumerds.Tables["ConsumerDetail"].ToString().Copy());
                                //            ds2.Tables.Add(consumerds.Tables["ConsumerDetail"].Copy());

                                //        }
                                //    }
                                //    string XML = ds2.GetXml();
                                //    rsXml.LoadXml(XML);
                                //    realTime = rsXml.OuterXml;
                                //}
                                if (string.IsNullOrEmpty(realTime) || realTime.Contains("NoResult"))
                                {
                                    DataSet ds3 = new DataSet("HomeAffairs");
                                    string XML = ds3.GetXml();
                                    rsXml.LoadXml(XML);
                                    realTime = rsXml.OuterXml;
                                }
                                Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                DataSet dsDataSegments = null;
                                XDSPortalLibrary.Entity_Layer.Response rp2 = new XDSPortalLibrary.Entity_Layer.Response();
                                XDSPortalEnquiry.Business.LexisNexisData lexisNexisData = new XDSPortalEnquiry.Business.LexisNexisData();

                                rp2 = lexisNexisData.RenderLexisNexisDataLite(con, AdminConnection, intSubscriberEnquiryID, intSubscriberEnquiryResultID, dsDataSegments, realTime, strClientID, strUserID, strPassword, strURL, 176, IDFirstname, IDSurname);
                                if (ConsumerExists == false && rp2.ResponseData.Contains("SupplierData") == false)
                                {
                                    rp.ResponseData = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                                }
                            }
                            else if (!string.IsNullOrEmpty(strPassportNo))
                            {
                                DataSet ds3 = new DataSet("HomeAffairs");
                                DataSet dsempty = new DataSet();
                                //MultipleTrace_IdentityVerification Mt = new MultipleTrace_IdentityVerification();

                                //XDSPortalLibrary.Entity_Layer.Response rp_Buerau = new XDSPortalLibrary.Entity_Layer.Response();
                                //rp_Buerau = Mt.GetBuerauDetails(con, AdminConnection, eSC.SubscriberEnquiryID, eSC.SubscriberEnquiryResultID, dsempty, false, "");
                                //if (rp_Buerau.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                                //{
                                //    System.Xml.XmlDocument rsXmlB = new System.Xml.XmlDocument();
                                //    if (!string.IsNullOrEmpty(rp_Buerau.ResponseData))
                                //    {
                                //        rsXmlB.LoadXml(rp_Buerau.ResponseData);
                                //        string ConsumerDetails = rsXmlB.OuterXml;
                                //        System.IO.StringReader xmlSRTIDVB = new System.IO.StringReader(ConsumerDetails);
                                //        DataSet consumerds = new DataSet();
                                //        consumerds.ReadXml(xmlSRTIDVB);
                                //        if (consumerds.Tables.Contains("ConsumerDetail"))
                                //        {
                                //            // ds2.Tables.Add(consumerds.Tables["ConsumerDetail"].ToString().Copy());
                                //            ds3.Tables.Add(consumerds.Tables["ConsumerDetail"].Copy());
                                //            string XML = ds3.GetXml();
                                //            rsXml.LoadXml(XML);
                                //            realTime = rsXml.OuterXml;
                                //        }

                                //    }


                                //}
                                if(string.IsNullOrEmpty(realTime))
                                {
                                    string XML = ds3.GetXml();
                                    rsXml.LoadXml(XML);
                                    realTime = rsXml.OuterXml;
                                }
                               
                                Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                DataSet dsDataSegments = null;
                                XDSPortalLibrary.Entity_Layer.Response rp2 = new XDSPortalLibrary.Entity_Layer.Response();
                                XDSPortalEnquiry.Business.LexisNexisData lexisNexisData = new XDSPortalEnquiry.Business.LexisNexisData();

                                rp2 = lexisNexisData.RenderLexisNexisDataLite(con, AdminConnection, intSubscriberEnquiryID, intSubscriberEnquiryResultID, dsDataSegments, realTime, strClientID, strUserID, strPassword, strURL, 176, strFirstName, strSurname);
                                if (ConsumerExists == false && rp2.ResponseData.Contains("SupplierData") == false)
                                {
                                    rp.ResponseData = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                                }
                            }
                            else
                            {
                                DataSet dsDataSegments = null;
                                XDSPortalLibrary.Entity_Layer.Response rp2 = new XDSPortalLibrary.Entity_Layer.Response();
                                XDSPortalEnquiry.Business.LexisNexisData lexisNexisData = new XDSPortalEnquiry.Business.LexisNexisData();

                                rp2 = lexisNexisData.RenderLexisNexisDataLite(con, AdminConnection, intSubscriberEnquiryID, intSubscriberEnquiryResultID, dsDataSegments, realTime, strClientID, strUserID, strPassword, strURL, 176, strFirstName, strSurname);
                                if (ConsumerExists == false && rp2.ResponseData.Contains("SupplierData") == false)
                                {
                                    rp.ResponseData = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                                }
                            }

                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);
                            
                        }

                    }

                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }
            catch (Exception oException)
            {

                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);

                MatchResult = eSe.SubscriberEnquiryID;
                rp.EnquiryID = eSe.SubscriberEnquiryID;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (oException.Message.Contains("A .NET Framework"))
                {
                    rp.ResponseData = "Error: The System is unable to process this request. Please contact XDS Support";
                }

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitConsumerTraceLiteCompliance(SqlConnection con, SqlConnection AdminConnection, 
            int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string strIDNo, string strPassportNo, 
            string strSurname, string strMaidenName, string strFirstName, string strSecondName, string strFirstInitial, 
            string strSecondInitial, string strBirthDate, string strGender, string strAddressLine1, string strAddressLine2,
            string strCity, string strProvince, string strPostalCode, string strCountry, string strCitizenship, string strCellNumber,
            string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode, string strDHAURL, string strClientID,
            string strUserID, string strPassword, string strURL)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            string sGender = "";
            string rXml = "";
            string rXmlMatch = "";
            int MatchResult = 0;
            int intSubscriberEnquiryResultID = 0;
            DataSet ds = new DataSet();
            Boolean boolSubmitEnquiry = true;
            string strValidationStatus = "";
            double Totalcost = 0;

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            DateTime dtBirthDate = Convert.ToDateTime("1900-01-01"); ;

            if (string.IsNullOrEmpty(strFirstName))
            {
                throw new Exception("Your search input must have the firstname");
            }
            if (string.IsNullOrEmpty(strSurname))
            {
                throw new Exception("Your search input must have the surname");
            }
            if (!string.IsNullOrEmpty(strGender))
            {
                System.Collections.Generic.List<string> possibleGender = new System.Collections.Generic.List<string>();

                // Adding elements to List
                possibleGender.Add("M");
                possibleGender.Add("F");
                possibleGender.Add("MALE");
                possibleGender.Add("FEMALE");

                if (!possibleGender.Contains(strGender.ToUpper()))
                    throw new Exception("Invalid gender");

                if (strGender.ToUpper() == "M")
                    strGender = "Male";

                if (strGender.ToUpper() == "F")
                    strGender = "Female";
            }
            if (!string.IsNullOrEmpty(strCellNumber))
            {
                if (strCellNumber.Length == 10 && strCellNumber.StartsWith("0"))
                    strCellNumber = "+27" + strCellNumber.Substring(1, 9);
            }
            if (!string.IsNullOrEmpty(strBirthDate))
            {
                if (!DateTime.TryParse(strBirthDate, out dtBirthDate))
                    throw new Exception("Invalid birthdate");
            }
            if (!string.IsNullOrEmpty(strIDNo) && string.IsNullOrEmpty(strGender))
            {
                if (FnValidateIDNo(strIDNo))
                {
                    string IDSubstring = strIDNo.Substring(6, 4);
                    int iGender = int.Parse(IDSubstring);

                    if (iGender < 5000)
                        strGender = "Female";
                    else
                        strGender = "Male";
                }
            }

            try
            {
                // Check if this subscriber is authorized to use the current product

                if (spr.ReportID > 0)
                {
                    // Check the PayAsYouGo settings for the subscriber
                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1)
                    {
                        //Calculate The Total cost of the Report , including Bonus Segments

                        Totalcost = spr.UnitPrice;

                        if (eoConsumerTraceWseManager.BonusSegments != null)
                        {

                            foreach (DataRow dr in eoConsumerTraceWseManager.BonusSegments.Tables[0].Rows)
                            {
                                if (dr["BonusViewed"].ToString().ToLower() == "true")
                                {
                                    Totalcost = Totalcost + Convert.ToInt16(dr["BonusPrice"].ToString());
                                }
                            }
                        }
                    }

                    if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {

                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;

                        eSe.BirthDate = dtBirthDate;
                        eSe.FirstInitial = strFirstInitial;
                        eSe.FirstName = strFirstName;
                        eSe.Gender = strGender;
                        eSe.IDNo = strIDNo;
                        eSe.MaidenName = strMaidenName;
                        eSe.PassportNo = strPassportNo;
                        eSe.ProductID = intProductId;
                        eSe.SecondInitial = strSecondInitial;
                        eSe.SecondName = strSecondName;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = strExtRef;
                        eSe.Surname = strSurname;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";

                        //Generate serach input string

                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.PassportNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.PassportNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.Surname))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.Surname;
                        }
                        if (!string.IsNullOrEmpty(eSe.FirstInitial))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.FirstName;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table

                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);

                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        dSe.InsertSubscriberConsumerComplianceEnquiry(con, intSubscriberEnquiryID.ToString(), sys.SystemUserFullName, strIDNo,
                            strPassportNo, strFirstName, strSurname, dtBirthDate, strGender, strAddressLine1, strAddressLine2, strCity,
                            strProvince, strPostalCode, strCountry, strCitizenship, strCellNumber);

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        //SqlConnection MatchingConstring = new SqlConnection(eSe.ExtraVarInput1.ToString());

                        //moConsumerTraceWseManager.ConnectionString = MatchingConstring;
                        eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoConsumerTraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoConsumerTraceWseManager.ConfirmationChkBox = bConfirmationChkBox;
                        eoConsumerTraceWseManager.DOB = dtBirthDate;
                        eoConsumerTraceWseManager.ExternalReference = strExtRef;
                        eoConsumerTraceWseManager.FirstInitial = strFirstInitial;
                        eoConsumerTraceWseManager.FirstName = strFirstName;
                        eoConsumerTraceWseManager.Gender = sGender;
                        eoConsumerTraceWseManager.IDno = strIDNo;
                        eoConsumerTraceWseManager.MaidenName = strMaidenName;
                        eoConsumerTraceWseManager.Passportno = strPassportNo;
                        eoConsumerTraceWseManager.ProductID = intProductId;
                        eoConsumerTraceWseManager.SecondInitial = strSecondInitial;
                        eoConsumerTraceWseManager.SecondName = strSecondName;
                        eoConsumerTraceWseManager.subscriberID = intSubscriberID;
                        eoConsumerTraceWseManager.Surname = strSurname;
                        eoConsumerTraceWseManager.BonusCheck = bBonusChecking;

                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.ConsumerTraceMatchLiteComplianceExposure(eoConsumerTraceWseManager);

                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.ConsumerTraceMatchLiteComplianceExposure(eoConsumerTraceWseManager);
                        }


                        //rp = moConsumerTraceWseManager.search(eoConsumerTraceWseManager);
                        rp.EnquiryID = intSubscriberEnquiryID;

                        // Get Response data

                        rXmlMatch = rp.ResponseData;
                        System.IO.StringReader xmlSRMatch = new System.IO.StringReader(rXmlMatch);

                        bool ConsumerExists = true;

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None ||
                            rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report ||
                            rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple ||
                            rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                        {
                            ds.ReadXml(xmlSRMatch);

                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                if (ds.Tables["ConsumerDetails"].Rows.Count > 1)
                                {
                                    for (int i = 0; i < ds.Tables["ConsumerDetails"].Rows.Count; i++)
                                    {
                                        if (i > 0)
                                        {
                                            ds.Tables["ConsumerDetails"].Rows[i].Delete();
                                        }
                                    }
                                }

                                ds.Tables[0].Columns.Add("EnquiryID");
                                ds.Tables[0].Columns.Add("EnquiryResultID");
                                ds.Tables[0].Columns.Add("Reference");

                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {

                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("SecondName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["SecondName"].ToString()))
                                        {
                                            eSC.SecondName = r["SecondName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.SecondName;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("BirthDate"))
                                    {
                                        if (!string.IsNullOrEmpty(r["BirthDate"].ToString()))
                                        {
                                            eSC.BirthDate = Convert.ToDateTime(r["BirthDate"]);
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + Convert.ToDateTime(r["BirthDate"]).ToShortDateString();
                                        }
                                    }
                                    if (r.Table.Columns.Contains("GenderInd"))
                                    {
                                        if (!string.IsNullOrEmpty(r["GenderInd"].ToString()))
                                        {
                                            eSC.Gender = r["GenderInd"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Gender;
                                        }
                                    }


                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }
                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {
                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");


                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);

                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;
                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;

                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);
                                            }



                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {
                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    }

                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }
                            else
                            {
                                eSC.SearchOutput = "";
                                eSC.IDNo = "";
                                eSC.PassportNo = "";
                                eSC.Surname = "";
                                eSC.FirstName = "";
                                eSC.BirthDate = DateTime.Parse("1900/01/01");
                                eSC.Gender = "";

                                if (!string.IsNullOrEmpty(strIDNo))
                                {
                                    eSC.IDNo = strIDNo;
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                }

                                if (!string.IsNullOrEmpty(strPassportNo))
                                {
                                    eSC.PassportNo = strPassportNo.Replace("'", "''");
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                }

                                if (!string.IsNullOrEmpty(strSurname))
                                {
                                    eSC.Surname = strSurname.Replace("'", "''");
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                }

                                if (!string.IsNullOrEmpty(strFirstName))
                                {
                                    eSC.FirstName = strFirstName.Replace("'", "''");
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                }

                                if (!string.IsNullOrEmpty(strSecondName))
                                {
                                    eSC.SecondName = strSecondName.Replace("'", "''");
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.SecondName;
                                }

                                if (!string.IsNullOrEmpty(dtBirthDate.ToString()))
                                {
                                    eSC.BirthDate = dtBirthDate;
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + Convert.ToDateTime(dtBirthDate.ToShortDateString());
                                }

                                if (!string.IsNullOrEmpty(sGender))
                                {
                                    eSC.Gender = sGender.Replace("'", "''");
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Gender;
                                }

                                if (eSC.SearchOutput.Length > 0)
                                {
                                    eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                }
                                eSC.DetailsViewedYN = false;
                                eSC.Billable = false;
                                eSC.CreatedByUser = eSe.CreatedByUser;
                                eSC.CreatedOnDate = DateTime.Now;
                                eSC.KeyID = 0;
                                eSC.KeyType = rp.ResponseKeyType;

                                eSC.BillingTypeID = spr.BillingTypeID;
                                eSC.BillingPrice = spr.UnitPrice;

                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                eSC.ExtraVarOutput1 = "";
                                eSC.ProductID = intProductId;
                                eSC.VoucherCode = strVoucherCode;

                                string BonusXML = string.Empty;

                                intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);

                                //Build list 
                                ds = new DataSet("ListOfConsumers");

                                DataTable dtSubscriberInput = new DataTable("ConsumerDetails");
                                dtSubscriberInput.Columns.Add("ConsumerID", typeof(Int32));
                                dtSubscriberInput.Columns.Add("FirstName", typeof(String));
                                dtSubscriberInput.Columns.Add("SecondName", typeof(String));
                                dtSubscriberInput.Columns.Add("ThirdName", typeof(String));
                                dtSubscriberInput.Columns.Add("Surname", typeof(String));
                                dtSubscriberInput.Columns.Add("IDNo", typeof(String));
                                dtSubscriberInput.Columns.Add("PassportNo", typeof(String));
                                dtSubscriberInput.Columns.Add("BirthDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("GenderInd", typeof(String));
                                dtSubscriberInput.Columns.Add("BonusXML", typeof(String));
                                dtSubscriberInput.Columns.Add("TempReference", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryID", typeof(Int32));
                                dtSubscriberInput.Columns.Add("EnquiryResultID", typeof(Int32));
                                dtSubscriberInput.Columns.Add("Reference", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["ConsumerID"] = 0;
                                drSubscriberInput["FirstName"] = strFirstName;
                                drSubscriberInput["SecondName"] = "";
                                drSubscriberInput["ThirdName"] = "";
                                drSubscriberInput["Surname"] = strSurname;
                                drSubscriberInput["IDNo"] = strIDNo;
                                drSubscriberInput["PassportNo"] = strPassportNo;
                                drSubscriberInput["BirthDate"] = dtBirthDate;
                                drSubscriberInput["GenderInd"] = sGender;
                                drSubscriberInput["BonusXML"] = "";
                                drSubscriberInput["TempReference"] = "";
                                drSubscriberInput["EnquiryID"] = intSubscriberEnquiryID;
                                drSubscriberInput["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                drSubscriberInput["Reference"] = strExtRef;

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);

                                rp.ResponseData = ds.GetXml();
                                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                                ConsumerExists = false;
                            }

                            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();
                            string realTime = string.Empty;
                            if (!string.IsNullOrEmpty(strIDNo))
                            {
                                XDSPortalLibrary.Entity_Layer.Response rp1 = new XDSPortalLibrary.Entity_Layer.Response();

                                //  XDSPortalEnquiry.Business.ConsumerTrace_IdentityVerification dtConsumertrace = new XDSPortalEnquiry.Business.ConsumerTrace_IdentityVerification();
                                XDSPortalEnquiry.Business.ConsumerTrace_IdentityVerification dtConsumertrace = new XDSPortalEnquiry.Business.ConsumerTrace_IdentityVerification();
                                //   rp1 = dtConsumertrace.SubmitRealTimeIdentityVerification(con, AdminConnection, sub.SubscriberID, intSystemUserID, 153, sub.SubscriberName, strIDNo, strSurname, strFirstName, "", dtBirthDate, strExtRef, true, true, strVoucherCode, strDHAURL);
                                //   rp1 = dtConsumertrace.SubmitRealTimeIdentityVerification(con, AdminConnection, sub.SubscriberID, intSystemUserID, 153, sub.SubscriberName, strIDNo, strSurname, strFirstName, "", dtBirthDate, strExtRef, true, true, strVoucherCode, strDHAURL, strClientIPAddress, "ConnectGetRealTimeIDVerification");
                                rp1 = dtConsumertrace.SubmitRealTimeIdentityVerification(con, AdminConnection, sub.SubscriberID, intSystemUserID, 153, sub.SubscriberName, strIDNo, strSurname, strFirstName, "", dtBirthDate, strExtRef, true, true, strVoucherCode, strDHAURL);
                                switch (rp1.ResponseStatus)
                                {
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single:
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple:
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                                        rsXml.LoadXml(rp1.ResponseData);
                                        break;
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                                        rXml = "<NoResult><Error>" + rp1.ResponseData + "</Error></NoResult>";
                                        rsXml.LoadXml(rXml);
                                        break;
                                    case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                                        rXml = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                                        rsXml.LoadXml(rXml);
                                        break;
                                }

                                realTime = rsXml.OuterXml;
                                System.IO.StringReader xmlSRTIDV = new System.IO.StringReader(realTime);
                                DataSet ds2 = new DataSet();
                                ds2.ReadXml(xmlSRTIDV);

                                if (ds2.Tables.Contains("RealTimeIDV"))
                                {
                                    if (ds2.Tables["RealTimeIDV"].Rows.Count > 1)
                                    {
                                        for (int i = 0; i < ds2.Tables["RealTimeIDV"].Rows.Count; i++)
                                        {
                                            if (i > 0)
                                            {
                                                ds2.Tables["RealTimeIDV"].Rows[i].Delete();
                                            }
                                        }
                                    }
                                }
                                string IDFirstname = string.Empty;
                                string IDSurname = string.Empty;

                                foreach (DataRow r in ds2.Tables["RealTimeIDV"].Rows)
                                {
                                    if (r.Table.Columns.Contains("HASurname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["HASurname"].ToString()))
                                        {
                                            IDSurname = r["HASurname"].ToString();
                                            //  eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("HANames"))
                                    {
                                        if (!string.IsNullOrEmpty(r["HANames"].ToString()))
                                        {
                                            IDFirstname = r["HANames"].ToString().Replace("'", "''");
                                            //  eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }
                                }

                                if (string.IsNullOrEmpty(realTime) || realTime.Contains("NoResult"))
                                {
                                    DataSet ds3 = new DataSet("HomeAffairs");
                                    string XML = ds3.GetXml();
                                    rsXml.LoadXml(XML);
                                    realTime = rsXml.OuterXml;
                                }
                                Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                DataSet dsDataSegments = null;
                                XDSPortalLibrary.Entity_Layer.Response rp2 = new XDSPortalLibrary.Entity_Layer.Response();
                                XDSPortalEnquiry.Business.LexisNexisData lexisNexisData = new XDSPortalEnquiry.Business.LexisNexisData();

                                rp2 = lexisNexisData.RenderLexisNexisDataLite(con, AdminConnection, intSubscriberEnquiryID, intSubscriberEnquiryResultID, dsDataSegments,
                                    realTime, strClientID, strUserID, strPassword, strURL, intProductId, IDFirstname, IDSurname, dtBirthDate, strGender,
                                    strAddressLine1, strAddressLine2, strCity, strProvince, strPostalCode, strCountry, strCitizenship, strCellNumber);
                                if (ConsumerExists == false && rp2.ResponseData.Contains("SupplierData") == false)
                                {
                                    rp.ResponseData = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                                }
                            }
                            else if (!string.IsNullOrEmpty(strPassportNo))
                            {
                                DataSet ds3 = new DataSet("HomeAffairs");
                                DataSet dsempty = new DataSet();

                                if (string.IsNullOrEmpty(realTime))
                                {
                                    string XML = ds3.GetXml();
                                    rsXml.LoadXml(XML);
                                    realTime = rsXml.OuterXml;
                                }

                                Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                DataSet dsDataSegments = null;
                                XDSPortalLibrary.Entity_Layer.Response rp2 = new XDSPortalLibrary.Entity_Layer.Response();
                                XDSPortalEnquiry.Business.LexisNexisData lexisNexisData = new XDSPortalEnquiry.Business.LexisNexisData();

                                rp2 = lexisNexisData.RenderLexisNexisDataLite(con, AdminConnection, intSubscriberEnquiryID, intSubscriberEnquiryResultID, dsDataSegments,
                                    realTime, strClientID, strUserID, strPassword, strURL, intProductId, strFirstName, strSurname, dtBirthDate, strGender,
                                    strAddressLine1, strAddressLine2, strCity, strProvince, strPostalCode, strCountry, strCitizenship, strCellNumber);
                                if (ConsumerExists == false && rp2.ResponseData.Contains("SupplierData") == false)
                                {
                                    rp.ResponseData = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                                }
                            }
                            else
                            {
                                DataSet dsDataSegments = null;
                                XDSPortalLibrary.Entity_Layer.Response rp2 = new XDSPortalLibrary.Entity_Layer.Response();
                                XDSPortalEnquiry.Business.LexisNexisData lexisNexisData = new XDSPortalEnquiry.Business.LexisNexisData();

                                rp2 = lexisNexisData.RenderLexisNexisDataLite(con, AdminConnection, intSubscriberEnquiryID, intSubscriberEnquiryResultID, dsDataSegments,
                                    realTime, strClientID, strUserID, strPassword, strURL, intProductId, strFirstName, strSurname, dtBirthDate, strGender,
                                    strAddressLine1, strAddressLine2, strCity, strProvince, strPostalCode, strCountry, strCitizenship, strCellNumber);
                                if (ConsumerExists == false && rp2.ResponseData.Contains("SupplierData") == false)
                                {
                                    rp.ResponseData = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                                }
                            }

                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);

                        }

                    }

                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }
            catch (Exception oException)
            {

                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);

                MatchResult = eSe.SubscriberEnquiryID;
                rp.EnquiryID = eSe.SubscriberEnquiryID;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (oException.Message.Contains("A .NET Framework"))
                {
                    rp.ResponseData = "Error: The System is unable to process this request. Please contact XDS Support";
                }

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitConsumerTraceComplianceStandard(SqlConnection con, SqlConnection AdminConnection,
            int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string strIDNo, string strPassportNo,
            string strSurname, string strFirstName, string strNationality, string strCitizenship, string strCountry, string strExtRef, bool bBonusChecking,
            string strVoucherCode)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            string sGender = "";
            string rXml = "";
            string rXmlMatch = "";
            int MatchResult = 0;
            int intSubscriberEnquiryResultID = 0;
            DataSet ds = new DataSet();
            Boolean boolSubmitEnquiry = true;
            string strValidationStatus = "";
            double Totalcost = 0;

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            DateTime dtBirthDate = Convert.ToDateTime("1900-01-01");

            if (string.IsNullOrEmpty(strFirstName))
            {
                throw new Exception("Your search input must have the firstname");
            }
            if (string.IsNullOrEmpty(strSurname))
            {
                throw new Exception("Your search input must have the surname");
            }

            try
            {
                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {
                    // Check the PayAsYouGo settings for the subscriber
                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }
                    }
                    else if (sub.PayAsYouGo == 1)
                    {
                        //Calculate The Total cost of the Report , including Bonus Segments
                        Totalcost = spr.UnitPrice;

                        if (eoConsumerTraceWseManager.BonusSegments != null)
                        {

                            foreach (DataRow dr in eoConsumerTraceWseManager.BonusSegments.Tables[0].Rows)
                            {
                                if (dr["BonusViewed"].ToString().ToLower() == "true")
                                {
                                    Totalcost = Totalcost + Convert.ToInt16(dr["BonusPrice"].ToString());
                                }
                            }
                        }
                    }

                    if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {
                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;

                        eSe.BirthDate = dtBirthDate;
                        eSe.FirstInitial = "";
                        eSe.FirstName = strFirstName;
                        eSe.Gender = "";
                        eSe.IDNo = strIDNo;
                        eSe.MaidenName = "";
                        eSe.PassportNo = strPassportNo;
                        eSe.ProductID = intProductId;
                        eSe.SecondInitial = "";
                        eSe.SecondName = "";
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = strExtRef;
                        eSe.Surname = strSurname;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";

                        //Generate serach input string

                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.PassportNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.PassportNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.Surname))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.Surname;
                        }
                        if (!string.IsNullOrEmpty(eSe.FirstInitial))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.FirstName;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table
                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);
                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;
                        dSe.InsertSubscriberConsumerComplianceEnquiry(con, intSubscriberEnquiryID.ToString(), sys.SystemUserFullName, strIDNo,
                            strPassportNo, strFirstName, strSurname, dtBirthDate, "", "", "", "",
                            strNationality, "", strCountry, strCitizenship, "");
                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoConsumerTraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoConsumerTraceWseManager.ConfirmationChkBox = true;
                        eoConsumerTraceWseManager.DOB = dtBirthDate;
                        eoConsumerTraceWseManager.ExternalReference = strExtRef;
                        eoConsumerTraceWseManager.FirstInitial = "";
                        eoConsumerTraceWseManager.FirstName = strFirstName;
                        eoConsumerTraceWseManager.Gender = sGender;
                        eoConsumerTraceWseManager.IDno = strIDNo;
                        eoConsumerTraceWseManager.MaidenName = "";
                        eoConsumerTraceWseManager.Passportno = strPassportNo;
                        eoConsumerTraceWseManager.ProductID = intProductId;
                        eoConsumerTraceWseManager.SecondInitial = "";
                        eoConsumerTraceWseManager.SecondName = "";
                        eoConsumerTraceWseManager.subscriberID = intSubscriberID;
                        eoConsumerTraceWseManager.Surname = strSurname;
                        eoConsumerTraceWseManager.BonusCheck = bBonusChecking;

                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.ConsumerTraceMatchComplianceExposure(eoConsumerTraceWseManager);

                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.ConsumerTraceMatchComplianceExposure(eoConsumerTraceWseManager);
                        }

                        rp.EnquiryID = intSubscriberEnquiryID;

                        // Get Response data
                        rXmlMatch = rp.ResponseData;
                        System.IO.StringReader xmlSRMatch = new System.IO.StringReader(rXmlMatch);

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None ||
                            rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report ||
                            rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple ||
                            rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                        {
                            ds.ReadXml(xmlSRMatch);

                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                if (ds.Tables["ConsumerDetails"].Rows.Count > 1)
                                {
                                    for (int i = 0; i < ds.Tables["ConsumerDetails"].Rows.Count; i++)
                                    {
                                        if (i > 0)
                                        {
                                            ds.Tables["ConsumerDetails"].Rows[i].Delete();
                                        }
                                    }
                                }

                                ds.Tables[0].Columns.Add("EnquiryID");
                                ds.Tables[0].Columns.Add("EnquiryResultID");
                                ds.Tables[0].Columns.Add("Reference");

                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {

                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("SecondName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["SecondName"].ToString()))
                                        {
                                            eSC.SecondName = r["SecondName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.SecondName;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("BirthDate"))
                                    {
                                        if (!string.IsNullOrEmpty(r["BirthDate"].ToString()))
                                        {
                                            eSC.BirthDate = Convert.ToDateTime(r["BirthDate"]);
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + Convert.ToDateTime(r["BirthDate"]).ToShortDateString();
                                        }
                                    }
                                    if (r.Table.Columns.Contains("GenderInd"))
                                    {
                                        if (!string.IsNullOrEmpty(r["GenderInd"].ToString()))
                                        {
                                            eSC.Gender = r["GenderInd"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Gender;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }
                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {
                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");

                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);

                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;
                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;

                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);
                                            }
                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {
                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    }

                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }
                            else
                            {
                                eSC.SearchOutput = "";
                                eSC.IDNo = "";
                                eSC.PassportNo = "";
                                eSC.Surname = "";
                                eSC.FirstName = "";
                                eSC.BirthDate = DateTime.Parse("1900/01/01");
                                eSC.Gender = "";

                                if (!string.IsNullOrEmpty(strIDNo))
                                {
                                    eSC.IDNo = strIDNo;
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                }

                                if (!string.IsNullOrEmpty(strPassportNo))
                                {
                                    eSC.PassportNo = strPassportNo.Replace("'", "''");
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                }

                                if (!string.IsNullOrEmpty(strSurname))
                                {
                                    eSC.Surname = strSurname.Replace("'", "''");
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                }

                                if (!string.IsNullOrEmpty(strFirstName))
                                {
                                    eSC.FirstName = strFirstName.Replace("'", "''");
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                }

                                if (!string.IsNullOrEmpty(dtBirthDate.ToString()))
                                {
                                    eSC.BirthDate = dtBirthDate;
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + Convert.ToDateTime(dtBirthDate.ToShortDateString());
                                }

                                if (!string.IsNullOrEmpty(sGender))
                                {
                                    eSC.Gender = sGender.Replace("'", "''");
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Gender;
                                }

                                if (eSC.SearchOutput.Length > 0)
                                {
                                    eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                }
                                eSC.DetailsViewedYN = false;
                                eSC.Billable = false;
                                eSC.CreatedByUser = eSe.CreatedByUser;
                                eSC.CreatedOnDate = DateTime.Now;
                                eSC.KeyID = 0;
                                eSC.KeyType = rp.ResponseKeyType;

                                eSC.BillingTypeID = spr.BillingTypeID;
                                eSC.BillingPrice = spr.UnitPrice;

                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                eSC.ExtraVarOutput1 = "";
                                eSC.ProductID = intProductId;
                                eSC.VoucherCode = strVoucherCode;

                                string BonusXML = string.Empty;

                                intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);

                                //Build list 
                                ds = new DataSet("ListOfConsumers");

                                DataTable dtSubscriberInput = new DataTable("ConsumerDetails");
                                dtSubscriberInput.Columns.Add("ConsumerID", typeof(Int32));
                                dtSubscriberInput.Columns.Add("FirstName", typeof(String));
                                dtSubscriberInput.Columns.Add("SecondName", typeof(String));
                                dtSubscriberInput.Columns.Add("ThirdName", typeof(String));
                                dtSubscriberInput.Columns.Add("Surname", typeof(String));
                                dtSubscriberInput.Columns.Add("IDNo", typeof(String));
                                dtSubscriberInput.Columns.Add("PassportNo", typeof(String));
                                dtSubscriberInput.Columns.Add("BirthDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("GenderInd", typeof(String));
                                dtSubscriberInput.Columns.Add("BonusXML", typeof(String));
                                dtSubscriberInput.Columns.Add("TempReference", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryID", typeof(Int32));
                                dtSubscriberInput.Columns.Add("EnquiryResultID", typeof(Int32));
                                dtSubscriberInput.Columns.Add("Reference", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["ConsumerID"] = 0;
                                drSubscriberInput["FirstName"] = strFirstName;
                                drSubscriberInput["SecondName"] = "";
                                drSubscriberInput["ThirdName"] = "";
                                drSubscriberInput["Surname"] = strSurname;
                                drSubscriberInput["IDNo"] = strIDNo;
                                drSubscriberInput["PassportNo"] = strPassportNo;
                                drSubscriberInput["BirthDate"] = dtBirthDate;
                                drSubscriberInput["GenderInd"] = sGender;
                                drSubscriberInput["BonusXML"] = "";
                                drSubscriberInput["TempReference"] = "";
                                drSubscriberInput["EnquiryID"] = intSubscriberEnquiryID;
                                drSubscriberInput["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                drSubscriberInput["Reference"] = strExtRef;

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);

                                rp.ResponseData = ds.GetXml();
                                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                            }
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSC.SearchOutput = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";

                            if (!string.IsNullOrEmpty(strIDNo))
                            {
                                eSC.IDNo = strIDNo;
                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                            }

                            if (!string.IsNullOrEmpty(strPassportNo))
                            {
                                eSC.PassportNo = strPassportNo.Replace("'", "''");
                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                            }

                            if (!string.IsNullOrEmpty(strSurname))
                            {
                                eSC.Surname = strSurname.Replace("'", "''");
                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                            }

                            if (!string.IsNullOrEmpty(strFirstName))
                            {
                                eSC.FirstName = strFirstName.Replace("'", "''");
                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                            }

                            if (!string.IsNullOrEmpty(dtBirthDate.ToString()))
                            {
                                eSC.BirthDate = dtBirthDate;
                                eSC.SearchOutput = eSC.SearchOutput + " | " + Convert.ToDateTime(dtBirthDate.ToShortDateString());
                            }

                            if (!string.IsNullOrEmpty(sGender))
                            {
                                eSC.Gender = sGender.Replace("'", "''");
                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Gender;
                            }

                            if (eSC.SearchOutput.Length > 0)
                            {
                                eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                            }
                            eSC.DetailsViewedYN = false;
                            eSC.Billable = false;
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;
                            eSC.KeyID = 0;
                            eSC.KeyType = rp.ResponseKeyType;

                            eSC.BillingTypeID = spr.BillingTypeID;
                            eSC.BillingPrice = spr.UnitPrice;

                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                            eSC.ExtraVarOutput1 = "";
                            eSC.ProductID = intProductId;
                            eSC.VoucherCode = strVoucherCode;

                            string BonusXML = string.Empty;

                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);

                            //Build list 
                            ds = new DataSet("ListOfConsumers");

                            DataTable dtSubscriberInput = new DataTable("ConsumerDetails");
                            dtSubscriberInput.Columns.Add("ConsumerID", typeof(Int32));
                            dtSubscriberInput.Columns.Add("FirstName", typeof(String));
                            dtSubscriberInput.Columns.Add("SecondName", typeof(String));
                            dtSubscriberInput.Columns.Add("ThirdName", typeof(String));
                            dtSubscriberInput.Columns.Add("Surname", typeof(String));
                            dtSubscriberInput.Columns.Add("IDNo", typeof(String));
                            dtSubscriberInput.Columns.Add("PassportNo", typeof(String));
                            dtSubscriberInput.Columns.Add("BirthDate", typeof(DateTime));
                            dtSubscriberInput.Columns.Add("GenderInd", typeof(String));
                            dtSubscriberInput.Columns.Add("BonusXML", typeof(String));
                            dtSubscriberInput.Columns.Add("TempReference", typeof(String));
                            dtSubscriberInput.Columns.Add("EnquiryID", typeof(Int32));
                            dtSubscriberInput.Columns.Add("EnquiryResultID", typeof(Int32));
                            dtSubscriberInput.Columns.Add("Reference", typeof(String));
                            DataRow drSubscriberInput;
                            drSubscriberInput = dtSubscriberInput.NewRow();

                            drSubscriberInput["ConsumerID"] = 0;
                            drSubscriberInput["FirstName"] = strFirstName;
                            drSubscriberInput["SecondName"] = "";
                            drSubscriberInput["ThirdName"] = "";
                            drSubscriberInput["Surname"] = strSurname;
                            drSubscriberInput["IDNo"] = strIDNo;
                            drSubscriberInput["PassportNo"] = strPassportNo;
                            drSubscriberInput["BirthDate"] = dtBirthDate;
                            drSubscriberInput["GenderInd"] = sGender;
                            drSubscriberInput["BonusXML"] = "";
                            drSubscriberInput["TempReference"] = "";
                            drSubscriberInput["EnquiryID"] = intSubscriberEnquiryID;
                            drSubscriberInput["EnquiryResultID"] = intSubscriberEnquiryResultID;
                            drSubscriberInput["Reference"] = strExtRef;

                            dtSubscriberInput.Rows.Add(drSubscriberInput);

                            ds.Tables.Add(dtSubscriberInput);

                            rp.ResponseData = ds.GetXml();
                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        }
                    }

                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);

                MatchResult = eSe.SubscriberEnquiryID;
                rp.EnquiryID = eSe.SubscriberEnquiryID;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (oException.Message.Contains("A .NET Framework"))
                {
                    rp.ResponseData = "Error: The System is unable to process this request. Please contact XDS Support";
                }

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }

            ds.Dispose();
            return rp;
        }

        public bool FnValidateIDNo(string input)
        {
            bool IsValid = true;

            try
            {
                double Product = 0, ProductSum = 0, Factor = 1;

                if (input.Trim() == string.Empty) return false;
                if (!FnIsNumeric(input)) return false;
                if (input.Trim().Length != 13) return false;
                if (int.Parse(input.Substring(0, 6)) == 0) return false;
                if (int.Parse(input.Substring(input.Length - 7)) == 0) return false;
                if (!FnIsDate("19" + input.Substring(0, 6)) && !FnIsDate("20" + input.Substring(0, 6))) return false;
                if (input.Trim().Length == 13 && int.Parse(input.Substring(10, 1)) > 2) return false;

                if (IsValid)
                {
                    char[] idchar = input.Substring(0, 12).ToArray();

                    foreach (char c in idchar)
                    {
                        Product = int.Parse(c.ToString()) * Factor;
                        ProductSum = ProductSum + (Product > 9 ? ((Product - 10.0) + 1.0) : Product);
                        Factor = Factor == 1 ? 2.0 : 1.0;
                    }

                    ProductSum = ProductSum + int.Parse(input.Substring(12, 1));

                    IsValid = (ProductSum / 10.0) - Math.Floor(ProductSum / 10.0) == 0.0 ? true : false;
                }
            }
            catch (Exception ex)
            {
                IsValid = false;
            }

            return IsValid;
        }

        public bool FnIsNumeric(string input)
        {
            bool isNumeric = false;

            int ivalue = 0;
            if (int.TryParse(input, NumberStyles.Any, new CultureInfo("en-US"), out ivalue))
                isNumeric = true;

            long lvalue = 0;
            if (long.TryParse(input, NumberStyles.Any, new CultureInfo("en-US"), out lvalue))
                isNumeric = true;

            Decimal dvalue = 0;
            if (Decimal.TryParse(input, NumberStyles.Any, new CultureInfo("en-US"), out dvalue))
                isNumeric = true;

            return isNumeric;
        }

        public bool FnIsDate(string input)
        {
            bool result = false;
            DateTime dtParse = new DateTime();
            if (DateTime.TryParseExact(input, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtParse))
            {
                if (dtParse.Year >= 1753) { result = true; } else { result = false; }
            }
            return result;
        }

    }
}