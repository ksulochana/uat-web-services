using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using XDSPortalLibrary;

namespace XDSPortalEnquiry.Business
{
    public class SubscriberVoucher
    {

        public SubscriberVoucher()
        {
        }


        public string ValidationStatus(SqlConnection AdminConnection, int intSubscriberID, string strVoucherCode)
        {
            string ValidationStatus = "1";
            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();
            Entity.SubscriberVoucher osv = dSV.GetSubscriberVoucher(AdminConnection, strVoucherCode);

            if (!string.IsNullOrEmpty(osv.VoucherCode))
            {
                if (osv.SubscriberID != intSubscriberID)
                {
                    ValidationStatus = "Voucher invalid for the subscriber";
                }

                else if (osv.VoucherType=="2" && osv.ExpiryDate < DateTime.Now)
                {
                    ValidationStatus = "Voucher Date Expired";
                }

                else if (osv.VoucherType=="1" && osv.Quantity == 0)
                {
                    ValidationStatus = "Voucher Enquiries Expired";
                }

                
            }
            else
            {
                ValidationStatus = "Invalid Voucher";
            }

            return ValidationStatus;
        }
    


    }
}
