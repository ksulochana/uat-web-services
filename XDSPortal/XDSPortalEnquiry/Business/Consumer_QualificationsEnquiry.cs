﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace XDSPortalEnquiry.Business
{
    public class ConsumerQualificationsEnquiry
    {
        private XDSPortalLibrary.Business_Layer.ConsumerTrace moConsumerTraceWseManager;
        private XDSPortalLibrary.Entity_Layer.ConsumerTrace eoConsumerTraceWseManager;

        private readonly Data.Subscriber dsub;
        private readonly Data.SubscriberEnquiry dSe;
        private readonly Entity.SubscriberEnquiryResult _eSC;
        Entity.SubscriberEnquiry _eSe;
        readonly Data.SubscriberEnquiryResult dSC;
        readonly Data.SubscriberEnquiryResultBonus dSB;
        Entity.SubscriberEnquiryResultBonus eSB;
        XDSPortalLibrary.Entity_Layer.Response rp;
        readonly Data.SubscriberVoucher dSV;

        readonly SqlConnection _enquirycon;
        readonly SqlConnection _adminConnection;
        readonly SqlConnection _dataConstring;

        readonly int _subscriberEnquiryID;
        readonly int _subscriberEnquiryResultID;
        readonly Entity.Subscriber _subscriber;
        readonly Entity.SystemUser _user;
        readonly DataSet _bonusDataSegment;

        readonly xdsBilling xb;
        readonly Entity.SubscriberProductReports spr;

        DataSet ds;

        public ConsumerQualificationsEnquiry(SqlConnection Enquirycon, SqlConnection AdminConnection, int SubscriberEnquiryID,
           int SubscriberEnquiryResultID, Entity.Subscriber Subscriber, Entity.SystemUser User, DataSet BonusDataSegment,
           bool BBonusChecking, Entity.SubscriberEnquiry eSe, Entity.SubscriberEnquiryResult eSC)
        {
            moConsumerTraceWseManager = new XDSPortalLibrary.Business_Layer.ConsumerTrace();
            eoConsumerTraceWseManager = new XDSPortalLibrary.Entity_Layer.ConsumerTrace();
            dSe = new Data.SubscriberEnquiry();
            dSC = new Data.SubscriberEnquiryResult();
            dSB = new Data.SubscriberEnquiryResultBonus();
            eSB = new Entity.SubscriberEnquiryResultBonus();
            rp = new XDSPortalLibrary.Entity_Layer.Response();
            dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();
            ds = new DataSet();
            xb = new xdsBilling();
            dsub = new Data.Subscriber();

            _enquirycon = Enquirycon;
            _adminConnection = AdminConnection;
            _subscriberEnquiryID = SubscriberEnquiryID;
            _subscriberEnquiryResultID = SubscriberEnquiryResultID;
            _subscriber = Subscriber;
            _user = User;
            _bonusDataSegment = BonusDataSegment;
            _eSe = eSe;
            _eSC = eSC;

            _dataConstring = new SqlConnection(_eSe.ExtraVarInput1);
            spr = xb.GetPrice(_adminConnection, _eSe.SubscriberID, _eSe.ProductID);

        }
        public XDSPortalLibrary.Entity_Layer.Response GetMultipleConsumerQualifications()
        {

            try
            {
                if (_enquirycon.State == ConnectionState.Closed)
                    _enquirycon.Open();
                if (_adminConnection.State == ConnectionState.Closed)
                    _adminConnection.Open();
            }
            catch (Exception)
            {
                //ignore the error
            }
            string rXml = "";
            double Totalcost = 0;
            try
            {
                if (spr.ReportID > 0)
                {
                    eoConsumerTraceWseManager.BonusSegments = null;
                    if (_bonusDataSegment != null && _bonusDataSegment.Tables.Count > 0 && _bonusDataSegment.Tables[0].Rows.Count > 0)
                    {
                        _bonusDataSegment.DataSetName = "BonusSegments";
                        _bonusDataSegment.Tables[0].TableName = "Segment";
                        eoConsumerTraceWseManager.BonusSegments = _bonusDataSegment;

                    }

                    // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML
                    // use this XML data for generating the Report

                    if (string.IsNullOrEmpty(_eSC.XMLData) || _eSC.XMLData == string.Empty)
                    {
                        if (!string.IsNullOrEmpty(_eSC.VoucherCode))
                        {
                            IsValidVoucher(_eSC.VoucherCode, _eSe.SubscriberID, _adminConnection);
                        }
                        else if (_subscriber.PayAsYouGo == 1)
                        {
                            //Calculate The Total cost of the Report , including Bonus Segments

                            Totalcost = spr.UnitPrice;
                            Totalcost = Totalcost + GetBonusSegmnetsCost();
                        }

                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((_subscriber.PayAsYouGo == 1 && _subscriber.PayAsyouGoEnquiryLimit >= (Totalcost)) || _subscriber.PayAsYouGo == 0)
                        {
                            System.IO.File.AppendAllText(@"C:\Log\Qualifications.txt", "before submnitrequest" + "\n");
                            rp = SubmitRequest();
                            System.IO.File.AppendAllText(@"C:\Log\Qualifications.txt", "aftersubmitrequest" + "\n");
                            rXml = rp.ResponseData;

                            System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                                LogNone();
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                                LogError();
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Bonus)
                                LogBonus(xmlSR);
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                                LogReport(xmlSR, Totalcost);
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                                LogMultiple(xmlSR);
                        }
                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.

                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                        }
                    }
                    else
                    {
                        // When User want to Re-Open a report

                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = _eSC.XMLData;
                        rp.EnquiryID = _eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = _eSC.SubscriberEnquiryResultID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                _enquirycon.Close();
                _adminConnection.Close();

                SqlConnection.ClearPool(_enquirycon);
                SqlConnection.ClearPool(_adminConnection);
            }
            catch (Exception oException)
            {
                _eSe.EnquiryResult = "E";
                _eSe.ErrorDescription = oException.Message;
                _eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(_enquirycon, _eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (_enquirycon.State == ConnectionState.Open)
                    _enquirycon.Close();
                if (_adminConnection.State == ConnectionState.Open)
                    _adminConnection.Close();

                SqlConnection.ClearPool(_enquirycon);
                SqlConnection.ClearPool(_adminConnection);
            }
            ds.Dispose();
            return rp;
        }

        private void IsValidVoucher(string voucherCode, int subscriberId, SqlConnection AdminConnection)
        {

            SubscriberVoucher sv = new SubscriberVoucher();
            string strValidationStatus = sv.ValidationStatus(AdminConnection, subscriberId, voucherCode);

            if (strValidationStatus == "")
            {
                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
            }
            else if (!(strValidationStatus == "1"))
            {
                throw new Exception(strValidationStatus);
            }

        }

        private double GetBonusSegmnetsCost()
        {
            double Totalcost = 0;
            if (eoConsumerTraceWseManager.BonusSegments != null)
            {

                foreach (DataRow dr in eoConsumerTraceWseManager.BonusSegments.Tables[0].Rows)
                {
                    if (dr["BonusViewed"].ToString().ToLower() == "true")
                    {
                        Totalcost = Totalcost + Convert.ToDouble(dr["BonusPrice"].ToString());
                    }
                }
            }

            return Totalcost;
        }

        private XDSPortalLibrary.Entity_Layer.Response SubmitRequest()
        {
            eoConsumerTraceWseManager.ConsumerID = _eSC.KeyID;
            eoConsumerTraceWseManager.ExternalReference = _eSe.SubscriberReference;
            eoConsumerTraceWseManager.ProductID = _eSe.ProductID;
            eoConsumerTraceWseManager.ReferenceNo = _eSC.Reference;
            eoConsumerTraceWseManager.subscriberID = _eSe.SubscriberID;
            eoConsumerTraceWseManager.TmpReference = _eSC.ExtraVarOutput1;
            moConsumerTraceWseManager.ConnectionString = _dataConstring;
            eoConsumerTraceWseManager.DataSegments = _eSe.ExtraVarInput2;
            eoConsumerTraceWseManager.SubscriberAssociationCode = _subscriber.SubscriberAssociationCode;
            eoConsumerTraceWseManager.AssociationTypeCode = _subscriber.AssociationTypeCode;
            eoConsumerTraceWseManager.SAFPSIndicator = _subscriber.SAFPSIndicator;
            eoConsumerTraceWseManager.AccountHolderName = _subscriber.SubscriberName;
            eoConsumerTraceWseManager.DisplayUnusableInformation = _subscriber.DisplayUnusableInformation;
            eoConsumerTraceWseManager.IDno = _eSC.IDNo;
            eoConsumerTraceWseManager.Surname = _eSC.Surname;

            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(_adminConnection.ConnectionString, _eSe.ExtraVarInput1);
            XDSPortalLibrary.Entity_Layer.Response rp = ra.GetQualificationsReport(eoConsumerTraceWseManager);

            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
            {
                _eSe = dSe.ErrorGetSubscriberEnquiryObject(_enquirycon, _eSe.SubscriberEnquiryID);
                ra = new XDSDataLibrary.ReportAccess(_adminConnection.ConnectionString, _eSe.ExtraVarInput1);
                rp = ra.GetQualificationsReport(eoConsumerTraceWseManager);
            }

            return rp;
        }

        private void LogNone()
        {
            _eSC.DetailsViewedYN = false;
            _eSC.Billable = false;
            _eSC.SubscriberEnquiryID = _subscriberEnquiryID;
            _eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
            _eSC.CreatedByUser = _eSe.CreatedByUser;
            _eSC.CreatedOnDate = DateTime.Now;

            dSC.UpdateSubscriberEnquiryResult(_enquirycon, _eSC);
            rp.EnquiryResultID = _eSC.SubscriberEnquiryResultID;
            rp.EnquiryID = _eSC.SubscriberEnquiryID;
        }

        private void LogError()
        {
            _eSC.EnquiryResult = "E";
            _eSe.ErrorDescription = rp.ResponseData.ToString();
            dSe.UpdateSubscriberEnquiryError(_enquirycon, _eSe);
        }

        private void LogBonus(System.IO.StringReader xmlSR)
        {
            string rXml = rp.ResponseData;
            ds.ReadXml(xmlSR);
            if (ds.Tables.Contains("Segments"))
            {

                _eSC.DetailsViewedYN = false;
                _eSC.Billable = false;
                _eSC.BonusIncluded = true;
                _eSC.KeyID = rp.ResponseKey;
                _eSC.KeyType = rp.ResponseKeyType;
                _eSC.XMLBonus = rXml.Replace("'", "''");
                _eSC.SubscriberEnquiryID = _subscriberEnquiryID;
                _eSC.CreatedByUser = _eSe.CreatedByUser;
                _eSC.CreatedOnDate = DateTime.Now;
                _eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.B.ToString();

                dSC.UpdateSubscriberEnquiryResult(_enquirycon, _eSC);
                rp.EnquiryID = _subscriberEnquiryID;
                rp.EnquiryResultID = _subscriberEnquiryResultID;

                foreach (DataRow r in ds.Tables["Segments"].Rows)
                {
                    eSB.SubscriberEnquiryResultID = _subscriberEnquiryResultID;
                    eSB.DataSegmentID = int.Parse(r["DataSegmentID"].ToString());
                    eSB.DataSegmentName = r["DataSegmentName"].ToString().Replace("'", "''");
                    eSB.DataSegmentDisplayText = r["DataSegmentDisplayText"].ToString().Replace("'", "''");
                    eSB.BonusViewed = bool.Parse(r["BonusViewed"].ToString());
                    eSB.Billable = false;
                    eSB.CreatedByUser = _eSe.CreatedByUser;
                    eSB.CreatedOnDate = DateTime.Now;

                    dSB.InsertSubscriberEnquiryResultBonus(_enquirycon, eSB);
                }
            }
        }

        private void LogReport(System.IO.StringReader xmlSR, double Totalcost)
        {
            string rXml = string.Empty;
            ds.ReadXml(xmlSR);
            if (ds.Tables.Contains("ConsumerDetail"))
            {
                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                Entity.Product eProduct = dProduct.GetProductRecord(_adminConnection, _eSe.ProductID);

                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                dtSubscriberInput.Columns.Add("EnquiryReason", typeof(String));

                DataRow drSubscriberInput;
                drSubscriberInput = dtSubscriberInput.NewRow();

                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                drSubscriberInput["SubscriberName"] = _subscriber.SubscriberName;
                drSubscriberInput["SubscriberUserName"] = _user.SystemUserFullName;
                drSubscriberInput["EnquiryInput"] = _eSe.SearchInput.Trim();
                drSubscriberInput["EnquiryReason"] = _eSe.EnquiryReason.Trim();

                dtSubscriberInput.Rows.Add(drSubscriberInput);

                ds.Tables.Add(dtSubscriberInput);

                if (_eSC.BonusIncluded == true)
                {
                    Data.SubscriberEnquiryResultBonus oSubscriberEnquiryResultBonus = new XDSPortalEnquiry.Data.SubscriberEnquiryResultBonus();
                    // DataSet dsBonus = oSubscriberEnquiryResultBonus.GetSelectedSubscriberEnquiryResultBonus(con, eSC.SubscriberEnquiryResultID);
                    DataSet dsBonus = eoConsumerTraceWseManager.BonusSegments;

                    DataTable dtBonusSelected = new DataTable("ConsumerBonusSelected");

                    dtBonusSelected.Columns.Add("DataSegmentID", typeof(String));
                    dtBonusSelected.Columns.Add("DataSegmentName", typeof(String));
                    dtBonusSelected.Columns.Add("DataSegmentDisplayText", typeof(String));

                    DataRow drBonusSelected;
                    if (dsBonus != null)
                    {
                        if (dsBonus.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow r in dsBonus.Tables[0].Rows)
                            {
                                drBonusSelected = dtBonusSelected.NewRow();
                                drBonusSelected["DataSegmentID"] = r["DataSegmentID"];
                                drBonusSelected["DataSegmentName"] = r["DataSegmentName"];
                                drBonusSelected["DataSegmentDisplayText"] = r["DataSegmentDisplayText"];
                                dtBonusSelected.Rows.Add(drBonusSelected);

                                eSB.SubscriberEnquiryResultBonusID = 0;
                                eSB.SubscriberEnquiryResultID = _subscriberEnquiryResultID;
                                eSB.DataSegmentID = Convert.ToInt32(r["DataSegmentID"]);
                                eSB.DataSegmentName = r["DataSegmentName"].ToString();
                                eSB.DataSegmentDisplayText = r["DataSegmentDisplayText"].ToString();
                                eSB.BonusViewed = Convert.ToBoolean(r["BonusViewed"].ToString());
                                if (eSB.BonusViewed)
                                {
                                    eSB.Billable = true;
                                }
                                else
                                {
                                    eSB.Billable = false;
                                }
                                eSB.ChangedByUser = eSB.CreatedByUser;
                                eSB.ChangedOnDate = DateTime.Now;
                                dSB.UpdateSubscriberEnquiryResultBonus(_enquirycon, eSB);
                            }
                            ds.Tables.Add(dtBonusSelected);
                        }
                        dsBonus.Dispose();
                    }
                }

                rXml = ds.GetXml();
                rp.ResponseData = rXml;

                foreach (DataRow r in ds.Tables["ConsumerDetail"].Rows)
                {
                    _eSC.DetailsViewedDate = DateTime.Now;
                    _eSC.DetailsViewedYN = true;
                    if (_subscriber.PayAsYouGo == 1 || !(string.IsNullOrEmpty(_eSC.VoucherCode)))
                    {
                        _eSC.Billable = false;
                    }
                    else
                    {
                        _eSC.Billable = true;
                    }
                    _eSC.ChangedByUser = _eSe.CreatedByUser;
                    _eSC.ChangedOnDate = DateTime.Now;
                    _eSC.SearchOutput = "";

                    if (r.Table.Columns.Contains("IDNo"))
                    {
                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                        {
                            _eSC.IDNo = r["IDNo"].ToString();
                            _eSC.SearchOutput = _eSC.SearchOutput + " | " + _eSC.IDNo;
                        }
                    }
                    if (r.Table.Columns.Contains("PassportNo"))
                    {
                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                        {
                            _eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                            _eSC.SearchOutput = _eSC.SearchOutput + " | " + _eSC.PassportNo;
                        }
                    }
                    if (r.Table.Columns.Contains("Surname"))
                    {
                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                        {
                            _eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                            _eSC.SearchOutput = _eSC.SearchOutput + " | " + _eSC.Surname;
                        }
                    }

                    if (r.Table.Columns.Contains("FirstName"))
                    {
                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                        {
                            _eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                            _eSC.SearchOutput = _eSC.SearchOutput + " | " + _eSC.FirstName;
                        }
                    }

                    if (_eSC.SearchOutput.Length > 0)
                    {
                        _eSC.SearchOutput = _eSC.SearchOutput.Substring(3, _eSC.SearchOutput.Length - 3).ToUpper();
                    }

                    _eSC.KeyID = rp.ResponseKey;
                    _eSC.KeyType = rp.ResponseKeyType;
                    _eSC.SubscriberEnquiryID = _subscriberEnquiryID;

                    _eSC.BillingTypeID = spr.BillingTypeID;
                    _eSC.BillingPrice = spr.UnitPrice;

                    _eSC.XMLData = rXml.Replace("'", "''");
                    _eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                    _eSC.ProductID = _eSe.ProductID;

                    if ((ds.Tables.Contains("ConsumerNLRDebtSummary") && ds.Tables["ConsumerNLRDebtSummary"].Rows.Count > 0) || (ds.Tables.Contains("ConsumerCPANLRDebtSummary") && ds.Tables["ConsumerCPANLRDebtSummary"].Rows.Count > 0) || (ds.Tables.Contains("ConsumerNLRAccountStatus") && ds.Tables["ConsumerNLRAccountStatus"].Rows.Count > 0) || (ds.Tables.Contains("ConsumerNLR24MonthlyPayment") && ds.Tables["ConsumerNLR24MonthlyPayment"].Rows.Count > 0))
                        _eSC.NLRDataIncluded = true;

                    dSC.UpdateSubscriberEnquiryResult(_enquirycon, _eSC);
                    rp.EnquiryResultID = _eSC.SubscriberEnquiryResultID;
                    rp.EnquiryID = _eSC.SubscriberEnquiryID;

                    // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                    if (_subscriber.PayAsYouGo == 1)
                    {
                        dsub.UpdatePayAsYouGoEnquiryLimit(_adminConnection, _eSe.SubscriberID, Totalcost);
                    }
                    if (!(string.IsNullOrEmpty(_eSC.VoucherCode)))
                    {
                        dSV.UpdateSubscriberVoucher(_adminConnection, _eSC.VoucherCode, _eSe.CreatedByUser);
                    }
                }
            }
        }

        private void LogMultiple(System.IO.StringReader xmlSR)
        {
            ds.ReadXml(xmlSR);
            if (ds.Tables.Contains("ConsumerDetails"))
            {
                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                {

                    _eSC.SearchOutput = "";

                    if (r.Table.Columns.Contains("IDNo"))
                    {
                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                        {
                            _eSC.IDNo = r["IDNo"].ToString();
                            _eSC.SearchOutput = _eSC.SearchOutput + " | " + _eSC.IDNo;
                        }
                    }
                    if (r.Table.Columns.Contains("PassportNo"))
                    {
                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                        {
                            _eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                            _eSC.SearchOutput = _eSC.SearchOutput + " | " + _eSC.PassportNo;
                        }
                    }
                    if (r.Table.Columns.Contains("Surname"))
                    {
                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                        {
                            _eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                            _eSC.SearchOutput = _eSC.SearchOutput + " | " + _eSC.Surname;
                        }
                    }

                    if (r.Table.Columns.Contains("FirstName"))
                    {
                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                        {
                            _eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                            _eSC.SearchOutput = _eSC.SearchOutput + " | " + _eSC.FirstName;
                        }
                    }

                    if (_eSC.SearchOutput.Length > 0)
                    {
                        _eSC.SearchOutput = _eSC.SearchOutput.Substring(3, _eSC.SearchOutput.Length - 3).ToUpper();
                    }

                    _eSC.DetailsViewedYN = false;
                    _eSC.Billable = false;
                    _eSC.ChangedByUser = _eSe.CreatedByUser;
                    _eSC.ChangedOnDate = DateTime.Now;
                    _eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                    _eSC.KeyType = rp.ResponseKeyType;

                    _eSC.BillingTypeID = spr.BillingTypeID;
                    _eSC.BillingPrice = spr.UnitPrice;

                    _eSC.SubscriberEnquiryID = _subscriberEnquiryID;
                    _eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();

                    dSC.UpdateSubscriberEnquiryResult(_enquirycon, _eSC);
                    rp.EnquiryResultID = _eSC.SubscriberEnquiryResultID;
                    rp.EnquiryID = _eSC.SubscriberEnquiryID;

                }
            }
        }
    }
}

