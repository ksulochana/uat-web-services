﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XDSPortalEnquiry.Entity;

namespace XDSPortalEnquiry.Business
{
    public class Batch_BatchReport
    {
        public BulkReportInfo Getdata(string Reference, string DataServiceURI, int SkipToken)
        {
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            BulkReportInfo oInfo = new BulkReportInfo();
            oInfo.Error = string.Empty;
            try
            {
                //int ReferenceNo = 0;
                //int SkipToken = 0;
                //Message message = Message.CreateMessage(MessageVersion.Soap11, "GetBatchReportData", new CustomBodyWriter(GetBulkReportDataImpl(0)));
                //return message;

                BulkReport oB = new BulkReport();

                WCFDataService.BulkReportEntities oEntities = new WCFDataService.BulkReportEntities(new Uri(DataServiceURI));


                var Data = oEntities.CreateQuery<WCFDataService.Serviceoutput>("GetReportData")
                    .AddQueryOption("Reference", "'"+Reference+"'")
                    .Skip(SkipToken).ToList();


                //   var Data = a.Execute<object>(new Uri(queryString, UriKind.Relative));

                oInfo.BulkReportInfoData = new BulkReport[Data.Count()];

                int i = 0;

                foreach (WCFDataService.Serviceoutput oData in Data)
                {
                    oB = new BulkReport();
                    oB.SubscriberReference = oData.SubscriberReference;
                    oB.FirstName = oData.FirstName;
                    oB.Surname = oData.Surname;
                    oB.IDno = oData.IDNo;
                    oB.PassportNo = oData.PassportNo;
                    oB.EnquiryStatusInd = oData.EnquiryStatusInd;
                    oB.EnquiryResultInd = oData.EnquiryResultInd;
                    oB.XMLData = oData.XMLData;
                    oB.ErrorDescription = oData.ErrorDescription;

                    oInfo.BulkReportInfoData[i] = oB;

                    i++;
                }


                //return string.Format("You entered: {0}", ReferenceNo);
            }
            catch (Exception ex)
            {
                oInfo.Error = ex.Message + ex.InnerException;

            }


            return oInfo;

            
        }
    }
}
