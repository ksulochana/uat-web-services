﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace XDSPortalEnquiry.Business
{
    public class HollardCampaign
    {
        public XDSPortalLibrary.Entity_Layer.Response GetCampaignCount(SqlConnection AdminCon, string Command, string Username)
        {
            int refNo = 0;
            SqlConnection InclineCon = null;
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                int campaignResult = 0;

                string strConnection = GetConnection(AdminCon);
                string strSQL = Command;//.Replace("'", "''");

                InclineCon = new SqlConnection(strConnection);
                SqlCommand objcmd = new SqlCommand(strSQL, InclineCon);
                objcmd.CommandType = CommandType.Text;
                objcmd.CommandTimeout = 0;

                if (InclineCon.State == ConnectionState.Closed)
                    InclineCon.Open();

                SqlDataReader oReader = objcmd.ExecuteReader();

                while (oReader.Read())
                    campaignResult = oReader.GetInt32(0);

                oReader.Close();
                oReader.Dispose();

                if (InclineCon.State == ConnectionState.Open)
                    InclineCon.Close();

                SqlConnection.ClearPool(InclineCon);

                if (!string.IsNullOrEmpty(campaignResult.ToString()))
                    refNo = LogCampaign(InclineCon, Command, Username, campaignResult);

                ObjResponse.ResponseKey = refNo;
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = campaignResult.ToString();
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;

                if (InclineCon.State == ConnectionState.Open)
                    InclineCon.Close();

                SqlConnection.ClearPool(AdminCon);
            }

            return ObjResponse;
        }

        public int LogCampaign(SqlConnection InclineCon, string CampaignCommand, string CampaignUser, int CampaignCount)
        {
            int refNo = 0;

            try
            {
                CampaignCommand = CampaignCommand.Replace("'", "''");

                string strSQL = string.Format("EXEC LogCampaign '{0}',{1},'{2}'", CampaignCommand, CampaignCount, CampaignUser);

                SqlCommand objcmd = new SqlCommand(strSQL, InclineCon);
                objcmd.CommandType = CommandType.Text;
                objcmd.CommandTimeout = 0;

                if (InclineCon.State == ConnectionState.Closed)
                    InclineCon.Open();

                objcmd.ExecuteNonQuery();

                strSQL = "SELECT @@IDENTITY";
                SqlCommand Imycommand = new SqlCommand(strSQL, InclineCon);
                SqlDataReader oLogReader = Imycommand.ExecuteReader();
                oLogReader.Read();
                refNo = Convert.ToInt32(oLogReader.GetValue(0));
                oLogReader.Close();

                if (InclineCon.State == ConnectionState.Open)
                    InclineCon.Close();

                SqlConnection.ClearPool(InclineCon);

                return refNo;
            }
            catch (Exception ex)
            {
                if (InclineCon.State == ConnectionState.Open)
                    InclineCon.Close();
                SqlConnection.ClearPool(InclineCon);

                return refNo;
            }
        }


        public string GetConnection(SqlConnection AdminCon)
        {
            string strConnection = string.Empty;

            try
            {
                string strSQL = "Select ConnectionString From InclineConnection nolock where Active = 1";

                SqlCommand objcmd = new SqlCommand(strSQL, AdminCon);
                objcmd.CommandType = CommandType.Text;
                objcmd.CommandTimeout = 0;

                if (AdminCon.State == ConnectionState.Closed)
                    AdminCon.Open();

                SqlDataReader oReader = objcmd.ExecuteReader();

                while (oReader.Read())
                    strConnection = oReader.GetString(0);

                oReader.Close();
                oReader.Dispose();

                if (AdminCon.State == ConnectionState.Open)
                    AdminCon.Close();

                SqlConnection.ClearPool(AdminCon);
            }
            catch (Exception ex)
            {
                strConnection = string.Empty;

                if (AdminCon.State == ConnectionState.Open)
                    AdminCon.Close();
                SqlConnection.ClearPool(AdminCon);
            }

            return strConnection;
        }

    }
}
