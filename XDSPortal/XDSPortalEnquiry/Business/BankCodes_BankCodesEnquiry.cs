﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;


namespace XDSPortalEnquiry.Business
{
    public class BankCodes_BankCodesEnquiry
    {
        XDSPortalLibrary.Business_Layer.BankCodeEnquiry moBankCodeWseManager;
        XDSPortalLibrary.Entity_Layer.BankCodes eoBankCodeWseManager;

        public BankCodes_BankCodesEnquiry()
        {
            moBankCodeWseManager = new XDSPortalLibrary.Business_Layer.BankCodeEnquiry();
            eoBankCodeWseManager = new XDSPortalLibrary.Entity_Layer.BankCodes();

        }
        public void ValidateInput(Entity.SubscriberEnquiryLog Validate)
        {
            if (string.IsNullOrEmpty(Validate.ReportType) || string.IsNullOrEmpty(Validate.BankName) || string.IsNullOrEmpty(Validate.BranchName) || string.IsNullOrEmpty(Validate.BranchCode) || string.IsNullOrEmpty(Validate.AccountNo) || string.IsNullOrEmpty(Validate.AccHolder) || Validate.Amount <= 0 || string.IsNullOrEmpty(Validate.Terms) || string.IsNullOrEmpty(Validate.EntityType))
            {
                throw new Exception("Please enter data to all mandatory fields");
            }

            else if (!string.IsNullOrEmpty(Validate.EntityType))
            {
                if (Validate.EntityType.ToLower().Contains("company") && Validate.RegNumber == "//")
                {
                    throw new Exception("Registration no is mandatory for the selected entity");
                }
                else if (Validate.EntityType.ToLower().Contains("sole") && string.IsNullOrEmpty(Validate.IDNo))
                {
                    throw new Exception("ID no is mandatory for the selected entity");
                }
                else if (Validate.EntityType.ToLower().Contains("trust") && string.IsNullOrEmpty(Validate.Trust))
                {
                    throw new Exception("Trust no is mandatory for the selected entity");
                }
                else if (Validate.EntityType.ToLower().Contains("individual") && string.IsNullOrEmpty(Validate.IDNo))
                {
                    throw new Exception("IDno is mandatory for the selected entity");
                }
                else if (string.IsNullOrEmpty(Validate.FirstName) || (string.IsNullOrEmpty(Validate.SurName)) || (string.IsNullOrEmpty(Validate.CompanyName)) || (string.IsNullOrEmpty(Validate.ContactNo)) || (string.IsNullOrEmpty(Validate.EmailAddress)))
                {
                    throw new Exception("FirstName, SurName, Company Name, Conatct No, Email Address are Mandatory");
                }
            }
            else if (string.IsNullOrEmpty(Validate.IDNo) || (Validate.RegNumber == "//") || (string.IsNullOrEmpty(Validate.Trust)))
            {
                throw new Exception("Either of IDNo and Registration number and Trust No are mandatory depending on the entity type selected.");
            }
            else if (string.IsNullOrEmpty(Validate.BusBusinessName) || (!(Validate.BusBusinessName.ToLower().Contains("gauteng") || Validate.BusBusinessName.ToLower().Contains("kzn") || Validate.BusBusinessName.ToLower().Contains("w cape"))))
            {
                throw new Exception("Valid values for Region are Gauteng, KZN and W Cape");
            }
        }

        public void NewValidateInput(Entity.SubscriberEnquiryLog Validate)
        {
            if (string.IsNullOrEmpty(Validate.ReportType) || string.IsNullOrEmpty(Validate.BankName) || string.IsNullOrEmpty(Validate.BranchName) || string.IsNullOrEmpty(Validate.BranchCode) || string.IsNullOrEmpty(Validate.AccountNo) || string.IsNullOrEmpty(Validate.AccHolder) || Validate.Amount <= 0 || string.IsNullOrEmpty(Validate.Terms) || string.IsNullOrEmpty(Validate.EntityType) || string.IsNullOrEmpty(Validate.VerificationType))
            {
                throw new Exception("Please enter data to all mandatory fields");
            }

            else if (!string.IsNullOrEmpty(Validate.EntityType))
            {
               if (Validate.VerificationType.ToLower().Contains("company") && !Validate.EntityType.ToLower().Contains("registered") && !Validate.EntityType.ToLower().Contains("trust") && !Validate.EntityType.ToLower().Contains("none"))
                {
                    throw new Exception("Verification type does not correspond with Entity Type");
                }
                else if (Validate.VerificationType.ToLower().Contains("south") && !Validate.EntityType.ToLower().Contains("sole") && !Validate.EntityType.ToLower().Contains("none"))
                {
                    throw new Exception("Verification type does not correspond with Entity Type");
                }
                else if (Validate.VerificationType.ToLower().Contains("foreign") && !Validate.EntityType.ToLower().Contains("sole") && !Validate.EntityType.ToLower().Contains("none"))
                {
                    throw new Exception("Verification type does not correspond with Entity Type");
                }
                else if (Validate.VerificationType.ToLower().Contains("south") && Validate.EntityType.ToLower().Contains("sole") && string.IsNullOrEmpty(Validate.IDNo))
                {
                    throw new Exception("ID no is mandatory for the selected entity and selected verification type");
                }
                else if (Validate.VerificationType.ToLower().Contains("foreign") && Validate.EntityType.ToLower().Contains("sole") && string.IsNullOrEmpty(Validate.PassportNo) || Validate.PassportNo.Length > 20)
                {
                    throw new Exception("Passport no is mandatory for the selected entity and selected verification type or Passport has more then 20 Characters");
                }
                else if (Validate.VerificationType.ToLower().Contains("company") && Validate.EntityType.ToLower().Contains("trust") && string.IsNullOrEmpty(Validate.Trust))
                {
                    throw new Exception("Trust no is mandatory for the selected entity");
                }
                else if (!System.Text.RegularExpressions.Regex.IsMatch(Validate.InternationalBusRegistrationNumber, "^[a-zA-Z0-9]+$") && !string.IsNullOrEmpty(Validate.InternationalBusRegistrationNumber))
                {
                    throw new Exception("InternationalBusRegistrationNumber Should Not Contain Special Characters");
                }
                
                else if (string.IsNullOrEmpty(Validate.FirstName) || (string.IsNullOrEmpty(Validate.SurName)) || (string.IsNullOrEmpty(Validate.EmailAddress)))
                {
                    throw new Exception("FirstName, SurName, Email Address are Mandatory");
                }
            }
            else if (string.IsNullOrEmpty(Validate.IDNo) || string.IsNullOrEmpty(Validate.PassportNo) || (string.IsNullOrEmpty(Validate.Trust)))
            {
                throw new Exception("Either of IDNo, Passport No and Trust No are mandatory depending on the entity type selected.");
            }
            else if (string.IsNullOrEmpty(Validate.BusBusinessName) || (!(Validate.BusBusinessName.ToLower().Contains("gauteng") || Validate.BusBusinessName.ToLower().Contains("kzn") || Validate.BusBusinessName.ToLower().Contains("w cape"))))
            {
                throw new Exception("Valid values for Region are Gauteng, KZN and W Cape");
            }
        }
        public XDSPortalLibrary.Entity_Layer.Response SubmitBankCodeEnquiry(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string strBankCodereqType, string verificationtype, string strEntityType, string strIDno, string strRegNo1, string strRegNo2, string strRegNo3, string TrustNo, string strBankName, string strBranchName, string strBranchCode, string strAccNo, string strAccholder, double dAmount, string strTerms, string strAdditionalInfo, string strSurname, string strFirstName, string strCOmpanyName, string strContactNo, string strEmailAddress, string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode)
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            int intSubscriberEnquiryLogID = 0;

            int MatchResult = 0;
            DataSet ds = new DataSet();
            string strValidationStatus = "";
            Boolean boolSubmitEnquiry = true;


            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            Entity.SubscriberEnquiryLog sel = new XDSPortalEnquiry.Entity.SubscriberEnquiryLog();
            Data.SubscriberEnquiryLog dsel = new XDSPortalEnquiry.Data.SubscriberEnquiryLog();


            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);


            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();


            try
            {


                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {

                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {
                        strEntityType = verificationtype.ToLower() == "individual" ? "Individual" : ((verificationtype.ToLower() == "company" && strEntityType.ToLower() == "none") ? "" : strEntityType);

                        sel.ReportType = strBankCodereqType;
                        sel.BankName = strBankName;
                        sel.BranchName = strBranchName;
                        sel.BranchCode = strBranchCode;
                        sel.AccountNo = strAccNo;
                        sel.AccHolder = strAccholder;
                        sel.Amount = dAmount;
                        sel.Terms = strTerms;
                        sel.ExtraVarInput1 = strAdditionalInfo;
                        sel.FirstName = strFirstName;
                        sel.SurName = strSurname;
                        sel.CompanyName = strCOmpanyName;
                        sel.ContactNo = strContactNo;
                        sel.EmailAddress = strEmailAddress;
                        sel.VoucherCode = strVoucherCode;
                        sel.SubscriberReference = strExtRef;
                        sel.SubscriberID = intSubscriberID;
                        sel.SubscriberName = strSubscriberName;
                        sel.SystemUserID = intSystemUserID;
                        sel.SystemUserName = sys.SystemUserFullName;
                        sel.CreatedByUser = sys.Username;
                        sel.CreatedOnDate = DateTime.Now;
                        sel.SubscriberEnquiryDate = DateTime.Now;
                        sel.EnquiryStatus = XDSPortalEnquiry.Entity.SubscriberEnquiryLog.EnquiryStatusInd.P.ToString();
                        sel.BillingTypeID = spr.BillingTypeID;
                        sel.KeyType = "F";
                        sel.EntityType = strEntityType;
                        sel.RegNumber = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        sel.BusRegistrationNo = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        sel.Trust = TrustNo;
                        sel.IDNo = strIDno;





                        ValidateInput(sel);

                        //Generate serach input string

                        if (!string.IsNullOrEmpty(sel.BankName))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.BankName;
                        }
                        if (!string.IsNullOrEmpty(sel.BranchName))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.BranchName;
                        }
                        if (!string.IsNullOrEmpty(sel.BranchCode))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.BranchCode;
                        }
                        if (!string.IsNullOrEmpty(sel.AccountNo))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.AccountNo;
                        }
                        if (!string.IsNullOrEmpty(sel.AccHolder))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.AccHolder;
                        }

                        if (sel.Searchinput.Length > 0)
                        {
                            sel.Searchinput = sel.Searchinput.Substring(3, sel.Searchinput.Length - 3).ToUpper();
                        }


                        sel.KeyType = "F";
                        sel.ProductID = intProductId;
                        sel.BillingPrice = spr.UnitPrice;

                        if (sub.PayAsYouGo == 1 || !(strVoucherCode == string.Empty))
                        {
                            sel.Billable = false;
                        }
                        else
                        {
                            sel.Billable = true;
                        }

                        intSubscriberEnquiryLogID = dsel.InsertSubscriberEnquiryLog(con, sel);



                        if (sub.PayAsYouGo == 1)
                        {
                            dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                        }
                        if (!(strVoucherCode == string.Empty))
                        {
                            dSV.UpdateSubscriberVoucher(AdminConnection, strVoucherCode, sys.Username);
                        }

                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = "Enquiry Submitted successfully";
                        rp.EnquiryLogID = intSubscriberEnquiryLogID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }


                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }



            catch (Exception oException)
            {
                sel.EnquiryResult = "E";
                sel.ErrorDescription = oException.Message;
                sel.SubscriberEnquiryLogID = dsel.UpdateSubscriberEnquiryLogError(con, sel);
                MatchResult = sel.SubscriberEnquiryLogID;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                MatchResult = sel.SubscriberEnquiryLogID;
                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;
            }



            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitBankCodeEnquiry(SqlConnection con, SqlConnection AdminConnection, SqlConnection BCConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string strBankCodereqType, string verificationtype, string strEntityType, string strIDno, string strRegNo1, string strRegNo2, string strRegNo3, string TrustNo, string strBankName, string strBranchName, string strBranchCode, string strAccNo, string strAccholder, double dAmount, string strTerms, string strRegion, string strCountryCode, string strAdditionalInfo, string strSurname, string strFirstName, string strCOmpanyName, string strContactNo, string strEmailAddress, string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode)
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            int intSubscriberEnquiryLogID = 0;

            int MatchResult = 0;
            DataSet ds = new DataSet();
            string strValidationStatus = "";
            Boolean boolSubmitEnquiry = true;
            string rXml = "";


            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            Entity.SubscriberEnquiryLog sel = new XDSPortalEnquiry.Entity.SubscriberEnquiryLog();
            Data.SubscriberEnquiryLog dsel = new XDSPortalEnquiry.Data.SubscriberEnquiryLog();


            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);


            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();


            try
            {


                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {

                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {
                        strEntityType = verificationtype.ToLower() == "individual" ? "Individual" : ((verificationtype.ToLower() == "company" && strEntityType.ToLower() == "none") ? "" : strEntityType);

                        sel.ReportType = strBankCodereqType;
                        sel.BankName = strBankName;
                        sel.BranchName = strBranchName;
                        sel.BranchCode = strBranchCode;
                        sel.AccountNo = strAccNo;
                        sel.AccHolder = strAccholder;
                        sel.Amount = dAmount;
                        sel.Terms = strTerms;
                        sel.ExtraVarInput1 = strAdditionalInfo;
                        sel.FirstName = strFirstName;
                        sel.SurName = strSurname;
                        sel.CompanyName = strCOmpanyName;
                        sel.ContactNo = strContactNo;
                        sel.EmailAddress = strEmailAddress;
                        sel.VoucherCode = strVoucherCode;
                        sel.SubscriberReference = strExtRef;
                        sel.SubscriberID = intSubscriberID;
                        sel.SubscriberName = strSubscriberName;
                        sel.SystemUserID = intSystemUserID;
                        sel.SystemUserName = sys.SystemUserFullName;
                        sel.CreatedByUser = sys.Username;
                        sel.CreatedOnDate = DateTime.Now;
                        sel.SubscriberEnquiryDate = DateTime.Now;
                        sel.EnquiryStatus = XDSPortalEnquiry.Entity.SubscriberEnquiryLog.EnquiryStatusInd.P.ToString();
                        sel.BillingTypeID = spr.BillingTypeID;
                        sel.KeyType = "F";
                        sel.EntityType = strEntityType;
                        //sel.RegNumber = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        sel.Trust = TrustNo;
                        sel.IDNo = strIDno;
                        sel.BusBusinessName = strRegion;
                        sel.BusRegistrationNo = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;

                        //Generate serach input string

                        if (!string.IsNullOrEmpty(sel.BankName))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.BankName;
                        }
                        if (!string.IsNullOrEmpty(sel.BranchName))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.BranchName;
                        }
                        if (!string.IsNullOrEmpty(sel.BranchCode))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.BranchCode;
                        }
                        if (!string.IsNullOrEmpty(sel.AccountNo))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.AccountNo;
                        }
                        if (!string.IsNullOrEmpty(sel.AccHolder))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.AccHolder;
                        }

                        if (sel.Searchinput.Length > 0)
                        {
                            sel.Searchinput = sel.Searchinput.Substring(3, sel.Searchinput.Length - 3).ToUpper();
                        }


                        sel.KeyType = "F";
                        sel.ProductID = intProductId;
                        sel.BillingPrice = spr.UnitPrice;

                        if (sub.PayAsYouGo == 1 || !(strVoucherCode == string.Empty))
                        {
                            sel.Billable = false;
                        }
                        else
                        {
                            sel.Billable = true;
                        }

                        intSubscriberEnquiryLogID = dsel.InsertSubscriberEnquiryLog(con, sel);
                        sel.SubscriberEnquiryLogID = intSubscriberEnquiryLogID;

                        ValidateInput(sel);

                        moBankCodeWseManager.ConnectionString = BCConnection;
                        eoBankCodeWseManager.RequestType = strBankCodereqType;
                        eoBankCodeWseManager.RequesterFirstName = strFirstName + " " + strSurname;
                        eoBankCodeWseManager.BankName = strBankName;
                        eoBankCodeWseManager.BranchName = strBranchName;
                        eoBankCodeWseManager.BranchCode = strBranchCode;
                        eoBankCodeWseManager.AccNo = strAccNo;
                        eoBankCodeWseManager.AccHolder = strAccholder;
                        eoBankCodeWseManager.Amount = dAmount;
                        eoBankCodeWseManager.Terms = strTerms;
                        eoBankCodeWseManager.Region = strRegion;
                        eoBankCodeWseManager.CountryCode = strCountryCode;
                        eoBankCodeWseManager.RegistrationNo = strRegNo1 + strRegNo2 + strRegNo3;

                        rp = moBankCodeWseManager.search(eoBankCodeWseManager);

                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                        sel.KeyID = rp.ResponseKey;
                        sel.KeyType = rp.ResponseKeyType;
                        sel.ExtraVarInput1 = rp.TmpReference;
                        sel.IsAHVRequest = true;
                        rp.EnquiryLogID = intSubscriberEnquiryLogID;

                        dsel.UpdateSubscriberEnquiryLog(con, sel);

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            sel.EnquiryResult = "E";
                            sel.ErrorDescription = rp.ResponseData.ToString();
                            dsel.UpdateSubscriberEnquiryLogError(con, sel);
                        }
                        else
                        {

                            if (sub.PayAsYouGo == 1)
                            {
                                dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                            }
                            if (!(strVoucherCode == string.Empty))
                            {
                                dSV.UpdateSubscriberVoucher(AdminConnection, strVoucherCode, sys.Username);
                            }
                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                            rp.ResponseData = "Enquiry Submitted successfully";
                        }


                        rp.EnquiryLogID = intSubscriberEnquiryLogID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }


                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }



            catch (Exception oException)
            {
                sel.EnquiryResult = "E";
                sel.ErrorDescription = oException.Message;
                sel.SubscriberEnquiryLogID = dsel.UpdateSubscriberEnquiryLogError(con, sel);
                MatchResult = sel.SubscriberEnquiryLogID;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                MatchResult = sel.SubscriberEnquiryLogID;
                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;
            }



            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitNewBankCodeEnquiry(SqlConnection con, SqlConnection AdminConnection, SqlConnection BCConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string strBankCodereqType, string verificationtype, string strEntityType, string strIDno, string strPassportNo, string strRegNo1, string strRegNo2, string strRegNo3, string strInternationalBusRegistrationNumber, string TrustNo, string strBankName, string strBranchName, string strBranchCode, string strAccNo, string strAccholder, double dAmount, string strTerms, string strRegion, string strCountryCode, string strAdditionalInfo, string strSurname, string strFirstName, string strEmailAddress, string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode)
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            int intSubscriberEnquiryLogID = 0;

            int MatchResult = 0;
            DataSet ds = new DataSet();
            string strValidationStatus = "";
            Boolean boolSubmitEnquiry = true;
            string rXml = "";


            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            Entity.SubscriberEnquiryLog sel = new XDSPortalEnquiry.Entity.SubscriberEnquiryLog();
            Data.SubscriberEnquiryLog dsel = new XDSPortalEnquiry.Data.SubscriberEnquiryLog();


            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);


            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();


            try
            {


                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {

                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {
                       // strEntityType = verificationtype.ToLower() == "individual" ? "Individual" : ((verificationtype.ToLower() == "company" && strEntityType.ToLower() == "none") ? "" : strEntityType);

                        sel.ReportType = strBankCodereqType;
                        sel.BankName = strBankName;
                        sel.BranchName = strBranchName;
                        sel.BranchCode = strBranchCode;
                        sel.AccountNo = strAccNo;
                        sel.AccHolder = strAccholder;
                        sel.Amount = dAmount;
                        sel.Terms = strTerms;
                        sel.ExtraVarInput1 = strAdditionalInfo;
                        sel.FirstName = strFirstName;
                        sel.SurName = strSurname;
                       // sel.CompanyName = strCOmpanyName;
                       // sel.ContactNo = strContactNo;
                        sel.EmailAddress = strEmailAddress;
                        sel.VoucherCode = strVoucherCode;
                        sel.SubscriberReference = strExtRef;
                        sel.SubscriberID = intSubscriberID;
                        sel.SubscriberName = strSubscriberName;
                        sel.SystemUserID = intSystemUserID;
                        sel.SystemUserName = sys.SystemUserFullName;
                        sel.CreatedByUser = sys.Username;
                        sel.CreatedOnDate = DateTime.Now;
                        sel.SubscriberEnquiryDate = DateTime.Now;
                        sel.EnquiryStatus = XDSPortalEnquiry.Entity.SubscriberEnquiryLog.EnquiryStatusInd.P.ToString();
                        sel.BillingTypeID = spr.BillingTypeID;
                        sel.KeyType = "F";
                        sel.EntityType = strEntityType;
                        //sel.RegNumber = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        sel.Trust = TrustNo;
                        sel.IDNo = strIDno;
                        sel.PassportNo = strPassportNo;
                        sel.BusBusinessName = strRegion;
                        sel.BusRegistrationNo = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        sel.InternationalBusRegistrationNumber = strInternationalBusRegistrationNumber;
                        sel.VerificationType = verificationtype;

                        //Generate serach input string

                        if (!string.IsNullOrEmpty(sel.BankName))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.BankName;
                        }
                        if (!string.IsNullOrEmpty(sel.BranchName))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.BranchName;
                        }
                        if (!string.IsNullOrEmpty(sel.BranchCode))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.BranchCode;
                        }
                        if (!string.IsNullOrEmpty(sel.AccountNo))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.AccountNo;
                        }
                        if (!string.IsNullOrEmpty(sel.AccHolder))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.AccHolder;
                        }

                        if (sel.Searchinput.Length > 0)
                        {
                            sel.Searchinput = sel.Searchinput.Substring(3, sel.Searchinput.Length - 3).ToUpper();
                        }


                        sel.KeyType = "F";
                        sel.ProductID = intProductId;
                        sel.BillingPrice = spr.UnitPrice;

                        if (sub.PayAsYouGo == 1 || !(strVoucherCode == string.Empty))
                        {
                            sel.Billable = false;
                        }
                        else
                        {
                            sel.Billable = true;
                        }

                        intSubscriberEnquiryLogID = dsel.InsertSubscriberEnquiryLog(con, sel);
                        sel.SubscriberEnquiryLogID = intSubscriberEnquiryLogID;

                        NewValidateInput(sel);

                        moBankCodeWseManager.ConnectionString = BCConnection;
                        eoBankCodeWseManager.RequestType = strBankCodereqType;
                        eoBankCodeWseManager.RequesterFirstName = strFirstName + " " + strSurname;
                        eoBankCodeWseManager.BankName = strBankName;
                        eoBankCodeWseManager.BranchName = strBranchName;
                        eoBankCodeWseManager.BranchCode = strBranchCode;
                        eoBankCodeWseManager.AccNo = strAccNo;
                        eoBankCodeWseManager.AccHolder = strAccholder;
                        eoBankCodeWseManager.Amount = dAmount;
                        eoBankCodeWseManager.Terms = strTerms;
                        eoBankCodeWseManager.Region = strRegion;
                        eoBankCodeWseManager.CountryCode = strCountryCode;
                        eoBankCodeWseManager.RegistrationNo = strRegNo1 + strRegNo2 + strRegNo3;

                        rp = moBankCodeWseManager.search(eoBankCodeWseManager);

                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                        sel.KeyID = rp.ResponseKey;
                        sel.KeyType = rp.ResponseKeyType;
                        sel.ExtraVarInput1 = rp.TmpReference;
                        sel.IsAHVRequest = true;
                        rp.EnquiryLogID = intSubscriberEnquiryLogID;

                        dsel.UpdateSubscriberEnquiryLog(con, sel);

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            sel.EnquiryResult = "E";
                            sel.ErrorDescription = rp.ResponseData.ToString();
                            dsel.UpdateSubscriberEnquiryLogError(con, sel);
                        }
                        else
                        {

                            if (sub.PayAsYouGo == 1)
                            {
                                dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                            }
                            if (!(strVoucherCode == string.Empty))
                            {
                                dSV.UpdateSubscriberVoucher(AdminConnection, strVoucherCode, sys.Username);
                            }
                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                            rp.ResponseData = "Enquiry Submitted successfully";
                        }


                        rp.EnquiryLogID = intSubscriberEnquiryLogID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }


                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }



            catch (Exception oException)
            {
                sel.EnquiryResult = "E";
                sel.ErrorDescription = oException.Message;
                sel.SubscriberEnquiryLogID = dsel.UpdateSubscriberEnquiryLogError(con, sel);
                MatchResult = sel.SubscriberEnquiryLogID;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                MatchResult = sel.SubscriberEnquiryLogID;
                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;
            }



            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response ConnectGetBankCodesRequestHistory(SqlConnection con, string Status, int SystemUserID, int SubscriberID)
        {
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            XDSPortalEnquiry.Data.SubscriberEnquiryLog dSubscriberEnquiryLog = new XDSPortalEnquiry.Data.SubscriberEnquiryLog();
            DataSet ds = new DataSet("RequestHistory");

            try
            {
                if (Status == "All")
                    ds = dSubscriberEnquiryLog.GetSubscriberEnquiryLogDataSetpersubscriber(con, SystemUserID, "44", "", SubscriberID);
                else
                    ds = dSubscriberEnquiryLog.GetSubscriberEnquiryLogDataSetpersubscriber(con, SystemUserID, "44", Status.Substring(0, 1));

                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                rp.ResponseData = ds.GetXml();
            }
            catch (Exception oException)
            {
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();

                SqlConnection.ClearPool(con);
            }

            ds.Dispose();
            return rp;
        }
        public XDSPortalLibrary.Entity_Layer.Response ConnectGetNewBankCodesRequestHistory(SqlConnection con, string Status, int SystemUserID, int SubscriberID)
        {
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            XDSPortalEnquiry.Data.SubscriberEnquiryLog dSubscriberEnquiryLog = new XDSPortalEnquiry.Data.SubscriberEnquiryLog();
            DataSet ds = new DataSet("RequestHistory");

            try
            {
                if (Status == "All")
                    ds = dSubscriberEnquiryLog.GetSubscriberEnquiryLogDataSetpersubscriber(con, SystemUserID, "199", "", SubscriberID);
                else
                    ds = dSubscriberEnquiryLog.GetSubscriberEnquiryLogDataSetpersubscriber(con, SystemUserID, "199", Status.Substring(0, 1));

                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                rp.ResponseData = ds.GetXml();
            }
            catch (Exception oException)
            {
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();

                SqlConnection.ClearPool(con);
            }

            ds.Dispose();
            return rp;
        }
    }
}