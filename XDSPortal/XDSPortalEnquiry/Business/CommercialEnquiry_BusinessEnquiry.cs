using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using XDSPortalLibrary;

namespace XDSPortalEnquiry.Business
{
    public class CommercialEnquiry_BusinessEnquiry
    {
        private XDSPortalLibrary.Business_Layer.BusinessEnquiry moCommercialEnquiryWseManager;
        private XDSPortalLibrary.Entity_Layer.BusinessEnquiry eoCommercialEnquiryWseManager;     


        public CommercialEnquiry_BusinessEnquiry()
        {
            moCommercialEnquiryWseManager = new XDSPortalLibrary.Business_Layer.BusinessEnquiry();
            eoCommercialEnquiryWseManager = new XDSPortalLibrary.Entity_Layer.BusinessEnquiry();
        }
        
        public XDSPortalLibrary.Entity_Layer.Response SubmitBusinessEnquiry(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string strRegNo1, string strRegNo2, string strRegNo3, string strBusinessName, string strVatNo, string strSolePropIDNo, string strTrustNo, string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode)
        {

            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            int intSubscriberEnquiryID = 0;
            int intSubscriberEnquiryResultID = 0;
            string rXml = "";
            int MatchResult = 0;
            DataSet ds = new DataSet();
            string strValidationStatus = "";
            Boolean boolSubmitEnquiry = true;


            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            try
            {
                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {


                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }


                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {

                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;
                        eSe.ProductID = intProductId;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = strExtRef;
                        eSe.SystemUserID = intSystemUserID;
                        if (string.IsNullOrEmpty(strRegNo1 + strRegNo2 + strRegNo3))
                        {
                            eSe.BusRegistrationNo = strRegNo1 + strRegNo2 + strRegNo3;
                        }
                        else
                        {
                            eSe.BusRegistrationNo = strTrustNo;
                        }
                        eSe.BusBusinessName = strBusinessName;
                        eSe.BusVatNumber = strVatNo;
                        eSe.IDNo = strSolePropIDNo;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";
                        if (!string.IsNullOrEmpty(strRegNo1 + strRegNo2 + strRegNo3))
                        {
                            eSe.BusRegistrationNo = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        }

                        //Generate serach input string

                        if (!string.IsNullOrEmpty(eSe.BusBusinessName))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.BusBusinessName;
                        }
                        if (!string.IsNullOrEmpty(eSe.BusRegistrationNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        }
                        if (!string.IsNullOrEmpty(eSe.BusVatNumber))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.BusVatNumber;
                        }
                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table
                        intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);
                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        eoCommercialEnquiryWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoCommercialEnquiryWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoCommercialEnquiryWseManager.BusinessName = strBusinessName;
                        eoCommercialEnquiryWseManager.ConfirmationChkBox = bConfirmationChkBox;
                        eoCommercialEnquiryWseManager.ExternalReference = strExtRef;
                        eoCommercialEnquiryWseManager.ProductID = intProductId;
                        eoCommercialEnquiryWseManager.RegistrationNo = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        eoCommercialEnquiryWseManager.subscriberID = intSubscriberID;
                        eoCommercialEnquiryWseManager.VATNumber = strVatNo;
                        eoCommercialEnquiryWseManager.SolePropIDNo = strSolePropIDNo;
                        eoCommercialEnquiryWseManager.TrustNo = strTrustNo;
                        eoCommercialEnquiryWseManager.BonusCheck = bBonusChecking;

                        // Submit data for matching 
                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.BusinessEnquiryMatch(eoCommercialEnquiryWseManager);

                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.BusinessEnquiryMatch(eoCommercialEnquiryWseManager);
                        }

                        // Get Response data
                        rp.EnquiryID = intSubscriberEnquiryID;
                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);


                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            eSC.SearchOutput = "";
                            eSC.BusBusinessName = "";
                            eSC.BusRegistrationNo = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;
                            eSC.SearchOutput = "";
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();

                            dSC.InsertSubscriberEnquiryResult(con, eSC);
                            rp.EnquiryID = eSC.SubscriberEnquiryID;
                        }


                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);



                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            ds.ReadXml(xmlSR);
                            if (ds.Tables.Contains("CommercialCompanyInformation"))
                            {
                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, intProductId);

                                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);
                                rXml = ds.GetXml();
                                rp.ResponseData = rXml;

                                foreach (DataRow r in ds.Tables["CommercialCompanyInformation"].Rows)
                                {

                                    eSC.SearchOutput = "";
                                    eSC.BusBusinessName = "";
                                    eSC.BusRegistrationNo = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    if (sub.PayAsYouGo == 1 || !(strVoucherCode == string.Empty))
                                    {
                                        eSC.Billable = false;
                                    }
                                    else
                                    {
                                        eSC.Billable = true;
                                    }

                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("CommercialName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["CommercialName"].ToString()))
                                        {
                                            eSC.BusBusinessName = r["CommercialName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusBusinessName;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("RegistrationNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["RegistrationNo"].ToString()))
                                        {
                                            eSC.BusRegistrationNo = r["RegistrationNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusRegistrationNo;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    // Change the PayAsYouGoEnquiryLimit  after viewing the report
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                                    dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                }
                            }
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("CommercialDetails"))
                            {
                                foreach (DataRow r in ds.Tables["CommercialDetails"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.BusBusinessName = "";
                                    eSC.BusRegistrationNo = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("BusinessName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["BusinessName"].ToString()))
                                        {
                                            eSC.BusBusinessName = r["BusinessName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusBusinessName;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("RegistrationNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["RegistrationNo"].ToString()))
                                        {
                                            eSC.BusRegistrationNo = r["RegistrationNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusRegistrationNo;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.KeyID = int.Parse(r["CommercialID"].ToString());
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {

                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");

                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;

                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;
                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);

                                            }

                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {
                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("CommercialDetails"))
                            {
                                foreach (DataRow r in ds.Tables["CommercialDetails"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.BusBusinessName = "";
                                    eSC.BusRegistrationNo = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("BusinessName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["BusinessName"].ToString()))
                                        {
                                            eSC.BusBusinessName = r["BusinessName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusBusinessName;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("RegistrationNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["RegistrationNo"].ToString()))
                                        {
                                            eSC.BusRegistrationNo = r["RegistrationNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusRegistrationNo;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {


                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");

                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;

                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;
                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);

                                            }
                                            dsBonus.Dispose();
                                        }
                                    }
                                    else
                                    {
                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }
                        }

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }

            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;


                MatchResult = eSe.SubscriberEnquiryID;
                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }

            ds.Dispose(); 

            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitBusinessEnquiryWorldCheck(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string strRegNo1, string strRegNo2, string strRegNo3, string strBusinessName, string strVatNo, string strSolePropIDNo, string strTrustNo, string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode, string strClientID, string strUserID, string strPassword, string strURL)
        {

            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            int intSubscriberEnquiryID = 0;
            int intSubscriberEnquiryResultID = 0;
            string rXml = "";
            string rXmlMatch = "";
            int MatchResult = 0;
            DataSet ds = new DataSet();
            string strValidationStatus = "";
            Boolean boolSubmitEnquiry = true;


            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            try
            {
                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {


                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }


                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {


                        //XDSPortalEnquiry.Data.XDSSettings odSystemUser = new XDSPortalEnquiry.Data.XDSSettings();
                        //strClientID = odSystemUser.GetSettings("ClientID", AdminConnection);
                        //strUserID = odSystemUser.GetSettings("UserID", AdminConnection);
                        //strPassword = odSystemUser.GetSettings("Password", AdminConnection);



                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;
                        eSe.ProductID = intProductId;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = strExtRef;
                        eSe.SystemUserID = intSystemUserID;
                        if (string.IsNullOrEmpty(strRegNo1 + strRegNo2 + strRegNo3))
                        {
                            eSe.BusRegistrationNo = strRegNo1 + strRegNo2 + strRegNo3;
                        }
                        else
                        {
                            eSe.BusRegistrationNo = strTrustNo;
                        }
                        eSe.BusBusinessName = strBusinessName;
                        eSe.BusVatNumber = strVatNo;
                        eSe.IDNo = strSolePropIDNo;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";

                        if (!string.IsNullOrEmpty(strRegNo1 + strRegNo2 + strRegNo3))
                        {
                            eSe.BusRegistrationNo = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        }

                        //Generate serach input string

                        if (!string.IsNullOrEmpty(eSe.BusBusinessName))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.BusBusinessName;
                        }
                        if (!string.IsNullOrEmpty(eSe.BusRegistrationNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        }
                        if (!string.IsNullOrEmpty(eSe.BusVatNumber))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.BusVatNumber;
                        }
                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table
                        intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);
                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        eoCommercialEnquiryWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoCommercialEnquiryWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoCommercialEnquiryWseManager.BusinessName = strBusinessName;
                        eoCommercialEnquiryWseManager.ConfirmationChkBox = bConfirmationChkBox;
                        eoCommercialEnquiryWseManager.ExternalReference = strExtRef;
                        eoCommercialEnquiryWseManager.ProductID = intProductId;
                        eoCommercialEnquiryWseManager.RegistrationNo = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        eoCommercialEnquiryWseManager.subscriberID = intSubscriberID;
                        eoCommercialEnquiryWseManager.VATNumber = strVatNo;
                        eoCommercialEnquiryWseManager.SolePropIDNo = strSolePropIDNo;
                        eoCommercialEnquiryWseManager.TrustNo = strTrustNo;
                        eoCommercialEnquiryWseManager.BonusCheck = bBonusChecking;

                        // Submit data for matching 
                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.BusinessEnquiryMatchComplianceExposure(eoCommercialEnquiryWseManager);

                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.BusinessEnquiryMatchComplianceExposure(eoCommercialEnquiryWseManager);
                        }

                        // Get Response data
                        rp.EnquiryID = intSubscriberEnquiryID;
                        rXmlMatch = rp.ResponseData;
                        System.IO.StringReader xmlSRMatch = new System.IO.StringReader(rXmlMatch);

                        bool ConsumerExists = true;

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None ||
                            rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report ||
                            rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple ||
                            rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                        {
                            ds.ReadXml(xmlSRMatch);

                            if (ds.Tables.Contains("CommercialDetails"))
                            {
                                ds.Tables[0].Columns.Add("EnquiryID");
                                ds.Tables[0].Columns.Add("EnquiryResultID");
                                ds.Tables[0].Columns.Add("Reference");

                                bool IsMatchFound = false;

                                foreach (DataRow r in ds.Tables["CommercialDetails"].Rows)
                                {
                                    if (r["BusinessName"].ToString().ToUpper().Contains(strBusinessName.ToUpper()))
                                        IsMatchFound = true;

                                    eSC.SearchOutput = "";
                                    eSC.BusBusinessName = "";
                                    eSC.BusRegistrationNo = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("BusinessName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["BusinessName"].ToString()))
                                        {
                                            eSC.BusBusinessName = r["BusinessName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusBusinessName;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("RegistrationNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["RegistrationNo"].ToString()))
                                        {
                                            eSC.BusRegistrationNo = r["RegistrationNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusRegistrationNo;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.KeyID = int.Parse(r["CommercialID"].ToString());
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {

                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");

                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;

                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;
                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);

                                            }

                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {
                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }

                                if (IsMatchFound == false)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.BusBusinessName = "";
                                    eSC.BusRegistrationNo = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (!string.IsNullOrEmpty(strBusinessName))
                                    {
                                        eSC.BusBusinessName = strBusinessName.Replace("'", "''");
                                        eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusBusinessName;
                                    }

                                    if (!string.IsNullOrEmpty(strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3))
                                    {
                                        eSC.BusRegistrationNo = (strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3).Replace("'", "''");
                                        eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusRegistrationNo;
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.KeyID = 0;
                                    eSC.KeyType = rp.ResponseKeyType;


                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    // Insert the match results to SubscriberEnquiryResult Table
                                    intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows.Add(0, strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3, strBusinessName, "", "", intSubscriberEnquiryID, intSubscriberEnquiryResultID, eSC.Reference);

                                    rp.ResponseData = ds.GetXml();
                                }
                            }
                            else
                            {
                                eSC.SearchOutput = "";
                                eSC.BusBusinessName = "";
                                eSC.BusRegistrationNo = "";
                                eSC.IDNo = "";
                                eSC.PassportNo = "";
                                eSC.Surname = "";
                                eSC.FirstName = "";
                                eSC.BirthDate = DateTime.Parse("1900/01/01");
                                eSC.Gender = "";
                                eSC.CreatedByUser = eSe.CreatedByUser;
                                eSC.CreatedOnDate = DateTime.Now;
                                eSC.SearchOutput = "";

                                if (!string.IsNullOrEmpty(strBusinessName))
                                {
                                    eSC.BusBusinessName = strBusinessName.Replace("'", "''");
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusBusinessName;
                                }

                                if (!string.IsNullOrEmpty(strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3))
                                {
                                    eSC.BusRegistrationNo = (strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3).Replace("'", "''");
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusRegistrationNo;
                                }

                                if (eSC.SearchOutput.Length > 0)
                                {
                                    eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                }

                                eSC.KeyID = 0;
                                eSC.KeyType = rp.ResponseKeyType;

                                eSC.BillingTypeID = spr.BillingTypeID;
                                eSC.BillingPrice = spr.UnitPrice;

                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                eSC.ExtraVarOutput1 = "";
                                eSC.ProductID = intProductId;
                                eSC.VoucherCode = strVoucherCode;

                                // Insert the match results to SubscriberEnquiryResult Table
                                intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                rp.EnquiryID = eSC.SubscriberEnquiryID;

                                //Build list 
                                ds = new DataSet("DsCommercialMatch");

                                DataTable dtSubscriberInput = new DataTable("CommercialDetails");
                                dtSubscriberInput.Columns.Add("CommercialID", typeof(Int32));
                                dtSubscriberInput.Columns.Add("RegistrationNo", typeof(String));
                                dtSubscriberInput.Columns.Add("Businessname", typeof(String));
                                dtSubscriberInput.Columns.Add("BonusXML", typeof(String));
                                dtSubscriberInput.Columns.Add("TempReference", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryID", typeof(Int32));
                                dtSubscriberInput.Columns.Add("EnquiryResultID", typeof(Int32));
                                dtSubscriberInput.Columns.Add("Reference", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["CommercialID"] = 0;
                                drSubscriberInput["RegistrationNo"] = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                                drSubscriberInput["Businessname"] = strBusinessName;
                                drSubscriberInput["BonusXML"] = "";
                                drSubscriberInput["TempReference"] = "";
                                drSubscriberInput["EnquiryID"] = intSubscriberEnquiryID;
                                drSubscriberInput["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                drSubscriberInput["Reference"] = strExtRef;

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);

                                rp.ResponseData = ds.GetXml();
                                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                                ConsumerExists = false;
                            }

                            //DataSet dsDataSegments = null;
                            //XDSPortalEnquiry.Business.LexisNexisData lexisNexisData = new XDSPortalEnquiry.Business.LexisNexisData();
                            //XDSPortalLibrary.Entity_Layer.Response rp1 = new XDSPortalLibrary.Entity_Layer.Response();
                            //rp1 = lexisNexisData.CommercialRenderLexisNexisData(con, AdminConnection, intSubscriberEnquiryID, intSubscriberEnquiryResultID, dsDataSegments, strClientID, strUserID, strPassword, strURL, 177);


                            ////Check if data returned by Bureau and LN, if not, return nothing
                            //if (ConsumerExists == false && rp1.ResponseData.Contains("SupplierData") == false)
                            //{
                            //    rp.ResponseData = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                            //}

                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);
                        }
                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }

            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.StackTrace.ToLower().Substring(oException.StackTrace.ToLower().IndexOf("xdsportalenquiry"), 100) + oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;


                MatchResult = eSe.SubscriberEnquiryID;
                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }

            ds.Dispose();

            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitBusinessEnquiryCustom(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string strRegNo1, string strRegNo2, string strRegNo3, string strBusinessName, string strVatNo, string strSolePropIDNo, string strTrustNo, string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode, string IDs, XDSPortalLibrary.Entity_Layer.BusinessSearchResponse oinSearchResponse, out XDSPortalLibrary.Entity_Layer.BusinessSearchResponse oBSearchResponse)
        {

            string strAdminCon = AdminConnection.ConnectionString;
            oBSearchResponse = oinSearchResponse;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            int intSubscriberEnquiryID = 0;
            int intSubscriberEnquiryResultID = 0;
            string rXml = "";
            int MatchResult = 0;
            DataSet ds = new DataSet();
            string strValidationStatus = "";
            Boolean boolSubmitEnquiry = true;


            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            try
            {
                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {

                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }
                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {

                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;
                        eSe.ProductID = intProductId;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = strExtRef;
                        eSe.SystemUserID = intSystemUserID;
                        if (string.IsNullOrEmpty(strRegNo1 + strRegNo2 + strRegNo3))
                        {
                            eSe.BusRegistrationNo = strRegNo1 + strRegNo2 + strRegNo3;
                        }
                        else
                        {
                            eSe.BusRegistrationNo = strTrustNo;
                        }
                        eSe.BusBusinessName = strBusinessName;
                        eSe.BusVatNumber = strVatNo;
                        eSe.IDNo = strSolePropIDNo;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";
                        if (!string.IsNullOrEmpty(strRegNo1 + strRegNo2 + strRegNo3))
                        {
                            eSe.BusRegistrationNo = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        }

                        //Generate serach input string

                        if (!string.IsNullOrEmpty(eSe.BusBusinessName))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.BusBusinessName;
                        }
                        if (!string.IsNullOrEmpty(eSe.BusRegistrationNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        }
                        if (!string.IsNullOrEmpty(eSe.BusVatNumber))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.BusVatNumber;
                        }
                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table
                        intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);
                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        eoCommercialEnquiryWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoCommercialEnquiryWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoCommercialEnquiryWseManager.BusinessName = strBusinessName;
                        eoCommercialEnquiryWseManager.ConfirmationChkBox = bConfirmationChkBox;
                        eoCommercialEnquiryWseManager.ExternalReference = strExtRef;
                        eoCommercialEnquiryWseManager.ProductID = intProductId;
                        eoCommercialEnquiryWseManager.RegistrationNo = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        eoCommercialEnquiryWseManager.subscriberID = intSubscriberID;
                        eoCommercialEnquiryWseManager.VATNumber = strVatNo;
                        eoCommercialEnquiryWseManager.SolePropIDNo = strSolePropIDNo;
                        eoCommercialEnquiryWseManager.TrustNo = strTrustNo;
                        eoCommercialEnquiryWseManager.BonusCheck = bBonusChecking;
                        eoCommercialEnquiryWseManager.ReportID = spr.ReportID;

                        // Submit data for matching 
                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.BusinessEnquiryCustom(eoCommercialEnquiryWseManager);

                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.BusinessEnquiryCustom(eoCommercialEnquiryWseManager);
                        }

                        // Get Response data
                        rp.EnquiryID = intSubscriberEnquiryID;
                        
                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);


                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            eSC.SearchOutput = "";
                            eSC.BusBusinessName = "";
                            eSC.BusRegistrationNo = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;
                            eSC.SearchOutput = "";
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();

                            dSC.InsertSubscriberEnquiryResult(con, eSC);
                            rp.EnquiryID = eSC.SubscriberEnquiryID;
                        }


                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);

                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            ds.ReadXml(xmlSR);
                            if (ds.Tables.Contains("CommercialCompanyInformation"))
                            {
                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, intProductId);

                                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);
                                rXml = ds.GetXml();
                                rp.ResponseData = rXml;

                                foreach (DataRow r in ds.Tables["CommercialCompanyInformation"].Rows)
                                {

                                    eSC.SearchOutput = "";
                                    eSC.BusBusinessName = "";
                                    eSC.BusRegistrationNo = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    if (sub.PayAsYouGo == 1 || !(strVoucherCode == string.Empty))
                                    {
                                        eSC.Billable = false;
                                    }
                                    else
                                    {
                                        eSC.Billable = true;
                                    }

                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("CommercialName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["CommercialName"].ToString()))
                                        {
                                            eSC.BusBusinessName = r["CommercialName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusBusinessName;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("RegistrationNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["RegistrationNo"].ToString()))
                                        {
                                            eSC.BusRegistrationNo = r["RegistrationNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusRegistrationNo;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    // Change the PayAsYouGoEnquiryLimit  after viewing the report
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                                    dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                }
                            }
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("CommercialDetails"))
                            {

                               // File.AppendAllText(@"C:\Log\MTNCom.txt", "Step1");
                                 int count = 0;

                    count = ds.Tables[0].Rows.Count;
                   // File.AppendAllText(@"C:\Log\MTNCom.txt", "Step3");
                    oBSearchResponse.BusinessSearchResult.SearchResponse.SearchResponse = new XDSPortalLibrary.Entity_Layer.BusinessSearchResponseBusinessSearchResultSearchResponseSearchResponse[count];

                    count = 0;

                                foreach (DataRow r in ds.Tables["CommercialDetails"].Rows)
                                {

                                    //File.AppendAllText(@"C:\Log\MTNCom.txt", "Step2");
                                    eSC.SearchOutput = "";
                                    eSC.BusBusinessName = "";
                                    eSC.BusRegistrationNo = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("BusinessName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["BusinessName"].ToString()))
                                        {
                                            eSC.BusBusinessName = r["BusinessName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusBusinessName;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("RegistrationNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["RegistrationNo"].ToString()))
                                        {
                                            eSC.BusRegistrationNo = r["RegistrationNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusRegistrationNo;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("RegistrationNo12"))
                                    {
                                        if (!string.IsNullOrEmpty(r["RegistrationNo12"].ToString()))
                                        {
                                            eSC.Ha_CauseOfDeath = r["RegistrationNo12"].ToString().Replace("'", "''");

                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.KeyID = int.Parse(r["CommercialID"].ToString());
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {

                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");

                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;

                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;
                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);

                                            }

                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {
                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                        XDSPortalLibrary.Entity_Layer.BusinessSearchResponseBusinessSearchResultSearchResponseSearchResponse oSearchResponse = new XDSPortalLibrary.Entity_Layer.BusinessSearchResponseBusinessSearchResultSearchResponseSearchResponse();
                                        oSearchResponse.Name = r["BusinessName"].ToString();
                                        oSearchResponse.NameType = "B";
                                        oSearchResponse.PhysicalAddress = r["PhysicalAddress"].ToString();
                                        oSearchResponse.Suburb = r["Suburb"].ToString();
                                        oSearchResponse.Town = r["Town"].ToString();
                                        oSearchResponse.Country = r["Country"].ToString();
                                        oSearchResponse.PostCode = r["PostCode"].ToString();
                                        oSearchResponse.PostalAddress = r["PostalAddress"].ToString();
                                        oSearchResponse.PostalSuburb = r["PostalSuburb"].ToString();
                                        oSearchResponse.PostalTown = r["PostalTown"].ToString();
                                        oSearchResponse.PostalCountry = r["PostalCountry"].ToString();
                                        oSearchResponse.PostalPostCode = r["PostalPostCode"].ToString();
                                        oSearchResponse.PhoneNo = r["PhoneNo"].ToString();
                                        oSearchResponse.FaxNo = r["FaxNo"].ToString();
                                        oSearchResponse.RegNo = r["RegistrationNo12"].ToString();
                                        oSearchResponse.RegStatus = r["RegStatus"].ToString();
                                        oSearchResponse.RegStatusCode = r["RegStatusCode"].ToString();
                                        oSearchResponse.TUNumber = intSubscriberEnquiryID.ToString() + ":" + intSubscriberEnquiryResultID.ToString(); 

                                        oBSearchResponse.BusinessSearchResult.SearchResponse.SearchResponse[count] = oSearchResponse;

                                        count++;

                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("CommercialDetails"))
                            {
                                foreach (DataRow r in ds.Tables["CommercialDetails"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.BusBusinessName = "";
                                    eSC.BusRegistrationNo = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("BusinessName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["BusinessName"].ToString()))
                                        {
                                            eSC.BusBusinessName = r["BusinessName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusBusinessName;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("RegistrationNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["RegistrationNo"].ToString()))
                                        {
                                            eSC.BusRegistrationNo = r["RegistrationNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusRegistrationNo;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {


                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");

                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;

                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;
                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);

                                            }
                                            dsBonus.Dispose();
                                        }
                                    }
                                    else
                                    {
                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }
                        }

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }

            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;


                MatchResult = eSe.SubscriberEnquiryID;
                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }

            ds.Dispose();

            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitBusinessEnquiryCustomA(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string strRegNo1, string strRegNo2, string strRegNo3, string strBusinessName, string strVatNo, string strSolePropIDNo, string strTrustNo, string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode, string IDs, XDSADPMODELS.BusinessSearchResponse oinSearchResponse, out XDSADPMODELS.BusinessSearchResponse oBSearchResponse)
        {

            string strAdminCon = AdminConnection.ConnectionString;
            oBSearchResponse = oinSearchResponse;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            int intSubscriberEnquiryID = 0;
            int intSubscriberEnquiryResultID = 0;
            string rXml = "";
            int MatchResult = 0;
            DataSet ds = new DataSet();
            string strValidationStatus = "";
            Boolean boolSubmitEnquiry = true;


            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            try
            {
                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {

                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }
                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {

                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;
                        eSe.ProductID = intProductId;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = strExtRef;
                        eSe.SystemUserID = intSystemUserID;
                        if (string.IsNullOrEmpty(strRegNo1 + strRegNo2 + strRegNo3))
                        {
                            eSe.BusRegistrationNo = strRegNo1 + strRegNo2 + strRegNo3;
                        }
                        else
                        {
                            eSe.BusRegistrationNo = strTrustNo;
                        }
                        eSe.BusBusinessName = strBusinessName;
                        eSe.BusVatNumber = strVatNo;
                        eSe.IDNo = strSolePropIDNo;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";
                        if (!string.IsNullOrEmpty(strRegNo1 + strRegNo2 + strRegNo3))
                        {
                            eSe.BusRegistrationNo = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        }

                        //Generate serach input string

                        if (!string.IsNullOrEmpty(eSe.BusBusinessName))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.BusBusinessName;
                        }
                        if (!string.IsNullOrEmpty(eSe.BusRegistrationNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        }
                        if (!string.IsNullOrEmpty(eSe.BusVatNumber))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.BusVatNumber;
                        }
                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table
                        intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);
                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        eoCommercialEnquiryWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoCommercialEnquiryWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoCommercialEnquiryWseManager.BusinessName = strBusinessName;
                        eoCommercialEnquiryWseManager.ConfirmationChkBox = bConfirmationChkBox;
                        eoCommercialEnquiryWseManager.ExternalReference = strExtRef;
                        eoCommercialEnquiryWseManager.ProductID = intProductId;
                        eoCommercialEnquiryWseManager.RegistrationNo = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        eoCommercialEnquiryWseManager.subscriberID = intSubscriberID;
                        eoCommercialEnquiryWseManager.VATNumber = strVatNo;
                        eoCommercialEnquiryWseManager.SolePropIDNo = strSolePropIDNo;
                        eoCommercialEnquiryWseManager.TrustNo = strTrustNo;
                        eoCommercialEnquiryWseManager.BonusCheck = bBonusChecking;
                        eoCommercialEnquiryWseManager.ReportID = spr.ReportID;

                        // Submit data for matching 
                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.BusinessEnquiryCustom(eoCommercialEnquiryWseManager);

                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.BusinessEnquiryCustom(eoCommercialEnquiryWseManager);
                        }

                        // Get Response data
                        rp.EnquiryID = intSubscriberEnquiryID;

                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);


                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            eSC.SearchOutput = "";
                            eSC.BusBusinessName = "";
                            eSC.BusRegistrationNo = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;
                            eSC.SearchOutput = "";
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();

                            dSC.InsertSubscriberEnquiryResult(con, eSC);
                            rp.EnquiryID = eSC.SubscriberEnquiryID;
                        }


                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);

                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            ds.ReadXml(xmlSR);
                            if (ds.Tables.Contains("CommercialCompanyInformation"))
                            {
                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, intProductId);

                                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);
                                rXml = ds.GetXml();
                                rp.ResponseData = rXml;

                                foreach (DataRow r in ds.Tables["CommercialCompanyInformation"].Rows)
                                {

                                    eSC.SearchOutput = "";
                                    eSC.BusBusinessName = "";
                                    eSC.BusRegistrationNo = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    if (sub.PayAsYouGo == 1 || !(strVoucherCode == string.Empty))
                                    {
                                        eSC.Billable = false;
                                    }
                                    else
                                    {
                                        eSC.Billable = true;
                                    }

                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("CommercialName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["CommercialName"].ToString()))
                                        {
                                            eSC.BusBusinessName = r["CommercialName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusBusinessName;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("RegistrationNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["RegistrationNo"].ToString()))
                                        {
                                            eSC.BusRegistrationNo = r["RegistrationNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusRegistrationNo;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    // Change the PayAsYouGoEnquiryLimit  after viewing the report
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                                    dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                }
                            }
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("CommercialDetails"))
                            {

                                // File.AppendAllText(@"C:\Log\MTNCom.txt", "Step1");
                                int count = 0;

                                count = ds.Tables[0].Rows.Count;
                                // File.AppendAllText(@"C:\Log\MTNCom.txt", "Step3");
                                oBSearchResponse.BusinessSearchResult.SearchResponse = new XDSADPMODELS.SearchResponseData[count];

                                count = 0;

                                foreach (DataRow r in ds.Tables["CommercialDetails"].Rows)
                                {

                                    //File.AppendAllText(@"C:\Log\MTNCom.txt", "Step2");
                                    eSC.SearchOutput = "";
                                    eSC.BusBusinessName = "";
                                    eSC.BusRegistrationNo = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("BusinessName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["BusinessName"].ToString()))
                                        {
                                            eSC.BusBusinessName = r["BusinessName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusBusinessName;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("RegistrationNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["RegistrationNo"].ToString()))
                                        {
                                            eSC.BusRegistrationNo = r["RegistrationNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusRegistrationNo;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.KeyID = int.Parse(r["CommercialID"].ToString());
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {

                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");

                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;

                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;
                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);

                                            }

                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {
                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                        XDSADPMODELS.SearchResponseData oSearchResponse = new XDSADPMODELS.SearchResponseData();
                                        oSearchResponse.Name = r["BusinessName"].ToString();
                                        oSearchResponse.NameType = "B";
                                        oSearchResponse.PhysicalAddress = r["PhysicalAddress"].ToString();
                                        oSearchResponse.Suburb = r["Suburb"].ToString();
                                        oSearchResponse.Town = r["Town"].ToString();
                                        oSearchResponse.Country = r["Country"].ToString();
                                        oSearchResponse.PostCode = r["PostCode"].ToString();
                                        oSearchResponse.PostalAddress = r["PostalAddress"].ToString();
                                        oSearchResponse.PostalSuburb = r["PostalSuburb"].ToString();
                                        oSearchResponse.PostalTown = r["PostalTown"].ToString();
                                        oSearchResponse.PostalCountry = r["PostalCountry"].ToString();
                                        oSearchResponse.PostalPostCode = r["PostalPostCode"].ToString();
                                        oSearchResponse.PhoneNo = r["PhoneNo"].ToString();
                                        oSearchResponse.FaxNo = r["FaxNo"].ToString();
                                        oSearchResponse.RegNo = r["RegistrationNo12"].ToString();
                                        oSearchResponse.RegStatus = r["RegStatus"].ToString();
                                        oSearchResponse.RegStatusCode = r["RegStatusCode"].ToString();
                                        oSearchResponse.BureauNumber = intSubscriberEnquiryID.ToString() + ":" + intSubscriberEnquiryResultID.ToString();

                                        oBSearchResponse.BusinessSearchResult.SearchResponse[count] = oSearchResponse;

                                        count++;

                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("CommercialDetails"))
                            {
                                foreach (DataRow r in ds.Tables["CommercialDetails"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.BusBusinessName = "";
                                    eSC.BusRegistrationNo = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("BusinessName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["BusinessName"].ToString()))
                                        {
                                            eSC.BusBusinessName = r["BusinessName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusBusinessName;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("RegistrationNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["RegistrationNo"].ToString()))
                                        {
                                            eSC.BusRegistrationNo = r["RegistrationNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusRegistrationNo;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {


                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");

                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;

                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;
                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);

                                            }
                                            dsBonus.Dispose();
                                        }
                                    }
                                    else
                                    {
                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }
                        }

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }

            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;


                MatchResult = eSe.SubscriberEnquiryID;
                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }

            ds.Dispose();

            return rp;
        }

        //public XDSPortalLibrary.Entity_Layer.Response SubmitBusinessEnquiryNonReg(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, 
        //    string strSubscriberName,  string strBusinessName, string strVatNo, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode, 
        //    XDSPortalLibrary.Entity_Layer.MTNSOA.MTNCommercialFinalOutput oinSearchResponse, out XDSPortalLibrary.Entity_Layer.MTNSOA.MTNCommercialFinalOutput oBSearchResponse)
        //{

        //    string strAdminCon = AdminConnection.ConnectionString;
        //    oBSearchResponse = oinSearchResponse;

        //    if (con.State == ConnectionState.Closed)
        //        con.Open();
        //    if (AdminConnection.State == ConnectionState.Closed)
        //        AdminConnection.Open();

        //    bBonusChecking = false;
        //    int intSubscriberEnquiryID = 0;
        //    int intSubscriberEnquiryResultID = 0;
        //    string rXml = "";
        //    int MatchResult = 0;
        //    DataSet ds = new DataSet();
        //    string strValidationStatus = "";
        //    Boolean boolSubmitEnquiry = true;


        //    Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
        //    Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

        //    Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
        //    Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

        //    Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
        //    Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

        //    Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
        //    Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

        //    Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
        //    Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

        //    XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

        //    xdsBilling xb = new xdsBilling();
        //    Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);

        //    Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

        //    try
        //    {
        //        // Check if this subscriber is authorized to use the current product
        //        if (spr.ReportID > 0)
        //        {


        //            if (!string.IsNullOrEmpty(strVoucherCode))
        //            {
        //                SubscriberVoucher sv = new SubscriberVoucher();
        //                strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

        //                if (strValidationStatus == "")
        //                {
        //                    throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
        //                }
        //                else if (!(strValidationStatus == "1"))
        //                {
        //                    throw new Exception(strValidationStatus);
        //                }


        //            }
        //            else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
        //            {
        //                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //                rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
        //                boolSubmitEnquiry = false;

        //            }
        //            if (boolSubmitEnquiry)
        //            {

        //                eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
        //                eSe.SubscriberID = intSubscriberID;
        //                eSe.ProductID = intProductId;
        //                eSe.SubscriberEnquiryDate = DateTime.Now;
        //                eSe.SubscriberName = strSubscriberName;
        //                eSe.SystemUserID = intSystemUserID;
        //                eSe.BusBusinessName = strBusinessName;
        //                eSe.BusVatNumber = strVatNo;
        //                eSe.SystemUserID = intSystemUserID;
        //                eSe.SystemUser = sys.SystemUserFullName;
        //                eSe.CreatedByUser = sys.Username;
        //                eSe.CreatedOnDate = DateTime.Now;
        //                eSe.SearchInput = "";

        //                //Generate search input string

        //                if (!string.IsNullOrEmpty(eSe.BusBusinessName))
        //                {
        //                    eSe.SearchInput = eSe.SearchInput + " | " + eSe.BusBusinessName;
        //                }
        //                if (!string.IsNullOrEmpty(eSe.BusVatNumber))
        //                {
        //                    eSe.SearchInput = eSe.SearchInput + " | " + eSe.BusVatNumber;
        //                }
        //                if (!string.IsNullOrEmpty(eSe.IDNo))
        //                {
        //                    eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
        //                }

        //                if (eSe.SearchInput.Length > 0)
        //                {
        //                    eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
        //                }


        //                eoCommercialEnquiryWseManager.DataSegments = eSe.ExtraVarInput2;
        //                eoCommercialEnquiryWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
        //                eoCommercialEnquiryWseManager.BusinessName = strBusinessName;
        //                eoCommercialEnquiryWseManager.ConfirmationChkBox = bConfirmationChkBox;
        //                eoCommercialEnquiryWseManager.ProductID = intProductId;
        //                eoCommercialEnquiryWseManager.subscriberID = intSubscriberID;
        //                eoCommercialEnquiryWseManager.VATNumber = strVatNo;
        //                eoCommercialEnquiryWseManager.BonusCheck = bBonusChecking;
        //                eoCommercialEnquiryWseManager.ReportID = spr.ReportID;

        //                // Submit data for matching 
        //                XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
        //                rp = ra.BusinessEnquiryCustom(eoCommercialEnquiryWseManager);

        //                //If error raise one last call with a different connection
        //                if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
        //                {
        //                    eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
        //                    ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
        //                    rp = ra.BusinessEnquiryCustom(eoCommercialEnquiryWseManager);
        //                }

        //                // Get Response data
        //                rp.EnquiryID = intSubscriberEnquiryID;

        //                rXml = rp.ResponseData;

        //                System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);


        //                if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
        //                {
        //                    eSC.SearchOutput = "";
        //                    eSC.BusBusinessName = "";
        //                    eSC.BusRegistrationNo = "";
        //                    eSC.IDNo = "";
        //                    eSC.PassportNo = "";
        //                    eSC.Surname = "";
        //                    eSC.FirstName = "";
        //                    eSC.BirthDate = DateTime.Parse("1900/01/01");
        //                    eSC.Gender = "";
        //                    eSC.CreatedByUser = eSe.CreatedByUser;
        //                    eSC.CreatedOnDate = DateTime.Now;
        //                    eSC.SearchOutput = "";
        //                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
        //                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();

        //                    dSC.InsertSubscriberEnquiryResult(con, eSC);
        //                    rp.EnquiryID = eSC.SubscriberEnquiryID;
        //                }


        //                else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
        //                {

        //                    eSC.EnquiryResult = "E";
        //                    eSe.ErrorDescription = rp.ResponseData.ToString();
        //                    dSe.UpdateSubscriberEnquiryError(con, eSe);

        //                }

        //                else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
        //                {
        //                    ds.ReadXml(xmlSR);
        //                    if (ds.Tables.Contains("CommercialCompanyInformation"))
        //                    {
        //                        Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
        //                        Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, intProductId);

        //                        DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
        //                        dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
        //                        dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
        //                        dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
        //                        dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
        //                        dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
        //                        DataRow drSubscriberInput;
        //                        drSubscriberInput = dtSubscriberInput.NewRow();

        //                        drSubscriberInput["EnquiryDate"] = DateTime.Now;
        //                        drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
        //                        drSubscriberInput["SubscriberName"] = sub.SubscriberName;
        //                        drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
        //                        drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();

        //                        dtSubscriberInput.Rows.Add(drSubscriberInput);

        //                        ds.Tables.Add(dtSubscriberInput);
        //                        rXml = ds.GetXml();
        //                        rp.ResponseData = rXml;

        //                        foreach (DataRow r in ds.Tables["CommercialCompanyInformation"].Rows)
        //                        {

        //                            eSC.SearchOutput = "";
        //                            eSC.BusBusinessName = "";
        //                            eSC.BusRegistrationNo = "";
        //                            eSC.IDNo = "";
        //                            eSC.PassportNo = "";
        //                            eSC.Surname = "";
        //                            eSC.FirstName = "";
        //                            eSC.BirthDate = DateTime.Parse("1900/01/01");
        //                            eSC.Gender = "";
        //                            eSC.DetailsViewedDate = DateTime.Now;
        //                            eSC.DetailsViewedYN = true;
        //                            if (sub.PayAsYouGo == 1 || !(strVoucherCode == string.Empty))
        //                            {
        //                                eSC.Billable = false;
        //                            }
        //                            else
        //                            {
        //                                eSC.Billable = true;
        //                            }

        //                            eSC.CreatedByUser = eSe.CreatedByUser;
        //                            eSC.CreatedOnDate = DateTime.Now;
        //                            eSC.SearchOutput = "";

        //                            if (r.Table.Columns.Contains("CommercialName"))
        //                            {
        //                                if (!string.IsNullOrEmpty(r["CommercialName"].ToString()))
        //                                {
        //                                    eSC.BusBusinessName = r["CommercialName"].ToString().Replace("'", "''");
        //                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusBusinessName;
        //                                }
        //                            }

        //                            if (r.Table.Columns.Contains("RegistrationNo"))
        //                            {
        //                                if (!string.IsNullOrEmpty(r["RegistrationNo"].ToString()))
        //                                {
        //                                    eSC.BusRegistrationNo = r["RegistrationNo"].ToString().Replace("'", "''");
        //                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusRegistrationNo;
        //                                }
        //                            }

        //                            if (eSC.SearchOutput.Length > 0)
        //                            {
        //                                eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
        //                            }

        //                            eSC.KeyID = rp.ResponseKey;
        //                            eSC.KeyType = rp.ResponseKeyType;

        //                            eSC.BillingTypeID = spr.BillingTypeID;
        //                            eSC.BillingPrice = spr.UnitPrice;

        //                            eSC.XMLData = rXml.Replace("'", "''");
        //                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();
        //                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

        //                            rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
        //                            rp.EnquiryID = eSC.SubscriberEnquiryID;

        //                            // Change the PayAsYouGoEnquiryLimit  after viewing the report
        //                            dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
        //                            dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
        //                        }
        //                    }
        //                }
        //                else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
        //                {
        //                    ds.ReadXml(xmlSR);
        //                    ds.Tables[0].Columns.Add("EnquiryID");
        //                    ds.Tables[0].Columns.Add("EnquiryResultID");
        //                    ds.Tables[0].Columns.Add("Reference");

        //                    if (ds.Tables.Contains("CommercialDetails"))
        //                    {

        //                        // File.AppendAllText(@"C:\Log\MTNCom.txt", "Step1");
        //                        int count = 0;

        //                        count = ds.Tables[0].Rows.Count;
        //                        // File.AppendAllText(@"C:\Log\MTNCom.txt", "Step3");
        //                        oBSearchResponse.BusinessSearchResult.SearchResponse.SearchResponse = new XDSPortalLibrary.Entity_Layer.BusinessSearchResponseBusinessSearchResultSearchResponseSearchResponse[count];

        //                        count = 0;

        //                        foreach (DataRow r in ds.Tables["CommercialDetails"].Rows)
        //                        {

        //                            //File.AppendAllText(@"C:\Log\MTNCom.txt", "Step2");
        //                            eSC.SearchOutput = "";
        //                            eSC.BusBusinessName = "";
        //                            eSC.BusRegistrationNo = "";
        //                            eSC.IDNo = "";
        //                            eSC.PassportNo = "";
        //                            eSC.Surname = "";
        //                            eSC.FirstName = "";
        //                            eSC.BirthDate = DateTime.Parse("1900/01/01");
        //                            eSC.Gender = "";
        //                            eSC.CreatedByUser = eSe.CreatedByUser;
        //                            eSC.CreatedOnDate = DateTime.Now;
        //                            eSC.SearchOutput = "";

        //                            if (r.Table.Columns.Contains("BusinessName"))
        //                            {
        //                                if (!string.IsNullOrEmpty(r["BusinessName"].ToString()))
        //                                {
        //                                    eSC.BusBusinessName = r["BusinessName"].ToString().Replace("'", "''");
        //                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusBusinessName;
        //                                }
        //                            }

        //                            if (r.Table.Columns.Contains("RegistrationNo"))
        //                            {
        //                                if (!string.IsNullOrEmpty(r["RegistrationNo"].ToString()))
        //                                {
        //                                    eSC.BusRegistrationNo = r["RegistrationNo"].ToString().Replace("'", "''");
        //                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusRegistrationNo;
        //                                }
        //                            }

        //                            if (eSC.SearchOutput.Length > 0)
        //                            {
        //                                eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
        //                            }

        //                            eSC.KeyID = int.Parse(r["CommercialID"].ToString());
        //                            eSC.KeyType = rp.ResponseKeyType;

        //                            eSC.BillingTypeID = spr.BillingTypeID;
        //                            eSC.BillingPrice = spr.UnitPrice;

        //                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
        //                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
        //                            eSC.ExtraVarOutput1 = "";
        //                            eSC.ProductID = intProductId;
        //                            eSC.VoucherCode = strVoucherCode;

        //                            string BonusXML = string.Empty;
        //                            BonusXML = r["BonusXML"].ToString();
        //                            if (BonusXML != string.Empty)
        //                            {
        //                                DataSet dsBonus = new DataSet();
        //                                System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
        //                                dsBonus.ReadXml(BonusxmlSR);
        //                                if (dsBonus.Tables.Contains("Segments"))
        //                                {

        //                                    eSC.BonusIncluded = true;
        //                                    eSC.XMLBonus = BonusXML.Replace("'", "''");

        //                                    intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
        //                                    rp.EnquiryResultID = intSubscriberEnquiryResultID;

        //                                    foreach (DataRow dr in dsBonus.Tables[0].Rows)
        //                                    {
        //                                        eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;

        //                                        eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
        //                                        eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
        //                                        eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
        //                                        eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
        //                                        eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
        //                                        eSB.Billable = false;
        //                                        eSB.CreatedByUser = eSe.CreatedByUser;
        //                                        eSB.CreatedOnDate = DateTime.Now;
        //                                        // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
        //                                        dSB.InsertSubscriberEnquiryResultBonus(con, eSB);

        //                                    }

        //                                }
        //                                dsBonus.Dispose();
        //                            }
        //                            else
        //                            {
        //                                // Insert the match results to SubscriberEnquiryResult Table
        //                                intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
        //                                rp.EnquiryID = eSC.SubscriberEnquiryID;

        //                                XDSPortalLibrary.Entity_Layer.BusinessSearchResponseBusinessSearchResultSearchResponseSearchResponse oSearchResponse = new XDSPortalLibrary.Entity_Layer.BusinessSearchResponseBusinessSearchResultSearchResponseSearchResponse();
        //                                oSearchResponse.Name = r["BusinessName"].ToString();
        //                                oSearchResponse.NameType = "B";
        //                                oSearchResponse.PhysicalAddress = r["PhysicalAddress"].ToString();
        //                                oSearchResponse.Suburb = r["Suburb"].ToString();
        //                                oSearchResponse.Town = r["Town"].ToString();
        //                                oSearchResponse.Country = r["Country"].ToString();
        //                                oSearchResponse.PostCode = r["PostCode"].ToString();
        //                                oSearchResponse.PostalAddress = r["PostalAddress"].ToString();
        //                                oSearchResponse.PostalSuburb = r["PostalSuburb"].ToString();
        //                                oSearchResponse.PostalTown = r["PostalTown"].ToString();
        //                                oSearchResponse.PostalCountry = r["PostalCountry"].ToString();
        //                                oSearchResponse.PostalPostCode = r["PostalPostCode"].ToString();
        //                                oSearchResponse.PhoneNo = r["PhoneNo"].ToString();
        //                                oSearchResponse.FaxNo = r["FaxNo"].ToString();
        //                                oSearchResponse.RegNo = r["RegistrationNo12"].ToString();
        //                                oSearchResponse.RegStatus = r["RegStatus"].ToString();
        //                                oSearchResponse.RegStatusCode = r["RegStatusCode"].ToString();
        //                                oSearchResponse.TUNumber = intSubscriberEnquiryID.ToString() + ":" + intSubscriberEnquiryResultID.ToString();

        //                                oBSearchResponse.BusinessSearchResult.SearchResponse.SearchResponse[count] = oSearchResponse;

        //                                count++;

        //                            }
        //                            eSC.XMLBonus = null;
        //                            eSC.BonusIncluded = false;

        //                            eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

        //                            ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
        //                            ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
        //                            ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

        //                            rp.ResponseData = ds.GetXml();
        //                        }
        //                    }
        //                }
        //                else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
        //                {
        //                    ds.ReadXml(xmlSR);
        //                    ds.Tables[0].Columns.Add("EnquiryID");
        //                    ds.Tables[0].Columns.Add("EnquiryResultID");
        //                    ds.Tables[0].Columns.Add("Reference");

        //                    if (ds.Tables.Contains("CommercialDetails"))
        //                    {
        //                        foreach (DataRow r in ds.Tables["CommercialDetails"].Rows)
        //                        {
        //                            eSC.SearchOutput = "";
        //                            eSC.BusBusinessName = "";
        //                            eSC.BusRegistrationNo = "";
        //                            eSC.IDNo = "";
        //                            eSC.PassportNo = "";
        //                            eSC.Surname = "";
        //                            eSC.FirstName = "";
        //                            eSC.BirthDate = DateTime.Parse("1900/01/01");
        //                            eSC.Gender = "";
        //                            eSC.CreatedByUser = eSe.CreatedByUser;
        //                            eSC.CreatedOnDate = DateTime.Now;
        //                            eSC.SearchOutput = "";

        //                            if (r.Table.Columns.Contains("BusinessName"))
        //                            {
        //                                if (!string.IsNullOrEmpty(r["BusinessName"].ToString()))
        //                                {
        //                                    eSC.BusBusinessName = r["BusinessName"].ToString().Replace("'", "''");
        //                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusBusinessName;
        //                                }
        //                            }

        //                            if (r.Table.Columns.Contains("RegistrationNo"))
        //                            {
        //                                if (!string.IsNullOrEmpty(r["RegistrationNo"].ToString()))
        //                                {
        //                                    eSC.BusRegistrationNo = r["RegistrationNo"].ToString().Replace("'", "''");
        //                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusRegistrationNo;
        //                                }
        //                            }

        //                            if (eSC.SearchOutput.Length > 0)
        //                            {
        //                                eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
        //                            }

        //                            eSC.KeyID = rp.ResponseKey;
        //                            eSC.KeyType = rp.ResponseKeyType;

        //                            eSC.BillingTypeID = spr.BillingTypeID;
        //                            eSC.BillingPrice = spr.UnitPrice;

        //                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();
        //                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
        //                            eSC.ExtraVarOutput1 = "";
        //                            eSC.ProductID = intProductId;
        //                            eSC.VoucherCode = strVoucherCode;

        //                            string BonusXML = string.Empty;
        //                            BonusXML = r["BonusXML"].ToString();
        //                            if (BonusXML != string.Empty)
        //                            {
        //                                DataSet dsBonus = new DataSet();
        //                                System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
        //                                dsBonus.ReadXml(BonusxmlSR);
        //                                if (dsBonus.Tables.Contains("Segments"))
        //                                {


        //                                    eSC.BonusIncluded = true;
        //                                    eSC.XMLBonus = BonusXML.Replace("'", "''");

        //                                    intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
        //                                    rp.EnquiryResultID = intSubscriberEnquiryResultID;

        //                                    foreach (DataRow dr in dsBonus.Tables[0].Rows)
        //                                    {
        //                                        eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;

        //                                        eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
        //                                        eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
        //                                        eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
        //                                        eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
        //                                        eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
        //                                        eSB.Billable = false;
        //                                        eSB.CreatedByUser = eSe.CreatedByUser;
        //                                        eSB.CreatedOnDate = DateTime.Now;
        //                                        // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
        //                                        dSB.InsertSubscriberEnquiryResultBonus(con, eSB);

        //                                    }
        //                                    dsBonus.Dispose();
        //                                }
        //                            }
        //                            else
        //                            {
        //                                // Insert the match results to SubscriberEnquiryResult Table
        //                                intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
        //                                rp.EnquiryID = eSC.SubscriberEnquiryID;

        //                            }
        //                            eSC.XMLBonus = null;
        //                            eSC.BonusIncluded = false;

        //                            eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

        //                            ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
        //                            ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
        //                            ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

        //                            rp.ResponseData = ds.GetXml();
        //                        }
        //                    }
        //                }

        //            }
        //        }
        //        else
        //        {
        //            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //            rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
        //        }

        //        con.Close();
        //        AdminConnection.Close();
        //        SqlConnection.ClearPool(con);
        //        SqlConnection.ClearPool(AdminConnection);

        //    }

        //    catch (Exception oException)
        //    {
        //        eSe.EnquiryResult = "E";
        //        eSe.ErrorDescription = oException.Message;
        //        eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
        //        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //        rp.ResponseData = oException.Message;


        //        MatchResult = eSe.SubscriberEnquiryID;
        //        if (con.State == ConnectionState.Open)
        //            con.Close();
        //        if (AdminConnection.State == ConnectionState.Open)
        //            AdminConnection.Close();
        //        SqlConnection.ClearPool(con);
        //        SqlConnection.ClearPool(AdminConnection);
        //    }

        //    ds.Dispose();

        //    return rp;
        //}

        public XDSPortalLibrary.Entity_Layer.Response SubmitBusinessEnquiryWithTracePlus(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string strRegNo1, string strRegNo2, string strRegNo3, string strBusinessName, string strVatNo, string strSolePropIDNo, string strTrustNo, string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            int intSubscriberEnquiryID = 0;
            int intSubscriberEnquiryResultID = 0;
            string rXml = "";
            int MatchResult = 0;
            DataSet ds = new DataSet();
            string strValidationStatus = "";
            Boolean boolSubmitEnquiry = true;


            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            try
            {
                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {


                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }


                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {

                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;
                        eSe.ProductID = intProductId;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = strExtRef;
                        eSe.SystemUserID = intSystemUserID;
                        if (string.IsNullOrEmpty(strRegNo1 + strRegNo2 + strRegNo3))
                        {
                            eSe.BusRegistrationNo = strRegNo1 + strRegNo2 + strRegNo3;
                        }
                        else
                        {
                            eSe.BusRegistrationNo = strTrustNo;
                        }
                        eSe.BusBusinessName = strBusinessName;
                        eSe.BusVatNumber = strVatNo;
                        eSe.IDNo = strSolePropIDNo;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";

                        //Generate serach input string

                        if (!string.IsNullOrEmpty(eSe.BusBusinessName))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.BusBusinessName;
                        }
                        if (!string.IsNullOrEmpty(eSe.BusRegistrationNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        }
                        if (!string.IsNullOrEmpty(eSe.BusVatNumber))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.BusVatNumber;
                        }
                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table
                        intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);
                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        eoCommercialEnquiryWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoCommercialEnquiryWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoCommercialEnquiryWseManager.BusinessName = strBusinessName;
                        eoCommercialEnquiryWseManager.ConfirmationChkBox = bConfirmationChkBox;
                        eoCommercialEnquiryWseManager.ExternalReference = strExtRef;
                        eoCommercialEnquiryWseManager.ProductID = intProductId;
                        eoCommercialEnquiryWseManager.RegistrationNo = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        eoCommercialEnquiryWseManager.subscriberID = intSubscriberID;
                        eoCommercialEnquiryWseManager.VATNumber = strVatNo;
                        eoCommercialEnquiryWseManager.SolePropIDNo = strSolePropIDNo;
                        eoCommercialEnquiryWseManager.TrustNo = strTrustNo;
                        eoCommercialEnquiryWseManager.BonusCheck = bBonusChecking;

                        // Submit data for matching 
                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.BusinessEnquiryTracePlusMatch(eoCommercialEnquiryWseManager);

                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.BusinessEnquiryMatch(eoCommercialEnquiryWseManager);
                        }

                        // Get Response data
                        rp.EnquiryID = intSubscriberEnquiryID;
                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);


                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            eSC.SearchOutput = "";
                            eSC.BusBusinessName = "";
                            eSC.BusRegistrationNo = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;
                            eSC.SearchOutput = "";
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();

                            dSC.InsertSubscriberEnquiryResult(con, eSC);
                            rp.EnquiryID = eSC.SubscriberEnquiryID;
                        }


                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);



                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            ds.ReadXml(xmlSR);
                            if (ds.Tables.Contains("CommercialCompanyInformation"))
                            {
                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, intProductId);

                                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);
                                rXml = ds.GetXml();
                                rp.ResponseData = rXml;

                                foreach (DataRow r in ds.Tables["CommercialCompanyInformation"].Rows)
                                {

                                    eSC.SearchOutput = "";
                                    eSC.BusBusinessName = "";
                                    eSC.BusRegistrationNo = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    if (sub.PayAsYouGo == 1 || !(strVoucherCode == string.Empty))
                                    {
                                        eSC.Billable = false;
                                    }
                                    else
                                    {
                                        eSC.Billable = true;
                                    }

                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("CommercialName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["CommercialName"].ToString()))
                                        {
                                            eSC.BusBusinessName = r["CommercialName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusBusinessName;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("RegistrationNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["RegistrationNo"].ToString()))
                                        {
                                            eSC.BusRegistrationNo = r["RegistrationNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusRegistrationNo;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    // Change the PayAsYouGoEnquiryLimit  after viewing the report
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                                    dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                }
                            }
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("CommercialDetails"))
                            {
                                foreach (DataRow r in ds.Tables["CommercialDetails"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.BusBusinessName = "";
                                    eSC.BusRegistrationNo = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("BusinessName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["BusinessName"].ToString()))
                                        {
                                            eSC.BusBusinessName = r["BusinessName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusBusinessName;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("RegistrationNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["RegistrationNo"].ToString()))
                                        {
                                            eSC.BusRegistrationNo = r["RegistrationNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusRegistrationNo;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.KeyID = int.Parse(r["CommercialID"].ToString());
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {

                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");

                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;

                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;
                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);

                                            }

                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {
                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("CommercialDetails"))
                            {
                                foreach (DataRow r in ds.Tables["CommercialDetails"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.BusBusinessName = "";
                                    eSC.BusRegistrationNo = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("BusinessName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["BusinessName"].ToString()))
                                        {
                                            eSC.BusBusinessName = r["BusinessName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusBusinessName;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("RegistrationNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["RegistrationNo"].ToString()))
                                        {
                                            eSC.BusRegistrationNo = r["RegistrationNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusRegistrationNo;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {


                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");

                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;

                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;
                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);

                                            }
                                            dsBonus.Dispose();
                                        }
                                    }
                                    else
                                    {
                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }
                        }

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }

            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;


                MatchResult = eSe.SubscriberEnquiryID;
                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }

            ds.Dispose();

            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitBusinessEnquiryLinkages(SqlConnection con, SqlConnection AdminConnection, SqlConnection LogConnection,int InvestigationID, string searchtype,int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string strRegNo1, string strRegNo2, string strRegNo3, string strBusinessName, string strIDno, string strFirstname, string strSurname, string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode)
        {

            string strAdminCon = AdminConnection.ConnectionString;
            string strLogCon = LogConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            int intSubscriberEnquiryID = 0;
            int intSubscriberEnquiryResultID = 0;
            string rXml = "";
            int MatchResult = 0;
            DataSet ds = new DataSet();
            string strValidationStatus = "";
            Boolean boolSubmitEnquiry = true;


            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);
          

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            Entity.SubscriberLinkageInvestigationDetails sli = new Entity.SubscriberLinkageInvestigationDetails();
            Data.XDSLog olog = new Data.XDSLog();

            try
            {
                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {
                    sli = olog.GetSubscriberLinkageInvestigation(strLogCon, InvestigationID);

                    if (sli != null)
                    {
                        if (sli.InvestigationID != 0)
                        {
                            if (sli.InvestigationStatus != Entity.SubscriberLinkageInvestigationDetails.StatusInd.C)
                            {
                                if (!string.IsNullOrEmpty(strVoucherCode))
                                {
                                    SubscriberVoucher sv = new SubscriberVoucher();
                                    strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                                    if (strValidationStatus == "")
                                    {
                                        throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                                    }
                                    else if (!(strValidationStatus == "1"))
                                    {
                                        throw new Exception(strValidationStatus);
                                    }
                                }
                                else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                                {
                                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                                    rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                                    boolSubmitEnquiry = false;

                                }

                            }
                            else
                            {
                                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                                rp.ResponseData = "This Investigation Completed already, please initiate a new one";
                                boolSubmitEnquiry = false;
                            }
                        }
                        else
                        {
                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "The Investigation reference provided can not be found";
                            boolSubmitEnquiry = false;

                        }
                    }
                    else 
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "The Investigation reference provided can not be found";
                        boolSubmitEnquiry = false;

                    }


                    if (boolSubmitEnquiry)
                    {

                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;
                        eSe.ProductID = intProductId;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = strExtRef;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.AccountNo = searchtype;
                        eSe.TelephoneNo = InvestigationID.ToString();
                        if (string.IsNullOrEmpty(strRegNo1 + strRegNo2 + strRegNo3))
                        {
                            eSe.BusRegistrationNo = strRegNo1 + strRegNo2 + strRegNo3;
                        }
                       
                        eSe.BusBusinessName = strBusinessName;
                        eSe.IDNo = strIDno;
                        eSe.FirstName = strFirstname;
                        eSe.Surname = strSurname;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";
                        if (!string.IsNullOrEmpty(strRegNo1 + strRegNo2 + strRegNo3))
                        {
                            eSe.BusRegistrationNo = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        }

                        //Generate serach input string
                        if (!string.IsNullOrEmpty(sli.InvestigationReference))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + sli.InvestigationReference;
                        }
                        if (!string.IsNullOrEmpty(eSe.BusBusinessName))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.BusBusinessName;
                        }
                        if (!string.IsNullOrEmpty(eSe.BusRegistrationNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        }
                        if (!string.IsNullOrEmpty(eSe.BusVatNumber))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.BusVatNumber;
                        }
                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table
                        intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);
                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        eoCommercialEnquiryWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoCommercialEnquiryWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoCommercialEnquiryWseManager.BusinessName = strBusinessName;
                        eoCommercialEnquiryWseManager.ConfirmationChkBox = bConfirmationChkBox;
                        eoCommercialEnquiryWseManager.ExternalReference = strExtRef;
                        eoCommercialEnquiryWseManager.ProductID = intProductId;
                        eoCommercialEnquiryWseManager.RegistrationNo = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        eoCommercialEnquiryWseManager.subscriberID = intSubscriberID;
                      

                        // Submit data for matching 
                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.BusinessEnquiryMatchPlus(eoCommercialEnquiryWseManager);

                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.BusinessEnquiryMatchPlus(eoCommercialEnquiryWseManager);
                        }

                        // Get Response data
                        rp.EnquiryID = intSubscriberEnquiryID;
                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);


                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            eSC.SearchOutput = "";
                            eSC.BusBusinessName = "";
                            eSC.BusRegistrationNo = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;
                            eSC.SearchOutput = "";
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();

                            dSC.InsertSubscriberEnquiryResult(con, eSC);
                            rp.EnquiryID = eSC.SubscriberEnquiryID;
                        }


                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);



                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            ds.ReadXml(xmlSR);
                            if (ds.Tables.Contains("CommercialCompanyInformation"))
                            {
                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, intProductId);

                                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);
                                rXml = ds.GetXml();
                                rp.ResponseData = rXml;

                                foreach (DataRow r in ds.Tables["CommercialCompanyInformation"].Rows)
                                {

                                    eSC.SearchOutput = "";
                                    eSC.BusBusinessName = "";
                                    eSC.BusRegistrationNo = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    if (sub.PayAsYouGo == 1 || !(strVoucherCode == string.Empty))
                                    {
                                        eSC.Billable = false;
                                    }
                                    else
                                    {
                                        eSC.Billable = true;
                                    }

                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("CommercialName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["CommercialName"].ToString()))
                                        {
                                            eSC.BusBusinessName = r["CommercialName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusBusinessName;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("RegistrationNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["RegistrationNo"].ToString()))
                                        {
                                            eSC.BusRegistrationNo = r["RegistrationNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusRegistrationNo;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    // Change the PayAsYouGoEnquiryLimit  after viewing the report
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                                    dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                }
                            }
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("EntityDetails"))
                            {
                                foreach (DataRow r in ds.Tables["EntityDetails"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.BusBusinessName = "";
                                    eSC.BusRegistrationNo = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";
                                    

                                    if (r.Table.Columns.Contains("EntityName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["EntityName"].ToString()))
                                        {
                                            eSC.BusBusinessName = r["EntityName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusBusinessName;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("EntityNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["EntityNo"].ToString()))
                                        {
                                            eSC.BusRegistrationNo = r["EntityNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusRegistrationNo;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("EntityType"))
                                    {
                                        if (!string.IsNullOrEmpty(r["EntityType"].ToString()))
                                        {
                                            eSC.FirstName = r["EntityType"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("EntityStatus"))
                                    {
                                        if (!string.IsNullOrEmpty(r["EntityStatus"].ToString()))
                                        {
                                            eSC.PassportNo = r["EntityStatus"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.SecondName = r["EntityCategory"].ToString();
                                    eSC.KeyID = int.Parse(r["EntityID"].ToString());
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;
                                   

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {

                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");

                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;

                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;
                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);

                                            }

                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {
                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);

                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;
                                    

                                    rp.ResponseData = ds.GetXml();
                                }

                                ds.Tables["EntityDetails"].Columns.Remove("EntityStatus");
                                ds.Tables["EntityDetails"].Columns.Remove("EntityType");
                            }
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("CommercialDetails"))
                            {
                                foreach (DataRow r in ds.Tables["CommercialDetails"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.BusBusinessName = "";
                                    eSC.BusRegistrationNo = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("BusinessName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["BusinessName"].ToString()))
                                        {
                                            eSC.BusBusinessName = r["BusinessName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusBusinessName;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("RegistrationNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["RegistrationNo"].ToString()))
                                        {
                                            eSC.BusRegistrationNo = r["RegistrationNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusRegistrationNo;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {


                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");

                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;

                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;
                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);

                                            }
                                            dsBonus.Dispose();
                                        }
                                    }
                                    else
                                    {
                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }
                        }

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }

            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;


                MatchResult = eSe.SubscriberEnquiryID;
                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }

            ds.Dispose();

            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response StartInvestigation(SqlConnection con, SqlConnection AdminConnection, SqlConnection logConnection, int intProductID, string strSubscribername, int intSubscriberID, int intSystemuserId, string strSystemusername, string strinvestigationReference, string strYourReference, string strVoucherCode)
        {
            string strAdminCon = AdminConnection.ConnectionString;
            string strlogCon = logConnection.ConnectionString;
            string strValidationStatus = string.Empty;
            double Totalcost = 0;
            string rXml = "";
            DataSet ds = new DataSet();


            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

          
            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            Entity.SubscriberLinkageInvestigationDetails sli = new Entity.SubscriberLinkageInvestigationDetails();
            Data.XDSLog olog = new Data.XDSLog();

            try
            {              
               
                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemuserId);
         
                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {

                    // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML 
                    // use this XML data for generating the Report 

                    if (spr.ReportID > 0 )
                    {                     
                        if (!string.IsNullOrEmpty(strVoucherCode))
                            {                              
                                    SubscriberVoucher sv = new SubscriberVoucher();
                                    strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                                    if (strValidationStatus == "")
                                    {
                                        throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                                    }
                                    else if (!(strValidationStatus == "1"))
                                    {
                                        throw new Exception(strValidationStatus);
                                    }
                                }                            
                            else if (sub.PayAsYouGo == 1)
                            {
                                //Calculate The Total cost of the Report , including Bonus Segments

                                Totalcost = spr.UnitPrice;                              
                            }


                            if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                            {
                              sli.SubscriberID = sys.SubscriberID;
                                sli.SubscriberName = sub.SubscriberName;
                                sli.SystemuserID = sys.SystemUserID;
                                sli.SystemUsername = sys.FirstName;
                                sli.InvestigationReference = strinvestigationReference;
                                sli.InvestigationStatus = Entity.SubscriberLinkageInvestigationDetails.StatusInd.P;
                                sli.SubscriberReference = strYourReference;
                                sli.CreatedbyUser = sys.Username;
                                sli.Createdondate = DateTime.Now;
                                sli.InvestigationID = olog.InsertSubscriberLinkageInvestigation(strlogCon,sli);

                               

                              
                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                            rp.ResponseData = "Intiated investigation successfully, please use the reference supplied for the subsequent calls.";
                            rp.EnquiryLogID = sli.InvestigationID;
                           
                        }
                    }
                    else
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "You are not authorized for this Investigation. Please contact XDS to activate";
                    }

                }

                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";

                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }

            catch (Exception oException)
            {
                sli.ErrorDescription = oException.Message.ToString();
                olog.InsertSubscriberLinkageInvestigationError(strlogCon, sli);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message; //"Error while Processing the Request at XDS";

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;

        }

        public XDSPortalLibrary.Entity_Layer.Response EndInvestigation(SqlConnection con, SqlConnection AdminConnection, SqlConnection logConnection, int intProductID, string strSubscribername, int intSubscriberID, int intSystemuserId, string strSystemusername, int intinvestigationReferenceID, string strYourReference, string strVoucherCode)
        {
            string strAdminCon = AdminConnection.ConnectionString;
            string strlogCon = logConnection.ConnectionString;
            string strValidationStatus = string.Empty;
            double Totalcost = 0;
            string rXml = "";
            DataSet ds = new DataSet();


            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();


            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            Entity.SubscriberLinkageInvestigationDetails sli = new Entity.SubscriberLinkageInvestigationDetails();
            Data.XDSLog olog = new Data.XDSLog();

            try
            {

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSubscriberID);

                // Check if this subscriber is authorized to use the current product
               
                    // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML 
                    // use this XML data for generating the Report 

                    if (spr.ReportID > 0)
                    {
                        if (!string.IsNullOrEmpty(strVoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }
                        }
                        else if (sub.PayAsYouGo == 1)
                        {
                            //Calculate The Total cost of the Report , including Bonus Segments

                            Totalcost = spr.UnitPrice;
                        }


                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {
                           // File.AppendAllText(@"C:\Log\Linkageerror.txt","step enter here\n");
                            sli = olog.GetSubscriberLinkageInvestigation(strlogCon, intinvestigationReferenceID);
                           // File.AppendAllText(@"C:\Log\Linkageerror.txt", "step enter finished\n");
                            if (sli != null)
                            {
                                if (sli.InvestigationStatus != Entity.SubscriberLinkageInvestigationDetails.StatusInd.C)
                                {

                                    sli.InvestigationStatus = Entity.SubscriberLinkageInvestigationDetails.StatusInd.C;
                                    sli.SubscriberReference = strYourReference;
                                    sli.CreatedbyUser = sys.Username;
                                    sli.ErrorDescription = string.IsNullOrEmpty(sli.ErrorDescription) ? string.Empty : sli.ErrorDescription;
                                  //  File.AppendAllText(@"C:\Log\Linkageerror.txt", "step Update enter here\n");
                                    olog.UpdateSubscriberLinkageInvestigation(strlogCon, sli);
                                  //  File.AppendAllText(@"C:\Log\Linkageerror.txt", "step update enter finished\n");


                                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                                    rp.ResponseData = "Investigation closed successfully";
                                    rp.EnquiryLogID = sli.InvestigationID;
                                }
                                else
                                {
                                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                                    rp.ResponseData = "Investigation already Closed.";
                                }
                            }
                            else
                            {
                                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                                rp.ResponseData = "No Investigation Found with the Provided Reference.";
                            }

                        }
                  
                }

                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";

                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }

            catch (Exception oException)
            {
                sli.ErrorDescription = oException.Message.ToString();
                olog.InsertSubscriberLinkageInvestigationError(strlogCon, sli);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = "Error while Processing the Request at XDS";

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;

        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitFindLinks(SqlConnection con, SqlConnection AdminConnection, SqlConnection LogConnection, int intSystemuserID, int intProductID, int InvestigationID, XDSPortalEnquiry.Entity.EntityList ListofEntities,int intPrimaryEnquiryResultID, string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode)
        {

            string strAdminCon = AdminConnection.ConnectionString;
            string strLogCon = LogConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            int intSubscriberEnquiryID = 0;
            int intSubscriberEnquiryResultID = 0;
            string rXml = "";
            int MatchResult = 0;
            DataSet ds = new DataSet();
            string strValidationStatus = "";
            Boolean boolSubmitEnquiry = true;


            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemuserID);

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, sys.SubscriberID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, sub.SubscriberID, intProductID);


            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            Entity.SubscriberLinkageDetectorEnquiry sld = new Entity.SubscriberLinkageDetectorEnquiry();
            Entity.SubscriberLinkageInvestigationDetails sli = new Entity.SubscriberLinkageInvestigationDetails();
            Data.XDSLog olog = new Data.XDSLog();

            string strEnquiryID = string.Empty;
            string strEnquiryResultID = string.Empty;
            string strLinkID = string.Empty;
            string strPrimaryLinkID = string.Empty;
            string Searchinput = string.Empty;
            bool bcheckPrimaryEnquiryResultID = false;

            try
            {
                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {

                    sli = olog.GetSubscriberLinkageInvestigation(strLogCon, InvestigationID);

                    if (sli != null)
                    {
                        if (sli.InvestigationID != 0)
                        {
                            //if (sli.InvestigationStatus != Entity.SubscriberLinkageInvestigationDetails.StatusInd.C)
                            //{
                                if (!string.IsNullOrEmpty(strVoucherCode))
                                {
                                    SubscriberVoucher sv = new SubscriberVoucher();
                                    strValidationStatus = sv.ValidationStatus(AdminConnection, sys.SubscriberID, strVoucherCode);

                                    if (strValidationStatus == "")
                                    {
                                        throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                                    }
                                    else if (!(strValidationStatus == "1"))
                                    {
                                        throw new Exception(strValidationStatus);
                                    }
                                }
                                else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                                {
                                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                                    rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                                    boolSubmitEnquiry = false;

                                }

                                if (ListofEntities != null)
                                {
                                    if (ListofEntities.Entities != null)
                                    {
                                        if (ListofEntities.Entities.Count >=2)
                                        {

                                            //ListofEntities.Entities.Sort();

                                            //foreach (XDSPortalEnquiry.Entity.EntityReference eref in ListofEntities.Entities)
                                            //{
                                            //ListofEntities.SortedEntities.Add()
                                            //}

                                            SortedList<int, int> sortedIds =  new SortedList<int, int>();
                                          

                                            foreach (XDSPortalEnquiry.Entity.EntityReference eref in ListofEntities.Entities)
                                            {
                                                sortedIds.Add(eref.EnquiryResultID, eref.EnquiryID);

                                                eSe = dSe.GetSubscriberEnquiryObject(con, eref.EnquiryID);


                                                if (eSe.TelephoneNo.Trim() != InvestigationID.ToString())
                                                {
                                                    throw new Exception("Some/All entities does not belong to same investigation");
                                                }

                                                if (eref.EnquiryResultID == intPrimaryEnquiryResultID)
                                                {
                                                    bcheckPrimaryEnquiryResultID = true;
                                                }
                                              
                                            }

                                            if (!bcheckPrimaryEnquiryResultID)
                                            {
                                                throw new Exception("Invalid PrimaryEnquiryResultID supplied, should be one of the entities selected");
                                            }

                                            foreach (KeyValuePair<int, int> kvp in sortedIds)
                                            {
                                                strEnquiryID = strEnquiryID + "," + kvp.Value;
                                                strEnquiryResultID = strEnquiryResultID + "," + kvp.Key;

                                                strEnquiryID = strEnquiryID.Trim(new char[] { ',' });
                                                strEnquiryResultID = strEnquiryResultID.Trim(new char[] { ',' });
                                            }


                                            sld.LinkSubscriberEnquiryID = strEnquiryID;
                                            sld.LinkSubscriberEnquiryResultID = strEnquiryResultID;
                                            sld.InvestigationID = sli.InvestigationID;
                                            sld.PrimaryEnquiryResultID = intPrimaryEnquiryResultID;

                                            sld = olog.GetLinkageDetectorReport(strLogCon, sld);

                                            if (!string.IsNullOrEmpty(sld.Xmldata))
                                            {
                                                boolSubmitEnquiry = false;
                                                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                                                rp.ResponseData = sld.Xmldata;
                                            }                                           

                                        }
                                        else
                                        {
                                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                                            rp.ResponseData = "Atleast two entities must be selected to find the links";
                                            boolSubmitEnquiry = false;
                                        }

                                    }
                                    else
                                    {
                                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                                        rp.ResponseData = "Please supply valid list of entities";
                                        boolSubmitEnquiry = false;
                                    }
                                }
                                else
                                {
                                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                                    rp.ResponseData = "Please supply valid list of entities";
                                    boolSubmitEnquiry = false;
                                }
                            //}
                            //else
                            //{
                            //    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            //    rp.ResponseData = "This Investigation Completed already, please initiate a new one";
                            //    boolSubmitEnquiry = false;
                            //}
                        }
                        else
                        {
                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "The Investigation reference provided can not be found";
                            boolSubmitEnquiry = false;

                        }
                    }
                    else
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "The Investigation reference provided can not be found";
                        boolSubmitEnquiry = false;

                    }


                    if (boolSubmitEnquiry)
                    
                    {

                    
                         if (sli.InvestigationStatus == Entity.SubscriberLinkageInvestigationDetails.StatusInd.C)
                         {
                             throw new Exception("This Investigation Completed already, please initiate a new one");
                         }
                            
                        foreach (XDSPortalEnquiry.Entity.EntityReference eref in ListofEntities.Entities)
                        {

                            eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, eref.EnquiryResultID);
                            strLinkID = strLinkID + "," + eSC.KeyType + eSC.KeyID +"-"+eSC.SubscriberEnquiryID+"-"+eSC.SubscriberEnquiryResultID;

                            strLinkID = strLinkID.Trim(new char[] { ',' });

                            if (!string.IsNullOrEmpty(eSC.BusRegistrationNo))
                            {
                                Searchinput = Searchinput + "|" + eSC.BusRegistrationNo;
                            }
                            if (eref.EnquiryResultID == intPrimaryEnquiryResultID)
                            {
                                strPrimaryLinkID = eSC.KeyType + eSC.KeyID;
                            }

                            Searchinput = Searchinput.Trim(new char['|']);
                        }

                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = sys.SubscriberID;
                        eSe.ProductID = intProductID;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = sub.SubscriberName;
                        eSe.SubscriberReference = strExtRef;
                        eSe.SystemUserID = intSystemuserID;
                        eSe.TelephoneNo = InvestigationID.ToString();

                        eSe.BusBusinessName = strLinkID;
                        eSe.FirstName = strEnquiryID;
                        eSe.Surname = strEnquiryResultID;
                        eSe.SystemUserID = sys.SystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SecondName = (sli.InvestigationReference + "|" + Searchinput).Trim(new char['|']);
                        eSe.SearchInput = "";
                        eSe.AccountNo = intPrimaryEnquiryResultID.ToString();
                        eSe.BranchCode = strPrimaryLinkID;

                        //Generate serach input string

                        if (!string.IsNullOrEmpty(eSe.BusBusinessName))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.BusBusinessName;
                        }


                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table
                        intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);
                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        
                        eoCommercialEnquiryWseManager.BusinessName = strLinkID;
                        eoCommercialEnquiryWseManager.ExternalReference = strExtRef;
                        eoCommercialEnquiryWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoCommercialEnquiryWseManager.RegistrationNo = strPrimaryLinkID;

                        // Submit data for matching 
                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.GetCommercialLinkagesDetectorReport(eoCommercialEnquiryWseManager);

                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.GetCommercialLinkagesDetectorReport(eoCommercialEnquiryWseManager);
                        }

                        // Get Response data
                        rp.EnquiryID = intSubscriberEnquiryID;
                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);


                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            ds.ReadXml(xmlSR);
                            //eSC.SearchOutput = "";
                            //eSC.BusBusinessName = "";
                            //eSC.BusRegistrationNo = "";
                            //eSC.IDNo = "";
                            //eSC.PassportNo = "";
                            //eSC.Surname = "";
                            //eSC.FirstName = "";
                            //eSC.BirthDate = DateTime.Parse("1900/01/01");
                            //eSC.Gender = "";
                            //eSC.CreatedByUser = eSe.CreatedByUser;
                            //eSC.CreatedOnDate = DateTime.Now;
                            //eSC.SearchOutput = "";
                            //eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            //eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();

                            //dSC.InsertSubscriberEnquiryResult(con, eSC);
                            //rp.EnquiryID = eSC.SubscriberEnquiryID;

                            //rp.ResponseData = "<FindLinksResult><NoofLinksFound>0</NoofLinksFound><EnquiryID>" + rp.EnquiryID + "</EnquiryID><EnquiryResultID>" + intSubscriberEnquiryResultID + "</EnquiryResultID></FindLinksResult>";

                            //eSC.SearchOutput = sli.InvestigationReference;
                            eSC.BusBusinessName = "";
                            eSC.BusRegistrationNo = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;
                            // eSC.SearchOutput = "";



                            eSC.KeyID = rp.ResponseKey;
                            eSC.KeyType = rp.ResponseKeyType;

                            //eSC.BillingTypeID = spr.BillingTypeID;
                            //eSC.BillingPrice = spr.UnitPrice;

                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.ExtraVarOutput1 = "";
                            eSC.ProductID = intProductID;
                            eSC.VoucherCode = strVoucherCode;
                            eSC.XMLData = rp.ResponseData;
                            eSC.DetailsViewedYN = false;
                            eSC.SearchOutput = (sli.InvestigationReference + "|" + Searchinput).Trim(new char['|']); ;

                            // Insert the match results to SubscriberEnquiryResult Table
                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                            eSC.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                            dSC.UpdateSubscriberEnquiryResult(con, eSC);

                            rp.EnquiryID = eSC.SubscriberEnquiryID;


                            eSC.XMLBonus = null;
                            eSC.BonusIncluded = false;

                            // eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);                                  


                            DataTable dt = new DataTable("FindLinksResult");

                            DataColumn dc1 = new DataColumn("NoofLinksFound");
                            DataColumn dc2 = new DataColumn("EnquiryID");
                            DataColumn dc3 = new DataColumn("EnquiryResultID");

                            dt.Columns.Add(dc1);
                            dt.Columns.Add(dc2);
                            dt.Columns.Add(dc3);

                            DataRow dr = dt.NewRow();

                            dr["EnquiryID"] = rp.EnquiryID;
                            dr["EnquiryResultID"] = intSubscriberEnquiryResultID;


                            dt.Rows.Add(dr);
                            ds.Tables.Add(dt);


                            if (ds.Tables.Contains("LinkageDetails"))
                            {
                                if (ds.Tables.Contains("LinkageDetails"))
                                {
                                    // rp.ResponseData = "<FindLinksResult><NoofLinksFound>" + ds.Tables["LinkageDetails"].Rows.Count.ToString() + "</NoofLinksFound><EnquiryID>" + rp.EnquiryID + "</EnquiryID><EnquiryResultID>" + intSubscriberEnquiryResultID + "</EnquiryResultID></FindLinksResult>";
                                    dr["NoofLinksFound"] = ds.Tables["LinkageDetails"].Rows.Count.ToString();
                                    ds.Tables.Remove("LinkageDetails");


                                }


                            }
                            else
                            {
                                // rp.ResponseData = "<FindLinksResult><NoofLinksFound>0</NoofLinksFound><EnquiryID>" + rp.EnquiryID + "</EnquiryID><EnquiryResultID>" + intSubscriberEnquiryResultID + "</EnquiryResultID></FindLinksResult>";
                                dr["NoofLinksFound"] = "0";
                            }

                            rp.ResponseData = ds.GetXml();
                        }


                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);



                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            ds.ReadXml(xmlSR);
                            if (ds.Tables.Contains("LinkageDetails"))
                            {
                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, intProductID);

                                DataTable dtSubscriberInput = new DataTable("InvestigationDetails");
                                dtSubscriberInput.Columns.Add("InvestigationStartDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("ReportDate", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["ReportDate"] = DateTime.Now;
                                drSubscriberInput["InvestigationStartDate"] = DateTime.Parse(sli.Createdondate.ToString(), System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.AssumeLocal);
                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);
                                rXml = ds.GetXml();
                                rp.ResponseData = rXml;

                               

                                    eSC.SearchOutput = "";
                                    eSC.BusBusinessName = "";
                                    eSC.BusRegistrationNo = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    if (sub.PayAsYouGo == 1 || !(strVoucherCode == string.Empty))
                                    {
                                        eSC.Billable = false;
                                    }
                                    else
                                    {
                                        eSC.Billable = true;
                                    }

                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    
                                   

                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    // Change the PayAsYouGoEnquiryLimit  after viewing the report
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, sys.SubscriberID, spr.UnitPrice);
                                    dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                
                            }
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                        {                         

                            ds.ReadXml(xmlSR);
                            //ds.Tables[0].Columns.Add("EnquiryID");
                            //ds.Tables[0].Columns.Add("EnquiryResultID");
                            //ds.Tables[0].Columns.Add("Reference");

                            DataTable dtSubscriberInput = new DataTable("InvestigationDetails");
                            dtSubscriberInput.Columns.Add("InvestigationStartDate", typeof(String));
                            dtSubscriberInput.Columns.Add("ReportDate", typeof(String));
                            dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                            dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                            dtSubscriberInput.Columns.Add("InvestigationName", typeof(String));

                            DataRow drSubscriberInput;
                            drSubscriberInput = dtSubscriberInput.NewRow();

                            drSubscriberInput["ReportDate"] = DateTime.Now;
                            drSubscriberInput["InvestigationStartDate"] = DateTime.Parse(sli.Createdondate.ToString(), System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.AssumeLocal);
                            drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                            drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                            drSubscriberInput["InvestigationName"] = sli.InvestigationReference;


                            dtSubscriberInput.Rows.Add(drSubscriberInput);

                            ds.Tables.Add(dtSubscriberInput);
                            rXml = ds.GetXml();
                            rp.ResponseData = rXml;


                          //  eSC.SearchOutput = sli.InvestigationReference;
                            eSC.BusBusinessName = "";
                            eSC.BusRegistrationNo = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;
                           // eSC.SearchOutput = "";
                            eSC.SearchOutput = (sli.InvestigationReference + "|" + Searchinput).Trim(new char['|']); ;


                            eSC.KeyID = rp.ResponseKey;
                            eSC.KeyType = rp.ResponseKeyType;

                            //eSC.BillingTypeID = spr.BillingTypeID;
                            //eSC.BillingPrice = spr.UnitPrice;

                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.ExtraVarOutput1 = "";
                            eSC.ProductID = intProductID;
                            eSC.VoucherCode = strVoucherCode;
                            eSC.XMLData = rp.ResponseData;
                            eSC.DetailsViewedYN = false;
                            File.AppendAllText(@"C:\Log\123.txt", eSC.SearchOutput+"|\n");
                            File.AppendAllText(@"C:\Log\123.txt", (sli.InvestigationReference + "|" + Searchinput).Trim(new char['|'])+"|\n"); 
                            // Insert the match results to SubscriberEnquiryResult Table
                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                            eSC.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                          //  dSC.UpdateSubscriberEnquiryResult(con, eSC);

                            rp.EnquiryID = eSC.SubscriberEnquiryID;


                            eSC.XMLBonus = null;
                            eSC.BonusIncluded = false;

                            // eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);                                  


                            DataTable dt = new DataTable("FindLinksResult");

                            DataColumn dc1 = new DataColumn("NoofLinksFound");
                            DataColumn dc2 = new DataColumn("EnquiryID");
                            DataColumn dc3 = new DataColumn("EnquiryResultID");

                            dt.Columns.Add(dc1);
                            dt.Columns.Add(dc2);
                            dt.Columns.Add(dc3);

                            DataRow dr = dt.NewRow();

                            dr["EnquiryID"] = rp.EnquiryID;
                            dr["EnquiryResultID"] = intSubscriberEnquiryResultID;

                            dt.Rows.Add(dr);
                            ds.Tables.Add(dt);

                            if (ds.Tables.Contains("LinkageDetails"))
                            {
                                if (ds.Tables.Contains("LinkageDetails"))
                                {
                                    // rp.ResponseData = "<FindLinksResult><NoofLinksFound>" + ds.Tables["LinkageDetails"].Rows.Count.ToString() + "</NoofLinksFound><EnquiryID>" + rp.EnquiryID + "</EnquiryID><EnquiryResultID>" + intSubscriberEnquiryResultID + "</EnquiryResultID></FindLinksResult>";
                                    dr["NoofLinksFound"] = ds.Tables["LinkageDetails"].Rows.Count.ToString();
                                    ds.Tables.Remove("LinkageDetails");

                                    
                                }


                            }
                            else
                            {
                                // rp.ResponseData = "<FindLinksResult><NoofLinksFound>0</NoofLinksFound><EnquiryID>" + rp.EnquiryID + "</EnquiryID><EnquiryResultID>" + intSubscriberEnquiryResultID + "</EnquiryResultID></FindLinksResult>";
                                dr["NoofLinksFound"] = "0";
                            }

                            rp.ResponseData = ds.GetXml();

                            
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("CommercialDetails"))
                            {
                                foreach (DataRow r in ds.Tables["CommercialDetails"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.BusBusinessName = "";
                                    eSC.BusRegistrationNo = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("BusinessName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["BusinessName"].ToString()))
                                        {
                                            eSC.BusBusinessName = r["BusinessName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusBusinessName;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("RegistrationNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["RegistrationNo"].ToString()))
                                        {
                                            eSC.BusRegistrationNo = r["RegistrationNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusRegistrationNo;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductID;
                                    eSC.VoucherCode = strVoucherCode;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {


                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");

                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;

                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;
                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);

                                            }
                                            dsBonus.Dispose();
                                        }
                                    }
                                    else
                                    {
                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }
                        }

                        sld.InvestigationID = InvestigationID;
                        sld.LinkSubscriberEnquiryID = strEnquiryID;
                        sld.LinkSubscriberEnquiryResultID = strEnquiryResultID;
                        sld.ProductID = eSe.ProductID;
                        sld.Status = eSe.EnquiryStatus;
                        sld.SubscriberEnquiryID = rp.EnquiryID;
                        sld.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                        sld.Xmldata = rp.ResponseData;
                        sld.Createdondate = DateTime.Now;
                        sld.CreatedbyUser = sys.Username;
                        sld.LinkID = strLinkID;
                        sld.PrimaryEnquiryResultID = intPrimaryEnquiryResultID;

                        olog.InsertSubscriberLinkageDetectorEnquiry(strLogCon, sld);

                    }
                  

                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }

            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;


                MatchResult = eSe.SubscriberEnquiryID;
                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }

            ds.Dispose();

            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response ValidateBusinessSearchR(XDSPortalLibrary.Entity_Layer.BusinessSearch objInput)
        {
            int i = 0;
            float f = 0;
            XDSPortalLibrary.Entity_Layer.Response oResponse = new XDSPortalLibrary.Entity_Layer.Response();

            if (string.IsNullOrEmpty(objInput.BusinessSearch1.User))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Username is Mandatory";
            }
            else if (string.IsNullOrEmpty(objInput.BusinessSearch1.Password))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Password is Mandatory";
            }

            else if (string.IsNullOrEmpty(objInput.BusinessSearch1.SearchType))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "SearchType is Mandatory";
            }
            else if (!string.IsNullOrEmpty(objInput.BusinessSearch1.SearchType) && objInput.BusinessSearch1.SearchType.ToUpper() != XDSPortalLibrary.Entity_Layer.Business.BusinessSearchType.NAME.ToString() && objInput.BusinessSearch1.SearchType.ToUpper() != XDSPortalLibrary.Entity_Layer.Business.BusinessSearchType.REGNO.ToString() && objInput.BusinessSearch1.SearchType.ToUpper() != XDSPortalLibrary.Entity_Layer.Business.BusinessSearchType.VATNO.ToString())
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "SearchType can be either NAME,REGNO or VATNO";
            }
            else if (!string.IsNullOrEmpty(objInput.BusinessSearch1.SearchType) && objInput.BusinessSearch1.SearchType.ToUpper() == XDSPortalLibrary.Entity_Layer.Business.BusinessSearchType.NAME.ToString() && string.IsNullOrEmpty(objInput.BusinessSearch1.SubjectName))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "SubjectName is Mandatory for the selected SearchType";
            }
            else if (!string.IsNullOrEmpty(objInput.BusinessSearch1.SearchType) && objInput.BusinessSearch1.SearchType.ToUpper() == XDSPortalLibrary.Entity_Layer.Business.BusinessSearchType.REGNO.ToString() && string.IsNullOrEmpty(objInput.BusinessSearch1.RegistrationNo))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "RegistrationNo is Mandatory for the selected SearchType";
            }
            else if (!string.IsNullOrEmpty(objInput.BusinessSearch1.SearchType) && objInput.BusinessSearch1.SearchType.ToUpper() == XDSPortalLibrary.Entity_Layer.Business.BusinessSearchType.VATNO.ToString() && string.IsNullOrEmpty(objInput.BusinessSearch1.VatNumber))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "VatNumber is Mandatory for the selected SearchType";
            }
           

            return oResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response ValidateBusinessSearchRA(XDSADPMODELS.BusinessSearch objInput)
        {
            int i = 0;
            float f = 0;
            XDSPortalLibrary.Entity_Layer.Response oResponse = new XDSPortalLibrary.Entity_Layer.Response();

            if (string.IsNullOrEmpty(objInput.BusinessSearch1.User))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Username is Mandatory";
            }
            else if (string.IsNullOrEmpty(objInput.BusinessSearch1.Password))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Password is Mandatory";
            }

            else if (string.IsNullOrEmpty(objInput.BusinessSearch1.SearchType))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "SearchType is Mandatory";
            }
            else if (!string.IsNullOrEmpty(objInput.BusinessSearch1.SearchType) && objInput.BusinessSearch1.SearchType.ToUpper() != XDSPortalLibrary.Entity_Layer.Business.BusinessSearchType.NAME.ToString() && objInput.BusinessSearch1.SearchType.ToUpper() != XDSPortalLibrary.Entity_Layer.Business.BusinessSearchType.REGNO.ToString() && objInput.BusinessSearch1.SearchType.ToUpper() != XDSPortalLibrary.Entity_Layer.Business.BusinessSearchType.VATNO.ToString())
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "SearchType can be either NAME,REGNO or VATNO";
            }
            else if (!string.IsNullOrEmpty(objInput.BusinessSearch1.SearchType) && objInput.BusinessSearch1.SearchType.ToUpper() == XDSPortalLibrary.Entity_Layer.Business.BusinessSearchType.NAME.ToString() && string.IsNullOrEmpty(objInput.BusinessSearch1.SubjectName))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "SubjectName is Mandatory for the selected SearchType";
            }
            else if (!string.IsNullOrEmpty(objInput.BusinessSearch1.SearchType) && objInput.BusinessSearch1.SearchType.ToUpper() == XDSPortalLibrary.Entity_Layer.Business.BusinessSearchType.REGNO.ToString() && string.IsNullOrEmpty(objInput.BusinessSearch1.RegistrationNo))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "RegistrationNo is Mandatory for the selected SearchType";
            }
            else if (!string.IsNullOrEmpty(objInput.BusinessSearch1.SearchType) && objInput.BusinessSearch1.SearchType.ToUpper() == XDSPortalLibrary.Entity_Layer.Business.BusinessSearchType.VATNO.ToString() && string.IsNullOrEmpty(objInput.BusinessSearch1.VatNumber))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "VatNumber is Mandatory for the selected SearchType";
            }


            return oResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitBusinessEnquiryCustomMIE(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string strRegNo1, string strRegNo2, string strRegNo3)
        {

            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bool bBonusChecking = false;
            int intSubscriberEnquiryID = 0;
            int intSubscriberEnquiryResultID = 0;
            string rXml = "";
            int MatchResult = 0;
            DataSet ds = new DataSet();
            string strValidationStatus = "";
            Boolean boolSubmitEnquiry = true;


            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            try
            {
                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {


                    if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {

                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;
                        eSe.ProductID = intProductId;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = "";
                        eSe.SystemUserID = intSystemUserID;
                        if (string.IsNullOrEmpty(strRegNo1 + strRegNo2 + strRegNo3))
                        {
                            eSe.BusRegistrationNo = strRegNo1 + strRegNo2 + strRegNo3;
                        }
                        
                        eSe.BusBusinessName = "";
                        eSe.BusVatNumber = "";
                        eSe.IDNo = "";
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";
                        if (!string.IsNullOrEmpty(strRegNo1 + strRegNo2 + strRegNo3))
                        {
                            eSe.BusRegistrationNo = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        }

                        //Generate serach input string

                        if (!string.IsNullOrEmpty(eSe.BusBusinessName))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.BusBusinessName;
                        }
                        if (!string.IsNullOrEmpty(eSe.BusRegistrationNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        }
                        if (!string.IsNullOrEmpty(eSe.BusVatNumber))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.BusVatNumber;
                        }
                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table
                        intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);
                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        eoCommercialEnquiryWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoCommercialEnquiryWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoCommercialEnquiryWseManager.BusinessName = "";
                        eoCommercialEnquiryWseManager.ConfirmationChkBox = true;
                        eoCommercialEnquiryWseManager.ExternalReference = "";
                        eoCommercialEnquiryWseManager.ProductID = intProductId;
                        eoCommercialEnquiryWseManager.RegistrationNo = strRegNo1 + "/" + strRegNo2 + "/" + strRegNo3;
                        eoCommercialEnquiryWseManager.subscriberID = intSubscriberID;
                        eoCommercialEnquiryWseManager.VATNumber = "";
                        eoCommercialEnquiryWseManager.SolePropIDNo = "";
                        eoCommercialEnquiryWseManager.TrustNo = "";
                        eoCommercialEnquiryWseManager.BonusCheck = bBonusChecking;

                        // Submit data for matching 
                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.BusinessEnquiryMatchMIE(eoCommercialEnquiryWseManager);

                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.BusinessEnquiryMatchMIE(eoCommercialEnquiryWseManager);
                        }

                        // Get Response data
                        rp.EnquiryID = intSubscriberEnquiryID;
                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);


                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            eSC.SearchOutput = "";
                            eSC.BusBusinessName = "";
                            eSC.BusRegistrationNo = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;
                            eSC.SearchOutput = "";
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.ProductID = intProductId;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();

                            dSC.InsertSubscriberEnquiryResult(con, eSC);
                            rp.EnquiryID = eSC.SubscriberEnquiryID;
                        }


                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);
                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("CommercialDetails"))
                            {
                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, intProductId);

                                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);
                                rXml = ds.GetXml();
                                rp.ResponseData = rXml;

                                foreach (DataRow r in ds.Tables["CommercialDetails"].Rows)
                                {

                                    eSC.SearchOutput = "";
                                    eSC.BusBusinessName = "";
                                    eSC.BusRegistrationNo = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    if (sub.PayAsYouGo == 1)
                                    {
                                        eSC.Billable = false;
                                    }
                                    else
                                    {
                                        eSC.Billable = true;
                                    }

                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("Businessname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Businessname"].ToString()))
                                        {
                                            eSC.BusBusinessName = r["Businessname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusBusinessName;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("RegistrationNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["RegistrationNo"].ToString()))
                                        {
                                            eSC.BusRegistrationNo = r["RegistrationNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusRegistrationNo;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;
                                    eSC.ProductID = intProductId;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    // Change the PayAsYouGoEnquiryLimit  after viewing the report
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                                    dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);

                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, rp.EnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = eSC.SubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("CommercialDetails"))
                            {
                                foreach (DataRow r in ds.Tables["CommercialDetails"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.BusBusinessName = "";
                                    eSC.BusRegistrationNo = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("BusinessName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["BusinessName"].ToString()))
                                        {
                                            eSC.BusBusinessName = r["BusinessName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusBusinessName;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("RegistrationNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["RegistrationNo"].ToString()))
                                        {
                                            eSC.BusRegistrationNo = r["RegistrationNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusRegistrationNo;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = "";

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {

                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");

                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;

                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;
                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);

                                            }

                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {
                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("CommercialDetails"))
                            {
                                foreach (DataRow r in ds.Tables["CommercialDetails"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.BusBusinessName = "";
                                    eSC.BusRegistrationNo = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("BusinessName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["BusinessName"].ToString()))
                                        {
                                            eSC.BusBusinessName = r["BusinessName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusBusinessName;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("RegistrationNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["RegistrationNo"].ToString()))
                                        {
                                            eSC.BusRegistrationNo = r["RegistrationNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusRegistrationNo;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = "";

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {


                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");

                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;

                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;
                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);

                                            }
                                            dsBonus.Dispose();
                                        }
                                    }
                                    else
                                    {
                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }
                        }

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }

            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;


                MatchResult = eSe.SubscriberEnquiryID;
                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }

            ds.Dispose();

            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response ValidateModuleRequestNR(XDSPortalLibrary.Entity.VettingVRequest objInput)
        {
            XDSPortalLibrary.Entity_Layer.Response oResponse = new XDSPortalLibrary.Entity_Layer.Response();
            oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;


            if (string.IsNullOrEmpty(objInput.User) || string.IsNullOrEmpty(objInput.Password))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Username and Password are mandatory";
            }
            else if (objInput.PrincipalDetails == null)
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Atleast one princiapl information must be supplied";
            }
            else if (objInput.PrincipalDetails != null && objInput.PrincipalDetails.Length == 0)
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Atleast one princiapl information must be supplied";
            }
            else if (!string.IsNullOrEmpty(objInput.IsExistingClient) &&
               (objInput.IsExistingClient != "2" &&
               objInput.IsExistingClient != "1" &&
               objInput.IsExistingClient != "3"))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Valid values for IsExistingClient are 1,2 and 3 for Add Sim, Upgrade and New respectively";
            }
            else if (objInput.PrincipalDetails != null && objInput.PrincipalDetails.Length > 0)
            {
                foreach (XDSPortalLibrary.Entity.PrincipalInfo principalInfo in objInput.PrincipalDetails)
                {
                    if (principalInfo != null)
                    {
                        if (string.IsNullOrEmpty(principalInfo.Designation))
                        {
                            oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            oResponse.ResponseData = "Designation is Mandatory";
                        }
                        else if (string.IsNullOrEmpty(principalInfo.Surname))
                        {
                            oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            oResponse.ResponseData = "Surname is Mandatory";
                        }
                        else if (string.IsNullOrEmpty(principalInfo.FirstName))
                        {
                            oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            oResponse.ResponseData = "FirstName is Mandatory";
                        }
                        else if (string.IsNullOrEmpty(principalInfo.IDType))
                        {
                            oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            oResponse.ResponseData = "IDType is Mandatory";
                        }
                        else if (!string.IsNullOrEmpty(principalInfo.IDType) && principalInfo.IDType.ToUpper() != "1" && principalInfo.IDType.ToUpper() != "2")
                        {
                            oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            oResponse.ResponseData = "IDType can be either 1 for SAID or 2 for PASSPORT";
                        }
                        else if (string.IsNullOrEmpty(principalInfo.IdentityNumber))
                        {
                            oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            oResponse.ResponseData = "IDNumber is Mandatory";
                        }
                        else if (!string.IsNullOrEmpty(principalInfo.IdentityNumber) && principalInfo.IDType.ToUpper() == "1" && principalInfo.IdentityNumber.Length < 13)
                        {
                            oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            oResponse.ResponseData = "IDNumber supplied is Invalid";
                        }
                    }
                }
            }
           


            return oResponse;
        }

    }
}

