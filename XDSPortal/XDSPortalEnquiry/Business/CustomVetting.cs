using DevExpress.XtraReports.UI;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Security.Permissions;
using System.Text.RegularExpressions;
using System.Web.Services.Configuration;
using System.Web.Services.Protocols;
using System.Xml;
using System.Xml.Serialization;
using XDSPortalLibrary.Entity_Layer;

namespace XDSPortalEnquiry.Business
{
    public class CustomVetting
    {
        private XDSPortalLibrary.Business_Layer.ConsumerTrace moConsumerTraceWseManager;
        private XDSPortalLibrary.Entity_Layer.ConsumerTrace eoConsumerTraceWseManager;
        private XDSPortalLibrary.Business_Layer.ConsumerCreditEnquiry moConsumerCreditGrantorWseManager;
        private XDSPortalLibrary.Entity_Layer.ConsumerCreditEnquiry eoConsumerCreditGrantorWseManager;
        private XDSPortalLibrary.Business_Layer.CustomVetting moCustomVetting;

        public CustomVetting()
        {
            moConsumerTraceWseManager = new XDSPortalLibrary.Business_Layer.ConsumerTrace();
            eoConsumerTraceWseManager = new XDSPortalLibrary.Entity_Layer.ConsumerTrace();
            moConsumerCreditGrantorWseManager = new XDSPortalLibrary.Business_Layer.ConsumerCreditEnquiry();
            eoConsumerCreditGrantorWseManager = new XDSPortalLibrary.Entity_Layer.ConsumerCreditEnquiry();
            moCustomVetting = new XDSPortalLibrary.Business_Layer.CustomVetting();

            DataTable dt = new DataTable("Details");

            DataColumn dc1 = new DataColumn("IDNumber");
            DataColumn dc6 = new DataColumn("ErrorDescription");
            DataColumn dc2 = new DataColumn("Result");
            DataColumn dc3 = new DataColumn("FailReason");
            DataColumn dc4 = new DataColumn("Action");
            DataColumn dc5 = new DataColumn("MaxInstalMent");

            dt.Columns.Add(dc1);
            dt.Columns.Add(dc6);
            dt.Columns.Add(dc2);
            dt.Columns.Add(dc3);
            dt.Columns.Add(dc4);
            dt.Columns.Add(dc5);

            DataRow Dr = dt.NewRow();
            Dr[0] = string.Empty;
            Dr[1] = string.Empty;
            Dr[2] = string.Empty;
            Dr[3] = string.Empty;
            Dr[4] = string.Empty;
            Dr[5] = string.Empty;

            dt.Rows.Add(Dr);

            dsresult.Tables.Add(dt);
        }

        public XDSPortalLibrary.Entity_Layer.XDSCellCCustomVetting oXDSCellCCustomVetting = new XDSPortalLibrary.Entity_Layer.XDSCellCCustomVetting();

        public DataSet dsresult = new DataSet("VettingResult");

        public XDSPortalLibrary.Entity_Layer.Response SubmitConsumerTrace(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string IdNumber, string PassportNo, string FirstName, string Surname, string DateOfBirth, string Gender, string HomePhoneNumber, string WorkPhoneNumber, double MonthlySalary, string BankAccountType, string BankName, string AccountHolderName, string BranchCode, string BankAccountNumber, string CreditCardNumber, string PostalCode, int EmploymentDuration, string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode)
        {
            string strAdminCon = AdminConnection.ConnectionString;
            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            string sGender = "";
            string rXml = "";
            int MatchResult = 0;
            int intSubscriberEnquiryResultID = 0;
            DataSet ds = new DataSet();
            Boolean boolSubmitEnquiry = true;
            string strValidationStatus = "";

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);


            try
            {
                // Check if this subscriber is authorized to use the current product

                if (spr.ReportID > 0)
                {
                    // Check the PayAsYouGo settings for the subscriber
                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {

                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;

                        eSe.FirstName = FirstName;
                        eSe.Gender = Gender;
                        eSe.IDNo = IdNumber;
                        eSe.PassportNo = PassportNo;
                        eSe.ProductID = intProductId;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = "";
                        eSe.Surname = Surname;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";
                        eSe.NetMonthlyIncomeAmt = (decimal)MonthlySalary;
                        eSe.AccountNo = BankAccountType;
                        eSe.BusBusinessName = BankName;
                        eSe.BusVatNumber = PostalCode;


                        //Generate serach input string

                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.PassportNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.PassportNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.Surname))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.Surname;
                        }
                        if (!string.IsNullOrEmpty(eSe.FirstInitial))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.FirstName;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table

                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);
                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoConsumerTraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoConsumerTraceWseManager.ConfirmationChkBox = bConfirmationChkBox;
                        DateTime dtBirthDate = new DateTime();
                        DateTime.TryParse(DateOfBirth == "" ? "1900/01/01" : DateOfBirth, out dtBirthDate);
                        eoConsumerTraceWseManager.DOB = dtBirthDate;
                        eoConsumerTraceWseManager.ExternalReference = strExtRef;
                        eoConsumerTraceWseManager.FirstName = FirstName;
                        eoConsumerTraceWseManager.IDno = IdNumber;
                        eoConsumerTraceWseManager.Passportno = PassportNo;
                        eoConsumerTraceWseManager.ProductID = 2;
                        eoConsumerTraceWseManager.subscriberID = intSubscriberID;
                        eoConsumerTraceWseManager.Surname = Surname;
                        eoConsumerTraceWseManager.BonusCheck = bBonusChecking;

                        // Submit data for matching
                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.ConsumerMatchCustom(eoConsumerTraceWseManager);

                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.ConsumerMatchCustom(eoConsumerTraceWseManager);
                        }
                        rp.EnquiryID = intSubscriberEnquiryID;

                        // Get Response data

                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            eSC.SearchOutput = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.DetailsViewedYN = false;
                            eSC.Billable = false;
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;

                            rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                            rp.EnquiryID = eSC.SubscriberEnquiryID;
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            ds.ReadXml(xmlSR);
                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, intProductId);

                                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);
                                rXml = ds.GetXml();
                                rp.ResponseData = rXml;

                                foreach (DataRow r in ds.Tables["ConsumerDetail"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    eSC.Billable = true;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                    eSC.ExtraVarOutput1 = rp.TmpReference;

                                    rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    // Change the PayAsYouGoEnquiryLimit  after viewing the report

                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                                }
                            }
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }
                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {
                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");


                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);

                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;
                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;

                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);
                                            }
                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {
                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();

                                }
                            }

                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {


                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");

                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;

                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;

                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);

                                            }

                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {
                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }
                        }
                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }
            catch (Exception oException)
            {

                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);

                MatchResult = eSe.SubscriberEnquiryID;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitConsumerTraceVettingO(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string IdNumber, string PassportNo, string FirstName, string Surname, string DateOfBirth, string Gender, string HomePhoneNumber, string WorkPhoneNumber, double MonthlySalary, string BankAccountType, string BankName, string AccountHolderName, string BranchCode, string BankAccountNumber, string CreditCardNumber, string PostalCode, int EmploymentDuration, string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode, string strMaritalStatus)
        {
            string strAdminCon = AdminConnection.ConnectionString;
            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            string sGender = "";
            string rXml = "";
            int MatchResult = 0;
            int intSubscriberEnquiryResultID = 0;
            DataSet ds = new DataSet();
            Boolean boolSubmitEnquiry = true;
            string strValidationStatus = "";

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);


            try
            {
                // Check if this subscriber is authorized to use the current product

                if (spr.ReportID > 0)
                {
                    // Check the PayAsYouGo settings for the subscriber
                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {
                        DateTime dtBirthDate = new DateTime();
                        if (!DateTime.TryParseExact(DateOfBirth, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtBirthDate))
                        {
                            DateTime.TryParse(DateOfBirth == "" ? "1900/01/01" : DateOfBirth, out dtBirthDate);
                        }


                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;

                        eSe.FirstName = FirstName;
                        eSe.Gender = Gender;
                        eSe.IDNo = IdNumber;
                        eSe.PassportNo = PassportNo;
                        eSe.ProductID = intProductId;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = "";
                        eSe.Surname = Surname;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";
                        eSe.NetMonthlyIncomeAmt = (decimal)MonthlySalary;
                        eSe.AccountNo = BankAccountType;
                        eSe.BusBusinessName = BankName;
                        eSe.BusVatNumber = PostalCode;
                        eSe.BirthDate = dtBirthDate;
                        eSe.Gender = Gender;
                        eSe.MaidenName = strMaritalStatus;


                        //Generate serach input string

                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.PassportNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.PassportNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.Surname))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.Surname;
                        }
                        if (!string.IsNullOrEmpty(eSe.FirstInitial))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.FirstName;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table

                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);
                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoConsumerTraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoConsumerTraceWseManager.ConfirmationChkBox = bConfirmationChkBox;

                        eoConsumerTraceWseManager.DOB = dtBirthDate;
                        eoConsumerTraceWseManager.ExternalReference = strExtRef;
                        eoConsumerTraceWseManager.FirstName = FirstName;
                        eoConsumerTraceWseManager.IDno = IdNumber;
                        eoConsumerTraceWseManager.Passportno = PassportNo;
                        eoConsumerTraceWseManager.ProductID = 2;
                        eoConsumerTraceWseManager.subscriberID = intSubscriberID;
                        eoConsumerTraceWseManager.Surname = Surname;
                        eoConsumerTraceWseManager.BonusCheck = bBonusChecking;
                        eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;


                        // Submit data for matching
                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);

                        if (intProductId == 146)
                        {
                            eoConsumerTraceWseManager.ProductID = intProductId;
                            rp = ra.ConsumerTraceMatch(eoConsumerTraceWseManager);
                        }
                        else
                        {
                            rp = ra.ConsumerMatchCustom(eoConsumerTraceWseManager);
                        }

                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            if (intProductId == 146)
                            {
                                eoConsumerTraceWseManager.ProductID = intProductId;
                                rp = ra.ConsumerTraceMatch(eoConsumerTraceWseManager);
                            }
                            else
                            {
                                rp = ra.ConsumerMatchCustom(eoConsumerTraceWseManager);
                            }
                        }


                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None && !string.IsNullOrEmpty(IdNumber))
                        {
                            XDSPortalLibrary.Entity_Layer.ConsumerIDVerificationTrace eoHATraceWseManager = new XDSPortalLibrary.Entity_Layer.ConsumerIDVerificationTrace(); ;

                            eoHATraceWseManager.DataSegments = eSe.ExtraVarInput2;
                            eoHATraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                            eoHATraceWseManager.ConfirmationChkBox = bConfirmationChkBox;
                            eoHATraceWseManager.DOB = dtBirthDate;
                            eoHATraceWseManager.ExternalReference = strExtRef;
                            eoHATraceWseManager.Firstname = FirstName;
                            eoHATraceWseManager.IDno = IdNumber;
                            eoHATraceWseManager.ProductID = intProductId;
                            eoHATraceWseManager.subscriberID = intSubscriberID;
                            eoHATraceWseManager.Surname = Surname;
                            eoHATraceWseManager.BonusCheck = bBonusChecking;
                            eoHATraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;



                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            if (intProductId == 146)
                            {
                                rp = ra.ConsumerIDVerificationJDMobiMatch(eoHATraceWseManager);
                            }
                            else
                            {
                                rp = ra.ConsumerIDVerificationMatch(eoHATraceWseManager);
                            }


                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                if (intProductId == 146)
                                {
                                    rp = ra.ConsumerIDVerificationJDMobiMatch(eoHATraceWseManager);
                                }
                                else
                                {
                                    rp = ra.ConsumerIDVerificationMatch(eoHATraceWseManager);
                                }
                            }


                        }
                        rp.EnquiryID = intSubscriberEnquiryID;

                        // Get Response data

                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);


                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            eSC.SearchOutput = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.DetailsViewedYN = false;
                            eSC.Billable = false;
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;
                            eSC.KeyID = rp.ResponseKey;
                            eSC.KeyType = rp.ResponseKeyType;


                            rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                            rp.EnquiryID = eSC.SubscriberEnquiryID;

                            if (!string.IsNullOrEmpty(IdNumber) || !string.IsNullOrEmpty(PassportNo))
                            {
                                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple;

                                ds = new DataSet("ListOfConsumers");

                                DataTable dt = new DataTable("ConsumerDetails");


                                ds.Tables.Add(dt);

                                ds.Tables[0].Columns.Add("EnquiryID");
                                ds.Tables[0].Columns.Add("EnquiryResultID");
                                ds.Tables[0].Columns.Add("Reference");

                                DataRow dr = ds.Tables[0].NewRow();

                                dr["EnquiryID"] = rp.EnquiryID;
                                dr["EnquiryResultID"] = rp.EnquiryResultID;
                                dr["Reference"] = eSC.Reference;

                                ds.Tables[0].Rows.Add(dr);

                                rp.ResponseData = ds.GetXml();

                            }

                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            ds.ReadXml(xmlSR);
                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, intProductId);

                                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);
                                rXml = ds.GetXml();
                                rp.ResponseData = rXml;

                                foreach (DataRow r in ds.Tables["ConsumerDetail"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    eSC.Billable = true;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                    eSC.ExtraVarOutput1 = rp.TmpReference;

                                    rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    // Change the PayAsYouGoEnquiryLimit  after viewing the report

                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                                }
                            }
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }
                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {
                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");


                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);

                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;
                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;

                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);
                                            }
                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {
                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();

                                }
                            }

                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {


                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");

                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;

                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;

                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);

                                            }

                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {
                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }
                        }
                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }
            catch (Exception oException)
            {

                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);

                MatchResult = eSe.SubscriberEnquiryID;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            ds.Dispose();
            return rp;
        }

        public int CalculateAge(DateTime birthDate, DateTime now)
        {
            int age = now.Year - birthDate.Year;

            if (now.Month < birthDate.Month || (now.Month == birthDate.Month && now.Day < birthDate.Day))
                age--;

            return age;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitConsumerTraceVettingQ(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string IdNumber, string PassportNo, string FirstName, string Surname, string DateOfBirth, string Gender, string MaritalStatus, string AddressLine1, string AddressLine2, string Suburb, string City, string PostalCode, string HomeTelephoneCode, string HomeTelephoneNumber, string WorkTelephoneCode, string WorkTelephoneNumber, string CellNumber, double MonthlySalary, string Occupation, string Employer, string BankAccountType, string BankName, string HighestQualification, string BranchCode, string BankAccountNumber, int EmploymentDuration, string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode)
        {
            string strAdminCon = AdminConnection.ConnectionString;
            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            string sGender = "";
            string rXml = "";
            int MatchResult = 0;
            int intSubscriberEnquiryResultID = 0;
            DataSet ds = new DataSet();
            Boolean boolSubmitEnquiry = true;
            string strValidationStatus = "";

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);


            try
            {
                // Check if this subscriber is authorized to use the current product

                if (spr.ReportID > 0)
                {
                    // Check the PayAsYouGo settings for the subscriber
                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {

                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;

                        eSe.FirstName = FirstName;
                        eSe.Gender = Gender;
                        eSe.IDNo = IdNumber;
                        eSe.PassportNo = PassportNo;
                        eSe.ProductID = intProductId;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = "";
                        eSe.Surname = Surname;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";
                        eSe.NetMonthlyIncomeAmt = (decimal)MonthlySalary;
                        eSe.AccountNo = BankAccountType;
                        eSe.BusBusinessName = BankName;
                        eSe.BusVatNumber = PostalCode;


                        //Generate serach input string

                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.PassportNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.PassportNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.Surname))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.Surname;
                        }
                        if (!string.IsNullOrEmpty(eSe.FirstInitial))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.FirstName;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table

                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);
                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoConsumerTraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoConsumerTraceWseManager.ConfirmationChkBox = bConfirmationChkBox;
                        DateTime dtBirthDate = new DateTime();

                        if (!DateTime.TryParseExact(DateOfBirth, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtBirthDate))
                        {
                            DateTime.TryParse(DateOfBirth == "" ? "1900/01/01" : DateOfBirth, out dtBirthDate);
                        }
                        eoConsumerTraceWseManager.DOB = dtBirthDate;
                        eoConsumerTraceWseManager.ExternalReference = strExtRef;
                        eoConsumerTraceWseManager.FirstName = FirstName;
                        eoConsumerTraceWseManager.IDno = IdNumber;
                        eoConsumerTraceWseManager.Passportno = PassportNo;
                        eoConsumerTraceWseManager.ProductID = 2;
                        eoConsumerTraceWseManager.subscriberID = intSubscriberID;
                        eoConsumerTraceWseManager.Surname = Surname;
                        eoConsumerTraceWseManager.BonusCheck = bBonusChecking;
                        eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;


                        // Submit data for matching
                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.ConsumerMatchCustomMTN(eoConsumerTraceWseManager);

                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.ConsumerMatchCustomMTN(eoConsumerTraceWseManager);
                        }


                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None && !string.IsNullOrEmpty(IdNumber))
                        {
                            XDSPortalLibrary.Entity_Layer.ConsumerIDVerificationTrace eoHATraceWseManager = new XDSPortalLibrary.Entity_Layer.ConsumerIDVerificationTrace(); ;

                            eoHATraceWseManager.DataSegments = eSe.ExtraVarInput2;
                            eoHATraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                            eoHATraceWseManager.ConfirmationChkBox = bConfirmationChkBox;
                            eoHATraceWseManager.DOB = dtBirthDate;
                            eoHATraceWseManager.ExternalReference = strExtRef;
                            eoHATraceWseManager.Firstname = FirstName;
                            eoHATraceWseManager.IDno = IdNumber;
                            eoHATraceWseManager.ProductID = intProductId;
                            eoHATraceWseManager.subscriberID = intSubscriberID;
                            eoHATraceWseManager.Surname = Surname;
                            eoHATraceWseManager.BonusCheck = bBonusChecking;
                            eoHATraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;



                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.ConsumerIDVerificationMatch(eoHATraceWseManager);


                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.ConsumerIDVerificationMatch(eoHATraceWseManager);
                            }


                        }
                        rp.EnquiryID = intSubscriberEnquiryID;

                        // Get Response data

                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);
                        //if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        //{
                        //    if (CalculateAge())
                        //}

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            eSC.SearchOutput = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.DetailsViewedYN = false;
                            eSC.Billable = false;
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;
                            eSC.KeyID = rp.ResponseKey;
                            eSC.KeyType = rp.ResponseKeyType;


                            rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                            rp.EnquiryID = eSC.SubscriberEnquiryID;

                            if (!string.IsNullOrEmpty(IdNumber) || !string.IsNullOrEmpty(PassportNo))
                            {
                                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple;

                                ds = new DataSet("ListOfConsumers");

                                DataTable dt = new DataTable("ConsumerDetails");


                                ds.Tables.Add(dt);

                                ds.Tables[0].Columns.Add("EnquiryID");
                                ds.Tables[0].Columns.Add("EnquiryResultID");
                                ds.Tables[0].Columns.Add("Reference");

                                DataRow dr = ds.Tables[0].NewRow();

                                dr["EnquiryID"] = rp.EnquiryID;
                                dr["EnquiryResultID"] = rp.EnquiryResultID;
                                dr["Reference"] = eSC.Reference;

                                ds.Tables[0].Rows.Add(dr);

                                rp.ResponseData = ds.GetXml();

                            }

                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            ds.ReadXml(xmlSR);
                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, intProductId);

                                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);
                                rXml = ds.GetXml();
                                rp.ResponseData = rXml;

                                foreach (DataRow r in ds.Tables["ConsumerDetail"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    eSC.Billable = true;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                    eSC.ExtraVarOutput1 = rp.TmpReference;

                                    rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    // Change the PayAsYouGoEnquiryLimit  after viewing the report

                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                                }
                            }
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }
                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {
                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");


                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);

                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;
                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;

                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);
                                            }
                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {
                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();

                                }
                            }

                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {


                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");

                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;

                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;

                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);

                                            }

                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {
                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }
                        }
                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }
            catch (Exception oException)
            {

                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);

                MatchResult = eSe.SubscriberEnquiryID;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitConsumerTraceCanvas(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string IdNumber, string PassportNo, string FirstName, string Surname, string DateOfBirth, string Gender, string MaritalStatus, string AddressLine1, string AddressLine2, string Suburb, string City, string PostalCode, string HomeTelephoneCode, string HomeTelephoneNumber, string WorkTelephoneCode, string WorkTelephoneNumber, string CellNumber, double MonthlySalary, string Occupation, string Employer, string BankAccountType, string BankName, string HighestQualification, string BranchCode, string BankAccountNumber, int EmploymentDuration, string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode)
        {
            string strAdminCon = AdminConnection.ConnectionString;
            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            string sGender = "";
            string rXml = "";
            int MatchResult = 0;
            int intSubscriberEnquiryResultID = 0;
            DataSet ds = new DataSet();
            Boolean boolSubmitEnquiry = true;
            string strValidationStatus = "";

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);


            try
            {
                // Check if this subscriber is authorized to use the current product

                if (spr.ReportID > 0)
                {
                    // Check the PayAsYouGo settings for the subscriber
                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {

                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;

                        eSe.FirstName = FirstName;
                        eSe.Gender = Gender;
                        eSe.IDNo = IdNumber;
                        eSe.PassportNo = PassportNo;
                        eSe.ProductID = intProductId;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = "";
                        eSe.Surname = Surname;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";
                        eSe.NetMonthlyIncomeAmt = (decimal)MonthlySalary;
                        eSe.AccountNo = BankAccountType;
                        eSe.BusBusinessName = BankName;
                        eSe.BusVatNumber = PostalCode;


                        //Generate serach input string

                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.PassportNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.PassportNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.Surname))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.Surname;
                        }
                        if (!string.IsNullOrEmpty(eSe.FirstInitial))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.FirstName;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table

                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);
                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoConsumerTraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoConsumerTraceWseManager.ConfirmationChkBox = bConfirmationChkBox;
                        DateTime dtBirthDate = new DateTime();

                        if (!DateTime.TryParseExact(DateOfBirth, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtBirthDate))
                        {
                            DateTime.TryParse(DateOfBirth == "" ? "1900/01/01" : DateOfBirth, out dtBirthDate);
                        }
                        eoConsumerTraceWseManager.DOB = dtBirthDate;
                        eoConsumerTraceWseManager.ExternalReference = strExtRef;
                        eoConsumerTraceWseManager.FirstName = FirstName;
                        eoConsumerTraceWseManager.IDno = IdNumber;
                        eoConsumerTraceWseManager.Passportno = PassportNo;
                        eoConsumerTraceWseManager.ProductID = 2;
                        eoConsumerTraceWseManager.subscriberID = intSubscriberID;
                        eoConsumerTraceWseManager.Surname = Surname;
                        eoConsumerTraceWseManager.BonusCheck = bBonusChecking;
                        eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;


                        // Submit data for matching
                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.ConsumerMatchCustomMTN(eoConsumerTraceWseManager);

                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.ConsumerMatchCustomMTN(eoConsumerTraceWseManager);
                        }


                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None && !string.IsNullOrEmpty(IdNumber))
                        {
                            XDSPortalLibrary.Entity_Layer.ConsumerIDVerificationTrace eoHATraceWseManager = new XDSPortalLibrary.Entity_Layer.ConsumerIDVerificationTrace(); ;

                            eoHATraceWseManager.DataSegments = eSe.ExtraVarInput2;
                            eoHATraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                            eoHATraceWseManager.ConfirmationChkBox = bConfirmationChkBox;
                            eoHATraceWseManager.DOB = dtBirthDate;
                            eoHATraceWseManager.ExternalReference = strExtRef;
                            eoHATraceWseManager.Firstname = FirstName;
                            eoHATraceWseManager.IDno = IdNumber;
                            eoHATraceWseManager.ProductID = intProductId;
                            eoHATraceWseManager.subscriberID = intSubscriberID;
                            eoHATraceWseManager.Surname = Surname;
                            eoHATraceWseManager.BonusCheck = bBonusChecking;
                            eoHATraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;



                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.ConsumerIDVerificationMatch(eoHATraceWseManager);


                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.ConsumerIDVerificationMatch(eoHATraceWseManager);
                            }


                        }
                        rp.EnquiryID = intSubscriberEnquiryID;

                        // Get Response data

                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);
                        //if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        //{
                        //    if (CalculateAge())
                        //}

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            eSC.SearchOutput = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.DetailsViewedYN = false;
                            eSC.Billable = false;
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;
                            eSC.KeyID = rp.ResponseKey;
                            eSC.KeyType = rp.ResponseKeyType;


                            rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                            rp.EnquiryID = eSC.SubscriberEnquiryID;

                            if (!string.IsNullOrEmpty(IdNumber) || !string.IsNullOrEmpty(PassportNo))
                            {
                                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple;

                                ds = new DataSet("ListOfConsumers");

                                DataTable dt = new DataTable("ConsumerDetails");


                                ds.Tables.Add(dt);

                                ds.Tables[0].Columns.Add("EnquiryID");
                                ds.Tables[0].Columns.Add("EnquiryResultID");
                                ds.Tables[0].Columns.Add("Reference");

                                DataRow dr = ds.Tables[0].NewRow();

                                dr["EnquiryID"] = rp.EnquiryID;
                                dr["EnquiryResultID"] = rp.EnquiryResultID;
                                dr["Reference"] = eSC.Reference;

                                ds.Tables[0].Rows.Add(dr);

                                rp.ResponseData = ds.GetXml();

                            }

                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            ds.ReadXml(xmlSR);
                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, intProductId);

                                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);
                                rXml = ds.GetXml();
                                rp.ResponseData = rXml;

                                foreach (DataRow r in ds.Tables["ConsumerDetail"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    eSC.Billable = true;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                    eSC.ExtraVarOutput1 = rp.TmpReference;

                                    rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    // Change the PayAsYouGoEnquiryLimit  after viewing the report

                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                                }
                            }
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }
                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {
                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");


                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);

                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;
                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;

                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);
                                            }
                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {
                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();

                                }
                            }

                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {


                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");

                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;

                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;

                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);

                                            }

                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {
                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }
                        }
                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }
            catch (Exception oException)
            {

                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);

                MatchResult = eSe.SubscriberEnquiryID;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitConsumerTraceVettingD(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string CompanyName, string Branch, string UserName, string Password, string RuleSet, string Title, string FirstName, string SecondName, string Surname, string DateOfBirth, string IDType, string IDNumber, string Gender, string AddressLine1, string AddressLine2, string Suburb, string City, string PhysicalPostalCode, string ProvinceCode, string OwnerTenant, string PostalAddressLine1, string PostalAddressLine2, string PostalSuburb, string PostalCity, string PostalCode, string PostalProvinceCode, string HomeTelephoneCode, string HomePhoneNumber, string WorkTelephoneCode, string WorkPhoneNumber, string YearsatCurrentEmployer, string BankAccountType, string Income, string SFID, bool bConfirmationChkBox, bool bBonusChecking)
        {
            string strAdminCon = AdminConnection.ConnectionString;
            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            string sGender = "";
            string rXml = "";
            int MatchResult = 0;
            int intSubscriberEnquiryResultID = 0;
            DataSet ds = new DataSet();
            Boolean boolSubmitEnquiry = true;
            string strValidationStatus = "";

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);


            try
            {
                // Check if this subscriber is authorized to use the current product

                if (spr.ReportID > 0)
                {
                    // Check the PayAsYouGo settings for the subscriber
                    //if (!string.IsNullOrEmpty(strVoucherCode))
                    //{
                    //    SubscriberVoucher sv = new SubscriberVoucher();
                    //    strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                    //    if (strValidationStatus == "")
                    //    {
                    //        throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                    //    }
                    //    else if (!(strValidationStatus == "1"))
                    //    {
                    //        throw new Exception(strValidationStatus);
                    //    }

                    //}
                    //else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    //{
                    //    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    //    rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                    //    boolSubmitEnquiry = false;

                    //}
                    if (boolSubmitEnquiry)
                    {
                        DateTime dtBirthDate = new DateTime();

                        if (!string.IsNullOrEmpty(DateOfBirth))
                        {
                            if (!DateTime.TryParseExact(DateOfBirth, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtBirthDate))
                            {
                                if (!DateTime.TryParseExact(DateOfBirth, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtBirthDate))
                                {
                                    if (!DateTime.TryParseExact(DateOfBirth, "yyyy-dd-MM", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtBirthDate))
                                    {
                                        if (!DateTime.TryParseExact(DateOfBirth, "yyyyddMM", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtBirthDate))
                                        {
                                            DateTime.TryParse(DateOfBirth == "" ? "1900/01/01" : DateOfBirth, out dtBirthDate);
                                        }
                                    }
                                }
                            }

                        }
                        else
                        {
                            DateTime.TryParse(DateOfBirth == "" ? "1900/01/01" : DateOfBirth, out dtBirthDate);
                        }

                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;

                        eSe.FirstName = FirstName;
                        eSe.Gender = Gender.ToString();
                        if (IDType.ToString().ToUpper() == "SAID")
                        {
                            eSe.IDNo = IDNumber;
                            eSe.PassportNo = "";
                        }
                        else if (IDType.ToString().ToUpper() == "PASSPORT")
                        {
                            eSe.IDNo = "";
                            eSe.PassportNo = IDNumber;
                        }
                        eSe.ProductID = intProductId;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = SFID;
                        eSe.Surname = Surname;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";
                        decimal dincome = 0;
                        decimal.TryParse(Income, out dincome);
                        eSe.NetMonthlyIncomeAmt = dincome;
                        eSe.AccountNo = BankAccountType;
                        eSe.BusBusinessName = CompanyName;
                        eSe.BusVatNumber = PostalCode;
                        eSe.BranchCode = IDType.ToString();
                        eSe.BirthDate = dtBirthDate;
                        eSe.AccountNo = Title;

                        //Generate serach input string

                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.PassportNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.PassportNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.Surname))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.Surname;
                        }
                        if (!string.IsNullOrEmpty(eSe.FirstInitial))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.FirstName;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table

                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);
                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoConsumerTraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoConsumerTraceWseManager.ConfirmationChkBox = bConfirmationChkBox;

                        eoConsumerTraceWseManager.DOB = dtBirthDate;
                        eoConsumerTraceWseManager.ExternalReference = SFID;
                        eoConsumerTraceWseManager.FirstName = FirstName;
                        eoConsumerTraceWseManager.SecondName = SecondName;
                        if (IDType.ToString().ToUpper() == "SAID")
                        {
                            eoConsumerTraceWseManager.IDno = IDNumber;
                            eoConsumerTraceWseManager.Passportno = "";
                        }
                        else if (IDType.ToString().ToUpper() == "PASSPORT")
                        {
                            eoConsumerTraceWseManager.IDno = "";
                            eoConsumerTraceWseManager.Passportno = IDNumber;
                        }
                        eoConsumerTraceWseManager.ProductID = 2;
                        eoConsumerTraceWseManager.subscriberID = intSubscriberID;
                        eoConsumerTraceWseManager.Surname = Surname;
                        eoConsumerTraceWseManager.BonusCheck = bBonusChecking;
                        eoConsumerTraceWseManager.Gender = Gender.ToString();
                        eoConsumerTraceWseManager.AccountType = BankAccountType;
                        //eoConsumerTraceWseManager.BankName = BankName;
                        eoConsumerTraceWseManager.CompanyName = CompanyName;
                        eoConsumerTraceWseManager.Branch = Branch;
                        eoConsumerTraceWseManager.RuleSet = RuleSet;
                        eoConsumerTraceWseManager.Title = Title;
                        eoConsumerTraceWseManager.AddressLine1 = AddressLine1;
                        eoConsumerTraceWseManager.AddressLine2 = AddressLine2;
                        eoConsumerTraceWseManager.Suburb = Suburb;
                        eoConsumerTraceWseManager.City = City;
                        eoConsumerTraceWseManager.PhysicalPostalCode = PhysicalPostalCode;
                        eoConsumerTraceWseManager.ProvinceCode = ProvinceCode;
                        eoConsumerTraceWseManager.OwnerTenant = OwnerTenant;
                        eoConsumerTraceWseManager.PostalAddressLine1 = PostalAddressLine1;
                        eoConsumerTraceWseManager.PostalAddressLine2 = PostalAddressLine2;
                        eoConsumerTraceWseManager.PostalSuburb = PostalSuburb;
                        eoConsumerTraceWseManager.PostalCity = PostalSuburb;
                        eoConsumerTraceWseManager.PostalCode = PostalCity;
                        eoConsumerTraceWseManager.PostalProvinceCode = PostalProvinceCode;
                        eoConsumerTraceWseManager.HomeTelephoneCode = HomeTelephoneCode;
                        eoConsumerTraceWseManager.HomePhoneNumber = HomePhoneNumber;
                        eoConsumerTraceWseManager.WorkTelephoneCode = WorkTelephoneCode;
                        eoConsumerTraceWseManager.WorkPhoneNumber = WorkPhoneNumber;
                        eoConsumerTraceWseManager.YearsatCurrentEmployer = YearsatCurrentEmployer;
                        //eoConsumerTraceWseManager.AccountHolderName = AccountHolderName;
                        //eoConsumerTraceWseManager.BranchCode = BranchCode;
                        //eoConsumerTraceWseManager.BankAccountNumber = BankAccountNumber;
                        //eoConsumerTraceWseManager.CreditCardNumber = CreditCardNumber;
                        eoConsumerTraceWseManager.SFID = SFID;
                        eoConsumerTraceWseManager.Income = Income;
                        eoConsumerTraceWseManager.IDType = IDType.ToString().ToUpper();

                        // Submit data for matching

                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.ConsumerMatchCustomVettingD(eoConsumerTraceWseManager);

                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.ConsumerMatchCustomVettingD(eoConsumerTraceWseManager);
                        }
                        rp.EnquiryID = intSubscriberEnquiryID;

                        // Get Response data

                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);


                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            eSC.SearchOutput = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.DetailsViewedYN = false;
                            eSC.Billable = false;
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;

                            rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                            rp.EnquiryID = eSC.SubscriberEnquiryID;
                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);


                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            ds.ReadXml(xmlSR);
                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, intProductId);

                                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);
                                rXml = ds.GetXml();
                                rp.ResponseData = rXml;

                                foreach (DataRow r in ds.Tables["ConsumerDetail"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    eSC.Billable = true;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                    eSC.ExtraVarOutput1 = rp.TmpReference;

                                    rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    // Change the PayAsYouGoEnquiryLimit  after viewing the report

                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);

                                    if (String.IsNullOrEmpty(eSC.VoucherCode))
                                    {
                                        //Log FootPrint

                                        eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                        eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                        eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                        eSubscriberFootPrint.KeyID = eSC.KeyID;
                                        eSubscriberFootPrint.KeyType = eSC.KeyType;
                                        eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                        eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                        if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                        {
                                            eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                        }
                                        eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                        eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                        eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                        eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                        eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                        eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                        eSubscriberFootPrint.CreatedByUser = sys.Username;
                                        eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                        eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                        eSubscriberFootPrint.ProductID = eSe.ProductID;
                                        eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                        int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                    }
                                }
                            }
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {

                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }
                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    //eSC.VoucherCode = strVoucherCode;


                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {
                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");


                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);

                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;
                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;

                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);
                                            }



                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {

                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();

                                }
                            }

                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = "";

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {


                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");

                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;

                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;

                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);

                                            }

                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {

                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }


                        }

                    }

                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }
            catch (Exception oException)
            {

                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);

                MatchResult = eSe.SubscriberEnquiryID;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitCustomVetingC(SqlConnection con, SqlConnection AdminConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, bool bBonusChecking, string strVoucherCode)
        {
            string strAdminCon = AdminConnection.ConnectionString;
            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            string rXml = "";
            double Totalcost = 0;

            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();


            moCustomVetting.ConnectionString = AdminConnection;
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML
                // use this XML data for generating the Report

                if (spr.ProductID > 0)
                {

                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {

                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }

                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {
                            eoConsumerTraceWseManager.IDno = eSe.IDNo;
                            eoConsumerTraceWseManager.ConsumerID = eSC.KeyID;
                            eoConsumerTraceWseManager.Surname = eSe.Surname;
                            eoConsumerTraceWseManager.FirstName = eSe.FirstName;
                            eoConsumerTraceWseManager.GrossMonthlyIncome = (double)eSe.NetMonthlyIncomeAmt;
                            eoConsumerTraceWseManager.ReferenceNo = eSC.Reference;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;

                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.CustomVettingCMatch(eoConsumerTraceWseManager);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.CustomVettingCMatch(eoConsumerTraceWseManager);
                            }

                            rXml = rp.ResponseData;

                            System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                ds.ReadXml(xmlSR);
                                if (ds.Tables.Contains("ConsumerDetail"))
                                {

                                    Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                    Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, eSe.ProductID);

                                    foreach (DataRow r in ds.Tables["ConsumerDetail"].Rows)
                                    {
                                        Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                        Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                        eSC.DetailsViewedDate = DateTime.Now;
                                        eSC.DetailsViewedYN = true;
                                        if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                        {
                                            eSC.Billable = false;
                                        }
                                        else
                                        {
                                            eSC.Billable = true;
                                        }
                                        eSC.ChangedByUser = eSe.CreatedByUser;
                                        eSC.ChangedOnDate = DateTime.Now;
                                        eSC.SearchOutput = "";


                                        if (r.Table.Columns.Contains("IDNo"))
                                        {
                                            if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                            {
                                                eSC.IDNo = r["IDNo"].ToString();
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                            }
                                        }
                                        if (r.Table.Columns.Contains("PassportNo"))
                                        {
                                            if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                            {
                                                eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                            }
                                        }
                                        if (r.Table.Columns.Contains("Surname"))
                                        {
                                            if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                            {
                                                eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                            }
                                        }

                                        if (r.Table.Columns.Contains("FirstName"))
                                        {
                                            if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                            {
                                                eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                            }
                                        }

                                        if (eSC.SearchOutput.Length > 0)
                                        {
                                            eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                        }

                                        eSC.KeyID = rp.ResponseKey;
                                        eSC.KeyType = rp.ResponseKeyType;
                                        eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                        eSC.BillingTypeID = spr.BillingTypeID;
                                        eSC.BillingPrice = spr.UnitPrice;

                                        eSC.XMLData = rXml.Replace("'", "''");
                                        eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                        eSC.ProductID = eSe.ProductID;

                                        dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                        // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                        if (sub.PayAsYouGo == 1)
                                        {
                                            dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                        }
                                        if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                        {
                                            dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                        }

                                        if (String.IsNullOrEmpty(eSC.VoucherCode))
                                        {
                                            //Log FootPrint

                                            eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                            eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                            eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                            eSubscriberFootPrint.KeyID = eSC.KeyID;
                                            eSubscriberFootPrint.KeyType = eSC.KeyType;
                                            eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                            eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                            if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                            {
                                                eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                            }
                                            eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                            eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                            eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                            eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                            eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                            eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                            eSubscriberFootPrint.CreatedByUser = sys.Username;
                                            eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                            eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                            eSubscriberFootPrint.ProductID = eSe.ProductID;
                                            eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                            int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                        }
                                    }
                                }

                                rXml = rp.ResponseData;

                            }
                        }
                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.

                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                        }



                    }
                    else
                    {
                        // When User want to Re-Open a report

                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = eSC.XMLData;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }


                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitCellcScoreCard(SqlConnection con, SqlConnection AdminConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, bool bBonusChecking, string strVoucherCode, string cVetURL, string cVetUsername, string cVetPassword, bool UseCustomLog,
              string CompanyName, string Branch, string User, string Password, string RuleSet, string Title, string FirstName, string SecondName, string Surname, string DateOfBirth, string IDType, string IDNumber, string Gender, string AddressLine1, string AddressLine2, string Suburb, string City, string PhysicalPostalCode, string ProvinceCode, string OwnerTenant, string PostalAddressLine1, string PostalAddressLine2, string PostalSuburb, string PostalCity, string PostalCode, string PostalProvinceCode, string HomeTelephoneCode, string HomeTelephoneNo, string WorkTelephoneCode, string WorkTelephoneNo, string YearsatCurrentEmployer, string BankAccountType, string MonthlySalary, string SFID, string strFMPLoginURL, string strFMPMatchURL)
        {
            string strAdminCon = AdminConnection.ConnectionString;
            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            string rXml = "";
            double Totalcost = 0;
            int NewCreditLimit = 0;
            int NewRecommendation = 0;
            int TransactionID = 0;
            string ticket = string.Empty;
            string VettingLEVEL = string.Empty;
            DataSet dsNewValues = new DataSet();

            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            moCustomVetting.ConnectionString = AdminConnection;
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            CustomVettingLog.CustomVettingWS oCVetLog = new CustomVettingLog.CustomVettingWS();

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML
                // use this XML data for generating the Report

                if (spr.ProductID > 0)
                {

                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {

                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }

                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {
                            eoConsumerTraceWseManager.IDno = eSe.IDNo;
                            eoConsumerTraceWseManager.ConsumerID = eSC.KeyID;
                            eoConsumerTraceWseManager.Surname = eSe.Surname;
                            eoConsumerTraceWseManager.FirstName = eSe.FirstName;
                            eoConsumerTraceWseManager.AccountType = eSe.AccountNo;
                            eoConsumerTraceWseManager.BankName = eSe.BusBusinessName;
                            eoConsumerTraceWseManager.PostalCode = eSe.BusVatNumber;
                            eoConsumerTraceWseManager.CreatedbyUser = eSe.CreatedByUser;
                            eoConsumerTraceWseManager.GrossMonthlyIncome = (double)eSe.NetMonthlyIncomeAmt;
                            eoConsumerTraceWseManager.ReferenceNo = eSC.Reference;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.EnquiryID = intSubscriberEnquiryID;
                            eoConsumerTraceWseManager.EnquiryResultID = intSubscriberEnquiryResultID;
                            eoConsumerTraceWseManager.Passportno = eSC.PassportNo;
                            eoConsumerTraceWseManager.IDType = eSe.BranchCode;
                            eoConsumerTraceWseManager.Title = eSe.AccountNo;
                            eoConsumerTraceWseManager.DOB = eSe.BirthDate;
                            eoConsumerTraceWseManager.SFID = eSe.SubscriberReference;

                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.CellcScoreCard(eoConsumerTraceWseManager);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.CellcScoreCard(eoConsumerTraceWseManager);
                            }

                            rXml = rp.ResponseData;

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                throw new Exception(rp.ResponseData);
                            }


                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                eSC.DetailsViewedDate = DateTime.Now;
                                eSC.DetailsViewedYN = true;
                                if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    eSC.Billable = false;
                                }
                                else
                                {
                                    eSC.Billable = true;
                                }
                                eSC.ChangedByUser = eSe.CreatedByUser;
                                eSC.ChangedOnDate = DateTime.Now;

                                eSC.KeyID = rp.ResponseKey;
                                eSC.KeyType = rp.ResponseKeyType;
                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                eSC.BillingTypeID = spr.BillingTypeID;
                                eSC.BillingPrice = spr.UnitPrice;

                                eSC.XMLData = rXml.Replace("'", "''");
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                eSC.ProductID = eSe.ProductID;

                                dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                rp.EnquiryID = eSC.SubscriberEnquiryID;

                                // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                if (sub.PayAsYouGo == 1)
                                {
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                }
                                if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                }

                                if (String.IsNullOrEmpty(eSC.VoucherCode))
                                {
                                    //Log FootPrint

                                    eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                    eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                    eSubscriberFootPrint.KeyID = eSC.KeyID;
                                    eSubscriberFootPrint.KeyType = eSC.KeyType;
                                    eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                    eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                    if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                    {
                                        eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                    }
                                    eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                    eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                    eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                    eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                    eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                    eSubscriberFootPrint.CreatedByUser = sys.Username;
                                    eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                    eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                    eSubscriberFootPrint.ProductID = eSe.ProductID;
                                    eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                    int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                }

                            }

                            rXml = rp.ResponseData;

                            System.IO.StringReader xmlResult = new System.IO.StringReader(rp.ResponseData);

                            ds.ReadXml(xmlResult);

                            if (ds.Tables.Contains("CellCCustomVetting"))
                            {
                                foreach (DataRow dr in ds.Tables["CellCCustomVetting"].Rows)
                                {
                                    oXDSCellCCustomVetting.Status = dr["Status"].ToString();
                                    oXDSCellCCustomVetting.ApplicationID = dr["ApplicationID"].ToString();
                                    oXDSCellCCustomVetting.Recommendation = Convert.ToInt32(dr["Recommendation"]);
                                    if (oXDSCellCCustomVetting.Recommendation == 1)
                                        oXDSCellCCustomVetting.Status = "1";
                                    else if (oXDSCellCCustomVetting.Recommendation == 0)
                                        oXDSCellCCustomVetting.Status = "0";
                                    oXDSCellCCustomVetting.Reason = dr["Reason"].ToString();
                                    oXDSCellCCustomVetting.PolicyFilter1 = Convert.ToInt32(dr["PolicyFilter1"]);
                                    oXDSCellCCustomVetting.PolicyFilter2 = Convert.ToInt32(dr["PolicyFilter2"]);
                                    oXDSCellCCustomVetting.PolicyFilter3 = Convert.ToInt32(dr["PolicyFilter3"]);
                                    oXDSCellCCustomVetting.PolicyFilter4 = Convert.ToInt32(dr["PolicyFilter4"]);
                                    oXDSCellCCustomVetting.PolicyFilter5 = Convert.ToInt32(dr["PolicyFilter5"]);
                                    oXDSCellCCustomVetting.PolicyFilter6 = Convert.ToInt32(dr["PolicyFilter6"]);
                                    oXDSCellCCustomVetting.PolicyFilter7 = Convert.ToInt32(dr["PolicyFilter7"]);
                                    oXDSCellCCustomVetting.PolicyFilter8 = Convert.ToInt32(dr["PolicyFilter8"]);
                                    oXDSCellCCustomVetting.PolicyFilter9 = Convert.ToInt32(dr["PolicyFilter9"]);
                                    oXDSCellCCustomVetting.PolicyFilter10 = Convert.ToInt32(dr["PolicyFilter10"]);
                                    oXDSCellCCustomVetting.PolicyFilter11 = Convert.ToInt32(dr["PolicyFilter11"]);
                                    oXDSCellCCustomVetting.PolicyFilter12 = Convert.ToInt32(dr["PolicyFilter12"]);
                                    oXDSCellCCustomVetting.PolicyFilter13 = Convert.ToInt32(dr["PolicyFilter13"]);
                                    oXDSCellCCustomVetting.PolicyFilter14 = Convert.ToInt32(dr["PolicyFilter14"]);
                                    oXDSCellCCustomVetting.PolicyFilter15 = Convert.ToInt32(dr["PolicyFilter15"]);
                                    oXDSCellCCustomVetting.PolicyFilter16 = Convert.ToInt32(dr["PolicyFilter16"]);
                                    oXDSCellCCustomVetting.PolicyFilter17 = Convert.ToInt32(dr["PolicyFilter17"]);
                                    oXDSCellCCustomVetting.PolicyFilter18 = Convert.ToInt32(dr["PolicyFilter18"]);
                                    oXDSCellCCustomVetting.PolicyFilter19 = Convert.ToInt32(dr["PolicyFilter19"]);
                                    oXDSCellCCustomVetting.BCCScore = Convert.ToInt32(dr["BCCScore"]);
                                    oXDSCellCCustomVetting.BCCRiskBand = dr["BCCRiskBand"].ToString();
                                    oXDSCellCCustomVetting.DemographicScore = Convert.ToInt32(dr["DemographicScore"]);
                                    oXDSCellCCustomVetting.DemographicRiskBand = dr["DemographicRiskBand"].ToString();
                                    oXDSCellCCustomVetting.DualMatrixRiskBand = dr["DualMatrixRiskBand"].ToString();
                                    oXDSCellCCustomVetting.BCCIndicator = dr["BCCindicator"].ToString();
                                    oXDSCellCCustomVetting.DemographicIndicator = dr["DemographicIndicator"].ToString();
                                    oXDSCellCCustomVetting.GrossIncome = dr["Grossincome"].ToString();
                                    oXDSCellCCustomVetting.TotalCommitments = Convert.ToInt32(dr["TotalCommitments"]);
                                    oXDSCellCCustomVetting.AmountAvailableforInstallment = Convert.ToInt32(dr["AmountAvailableforinstallment"]);
                                    oXDSCellCCustomVetting.CreditLimit = Convert.ToInt32(dr["CreditLimit"]);
                                    oXDSCellCCustomVetting.IncomeRefer = Convert.ToInt32(dr["IncomeRefer"]);
                                    oXDSCellCCustomVetting.Incomepercentagedifference = Convert.ToInt32(dr["Incomepercentagedifference"]);
                                    VettingLEVEL = "P";

                                    eoConsumerTraceWseManager.FirstName = FirstName;
                                    eoConsumerTraceWseManager.SecondName = SecondName;
                                    eoConsumerTraceWseManager.IDno = IDNumber;
                                    eoConsumerTraceWseManager.Surname = Surname;
                                    eoConsumerTraceWseManager.Gender = Gender.ToString();
                                    //eoConsumerTraceWseManager.AccountType = BankAccountType;
                                    eoConsumerTraceWseManager.CompanyName = CompanyName;
                                    eoConsumerTraceWseManager.Branch = Branch;
                                    eoConsumerTraceWseManager.RuleSet = RuleSet;
                                    eoConsumerTraceWseManager.Title = Title;
                                    eoConsumerTraceWseManager.AddressLine1 = AddressLine1;
                                    eoConsumerTraceWseManager.AddressLine2 = AddressLine2;
                                    eoConsumerTraceWseManager.Suburb = Suburb;
                                    eoConsumerTraceWseManager.City = City;
                                    eoConsumerTraceWseManager.PhysicalPostalCode = PhysicalPostalCode;
                                    eoConsumerTraceWseManager.ProvinceCode = ProvinceCode;
                                    //eoConsumerTraceWseManager.OwnerTenant = OwnerTenant;
                                    eoConsumerTraceWseManager.PostalAddressLine1 = PostalAddressLine1;
                                    eoConsumerTraceWseManager.PostalAddressLine2 = PostalAddressLine2;
                                    eoConsumerTraceWseManager.PostalSuburb = PostalSuburb;
                                    eoConsumerTraceWseManager.PostalCity = PostalSuburb;
                                    eoConsumerTraceWseManager.PostalCode = PostalCity;
                                    eoConsumerTraceWseManager.PostalProvinceCode = PostalProvinceCode;
                                    eoConsumerTraceWseManager.HomeTelephoneCode = HomeTelephoneCode;
                                    eoConsumerTraceWseManager.HomePhoneNumber = HomeTelephoneNo;
                                    eoConsumerTraceWseManager.WorkTelephoneCode = WorkTelephoneCode;
                                    eoConsumerTraceWseManager.WorkPhoneNumber = WorkTelephoneNo;
                                    //eoConsumerTraceWseManager.YearsatCurrentEmployer = YearsatCurrentEmployer;
                                    //eoConsumerTraceWseManager.SFID = SFID;
                                    eoConsumerTraceWseManager.Income = MonthlySalary;
                                    eoConsumerTraceWseManager.IDType = IDType.ToString().ToUpper();

                                    string FraudRules = "";
                                    XDSPortalLibrary.Entity_Layer.OutputData FMPResult = ra.CellcFMP(eoConsumerTraceWseManager, User, Password, strFMPLoginURL, strFMPMatchURL, out FraudRules);

                                    oXDSCellCCustomVetting.FraudScoreFS01.RecordSequence = "01";
                                    oXDSCellCCustomVetting.FraudScoreFS01.Part = "001";
                                    oXDSCellCCustomVetting.FraudScoreFS01.PartSequence = "01";
                                    oXDSCellCCustomVetting.FraudScoreFS01.ConsumerNo = eSC.KeyID.ToString();
                                    oXDSCellCCustomVetting.FraudScoreFS01.Rating = FMPResult.FraudScore.ToString();
                                    oXDSCellCCustomVetting.FraudScoreFS01.RatingDescription = FMPResult.CharacteristicAttribute[0];

                                    //System.Collections.Generic.List<string> strListCode = new System.Collections.Generic.List<string>();
                                    //System.Collections.Generic.List<string> strListCodeDesc = new System.Collections.Generic.List<string>();
                                    //strListCode = FMPResult.FraudRuleTriggered.ToList();
                                    //strListCodeDesc = FMPResult.CharacteristicAttribute.ToList();

                                    //strListCode.Add("BS18");
                                    //strListCode.Add("BS28");
                                    //strListCode.Add("BS32");
                                    //strListCode.Add("BS40");
                                    //strListCode.Add("BS44");
                                    //strListCode.Add("BS48");
                                    //strListCode.Add("BS49");
                                    //strListCode.Add("BS61");
                                    //strListCode.Add("HA100");
                                    //strListCode.Add("HA147");
                                    //strListCodeDesc.Add("ALERT - HIGH NUMBER OF HOME TEL NUMBER CHANGES WITHIN 30 DAYS");
                                    //strListCodeDesc.Add("CELLULAR NUMBER ON BUREAU HEADER AND NO CELLULAR NUMBER ON APPLICATION");
                                    //strListCodeDesc.Add("WORK TEL NUMBER ON BUREAU HEADER AND NO WORK TEL NUMBER ON APPLICATION");
                                    //strListCodeDesc.Add("EMPLOYER NAME ON BUREAU HEADER AND NO EMPLOYER NAME ON APPLICATION");
                                    //strListCodeDesc.Add("ADDRESS1 ON BUREAU HEADER AND NO ADDRESS1 ON APPLICATION");
                                    //strListCodeDesc.Add("ADDRESS2 ON BUREAU HEADER AND NO ADDRESS2 ON APPLICATION");
                                    //strListCodeDesc.Add("SERIOUS ALERT - HIGH NUMBER OF ENQUIRIES WITHIN 12 HOURS");
                                    //strListCodeDesc.Add("ID MATCHED TO AN EXISTING CONSUMER RECORD WITH A CONSUMER LOCK ACTIVE");
                                    //strListCodeDesc.Add("HIGH NUMBER OF SEARCHES ON HOME TEL NUMBER WITHIN 12 HOURS");
                                    //strListCodeDesc.Add("VERY HIGH NUMBER OF CONSUMERS USING THE SAME HOME TEL NUMBER WI");

                                    //oXDSCellCCustomVetting.FraudScoreFS01.ReasonCode = new string[strListCode.Count];
                                    //oXDSCellCCustomVetting.FraudScoreFS01.ReasonDescription = new string[strListCodeDesc.Count];

                                    oXDSCellCCustomVetting.FraudScoreFS01.ReasonCode = FMPResult.FraudRuleTriggered;
                                    oXDSCellCCustomVetting.FraudScoreFS01.ReasonDescription = FMPResult.CharacteristicAttribute;

                                    //switch (oXDSCellCCustomVetting.Recommendation)
                                    //{
                                    //    case 1:
                                    //        oXDSCellCCustomVetting.Recommendation = 1;
                                    //        dr["Recommendation"] = "1";
                                    //        //oXDSCellCCustomVetting.Reason = "DECLINE";
                                    //        break;
                                    //    case 5:
                                    //        switch (RiskGrade)
                                    //        {
                                    //            case 5:
                                    //                oXDSCellCCustomVetting.Recommendation = 1;
                                    //                oXDSCellCCustomVetting.Reason = "Application has exceeded the risk threshold - " + FraudRules;
                                    //                dr["Recommendation"] = "1";
                                    //                dr["Reason"] = "Application has exceeded the risk threshold - " + FraudRules;
                                    //                //oXDSCellCCustomVetting.Reason = "DECLINE";
                                    //                break;
                                    //            case 4:
                                    //                oXDSCellCCustomVetting.Recommendation = 5;
                                    //                dr["Recommendation"] = "5";
                                    //                //oXDSCellCCustomVetting.Reason = "REFER";
                                    //                break;
                                    //            default:
                                    //                oXDSCellCCustomVetting.Recommendation = 5;
                                    //                dr["Recommendation"] = "5";
                                    //                //oXDSCellCCustomVetting.Reason = "REFER";
                                    //                break;
                                    //        }
                                    //        break;
                                    //    case 0:
                                    //        switch (RiskGrade)
                                    //        {
                                    //            case 5:
                                    //                oXDSCellCCustomVetting.Recommendation = 1;
                                    //                oXDSCellCCustomVetting.Reason = "Application has exceeded the risk threshold - " + FraudRules;
                                    //                dr["Recommendation"] = "1";
                                    //                dr["Reason"] = "Application has exceeded the risk threshold - " + FraudRules;
                                    //                //oXDSCellCCustomVetting.Reason = "DECLINE";
                                    //                break;
                                    //            case 4:
                                    //                oXDSCellCCustomVetting.Recommendation = 5;
                                    //                dr["Recommendation"] = "5";
                                    //                //oXDSCellCCustomVetting.Reason = "REFER";
                                    //                break;
                                    //            default:
                                    //                oXDSCellCCustomVetting.Recommendation = 0;
                                    //                dr["Recommendation"] = "0";
                                    //                //oXDSCellCCustomVetting.Reason = "ACCEPT";
                                    //                break;
                                    //        }
                                    //        break;
                                    //}

                                    //rXml = ds.GetXml();
                                    //eSC.XMLData = rXml.Replace("'", "''");
                                    //dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                    XmlSerializer serializer = new XmlSerializer(oXDSCellCCustomVetting.GetType());
                                    System.IO.StringWriter sw = new System.IO.StringWriter();
                                    serializer.Serialize(sw, oXDSCellCCustomVetting);
                                    System.IO.StringReader reader = new System.IO.StringReader(sw.ToString());

                                    eSC.XMLData = reader.ReadToEnd();
                                    dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                }

                            }

                            if (UseCustomLog)
                            {
                                oCVetLog.Url = cVetURL;

                                ticket = oCVetLog.Login(cVetUsername, cVetPassword);

                                if (oCVetLog.IsTicketValid(ticket))
                                {
                                    string Response = oCVetLog.GetTransaction(ticket, eSC.KeyID);

                                    System.IO.StringReader xmlSR = new System.IO.StringReader(Response);

                                    dsNewValues.ReadXml(xmlSR);

                                    if (!dsNewValues.Tables.Contains("NoResult"))
                                    {
                                        VettingLEVEL = string.Empty;
                                        NewRecommendation = int.Parse(dsNewValues.Tables[0].Rows[0]["NewDecision"].ToString());
                                        NewCreditLimit = int.Parse(dsNewValues.Tables[0].Rows[0]["NewLimit"].ToString());
                                        TransactionID = int.Parse(dsNewValues.Tables[0].Rows[0]["TransactionID"].ToString());

                                        if (NewRecommendation == 1)
                                        {
                                            oXDSCellCCustomVetting.PolicyFilter1 = 0;
                                            oXDSCellCCustomVetting.PolicyFilter2 = 0;
                                            oXDSCellCCustomVetting.PolicyFilter3 = 0;
                                            oXDSCellCCustomVetting.PolicyFilter4 = 0;
                                            oXDSCellCCustomVetting.PolicyFilter5 = 0;
                                            oXDSCellCCustomVetting.PolicyFilter6 = 0;
                                            oXDSCellCCustomVetting.PolicyFilter7 = 0;
                                            oXDSCellCCustomVetting.PolicyFilter8 = 0;
                                            oXDSCellCCustomVetting.PolicyFilter9 = 0;
                                            oXDSCellCCustomVetting.PolicyFilter10 = 0;
                                            oXDSCellCCustomVetting.PolicyFilter11 = 0;
                                            oXDSCellCCustomVetting.PolicyFilter12 = 0;
                                            oXDSCellCCustomVetting.PolicyFilter13 = 0;
                                            oXDSCellCCustomVetting.PolicyFilter14 = 0;
                                            oXDSCellCCustomVetting.PolicyFilter15 = 0;
                                            oXDSCellCCustomVetting.PolicyFilter16 = 0;
                                            oXDSCellCCustomVetting.PolicyFilter17 = 0;
                                            oXDSCellCCustomVetting.PolicyFilter18 = 0;
                                            oXDSCellCCustomVetting.PolicyFilter19 = 0;
                                            oXDSCellCCustomVetting.Reason = string.Empty;
                                        }

                                        oXDSCellCCustomVetting.Recommendation = NewRecommendation;
                                        oXDSCellCCustomVetting.CreditLimit = NewCreditLimit;

                                        if (oXDSCellCCustomVetting.Recommendation == 1)
                                            oXDSCellCCustomVetting.Status = "1";
                                        else if (oXDSCellCCustomVetting.Recommendation == 0)
                                            oXDSCellCCustomVetting.Status = "0";

                                        oCVetLog.ChangeTransactionStatus(ticket, TransactionID, 1);
                                    }

                                    oCVetLog.SubmitTransaction(ticket, eSC.KeyID, Title, Surname, FirstName, SecondName, DateOfBirth,
                                        IDNumber, IDType, BankAccountType, MonthlySalary.ToString(), SFID, intSubscriberEnquiryResultID.ToString(), Branch, oXDSCellCCustomVetting.Status, oXDSCellCCustomVetting.ErrorDescription, oXDSCellCCustomVetting.ApplicationID, oXDSCellCCustomVetting.Recommendation, oXDSCellCCustomVetting.Reason, oXDSCellCCustomVetting.PolicyFilter1, oXDSCellCCustomVetting.PolicyFilter2, oXDSCellCCustomVetting.PolicyFilter3, oXDSCellCCustomVetting.PolicyFilter4, oXDSCellCCustomVetting.PolicyFilter5, oXDSCellCCustomVetting.PolicyFilter6, oXDSCellCCustomVetting.PolicyFilter7, oXDSCellCCustomVetting.PolicyFilter8, oXDSCellCCustomVetting.PolicyFilter9, oXDSCellCCustomVetting.PolicyFilter10, oXDSCellCCustomVetting.PolicyFilter11, oXDSCellCCustomVetting.PolicyFilter12, oXDSCellCCustomVetting.PolicyFilter13, oXDSCellCCustomVetting.PolicyFilter14, oXDSCellCCustomVetting.PolicyFilter15, oXDSCellCCustomVetting.PolicyFilter16, oXDSCellCCustomVetting.PolicyFilter17, oXDSCellCCustomVetting.PolicyFilter18,
                                        oXDSCellCCustomVetting.PolicyFilter19, oXDSCellCCustomVetting.BCCScore,
                                        oXDSCellCCustomVetting.BCCRiskBand,
                                        oXDSCellCCustomVetting.DemographicScore, oXDSCellCCustomVetting.DemographicRiskBand,
                                        oXDSCellCCustomVetting.DualMatrixRiskBand, oXDSCellCCustomVetting.BCCIndicator,
                                        oXDSCellCCustomVetting.DemographicIndicator, oXDSCellCCustomVetting.GrossIncome,
                                        oXDSCellCCustomVetting.TotalCommitments, oXDSCellCCustomVetting.AmountAvailableforInstallment,
                                        oXDSCellCCustomVetting.CreditLimit, oXDSCellCCustomVetting.IncomeRefer, oXDSCellCCustomVetting.Incomepercentagedifference, VettingLEVEL);
                                }
                            }

                        }

                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.

                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                        }

                    }
                    else
                    {
                        // When User want to Re-Open a report

                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = eSC.XMLData;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }


                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitConsumerTraceVettingF(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string IdNumber, int RequestNo, string CustomerName, string CustomerSurname, int RequestMSISDN, string ContactNo, bool bConfirmationChkBox, bool bBonusChecking)
        {
            string strAdminCon = AdminConnection.ConnectionString;
            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            string sGender = "";
            string rXml = "";
            int MatchResult = 0;
            int intSubscriberEnquiryResultID = 0;
            DataSet ds = new DataSet();
            Boolean boolSubmitEnquiry = true;
            string strValidationStatus = "";

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);


            try
            {
                // Check if this subscriber is authorized to use the current product

                if (spr.ReportID > 0)
                {
                    // Check the PayAsYouGo settings for the subscriber
                    //if (!string.IsNullOrEmpty(strVoucherCode))
                    //{
                    //    SubscriberVoucher sv = new SubscriberVoucher();
                    //    strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                    //    if (strValidationStatus == "")
                    //    {
                    //        throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                    //    }
                    //    else if (!(strValidationStatus == "1"))
                    //    {
                    //        throw new Exception(strValidationStatus);
                    //    }

                    //}
                    //else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    //{
                    //    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    //    rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                    //    boolSubmitEnquiry = false;

                    //}
                    if (boolSubmitEnquiry)
                    {

                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;

                        eSe.FirstName = CustomerName;
                        eSe.Gender = "";
                        eSe.IDNo = IdNumber;
                        eSe.PassportNo = "";
                        eSe.ProductID = intProductId;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = RequestNo.ToString();
                        eSe.TelephoneCode = RequestMSISDN.ToString();
                        eSe.TelephoneNo = ContactNo;
                        eSe.Surname = CustomerSurname;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";
                        eSe.EnquiryReason = "VodacomCustomVetting";

                        //Generate serach input string

                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.PassportNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.PassportNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.Surname))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.Surname;
                        }
                        if (!string.IsNullOrEmpty(eSe.FirstInitial))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.FirstName;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table

                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);
                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoConsumerTraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoConsumerTraceWseManager.ConfirmationChkBox = bConfirmationChkBox;
                        eoConsumerTraceWseManager.ExternalReference = "";
                        eoConsumerTraceWseManager.FirstName = CustomerName;
                        eoConsumerTraceWseManager.IDno = IdNumber;
                        eoConsumerTraceWseManager.Passportno = "";
                        eoConsumerTraceWseManager.ProductID = 2;
                        eoConsumerTraceWseManager.subscriberID = intSubscriberID;
                        eoConsumerTraceWseManager.Surname = CustomerSurname;
                        eoConsumerTraceWseManager.BonusCheck = bBonusChecking;
                        eoConsumerTraceWseManager.Gender = "";
                        eoConsumerTraceWseManager.RequestNo = RequestNo;
                        eoConsumerTraceWseManager.RequestMSISDN = RequestMSISDN;
                        eoConsumerTraceWseManager.ContactNo = ContactNo;
                        eoConsumerTraceWseManager.IsCustomVetting = true;

                        // Submit data for matching

                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.ConsumerMatchCustom(eoConsumerTraceWseManager);

                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.ConsumerMatchCustom(eoConsumerTraceWseManager);
                        }
                        rp.EnquiryID = intSubscriberEnquiryID;

                        // Get Response data

                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);


                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            eSC.SearchOutput = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.DetailsViewedYN = false;
                            eSC.Billable = false;
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;

                            rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                            rp.EnquiryID = eSC.SubscriberEnquiryID;
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            ds.ReadXml(xmlSR);
                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, intProductId);

                                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);
                                rXml = ds.GetXml();
                                rp.ResponseData = rXml;

                                foreach (DataRow r in ds.Tables["ConsumerDetail"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    eSC.Billable = true;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                    eSC.ExtraVarOutput1 = rp.TmpReference;

                                    rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    // Change the PayAsYouGoEnquiryLimit  after viewing the report

                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                                }
                            }
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {

                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }
                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    //eSC.VoucherCode = strVoucherCode;


                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {
                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");


                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);

                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;
                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;

                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);
                                            }
                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {
                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = "";

                                    string BonusXML = string.Empty;
                                    BonusXML = r["BonusXML"].ToString();
                                    if (BonusXML != string.Empty)
                                    {
                                        DataSet dsBonus = new DataSet();
                                        System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                        dsBonus.ReadXml(BonusxmlSR);
                                        if (dsBonus.Tables.Contains("Segments"))
                                        {


                                            eSC.BonusIncluded = true;
                                            eSC.XMLBonus = BonusXML.Replace("'", "''");

                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                            rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                            foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                            {
                                                eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;

                                                eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                eSB.Billable = false;
                                                eSB.CreatedByUser = eSe.CreatedByUser;
                                                eSB.CreatedOnDate = DateTime.Now;

                                                // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                dSB.InsertSubscriberEnquiryResultBonus(con, eSB);
                                            }
                                        }
                                        dsBonus.Dispose();
                                    }
                                    else
                                    {
                                        // Insert the match results to SubscriberEnquiryResult Table
                                        intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    }
                                    eSC.XMLBonus = null;
                                    eSC.BonusIncluded = false;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                    rp.ResponseData = ds.GetXml();
                                }
                            }
                        }
                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);

                MatchResult = eSe.SubscriberEnquiryID;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitCustomVettingF(SqlConnection con, SqlConnection AdminConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, int RequestNo, int RequestMSISDN, string ContactNo, bool bBonusChecking, string strVoucherCode)
        {
            string strAdminCon = AdminConnection.ConnectionString;
            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            string rXml = "";
            double Totalcost = 0;

            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();


            moCustomVetting.ConnectionString = AdminConnection;
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML
                // use this XML data for generating the Report

                if (spr.ProductID > 0)
                {

                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {

                        //string strValidationStatus = "";
                        //if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        //{
                        //    SubscriberVoucher sv = new SubscriberVoucher();
                        //    strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                        //    if (strValidationStatus == "")
                        //    {
                        //        throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        //    }
                        //    else if (!(strValidationStatus == "1"))
                        //    {
                        //        throw new Exception(strValidationStatus);
                        //    }

                        //}

                        // Check if the Subscriber has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {
                            eoConsumerTraceWseManager.IDno = eSe.IDNo;
                            eoConsumerTraceWseManager.ConsumerID = eSC.KeyID;
                            eoConsumerTraceWseManager.Surname = eSe.Surname;
                            eoConsumerTraceWseManager.FirstName = eSe.FirstName;
                            eoConsumerTraceWseManager.AccountType = eSe.AccountNo;
                            eoConsumerTraceWseManager.BankName = eSe.BusBusinessName;
                            eoConsumerTraceWseManager.PostalCode = eSe.BusVatNumber;
                            eoConsumerTraceWseManager.CreatedbyUser = eSe.CreatedByUser;
                            eoConsumerTraceWseManager.Income = eSe.Income;
                            eoConsumerTraceWseManager.ReferenceNo = eSC.Reference;
                            eoConsumerTraceWseManager.subscriberID = eSe.SubscriberID;
                            eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;
                            eoConsumerTraceWseManager.AssociationTypeCode = sub.AssociationTypeCode;
                            eoConsumerTraceWseManager.ProductID = eSe.ProductID;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.EnquiryID = intSubscriberEnquiryID;
                            eoConsumerTraceWseManager.EnquiryResultID = intSubscriberEnquiryResultID;
                            eoConsumerTraceWseManager.RequestNo = RequestNo;
                            eoConsumerTraceWseManager.RequestMSISDN = RequestMSISDN;
                            eoConsumerTraceWseManager.ContactNo = ContactNo;

                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.GetVodacomCustomVetting(eoConsumerTraceWseManager);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.GetVodacomCustomVetting(eoConsumerTraceWseManager);
                            }

                            rXml = rp.ResponseData;

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                throw new Exception(rp.ResponseData);
                            }
                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                bool IsFootprintMatch;
                                IsFootprintMatch = ra.ConsumerFootprintMatch(eSC.KeyID, eSe.SubscriberID, eSe.ProductID, intSubscriberEnquiryResultID);

                                if (!IsFootprintMatch)
                                {
                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                    {
                                        eSC.Billable = false;
                                    }
                                    else
                                    {
                                        eSC.Billable = true;
                                    }
                                    eSC.ChangedByUser = eSe.CreatedByUser;
                                    eSC.ChangedOnDate = DateTime.Now;

                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                    eSC.ProductID = eSe.ProductID;

                                    dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                    if (sub.PayAsYouGo == 1)
                                    {
                                        dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                    }
                                    if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                    {
                                        dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                    }

                                    //Log FootPrint
                                    Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                    Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                    if (String.IsNullOrEmpty(eSC.VoucherCode))
                                    {
                                        eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                        eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                        eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                        eSubscriberFootPrint.KeyID = eSC.KeyID;
                                        eSubscriberFootPrint.KeyType = eSC.KeyType;
                                        eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                        eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                        if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                        {
                                            eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                        }
                                        eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                        eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                        eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                        eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                        eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                        eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                        eSubscriberFootPrint.CreatedByUser = sys.Username;
                                        eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                        eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                        eSubscriberFootPrint.ProductID = eSe.ProductID;
                                        eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                        int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                    }
                                }
                                else
                                {
                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    eSC.Billable = false;
                                    eSC.ChangedByUser = eSe.CreatedByUser;
                                    eSC.ChangedOnDate = DateTime.Now;

                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    //eSC.BillingPrice = spr.UnitPrice;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                    eSC.ProductID = eSe.ProductID;

                                    dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;
                                }
                            }

                            rXml = rp.ResponseData;
                        }
                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.

                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        }
                    }
                    else
                    {
                        // When User want to Re-Open a report

                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = eSC.XMLData;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitCustomVettingG(SqlConnection con, SqlConnection AdminConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, bool bBonusChecking, string strVoucherCode)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            string rXml = "";
            double Totalcost = 0;

            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();


            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML
                // use this XML data for generating the Report

                if (spr.ProductID > 0)
                {

                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {

                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }

                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {
                            eoConsumerTraceWseManager.ConsumerID = eSC.KeyID;
                            eoConsumerTraceWseManager.CreatedbyUser = eSe.CreatedByUser;
                            eoConsumerTraceWseManager.GrossMonthlyIncome = (double)eSe.NetMonthlyIncomeAmt;
                            eoConsumerTraceWseManager.ReferenceNo = eSC.Reference;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.EnquiryID = intSubscriberEnquiryID;
                            eoConsumerTraceWseManager.EnquiryResultID = intSubscriberEnquiryResultID;
                            eoConsumerTraceWseManager.subscriberID = eSe.SubscriberID;
                            eoConsumerTraceWseManager.ProductID = eSe.ProductID;

                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.CustomvettingG(eoConsumerTraceWseManager);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.CustomvettingG(eoConsumerTraceWseManager);
                            }

                            rXml = rp.ResponseData;

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                throw new Exception(rp.ResponseData);
                            }


                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                eSC.DetailsViewedDate = DateTime.Now;
                                eSC.DetailsViewedYN = true;
                                if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    eSC.Billable = false;
                                }
                                else
                                {
                                    eSC.Billable = true;
                                }
                                eSC.ChangedByUser = eSe.CreatedByUser;
                                eSC.ChangedOnDate = DateTime.Now;

                                eSC.KeyID = rp.ResponseKey;
                                eSC.KeyType = rp.ResponseKeyType;
                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                eSC.BillingTypeID = spr.BillingTypeID;
                                eSC.BillingPrice = spr.UnitPrice;

                                eSC.XMLData = rXml.Replace("'", "''");
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                eSC.ProductID = eSe.ProductID;

                                dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                rp.EnquiryID = eSC.SubscriberEnquiryID;

                                // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                if (sub.PayAsYouGo == 1)
                                {
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                }
                                if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                }

                                if (String.IsNullOrEmpty(eSC.VoucherCode))
                                {
                                    //Log FootPrint

                                    eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                    eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                    eSubscriberFootPrint.KeyID = eSC.KeyID;
                                    eSubscriberFootPrint.KeyType = eSC.KeyType;
                                    eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                    eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                    if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                    {
                                        eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                    }
                                    eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                    eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                    eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                    eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                    eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                    eSubscriberFootPrint.CreatedByUser = sys.Username;
                                    eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                    eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                    eSubscriberFootPrint.ProductID = eSe.ProductID;
                                    eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                    int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                }

                            }

                            rXml = rp.ResponseData;

                            System.IO.StringReader xmlResult = new System.IO.StringReader(rp.ResponseData);

                            ds.ReadXml(xmlResult);

                            if (ds.Tables.Contains("VettingResult"))
                            {
                                foreach (DataRow dr in ds.Tables["VettingResult"].Rows)
                                {
                                    dsresult.Tables[0].Rows[0][0] = string.IsNullOrEmpty(eSe.IDNo) ? (string.IsNullOrEmpty(eSe.PassportNo) ? string.Empty : eSe.PassportNo) : eSe.IDNo;
                                    dsresult.Tables[0].Rows[0][1] = dr["ErrorDescription"].ToString();
                                    dsresult.Tables[0].Rows[0][2] = dr["Result"].ToString();
                                    dsresult.Tables[0].Rows[0][3] = dr["FailReason"].ToString();
                                    dsresult.Tables[0].Rows[0][4] = dr["Action"].ToString();
                                    dsresult.Tables[0].Rows[0][5] = dr["MaxInstalMent"].ToString();
                                }
                            }
                        }

                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.

                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                        }

                    }
                    else
                    {
                        // When User want to Re-Open a report

                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = eSC.XMLData;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }


                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;


        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitCustomVettingH(SqlConnection con, SqlConnection AdminConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, bool bBonusChecking, string strVoucherCode)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            string rXml = "";
            double Totalcost = 0;

            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            XDSPortalLibrary.Entity_Layer.Response rpCr = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                SqlConnection objConstring = new SqlConnection(eSe.ExtraVarInput1.ToString());

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML
                // use this XML data for generating the Report

                if (spr.ProductID > 0)
                {

                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {

                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }

                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {
                            eoConsumerTraceWseManager.ConsumerID = eSC.KeyID;
                            eoConsumerTraceWseManager.CreatedbyUser = eSe.CreatedByUser;
                            //eoConsumerTraceWseManager.GrossMonthlyIncome = (double)eSe.NetMonthlyIncomeAmt;
                            eoConsumerTraceWseManager.GrossMonthlyIncome = 0;
                            eoConsumerTraceWseManager.ReferenceNo = eSC.Reference;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.EnquiryID = intSubscriberEnquiryID;
                            eoConsumerTraceWseManager.EnquiryResultID = intSubscriberEnquiryResultID;
                            eoConsumerTraceWseManager.subscriberID = eSe.SubscriberID;
                            eoConsumerTraceWseManager.ProductID = eSe.ProductID;
                            eoConsumerTraceWseManager.UserName = sys.Username;
                            eoConsumerTraceWseManager.AssociationTypeCode = sub.AssociationTypeCode;
                            eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;

                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.CustomvettingH(eoConsumerTraceWseManager);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.CustomvettingH(eoConsumerTraceWseManager);
                            }

                            eoConsumerCreditGrantorWseManager.ConsumerID = eSC.KeyID;
                            eoConsumerCreditGrantorWseManager.EnquiryReason = eSe.EnquiryReason;
                            eoConsumerCreditGrantorWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerCreditGrantorWseManager.ReferenceNo = eSC.Reference;
                            eoConsumerCreditGrantorWseManager.subscriberID = eSe.SubscriberID;
                            eoConsumerCreditGrantorWseManager.ProductID = 15;
                            eoConsumerCreditGrantorWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerCreditGrantorWseManager.TmpReference = eSC.ExtraVarOutput1;
                            moConsumerCreditGrantorWseManager.ConnectionString = objConstring;
                            eoConsumerCreditGrantorWseManager.DataSegments = eSe.ExtraVarInput2;
                            eoConsumerCreditGrantorWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;
                            eoConsumerCreditGrantorWseManager.AssociationTypeCode = sub.AssociationTypeCode;
                            eoConsumerCreditGrantorWseManager.EnquiryID = intSubscriberEnquiryID;
                            eoConsumerCreditGrantorWseManager.EnquiryResultID = intSubscriberEnquiryResultID;
                            eoConsumerCreditGrantorWseManager.Username = eSe.CreatedByUser;
                            eoConsumerCreditGrantorWseManager.SAFPSIndicator = sub.SAFPSIndicator;
                            eoConsumerCreditGrantorWseManager.DisplayUnusableInformation = sub.DisplayUnusableInformation;

                            //XDSPortalEnquiry.Data.Consumer oEnquiryConsumer = new XDSPortalEnquiry.Data.Consumer();
                            //oEnquiryConsumer.CheckBlockedStatus(AdminConnection, int.Parse(eSe.SubscriberID.ToString()), int.Parse(eSC.KeyID.ToString()), 15, sub.SubscriberAssociationCode, 1);

                            XDSDataLibrary.ReportAccess raCr = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rpCr = raCr.GetCreditEnquiryReport(eoConsumerCreditGrantorWseManager);

                            if (rpCr.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                raCr = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rpCr = raCr.GetCreditEnquiryReport(eoConsumerCreditGrantorWseManager);
                            }

                            //rXml = rp.ResponseData;

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error || rpCr.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSC.EnquiryResult = "E";
                                eSe.ErrorDescription = rp.ResponseData.ToString() + "|" + rpCr.ResponseData.ToString();
                                dSe.UpdateSubscriberEnquiryError(con, eSe);
                                throw new Exception(rp.ResponseData + "|" + rpCr.ResponseData);
                            }

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report && rpCr.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                DataSet dsCu = new DataSet();
                                DataSet dsCr = new DataSet();
                                System.Xml.XmlDocument rsXmlCu = new System.Xml.XmlDocument();
                                System.Xml.XmlDocument rsXmlCr = new System.Xml.XmlDocument();

                                rsXmlCu.LoadXml(rp.ResponseData);
                                rsXmlCr.LoadXml(rpCr.ResponseData);

                                XmlNodeReader xnrCu = new XmlNodeReader(rsXmlCu);
                                XmlNodeReader xnrCr = new XmlNodeReader(rsXmlCr);

                                dsCu.ReadXml(xnrCu);
                                dsCr.ReadXml(xnrCr);

                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, eSe.ProductID);

                                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryReason", typeof(String));

                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();
                                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();
                                drSubscriberInput["EnquiryReason"] = eSe.EnquiryReason.Trim();

                                dtSubscriberInput.Rows.Add(drSubscriberInput);
                                dsCu.Tables.Add(dtSubscriberInput);

                                dsCu.Merge(dsCr);

                                rXml = dsCu.GetXml();

                                byte[] ReportPdf = GetCustomHReport(rXml);

                                DataTable dtReportPdf = new DataTable("ReportPdf");
                                dtReportPdf.Columns.Add("Binary", typeof(byte[]));

                                DataRow drReportPdf;
                                drReportPdf = dtReportPdf.NewRow();
                                drReportPdf["Binary"] = ReportPdf;
                                dtReportPdf.Rows.Add(drReportPdf);

                                DataSet dsResult = new DataSet();
                                System.IO.StringReader xmlSR1 = new System.IO.StringReader(rXml);
                                dsResult.ReadXml(xmlSR1);

                                dsResult.Tables.Add(dtReportPdf);

                                rXml = dsResult.GetXml();

                                foreach (DataRow r in dsCu.Tables["ConsumerDetail"].Rows)
                                {
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }
                                }

                                eSC.DetailsViewedDate = DateTime.Now;
                                eSC.DetailsViewedYN = true;
                                if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    eSC.Billable = false;
                                }
                                else
                                {
                                    eSC.Billable = true;
                                }
                                eSC.ChangedByUser = eSe.CreatedByUser;
                                eSC.ChangedOnDate = DateTime.Now;

                                eSC.KeyID = rp.ResponseKey;
                                eSC.KeyType = rp.ResponseKeyType;
                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                eSC.BillingTypeID = spr.BillingTypeID;
                                eSC.BillingPrice = spr.UnitPrice;

                                eSC.XMLData = rXml.Replace("'", "''");
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                eSC.ProductID = eSe.ProductID;

                                if ((dsCu.Tables.Contains("ConsumerNLRDebtSummary") && dsCu.Tables["ConsumerNLRDebtSummary"].Rows.Count > 0) || (dsCu.Tables.Contains("ConsumerCPANLRDebtSummary") && dsCu.Tables["ConsumerCPANLRDebtSummary"].Rows.Count > 0) || (dsCu.Tables.Contains("ConsumerNLRAccountStatus") && dsCu.Tables["ConsumerNLRAccountStatus"].Rows.Count > 0) || (dsCu.Tables.Contains("ConsumerNLR24MonthlyPayment") && dsCu.Tables["ConsumerNLR24MonthlyPayment"].Rows.Count > 0))
                                    eSC.NLRDataIncluded = true;

                                dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                rp.EnquiryID = eSC.SubscriberEnquiryID;
                                rp.ResponseData = eSC.XMLData;

                                // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                if (sub.PayAsYouGo == 1)
                                {
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                }
                                if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                }

                                if (String.IsNullOrEmpty(eSC.VoucherCode))
                                {
                                    //Log FootPrint

                                    eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                    eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                    eSubscriberFootPrint.KeyID = eSC.KeyID;
                                    eSubscriberFootPrint.KeyType = eSC.KeyType;
                                    eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                    eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                    if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                    {
                                        eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                    }
                                    eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                    eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                    eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                    eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                    eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                    eSubscriberFootPrint.CreatedByUser = sys.Username;
                                    eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                    eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                    eSubscriberFootPrint.ProductID = eSe.ProductID;
                                    eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                    int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                }

                                //if (String.IsNullOrEmpty(eSC.VoucherCode))
                                //{
                                //    //Log FootPrint
                                //    Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                //    Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                //    eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                //    eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                //    eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                //    eSubscriberFootPrint.KeyID = eSC.KeyID;
                                //    eSubscriberFootPrint.KeyType = eSC.KeyType;
                                //    eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                //    eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                //    if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                //    {
                                //        eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                //    }
                                //    eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                //    eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                //    eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                //    eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                //    eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                //    eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                //    eSubscriberFootPrint.CreatedByUser = sys.Username;
                                //    eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                //    eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                //    eSubscriberFootPrint.ProductID = eSe.ProductID;

                                //    int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                //}
                                //Log FootPrint
                            }

                            //rXml = rp.ResponseData;
                        }
                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.

                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                        }
                    }
                    else
                    {
                        // When User want to Re-Open a report

                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = eSC.XMLData;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }


                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;


        }

        public byte[] GetCustomHReport(string rXml)
        {
            byte[] binFile = new byte[0];
            MemoryStream oMemoryStream = new MemoryStream();
            XtraReport oXdsReport = new XtraReport();

            try
            {
                oXdsReport = new XdsPortalReports.MPOWACustomCreditReport(rXml);
                oXdsReport.ExportToPdf(oMemoryStream);
            }
            catch (Exception oException)
            {
                rXml = "<NoResult><Error>" + oException.Message + "</Error></NoResult>";
                oXdsReport = new XdsPortalReports.ErrorReport(rXml);
                oXdsReport.ExportToPdf(oMemoryStream);
            }

            oMemoryStream.Seek(0, System.IO.SeekOrigin.Begin);
            BinaryReader binReader = new BinaryReader(oMemoryStream);
            binReader.BaseStream.Position = 0;
            binFile = binReader.ReadBytes(Convert.ToInt32(binReader.BaseStream.Length));
            binReader.Close();

            oMemoryStream = null;
            //File.WriteAllBytes(@"C:\Credit4Life\4.pdf", binFile);
            return binFile;

        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitCustomVettingI(SqlConnection con, SqlConnection AdminConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, bool bBonusChecking, string strVoucherCode)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            string rXml = "";
            double Totalcost = 0;

            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML
                // use this XML data for generating the Report

                if (spr.ProductID > 0)
                {

                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {

                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }

                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {
                            eoConsumerTraceWseManager.ConsumerID = eSC.KeyID;
                            eoConsumerTraceWseManager.CreatedbyUser = eSe.CreatedByUser;
                            //eoConsumerTraceWseManager.GrossMonthlyIncome = (double)eSe.NetMonthlyIncomeAmt;
                            eoConsumerTraceWseManager.GrossMonthlyIncome = 0;
                            eoConsumerTraceWseManager.ReferenceNo = eSC.Reference;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.EnquiryID = intSubscriberEnquiryID;
                            eoConsumerTraceWseManager.EnquiryResultID = intSubscriberEnquiryResultID;
                            eoConsumerTraceWseManager.subscriberID = eSe.SubscriberID;
                            eoConsumerTraceWseManager.ProductID = eSe.ProductID;
                            eoConsumerTraceWseManager.UserName = sys.Username;
                            eoConsumerTraceWseManager.AssociationTypeCode = sub.AssociationTypeCode;
                            eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;

                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.CustomvettingI(eoConsumerTraceWseManager);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.CustomvettingI(eoConsumerTraceWseManager);
                            }

                            rXml = rp.ResponseData;

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                throw new Exception(rp.ResponseData);
                            }


                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                eSC.DetailsViewedDate = DateTime.Now;
                                eSC.DetailsViewedYN = true;
                                if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    eSC.Billable = false;
                                }
                                else
                                {
                                    eSC.Billable = true;
                                }
                                eSC.ChangedByUser = eSe.CreatedByUser;
                                eSC.ChangedOnDate = DateTime.Now;

                                eSC.KeyID = rp.ResponseKey;
                                eSC.KeyType = rp.ResponseKeyType;
                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                eSC.BillingTypeID = spr.BillingTypeID;
                                eSC.BillingPrice = spr.UnitPrice;

                                eSC.XMLData = rXml.Replace("'", "''");
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                eSC.ProductID = eSe.ProductID;

                                dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                rp.EnquiryID = eSC.SubscriberEnquiryID;

                                // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                if (sub.PayAsYouGo == 1)
                                {
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                }
                                if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                }

                                if (String.IsNullOrEmpty(eSC.VoucherCode))
                                {
                                    //Log FootPrint

                                    eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                    eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                    eSubscriberFootPrint.KeyID = eSC.KeyID;
                                    eSubscriberFootPrint.KeyType = eSC.KeyType;
                                    eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                    eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                    if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                    {
                                        eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                    }
                                    eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                    eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                    eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                    eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                    eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                    eSubscriberFootPrint.CreatedByUser = sys.Username;
                                    eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                    eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                    eSubscriberFootPrint.ProductID = eSe.ProductID;
                                    eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                    int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                }
                            }

                            rXml = rp.ResponseData;
                        }
                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.

                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                        }
                    }
                    else
                    {
                        // When User want to Re-Open a report

                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = eSC.XMLData;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }


                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitCustomVettingJ(SqlConnection con, SqlConnection AdminConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, bool bBonusChecking, string strVoucherCode)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            string rXml = "";
            double Totalcost = 0;

            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML
                // use this XML data for generating the Report

                if (spr.ProductID > 0)
                {

                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {

                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }

                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {
                            eoConsumerTraceWseManager.ConsumerID = eSC.KeyID;
                            eoConsumerTraceWseManager.CreatedbyUser = eSe.CreatedByUser;
                            //eoConsumerTraceWseManager.GrossMonthlyIncome = (double)eSe.NetMonthlyIncomeAmt;
                            eoConsumerTraceWseManager.GrossMonthlyIncome = 0;
                            eoConsumerTraceWseManager.ReferenceNo = eSC.Reference;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.EnquiryID = intSubscriberEnquiryID;
                            eoConsumerTraceWseManager.EnquiryResultID = intSubscriberEnquiryResultID;
                            eoConsumerTraceWseManager.subscriberID = eSe.SubscriberID;
                            eoConsumerTraceWseManager.ProductID = eSe.ProductID;
                            eoConsumerTraceWseManager.UserName = sys.Username;
                            eoConsumerTraceWseManager.AssociationTypeCode = sub.AssociationTypeCode;
                            eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;

                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.CustomvettingJ(eoConsumerTraceWseManager);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.CustomvettingJ(eoConsumerTraceWseManager);
                            }

                            rXml = rp.ResponseData;

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                throw new Exception(rp.ResponseData);
                            }


                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                eSC.DetailsViewedDate = DateTime.Now;
                                eSC.DetailsViewedYN = true;
                                if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    eSC.Billable = false;
                                }
                                else
                                {
                                    eSC.Billable = true;
                                }
                                eSC.ChangedByUser = eSe.CreatedByUser;
                                eSC.ChangedOnDate = DateTime.Now;

                                eSC.KeyID = rp.ResponseKey;
                                eSC.KeyType = rp.ResponseKeyType;
                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                eSC.BillingTypeID = spr.BillingTypeID;
                                eSC.BillingPrice = spr.UnitPrice;

                                eSC.XMLData = rXml.Replace("'", "''");
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                eSC.ProductID = eSe.ProductID;

                                dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                rp.EnquiryID = eSC.SubscriberEnquiryID;

                                // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                if (sub.PayAsYouGo == 1)
                                {
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                }
                                if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                }

                                if (String.IsNullOrEmpty(eSC.VoucherCode))
                                {
                                    //Log FootPrint

                                    eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                    eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                    eSubscriberFootPrint.KeyID = eSC.KeyID;
                                    eSubscriberFootPrint.KeyType = eSC.KeyType;
                                    eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                    eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                    if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                    {
                                        eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                    }
                                    eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                    eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                    eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                    eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                    eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                    eSubscriberFootPrint.CreatedByUser = sys.Username;
                                    eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                    eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                    eSubscriberFootPrint.ProductID = eSe.ProductID;
                                    eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                    int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                }
                            }

                            rXml = rp.ResponseData;
                        }
                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.

                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                        }
                    }
                    else
                    {
                        // When User want to Re-Open a report

                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = eSC.XMLData;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }


                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitCustomVettingK(SqlConnection con, SqlConnection AdminConnection,
             int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, bool bBonusChecking, string strVoucherCode, string[] ApplicantionDetails)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            string rXml = "";
            double Totalcost = 0;

            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML
                // use this XML data for generating the Report

                if (spr.ProductID > 0)
                {
                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {

                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }

                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {
                            eoConsumerTraceWseManager.ConsumerID = eSC.KeyID;
                            eoConsumerTraceWseManager.CreatedbyUser = eSe.CreatedByUser;
                            eoConsumerTraceWseManager.GrossMonthlyIncome = 0;
                            eoConsumerTraceWseManager.ReferenceNo = eSC.Reference;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.EnquiryID = intSubscriberEnquiryID;
                            eoConsumerTraceWseManager.EnquiryResultID = intSubscriberEnquiryResultID;
                            eoConsumerTraceWseManager.subscriberID = eSe.SubscriberID;
                            eoConsumerTraceWseManager.ProductID = eSe.ProductID;
                            eoConsumerTraceWseManager.UserName = sys.Username;
                            eoConsumerTraceWseManager.AssociationTypeCode = sub.AssociationTypeCode;
                            eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;
                            eoConsumerTraceWseManager.SFID = spr.ReportID.ToString();

                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.CustomvettingK(eoConsumerTraceWseManager, ApplicantionDetails);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.CustomvettingK(eoConsumerTraceWseManager, ApplicantionDetails);
                            }

                            rXml = rp.ResponseData;

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                throw new Exception(rp.ResponseData);
                            }


                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                eSC.DetailsViewedDate = DateTime.Now;
                                eSC.DetailsViewedYN = true;
                                if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    eSC.Billable = false;
                                }
                                else
                                {
                                    eSC.Billable = true;
                                }
                                eSC.ChangedByUser = eSe.CreatedByUser;
                                eSC.ChangedOnDate = DateTime.Now;

                                eSC.KeyID = rp.ResponseKey;
                                eSC.KeyType = rp.ResponseKeyType;
                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                eSC.BillingTypeID = spr.BillingTypeID;
                                eSC.BillingPrice = spr.UnitPrice;

                                eSC.XMLData = rXml.Replace("'", "''");
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                eSC.ProductID = eSe.ProductID;

                                dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                rp.EnquiryID = eSC.SubscriberEnquiryID;

                                // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                if (sub.PayAsYouGo == 1)
                                {
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                }
                                if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                }
                            }

                            rXml = rp.ResponseData;
                        }
                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.

                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                        }
                    }
                    else
                    {
                        // When User want to Re-Open a report

                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = eSC.XMLData;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }


                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitCustomVettingL(SqlConnection con, SqlConnection AdminConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, bool bBonusChecking, string strVoucherCode)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            string rXml = "";
            double Totalcost = 0;

            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML
                // use this XML data for generating the Report

                if (spr.ProductID > 0)
                {

                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {

                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }

                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {
                            eoConsumerTraceWseManager.ConsumerID = eSC.KeyID;
                            eoConsumerTraceWseManager.CreatedbyUser = eSe.CreatedByUser;
                            //eoConsumerTraceWseManager.GrossMonthlyIncome = (double)eSe.NetMonthlyIncomeAmt;
                            eoConsumerTraceWseManager.GrossMonthlyIncome = 0;
                            eoConsumerTraceWseManager.ReferenceNo = eSC.Reference;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.EnquiryID = intSubscriberEnquiryID;
                            eoConsumerTraceWseManager.EnquiryResultID = intSubscriberEnquiryResultID;
                            eoConsumerTraceWseManager.subscriberID = eSe.SubscriberID;
                            eoConsumerTraceWseManager.ProductID = eSe.ProductID;
                            eoConsumerTraceWseManager.UserName = sys.Username;
                            eoConsumerTraceWseManager.AssociationTypeCode = sub.AssociationTypeCode;
                            eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;

                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.CustomvettingL(eoConsumerTraceWseManager);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.CustomvettingL(eoConsumerTraceWseManager);
                            }

                            rXml = rp.ResponseData;

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                throw new Exception(rp.ResponseData);
                            }


                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                eSC.DetailsViewedDate = DateTime.Now;
                                eSC.DetailsViewedYN = true;
                                if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    eSC.Billable = false;
                                }
                                else
                                {
                                    eSC.Billable = true;
                                }
                                eSC.ChangedByUser = eSe.CreatedByUser;
                                eSC.ChangedOnDate = DateTime.Now;

                                eSC.KeyID = rp.ResponseKey;
                                eSC.KeyType = rp.ResponseKeyType;
                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                eSC.BillingTypeID = spr.BillingTypeID;
                                eSC.BillingPrice = spr.UnitPrice;

                                eSC.XMLData = rXml.Replace("'", "''");
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                eSC.ProductID = eSe.ProductID;

                                dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                rp.EnquiryID = eSC.SubscriberEnquiryID;

                                // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                if (sub.PayAsYouGo == 1)
                                {
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                }
                                if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                }

                                if (String.IsNullOrEmpty(eSC.VoucherCode))
                                {
                                    //Log FootPrint

                                    eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                    eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                    eSubscriberFootPrint.KeyID = eSC.KeyID;
                                    eSubscriberFootPrint.KeyType = eSC.KeyType;
                                    eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                    eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                    if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                    {
                                        eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                    }
                                    eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                    eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                    eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                    eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                    eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                    eSubscriberFootPrint.CreatedByUser = sys.Username;
                                    eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                    eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                    eSubscriberFootPrint.ProductID = eSe.ProductID;
                                    eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                    int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                }
                            }

                            rXml = rp.ResponseData;
                        }
                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.

                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                        }
                    }
                    else
                    {
                        // When User want to Re-Open a report

                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = eSC.XMLData;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }


                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitCustomVettingMFin(SqlConnection con, SqlConnection AdminConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, bool bBonusChecking, string strVoucherCode)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            string rXml = "";
            double Totalcost = 0;

            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML
                // use this XML data for generating the Report

                if (spr.ProductID > 0)
                {

                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {

                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }

                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {
                            eoConsumerTraceWseManager.ConsumerID = eSC.KeyID;
                            eoConsumerTraceWseManager.CreatedbyUser = eSe.CreatedByUser;
                            //eoConsumerTraceWseManager.GrossMonthlyIncome = (double)eSe.NetMonthlyIncomeAmt;
                            eoConsumerTraceWseManager.GrossMonthlyIncome = 0;
                            eoConsumerTraceWseManager.ReferenceNo = eSC.Reference;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.EnquiryID = intSubscriberEnquiryID;
                            eoConsumerTraceWseManager.EnquiryResultID = intSubscriberEnquiryResultID;
                            eoConsumerTraceWseManager.subscriberID = eSe.SubscriberID;
                            eoConsumerTraceWseManager.ProductID = eSe.ProductID;
                            eoConsumerTraceWseManager.UserName = sys.Username;
                            eoConsumerTraceWseManager.AssociationTypeCode = sub.AssociationTypeCode;
                            eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;

                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.CustomvettingMFin(eoConsumerTraceWseManager);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.CustomvettingMFin(eoConsumerTraceWseManager);
                            }

                            rXml = rp.ResponseData;

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                throw new Exception(rp.ResponseData);
                            }


                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                eSC.DetailsViewedDate = DateTime.Now;
                                eSC.DetailsViewedYN = true;
                                if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    eSC.Billable = false;
                                }
                                else
                                {
                                    eSC.Billable = true;
                                }
                                eSC.ChangedByUser = eSe.CreatedByUser;
                                eSC.ChangedOnDate = DateTime.Now;

                                eSC.KeyID = rp.ResponseKey;
                                eSC.KeyType = rp.ResponseKeyType;
                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                eSC.BillingTypeID = spr.BillingTypeID;
                                eSC.BillingPrice = spr.UnitPrice;

                                eSC.XMLData = rXml.Replace("'", "''");
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                eSC.ProductID = eSe.ProductID;

                                dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                rp.EnquiryID = eSC.SubscriberEnquiryID;

                                // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                if (sub.PayAsYouGo == 1)
                                {
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                }
                                if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                }

                                if (String.IsNullOrEmpty(eSC.VoucherCode))
                                {
                                    //Log FootPrint

                                    eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                    eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                    eSubscriberFootPrint.KeyID = eSC.KeyID;
                                    eSubscriberFootPrint.KeyType = eSC.KeyType;
                                    eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                    eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                    if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                    {
                                        eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                    }
                                    eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                    eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                    eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                    eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                    eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                    eSubscriberFootPrint.CreatedByUser = sys.Username;
                                    eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                    eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                    eSubscriberFootPrint.ProductID = eSe.ProductID;
                                    eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                    int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                }
                            }

                            rXml = rp.ResponseData;
                        }
                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.

                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                        }
                    }
                    else
                    {
                        // When User want to Re-Open a report

                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = eSC.XMLData;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }


                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitCustomVettingN(SqlConnection con, SqlConnection AdminConnection,
           int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, bool bBonusChecking, string strVoucherCode, string[] ApplicantionDetails, string EndpointAddress,
            string smtpServer, string smtpUser, string smtpPassword, string smtpPort, string fromAddress, string toAddress, string ccAddress)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            string rXml = "";
            double Totalcost = 0;

            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML
                // use this XML data for generating the Report

                if (spr.ProductID > 0)
                {
                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {

                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }

                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {
                            if (eSC.KeyType == "H")
                                eoConsumerTraceWseManager.HomeAffairsID = eSC.KeyID;
                            else
                                eoConsumerTraceWseManager.ConsumerID = eSC.KeyID;

                            eoConsumerTraceWseManager.CreatedbyUser = eSe.CreatedByUser;
                            eoConsumerTraceWseManager.GrossMonthlyIncome = 0;
                            eoConsumerTraceWseManager.ReferenceNo = eSC.Reference;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.EnquiryID = intSubscriberEnquiryID;
                            eoConsumerTraceWseManager.EnquiryResultID = intSubscriberEnquiryResultID;
                            eoConsumerTraceWseManager.subscriberID = eSe.SubscriberID;
                            eoConsumerTraceWseManager.ProductID = eSe.ProductID;
                            eoConsumerTraceWseManager.UserName = sys.Username;
                            eoConsumerTraceWseManager.AssociationTypeCode = sub.AssociationTypeCode;
                            eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;
                            eoConsumerTraceWseManager.SFID = spr.ReportID.ToString();

                            if (eoConsumerTraceWseManager.ConsumerID == 0)
                            {
                                SqlConnection objConstring = new SqlConnection(eSe.ExtraVarInput1.ToString());

                                XDSPortalEnquiry.Data.Consumer oConsumer = new XDSPortalEnquiry.Data.Consumer();
                                eoConsumerTraceWseManager.ConsumerID = oConsumer.GetConsumerID(objConstring, 0, eoConsumerTraceWseManager.HomeAffairsID);
                            }

                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.CustomvettingN(eoConsumerTraceWseManager, ApplicantionDetails);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.CustomvettingN(eoConsumerTraceWseManager, ApplicantionDetails);
                            }

                            rXml = rp.ResponseData;


                            System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                throw new Exception(rp.ResponseData);
                            }


                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {

                                ds.ReadXml(xmlSR);
                                Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                eSC.DetailsViewedDate = DateTime.Now;
                                eSC.DetailsViewedYN = true;
                                if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    eSC.Billable = false;
                                }
                                else
                                {
                                    eSC.Billable = true;
                                }
                                eSC.ChangedByUser = eSe.CreatedByUser;
                                eSC.ChangedOnDate = DateTime.Now;

                                eSC.KeyID = rp.ResponseKey;
                                eSC.KeyType = rp.ResponseKeyType;
                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                eSC.BillingTypeID = spr.BillingTypeID;
                                eSC.BillingPrice = spr.UnitPrice;


                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                eSC.ProductID = eSe.ProductID;


                                DateTime Applicantdateofbirth = eSC.BirthDate;

                                if (ds.Tables.Count > 0)
                                {
                                    DateTime.TryParse(ds.Tables[0].Rows[0]["BirthDate"].ToString(), out Applicantdateofbirth);
                                }


                                decimal income;
                                Decimal.TryParse(ApplicantionDetails[3].ToString(), out income);


                                string step = string.Empty;


                                eSC.ExtraVarOutput1 = ApplicantionDetails[15].ToString();


                                eSC.ExtraVarOutput3 = Applicantdateofbirth.ToShortDateString();

                                DataColumn dcResponse = new DataColumn("VodacomResponse", typeof(string));
                                DataColumn dcResponseType = new DataColumn("VodacomResponseType", typeof(string));
                                DataColumn dcEmail = new DataColumn("EmailStatus", typeof(string));



                                try
                                {


                                    ////VodacomManageMarketingCamPaignsV1.CreateVettingPreApprovalsType oVettingRequest = new XDSPortalEnquiry.VodacomManageMarketingCamPaignsV1.CreateVettingPreApprovalsType();


                                    ////oVettingRequest.ReferenceNo = eSC.Reference;
                                    ////oVettingRequest.EnquiryId = eSC.SubscriberEnquiryID.ToString();
                                    ////oVettingRequest.DealerId = ApplicantionDetails[8].ToString();
                                    ////oVettingRequest.PromotionDeal = ApplicantionDetails[9].ToString();
                                    ////oVettingRequest.PromoCode = ApplicantionDetails[10].ToString();
                                    ////oVettingRequest.FirstName = ApplicantionDetails[0].ToString();
                                    ////oVettingRequest.Surname = ApplicantionDetails[1].ToString();
                                    ////oVettingRequest.IdNo = ApplicantionDetails[2].ToString();
                                    ////oVettingRequest.BirthDate = Applicantdateofbirth;
                                    ////oVettingRequest.Income = income;
                                    ////oVettingRequest.ContactTelNo = ApplicantionDetails[4].ToString();
                                    ////oVettingRequest.OtherContactTelNo = ApplicantionDetails[5].ToString();
                                    ////oVettingRequest.EmailAddress = ApplicantionDetails[6].ToString();
                                    ////oVettingRequest.CreatedByUser = ApplicantionDetails[13].ToString();
                                    ////oVettingRequest.CreatedOnDate = DateTime.Now;

                                    ////if (ds.Tables.Count > 0)
                                    ////{
                                    ////    oVettingRequest.Portfolio = ds.Tables[0].Rows[0]["Portfolio"].ToString();
                                    ////    oVettingRequest.Score = ds.Tables[0].Rows[0]["Score"].ToString();
                                    ////    oVettingRequest.Decision = ds.Tables[0].Rows[0]["ApplicationStatus"].ToString();
                                    ////}

                                    ////VodacomManageMarketingCamPaignsV1.CreateVettingPreApprovalsResponseType oVettingResponse = new XDSPortalEnquiry.VodacomManageMarketingCamPaignsV1.CreateVettingPreApprovalsResponseType();

                                    ////System.ServiceModel.EndpointAddress oaddress = new System.ServiceModel.EndpointAddress(EndpointAddress);

                                    ////System.ServiceModel.Description.ServiceEndpoint oEndPoint = new System.ServiceModel.Description.ServiceEndpoint(new System.ServiceModel.Description.ContractDescription("VodacomManageMarketingCamPaignsV1.ManageMarketingCampaignsPortType"));
                                    ////oEndPoint.Address = oaddress;

                                    ////System.ServiceModel.Description.ClientCredentials ocredentials = new System.ServiceModel.Description.ClientCredentials();


                                    ////VodacomManageMarketingCamPaignsV1.ManageMarketingCampaignsPortTypeClient oclient = new XDSPortalEnquiry.VodacomManageMarketingCamPaignsV1.ManageMarketingCampaignsPortTypeClient();
                                    ////oVettingResponse = oclient.CreateVettingPreApprovals(oVettingRequest);

                                    ////VodacomManageMarketingCamPaignsV1.BaseESBFault oFault = new XDSPortalEnquiry.VodacomManageMarketingCamPaignsV1.BaseESBFault();

                                    //VodacomManageMarketingCamPaignsV1.CreateVettingPreApprovalsType oVettingRequest = new VodacomManageMarketingCamPaignsV1.CreateVettingPreApprovalsType();


                                    //oVettingRequest.ReferenceNo = eSC.Reference;
                                    //oVettingRequest.EnquiryId = eSC.SubscriberEnquiryID.ToString();
                                    //oVettingRequest.DealerId = ApplicantionDetails[8].ToString();
                                    //oVettingRequest.PromotionDeal = ApplicantionDetails[9].ToString();
                                    //oVettingRequest.PromoCode = ApplicantionDetails[10].ToString();
                                    //oVettingRequest.FirstName = ApplicantionDetails[0].ToString();
                                    //oVettingRequest.Surname = ApplicantionDetails[1].ToString();
                                    //oVettingRequest.IdNo = ApplicantionDetails[2].ToString();
                                    //oVettingRequest.BirthDate = Applicantdateofbirth;
                                    //oVettingRequest.Income = income;
                                    //oVettingRequest.ContactTelNo = ApplicantionDetails[4].ToString();
                                    //oVettingRequest.OtherContactTelNo = ApplicantionDetails[5].ToString();
                                    //oVettingRequest.EmailAddress = ApplicantionDetails[6].ToString();
                                    //oVettingRequest.CreatedByUser = ApplicantionDetails[13].ToString();
                                    //oVettingRequest.CreatedOnDate = DateTime.Now;

                                    //if (ds.Tables.Count > 0)
                                    //{
                                    //    oVettingRequest.Portfolio = ds.Tables[0].Rows[0]["Portfolio"].ToString();
                                    //    oVettingRequest.Score = ds.Tables[0].Rows[0]["Score"].ToString();
                                    //    oVettingRequest.Decision = ds.Tables[0].Rows[0]["ApplicationStatus"].ToString();
                                    //}

                                    //VodacomManageMarketingCamPaignsV1.CreateVettingPreApprovalsRequest orequest = new VodacomManageMarketingCamPaignsV1.CreateVettingPreApprovalsRequest(oVettingRequest);
                                    //step = "2";
                                    //VodacomManageMarketingCamPaignsV1.CreateVettingPreApprovalsResponse oVettingResponse = new VodacomManageMarketingCamPaignsV1.CreateVettingPreApprovalsResponse();
                                    //step = "3";
                                    //EndpointAddress oaddress = new EndpointAddress(new Uri(EndpointAddress), EndpointIdentity.CreateDnsIdentity("c3d-osbmgr.vodacom.corp"));

                                    //step = "4";
                                    //BasicHttpBinding basicHttpBinding = new BasicHttpBinding();

                                    //step = "4.1";
                                    //VodacomManageMarketingCamPaignsV1.ManageMarketingCampaignsPortType iclient = new ChannelFactory<VodacomManageMarketingCamPaignsV1.ManageMarketingCampaignsPortType>(basicHttpBinding, oaddress).CreateChannel();

                                    ////IClientChannel oIClientchannel = new ChannelFactory<VodacomManageMarketingCamPaignsV1.ManageMarketingCampaignsPortTypeChannel>(basicHttpBinding, oaddress).CreateChannel();
                                    ////OperationContext oContext = new OperationContext(oIClientchannel);

                                    ////using (new OperationContextScope(oContext))
                                    ////{
                                    ////    VodacomManageMarketingCamPaignsV1.RequestHeaderType oreqHeader = new VodacomManageMarketingCamPaignsV1.RequestHeaderType();
                                    ////    oreqHeader.UserId = "XDS";
                                    ////    oreqHeader.TransactionId = "5467892";
                                    ////    oreqHeader.TransactionCreationTimestamp = DateTime.Now;
                                    ////    oreqHeader.RequestApplicationId = "1";
                                    ////    oreqHeader.MessageCreationTimestamp = DateTime.Now;




                                    ////    MessageHeader oMessageHeader = MessageHeader.CreateHeader("RequestHeader", "http://tempuri.org", oreqHeader);
                                    ////    OperationContext.Current.OutgoingMessageHeaders.Add(oMessageHeader);

                                    ////    //HttpRequestMessageProperty requestMessage = new HttpRequestMessageProperty();
                                    ////    //requestMessage.Headers["MyHttpHeader"] = "MyHttpHeaderValue";
                                    ////    //OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = requestMessage;



                                    ////    XmlSerializer serializer = new XmlSerializer(orequest.GetType());
                                    ////    System.IO.StringWriter sw = new System.IO.StringWriter();
                                    ////    serializer.Serialize(sw, orequest);
                                    ////    System.IO.StringReader reader = new System.IO.StringReader(sw.ToString());

                                    ////    //Console.WriteLine(reader.ReadToEnd());

                                    ////    //XmlSerializer serializer1 = new XmlSerializer(oMessageHeader.GetType());
                                    ////    //System.IO.StringWriter sw1 = new System.IO.StringWriter();
                                    ////    //serializer1.Serialize(sw1, oMessageHeader);
                                    ////    //System.IO.StringReader reader2 = new System.IO.StringReader(sw1.ToString());

                                    ////    //Console.WriteLine(reader2.ReadToEnd());


                                    ////    oVettingResponse = iclient.CreateVettingPreApprovals(orequest);
                                    ////}

                                    //step = "4.2";


                                    //oVettingResponse = iclient.CreateVettingPreApprovals(orequest);

                                    XDSPortalLibrary.Entity_Layer.Response rpVodacom = new XDSPortalLibrary.Entity_Layer.Response();

                                    string Portfolio = string.Empty, Score = string.Empty,
                                       Decision = string.Empty, Region = string.Empty,
                                       AccountNo = string.Empty, SourceId = string.Empty, ReasonCode = string.Empty;
                                    ;

                                    if (ds.Tables.Count > 0)
                                    {
                                        Portfolio = ds.Tables[0].Rows[0]["Portfolio"].ToString();
                                        Score = ds.Tables[0].Rows[0]["Score"].ToString();
                                        Decision = ds.Tables[0].Rows[0]["ApplicationStatus"].ToString();

                                        Region = ds.Tables[0].Rows[0]["Region"].ToString();
                                        AccountNo = ds.Tables[0].Rows[0]["AccountNo"].ToString();
                                        SourceId = ds.Tables[0].Rows[0]["SourceInd"].ToString();
                                        ReasonCode = ds.Tables[0].Rows[0]["ReasonCode"].ToString();

                                        Decimal.TryParse(ds.Tables[0].Rows[0]["Income"].ToString(), out income);

                                        ds.Tables[0].Columns.Add(dcResponse);
                                        ds.Tables[0].Columns.Add(dcResponseType);
                                        ds.Tables[0].Columns.Add(dcEmail);
                                    }


                                    rpVodacom = submitTransactionToVodacom(ApplicantionDetails[13].ToString(), eSC.Reference, DateTime.Now, eSC.Reference, eSC.Reference,
                                        eSC.SubscriberEnquiryID.ToString(), ApplicantionDetails[8].ToString(), ApplicantionDetails[15].ToString(),
                                        ApplicantionDetails[10].ToString(), string.IsNullOrEmpty(ApplicantionDetails[0].ToString()) ? eSC.FirstName : ApplicantionDetails[0].ToString(),
                                        ApplicantionDetails[1].ToString(), ApplicantionDetails[2].ToString(), Applicantdateofbirth, income, ApplicantionDetails[4].ToString(),
                                        ApplicantionDetails[5].ToString(), ApplicantionDetails[6].ToString(), ApplicantionDetails[13].ToString(), DateTime.Now, "XDS", Portfolio, Score, Decision, EndpointAddress,
                                        smtpServer, smtpUser, smtpPassword, smtpPort, true, toAddress, fromAddress, ccAddress, "XDS UAT", Region, AccountNo, SourceId, ReasonCode);


                                    eSC.ExtraVarOutput2 = rpVodacom.ResponseData;


                                    ds.Tables[0].Rows[0]["VodacomResponse"] = rpVodacom.ResponseData.ToString();
                                    ds.Tables[0].Rows[0]["VodacomResponseType"] = rpVodacom.ResponseStatus.ToString();
                                    ds.Tables[0].Rows[0]["EmailStatus"] = rpVodacom.TmpReference.ToString();

                                }
                                catch (Exception ex)
                                {
                                    eSe.ErrorDescription = step + ex.Message + ex.StackTrace;
                                    //throw new Exception(step + ex.Message);

                                    ds.Tables[0].Rows[0]["VodacomResponse"] = ex.Message;
                                    ds.Tables[0].Rows[0]["VodacomResponseType"] = "Error";
                                }

                                rXml = ds.GetXml();
                                rp.ResponseData = rXml;
                                eSC.XMLData = rXml.Replace("'", "''");

                                dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                rp.EnquiryID = eSC.SubscriberEnquiryID;

                                // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                if (sub.PayAsYouGo == 1)
                                {
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                }
                                if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                }

                                if (String.IsNullOrEmpty(eSC.VoucherCode))
                                {
                                    //Log FootPrint

                                    eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                    eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                    eSubscriberFootPrint.KeyID = eSC.KeyID;
                                    eSubscriberFootPrint.KeyType = eSC.KeyType;
                                    eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                    eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                    if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                    {
                                        eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                    }
                                    eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                    eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                    eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                    eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                    eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                    eSubscriberFootPrint.CreatedByUser = sys.Username;
                                    eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                    eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                    eSubscriberFootPrint.ProductID = eSe.ProductID;
                                    eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                    int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                }
                            }

                            rXml = rp.ResponseData;
                        }
                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.

                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                        }
                    }
                    else
                    {
                        // When User want to Re-Open a report

                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = eSC.XMLData;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }


                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }
        public XDSPortalLibrary.Entity_Layer.Response SubmitCustomVettingP(SqlConnection con, SqlConnection AdminConnection,
           int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, bool bBonusChecking, string strVoucherCode, string[] ApplicantionDetails, string EndpointAddress,
            string smtpServer, string smtpUser, string smtpPassword, string smtpPort, string fromAddress, string toAddress, string ccAddress)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            string rXml = "";
            double Totalcost = 0;

            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML
                // use this XML data for generating the Report

                if (spr.ProductID > 0)
                {
                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {

                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }

                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {
                            if (eSC.KeyType == "H")
                                eoConsumerTraceWseManager.HomeAffairsID = eSC.KeyID;
                            else
                                eoConsumerTraceWseManager.ConsumerID = eSC.KeyID;

                            eoConsumerTraceWseManager.CreatedbyUser = eSe.CreatedByUser;
                            eoConsumerTraceWseManager.GrossMonthlyIncome = 0;
                            eoConsumerTraceWseManager.ReferenceNo = eSC.Reference;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.EnquiryID = intSubscriberEnquiryID;
                            eoConsumerTraceWseManager.EnquiryResultID = intSubscriberEnquiryResultID;
                            eoConsumerTraceWseManager.subscriberID = eSe.SubscriberID;
                            eoConsumerTraceWseManager.ProductID = eSe.ProductID;
                            eoConsumerTraceWseManager.UserName = sys.Username;
                            eoConsumerTraceWseManager.AssociationTypeCode = sub.AssociationTypeCode;
                            eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;
                            eoConsumerTraceWseManager.SFID = spr.ReportID.ToString();

                            if (eoConsumerTraceWseManager.ConsumerID == 0)
                            {
                                SqlConnection objConstring = new SqlConnection(eSe.ExtraVarInput1.ToString());

                                XDSPortalEnquiry.Data.Consumer oConsumer = new XDSPortalEnquiry.Data.Consumer();
                                eoConsumerTraceWseManager.ConsumerID = oConsumer.GetConsumerID(objConstring, 0, eoConsumerTraceWseManager.HomeAffairsID);
                            }

                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.CustomvettingP(eoConsumerTraceWseManager, ApplicantionDetails);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.CustomvettingP(eoConsumerTraceWseManager, ApplicantionDetails);
                            }

                            rXml = rp.ResponseData;


                            System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                throw new Exception(rp.ResponseData);
                            }


                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {

                                ds.ReadXml(xmlSR);
                                Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                eSC.DetailsViewedDate = DateTime.Now;
                                eSC.DetailsViewedYN = true;
                                if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    eSC.Billable = false;
                                }
                                else
                                {
                                    eSC.Billable = true;
                                }
                                eSC.ChangedByUser = eSe.CreatedByUser;
                                eSC.ChangedOnDate = DateTime.Now;

                                eSC.KeyID = rp.ResponseKey;
                                eSC.KeyType = rp.ResponseKeyType;
                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                eSC.BillingTypeID = spr.BillingTypeID;
                                eSC.BillingPrice = spr.UnitPrice;


                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                eSC.ProductID = eSe.ProductID;


                                DateTime Applicantdateofbirth = eSC.BirthDate;

                                if (ds.Tables.Count > 0)
                                {
                                    DateTime.TryParse(ds.Tables[0].Rows[0]["BirthDate"].ToString(), out Applicantdateofbirth);
                                }


                                decimal income;
                                Decimal.TryParse(ApplicantionDetails[3].ToString(), out income);


                                string step = string.Empty;


                                eSC.ExtraVarOutput1 = ApplicantionDetails[15].ToString();


                                eSC.ExtraVarOutput3 = Applicantdateofbirth.ToShortDateString();

                                DataColumn dcResponse = new DataColumn("CellCResponse", typeof(string));
                                DataColumn dcResponseType = new DataColumn("CellCResponseType", typeof(string));
                                DataColumn dcEmail = new DataColumn("EmailStatus", typeof(string));



                                //try
                                //{




                                //    XDSPortalLibrary.Entity_Layer.Response rpVodacom = new XDSPortalLibrary.Entity_Layer.Response();

                                //    string Portfolio = string.Empty, Score = string.Empty,
                                //       Decision = string.Empty, Region = string.Empty,
                                //       AccountNo = string.Empty, SourceId = string.Empty, ReasonCode = string.Empty;
                                //    ;

                                //    if (ds.Tables.Count > 0)
                                //    {
                                //        Portfolio = ds.Tables[0].Rows[0]["Portfolio"].ToString();
                                //        Score = ds.Tables[0].Rows[0]["Score"].ToString();
                                //        Decision = ds.Tables[0].Rows[0]["ApplicationStatus"].ToString();

                                //        Region = ds.Tables[0].Rows[0]["Region"].ToString();
                                //        AccountNo = ds.Tables[0].Rows[0]["AccountNo"].ToString();
                                //        SourceId = ds.Tables[0].Rows[0]["SourceInd"].ToString();
                                //        ReasonCode = ds.Tables[0].Rows[0]["ReasonCode"].ToString();


                                //        ds.Tables[0].Columns.Add(dcResponse);
                                //        ds.Tables[0].Columns.Add(dcResponseType);
                                //        ds.Tables[0].Columns.Add(dcEmail);
                                //    }


                                //    rpVodacom = submitTransactionToVodacom(ApplicantionDetails[13].ToString(), eSC.Reference, DateTime.Now, eSC.Reference, eSC.Reference,
                                //        eSC.SubscriberEnquiryID.ToString(), ApplicantionDetails[8].ToString(), ApplicantionDetails[15].ToString(),
                                //        ApplicantionDetails[10].ToString(), ApplicantionDetails[0].ToString(),
                                //        ApplicantionDetails[1].ToString(), ApplicantionDetails[2].ToString(), Applicantdateofbirth, income, ApplicantionDetails[4].ToString(),
                                //        ApplicantionDetails[5].ToString(), ApplicantionDetails[6].ToString(), ApplicantionDetails[13].ToString(), DateTime.Now, "XDS", Portfolio, Score, Decision, EndpointAddress,
                                //        smtpServer, smtpUser, smtpPassword, smtpPort, true, toAddress, fromAddress, ccAddress, "XDS UAT", Region, AccountNo, SourceId, ReasonCode);


                                //    eSC.ExtraVarOutput2 = rpVodacom.ResponseData;


                                //    ds.Tables[0].Rows[0]["CellCResponse"] = rpVodacom.ResponseData.ToString();
                                //    ds.Tables[0].Rows[0]["CellCResponseType"] = rpVodacom.ResponseStatus.ToString();
                                //    ds.Tables[0].Rows[0]["EmailStatus"] = rpVodacom.TmpReference.ToString();

                                //}
                                //catch (Exception ex)
                                //{
                                //    eSe.ErrorDescription = step + ex.Message + ex.StackTrace;
                                //    //throw new Exception(step + ex.Message);

                                //    ds.Tables[0].Rows[0]["CellCResponse"] = ex.Message;
                                //    ds.Tables[0].Rows[0]["CellCResponseType"] = "Error";
                                //}

                                rXml = ds.GetXml();
                                rp.ResponseData = rXml;
                                eSC.XMLData = rXml.Replace("'", "''");

                                dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                rp.EnquiryID = eSC.SubscriberEnquiryID;

                                // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                if (sub.PayAsYouGo == 1)
                                {
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                }
                                if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                }

                                if (String.IsNullOrEmpty(eSC.VoucherCode))
                                {
                                    //Log FootPrint

                                    eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                    eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                    eSubscriberFootPrint.KeyID = eSC.KeyID;
                                    eSubscriberFootPrint.KeyType = eSC.KeyType;
                                    eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                    eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                    if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                    {
                                        eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                    }
                                    eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                    eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                    eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                    eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                    eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                    eSubscriberFootPrint.CreatedByUser = sys.Username;
                                    eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                    eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                    eSubscriberFootPrint.ProductID = eSe.ProductID;
                                    eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                    int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                }
                            }

                            rXml = rp.ResponseData;
                        }
                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.

                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                        }
                    }
                    else
                    {
                        // When User want to Re-Open a report

                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = eSC.XMLData;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }


                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitXDSCustomVetting(SqlConnection con, SqlConnection AdminConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID,
            bool bBonusChecking, string strVoucherCode)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            string rXml = "";
            double Totalcost = 0;

            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML
                // use this XML data for generating the Report

                if (spr.ProductID > 0)
                {
                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {

                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }

                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {
                            if (eSC.KeyType.ToUpper().Trim() == "C")
                            {
                                eoConsumerTraceWseManager.ConsumerID = eSC.KeyID;
                            }
                            else if (eSC.KeyType.ToUpper().Trim() == "H")
                            {
                                eoConsumerTraceWseManager.HomeAffairsID = eSC.KeyID;
                            }
                            eoConsumerTraceWseManager.CreatedbyUser = eSe.CreatedByUser;
                            eoConsumerTraceWseManager.GrossMonthlyIncome = 0;
                            eoConsumerTraceWseManager.ReferenceNo = eSC.Reference;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.EnquiryID = intSubscriberEnquiryID;
                            eoConsumerTraceWseManager.EnquiryResultID = intSubscriberEnquiryResultID;
                            eoConsumerTraceWseManager.subscriberID = eSe.SubscriberID;
                            eoConsumerTraceWseManager.ProductID = eSe.ProductID;
                            eoConsumerTraceWseManager.UserName = sys.Username;
                            eoConsumerTraceWseManager.AssociationTypeCode = sub.AssociationTypeCode;
                            eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;
                            eoConsumerTraceWseManager.SFID = spr.ReportID.ToString();
                            eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                            eoConsumerTraceWseManager.IDno = eSe.IDNo;
                            eoConsumerTraceWseManager.Passportno = eSe.PassportNo;
                            eoConsumerTraceWseManager.Gender = eSe.Gender;
                            eoConsumerTraceWseManager.DOB = eSe.BirthDate;
                            eoConsumerTraceWseManager.MaidenName = eSe.MaidenName;



                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);

                            if (spr.ProductID == 141 || spr.ProductID == 142 || spr.ProductID == 143 || spr.ProductID == 144 || spr.ProductID == 145 || spr.ProductID == 146 || spr.ProductID == 203 || spr.ProductID == 204 || spr.ProductID == 205 || spr.ProductID == 206 || spr.ProductID == 207)
                            {

                                if (spr.ProductID == 146)
                                {
                                    XDSPortalEnquiry.Data.MDPIntegration mDPIntegration = new XDSPortalEnquiry.Data.MDPIntegration();
                                    mDPIntegration.GetMDPIntegrationinfo(AdminConnection, "JD Score Card");
                                }
                                rp = ra.GetConsumerSegmentData(eoConsumerTraceWseManager);
                            }
                            else
                            {
                                rp = ra.XDSCustomvetting(eoConsumerTraceWseManager, spr.ReportID);
                            }

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);

                                if (spr.ProductID == 141 || spr.ProductID == 142 || spr.ProductID == 143 || spr.ProductID == 144 || spr.ProductID == 145 || spr.ProductID == 146)
                                {
                                    rp = ra.GetConsumerSegmentData(eoConsumerTraceWseManager);
                                }
                                else
                                {
                                    rp = ra.XDSCustomvetting(eoConsumerTraceWseManager, spr.ReportID);
                                }
                            }

                            rXml = rp.ResponseData;

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                throw new Exception(rp.ResponseData);
                            }


                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);
                                ds.ReadXml(xmlSR);

                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, eSe.ProductID);

                                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryReason", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();
                                drSubscriberInput["EnquiryReason"] = eSe.EnquiryReason.Trim();

                                dtSubscriberInput.Rows.Add(drSubscriberInput);
                                ds.Tables.Add(dtSubscriberInput);

                                rXml = ds.GetXml();
                                rp.ResponseData = rXml;

                                Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                eSC.DetailsViewedDate = DateTime.Now;
                                eSC.DetailsViewedYN = true;
                                if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    eSC.Billable = false;
                                }
                                else
                                {
                                    eSC.Billable = true;
                                }
                                eSC.ChangedByUser = eSe.CreatedByUser;
                                eSC.ChangedOnDate = DateTime.Now;

                                eSC.KeyID = rp.ResponseKey;
                                eSC.KeyType = rp.ResponseKeyType;
                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                eSC.BillingTypeID = spr.BillingTypeID;
                                eSC.BillingPrice = spr.UnitPrice;

                                eSC.XMLData = rXml.Replace("'", "''");
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                eSC.ProductID = eSe.ProductID;

                                dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                rp.EnquiryID = eSC.SubscriberEnquiryID;

                                // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                if (sub.PayAsYouGo == 1)
                                {
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                }
                                if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                }

                                if (String.IsNullOrEmpty(eSC.VoucherCode))
                                {
                                    //Log FootPrint

                                    eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                    eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                    eSubscriberFootPrint.KeyID = eSC.KeyID;
                                    eSubscriberFootPrint.KeyType = eSC.KeyType;
                                    eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                    eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                    if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                    {
                                        eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                    }
                                    eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                    eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                    eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                    eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                    eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                    eSubscriberFootPrint.CreatedByUser = sys.Username;
                                    eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                    eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                    eSubscriberFootPrint.ProductID = eSe.ProductID;
                                    eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                    int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                }
                            }

                            rXml = rp.ResponseData;
                        }
                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.

                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                        }
                    }
                    else
                    {
                        // When User want to Re-Open a report

                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = eSC.XMLData;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }


                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitXDSGenericCustomVetting(SqlConnection con, SqlConnection AdminConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID,
             bool bBonusChecking, string strVoucherCode)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            string rXml = "";
            double Totalcost = 0;

            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML
                // use this XML data for generating the Report

                if (spr.ProductID > 0)
                {
                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {

                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }

                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {
                            eoConsumerTraceWseManager.ConsumerID = eSC.KeyID;
                            eoConsumerTraceWseManager.CreatedbyUser = eSe.CreatedByUser;
                            eoConsumerTraceWseManager.GrossMonthlyIncome = 0;
                            eoConsumerTraceWseManager.ReferenceNo = eSC.Reference;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.EnquiryID = intSubscriberEnquiryID;
                            eoConsumerTraceWseManager.EnquiryResultID = intSubscriberEnquiryResultID;
                            eoConsumerTraceWseManager.subscriberID = eSe.SubscriberID;
                            eoConsumerTraceWseManager.ProductID = eSe.ProductID;
                            eoConsumerTraceWseManager.UserName = sys.Username;
                            eoConsumerTraceWseManager.AssociationTypeCode = sub.AssociationTypeCode;
                            eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;
                            eoConsumerTraceWseManager.SFID = spr.ReportID.ToString();
                            eoConsumerTraceWseManager.IDno = eSC.IDNo;

                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.XDSGeneriCustomvetting(eoConsumerTraceWseManager, spr.ReportID);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.XDSGeneriCustomvetting(eoConsumerTraceWseManager, spr.ReportID);
                            }

                            rXml = rp.ResponseData;

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                throw new Exception(rp.ResponseData);
                            }


                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                eSC.DetailsViewedDate = DateTime.Now;
                                eSC.DetailsViewedYN = true;
                                if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    eSC.Billable = false;
                                }
                                else
                                {
                                    eSC.Billable = true;
                                }
                                eSC.ChangedByUser = eSe.CreatedByUser;
                                eSC.ChangedOnDate = DateTime.Now;

                                eSC.KeyID = rp.ResponseKey;
                                eSC.KeyType = rp.ResponseKeyType;
                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                eSC.BillingTypeID = spr.BillingTypeID;
                                eSC.BillingPrice = spr.UnitPrice;

                                eSC.XMLData = rXml.Replace("'", "''");
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                eSC.ProductID = eSe.ProductID;

                                dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                rp.EnquiryID = eSC.SubscriberEnquiryID;

                                // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                if (sub.PayAsYouGo == 1)
                                {
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                }
                                if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                }

                                if (String.IsNullOrEmpty(eSC.VoucherCode))
                                {
                                    //Log FootPrint

                                    eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                    eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                    eSubscriberFootPrint.KeyID = eSC.KeyID;
                                    eSubscriberFootPrint.KeyType = eSC.KeyType;
                                    eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                    eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                    if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                    {
                                        eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                    }
                                    eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                    eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                    eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                    eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                    eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                    eSubscriberFootPrint.CreatedByUser = sys.Username;
                                    eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                    eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                    eSubscriberFootPrint.ProductID = eSe.ProductID;
                                    eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                    int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                }
                            }

                            rXml = rp.ResponseData;
                        }
                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.

                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                        }
                    }
                    else
                    {
                        // When User want to Re-Open a report

                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = eSC.XMLData;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }


                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitXDSCustomVettingY(SqlConnection con, SqlConnection AdminConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID,
           bool bBonusChecking, string strVoucherCode, XDSPortalLibrary.Entity_Layer.YRESULT yIRESULT, out XDSPortalLibrary.Entity_Layer.YRESULT yRESULT)
        {
            yRESULT = yIRESULT;
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            string rXml = "";
            double Totalcost = 0;

            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML
                // use this XML data for generating the Report

                if (spr.ProductID > 0)
                {
                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {

                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }

                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {
                            if (eSC.KeyType.ToUpper().Trim() == "C")
                            {
                                eoConsumerTraceWseManager.ConsumerID = eSC.KeyID;
                            }
                            else if (eSC.KeyType.ToUpper().Trim() == "H")
                            {
                                eoConsumerTraceWseManager.HomeAffairsID = eSC.KeyID;
                            }
                            eoConsumerTraceWseManager.CreatedbyUser = eSe.CreatedByUser;
                            eoConsumerTraceWseManager.GrossMonthlyIncome = 0;
                            eoConsumerTraceWseManager.ReferenceNo = eSC.Reference;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.EnquiryID = intSubscriberEnquiryID;
                            eoConsumerTraceWseManager.EnquiryResultID = intSubscriberEnquiryResultID;
                            eoConsumerTraceWseManager.subscriberID = eSe.SubscriberID;
                            eoConsumerTraceWseManager.ProductID = eSe.ProductID;
                            eoConsumerTraceWseManager.UserName = sys.Username;
                            eoConsumerTraceWseManager.AssociationTypeCode = sub.AssociationTypeCode;
                            eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;
                            eoConsumerTraceWseManager.SFID = spr.ReportID.ToString();
                            eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                            eoConsumerTraceWseManager.IDno = eSe.IDNo;
                            eoConsumerTraceWseManager.Passportno = eSe.PassportNo;
                            eoConsumerTraceWseManager.Gender = eSe.Gender;
                            eoConsumerTraceWseManager.DOB = eSe.BirthDate;
                            eoConsumerTraceWseManager.MaidenName = eSe.MaidenName;



                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);

                            rp = ra.GetCustomvettingY(eoConsumerTraceWseManager, spr.ReportID, yIRESULT, out yRESULT);


                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);

                                rp = ra.GetCustomvettingY(eoConsumerTraceWseManager, spr.ReportID, yIRESULT, out yRESULT);
                            }

                            rXml = rp.ResponseData;

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                throw new Exception(rp.ResponseData);
                            }


                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);
                                ds.ReadXml(xmlSR);

                                rXml = ds.GetXml();
                                rp.ResponseData = rXml;

                                Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                eSC.DetailsViewedDate = DateTime.Now;
                                eSC.DetailsViewedYN = true;
                                if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    eSC.Billable = false;
                                }
                                else
                                {
                                    eSC.Billable = true;
                                }
                                eSC.ChangedByUser = eSe.CreatedByUser;
                                eSC.ChangedOnDate = DateTime.Now;

                                eSC.KeyID = rp.ResponseKey;
                                eSC.KeyType = rp.ResponseKeyType;
                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                eSC.BillingTypeID = spr.BillingTypeID;
                                eSC.BillingPrice = spr.UnitPrice;

                                eSC.XMLData = rXml.Replace("'", "''");
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                eSC.ProductID = eSe.ProductID;

                                dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                rp.EnquiryID = eSC.SubscriberEnquiryID;

                                // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                if (sub.PayAsYouGo == 1)
                                {
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                }
                                if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                }

                                if (String.IsNullOrEmpty(eSC.VoucherCode))
                                {
                                    //Log FootPrint

                                    eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                    eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                    eSubscriberFootPrint.KeyID = eSC.KeyID;
                                    eSubscriberFootPrint.KeyType = eSC.KeyType;
                                    eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                    eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                    if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                    {
                                        eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                    }
                                    eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                    eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                    eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                    eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                    eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                    eSubscriberFootPrint.CreatedByUser = sys.Username;
                                    eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                    eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                    eSubscriberFootPrint.ProductID = eSe.ProductID;
                                    eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                    int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                }
                            }

                            rXml = rp.ResponseData;
                        }
                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.

                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                            yRESULT.Status = "F";
                            yRESULT.ErrorDescription = rp.ResponseData;

                        }
                    }
                    else
                    {
                        // When User want to Re-Open a report

                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = eSC.XMLData;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";

                    yRESULT.Status = "F";
                    yRESULT.ErrorDescription = rp.ResponseData;
                }


                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                yRESULT.Status = "F";
                yRESULT.ErrorDescription = "Error While Processing your request, Please Contact XDS";

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitXDSCustomVettingZ(SqlConnection con, SqlConnection AdminConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID,
   bool bBonusChecking, string strVoucherCode, XDSPortalLibrary.Entity_Layer.ZRESULT zIRESULT, out XDSPortalLibrary.Entity_Layer.ZRESULT zRESULT, string strFMPLoginURL, string strFMPMatchURL, string strFMPUserName, string strFMPPassword)
        {
            zRESULT = zIRESULT;
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            string rXml = "";
            double Totalcost = 0;

            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML
                // use this XML data for generating the Report

                if (spr.ProductID > 0)
                {
                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {

                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (strValidationStatus != "1")
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }

                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {
                            if (eSC.KeyType.ToUpper().Trim() == "C")
                            {
                                eoConsumerTraceWseManager.ConsumerID = eSC.KeyID;
                            }
                            else if (eSC.KeyType.ToUpper().Trim() == "H")
                            {
                                eoConsumerTraceWseManager.HomeAffairsID = eSC.KeyID;
                            }
                            eoConsumerTraceWseManager.CreatedbyUser = eSe.CreatedByUser;
                            eoConsumerTraceWseManager.GrossMonthlyIncome = 0;
                            eoConsumerTraceWseManager.ReferenceNo = eSC.Reference;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.EnquiryID = intSubscriberEnquiryID;
                            eoConsumerTraceWseManager.EnquiryResultID = intSubscriberEnquiryResultID;
                            eoConsumerTraceWseManager.subscriberID = eSe.SubscriberID;
                            eoConsumerTraceWseManager.ProductID = eSe.ProductID;
                            eoConsumerTraceWseManager.UserName = sys.Username;
                            eoConsumerTraceWseManager.AssociationTypeCode = sub.AssociationTypeCode;
                            eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;
                            eoConsumerTraceWseManager.SFID = spr.ReportID.ToString();
                            eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                            eoConsumerTraceWseManager.IDno = eSe.IDNo;
                            eoConsumerTraceWseManager.Passportno = eSe.PassportNo;
                            eoConsumerTraceWseManager.Gender = eSe.Gender;
                            eoConsumerTraceWseManager.DOB = eSe.BirthDate;
                            eoConsumerTraceWseManager.MaidenName = eSe.MaidenName;
                            eoConsumerTraceWseManager.AddressLine1 = strFMPLoginURL;
                            eoConsumerTraceWseManager.AddressLine2 = strFMPMatchURL;
                            eoConsumerTraceWseManager.PostalAddressLine1 = strFMPLoginURL;
                            eoConsumerTraceWseManager.PostalAddressLine2 = strFMPPassword;

                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);

                            rp = ra.GetCustomvettingZ(eoConsumerTraceWseManager, spr.ReportID, zIRESULT, out zRESULT);


                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);

                                rp = ra.GetCustomvettingZ(eoConsumerTraceWseManager, spr.ReportID, zIRESULT, out zRESULT);
                            }

                            rXml = rp.ResponseData;

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                throw new Exception(rp.ResponseData);
                            }


                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);
                                ds.ReadXml(xmlSR);

                                rXml = ds.GetXml();
                                rp.ResponseData = rXml;

                                Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                eSC.DetailsViewedDate = DateTime.Now;
                                eSC.DetailsViewedYN = true;
                                if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    eSC.Billable = false;
                                }
                                else
                                {
                                    eSC.Billable = true;
                                }
                                eSC.ChangedByUser = eSe.CreatedByUser;
                                eSC.ChangedOnDate = DateTime.Now;

                                eSC.KeyID = rp.ResponseKey;
                                eSC.KeyType = rp.ResponseKeyType;
                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                eSC.BillingTypeID = spr.BillingTypeID;
                                eSC.BillingPrice = spr.UnitPrice;

                                eSC.XMLData = rXml.Replace("'", "''");
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                eSC.ProductID = eSe.ProductID;

                                dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                rp.EnquiryID = eSC.SubscriberEnquiryID;

                                // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                if (sub.PayAsYouGo == 1)
                                {
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                }
                                if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                }

                                if (String.IsNullOrEmpty(eSC.VoucherCode))
                                {
                                    //Log FootPrint

                                    eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                    eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                    eSubscriberFootPrint.KeyID = eSC.KeyID;
                                    eSubscriberFootPrint.KeyType = eSC.KeyType;
                                    eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                    eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                    if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                    {
                                        eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                    }
                                    eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                    eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                    eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                    eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                    eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                    eSubscriberFootPrint.CreatedByUser = sys.Username;
                                    eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                    eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                    eSubscriberFootPrint.ProductID = eSe.ProductID;
                                    eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                    int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                }
                            }

                            rXml = rp.ResponseData;
                        }
                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.

                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                            zRESULT.Status = "F";
                            zRESULT.ErrorDescription = rp.ResponseData;

                        }
                    }
                    else
                    {
                        // When User want to Re-Open a report

                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = eSC.XMLData;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";

                    zRESULT.Status = "F";
                    zRESULT.ErrorDescription = rp.ResponseData;
                }


                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                zRESULT.Status = "F";
                zRESULT.ErrorDescription = "Error While Processing your request, Please Contact XDS";

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }
        public XDSPortalLibrary.Entity_Layer.Response ValidateConnectCustomVettingO(XDSPortalLibrary.Entity_Layer.Submit objInput)
        {
            int i = 0;
            float f = 0;
            XDSPortalLibrary.Entity_Layer.Response oResponse = new XDSPortalLibrary.Entity_Layer.Response();

            if (string.IsNullOrEmpty(objInput.Items.User))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Username is Mandatory";
            }
            else if (string.IsNullOrEmpty(objInput.Items.Password))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Password is Mandatory";
            }
            else if (string.IsNullOrEmpty(objInput.Items.Password))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Password is Mandatory";
            }
            else if (string.IsNullOrEmpty(objInput.Items.IDType))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "IDType is Mandatory";
            }
            else if (!string.IsNullOrEmpty(objInput.Items.IDType) && objInput.Items.IDType.ToUpper() != XDSPortalLibrary.Entity_Layer.JDFSPrincipaRequest.IDType.PASSPORT.ToString() && objInput.Items.IDType.ToUpper() != XDSPortalLibrary.Entity_Layer.JDFSPrincipaRequest.IDType.SAID.ToString())
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "IDType can be either SAID or PASSPORT";
            }
            else if (string.IsNullOrEmpty(objInput.Items.IDNumber))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "IDNumber is Mandatory";
            }
            else if (!string.IsNullOrEmpty(objInput.Items.IDNumber) && objInput.Items.IDType.ToUpper() == XDSPortalLibrary.Entity_Layer.JDFSPrincipaRequest.IDType.SAID.ToString() && objInput.Items.IDNumber.Length < 13)
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "IDNumber supplied is Invalid";
            }
            else if (string.IsNullOrEmpty(objInput.Items.Surname))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Surname is Mandatory";
            }
            else if (!int.TryParse(objInput.Items.NumberOfDependants, out i) ||
                    !int.TryParse(objInput.Items.PhysicalPostalCode, out i) ||
                !int.TryParse(objInput.Items.NumberOfYearsAtEmployer, out i) ||
                !int.TryParse(objInput.Items.CustomerAge, out i) ||
                !int.TryParse(objInput.Items.NumberOfMonthsAtEmployer, out i))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Invalid value supplied for either of NumberOfDependants, PhysicalPostalCode, NumberOfYearsAtEmployer, CustomerAge, NumberOfMonthsAtEmployer";
            }
            else if (!float.TryParse(objInput.Items.Income, out f) ||
                    !float.TryParse(objInput.Items.GrossIncome, out f))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Invalid value supplied for either of Income, GrossIncome";
            }

            return oResponse;
        }


        public XDSPortalLibrary.Entity_Layer.Response ValidateConnectCustomVettingQ(XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplication objInput)
        {
            int i = 0;
            float f = 0;
            bool b = false;
            XDSPortalLibrary.Entity_Layer.Response oResponse = new XDSPortalLibrary.Entity_Layer.Response();

            if (string.IsNullOrEmpty(objInput.input.User))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Username is Mandatory";
            }
            else if (string.IsNullOrEmpty(objInput.input.Password))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Password is Mandatory";
            }
            else if (string.IsNullOrEmpty(objInput.input.Password))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Password is Mandatory";
            }
            //else if (string.IsNullOrEmpty(objInput.input.IsExistingClient))
            //{
            //    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            //    oResponse.ResponseData = "IsExistingClient is Mandatory, Valid values are true/false";
            //}
            else if (!string.IsNullOrEmpty(objInput.input.IsExistingClient) && objInput.input.IsExistingClient.ToUpper() != "F" && objInput.input.IsExistingClient.ToUpper() != "T")
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "IsExistingClient is Mandatory, Valid values are F/T";
            }
            else if (string.IsNullOrEmpty(objInput.input.IDType))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "IDType is Mandatory";
            }
            else if (!string.IsNullOrEmpty(objInput.input.IDType) && objInput.input.IDType.ToUpper() != "1" && objInput.input.IDType.ToUpper() != "2")
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "IDType can be either 1 for SAID or 2 for PASSPORT";
            }
            else if (string.IsNullOrEmpty(objInput.input.IdentityNumber))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "IDNumber is Mandatory";
            }
            else if (!string.IsNullOrEmpty(objInput.input.IdentityNumber) && objInput.input.IDType.ToUpper() == XDSPortalLibrary.Entity_Layer.JDFSPrincipaRequest.IDType.SAID.ToString() && objInput.input.IdentityNumber.Length < 13)
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "IDNumber supplied is Invalid";
            }
            else if (string.IsNullOrEmpty(objInput.input.Surname))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Surname is Mandatory";
            }
            else if (string.IsNullOrEmpty(objInput.input.FirstName))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "FirstName is Mandatory";
            }
            else if (string.IsNullOrEmpty(objInput.input.DealerCode))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "DealerCode is Mandatory";
            }
            else if (string.IsNullOrEmpty(objInput.input.CustomerName))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "CustomerName is Mandatory";
            }
            //else if (string.IsNullOrEmpty(objInput.input.Gender))
            //{
            //    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            //    oResponse.ResponseData = "Gender is Mandatory,Valid values are M or F";
            //}
            else if (!string.IsNullOrEmpty(objInput.input.Gender) && (objInput.input.Gender.ToUpper() != "M" && objInput.input.Gender.ToUpper() != "F"))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Valid values for Gender are M or F";
            }
            //else if (string.IsNullOrEmpty(objInput.input.AddressLine1) || string.IsNullOrEmpty(objInput.input.AddressLine2) || string.IsNullOrEmpty(objInput.input.Suburb) || string.IsNullOrEmpty(objInput.input.City)|| string.IsNullOrEmpty(objInput.input.PostalCode))
            //{
            //    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            //    oResponse.ResponseData = "AddressLine1,AddressLine2,Suburb,City and PostalCode are Mandatory";
            //}
            //else if (string.IsNullOrEmpty(objInput.input.HighestQualification))
            //{
            //    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            //    oResponse.ResponseData = "HighestQualification is Mandatory";
            //}
            //else if (!int.TryParse(objInput.input.PostalCode, out i) ||
            //    !int.TryParse(objInput.input.NumberOfYearsAtEmployer, out i) )
            //{
            //    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            //    oResponse.ResponseData = "Invalid value supplied for either of  PostalCode, NumberOfYearsAtEmployer";
            //}
            else if (!float.TryParse(objInput.input.GrossMonthlyIncome, out f))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Invalid value supplied for GrossMonthlyIncome";
            }
            else if (float.TryParse(objInput.input.GrossMonthlyIncome, out f))
            {
                if (f <= 0)
                {
                    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    oResponse.ResponseData = "Invalid value supplied for GrossMonthlyIncome";
                }
            }

            return oResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response ValidateConnectCustomVettingQTeleSales(XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplication objInput)
        {
            int i = 0;
            float f = 0;
            bool b = false;
            XDSPortalLibrary.Entity_Layer.Response oResponse = new XDSPortalLibrary.Entity_Layer.Response();

            if (string.IsNullOrEmpty(objInput.input.User))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Username is Mandatory";
            }
            else if (string.IsNullOrEmpty(objInput.input.Password))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Password is Mandatory";
            }
            else if (string.IsNullOrEmpty(objInput.input.Password))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Password is Mandatory";
            }
            //else if (string.IsNullOrEmpty(objInput.input.IsExistingClient))
            //{
            //    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            //    oResponse.ResponseData = "IsExistingClient is Mandatory, Valid values are true/false";
            //}
            else if (!string.IsNullOrEmpty(objInput.input.IsExistingClient) && objInput.input.IsExistingClient.ToUpper() != "F" && objInput.input.IsExistingClient.ToUpper() != "T")
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "IsExistingClient is Mandatory, Valid values are F/T";
            }
            else if (string.IsNullOrEmpty(objInput.input.IDType))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "IDType is Mandatory";
            }
            else if (!string.IsNullOrEmpty(objInput.input.IDType) && objInput.input.IDType.ToUpper() != "1" && objInput.input.IDType.ToUpper() != "2")
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "IDType can be either 1 for SAID or 2 for PASSPORT";
            }
            else if (string.IsNullOrEmpty(objInput.input.IdentityNumber))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "IDNumber is Mandatory";
            }
            else if (!string.IsNullOrEmpty(objInput.input.IdentityNumber) && objInput.input.IDType.ToUpper() == XDSPortalLibrary.Entity_Layer.JDFSPrincipaRequest.IDType.SAID.ToString() && objInput.input.IdentityNumber.Length < 13)
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "IDNumber supplied is Invalid";
            }
            else if (string.IsNullOrEmpty(objInput.input.Surname))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Surname is Mandatory";
            }
            else if (string.IsNullOrEmpty(objInput.input.FirstName))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "FirstName is Mandatory";
            }
            else if (string.IsNullOrEmpty(objInput.input.DealerCode))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "DealerCode is Mandatory";
            }
            else if (string.IsNullOrEmpty(objInput.input.CustomerName))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "CustomerName is Mandatory";
            }
            else if (string.IsNullOrEmpty(objInput.input.Channel))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Channel is Mandatory";
            }
            //else if (string.IsNullOrEmpty(objInput.input.Gender))
            //{
            //    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            //    oResponse.ResponseData = "Gender is Mandatory,Valid values are M or F";
            //}
            else if (!string.IsNullOrEmpty(objInput.input.Gender) && (objInput.input.Gender.ToUpper() != "M" && objInput.input.Gender.ToUpper() != "F"))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Valid values for Gender are M or F";
            }
            //else if (string.IsNullOrEmpty(objInput.input.AddressLine1) || string.IsNullOrEmpty(objInput.input.AddressLine2) || string.IsNullOrEmpty(objInput.input.Suburb) || string.IsNullOrEmpty(objInput.input.City)|| string.IsNullOrEmpty(objInput.input.PostalCode))
            //{
            //    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            //    oResponse.ResponseData = "AddressLine1,AddressLine2,Suburb,City and PostalCode are Mandatory";
            //}
            //else if (string.IsNullOrEmpty(objInput.input.HighestQualification))
            //{
            //    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            //    oResponse.ResponseData = "HighestQualification is Mandatory";
            //}
            //else if (!int.TryParse(objInput.input.PostalCode, out i) ||
            //    !int.TryParse(objInput.input.NumberOfYearsAtEmployer, out i) )
            //{
            //    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            //    oResponse.ResponseData = "Invalid value supplied for either of  PostalCode, NumberOfYearsAtEmployer";
            //}
            else if (!float.TryParse(objInput.input.GrossMonthlyIncome, out f))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Invalid value supplied for GrossMonthlyIncome";
            }
            else if (float.TryParse(objInput.input.GrossMonthlyIncome, out f))
            {
                if (f <= 0)
                {
                    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    oResponse.ResponseData = "Invalid value supplied for GrossMonthlyIncome";
                }
            }

            return oResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response ValidateConnectCustomVettingQA(XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplication objInput)
        {
            int i = 0;
            float f = 0;
            bool b = false;
            XDSPortalLibrary.Entity_Layer.Response oResponse = new XDSPortalLibrary.Entity_Layer.Response();

            if (string.IsNullOrEmpty(objInput.input.User))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Username is Mandatory";
            }
            else if (string.IsNullOrEmpty(objInput.input.Password))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Password is Mandatory";
            }
            else if (string.IsNullOrEmpty(objInput.input.Password))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Password is Mandatory";
            }
            //else if (string.IsNullOrEmpty(objInput.input.IsExistingClient))
            //{
            //    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            //    oResponse.ResponseData = "IsExistingClient is Mandatory, Valid values are true/false";
            //}
            else if (!string.IsNullOrEmpty(objInput.input.IsExistingClient) && objInput.input.IsExistingClient.ToUpper() != "F" && objInput.input.IsExistingClient.ToUpper() != "T")
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "IsExistingClient is Mandatory, Valid values are F/T";
            }
            else if (string.IsNullOrEmpty(objInput.input.IDType))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "IDType is Mandatory";
            }
            else if (!string.IsNullOrEmpty(objInput.input.IDType) && objInput.input.IDType.ToUpper() != "1" && objInput.input.IDType.ToUpper() != "2")
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "IDType can be either 1 for SAID or 2 for PASSPORT";
            }
            else if (string.IsNullOrEmpty(objInput.input.IdentityNumber))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "IDNumber is Mandatory";
            }
            else if (!string.IsNullOrEmpty(objInput.input.IdentityNumber) && objInput.input.IDType.ToUpper() == XDSPortalLibrary.Entity_Layer.JDFSPrincipaRequest.IDType.SAID.ToString() && objInput.input.IdentityNumber.Length < 13)
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "IDNumber supplied is Invalid";
            }
            else if (string.IsNullOrEmpty(objInput.input.Surname))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Surname is Mandatory";
            }
            else if (string.IsNullOrEmpty(objInput.input.FirstName))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "FirstName is Mandatory";
            }
            //else if (string.IsNullOrEmpty(objInput.input.DealerCode))
            //{
            //    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            //    oResponse.ResponseData = "DealerCode is Mandatory";
            //}
            //else if (string.IsNullOrEmpty(objInput.input.CustomerName))
            //{
            //    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            //    oResponse.ResponseData = "CustomerName is Mandatory";
            //}
            else if (string.IsNullOrEmpty(objInput.input.Gender))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Gender is Mandatory,Valid values are M or F";
            }
            else if (!string.IsNullOrEmpty(objInput.input.Gender) && (objInput.input.Gender.ToUpper() != "M" && objInput.input.Gender.ToUpper() != "F"))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Valid values for Gender are M or F";
            }
            //else if (string.IsNullOrEmpty(objInput.input.AddressLine1) || string.IsNullOrEmpty(objInput.input.AddressLine2) || string.IsNullOrEmpty(objInput.input.Suburb) || string.IsNullOrEmpty(objInput.input.City)|| string.IsNullOrEmpty(objInput.input.PostalCode))
            //{
            //    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            //    oResponse.ResponseData = "AddressLine1,AddressLine2,Suburb,City and PostalCode are Mandatory";
            //}
            //else if (string.IsNullOrEmpty(objInput.input.HighestQualification))
            //{
            //    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            //    oResponse.ResponseData = "HighestQualification is Mandatory";
            //}
            //else if (!int.TryParse(objInput.input.PostalCode, out i) ||
            //    !int.TryParse(objInput.input.NumberOfYearsAtEmployer, out i) )
            //{
            //    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            //    oResponse.ResponseData = "Invalid value supplied for either of  PostalCode, NumberOfYearsAtEmployer";
            //}
            else if (!float.TryParse(objInput.input.GrossMonthlyIncome, out f))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Invalid value supplied for GrossMonthlyIncome";
            }
            else if (float.TryParse(objInput.input.GrossMonthlyIncome, out f))
            {
                if (f <= 0)
                {
                    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    oResponse.ResponseData = "Invalid value supplied for GrossMonthlyIncome";
                }
            }

            return oResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response ValidateConnectCustomVettingQATeleSales(XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplication objInput)
        {
            int i = 0;
            float f = 0;
            bool b = false;
            XDSPortalLibrary.Entity_Layer.Response oResponse = new XDSPortalLibrary.Entity_Layer.Response();

            if (string.IsNullOrEmpty(objInput.input.User))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Username is Mandatory";
            }
            else if (string.IsNullOrEmpty(objInput.input.Password))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Password is Mandatory";
            }
            else if (string.IsNullOrEmpty(objInput.input.Password))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Password is Mandatory";
            }
            //else if (string.IsNullOrEmpty(objInput.input.IsExistingClient))
            //{
            //    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            //    oResponse.ResponseData = "IsExistingClient is Mandatory, Valid values are true/false";
            //}
            else if (!string.IsNullOrEmpty(objInput.input.IsExistingClient) && objInput.input.IsExistingClient.ToUpper() != "F" && objInput.input.IsExistingClient.ToUpper() != "T")
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "IsExistingClient is Mandatory, Valid values are F/T";
            }
            else if (string.IsNullOrEmpty(objInput.input.IDType))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "IDType is Mandatory";
            }
            else if (!string.IsNullOrEmpty(objInput.input.IDType) && objInput.input.IDType.ToUpper() != "1" && objInput.input.IDType.ToUpper() != "2")
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "IDType can be either 1 for SAID or 2 for PASSPORT";
            }
            else if (string.IsNullOrEmpty(objInput.input.IdentityNumber))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "IDNumber is Mandatory";
            }
            else if (!string.IsNullOrEmpty(objInput.input.IdentityNumber) && objInput.input.IDType.ToUpper() == XDSPortalLibrary.Entity_Layer.JDFSPrincipaRequest.IDType.SAID.ToString() && objInput.input.IdentityNumber.Length < 13)
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "IDNumber supplied is Invalid";
            }
            else if (string.IsNullOrEmpty(objInput.input.Surname))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Surname is Mandatory";
            }
            else if (string.IsNullOrEmpty(objInput.input.FirstName))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "FirstName is Mandatory";
            }
            else if (string.IsNullOrEmpty(objInput.input.DealerCode))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "DealerCode is Mandatory";
            }
            else if (string.IsNullOrEmpty(objInput.input.CustomerName))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "CustomerName is Mandatory";
            }
            //else if (string.IsNullOrEmpty(objInput.input.Channel))
            //{
            //    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            //    oResponse.ResponseData = "Channel is Mandatory";
            //}
            else if (string.IsNullOrEmpty(objInput.input.Gender))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Gender is Mandatory,Valid values are M or F";
            }
            else if (!string.IsNullOrEmpty(objInput.input.Gender) && (objInput.input.Gender.ToUpper() != "M" && objInput.input.Gender.ToUpper() != "F"))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Valid values for Gender are M or F";
            }
            //else if (string.IsNullOrEmpty(objInput.input.AddressLine1) || string.IsNullOrEmpty(objInput.input.AddressLine2) || string.IsNullOrEmpty(objInput.input.Suburb) || string.IsNullOrEmpty(objInput.input.City)|| string.IsNullOrEmpty(objInput.input.PostalCode))
            //{
            //    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            //    oResponse.ResponseData = "AddressLine1,AddressLine2,Suburb,City and PostalCode are Mandatory";
            //}
            //else if (string.IsNullOrEmpty(objInput.input.HighestQualification))
            //{
            //    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            //    oResponse.ResponseData = "HighestQualification is Mandatory";
            //}
            //else if (!int.TryParse(objInput.input.PostalCode, out i) ||
            //    !int.TryParse(objInput.input.NumberOfYearsAtEmployer, out i) )
            //{
            //    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            //    oResponse.ResponseData = "Invalid value supplied for either of  PostalCode, NumberOfYearsAtEmployer";
            //}
            else if (!float.TryParse(objInput.input.GrossMonthlyIncome, out f))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Invalid value supplied for GrossMonthlyIncome";
            }
            else if (float.TryParse(objInput.input.GrossMonthlyIncome, out f))
            {
                if (f <= 0)
                {
                    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    oResponse.ResponseData = "Invalid value supplied for GrossMonthlyIncome";
                }
            }

            return oResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response ValidateConnectCustomVettingQSOA(XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplication objInput)
        {
            int i = 0;
            float f = 0;
            bool b = false;
            XDSPortalLibrary.Entity_Layer.Response oResponse = new XDSPortalLibrary.Entity_Layer.Response();

            if (string.IsNullOrEmpty(objInput.input.User))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Username is Mandatory";
            }
            else if (string.IsNullOrEmpty(objInput.input.Password))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Password is Mandatory";
            }
            else if (string.IsNullOrEmpty(objInput.input.Password))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Password is Mandatory";
            }
            //else if (string.IsNullOrEmpty(objInput.input.IsExistingClient))
            //{
            //    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            //    oResponse.ResponseData = "IsExistingClient is Mandatory, Valid values are true/false";
            //}
            else if (!string.IsNullOrEmpty(objInput.input.IsExistingClient) && !bool.TryParse(objInput.input.IsExistingClient, out b))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "IsExistingClient is Mandatory, Valid values are true/false";
            }
            else if (string.IsNullOrEmpty(objInput.input.IDType))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "IDType is Mandatory";
            }
            else if (!string.IsNullOrEmpty(objInput.input.IDType) && objInput.input.IDType.ToUpper() != "SAID" && objInput.input.IDType.ToUpper() != "PASSPORT")
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "IDType can be either SAID or PASSPORT";
            }
            else if (string.IsNullOrEmpty(objInput.input.IdentityNumber))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "IDNumber is Mandatory";
            }
            else if (!string.IsNullOrEmpty(objInput.input.IdentityNumber) && objInput.input.IDType.ToUpper() == XDSPortalLibrary.Entity_Layer.JDFSPrincipaRequest.IDType.SAID.ToString() && objInput.input.IdentityNumber.Length < 13)
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "IDNumber supplied is Invalid";
            }
            else if (string.IsNullOrEmpty(objInput.input.Surname))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Surname is Mandatory";
            }
            else if (string.IsNullOrEmpty(objInput.input.FirstName))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "FirstName is Mandatory";
            }
            //else if (string.IsNullOrEmpty(objInput.input.Gender))
            //{
            //    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            //    oResponse.ResponseData = "Gender is Mandatory,Valid values are M or F";
            //}
            else if (!string.IsNullOrEmpty(objInput.input.Gender) && (objInput.input.Gender.ToUpper() != "M" && objInput.input.Gender.ToUpper() != "F"))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Valid values for Gender are M or F";
            }
            //else if (string.IsNullOrEmpty(objInput.input.AddressLine1) || string.IsNullOrEmpty(objInput.input.AddressLine2) || string.IsNullOrEmpty(objInput.input.Suburb) || string.IsNullOrEmpty(objInput.input.City)|| string.IsNullOrEmpty(objInput.input.PostalCode))
            //{
            //    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            //    oResponse.ResponseData = "AddressLine1,AddressLine2,Suburb,City and PostalCode are Mandatory";
            //}
            //else if (string.IsNullOrEmpty(objInput.input.HighestQualification))
            //{
            //    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            //    oResponse.ResponseData = "HighestQualification is Mandatory";
            //}
            //else if (!int.TryParse(objInput.input.PostalCode, out i) ||
            //    !int.TryParse(objInput.input.NumberOfYearsAtEmployer, out i) )
            //{
            //    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            //    oResponse.ResponseData = "Invalid value supplied for either of  PostalCode, NumberOfYearsAtEmployer";
            //}
            else if (!float.TryParse(objInput.input.GrossMonthlyIncome, out f))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Invalid value supplied for GrossMonthlyIncome";
            }
            else if (float.TryParse(objInput.input.GrossMonthlyIncome, out f))
            {
                if (f <= 0)
                {
                    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    oResponse.ResponseData = "Invalid value supplied for GrossMonthlyIncome";
                }
            }

            return oResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response ValidateMTNCanVassingInput(XDSPortalLibrary.Entity_Layer.MTNCanvasInput objInput)
        {
            int i = 0;
            float f = 0;
            bool b = false;
            XDSPortalLibrary.Entity_Layer.Response oResponse = new XDSPortalLibrary.Entity_Layer.Response();

            if (string.IsNullOrEmpty(objInput.User))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Username is Mandatory";
            }
            else if (string.IsNullOrEmpty(objInput.Password))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Password is Mandatory";
            }
            else if (string.IsNullOrEmpty(objInput.Password))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Password is Mandatory";
            }
            //else if (string.IsNullOrEmpty(objInput.input.IsExistingClient))
            //{
            //    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            //    oResponse.ResponseData = "IsExistingClient is Mandatory, Valid values are true/false";
            //}
            else if (!string.IsNullOrEmpty(objInput.IsExistingClient) && objInput.IsExistingClient.ToUpper() != "F" && objInput.IsExistingClient.ToUpper() != "T")
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "IsExistingClient is Mandatory, Valid values are F/T";
            }
            else if (string.IsNullOrEmpty(objInput.IDType))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "IDType is Mandatory";
            }
            else if (!string.IsNullOrEmpty(objInput.IDType) && objInput.IDType.ToUpper() != "1" && objInput.IDType.ToUpper() != "2")
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "IDType can be either 1 for SAID or 2 for PASSPORT";
            }
            else if (string.IsNullOrEmpty(objInput.IdentityNumber))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "IDNumber is Mandatory";
            }
            else if (!string.IsNullOrEmpty(objInput.IdentityNumber) && objInput.IDType.ToUpper() == XDSPortalLibrary.Entity_Layer.JDFSPrincipaRequest.IDType.SAID.ToString() && objInput.IdentityNumber.Length < 13)
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "IDNumber supplied is Invalid";
            }
            else if (string.IsNullOrEmpty(objInput.Surname))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Surname is Mandatory";
            }
            else if (string.IsNullOrEmpty(objInput.FirstName))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "FirstName is Mandatory";
            }
            //else if (string.IsNullOrEmpty(objInput.input.DealerCode))
            //{
            //    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            //    oResponse.ResponseData = "DealerCode is Mandatory";
            //}
            //else if (string.IsNullOrEmpty(objInput.input.CustomerName))
            //{
            //    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            //    oResponse.ResponseData = "CustomerName is Mandatory";
            //}
            else if (string.IsNullOrEmpty(objInput.Gender))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Gender is Mandatory,Valid values are M or F";
            }
            else if (!string.IsNullOrEmpty(objInput.Gender) && (objInput.Gender.ToUpper() != "M" && objInput.Gender.ToUpper() != "F"))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Valid values for Gender are M or F";
            }
            //else if (string.IsNullOrEmpty(objInput.input.AddressLine1) || string.IsNullOrEmpty(objInput.input.AddressLine2) || string.IsNullOrEmpty(objInput.input.Suburb) || string.IsNullOrEmpty(objInput.input.City)|| string.IsNullOrEmpty(objInput.input.PostalCode))
            //{
            //    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            //    oResponse.ResponseData = "AddressLine1,AddressLine2,Suburb,City and PostalCode are Mandatory";
            //}
            //else if (string.IsNullOrEmpty(objInput.input.HighestQualification))
            //{
            //    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            //    oResponse.ResponseData = "HighestQualification is Mandatory";
            //}
            //else if (!int.TryParse(objInput.input.PostalCode, out i) ||
            //    !int.TryParse(objInput.input.NumberOfYearsAtEmployer, out i) )
            //{
            //    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            //    oResponse.ResponseData = "Invalid value supplied for either of  PostalCode, NumberOfYearsAtEmployer";
            //}
            else if (!float.TryParse(objInput.GrossMonthlyIncome, out f))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Invalid value supplied for GrossMonthlyIncome";
            }
            else if (float.TryParse(objInput.GrossMonthlyIncome, out f))
            {
                if (f <= 0)
                {
                    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    oResponse.ResponseData = "Invalid value supplied for GrossMonthlyIncome";
                }
            }

            return oResponse;
        }
        public XDSPortalLibrary.Entity_Layer.Response SubmitCustomVettingO(SqlConnection con, SqlConnection AdminConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, bool bBonusChecking, string strVoucherCode, XDSPortalLibrary.Entity_Layer.SubmitResponse oinputResponse, XDSPortalLibrary.Entity_Layer.Submit objInput, out XDSPortalLibrary.Entity_Layer.SubmitResponse oSResponse)
        {
            string strAdminCon = AdminConnection.ConnectionString;
            oSResponse = oinputResponse;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            string rXml = "";
            double Totalcost = 0;

            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML
                // use this XML data for generating the Report

                if (spr.ProductID > 0)
                {

                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {

                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }

                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {

                            if (eSC.KeyType.ToUpper().Trim() == "C")
                            {
                                eoConsumerTraceWseManager.ConsumerID = eSC.KeyID;
                            }
                            else if (eSC.KeyType.ToUpper().Trim() == "H")
                            {
                                eoConsumerTraceWseManager.HomeAffairsID = eSC.KeyID;
                            }
                            eoConsumerTraceWseManager.IDno = eSe.IDNo;
                            eoConsumerTraceWseManager.Passportno = eSe.PassportNo;
                            eoConsumerTraceWseManager.CreatedbyUser = eSe.CreatedByUser;
                            //eoConsumerTraceWseManager.GrossMonthlyIncome = (double)eSe.NetMonthlyIncomeAmt;
                            eoConsumerTraceWseManager.GrossMonthlyIncome = 0;
                            eoConsumerTraceWseManager.ReferenceNo = eSC.Reference;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.EnquiryID = intSubscriberEnquiryID;
                            eoConsumerTraceWseManager.EnquiryResultID = intSubscriberEnquiryResultID;
                            eoConsumerTraceWseManager.subscriberID = eSe.SubscriberID;
                            eoConsumerTraceWseManager.ProductID = eSe.ProductID;
                            eoConsumerTraceWseManager.UserName = sys.Username;
                            eoConsumerTraceWseManager.AssociationTypeCode = sub.AssociationTypeCode;
                            eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;
                            eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                            eoConsumerTraceWseManager.EmploymentType = objInput.Items.EmploymentStatus;
                            eoConsumerTraceWseManager.EmploymentSector = string.Empty;
                            eoConsumerTraceWseManager.NoOfDependants = objInput.Items.NumberOfDependants;
                            eoConsumerTraceWseManager.ProvinceCode = objInput.Items.ProvinceCode;
                            eoConsumerTraceWseManager.Income = objInput.Items.Income;
                            //eoConsumerTraceWseManager.IDType = objInput.Items.IDType.ToUpper();//added by Kopano
                            eoConsumerTraceWseManager.DOB = eSe.BirthDate;
                            eoConsumerTraceWseManager.MaidenName = eSe.MaidenName;
                            eoConsumerTraceWseManager.Gender = eSe.Gender;


                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.CustomvettingO(eoConsumerTraceWseManager, oSResponse, out oSResponse);

                            //if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            //{
                            //    eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            //    ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            //    rp = ra.CustomvettingO(eoConsumerTraceWseManager, oSResponse, out oSResponse);
                            //}

                            rXml = rp.ResponseData;

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe.ExtraVarInput2 = rp.TmpReference;
                                throw new Exception(rp.ResponseData);
                            }


                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                eSC.DetailsViewedDate = DateTime.Now;
                                eSC.DetailsViewedYN = true;
                                if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    eSC.Billable = false;
                                }
                                else
                                {
                                    eSC.Billable = true;
                                }
                                eSC.ChangedByUser = eSe.CreatedByUser;
                                eSC.ChangedOnDate = DateTime.Now;

                                eSC.KeyID = rp.ResponseKey;
                                eSC.KeyType = rp.ResponseKeyType;
                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                eSC.BillingTypeID = spr.BillingTypeID;
                                eSC.BillingPrice = spr.UnitPrice;

                                eSC.XMLData = rXml.Replace("'", "''");
                                eSC.XMLBonus = rp.TmpReference;
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                eSC.ProductID = eSe.ProductID;

                                dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                rp.EnquiryID = eSC.SubscriberEnquiryID;

                                // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                if (sub.PayAsYouGo == 1)
                                {
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                }
                                if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                }

                                if (String.IsNullOrEmpty(eSC.VoucherCode))
                                {
                                    //Log FootPrint

                                    eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                    eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                    eSubscriberFootPrint.KeyID = eSC.KeyID;
                                    eSubscriberFootPrint.KeyType = eSC.KeyType;
                                    eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                    eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                    if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                    {
                                        eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                    }
                                    eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                    eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                    eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                    eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                    eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                    eSubscriberFootPrint.CreatedByUser = sys.Username;
                                    eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                    eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                    eSubscriberFootPrint.ProductID = eSe.ProductID;
                                    eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                    int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                }
                            }

                            rXml = rp.ResponseData;
                        }
                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.

                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                        }
                    }
                    else
                    {
                        // When User want to Re-Open a report

                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = eSC.XMLData;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }


                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryErrorJD(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }





        public XDSPortalLibrary.Entity_Layer.Response SubmitCustomVettingQ(SqlConnection con, SqlConnection AdminConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, bool bBonusChecking, string strVoucherCode, XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplicationResponse oinputResponse, XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplication objInput, out XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplicationResponse oSResponse, int intProductindicator, string strIsExistingClient, int intCustomVettingOinputID)
        {
            string strAdminCon = AdminConnection.ConnectionString;
            oSResponse = oinputResponse;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            string rXml = "";
            double Totalcost = 0;

            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML
                // use this XML data for generating the Report

                if (spr.ProductID > 0)
                {

                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {

                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }

                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {

                            if (eSC.KeyType.ToUpper().Trim() == "C")
                            {
                                eoConsumerTraceWseManager.ConsumerID = eSC.KeyID;
                            }
                            else if (eSC.KeyType.ToUpper().Trim() == "H")
                            {
                                eoConsumerTraceWseManager.HomeAffairsID = eSC.KeyID;
                            }
                            eoConsumerTraceWseManager.IDno = eSe.IDNo;
                            eoConsumerTraceWseManager.Passportno = eSe.PassportNo;
                            DateTime dtBirthDate = new DateTime();

                            if (!DateTime.TryParseExact(objInput.input.BirthDate, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtBirthDate))
                            {
                                DateTime.TryParse(objInput.input.BirthDate == "" ? "1900/01/01" : objInput.input.BirthDate, out dtBirthDate);
                            }

                            eoConsumerTraceWseManager.DOB = dtBirthDate;
                            eoConsumerTraceWseManager.Gender = objInput.input.Gender.Substring(0, 1);
                            eoConsumerTraceWseManager.CreatedbyUser = eSe.CreatedByUser;
                            eoConsumerTraceWseManager.GrossMonthlyIncome = (double)eSe.NetMonthlyIncomeAmt;
                            eoConsumerTraceWseManager.ReferenceNo = eSC.Reference;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.EnquiryID = intSubscriberEnquiryID;
                            eoConsumerTraceWseManager.EnquiryResultID = intSubscriberEnquiryResultID;
                            eoConsumerTraceWseManager.subscriberID = eSe.SubscriberID;
                            eoConsumerTraceWseManager.ProductID = eSe.ProductID;
                            eoConsumerTraceWseManager.UserName = sys.Username;
                            eoConsumerTraceWseManager.AssociationTypeCode = sub.AssociationTypeCode;
                            eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;
                            eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                            eoConsumerTraceWseManager.Branch = string.IsNullOrEmpty(strIsExistingClient) ? "false" : (strIsExistingClient.ToUpper().Trim() == "TRUE" ? "true" : (strIsExistingClient.ToUpper().Trim() == "FALSE" ? "false" : (strIsExistingClient.ToUpper().Trim() == "T" ? "true" : "false")));

                            //eoConsumerTraceWseManager.EmploymentType = objInput.Items.EmploymentStatus;
                            //eoConsumerTraceWseManager.EmploymentSector = string.Empty;
                            //eoConsumerTraceWseManager.NoOfDependants = objInput.Items.NumberOfDependants;
                            //eoConsumerTraceWseManager.ProvinceCode = objInput.Items.ProvinceCode;
                            //eoConsumerTraceWseManager.Income = objInput.Items.Income;
                            //eoConsumerTraceWseManager.IDType = objInput.Items.IDType.ToUpper();//added by Kopano

                            //Call FMP First
                            //Call FMP
                            ////////XDSPortalLibrary.Entity_Layer.InputData FMPInput = new XDSPortalLibrary.Entity_Layer.InputData();
                            ////////FMPInput.AccountTypeID = 1;
                            ////////FMPInput.BusinessProcessID = 2;
                            ////////FMPInput.CustomerTypeID = 1;
                            ////////FMPInput.TransactionTypeID = 1;
                            ////////FMPInput.ProductID = 1;
                            ////////FMPInput.SubscriberID = eSe.SubscriberID;
                            ////////FMPInput.ApplicationDateTime = DateTime.Now;
                            ////////FMPInput.IDNumber = objInput.input.IdentityNumber;
                            ////////FMPInput.FirstName = objInput.input.FirstName;
                            ////////FMPInput.LastName = objInput.input.Surname;
                            ////////FMPInput.IdentificationTypeID = int.Parse(objInput.input.IDType);
                            ////////if (objInput.input.MaritalStatus == "Single")
                            ////////{
                            ////////    FMPInput.MaritalStatusID = 1;
                            ////////}
                            ////////else if (objInput.input.MaritalStatus == "Married")
                            ////////{
                            ////////    FMPInput.MaritalStatusID = 2;
                            ////////}
                            ////////else if (objInput.input.MaritalStatus == "Widowed")
                            ////////{
                            ////////    FMPInput.MaritalStatusID = 3;
                            ////////}
                            ////////else if (objInput.input.MaritalStatus == "Divorced")
                            ////////{
                            ////////    FMPInput.MaritalStatusID = 4;
                            ////////}
                            ////////if (objInput.input.Gender == "M")
                            ////////{
                            ////////    FMPInput.GenderID = 1;
                            ////////}
                            ////////else if (objInput.input.Gender == "F")
                            ////////{
                            ////////    FMPInput.GenderID = 2;
                            ////////}
                            ////////FMPInput.CellphoneNumber = objInput.input.CellNumber;
                            ////////FMPInput.HomeTelephoneNumber = objInput.input.HomeTelephoneNumber;
                            ////////FMPInput.WorkTelephoneNumber = objInput.input.WorkTelephoneNumber;
                            ////////FMPInput.AlternateContactNumber = objInput.input.AlternateContactNumber;
                            ////////FMPInput.EmailAddress = objInput.input.EmailAddress;
                            ////////FMPInput.ResidentialAddress1 = objInput.input.AddressLine1;
                            ////////FMPInput.ResidentialAddress2 = objInput.input.AddressLine2;
                            ////////FMPInput.ResidentialAddress3 = objInput.input.Suburb;
                            ////////FMPInput.ResidentialAddress4 = objInput.input.City;
                            ////////FMPInput.ResidentialAddressPostalCode = objInput.input.PostalCode;
                            ////////if (!string.IsNullOrEmpty(objInput.input.GrossMonthlyIncome))
                            ////////{
                            ////////    FMPInput.MonthlyIncome = Convert.ToDouble(objInput.input.GrossMonthlyIncome);
                            ////////}
                            ////////FMPInput.BankBranchCode = objInput.input.BankBranchCode;
                            ////////FMPInput.BankAccountNumber = objInput.input.BankAccountNumber;
                            ////////FMPInput.DealerCode = objInput.input.DealerCode;

                            ////////XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            ////////XDSPortalLibrary.Entity_Layer.OutputData FMPOutput = ra.FMP(FMPInput, objInput.input.User, objInput.input.Password, System.Configuration.ConfigurationManager.AppSettings["FMPLoginURL"].ToString(), System.Configuration.ConfigurationManager.AppSettings["FMPMatchURL"].ToString());
                            ////////string FraudRules = "", FraudCharacteristics = "";
                            ////////if (FMPOutput.FraudRuleTriggered != null)
                            ////////{
                            ////////    FraudRules = string.Join("|", FMPOutput.FraudRuleTriggered);
                            ////////}
                            ////////if (FMPOutput.CharacteristicAttribute != null)
                            ////////{
                            ////////    FraudCharacteristics = string.Join("|", FMPOutput.CharacteristicAttribute);
                            ////////}

                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.CustomvettingQ(eoConsumerTraceWseManager, oSResponse, out oSResponse, objInput, intCustomVettingOinputID);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.CustomvettingQ(eoConsumerTraceWseManager, oSResponse, out oSResponse, objInput, intCustomVettingOinputID);
                            }

                            rXml = rp.ResponseData;

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                throw new Exception(rp.ResponseData);
                            }


                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                eSC.DetailsViewedDate = DateTime.Now;
                                eSC.DetailsViewedYN = true;
                                if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    eSC.Billable = false;
                                }
                                else
                                {
                                    eSC.Billable = true;
                                }
                                eSC.ChangedByUser = eSe.CreatedByUser;
                                eSC.ChangedOnDate = DateTime.Now;

                                eSC.KeyID = rp.ResponseKey;
                                eSC.KeyType = rp.ResponseKeyType;
                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                eSC.BillingTypeID = spr.BillingTypeID;
                                eSC.BillingPrice = spr.UnitPrice;

                                eSC.XMLData = rXml.Replace("'", "''");
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                eSC.ProductID = eSe.ProductID;

                                dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                rp.EnquiryID = eSC.SubscriberEnquiryID;

                                // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                if (sub.PayAsYouGo == 1)
                                {
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                }
                                if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                }

                                if (String.IsNullOrEmpty(eSC.VoucherCode))
                                {
                                    //Log FootPrint

                                    eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                    eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                    eSubscriberFootPrint.KeyID = eSC.KeyID;
                                    eSubscriberFootPrint.KeyType = eSC.KeyType;
                                    eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                    eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                    if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                    {
                                        eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                    }
                                    eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                    eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                    eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                    eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                    eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                    eSubscriberFootPrint.CreatedByUser = sys.Username;
                                    eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                    eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                    eSubscriberFootPrint.ProductID = eSe.ProductID;
                                    eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                    int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                }
                            }

                            rXml = rp.ResponseData;
                        }
                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.

                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                        }
                    }
                    else
                    {
                        // When User want to Re-Open a report

                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = eSC.XMLData;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }


                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response ProcessCanvasVetting(SqlConnection con, SqlConnection AdminConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, bool bBonusChecking, string strVoucherCode, XDSPortalLibrary.Entity_Layer.CanvasVettingResponse oinputResponse, XDSPortalLibrary.Entity_Layer.MTNCanvasInput objInput, out XDSPortalLibrary.Entity_Layer.CanvasVettingResponse oSResponse, int intProductindicator, string strIsExistingClient)
        {
            string strAdminCon = AdminConnection.ConnectionString;
            oSResponse = oinputResponse;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            string rXml = "";
            double Totalcost = 0;

            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML
                // use this XML data for generating the Report

                if (spr.ProductID > 0)
                {

                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {

                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }

                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {

                            if (eSC.KeyType.ToUpper().Trim() == "C")
                            {
                                eoConsumerTraceWseManager.ConsumerID = eSC.KeyID;
                            }
                            else if (eSC.KeyType.ToUpper().Trim() == "H")
                            {
                                eoConsumerTraceWseManager.HomeAffairsID = eSC.KeyID;
                            }
                            eoConsumerTraceWseManager.IDno = eSe.IDNo;
                            eoConsumerTraceWseManager.Passportno = eSe.PassportNo;
                            DateTime dtBirthDate = new DateTime();

                            if (!DateTime.TryParseExact(objInput.BirthDate, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtBirthDate))
                            {
                                DateTime.TryParse(objInput.BirthDate == "" ? "1900/01/01" : objInput.BirthDate, out dtBirthDate);
                            }

                            eoConsumerTraceWseManager.DOB = dtBirthDate;
                            eoConsumerTraceWseManager.Gender = objInput.Gender.Substring(0, 1);
                            eoConsumerTraceWseManager.CreatedbyUser = eSe.CreatedByUser;
                            eoConsumerTraceWseManager.GrossMonthlyIncome = (double)eSe.NetMonthlyIncomeAmt;
                            eoConsumerTraceWseManager.ReferenceNo = eSC.Reference;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.EnquiryID = intSubscriberEnquiryID;
                            eoConsumerTraceWseManager.EnquiryResultID = intSubscriberEnquiryResultID;
                            eoConsumerTraceWseManager.subscriberID = eSe.SubscriberID;
                            eoConsumerTraceWseManager.ProductID = eSe.ProductID;
                            eoConsumerTraceWseManager.UserName = sys.Username;
                            eoConsumerTraceWseManager.AssociationTypeCode = sub.AssociationTypeCode;
                            eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;
                            eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                            eoConsumerTraceWseManager.Branch = string.IsNullOrEmpty(strIsExistingClient) ? "false" : (strIsExistingClient.ToUpper().Trim() == "TRUE" ? "true" : (strIsExistingClient.ToUpper().Trim() == "FALSE" ? "false" : (strIsExistingClient.ToUpper().Trim() == "T" ? "true" : "false")));
                            oSResponse.UniqueID = eSC.KeyID.ToString();

                            //eoConsumerTraceWseManager.EmploymentType = objInput.Items.EmploymentStatus;
                            //eoConsumerTraceWseManager.EmploymentSector = string.Empty;
                            //eoConsumerTraceWseManager.NoOfDependants = objInput.Items.NumberOfDependants;
                            //eoConsumerTraceWseManager.ProvinceCode = objInput.Items.ProvinceCode;
                            //eoConsumerTraceWseManager.Income = objInput.Items.Income;
                            //eoConsumerTraceWseManager.IDType = objInput.Items.IDType.ToUpper();//added by Kopano

                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.MTNCanvassing(eoConsumerTraceWseManager, oSResponse, out oSResponse, objInput);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.MTNCanvassing(eoConsumerTraceWseManager, oSResponse, out oSResponse, objInput);
                            }

                            rXml = rp.ResponseData;

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                throw new Exception(rp.ResponseData);
                            }


                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                eSC.DetailsViewedDate = DateTime.Now;
                                eSC.DetailsViewedYN = true;
                                if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    eSC.Billable = false;
                                }
                                else
                                {
                                    eSC.Billable = true;
                                }
                                eSC.ChangedByUser = eSe.CreatedByUser;
                                eSC.ChangedOnDate = DateTime.Now;

                                eSC.KeyID = rp.ResponseKey;
                                eSC.KeyType = rp.ResponseKeyType;
                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                eSC.BillingTypeID = spr.BillingTypeID;
                                eSC.BillingPrice = spr.UnitPrice;

                                eSC.XMLData = rXml.Replace("'", "''");
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                eSC.ProductID = eSe.ProductID;

                                dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                rp.EnquiryID = eSC.SubscriberEnquiryID;

                                // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                if (sub.PayAsYouGo == 1)
                                {
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                }
                                if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                }

                                if (String.IsNullOrEmpty(eSC.VoucherCode))
                                {
                                    //Log FootPrint

                                    eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                    eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                    eSubscriberFootPrint.KeyID = eSC.KeyID;
                                    eSubscriberFootPrint.KeyType = eSC.KeyType;
                                    eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                    eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                    if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                    {
                                        eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                    }
                                    eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                    eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                    eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                    eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                    eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                    eSubscriberFootPrint.CreatedByUser = sys.Username;
                                    eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                    eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                    eSubscriberFootPrint.ProductID = eSe.ProductID;
                                    eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                    int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                }
                            }

                            rXml = rp.ResponseData;
                        }
                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.

                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                        }
                    }
                    else
                    {
                        // When User want to Re-Open a report

                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = eSC.XMLData;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }


                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitCanvasReport(SqlConnection con, SqlConnection AdminConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, bool bBonusChecking, string strVoucherCode, XDSPortalLibrary.Entity_Layer.CanvasVettingResult oinputResult, XDSPortalLibrary.Entity_Layer.MTNCanvasInput objInput, out XDSPortalLibrary.Entity_Layer.CanvasVettingResult oSResult, int intProductindicator, string strIsExistingClient, string User, string Password, string strFMPLoginURL, string strFMPMatchURL)
        {
            string strAdminCon = AdminConnection.ConnectionString;
            oSResult = oinputResult;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            string rXml = "";
            double Totalcost = 0;

            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);



                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML
                // use this XML data for generating the Report



                if (!string.IsNullOrEmpty(eSC.XMLData))
                {

                    //Trigger FMP
                    if (string.IsNullOrEmpty(oSResult.AVSResult.ErrorCode) || objInput.OverwriteAVSResult || (!string.IsNullOrEmpty(oSResult.AVSResult.ErrorCode) && (oSResult.AVSResult.ErrorCode == "AVS004" || oSResult.AVSResult.ErrorCode == "AVS002")))
                    {
                        eoConsumerTraceWseManager.FirstName = objInput.FirstName;
                        eoConsumerTraceWseManager.IDno = objInput.IdentityNumber;
                        eoConsumerTraceWseManager.Surname = objInput.Surname;
                        eoConsumerTraceWseManager.Gender = objInput.Gender.ToString();
                        eoConsumerTraceWseManager.AccountType = objInput.BankAccountType;
                        //eoConsumerTraceWseManager.CompanyName = objInput.CompanyName;
                        eoConsumerTraceWseManager.Branch = objInput.Branch;
                        eoConsumerTraceWseManager.RuleSet = objInput.RuleSet;
                        // eoConsumerTraceWseManager.Title = objInput.Title;
                        eoConsumerTraceWseManager.AddressLine1 = objInput.AddressLine1;
                        eoConsumerTraceWseManager.AddressLine2 = objInput.AddressLine2;
                        eoConsumerTraceWseManager.Suburb = objInput.Suburb;
                        eoConsumerTraceWseManager.City = objInput.City;
                        eoConsumerTraceWseManager.PhysicalPostalCode = objInput.PostalCode;
                        //eoConsumerTraceWseManager.ProvinceCode = objInput.provi;
                        //eoConsumerTraceWseManager.OwnerTenant = objInput.OwnerTenant;
                        //eoConsumerTraceWseManager.PostalAddressLine1 = objInput.;
                        //eoConsumerTraceWseManager.PostalAddressLine2 = objInput.PostalAddressLine2;
                        //eoConsumerTraceWseManager.PostalSuburb = objInput.PostalSuburb;
                        //eoConsumerTraceWseManager.PostalCity = objInput.PostalSuburb;
                        //eoConsumerTraceWseManager.PostalCode = objInput.PostalCity;
                        //eoConsumerTraceWseManager.PostalProvinceCode = objInput.PostalProvinceCode;
                        eoConsumerTraceWseManager.HomeTelephoneCode = objInput.HomeTelephoneCode;
                        eoConsumerTraceWseManager.HomePhoneNumber = objInput.HomeTelephoneNumber;
                        eoConsumerTraceWseManager.WorkTelephoneCode = objInput.WorkTelephoneCode;
                        eoConsumerTraceWseManager.WorkPhoneNumber = objInput.WorkTelephoneNumber;
                        eoConsumerTraceWseManager.Cellphonenumber = objInput.CellNumber;
                        eoConsumerTraceWseManager.BankName = objInput.BankName;
                        eoConsumerTraceWseManager.BranchCode = objInput.BankBranchCode;
                        eoConsumerTraceWseManager.BankAccountNumber = objInput.BankAccountNumber;

                        //eoConsumerTraceWseManager.YearsatCurrentEmployer =objInput. YearsatCurrentEmployer;
                        //eoConsumerTraceWseManager.SFID = objInput.SFID;
                        eoConsumerTraceWseManager.Income = objInput.GrossMonthlyIncome;
                        eoConsumerTraceWseManager.IDType = objInput.IDType.ToString().ToUpper();
                        if (oSResult.AVSResult.ErrorCode == "AVS003")
                        {
                            eoConsumerTraceWseManager.AVSNeeded = true;
                        }
                        else
                        {
                            eoConsumerTraceWseManager.AVSNeeded = false;
                        }
                        eoConsumerTraceWseManager.AVSCallNotSuccessful = (oSResult.AVSResult.ErrorCode == "AVS003" || oSResult.AVSResult.ErrorCode == "AVS001") ? true : false;
                        eoConsumerTraceWseManager.AVSNotPerformed = oSResult.AVSResult.ErrorCode == "AVS004" ? true : false;
                        eoConsumerTraceWseManager.EnquiryResultID = intSubscriberEnquiryResultID;

                        if (string.IsNullOrEmpty(oSResult.AVSResult.ErrorCode) && oSResult.AVSResult.ResultFile != null &&
                             oSResult.AVSResult.ResultFile.ACCOUNTFOUND.ToLower() == "yes" &&
                             oSResult.AVSResult.ResultFile.IDNUMBERMATCH.ToLower() == "yes" &&
                             //  oSResult.AVSResult.ResultFile.INITIALSMATCH.ToLower() == "yes" &&
                             oSResult.AVSResult.ResultFile.SURNAMEMATCH.ToLower() == "yes")
                        {
                            eoConsumerTraceWseManager.AVSPassed = true;
                        }
                        eoConsumerTraceWseManager.AVSSystemError = (oSResult.AVSResult.ErrorCode == "AVS001") ? true : false;

                        if (oSResult.AVSResult.ResultFile != null && eoConsumerTraceWseManager.AVSNeeded)
                        {
                            eoConsumerTraceWseManager.BankAccountAcceptsDebits = oSResult.AVSResult.ResultFile.ACCOUNTACCEPTSDEBITS.ToLower() == "yes" ? true : false;
                            eoConsumerTraceWseManager.BankAccountActive = oSResult.AVSResult.ResultFile.ACCOUNTOPEN.ToLower() == "yes" ? true : false;
                            eoConsumerTraceWseManager.BankAccountFound = oSResult.AVSResult.ResultFile.ACCOUNTFOUND.ToLower() == "yes" ? true : false;
                            eoConsumerTraceWseManager.BankAccountIDMatched = oSResult.AVSResult.ResultFile.IDNUMBERMATCH.ToLower() == "yes" ? true : false;
                            eoConsumerTraceWseManager.BankAccountOlderThan3Months = oSResult.AVSResult.ResultFile.ACCOUNTOPENFORATLEASTTHREEMONTHS.ToLower() == "yes" ? true : false;
                        }
                        //eoConsumerTraceWseManager.avs
                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        string FraudRules = "";
                        XDSPortalLibrary.Entity_Layer.OutputData FMPResult = ra.CellcFMP(eoConsumerTraceWseManager, User, Password, strFMPLoginURL, strFMPMatchURL, out FraudRules);

                        oSResult.FMPResult = new FMPResponse();


                        oSResult.FMPResult.FMPScore = FMPResult.FraudScore.ToString();
                        oSResult.FMPResult.FMPCharacterstics = string.Join(",", FMPResult.CharacteristicAttribute);
                        oSResult.FMPResult.ReasonCode = FMPResult.FraudRuleTriggered;
                        oSResult.FMPResult.ReasonDescription = FMPResult.CharacteristicAttribute;

                        oSResult.FMPResult.FMPCharacterstics = string.IsNullOrEmpty(oSResult.FMPResult.FMPCharacterstics) ? string.Empty : oSResult.FMPResult.FMPCharacterstics;

                        if (FMPResult.FraudScore > 0)
                        {
                            oSResult.FMPResult.ClientMessage = "You have been declined however please visit your nearest MTN store to view pre-paid options";
                        }
                        else
                        {
                            oSResult.FMPResult.ClientMessage = "No fraud rules triggered";
                        }

                    }

                    XmlSerializer serializer = new XmlSerializer(oSResult.GetType());
                    System.IO.StringWriter sw = new System.IO.StringWriter();
                    serializer.Serialize(sw, oSResult);
                    System.IO.StringReader reader = new System.IO.StringReader(sw.ToString());

                    eSC.XMLData = reader.ReadToEnd();
                    dSC.UpdateSubscriberEnquiryResult(con, eSC);

                }
                else
                {
                    // When User want to Re-Open a report

                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "Error getting the vetting result";
                    rp.EnquiryID = eSC.SubscriberEnquiryID;
                    rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;

                }



                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitCustomVettingQTeleSales(SqlConnection con, SqlConnection AdminConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, bool bBonusChecking, string strVoucherCode, XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplicationResponse oinputResponse, XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplication objInput, out XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplicationResponse oSResponse, int intProductindicator, string strIsExistingClient)
        {
            string strAdminCon = AdminConnection.ConnectionString;
            oSResponse = oinputResponse;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            string rXml = "";
            double Totalcost = 0;

            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML
                // use this XML data for generating the Report

                if (spr.ProductID > 0)
                {

                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {

                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }

                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {

                            if (eSC.KeyType.ToUpper().Trim() == "C")
                            {
                                eoConsumerTraceWseManager.ConsumerID = eSC.KeyID;
                            }
                            else if (eSC.KeyType.ToUpper().Trim() == "H")
                            {
                                eoConsumerTraceWseManager.HomeAffairsID = eSC.KeyID;
                            }
                            eoConsumerTraceWseManager.IDno = eSe.IDNo;
                            eoConsumerTraceWseManager.Passportno = eSe.PassportNo;
                            DateTime dtBirthDate = new DateTime();

                            if (!DateTime.TryParseExact(objInput.input.BirthDate, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtBirthDate))
                            {
                                DateTime.TryParse(objInput.input.BirthDate == "" ? "1900/01/01" : objInput.input.BirthDate, out dtBirthDate);
                            }

                            eoConsumerTraceWseManager.DOB = dtBirthDate;
                            eoConsumerTraceWseManager.Gender = objInput.input.Gender.Substring(0, 1);
                            eoConsumerTraceWseManager.CreatedbyUser = eSe.CreatedByUser;
                            eoConsumerTraceWseManager.GrossMonthlyIncome = (double)eSe.NetMonthlyIncomeAmt;
                            eoConsumerTraceWseManager.ReferenceNo = eSC.Reference;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.EnquiryID = intSubscriberEnquiryID;
                            eoConsumerTraceWseManager.EnquiryResultID = intSubscriberEnquiryResultID;
                            eoConsumerTraceWseManager.subscriberID = eSe.SubscriberID;
                            eoConsumerTraceWseManager.ProductID = eSe.ProductID;
                            eoConsumerTraceWseManager.UserName = sys.Username;
                            eoConsumerTraceWseManager.AssociationTypeCode = sub.AssociationTypeCode;
                            eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;
                            eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                            eoConsumerTraceWseManager.Branch = string.IsNullOrEmpty(strIsExistingClient) ? "false" : (strIsExistingClient.ToUpper().Trim() == "TRUE" ? "true" : (strIsExistingClient.ToUpper().Trim() == "FALSE" ? "false" : (strIsExistingClient.ToUpper().Trim() == "T" ? "true" : "false")));

                            //eoConsumerTraceWseManager.EmploymentType = objInput.Items.EmploymentStatus;
                            //eoConsumerTraceWseManager.EmploymentSector = string.Empty;
                            //eoConsumerTraceWseManager.NoOfDependants = objInput.Items.NumberOfDependants;
                            //eoConsumerTraceWseManager.ProvinceCode = objInput.Items.ProvinceCode;
                            //eoConsumerTraceWseManager.Income = objInput.Items.Income;
                            //eoConsumerTraceWseManager.IDType = objInput.Items.IDType.ToUpper();//added by Kopano

                            //Call FMP First
                            //Call FMP
                            ////////XDSPortalLibrary.Entity_Layer.InputData FMPInput = new XDSPortalLibrary.Entity_Layer.InputData();
                            ////////FMPInput.AccountTypeID = 1;
                            ////////FMPInput.BusinessProcessID = 2;
                            ////////FMPInput.CustomerTypeID = 1;
                            ////////FMPInput.TransactionTypeID = 1;
                            ////////FMPInput.ProductID = 1;
                            ////////FMPInput.SubscriberID = eSe.SubscriberID;
                            ////////FMPInput.ApplicationDateTime = DateTime.Now;
                            ////////FMPInput.IDNumber = objInput.input.IdentityNumber;
                            ////////FMPInput.FirstName = objInput.input.FirstName;
                            ////////FMPInput.LastName = objInput.input.Surname;
                            ////////FMPInput.IdentificationTypeID = int.Parse(objInput.input.IDType);
                            ////////if (objInput.input.MaritalStatus == "Single")
                            ////////{
                            ////////    FMPInput.MaritalStatusID = 1;
                            ////////}
                            ////////else if (objInput.input.MaritalStatus == "Married")
                            ////////{
                            ////////    FMPInput.MaritalStatusID = 2;
                            ////////}
                            ////////else if (objInput.input.MaritalStatus == "Widowed")
                            ////////{
                            ////////    FMPInput.MaritalStatusID = 3;
                            ////////}
                            ////////else if (objInput.input.MaritalStatus == "Divorced")
                            ////////{
                            ////////    FMPInput.MaritalStatusID = 4;
                            ////////}
                            ////////if (objInput.input.Gender == "M")
                            ////////{
                            ////////    FMPInput.GenderID = 1;
                            ////////}
                            ////////else if (objInput.input.Gender == "F")
                            ////////{
                            ////////    FMPInput.GenderID = 2;
                            ////////}
                            ////////FMPInput.CellphoneNumber = objInput.input.CellNumber;
                            ////////FMPInput.HomeTelephoneNumber = objInput.input.HomeTelephoneNumber;
                            ////////FMPInput.WorkTelephoneNumber = objInput.input.WorkTelephoneNumber;
                            ////////FMPInput.AlternateContactNumber = objInput.input.AlternateContactNumber;
                            ////////FMPInput.EmailAddress = objInput.input.EmailAddress;
                            ////////FMPInput.ResidentialAddress1 = objInput.input.AddressLine1;
                            ////////FMPInput.ResidentialAddress2 = objInput.input.AddressLine2;
                            ////////FMPInput.ResidentialAddress3 = objInput.input.Suburb;
                            ////////FMPInput.ResidentialAddress4 = objInput.input.City;
                            ////////FMPInput.ResidentialAddressPostalCode = objInput.input.PostalCode;
                            ////////if (!string.IsNullOrEmpty(objInput.input.GrossMonthlyIncome))
                            ////////{
                            ////////    FMPInput.MonthlyIncome = Convert.ToDouble(objInput.input.GrossMonthlyIncome);
                            ////////}
                            ////////FMPInput.BankBranchCode = objInput.input.BankBranchCode;
                            ////////FMPInput.BankAccountNumber = objInput.input.BankAccountNumber;
                            ////////FMPInput.DealerCode = objInput.input.DealerCode;

                            ////////XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            ////////XDSPortalLibrary.Entity_Layer.OutputData FMPOutput = ra.FMP(FMPInput, objInput.input.User, objInput.input.Password, System.Configuration.ConfigurationManager.AppSettings["FMPLoginURL"].ToString(), System.Configuration.ConfigurationManager.AppSettings["FMPMatchURL"].ToString());
                            ////////string FraudRules = "", FraudCharacteristics = "";
                            ////////if (FMPOutput.FraudRuleTriggered != null)
                            ////////{
                            ////////    FraudRules = string.Join("|", FMPOutput.FraudRuleTriggered);
                            ////////}
                            ////////if (FMPOutput.CharacteristicAttribute != null)
                            ////////{
                            ////////    FraudCharacteristics = string.Join("|", FMPOutput.CharacteristicAttribute);
                            ////////}

                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.CustomvettingQTeleSales(eoConsumerTraceWseManager, oSResponse, out oSResponse, objInput, 0);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.CustomvettingQTeleSales(eoConsumerTraceWseManager, oSResponse, out oSResponse, objInput, 0);
                            }

                            rXml = rp.ResponseData;

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                throw new Exception(rp.ResponseData);
                            }


                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                eSC.DetailsViewedDate = DateTime.Now;
                                eSC.DetailsViewedYN = true;
                                if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    eSC.Billable = false;
                                }
                                else
                                {
                                    eSC.Billable = true;
                                }
                                eSC.ChangedByUser = eSe.CreatedByUser;
                                eSC.ChangedOnDate = DateTime.Now;

                                eSC.KeyID = rp.ResponseKey;
                                eSC.KeyType = rp.ResponseKeyType;
                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                eSC.BillingTypeID = spr.BillingTypeID;
                                eSC.BillingPrice = spr.UnitPrice;

                                eSC.XMLData = rXml.Replace("'", "''");
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                eSC.ProductID = eSe.ProductID;

                                dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                rp.EnquiryID = eSC.SubscriberEnquiryID;

                                // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                if (sub.PayAsYouGo == 1)
                                {
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                }
                                if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                }

                                if (String.IsNullOrEmpty(eSC.VoucherCode))
                                {
                                    //Log FootPrint

                                    eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                    eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                    eSubscriberFootPrint.KeyID = eSC.KeyID;
                                    eSubscriberFootPrint.KeyType = eSC.KeyType;
                                    eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                    eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                    if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                    {
                                        eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                    }
                                    eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                    eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                    eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                    eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                    eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                    eSubscriberFootPrint.CreatedByUser = sys.Username;
                                    eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                    eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                    eSubscriberFootPrint.ProductID = eSe.ProductID;
                                    eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                    int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                }
                            }

                            rXml = rp.ResponseData;
                        }
                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.

                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                        }
                    }
                    else
                    {
                        // When User want to Re-Open a report

                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = eSC.XMLData;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }


                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitCustomVettingQA(SqlConnection con, SqlConnection AdminConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, bool bBonusChecking, string strVoucherCode, XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplicationResponse oinputResponse, XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplication objInput, out XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplicationResponse oSResponse, int intProductindicator, string strIsExistingClient)
        {
            string strAdminCon = AdminConnection.ConnectionString;
            oSResponse = oinputResponse;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            string rXml = "";
            double Totalcost = 0;

            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML
                // use this XML data for generating the Report

                if (spr.ProductID > 0)
                {

                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {

                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }

                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {

                            if (eSC.KeyType.ToUpper().Trim() == "C")
                            {
                                eoConsumerTraceWseManager.ConsumerID = eSC.KeyID;
                            }
                            else if (eSC.KeyType.ToUpper().Trim() == "H")
                            {
                                eoConsumerTraceWseManager.HomeAffairsID = eSC.KeyID;
                            }
                            eoConsumerTraceWseManager.IDno = eSe.IDNo;
                            eoConsumerTraceWseManager.Passportno = eSe.PassportNo;
                            DateTime dtBirthDate = new DateTime();

                            if (!DateTime.TryParseExact(objInput.input.BirthDate, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtBirthDate))
                            {
                                DateTime.TryParse(objInput.input.BirthDate == "" ? "1900/01/01" : objInput.input.BirthDate, out dtBirthDate);
                            }

                            eoConsumerTraceWseManager.DOB = dtBirthDate;
                            eoConsumerTraceWseManager.Gender = objInput.input.Gender.Substring(0, 1);
                            eoConsumerTraceWseManager.CreatedbyUser = eSe.CreatedByUser;
                            eoConsumerTraceWseManager.GrossMonthlyIncome = (double)eSe.NetMonthlyIncomeAmt;
                            eoConsumerTraceWseManager.ReferenceNo = eSC.Reference;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.EnquiryID = intSubscriberEnquiryID;
                            eoConsumerTraceWseManager.EnquiryResultID = intSubscriberEnquiryResultID;
                            eoConsumerTraceWseManager.subscriberID = eSe.SubscriberID;
                            eoConsumerTraceWseManager.ProductID = eSe.ProductID;
                            eoConsumerTraceWseManager.UserName = sys.Username;
                            eoConsumerTraceWseManager.AssociationTypeCode = sub.AssociationTypeCode;
                            eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;
                            eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                            eoConsumerTraceWseManager.Branch = string.IsNullOrEmpty(strIsExistingClient) ? "false" : (strIsExistingClient.ToUpper().Trim() == "TRUE" ? "true" : (strIsExistingClient.ToUpper().Trim() == "FALSE" ? "false" : (strIsExistingClient.ToUpper().Trim() == "T" ? "true" : "false")));

                            //eoConsumerTraceWseManager.EmploymentType = objInput.Items.EmploymentStatus;
                            //eoConsumerTraceWseManager.EmploymentSector = string.Empty;
                            //eoConsumerTraceWseManager.NoOfDependants = objInput.Items.NumberOfDependants;
                            //eoConsumerTraceWseManager.ProvinceCode = objInput.Items.ProvinceCode;
                            //eoConsumerTraceWseManager.Income = objInput.Items.Income;
                            //eoConsumerTraceWseManager.IDType = objInput.Items.IDType.ToUpper();//added by Kopano
                            //File.AppendAllText(@"C:\Log\Response.txt", "ReportAccess");

                            //Call FMP First
                            //Call FMP
                            ////////XDSPortalLibrary.Entity_Layer.InputData FMPInput = new XDSPortalLibrary.Entity_Layer.InputData();
                            ////////FMPInput.AccountTypeID = 1;
                            ////////FMPInput.BusinessProcessID = 2;
                            ////////FMPInput.CustomerTypeID = 1;
                            ////////FMPInput.TransactionTypeID = 1;
                            ////////FMPInput.ProductID = 1;
                            ////////FMPInput.SubscriberID = eSe.SubscriberID;
                            ////////FMPInput.ApplicationDateTime = DateTime.Now;
                            ////////FMPInput.IDNumber = objInput.input.IdentityNumber;
                            ////////FMPInput.FirstName = objInput.input.FirstName;
                            ////////FMPInput.LastName = objInput.input.Surname;
                            ////////FMPInput.IdentificationTypeID = int.Parse(objInput.input.IDType);
                            ////////if (objInput.input.MaritalStatus == "Single")
                            ////////{
                            ////////    FMPInput.MaritalStatusID = 1;
                            ////////}
                            ////////else if (objInput.input.MaritalStatus == "Married")
                            ////////{
                            ////////    FMPInput.MaritalStatusID = 2;
                            ////////}
                            ////////else if (objInput.input.MaritalStatus == "Widowed")
                            ////////{
                            ////////    FMPInput.MaritalStatusID = 3;
                            ////////}
                            ////////else if (objInput.input.MaritalStatus == "Divorced")
                            ////////{
                            ////////    FMPInput.MaritalStatusID = 4;
                            ////////}
                            ////////if (objInput.input.Gender == "M")
                            ////////{
                            ////////    FMPInput.GenderID = 1;
                            ////////}
                            ////////else if (objInput.input.Gender == "F")
                            ////////{
                            ////////    FMPInput.GenderID = 2;
                            ////////}
                            ////////FMPInput.CellphoneNumber = objInput.input.CellNumber;
                            ////////FMPInput.HomeTelephoneNumber = objInput.input.HomeTelephoneNumber;
                            ////////FMPInput.WorkTelephoneNumber = objInput.input.WorkTelephoneNumber;
                            ////////FMPInput.AlternateContactNumber = objInput.input.AlternateContactNumber;
                            ////////FMPInput.EmailAddress = objInput.input.EmailAddress;
                            ////////FMPInput.ResidentialAddress1 = objInput.input.AddressLine1;
                            ////////FMPInput.ResidentialAddress2 = objInput.input.AddressLine2;
                            ////////FMPInput.ResidentialAddress3 = objInput.input.Suburb;
                            ////////FMPInput.ResidentialAddress4 = objInput.input.City;
                            ////////FMPInput.ResidentialAddressPostalCode = objInput.input.PostalCode;
                            ////////if (!string.IsNullOrEmpty(objInput.input.GrossMonthlyIncome))
                            ////////{
                            ////////    FMPInput.MonthlyIncome = Convert.ToDouble(objInput.input.GrossMonthlyIncome);
                            ////////}
                            ////////FMPInput.BankBranchCode = objInput.input.BankBranchCode;
                            ////////FMPInput.BankAccountNumber = objInput.input.BankAccountNumber;
                            ////////FMPInput.DealerCode = objInput.input.DealerCode;

                            ////////XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            ////////XDSPortalLibrary.Entity_Layer.OutputData FMPOutput = ra.FMP(FMPInput, objInput.input.User, objInput.input.Password, System.Configuration.ConfigurationManager.AppSettings["FMPLoginURL"].ToString(), System.Configuration.ConfigurationManager.AppSettings["FMPMatchURL"].ToString());
                            ////////string FraudRules = "", FraudCharacteristics = "";
                            ////////if (FMPOutput.FraudRuleTriggered != null)
                            ////////{
                            ////////    FraudRules = string.Join("|", FMPOutput.FraudRuleTriggered);
                            ////////}
                            ////////if (FMPOutput.CharacteristicAttribute != null)
                            ////////{
                            ////////    FraudCharacteristics = string.Join("|", FMPOutput.CharacteristicAttribute);
                            ////////}

                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.CustomvettingQA(eoConsumerTraceWseManager, oSResponse, out oSResponse, objInput, 0);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.CustomvettingQA(eoConsumerTraceWseManager, oSResponse, out oSResponse, objInput, 0);
                            }

                            rXml = rp.ResponseData;

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                throw new Exception(rp.ResponseData);
                            }


                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                eSC.DetailsViewedDate = DateTime.Now;
                                eSC.DetailsViewedYN = true;
                                if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    eSC.Billable = false;
                                }
                                else
                                {
                                    eSC.Billable = true;
                                }
                                eSC.ChangedByUser = eSe.CreatedByUser;
                                eSC.ChangedOnDate = DateTime.Now;

                                eSC.KeyID = rp.ResponseKey;
                                eSC.KeyType = rp.ResponseKeyType;
                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                eSC.BillingTypeID = spr.BillingTypeID;
                                eSC.BillingPrice = spr.UnitPrice;

                                eSC.XMLData = rXml.Replace("'", "''");
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                eSC.ProductID = eSe.ProductID;

                                dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                rp.EnquiryID = eSC.SubscriberEnquiryID;

                                // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                if (sub.PayAsYouGo == 1)
                                {
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                }
                                if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                }

                                if (String.IsNullOrEmpty(eSC.VoucherCode))
                                {
                                    //Log FootPrint

                                    eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                    eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                    eSubscriberFootPrint.KeyID = eSC.KeyID;
                                    eSubscriberFootPrint.KeyType = eSC.KeyType;
                                    eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                    eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                    if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                    {
                                        eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                    }
                                    eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                    eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                    eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                    eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                    eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                    eSubscriberFootPrint.CreatedByUser = sys.Username;
                                    eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                    eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                    eSubscriberFootPrint.ProductID = eSe.ProductID;
                                    eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                    int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                }
                            }

                            rXml = rp.ResponseData;
                        }
                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.

                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                        }
                    }
                    else
                    {
                        // When User want to Re-Open a report

                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = eSC.XMLData;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }


                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitCustomVettingQSOA(SqlConnection con, SqlConnection AdminConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, bool bBonusChecking, string strVoucherCode, XDSPortalLibrary.Entity_Layer.MTN.SubmitResponse oinputResponse, XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplication objInput, out XDSPortalLibrary.Entity_Layer.MTN.SubmitResponse oSResponse, int intProductindicator, string strIsExistingClient)
        {
            string strAdminCon = AdminConnection.ConnectionString;
            oSResponse = oinputResponse;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            string rXml = "";
            double Totalcost = 0;

            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML
                // use this XML data for generating the Report

                if (spr.ProductID > 0)
                {

                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {

                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }

                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {

                            if (eSC.KeyType.ToUpper().Trim() == "C")
                            {
                                eoConsumerTraceWseManager.ConsumerID = eSC.KeyID;
                            }
                            else if (eSC.KeyType.ToUpper().Trim() == "H")
                            {
                                eoConsumerTraceWseManager.HomeAffairsID = eSC.KeyID;
                            }
                            eoConsumerTraceWseManager.IDno = eSe.IDNo;
                            eoConsumerCreditGrantorWseManager.Passportno = eSe.PassportNo;
                            eoConsumerTraceWseManager.CreatedbyUser = eSe.CreatedByUser;
                            eoConsumerTraceWseManager.GrossMonthlyIncome = (double)eSe.NetMonthlyIncomeAmt;
                            eoConsumerTraceWseManager.ReferenceNo = eSC.Reference;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.EnquiryID = intSubscriberEnquiryID;
                            eoConsumerTraceWseManager.EnquiryResultID = intSubscriberEnquiryResultID;
                            eoConsumerTraceWseManager.subscriberID = eSe.SubscriberID;
                            eoConsumerTraceWseManager.ProductID = eSe.ProductID;
                            eoConsumerTraceWseManager.UserName = sys.Username;
                            eoConsumerTraceWseManager.AssociationTypeCode = sub.AssociationTypeCode;
                            eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;
                            eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                            eoConsumerTraceWseManager.Branch = string.IsNullOrEmpty(strIsExistingClient) ? "false" : (strIsExistingClient.ToUpper().Trim() == "TRUE" ? "true" : (strIsExistingClient.ToUpper().Trim() == "FALSE" ? "false" : (strIsExistingClient.ToUpper().Trim() == "T" ? "true" : "false")));
                            eoConsumerTraceWseManager.NoOfDependants = intProductindicator.ToString();
                            //eoConsumerTraceWseManager.EmploymentType = objInput.Items.EmploymentStatus;
                            //eoConsumerTraceWseManager.EmploymentSector = string.Empty;
                            //eoConsumerTraceWseManager.NoOfDependants = objInput.Items.NumberOfDependants;
                            //eoConsumerTraceWseManager.ProvinceCode = objInput.Items.ProvinceCode;
                            //eoConsumerTraceWseManager.Income = objInput.Items.Income;
                            //eoConsumerTraceWseManager.IDType = objInput.Items.IDType.ToUpper();//added by Kopano

                            //Call FMP First
                            //Call FMP
                            ////////XDSPortalLibrary.Entity_Layer.InputData FMPInput = new XDSPortalLibrary.Entity_Layer.InputData();
                            ////////FMPInput.AccountTypeID = 1;
                            ////////FMPInput.BusinessProcessID = 2;
                            ////////FMPInput.CustomerTypeID = 1;
                            ////////FMPInput.TransactionTypeID = 1;
                            ////////FMPInput.ProductID = 1;
                            ////////FMPInput.SubscriberID = eSe.SubscriberID;
                            ////////FMPInput.ApplicationDateTime = DateTime.Now;
                            ////////FMPInput.IDNumber = objInput.input.IdentityNumber;
                            ////////FMPInput.FirstName = objInput.input.FirstName;
                            ////////FMPInput.LastName = objInput.input.Surname;
                            ////////if (objInput.input.IDType == "SAID")
                            ////////{
                            ////////    FMPInput.IdentificationTypeID = 1;
                            ////////}
                            ////////else if (objInput.input.IDType == "PASSPORT")
                            ////////{
                            ////////    FMPInput.IdentificationTypeID = 2;
                            ////////}
                            ////////if (objInput.input.MaritalStatus == "Single")
                            ////////{
                            ////////    FMPInput.MaritalStatusID = 1;
                            ////////}
                            ////////else if (objInput.input.MaritalStatus == "Married")
                            ////////{
                            ////////    FMPInput.MaritalStatusID = 2;
                            ////////}
                            ////////else if (objInput.input.MaritalStatus == "Widowed")
                            ////////{
                            ////////    FMPInput.MaritalStatusID = 3;
                            ////////}
                            ////////else if (objInput.input.MaritalStatus == "Divorced")
                            ////////{
                            ////////    FMPInput.MaritalStatusID = 4;
                            ////////}
                            ////////if (objInput.input.Gender == "M")
                            ////////{
                            ////////    FMPInput.GenderID = 1;
                            ////////}
                            ////////else if (objInput.input.Gender == "F")
                            ////////{
                            ////////    FMPInput.GenderID = 2;
                            ////////}
                            ////////FMPInput.CellphoneNumber = objInput.input.CellNumber;
                            ////////FMPInput.HomeTelephoneNumber = objInput.input.HomeTelephoneNumber;
                            ////////FMPInput.WorkTelephoneNumber = objInput.input.WorkTelephoneNumber;
                            ////////FMPInput.AlternateContactNumber = objInput.input.AlternateContactNumber;
                            ////////FMPInput.EmailAddress = objInput.input.EmailAddress;
                            ////////FMPInput.ResidentialAddress1 = objInput.input.AddressLine1;
                            ////////FMPInput.ResidentialAddress2 = objInput.input.AddressLine2;
                            ////////FMPInput.ResidentialAddress3 = objInput.input.Suburb;
                            ////////FMPInput.ResidentialAddress4 = objInput.input.City;
                            ////////FMPInput.ResidentialAddressPostalCode = objInput.input.PostalCode;
                            ////////if (!string.IsNullOrEmpty(objInput.input.GrossMonthlyIncome))
                            ////////{
                            ////////    FMPInput.MonthlyIncome = Convert.ToDouble(objInput.input.GrossMonthlyIncome);
                            ////////}
                            ////////FMPInput.BankBranchCode = objInput.input.BankBranchCode;
                            ////////FMPInput.BankAccountNumber = objInput.input.BankAccountNumber;
                            ////////FMPInput.DealerCode = objInput.input.DealerCode;

                            ////////XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            ////////XDSPortalLibrary.Entity_Layer.OutputData FMPOutput = ra.FMP(FMPInput, objInput.input.User, objInput.input.Password, System.Configuration.ConfigurationManager.AppSettings["FMPLoginURL"].ToString(), System.Configuration.ConfigurationManager.AppSettings["FMPMatchURL"].ToString());
                            ////////string FraudRules = "", FraudCharacteristics = "";
                            ////////if (FMPOutput.FraudRuleTriggered != null)
                            ////////{
                            ////////    FraudRules = string.Join("|", FMPOutput.FraudRuleTriggered);
                            ////////}
                            ////////if (FMPOutput.CharacteristicAttribute != null)
                            ////////{
                            ////////    FraudCharacteristics = string.Join("|", FMPOutput.CharacteristicAttribute);
                            ////////}

                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.CustomvettingQSOA(eoConsumerTraceWseManager, oSResponse, out oSResponse, objInput, 0);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.CustomvettingQSOA(eoConsumerTraceWseManager, oSResponse, out oSResponse, objInput, 0);
                            }

                            rXml = rp.ResponseData;

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                throw new Exception(rp.ResponseData);
                            }


                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                eSC.DetailsViewedDate = DateTime.Now;
                                eSC.DetailsViewedYN = true;
                                if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    eSC.Billable = false;
                                }
                                else
                                {
                                    eSC.Billable = true;
                                }
                                eSC.ChangedByUser = eSe.CreatedByUser;
                                eSC.ChangedOnDate = DateTime.Now;

                                eSC.KeyID = rp.ResponseKey;
                                eSC.KeyType = rp.ResponseKeyType;
                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                eSC.BillingTypeID = spr.BillingTypeID;
                                eSC.BillingPrice = spr.UnitPrice;

                                eSC.XMLData = rXml.Replace("'", "''");
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                eSC.ProductID = eSe.ProductID;

                                dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                rp.EnquiryID = eSC.SubscriberEnquiryID;

                                // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                if (sub.PayAsYouGo == 1)
                                {
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                }
                                if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                }

                                if (String.IsNullOrEmpty(eSC.VoucherCode))
                                {
                                    //Log FootPrint

                                    eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                    eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                    eSubscriberFootPrint.KeyID = eSC.KeyID;
                                    eSubscriberFootPrint.KeyType = eSC.KeyType;
                                    eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                    eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                    if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                    {
                                        eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                    }
                                    eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                    eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                    eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                    eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                    eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                    eSubscriberFootPrint.CreatedByUser = sys.Username;
                                    eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                    eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                    eSubscriberFootPrint.ProductID = eSe.ProductID;
                                    eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                    int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                }
                            }

                            rXml = rp.ResponseData;
                        }
                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.

                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                        }
                    }
                    else
                    {
                        // When User want to Re-Open a report

                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = eSC.XMLData;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }


                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response submitTransactionToVodacom(string userID, string transactionId, DateTime transactionCreationTimestamp, string requestApplicationId,
           string referenceNo, string enquiryId, string dealerId, string promotionDeal, string promoCode, string firstName, string surname, string idNo,
            DateTime birthDate, decimal income, string contactTelNo, string otherContactTelNo, string emailAddress, string createdByUser, DateTime createdOnDate,
            string sourceSystem, string portfolio, string score, string decision, string EndpointAddress,
            string smtpServer, string smtpUser, string smtpPassword, string smtpPort,
            bool sendEmailonFailure, string toAddress, string fromAddress, string ccAddress, string source, string Region, string AccountNo, string SourceId, string ReasonCode)
        {
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            string errorMessage = string.Empty;
            string step = string.Empty;
            try
            {
                RegisterSOAP.RegisterSoapExtension(typeof(myExtension), 1, PriorityGroup.Low);
                step = "11";
                Vodacomtest.RequestHeaderType oreq = new Vodacomtest.RequestHeaderType();
                step = "12";
                oreq.UserId = userID;
                oreq.TransactionId = transactionId;
                oreq.TransactionCreationTimestamp = transactionCreationTimestamp;
                oreq.RequestApplicationId = requestApplicationId;
                oreq.MessageCreationTimestamp = transactionCreationTimestamp;
                step = "13";
                Vodacomtest.CreateVettingPreApprovalsType ov = new Vodacomtest.CreateVettingPreApprovalsType();

                ov.ReferenceNo = referenceNo.IndexOf("-") > 0 ? referenceNo.Substring(1, referenceNo.IndexOf("-") - 1) : referenceNo;
                ov.EnquiryId = enquiryId;
                ov.DealerId = dealerId;
                ov.PromotionDeal = promotionDeal;
                ov.PromoCode = promoCode;
                ov.Region = Region;
                ov.FirstName = firstName;
                ov.Surname = surname;
                ov.IdNo = idNo;
                ov.BirthDate = birthDate;
                ov.Income = income;
                ov.AccountNo = AccountNo;
                ov.SourceId = SourceId;
                ov.ContactTelNo = contactTelNo;
                ov.OtherContactTelNo = otherContactTelNo;

                if (string.IsNullOrEmpty(emailAddress))
                {
                    ov.EmailAddress = "NA";
                }
                else
                {
                    ov.EmailAddress = emailAddress;
                }
                ov.CreatedByUser = createdByUser;
                ov.CreatedOnDate = createdOnDate;
                ov.SourceSystem = sourceSystem;


                ov.Portfolio = portfolio;
                ov.Score = score;
                ov.ReasonCode = ReasonCode;
                ov.Decision = decision;
                step = "14";
                Vodacomtest.ManageMarketingCampaignsSoap11BindingQSService OSER = new Vodacomtest.ManageMarketingCampaignsSoap11BindingQSService();
                step = "15";
                OSER.RequestHeader = oreq;
                OSER.Url = EndpointAddress;
                step = "16";

                Vodacomtest.CreateVettingPreApprovalsResponseType oresponse = OSER.CreateVettingPreApprovals(ov);

                rp.ResponseData = oresponse.Status + ":" + oresponse.ReferenceNo;

                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                rp.TmpReference = "NA";
                File.AppendAllText(@"c:\Log\Response_Kopano.txt", ov.AccountNo + " " + ov.ReasonCode + " " + ov.Region + " " + ov.SourceId);
            }
            catch (Exception ex)
            {
                string emailBody = "<p>Hi</p><br/><P>Please note that the submission to Vodacom Endpoint(" + EndpointAddress + ") failed with the error " + ex.Message + "</P>";
                string subject = source + " - Failure Notofication : Error Submitting the Enquiry to Vodacom End point(" + EndpointAddress + ")";
                File.AppendAllText(@"c:\Log\Response_Kopano.txt", step + " \n " + ex.Message);
                string status = SendMail(smtpServer, smtpUser, smtpPassword, smtpPort, toAddress, fromAddress, ccAddress, emailBody, subject, source);

                rp.ResponseData = ex.Message;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.TmpReference = status;
            }

            return rp;
        }

        private string SendMail(string Server, string User, string Password, string Port, string toAddress,
        string fromAddress, string ccAddress, string mailMessage, string mailSubject, string source)
        {
            string emailStatus = string.Empty;
            try
            {

                MailMessage Message = new MailMessage();
                MailAddress oEmailAdd = new MailAddress(fromAddress);

                foreach (string toEmail in toAddress.Split(','))
                {
                    Message.To.Add(toEmail);
                }
                Message.From = oEmailAdd;

                foreach (string ccEmail in ccAddress.Split(','))
                {
                    Message.CC.Add(ccEmail);
                }

                Message.Subject = mailSubject;

                Message.IsBodyHtml = true;

                Message.Body = mailMessage;
                Message.From = oEmailAdd;

                SmtpClient oSmtpClient = new SmtpClient(Server);
                oSmtpClient.UseDefaultCredentials = false;
                oSmtpClient.Credentials = new NetworkCredential(User, Password);
                oSmtpClient.Send(Message);
                emailStatus = "1";
            }
            catch (Exception ex)
            {
                emailStatus = ex.Message;
            }

            return emailStatus;
        }

        public XDSPortalLibrary.Entity_Layer.Response GetDAPulseScore(SqlConnection con, SqlConnection AdminConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, bool bBonusChecking, string strVoucherCode)
        {
            string strAdminCon = AdminConnection.ConnectionString;
            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            string rXml = "";
            double Totalcost = 0;

            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();


            moCustomVetting.ConnectionString = AdminConnection;
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML
                // use this XML data for generating the Report

                if (spr.ProductID > 0)
                {

                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {

                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }

                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {


                            eoConsumerTraceWseManager.IDno = eSe.IDNo;
                            eoConsumerTraceWseManager.ConsumerID = eSC.KeyID;
                            eoConsumerTraceWseManager.Surname = eSe.Surname;
                            eoConsumerTraceWseManager.FirstName = eSe.FirstName;
                            eoConsumerTraceWseManager.ReferenceNo = eSC.Reference;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                            eoConsumerTraceWseManager.AssociationTypeCode = sub.AssociationTypeCode;
                            eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;

                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);


                            rp = ra.GetDAPulseScoreReport(eoConsumerTraceWseManager);
                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.GetDAPulseScoreReport(eoConsumerTraceWseManager);
                            }

                            rXml = rp.ResponseData;

                            System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                ds.ReadXml(xmlSR);
                                if (ds.Tables.Contains("ConsumerDetail"))
                                {

                                    Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                    Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, eSe.ProductID);

                                    foreach (DataRow r in ds.Tables["ConsumerDetail"].Rows)
                                    {
                                        Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                        Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                        eSC.DetailsViewedDate = DateTime.Now;
                                        eSC.DetailsViewedYN = true;
                                        if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                        {
                                            eSC.Billable = false;
                                        }
                                        else
                                        {
                                            eSC.Billable = true;
                                        }
                                        eSC.ChangedByUser = eSe.CreatedByUser;
                                        eSC.ChangedOnDate = DateTime.Now;
                                        eSC.SearchOutput = "";


                                        if (r.Table.Columns.Contains("IDNo"))
                                        {
                                            if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                            {
                                                eSC.IDNo = r["IDNo"].ToString();
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                            }
                                        }
                                        if (r.Table.Columns.Contains("PassportNo"))
                                        {
                                            if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                            {
                                                eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                            }
                                        }
                                        if (r.Table.Columns.Contains("Surname"))
                                        {
                                            if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                            {
                                                eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                            }
                                        }

                                        if (r.Table.Columns.Contains("FirstName"))
                                        {
                                            if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                            {
                                                eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                            }
                                        }

                                        if (eSC.SearchOutput.Length > 0)
                                        {
                                            eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                        }

                                        eSC.KeyID = rp.ResponseKey;
                                        eSC.KeyType = rp.ResponseKeyType;
                                        eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                        eSC.BillingTypeID = spr.BillingTypeID;
                                        eSC.BillingPrice = spr.UnitPrice;

                                        eSC.XMLData = rXml.Replace("'", "''");
                                        eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                        eSC.ProductID = eSe.ProductID;

                                        dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                        // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                        if (sub.PayAsYouGo == 1)
                                        {
                                            dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                        }
                                        if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                        {
                                            dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                        }

                                        if (String.IsNullOrEmpty(eSC.VoucherCode))
                                        {
                                            //Log FootPrint

                                            eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                            eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                            eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                            eSubscriberFootPrint.KeyID = eSC.KeyID;
                                            eSubscriberFootPrint.KeyType = eSC.KeyType;
                                            eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                            eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                            if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                            {
                                                eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                            }
                                            eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                            eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                            eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                            eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                            eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                            eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                            eSubscriberFootPrint.CreatedByUser = sys.Username;
                                            eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                            eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                            eSubscriberFootPrint.ProductID = eSe.ProductID;
                                            eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                            int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                        }
                                    }
                                }

                                rXml = rp.ResponseData;

                            }
                        }
                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.

                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                        }



                    }
                    else
                    {
                        // When User want to Re-Open a report

                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = eSC.XMLData;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }


                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitCustomVettingQSOAA(SqlConnection con, SqlConnection AdminConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, bool bBonusChecking, string strVoucherCode, XDSPortalLibrary.Entity_Layer.MTN.SubmitResponse oinputResponse, XDSPortalLibrary.Entity_Layer.MTNHResponse.SubmitApplication objInput, out XDSPortalLibrary.Entity_Layer.MTN.SubmitResponse oSResponse, int intProductindicator, string strIsExistingClient)
        {
            string strAdminCon = AdminConnection.ConnectionString;
            oSResponse = oinputResponse;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            string rXml = "";
            double Totalcost = 0;

            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML
                // use this XML data for generating the Report

                if (spr.ProductID > 0)
                {

                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {

                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }

                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {

                            if (eSC.KeyType.ToUpper().Trim() == "C")
                            {
                                eoConsumerTraceWseManager.ConsumerID = eSC.KeyID;
                            }
                            else if (eSC.KeyType.ToUpper().Trim() == "H")
                            {
                                eoConsumerTraceWseManager.HomeAffairsID = eSC.KeyID;
                            }
                            eoConsumerTraceWseManager.IDno = eSe.IDNo;
                            eoConsumerCreditGrantorWseManager.Passportno = eSe.PassportNo;
                            eoConsumerTraceWseManager.CreatedbyUser = eSe.CreatedByUser;
                            eoConsumerTraceWseManager.GrossMonthlyIncome = (double)eSe.NetMonthlyIncomeAmt;
                            eoConsumerTraceWseManager.ReferenceNo = eSC.Reference;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.EnquiryID = intSubscriberEnquiryID;
                            eoConsumerTraceWseManager.EnquiryResultID = intSubscriberEnquiryResultID;
                            eoConsumerTraceWseManager.subscriberID = eSe.SubscriberID;
                            eoConsumerTraceWseManager.ProductID = eSe.ProductID;
                            eoConsumerTraceWseManager.UserName = sys.Username;
                            eoConsumerTraceWseManager.AssociationTypeCode = sub.AssociationTypeCode;
                            eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;
                            eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                            eoConsumerTraceWseManager.Branch = string.IsNullOrEmpty(strIsExistingClient) ? "false" : (strIsExistingClient.ToUpper().Trim() == "TRUE" ? "true" : (strIsExistingClient.ToUpper().Trim() == "FALSE" ? "false" : (strIsExistingClient.ToUpper().Trim() == "T" ? "true" : "false")));
                            eoConsumerTraceWseManager.NoOfDependants = intProductindicator.ToString();
                            //eoConsumerTraceWseManager.EmploymentType = objInput.Items.EmploymentStatus;
                            //eoConsumerTraceWseManager.EmploymentSector = string.Empty;
                            //eoConsumerTraceWseManager.NoOfDependants = objInput.Items.NumberOfDependants;
                            //eoConsumerTraceWseManager.ProvinceCode = objInput.Items.ProvinceCode;
                            //eoConsumerTraceWseManager.Income = objInput.Items.Income;
                            //eoConsumerTraceWseManager.IDType = objInput.Items.IDType.ToUpper();//added by Kopano

                            //Call FMP First
                            //Call FMP
                            ////////XDSPortalLibrary.Entity_Layer.InputData FMPInput = new XDSPortalLibrary.Entity_Layer.InputData();
                            ////////FMPInput.AccountTypeID = 1;
                            ////////FMPInput.BusinessProcessID = 2;
                            ////////FMPInput.CustomerTypeID = 1;
                            ////////FMPInput.TransactionTypeID = 1;
                            ////////FMPInput.ProductID = 1;
                            ////////FMPInput.SubscriberID = eSe.SubscriberID;
                            ////////FMPInput.ApplicationDateTime = DateTime.Now;
                            ////////FMPInput.IDNumber = objInput.input.IdentityNumber;
                            ////////FMPInput.FirstName = objInput.input.FirstName;
                            ////////FMPInput.LastName = objInput.input.Surname;
                            ////////if (objInput.input.IDType == "SAID")
                            ////////{
                            ////////    FMPInput.IdentificationTypeID = 1;
                            ////////}
                            ////////else if (objInput.input.IDType == "PASSPORT")
                            ////////{
                            ////////    FMPInput.IdentificationTypeID = 2;
                            ////////}
                            ////////if (objInput.input.MaritalStatus == "Single")
                            ////////{
                            ////////    FMPInput.MaritalStatusID = 1;
                            ////////}
                            ////////else if (objInput.input.MaritalStatus == "Married")
                            ////////{
                            ////////    FMPInput.MaritalStatusID = 2;
                            ////////}
                            ////////else if (objInput.input.MaritalStatus == "Widowed")
                            ////////{
                            ////////    FMPInput.MaritalStatusID = 3;
                            ////////}
                            ////////else if (objInput.input.MaritalStatus == "Divorced")
                            ////////{
                            ////////    FMPInput.MaritalStatusID = 4;
                            ////////}
                            ////////if (objInput.input.Gender == "M")
                            ////////{
                            ////////    FMPInput.GenderID = 1;
                            ////////}
                            ////////else if (objInput.input.Gender == "F")
                            ////////{
                            ////////    FMPInput.GenderID = 2;
                            ////////}
                            ////////FMPInput.CellphoneNumber = objInput.input.CellNumber;
                            ////////FMPInput.HomeTelephoneNumber = objInput.input.HomeTelephoneNumber;
                            ////////FMPInput.WorkTelephoneNumber = objInput.input.WorkTelephoneNumber;
                            ////////FMPInput.AlternateContactNumber = objInput.input.AlternateContactNumber;
                            ////////FMPInput.EmailAddress = objInput.input.EmailAddress;
                            ////////FMPInput.ResidentialAddress1 = objInput.input.AddressLine1;
                            ////////FMPInput.ResidentialAddress2 = objInput.input.AddressLine2;
                            ////////FMPInput.ResidentialAddress3 = objInput.input.Suburb;
                            ////////FMPInput.ResidentialAddress4 = objInput.input.City;
                            ////////FMPInput.ResidentialAddressPostalCode = objInput.input.PostalCode;
                            ////////if (!string.IsNullOrEmpty(objInput.input.GrossMonthlyIncome))
                            ////////{
                            ////////    FMPInput.MonthlyIncome = Convert.ToDouble(objInput.input.GrossMonthlyIncome);
                            ////////}
                            ////////FMPInput.BankBranchCode = objInput.input.BankBranchCode;
                            ////////FMPInput.BankAccountNumber = objInput.input.BankAccountNumber;
                            ////////FMPInput.DealerCode = objInput.input.DealerCode;

                            ////////XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            ////////XDSPortalLibrary.Entity_Layer.OutputData FMPOutput = ra.FMP(FMPInput, objInput.input.User, objInput.input.Password, System.Configuration.ConfigurationManager.AppSettings["FMPLoginURL"].ToString(), System.Configuration.ConfigurationManager.AppSettings["FMPMatchURL"].ToString());
                            ////////string FraudRules = "", FraudCharacteristics = "";
                            ////////if (FMPOutput.FraudRuleTriggered != null)
                            ////////{
                            ////////    FraudRules = string.Join("|", FMPOutput.FraudRuleTriggered);
                            ////////}
                            ////////if (FMPOutput.CharacteristicAttribute != null)
                            ////////{
                            ////////    FraudCharacteristics = string.Join("|", FMPOutput.CharacteristicAttribute);
                            ////////}

                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.CustomvettingQSOAA(eoConsumerTraceWseManager, oSResponse, out oSResponse, objInput, 0);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.CustomvettingQSOAA(eoConsumerTraceWseManager, oSResponse, out oSResponse, objInput, 0);
                            }

                            rXml = rp.ResponseData;

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                throw new Exception(rp.ResponseData);
                            }


                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                eSC.DetailsViewedDate = DateTime.Now;
                                eSC.DetailsViewedYN = true;
                                if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    eSC.Billable = false;
                                }
                                else
                                {
                                    eSC.Billable = true;
                                }
                                eSC.ChangedByUser = eSe.CreatedByUser;
                                eSC.ChangedOnDate = DateTime.Now;

                                eSC.KeyID = rp.ResponseKey;
                                eSC.KeyType = rp.ResponseKeyType;
                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                eSC.BillingTypeID = spr.BillingTypeID;
                                eSC.BillingPrice = spr.UnitPrice;

                                eSC.XMLData = rXml.Replace("'", "''");
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                eSC.ProductID = eSe.ProductID;

                                dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                rp.EnquiryID = eSC.SubscriberEnquiryID;

                                // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                if (sub.PayAsYouGo == 1)
                                {
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                }
                                if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                }

                                if (String.IsNullOrEmpty(eSC.VoucherCode))
                                {
                                    //Log FootPrint

                                    eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                    eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                    eSubscriberFootPrint.KeyID = eSC.KeyID;
                                    eSubscriberFootPrint.KeyType = eSC.KeyType;
                                    eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                    eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                    if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                    {
                                        eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                    }
                                    eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                    eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                    eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                    eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                    eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                    eSubscriberFootPrint.CreatedByUser = sys.Username;
                                    eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                    eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                    eSubscriberFootPrint.ProductID = eSe.ProductID;
                                    eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                    int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                }
                            }

                            rXml = rp.ResponseData;
                        }
                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.

                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                        }
                    }
                    else
                    {
                        // When User want to Re-Open a report

                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = eSC.XMLData;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }


                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitConsumerTraceVettingU(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string IdNumber, string PassportNo, string FirstName, string Surname, string DateOfBirth, double MonthlySalary, string EmailAddress, string Contactno, string AlternateContactNo, string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode, out VettingU.UResponse oResponse)
        {
            oResponse = new VettingU.UResponse();
            string strAdminCon = AdminConnection.ConnectionString;
            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            string sGender = "";
            string rXml = "";
            int MatchResult = 0;
            int intSubscriberEnquiryResultID = 0;
            DataSet ds = new DataSet();
            Boolean boolSubmitEnquiry = true;
            string strValidationStatus = "";

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);
            Entity.SubscriberProductReports sprFMP = xb.GetPrice(AdminConnection, intSubscriberID, 166);

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            string PressageScoreVersion = dsub.GetPressageVersion(AdminConnection, intSubscriberID);

            try
            {
                // Check if this subscriber is authorized to use the current product

                if (spr.ReportID > 0 && sprFMP.ReportID > 0)
                {
                    // Check the PayAsYouGo settings for the subscriber
                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1 && (spr.UnitPrice + sprFMP.UnitPrice) > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {
                        DateTime dtBirthDate = new DateTime();
                        if (!DateTime.TryParseExact(DateOfBirth, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtBirthDate))
                        {
                            DateTime.TryParse(DateOfBirth == "" ? "1900/01/01" : DateOfBirth, out dtBirthDate);
                        }


                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;

                        eSe.FirstName = FirstName;
                        eSe.IDNo = IdNumber;
                        eSe.PassportNo = PassportNo;
                        eSe.ProductID = intProductId;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = "";
                        eSe.Surname = Surname;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";
                        eSe.NetMonthlyIncomeAmt = (decimal)MonthlySalary;
                        eSe.BirthDate = dtBirthDate;
                        eSe.TelephoneNo = Contactno;
                        eSe.AccountNo = AlternateContactNo;
                        eSe.BusBusinessName = EmailAddress;

                        //Generate serach input string

                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.PassportNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.PassportNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.Surname))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.Surname;
                        }
                        if (!string.IsNullOrEmpty(eSe.FirstInitial))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.FirstName;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table

                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);
                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        //File.AppendAllText(@"C:\Log\Rewardsco.txt", "Validation begin");
                        rp = ValidateCustomVettingU(IdNumber, PassportNo, FirstName, Surname, DateOfBirth,
                            MonthlySalary, EmailAddress, Contactno, AlternateContactNo);
                        //File.AppendAllText(@"C:\Log\Rewardsco.txt", "Validation end");
                        //File.AppendAllText(@"C:\Log\Rewardsco.txt", rp.ResponseData);
                        //File.AppendAllText(@"C:\Log\Rewardsco.txt", rp.ResponseStatus.ToString());

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            oResponse.ErrorCode = rp.ResponseReferenceNo;
                            eSe.ErrorDescription = rp.ResponseData;
                            eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                            return rp;
                        }

                        //File.AppendAllText(@"C:\Log\Rewardsco.txt", "GetSubscriberEnquiryObject");

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        //File.AppendAllText(@"C:\Log\Rewardsco.txt", "GetSubscriberEnquiryObject done");

                        eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoConsumerTraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoConsumerTraceWseManager.ConfirmationChkBox = bConfirmationChkBox;

                        eoConsumerTraceWseManager.DOB = dtBirthDate;
                        eoConsumerTraceWseManager.ExternalReference = strExtRef;
                        eoConsumerTraceWseManager.FirstName = FirstName;
                        eoConsumerTraceWseManager.IDno = IdNumber;
                        eoConsumerTraceWseManager.Passportno = PassportNo;
                        eoConsumerTraceWseManager.ProductID = intProductId;
                        eoConsumerTraceWseManager.subscriberID = intSubscriberID;
                        eoConsumerTraceWseManager.Surname = Surname;
                        eoConsumerTraceWseManager.ContactNo = Contactno;
                        eoConsumerTraceWseManager.AccountHolderName = AlternateContactNo;
                        eoConsumerTraceWseManager.Income = MonthlySalary.ToString();
                        eoConsumerTraceWseManager.AccountType = EmailAddress;
                        eoConsumerTraceWseManager.UserName = sys.Username;
                        eoConsumerTraceWseManager.AssociationTypeCode = sub.AssociationTypeCode;
                        eoConsumerTraceWseManager.EnquiryID = eSe.SubscriberEnquiryID;

                        eoConsumerTraceWseManager.BonusCheck = bBonusChecking;
                        eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;
                        eoConsumerTraceWseManager.PressageScoreVersion = PressageScoreVersion;
                        eoConsumerTraceWseManager.ConsumerID = eSC.KeyID;
                        eoConsumerTraceWseManager.CreatedbyUser = eSe.SystemUser;


                        // Submit data for matching
                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.GetCustomvettingUReport(eoConsumerTraceWseManager, out oResponse);

                        //File.AppendAllText(@"C:\Log\Rewardsco.txt", rp.ResponseStatus.ToString());
                        //File.AppendAllText(@"C:\Log\Rewardsco.txt", rp.ResponseData);
                        //File.AppendAllText(@"C:\Log\Rewardsco.txt", oResponse.ErrorCode);
                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error && oResponse.ErrorCode != "009")
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.GetCustomvettingUReport(eoConsumerTraceWseManager, out oResponse);
                        }


                        rp.EnquiryID = intSubscriberEnquiryID;

                        // Get Response data

                        //rXml = rp.ResponseData;

                        //System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);


                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            eSC.SearchOutput = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.DetailsViewedYN = false;
                            eSC.Billable = false;
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;
                            eSC.KeyID = rp.ResponseKey;
                            eSC.KeyType = rp.ResponseKeyType;
                            eSC.XMLData = rp.ResponseData;


                            rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                            rp.EnquiryID = eSC.SubscriberEnquiryID;

                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                            Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                            eSC.IDNo = oResponse.RPrevetResponse.IDNumber;
                            eSC.PassportNo = oResponse.RPrevetResponse.Passportno;
                            eSC.Surname = oResponse.RPrevetResponse.Surname;
                            eSC.FirstName = oResponse.RPrevetResponse.FirstName;
                            eSC.BirthDate = oResponse.RPrevetResponse.BirthDate == null ? DateTime.Parse("1900/01/01") : (DateTime)oResponse.RPrevetResponse.BirthDate;
                            eSC.Gender = string.Empty;
                            eSC.DetailsViewedDate = DateTime.Now;
                            eSC.DetailsViewedYN = true;

                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;
                            eSC.SearchOutput = oResponse.RPrevetResponse.IDNumber + "|" + oResponse.RPrevetResponse.Surname + "|" + oResponse.RPrevetResponse.FirstName;

                            if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                            {
                                eSC.Billable = false;
                            }
                            else
                            {
                                eSC.Billable = true;
                            }
                            eSC.ChangedByUser = eSe.CreatedByUser;
                            eSC.ChangedOnDate = DateTime.Now;

                            eSC.KeyID = rp.ResponseKey;
                            eSC.KeyType = rp.ResponseKeyType;
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                            eSC.BillingTypeID = spr.BillingTypeID;
                            eSC.BillingPrice = spr.UnitPrice;

                            eSC.XMLData = rp.ResponseData;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                            eSC.ProductID = intProductId;
                            // File.AppendAllText(@"C:\Log\Rewardsco.txt", "InsertSubscriberEnquiryResult");
                            rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                            //File.AppendAllText(@"C:\Log\Rewardsco.txt", "InsertSubscriberEnquiryResult Done");
                            rp.EnquiryID = eSC.SubscriberEnquiryID;


                            // Change the PayAsYouGoEnquiryLimit  after viewing the report

                            if (sub.PayAsYouGo == 1)
                            {
                                dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, spr.UnitPrice);
                            }
                            if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                            {
                                dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                            }
                            if (String.IsNullOrEmpty(eSC.VoucherCode))
                            {
                                //Log FootPrint

                                eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                eSubscriberFootPrint.KeyID = eSC.KeyID;
                                eSubscriberFootPrint.KeyType = eSC.KeyType;
                                eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                {
                                    eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                }
                                eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                eSubscriberFootPrint.CreatedByUser = sys.Username;
                                eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                eSubscriberFootPrint.ProductID = eSe.ProductID;
                                eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                            }


                            if (oResponse.RPrevetResponse.Result.ToLower() == "approve")
                            {
                                eSe.ExtraVarInput3 = string.Empty;
                                eSC.ExtraIntOutput1 = eSe.SubscriberEnquiryID;
                                eSC.BillingTypeID = sprFMP.BillingTypeID;
                                eSC.BillingPrice = sprFMP.UnitPrice;

                                eSe.SubscriberEnquiryDate = DateTime.Now;
                                eSe.ProductID = sprFMP.ProductID;
                                eSe.SubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);

                                eSC.SubscriberEnquiryID = eSe.SubscriberEnquiryID;
                                eSC.ProductID = eSe.ProductID;
                                dSC.InsertSubscriberEnquiryResult(con, eSC);

                                if (sub.PayAsYouGo == 1)
                                {
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, sprFMP.UnitPrice);
                                }

                            }
                        }

                    }


                }

                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }
            catch (Exception oException)
            {

                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                oResponse.ErrorCode = "010";
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response ValidateCustomVettingU(string IdNumber, string PassportNo, string FirstName, string Surname, string DateOfBirth, double MonthlySalary, string EmailAddress, string Contactno, string AlternateContactNo)
        {
            DateTime dt = new DateTime();
            Response oVResponse = new Response();
            XDSData.Common.Functions oFunctions = new XDSData.Common.Functions();
            string EmailPattern = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|"
                                       + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)"
                                       + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";

            bool validcontact = Regex.IsMatch(Contactno, @"^\d{10}$");
            bool validalternatecontact = Regex.IsMatch(AlternateContactNo, @"^\d{10}$");
            bool validEmailPattern = Regex.IsMatch(EmailAddress.ToLower().Trim(), EmailPattern);

            if (string.IsNullOrEmpty(IdNumber) && string.IsNullOrEmpty(PassportNo))
            {
                oVResponse.ResponseStatus = Response.ResponseStatusEnum.Error;
                oVResponse.ResponseData = "Please supply ID Number or PassportNo";
                oVResponse.ResponseReferenceNo = "001";
            }
            else if (!string.IsNullOrEmpty(IdNumber) && !oFunctions.FnValidateIDNo(IdNumber))
            {
                oVResponse.ResponseStatus = Response.ResponseStatusEnum.Error;
                oVResponse.ResponseData = "Invalid Id number supplied";
                oVResponse.ResponseReferenceNo = "002";
            }

            //if (string.IsNullOrEmpty(IdNumber))
            //{
            //    oVResponse.ResponseStatus = Response.ResponseStatusEnum.Error;
            //    oVResponse.ResponseData = "ID Number Mandatory";
            //    oVResponse.ResponseKey = "001";
            //}
            //else if ( !oFunctions.FnValidateIDNo(IdNumber))
            //{
            //    oVResponse.ResponseStatus = Response.ResponseStatusEnum.Error;
            //    oVResponse.ResponseData = "Invalid Id number supplied";
            //    oVResponse.ResponseKey = "002";
            //}
            else if (string.IsNullOrEmpty(Surname))
            {
                oVResponse.ResponseStatus = Response.ResponseStatusEnum.Error;
                oVResponse.ResponseData = "Surname manadatory";
                oVResponse.ResponseReferenceNo = "003";
            }
            else if (string.IsNullOrEmpty(FirstName))
            {
                oVResponse.ResponseStatus = Response.ResponseStatusEnum.Error;
                oVResponse.ResponseData = "Firstname manadatory";
                oVResponse.ResponseReferenceNo = "004";
            }
            else if (!string.IsNullOrEmpty(PassportNo) && !DateTime.TryParse(DateOfBirth, out dt))
            {
                oVResponse.ResponseStatus = Response.ResponseStatusEnum.Error;
                oVResponse.ResponseData = "Date of birth manadatory for Passportnos";
                oVResponse.ResponseReferenceNo = "005";
            }
            else if (!string.IsNullOrEmpty(DateOfBirth) && !DateTime.TryParse(DateOfBirth, out dt))
            {
                oVResponse.ResponseStatus = Response.ResponseStatusEnum.Error;
                oVResponse.ResponseData = "Please supply valid date of birth";
                oVResponse.ResponseReferenceNo = "006";
            }
            else if (!string.IsNullOrEmpty(Contactno) && !validcontact)
            {
                oVResponse.ResponseStatus = Response.ResponseStatusEnum.Error;
                oVResponse.ResponseData = "Please supply valid contact no";
                oVResponse.ResponseReferenceNo = "007";
            }
            else if (!string.IsNullOrEmpty(AlternateContactNo) && !validalternatecontact)
            {
                oVResponse.ResponseStatus = Response.ResponseStatusEnum.Error;
                oVResponse.ResponseData = "Please supply valid alternate contact no";
                oVResponse.ResponseReferenceNo = "008";
            }
            else if (!string.IsNullOrEmpty(EmailAddress) && !validEmailPattern)
            {
                oVResponse.ResponseStatus = Response.ResponseStatusEnum.Error;
                oVResponse.ResponseData = "Please supply valid email address";
                oVResponse.ResponseReferenceNo = "009";
            }


            return oVResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response GetCreditScore(SqlConnection con, SqlConnection AdminConnection, SimRequest simRequest)
        {
            string strAdminCon = AdminConnection.ConnectionString;
            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            string rXml = "";
            double Totalcost = 0;

            DataSet ds = new DataSet();
            bool Billable = false;
            double BillingPrice = 0;

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();


            Entity.SubscriberEnquiry eSeM = new Entity.SubscriberEnquiry();
            Entity.SubscriberEnquiryResult eSCM = new Entity.SubscriberEnquiryResult();

            try
            {

                eSe = dSe.GetSubscriberEnquiryObject(con, simRequest.EnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, simRequest.EnquiryResultID);

                SqlConnection objConstring = new SqlConnection(eSe.ExtraVarInput1.ToString());

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, simRequest.ProductID);


                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                if (spr.ReportID > 0)
                {
                    if (eSe.ProductID != 15)
                    {
                        throw new Exception("Please supply the EnquiryID and Enquiry ResultID for the credit report");
                    }
                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {
                        rp.ResponseData = "Please complete the credit Enquiry";
                    }
                    else
                    {
                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }
                        else if (sub.PayAsYouGo == 1)
                        {
                            //Calculate The Total cost of the Report , including Bonus Segments

                            Totalcost = spr.UnitPrice;

                        }
                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {
                            xtension.CopyTo(eSC, eSCM);
                            //  File.AppendAllText(@"C:\Log\CrAppSumm.txt", "2:" + eSCM.SubscriberEnquiryID.ToString());
                            xtension.CopyTo(eSe, eSeM);

                            string consumerAccountID = string.Empty, amount = string.Empty;

                            if (simRequest.AmountChanges != null && simRequest.AmountChanges.Count() > 0)
                            {
                                foreach (AmountChange ochange in simRequest.AmountChanges)
                                {
                                    consumerAccountID = consumerAccountID + ochange.ConsumerAccountID;
                                    amount = amount + ochange.NewAmount;
                                }
                            }


                            eSeM.ProductID = simRequest.ProductID;
                            eSeM.EnquiryStatus = "P";
                            eSeM.SubscriberEnquiryDate = DateTime.Now;
                            eSeM.CreatedOnDate = DateTime.Now;
                            eSeM.ExtraIntInput1 = eSe.SubscriberEnquiryID;
                            eSeM.BusBusinessName = consumerAccountID;
                            eSeM.BusRegistrationNo = amount;



                            eSeM.SubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSeM);


                            eSCM.SubscriberEnquiryID = eSeM.SubscriberEnquiryID;
                            eSCM.ProductID = eSeM.ProductID;
                            eSCM.EnquiryResult = "M";
                            eSCM.XMLData = string.Empty;
                            eSCM.DetailsViewedYN = false;
                            eSCM.Billable = false;
                            eSCM.CreatedOnDate = DateTime.Now;
                            eSCM.ExtraIntOutput1 = eSe.SubscriberEnquiryID;
                            eSCM.ExtraIntOutput2 = eSC.SubscriberEnquiryResultID;


                            eSCM.SubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSCM);

                            eSeM = dSe.GetSubscriberEnquiryObject(con, eSeM.SubscriberEnquiryID);

                            eoConsumerCreditGrantorWseManager.ConsumerID = eSCM.KeyID;
                            eoConsumerCreditGrantorWseManager.subscriberID = eSe.SubscriberID;
                            eoConsumerCreditGrantorWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;
                            eoConsumerCreditGrantorWseManager.DataSegments = eSeM.ExtraVarInput2;
                            eoConsumerCreditGrantorWseManager.EnquiryID = eSeM.SubscriberEnquiryID;
                            eoConsumerCreditGrantorWseManager.EnquiryResultID = eSCM.SubscriberEnquiryResultID;
                            eoConsumerCreditGrantorWseManager.ProductID = spr.ProductID;
                            eoConsumerCreditGrantorWseManager.Username = eSeM.CreatedByUser;

                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.GetConsumerCreditScoreReport(eoConsumerCreditGrantorWseManager, simRequest);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSeM = dSe.ErrorGetSubscriberEnquiryObject(con, eSeM.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSeM.ExtraVarInput1);
                                rp = ra.GetConsumerCreditScoreReport(eoConsumerCreditGrantorWseManager, simRequest);
                            }

                            rXml = rp.ResponseData;

                            System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);


                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                            {

                                eSCM.DetailsViewedYN = false;
                                eSCM.Billable = false;
                                eSCM.SubscriberEnquiryID = eSCM.SubscriberEnquiryID;
                                eSCM.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                                eSCM.CreatedByUser = eSeM.CreatedByUser;
                                eSCM.CreatedOnDate = DateTime.Now;

                                dSC.UpdateSubscriberEnquiryResult(con, eSCM);
                                rp.EnquiryResultID = eSCM.SubscriberEnquiryResultID;
                                rp.EnquiryID = eSCM.SubscriberEnquiryID;

                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSCM.EnquiryResult = "E";
                                eSeM.ErrorDescription = rp.ResponseData.ToString();
                                dSe.UpdateSubscriberEnquiryError(con, eSeM);

                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                ds.ReadXml(xmlSR);
                                if (ds.Tables.Contains("ConsumerScoring"))
                                {

                                    Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                    Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, eSeM.ProductID);

                                    DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                    dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                    dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                    dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                    dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                    dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                    dtSubscriberInput.Columns.Add("EnquiryReason", typeof(String));
                                    DataRow drSubscriberInput;
                                    drSubscriberInput = dtSubscriberInput.NewRow();

                                    drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                    drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                    drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                    drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                    drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();
                                    drSubscriberInput["EnquiryReason"] = eSe.EnquiryReason.Trim();

                                    dtSubscriberInput.Rows.Add(drSubscriberInput);
                                    ds.Tables.Add(dtSubscriberInput);

                                    rXml = ds.GetXml();
                                    rp.ResponseData = rXml;

                                    foreach (DataRow r in ds.Tables["ConsumerScoring"].Rows)
                                    {
                                        Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                        Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                        eSCM.DetailsViewedDate = DateTime.Now;
                                        eSCM.DetailsViewedYN = true;
                                        if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSCM.VoucherCode)))
                                        {
                                            eSCM.Billable = false;
                                        }
                                        else
                                        {
                                            eSCM.Billable = true;
                                        }
                                        eSCM.ChangedByUser = eSeM.CreatedByUser;
                                        eSCM.ChangedOnDate = DateTime.Now;
                                        eSCM.SearchOutput = "";

                                        eSCM.KeyID = rp.ResponseKey;
                                        eSCM.KeyType = rp.ResponseKeyType;
                                        eSCM.SubscriberEnquiryID = eSeM.SubscriberEnquiryID;

                                        eSCM.BillingTypeID = spr.BillingTypeID;
                                        eSCM.BillingPrice = spr.UnitPrice;

                                        eSCM.XMLData = rXml.Replace("'", "''");
                                        eSCM.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                        eSCM.ProductID = eSe.ProductID;

                                        dSC.UpdateSubscriberEnquiryResult(con, eSCM);

                                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                        // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                        if (sub.PayAsYouGo == 1)
                                        {
                                            dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSeM.SubscriberID, Totalcost);
                                        }
                                        if (!(string.IsNullOrEmpty(eSCM.VoucherCode)))
                                        {
                                            dSV.UpdateSubscriberVoucher(AdminConnection, eSCM.VoucherCode, eSeM.CreatedByUser);
                                        }

                                        if (String.IsNullOrEmpty(eSCM.VoucherCode))
                                        {
                                            //Log FootPrint

                                            eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                            eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                            eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                            eSubscriberFootPrint.KeyID = eSC.KeyID;
                                            eSubscriberFootPrint.KeyType = eSC.KeyType;
                                            eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                            eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                            if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                            {
                                                eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                            }
                                            eSubscriberFootPrint.SubscriberEnquiryDate = eSCM.CreatedOnDate;
                                            eSubscriberFootPrint.SubscriberEnquiryID = eSCM.SubscriberEnquiryID;
                                            eSubscriberFootPrint.SubscriberEnquiryResultID = eSCM.SubscriberEnquiryResultID;
                                            eSubscriberFootPrint.SubscriberID = eSeM.SubscriberID;
                                            eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                            eSubscriberFootPrint.SubscriberUser = eSeM.SystemUser;
                                            eSubscriberFootPrint.CreatedByUser = sys.Username;
                                            eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                            eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                            eSubscriberFootPrint.ProductID = eSeM.ProductID;
                                            eSubscriberFootPrint.FootprintActive = spr.FootprintActive;


                                            int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                        }
                                        //Log FootPrint
                                    }
                                }
                            }

                        }
                        else
                        {
                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        }
                    }
                }

                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

            }
            catch (Exception oException)
            {
                if (eSeM.SubscriberEnquiryID == 0)
                {
                    eSe.EnquiryResult = "E";
                    eSe.ErrorDescription = oException.Message;

                    eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                }
                else
                {
                    eSeM.EnquiryResult = "E";
                    eSeM.ErrorDescription = oException.Message;

                    eSeM.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSeM);
                }
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;


                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitConsumerTraceVettingV(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, XDSPortalLibrary.Entity.PrincipalInfo[] oPrincipalInfo, string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode)
        {
            string strAdminCon = AdminConnection.ConnectionString;
            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            string sGender = "";
            string rXml = "";
            int MatchResult = 0;
            int intSubscriberEnquiryResultID = 0;
            DataSet ds = new DataSet();
            Boolean boolSubmitEnquiry = true;
            string strValidationStatus = "";

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);


            try
            {
                // Check if this subscriber is authorized to use the current product

                if (spr.ReportID > 0)
                {
                    // Check the PayAsYouGo settings for the subscriber
                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {

                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;


                        eSe.ProductID = intProductId;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = "";
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";


                        DataTable dtIdnos = new DataTable();
                        DataColumn dcIDType = new DataColumn("IDType");
                        DataColumn dcIDNo = new DataColumn("IDNo");
                        DataColumn dcSurname = new DataColumn("Surname");
                        DataColumn dcFirstName = new DataColumn("FirstName");

                        dtIdnos.Columns.Add(dcIDType);
                        dtIdnos.Columns.Add(dcIDNo);
                        dtIdnos.Columns.Add(dcSurname);
                        dtIdnos.Columns.Add(dcFirstName);

                        //Generate serach input string

                        foreach (XDSPortalLibrary.Entity.PrincipalInfo oInfo in oPrincipalInfo)
                        {

                            if (oInfo != null && !string.IsNullOrEmpty(oInfo.IdentityNumber))
                            {
                                eSe.SearchInput = eSe.SearchInput + " | " + oInfo.IdentityNumber;

                                DataRow drIDnos = dtIdnos.NewRow();
                                drIDnos["IDType"] = oInfo.IDType;
                                drIDnos["IDNo"] = oInfo.IdentityNumber;
                                drIDnos["Surname"] = oInfo.Surname;
                                drIDnos["FirstName"] = oInfo.FirstName;

                                dtIdnos.Rows.Add(drIDnos);

                            }


                        }


                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table

                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);
                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoConsumerTraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoConsumerTraceWseManager.ConfirmationChkBox = bConfirmationChkBox;
                        eoConsumerTraceWseManager.ExternalReference = strExtRef;
                        eoConsumerTraceWseManager.IDnosList = dtIdnos;
                        eoConsumerTraceWseManager.ProductID = intProductId;
                        eoConsumerTraceWseManager.subscriberID = intSubscriberID;

                        eoConsumerTraceWseManager.BonusCheck = bBonusChecking;
                        eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;


                        // Submit data for matching
                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.ConsumerMatchMultiple(eoConsumerTraceWseManager);

                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.ConsumerMatchMultiple(eoConsumerTraceWseManager);
                        }

                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);


                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            eSC.SearchOutput = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.DetailsViewedYN = false;
                            eSC.Billable = false;
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;
                            eSC.KeyID = rp.ResponseKey;
                            eSC.KeyType = rp.ResponseKeyType;


                            rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                            rp.EnquiryID = eSC.SubscriberEnquiryID;

                            foreach (DataRow r in dtIdnos.Rows)
                            {

                                if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                {
                                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple;

                                    ds = new DataSet("ListOfConsumers");

                                    DataTable dt = new DataTable("ConsumerDetails");


                                    ds.Tables.Add(dt);


                                    ds.Tables[0].Columns.Add("EnquiryID");
                                    ds.Tables[0].Columns.Add("EnquiryResultID");
                                    ds.Tables[0].Columns.Add("Reference");
                                    ds.Tables[0].Columns.Add("IDType");
                                    ds.Tables[0].Columns.Add("IDNo");
                                    ds.Tables[0].Columns.Add("Surname");
                                    ds.Tables[0].Columns.Add("FirstName");

                                    DataRow dr = ds.Tables[0].NewRow();

                                    dr["EnquiryID"] = rp.EnquiryID;
                                    dr["EnquiryResultID"] = rp.EnquiryResultID;
                                    dr["Reference"] = eSC.Reference;
                                    dr["IDType"] = r["IDType"].ToString();
                                    dr["IDNo"] = r["IDNo"].ToString();
                                    dr["Surname"] = r["Surname"].ToString();
                                    dr["FirstName"] = r["FirstName"].ToString();


                                    ds.Tables[0].Rows.Add(dr);

                                    rp.ResponseData = ds.GetXml();

                                }
                            }

                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);
                        }

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                        {
                            ds.ReadXml(xmlSR);
                            ds.Tables[0].Columns.Add("EnquiryID");
                            ds.Tables[0].Columns.Add("EnquiryResultID");
                            ds.Tables[0].Columns.Add("Reference");

                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                eSC.SearchOutput = "";
                                eSC.IDNo = "";
                                eSC.PassportNo = "";
                                eSC.Surname = "";
                                eSC.FirstName = "";
                                eSC.BirthDate = DateTime.Parse("1900/01/01");
                                eSC.Gender = "";

                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {

                                    //if (r["ConsumerID"].ToString() == "0" && r["IDType"].ToString() == "1")
                                    //{
                                    //    XDSPortalLibrary.Entity_Layer.ConsumerIDVerificationTrace eoHATraceWseManager = new XDSPortalLibrary.Entity_Layer.ConsumerIDVerificationTrace(); ;

                                    //    eoHATraceWseManager.DataSegments = eSe.ExtraVarInput2;
                                    //    eoHATraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                                    //    eoHATraceWseManager.ConfirmationChkBox = bConfirmationChkBox;
                                    //    eoHATraceWseManager.ExternalReference = strExtRef;
                                    //    eoHATraceWseManager.Firstname = r["FirstName"].ToString();
                                    //    eoHATraceWseManager.IDno = r["IDno"].ToString();
                                    //    eoHATraceWseManager.ProductID = intProductId;
                                    //    eoHATraceWseManager.subscriberID = intSubscriberID;
                                    //    eoHATraceWseManager.Surname = r["Surname"].ToString();
                                    //    eoHATraceWseManager.BonusCheck = bBonusChecking;
                                    //    eoHATraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;



                                    //    ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                    //    rp = ra.ConsumerIDVerificationMatch(eoHATraceWseManager);


                                    //    if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                                    //    {
                                    //        eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                    //        ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                    //        rp = ra.ConsumerIDVerificationMatch(eoHATraceWseManager);
                                    //    }
                                    //}



                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + r["IDNo"].ToString() + "-" + r["IDType"].ToString();
                                        }
                                    }

                                    if (r.Table.Columns.Contains("ConsumerID"))
                                    {
                                        if (!string.IsNullOrEmpty(r["ConsumerID"].ToString()) && r["ConsumerID"].ToString() != "0")
                                        {
                                            eSC.BusBusinessName = eSC.BusBusinessName + " | " + r["ConsumerID"].ToString();
                                        }
                                    }

                                }

                                if (eSC.SearchOutput.Length > 0)
                                {
                                    eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                }

                                eSC.DetailsViewedYN = false;
                                eSC.Billable = false;
                                eSC.CreatedByUser = eSe.CreatedByUser;
                                eSC.CreatedOnDate = DateTime.Now;
                                eSC.KeyID = 0;
                                eSC.KeyType = "C";

                                eSC.BillingTypeID = spr.BillingTypeID;
                                eSC.BillingPrice = spr.UnitPrice;

                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                eSC.ExtraVarOutput1 = "";
                                eSC.ProductID = intProductId;
                                eSC.VoucherCode = strVoucherCode;



                                // Insert the match results to SubscriberEnquiryResult Table
                                intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                rp.EnquiryID = eSC.SubscriberEnquiryID;
                                rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                eSC.XMLBonus = null;
                                eSC.BonusIncluded = false;

                                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;
                                }

                                rp.ResponseData = ds.GetXml();
                            }

                        }


                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }
            catch (Exception oException)
            {

                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);

                MatchResult = eSe.SubscriberEnquiryID;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitConsumerTraceVettingW(SqlConnection con, SqlConnection AdminConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, bool bBonusChecking, string strVoucherCode, VettingW.WResponse wResponse, out VettingW.WResponse wResponseo)
        {
            wResponseo = wResponse;
            string strAdminCon = AdminConnection.ConnectionString;
            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            string rXml = "";
            double Totalcost = 0;

            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();


            moCustomVetting.ConnectionString = AdminConnection;
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML
                // use this XML data for generating the Report

                if (spr.ProductID > 0)
                {

                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {

                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }

                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {
                            eoConsumerTraceWseManager.IDno = eSe.IDNo;
                            eoConsumerTraceWseManager.ConsumerID = eSC.KeyID;
                            eoConsumerTraceWseManager.Surname = eSe.Surname;
                            eoConsumerTraceWseManager.FirstName = eSe.FirstName;
                            eoConsumerTraceWseManager.GrossMonthlyIncome = (double)eSe.NetMonthlyIncomeAmt;
                            eoConsumerTraceWseManager.ReferenceNo = eSC.Reference;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                            eoConsumerTraceWseManager.subscriberID = sys.SubscriberID;
                            eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;


                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.GetIgniteReport(eoConsumerTraceWseManager);


                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.GetIgniteReport(eoConsumerTraceWseManager);
                            }

                            rXml = rp.ResponseData;

                            System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSC.EnquiryResult = "E";
                                eSe.ErrorDescription = rp.ResponseData.ToString();
                                dSe.UpdateSubscriberEnquiryError(con, eSe);
                                wResponseo.EnquiryHeader.EnquiryStatus = VettingW.Header.Estatus.FAILURE;
                                wResponseo.EnquiryHeader.Error = "Error while processing the request, please contact XDS";
                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                ds.ReadXml(xmlSR);
                                if (ds.Tables.Contains("ConsumerScoring"))
                                {

                                    Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                    Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, eSe.ProductID);

                                    foreach (DataRow r in ds.Tables["ConsumerScoring"].Rows)
                                    {
                                        Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                        Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                        eSC.DetailsViewedDate = DateTime.Now;
                                        eSC.DetailsViewedYN = true;
                                        if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                        {
                                            eSC.Billable = false;
                                        }
                                        else
                                        {
                                            eSC.Billable = true;
                                        }
                                        eSC.ChangedByUser = eSe.CreatedByUser;
                                        eSC.ChangedOnDate = DateTime.Now;
                                        eSC.SearchOutput = "";


                                        if (r.Table.Columns.Contains("FinalScore"))
                                        {
                                            if (!string.IsNullOrEmpty(r["FinalScore"].ToString()))
                                            {
                                                eSC.PassportNo = r["FinalScore"].ToString();
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;

                                                wResponseo.ApplicantInformation.CreditScore = float.Parse(r["FinalScore"].ToString());
                                                wResponse.EnquiryHeader.XDSReference = eSC.KeyType + eSC.SubscriberEnquiryID.ToString() + "-" + eSC.KeyID;
                                                //File.AppendAllText(@"C:\Log\vettingN.txt", eSC.KeyType + eSC.SubscriberEnquiryID.ToString() + "-" + eSC.KeyID + "::::2\n");
                                            }
                                        }

                                        if (eSC.SearchOutput.Length > 0)
                                        {
                                            eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                        }

                                        eSC.KeyID = rp.ResponseKey;
                                        eSC.KeyType = rp.ResponseKeyType;
                                        eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                        eSC.BillingTypeID = spr.BillingTypeID;
                                        eSC.BillingPrice = spr.UnitPrice;

                                        eSC.XMLData = rXml.Replace("'", "''");
                                        eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                        eSC.ProductID = eSe.ProductID;

                                        dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;



                                        // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                        if (sub.PayAsYouGo == 1)
                                        {
                                            dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                        }
                                        if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                        {
                                            dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                        }

                                        if (String.IsNullOrEmpty(eSC.VoucherCode))
                                        {
                                            //Log FootPrint

                                            eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                            eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                            eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                            eSubscriberFootPrint.KeyID = eSC.KeyID;
                                            eSubscriberFootPrint.KeyType = eSC.KeyType;
                                            eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                            eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                            if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                            {
                                                eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                            }
                                            eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                            eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                            eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                            eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                            eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                            eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                            eSubscriberFootPrint.CreatedByUser = sys.Username;
                                            eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                            eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                            eSubscriberFootPrint.ProductID = eSe.ProductID;
                                            eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                            int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                        }
                                    }
                                }

                                rXml = rp.ResponseData;

                            }
                        }
                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.

                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        }

                    }
                    else
                    {
                        // When User want to Re-Open a report

                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = eSC.XMLData;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }


                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

                wResponseo.EnquiryHeader.EnquiryStatus = VettingW.Header.Estatus.FAILURE;
                wResponseo.EnquiryHeader.Error = "Error while processing the request, please contact XDS";
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitConsumerTraceW(SqlConnection con, SqlConnection AdminConnection, int intSystemUserID, int intProductId, string strFirstname, string strSurname, string strCellNumber, string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode, out VettingW.WResponse wResponse)
        {
            string strAdminCon = AdminConnection.ConnectionString;
            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            wResponse = new VettingW.WResponse();
            wResponse.EnquiryHeader = new VettingW.Header();
            wResponse.ApplicantInformation = new VettingW.Information();

            bBonusChecking = false;
            string rXml = "";
            int MatchResult = 0;
            int intSubscriberEnquiryResultID = 0;
            DataSet ds = new DataSet();
            Boolean boolSubmitEnquiry = true;
            string strValidationStatus = "";

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, sys.SubscriberID);



            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, sys.SubscriberID, intProductId);


            try
            {
                // Check if this subscriber is authorized to use the current product

                if (spr.ReportID > 0)
                {
                    // Check the PayAsYouGo settings for the subscriber
                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, sys.SubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {

                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = sys.SubscriberID;

                        eSe.FirstName = strFirstname;
                        eSe.ProductID = intProductId;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = sub.SubscriberName;
                        eSe.SubscriberReference = strExtRef;
                        eSe.Surname = strSurname;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";
                        eSe.TelephoneNo = strCellNumber;


                        //Generate serach input string

                        if (!string.IsNullOrEmpty(eSe.TelephoneNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.TelephoneNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.Surname))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.Surname;
                        }
                        if (!string.IsNullOrEmpty(eSe.FirstName))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.FirstName;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }



                        // Log the Current Enquiry to SubscriberEnquiry Table

                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);
                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoConsumerTraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoConsumerTraceWseManager.ConfirmationChkBox = bConfirmationChkBox;
                        eoConsumerTraceWseManager.ExternalReference = strExtRef;
                        eoConsumerTraceWseManager.FirstName = strFirstname;
                        eoConsumerTraceWseManager.ProductID = 2;
                        eoConsumerTraceWseManager.subscriberID = sys.SubscriberID;
                        eoConsumerTraceWseManager.Surname = strSurname;
                        eoConsumerTraceWseManager.BonusCheck = bBonusChecking;
                        eoConsumerTraceWseManager.Cellphonenumber = strCellNumber;

                        rp.EnquiryID = intSubscriberEnquiryID;
                        rp.ResponseReferenceNo = "C" + rp.EnquiryID.ToString() + "-" + rp.ResponseKey;

                        rp = ValidateConnectCustomVettingW(eoConsumerTraceWseManager);

                        if (rp.ResponseStatus != Response.ResponseStatusEnum.Error)
                        {
                            // Submit data for matching
                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.ConsumerMatchCustomW(eoConsumerTraceWseManager);

                            //If error raise one last call with a different connection
                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.ConsumerMatchCustomW(eoConsumerTraceWseManager);
                            }


                            // Get Response data

                            rXml = rp.ResponseData;

                            System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                            {
                                eSC.SearchOutput = "";
                                eSC.IDNo = "";
                                eSC.PassportNo = "";
                                eSC.Surname = "";
                                eSC.FirstName = "";
                                eSC.BirthDate = DateTime.Parse("1900/01/01");
                                eSC.Gender = "";
                                eSC.DetailsViewedYN = false;
                                eSC.Billable = false;
                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                                eSC.CreatedByUser = eSe.CreatedByUser;
                                eSC.CreatedOnDate = DateTime.Now;

                                rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                rp.EnquiryID = eSC.SubscriberEnquiryID;

                                wResponse.EnquiryHeader.EnquiryStatus = VettingW.Header.Estatus.SUCCESS;
                                wResponse.EnquiryHeader.Error = rp.ResponseData;
                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSC.EnquiryResult = "E";
                                eSe.ErrorDescription = rp.ResponseData.ToString();
                                dSe.UpdateSubscriberEnquiryError(con, eSe);

                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                ds.ReadXml(xmlSR);
                                if (ds.Tables.Contains("ConsumerDetails"))
                                {
                                    Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                    Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, intProductId);

                                    DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                    dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                    dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                    dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                    dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                    dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                    DataRow drSubscriberInput;
                                    drSubscriberInput = dtSubscriberInput.NewRow();

                                    drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                    drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                    drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                    drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                    drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();

                                    dtSubscriberInput.Rows.Add(drSubscriberInput);

                                    ds.Tables.Add(dtSubscriberInput);
                                    rXml = ds.GetXml();
                                    rp.ResponseData = rXml;

                                    foreach (DataRow r in ds.Tables["ConsumerDetail"].Rows)
                                    {
                                        eSC.SearchOutput = "";
                                        eSC.IDNo = "";
                                        eSC.PassportNo = "";
                                        eSC.Surname = "";
                                        eSC.FirstName = "";
                                        eSC.BirthDate = DateTime.Parse("1900/01/01");
                                        eSC.Gender = "";
                                        eSC.DetailsViewedDate = DateTime.Now;
                                        eSC.DetailsViewedYN = true;
                                        eSC.Billable = true;
                                        eSC.CreatedByUser = eSe.CreatedByUser;
                                        eSC.CreatedOnDate = DateTime.Now;
                                        eSC.SearchOutput = "";

                                        if (r.Table.Columns.Contains("IDNo"))
                                        {
                                            if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                            {
                                                eSC.IDNo = r["IDNo"].ToString();
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                            }
                                        }
                                        if (r.Table.Columns.Contains("PassportNo"))
                                        {
                                            if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                            {
                                                eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                            }
                                        }
                                        if (r.Table.Columns.Contains("Surname"))
                                        {
                                            if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                            {
                                                eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                            }
                                        }

                                        if (r.Table.Columns.Contains("FirstName"))
                                        {
                                            if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                            {
                                                eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                            }
                                        }

                                        if (eSC.SearchOutput.Length > 0)
                                        {
                                            eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                        }

                                        eSC.KeyID = rp.ResponseKey;
                                        eSC.KeyType = rp.ResponseKeyType;
                                        eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                        eSC.BillingTypeID = spr.BillingTypeID;
                                        eSC.BillingPrice = spr.UnitPrice;

                                        eSC.XMLData = rXml.Replace("'", "''");
                                        eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                        eSC.ExtraVarOutput1 = rp.TmpReference;

                                        rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                        // Change the PayAsYouGoEnquiryLimit  after viewing the report

                                        dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, sys.SubscriberID, spr.UnitPrice);
                                    }
                                }
                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                            {
                                ds.ReadXml(xmlSR);
                                ds.Tables[0].Columns.Add("EnquiryID");
                                ds.Tables[0].Columns.Add("EnquiryResultID");
                                ds.Tables[0].Columns.Add("Reference");

                                if (ds.Tables.Contains("ConsumerDetails"))
                                {
                                    foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                    {
                                        eSC.SearchOutput = "";
                                        eSC.IDNo = "";
                                        eSC.PassportNo = "";
                                        eSC.Surname = "";
                                        eSC.FirstName = "";
                                        eSC.BirthDate = DateTime.Parse("1900/01/01");
                                        eSC.Gender = "";

                                        if (r.Table.Columns.Contains("IDNo"))
                                        {
                                            if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                            {
                                                eSC.IDNo = r["IDNo"].ToString();
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                            }
                                        }
                                        if (r.Table.Columns.Contains("PassportNo"))
                                        {
                                            if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                            {
                                                eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                            }
                                        }
                                        if (r.Table.Columns.Contains("Surname"))
                                        {
                                            if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                            {
                                                eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                            }
                                        }

                                        if (r.Table.Columns.Contains("FirstName"))
                                        {
                                            if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                            {
                                                eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                            }
                                        }


                                        if (eSC.SearchOutput.Length > 0)
                                        {
                                            eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                        }
                                        eSC.DetailsViewedYN = false;
                                        eSC.Billable = false;
                                        eSC.CreatedByUser = eSe.CreatedByUser;
                                        eSC.CreatedOnDate = DateTime.Now;
                                        eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                        eSC.KeyType = rp.ResponseKeyType;

                                        eSC.BillingTypeID = spr.BillingTypeID;
                                        eSC.BillingPrice = spr.UnitPrice;

                                        eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                        eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                        eSC.ExtraVarOutput1 = "";
                                        eSC.ProductID = intProductId;
                                        eSC.VoucherCode = strVoucherCode;

                                        string BonusXML = string.Empty;
                                        BonusXML = r["BonusXML"].ToString();
                                        if (BonusXML != string.Empty)
                                        {
                                            DataSet dsBonus = new DataSet();
                                            System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                            dsBonus.ReadXml(BonusxmlSR);
                                            if (dsBonus.Tables.Contains("Segments"))
                                            {
                                                eSC.BonusIncluded = true;
                                                eSC.XMLBonus = BonusXML.Replace("'", "''");


                                                intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);

                                                rp.EnquiryResultID = intSubscriberEnquiryResultID;
                                                foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                                {
                                                    eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                                    eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                    eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                    eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                    eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                    eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                    eSB.Billable = false;
                                                    eSB.CreatedByUser = eSe.CreatedByUser;
                                                    eSB.CreatedOnDate = DateTime.Now;

                                                    // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                    dSB.InsertSubscriberEnquiryResultBonus(con, eSB);
                                                }
                                            }
                                            dsBonus.Dispose();
                                        }
                                        else
                                        {
                                            // Insert the match results to SubscriberEnquiryResult Table
                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                            rp.EnquiryID = eSC.SubscriberEnquiryID;
                                        }
                                        eSC.XMLBonus = null;
                                        eSC.BonusIncluded = false;

                                        eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                        ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                        ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                        ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                        rp.ResponseData = ds.GetXml();
                                        rp.ResponseReferenceNo = eSC.KeyType + rp.EnquiryID.ToString() + "-" + eSC.KeyID;

                                        wResponse.ApplicantInformation.IDNumber = r["IDNo"].ToString();
                                        wResponse.ApplicantInformation.FirstName = r["FirstName"].ToString();
                                        wResponse.ApplicantInformation.Surname = r["Surname"].ToString();

                                        string birthdate = r["BirthDate"].ToString();
                                        DateTime dtBdate = new DateTime();

                                        if (DateTime.TryParse(birthdate, out dtBdate))
                                        {
                                            wResponse.ApplicantInformation.BirthDate = dtBdate;
                                        }
                                        wResponse.ApplicantInformation.Title = r["TitleCode"].ToString();
                                        wResponse.EnquiryHeader.ClientReference = strExtRef;

                                        // File.AppendAllText(@"C:\Log\vettingN.txt", rp.ResponseReferenceNo+"::::1\n");
                                        wResponse.EnquiryHeader.XDSReference = rp.ResponseReferenceNo;

                                    }
                                }

                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                            {
                                ds.ReadXml(xmlSR);
                                ds.Tables[0].Columns.Add("EnquiryID");
                                ds.Tables[0].Columns.Add("EnquiryResultID");
                                ds.Tables[0].Columns.Add("Reference");

                                if (ds.Tables.Contains("ConsumerDetails"))
                                {
                                    foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                    {
                                        eSC.SearchOutput = "";
                                        eSC.IDNo = "";
                                        eSC.PassportNo = "";
                                        eSC.Surname = "";
                                        eSC.FirstName = "";
                                        eSC.BirthDate = DateTime.Parse("1900/01/01");
                                        eSC.Gender = "";

                                        if (r.Table.Columns.Contains("IDNo"))
                                        {
                                            if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                            {
                                                eSC.IDNo = r["IDNo"].ToString();
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                            }
                                        }
                                        if (r.Table.Columns.Contains("PassportNo"))
                                        {
                                            if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                            {
                                                eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                            }
                                        }
                                        if (r.Table.Columns.Contains("Surname"))
                                        {
                                            if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                            {
                                                eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                            }
                                        }

                                        if (r.Table.Columns.Contains("FirstName"))
                                        {
                                            if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                            {
                                                eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                            }
                                        }

                                        if (eSC.SearchOutput.Length > 0)
                                        {
                                            eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                        }

                                        eSC.DetailsViewedYN = false;
                                        eSC.Billable = false;
                                        eSC.CreatedByUser = eSe.CreatedByUser;
                                        eSC.CreatedOnDate = DateTime.Now;
                                        eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                        eSC.KeyType = rp.ResponseKeyType;

                                        eSC.BillingTypeID = spr.BillingTypeID;
                                        eSC.BillingPrice = spr.UnitPrice;

                                        eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                        eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();
                                        eSC.ExtraVarOutput1 = "";
                                        eSC.ProductID = intProductId;
                                        eSC.VoucherCode = strVoucherCode;

                                        string BonusXML = string.Empty;
                                        BonusXML = r["BonusXML"].ToString();
                                        if (BonusXML != string.Empty)
                                        {
                                            DataSet dsBonus = new DataSet();
                                            System.IO.StringReader BonusxmlSR = new System.IO.StringReader(BonusXML);
                                            dsBonus.ReadXml(BonusxmlSR);
                                            if (dsBonus.Tables.Contains("Segments"))
                                            {


                                                eSC.BonusIncluded = true;
                                                eSC.XMLBonus = BonusXML.Replace("'", "''");

                                                intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                                rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                                foreach (DataRow dr in dsBonus.Tables[0].Rows)
                                                {
                                                    eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;

                                                    eSB.DataSegmentID = int.Parse(dr["DataSegmentID"].ToString());
                                                    eSB.DataSegmentName = dr["DataSegmentName"].ToString().Replace("'", "''");
                                                    eSB.DataSegmentDisplayText = dr["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                                    eSB.BonusViewed = bool.Parse(dr["BonusViewed"].ToString());
                                                    eSB.BillingPrice = Convert.ToDouble(dr["BonusPrice"].ToString());
                                                    eSB.Billable = false;
                                                    eSB.CreatedByUser = eSe.CreatedByUser;
                                                    eSB.CreatedOnDate = DateTime.Now;

                                                    // Insert the Bonus Segments to SubscriberEnquiryResultBonus Table
                                                    dSB.InsertSubscriberEnquiryResultBonus(con, eSB);

                                                }

                                            }
                                            dsBonus.Dispose();
                                        }
                                        else
                                        {
                                            // Insert the match results to SubscriberEnquiryResult Table
                                            intSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                            rp.EnquiryID = eSC.SubscriberEnquiryID;
                                        }
                                        eSC.XMLBonus = null;
                                        eSC.BonusIncluded = false;

                                        eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                                        ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                        ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = intSubscriberEnquiryResultID;
                                        ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["Reference"] = eSC.Reference;

                                        rp.ResponseData = ds.GetXml();
                                    }
                                }
                            }
                        }

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }
            catch (Exception oException)
            {

                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);

                MatchResult = eSe.SubscriberEnquiryID;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response ValidateConnectCustomVettingW(XDSPortalLibrary.Entity_Layer.ConsumerTrace objInput)
        {

            XDSPortalLibrary.Entity_Layer.Response oResponse = new XDSPortalLibrary.Entity_Layer.Response();

            oResponse.ResponseData = "Please enter valid ";

            if (String.IsNullOrEmpty(objInput.FirstName) ||
                (!String.IsNullOrEmpty(objInput.FirstName) && objInput.FirstName.Length <= 3))
            {
                oResponse.ResponseStatus = Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = oResponse.ResponseData + "Firstname,";
            }
            if (String.IsNullOrEmpty(objInput.Surname) ||
                (!String.IsNullOrEmpty(objInput.Surname) && objInput.Surname.Length <= 3))
            {
                oResponse.ResponseStatus = Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = oResponse.ResponseData + "Surname,";
            }
            if (String.IsNullOrEmpty(objInput.Cellphonenumber) ||
                (!String.IsNullOrEmpty(objInput.Cellphonenumber) && objInput.Cellphonenumber.Length < 10))
            {

                oResponse.ResponseStatus = Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = oResponse.ResponseData + "Cell Phone number,";
            }

            if (oResponse.ResponseStatus == Response.ResponseStatusEnum.Error)
            {
                oResponse.ResponseData = oResponse.ResponseData.Trim(new char[] { ',' }) + ".";
            }
            else
            {
                oResponse.ResponseData = string.Empty;
            }

            return oResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response ValidateConnectCustomVettingY(XDSPortalLibrary.Entity_Layer.Yrequest objInput)
        {
            int i = 0;
            float f = 0;
            bool b = false;
            XDSPortalLibrary.Entity_Layer.Response oResponse = new XDSPortalLibrary.Entity_Layer.Response();

            if (string.IsNullOrEmpty(objInput.pUsername))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Username is Mandatory";
            }
            else if (string.IsNullOrEmpty(objInput.pPassword))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Password is Mandatory";
            }
            else if (string.IsNullOrEmpty(objInput.pIdNumber) && string.IsNullOrEmpty(objInput.pPassportNo))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "IDNumber is Mandatory";
            }
            else if (string.IsNullOrEmpty(objInput.pLastName))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "LastName is Mandatory";
            }
            else if (string.IsNullOrEmpty(objInput.pPDFIndicator))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "PDFIndicator is Mandatory,Valid values are Y or N";
            }


            return oResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response ValidateConnectCustomVettingZ(XDSPortalLibrary.Entity_Layer.Zrequest objInput)
        {
            int i = 0;
            float f = 0;
            bool b = false;
            XDSPortalLibrary.Entity_Layer.Response oResponse = new XDSPortalLibrary.Entity_Layer.Response();

            if (string.IsNullOrEmpty(objInput.Username))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Username is Mandatory";
            }
            else if (string.IsNullOrEmpty(objInput.Password))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Password is Mandatory";
            }
            else if (string.IsNullOrEmpty(objInput.IdNumber) && string.IsNullOrEmpty(objInput.PassportNo))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "IDNumber is Mandatory";
            }
            else if (string.IsNullOrEmpty(objInput.SurName))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "SurName is Mandatory";
            }

            return oResponse;
        }
    }

    public static class RegisterSOAP
    {
        /// <summary>
        /// Programatically registers a <see cref="SoapExtension"/> at runtime with the specified
        /// <see cref="SoapExtensionTypeElement.Priority"/> and <see cref="SoapExtensionTypeElement.Group"/> settings.
        /// </summary>
        /// <param name="type">The <see cref="Type"/> of the <see cref="SoapExtension"/> to register.</param>
        /// <param name="priority">
        /// A value that indicates the relative order in which this SOAP extension runs when multiple SOAP extensions are
        /// specified. Within each group the priority attribute distinguishes the overall relative priority of the SOAP
        /// extension. A lower priority number indicates a higher priority for the SOAP extension. The lowest possible
        /// value for the priority attribute is 1.
        /// </param>
        /// <param name="group">
        /// The relative priority group (e.g. Low or High) in which this SOAP extension runs when multiple SOAP extensions
        /// are configured to run.
        /// </param>
        [ReflectionPermission(SecurityAction.Demand, Unrestricted = true)]
        public static void RegisterSoapExtension(Type type, int priority, PriorityGroup group)
        {
            if (!type.IsSubclassOf(typeof(SoapExtension)))
                throw new ArgumentException("Type must be derived from SoapException.", "type");
            if (priority < 1)
                throw new ArgumentOutOfRangeException("priority", priority, "Priority must be greater or equal to 1.");
            // get the current web services settings...
            WebServicesSection wss = WebServicesSection.Current;

            // set SoapExtensionTypes collection to read/write...
            FieldInfo readOnlyField = typeof(System.Configuration.ConfigurationElementCollection).GetField("bReadOnly", BindingFlags.NonPublic | BindingFlags.Instance);
            readOnlyField.SetValue(wss.SoapExtensionTypes, false);

            // inject SoapExtension...
            wss.SoapExtensionTypes.Add(new SoapExtensionTypeElement(type, priority, group));

            // set SoapExtensionTypes collection back to readonly and clear modified flags...
            MethodInfo resetModifiedMethod = typeof(System.Configuration.ConfigurationElement).GetMethod("ResetModified", BindingFlags.NonPublic | BindingFlags.Instance);
            resetModifiedMethod.Invoke(wss.SoapExtensionTypes, null);
            MethodInfo setReadOnlyMethod = typeof(System.Configuration.ConfigurationElement).GetMethod("SetReadOnly", BindingFlags.NonPublic | BindingFlags.Instance);
            setReadOnlyMethod.Invoke(wss.SoapExtensionTypes, null);
        }
    }

    public class myExtension : SoapExtension
    {
        public bool outgoing = true;
        public bool incoming = false;
        private Stream outputStream;
        public Stream oldStream;
        public Stream newStream;

        // The ChainStream, GetInitializers, Initialize, and ProcessMessage are required
        // for a Soap override.  ChainStream and ProcessMessage are what we need.
        /// <summary>
        /// Override.  Save old stream, create new one.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public override Stream ChainStream(Stream stream)
        {
            // save a copy of the stream, create a new one for manipulating.
            this.outputStream = stream;
            oldStream = stream;
            newStream = new MemoryStream();
            return newStream;
        }
        #region "overrides of no interest"
        public override object GetInitializer(LogicalMethodInfo methodInfo, SoapExtensionAttribute attribute)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public override object GetInitializer(Type serviceType)
        {
            return null;
        }
        public override void Initialize(object initializer)
        {
            return;
        }
        #endregion
        /// <summary>
        /// Return a string version of the XML
        /// </summary>
        /// <returns></returns>
        public string getXMLFromCache()
        {
            newStream.Position = 0; // start at the beginning!
            string strSOAPresponse = ExtractFromStream(newStream);
            return strSOAPresponse;
        }
        /// <summary>
        /// Transfer the text from the target stream from the
        /// current position.
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        private String ExtractFromStream(Stream target)
        {
            if (target != null)
                return (new StreamReader(target)).ReadToEnd();
            return "";
        }
        /// <summary>
        /// Override.  Process .AfterSerialize and .BeforeDeserialize
        /// to insert a non-standard SOAP header with username and
        /// password (currently hard-coded).
        /// </summary>
        /// <param name="message"></param>
        public override void ProcessMessage(System.Web.Services.Protocols.SoapMessage message)
        {
            StreamReader readStr;
            StreamWriter writeStr;
            string soapMsg1;
            XmlDocument xDoc = new XmlDocument();
            // a SOAP message has 4 stages.  We're interested in .AfterSerialize
            switch (message.Stage)
            {
                case SoapMessageStage.BeforeSerialize:
                    break;

                case SoapMessageStage.AfterSerialize:
                    {
                        // Get the SOAP body as a string, so we can manipulate...
                        String soapBodyString = getXMLFromCache();


                        // Strip off the old header stuff before the message body
                        // I'm not completely sure, but the soap:encodingStyle might be
                        // unique to the WebSphere environment.  Dunno.
                        //String BodString = "<soap:Body soap:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">";
                        //int pos1 = soapBodyString.IndexOf(BodString) + BodString.Length;
                        //int pos2 = soapBodyString.Length - pos1;
                        //soapBodyString = soapBodyString.Substring(pos1, pos2);
                        //soapBodyString = "<soap:Body>" + soapBodyString;

                        // Create the SOAP Message
                        // It's comprised of a <soap:Element> that's enclosed in <soap:Body>.
                        // Pack the XML document inside the <soap:Body> element
                        //String xmlVersionString = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
                        //String soapEnvelopeBeginString = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:tns=\"http://tempuri.org/\" xmlns:types=\"http://tempuri.org/encodedTypes\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">";
                        //String soapEnvHeaderString = "<soap:Header><Security><UsernameToken><Username>";
                        //String soapEnvHeaderString2 = "</Username><Password>";
                        //String soapEnvHeaderString3 = "</Password></UsernameToken></Security></soap:Header>";
                        UniqueId ounique = new UniqueId(Guid.NewGuid());
                        int index = soapBodyString.IndexOf("</soap:Header>");
                        string headerpart = soapBodyString.Substring(0, index);
                        string rest = soapBodyString.Substring(index);
                        string mid = " <add:MessageID xmlns:add=\"http://www.w3.org/2005/08/addressing\">" + ounique.ToString() + "</add:MessageID> ";
                        string finalxml = headerpart + mid + rest;
                        // File.AppendAllText(@"C:\Log\Response.txt", finalxml+"\n\n");
                        //string strmessage = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:v4=\"http://xmlns.vodacom.co.za/types/common/header/V4\" xmlns:v1=\"http://xmlns.vodacom.co.za/types/integration/ManageMarketingCampaigns/V1\">  <soapenv:Header>    <v4:RequestHeader>      <v4:MessageCreationTimestamp>2016-04-13T21:41:15+02:00</v4:MessageCreationTimestamp>      <v4:TransactionId>H145211975-29077283</v4:TransactionId>      <v4:TransactionCreationTimestamp>2016-04-12T16:38:57+02:00</v4:TransactionCreationTimestamp>      <v4:RequestApplicationId>H145211975-29077283</v4:RequestApplicationId>      <v4:UserId>KEABE MAUTLA</v4:UserId>    </v4:RequestHeader>    <add:MessageID xmlns:add=\"http://www.w3.org/2005/08/addressing\">uuid:790930c0-67c6-449f-b780-92459cc1f362</add:MessageID>  </soapenv:Header>  <soapenv:Body>    <v1:CreateVettingPreApprovals>      <v1:ReferenceNo>REFTEST001</v1:ReferenceNo>      <v1:EnquiryId>H145211975-29077283</v1:EnquiryId>      <v1:DealerId>WESTG</v1:DealerId>      <v1:PromotionDeal>V16AP31129</v1:PromotionDeal>      <v1:PromoCode>Pilot Canvas2016</v1:PromoCode>      <v1:FirstName>MARC</v1:FirstName>      <v1:Surname>JACOBS</v1:Surname>      <v1:IdNo>7505183200188</v1:IdNo>      <v1:BirthDate>1975-05-18+00:00</v1:BirthDate>      <v1:Income>17000</v1:Income>      <v1:ContactTelNo>0736453189</v1:ContactTelNo>      <v1:OtherContactTelNo>N/A</v1:OtherContactTelNo>      <v1:EmailAddress>KEABETSOE.MAUTLA@VCONTRACTOR.CO.ZA</v1:EmailAddress>      <v1:Decision>Passed</v1:Decision>      <v1:Portfolio>P0600</v1:Portfolio>      <v1:Score>681.0000</v1:Score>      <v1:CreatedByUser>KBTEST</v1:CreatedByUser>      <v1:CreatedOnDate>2016-04-12T16:38:57+02:00</v1:CreatedOnDate>      <v1:SourceSystem>XDS</v1:SourceSystem>    </v1:CreateVettingPreApprovals>  </soapenv:Body></soapenv:Envelope>";
                        Stream appOutputStream = new MemoryStream();
                        StreamWriter soapMessageWriter = new StreamWriter(appOutputStream);
                        //soapMessageWriter.Write(xmlVersionString);
                        //soapMessageWriter.Write(soapEnvelopeBeginString);
                        //// The heavy-handed part - forcing the right headers AND the uname/pw :)
                        //soapMessageWriter.Write(soapEnvHeaderString);
                        //soapMessageWriter.Write("usernamestring");
                        //soapMessageWriter.Write(soapEnvHeaderString2);
                        //soapMessageWriter.Write("passwordstring");
                        //soapMessageWriter.Write(soapEnvHeaderString3);
                        // End clubbing of baby seals
                        // Add the soapBodyString back in - it's got all the closing XML we need.
                        //soapBodyString = dammit;
                        soapMessageWriter.Write(finalxml);
                        // write it all out.
                        soapMessageWriter.Flush();
                        appOutputStream.Flush();
                        appOutputStream.Position = 0;
                        StreamReader reader = new StreamReader(appOutputStream);
                        StreamWriter writer = new StreamWriter(this.outputStream);
                        writer.Write(reader.ReadToEnd());
                        writer.Flush();
                        appOutputStream.Close();
                        this.outgoing = false;
                        this.incoming = true;
                        break;
                    }
                case SoapMessageStage.BeforeDeserialize:
                    {
                        // Make the output available for the client to parse...
                        readStr = new StreamReader(oldStream);
                        writeStr = new StreamWriter(newStream);
                        soapMsg1 = readStr.ReadToEnd();
                        xDoc.LoadXml(soapMsg1);
                        soapMsg1 = xDoc.InnerXml;
                        writeStr.Write(soapMsg1);
                        writeStr.Flush();
                        newStream.Position = 0;
                        break;
                    }
                case SoapMessageStage.AfterDeserialize:
                    break;
                default:
                    throw new Exception("invalid stage!");
            }
        }



    }

    public class InputData
    {
        public string ConnectTicket { get; set; }
        public int SubscriberID { get; set; }
        public int CustomerTypeID { get; set; }
        public int AccountTypeID { get; set; }
        public int BusinessProcessID { get; set; }
        public int TransactionTypeID { get; set; }
        public int ProductID { get; set; }
        public string ClientBillingAccountNumber { get; set; }
        public string SalesGroup { get; set; }
        public string SubGroup { get; set; }
        public string SalesChannel { get; set; }
        public string SubChannel { get; set; }
        public string BranchRegion { get; set; }
        public string BranchName { get; set; }
        public string BranchID { get; set; }
        public string BranchTelephoneISDcode { get; set; }
        public string BranchTelephoneNumber { get; set; }
        public string BranchEmailAddress { get; set; }
        public string SystemUsername { get; set; }
        public string SystemUserIDNumber { get; set; }
        public string SystemUserCellphoneISDcode { get; set; }
        public string SystemUserCellphoneNumber { get; set; }
        public string SystemUserEmailAddress { get; set; }
        public string SalesPersonCode { get; set; }
        public DateTime ApplicationDateTime { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int IdentificationTypeID { get; set; }
        public string IDNumber { get; set; }
        public DateTime IDIssueDate { get; set; }
        public DateTime DateofBirth { get; set; }
        public DateTime PassportExpiryDate { get; set; }
        public string PassportIssueCountryCode { get; set; }
        public int MaritalStatusID { get; set; }
        public int GenderID { get; set; }
        public string CellphoneNumber { get; set; }
        public string HomeTelephoneISDCode { get; set; }
        public string HomeTelephoneNumber { get; set; }
        public string WorkTelephoneISDCode { get; set; }
        public string WorkTelephoneNumber { get; set; }
        public string AlternateContactNumberISDCode { get; set; }
        public string AlternateContactNumber { get; set; }
        public string EmailAddress { get; set; }
        public string ResidentialAddress1 { get; set; }
        public string ResidentialAddress2 { get; set; }
        public string ResidentialAddress3 { get; set; }
        public string ResidentialAddress4 { get; set; }
        public string ResidentialAddressPostalCode { get; set; }
        public string PostalAddress1 { get; set; }
        public string PostalAddress2 { get; set; }
        public string PostalAddress3 { get; set; }
        public string PostalAddress4 { get; set; }
        public string PostalCode { get; set; }
        public string DeliveryAddress1 { get; set; }
        public string DeliveryAddress2 { get; set; }
        public string DeliveryAddress3 { get; set; }
        public string DeliveryAddress4 { get; set; }
        public string DeliveryAddressPostalCode { get; set; }
        public string Employer { get; set; }
        public string Designation { get; set; }
        public double MonthlyIncome { get; set; }
        public double MonthlyExpenses { get; set; }
        public double SpouseMonthlyIncome { get; set; }
        public int SpouseIdentificationTypeID { get; set; }
        public string BankName { get; set; }
        public string BankBranchCode { get; set; }
        public string BankAccountNumber { get; set; }
        public string DeviceID { get; set; }
        public string DeviceLocation { get; set; }
        public string DeviceLocale { get; set; }
        public bool FingerprintAuthenticationNeeded { get; set; }
        public bool FingerprintAuthenticationPassed { get; set; }
        public bool FingerprintAuthenticationSystemError { get; set; }
        public bool FacialRecognitionNeeded { get; set; }
        public bool FacialRecognitionPassed { get; set; }
        public bool FacialRecognitionSystemError { get; set; }
        public bool VoiceRecognitionNeeded { get; set; }
        public bool VoiceRecognitionPassed { get; set; }
        public bool VoiceRecognitionSystemError { get; set; }
        public bool KBANeeded { get; set; }
        public bool KBAPassed { get; set; }
        public bool KBASystemError { get; set; }
        public int KBAAttempts { get; set; }
        public bool AVSNeeded { get; set; }
        public bool AVSPassed { get; set; }
        public bool AVSSystemError { get; set; }
        public bool OtherAuthenticationNeeded { get; set; }
        public bool OtherAuthenticationPassed { get; set; }
        public bool OtherAuthenticationSystemError { get; set; }
        public string OtherauthenticationType { get; set; }
        public string CreditVettingDecision { get; set; }
        public string CreditVettingAuthenticationUser { get; set; }
        public bool DeviceMasked { get; set; }
        public string DeviceMacAddress { get; set; }
        public string GPSLocationCoordinates { get; set; }
        public string PublicIPAddress { get; set; }
    }

    public class OutputData
    {
        public string FMPReference { get; set; }
        public DateTime FMPDateTime { get; set; }
        public string ScorecardCharacteristic { get; set; }
        public string CharacteristicAttribute { get; set; }
        public string FraudStatus { get; set; }
        public string FraudRuleTriggered { get; set; }
        public int FraudScore { get; set; }
        public string ApplicationError { get; set; }
        public ValidationError[] ValidationError { get; set; }
    }

    public class ValidationError
    {
        public string Field { get; set; }
        public string Message { get; set; }
    }

    public class ConnectTicket
    {
        public string Value { get; set; }
        public string Error { get; set; }
    }
}

