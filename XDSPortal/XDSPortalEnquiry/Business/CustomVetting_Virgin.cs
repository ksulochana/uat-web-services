﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace XDSPortalEnquiry.Business
{
    public class CustomVetting_Virgin
    {
         private XDSPortalLibrary.Business_Layer.ConsumerCreditEnquiry moConsumerCreditGrantorWseManager;
        private XDSPortalLibrary.Entity_Layer.ConsumerCreditEnquiry eoConsumerCreditGrantorWseManager;


        public CustomVetting_Virgin()
        {
            moConsumerCreditGrantorWseManager = new XDSPortalLibrary.Business_Layer.ConsumerCreditEnquiry();
            eoConsumerCreditGrantorWseManager = new XDSPortalLibrary.Entity_Layer.ConsumerCreditEnquiry();
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitMulipleCreditEnquiry(SqlConnection con, SqlConnection AdminConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID,bool bBonusChecking, string strVoucherCode, string Unique_ref, string QueryNumber)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            //string sGender = "";
            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            string rXml = "";
            double Totalcost = 0;
            DataSet ds = new DataSet();

            string PressageScore = string.Empty, PolicyRule = string.Empty, address = string.Empty;
            double DMSCore = 0, CreditLimit =0;
            string CRCategory = string.Empty;
            string PolicyRuleDesc = string.Empty;
            

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                SqlConnection objConstring = new SqlConnection(eSe.ExtraVarInput1.ToString());

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                if (spr.ReportID > 0)
                {
                    eoConsumerCreditGrantorWseManager.BonusSegments = null;
                    

                    // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML 
                    // use this XML data for generating the Report 

                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {

                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }
                         else if (sub.PayAsYouGo == 1)
                        {
                            //Calculate The Total cost of the Report , including Bonus Segments

                            Totalcost = spr.UnitPrice;

                            if (eoConsumerCreditGrantorWseManager.BonusSegments != null)
                            {

                                foreach (DataRow dr in eoConsumerCreditGrantorWseManager.BonusSegments.Tables[0].Rows)
                                {
                                    if (dr["BonusViewed"].ToString().ToLower() == "true")
                                    {
                                        Totalcost = Totalcost + Convert.ToInt16(dr["BillingPrice"].ToString());
                                    }
                                }
                            }
                        }

                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {

                            eoConsumerCreditGrantorWseManager.ConsumerID = eSC.KeyID;
                            eoConsumerCreditGrantorWseManager.EnquiryReason = eSe.EnquiryReason;
                            eoConsumerCreditGrantorWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerCreditGrantorWseManager.ReferenceNo = eSC.Reference;
                            eoConsumerCreditGrantorWseManager.subscriberID = eSe.SubscriberID;
                            eoConsumerCreditGrantorWseManager.ProductID = eSe.ProductID;
                            eoConsumerCreditGrantorWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerCreditGrantorWseManager.TmpReference = eSC.ExtraVarOutput1;
                            moConsumerCreditGrantorWseManager.ConnectionString = objConstring;
                            eoConsumerCreditGrantorWseManager.DataSegments = eSe.ExtraVarInput2;
                            eoConsumerCreditGrantorWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;
                            eoConsumerCreditGrantorWseManager.GrossMonthlyIncome = (double) eSe.NetMonthlyIncomeAmt;
                            eoConsumerCreditGrantorWseManager.EnquiryID = intSubscriberEnquiryID;
                            eoConsumerCreditGrantorWseManager.EnquiryResultID = intSubscriberEnquiryResultID;
                            eoConsumerCreditGrantorWseManager.ProductID = eSe.ProductID;
                            eoConsumerCreditGrantorWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;
                            eoConsumerCreditGrantorWseManager.Username = sys.Username;

                            XDSPortalEnquiry.Data.Consumer oEnquiryConsumer = new XDSPortalEnquiry.Data.Consumer();

                            oEnquiryConsumer.CheckBlockedStatus(AdminConnection, int.Parse(eSe.SubscriberID.ToString()), int.Parse(eSC.KeyID.ToString()), eSe.ProductID, sub.SubscriberAssociationCode, eSe.ProductID);


                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            
                            rp = ra.GetCustomVettingBReport(eoConsumerCreditGrantorWseManager);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.GetCustomVettingBReport(eoConsumerCreditGrantorWseManager);
                            }

                            rXml = rp.ResponseData;

                            System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);


                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                            {

                                eSC.DetailsViewedYN = false;
                                eSC.Billable = false;
                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                                eSC.CreatedByUser = eSe.CreatedByUser;
                                eSC.CreatedOnDate = DateTime.Now;

                                dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                rp.EnquiryID = eSC.SubscriberEnquiryID;

                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSC.EnquiryResult = "E";
                                eSe.ErrorDescription = rp.ResponseData.ToString();
                                dSe.UpdateSubscriberEnquiryError(con, eSe);

                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Bonus)
                            {
                                ds.ReadXml(xmlSR);
                                if (ds.Tables.Contains("Segments"))
                                {

                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.BonusIncluded = true;
                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;
                                    eSC.XMLBonus = rXml.Replace("'", "''");
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.B.ToString();

                                    dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = intSubscriberEnquiryID;
                                    rp.EnquiryResultID = intSubscriberEnquiryResultID;


                                    foreach (DataRow r in ds.Tables["Segments"].Rows)
                                    {
                                        eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                        eSB.DataSegmentID = int.Parse(r["DataSegmentID"].ToString());
                                        eSB.DataSegmentName = r["DataSegmentName"].ToString().Replace("'", "''");
                                        eSB.DataSegmentDisplayText = r["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                        eSB.BonusViewed = bool.Parse(r["BonusViewed"].ToString());
                                        eSB.Billable = false;
                                        eSB.CreatedByUser = eSe.CreatedByUser;
                                        eSB.CreatedOnDate = DateTime.Now;

                                        dSB.InsertSubscriberEnquiryResultBonus(con, eSB);
                                    }
                                }
                            }

                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                ds.ReadXml(xmlSR);

                                foreach (DataTable dt in ds.Tables)
                                {
                                    if (dt.TableName == "VirginMobileCustomizedScoring")
                                    {
                                        DMSCore = Convert.ToDouble(dt.Rows[0].Field<string>("DMScore").ToString());
                                        PolicyRule = dt.Rows[0].Field<string>("VMExclusionReason").ToString();
                                        CreditLimit = Convert.ToDouble(dt.Rows[0].Field<string>("VMCreditLimit").ToString());
                                        CRCategory = dt.Rows[0].Field<string>("CRCategory").ToString();
                                        PolicyRuleDesc = dt.Rows[0].Field<string>("PolicyRule").ToString();
                                        //CreditLimit = Convert.ToDouble(CreditLimit.ToString().Substring(0, CreditLimit.ToString().IndexOf('.')));
                                    }
                                    else if (dt.TableName == "ConsumerDetail")
                                    {
                                        address = dt.Rows[0].Field<string>("ResidentialAddress").ToString();
                                    }
                                    if (dt.TableName == "ConsumerScoring")
                                    {
                                        PressageScore = dt.Rows[0].Field<string>("FinalScore").ToString();
                                        PressageScore = PressageScore.Substring(0, PressageScore.IndexOf('.'));
                                    }

                                }
                                DataSet dsResponse = new DataSet("XDSConnectResponse");
                                DataTable dtResponse = new DataTable("Response");
                                dtResponse.Columns.Add("Unique_ref", typeof(string));
                                dtResponse.Columns.Add("QueryNumber", typeof(string));
                                dtResponse.Columns.Add("QueryType", typeof(string));
                                dtResponse.Columns.Add("Decision", typeof(string));
                                dtResponse.Columns.Add("RiskGrade", typeof(string));
                                dtResponse.Columns.Add("PolicyRule1", typeof(string));
                                dtResponse.Columns.Add("PolicyRule2", typeof(string));
                                dtResponse.Columns.Add("PolicyRule3", typeof(string));
                                dtResponse.Columns.Add("PolicyRule4", typeof(string));
                                dtResponse.Columns.Add("PolicyRule5", typeof(string));
                                dtResponse.Columns.Add("Action", typeof(string));
                                dtResponse.Columns.Add("CreditLimit", typeof(string));
                                dtResponse.Columns.Add("EstimatedTelcoSpend", typeof(string));
                                dtResponse.Columns.Add("EstimatedIncome", typeof(string));
                                dtResponse.Columns.Add("ApplicationScore", typeof(string));
                                dtResponse.Columns.Add("BureauAddressLine1", typeof(string));
                                dtResponse.Columns.Add("BureauAddressLine2", typeof(string));
                                dtResponse.Columns.Add("BureauAddressSuburb", typeof(string));
                                dtResponse.Columns.Add("BureauAddressCity", typeof(string));
                                dtResponse.Columns.Add("BureauAddressPostalCode", typeof(string));

                                DataRow drResponse = dtResponse.NewRow();

                                drResponse["Unique_ref"] = Unique_ref;
                                drResponse["QueryNumber"] = QueryNumber;
                                drResponse["QueryType"] = string.Empty;

                                //if (DMSCore == 0 || DMSCore == 99 || (DMSCore >= 7 && DMSCore < 13))
                                //{
                                //    //if (DMSCore == 7 && eSe.NetMonthlyIncomeAmt <= 2999)
                                //    //{
                                //    //    drResponse["Decision"] = "D";
                                //    //}
                                //    if (DMSCore == 7 && eSe.NetMonthlyIncomeAmt >= 3000)
                                //    {
                                //        drResponse["Decision"] = "A";
                                //    }
                                //    else
                                //    {
                                //        drResponse["Decision"] = "D";
                                //    }

                                drResponse["RiskGrade"] = DMSCore.ToString();
                                drResponse["PolicyRule1"] = PolicyRuleDesc;

                                //if (PolicyRule == "FILTER_1")
                                //{
                                //    drResponse["PolicyRule1"] = "Consumer Found On SAFPS";
                                //}
                                //else if (PolicyRule == "FILTER_2")
                                //{
                                //    drResponse["PolicyRule1"] = "Consumer Listed as deceased at Home Affairs";
                                //}
                                //else if (PolicyRule == "FILTER_3")
                                //{
                                //    drResponse["PolicyRule1"] = "Consumer listed as deceased on payment profile";
                                //}
                                //else if (PolicyRule == "FILTER_4")
                                //{
                                //    drResponse["PolicyRule1"] = "Administration order listed against consumer";
                                //}
                                //else if (PolicyRule == "FILTER_5")
                                //{
                                //    drResponse["PolicyRule1"] = "Debt review listed against consumer";
                                //}
                                //else if (PolicyRule == "FILTER_6")
                                //{
                                //    drResponse["PolicyRule1"] = "Consumer disputing information";
                                //}
                                //else if (PolicyRule == "FILTER_7")
                                //{
                                //    drResponse["PolicyRule1"] = "Lack of usable account information, derogatory records present";
                                //}
                                //else if (PolicyRule == "FILTER_8")
                                //{
                                //    drResponse["PolicyRule1"] = "lack of usable account information";
                                //}
                                //else if (PolicyRule == "Policy_1")
                                //{
                                //    drResponse["PolicyRule1"] = "Sequestration listed against consumer";
                                //}
                                //else if (PolicyRule == "Policy_2")
                                //{
                                //    drResponse["PolicyRule1"] = "More than one judgment listed within the past 24 months";
                                //}
                                //else if (PolicyRule == "Policy_3")
                                //{
                                //    drResponse["PolicyRule1"] = "Judgement listed against consumer";
                                //}
                                //else if (PolicyRule == "Policy_4")
                                //{
                                //    drResponse["PolicyRule1"] = "More than one default listed with in the past 24 months";
                                //}
                                //else if (PolicyRule == "Policy_5")
                                //{
                                //    drResponse["PolicyRule1"] = "Default listed in the past 36 months";
                                //}
                                //else if (PolicyRule == "Policy_6")
                                //{
                                //    drResponse["PolicyRule1"] = "one or more account(s) listed as in legal/collection";
                                //}
                                //else if (PolicyRule == "Policy_7")
                                //{
                                //    drResponse["PolicyRule1"] = "More than 2 account(s) listed as seriously delinquent";
                                //}
                                //else if (PolicyRule == "Policy_8")
                                //{
                                //    drResponse["PolicyRule1"] = "50% or More accounts listed with seriously delinquency";
                                //}
                                //else
                                //{
                                //    drResponse["PolicyRule1"] = string.Empty;
                                //}


                                //drResponse["CreditLimit"] = "0000000";




                                //}
                                //else
                                //{
                                //    drResponse["Decision"] = "A";
                                //    drResponse["RiskGrade"] = DMSCore.ToString();
                                //    drResponse["PolicyRule1"] = string.Empty;

                                //    //if (DMSCore == 1)
                                //    //{
                                //    //    drResponse["CreditLimit"] = "0003400";
                                //    //}
                                //    //else if (DMSCore == 2)
                                //    //{
                                //    //    drResponse["CreditLimit"] = "0003400";
                                //    //}
                                //    //else if (DMSCore == 3)
                                //    //{
                                //    //    drResponse["CreditLimit"] = "0002100";
                                //    //}
                                //    //else if (DMSCore == 4)
                                //    //{
                                //    //    drResponse["CreditLimit"] = "0002100";
                                //    //}
                                //    //else if (DMSCore == 5)
                                //    //{
                                //    //    drResponse["CreditLimit"] = "0001700";
                                //    //}
                                //    //else if (DMSCore == 6)
                                //    //{
                                //    //    drResponse["CreditLimit"] = "0001700";
                                //    //}
                                //    //else
                                //    //{
                                //    //    drResponse["CreditLimit"] = "0000000";
                                //    //}
                                //}

                                drResponse["Decision"] = CRCategory == "D" ? "D" : "A";
                                drResponse["CreditLimit"] = CreditLimit.ToString();
                                drResponse["PolicyRule2"] = string.Empty;
                                drResponse["PolicyRule3"] = string.Empty;
                                drResponse["PolicyRule4"] = string.Empty;
                                drResponse["PolicyRule5"] = string.Empty;
                                drResponse["Action"] = string.Empty;
                                drResponse["EstimatedTelcoSpend"] = string.Empty;
                                drResponse["EstimatedIncome"] = string.Empty;
                                drResponse["ApplicationScore"] = PressageScore;
                                drResponse["BureauAddressLine1"] = address;
                                drResponse["BureauAddressLine2"] = string.Empty;
                                drResponse["BureauAddressSuburb"] = string.Empty;
                                drResponse["BureauAddressCity"] = string.Empty;
                                drResponse["BureauAddressPostalCode"] = string.Empty;

                                dtResponse.Rows.Add(drResponse);

                                dsResponse.Tables.Add(dtResponse);
                                rp.ResponseData = dsResponse.GetXml();
                                dsResponse.Dispose();
                                rXml = rp.ResponseData;

                                if (ds.Tables.Contains("ConsumerDetail"))
                                {

                                    Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                    Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, eSe.ProductID);

                                    foreach (DataRow r in ds.Tables["ConsumerDetail"].Rows)
                                    {
                                        Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                        Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                        eSC.DetailsViewedDate = DateTime.Now;
                                        eSC.DetailsViewedYN = true;
                                        if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                        {
                                            eSC.Billable = false;
                                        }
                                        else
                                        {
                                            eSC.Billable = true;
                                        }
                                        eSC.ChangedByUser = eSe.CreatedByUser;
                                        eSC.ChangedOnDate = DateTime.Now;
                                        eSC.SearchOutput = "";


                                        if (r.Table.Columns.Contains("IDNo"))
                                        {
                                            if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                            {
                                                eSC.IDNo = r["IDNo"].ToString();
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                            }
                                        }
                                        if (r.Table.Columns.Contains("PassportNo"))
                                        {
                                            if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                            {
                                                eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                            }
                                        }
                                        if (r.Table.Columns.Contains("Surname"))
                                        {
                                            if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                            {
                                                eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                            }
                                        }

                                        if (r.Table.Columns.Contains("FirstName"))
                                        {
                                            if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                            {
                                                eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                            }
                                        }

                                        if (eSC.SearchOutput.Length > 0)
                                        {
                                            eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                        }

                                        eSC.KeyID = rp.ResponseKey;
                                        eSC.KeyType = rp.ResponseKeyType;
                                        eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                        eSC.BillingTypeID = spr.BillingTypeID;
                                        eSC.BillingPrice = spr.UnitPrice;

                                        eSC.XMLData = rXml.Replace("'", "''");
                                        eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                        eSC.ProductID = eSe.ProductID;
                                        eSC.ExtraVarOutput1 = QueryNumber;

                                        dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                        // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                        if (sub.PayAsYouGo == 1)
                                        {
                                            dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                        }
                                        if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                        {
                                            dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                        }
                                    }
                                }
                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                            {
                                ds.ReadXml(xmlSR);
                                if (ds.Tables.Contains("ConsumerDetails"))
                                {
                                    foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                    {

                                        eSC.SearchOutput = "";

                                        if (r.Table.Columns.Contains("IDNo"))
                                        {
                                            if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                            {
                                                eSC.IDNo = r["IDNo"].ToString();
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                            }
                                        }
                                        if (r.Table.Columns.Contains("PassportNo"))
                                        {
                                            if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                            {
                                                eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                            }
                                        }
                                        if (r.Table.Columns.Contains("Surname"))
                                        {
                                            if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                            {
                                                eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                            }
                                        }

                                        if (r.Table.Columns.Contains("FirstName"))
                                        {
                                            if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                            {
                                                eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                            }
                                        }

                                        if (eSC.SearchOutput.Length > 0)
                                        {
                                            eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                        }

                                        eSC.DetailsViewedYN = false;
                                        eSC.Billable = false;
                                        eSC.ChangedByUser = eSe.CreatedByUser;
                                        eSC.ChangedOnDate = DateTime.Now;
                                        eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                        eSC.KeyType = rp.ResponseKeyType;

                                        eSC.BillingTypeID = spr.BillingTypeID;
                                        eSC.BillingPrice = spr.UnitPrice;

                                        eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                        eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();

                                        dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                }
                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                            {
                                ds.ReadXml(xmlSR);
                                if (ds.Tables.Contains("ConsumerDetails"))
                                {
                                    foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                    {
                                        eSC.SearchOutput = "";

                                        if (r.Table.Columns.Contains("IDNo"))
                                        {
                                            if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                            {
                                                eSC.IDNo = r["IDNo"].ToString();
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                            }
                                        }
                                        if (r.Table.Columns.Contains("PassportNo"))
                                        {
                                            if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                            {
                                                eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                            }
                                        }
                                        if (r.Table.Columns.Contains("Surname"))
                                        {
                                            if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                            {
                                                eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                            }
                                        }

                                        if (r.Table.Columns.Contains("FirstName"))
                                        {
                                            if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                            {
                                                eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                            }
                                        }

                                        if (eSC.SearchOutput.Length > 0)
                                        {
                                            eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                        }

                                        eSC.DetailsViewedYN = false;
                                        eSC.Billable = false;
                                        eSC.CreatedByUser = eSe.CreatedByUser;
                                        eSC.CreatedOnDate = DateTime.Now;
                                        eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                        eSC.KeyType = rp.ResponseKeyType;

                                        eSC.BillingTypeID = spr.BillingTypeID;
                                        eSC.BillingPrice = spr.UnitPrice;

                                        eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                        eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();

                                        dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                }
                            }

                        }
                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.

                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                        }
                    }
                    else
                    {
                        // When User want to Re - Open a report 
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = eSC.XMLData;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }
                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;


                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            ds.Dispose();
            return rp;
        }
    
    }
}
