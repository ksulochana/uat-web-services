﻿using Newtonsoft.Json.Linq;
using System;

using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Net;

namespace XDSPortalEnquiry.Business
{
    public class ConsumerTrace_DOVS
    {

        private XDSPortalLibrary.Business_Layer.ConsumerTrace moConsumerTraceWseManager;
        private XDSPortalLibrary.Entity_Layer.ConsumerTrace eoConsumerTraceWseManager;


        public ConsumerTrace_DOVS()
        {
            moConsumerTraceWseManager = new XDSPortalLibrary.Business_Layer.ConsumerTrace();
            eoConsumerTraceWseManager = new XDSPortalLibrary.Entity_Layer.ConsumerTrace();
        }

        public XDSPortalLibrary.Entity_Layer.XDSDOVResponse SubmitDOVResult(SqlConnection con, SqlConnection AdminConnection, XDSPortalLibrary.Entity_Layer.DIAResponse resp)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            string rXml = "";
            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();

            xdsBilling xb = new xdsBilling();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            XDSPortalLibrary.Entity_Layer.XDSDOVResponse result = new XDSPortalLibrary.Entity_Layer.XDSDOVResponse();

            try
            {
                if (String.IsNullOrEmpty(resp.UnigueOTLref))
                {
                    throw new Exception("UniqueOTLref is missing");
                }

                eSe = dSe.GetSubscriberEnquiryObjectByBranchCode(con, resp.UnigueOTLref);
                eSC = dSC.GetSubscriberEnquiryResultObjectBySubscriberEnquiryID(con, eSe.SubscriberEnquiryID);

                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                eoConsumerTraceWseManager.ReferenceNo = eSe.SubscriberEnquiryID.ToString();
                eoConsumerTraceWseManager.IDno = eSe.IDNo;
                eoConsumerTraceWseManager.HomePhoneNumber = eSe.TelephoneNo;
                eoConsumerTraceWseManager.ProductID = eSe.ProductID;
                eoConsumerTraceWseManager.subscriberID = eSe.SubscriberID;
                Dictionary<string, string> AddressList = new Dictionary<string, string>();


                //AddressList = GetAddress(resp.Latitude, resp.Longitude);



                //  File.AppendAllText("C:\\Log\\DOV.txt", "SaveError " + "Kavya");
                //if (File.Exists(path))
                //{
                //    using (var sss = new StreamWriter(path, true))
                //    {
                //        sss.WriteLine(sw.ToString());
                //        sss.Close();
                //    }
                //}
                //    var json = new JavaScriptSerializer().Serialize(obj);
                // Console.WriteLine(json);
                if (AddressList != null)
                {
                    foreach (KeyValuePair<string, string> Address in AddressList)
                    {
                        if (Address.Key == "StreetNumber")
                        {
                            resp.StreetNumber = Address.Value;
                        }
                        else if (Address.Key == "Route")
                        {
                            resp.Route = Address.Value;
                        }
                        else if (Address.Key == "sublocality")
                        {
                            resp.Sublocality = Address.Value;
                        }
                        else if (Address.Key == "Locality")
                        {
                            resp.Locality = Address.Value;
                        }
                        else if (Address.Key == "country")
                        {
                            resp.Country = Address.Value;
                        }
                        else if (Address.Key == "PostalCode")
                        {
                            resp.PostalCode = Address.Value;
                        }
                    }
                }

                InsertconsumerLocationSubscriberEnquiry(con, eSe, resp);
                XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);

                if (eSe.ProductID == 194 || eSe.ProductID == 227 || eSe.ProductID == 232)
                {
                    rp = ra.ConsumerTraceMatchDOVS(eoConsumerTraceWseManager, spr.ReportID.ToString(), "DOVS");

                    //If error raise one last call with a different connection
                    if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                    {
                        result.ResponseCode = -1;
                        result.ResponseMsg = "Failure";
                        result.Errors = rp.ResponseData.ToString();

                        eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                        ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.ConsumerTraceMatchDOVS(eoConsumerTraceWseManager, spr.ReportID.ToString(), "DOVS");
                    }

                    rXml = rp.ResponseData;

                    System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                    if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                    {
                        eSC.SearchOutput = "";
                        eSC.IDNo = "";
                        eSC.PassportNo = "";
                        eSC.Surname = "";
                        eSC.FirstName = "";
                        eSC.BirthDate = DateTime.Parse("1900/01/01");
                        eSC.Gender = "";
                        eSC.DetailsViewedYN = false;
                        eSC.Billable = false;
                        eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();

                        dSC.UpdateSubscriberEnquiryResult(con, eSC);

                        result.ResponseCode = -1;
                        result.ResponseMsg = "Failure";
                        result.Errors = "No record found";
                    }
                    else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                    {
                        eSC.EnquiryResult = "E";
                        eSe.ErrorDescription = rp.ResponseData.ToString();
                        dSe.UpdateSubscriberEnquiryError(con, eSe);

                        result.ResponseCode = -1;
                        result.ResponseMsg = "Failure";
                        result.Errors = rp.ResponseData.ToString();
                    }
                    else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report ||
                             rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single ||
                             rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                    {
                        ds.ReadXml(xmlSR);
                        if (ds.Tables.Contains("ConsumerDetails"))
                        {
                            Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                            Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, eSe.ProductID);

                            DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                            dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                            dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                            dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                            dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                            dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                            DataRow drSubscriberInput;
                            drSubscriberInput = dtSubscriberInput.NewRow();

                            drSubscriberInput["EnquiryDate"] = DateTime.Now;
                            drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                            drSubscriberInput["SubscriberName"] = eSe.SubscriberName;
                            drSubscriberInput["SubscriberUserName"] = eSe.SystemUser;
                            drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();

                            dtSubscriberInput.Rows.Add(drSubscriberInput);

                            ds.Tables.Add(dtSubscriberInput);

                            string Img = "";

                            if (resp.MatchedImgArray != null)
                            {
                                Img = Convert.ToBase64String(resp.MatchedImgArray);
                            }

                            string CapturedImg = "";

                            if (resp.CapturedImgArray != null)
                            {
                                CapturedImg = Convert.ToBase64String(resp.CapturedImgArray);
                            }

                            DataTable dtDOVResult = new DataTable("DOVDetails");
                            dtDOVResult.Columns.Add("Name", typeof(String));
                            dtDOVResult.Columns.Add("Surname", typeof(String));
                            dtDOVResult.Columns.Add("DeceasedStatus", typeof(String));
                            dtDOVResult.Columns.Add("DeceasedDate", typeof(String));
                            dtDOVResult.Columns.Add("IDBookDate", typeof(String));
                            dtDOVResult.Columns.Add("IDCardInd", typeof(String));
                            dtDOVResult.Columns.Add("IDCardDate", typeof(String));
                            dtDOVResult.Columns.Add("ConsumerIDPhotoMatch", typeof(String));
                            dtDOVResult.Columns.Add("MatchResponseCode", typeof(String));
                            dtDOVResult.Columns.Add("LivenessDetectionResult", typeof(String));
                            dtDOVResult.Columns.Add("AgeEstimationOfLiveness", typeof(String));
                            dtDOVResult.Columns.Add("ConsumerIDPhoto", typeof(String));
                            dtDOVResult.Columns.Add("ConsumerCapturedPhoto", typeof(String));
                            dtDOVResult.Columns.Add("StreetNumber", typeof(String));
                            dtDOVResult.Columns.Add("Route", typeof(String));
                            dtDOVResult.Columns.Add("Sublocality", typeof(String));
                            dtDOVResult.Columns.Add("Locality", typeof(String));
                            dtDOVResult.Columns.Add("Country", typeof(String));
                            dtDOVResult.Columns.Add("PostalCode", typeof(String));
                            dtDOVResult.Columns.Add("Latitude", typeof(String));
                            dtDOVResult.Columns.Add("Longitude", typeof(String));
                            DataRow drDOVResult;
                            drDOVResult = dtDOVResult.NewRow();

                            drDOVResult["Name"] = resp.Name;
                            drDOVResult["Surname"] = resp.Surname;
                            drDOVResult["DeceasedStatus"] = resp.Status;
                            drDOVResult["DeceasedDate"] = string.IsNullOrEmpty(resp.DeceasedDate) ? "" : resp.DeceasedDate;
                            drDOVResult["IDBookDate"] = string.IsNullOrEmpty(resp.HAIDBookIssuedDate) ? "" : resp.HAIDBookIssuedDate;
                            drDOVResult["IDCardInd"] = string.IsNullOrEmpty(resp.IdCardInd) ? "" : resp.IdCardInd;
                            drDOVResult["IDCardDate"] = string.IsNullOrEmpty(resp.IdCardDate) ? "" : resp.IdCardDate;
                            drDOVResult["ConsumerIDPhotoMatch"] = resp.ResponseMsg;
                            drDOVResult["MatchResponseCode"] = resp.ResponseCode.ToString();
                            drDOVResult["LivenessDetectionResult"] = resp.livenessStatus == 1 ? "Failed" : "Passed";
                            drDOVResult["AgeEstimationOfLiveness"] = resp.ageEstimateGroup.ToString();
                            drDOVResult["ConsumerIDPhoto"] = Img;
                            drDOVResult["ConsumerCapturedPhoto"] = CapturedImg;
                            drDOVResult["StreetNumber"] = resp.StreetNumber;
                            drDOVResult["Route"] = resp.Route;
                            drDOVResult["Sublocality"] = resp.Sublocality;
                            drDOVResult["Locality"] = resp.Locality;
                            drDOVResult["Country"] = resp.Country;
                            drDOVResult["PostalCode"] = resp.PostalCode;
                            drDOVResult["Latitude"] = resp.Latitude;
                            drDOVResult["Longitude"] = resp.Longitude;

                            dtDOVResult.Rows.Add(drDOVResult);

                            ds.Tables.Add(dtDOVResult);

                            rXml = ds.GetXml();
                            rp.ResponseData = rXml;

                            foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                            {
                                eSC.SearchOutput = "";
                                eSC.IDNo = "";
                                eSC.PassportNo = "";
                                eSC.Surname = "";
                                eSC.FirstName = "";
                                eSC.BirthDate = DateTime.Parse("1900/01/01");
                                eSC.Gender = "";
                                eSC.SearchOutput = "";

                                if (r.Table.Columns.Contains("IDNo"))
                                {
                                    if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                    {
                                        eSC.IDNo = r["IDNo"].ToString();
                                        eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                    }
                                }
                                if (r.Table.Columns.Contains("PassportNo"))
                                {
                                    if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                    {
                                        eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                        eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                    }
                                }
                                if (r.Table.Columns.Contains("Surname"))
                                {
                                    if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                    {
                                        eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                        eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                    }
                                }

                                if (r.Table.Columns.Contains("FirstName"))
                                {
                                    if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                    {
                                        eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                        eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                    }
                                }

                                if (eSC.SearchOutput.Length > 0)
                                {
                                    eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                }

                                eSC.BillingTypeID = spr.BillingTypeID;
                                eSC.BillingPrice = spr.UnitPrice;
                                eSC.DetailsViewedDate = DateTime.Now;
                                eSC.DetailsViewedYN = true;
                                eSC.Billable = true;
                                eSC.XMLData = rXml.Replace("'", "''");
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();

                                dSC.UpdateSubscriberEnquiryResult(con, eSC);

                                result.ResponseCode = 1;
                                result.ResponseMsg = "Success";
                                result.Errors = "";
                            }
                        }
                    }
                }
                else
                {
                    ds = new DataSet("Consumer");

                    DataTable dtResult = new DataTable("ConsumerDetails");
                    dtResult.Columns.Add("IDNo", typeof(String));
                    dtResult.Columns.Add("FirstName", typeof(String));
                    dtResult.Columns.Add("SecondName", typeof(String));
                    dtResult.Columns.Add("Surname", typeof(String));
                    dtResult.Columns.Add("DeceasedStatus", typeof(String));
                    dtResult.Columns.Add("DeceasedDate", typeof(String));
                    dtResult.Columns.Add("IDBookDate", typeof(String));
                    dtResult.Columns.Add("IDCardInd", typeof(String));
                    dtResult.Columns.Add("IDCardDate", typeof(String));
                    dtResult.Columns.Add("ConsumerIDPhotoMatch", typeof(String));
                    dtResult.Columns.Add("LivenessDetectionResult", typeof(String));
                    dtResult.Columns.Add("ConsumerIDPhoto", typeof(String));
                    dtResult.Columns.Add("ConsumerCapturedPhoto", typeof(String));
                    dtResult.Columns.Add("IDMatch", typeof(String));
                    dtResult.Columns.Add("NameMatch", typeof(String));
                    dtResult.Columns.Add("VerificationResult", typeof(String));
                    DataRow drResult;
                    drResult = dtResult.NewRow();

                    string[] Names = resp.Name.Split(' ');

                    string Img = "";

                    if (resp.MatchedImgArray != null)
                    {
                        Img = Convert.ToBase64String(resp.MatchedImgArray);
                    }

                    string CapturedImg = "";

                    if (resp.CapturedImgArray != null)
                    {
                        CapturedImg = Convert.ToBase64String(resp.CapturedImgArray);
                    }

                    drResult["IDNo"] = eSe.IDNo;
                    drResult["FirstName"] = Names[0].ToString();
                    drResult["SecondName"] = Names.Length > 1 ? Names[1].ToString() : "";
                    drResult["Surname"] = resp.Surname;
                    drResult["DeceasedStatus"] = resp.Status;
                    drResult["DeceasedDate"] = string.IsNullOrEmpty(resp.DeceasedDate) ? "" : resp.DeceasedDate;
                    drResult["IDBookDate"] = string.IsNullOrEmpty(resp.HAIDBookIssuedDate) ? "" : resp.HAIDBookIssuedDate;
                    drResult["IDCardInd"] = string.IsNullOrEmpty(resp.IdCardInd) ? "" : resp.IdCardInd;
                    drResult["IDCardDate"] = string.IsNullOrEmpty(resp.IdCardDate) ? "" : resp.IdCardDate;
                    drResult["ConsumerIDPhotoMatch"] = resp.ResponseMsg;
                    drResult["LivenessDetectionResult"] = resp.livenessStatus == 1 ? "Failed" : "Passed";
                    drResult["ConsumerIDPhoto"] = Img;
                    drResult["ConsumerCapturedPhoto"] = CapturedImg;
                    drResult["IDMatch"] = "Matched";
                    drResult["NameMatch"] = eSe.FirstName.Trim().ToUpper() == Names[0].ToString().Trim().ToUpper() ? "Matched" : "Not Matched";

                    if (resp.Status != "Alive" || drResult["ConsumerIDPhotoMatch"].ToString() != "Matched" || drResult["IDMatch"].ToString() != "Matched")
                    {
                        drResult["VerificationResult"] = "Failed";
                    }
                    else
                    {
                        drResult["VerificationResult"] = "Passed";
                    }

                    dtResult.Rows.Add(drResult);

                    ds.Tables.Add(dtResult);

                    if (ds.Tables.Contains("ConsumerDetails"))
                    {
                        Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                        Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, eSe.ProductID);

                        DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                        dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                        dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                        dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                        dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                        dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                        DataRow drSubscriberInput;
                        drSubscriberInput = dtSubscriberInput.NewRow();

                        drSubscriberInput["EnquiryDate"] = DateTime.Now;
                        drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                        drSubscriberInput["SubscriberName"] = eSe.SubscriberName;
                        drSubscriberInput["SubscriberUserName"] = eSe.SystemUser;
                        drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();

                        dtSubscriberInput.Rows.Add(drSubscriberInput);

                        ds.Tables.Add(dtSubscriberInput);

                        rXml = ds.GetXml();
                        rp.ResponseData = rXml;

                        foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                        {
                            eSC.SearchOutput = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.SearchOutput = "";

                            if (r.Table.Columns.Contains("IDNo"))
                            {
                                if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                {
                                    eSC.IDNo = r["IDNo"].ToString();
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                }
                            }
                            if (r.Table.Columns.Contains("PassportNo"))
                            {
                                if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                {
                                    eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                }
                            }
                            if (r.Table.Columns.Contains("Surname"))
                            {
                                if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                {
                                    eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                }
                            }

                            if (r.Table.Columns.Contains("FirstName"))
                            {
                                if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                {
                                    eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                }
                            }

                            if (eSC.SearchOutput.Length > 0)
                            {
                                eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                            }

                            eSC.BillingTypeID = spr.BillingTypeID;
                            eSC.BillingPrice = spr.UnitPrice;
                            eSC.DetailsViewedDate = DateTime.Now;
                            eSC.DetailsViewedYN = true;
                            eSC.Billable = true;
                            eSC.XMLData = rXml.Replace("'", "''");
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();

                            dSC.UpdateSubscriberEnquiryResult(con, eSC);

                            result.ResponseCode = 1;
                            result.ResponseMsg = "Success";
                            result.Errors = "";
                        }
                    }
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

                result.ResponseCode = -1;
                result.ResponseMsg = "Failure";
                result.Errors = oException.Message;
            }

            return result;
        }

        public void InsertconsumerLocationSubscriberEnquiry(SqlConnection conString, Entity.SubscriberEnquiry S, XDSPortalLibrary.Entity_Layer.DIAResponse resp)
        {

            using (SqlCommand cmd = new SqlCommand("INSERT INTO ConsumerLocationTrcaking(SytemUserID,Username,SubscriberID,IDNo,OTLReferenceNo,EnquiryID,ProductID,CreatedOnDate,CreatedByuser,streetnumber,addressRoute,sublocality,locality,country,PostalCode,Latitude,Longitude) VALUES (@SytemUserID,@Username,@SubscriberID,@IDNumber,@OTLReferenceNo,@EnquiryID,@ProductID,@CreatedOnDate,@CreatedByuser, @streetnumber,@addressRoute,@sublocality,@locality,@country,@PostalCode,@Latitude,@Longitude)", conString))
            {
                cmd.Parameters.Add("@SytemUserID", SqlDbType.BigInt).Value = S.SystemUserID;
                cmd.Parameters.Add("@Username", SqlDbType.VarChar).Value = S.CreatedByUser;
                cmd.Parameters.Add("@SubscriberID", SqlDbType.Int).Value = S.SubscriberID;
                cmd.Parameters.Add("@IDNumber", SqlDbType.VarChar).Value = S.IDNo;
                cmd.Parameters.Add("@OTLReferenceNo", SqlDbType.VarChar).Value = resp.UnigueOTLref;

                cmd.Parameters.Add("@EnquiryID", SqlDbType.BigInt).Value = S.SubscriberEnquiryID;
                cmd.Parameters.Add("@ProductID", SqlDbType.Int).Value = S.ProductID;
                cmd.Parameters.Add("@CreatedOnDate", SqlDbType.DateTime).Value = S.CreatedOnDate;
                cmd.Parameters.Add("@CreatedByuser", SqlDbType.VarChar, 100).Value = S.CreatedByUser;
                cmd.Parameters.Add("@streetnumber", SqlDbType.VarChar, 100).Value = resp.StreetNumber == null ? string.Empty : resp.StreetNumber;
                cmd.Parameters.Add("@addressRoute", SqlDbType.VarChar, 100).Value = resp.Route == null ? string.Empty : resp.Route;
                cmd.Parameters.Add("@sublocality", SqlDbType.VarChar, 100).Value = resp.Sublocality == null ? string.Empty : resp.Sublocality;
                cmd.Parameters.Add("@locality", SqlDbType.VarChar, 100).Value = resp.Locality == null ? string.Empty : resp.Locality;
                cmd.Parameters.Add("@country", SqlDbType.VarChar, 100).Value = resp.Country == null ? string.Empty : resp.Country;
                cmd.Parameters.Add("@PostalCode", SqlDbType.VarChar, 100).Value = resp.PostalCode == null ? string.Empty : resp.PostalCode;
                cmd.Parameters.Add("@Latitude", SqlDbType.VarChar, 100).Value = resp.Latitude == null ? string.Empty : resp.Latitude;
                cmd.Parameters.Add("@Longitude", SqlDbType.VarChar, 100).Value = resp.Longitude == null ? string.Empty : resp.Longitude;


                if (conString.State == ConnectionState.Closed)
                    conString.Open();

                cmd.ExecuteNonQuery();


            }
        }
        public Dictionary<string, string> GetAddress(string lattitude, string longitude)
        {
            Dictionary<string, string> addressDeatils = new Dictionary<string, string>();
            var TmpLat = lattitude;
            var TmpLong = longitude;
            if (!string.IsNullOrEmpty(lattitude) && !string.IsNullOrEmpty(longitude))
            {
                if (lattitude.Replace(" ", "") != "DeviceLocationDisabled/Denied" && longitude.Replace(" ", "") != "DeviceLocationDisabled/Denied")
                {


                    var api_url = "https://maps.googleapis.com/maps/api/geocode/json";
                    string key = "AIzaSyBJgd-E4DXfzElShY-csnI5FULqSEPL-PU";

                    string url = api_url + "?latlng=" + lattitude.ToString(CultureInfo.InvariantCulture) + "," + longitude.ToString(CultureInfo.InvariantCulture) + "&key=" + key;
                    var request = (HttpWebRequest)WebRequest.Create(url);

                    request.Method = "GET";

                    request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36";

                    request.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;

                    var response = (HttpWebResponse)request.GetResponse();

                    string content = string.Empty;

                    using (var stream = response.GetResponseStream())
                    {

                        using (var sr = new StreamReader(stream))
                        {

                            content = sr.ReadToEnd();




                        }

                    }


                    string Address = string.Empty;
                    string StreetNumber = string.Empty;
                    string Route = string.Empty;
                    string Sublocality = string.Empty;
                    string Locality = string.Empty;
                    string Country = string.Empty;
                    string PostalCode = string.Empty;
                    string Lattitude = string.Empty;
                    string Longitude = string.Empty;

                    if (!string.IsNullOrEmpty(content))
                    {

                        var userObj = JObject.Parse(content);
                        var userGuid = userObj["results"][0];

                        JArray categories = (JArray)userGuid["address_components"];
                        foreach (var item in categories.Children())
                        {
                            var itemProperties = item.Children<JProperty>();
                            string type = item.SelectToken("types").ToString();
                            if (type.Contains("street_number"))
                            {
                                StreetNumber = item["long_name"].ToString();
                                addressDeatils.Add("StreetNumber", StreetNumber);
                            }
                            else if (type.Contains("route"))
                            {
                                Route = item["long_name"].ToString();
                                addressDeatils.Add("Route", Route);
                            }
                            else if (type.Contains("sublocality"))
                            {
                                Sublocality = item["long_name"].ToString();
                                addressDeatils.Add("Sublocality", Sublocality);
                            }
                            else if (type.Contains("locality"))
                            {
                                Locality = item["long_name"].ToString();
                                addressDeatils.Add("Locality", Locality);

                            }
                            else if (type.Contains("country"))
                            {
                                Country = item["long_name"].ToString();
                                addressDeatils.Add("country", Country);
                            }

                            else if (type.Contains("postal_code"))
                            {
                                PostalCode = item["long_name"].ToString();
                                addressDeatils.Add("PostalCode", PostalCode);
                            }
                            //you could do a foreach or a linq here depending on what you need to do exactly with the value








                        }
                    }


                }


                return addressDeatils;
            }
            else
            {
                return addressDeatils;

            }

        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitConsumerTraceDOVS(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string strIDNo, string strCellNo, string strExtRef, string strVoucherCode)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bool bBonusChecking = false;
            string sGender = "";
            string rXml = "";
            int MatchResult = 0;
            int intSubscriberEnquiryResultID = 0;
            DataSet ds = new DataSet();
            Boolean boolSubmitEnquiry = true;
            string strValidationStatus = "";


            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);


            try
            {
                // Check if this subscriber is authorized to use the current product

                if (spr.ReportID > 0)
                {
                    // Check the PayAsYouGo settings for the subscriber
                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {

                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;

                        eSe.IDNo = strIDNo;
                        eSe.TelephoneNo = strCellNo;
                        eSe.ProductID = intProductId;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = strExtRef;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";

                        //Generate serach input string

                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.TelephoneNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.TelephoneNo;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table

                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);

                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;
                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        //SqlConnection MatchingConstring = new SqlConnection(eSe.ExtraVarInput1.ToString());

                        //moConsumerTraceWseManager.ConnectionString = MatchingConstring;
                        eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoConsumerTraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoConsumerTraceWseManager.ExternalReference = strExtRef;
                        eoConsumerTraceWseManager.IDno = strIDNo;
                        eoConsumerTraceWseManager.HomePhoneNumber = strCellNo;
                        eoConsumerTraceWseManager.ProductID = intProductId;
                        eoConsumerTraceWseManager.subscriberID = intSubscriberID;
                        eoConsumerTraceWseManager.BonusCheck = bBonusChecking;

                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.ConsumerTraceMatchDOVS(eoConsumerTraceWseManager, spr.ReportID.ToString(), "DOVS");

                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.ConsumerTraceMatchDOVS(eoConsumerTraceWseManager, spr.ReportID.ToString(), "DOVS");
                        }

                        //rp = moConsumerTraceWseManager.search(eoConsumerTraceWseManager);
                        rp.EnquiryID = intSubscriberEnquiryID;

                        // Get Response data

                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);


                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            eSC.SearchOutput = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.DetailsViewedYN = false;
                            eSC.Billable = false;
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;

                            rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                            rp.EnquiryID = eSC.SubscriberEnquiryID;
                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);


                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report ||
                                 rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single ||
                                 rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                        {
                            string ConsumerID = "";
                            ds.ReadXml(xmlSR);
                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                ConsumerID = ds.Tables[0].Rows[0]["ConsumerID"].ToString();
                                ds.Tables[0].Columns.Add("EnquiryID");
                                ds.Tables[0].Columns.Add("EnquiryResultID");

                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, intProductId);

                                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);

                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.KeyID = Int32.Parse(ConsumerID);
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                    intSubscriberEnquiryResultID = rp.EnquiryResultID;
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);
                                    // Change the PayAsYouGoEnquiryLimit  after viewing the report

                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);

                                    ds.Tables[0].Rows[0]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[0]["EnquiryResultID"] = eSC.SubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[0]["ReferenceNo"] = eSC.Reference;

                                    rXml = ds.GetXml();
                                    rp.ResponseData = rXml;
                                }
                            }
                        }

                    }

                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }
            catch (Exception oException)
            {

                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);

                MatchResult = eSe.SubscriberEnquiryID;
                rp.EnquiryID = eSe.SubscriberEnquiryID;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (oException.Message.Contains("A .NET Framework"))
                {
                    rp.ResponseData = "Error: The System is unable to process this request. Please contact XDS Support";
                }

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitConsumerTraceDOVS(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string strIDNo, string strCellNo, string strExtRef, string strVoucherCode, string SolutionTransactionID)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bool bBonusChecking = false;
            string sGender = "";
            string rXml = "";
            int MatchResult = 0;
            int intSubscriberEnquiryResultID = 0;
            DataSet ds = new DataSet();
            Boolean boolSubmitEnquiry = true;
            string strValidationStatus = "";


            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);


            try
            {
                // Check if this subscriber is authorized to use the current product

                if (spr.ReportID > 0)
                {
                    // Check the PayAsYouGo settings for the subscriber
                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {

                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;

                        eSe.IDNo = strIDNo;
                        eSe.TelephoneNo = strCellNo;
                        eSe.ProductID = intProductId;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = strExtRef;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";

                        //Generate serach input string

                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.TelephoneNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.TelephoneNo;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table

                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);

                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        //Submit to MDP
                        MDPEnquiryLog mdpLog = new MDPEnquiryLog();
                        XDSPortalLibrary.Entity_Layer.MDPResponse mdpResp;

                        if (string.IsNullOrEmpty(SolutionTransactionID))
                        {
                            mdpResp = mdpLog.SubmitToMDP(eSe.ProductID.ToString(), ConfigurationManager.AppSettings["MDPDOVSProductName"].ToString(), eSe.SubscriberID.ToString(), eSe.SubscriberName, intSubscriberEnquiryID.ToString());
                        }
                        else
                        {
                            mdpResp = mdpLog.SubmitToMDP(eSe.ProductID.ToString(), ConfigurationManager.AppSettings["MDPDOVSProductName"].ToString(), eSe.SubscriberID.ToString(), eSe.SubscriberName, intSubscriberEnquiryID.ToString(), SolutionTransactionID);
                        }

                        if (mdpResp.response.ProductTransactionId != "")
                        {
                            XDSPortalEnquiry.Data.SubscriberEnquiry se = new Data.SubscriberEnquiry();
                            se.InsertSubscriberEnquiryMDP(con, intSubscriberEnquiryID, mdpResp.response.ProductTransactionId, mdpResp.response.Message, eSe.CreatedByUser, DateTime.Now);
                        }

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        //SqlConnection MatchingConstring = new SqlConnection(eSe.ExtraVarInput1.ToString());

                        //moConsumerTraceWseManager.ConnectionString = MatchingConstring;
                        eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoConsumerTraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoConsumerTraceWseManager.ExternalReference = strExtRef;
                        eoConsumerTraceWseManager.IDno = strIDNo;
                        eoConsumerTraceWseManager.HomePhoneNumber = strCellNo;
                        eoConsumerTraceWseManager.ProductID = intProductId;
                        eoConsumerTraceWseManager.subscriberID = intSubscriberID;
                        eoConsumerTraceWseManager.BonusCheck = bBonusChecking;

                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.ConsumerTraceMatchDOVS(eoConsumerTraceWseManager, spr.ReportID.ToString(), "DOVS");

                        //If error raise one last call with a different connection
                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.ConsumerTraceMatchDOVS(eoConsumerTraceWseManager, spr.ReportID.ToString(), "DOVS");
                        }

                        //rp = moConsumerTraceWseManager.search(eoConsumerTraceWseManager);
                        rp.EnquiryID = intSubscriberEnquiryID;

                        // Get Response data

                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);


                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            eSC.SearchOutput = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.DetailsViewedYN = false;
                            eSC.Billable = false;
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;

                            rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                            rp.EnquiryID = eSC.SubscriberEnquiryID;
                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {

                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);


                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report ||
                                 rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single ||
                                 rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                        {
                            string ConsumerID = "";
                            ds.ReadXml(xmlSR);
                            if (ds.Tables.Contains("ConsumerDetails"))
                            {
                                ConsumerID = ds.Tables[0].Rows[0]["ConsumerID"].ToString();
                                ds.Tables[0].Columns.Add("EnquiryID");
                                ds.Tables[0].Columns.Add("EnquiryResultID");

                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, intProductId);

                                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();

                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);

                                foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                {
                                    eSC.SearchOutput = "";
                                    eSC.IDNo = "";
                                    eSC.PassportNo = "";
                                    eSC.Surname = "";
                                    eSC.FirstName = "";
                                    eSC.BirthDate = DateTime.Parse("1900/01/01");
                                    eSC.Gender = "";
                                    eSC.SearchOutput = "";

                                    if (r.Table.Columns.Contains("IDNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                        {
                                            eSC.IDNo = r["IDNo"].ToString();
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("PassportNo"))
                                    {
                                        if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                        {
                                            eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                        }
                                    }
                                    if (r.Table.Columns.Contains("Surname"))
                                    {
                                        if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                        {
                                            eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                        }
                                    }

                                    if (r.Table.Columns.Contains("FirstName"))
                                    {
                                        if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                        {
                                            eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                        }
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.KeyID = Int32.Parse(ConsumerID);
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                    eSC.ExtraVarOutput1 = "";
                                    eSC.ProductID = intProductId;
                                    eSC.VoucherCode = strVoucherCode;

                                    rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                                    intSubscriberEnquiryResultID = rp.EnquiryResultID;
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);
                                    // Change the PayAsYouGoEnquiryLimit  after viewing the report

                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);

                                    ds.Tables[0].Rows[0]["EnquiryID"] = eSC.SubscriberEnquiryID;
                                    ds.Tables[0].Rows[0]["EnquiryResultID"] = eSC.SubscriberEnquiryResultID;
                                    ds.Tables[0].Rows[0]["ReferenceNo"] = eSC.Reference;

                                    rXml = ds.GetXml();
                                    rp.ResponseData = rXml;
                                }
                            }
                        }

                    }

                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }
            catch (Exception oException)
            {

                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);

                MatchResult = eSe.SubscriberEnquiryID;
                rp.EnquiryID = eSe.SubscriberEnquiryID;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (oException.Message.Contains("A .NET Framework"))
                {
                    rp.ResponseData = "Error: The System is unable to process this request. Please contact XDS Support";
                }

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitDIARequest(SqlConnection Enquirycon, SqlConnection AdminConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, string strVoucherCode, string Entity, int OTLType, int SubscriberID)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            try
            {
                if (Enquirycon.State == ConnectionState.Closed)
                    Enquirycon.Open();
                if (AdminConnection.State == ConnectionState.Closed)
                    AdminConnection.Open();
            }
            catch (Exception ex)
            {
            }

            string rXml = "";
            double Totalcost = 0;
            string strValidationStatus = "";

            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(Enquirycon, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(Enquirycon, intSubscriberEnquiryResultID);

                SqlConnection objConstring = new SqlConnection(eSe.ExtraVarInput1.ToString());

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                if (spr.ReportID > 0)
                {

                    eoConsumerTraceWseManager.BonusSegments = null;

                    // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML 
                    // use this XML data for generating the Report 

                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }
                        else if (sub.PayAsYouGo == 1)
                        {
                            //Calculate The Total cost of the Report , including Bonus Segments

                            Totalcost = spr.UnitPrice;

                            if (eoConsumerTraceWseManager.BonusSegments != null)
                            {

                                foreach (DataRow dr in eoConsumerTraceWseManager.BonusSegments.Tables[0].Rows)
                                {
                                    if (dr["BonusViewed"].ToString().ToLower() == "true")
                                    {
                                        Totalcost = Totalcost + Convert.ToDouble(dr["BonusPrice"].ToString());
                                    }
                                }
                            }
                        }

                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {
                            eoConsumerTraceWseManager.ConsumerID = eSC.KeyID;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.ProductID = eSe.ProductID;
                            eoConsumerTraceWseManager.ReferenceNo = eSC.Reference;
                            eoConsumerTraceWseManager.subscriberID = eSe.SubscriberID;
                            eoConsumerTraceWseManager.TmpReference = eSC.ExtraVarOutput1;
                            moConsumerTraceWseManager.ConnectionString = objConstring;
                            eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                            eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;
                            eoConsumerTraceWseManager.AssociationTypeCode = sub.AssociationTypeCode;
                            eoConsumerTraceWseManager.IDno = eSC.IDNo;
                            eoConsumerTraceWseManager.HomePhoneNumber = eSe.TelephoneNo;

                            XDSPortalEnquiry.Data.Consumer oEnquiryConsumer = new XDSPortalEnquiry.Data.Consumer();
                            oEnquiryConsumer.CheckBlockedStatus(AdminConnection, int.Parse(eSe.SubscriberID.ToString()), int.Parse(eSC.KeyID.ToString()), eSe.ProductID, sub.SubscriberAssociationCode, eSe.ProductID);

                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);

                            string[] Subscribers = ConfigurationManager.AppSettings["RequestOTLSubscribers"].ToString().Split(',');
                            bool SubscriberExists = false;

                            foreach (var Subscriber in Subscribers)
                            {
                                if (sub.SubscriberID.ToString().Trim() == Subscriber.Trim())
                                    SubscriberExists = true;
                            }

                            if (SubscriberExists)
                            {
                                rp = ra.SubmitDIARequestOTLVerification(eoConsumerTraceWseManager);

                                if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                                {
                                    eSe = dSe.ErrorGetSubscriberEnquiryObject(Enquirycon, eSe.SubscriberEnquiryID);
                                    ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                    rp = ra.SubmitDIARequestOTLVerification(eoConsumerTraceWseManager);
                                }
                            }
                            else
                            {
								string DOVUsername = string.Empty;
								string DOVPassword = string.Empty;
								string DOVInstitution = string.Empty;

								if (SubscriberID > 0)
                                {
                                    XDSPortalEnquiry.Data.XDSSettings odSettings = new XDSPortalEnquiry.Data.XDSSettings();
									XDSPortalEnquiry.Entity.DOVSetting dOVSetting = odSettings.GetDOVSSettings(objConstring, SubscriberID);

									DOVUsername = dOVSetting.Username;
									DOVPassword = dOVSetting.Password;
									DOVInstitution = dOVSetting.Institution;
								}

								rp = ra.SubmitDIAVerification(eoConsumerTraceWseManager, Entity, OTLType, DOVUsername, DOVPassword, DOVInstitution);

                                if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                                {
                                    eSe = dSe.ErrorGetSubscriberEnquiryObject(Enquirycon, eSe.SubscriberEnquiryID);
                                    ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                    rp = ra.SubmitDIAVerification(eoConsumerTraceWseManager, Entity, OTLType, DOVUsername, DOVPassword, DOVInstitution);
                                }
                            }

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSC.EnquiryResult = "E";
                                eSe.ErrorDescription = rp.ResponseData.ToString();
                                dSe.UpdateSubscriberEnquiryError(Enquirycon, eSe);
                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                dSe.UpdateSubscriberEnquiryBranchCode(Enquirycon, eSe.SubscriberEnquiryID, rp.ResponseData.ToString());
                                if (string.IsNullOrEmpty(rp.ResponseReferenceNo))
                                {
                                    rp.ResponseData = eSe.SubscriberEnquiryID.ToString();
                                }
                                else
                                {
                                    rp.ResponseData = eSe.SubscriberEnquiryID.ToString() + "-" + rp.ResponseReferenceNo;
                                }
                            }
                        }
                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.

                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                        }
                    }
                    else
                    {
                        // When User want to Re-Open a report 

                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = eSC.XMLData;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                Enquirycon.Close();
                AdminConnection.Close();

                SqlConnection.ClearPool(Enquirycon);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(Enquirycon, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (Enquirycon.State == ConnectionState.Open)
                    Enquirycon.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(Enquirycon);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response GetDOVRequestHistory(SqlConnection con, string Status, int SystemUserID, int SubscriberID)
        {
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            XDSPortalEnquiry.Data.SubscriberEnquiry dSubscriberEnquiry = new XDSPortalEnquiry.Data.SubscriberEnquiry();
            DataSet ds = new DataSet("RequestHistory");

            try
            {
                if (Status == "All")
                    ds = dSubscriberEnquiry.GetDOVSubscriberEnquiryLogDataSet(con, SystemUserID, "194", "", SubscriberID);
                else
                    ds = dSubscriberEnquiry.GetDOVSubscriberEnquiryLogDataSet(con, SystemUserID, "194", Status.Substring(0, 1));

                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;

                ds.DataSetName = "RequestHistory";

                if (ds.Tables.Count > 0)
                    ds.Tables[0].TableName = "HistoryItem";

                rp.ResponseData = ds.GetXml();
            }
            catch (Exception oException)
            {
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();

                SqlConnection.ClearPool(con);
            }

            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitConsumerDigitalVerification(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string strIDNo, string strFirstName, string strSurname, string strCellNo, string strExtRef, string strVoucherCode)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bool bBonusChecking = false;
            string rXml = "";
            int MatchResult = 0;
            int intSubscriberEnquiryResultID = 0;
            DataSet ds = new DataSet();
            Boolean boolSubmitEnquiry = true;
            string strValidationStatus = "";


            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);


            try
            {
                // Check if this subscriber is authorized to use the current product

                if (spr.ReportID > 0)
                {
                    // Check the PayAsYouGo settings for the subscriber
                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {

                        eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                        eSe.SubscriberID = intSubscriberID;

                        eSe.IDNo = strIDNo;
                        eSe.FirstName = strFirstName;
                        eSe.Surname = strSurname;
                        eSe.TelephoneNo = strCellNo;
                        eSe.ProductID = intProductId;
                        eSe.SubscriberEnquiryDate = DateTime.Now;
                        eSe.SubscriberName = strSubscriberName;
                        eSe.SubscriberReference = strExtRef;
                        eSe.SystemUserID = intSystemUserID;
                        eSe.SystemUser = sys.SystemUserFullName;
                        eSe.CreatedByUser = sys.Username;
                        eSe.CreatedOnDate = DateTime.Now;
                        eSe.SearchInput = "";

                        //Generate serach input string

                        if (!string.IsNullOrEmpty(eSe.IDNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.IDNo;
                        }
                        if (!string.IsNullOrEmpty(eSe.FirstName))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.FirstName;
                        }
                        if (!string.IsNullOrEmpty(eSe.Surname))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.Surname;
                        }
                        if (!string.IsNullOrEmpty(eSe.TelephoneNo))
                        {
                            eSe.SearchInput = eSe.SearchInput + " | " + eSe.TelephoneNo;
                        }

                        if (eSe.SearchInput.Length > 0)
                        {
                            eSe.SearchInput = eSe.SearchInput.Substring(3, eSe.SearchInput.Length - 3).ToUpper();
                        }

                        // Log the Current Enquiry to SubscriberEnquiry Table

                        int intSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);

                        eSe.SubscriberEnquiryID = intSubscriberEnquiryID;

                        eSe = dSe.GetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);

                        SqlConnection objConstring = new SqlConnection(eSe.ExtraVarInput1.ToString());

                        eoConsumerTraceWseManager.ConsumerID = eSC.KeyID;
                        eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                        eoConsumerTraceWseManager.ProductID = eSe.ProductID;
                        eoConsumerTraceWseManager.ReferenceNo = eSC.Reference;
                        eoConsumerTraceWseManager.subscriberID = eSe.SubscriberID;
                        eoConsumerTraceWseManager.TmpReference = eSC.ExtraVarOutput1;
                        moConsumerTraceWseManager.ConnectionString = objConstring;
                        eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                        eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;
                        eoConsumerTraceWseManager.AssociationTypeCode = sub.AssociationTypeCode;
                        eoConsumerTraceWseManager.IDno = strIDNo;
                        eoConsumerTraceWseManager.HomePhoneNumber = eSe.TelephoneNo;

                        XDSPortalEnquiry.Data.Consumer oEnquiryConsumer = new XDSPortalEnquiry.Data.Consumer();
                        oEnquiryConsumer.CheckBlockedStatus(AdminConnection, int.Parse(eSe.SubscriberID.ToString()), int.Parse(eSC.KeyID.ToString()), eSe.ProductID, sub.SubscriberAssociationCode, eSe.ProductID);

                        //Check Consent before continuing
                        /////////XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        ////////rp = ra.GetConsent(strFirstName, strSurname);

                        ////////if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        ////////{
                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                        rp = ra.SubmitDIAVerification(eoConsumerTraceWseManager, "", 2, "", "", "");

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.SubmitDIAVerification(eoConsumerTraceWseManager, "", 2, "", "", "");
                        }
                        ////////}

                        //rp = moConsumerTraceWseManager.search(eoConsumerTraceWseManager);
                        rp.EnquiryID = intSubscriberEnquiryID;

                        // Get Response data

                        rXml = rp.ResponseData;

                        System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);


                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                        {
                            eSC.SearchOutput = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.DetailsViewedYN = false;
                            eSC.Billable = false;
                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;

                            rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                            rp.EnquiryID = eSC.SubscriberEnquiryID;
                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSC.EnquiryResult = "E";
                            eSe.ErrorDescription = rp.ResponseData.ToString();
                            dSe.UpdateSubscriberEnquiryError(con, eSe);
                        }

                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report ||
                                 rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single ||
                                 rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                        {
                            dSe.UpdateSubscriberEnquiryBranchCode(con, eSe.SubscriberEnquiryID, rp.ResponseData.ToString());

                            eSC.SearchOutput = "";
                            eSC.IDNo = "";
                            eSC.PassportNo = "";
                            eSC.Surname = "";
                            eSC.FirstName = "";
                            eSC.BirthDate = DateTime.Parse("1900/01/01");
                            eSC.Gender = "";
                            eSC.SearchOutput = "";

                            eSC.IDNo = strIDNo;
                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;

                            eSC.Surname = strSurname.Replace("'", "''");
                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;

                            eSC.FirstName = strFirstName.Replace("'", "''");
                            eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;

                            if (eSC.SearchOutput.Length > 0)
                            {
                                eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                            }

                            eSC.DetailsViewedYN = false;
                            eSC.Billable = false;
                            eSC.CreatedByUser = eSe.CreatedByUser;
                            eSC.CreatedOnDate = DateTime.Now;
                            eSC.KeyID = 0;
                            eSC.KeyType = "D";

                            eSC.BillingTypeID = spr.BillingTypeID;
                            eSC.BillingPrice = spr.UnitPrice;

                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                            eSC.ExtraVarOutput1 = "";
                            eSC.ProductID = intProductId;
                            eSC.VoucherCode = strVoucherCode;

                            rp.EnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);
                            intSubscriberEnquiryResultID = rp.EnquiryResultID;
                            rp.EnquiryID = eSC.SubscriberEnquiryID;

                            eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);
                            // Change the PayAsYouGoEnquiryLimit  after viewing the report

                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                            rp.ResponseData = "Submitted Successfully";

                        }

                    }

                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }
            catch (Exception oException)
            {

                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);

                MatchResult = eSe.SubscriberEnquiryID;
                rp.EnquiryID = eSe.SubscriberEnquiryID;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (oException.Message.Contains("A .NET Framework"))
                {
                    rp.ResponseData = "Error: The System is unable to process this request. Please contact XDS Support";
                }

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            ds.Dispose();
            return rp;
        }
    }
}
