﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace XDSPortalEnquiry.Business
{
   public  class DefaultAlertListing_Commercial
    {
        
        private XDSPortalLibrary.Entity_Layer.CommercialDefaultAlertListing eoDefaultAlertListing;

        public DefaultAlertListing_Commercial()
        {
            
            eoDefaultAlertListing = new XDSPortalLibrary.Entity_Layer.CommercialDefaultAlertListing();
        }

        public XDSPortalLibrary.Entity_Layer.Response LoadCommercialDefaultAlert(SqlConnection con, SqlConnection AdminConnection, SqlConnection SMSConnection, int intSMSProductID, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, string strSubscribername, string strVatno, string stridno, string strPassportNo, string strSurName, string strFirstName, string strSecondName, string strThirdName, string strGender, string strBirthDate, string strAccountNo, string strSubAccountNumber, double dbAmount, string strStatusCode, string strAccountype, string strAddress1, string strAddress2, string strAddress3, string strAddress4, string strPostalcode, string strWorkTelephone, string strHometelephone, string strMobile, string strEmailId, string strEffectivedate, string strcomments, bool bSMSNotification, bool bEmailNotification, bool bClientResponsibleToNotify, string strClientContactDetails, string strAlertType, bool bConsumerNotified, bool bDefaultMoveafter20D, DataSet dsBonusDataSegment)
        {
            string strAdminCon = AdminConnection.ConnectionString;
            string strValidationStatus = string.Empty;
            double Totalcost = 0;
            string rXml = "";
            DataSet ds = new DataSet();

            //string strRegNo = Reg1 + "/" + Reg2 + "/" + Reg3;
            

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();


            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                SqlConnection objConstring = new SqlConnection(eSe.ExtraVarInput1.ToString());

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);
                Entity.SubscriberProductReports sprSMS = xb.GetPrice(AdminConnection, eSe.SubscriberID, 55);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                Data.DefaultAlerts oDefaultAlerts = new XDSPortalEnquiry.Data.DefaultAlerts();

                //XDSPortalEnquiry.Data.SubscriberEnquiryResultBonus odSubscriberEnquiryResultBonus = new XDSPortalEnquiry.Data.SubscriberEnquiryResultBonus();
                //eoDefaultAlertListing.BonusSegments = odSubscriberEnquiryResultBonus.GetSubscriberEnquiryResultBonusDataSet(con, intSubscriberEnquiryResultID);

                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {
                    if ((sprSMS.ReportID > 0 && bSMSNotification == true) || (bSMSNotification == false))
                    {

                        // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML 
                        // use this XML data for generating the Report 
                        if (dsBonusDataSegment != null)
                        {
                            if (dsBonusDataSegment.Tables.Count > 0 && dsBonusDataSegment.Tables[0].Rows.Count > 0)
                            {
                                dsBonusDataSegment.DataSetName = "BonusSegments";
                                dsBonusDataSegment.Tables[0].TableName = "Segment";
                                eoDefaultAlertListing.BonusSegments = dsBonusDataSegment;
                            }
                        }

                        if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                        {
                            if (!string.IsNullOrEmpty(eSC.VoucherCode))
                            {
                                if (bSMSNotification == true)
                                {
                                    throw new Exception("SMS functionality is not available for Voucher Code users");
                                }
                                else
                                {
                                    SubscriberVoucher sv = new SubscriberVoucher();
                                    strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                                    if (strValidationStatus == "")
                                    {
                                        throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                                    }
                                    else if (!(strValidationStatus == "1"))
                                    {
                                        throw new Exception(strValidationStatus);
                                    }
                                }

                            }
                            else if (sub.PayAsYouGo == 1)
                            {
                                //Calculate The Total cost of the Report , including Bonus Segments

                                Totalcost = spr.UnitPrice;
                                if (bSMSNotification == true)
                                {
                                    Totalcost = Totalcost + sprSMS.UnitPrice;
                                }


                                if (eoDefaultAlertListing.BonusSegments != null)
                                {

                                    foreach (DataRow dr in eoDefaultAlertListing.BonusSegments.Tables[0].Rows)
                                    {
                                        if (dr["BonusViewed"].ToString().ToLower() == "true")
                                        {
                                            Totalcost = Totalcost + Convert.ToDouble(dr["BonusPrice"].ToString());
                                        }
                                    }
                                }
                            }


                            if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                            {
                                if (!string.IsNullOrEmpty(strGender))
                                {
                                    if ((strGender.Trim().ToLower().Contains("female")) || (strGender.Trim().ToLower().Contains("f")))
                                    {
                                        strGender = XDSPortalEnquiry.Entity.SubscriberEnquiry.GenderInd.F.ToString();
                                    }
                                    else if ((strGender.Trim().ToLower().Contains("male")) || (strGender.Trim().ToLower().Contains("m")))
                                    {
                                        strGender = XDSPortalEnquiry.Entity.SubscriberEnquiry.GenderInd.M.ToString();
                                    }
                                    else
                                    {
                                        strGender = string.Empty;
                                    }
                                }

                                eSe.FirstInitial = strAlertType;
                                eSe.SecondInitial = bConsumerNotified == false ? "0" : "1";
                                eSe.AccountNo = strEmailId;
                                eSe.BusBusinessName = strMobile;
                                eSe.Age = bSMSNotification ? 1 : 0;
                                eSe.AgeDeviation = bEmailNotification ? 1 : 0;
                                eSe.ExtraVarInput3 = strSubscribername;
                                eSe.AgeSearchTypeInd = bDefaultMoveafter20D ? "1" : "0";

                                dSe.UpdateSubscriberEnquiry(con, eSe);


                                eSC.IDNo = strVatno;
                                eSC.PassportNo = strAccountNo;
                                eSC.Surname = strSurName;
                                eSC.FirstName = strFirstName;
                                eSC.SecondName = strSecondName;
                                eSC.Gender = strGender;
                                //eSC.BusBusinessName = strBusinessName;
                                //eSC.BusRegistrationNo = strRegNo;
                                eSC.Ha_FirstName = strSubAccountNumber;
                                eSC.Ha_Surname = dbAmount.ToString();
                                eSC.FirstInitial = strStatusCode;
                                eSC.SecondInitial = strAccountype;
                                eSC.ExtraVarOutput1 = strcomments;
                                eSC.ExtraVarOutput2 = strEffectivedate;
                                eSC.Ha_IDNo = strMobile;
                                eSC.Ha_DeceasedStatus = strEmailId;
                                eSC.Ha_CauseOfDeath = bEmailNotification.ToString();
                                eSC.Ha_DeceasedDate = bSMSNotification.ToString();
                                

                                dSC.UpdateSubscriberEnquiryResult(con, eSC);

                                int StatusCodeCheck = 0;
                                StatusCodeCheck = oDefaultAlerts.GetStatusCodes(AdminConnection, strStatusCode);

                                if (StatusCodeCheck == 0)
                                {
                                    throw new Exception("Invalid Status Code supplied");
                                }

                                int AccountTypeCheck = 0;
                                AccountTypeCheck = oDefaultAlerts.GetAccountTypes(AdminConnection, strAccountype);
                                if (AccountTypeCheck == 0)
                                {
                                    strAccountype = string.Empty;
                                }


                                eoDefaultAlertListing.idno = stridno;
                                eoDefaultAlertListing.PassportNo = strPassportNo;
                                eoDefaultAlertListing.Surname = strSurName;
                                eoDefaultAlertListing.FirstName = strFirstName;
                                eoDefaultAlertListing.SecondName = strSecondName;
                                eoDefaultAlertListing.ThirdName = strThirdName;
                                eoDefaultAlertListing.BirthDate = strBirthDate;
                                eoDefaultAlertListing.Gender = strGender.ToString();
                                eoDefaultAlertListing.AccountNo = strAccountNo;
                                eoDefaultAlertListing.SubAccountNo = strSubAccountNumber;
                                eoDefaultAlertListing.Accountype = strAccountype;
                                eoDefaultAlertListing.StatusCode = strStatusCode;
                                eoDefaultAlertListing.Effectivedate = strEffectivedate;
                                eoDefaultAlertListing.Amount = dbAmount;
                                eoDefaultAlertListing.comments = strcomments;
                                eoDefaultAlertListing.Hometelephone = strHometelephone;
                                eoDefaultAlertListing.WorkTelephone = strWorkTelephone;
                                eoDefaultAlertListing.Mobile = strMobile;
                                eoDefaultAlertListing.Address1 = strAddress1;
                                eoDefaultAlertListing.Address2 = strAddress2;
                                eoDefaultAlertListing.Address3 = strAddress3;
                                eoDefaultAlertListing.Address4 = strAddress4;
                                eoDefaultAlertListing.EmailId = strEmailId;
                                eoDefaultAlertListing.Postalcode = strPostalcode;
                                eoDefaultAlertListing.Createdbyuser = sys.Username;
                                eoDefaultAlertListing.SystemUserID = sys.SystemUserID;
                                eoDefaultAlertListing.CommercialID = eSC.KeyID;
                                eoDefaultAlertListing.SMSNotification = bSMSNotification;
                                eoDefaultAlertListing.EmailNotification = bEmailNotification;
                                eoDefaultAlertListing.RegistrationNumber = eSC.BusRegistrationNo;
                                eoDefaultAlertListing.CommercialName = eSC.BusBusinessName;
                                eoDefaultAlertListing.VatNo = strVatno;
                                eoDefaultAlertListing.ClientContactDetails = strClientContactDetails;
                                eoDefaultAlertListing.SubscriberName = strSubscribername;
                                eoDefaultAlertListing.AlertType = strAlertType;
                                eoDefaultAlertListing.ConsumerNotified = bConsumerNotified ? "1" : "0";
                                eoDefaultAlertListing.ClientResponsibleToNotify = bClientResponsibleToNotify?"1":"0";
                                eoDefaultAlertListing.DefaultMoveAfter20D = bDefaultMoveafter20D ? "1" : "0";

                                XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);

                                // Submit data for matching 
                                rp = ra.LoadCommercialDefaultAlert(eoDefaultAlertListing);

                                // Get Response data
                                rXml = rp.ResponseData;

                                System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                                if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                                {
                                    eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                    ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                    rp = ra.LoadCommercialDefaultAlert(eoDefaultAlertListing);
                                }

                                rXml = rp.ResponseData;

                                xmlSR = new System.IO.StringReader(rXml);

                                if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                                {

                                    eSC.EnquiryResult = "E";
                                    eSe.ErrorDescription = rp.ResponseData.ToString();
                                    dSe.UpdateSubscriberEnquiryError(con, eSe);
                                }
                                else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                                {

                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;

                                    dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;

                                }

                                else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Bonus)
                                {
                                    ds.ReadXml(xmlSR);
                                    if (ds.Tables.Contains("Segments"))
                                    {

                                        eSC.DetailsViewedYN = false;
                                        eSC.Billable = false;
                                        eSC.BonusIncluded = true;
                                        eSC.KeyID = rp.ResponseKey;
                                        eSC.KeyType = rp.ResponseKeyType;
                                        eSC.XMLBonus = rXml.Replace("'", "''");
                                        eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                        eSC.CreatedByUser = eSe.CreatedByUser;
                                        eSC.CreatedOnDate = DateTime.Now;
                                        eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.B.ToString();

                                        dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryID = intSubscriberEnquiryID;
                                        rp.EnquiryResultID = intSubscriberEnquiryResultID;


                                        foreach (DataRow r in ds.Tables["Segments"].Rows)
                                        {
                                            eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                            eSB.DataSegmentID = int.Parse(r["DataSegmentID"].ToString());
                                            eSB.DataSegmentName = r["DataSegmentName"].ToString().Replace("'", "''");
                                            eSB.DataSegmentDisplayText = r["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                            eSB.BonusViewed = bool.Parse(r["BonusViewed"].ToString());
                                            eSB.Billable = false;
                                            eSB.CreatedByUser = eSe.CreatedByUser;
                                            eSB.CreatedOnDate = DateTime.Now;

                                            dSB.InsertSubscriberEnquiryResultBonus(con, eSB);
                                        }
                                    }
                                }

                                else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                                {
                                    ds.ReadXml(xmlSR);

                                    Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                    Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, eSe.ProductID);

                                    if (eSC.BonusIncluded == true)
                                    {
                                        Data.SubscriberEnquiryResultBonus oSubscriberEnquiryResultBonus = new XDSPortalEnquiry.Data.SubscriberEnquiryResultBonus();
                                        DataSet dsBonus = oSubscriberEnquiryResultBonus.GetSelectedSubscriberEnquiryResultBonus(con, eSC.SubscriberEnquiryResultID);

                                        DataTable dtBonusSelected = new DataTable("ConsumerBonusSelected");

                                        dtBonusSelected.Columns.Add("DataSegmentID", typeof(String));
                                        dtBonusSelected.Columns.Add("DataSegmentName", typeof(String));
                                        dtBonusSelected.Columns.Add("DataSegmentDisplayText", typeof(String));

                                        DataRow drBonusSelected;
                                        if (dsBonus.Tables[0].Rows.Count > 0)
                                        {
                                            foreach (DataRow r in dsBonus.Tables[0].Rows)
                                            {
                                                drBonusSelected = dtBonusSelected.NewRow();
                                                drBonusSelected["DataSegmentID"] = r["DataSegmentID"];
                                                drBonusSelected["DataSegmentName"] = r["DataSegmentName"];
                                                drBonusSelected["DataSegmentDisplayText"] = r["DataSegmentDisplayText"];
                                                dtBonusSelected.Rows.Add(drBonusSelected);
                                            }
                                            ds.Tables.Add(dtBonusSelected);
                                        }
                                        dsBonus.Dispose();
                                    }



                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                    {
                                        eSC.Billable = false;
                                    }
                                    else
                                    {
                                        eSC.Billable = true;
                                    }
                                    eSC.ChangedByUser = eSe.CreatedByUser;
                                    eSC.ChangedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (!(string.IsNullOrEmpty(eSe.BusRegistrationNo)))
                                    {
                                        eSC.BusRegistrationNo = eSe.BusRegistrationNo;
                                        eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusRegistrationNo;
                                    }
                                    if (!(string.IsNullOrEmpty(eSe.BusBusinessName)))
                                    {
                                        eSC.BusBusinessName = eSe.BusBusinessName;
                                        eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusBusinessName;
                                    }

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                    eSC.ProductID = eSe.ProductID;

                                    dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    
                                    if (bSMSNotification == true)
                                    {
                                        XDSPortalLibrary.Entity_Layer.SMSNotification eSMSNotification = new XDSPortalLibrary.Entity_Layer.SMSNotification();
                                        XDSPortalLibrary.Entity_Layer.Response rpSMS = new XDSPortalLibrary.Entity_Layer.Response();

                                        SMSNotification oSMSNotification = new SMSNotification();

                                        eSMSNotification.ClientContactDetails = strClientContactDetails;
                                        eSMSNotification.ContactNo = strMobile;
                                        eSMSNotification.SubscriberName = sub.SubscriberName;
                                        eSMSNotification.IsCommercial = false;
                                        eSMSNotification.AccountNo = strAccountNo;
                                        eSMSNotification.AlertType = strAlertType.ToLower() == "p" ? (bDefaultMoveafter20D == true ? "ListingdefaultMove" : "Listing") : "MoveNotification";

                                        rpSMS = oSMSNotification.SendSMSNotification(con, AdminConnection, SMSConnection, intSMSProductID, eSe.SubscriberID, eSe.SystemUserID, rp.ResponseKey, "Commercial Listing", eSMSNotification);

                                        if ((sub.PayAsYouGo == 1) && (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error))
                                        {
                                            Totalcost = Totalcost - sprSMS.UnitPrice;
                                        }

                                        if (rpSMS.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                                        {
                                            string Status = ds.Tables[0].Rows[0].Field<string>("Status").ToString() + ", Failure Sending SMS";
                                            ds.Tables[0].Rows[0].SetField<string>("Status", Status);
                                        }
                                    }

                                    // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                    if (sub.PayAsYouGo == 1)
                                    {
                                        dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                    }
                                    if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                    {
                                        dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                    }
                                    


                                }

                            }
                            else
                            {
                                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                                rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                            }

                        }
                        else
                        {
                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                            rp.ResponseData = eSC.XMLData;
                            rp.EnquiryID = eSC.SubscriberEnquiryID;
                            rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                            rp.ResponseKey = eSC.KeyID;
                        }
                    }
                    else
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "You are not authorized for sending SMS. Please contact XDS to activate the product";
                    }
                }
                    

                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";

                }
            
                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
                SqlConnection.ClearPool(SMSConnection);


            }

            catch (Exception oException)
            {
                eSC.EnquiryResult = "E";
                eSe.ErrorDescription = rp.ResponseData.ToString();
                dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;
                
                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
                SqlConnection.ClearPool(SMSConnection);
            }
            ds.Dispose();
            return rp;
        }

        public string GetCommercialDefaultAlertHistory(SqlConnection con, SqlConnection AdminConnection, int SubscriberID)
        {
            string strAdminCon = AdminConnection.ConnectionString;
            string strValidationStatus = string.Empty;

            string rXml = "";
            DataSet dsHistory = new DataSet();


            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();


            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();


            try
            {
                //xdsBilling xb = new xdsBilling();
                //Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, SubscriberID, 53);

                Data.DefaultAlerts oDefaultAlerts = new XDSPortalEnquiry.Data.DefaultAlerts();



                //// Check if this subscriber is authorized to use the current product
                //if (spr.ReportID > 0)
                //{
                    dsHistory = oDefaultAlerts.GetCommercialDefaultAlertHistory(con, SubscriberID);
                    rXml = dsHistory.GetXml();

                //}
                //else
                //{
                //    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                //    rp.ResponseData = "<CommercialDefaultAlert><Error>This enquiry Product is not activated in your profile. Please contact XDS to activate the product</Error></CommercialDefaultAlert>";
                //    rXml = rp.ResponseData;
                //}

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }
            catch (Exception oException)
            {
                rXml = "<CommercialDefaultAlert><Error>" + oException.Message + "</Error></CommercialDefaultAlert>";
            }
            dsHistory.Dispose();
            return rXml;

        }

        public XDSPortalLibrary.Entity_Layer.Response RemoveCommercialDefaultAlert(SqlConnection con, SqlConnection AdminConnection, SqlConnection SMSConnection, int intCommercialDefaultAlertID, int intSystemUserID, int intSubscriberID, int intProductID, int intSMSProductID, string strAccountNo, string strContactNo, string strEmailAddress, bool bSMSNotification, bool bEmailnotification, bool bClientResponsibleToNotify, string strClientContactDetails)
        {
            string strAdminCon = AdminConnection.ConnectionString;
            string strValidationStatus = string.Empty;
            double Totalcost = 0;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            XDSPortalEnquiry.Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            XDSPortalEnquiry.Entity.SystemUser sys = dsys.GetSystemUserRecord(strAdminCon, intSystemUserID);

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();


            try
            {

                if (bEmailnotification == false && bSMSNotification == false && bClientResponsibleToNotify == false)
                {
                    throw new Exception("Please select the option I am Responsible for notifying the Client");
                }
                else if (bEmailnotification == true && (Regex.IsMatch(strEmailAddress, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$") == false))
                {
                    throw new Exception("Invalid Email ID supplied");
                }
                else if (bSMSNotification == true && (strContactNo.Length != 10 || (!Regex.IsMatch(strContactNo, "^[0-9]*$"))))
                {
                    throw new Exception("Invalid phone number supplied");
                }
                else if (!String.IsNullOrEmpty(strClientContactDetails) && (Regex.IsMatch(strClientContactDetails, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$") == false) && (strClientContactDetails.Length != 10 || Regex.IsMatch(strClientContactDetails, "^[0-9]*$") == false))
                {
                    throw new Exception("Invalid Client Contact Details supplied! Please Enter a 10 digit Phone Number or a valid Email Address.");
                }

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductID);
                 Entity.SubscriberProductReports sprSMS = xb.GetPrice(AdminConnection, intSubscriberID, intSMSProductID);

                Data.DefaultAlerts oDefaultAlerts = new XDSPortalEnquiry.Data.DefaultAlerts();



                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {
                    if ((bSMSNotification == true && sprSMS.ReportID > 0) || (bSMSNotification == false))
                    {
                        if (sub.PayAsYouGo == 1)
                        {
                            if (bSMSNotification)
                            {
                                Totalcost = sprSMS.UnitPrice;
                            }
                        }

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {
                            int RowsProcessed = oDefaultAlerts.RemoveCommercialDefaultAlert(con, intCommercialDefaultAlertID, sys.SystemUserFullName, intSystemUserID, (bClientResponsibleToNotify ? "1" : "0"));
                            if (RowsProcessed == 0)
                            {
                                rp.ResponseData = "No Record Found with the given CommercialDefaultAlertID";
                            }
                            else if (RowsProcessed > 0)
                            {
                                if (bSMSNotification == true)
                                {
                                    XDSPortalLibrary.Entity_Layer.SMSNotification eSMSNotification = new XDSPortalLibrary.Entity_Layer.SMSNotification();
                                    XDSPortalLibrary.Entity_Layer.Response rpSMS = new XDSPortalLibrary.Entity_Layer.Response();

                                    SMSNotification oSMSNotification = new SMSNotification();

                                    eSMSNotification.ClientContactDetails = strClientContactDetails;
                                    eSMSNotification.ContactNo = strContactNo;
                                    eSMSNotification.SubscriberName = sub.SubscriberName;
                                    eSMSNotification.IsCommercial = true;
                                    eSMSNotification.AccountNo = strAccountNo;
                                    eSMSNotification.AlertType = "Delisting";

                                    rpSMS = oSMSNotification.SendSMSNotification(con, AdminConnection, SMSConnection, intSMSProductID, intSubscriberID, intSystemUserID, intCommercialDefaultAlertID, "Commercial DeListing", eSMSNotification);

                                    if ((sub.PayAsYouGo == 1) && (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error))
                                    {
                                        Totalcost = Totalcost - sprSMS.UnitPrice;
                                    }
                                    if (sub.PayAsYouGo == 1)
                                    {
                                        dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, Totalcost);
                                    }
                                    
                                        rp.ResponseData = "Company delisted successfully";
                                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                                        rp.ResponseKey = intCommercialDefaultAlertID;

                                    if (rpSMS.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                                    {
                                        rp.ResponseData = rp.ResponseData + ", Failure Sending SMS";
                                    }

                                }
                                else
                                {
                                    rp.ResponseData = "Company delisted successfully";
                                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                                    rp.ResponseKey = intCommercialDefaultAlertID;
                                }
                            }

                        }
                        else
                        {
                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        }
                    }
                    else
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "You are not authorized for sending the SMS. Please contact XDS to activate";
                    }


                }

                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "<CommercialDefaultAlert><Error>This enquiry Product is not activated in your profile. Please contact XDS to activate the product</Error></CommercialDefaultAlert>";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
                SqlConnection.ClearPool(SMSConnection);


            }
            catch (Exception oException)
            {
                rp.ResponseData = oException.Message ;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            }
            
            return rp;

        }

        public XDSPortalLibrary.Entity_Layer.Response MoveCommercialDefaultAlert(SqlConnection con, SqlConnection AdminConnection, SqlConnection SMSConnection, int intCommercialDefaultAlertID, int intSystemUserID, int intSubscriberID, int intProductID, int intSMSProductID, string strAccountNo, string strContactNo, string strEmailAddress, bool bSMSNotification, bool bEmailnotification, bool bClientResponsibleToNotify, string strClientContactDetails)
        {
            string strAdminCon = AdminConnection.ConnectionString;
            string strValidationStatus = string.Empty;
            double Totalcost = 0;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            XDSPortalEnquiry.Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            XDSPortalEnquiry.Entity.SystemUser sys = dsys.GetSystemUserRecord(strAdminCon, intSystemUserID);

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();


            try
            {
                //if (bEmailnotification == false && bSMSNotification == false)
                //{
                //    throw new Exception("User needs to be communicated via emial or SMS. Please enter valid details");
                //}
                if (bEmailnotification == false && bSMSNotification == false && bClientResponsibleToNotify == false)
                {
                    throw new Exception("Please select the option I am Responsible for notifying the Client");
                }
                else if (bEmailnotification == true && (Regex.IsMatch(strEmailAddress, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$") == false))
                {
                    throw new Exception("Invalid Email ID supplied");
                }
                else if (bSMSNotification == true && (strContactNo.Length != 10 || (!Regex.IsMatch(strContactNo, "^[0-9]*$"))))
                {
                    throw new Exception("Invalid phone number supplied");
                }
                else if (!String.IsNullOrEmpty(strClientContactDetails) && (Regex.IsMatch(strClientContactDetails, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$") == false) && (strClientContactDetails.Length != 10 || Regex.IsMatch(strClientContactDetails, "^[0-9]*$") == false))
                {
                    throw new Exception("Invalid Client Contact Details supplied! Please Enter a 10 digit Phone Number or a valid Email Address.");
                }

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductID);
                Entity.SubscriberProductReports sprSMS = xb.GetPrice(AdminConnection, intSubscriberID, intSMSProductID);

                Data.DefaultAlerts oDefaultAlerts = new XDSPortalEnquiry.Data.DefaultAlerts();



                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {
                    if ((bSMSNotification == true && sprSMS.ReportID > 0) || (bSMSNotification == false))
                    {
                        if (sub.PayAsYouGo == 1)
                        {
                            if (bSMSNotification)
                            {
                                Totalcost = sprSMS.UnitPrice;
                            }
                        }

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {
                            int RowsProcessed = oDefaultAlerts.MoveCommercialDefaultAlert(con, intCommercialDefaultAlertID, sys.SystemUserFullName, intSystemUserID, strContactNo, strEmailAddress, (bClientResponsibleToNotify ? "1" : "0"));

                            if (RowsProcessed == 0)
                            {
                                rp.ResponseData = "No Record Found with the given CommercialDefaultAlertID";
                            }
                            else if (RowsProcessed > 0)
                            {
                                if (bSMSNotification == true)
                                {
                                    XDSPortalLibrary.Entity_Layer.SMSNotification eSMSNotification = new XDSPortalLibrary.Entity_Layer.SMSNotification();
                                    XDSPortalLibrary.Entity_Layer.Response rpSMS = new XDSPortalLibrary.Entity_Layer.Response();

                                    SMSNotification oSMSNotification = new SMSNotification();

                                    eSMSNotification.ClientContactDetails = strClientContactDetails;
                                    eSMSNotification.ContactNo = strContactNo;
                                    eSMSNotification.SubscriberName = sub.SubscriberName;
                                    eSMSNotification.IsCommercial = true;
                                    eSMSNotification.AccountNo = strAccountNo;
                                    eSMSNotification.AlertType = "MoveNotification";

                                    rpSMS = oSMSNotification.SendSMSNotification(con, AdminConnection, SMSConnection, intSMSProductID, intSubscriberID, intSystemUserID, intCommercialDefaultAlertID, "Commercial Payment Notification moved to Default Listing", eSMSNotification);

                                    if ((sub.PayAsYouGo == 1) && (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error))
                                    {
                                        Totalcost = Totalcost - sprSMS.UnitPrice;
                                    }
                                    if (sub.PayAsYouGo == 1)
                                    {
                                        dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, Totalcost);
                                    }


                                    rp.ResponseData = "Payment Notification moved to Default Listing successfully";
                                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                                    rp.ResponseKey = intCommercialDefaultAlertID;
                                    
                                    if (rpSMS.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                                    {
                                        rp.ResponseData = rp.ResponseData + ", Failure Sending SMS";
                                    }

                                }
                                else
                                {
                                    rp.ResponseData = "Payment Notification moved to Default Listing successfully";
                                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                                    rp.ResponseKey = intCommercialDefaultAlertID;
                                }
                            }

                        }
                        else
                        {
                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        }
                    }
                    else
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "You are not authorized for sending the SMS. Please contact XDS to activate";
                    }


                }

                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "<CommercialDefaultAlert><Error>This enquiry Product is not activated in your profile. Please contact XDS to activate the product</Error></CommercialDefaultAlert>";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
                SqlConnection.ClearPool(SMSConnection);


            }
            catch (Exception oException)
            {
                rp.ResponseData = oException.Message;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            }

            return rp;

        }

    }
}
