﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Xml.Serialization;
using XDSPortalLibrary.Entity_Layer;
using System.Globalization;

namespace XDSPortalEnquiry.Business
{
    public class CommercialEnquiry_BusinessInvestigativeEnquiry
    {
        public void ValidateInput(Entity.SubscriberEnquiryLog Validate)
        {
            if (string.IsNullOrEmpty(Validate.BusBusinessName))
            {
                throw new Exception("Business Name is mandatory");
            }
            else if (string.IsNullOrEmpty(Validate.FirstName) || (string.IsNullOrEmpty(Validate.SurName)) || (string.IsNullOrEmpty(Validate.CompanyName)) || (string.IsNullOrEmpty(Validate.ContactNo)) || (string.IsNullOrEmpty(Validate.EmailAddress)))
            {
                throw new Exception("FirstName, SurName, Company Name, Conatct No, Email Address are Mandatory");
            }
        }
        public XDSPortalLibrary.Entity_Layer.Response SubmitBusinessInvestigativeEnquiry(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strCountry, string strRegNo1, string strRegNo2, string strRegNo3, string strBusinessName, string strSurname, string strFirstName, string strCOmpanyName, string strContactNo, string strEmailAddress, string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode, string strReporttype, string strTimeFrame, string strIDNo, string strAdditionalInfo, string strComapnyContactNo, string strTerms, double damount)
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            int intSubscriberEnquiryLogID = 0;

            int MatchResult = 0;
            DataSet ds = new DataSet();
            string strValidationStatus = "";
            Boolean boolSubmitEnquiry = true;


            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            Entity.SubscriberEnquiryLog sel = new XDSPortalEnquiry.Entity.SubscriberEnquiryLog();
            Data.SubscriberEnquiryLog dsel = new XDSPortalEnquiry.Data.SubscriberEnquiryLog();


            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);


            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            try
            {

                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {

                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }


                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {
                        strRegNo2 = string.IsNullOrEmpty(strRegNo2) ? string.Empty : "/" + strRegNo2;
                        strRegNo3 = string.IsNullOrEmpty(strRegNo3) ? string.Empty : "/" + strRegNo3;

                        sel.BusRegistrationNo = strRegNo1 + strRegNo2 + strRegNo3;
                        sel.BusBusinessName = strBusinessName;
                        sel.FirstName = strFirstName;
                        sel.SurName = strSurname;
                        sel.CompanyName = strCOmpanyName;
                        sel.ContactNo = strContactNo;
                        sel.EmailAddress = strEmailAddress;
                        sel.VoucherCode = strVoucherCode;
                        sel.SubscriberReference = strExtRef;
                        sel.SubscriberID = intSubscriberID;
                        sel.SubscriberName = sub.SubscriberName;
                        sel.SystemUserID = intSystemUserID;
                        sel.SystemUserName = sys.SystemUserFullName;
                        sel.CreatedByUser = sys.Username;
                        sel.CreatedOnDate = DateTime.Now;
                        sel.SubscriberEnquiryDate = DateTime.Now;
                        sel.EnquiryStatus = XDSPortalEnquiry.Entity.SubscriberEnquiryLog.EnquiryStatusInd.P.ToString();
                        sel.ReportType = strReporttype;
                        sel.TimeFrame = strTimeFrame;
                        sel.ExtraVarInput1 = strAdditionalInfo;
                        sel.IDNo = strIDNo;
                        sel.CompanyContactNo = strComapnyContactNo;
                        sel.BillingTypeID = spr.BillingTypeID;
                        sel.Terms = strTerms;
                        sel.Amount = damount;
                        sel.ExtraVarInput2 = strCountry;



                        ValidateInput(sel);

                        // Generate serach input string

                        if (!string.IsNullOrEmpty(sel.BusBusinessName))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.BusBusinessName;
                        }
                        if (!string.IsNullOrEmpty(sel.BusRegistrationNo.Replace("/", "")))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.BusRegistrationNo;
                        }

                        if (sel.Searchinput.Length > 0)
                        {
                            sel.Searchinput = sel.Searchinput.Substring(3, sel.Searchinput.Length - 3).ToUpper();
                        }

                        sel.KeyType = "B";
                        sel.ProductID = intProductId;
                        sel.BillingPrice = spr.UnitPrice;

                        if (sub.PayAsYouGo == 1 || !(strVoucherCode == string.Empty))
                        {
                            sel.Billable = false;
                        }
                        else
                        {
                            sel.Billable = true;
                        }

                        intSubscriberEnquiryLogID = dsel.InsertSubscriberEnquiryLog(con, sel);

                        if (sub.PayAsYouGo == 1)
                        {
                            dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                        }
                        if (!(strVoucherCode == string.Empty))
                        {
                            dSV.UpdateSubscriberVoucher(AdminConnection, strVoucherCode, sys.Username);
                        }

                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = "Enquiry Submitted successfully";
                        rp.EnquiryLogID = intSubscriberEnquiryLogID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }

            catch (Exception oException)
            {
                sel.EnquiryResult = "E";
                sel.ErrorDescription = oException.Message;
                sel.SubscriberEnquiryLogID = dsel.UpdateSubscriberEnquiryLogError(con, sel);

                MatchResult = sel.SubscriberEnquiryLogID;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;
            }

            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitBusinessInvestigativeEnquiry(SqlConnection con, SqlConnection AdminConnection, SqlConnection MTNCommercialQuestionnaire, int intSubscriberID, int intSystemUserID, int intProductId, string strCountry, string strRegNo1, string strRegNo2, string strRegNo3, string strBusinessName, string strSurname, string strFirstName, string strCOmpanyName, string strContactNo, string strEmailAddress, string strExtRef, bool bConfirmationChkBox, bool bBonusChecking, string strVoucherCode, string strReporttype, string strTimeFrame, string strIDNo, string strAdditionalInfo, string strComapnyContactNo, string strTerms, double damount)
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();
            if (MTNCommercialQuestionnaire.State == ConnectionState.Closed)
                MTNCommercialQuestionnaire.Open();

            int intSubscriberEnquiryLogID = 0;

            int MatchResult = 0;
            DataSet ds = new DataSet();
            string strValidationStatus = "";
            Boolean boolSubmitEnquiry = true;


            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            Entity.SubscriberEnquiryLog sel = new XDSPortalEnquiry.Entity.SubscriberEnquiryLog();
            Data.SubscriberEnquiryLog dsel = new XDSPortalEnquiry.Data.SubscriberEnquiryLog();


            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);


            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            try
            {

                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {

                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }


                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {
                        strRegNo2 = string.IsNullOrEmpty(strRegNo2) ? string.Empty : "/" + strRegNo2;
                        strRegNo3 = string.IsNullOrEmpty(strRegNo3) ? string.Empty : "/" + strRegNo3;

                        sel.BusRegistrationNo = strRegNo1 + strRegNo2 + strRegNo3;
                        sel.BusBusinessName = strBusinessName;
                        sel.FirstName = strFirstName;
                        sel.SurName = strSurname;
                        sel.CompanyName = strCOmpanyName;
                        sel.ContactNo = strContactNo;
                        sel.EmailAddress = strEmailAddress;
                        sel.VoucherCode = strVoucherCode;
                        sel.SubscriberReference = strExtRef;
                        sel.SubscriberID = intSubscriberID;
                        sel.SubscriberName = sub.SubscriberName;
                        sel.SystemUserID = intSystemUserID;
                        sel.SystemUserName = sys.SystemUserFullName;
                        sel.CreatedByUser = sys.Username;
                        sel.CreatedOnDate = DateTime.Now;
                        sel.SubscriberEnquiryDate = DateTime.Now;
                        sel.EnquiryStatus = XDSPortalEnquiry.Entity.SubscriberEnquiryLog.EnquiryStatusInd.P.ToString();
                        sel.ReportType = strReporttype;
                        sel.TimeFrame = strTimeFrame;
                        sel.ExtraVarInput1 = strAdditionalInfo;
                        sel.IDNo = strIDNo;
                        sel.CompanyContactNo = strComapnyContactNo;
                        sel.BillingTypeID = spr.BillingTypeID;
                        sel.Terms = strTerms;
                        sel.Amount = damount;
                        sel.ExtraVarInput2 = strCountry;



                        ValidateInput(sel);

                        // Generate serach input string

                        if (!string.IsNullOrEmpty(sel.BusBusinessName))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.BusBusinessName;
                        }
                        if (!string.IsNullOrEmpty(sel.BusRegistrationNo.Replace("/", "")))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.BusRegistrationNo;
                        }

                        if (sel.Searchinput.Length > 0)
                        {
                            sel.Searchinput = sel.Searchinput.Substring(3, sel.Searchinput.Length - 3).ToUpper();
                        }

                        sel.KeyType = "B";
                        sel.ProductID = intProductId;
                        sel.BillingPrice = spr.UnitPrice;

                        if (sub.PayAsYouGo == 1 || !(strVoucherCode == string.Empty))
                        {
                            sel.Billable = false;
                        }
                        else
                        {
                            sel.Billable = true;
                        }

                        //Log enquiry on Commercial Blue
                        Data.BusinessInvestigativeEnquiry bie = new Data.BusinessInvestigativeEnquiry();
                        int EnquiryID = bie.LogCommercialEnquiry(MTNCommercialQuestionnaire, strReporttype, strTimeFrame, strCountry, strEmailAddress, strFirstName, strSurname, strCOmpanyName, strComapnyContactNo, strRegNo1, strRegNo2, strRegNo3, strBusinessName, damount);

                        sel.KeyID = EnquiryID;
                        ////////dsel.UpdateSubscriberEnquiryLog(con, sel);

                        intSubscriberEnquiryLogID = dsel.InsertSubscriberEnquiryLog(con, sel);

                        if (sub.PayAsYouGo == 1)
                        {
                            dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                        }
                        if (!(strVoucherCode == string.Empty))
                        {
                            dSV.UpdateSubscriberVoucher(AdminConnection, strVoucherCode, sys.Username);
                        }

                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = "Enquiry Submitted successfully";
                        rp.EnquiryLogID = intSubscriberEnquiryLogID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                MTNCommercialQuestionnaire.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
                SqlConnection.ClearPool(MTNCommercialQuestionnaire);
            }

            catch (Exception oException)
            {
                sel.EnquiryResult = "E";
                sel.ErrorDescription = oException.Message;
                sel.SubscriberEnquiryLogID = dsel.UpdateSubscriberEnquiryLogError(con, sel);

                MatchResult = sel.SubscriberEnquiryLogID;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                if (MTNCommercialQuestionnaire.State == ConnectionState.Open)
                    MTNCommercialQuestionnaire.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
                SqlConnection.ClearPool(MTNCommercialQuestionnaire);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;
            }

            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitCustomisedBusinessInvestigativeEnquiry(SqlConnection con, SqlConnection AdminConnection, SqlConnection MTNCommercialQuestionnaireCon, int intSubscriberID, int intSystemUserID, int intProductId, CustomisedInvestigativeReport CustomReport)
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            int intSubscriberEnquiryLogID = 0;

            int MatchResult = 0;
            DataSet ds = new DataSet();
            string strValidationStatus = "";
            Boolean boolSubmitEnquiry = true;

            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            Entity.SubscriberEnquiryLog sel = new XDSPortalEnquiry.Entity.SubscriberEnquiryLog();
            Data.SubscriberEnquiryLog dsel = new XDSPortalEnquiry.Data.SubscriberEnquiryLog();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            Data.BusinessInvestigativeEnquiry bie = new Data.BusinessInvestigativeEnquiry();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            try
            {

                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {

                    if (!string.IsNullOrEmpty(CustomReport.VoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, CustomReport.VoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }

                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {
                        string strRegNo2 = string.IsNullOrEmpty(CustomReport.Reg2) ? string.Empty : "/" + CustomReport.Reg2;
                        string strRegNo3 = string.IsNullOrEmpty(CustomReport.Reg3) ? string.Empty : "/" + CustomReport.Reg3;

                        sel.BusRegistrationNo = CustomReport.Reg1 + strRegNo2 + strRegNo3;
                        sel.BusBusinessName = CustomReport.BusinessName;
                        sel.FirstName = CustomReport.FirstName;
                        sel.SurName = CustomReport.SurName;
                        sel.CompanyName = CustomReport.CompanyName;
                        sel.ContactNo = CustomReport.TelNo;
                        sel.EmailAddress = CustomReport.EmailAddress;
                        sel.VoucherCode = CustomReport.VoucherCode;
                        sel.SubscriberReference = CustomReport.YourReference;
                        sel.SubscriberID = intSubscriberID;
                        sel.SubscriberName = sub.SubscriberName;
                        sel.SystemUserID = intSystemUserID;
                        sel.SystemUserName = sys.SystemUserFullName;
                        sel.CreatedByUser = sys.Username;
                        sel.CreatedOnDate = DateTime.Now;
                        sel.SubscriberEnquiryDate = DateTime.Now;
                        sel.EnquiryStatus = XDSPortalEnquiry.Entity.SubscriberEnquiryLog.EnquiryStatusInd.P.ToString();
                        sel.ReportType = CustomReport.ReportType;
                        sel.TimeFrame = CustomReport.TimeFrame;
                        sel.ExtraVarInput1 = CustomReport.AdditionalInfo;
                        sel.IDNo = CustomReport.IDNo;
                        sel.CompanyContactNo = CustomReport.CompanyContactNo;
                        sel.BillingTypeID = spr.BillingTypeID;
                        sel.Terms = CustomReport.Terms;
                        sel.Amount = CustomReport.Amount;
                        sel.ExtraVarInput2 = CustomReport.Country;

                        ValidateInput(sel);

                        // Generate serach input string

                        if (!string.IsNullOrEmpty(sel.BusBusinessName))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.BusBusinessName;
                        }
                        if (!string.IsNullOrEmpty(sel.BusRegistrationNo.Replace("/", "")))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.BusRegistrationNo;
                        }

                        if (sel.Searchinput.Length > 0)
                        {
                            sel.Searchinput = sel.Searchinput.Substring(3, sel.Searchinput.Length - 3).ToUpper();
                        }

                        sel.KeyType = "B";
                        sel.ProductID = intProductId;
                        sel.BillingPrice = spr.UnitPrice;

                        if (sub.PayAsYouGo == 1 || !(CustomReport.VoucherCode == string.Empty))
                        {
                            sel.Billable = false;
                        }
                        else
                        {
                            sel.Billable = true;
                        }

                        intSubscriberEnquiryLogID = dsel.InsertSubscriberEnquiryLog(con, sel);

                        if (sub.PayAsYouGo == 1)
                        {
                            dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                        }
                        if (!(CustomReport.VoucherCode == string.Empty))
                        {
                            dSV.UpdateSubscriberVoucher(AdminConnection, CustomReport.VoucherCode, sys.Username);
                        }

                        int result = 0;

                        if (CustomReport.BankCode != null)
                        {
                            result = bie.InsertCustomisedReportBankCode(AdminConnection, intSubscriberEnquiryLogID, CustomReport.BankCode.BankName, CustomReport.BankCode.Branch, CustomReport.BankCode.BranchNumber, CustomReport.BankCode.AccountNumber, CustomReport.BankCode.AccountName);

                            if (result != 1)
                            {
                                throw new Exception("Could not save Bank Information.");
                            }
                        }

                        if (CustomReport.TradeReferences != null)
                        {
                            foreach (TradeReference traderef in CustomReport.TradeReferences)
                            {
                                result = bie.InsertCustomisedReportTradeReferences(AdminConnection, intSubscriberEnquiryLogID, traderef.Supplier, traderef.SupplierContact);

                                if (result != 1)
                                {
                                    throw new Exception("Could not save Trade References.");
                                }
                            }
                        }

                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = "Enquiry Submitted successfully";
                        rp.EnquiryLogID = intSubscriberEnquiryLogID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }

            catch (Exception oException)
            {
                sel.EnquiryResult = "E";
                sel.ErrorDescription = oException.Message;
                sel.SubscriberEnquiryLogID = dsel.UpdateSubscriberEnquiryLogError(con, sel);

                MatchResult = sel.SubscriberEnquiryLogID;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;
            }

            ds.Dispose();
            return rp;
        }

        //public XDSPortalLibrary.Entity_Layer.Response Validat
         public XDSPortalLibrary.Entity_Layer.Response ValidateCommercialQuestionnaire(XDSPortalLibrary.Entity_Layer.CommercialQuestionnaire oCommercialQuestionnaire)
        {
            DateTime dtdate = new DateTime();
            XDSPortalLibrary.Entity_Layer.Response oResponse = new XDSPortalLibrary.Entity_Layer.Response();
            int i = 0;
            long l = 0;
            float f = 0;
            double d = 0;
             uint ui  = 0;
            if (oCommercialQuestionnaire.CIPC == null)
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Please supply valid CIPC Information";
            }
            else if (oCommercialQuestionnaire.CIPC.CompanyInformation == null)
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Please supply valid CIPC CompanyInformation Information";
            }
            else if (string.IsNullOrEmpty(oCommercialQuestionnaire.CIPC.CompanyInformation.RegisteredName))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "RegisteredName is mandatory.";
            }
            else if (string.IsNullOrEmpty(oCommercialQuestionnaire.CIPC.CompanyInformation.BusinessRegistrationNumber))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "BusinessRegistrationNumber is mandatory.";
            }
            else if (!string.IsNullOrEmpty(oCommercialQuestionnaire.CIPC.CompanyInformation.RegistrationDate) &&
                !DateTime.TryParseExact(oCommercialQuestionnaire.CIPC.CompanyInformation.RegistrationDate, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtdate))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "RegistrationDate should be in the format CCYYMMDD.";
            }
            else if (!string.IsNullOrEmpty(oCommercialQuestionnaire.CIPC.CompanyInformation.BusinessStartDate) &&
                !DateTime.TryParseExact(oCommercialQuestionnaire.CIPC.CompanyInformation.BusinessStartDate, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtdate))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "BusinessStartDate should be in the format CCYYMMDD.";
            }
            else if (oCommercialQuestionnaire.CIPC.CompanyInformation != null && !string.IsNullOrEmpty(oCommercialQuestionnaire.CIPC.CompanyInformation.TaxNumber) &&
                !long.TryParse(oCommercialQuestionnaire.CIPC.CompanyInformation.TaxNumber, out l))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Please supply valid Taxnumber.It should be numbers only";
            }
            else if (!string.IsNullOrEmpty(oCommercialQuestionnaire.CIPC.CompanyInformation.FinancialEffectiveDate) &&
                !DateTime.TryParseExact(oCommercialQuestionnaire.CIPC.CompanyInformation.FinancialEffectiveDate, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtdate))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "FinancialEffectiveDate should be in the format CCYYMMDD.";
            }
            else if (!string.IsNullOrEmpty(oCommercialQuestionnaire.CIPC.CompanyInformation.FinancialYearEnd) &&
              !int.TryParse(oCommercialQuestionnaire.CIPC.CompanyInformation.FinancialYearEnd, out i))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Please supply valid FinancialYearEnd.It should be numbers only";

            }
            else if (!string.IsNullOrEmpty(oCommercialQuestionnaire.CIPC.CompanyInformation.AuthorisedCapitalAmount) &&
              !double.TryParse(oCommercialQuestionnaire.CIPC.CompanyInformation.AuthorisedCapitalAmount,NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out d))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Please supply valid AuthorisedCapitalAmount.It should be numbers only";
            }
            else if (!string.IsNullOrEmpty(oCommercialQuestionnaire.CIPC.CompanyInformation.NumberofAuthorisedShares) &&
             !double.TryParse(oCommercialQuestionnaire.CIPC.CompanyInformation.NumberofAuthorisedShares, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out d))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Please supply valid NumberofAuthorisedShares.It should be numbers only";
            }
            else if (!string.IsNullOrEmpty(oCommercialQuestionnaire.CIPC.CompanyInformation.NumberofIssuedShares) &&
            !double.TryParse(oCommercialQuestionnaire.CIPC.CompanyInformation.NumberofIssuedShares, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out d))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Please supply valid NumberofIssuedShares.It should be numbers only";
            }
            //else if (oCommercialQuestionnaire.CIPC.Auditors != null && string.IsNullOrEmpty(oCommercialQuestionnaire.CIPC.Auditors.AuditorName))
            //{
            //    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            //    oResponse.ResponseData = "AuditorName is mandatory.";
            //}
            else if (oCommercialQuestionnaire.CIPC.Auditors != null && !string.IsNullOrEmpty(oCommercialQuestionnaire.CIPC.Auditors.AuditorStartDate) &&
                !DateTime.TryParseExact(oCommercialQuestionnaire.CIPC.Auditors.AuditorStartDate, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtdate))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "AuditorStartDate should be in the format CCYYMMDD.";
            }
            else if (oCommercialQuestionnaire.CIPC.Auditors != null && !string.IsNullOrEmpty(oCommercialQuestionnaire.CIPC.Auditors.TimewithAuditors) &&
           !int.TryParse(oCommercialQuestionnaire.CIPC.Auditors.TimewithAuditors, out i))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Please supply valid TimewithAuditors.It should be numbers only";
            }
            else if (oCommercialQuestionnaire.CIPC.Principals != null)
            {
                int x = 0;
                foreach (XDSPortalLibrary.Entity_Layer.Principals oPrincipal in oCommercialQuestionnaire.CIPC.Principals)
                {
                    if (string.IsNullOrEmpty(oPrincipal.FirstName))
                    {
                        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        oResponse.ResponseData = "Principal" + x.ToString() + " FirstName is not supplied";
                        break;
                    }
                    else if (string.IsNullOrEmpty(oPrincipal.Surname))
                    {
                        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        oResponse.ResponseData = "Principal" + x.ToString() + " Surname is not supplied";
                        break;
                    }
                    else if (string.IsNullOrEmpty(oPrincipal.IDNumber))
                    {
                        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        oResponse.ResponseData = "Principal" + x.ToString() + " IDNumber is not supplied";
                        break;
                    }
                    else if (!string.IsNullOrEmpty(oPrincipal.AppointmentDate) &&
             !DateTime.TryParseExact(oPrincipal.AppointmentDate, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtdate))
                    {
                        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        oResponse.ResponseData = "Principal" + x.ToString() + " AppointmentDate should be in the format CCYYMMDD";
                        break;
                    }
                    else if (!string.IsNullOrEmpty(oPrincipal.YearswithBusiness) &&
         !int.TryParse(oPrincipal.YearswithBusiness, out i))
                    {
                        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        oResponse.ResponseData = "Principal" + x.ToString() + " Please supply valid YearswithBusiness.It should be numbers only";
                        break;
                    }
                    else if (!string.IsNullOrEmpty(oPrincipal.PercentageMemberShareholding) &&
        !int.TryParse(oPrincipal.PercentageMemberShareholding, out i))
                    {
                        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        oResponse.ResponseData = "Principal" + x.ToString() + " Please supply valid PercentageMemberShareholding.It should be numbers only";
                        break;
                    }
                    x++;
                }
            }


            if (oResponse.ResponseStatus != XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error && 
                oCommercialQuestionnaire.VAT != null && string.IsNullOrEmpty(oCommercialQuestionnaire.VAT.CompanyName))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "CompanyName is mandatory.";
            }
            else if (oResponse.ResponseStatus != XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error && 
                oCommercialQuestionnaire.VAT != null && !string.IsNullOrEmpty(oCommercialQuestionnaire.VAT.VatNumber) &&
         !long.TryParse(oCommercialQuestionnaire.VAT.VatNumber, out l))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Please supply valid VatNumber.It should be numbers only";
            }

            else if (oResponse.ResponseStatus != XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error && 
                oCommercialQuestionnaire.ContatcInfo != null && oCommercialQuestionnaire.ContatcInfo.AddressHistory != null)
            {
                int y = 0;
                foreach (XDSPortalLibrary.Entity_Layer.AddressHistory oAddress in oCommercialQuestionnaire.ContatcInfo.AddressHistory)
                {
                    if (string.IsNullOrEmpty(oAddress.AddressType))
                    {
                        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        oResponse.ResponseData = "Address" + y.ToString() + " AddressType is not supplied";
                        break;
                    }
                    else if (string.IsNullOrEmpty(oAddress.AddressLine1))
                    {
                        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        oResponse.ResponseData = "Address" + y.ToString() + " AddressLine1 is not supplied";
                        break;
                    }

                    y++;
                }
            }

            if (oResponse.ResponseStatus != XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error && 
                oCommercialQuestionnaire.ContatcInfo != null && oCommercialQuestionnaire.ContatcInfo.ContactHistory != null)
            {
                int y = 0;
                foreach (XDSPortalLibrary.Entity_Layer.ContactHistory oContact in oCommercialQuestionnaire.ContatcInfo.ContactHistory)
                {
                    if (string.IsNullOrEmpty(oContact.ContactType))
                    {
                        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        oResponse.ResponseData = "ContactHistory" + y.ToString() + " ContactType is not supplied";
                        break;
                    }
                    else if (string.IsNullOrEmpty(oContact.Details))
                    {
                        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        oResponse.ResponseData = "ContactHistory" + y.ToString() + " Details is not supplied";
                        break;
                    }

                    y++;
                }
            }

            if (oResponse.ResponseStatus != XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error && 
                oCommercialQuestionnaire.CompanyOverview != null)
            {
                int z = 0;
                foreach (XDSPortalLibrary.Entity_Layer.CompanyOverview oCompanyOverview in oCommercialQuestionnaire.CompanyOverview)
                {
                    if (string.IsNullOrEmpty(oCompanyOverview.TypeofStructure))
                    {
                        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        oResponse.ResponseData = "CompanyOverview" + z.ToString() + " TypeofStructure is not supplied";
                        break;
                    }
                    else if (string.IsNullOrEmpty(oCompanyOverview.NumberofStructure))
                    {
                        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        oResponse.ResponseData = "CompanyOverview" + z.ToString() + " NumberofStructure is not supplied";
                        break;
                    }
                    else if (!string.IsNullOrEmpty(oCompanyOverview.NumberofStructure) &&
        !int.TryParse(oCompanyOverview.NumberofStructure, out i))
                    {
                        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        oResponse.ResponseData = "CompanyOverview" + z.ToString() + " Please supply valid NumberofStructure.It should be numbers only";
                        break;
                    }
                    z++;
                }
            }

            if (oResponse.ResponseStatus != XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error && 
                oCommercialQuestionnaire.CompanyStructure != null)
            {
                int z = 0;
                foreach (XDSPortalLibrary.Entity_Layer.CompanyStructure oCompanyStructure in oCommercialQuestionnaire.CompanyStructure)
                {
                    if (string.IsNullOrEmpty(oCompanyStructure.Name))
                    {
                        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        oResponse.ResponseData = "CompanyStructure" + z.ToString() + " Name is not supplied";
                        break;
                    }
                    else if (string.IsNullOrEmpty(oCompanyStructure.PercentageShareholding))
                    {
                        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        oResponse.ResponseData = "CompanyStructure" + z.ToString() + " PercentageShareholding is not supplied";
                        break;
                    }
                    else if (!string.IsNullOrEmpty(oCompanyStructure.PercentageShareholding) &&
        !int.TryParse(oCompanyStructure.PercentageShareholding, out i))
                    {
                        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        oResponse.ResponseData = "CompanyStructure" + z.ToString() + " Please supply valid PercentageShareholding.It should be numbers only";
                        break;
                    }
                    z++;
                }
            }

            if (oResponse.ResponseStatus != XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error && 
                oCommercialQuestionnaire.Employees != null &&
                    string.IsNullOrEmpty(oCommercialQuestionnaire.Employees.NumberofSalariedEmployees) &&
                    string.IsNullOrEmpty(oCommercialQuestionnaire.Employees.NumberofWagedEmployees) &&
                    string.IsNullOrEmpty(oCommercialQuestionnaire.Employees.NumberofCasualEmployees) &&
                    string.IsNullOrEmpty(oCommercialQuestionnaire.Employees.NumberofContractedEmployees))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Atleast one of NumberofSalariedEmployees, NumberofWagedEmployees, NumberofCasualEmployees and NumberofContractedEmployees should be supplied";
            }
            else if (oResponse.ResponseStatus != XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error && 
                oCommercialQuestionnaire.CompanyFleet != null)
            {
                int z = 0;
                foreach (XDSPortalLibrary.Entity_Layer.CompanyFleet oCompanyFleet in oCommercialQuestionnaire.CompanyFleet)
                {
                    if (string.IsNullOrEmpty(oCompanyFleet.Typeofvehicle))
                    {
                        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        oResponse.ResponseData = "CompanyFleet" + z.ToString() + " Typeofvehicle is mandatory";
                    }
                    else if (string.IsNullOrEmpty(oCompanyFleet.Numberofvehicles))
                    {
                        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        oResponse.ResponseData = "CompanyFleet" + z.ToString() + " Numberofvehicles is mandatory";
                    }
                    else if (!string.IsNullOrEmpty(oCompanyFleet.Numberofvehicles) && !int.TryParse(oCompanyFleet.Numberofvehicles, out i))
                    {
                        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        oResponse.ResponseData = "CompanyFleet" + z.ToString() + " Please supply valid Numberofvehicles.It should be numbers only"; ;
                    }
                    else if (!string.IsNullOrEmpty(oCompanyFleet.Value) && !double.TryParse(oCompanyFleet.Value,NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out d))
                    {
                        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        oResponse.ResponseData = "CompanyFleet" + z.ToString() + " Please supply valid Value.It should be numbers only"; ;
                    }
                    else if (!string.IsNullOrEmpty(oCompanyFleet.OutstandingBalance) && !double.TryParse(oCompanyFleet.OutstandingBalance, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture,out d))
                    {
                        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        oResponse.ResponseData = "CompanyFleet" + z.ToString() + " Please supply valid OutstandingBalance.It should be numbers only"; ;
                    }
                    z++;
                }
            }

            //if (oResponse.ResponseStatus != XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error && 
            //    oCommercialQuestionnaire.OperationAssesment != null && string.IsNullOrEmpty(oCommercialQuestionnaire.OperationAssesment.OperationalDetail))
            //{
            //    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            //    oResponse.ResponseData = "OperationalDetail is mandatory.";
            //}
            //else if (oResponse.ResponseStatus != XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error && 
            //    oCommercialQuestionnaire.BEE != null && string.IsNullOrEmpty(oCommercialQuestionnaire.BEE.Level))
            //{
            //    oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            //    oResponse.ResponseData = "Level is mandatory.";
            //}
            else if (oResponse.ResponseStatus != XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error && 
                oCommercialQuestionnaire.ISO != null)
            {
                int z = 0;
                foreach(XDSPortalLibrary.Entity_Layer.ISO oISO in oCommercialQuestionnaire.ISO)
                {
                    if (string.IsNullOrEmpty(oISO.Status))
                    {
                        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        oResponse.ResponseData = "ISO" + z.ToString() + " Status is mandatory";
                    }
                }
            }
            else if (oResponse.ResponseStatus != XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error && 
                oCommercialQuestionnaire.TradeReferences != null)
            {
                foreach(XDSPortalLibrary.Entity_Layer.TradeReferences oTradeReferences in oCommercialQuestionnaire.TradeReferences)
                {
                    if (string.IsNullOrEmpty(oTradeReferences.Supplier))
                    {
                        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        oResponse.ResponseData = "TradeReferences Supplier is mandatory";
                    }
                    else if (string.IsNullOrEmpty(oTradeReferences.SupplierContact))
                    {
                        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        oResponse.ResponseData = "TradeReferences SupplierContact is mandatory";
                    }
                }
            }

            if (oResponse.ResponseStatus != XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error && 
                oCommercialQuestionnaire.Financials != null &&
                string.IsNullOrEmpty( oCommercialQuestionnaire.Financials.Currentassets) &&
                string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.Inventory) &&
                string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.AccountsReceivable) &&
                string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.Fixedassets) &&
                string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.PlantEquipmentVehicles) &&
                string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.Property) &&
                string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.Currentliabilities) &&
                string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.AccountsPayable) &&
                string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.ShortTermsLoans) &&
                string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.LongTermLoans) &&
                string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.Longtermliabilities) &&
                string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.MortgageBonds) &&
                string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.PreviousYearTurnover) &&
                string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.CurrentTurnoverpermonth) &&
                string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.CurrentTurnoverperannum))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Atleast one field should be supplied for Financials Section";
            }
            else if (oResponse.ResponseStatus != XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error && 
                oCommercialQuestionnaire.Financials != null &&
            ( (!string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.Currentassets) && !double.TryParse(oCommercialQuestionnaire.Financials.Currentassets,NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture,out d) )||
              (!string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.Inventory) && !double.TryParse(oCommercialQuestionnaire.Financials.Inventory, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture,out d)) ||
              (!string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.AccountsReceivable) && !double.TryParse(oCommercialQuestionnaire.Financials.AccountsReceivable, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture,out d)) ||
              (!string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.Fixedassets) && !double.TryParse(oCommercialQuestionnaire.Financials.Fixedassets,NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out d) )||
              (!string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.PlantEquipmentVehicles) && !double.TryParse(oCommercialQuestionnaire.Financials.PlantEquipmentVehicles,NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out d)) ||
              (!string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.Property) && !double.TryParse(oCommercialQuestionnaire.Financials.Property, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture,out d) )||
              (!string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.Currentliabilities) && !double.TryParse(oCommercialQuestionnaire.Financials.Currentliabilities,NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out d) )||
              (!string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.AccountsPayable) && !double.TryParse(oCommercialQuestionnaire.Financials.AccountsPayable,NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out d)) ||
              (!string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.ShortTermsLoans) && !double.TryParse(oCommercialQuestionnaire.Financials.ShortTermsLoans,NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out d)) ||
              (!string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.LongTermLoans) && !double.TryParse(oCommercialQuestionnaire.Financials.LongTermLoans, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture,out d)) ||
              (!string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.Longtermliabilities) && !double.TryParse(oCommercialQuestionnaire.Financials.Longtermliabilities,NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out d)) ||
              (!string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.MortgageBonds) && !double.TryParse(oCommercialQuestionnaire.Financials.MortgageBonds, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture,out d)) ||
              (!string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.PreviousYearTurnover) && !double.TryParse(oCommercialQuestionnaire.Financials.PreviousYearTurnover,NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out d)) ||
              (!string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.CurrentTurnoverpermonth) && !double.TryParse(oCommercialQuestionnaire.Financials.CurrentTurnoverpermonth, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture,out d)) ||
              (!string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.CurrentTurnoverperannum) && !double.TryParse(oCommercialQuestionnaire.Financials.CurrentTurnoverperannum,NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out d))))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Data supplied for finanacials should be numeric";
            }
            else if (oResponse.ResponseStatus != XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error && 
                oCommercialQuestionnaire.NewBankCodes != null &&
                (string.IsNullOrEmpty( oCommercialQuestionnaire.NewBankCodes.AccountName) ||
                string.IsNullOrEmpty(oCommercialQuestionnaire.NewBankCodes.Bank) ||
                string.IsNullOrEmpty(oCommercialQuestionnaire.NewBankCodes.BranchName) ||
                string.IsNullOrEmpty(oCommercialQuestionnaire.NewBankCodes.BranchCode) ||
                string.IsNullOrEmpty(oCommercialQuestionnaire.NewBankCodes.AccountNumber) ||
                string.IsNullOrEmpty(oCommercialQuestionnaire.NewBankCodes.Terms)))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "All Data for NewBankCodes should be supplied";
            }
            else if (oResponse.ResponseStatus != XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error && 
                oCommercialQuestionnaire.NewBankCodes != null &&
            ((!string.IsNullOrEmpty(oCommercialQuestionnaire.NewBankCodes.BranchCode) && !FnIsNumericPositive(oCommercialQuestionnaire.NewBankCodes.BranchCode)) ||
              (!string.IsNullOrEmpty(oCommercialQuestionnaire.NewBankCodes.AccountNumber) && !FnIsNumericPositive(oCommercialQuestionnaire.NewBankCodes.AccountNumber))))
            {
                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                oResponse.ResponseData = "Data supplied for AccountNumber and BranchCode should be numeric";
            }
            else if (oResponse.ResponseStatus != XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error && oCommercialQuestionnaire.Contracts != null)
            {
                int z = 0;
                foreach (XDSPortalLibrary.Entity_Layer.Contracts oContracts in oCommercialQuestionnaire.Contracts)
                {
                    if (string.IsNullOrEmpty(oContracts.ContractValue))
                    {
                        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        oResponse.ResponseData = "Contracts ContractValue is mandatory";
                    }
                    else if (string.IsNullOrEmpty(oContracts.MainContractor))
                    {
                        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        oResponse.ResponseData = "Contracts MainContractor is mandatory";
                    }
                    else if (!string.IsNullOrEmpty(oContracts.ContractValue) && !double.TryParse(oContracts.ContractValue,NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out d))
                    {
                        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        oResponse.ResponseData = "Data supplied for ContractValue should be numeric";
                    }
                    z++;
                }
            }
            return oResponse;
        }
        //eCommercialQuestionnaire(XDSPortalLibrary.Entity_Layer.CommercialQuestionnaire oCommercialQuestionnaire)
        //{
        //    DateTime dtdate = new DateTime();
        //    XDSPortalLibrary.Entity_Layer.Response oResponse = new XDSPortalLibrary.Entity_Layer.Response();
        //    int i = 0;
        //    long l = 0;
        //    float f = 0;
        //    double d = 0;
        //    if (oCommercialQuestionnaire.CIPC == null)
        //    {
        //        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //        oResponse.ResponseData = "Please supply valid CIPC Information";
        //    }
        //    else if (oCommercialQuestionnaire.CIPC.CompanyInformation == null)
        //    {
        //        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //        oResponse.ResponseData = "Please supply valid CIPC CompanyInformation Information";
        //    }
        //    else if (string.IsNullOrEmpty(oCommercialQuestionnaire.CIPC.CompanyInformation.RegisteredName))
        //    {
        //        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //        oResponse.ResponseData = "RegisteredName is mandatory.";
        //    }
        //    else if (string.IsNullOrEmpty(oCommercialQuestionnaire.CIPC.CompanyInformation.BusinessRegistrationNumber))
        //    {
        //        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //        oResponse.ResponseData = "BusinessRegistrationNumber is mandatory.";
        //    }
        //    else if (!string.IsNullOrEmpty(oCommercialQuestionnaire.CIPC.CompanyInformation.RegistrationDate) &&
        //        !DateTime.TryParseExact(oCommercialQuestionnaire.CIPC.CompanyInformation.RegistrationDate, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtdate))
        //    {
        //        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //        oResponse.ResponseData = "RegistrationDate should be in the format CCYYMMDD.";
        //    }
        //    else if (!string.IsNullOrEmpty(oCommercialQuestionnaire.CIPC.CompanyInformation.BusinessStartDate) &&
        //        !DateTime.TryParseExact(oCommercialQuestionnaire.CIPC.CompanyInformation.BusinessStartDate, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtdate))
        //    {
        //        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //        oResponse.ResponseData = "BusinessStartDate should be in the format CCYYMMDD.";
        //    }
        //    else if (oCommercialQuestionnaire.CIPC.CompanyInformation != null && !string.IsNullOrEmpty(oCommercialQuestionnaire.CIPC.CompanyInformation.TaxNumber) &&
        //        !long.TryParse(oCommercialQuestionnaire.CIPC.CompanyInformation.TaxNumber, out l))
        //    {
        //        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //        oResponse.ResponseData = "Please supply valid Taxnumber.It should be numbers only";
        //    }
        //    else if (!string.IsNullOrEmpty(oCommercialQuestionnaire.CIPC.CompanyInformation.FinancialEffectiveDate) &&
        //        !DateTime.TryParseExact(oCommercialQuestionnaire.CIPC.CompanyInformation.FinancialEffectiveDate, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtdate))
        //    {
        //        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //        oResponse.ResponseData = "FinancialEffectiveDate should be in the format CCYYMMDD.";
        //    }
        //    else if (!string.IsNullOrEmpty(oCommercialQuestionnaire.CIPC.CompanyInformation.FinancialYearEnd) &&
        //      !int.TryParse(oCommercialQuestionnaire.CIPC.CompanyInformation.FinancialYearEnd, out i))
        //    {
        //        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //        oResponse.ResponseData = "Please supply valid FinancialYearEnd.It should be numbers only";

        //    }
        //    else if (!string.IsNullOrEmpty(oCommercialQuestionnaire.CIPC.CompanyInformation.AuthorisedCapitalAmount) &&
        //      !double.TryParse(oCommercialQuestionnaire.CIPC.CompanyInformation.AuthorisedCapitalAmount,NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out d))
        //    {
        //        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //        oResponse.ResponseData = "Please supply valid AuthorisedCapitalAmount.It should be numbers only";
        //    }
        //    else if (!string.IsNullOrEmpty(oCommercialQuestionnaire.CIPC.CompanyInformation.NumberofAuthorisedShares) &&
        //     !double.TryParse(oCommercialQuestionnaire.CIPC.CompanyInformation.NumberofAuthorisedShares, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out d))
        //    {
        //        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //        oResponse.ResponseData = "Please supply valid NumberofAuthorisedShares.It should be numbers only";
        //    }
        //    else if (!string.IsNullOrEmpty(oCommercialQuestionnaire.CIPC.CompanyInformation.NumberofIssuedShares) &&
        //    !double.TryParse(oCommercialQuestionnaire.CIPC.CompanyInformation.NumberofIssuedShares, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out d))
        //    {
        //        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //        oResponse.ResponseData = "Please supply valid NumberofIssuedShares.It should be numbers only";
        //    }
        //    else if (oCommercialQuestionnaire.CIPC.Auditors != null && string.IsNullOrEmpty(oCommercialQuestionnaire.CIPC.Auditors.AuditorName))
        //    {
        //        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //        oResponse.ResponseData = "AuditorName is mandatory.";
        //    }
        //    else if (oCommercialQuestionnaire.CIPC.Auditors != null && !string.IsNullOrEmpty(oCommercialQuestionnaire.CIPC.Auditors.AuditorStartDate) &&
        //        !DateTime.TryParseExact(oCommercialQuestionnaire.CIPC.Auditors.AuditorStartDate, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtdate))
        //    {
        //        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //        oResponse.ResponseData = "AuditorStartDate should be in the format CCYYMMDD.";
        //    }
        //    else if (oCommercialQuestionnaire.CIPC.Auditors != null && !string.IsNullOrEmpty(oCommercialQuestionnaire.CIPC.Auditors.TimewithAuditors) &&
        //   !int.TryParse(oCommercialQuestionnaire.CIPC.Auditors.TimewithAuditors, out i))
        //    {
        //        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //        oResponse.ResponseData = "Please supply valid TimewithAuditors.It should be numbers only";
        //    }
        //    else if (oCommercialQuestionnaire.CIPC.Principals != null)
        //    {
        //        int x = 0;
        //        //foreach (XDSPortalLibrary.Entity_Layer.CommercialQuestionnaireCIPCPrincipals oPrincipal in oCommercialQuestionnaire.CIPC.Principals)
        //        //{
        //            if (string.IsNullOrEmpty(oCommercialQuestionnaire.CIPC.Principals.Principals.FirstName))
        //            {
        //                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //                oResponse.ResponseData = "Principal" + x.ToString() + " FirstName is not supplied";
        //               // break;
        //            }
        //            else if (string.IsNullOrEmpty(oCommercialQuestionnaire.CIPC.Principals.Principals.Surname))
        //            {
        //                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //                oResponse.ResponseData = "Principal" + x.ToString() + " Surname is not supplied";
        //               // break;
        //            }
        //            else if (string.IsNullOrEmpty(oCommercialQuestionnaire.CIPC.Principals.Principals.IDNumber))
        //            {
        //                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //                oResponse.ResponseData = "Principal" + x.ToString() + " IDNumber is not supplied";
        //               // break;
        //            }
        //            else if (!string.IsNullOrEmpty(oCommercialQuestionnaire.CIPC.Principals.Principals.AppointmentDate) &&
        //     !DateTime.TryParseExact(oCommercialQuestionnaire.CIPC.Principals.Principals.AppointmentDate, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dtdate))
        //            {
        //                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //                oResponse.ResponseData = "Principal" + x.ToString() + " AppointmentDate should be in the format CCYYMMDD";
        //              //  break;
        //            }
        //            else if (!string.IsNullOrEmpty(oCommercialQuestionnaire.CIPC.Principals.Principals.YearswithBusiness) &&
        // !int.TryParse(oCommercialQuestionnaire.CIPC.Principals.Principals.YearswithBusiness, out i))
        //            {
        //                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //                oResponse.ResponseData = "Principal" + x.ToString() + " Please supply valid YearswithBusiness.It should be numbers only";
        //              //  break;
        //            }
        //            else if (!string.IsNullOrEmpty(oCommercialQuestionnaire.CIPC.Principals.Principals.PercentageMemberShareholding) &&
        //!int.TryParse(oCommercialQuestionnaire.CIPC.Principals.Principals.PercentageMemberShareholding, out i))
        //            {
        //                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //                oResponse.ResponseData = "Principal" + x.ToString() + " Please supply valid PercentageMemberShareholding.It should be numbers only";
        //               // break;
        //            }
        //            x++;
        //        }
        //   // }


        // //   if (oResponse.ResponseStatus != XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error && 
        // //       oCommercialQuestionnaire.VAT != null && string.IsNullOrEmpty(oCommercialQuestionnaire.VAT.CompanyName))
        // //   {
        // //       oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        // //       oResponse.ResponseData = "CompanyName is mandatory.";
        // //   }
        // //   else if (oResponse.ResponseStatus != XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error && 
        // //       oCommercialQuestionnaire.VAT != null && !string.IsNullOrEmpty(oCommercialQuestionnaire.VAT.VatNumber) &&
        // //!long.TryParse(oCommercialQuestionnaire.VAT.VatNumber, out l))
        // //   {
        // //       oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        // //       oResponse.ResponseData = "Please supply valid VatNumber.It should be numbers only";
        // //   }

        // //   else if (oResponse.ResponseStatus != XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error && 
        // //       oCommercialQuestionnaire.ContatcInfo != null && oCommercialQuestionnaire.ContatcInfo.AddressHistory != null)
        // //   {
        // //       int y = 0;
        // //       foreach (XDSPortalLibrary.Entity_Layer.AddressHistory oAddress in oCommercialQuestionnaire.ContatcInfo.AddressHistory)
        // //       {
        // //           if (string.IsNullOrEmpty(oAddress.AddressType))
        // //           {
        // //               oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        // //               oResponse.ResponseData = "Address" + y.ToString() + " AddressType is not supplied";
        // //               break;
        // //           }
        // //           else if (string.IsNullOrEmpty(oAddress.AddressLine1))
        // //           {
        // //               oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        // //               oResponse.ResponseData = "Address" + y.ToString() + " AddressLine1 is not supplied";
        // //               break;
        // //           }

        // //           y++;
        // //       }
        // //   }

        // //   if (oResponse.ResponseStatus != XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error && 
        // //       oCommercialQuestionnaire.ContatcInfo != null && oCommercialQuestionnaire.ContatcInfo.ContactHistory != null)
        // //   {
        // //       int y = 0;
        // //       foreach (XDSPortalLibrary.Entity_Layer.ContactHistory oContact in oCommercialQuestionnaire.ContatcInfo.ContactHistory)
        // //       {
        // //           if (string.IsNullOrEmpty(oContact.ContactType))
        // //           {
        // //               oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        // //               oResponse.ResponseData = "ContactHistory" + y.ToString() + " ContactType is not supplied";
        // //               break;
        // //           }
        // //           else if (string.IsNullOrEmpty(oContact.Details))
        // //           {
        // //               oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        // //               oResponse.ResponseData = "ContactHistory" + y.ToString() + " Details is not supplied";
        // //               break;
        // //           }

        // //           y++;
        // //       }
        // //   }

        //    if (oResponse.ResponseStatus != XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error && 
        //        oCommercialQuestionnaire.CompanyOverview != null)
        //    {
        //        int z = 0;
        //        //foreach (XDSPortalLibrary.Entity_Layer.CompanyOverview oCompanyOverview in oCommercialQuestionnaire.CompanyOverview)
        //        //{
        //        if (string.IsNullOrEmpty(oCommercialQuestionnaire.CompanyOverview.CompanyOverview.TypeofStructure))
        //            {
        //                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //                oResponse.ResponseData = "CompanyOverview" + z.ToString() + " TypeofStructure is not supplied";
        //               // break;
        //            }
        //        else if (string.IsNullOrEmpty(oCommercialQuestionnaire.CompanyOverview.CompanyOverview.NumberofStructure))
        //            {
        //                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //                oResponse.ResponseData = "CompanyOverview" + z.ToString() + " NumberofStructure is not supplied";
        //               // break;
        //            }
        //        else if (!string.IsNullOrEmpty(oCommercialQuestionnaire.CompanyOverview.CompanyOverview.NumberofStructure) &&
        //!int.TryParse(oCommercialQuestionnaire.CompanyOverview.CompanyOverview.NumberofStructure, out i))
        //            {
        //                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //                oResponse.ResponseData = "CompanyOverview" + z.ToString() + " Please supply valid NumberofStructure.It should be numbers only";
        //              //  break;
        //            }
        //            z++;
        //       // }
        //    }

        //    if (oResponse.ResponseStatus != XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error && 
        //        oCommercialQuestionnaire.CompanyStructure != null)
        //    {
        //        int z = 0;
        //        //foreach (XDSPortalLibrary.Entity_Layer.CompanyStructure oCompanyStructure in oCommercialQuestionnaire.CompanyStructure)
        //        //{
        //        if (string.IsNullOrEmpty(oCommercialQuestionnaire.CompanyStructure.CompanyStructure.Name))
        //            {
        //                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //                oResponse.ResponseData = "CompanyStructure" + z.ToString() + " Name is not supplied";
        //                //break;
        //            }
        //        else if (string.IsNullOrEmpty(oCommercialQuestionnaire.CompanyStructure.CompanyStructure.PercentageShareholding))
        //            {
        //                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //                oResponse.ResponseData = "CompanyStructure" + z.ToString() + " PercentageShareholding is not supplied";
        //                //break;
        //            }
        //        else if (!string.IsNullOrEmpty(oCommercialQuestionnaire.CompanyStructure.CompanyStructure.PercentageShareholding) &&
        //!int.TryParse(oCommercialQuestionnaire.CompanyStructure.CompanyStructure.PercentageShareholding, out i))
        //            {
        //                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //                oResponse.ResponseData = "CompanyStructure" + z.ToString() + " Please supply valid PercentageShareholding.It should be numbers only";
        //               // break;
        //            }
        //            z++;
        //       // }
        //    }

        //    if (oResponse.ResponseStatus != XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error && 
        //        oCommercialQuestionnaire.Employees != null &&
        //            string.IsNullOrEmpty(oCommercialQuestionnaire.Employees.NumberofSalariedEmployees) &&
        //            string.IsNullOrEmpty(oCommercialQuestionnaire.Employees.NumberofWagedEmployees) &&
        //            string.IsNullOrEmpty(oCommercialQuestionnaire.Employees.NumberofCasualEmployees) &&
        //            string.IsNullOrEmpty(oCommercialQuestionnaire.Employees.NumberofContractedEmployees))
        //    {
        //        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //        oResponse.ResponseData = "Atleast one of NumberofSalariedEmployees, NumberofWagedEmployees, NumberofCasualEmployees and NumberofContractedEmployees should be supplied";
        //    }
        //    else if (oResponse.ResponseStatus != XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error && 
        //        oCommercialQuestionnaire.CompanyFleet != null)
        //    {
        //        int z = 0;
        //        //foreach (XDSPortalLibrary.Entity_Layer.CompanyFleet oCompanyFleet in oCommercialQuestionnaire.CompanyFleet)
        //        //{
        //            if (string.IsNullOrEmpty(oCommercialQuestionnaire.CompanyFleet.CompanyFleet.Typeofvehicle))
        //            {
        //                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //                oResponse.ResponseData = "CompanyFleet" + z.ToString() + " Typeofvehicle is mandatory";
        //            }
        //            else if (string.IsNullOrEmpty(oCommercialQuestionnaire.CompanyFleet.CompanyFleet.Numberofvehicles))
        //            {
        //                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //                oResponse.ResponseData = "CompanyFleet" + z.ToString() + " Numberofvehicles is mandatory";
        //            }
        //            else if (!string.IsNullOrEmpty(oCommercialQuestionnaire.CompanyFleet.CompanyFleet.Numberofvehicles) && !int.TryParse(oCommercialQuestionnaire.CompanyFleet.CompanyFleet.Numberofvehicles, out i))
        //            {
        //                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //                oResponse.ResponseData = "CompanyFleet" + z.ToString() + " Please supply valid Numberofvehicles.It should be numbers only"; ;
        //            }
        //            else if (!string.IsNullOrEmpty(oCommercialQuestionnaire.CompanyFleet.CompanyFleet.Value) && !int.TryParse(oCommercialQuestionnaire.CompanyFleet.CompanyFleet.Value, out i))
        //            {
        //                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //                oResponse.ResponseData = "CompanyFleet" + z.ToString() + " Please supply valid Value.It should be numbers only"; ;
        //            }
        //            else if (!string.IsNullOrEmpty(oCommercialQuestionnaire.CompanyFleet.CompanyFleet.OutstandingBalance) && !int.TryParse(oCommercialQuestionnaire.CompanyFleet.CompanyFleet.OutstandingBalance, out i))
        //            {
        //                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //                oResponse.ResponseData = "CompanyFleet" + z.ToString() + " Please supply valid OutstandingBalance.It should be numbers only"; ;
        //            }
        //            z++;
        //       // }
        //    }

        //    if (oResponse.ResponseStatus != XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error && 
        //        oCommercialQuestionnaire.OperationAssesment != null && string.IsNullOrEmpty(oCommercialQuestionnaire.OperationAssesment.OperationalDetail))
        //    {
        //        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //        oResponse.ResponseData = "OperationalDetail is mandatory.";
        //    }
        //    else if (oResponse.ResponseStatus != XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error && 
        //        oCommercialQuestionnaire.BEE != null && string.IsNullOrEmpty(oCommercialQuestionnaire.BEE.Level))
        //    {
        //        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //        oResponse.ResponseData = "Level is mandatory.";
        //    }
        //    else if (oResponse.ResponseStatus != XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error && 
        //        oCommercialQuestionnaire.ISO != null)
        //    {
        //        int z = 0;
        //        //foreach(XDSPortalLibrary.Entity_Layer.CommercialQuestionnaireISO oISO in oCommercialQuestionnaire.ISO)
        //        //{
        //        if (string.IsNullOrEmpty(oCommercialQuestionnaire.ISO.ISO.Status))
        //            {
        //                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //                oResponse.ResponseData = "ISO" + z.ToString() + " Status is mandatory";
        //            }
        //       // }
        //    }
        //    else if (oResponse.ResponseStatus != XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error && 
        //        oCommercialQuestionnaire.TradeReferences != null)
        //    {
        //        //foreach(XDSPortalLibrary.Entity_Layer.CommercialQuestionnaireTradeReferences oTradeReferences in oCommercialQuestionnaire.TradeReferences)
        //        //{
        //        if (string.IsNullOrEmpty(oCommercialQuestionnaire.TradeReferences.TradeReferences.Supplier))
        //            {
        //                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //                oResponse.ResponseData = "TradeReferences Supplier is mandatory";
        //            }
        //        else if (string.IsNullOrEmpty(oCommercialQuestionnaire.TradeReferences.TradeReferences.SupplierContact))
        //            {
        //                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //                oResponse.ResponseData = "TradeReferences SupplierContact is mandatory";
        //            }
        //        //}
        //    }

        //    if (oResponse.ResponseStatus != XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error && 
        //        oCommercialQuestionnaire.Financials != null &&
        //        string.IsNullOrEmpty( oCommercialQuestionnaire.Financials.Currentassets) &&
        //        string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.Inventory) &&
        //        string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.AccountsReceivable) &&
        //        string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.Fixedassets) &&
        //        string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.PlantEquipmentVehicles) &&
        //        string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.Property) &&
        //        string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.Currentliabilities) &&
        //        string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.AccountsPayable) &&
        //        string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.ShortTermsLoans) &&
        //        string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.LongTermLoans) &&
        //        string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.Longtermliabilities) &&
        //        string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.MortgageBonds) &&
        //        string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.PreviousYearTurnover) &&
        //        string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.CurrentTurnoverpermonth) &&
        //        string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.CurrentTurnoverperannum))
        //    {
        //        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //        oResponse.ResponseData = "Atleast one field should be supplied for Financials Section";
        //    }
        //    else if (oResponse.ResponseStatus != XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error && 
        //        oCommercialQuestionnaire.Financials != null &&
        //    ( (!string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.Currentassets) && !int.TryParse(oCommercialQuestionnaire.Financials.Currentassets,out i) )||
        //      (!string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.Inventory) && !int.TryParse(oCommercialQuestionnaire.Financials.Inventory, out i)) ||
        //      (!string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.AccountsReceivable) && !int.TryParse(oCommercialQuestionnaire.Financials.AccountsReceivable, out i)) ||
        //      (!string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.Fixedassets) && !int.TryParse(oCommercialQuestionnaire.Financials.Fixedassets, out i) )||
        //      (!string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.PlantEquipmentVehicles) && !int.TryParse(oCommercialQuestionnaire.Financials.PlantEquipmentVehicles, out i)) ||
        //      (!string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.Property) && !int.TryParse(oCommercialQuestionnaire.Financials.Property, out i) )||
        //      (!string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.Currentliabilities) && !int.TryParse(oCommercialQuestionnaire.Financials.Currentliabilities, out i) )||
        //      (!string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.AccountsPayable) && !int.TryParse(oCommercialQuestionnaire.Financials.AccountsPayable, out i)) ||
        //      (!string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.ShortTermsLoans) && !int.TryParse(oCommercialQuestionnaire.Financials.ShortTermsLoans, out i)) ||
        //      (!string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.LongTermLoans) && !int.TryParse(oCommercialQuestionnaire.Financials.LongTermLoans, out i)) ||
        //      (!string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.Longtermliabilities) && !int.TryParse(oCommercialQuestionnaire.Financials.Longtermliabilities, out i)) ||
        //      (!string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.MortgageBonds) && !int.TryParse(oCommercialQuestionnaire.Financials.MortgageBonds, out i)) ||
        //      (!string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.PreviousYearTurnover) && !int.TryParse(oCommercialQuestionnaire.Financials.PreviousYearTurnover, out i)) ||
        //      (!string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.CurrentTurnoverpermonth) && !int.TryParse(oCommercialQuestionnaire.Financials.CurrentTurnoverpermonth, out i)) ||
        //      (!string.IsNullOrEmpty(oCommercialQuestionnaire.Financials.CurrentTurnoverperannum) && !int.TryParse(oCommercialQuestionnaire.Financials.CurrentTurnoverperannum, out i))))
        //    {
        //        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //        oResponse.ResponseData = "Data supplied for finanacials should be numeric";
        //    }
        //    else if (oResponse.ResponseStatus != XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error && 
        //        oCommercialQuestionnaire.NewBankCodes != null &&
        //        (string.IsNullOrEmpty( oCommercialQuestionnaire.NewBankCodes.AccountName) ||
        //        string.IsNullOrEmpty(oCommercialQuestionnaire.NewBankCodes.Bank) ||
        //        string.IsNullOrEmpty(oCommercialQuestionnaire.NewBankCodes.BranchName) ||
        //        string.IsNullOrEmpty(oCommercialQuestionnaire.NewBankCodes.BranchCode) ||
        //        string.IsNullOrEmpty(oCommercialQuestionnaire.NewBankCodes.AccountNumber)))
        //    {
        //        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //        oResponse.ResponseData = "All Data for NewBankCodes should be supplied";
        //    }
        //    else if (oResponse.ResponseStatus != XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error && 
        //        oCommercialQuestionnaire.NewBankCodes != null &&
        //    ((!string.IsNullOrEmpty(oCommercialQuestionnaire.NewBankCodes.BranchCode) && !int.TryParse(oCommercialQuestionnaire.NewBankCodes.BranchCode, out i)) ||
        //      (!string.IsNullOrEmpty(oCommercialQuestionnaire.NewBankCodes.AccountNumber) && !int.TryParse(oCommercialQuestionnaire.NewBankCodes.AccountNumber, out i))))
        //    {
        //        oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //        oResponse.ResponseData = "Data supplied for AccountNumber and BranchCode should be numeric";
        //    }
        //    else if (oResponse.ResponseStatus != XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error && oCommercialQuestionnaire.Contracts != null)
        //    {
        //        int z = 0;
        //        //foreach (XDSPortalLibrary.Entity_Layer.Contracts oContracts in oCommercialQuestionnaire.Contracts)
        //        //{
        //        if (string.IsNullOrEmpty(oCommercialQuestionnaire.Contracts.Contracts.ContractValue))
        //            {
        //                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //                oResponse.ResponseData = "Contracts ContractValue is mandatory";
        //            }
        //        else if (string.IsNullOrEmpty(oCommercialQuestionnaire.Contracts.Contracts.MainContractor))
        //            {
        //                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //                oResponse.ResponseData = "Contracts MainContractor is mandatory";
        //            }
        //        else if (!string.IsNullOrEmpty(oCommercialQuestionnaire.Contracts.Contracts.ContractValue) && !long.TryParse(oCommercialQuestionnaire.Contracts.Contracts.ContractValue, out l))
        //            {
        //                oResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //                oResponse.ResponseData = "Data supplied for ContractValue should be numeric";
        //            }
        //            z++;
        //       // }
        //    }
        //    return oResponse;
        //}

        public CommercialQuestionnaireResult SubmitCommercialQuestionnaire(SqlConnection Enquirycon, SqlConnection AdminConnection, SqlConnection LogConnection, XDSPortalLibrary.Entity_Layer.CommercialQuestionnaire oCommercialQuestionnaire, string strSubscriberReference, int intsubscriberID, int intSystemUserID, int intProductID, string strVoucherCode)
        {
            string strAdminCon = AdminConnection.ConnectionString;



            if (Enquirycon.State == ConnectionState.Closed)
                Enquirycon.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();


            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            Data.BusinessInvestigativeEnquiry oInvestigative = new Data.BusinessInvestigativeEnquiry();
            int Reference = 0;
            bool boolSubmitEnquiry = true;
            string strValidationStatus = string.Empty;

            CommercialQuestionnaireResult oResponse = new CommercialQuestionnaireResult();
            try
            {
               
                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intsubscriberID, intProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intsubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);



                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {
                    //  if (!string.IsNullOrEmpty(strVoucherCode))
                    //{
                    //    SubscriberVoucher sv = new SubscriberVoucher();
                    //    strValidationStatus = sv.ValidationStatus(AdminConnection, intsubscriberID, strVoucherCode);

                    //    if (strValidationStatus == "")
                    //    {
                    //        throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                    //    }
                    //    else if (!(strValidationStatus == "1"))
                    //    {
                    //        throw new Exception(strValidationStatus);
                    //    }


                    //}
                    //else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    //{
                    //    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    //    rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                    //    boolSubmitEnquiry = false;

                    //}
                      if (boolSubmitEnquiry)
                      {
                          rp =  ValidateCommercialQuestionnaire(oCommercialQuestionnaire);

                          if (rp.ResponseStatus != XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                          {
                              XmlSerializer serializer = new XmlSerializer(oCommercialQuestionnaire.GetType());
                              System.IO.StringWriter sw = new System.IO.StringWriter();
                              serializer.Serialize(sw, oCommercialQuestionnaire);
                              System.IO.StringReader reader = new System.IO.StringReader(sw.ToString());

                              Reference = oInvestigative.LogCommercialQuestionnaire(LogConnection, oCommercialQuestionnaire, sub.SubscriberID, oCommercialQuestionnaire.CIPC.CompanyInformation.BusinessRegistrationNumber,
                              oCommercialQuestionnaire.CIPC.CompanyInformation.RegisteredName, reader.ReadToEnd(), strSubscriberReference, sub.SubscriberName, string.Empty,
                              string.Empty, sub.CompanyTelephoneCode + sub.CompanyTelephoneNo, sys.FirstName + " " + sys.Surname, string.Empty, sys.Username);

                              if (sub.PayAsYouGo == 1)
                              {
                                  dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intsubscriberID, spr.UnitPrice);
                              }
                              if (!(strVoucherCode == string.Empty))
                              {
                                  dSV.UpdateSubscriberVoucher(AdminConnection, strVoucherCode, sys.Username);
                              }
                              rp.EnquiryLogID = Reference;

                              oResponse.Result = "Information successfully saved";
                              oResponse.XDSReferenceNo = rp.EnquiryLogID.ToString();
                              oResponse.Error = string.Empty;

                              XmlSerializer serializer1 = new XmlSerializer(oResponse.GetType());
                              System.IO.StringWriter sw1 = new System.IO.StringWriter();
                              serializer1.Serialize(sw1, oResponse);
                              System.IO.StringReader reader1 = new System.IO.StringReader(sw1.ToString());

                              rp.ResponseData = reader1.ReadToEnd();
                              rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                          }
                          else
                          {
                              oResponse.Result = string.Empty;
                              oResponse.XDSReferenceNo = rp.EnquiryLogID.ToString();
                              oResponse.Error = rp.ResponseData;

                              XmlSerializer serializer = new XmlSerializer(oResponse.GetType());
                              System.IO.StringWriter sw = new System.IO.StringWriter();
                              serializer.Serialize(sw, oResponse);
                              System.IO.StringReader reader = new System.IO.StringReader(sw.ToString());

                              rp.ResponseData = reader.ReadToEnd();
                          }
                        

                      }

                  
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";

                    oResponse.Result = string.Empty;
                    oResponse.XDSReferenceNo = rp.EnquiryLogID.ToString();
                    oResponse.Error = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";

                    XmlSerializer serializer = new XmlSerializer(oResponse.GetType());
                    System.IO.StringWriter sw = new System.IO.StringWriter();
                    serializer.Serialize(sw, oResponse);
                    System.IO.StringReader reader = new System.IO.StringReader(sw.ToString());

                    rp.ResponseData = reader.ReadToEnd();

                }

                Enquirycon.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(Enquirycon);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception ex)
            {


                rp.EnquiryLogID = Reference;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = ex.Message;

                if (ex.Message.Contains("A .NET Framework"))
                {
                    rp.ResponseData = "Error: The System is unable to process this request. Please contact XDS Support";
                }

                oResponse.Result = string.Empty;
                oResponse.XDSReferenceNo = rp.EnquiryLogID.ToString();
                oResponse.Error = rp.ResponseData;

                XmlSerializer serializer = new XmlSerializer(oResponse.GetType());
                System.IO.StringWriter sw = new System.IO.StringWriter();
                serializer.Serialize(sw, oResponse);
                System.IO.StringReader reader = new System.IO.StringReader(sw.ToString());

                rp.ResponseData = reader.ReadToEnd();

                if (Enquirycon.State == ConnectionState.Open)
                    Enquirycon.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(Enquirycon);
                SqlConnection.ClearPool(AdminConnection);
            }
            return oResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response GetBusinessInvestigativeResult(SqlConnection MTNCommercialQuestionnaire, int intEnquiryID)
        {
            if (MTNCommercialQuestionnaire.State == ConnectionState.Closed)
                MTNCommercialQuestionnaire.Open();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                //Log enquiry on Commercial Blue
                Data.BusinessInvestigativeEnquiry bie = new Data.BusinessInvestigativeEnquiry();
                string Result = bie.GetCommercialReport(MTNCommercialQuestionnaire, intEnquiryID);

                if (!Result.Contains("NoResult"))
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                }

                rp.EnquiryLogID = intEnquiryID;
                rp.ResponseData = Result;
                MTNCommercialQuestionnaire.Close();
                SqlConnection.ClearPool(MTNCommercialQuestionnaire);
            }

            catch (Exception oException)
            {
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (MTNCommercialQuestionnaire.State == ConnectionState.Open)
                    MTNCommercialQuestionnaire.Close();

                SqlConnection.ClearPool(MTNCommercialQuestionnaire);
            }

            return rp;
        }

        public bool FnIsNumericPositive(string input)
        {
            bool isNumeric = false;

            int ivalue = 0;
            if (int.TryParse(input, NumberStyles.None, new CultureInfo("en-US"), out ivalue))
                isNumeric = true;

            long lvalue = 0;
            if (long.TryParse(input, NumberStyles.None, new CultureInfo("en-US"), out lvalue))
                isNumeric = true;

            Decimal dvalue = 0;
            if (Decimal.TryParse(input, NumberStyles.None, new CultureInfo("en-US"), out dvalue))
                isNumeric = true;

            if (!string.IsNullOrEmpty(input) && input[0] == '-')
                isNumeric = false;


            return isNumeric;
        }


    }
}

