﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace XDSPortalEnquiry.Business
{
    public class LexisNexisData
    {
        private XDSPortalLibrary.Business_Layer.ConsumerIDVerificationTrace moConsumerTraceWseManager;
        private XDSPortalLibrary.Entity_Layer.ConsumerIDVerificationTrace eoConsumerTraceWseManager;

        public LexisNexisData()
        {
            moConsumerTraceWseManager = new XDSPortalLibrary.Business_Layer.ConsumerIDVerificationTrace();
            eoConsumerTraceWseManager = new XDSPortalLibrary.Entity_Layer.ConsumerIDVerificationTrace();
        }

        public XDSPortalLibrary.Entity_Layer.Response RenderLexisNexisData(SqlConnection con, SqlConnection AdminConnection, 
            int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, DataSet dsBonusDataSegment, string reportXml, string ClientID, 
            string UserID, string Password, string URL, int ProductID, string FirstName, string Surname, DateTime BirthDate, string Gender,
            string AddressLine1, string AddressLine2, string City, string Province, string PostalCode, string Country, string Citizenship, 
            string CellNumber)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();


            string rXml = "";
            double Totalcost = 0;

            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                SqlConnection objConstring = new SqlConnection(eSe.ExtraVarInput1.ToString());


                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                if (spr.ReportID > 0)
                {
                    eoConsumerTraceWseManager.BonusSegments = null;
                    if (dsBonusDataSegment != null)
                    {
                        if (dsBonusDataSegment.Tables.Count > 0 && dsBonusDataSegment.Tables[0].Rows.Count > 0)
                        {
                            dsBonusDataSegment.DataSetName = "BonusSegments";
                            dsBonusDataSegment.Tables[0].TableName = "Segment";
                            eoConsumerTraceWseManager.BonusSegments = dsBonusDataSegment;
                        }
                    }

                    // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML 
                    // use this XML data for generating the Report 


                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {
                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }
                        else if (sub.PayAsYouGo == 1)
                        {
                            //Calculate The Total cost of the Report , including Bonus Segments

                            Totalcost = spr.UnitPrice;

                            if (eoConsumerTraceWseManager.BonusSegments != null)
                            {

                                foreach (DataRow dr in eoConsumerTraceWseManager.BonusSegments.Tables[0].Rows)
                                {
                                    if (dr["BonusViewed"].ToString().ToLower() == "true")
                                    {
                                        Totalcost = Totalcost + Convert.ToInt16(dr["BonusPrice"].ToString());
                                    }
                                }
                            }
                        }

                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {

                            if (eSC.KeyType == "H")
                            {
                                eoConsumerTraceWseManager.HomeAffairsID = eSC.KeyID;
                            }
                            else
                            {
                                eoConsumerTraceWseManager.ConsumerID = eSC.KeyID;
                            }
                            eoConsumerTraceWseManager.ProductID = eSe.ProductID;
                            eoConsumerTraceWseManager.subscriberID = eSe.SubscriberID;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.ReferenceNo = eSC.Reference;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.TmpReference = eSC.ExtraVarOutput1;
                            moConsumerTraceWseManager.ConnectionString = objConstring;
                            eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                            eoConsumerTraceWseManager.IDno = eSe.IDNo;
                            eoConsumerTraceWseManager.Firstname = eSe.FirstName;
                            eoConsumerTraceWseManager.Surname = eSe.Surname;
                            eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;

                            XDSPortalEnquiry.Data.Consumer oConsumer = new XDSPortalEnquiry.Data.Consumer();

                            if (eoConsumerTraceWseManager.ConsumerID == 0)
                            {
                                eoConsumerTraceWseManager.ConsumerID = oConsumer.GetConsumerID(objConstring, 0, eoConsumerTraceWseManager.HomeAffairsID);
                            }

                            XDSPortalEnquiry.Data.Consumer oEnquiryConsumer = new XDSPortalEnquiry.Data.Consumer();
                            oEnquiryConsumer.CheckBlockedStatus(AdminConnection, int.Parse(eSe.SubscriberID.ToString()), int.Parse(eoConsumerTraceWseManager.ConsumerID.ToString()), eSe.ProductID, sub.SubscriberAssociationCode, eSe.ProductID);

                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.RenderLexisNexisData(reportXml, ClientID, UserID, Password, URL, FirstName, Surname, ProductID, BirthDate, Gender,
                                AddressLine1, AddressLine2, City, Province, PostalCode, Country, Citizenship, CellNumber);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.RenderLexisNexisData(reportXml, ClientID, UserID, Password, URL, FirstName, Surname, ProductID, BirthDate, Gender,
                                    AddressLine1, AddressLine2, City, Province, PostalCode, Country, Citizenship, CellNumber);
                            }

                            rXml = rp.ResponseData;

                            System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                            {

                                eSC.DetailsViewedYN = false;
                                eSC.Billable = false;
                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                                eSC.CreatedByUser = eSe.CreatedByUser;
                                eSC.CreatedOnDate = DateTime.Now;

                                dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                rp.EnquiryID = eSC.SubscriberEnquiryID;

                            }

                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {

                                eSC.EnquiryResult = "E";
                                eSe.ErrorDescription = rp.ResponseData.ToString();
                                dSe.UpdateSubscriberEnquiryError(con, eSe);


                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Bonus)
                            {
                                ds.ReadXml(xmlSR);
                                if (ds.Tables.Contains("Segments"))
                                {

                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.BonusIncluded = true;
                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;
                                    eSC.XMLBonus = rXml.Replace("'", "''");
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.B.ToString();

                                    dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = intSubscriberEnquiryID;
                                    rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                    foreach (DataRow r in ds.Tables["Segments"].Rows)
                                    {
                                        eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                        eSB.DataSegmentID = int.Parse(r["DataSegmentID"].ToString());
                                        eSB.DataSegmentName = r["DataSegmentName"].ToString().Replace("'", "''");
                                        eSB.DataSegmentDisplayText = r["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                        eSB.BonusViewed = bool.Parse(r["BonusViewed"].ToString());
                                        eSB.Billable = false;
                                        eSB.CreatedByUser = eSe.CreatedByUser;
                                        eSB.CreatedOnDate = DateTime.Now;

                                        dSB.InsertSubscriberEnquiryResultBonus(con, eSB);
                                    }
                                }
                            }

                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                if (!string.IsNullOrEmpty(rp.ResponseData))
                                {
                                 ds.ReadXml(xmlSR);
                                if (ds.Tables.Contains("RealTimeIDV"))
                                {
                                    ds.Tables["ReportInformation"].Rows[0]["ReportID"] = "152";
                                    ds.Tables["ReportInformation"].Rows[0]["ReportName"] = "XDS Compliance Exposure Consumer Report";
                                    ds.Tables["SubscriberInputDetails"].Rows[0]["EnquiryType"] = "XDS Compliance Exposure Consumer Report";

                                    Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                    Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, eSe.ProductID);

                                    if (eSC.BonusIncluded == true)
                                    {
                                        Data.SubscriberEnquiryResultBonus oSubscriberEnquiryResultBonus = new XDSPortalEnquiry.Data.SubscriberEnquiryResultBonus();
                                        //DataSet dsBonus = oSubscriberEnquiryResultBonus.GetSelectedSubscriberEnquiryResultBonus(con, eSC.SubscriberEnquiryResultID);
                                        DataSet dsBonus = eoConsumerTraceWseManager.BonusSegments;

                                        DataTable dtBonusSelected = new DataTable("ConsumerBonusSelected");

                                        dtBonusSelected.Columns.Add("DataSegmentID", typeof(String));
                                        dtBonusSelected.Columns.Add("DataSegmentName", typeof(String));
                                        dtBonusSelected.Columns.Add("DataSegmentDisplayText", typeof(String));

                                        DataRow drBonusSelected;
                                        if (dsBonus != null)
                                        {
                                            if (dsBonus.Tables[0].Rows.Count > 0)
                                            {
                                                foreach (DataRow r in dsBonus.Tables[0].Rows)
                                                {
                                                    drBonusSelected = dtBonusSelected.NewRow();
                                                    drBonusSelected["DataSegmentID"] = r["DataSegmentID"];
                                                    drBonusSelected["DataSegmentName"] = r["DataSegmentName"];
                                                    drBonusSelected["DataSegmentDisplayText"] = r["DataSegmentDisplayText"];
                                                    dtBonusSelected.Rows.Add(drBonusSelected);

                                                    eSB.SubscriberEnquiryResultBonusID = 0;
                                                    eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                                    eSB.DataSegmentID = Convert.ToInt32(r["DataSegmentID"]);
                                                    eSB.DataSegmentName = r["DataSegmentName"].ToString();
                                                    eSB.DataSegmentDisplayText = r["DataSegmentDisplayText"].ToString();
                                                    eSB.BonusViewed = Convert.ToBoolean(r["BonusViewed"].ToString());
                                                    if (eSB.BonusViewed)
                                                    {
                                                        eSB.Billable = true;
                                                    }
                                                    else
                                                    {
                                                        eSB.Billable = false;
                                                    }
                                                    eSB.ChangedByUser = eSB.CreatedByUser;
                                                    eSB.ChangedOnDate = DateTime.Now;
                                                    dSB.UpdateSubscriberEnquiryResultBonus(con, eSB);
                                                }
                                                ds.Tables.Add(dtBonusSelected);
                                            }
                                            dsBonus.Dispose();
                                        }
                                    }

                                    rXml = ds.GetXml();
                                    rp.ResponseData = rXml;

                                    foreach (DataRow r in ds.Tables["RealTimeIDV"].Rows)
                                    {
                                        Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                        Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                        eSC.DetailsViewedDate = DateTime.Now;
                                        eSC.DetailsViewedYN = true;
                                        if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                        {
                                            eSC.Billable = false;
                                        }
                                        else
                                        {
                                            eSC.Billable = true;
                                        }
                                        eSC.ChangedByUser = eSe.CreatedByUser;
                                        eSC.ChangedOnDate = DateTime.Now;
                                        eSC.SearchOutput = "";


                                        if (r.Table.Columns.Contains("HASurname"))
                                        {
                                            if (!string.IsNullOrEmpty(r["HASurname"].ToString()))
                                            {
                                                eSC.Surname = r["HASurname"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                            }
                                        }

                                        if (r.Table.Columns.Contains("HANames"))
                                        {
                                            if (!string.IsNullOrEmpty(r["HANames"].ToString()))
                                            {
                                                eSC.FirstName = r["HANames"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                            }
                                        }

                                        if (eSC.SearchOutput.Length > 0)
                                        {
                                            eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                        }

                                        eSC.DetailsViewedYN = true;
                                        eSC.Billable = true;
                                        eSC.CreatedByUser = eSe.CreatedByUser;
                                        eSC.CreatedOnDate = DateTime.Now;
                                        eSC.KeyID = rp.ResponseKey; ;
                                        eSC.KeyType = rp.ResponseKeyType;

                                        eSC.BillingTypeID = spr.BillingTypeID;
                                        eSC.BillingPrice = spr.UnitPrice;

                                        eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                        eSC.XMLData = rXml.Replace("'", "''");
                                        eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                        eSC.ProductID = ProductID;

                                        dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                        // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                        if (sub.PayAsYouGo == 1)
                                        {
                                            dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                        }
                                        if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                        {
                                            dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                        }

                                        if (String.IsNullOrEmpty(eSC.VoucherCode))
                                        {
                                            //Log FootPrint

                                            eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                            eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                            eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                            eSubscriberFootPrint.KeyID = eSC.KeyID;
                                            eSubscriberFootPrint.KeyType = eSC.KeyType;
                                            eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                            eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                            if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                            {
                                                eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                            }
                                            eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                            eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                            eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                            eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                            eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                            eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                            eSubscriberFootPrint.CreatedByUser = sys.Username;
                                            eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                            eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                            eSubscriberFootPrint.ProductID = eSe.ProductID;
                                            eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                            int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                        }
                                    }
                                }
                                else
                                {
                                    DataTable dtReportInformation = new DataTable("ReportInformation");
                                    dtReportInformation.Columns.Add("ReportID", typeof(String));
                                    dtReportInformation.Columns.Add("ReportName", typeof(String));

                                    DataRow drReportInformation = dtReportInformation.NewRow();
                                    drReportInformation["ReportID"] = "152";
                                    drReportInformation["ReportName"] = "XDS Compliance Exposure Consumer Report";
                                    dtReportInformation.Rows.Add(drReportInformation);
                                    ds.Tables.Add(dtReportInformation);

                                    Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                    Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, ProductID);

                                    DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                    dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                    dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                    dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                    dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                    dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                    dtSubscriberInput.Columns.Add("EnquiryReference", typeof(String));
                                    dtSubscriberInput.Columns.Add("YourReference", typeof(String));
                                    DataRow drSubscriberInput;
                                    drSubscriberInput = dtSubscriberInput.NewRow();

                                    drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                    drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                    drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                    drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                    drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();
                                    drSubscriberInput["EnquiryReference"] = "R-" + intSubscriberEnquiryID.ToString();
                                    drSubscriberInput["YourReference"] = "";
                                    // DataSet ds1 = new DataSet("HomeAffairs");
                                    dtSubscriberInput.Rows.Add(drSubscriberInput);
                                    ds.Tables.Add(dtSubscriberInput);
                                    rXml = ds.GetXml();
                                    // rp.ResponseData = rXml;
                                    Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                    Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();
                                        eSC.DetailsViewedDate = DateTime.Now;
                                        eSC.DetailsViewedYN = true;
                                        if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                        {
                                            eSC.Billable = false;
                                        }
                                        else
                                        {
                                            eSC.Billable = true;
                                        }
                                        eSC.ChangedByUser = eSe.CreatedByUser;
                                        eSC.ChangedOnDate = DateTime.Now;
                                        eSC.SearchOutput = "";
                                        if (eSC.SearchOutput.Length > 0)
                                        {
                                            eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                        }

                                        eSC.DetailsViewedYN = true;
                                        eSC.Billable = true;
                                        eSC.CreatedByUser = eSe.CreatedByUser;
                                        eSC.CreatedOnDate = DateTime.Now;
                                        eSC.KeyID = rp.ResponseKey; ;
                                        eSC.KeyType = rp.ResponseKeyType;

                                        eSC.BillingTypeID = spr.BillingTypeID;
                                        eSC.BillingPrice = spr.UnitPrice;

                                        eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                        eSC.XMLData = rXml.Replace("'", "''");
                                        eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                        eSC.ProductID = ProductID;

                                        dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                        // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                        if (sub.PayAsYouGo == 1)
                                        {
                                            dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                        }
                                        if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                        {
                                            dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                        }

                                        if (String.IsNullOrEmpty(eSC.VoucherCode))
                                        {
                                            //Log FootPrint

                                            eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                            eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                            eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                            eSubscriberFootPrint.KeyID = eSC.KeyID;
                                            eSubscriberFootPrint.KeyType = eSC.KeyType;
                                            eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                            eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                            if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                            {
                                                eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                            }
                                            eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                            eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                            eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                            eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                            eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                            eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                            eSubscriberFootPrint.CreatedByUser = sys.Username;
                                            eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                            eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                            eSubscriberFootPrint.ProductID = eSe.ProductID;
                                            eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                            int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                        }

                                    }
                                }
                            else
                            {
                                Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, ProductID);

                                DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                dtSubscriberInput.Columns.Add("EnquiryReference", typeof(String));
                                dtSubscriberInput.Columns.Add("YourReference", typeof(String));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();
                                drSubscriberInput["EnquiryReference"] = "R-" + intSubscriberEnquiryID.ToString();
                                drSubscriberInput["YourReference"] = "";
                                DataSet ds1 = new DataSet("HomeAffairs");
                                dtSubscriberInput.Rows.Add(drSubscriberInput);
                                ds1.Tables.Add(dtSubscriberInput);
                                rXml = ds1.GetXml();

                                    Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                    Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();
                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                    {
                                        eSC.Billable = false;
                                    }
                                    else
                                    {
                                        eSC.Billable = true;
                                    }
                                    eSC.ChangedByUser = eSe.CreatedByUser;
                                    eSC.ChangedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";
                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.DetailsViewedYN = true;
                                    eSC.Billable = true;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.KeyID = rp.ResponseKey; ;
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                    eSC.ProductID = ProductID;

                                    dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                    if (sub.PayAsYouGo == 1)
                                    {
                                        dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                    }
                                    if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                    {
                                        dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                    }

                                    if (String.IsNullOrEmpty(eSC.VoucherCode))
                                    {
                                        //Log FootPrint

                                        eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                        eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                        eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                        eSubscriberFootPrint.KeyID = eSC.KeyID;
                                        eSubscriberFootPrint.KeyType = eSC.KeyType;
                                        eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                        eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                        if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                        {
                                            eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                        }
                                        eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                        eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                        eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                        eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                        eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                        eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                        eSubscriberFootPrint.CreatedByUser = sys.Username;
                                        eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                        eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                        eSubscriberFootPrint.ProductID = eSe.ProductID;
                                        eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                        int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                    }









                                }

                             }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                            {
                                ds.ReadXml(xmlSR);
                                if (ds.Tables.Contains("ConsumerDetails"))
                                {
                                    foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                    {

                                        eSC.SearchOutput = "";

                                        if (r.Table.Columns.Contains("HomeAffairsSurname"))
                                        {
                                            if (!string.IsNullOrEmpty(r["HomeAffairsSurname"].ToString()))
                                            {
                                                eSC.Surname = r["HomeAffairsSurname"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                            }
                                        }

                                        if (r.Table.Columns.Contains("HomeAffairsFirstName"))
                                        {
                                            if (!string.IsNullOrEmpty(r["HomeAffairsFirstName"].ToString()))
                                            {
                                                eSC.FirstName = r["HomeAffairsFirstName"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                            }
                                        }

                                        if (eSC.SearchOutput.Length > 0)
                                        {
                                            eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                        }

                                        eSC.DetailsViewedYN = false;
                                        eSC.Billable = false;
                                        eSC.ChangedByUser = eSe.CreatedByUser;
                                        eSC.ChangedOnDate = DateTime.Now;
                                        eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                        eSC.KeyType = rp.ResponseKeyType;

                                        eSC.BillingTypeID = spr.BillingTypeID;
                                        eSC.BillingPrice = spr.UnitPrice;

                                        eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                        eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();

                                        dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                }
                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                            {
                                ds.ReadXml(xmlSR);
                                if (ds.Tables.Contains("ConsumerDetails"))
                                {
                                    foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                    {
                                        eSC.SearchOutput = "";

                                        if (r.Table.Columns.Contains("HomeAffairsSurname"))
                                        {
                                            if (!string.IsNullOrEmpty(r["HomeAffairsSurname"].ToString()))
                                            {
                                                eSC.Surname = r["HomeAffairsSurname"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                            }
                                        }

                                        if (r.Table.Columns.Contains("HomeAffairsFirstName"))
                                        {
                                            if (!string.IsNullOrEmpty(r["HomeAffairsFirstName"].ToString()))
                                            {
                                                eSC.FirstName = r["HomeAffairsFirstName"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                            }
                                        }

                                        if (eSC.SearchOutput.Length > 0)
                                        {
                                            eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                        }

                                        eSC.DetailsViewedYN = false;
                                        eSC.Billable = false;
                                        eSC.CreatedByUser = eSe.CreatedByUser;
                                        eSC.CreatedOnDate = DateTime.Now;
                                        eSC.KeyID = rp.ResponseKey; ;
                                        eSC.KeyType = rp.ResponseKeyType;

                                        eSC.BillingTypeID = spr.BillingTypeID;
                                        eSC.BillingPrice = spr.UnitPrice;

                                        eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                        eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();

                                        dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                }
                            }

                        }
                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.
                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                        }
                    }
                    else
                    {
                        // When User want to ReOpen a report 
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = eSC.XMLData;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }
                con.Close();
                AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            ds.Dispose();

            return rp;

        }

        public XDSPortalLibrary.Entity_Layer.Response RenderLexisNexisDataLite(SqlConnection con, SqlConnection AdminConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, DataSet dsBonusDataSegment, string reportXml, string ClientID, string UserID, string Password, string URL, int ProductID, string FirstName, string Surname)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();


            string rXml = "";
            double Totalcost = 0;

            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                SqlConnection objConstring = new SqlConnection(eSe.ExtraVarInput1.ToString());


                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                if (spr.ReportID > 0)
                {
                    eoConsumerTraceWseManager.BonusSegments = null;
                    if (dsBonusDataSegment != null)
                    {
                        if (dsBonusDataSegment.Tables.Count > 0 && dsBonusDataSegment.Tables[0].Rows.Count > 0)
                        {
                            dsBonusDataSegment.DataSetName = "BonusSegments";
                            dsBonusDataSegment.Tables[0].TableName = "Segment";
                            eoConsumerTraceWseManager.BonusSegments = dsBonusDataSegment;
                        }
                    }

                    // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML 
                    // use this XML data for generating the Report 


                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {
                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }
                        else if (sub.PayAsYouGo == 1)
                        {
                            //Calculate The Total cost of the Report , including Bonus Segments

                            Totalcost = spr.UnitPrice;

                            if (eoConsumerTraceWseManager.BonusSegments != null)
                            {

                                foreach (DataRow dr in eoConsumerTraceWseManager.BonusSegments.Tables[0].Rows)
                                {
                                    if (dr["BonusViewed"].ToString().ToLower() == "true")
                                    {
                                        Totalcost = Totalcost + Convert.ToInt16(dr["BonusPrice"].ToString());
                                    }
                                }
                            }
                        }

                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {

                            if (eSC.KeyType == "H")
                            {
                                eoConsumerTraceWseManager.HomeAffairsID = eSC.KeyID;
                            }
                            else
                            {
                                eoConsumerTraceWseManager.ConsumerID = eSC.KeyID;
                            }
                            eoConsumerTraceWseManager.ProductID = eSe.ProductID;
                            eoConsumerTraceWseManager.subscriberID = eSe.SubscriberID;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.ReferenceNo = eSC.Reference;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.TmpReference = eSC.ExtraVarOutput1;
                            moConsumerTraceWseManager.ConnectionString = objConstring;
                            eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                            eoConsumerTraceWseManager.IDno = eSe.IDNo;
                            eoConsumerTraceWseManager.Firstname = eSe.FirstName;
                            eoConsumerTraceWseManager.Surname = eSe.Surname;
                            eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;

                            XDSPortalEnquiry.Data.Consumer oConsumer = new XDSPortalEnquiry.Data.Consumer();

                            if (eoConsumerTraceWseManager.ConsumerID == 0)
                            {
                                eoConsumerTraceWseManager.ConsumerID = oConsumer.GetConsumerID(objConstring, 0, eoConsumerTraceWseManager.HomeAffairsID);
                            }

                            XDSPortalEnquiry.Data.Consumer oEnquiryConsumer = new XDSPortalEnquiry.Data.Consumer();
                            oEnquiryConsumer.CheckBlockedStatus(AdminConnection, int.Parse(eSe.SubscriberID.ToString()), int.Parse(eoConsumerTraceWseManager.ConsumerID.ToString()), eSe.ProductID, sub.SubscriberAssociationCode, eSe.ProductID);

                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.RenderLexisNexisDataLite(reportXml, ClientID, UserID, Password, URL, FirstName, Surname);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.RenderLexisNexisDataLite(reportXml, ClientID, UserID, Password, URL, FirstName, Surname);
                            }

                            rXml = rp.ResponseData;

                            System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                            {

                                eSC.DetailsViewedYN = false;
                                eSC.Billable = false;
                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                                eSC.CreatedByUser = eSe.CreatedByUser;
                                eSC.CreatedOnDate = DateTime.Now;

                                dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                rp.EnquiryID = eSC.SubscriberEnquiryID;

                            }

                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {

                                eSC.EnquiryResult = "E";
                                eSe.ErrorDescription = rp.ResponseData.ToString();
                                dSe.UpdateSubscriberEnquiryError(con, eSe);


                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Bonus)
                            {
                                ds.ReadXml(xmlSR);
                                if (ds.Tables.Contains("Segments"))
                                {

                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.BonusIncluded = true;
                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;
                                    eSC.XMLBonus = rXml.Replace("'", "''");
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.B.ToString();

                                    dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = intSubscriberEnquiryID;
                                    rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                    foreach (DataRow r in ds.Tables["Segments"].Rows)
                                    {
                                        eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                        eSB.DataSegmentID = int.Parse(r["DataSegmentID"].ToString());
                                        eSB.DataSegmentName = r["DataSegmentName"].ToString().Replace("'", "''");
                                        eSB.DataSegmentDisplayText = r["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                        eSB.BonusViewed = bool.Parse(r["BonusViewed"].ToString());
                                        eSB.Billable = false;
                                        eSB.CreatedByUser = eSe.CreatedByUser;
                                        eSB.CreatedOnDate = DateTime.Now;

                                        dSB.InsertSubscriberEnquiryResultBonus(con, eSB);
                                    }
                                }
                            }

                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                if (!string.IsNullOrEmpty(rp.ResponseData))
                                {
                                    ds.ReadXml(xmlSR);
                                    if (ds.Tables.Contains("RealTimeIDV"))
                                    {
                                        ds.Tables["ReportInformation"].Rows[0]["ReportID"] = "152";
                                        ds.Tables["ReportInformation"].Rows[0]["ReportName"] = "XDS Compliance Exposure Consumer Report";
                                        ds.Tables["SubscriberInputDetails"].Rows[0]["EnquiryType"] = "XDS Compliance Exposure Consumer Report";

                                        Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                        Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, eSe.ProductID);

                                        if (eSC.BonusIncluded == true)
                                        {
                                            Data.SubscriberEnquiryResultBonus oSubscriberEnquiryResultBonus = new XDSPortalEnquiry.Data.SubscriberEnquiryResultBonus();
                                            //DataSet dsBonus = oSubscriberEnquiryResultBonus.GetSelectedSubscriberEnquiryResultBonus(con, eSC.SubscriberEnquiryResultID);
                                            DataSet dsBonus = eoConsumerTraceWseManager.BonusSegments;

                                            DataTable dtBonusSelected = new DataTable("ConsumerBonusSelected");

                                            dtBonusSelected.Columns.Add("DataSegmentID", typeof(String));
                                            dtBonusSelected.Columns.Add("DataSegmentName", typeof(String));
                                            dtBonusSelected.Columns.Add("DataSegmentDisplayText", typeof(String));

                                            DataRow drBonusSelected;
                                            if (dsBonus != null)
                                            {
                                                if (dsBonus.Tables[0].Rows.Count > 0)
                                                {
                                                    foreach (DataRow r in dsBonus.Tables[0].Rows)
                                                    {
                                                        drBonusSelected = dtBonusSelected.NewRow();
                                                        drBonusSelected["DataSegmentID"] = r["DataSegmentID"];
                                                        drBonusSelected["DataSegmentName"] = r["DataSegmentName"];
                                                        drBonusSelected["DataSegmentDisplayText"] = r["DataSegmentDisplayText"];
                                                        dtBonusSelected.Rows.Add(drBonusSelected);

                                                        eSB.SubscriberEnquiryResultBonusID = 0;
                                                        eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                                        eSB.DataSegmentID = Convert.ToInt32(r["DataSegmentID"]);
                                                        eSB.DataSegmentName = r["DataSegmentName"].ToString();
                                                        eSB.DataSegmentDisplayText = r["DataSegmentDisplayText"].ToString();
                                                        eSB.BonusViewed = Convert.ToBoolean(r["BonusViewed"].ToString());
                                                        if (eSB.BonusViewed)
                                                        {
                                                            eSB.Billable = true;
                                                        }
                                                        else
                                                        {
                                                            eSB.Billable = false;
                                                        }
                                                        eSB.ChangedByUser = eSB.CreatedByUser;
                                                        eSB.ChangedOnDate = DateTime.Now;
                                                        dSB.UpdateSubscriberEnquiryResultBonus(con, eSB);
                                                    }
                                                    ds.Tables.Add(dtBonusSelected);
                                                }
                                                dsBonus.Dispose();
                                            }
                                        }

                                        rXml = ds.GetXml();
                                        rp.ResponseData = rXml;

                                        foreach (DataRow r in ds.Tables["RealTimeIDV"].Rows)
                                        {
                                            Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                            Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                            eSC.DetailsViewedDate = DateTime.Now;
                                            eSC.DetailsViewedYN = true;
                                            if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                            {
                                                eSC.Billable = false;
                                            }
                                            else
                                            {
                                                eSC.Billable = true;
                                            }
                                            eSC.ChangedByUser = eSe.CreatedByUser;
                                            eSC.ChangedOnDate = DateTime.Now;
                                            eSC.SearchOutput = "";


                                            if (r.Table.Columns.Contains("HASurname"))
                                            {
                                                if (!string.IsNullOrEmpty(r["HASurname"].ToString()))
                                                {
                                                    eSC.Surname = r["HASurname"].ToString().Replace("'", "''");
                                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                                }
                                            }

                                            if (r.Table.Columns.Contains("HANames"))
                                            {
                                                if (!string.IsNullOrEmpty(r["HANames"].ToString()))
                                                {
                                                    eSC.FirstName = r["HANames"].ToString().Replace("'", "''");
                                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                                }
                                            }

                                            if (eSC.SearchOutput.Length > 0)
                                            {
                                                eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                            }

                                            eSC.DetailsViewedYN = true;
                                            eSC.Billable = true;
                                            eSC.CreatedByUser = eSe.CreatedByUser;
                                            eSC.CreatedOnDate = DateTime.Now;
                                            eSC.KeyID = rp.ResponseKey; ;
                                            eSC.KeyType = rp.ResponseKeyType;

                                            eSC.BillingTypeID = spr.BillingTypeID;
                                            eSC.BillingPrice = spr.UnitPrice;

                                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                            eSC.XMLData = rXml.Replace("'", "''");
                                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                            eSC.ProductID = ProductID;

                                            dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                            rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                            rp.EnquiryID = eSC.SubscriberEnquiryID;

                                            // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                            if (sub.PayAsYouGo == 1)
                                            {
                                                dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                            }
                                            if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                            {
                                                dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                            }

                                            if (String.IsNullOrEmpty(eSC.VoucherCode))
                                            {
                                                //Log FootPrint

                                                eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                                eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                                eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                                eSubscriberFootPrint.KeyID = eSC.KeyID;
                                                eSubscriberFootPrint.KeyType = eSC.KeyType;
                                                eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                                eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                                if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                                {
                                                    eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                                }
                                                eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                                eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                                eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                                eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                                eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                                eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                                eSubscriberFootPrint.CreatedByUser = sys.Username;
                                                eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                                eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                                eSubscriberFootPrint.ProductID = eSe.ProductID;
                                                eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                                int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                        Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, ProductID);

                                        ds.DataSetName = "HomeAffairs";

                                        DataTable dtReportInformation = new DataTable("ReportInformation");
                                        dtReportInformation.Columns.Add("ReportID", typeof(String));
                                        dtReportInformation.Columns.Add("ReportName", typeof(String));
                                        DataRow drReportInformation;
                                        drReportInformation = dtReportInformation.NewRow();

                                        drReportInformation["ReportID"] = "152";
                                        drReportInformation["ReportName"] = "XDS Compliance Exposure Consumer Report";

                                        dtReportInformation.Rows.Add(drReportInformation);
                                        ds.Tables.Add(dtReportInformation);

                                        DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                        dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                        dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                        dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                        dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                        dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                        dtSubscriberInput.Columns.Add("EnquiryReference", typeof(String));
                                        dtSubscriberInput.Columns.Add("YourReference", typeof(String));
                                        DataRow drSubscriberInput;
                                        drSubscriberInput = dtSubscriberInput.NewRow();

                                        drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                        drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                        drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                        drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                        drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();
                                        drSubscriberInput["EnquiryReference"] = "R-" + intSubscriberEnquiryID.ToString();
                                        drSubscriberInput["YourReference"] = "";
                                        // DataSet ds1 = new DataSet("HomeAffairs");
                                        dtSubscriberInput.Rows.Add(drSubscriberInput);
                                        ds.Tables.Add(dtSubscriberInput);
                                        rXml = ds.GetXml();
                                        // rp.ResponseData = rXml;
                                        Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                        Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();
                                        eSC.DetailsViewedDate = DateTime.Now;
                                        eSC.DetailsViewedYN = true;
                                        if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                        {
                                            eSC.Billable = false;
                                        }
                                        else
                                        {
                                            eSC.Billable = true;
                                        }
                                        eSC.ChangedByUser = eSe.CreatedByUser;
                                        eSC.ChangedOnDate = DateTime.Now;
                                        eSC.SearchOutput = "";
                                        if (eSC.SearchOutput.Length > 0)
                                        {
                                            eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                        }

                                        eSC.DetailsViewedYN = true;
                                        eSC.Billable = true;
                                        eSC.CreatedByUser = eSe.CreatedByUser;
                                        eSC.CreatedOnDate = DateTime.Now;
                                        eSC.KeyID = rp.ResponseKey; ;
                                        eSC.KeyType = rp.ResponseKeyType;

                                        eSC.BillingTypeID = spr.BillingTypeID;
                                        eSC.BillingPrice = spr.UnitPrice;

                                        eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                        eSC.XMLData = rXml.Replace("'", "''");
                                        eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                        eSC.ProductID = ProductID;

                                        dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                        // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                        if (sub.PayAsYouGo == 1)
                                        {
                                            dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                        }
                                        if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                        {
                                            dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                        }

                                        if (String.IsNullOrEmpty(eSC.VoucherCode))
                                        {
                                            //Log FootPrint

                                            eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                            eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                            eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                            eSubscriberFootPrint.KeyID = eSC.KeyID;
                                            eSubscriberFootPrint.KeyType = eSC.KeyType;
                                            eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                            eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                            if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                            {
                                                eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                            }
                                            eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                            eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                            eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                            eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                            eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                            eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                            eSubscriberFootPrint.CreatedByUser = sys.Username;
                                            eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                            eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                            eSubscriberFootPrint.ProductID = eSe.ProductID;
                                            eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                            int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                        }

                                    }
                                }
                                else
                                {
                                    Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                    Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, ProductID);

                                    DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                    dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                    dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                    dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                    dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                    dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                    dtSubscriberInput.Columns.Add("EnquiryReference", typeof(String));
                                    dtSubscriberInput.Columns.Add("YourReference", typeof(String));
                                    DataRow drSubscriberInput;
                                    drSubscriberInput = dtSubscriberInput.NewRow();

                                    drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                    drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                    drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                    drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                    drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();
                                    drSubscriberInput["EnquiryReference"] = "R-" + intSubscriberEnquiryID.ToString();
                                    drSubscriberInput["YourReference"] = "";
                                    DataSet ds1 = new DataSet("HomeAffairs");
                                    dtSubscriberInput.Rows.Add(drSubscriberInput);
                                    ds1.Tables.Add(dtSubscriberInput);
                                    rXml = ds1.GetXml();

                                    Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                    Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();
                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                    {
                                        eSC.Billable = false;
                                    }
                                    else
                                    {
                                        eSC.Billable = true;
                                    }
                                    eSC.ChangedByUser = eSe.CreatedByUser;
                                    eSC.ChangedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";
                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.DetailsViewedYN = true;
                                    eSC.Billable = true;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.KeyID = rp.ResponseKey; ;
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                    eSC.ProductID = ProductID;

                                    dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                    if (sub.PayAsYouGo == 1)
                                    {
                                        dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                    }
                                    if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                    {
                                        dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                    }

                                    if (String.IsNullOrEmpty(eSC.VoucherCode))
                                    {
                                        //Log FootPrint

                                        eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                        eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                        eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                        eSubscriberFootPrint.KeyID = eSC.KeyID;
                                        eSubscriberFootPrint.KeyType = eSC.KeyType;
                                        eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                        eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                        if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                        {
                                            eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                        }
                                        eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                        eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                        eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                        eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                        eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                        eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                        eSubscriberFootPrint.CreatedByUser = sys.Username;
                                        eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                        eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                        eSubscriberFootPrint.ProductID = eSe.ProductID;
                                        eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                        int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                    }









                                }

                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                            {
                                ds.ReadXml(xmlSR);
                                if (ds.Tables.Contains("ConsumerDetails"))
                                {
                                    foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                    {

                                        eSC.SearchOutput = "";

                                        if (r.Table.Columns.Contains("HomeAffairsSurname"))
                                        {
                                            if (!string.IsNullOrEmpty(r["HomeAffairsSurname"].ToString()))
                                            {
                                                eSC.Surname = r["HomeAffairsSurname"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                            }
                                        }

                                        if (r.Table.Columns.Contains("HomeAffairsFirstName"))
                                        {
                                            if (!string.IsNullOrEmpty(r["HomeAffairsFirstName"].ToString()))
                                            {
                                                eSC.FirstName = r["HomeAffairsFirstName"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                            }
                                        }

                                        if (eSC.SearchOutput.Length > 0)
                                        {
                                            eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                        }

                                        eSC.DetailsViewedYN = false;
                                        eSC.Billable = false;
                                        eSC.ChangedByUser = eSe.CreatedByUser;
                                        eSC.ChangedOnDate = DateTime.Now;
                                        eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                        eSC.KeyType = rp.ResponseKeyType;

                                        eSC.BillingTypeID = spr.BillingTypeID;
                                        eSC.BillingPrice = spr.UnitPrice;

                                        eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                        eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();

                                        dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                }
                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                            {
                                ds.ReadXml(xmlSR);
                                if (ds.Tables.Contains("ConsumerDetails"))
                                {
                                    foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                    {
                                        eSC.SearchOutput = "";

                                        if (r.Table.Columns.Contains("HomeAffairsSurname"))
                                        {
                                            if (!string.IsNullOrEmpty(r["HomeAffairsSurname"].ToString()))
                                            {
                                                eSC.Surname = r["HomeAffairsSurname"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                            }
                                        }

                                        if (r.Table.Columns.Contains("HomeAffairsFirstName"))
                                        {
                                            if (!string.IsNullOrEmpty(r["HomeAffairsFirstName"].ToString()))
                                            {
                                                eSC.FirstName = r["HomeAffairsFirstName"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                            }
                                        }

                                        if (eSC.SearchOutput.Length > 0)
                                        {
                                            eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                        }

                                        eSC.DetailsViewedYN = false;
                                        eSC.Billable = false;
                                        eSC.CreatedByUser = eSe.CreatedByUser;
                                        eSC.CreatedOnDate = DateTime.Now;
                                        eSC.KeyID = rp.ResponseKey; ;
                                        eSC.KeyType = rp.ResponseKeyType;

                                        eSC.BillingTypeID = spr.BillingTypeID;
                                        eSC.BillingPrice = spr.UnitPrice;

                                        eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                        eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();

                                        dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                }
                            }

                        }
                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.
                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                        }
                    }
                    else
                    {
                        // When User want to ReOpen a report 
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = eSC.XMLData;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }
                con.Close();
                AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            ds.Dispose();

            return rp;

        }

        public XDSPortalLibrary.Entity_Layer.Response RenderLexisNexisDataLite(SqlConnection con, SqlConnection AdminConnection,
            int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, DataSet dsBonusDataSegment, string reportXml, string ClientID,
            string UserID, string Password, string URL, int ProductID, string FirstName, string Surname, DateTime BirthDate, string Gender,
            string AddressLine1, string AddressLine2, string City, string Province, string PostalCode, string Country, string Citizenship,
            string CellNumber)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();


            string rXml = "";
            double Totalcost = 0;

            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                SqlConnection objConstring = new SqlConnection(eSe.ExtraVarInput1.ToString());


                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                if (spr.ReportID > 0)
                {
                    eoConsumerTraceWseManager.BonusSegments = null;
                    if (dsBonusDataSegment != null)
                    {
                        if (dsBonusDataSegment.Tables.Count > 0 && dsBonusDataSegment.Tables[0].Rows.Count > 0)
                        {
                            dsBonusDataSegment.DataSetName = "BonusSegments";
                            dsBonusDataSegment.Tables[0].TableName = "Segment";
                            eoConsumerTraceWseManager.BonusSegments = dsBonusDataSegment;
                        }
                    }

                    // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML 
                    // use this XML data for generating the Report 


                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {
                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }
                        else if (sub.PayAsYouGo == 1)
                        {
                            //Calculate The Total cost of the Report , including Bonus Segments

                            Totalcost = spr.UnitPrice;

                            if (eoConsumerTraceWseManager.BonusSegments != null)
                            {

                                foreach (DataRow dr in eoConsumerTraceWseManager.BonusSegments.Tables[0].Rows)
                                {
                                    if (dr["BonusViewed"].ToString().ToLower() == "true")
                                    {
                                        Totalcost = Totalcost + Convert.ToInt16(dr["BonusPrice"].ToString());
                                    }
                                }
                            }
                        }

                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {

                            if (eSC.KeyType == "H")
                            {
                                eoConsumerTraceWseManager.HomeAffairsID = eSC.KeyID;
                            }
                            else
                            {
                                eoConsumerTraceWseManager.ConsumerID = eSC.KeyID;
                            }
                            eoConsumerTraceWseManager.ProductID = eSe.ProductID;
                            eoConsumerTraceWseManager.subscriberID = eSe.SubscriberID;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.ReferenceNo = eSC.Reference;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.TmpReference = eSC.ExtraVarOutput1;
                            moConsumerTraceWseManager.ConnectionString = objConstring;
                            eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                            eoConsumerTraceWseManager.IDno = eSe.IDNo;
                            eoConsumerTraceWseManager.Firstname = eSe.FirstName;
                            eoConsumerTraceWseManager.Surname = eSe.Surname;
                            eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;

                            XDSPortalEnquiry.Data.Consumer oConsumer = new XDSPortalEnquiry.Data.Consumer();

                            if (eoConsumerTraceWseManager.ConsumerID == 0)
                            {
                                eoConsumerTraceWseManager.ConsumerID = oConsumer.GetConsumerID(objConstring, 0, eoConsumerTraceWseManager.HomeAffairsID);
                            }

                            XDSPortalEnquiry.Data.Consumer oEnquiryConsumer = new XDSPortalEnquiry.Data.Consumer();
                            oEnquiryConsumer.CheckBlockedStatus(AdminConnection, int.Parse(eSe.SubscriberID.ToString()), int.Parse(eoConsumerTraceWseManager.ConsumerID.ToString()), eSe.ProductID, sub.SubscriberAssociationCode, eSe.ProductID);

                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.RenderLexisNexisDataLite(reportXml, ClientID, UserID, Password, URL, FirstName, Surname, ProductID, BirthDate, Gender,
                                AddressLine1, AddressLine2, City, Province, PostalCode, Country, Citizenship, CellNumber);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.RenderLexisNexisDataLite(reportXml, ClientID, UserID, Password, URL, FirstName, Surname, ProductID, BirthDate, Gender,
                                AddressLine1, AddressLine2, City, Province, PostalCode, Country, Citizenship, CellNumber);
                            }

                            rXml = rp.ResponseData;

                            System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                            {

                                eSC.DetailsViewedYN = false;
                                eSC.Billable = false;
                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                                eSC.CreatedByUser = eSe.CreatedByUser;
                                eSC.CreatedOnDate = DateTime.Now;

                                dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                rp.EnquiryID = eSC.SubscriberEnquiryID;

                            }

                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {

                                eSC.EnquiryResult = "E";
                                eSe.ErrorDescription = rp.ResponseData.ToString();
                                dSe.UpdateSubscriberEnquiryError(con, eSe);


                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Bonus)
                            {
                                ds.ReadXml(xmlSR);
                                if (ds.Tables.Contains("Segments"))
                                {

                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.BonusIncluded = true;
                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;
                                    eSC.XMLBonus = rXml.Replace("'", "''");
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.B.ToString();

                                    dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = intSubscriberEnquiryID;
                                    rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                    foreach (DataRow r in ds.Tables["Segments"].Rows)
                                    {
                                        eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                        eSB.DataSegmentID = int.Parse(r["DataSegmentID"].ToString());
                                        eSB.DataSegmentName = r["DataSegmentName"].ToString().Replace("'", "''");
                                        eSB.DataSegmentDisplayText = r["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                        eSB.BonusViewed = bool.Parse(r["BonusViewed"].ToString());
                                        eSB.Billable = false;
                                        eSB.CreatedByUser = eSe.CreatedByUser;
                                        eSB.CreatedOnDate = DateTime.Now;

                                        dSB.InsertSubscriberEnquiryResultBonus(con, eSB);
                                    }
                                }
                            }

                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                if (!string.IsNullOrEmpty(rp.ResponseData))
                                {
                                    ds.ReadXml(xmlSR);
                                    if (ds.Tables.Contains("RealTimeIDV"))
                                    {
                                        ds.Tables["ReportInformation"].Rows[0]["ReportID"] = "230";
                                        ds.Tables["ReportInformation"].Rows[0]["ReportName"] = "XDS Compliance Exposure Consumer Lite Report";
                                        ds.Tables["SubscriberInputDetails"].Rows[0]["EnquiryType"] = "XDS Compliance Exposure Consumer Lite Report";

                                        Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                        Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, eSe.ProductID);

                                        if (eSC.BonusIncluded == true)
                                        {
                                            Data.SubscriberEnquiryResultBonus oSubscriberEnquiryResultBonus = new XDSPortalEnquiry.Data.SubscriberEnquiryResultBonus();
                                            //DataSet dsBonus = oSubscriberEnquiryResultBonus.GetSelectedSubscriberEnquiryResultBonus(con, eSC.SubscriberEnquiryResultID);
                                            DataSet dsBonus = eoConsumerTraceWseManager.BonusSegments;

                                            DataTable dtBonusSelected = new DataTable("ConsumerBonusSelected");

                                            dtBonusSelected.Columns.Add("DataSegmentID", typeof(String));
                                            dtBonusSelected.Columns.Add("DataSegmentName", typeof(String));
                                            dtBonusSelected.Columns.Add("DataSegmentDisplayText", typeof(String));

                                            DataRow drBonusSelected;
                                            if (dsBonus != null)
                                            {
                                                if (dsBonus.Tables[0].Rows.Count > 0)
                                                {
                                                    foreach (DataRow r in dsBonus.Tables[0].Rows)
                                                    {
                                                        drBonusSelected = dtBonusSelected.NewRow();
                                                        drBonusSelected["DataSegmentID"] = r["DataSegmentID"];
                                                        drBonusSelected["DataSegmentName"] = r["DataSegmentName"];
                                                        drBonusSelected["DataSegmentDisplayText"] = r["DataSegmentDisplayText"];
                                                        dtBonusSelected.Rows.Add(drBonusSelected);

                                                        eSB.SubscriberEnquiryResultBonusID = 0;
                                                        eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                                        eSB.DataSegmentID = Convert.ToInt32(r["DataSegmentID"]);
                                                        eSB.DataSegmentName = r["DataSegmentName"].ToString();
                                                        eSB.DataSegmentDisplayText = r["DataSegmentDisplayText"].ToString();
                                                        eSB.BonusViewed = Convert.ToBoolean(r["BonusViewed"].ToString());
                                                        if (eSB.BonusViewed)
                                                        {
                                                            eSB.Billable = true;
                                                        }
                                                        else
                                                        {
                                                            eSB.Billable = false;
                                                        }
                                                        eSB.ChangedByUser = eSB.CreatedByUser;
                                                        eSB.ChangedOnDate = DateTime.Now;
                                                        dSB.UpdateSubscriberEnquiryResultBonus(con, eSB);
                                                    }
                                                    ds.Tables.Add(dtBonusSelected);
                                                }
                                                dsBonus.Dispose();
                                            }
                                        }

                                        rXml = ds.GetXml();
                                        rp.ResponseData = rXml;

                                        foreach (DataRow r in ds.Tables["RealTimeIDV"].Rows)
                                        {
                                            Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                            Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                            eSC.DetailsViewedDate = DateTime.Now;
                                            eSC.DetailsViewedYN = true;
                                            if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                            {
                                                eSC.Billable = false;
                                            }
                                            else
                                            {
                                                eSC.Billable = true;
                                            }
                                            eSC.ChangedByUser = eSe.CreatedByUser;
                                            eSC.ChangedOnDate = DateTime.Now;
                                            eSC.SearchOutput = "";


                                            if (r.Table.Columns.Contains("HASurname"))
                                            {
                                                if (!string.IsNullOrEmpty(r["HASurname"].ToString()))
                                                {
                                                    eSC.Surname = r["HASurname"].ToString().Replace("'", "''");
                                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                                }
                                            }

                                            if (r.Table.Columns.Contains("HANames"))
                                            {
                                                if (!string.IsNullOrEmpty(r["HANames"].ToString()))
                                                {
                                                    eSC.FirstName = r["HANames"].ToString().Replace("'", "''");
                                                    eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                                }
                                            }

                                            if (eSC.SearchOutput.Length > 0)
                                            {
                                                eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                            }

                                            eSC.DetailsViewedYN = true;
                                            eSC.Billable = true;
                                            eSC.CreatedByUser = eSe.CreatedByUser;
                                            eSC.CreatedOnDate = DateTime.Now;
                                            eSC.KeyID = rp.ResponseKey; ;
                                            eSC.KeyType = rp.ResponseKeyType;

                                            eSC.BillingTypeID = spr.BillingTypeID;
                                            eSC.BillingPrice = spr.UnitPrice;

                                            eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                            eSC.XMLData = rXml.Replace("'", "''");
                                            eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                            eSC.ProductID = ProductID;

                                            dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                            rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                            rp.EnquiryID = eSC.SubscriberEnquiryID;

                                            // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                            if (sub.PayAsYouGo == 1)
                                            {
                                                dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                            }
                                            if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                            {
                                                dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                            }

                                            if (String.IsNullOrEmpty(eSC.VoucherCode))
                                            {
                                                //Log FootPrint

                                                eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                                eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                                eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                                eSubscriberFootPrint.KeyID = eSC.KeyID;
                                                eSubscriberFootPrint.KeyType = eSC.KeyType;
                                                eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                                eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                                if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                                {
                                                    eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                                }
                                                eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                                eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                                eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                                eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                                eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                                eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                                eSubscriberFootPrint.CreatedByUser = sys.Username;
                                                eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                                eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                                eSubscriberFootPrint.ProductID = eSe.ProductID;
                                                eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                                int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                        Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, ProductID);

                                        ds.DataSetName = "HomeAffairs";

                                        DataTable dtReportInformation = new DataTable("ReportInformation");
                                        dtReportInformation.Columns.Add("ReportID", typeof(String));
                                        dtReportInformation.Columns.Add("ReportName", typeof(String));
                                        DataRow drReportInformation;
                                        drReportInformation = dtReportInformation.NewRow();

                                        drReportInformation["ReportID"] = "230";
                                        drReportInformation["ReportName"] = "XDS Compliance Exposure Consumer Lite Report";

                                        dtReportInformation.Rows.Add(drReportInformation);
                                        ds.Tables.Add(dtReportInformation);

                                        DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                        dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                        dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                        dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                        dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                        dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                        dtSubscriberInput.Columns.Add("EnquiryReference", typeof(String));
                                        dtSubscriberInput.Columns.Add("YourReference", typeof(String));
                                        DataRow drSubscriberInput;
                                        drSubscriberInput = dtSubscriberInput.NewRow();

                                        drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                        drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                        drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                        drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                        drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();
                                        drSubscriberInput["EnquiryReference"] = "R-" + intSubscriberEnquiryID.ToString();
                                        drSubscriberInput["YourReference"] = "";
                                        // DataSet ds1 = new DataSet("HomeAffairs");
                                        dtSubscriberInput.Rows.Add(drSubscriberInput);
                                        ds.Tables.Add(dtSubscriberInput);
                                        rXml = ds.GetXml();
                                        // rp.ResponseData = rXml;
                                        Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                        Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();
                                        eSC.DetailsViewedDate = DateTime.Now;
                                        eSC.DetailsViewedYN = true;
                                        if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                        {
                                            eSC.Billable = false;
                                        }
                                        else
                                        {
                                            eSC.Billable = true;
                                        }
                                        eSC.ChangedByUser = eSe.CreatedByUser;
                                        eSC.ChangedOnDate = DateTime.Now;
                                        eSC.SearchOutput = "";
                                        if (eSC.SearchOutput.Length > 0)
                                        {
                                            eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                        }

                                        eSC.DetailsViewedYN = true;
                                        eSC.Billable = true;
                                        eSC.CreatedByUser = eSe.CreatedByUser;
                                        eSC.CreatedOnDate = DateTime.Now;
                                        eSC.KeyID = rp.ResponseKey; ;
                                        eSC.KeyType = rp.ResponseKeyType;

                                        eSC.BillingTypeID = spr.BillingTypeID;
                                        eSC.BillingPrice = spr.UnitPrice;

                                        eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                        eSC.XMLData = rXml.Replace("'", "''");
                                        eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                        eSC.ProductID = ProductID;

                                        dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                        // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                        if (sub.PayAsYouGo == 1)
                                        {
                                            dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                        }
                                        if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                        {
                                            dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                        }

                                        if (String.IsNullOrEmpty(eSC.VoucherCode))
                                        {
                                            //Log FootPrint

                                            eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                            eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                            eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                            eSubscriberFootPrint.KeyID = eSC.KeyID;
                                            eSubscriberFootPrint.KeyType = eSC.KeyType;
                                            eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                            eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                            if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                            {
                                                eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                            }
                                            eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                            eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                            eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                            eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                            eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                            eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                            eSubscriberFootPrint.CreatedByUser = sys.Username;
                                            eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                            eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                            eSubscriberFootPrint.ProductID = eSe.ProductID;
                                            eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                            int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                        }

                                    }
                                }
                                else
                                {
                                    Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                    Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, ProductID);

                                    DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                    dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                    dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                    dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                    dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                    dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                    dtSubscriberInput.Columns.Add("EnquiryReference", typeof(String));
                                    dtSubscriberInput.Columns.Add("YourReference", typeof(String));
                                    DataRow drSubscriberInput;
                                    drSubscriberInput = dtSubscriberInput.NewRow();

                                    drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                    drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                    drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                    drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                    drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();
                                    drSubscriberInput["EnquiryReference"] = "R-" + intSubscriberEnquiryID.ToString();
                                    drSubscriberInput["YourReference"] = "";
                                    DataSet ds1 = new DataSet("HomeAffairs");
                                    dtSubscriberInput.Rows.Add(drSubscriberInput);
                                    ds1.Tables.Add(dtSubscriberInput);
                                    rXml = ds1.GetXml();

                                    Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                    Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();
                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                    {
                                        eSC.Billable = false;
                                    }
                                    else
                                    {
                                        eSC.Billable = true;
                                    }
                                    eSC.ChangedByUser = eSe.CreatedByUser;
                                    eSC.ChangedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";
                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.DetailsViewedYN = true;
                                    eSC.Billable = true;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.KeyID = rp.ResponseKey; ;
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                    eSC.ProductID = ProductID;

                                    dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                    if (sub.PayAsYouGo == 1)
                                    {
                                        dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                    }
                                    if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                    {
                                        dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                    }

                                    if (String.IsNullOrEmpty(eSC.VoucherCode))
                                    {
                                        //Log FootPrint

                                        eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                        eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                        eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                        eSubscriberFootPrint.KeyID = eSC.KeyID;
                                        eSubscriberFootPrint.KeyType = eSC.KeyType;
                                        eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                        eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                        if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                        {
                                            eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                        }
                                        eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                        eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                        eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                        eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                        eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                        eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                        eSubscriberFootPrint.CreatedByUser = sys.Username;
                                        eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                        eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                        eSubscriberFootPrint.ProductID = eSe.ProductID;
                                        eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                        int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                    }
                                }
                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                            {
                                ds.ReadXml(xmlSR);
                                if (ds.Tables.Contains("ConsumerDetails"))
                                {
                                    foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                    {

                                        eSC.SearchOutput = "";

                                        if (r.Table.Columns.Contains("HomeAffairsSurname"))
                                        {
                                            if (!string.IsNullOrEmpty(r["HomeAffairsSurname"].ToString()))
                                            {
                                                eSC.Surname = r["HomeAffairsSurname"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                            }
                                        }

                                        if (r.Table.Columns.Contains("HomeAffairsFirstName"))
                                        {
                                            if (!string.IsNullOrEmpty(r["HomeAffairsFirstName"].ToString()))
                                            {
                                                eSC.FirstName = r["HomeAffairsFirstName"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                            }
                                        }

                                        if (eSC.SearchOutput.Length > 0)
                                        {
                                            eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                        }

                                        eSC.DetailsViewedYN = false;
                                        eSC.Billable = false;
                                        eSC.ChangedByUser = eSe.CreatedByUser;
                                        eSC.ChangedOnDate = DateTime.Now;
                                        eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                        eSC.KeyType = rp.ResponseKeyType;

                                        eSC.BillingTypeID = spr.BillingTypeID;
                                        eSC.BillingPrice = spr.UnitPrice;

                                        eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                        eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();

                                        dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                }
                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                            {
                                ds.ReadXml(xmlSR);
                                if (ds.Tables.Contains("ConsumerDetails"))
                                {
                                    foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                    {
                                        eSC.SearchOutput = "";

                                        if (r.Table.Columns.Contains("HomeAffairsSurname"))
                                        {
                                            if (!string.IsNullOrEmpty(r["HomeAffairsSurname"].ToString()))
                                            {
                                                eSC.Surname = r["HomeAffairsSurname"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                            }
                                        }

                                        if (r.Table.Columns.Contains("HomeAffairsFirstName"))
                                        {
                                            if (!string.IsNullOrEmpty(r["HomeAffairsFirstName"].ToString()))
                                            {
                                                eSC.FirstName = r["HomeAffairsFirstName"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                            }
                                        }

                                        if (eSC.SearchOutput.Length > 0)
                                        {
                                            eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                        }

                                        eSC.DetailsViewedYN = false;
                                        eSC.Billable = false;
                                        eSC.CreatedByUser = eSe.CreatedByUser;
                                        eSC.CreatedOnDate = DateTime.Now;
                                        eSC.KeyID = rp.ResponseKey; ;
                                        eSC.KeyType = rp.ResponseKeyType;

                                        eSC.BillingTypeID = spr.BillingTypeID;
                                        eSC.BillingPrice = spr.UnitPrice;

                                        eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                        eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();

                                        dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                }
                            }

                        }
                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.
                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                        }
                    }
                    else
                    {
                        // When User want to ReOpen a report 
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = eSC.XMLData;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }
                con.Close();
                AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            ds.Dispose();

            return rp;

        }

        public XDSPortalLibrary.Entity_Layer.Response CommercialRenderLexisNexisData(SqlConnection con, SqlConnection AdminConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, DataSet dsBonusDataSegment, string ClientID, string UserID, string Password, string URL, int ProductID)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();


            string rXml = "";
            double Totalcost = 0;

            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                SqlConnection objConstring = new SqlConnection(eSe.ExtraVarInput1.ToString());


                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                Data.Reports dRe = new Data.Reports();

                Entity.Reports eRe = new Entity.Reports();

                eRe = dRe.GetProductReportsRecord(AdminConnection, spr.ReportID);



                if (spr.ReportID > 0)
                {
                    eRe = dRe.GetProductReportsRecord(AdminConnection, spr.ReportID);
                    eoConsumerTraceWseManager.BonusSegments = null;
                    if (dsBonusDataSegment != null)
                    {
                        if (dsBonusDataSegment.Tables.Count > 0 && dsBonusDataSegment.Tables[0].Rows.Count > 0)
                        {
                            dsBonusDataSegment.DataSetName = "BonusSegments";
                            dsBonusDataSegment.Tables[0].TableName = "Segment";
                            eoConsumerTraceWseManager.BonusSegments = dsBonusDataSegment;
                        }
                    }

                    // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML 
                    // use this XML data for generating the Report 


                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {
                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }
                        else if (sub.PayAsYouGo == 1)
                        {
                            //Calculate The Total cost of the Report , including Bonus Segments

                            Totalcost = spr.UnitPrice;

                            if (eoConsumerTraceWseManager.BonusSegments != null)
                            {

                                foreach (DataRow dr in eoConsumerTraceWseManager.BonusSegments.Tables[0].Rows)
                                {
                                    if (dr["BonusViewed"].ToString().ToLower() == "true")
                                    {
                                        Totalcost = Totalcost + Convert.ToInt16(dr["BonusPrice"].ToString());
                                    }
                                }
                            }
                        }

                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {
                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);

                            if (eSC.KeyID != 0)
                            {
                                XDSPortalLibrary.Entity_Layer.BusinessEnquiry eoCommercialEnquiryWseManager = new XDSPortalLibrary.Entity_Layer.BusinessEnquiry();

                                eoCommercialEnquiryWseManager.BusinessName = eSC.BusBusinessName;
                                eoCommercialEnquiryWseManager.CommercialID = eSC.KeyID;
                                eoCommercialEnquiryWseManager.ExternalReference = eSe.SubscriberReference;
                                eoCommercialEnquiryWseManager.ProductID = ProductID;
                                eoCommercialEnquiryWseManager.ReferenceNo = eSC.Reference;
                                eoCommercialEnquiryWseManager.RegistrationNo = eSC.BusRegistrationNo;
                                eoCommercialEnquiryWseManager.subscriberID = eSe.SubscriberID;
                                eoCommercialEnquiryWseManager.TmpReference = eSC.ExtraVarOutput1;
                                //moCommercialEnquiryWseManager.ConnectionString = objConstring;
                                eoCommercialEnquiryWseManager.DataSegments = eSe.ExtraVarInput2;
                                eoCommercialEnquiryWseManager.ReportID = eRe.ReportID;
                                eoCommercialEnquiryWseManager.ReportName = eRe.ReportName;


                                rp = ra.GetBusinessEnquiryReport(eoCommercialEnquiryWseManager);

                                if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                                {
                                    eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                    ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                    rp = ra.GetBusinessEnquiryReport(eoCommercialEnquiryWseManager);
                                }
                            }
                            else
                            {
                                ds = new DataSet("Commercial");

                                //DataSet dsReport = new DataSet("Commercial");

                                DataTable dtReportInformation = new DataTable("ReportInformation");
                                DataColumn dcReportID = new DataColumn("ReportID");
                                DataColumn dcReportName = new DataColumn("ReportName");

                                dtReportInformation.Columns.Add(dcReportID);
                                dtReportInformation.Columns.Add(dcReportName);

                                DataRow drReportInformation = dtReportInformation.NewRow();
                                //drReportInformation["ReportID"] = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();
                                //drReportInformation["ReportName"] = dsDataSegment.Tables[0].Rows[0]["ReportName"].ToString();
                                drReportInformation["ReportID"] = eRe.ReportID.ToString();
                                drReportInformation["ReportName"] = eRe.ReportName.ToString();

                                dtReportInformation.Rows.Add(drReportInformation);
                                ds.Tables.Add(dtReportInformation);

                                DataTable dtBusinessInformation = new DataTable("CommercialBusinessInformation");
                                dtBusinessInformation.Columns.Add("DisplayText", typeof(String));
                                dtBusinessInformation.Columns.Add("CommercialName", typeof(String));
                                dtBusinessInformation.Columns.Add("RegistrationNo", typeof(String));
                                dtBusinessInformation.Columns.Add("BusinessStartDate", typeof(DateTime));
                                dtBusinessInformation.Columns.Add("FinancialYearEnd", typeof(String));
                                dtBusinessInformation.Columns.Add("RegistrationNoOld", typeof(String));
                                dtBusinessInformation.Columns.Add("CommercialStatus", typeof(String));
                                dtBusinessInformation.Columns.Add("CommercialType", typeof(String));
                                dtBusinessInformation.Columns.Add("SIC", typeof(String));
                                dtBusinessInformation.Columns.Add("TaxNo", typeof(String));
                                dtBusinessInformation.Columns.Add("ReferenceNo", typeof(String));
                                dtBusinessInformation.Columns.Add("ExternalReference", typeof(String));
                                dtBusinessInformation.Columns.Add("TradeName", typeof(String));
                                dtBusinessInformation.Columns.Add("PreviousBussName", typeof(String));
                                dtBusinessInformation.Columns.Add("PhysicalAddress", typeof(String));
                                dtBusinessInformation.Columns.Add("PostalAddress", typeof(String));
                                dtBusinessInformation.Columns.Add("RegistrationDate", typeof(DateTime));
                                dtBusinessInformation.Columns.Add("BusinessDesc", typeof(String));
                                dtBusinessInformation.Columns.Add("TelephoneNo", typeof(String));
                                dtBusinessInformation.Columns.Add("FaxNo", typeof(String));
                                dtBusinessInformation.Columns.Add("BussEmail", typeof(String));
                                dtBusinessInformation.Columns.Add("BussWebsite", typeof(String));
                                dtBusinessInformation.Columns.Add("NoOfEnquiries", typeof(Int16));
                                dtBusinessInformation.Columns.Add("NameChangeDate", typeof(DateTime));
                                dtBusinessInformation.Columns.Add("AgeofBusiness", typeof(String));
                                dtBusinessInformation.Columns.Add("AuthorisedCapitalAmt", typeof(Decimal));
                                dtBusinessInformation.Columns.Add("IssuedNoOfShares", typeof(Int16));
                                dtBusinessInformation.Columns.Add("RegistrationNoConverted", typeof(String));
                                dtBusinessInformation.Columns.Add("FinancialEffectiveDate", typeof(DateTime));
                                dtBusinessInformation.Columns.Add("AuthorisedNoOfShares", typeof(Int16));
                                dtBusinessInformation.Columns.Add("IssuedCapitalAmt", typeof(Decimal));
                                dtBusinessInformation.Columns.Add("CommercialStatusDate", typeof(DateTime));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtBusinessInformation.NewRow();

                                drSubscriberInput["DisplayText"] = "Commercial Business Information";
                                drSubscriberInput["CommercialName"] = eSC.BusBusinessName;
                                drSubscriberInput["RegistrationNo"] = eSC.BusRegistrationNo;
                                drSubscriberInput["BusinessStartDate"] = "1900-01-01";
                                drSubscriberInput["FinancialYearEnd"] = "";
                                drSubscriberInput["RegistrationNoOld"] = "";
                                drSubscriberInput["CommercialStatus"] = "";
                                drSubscriberInput["CommercialType"] = "";
                                drSubscriberInput["SIC"] = "";
                                drSubscriberInput["TaxNo"] = "";
                                drSubscriberInput["ReferenceNo"] = eSC.Reference;
                                drSubscriberInput["ExternalReference"] = eSe.SubscriberReference;
                                drSubscriberInput["TradeName"] = "";
                                drSubscriberInput["PreviousBussName"] = "";
                                drSubscriberInput["PhysicalAddress"] = "";
                                drSubscriberInput["PostalAddress"] = "";
                                drSubscriberInput["RegistrationDate"] = "1900-01-01";
                                drSubscriberInput["BusinessDesc"] = "No Information Available";
                                drSubscriberInput["TelephoneNo"] = "";
                                drSubscriberInput["FaxNo"] = "";
                                drSubscriberInput["BussEmail"] = "";
                                drSubscriberInput["BussWebsite"] = "";
                                drSubscriberInput["NoOfEnquiries"] = 0;
                                drSubscriberInput["NameChangeDate"] = "1900-01-01";
                                drSubscriberInput["AgeofBusiness"] = "";
                                drSubscriberInput["AuthorisedCapitalAmt"] = 0;
                                drSubscriberInput["IssuedNoOfShares"] = 0;
                                drSubscriberInput["RegistrationNoConverted"] = "";
                                drSubscriberInput["FinancialEffectiveDate"] = "1900-01-01";
                                drSubscriberInput["AuthorisedNoOfShares"] = 0;
                                drSubscriberInput["IssuedCapitalAmt"] = 0;
                                drSubscriberInput["CommercialStatusDate"] = "1900-01-01";

                                dtBusinessInformation.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtBusinessInformation);

                                rp.ResponseData = ds.GetXml();
                                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                            }

                            if (eSC.KeyType == "H")
                            {
                                eoConsumerTraceWseManager.HomeAffairsID = eSC.KeyID;
                            }
                            else
                            {
                                eoConsumerTraceWseManager.ConsumerID = eSC.KeyID;
                            }
                            eoConsumerTraceWseManager.ProductID = eSe.ProductID;
                            eoConsumerTraceWseManager.subscriberID = eSe.SubscriberID;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.ReferenceNo = eSC.Reference;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.TmpReference = eSC.ExtraVarOutput1;
                            moConsumerTraceWseManager.ConnectionString = objConstring;
                            eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                            eoConsumerTraceWseManager.IDno = eSe.IDNo;
                            eoConsumerTraceWseManager.Firstname = eSe.FirstName;
                            eoConsumerTraceWseManager.Surname = eSe.Surname;
                            eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;

                            XDSPortalEnquiry.Data.Consumer oConsumer = new XDSPortalEnquiry.Data.Consumer();

                            if (eoConsumerTraceWseManager.ConsumerID == 0)
                            {
                                eoConsumerTraceWseManager.ConsumerID = oConsumer.GetConsumerID(objConstring, 0, eoConsumerTraceWseManager.HomeAffairsID);
                            }

                            XDSPortalEnquiry.Data.Consumer oEnquiryConsumer = new XDSPortalEnquiry.Data.Consumer();
                            oEnquiryConsumer.CheckBlockedStatus(AdminConnection, int.Parse(eSe.SubscriberID.ToString()), int.Parse(eoConsumerTraceWseManager.ConsumerID.ToString()), eSe.ProductID, sub.SubscriberAssociationCode, eSe.ProductID);

                            rp = ra.CommercialRenderLexisNexisData(rp.ResponseData, ClientID, UserID, Password, URL);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.CommercialRenderLexisNexisData(rp.ResponseData, ClientID, UserID, Password, URL);
                            }

                            rXml = rp.ResponseData;

                            System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                            {

                                eSC.DetailsViewedYN = false;
                                eSC.Billable = false;
                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                                eSC.CreatedByUser = eSe.CreatedByUser;
                                eSC.CreatedOnDate = DateTime.Now;

                                dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                rp.EnquiryID = eSC.SubscriberEnquiryID;

                            }

                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {

                                eSC.EnquiryResult = "E";
                                eSe.ErrorDescription = rp.ResponseData.ToString();
                                dSe.UpdateSubscriberEnquiryError(con, eSe);


                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Bonus)
                            {
                                ds.ReadXml(xmlSR);
                                if (ds.Tables.Contains("Segments"))
                                {

                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.BonusIncluded = true;
                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;
                                    eSC.XMLBonus = rXml.Replace("'", "''");
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.B.ToString();

                                    dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = intSubscriberEnquiryID;
                                    rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                    foreach (DataRow r in ds.Tables["Segments"].Rows)
                                    {
                                        eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                        eSB.DataSegmentID = int.Parse(r["DataSegmentID"].ToString());
                                        eSB.DataSegmentName = r["DataSegmentName"].ToString().Replace("'", "''");
                                        eSB.DataSegmentDisplayText = r["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                        eSB.BonusViewed = bool.Parse(r["BonusViewed"].ToString());
                                        eSB.Billable = false;
                                        eSB.CreatedByUser = eSe.CreatedByUser;
                                        eSB.CreatedOnDate = DateTime.Now;

                                        dSB.InsertSubscriberEnquiryResultBonus(con, eSB);
                                    }
                                }
                            }

                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                ds = new DataSet();
                                ds.ReadXml(xmlSR);
                                if (ds.Tables.Contains("CommercialBusinessInformation"))
                                {
                                    Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                    Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, ProductID);

                                    //////////////////////////
                                    DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                    dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                    dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                    dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                    dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                    dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                    dtSubscriberInput.Columns.Add("TrustNumber", typeof(String));
                                    dtSubscriberInput.Columns.Add("PrebuiltPackage", typeof(String));
                                    dtSubscriberInput.Columns.Add("VoucherCode", typeof(String));
                                    dtSubscriberInput.Columns.Add("Country", typeof(String));
                                    dtSubscriberInput.Columns.Add("RegistrationNumber", typeof(String));
                                    dtSubscriberInput.Columns.Add("BusinessName", typeof(String));
                                    dtSubscriberInput.Columns.Add("VatNumber", typeof(String));
                                    dtSubscriberInput.Columns.Add("IDNumber", typeof(String));
                                    dtSubscriberInput.Columns.Add("NPONumber", typeof(String));
                                    dtSubscriberInput.Columns.Add("ExternalReference", typeof(String));
                                    dtSubscriberInput.Columns.Add("Reference", typeof(String));
                                    dtSubscriberInput.Columns.Add("LegalEntity", typeof(String));
                                    DataRow drSubscriberInput;
                                    drSubscriberInput = dtSubscriberInput.NewRow();

                                    drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                    drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                    drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                    drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                    drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();
                                    drSubscriberInput["VoucherCode"] = eSC.VoucherCode;
                                    drSubscriberInput["Country"] = "South Africa";
                                    drSubscriberInput["ExternalReference"] = eSe.SubscriberReference;
                                    drSubscriberInput["Reference"] = eSC.Reference;

                                    if (eSe.IDNo == "" && eSe.BusRegistrationNo == "")
                                    {
                                        drSubscriberInput["RegistrationNumber"] = "";
                                        drSubscriberInput["BusinessName"] = eSe.BusBusinessName;
                                        drSubscriberInput["VatNumber"] = "";
                                        drSubscriberInput["IDNumber"] = "";
                                        drSubscriberInput["NPONumber"] = "";
                                        drSubscriberInput["TrustNumber"] = "";
                                        drSubscriberInput["LegalEntity"] = "Registered Company";
                                    }

                                    else if (eSe.IDNo != "")
                                    {
                                        drSubscriberInput["RegistrationNumber"] = "";
                                        drSubscriberInput["BusinessName"] = eSe.BusBusinessName;
                                        drSubscriberInput["VatNumber"] = "";
                                        drSubscriberInput["IDNumber"] = eSe.IDNo;
                                        drSubscriberInput["NPONumber"] = "";
                                        drSubscriberInput["TrustNumber"] = "";
                                        drSubscriberInput["LegalEntity"] = "Sole Proprietor";
                                    }
                                    else if (eSe.BusVatNumber != "")
                                    {
                                        drSubscriberInput["RegistrationNumber"] = "";
                                        drSubscriberInput["BusinessName"] = eSe.BusBusinessName;
                                        drSubscriberInput["VatNumber"] = eSe.BusVatNumber;
                                        drSubscriberInput["IDNumber"] = "";
                                        drSubscriberInput["NPONumber"] = "";
                                        drSubscriberInput["TrustNumber"] = "";
                                        drSubscriberInput["LegalEntity"] = "Vat Number";
                                    }

                                    else if (eSe.BusRegistrationNo != "")
                                    {
                                        drSubscriberInput["RegistrationNumber"] = eSe.BusRegistrationNo;
                                        drSubscriberInput["BusinessName"] = eSe.BusBusinessName;
                                        drSubscriberInput["VatNumber"] = "";
                                        drSubscriberInput["IDNumber"] = "";
                                        drSubscriberInput["NPONumber"] = "";
                                        drSubscriberInput["TrustNumber"] = "";
                                        drSubscriberInput["LegalEntity"] = "Registered Company";

                                    }

                                    dtSubscriberInput.Rows.Add(drSubscriberInput);

                                    ds.Tables.Add(dtSubscriberInput);
                                    ///////////////////////////
                                    if (eSC.BonusIncluded == true)
                                    {
                                        Data.SubscriberEnquiryResultBonus oSubscriberEnquiryResultBonus = new XDSPortalEnquiry.Data.SubscriberEnquiryResultBonus();
                                        //DataSet dsBonus = oSubscriberEnquiryResultBonus.GetSelectedSubscriberEnquiryResultBonus(con, eSC.SubscriberEnquiryResultID);
                                        DataSet dsBonus = eoConsumerTraceWseManager.BonusSegments;

                                        DataTable dtBonusSelected = new DataTable("ConsumerBonusSelected");

                                        dtBonusSelected.Columns.Add("DataSegmentID", typeof(String));
                                        dtBonusSelected.Columns.Add("DataSegmentName", typeof(String));
                                        dtBonusSelected.Columns.Add("DataSegmentDisplayText", typeof(String));

                                        DataRow drBonusSelected;
                                        if (dsBonus != null)
                                        {
                                            if (dsBonus.Tables[0].Rows.Count > 0)
                                            {
                                                foreach (DataRow r in dsBonus.Tables[0].Rows)
                                                {
                                                    drBonusSelected = dtBonusSelected.NewRow();
                                                    drBonusSelected["DataSegmentID"] = r["DataSegmentID"];
                                                    drBonusSelected["DataSegmentName"] = r["DataSegmentName"];
                                                    drBonusSelected["DataSegmentDisplayText"] = r["DataSegmentDisplayText"];
                                                    dtBonusSelected.Rows.Add(drBonusSelected);

                                                    eSB.SubscriberEnquiryResultBonusID = 0;
                                                    eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                                    eSB.DataSegmentID = Convert.ToInt32(r["DataSegmentID"]);
                                                    eSB.DataSegmentName = r["DataSegmentName"].ToString();
                                                    eSB.DataSegmentDisplayText = r["DataSegmentDisplayText"].ToString();
                                                    eSB.BonusViewed = Convert.ToBoolean(r["BonusViewed"].ToString());
                                                    if (eSB.BonusViewed)
                                                    {
                                                        eSB.Billable = true;
                                                    }
                                                    else
                                                    {
                                                        eSB.Billable = false;
                                                    }
                                                    eSB.ChangedByUser = eSB.CreatedByUser;
                                                    eSB.ChangedOnDate = DateTime.Now;
                                                    dSB.UpdateSubscriberEnquiryResultBonus(con, eSB);
                                                }
                                                ds.Tables.Add(dtBonusSelected);
                                            }
                                            dsBonus.Dispose();
                                        }
                                    }

									//removed commercial scoring from xml
									if (ds.Tables.Contains("CommercialScoring"))
									    ds.Tables.Remove("CommercialScoring");

                                    rXml = ds.GetXml();
                                    rp.ResponseData = rXml;

                                    foreach (DataRow r in ds.Tables["CommercialBusinessInformation"].Rows)
                                    {
                                        Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                        Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                        eSC.DetailsViewedDate = DateTime.Now;
                                        eSC.DetailsViewedYN = true;
                                        if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                        {
                                            eSC.Billable = false;
                                        }
                                        else
                                        {
                                            eSC.Billable = true;
                                        }
                                        eSC.ChangedByUser = eSe.CreatedByUser;
                                        eSC.ChangedOnDate = DateTime.Now;
                                        eSC.SearchOutput = "";


                                        if (r.Table.Columns.Contains("CommercialName"))
                                        {
                                            if (!string.IsNullOrEmpty(r["CommercialName"].ToString()))
                                            {
                                                eSC.BusBusinessName = r["CommercialName"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusBusinessName;
                                            }
                                        }

                                        if (r.Table.Columns.Contains("RegistrationNo"))
                                        {
                                            if (!string.IsNullOrEmpty(r["RegistrationNo"].ToString()))
                                            {
                                                eSC.BusRegistrationNo = r["RegistrationNo"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusRegistrationNo;
                                            }
                                        }

                                        if (eSC.SearchOutput.Length > 0)
                                        {
                                            eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                        }

                                        eSC.DetailsViewedYN = true;
                                        eSC.Billable = true;
                                        eSC.CreatedByUser = eSe.CreatedByUser;
                                        eSC.CreatedOnDate = DateTime.Now;

                                        eSC.KeyID = rp.ResponseKey;
                                        eSC.KeyType = rp.ResponseKeyType;
                                        eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                        eSC.BillingTypeID = spr.BillingTypeID;
                                        eSC.BillingPrice = spr.UnitPrice;

                                        eSC.XMLData = rXml.Replace("'", "''");
                                        eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                        eSC.ProductID = ProductID;

                                        dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                        // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                        if (sub.PayAsYouGo == 1)
                                        {
                                            dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                        }
                                        if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                        {
                                            dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                        }

                                        if (String.IsNullOrEmpty(eSC.VoucherCode))
                                        {
                                            //Log FootPrint

                                            eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                            eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                            eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                            eSubscriberFootPrint.KeyID = eSC.KeyID;
                                            eSubscriberFootPrint.KeyType = eSC.KeyType;
                                            eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                            eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                            if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                            {
                                                eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                            }
                                            eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                            eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                            eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                            eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                            eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                            eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                            eSubscriberFootPrint.CreatedByUser = sys.Username;
                                            eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                            eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                            eSubscriberFootPrint.ProductID = ProductID;
                                            eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                            int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                        }
                                    }
                                }
                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                            {
                                ds.ReadXml(xmlSR);
                                if (ds.Tables.Contains("ConsumerDetails"))
                                {
                                    foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                    {

                                        eSC.SearchOutput = "";

                                        if (r.Table.Columns.Contains("HomeAffairsSurname"))
                                        {
                                            if (!string.IsNullOrEmpty(r["HomeAffairsSurname"].ToString()))
                                            {
                                                eSC.Surname = r["HomeAffairsSurname"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                            }
                                        }

                                        if (r.Table.Columns.Contains("HomeAffairsFirstName"))
                                        {
                                            if (!string.IsNullOrEmpty(r["HomeAffairsFirstName"].ToString()))
                                            {
                                                eSC.FirstName = r["HomeAffairsFirstName"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                            }
                                        }

                                        if (eSC.SearchOutput.Length > 0)
                                        {
                                            eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                        }

                                        eSC.DetailsViewedYN = false;
                                        eSC.Billable = false;
                                        eSC.ChangedByUser = eSe.CreatedByUser;
                                        eSC.ChangedOnDate = DateTime.Now;
                                        eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                        eSC.KeyType = rp.ResponseKeyType;

                                        eSC.BillingTypeID = spr.BillingTypeID;
                                        eSC.BillingPrice = spr.UnitPrice;

                                        eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                        eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();

                                        dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                }
                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                            {
                                ds.ReadXml(xmlSR);
                                if (ds.Tables.Contains("ConsumerDetails"))
                                {
                                    foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                    {
                                        eSC.SearchOutput = "";

                                        if (r.Table.Columns.Contains("HomeAffairsSurname"))
                                        {
                                            if (!string.IsNullOrEmpty(r["HomeAffairsSurname"].ToString()))
                                            {
                                                eSC.Surname = r["HomeAffairsSurname"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                            }
                                        }

                                        if (r.Table.Columns.Contains("HomeAffairsFirstName"))
                                        {
                                            if (!string.IsNullOrEmpty(r["HomeAffairsFirstName"].ToString()))
                                            {
                                                eSC.FirstName = r["HomeAffairsFirstName"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                            }
                                        }

                                        if (eSC.SearchOutput.Length > 0)
                                        {
                                            eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                        }

                                        eSC.DetailsViewedYN = false;
                                        eSC.Billable = false;
                                        eSC.CreatedByUser = eSe.CreatedByUser;
                                        eSC.CreatedOnDate = DateTime.Now;
                                        eSC.KeyID = rp.ResponseKey; ;
                                        eSC.KeyType = rp.ResponseKeyType;

                                        eSC.BillingTypeID = spr.BillingTypeID;
                                        eSC.BillingPrice = spr.UnitPrice;

                                        eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                        eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();

                                        dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                }
                            }

                        }
                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.
                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                        }
                    }
                    else
                    {
                        // When User want to ReOpen a report 
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = eSC.XMLData;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }
                con.Close();
                AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            ds.Dispose();

            return rp;

        }

        public XDSPortalLibrary.Entity_Layer.Response RenderSanctionsStandardData(SqlConnection con, SqlConnection AdminConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, DataSet dsBonusDataSegment, int ProductID)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            string rXml = "";
            double Totalcost = 0;
            string reportXml = "", MinScore = "", MemberKey = "", Password = "", URL1 = "", URL2 = "", FirstName = "", Surname = "", Nationality = "",
                Citizenship = "", Country = "";

            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                SqlConnection objConstring = new SqlConnection(eSe.ExtraVarInput1.ToString());

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                if (spr.ReportID > 0)
                {
                    eoConsumerTraceWseManager.BonusSegments = null;
                    if (dsBonusDataSegment != null)
                    {
                        if (dsBonusDataSegment.Tables.Count > 0 && dsBonusDataSegment.Tables[0].Rows.Count > 0)
                        {
                            dsBonusDataSegment.DataSetName = "BonusSegments";
                            dsBonusDataSegment.Tables[0].TableName = "Segment";
                            eoConsumerTraceWseManager.BonusSegments = dsBonusDataSegment;
                        }
                    }

                    // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML 
                    // use this XML data for generating the Report 
                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {
                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }
                        }
                        else if (sub.PayAsYouGo == 1)
                        {
                            //Calculate The Total cost of the Report , including Bonus Segments
                            Totalcost = spr.UnitPrice;

                            if (eoConsumerTraceWseManager.BonusSegments != null)
                            {
                                foreach (DataRow dr in eoConsumerTraceWseManager.BonusSegments.Tables[0].Rows)
                                {
                                    if (dr["BonusViewed"].ToString().ToLower() == "true")
                                    {
                                        Totalcost = Totalcost + Convert.ToInt16(dr["BonusPrice"].ToString());
                                    }
                                }
                            }
                        }

                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report
                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {
                            if (eSC.KeyType == "H")
                            {
                                eoConsumerTraceWseManager.HomeAffairsID = eSC.KeyID;
                            }
                            else
                            {
                                eoConsumerTraceWseManager.ConsumerID = eSC.KeyID;
                            }
                            eoConsumerTraceWseManager.ProductID = eSe.ProductID;
                            eoConsumerTraceWseManager.subscriberID = eSe.SubscriberID;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.ReferenceNo = eSC.Reference;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.TmpReference = eSC.ExtraVarOutput1;
                            moConsumerTraceWseManager.ConnectionString = objConstring;
                            eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                            eoConsumerTraceWseManager.IDno = eSe.IDNo;
                            eoConsumerTraceWseManager.Firstname = eSe.FirstName;
                            eoConsumerTraceWseManager.Surname = eSe.Surname;
                            eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;

                            XDSPortalEnquiry.Data.Consumer oConsumer = new XDSPortalEnquiry.Data.Consumer();

                            if (eoConsumerTraceWseManager.ConsumerID == 0)
                            {
                                eoConsumerTraceWseManager.ConsumerID = oConsumer.GetConsumerID(objConstring, 0, eoConsumerTraceWseManager.HomeAffairsID);
                            }

                            XDSPortalEnquiry.Data.Consumer oEnquiryConsumer = new XDSPortalEnquiry.Data.Consumer();
                            oEnquiryConsumer.CheckBlockedStatus(AdminConnection, int.Parse(eSe.SubscriberID.ToString()), int.Parse(eoConsumerTraceWseManager.ConsumerID.ToString()), eSe.ProductID, sub.SubscriberAssociationCode, eSe.ProductID);

                            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();
                            MultipleTrace_IdentityVerification Mt = new MultipleTrace_IdentityVerification();
                            DataSet ds1 = new DataSet();
                            XDSPortalLibrary.Entity_Layer.Response rp_Buerau = new XDSPortalLibrary.Entity_Layer.Response();
                            rp_Buerau = Mt.GetBuerauDetails(con, AdminConnection, eSC.SubscriberEnquiryID, eSC.SubscriberEnquiryResultID, ds1, false, "");
                            if (rp_Buerau.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                System.Xml.XmlDocument rsXmlB = new System.Xml.XmlDocument();
                                if (!string.IsNullOrEmpty(rp_Buerau.ResponseData))
                                {
                                    rsXmlB.LoadXml(rp_Buerau.ResponseData);
                                    string ConsumerDetails = rsXmlB.OuterXml;
                                    System.IO.StringReader xmlSRTIDVB = new System.IO.StringReader(ConsumerDetails);
                                    DataSet consumerds = new DataSet();
                                    consumerds.ReadXml(xmlSRTIDVB);
                                    if (consumerds.Tables.Contains("ConsumerDetail"))
                                    {
                                        string XML = consumerds.GetXml();

                                        rsXml.LoadXml(XML);
                                        reportXml = rsXml.OuterXml;
                                    }
                                    else
                                    {
                                        if (consumerds.Tables.Count > 0)
                                        {
                                            string XML = consumerds.GetXml();

                                            rsXml.LoadXml(XML);
                                            reportXml = rsXml.OuterXml;
                                        }
                                    }
                                }
                            }

                            XDSPortalEnquiry.Data.XDSSettings odSystemUser = new XDSPortalEnquiry.Data.XDSSettings();
                            Entity.SanctionsConfigSettings objSettings = odSystemUser.GetSanctionStandardSettings(AdminConnection);
                            MemberKey = objSettings.MemberKey;
                            Password = objSettings.Password;
                            URL1 = objSettings.URL1;
                            URL2 = objSettings.URL2;
                            MinScore = sub.SanctionsMinScore.ToString();

                            Entity.SubscriberSanction eSs = new Entity.SubscriberSanction();
                            eSs = dSe.GetSubscriberSanctionObject(con, intSubscriberEnquiryID);
                            FirstName = eSs.FirstName;
                            Surname = eSs.Surname;
                            Nationality = eSs.Nationality;
                            Citizenship = eSs.Citizenship;
                            Country = eSs.Country;

                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.RenderDatanamixSanctionsData(reportXml, MinScore, MemberKey, Password, URL1, URL2, FirstName, Surname, Nationality, Citizenship, Country);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.RenderDatanamixSanctionsData(reportXml, MinScore, MemberKey, Password, URL1, URL2, FirstName, Surname, Nationality, Citizenship, Country);
                            }

                            rXml = rp.ResponseData;

                            System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                            {
                                eSC.DetailsViewedYN = false;
                                eSC.Billable = false;
                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                                eSC.CreatedByUser = eSe.CreatedByUser;
                                eSC.CreatedOnDate = DateTime.Now;

                                dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                rp.EnquiryID = eSC.SubscriberEnquiryID;
                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSC.EnquiryResult = "E";
                                eSe.ErrorDescription = rp.ResponseData.ToString();
                                dSe.UpdateSubscriberEnquiryError(con, eSe);
                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Bonus)
                            {
                                ds.ReadXml(xmlSR);
                                if (ds.Tables.Contains("Segments"))
                                {
                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.BonusIncluded = true;
                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;
                                    eSC.XMLBonus = rXml.Replace("'", "''");
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.B.ToString();

                                    dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = intSubscriberEnquiryID;
                                    rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                    foreach (DataRow r in ds.Tables["Segments"].Rows)
                                    {
                                        eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                        eSB.DataSegmentID = int.Parse(r["DataSegmentID"].ToString());
                                        eSB.DataSegmentName = r["DataSegmentName"].ToString().Replace("'", "''");
                                        eSB.DataSegmentDisplayText = r["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                        eSB.BonusViewed = bool.Parse(r["BonusViewed"].ToString());
                                        eSB.Billable = false;
                                        eSB.CreatedByUser = eSe.CreatedByUser;
                                        eSB.CreatedOnDate = DateTime.Now;

                                        dSB.InsertSubscriberEnquiryResultBonus(con, eSB);
                                    }
                                }
                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                if (!string.IsNullOrEmpty(rp.ResponseData))
                                {
                                    ds.ReadXml(xmlSR);

                                    Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                    Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, ProductID);

                                    DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                    dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                    dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                    dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                    dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                    dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                    dtSubscriberInput.Columns.Add("EnquiryReference", typeof(String));
                                    dtSubscriberInput.Columns.Add("YourReference", typeof(String));
                                    DataRow drSubscriberInput;
                                    drSubscriberInput = dtSubscriberInput.NewRow();

                                    drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                    drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                    drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                    drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                    drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();
                                    drSubscriberInput["EnquiryReference"] = "R-" + intSubscriberEnquiryID.ToString();
                                    drSubscriberInput["YourReference"] = "";
                                    dtSubscriberInput.Rows.Add(drSubscriberInput);

                                    if (ds.Tables.Contains("SubscriberInputDetails"))
                                    {
                                        ds.Tables.Remove(ds.Tables["SubscriberInputDetails"].ToString());
                                    }

                                    ds.Tables.Add(dtSubscriberInput);
                                    rXml = ds.GetXml();

                                    Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                    Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                    {
                                        eSC.Billable = false;
                                    }
                                    else
                                    {
                                        eSC.Billable = true;
                                    }

                                    eSC.ChangedByUser = eSe.CreatedByUser;
                                    eSC.ChangedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.DetailsViewedYN = true;
                                    eSC.Billable = true;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.KeyID = rp.ResponseKey; ;
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                    eSC.ProductID = ProductID;

                                    dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    rp.ResponseData = rXml;

                                    // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                    if (sub.PayAsYouGo == 1)
                                    {
                                        dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                    }

                                    if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                    {
                                        dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                    }

                                    if (String.IsNullOrEmpty(eSC.VoucherCode))
                                    {
                                        //Log FootPrint
                                        eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                        eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                        eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                        eSubscriberFootPrint.KeyID = eSC.KeyID;
                                        eSubscriberFootPrint.KeyType = eSC.KeyType;
                                        eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                        eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                        if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                        {
                                            eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                        }
                                        eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                        eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                        eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                        eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                        eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                        eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                        eSubscriberFootPrint.CreatedByUser = sys.Username;
                                        eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                        eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                        eSubscriberFootPrint.ProductID = eSe.ProductID;
                                        eSubscriberFootPrint.FootprintActive = spr.FootprintActive;
                                        int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                    }
                                }
                                else
                                {
                                    Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                    Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, ProductID);

                                    DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                    dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                    dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                    dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                    dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                    dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                    dtSubscriberInput.Columns.Add("EnquiryReference", typeof(String));
                                    dtSubscriberInput.Columns.Add("YourReference", typeof(String));
                                    DataRow drSubscriberInput;
                                    drSubscriberInput = dtSubscriberInput.NewRow();

                                    drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                    drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                    drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                    drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                    drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();
                                    drSubscriberInput["EnquiryReference"] = "R-" + intSubscriberEnquiryID.ToString();
                                    drSubscriberInput["YourReference"] = "";
                                    DataSet ds2 = new DataSet("HomeAffairs");
                                    dtSubscriberInput.Rows.Add(drSubscriberInput);
                                    if (ds2.Tables.Contains("SubscriberInputDetails"))
                                    {
                                        ds2.Tables.Remove(ds1.Tables["SubscriberInputDetails"].ToString());
                                    }
                                    ds2.Tables.Add(dtSubscriberInput);
                                    rXml = ds2.GetXml();
                                    // rp.ResponseData = rXml;
                                    Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                    Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                    {
                                        eSC.Billable = false;
                                    }
                                    else
                                    {
                                        eSC.Billable = true;
                                    }
                                    eSC.ChangedByUser = eSe.CreatedByUser;
                                    eSC.ChangedOnDate = DateTime.Now;
                                    eSC.SearchOutput = "";

                                    if (eSC.SearchOutput.Length > 0)
                                    {
                                        eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                    }

                                    eSC.DetailsViewedYN = true;
                                    eSC.Billable = true;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.KeyID = rp.ResponseKey; ;
                                    eSC.KeyType = rp.ResponseKeyType;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                    eSC.ProductID = ProductID;

                                    dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    rp.ResponseData = rXml;
                                    // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                    if (sub.PayAsYouGo == 1)
                                    {
                                        dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                    }
                                    if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                    {
                                        dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                    }
                                    if (String.IsNullOrEmpty(eSC.VoucherCode))
                                    {
                                        //Log FootPrint
                                        eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                        eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                        eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                        eSubscriberFootPrint.KeyID = eSC.KeyID;
                                        eSubscriberFootPrint.KeyType = eSC.KeyType;
                                        eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                        eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                        if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                        {
                                            eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                        }
                                        eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                        eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                        eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                        eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                        eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                        eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                        eSubscriberFootPrint.CreatedByUser = sys.Username;
                                        eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                        eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                        eSubscriberFootPrint.ProductID = eSe.ProductID;
                                        eSubscriberFootPrint.FootprintActive = spr.FootprintActive;
                                        int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                    }
                                }
                            }
                        }
                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.
                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                        }
                    }
                    else
                    {
                        // When User want to ReOpen a report 
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = eSC.XMLData;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }

            ds.Dispose();

            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response CommercialRenderSanctionsStandardData(SqlConnection con, SqlConnection AdminConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, DataSet dsBonusDataSegment, int ProductID)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();


            string rXml = "";
            double Totalcost = 0;
            string reportXml = "", MinScore = "", MemberKey = "", Password = "", URL1 = "", URL2 = "";

            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                SqlConnection objConstring = new SqlConnection(eSe.ExtraVarInput1.ToString());


                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                Data.Reports dRe = new Data.Reports();

                Entity.Reports eRe = new Entity.Reports();

                eRe = dRe.GetProductReportsRecord(AdminConnection, spr.ReportID);

                if (spr.ReportID > 0)
                {
                    eRe = dRe.GetProductReportsRecord(AdminConnection, spr.ReportID);
                    eoConsumerTraceWseManager.BonusSegments = null;
                    if (dsBonusDataSegment != null)
                    {
                        if (dsBonusDataSegment.Tables.Count > 0 && dsBonusDataSegment.Tables[0].Rows.Count > 0)
                        {
                            dsBonusDataSegment.DataSetName = "BonusSegments";
                            dsBonusDataSegment.Tables[0].TableName = "Segment";
                            eoConsumerTraceWseManager.BonusSegments = dsBonusDataSegment;
                        }
                    }

                    // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML 
                    // use this XML data for generating the Report 
                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {
                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }
                        else if (sub.PayAsYouGo == 1)
                        {
                            //Calculate The Total cost of the Report , including Bonus Segments

                            Totalcost = spr.UnitPrice;

                            if (eoConsumerTraceWseManager.BonusSegments != null)
                            {

                                foreach (DataRow dr in eoConsumerTraceWseManager.BonusSegments.Tables[0].Rows)
                                {
                                    if (dr["BonusViewed"].ToString().ToLower() == "true")
                                    {
                                        Totalcost = Totalcost + Convert.ToInt16(dr["BonusPrice"].ToString());
                                    }
                                }
                            }
                        }

                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {
                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);

                            if (eSC.KeyID != 0)
                            {
                                XDSPortalLibrary.Entity_Layer.BusinessEnquiry eoCommercialEnquiryWseManager = new XDSPortalLibrary.Entity_Layer.BusinessEnquiry();

                                eoCommercialEnquiryWseManager.BusinessName = eSC.BusBusinessName;
                                eoCommercialEnquiryWseManager.CommercialID = eSC.KeyID;
                                eoCommercialEnquiryWseManager.ExternalReference = eSe.SubscriberReference;
                                eoCommercialEnquiryWseManager.ProductID = ProductID;
                                eoCommercialEnquiryWseManager.ReferenceNo = eSC.Reference;
                                eoCommercialEnquiryWseManager.RegistrationNo = eSC.BusRegistrationNo;
                                eoCommercialEnquiryWseManager.subscriberID = eSe.SubscriberID;
                                eoCommercialEnquiryWseManager.TmpReference = eSC.ExtraVarOutput1;
                                //moCommercialEnquiryWseManager.ConnectionString = objConstring;
                                eoCommercialEnquiryWseManager.DataSegments = eSe.ExtraVarInput2;
                                eoCommercialEnquiryWseManager.ReportID = eRe.ReportID;
                                eoCommercialEnquiryWseManager.ReportName = eRe.ReportName;

                                rp = ra.GetBusinessEnquiryReport(eoCommercialEnquiryWseManager);

                                if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                                {
                                    eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                    ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                    rp = ra.GetBusinessEnquiryReport(eoCommercialEnquiryWseManager);
                                }
                            }
                            else
                            {
                                ds = new DataSet("Commercial");

                                //DataSet dsReport = new DataSet("Commercial");

                                DataTable dtReportInformation = new DataTable("ReportInformation");
                                DataColumn dcReportID = new DataColumn("ReportID");
                                DataColumn dcReportName = new DataColumn("ReportName");

                                dtReportInformation.Columns.Add(dcReportID);
                                dtReportInformation.Columns.Add(dcReportName);

                                DataRow drReportInformation = dtReportInformation.NewRow();
                                //drReportInformation["ReportID"] = dsDataSegment.Tables[0].Rows[0]["ReportID"].ToString();
                                //drReportInformation["ReportName"] = dsDataSegment.Tables[0].Rows[0]["ReportName"].ToString();
                                drReportInformation["ReportID"] = eRe.ReportID.ToString();
                                drReportInformation["ReportName"] = eRe.ReportName.ToString();

                                dtReportInformation.Rows.Add(drReportInformation);
                                ds.Tables.Add(dtReportInformation);

                                DataTable dtBusinessInformation = new DataTable("CommercialBusinessInformation");
                                dtBusinessInformation.Columns.Add("DisplayText", typeof(String));
                                dtBusinessInformation.Columns.Add("CommercialName", typeof(String));
                                dtBusinessInformation.Columns.Add("RegistrationNo", typeof(String));
                                dtBusinessInformation.Columns.Add("BusinessStartDate", typeof(DateTime));
                                dtBusinessInformation.Columns.Add("FinancialYearEnd", typeof(String));
                                dtBusinessInformation.Columns.Add("RegistrationNoOld", typeof(String));
                                dtBusinessInformation.Columns.Add("CommercialStatus", typeof(String));
                                dtBusinessInformation.Columns.Add("CommercialType", typeof(String));
                                dtBusinessInformation.Columns.Add("SIC", typeof(String));
                                dtBusinessInformation.Columns.Add("TaxNo", typeof(String));
                                dtBusinessInformation.Columns.Add("ReferenceNo", typeof(String));
                                dtBusinessInformation.Columns.Add("ExternalReference", typeof(String));
                                dtBusinessInformation.Columns.Add("TradeName", typeof(String));
                                dtBusinessInformation.Columns.Add("PreviousBussName", typeof(String));
                                dtBusinessInformation.Columns.Add("PhysicalAddress", typeof(String));
                                dtBusinessInformation.Columns.Add("PostalAddress", typeof(String));
                                dtBusinessInformation.Columns.Add("RegistrationDate", typeof(DateTime));
                                dtBusinessInformation.Columns.Add("BusinessDesc", typeof(String));
                                dtBusinessInformation.Columns.Add("TelephoneNo", typeof(String));
                                dtBusinessInformation.Columns.Add("FaxNo", typeof(String));
                                dtBusinessInformation.Columns.Add("BussEmail", typeof(String));
                                dtBusinessInformation.Columns.Add("BussWebsite", typeof(String));
                                dtBusinessInformation.Columns.Add("NoOfEnquiries", typeof(Int16));
                                dtBusinessInformation.Columns.Add("NameChangeDate", typeof(DateTime));
                                dtBusinessInformation.Columns.Add("AgeofBusiness", typeof(String));
                                dtBusinessInformation.Columns.Add("AuthorisedCapitalAmt", typeof(Decimal));
                                dtBusinessInformation.Columns.Add("IssuedNoOfShares", typeof(Int16));
                                dtBusinessInformation.Columns.Add("RegistrationNoConverted", typeof(String));
                                dtBusinessInformation.Columns.Add("FinancialEffectiveDate", typeof(DateTime));
                                dtBusinessInformation.Columns.Add("AuthorisedNoOfShares", typeof(Int16));
                                dtBusinessInformation.Columns.Add("IssuedCapitalAmt", typeof(Decimal));
                                dtBusinessInformation.Columns.Add("CommercialStatusDate", typeof(DateTime));
                                DataRow drSubscriberInput;
                                drSubscriberInput = dtBusinessInformation.NewRow();

                                drSubscriberInput["DisplayText"] = "Commercial Business Information";
                                drSubscriberInput["CommercialName"] = eSC.BusBusinessName;
                                drSubscriberInput["RegistrationNo"] = eSC.BusRegistrationNo;
                                drSubscriberInput["BusinessStartDate"] = "1900-01-01";
                                drSubscriberInput["FinancialYearEnd"] = "";
                                drSubscriberInput["RegistrationNoOld"] = "";
                                drSubscriberInput["CommercialStatus"] = "";
                                drSubscriberInput["CommercialType"] = "";
                                drSubscriberInput["SIC"] = "";
                                drSubscriberInput["TaxNo"] = "";
                                drSubscriberInput["ReferenceNo"] = eSC.Reference;
                                drSubscriberInput["ExternalReference"] = eSe.SubscriberReference;
                                drSubscriberInput["TradeName"] = "";
                                drSubscriberInput["PreviousBussName"] = "";
                                drSubscriberInput["PhysicalAddress"] = "";
                                drSubscriberInput["PostalAddress"] = "";
                                drSubscriberInput["RegistrationDate"] = "1900-01-01";
                                drSubscriberInput["BusinessDesc"] = "No Information Available";
                                drSubscriberInput["TelephoneNo"] = "";
                                drSubscriberInput["FaxNo"] = "";
                                drSubscriberInput["BussEmail"] = "";
                                drSubscriberInput["BussWebsite"] = "";
                                drSubscriberInput["NoOfEnquiries"] = 0;
                                drSubscriberInput["NameChangeDate"] = "1900-01-01";
                                drSubscriberInput["AgeofBusiness"] = "";
                                drSubscriberInput["AuthorisedCapitalAmt"] = 0;
                                drSubscriberInput["IssuedNoOfShares"] = 0;
                                drSubscriberInput["RegistrationNoConverted"] = "";
                                drSubscriberInput["FinancialEffectiveDate"] = "1900-01-01";
                                drSubscriberInput["AuthorisedNoOfShares"] = 0;
                                drSubscriberInput["IssuedCapitalAmt"] = 0;
                                drSubscriberInput["CommercialStatusDate"] = "1900-01-01";

                                dtBusinessInformation.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtBusinessInformation);

                                rp.ResponseData = ds.GetXml();
                                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                            }

                            if (eSC.KeyType == "H")
                            {
                                eoConsumerTraceWseManager.HomeAffairsID = eSC.KeyID;
                            }
                            else
                            {
                                eoConsumerTraceWseManager.ConsumerID = eSC.KeyID;
                            }
                            eoConsumerTraceWseManager.ProductID = eSe.ProductID;
                            eoConsumerTraceWseManager.subscriberID = eSe.SubscriberID;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.ReferenceNo = eSC.Reference;
                            eoConsumerTraceWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerTraceWseManager.TmpReference = eSC.ExtraVarOutput1;
                            moConsumerTraceWseManager.ConnectionString = objConstring;
                            eoConsumerTraceWseManager.DataSegments = eSe.ExtraVarInput2;
                            eoConsumerTraceWseManager.IDno = eSe.IDNo;
                            eoConsumerTraceWseManager.Firstname = eSe.FirstName;
                            eoConsumerTraceWseManager.Surname = eSe.Surname;
                            eoConsumerTraceWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;

                            XDSPortalEnquiry.Data.Consumer oConsumer = new XDSPortalEnquiry.Data.Consumer();

                            if (eoConsumerTraceWseManager.ConsumerID == 0)
                            {
                                eoConsumerTraceWseManager.ConsumerID = oConsumer.GetConsumerID(objConstring, 0, eoConsumerTraceWseManager.HomeAffairsID);
                            }

                            XDSPortalEnquiry.Data.Consumer oEnquiryConsumer = new XDSPortalEnquiry.Data.Consumer();
                            oEnquiryConsumer.CheckBlockedStatus(AdminConnection, int.Parse(eSe.SubscriberID.ToString()), int.Parse(eoConsumerTraceWseManager.ConsumerID.ToString()), eSe.ProductID, sub.SubscriberAssociationCode, eSe.ProductID);

                            XDSPortalEnquiry.Data.XDSSettings odSystemUser = new XDSPortalEnquiry.Data.XDSSettings();
                            Entity.SanctionsConfigSettings objSettings = odSystemUser.GetSanctionStandardSettings(AdminConnection);
                            MemberKey = objSettings.MemberKey;
                            Password = objSettings.Password;
                            URL1 = objSettings.URL1;
                            URL2 = objSettings.URL2;
                            MinScore = sub.SanctionsMinScore.ToString();
                            reportXml = rp.ResponseData;

                            rp = ra.CommercialRenderDatanamixSanctionsData(reportXml, MemberKey, Password, URL1, URL2, MinScore);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.CommercialRenderDatanamixSanctionsData(reportXml, MemberKey, Password, URL1, URL2, MinScore);
                            }

                            rXml = rp.ResponseData;

                            System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                            {
                                eSC.DetailsViewedYN = false;
                                eSC.Billable = false;
                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                                eSC.CreatedByUser = eSe.CreatedByUser;
                                eSC.CreatedOnDate = DateTime.Now;

                                dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                rp.EnquiryID = eSC.SubscriberEnquiryID;

                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSC.EnquiryResult = "E";
                                eSe.ErrorDescription = rp.ResponseData.ToString();
                                dSe.UpdateSubscriberEnquiryError(con, eSe);
                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Bonus)
                            {
                                ds.ReadXml(xmlSR);
                                if (ds.Tables.Contains("Segments"))
                                {
                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.BonusIncluded = true;
                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;
                                    eSC.XMLBonus = rXml.Replace("'", "''");
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.B.ToString();

                                    dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = intSubscriberEnquiryID;
                                    rp.EnquiryResultID = intSubscriberEnquiryResultID;

                                    foreach (DataRow r in ds.Tables["Segments"].Rows)
                                    {
                                        eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                        eSB.DataSegmentID = int.Parse(r["DataSegmentID"].ToString());
                                        eSB.DataSegmentName = r["DataSegmentName"].ToString().Replace("'", "''");
                                        eSB.DataSegmentDisplayText = r["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                        eSB.BonusViewed = bool.Parse(r["BonusViewed"].ToString());
                                        eSB.Billable = false;
                                        eSB.CreatedByUser = eSe.CreatedByUser;
                                        eSB.CreatedOnDate = DateTime.Now;

                                        dSB.InsertSubscriberEnquiryResultBonus(con, eSB);
                                    }
                                }
                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                ds = new DataSet();
                                ds.ReadXml(xmlSR);
                                if (ds.Tables.Contains("CommercialBusinessInformation"))
                                {
                                    Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                    Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, ProductID);

                                    //////////////////////////
                                    DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                    dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                    dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                    dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                    dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                    dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                    dtSubscriberInput.Columns.Add("TrustNumber", typeof(String));
                                    dtSubscriberInput.Columns.Add("PrebuiltPackage", typeof(String));
                                    dtSubscriberInput.Columns.Add("VoucherCode", typeof(String));
                                    dtSubscriberInput.Columns.Add("Country", typeof(String));
                                    dtSubscriberInput.Columns.Add("RegistrationNumber", typeof(String));
                                    dtSubscriberInput.Columns.Add("BusinessName", typeof(String));
                                    dtSubscriberInput.Columns.Add("VatNumber", typeof(String));
                                    dtSubscriberInput.Columns.Add("IDNumber", typeof(String));
                                    dtSubscriberInput.Columns.Add("NPONumber", typeof(String));
                                    dtSubscriberInput.Columns.Add("ExternalReference", typeof(String));
                                    dtSubscriberInput.Columns.Add("Reference", typeof(String));
                                    dtSubscriberInput.Columns.Add("LegalEntity", typeof(String));
                                    DataRow drSubscriberInput;
                                    drSubscriberInput = dtSubscriberInput.NewRow();

                                    drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                    drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                    drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                    drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                    drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();
                                    drSubscriberInput["VoucherCode"] = eSC.VoucherCode;
                                    drSubscriberInput["Country"] = "South Africa";
                                    drSubscriberInput["ExternalReference"] = eSe.SubscriberReference;
                                    drSubscriberInput["Reference"] = eSC.Reference;

                                    if (eSe.IDNo == "" && eSe.BusRegistrationNo == "")
                                    {
                                        drSubscriberInput["RegistrationNumber"] = "";
                                        drSubscriberInput["BusinessName"] = eSe.BusBusinessName;
                                        drSubscriberInput["VatNumber"] = "";
                                        drSubscriberInput["IDNumber"] = "";
                                        drSubscriberInput["NPONumber"] = "";
                                        drSubscriberInput["TrustNumber"] = "";
                                        drSubscriberInput["LegalEntity"] = "Registered Company";
                                    }

                                    else if (eSe.IDNo != "")
                                    {
                                        drSubscriberInput["RegistrationNumber"] = "";
                                        drSubscriberInput["BusinessName"] = eSe.BusBusinessName;
                                        drSubscriberInput["VatNumber"] = "";
                                        drSubscriberInput["IDNumber"] = eSe.IDNo;
                                        drSubscriberInput["NPONumber"] = "";
                                        drSubscriberInput["TrustNumber"] = "";
                                        drSubscriberInput["LegalEntity"] = "Sole Proprietor";
                                    }
                                    else if (eSe.BusVatNumber != "")
                                    {
                                        drSubscriberInput["RegistrationNumber"] = "";
                                        drSubscriberInput["BusinessName"] = eSe.BusBusinessName;
                                        drSubscriberInput["VatNumber"] = eSe.BusVatNumber;
                                        drSubscriberInput["IDNumber"] = "";
                                        drSubscriberInput["NPONumber"] = "";
                                        drSubscriberInput["TrustNumber"] = "";
                                        drSubscriberInput["LegalEntity"] = "Vat Number";
                                    }

                                    else if (eSe.BusRegistrationNo != "")
                                    {
                                        drSubscriberInput["RegistrationNumber"] = eSe.BusRegistrationNo;
                                        drSubscriberInput["BusinessName"] = eSe.BusBusinessName;
                                        drSubscriberInput["VatNumber"] = "";
                                        drSubscriberInput["IDNumber"] = "";
                                        drSubscriberInput["NPONumber"] = "";
                                        drSubscriberInput["TrustNumber"] = "";
                                        drSubscriberInput["LegalEntity"] = "Registered Company";

                                    }

                                    dtSubscriberInput.Rows.Add(drSubscriberInput);

                                    ds.Tables.Add(dtSubscriberInput);
                                    ///////////////////////////
                                    if (eSC.BonusIncluded == true)
                                    {
                                        Data.SubscriberEnquiryResultBonus oSubscriberEnquiryResultBonus = new XDSPortalEnquiry.Data.SubscriberEnquiryResultBonus();
                                        //DataSet dsBonus = oSubscriberEnquiryResultBonus.GetSelectedSubscriberEnquiryResultBonus(con, eSC.SubscriberEnquiryResultID);
                                        DataSet dsBonus = eoConsumerTraceWseManager.BonusSegments;

                                        DataTable dtBonusSelected = new DataTable("ConsumerBonusSelected");

                                        dtBonusSelected.Columns.Add("DataSegmentID", typeof(String));
                                        dtBonusSelected.Columns.Add("DataSegmentName", typeof(String));
                                        dtBonusSelected.Columns.Add("DataSegmentDisplayText", typeof(String));

                                        DataRow drBonusSelected;
                                        if (dsBonus != null)
                                        {
                                            if (dsBonus.Tables[0].Rows.Count > 0)
                                            {
                                                foreach (DataRow r in dsBonus.Tables[0].Rows)
                                                {
                                                    drBonusSelected = dtBonusSelected.NewRow();
                                                    drBonusSelected["DataSegmentID"] = r["DataSegmentID"];
                                                    drBonusSelected["DataSegmentName"] = r["DataSegmentName"];
                                                    drBonusSelected["DataSegmentDisplayText"] = r["DataSegmentDisplayText"];
                                                    dtBonusSelected.Rows.Add(drBonusSelected);

                                                    eSB.SubscriberEnquiryResultBonusID = 0;
                                                    eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                                    eSB.DataSegmentID = Convert.ToInt32(r["DataSegmentID"]);
                                                    eSB.DataSegmentName = r["DataSegmentName"].ToString();
                                                    eSB.DataSegmentDisplayText = r["DataSegmentDisplayText"].ToString();
                                                    eSB.BonusViewed = Convert.ToBoolean(r["BonusViewed"].ToString());
                                                    if (eSB.BonusViewed)
                                                    {
                                                        eSB.Billable = true;
                                                    }
                                                    else
                                                    {
                                                        eSB.Billable = false;
                                                    }
                                                    eSB.ChangedByUser = eSB.CreatedByUser;
                                                    eSB.ChangedOnDate = DateTime.Now;
                                                    dSB.UpdateSubscriberEnquiryResultBonus(con, eSB);
                                                }
                                                ds.Tables.Add(dtBonusSelected);
                                            }
                                            dsBonus.Dispose();
                                        }
                                    }

                                    rXml = ds.GetXml();
                                    rp.ResponseData = rXml;

                                    foreach (DataRow r in ds.Tables["CommercialBusinessInformation"].Rows)
                                    {
                                        Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                        Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                        eSC.DetailsViewedDate = DateTime.Now;
                                        eSC.DetailsViewedYN = true;
                                        if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                        {
                                            eSC.Billable = false;
                                        }
                                        else
                                        {
                                            eSC.Billable = true;
                                        }
                                        eSC.ChangedByUser = eSe.CreatedByUser;
                                        eSC.ChangedOnDate = DateTime.Now;
                                        eSC.SearchOutput = "";


                                        if (r.Table.Columns.Contains("CommercialName"))
                                        {
                                            if (!string.IsNullOrEmpty(r["CommercialName"].ToString()))
                                            {
                                                eSC.BusBusinessName = r["CommercialName"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusBusinessName;
                                            }
                                        }

                                        if (r.Table.Columns.Contains("RegistrationNo"))
                                        {
                                            if (!string.IsNullOrEmpty(r["RegistrationNo"].ToString()))
                                            {
                                                eSC.BusRegistrationNo = r["RegistrationNo"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.BusRegistrationNo;
                                            }
                                        }

                                        if (eSC.SearchOutput.Length > 0)
                                        {
                                            eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                        }

                                        eSC.DetailsViewedYN = true;
                                        eSC.Billable = true;
                                        eSC.CreatedByUser = eSe.CreatedByUser;
                                        eSC.CreatedOnDate = DateTime.Now;

                                        eSC.KeyID = rp.ResponseKey;
                                        eSC.KeyType = rp.ResponseKeyType;
                                        eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                        eSC.BillingTypeID = spr.BillingTypeID;
                                        eSC.BillingPrice = spr.UnitPrice;

                                        eSC.XMLData = rXml.Replace("'", "''");
                                        eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                        eSC.ProductID = ProductID;

                                        dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                        // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                        if (sub.PayAsYouGo == 1)
                                        {
                                            dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                        }
                                        if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                        {
                                            dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                        }

                                        if (String.IsNullOrEmpty(eSC.VoucherCode))
                                        {
                                            //Log FootPrint

                                            eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                            eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                            eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                            eSubscriberFootPrint.KeyID = eSC.KeyID;
                                            eSubscriberFootPrint.KeyType = eSC.KeyType;
                                            eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                            eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                            if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                            {
                                                eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                            }
                                            eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                            eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                            eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                            eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                            eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                            eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                            eSubscriberFootPrint.CreatedByUser = sys.Username;
                                            eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                            eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                            eSubscriberFootPrint.ProductID = ProductID;
                                            eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                            int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                        }
                                    }
                                }
                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                            {
                                ds.ReadXml(xmlSR);
                                if (ds.Tables.Contains("ConsumerDetails"))
                                {
                                    foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                    {

                                        eSC.SearchOutput = "";

                                        if (r.Table.Columns.Contains("HomeAffairsSurname"))
                                        {
                                            if (!string.IsNullOrEmpty(r["HomeAffairsSurname"].ToString()))
                                            {
                                                eSC.Surname = r["HomeAffairsSurname"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                            }
                                        }

                                        if (r.Table.Columns.Contains("HomeAffairsFirstName"))
                                        {
                                            if (!string.IsNullOrEmpty(r["HomeAffairsFirstName"].ToString()))
                                            {
                                                eSC.FirstName = r["HomeAffairsFirstName"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                            }
                                        }

                                        if (eSC.SearchOutput.Length > 0)
                                        {
                                            eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                        }

                                        eSC.DetailsViewedYN = false;
                                        eSC.Billable = false;
                                        eSC.ChangedByUser = eSe.CreatedByUser;
                                        eSC.ChangedOnDate = DateTime.Now;
                                        eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                        eSC.KeyType = rp.ResponseKeyType;

                                        eSC.BillingTypeID = spr.BillingTypeID;
                                        eSC.BillingPrice = spr.UnitPrice;

                                        eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                        eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();

                                        dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                }
                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                            {
                                ds.ReadXml(xmlSR);
                                if (ds.Tables.Contains("ConsumerDetails"))
                                {
                                    foreach (DataRow r in ds.Tables["ConsumerDetails"].Rows)
                                    {
                                        eSC.SearchOutput = "";

                                        if (r.Table.Columns.Contains("HomeAffairsSurname"))
                                        {
                                            if (!string.IsNullOrEmpty(r["HomeAffairsSurname"].ToString()))
                                            {
                                                eSC.Surname = r["HomeAffairsSurname"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                            }
                                        }

                                        if (r.Table.Columns.Contains("HomeAffairsFirstName"))
                                        {
                                            if (!string.IsNullOrEmpty(r["HomeAffairsFirstName"].ToString()))
                                            {
                                                eSC.FirstName = r["HomeAffairsFirstName"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                            }
                                        }

                                        if (eSC.SearchOutput.Length > 0)
                                        {
                                            eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                        }

                                        eSC.DetailsViewedYN = false;
                                        eSC.Billable = false;
                                        eSC.CreatedByUser = eSe.CreatedByUser;
                                        eSC.CreatedOnDate = DateTime.Now;
                                        eSC.KeyID = rp.ResponseKey; ;
                                        eSC.KeyType = rp.ResponseKeyType;

                                        eSC.BillingTypeID = spr.BillingTypeID;
                                        eSC.BillingPrice = spr.UnitPrice;

                                        eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                        eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();

                                        dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                }
                            }
                        }
                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.
                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        }
                    }
                    else
                    {
                        // When User want to ReOpen a report 
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = eSC.XMLData;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }
                con.Close();
                AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();

            return rp;
        }
    }
}
