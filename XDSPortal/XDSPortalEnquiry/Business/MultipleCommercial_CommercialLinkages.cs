﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDSPortalEnquiry.Business
{
    public class MultipleCommercial_CommercialLinkages
    {

        private XDSPortalLibrary.Entity_Layer.Linkages eoLinkagesEnquiryWseManager;


        public MultipleCommercial_CommercialLinkages()
        {

            eoLinkagesEnquiryWseManager = new XDSPortalLibrary.Entity_Layer.Linkages();

        }

        public XDSPortalLibrary.Entity_Layer.Response GetLinkedPrincipals(SqlConnection con, SqlConnection AdminConnection, SqlConnection logConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, DataSet dsBonusDataSegment, bool bBonusChecking, string strVoucherCode)
        {
            string strAdminCon = AdminConnection.ConnectionString;
            string strlogcon = logConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();


            string rXml = "";
            double Totalcost = 0;
            string ExtractType = string.Empty;
            DataSet ds = new DataSet();
            bool Billable = false;
            double BillingPrice = 0;

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            Data.XDSLog dxdslog = new XDSPortalEnquiry.Data.XDSLog();
            Entity.SubscriberLinkageInvestigationDetails sli = new Entity.SubscriberLinkageInvestigationDetails();

            try
            {

                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                SqlConnection objConstring = new SqlConnection(eSe.ExtraVarInput1.ToString());

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                if (spr.ReportID > 0)
                {
                   
                    eoLinkagesEnquiryWseManager.BonusSegments = null;
                    if (dsBonusDataSegment != null)
                    {
                        if (dsBonusDataSegment.Tables.Count > 0 && dsBonusDataSegment.Tables[0].Rows.Count > 0)
                        {
                            dsBonusDataSegment.DataSetName = "BonusSegments";
                            dsBonusDataSegment.Tables[0].TableName = "Segment";
                            eoLinkagesEnquiryWseManager.BonusSegments = dsBonusDataSegment;
                        }
                    }

                    // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML 
                    // use this XML data for generating the Report 


                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {
                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }
                        else if (sub.PayAsYouGo == 1)
                        {
                            //Calculate The Total cost of the Report , including Bonus Segments

                            Totalcost = spr.UnitPrice;

                            if (eoLinkagesEnquiryWseManager.BonusSegments != null)
                            {

                                foreach (DataRow dr in eoLinkagesEnquiryWseManager.BonusSegments.Tables[0].Rows)
                                {
                                    if (dr["BonusViewed"].ToString().ToLower() == "true")
                                    {
                                        Totalcost = Totalcost + Convert.ToInt16(dr["BonusPrice"].ToString());
                                    }
                                }
                            }
                        }
                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {
                            
                            sli.InvestigationID = int.Parse(eSe.TelephoneNo);

                            sli = dxdslog.GetSubscriberLinkageInvestigation(strlogcon, sli.InvestigationID);

                            

                            if (sli != null)
                            {
                                if (sli.InvestigationID != 0)
                                {
                                    if (sli.InvestigationStatus == Entity.SubscriberLinkageInvestigationDetails.StatusInd.C)
                                    {
                                        throw new Exception("This Investigation Completed already, please initiate a new one");
                                    }
                                }
                            }

                            if (eSC.KeyType == "B")
                            {
                                eoLinkagesEnquiryWseManager.CommercialID = eSC.KeyID;
                            }
                            else if (eSC.KeyType == "D")
                            {
                                eoLinkagesEnquiryWseManager.DirectorID = eSC.KeyID;
                            }
                            eoLinkagesEnquiryWseManager.ExternalReference = eSe.SubscriberReference;
                            eoLinkagesEnquiryWseManager.ProductID = eSe.ProductID;
                            eoLinkagesEnquiryWseManager.ReferenceNo = eSC.Reference;
                            eoLinkagesEnquiryWseManager.subscriberID = eSe.SubscriberID;
                            eoLinkagesEnquiryWseManager.TmpReference = eSC.ExtraVarOutput1;
                            // eoLinkagesEnquiryWseManager.ConnectionString = objConstring;
                            eoLinkagesEnquiryWseManager.DataSegments = eSe.ExtraVarInput2;



                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.GetLinkages(eoLinkagesEnquiryWseManager);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.GetLinkages(eoLinkagesEnquiryWseManager);
                            }

                            rXml = rp.ResponseData;



                            System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                            {
                                eSC.DetailsViewedYN = false;
                                eSC.Billable = false;
                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                                eSC.CreatedByUser = eSe.CreatedByUser;
                                eSC.CreatedOnDate = DateTime.Now;
                                dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                rp.EnquiryID = eSC.SubscriberEnquiryID;

                            }


                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {

                                eSC.EnquiryResult = "E";
                                eSe.ErrorDescription = rp.ResponseData.ToString();
                                dSe.UpdateSubscriberEnquiryError(con, eSe);


                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Bonus)
                            {
                                ds.ReadXml(xmlSR);
                                if (ds.Tables.Contains("Segments"))
                                {

                                    eSC.DetailsViewedYN = false;
                                    eSC.Billable = false;
                                    eSC.BonusIncluded = true;
                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;
                                    eSC.XMLBonus = rXml.Replace("'", "''");
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                    eSC.CreatedByUser = eSe.CreatedByUser;
                                    eSC.CreatedOnDate = DateTime.Now;
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.B.ToString();

                                    dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryID = intSubscriberEnquiryID;
                                    rp.EnquiryResultID = intSubscriberEnquiryResultID;


                                    foreach (DataRow r in ds.Tables["Segments"].Rows)
                                    {
                                        eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                        eSB.DataSegmentID = int.Parse(r["DataSegmentID"].ToString());
                                        eSB.DataSegmentName = r["DataSegmentName"].ToString().Replace("'", "''");
                                        eSB.DataSegmentDisplayText = r["DataSegmentDisplayText"].ToString().Replace("'", "''");
                                        eSB.BonusViewed = bool.Parse(r["BonusViewed"].ToString());
                                        eSB.Billable = false;
                                        eSB.CreatedByUser = eSe.CreatedByUser;
                                        eSB.CreatedOnDate = DateTime.Now;

                                        dSB.InsertSubscriberEnquiryResultBonus(con, eSB);
                                    }
                                }
                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {

                                ds.ReadXml(xmlSR);
                                ds.Tables[0].Columns.Add("EnquiryID");
                                ds.Tables[0].Columns.Add("EnquiryResultID");


                                if (ds.Tables.Contains("LinkageDetails"))
                                {
                                    Data.Product dProduct = new XDSPortalEnquiry.Data.Product();
                                    Entity.Product eProduct = dProduct.GetProductRecord(AdminConnection, eSe.ProductID);


                                    DataTable dtSubscriberInput = new DataTable("SubscriberInputDetails");
                                    dtSubscriberInput.Columns.Add("EnquiryDate", typeof(DateTime));
                                    dtSubscriberInput.Columns.Add("EnquiryType", typeof(String));
                                    dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                    dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                    dtSubscriberInput.Columns.Add("EnquiryInput", typeof(String));
                                    dtSubscriberInput.Columns.Add("EnquiryID", typeof(String));
                                    dtSubscriberInput.Columns.Add("EnquiryResultID", typeof(String));
                                    dtSubscriberInput.Columns.Add("EntityType", typeof(String));
                                    dtSubscriberInput.Columns.Add("EntityNo", typeof(String));
                                    dtSubscriberInput.Columns.Add("EntityName", typeof(String));
                                    dtSubscriberInput.Columns.Add("ProductID", typeof(String));
                                    DataRow drSubscriberInput;
                                    drSubscriberInput = dtSubscriberInput.NewRow();

                                    drSubscriberInput["EnquiryDate"] = DateTime.Now;
                                    drSubscriberInput["EnquiryType"] = eProduct.ProductDesc;
                                    drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                    drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                    drSubscriberInput["EnquiryInput"] = eSe.SearchInput.Trim();
                                    drSubscriberInput["EnquiryID"] = eSe.SubscriberEnquiryID;
                                    drSubscriberInput["EnquiryResultID"] = eSC.SubscriberEnquiryResultID;
                                    drSubscriberInput["EntityType"] = eSC.FirstName;

                                    //if (eSC.KeyType.Trim().ToUpper() == "D")
                                    //{
                                    drSubscriberInput["EntityNo"] = eSC.BusRegistrationNo;
                                    drSubscriberInput["EntityName"] = eSC.BusBusinessName;
                                    //}
                                    //else if (eSC.KeyType.Trim().ToUpper() == "B")
                                    //{
                                    //    drSubscriberInput["EntityNo"] = eSC.BusRegistrationNo;
                                    //    drSubscriberInput["EntityName"] = eSC.BusBusinessName;
                                    //}
                                    if (eSC.KeyType.Trim().ToUpper() == "D")
                                    {
                                        drSubscriberInput["ProductID"] = "14";
                                    }
                                    else if (eSC.KeyType.Trim().ToUpper() == "B")
                                    {
                                        drSubscriberInput["ProductID"] = "12";
                                    }

                                    dtSubscriberInput.Rows.Add(drSubscriberInput);

                                    ds.Tables.Add(dtSubscriberInput);


                                    Entity.SubscriberInputDetails oSid = new Entity.SubscriberInputDetails();

                                    oSid.InvestigationID = int.Parse(eSe.TelephoneNo);
                                    oSid.SubscriberID = eSe.SubscriberID;
                                    oSid.SubscriberName = eSe.SubscriberName;
                                    oSid.SystemuserID = eSe.SystemUserID;
                                    oSid.SubscriberReference = eSe.SubscriberReference;
                                    oSid.EnquiryDate = DateTime.Parse(drSubscriberInput["EnquiryDate"].ToString());
                                    oSid.EnquiryType = drSubscriberInput["EnquiryType"].ToString();
                                    oSid.EnquiryInput = drSubscriberInput["EnquiryType"].ToString();
                                    oSid.EnquiryID = eSC.SubscriberEnquiryID;
                                    oSid.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                    oSid.EntityType = drSubscriberInput["EntityType"].ToString();
                                    oSid.EntityNo = drSubscriberInput["EntityNo"].ToString();
                                    oSid.EntityName = drSubscriberInput["EntityName"].ToString();
                                    oSid.ProductID = int.Parse(drSubscriberInput["ProductID"].ToString());
                                    oSid.CreatedbyUser = sys.Username;
                                    oSid.SystemUsername = drSubscriberInput["SubscriberUserName"].ToString();

                                    dxdslog.InsertSubscriberInputDetails(strlogcon, oSid); 

                                    if (eSC.BonusIncluded == true)
                                    {
                                        Data.SubscriberEnquiryResultBonus oSubscriberEnquiryResultBonus = new XDSPortalEnquiry.Data.SubscriberEnquiryResultBonus();
                                        //DataSet dsBonus = oSubscriberEnquiryResultBonus.GetSelectedSubscriberEnquiryResultBonus(con, eSC.SubscriberEnquiryResultID);
                                        DataSet dsBonus = eoLinkagesEnquiryWseManager.BonusSegments;

                                        DataTable dtBonusSelected = new DataTable("ConsumerBonusSelected");

                                        dtBonusSelected.Columns.Add("DataSegmentID", typeof(String));
                                        dtBonusSelected.Columns.Add("DataSegmentName", typeof(String));
                                        dtBonusSelected.Columns.Add("DataSegmentDisplayText", typeof(String));

                                        DataRow drBonusSelected;
                                        if (dsBonus != null)
                                        {
                                            if (dsBonus.Tables[0].Rows.Count > 0)
                                            {
                                                foreach (DataRow r in dsBonus.Tables[0].Rows)
                                                {
                                                    drBonusSelected = dtBonusSelected.NewRow();
                                                    drBonusSelected["DataSegmentID"] = r["DataSegmentID"];
                                                    drBonusSelected["DataSegmentName"] = r["DataSegmentName"];
                                                    drBonusSelected["DataSegmentDisplayText"] = r["DataSegmentDisplayText"];
                                                    dtBonusSelected.Rows.Add(drBonusSelected);

                                                    eSB.SubscriberEnquiryResultBonusID = 0;
                                                    eSB.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                                    eSB.DataSegmentID = Convert.ToInt32(r["DataSegmentID"]);
                                                    eSB.DataSegmentName = r["DataSegmentName"].ToString();
                                                    eSB.DataSegmentDisplayText = r["DataSegmentDisplayText"].ToString();
                                                    eSB.BonusViewed = Convert.ToBoolean(r["BonusViewed"].ToString());
                                                    if (eSB.BonusViewed)
                                                    {
                                                        eSB.Billable = true;
                                                    }
                                                    else
                                                    {
                                                        eSB.Billable = false;
                                                    }
                                                    eSB.ChangedByUser = eSB.CreatedByUser;
                                                    eSB.ChangedOnDate = DateTime.Now;
                                                    dSB.UpdateSubscriberEnquiryResultBonus(con, eSB);
                                                }
                                                ds.Tables.Add(dtBonusSelected);
                                            }
                                        }
                                        dsBonus.Dispose();
                                    }



                                    #region INsert SubscriberEnquiry for the child request

                                    Entity.SubscriberEnquiry eSeC = new Entity.SubscriberEnquiry();


                                    eSeC.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                                    eSeC.SubscriberID = sys.SubscriberID;
                                    eSeC.ProductID = eSe.ProductID;
                                    eSeC.SubscriberEnquiryDate = DateTime.Now;
                                    eSeC.SubscriberName = sub.SubscriberName;
                                    eSeC.SubscriberReference = eSe.SubscriberReference;
                                    eSeC.SystemUserID = sys.SystemUserID;
                                    eSeC.BusRegistrationNo = eSC.BusRegistrationNo;
                                    eSeC.BusBusinessName = eSC.BusBusinessName;
                                    eSeC.IDNo = eSC.IDNo;
                                    eSeC.FirstName = eSC.FirstName;
                                    eSeC.Surname = eSC.Surname;
                                    eSeC.SystemUser = sys.SystemUserFullName;
                                    eSeC.CreatedByUser = sys.Username;
                                    eSeC.CreatedOnDate = DateTime.Now;
                                    eSeC.SearchInput = "";
                                    eSeC.TelephoneNo = eSe.TelephoneNo;


                                    //Generate serach input string

                                    if (!string.IsNullOrEmpty(eSeC.BusBusinessName))
                                    {
                                        eSeC.SearchInput = eSeC.SearchInput + " | " + eSeC.BusBusinessName;
                                    }
                                    if (!string.IsNullOrEmpty(eSeC.BusRegistrationNo))
                                    {
                                        eSeC.SearchInput = eSeC.SearchInput + " | " + eSeC.BusRegistrationNo;
                                    }
                                    if (!string.IsNullOrEmpty(eSeC.IDNo))
                                    {
                                        eSeC.SearchInput = eSeC.SearchInput + " | " + eSeC.IDNo;
                                    }


                                    if (eSeC.SearchInput.Length > 0)
                                    {
                                        eSeC.SearchInput = eSeC.SearchInput.Substring(3, eSeC.SearchInput.Length - 3).ToUpper();
                                    }

                                    // Log the Current Enquiry to SubscriberEnquiry Table
                                    int SubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSeC);

                                    #endregion




                                    sli.PrimarySubscriberEnquiryID = intSubscriberEnquiryID;
                                    sli.PrimarySubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                                    sli.EntityType = eSC.FirstName;
                                    sli.EntityStatus = string.IsNullOrEmpty(eSC.PassportNo) ? string.Empty : eSC.PassportNo;
                                    sli.InvestigationID = int.Parse(eSe.TelephoneNo);

                                    sli = dxdslog.GetEventInfo(strlogcon, sli);


                                    if ((sli.HeadSubscriberEnquiryID == 0 || sli.HeadSubscriberEnquiryresultID == 0) && sli.InvestigationID == 0)
                                    {
                                        sli.HeadSubscriberEnquiryID = sli.PrimarySubscriberEnquiryID;
                                        sli.HeadSubscriberEnquiryresultID = sli.PrimarySubscriberEnquiryResultID;
                                        sli.EventID = 1;
                                        sli.InvestigationID = int.Parse(eSe.TelephoneNo);
                                    }
                                    else
                                    {
                                        sli.EventID++;
                                    }
                                    sli.CreatedbyUser = sys.Username;
                                    sli.Billable = true;
                                    sli.BillingPrice = spr.UnitPrice;
                                    sli.DetailsViewedYN = true;
                                    sli.BillingTypeID = spr.BillingTypeID;

                                    Billable = true;
                                    BillingPrice = spr.UnitPrice;

                                    sli = dxdslog.InsertSubscriberLinkageInvestigationBillingDetails(strlogcon, sli);



                                    foreach (DataRow r in ds.Tables["LinkageDetails"].Rows)
                                    {


                                        #region insert subscriberEnquiryresult

                                        Entity.SubscriberEnquiryResult eSCC = new Entity.SubscriberEnquiryResult();
                                        string EntityType = string.Empty;



                                        eSCC.SearchOutput = "";
                                        eSCC.IDNo = "";
                                        eSCC.PassportNo = "";
                                        eSCC.Surname = "";
                                        eSCC.FirstName = "";
                                        eSCC.BirthDate = DateTime.Parse("1900/01/01");
                                        eSCC.Gender = "";

                                    
                                            if (!string.IsNullOrEmpty(sli.InvestigationReference))
                                            {
                                                eSCC.SearchOutput = eSCC.SearchOutput + " | " + sli.InvestigationReference;
                                            }
                                        
                                        if (r.Table.Columns.Contains("EntityNo"))
                                        {
                                            if (!string.IsNullOrEmpty(r["EntityNo"].ToString()))
                                            {
                                                eSCC.BusRegistrationNo = r["EntityNo"].ToString();
                                                eSCC.SearchOutput = eSCC.SearchOutput + " | " + eSC.BusRegistrationNo;
                                            }
                                        }
                                        if (r.Table.Columns.Contains("EntityCategory"))
                                        {
                                            if (!string.IsNullOrEmpty(r["EntityCategory"].ToString()))
                                            {
                                                EntityType = r["EntityCategory"].ToString().Replace("'", "''");
                                                eSCC.SecondName = r["EntityCategory"].ToString().Replace("'", "''");
                                                eSCC.SearchOutput = eSCC.SearchOutput + " | " + eSCC.SecondName;
                                            }
                                        }
                                        if (r.Table.Columns.Contains("EntityName"))
                                        {
                                            if (!string.IsNullOrEmpty(r["EntityName"].ToString()))
                                            {
                                                eSCC.BusBusinessName = r["EntityName"].ToString().Replace("'", "''");
                                                eSCC.SearchOutput = eSCC.SearchOutput + " | " + eSCC.BusBusinessName;
                                            }
                                        }

                                        if (r.Table.Columns.Contains("EntityStatus"))
                                        {
                                            if (!string.IsNullOrEmpty(r["EntityStatus"].ToString()))
                                            {
                                                eSCC.PassportNo = r["EntityStatus"].ToString().Replace("'", "''");
                                                eSCC.SearchOutput = eSCC.SearchOutput + " | " + eSCC.PassportNo;
                                            }
                                        }


                                        if (eSCC.SearchOutput.Length > 0)
                                        {
                                            eSCC.SearchOutput = eSCC.SearchOutput.Substring(3, eSCC.SearchOutput.Length - 3).ToUpper();
                                        }
                                        eSCC.DetailsViewedYN = false;
                                        eSCC.Billable = false;
                                        eSCC.CreatedByUser = eSe.CreatedByUser;
                                        eSCC.CreatedOnDate = DateTime.Now;
                                        eSCC.KeyID = int.Parse(r["EntityID"].ToString());

                                        if (r["EntityType"].ToString().ToLower().Trim() == "principal")
                                        {
                                            eSCC.KeyType = "D";
                                        }
                                        else if (r["EntityType"].ToString().ToLower().Trim() == "enterprise")
                                        {
                                            eSCC.KeyType = "B";
                                        }
                                        eSCC.FirstName = r["EntityType"].ToString();
                                        eSCC.BillingTypeID = spr.BillingTypeID;
                                        eSCC.BillingPrice = spr.UnitPrice;

                                        eSCC.SubscriberEnquiryID = SubscriberEnquiryID;
                                        eSCC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();
                                        eSCC.ExtraVarOutput1 = "";
                                        eSCC.ProductID = eSeC.ProductID;
                                        eSCC.VoucherCode = strVoucherCode;


                                        // Insert the match results to SubscriberEnquiryResult Table
                                        int SubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSCC);


                                        ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryID"] = SubscriberEnquiryID;
                                        ds.Tables[0].Rows[ds.Tables[0].Rows.IndexOf(r)]["EnquiryResultID"] = SubscriberEnquiryResultID;


                                        #endregion


                                        #region Generate investigation history

                                        sli.SecondarySubscriberEnquiryID = eSCC.SubscriberEnquiryID;
                                        sli.SecondarySubscriberEnquiryResultID = SubscriberEnquiryResultID;

                                        sli.KeyType = eSC.KeyType;
                                        sli.KeyID = eSC.KeyID;
                                        sli.LinkedID = eSCC.KeyID;
                                        sli.LinkedNo = eSCC.BusRegistrationNo;
                                        sli.LinkedName = eSCC.BusBusinessName;
                                        sli.LinkType = r["EntityType"].ToString().ToUpper();

                                        sli.LinkedEntityStatus = r["EntityStatus"].ToString().ToUpper();
                                        sli.EntityStatus = r["SearchedonEntityStatus"].ToString();
                                        sli.EntityType = string.IsNullOrEmpty(eSC.SecondName) ? string.Empty : eSC.SecondName;
                                        sli.LinkedEntityCategory = r["EntityCategory"].ToString();

                                        //if (sli.KeyType.Trim().ToUpper() == "B")
                                        //{
                                        //    sli.KeyNo = eSC.BusRegistrationNo;
                                        //    sli.KeyName = eSC.BusBusinessName;
                                        //}
                                        //else if (sli.KeyType.Trim().ToUpper() == "D")
                                        //{
                                        sli.KeyNo = eSC.BusRegistrationNo;
                                        sli.KeyName = eSC.BusBusinessName;

                                        // }



                                        dxdslog.InsertSubscriberLinkageInvestigationDetails(strlogcon, sli);

                                        #endregion Generate investigation history

                                    }

                                    rXml = ds.GetXml();
                                    rp.ResponseData = rXml;
                                    ExtractType = "N";

                                    Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                    Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                    eSC.DetailsViewedDate = DateTime.Now;
                                    eSC.DetailsViewedYN = true;
                                    if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                    {
                                        eSC.Billable = false;
                                    }
                                    else
                                    {
                                        eSC.Billable = true;
                                    }
                                    eSC.ChangedByUser = eSe.CreatedByUser;
                                    eSC.ChangedOnDate = DateTime.Now;
                                    

                                    eSC.KeyID = rp.ResponseKey;
                                    eSC.KeyType = rp.ResponseKeyType;
                                    eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                    eSC.BillingTypeID = spr.BillingTypeID;
                                    eSC.BillingPrice = spr.UnitPrice;

                                    eSC.XMLData = rXml.Replace("'", "''");
                                    eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                    eSC.ProductID = eSe.ProductID;

                                    dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                    rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                    rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                    if (sub.PayAsYouGo == 1)
                                    {
                                        dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                    }
                                    if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                    {
                                        dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                    }

                                    if (String.IsNullOrEmpty(eSC.VoucherCode))
                                    {
                                        //Log FootPrint

                                        eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                        eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                        eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                        eSubscriberFootPrint.KeyID = eSC.KeyID;
                                        eSubscriberFootPrint.KeyType = eSC.KeyType;
                                        eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                        eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                        if (string.IsNullOrEmpty(sub.CompanyTelephoneCode) && string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                        {
                                            eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                        }
                                        eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                        eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                        eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                        eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                        eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                        eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                        eSubscriberFootPrint.CreatedByUser = sys.Username;
                                        eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                        eSubscriberFootPrint.ProductID = eSe.ProductID;
                                        eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                        int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                    }




                                }
                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                            {
                                ds.ReadXml(xmlSR);
                                if (ds.Tables.Contains("DirectorDetails"))
                                {
                                    foreach (DataRow r in ds.Tables["DirectorDetails"].Rows)
                                    {

                                        eSC.SearchOutput = "";

                                        if (r.Table.Columns.Contains("IDNo"))
                                        {
                                            if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                            {
                                                eSC.IDNo = r["IDNo"].ToString();
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                            }
                                        }
                                        if (r.Table.Columns.Contains("PassportNo"))
                                        {
                                            if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                            {
                                                eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                            }
                                        }
                                        if (r.Table.Columns.Contains("Surname"))
                                        {
                                            if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                            {
                                                eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                            }
                                        }

                                        if (r.Table.Columns.Contains("FirstName"))
                                        {
                                            if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                            {
                                                eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                            }
                                        }

                                        if (eSC.SearchOutput.Length > 0)
                                        {
                                            eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                        }

                                        eSC.DetailsViewedYN = false;
                                        eSC.Billable = false;
                                        eSC.ChangedByUser = eSe.CreatedByUser;
                                        eSC.ChangedOnDate = DateTime.Now;
                                        eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                        eSC.KeyType = rp.ResponseKeyType;

                                        eSC.BillingTypeID = spr.BillingTypeID;
                                        eSC.BillingPrice = spr.UnitPrice;

                                        eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                        eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.M.ToString();

                                        dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;

                                    }
                                }
                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single)
                            {
                                ds.ReadXml(xmlSR);
                                if (ds.Tables.Contains("DirectorDetails"))
                                {
                                    foreach (DataRow r in ds.Tables["DirectorDetails"].Rows)
                                    {
                                        eSC.SearchOutput = "";

                                        if (r.Table.Columns.Contains("IDNo"))
                                        {
                                            if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                            {
                                                eSC.IDNo = r["IDNo"].ToString();
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.IDNo;
                                            }
                                        }
                                        if (r.Table.Columns.Contains("PassportNo"))
                                        {
                                            if (!string.IsNullOrEmpty(r["PassportNo"].ToString()))
                                            {
                                                eSC.PassportNo = r["PassportNo"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.PassportNo;
                                            }
                                        }
                                        if (r.Table.Columns.Contains("Surname"))
                                        {
                                            if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                            {
                                                eSC.Surname = r["Surname"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.Surname;
                                            }
                                        }

                                        if (r.Table.Columns.Contains("FirstName"))
                                        {
                                            if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                            {
                                                eSC.FirstName = r["FirstName"].ToString().Replace("'", "''");
                                                eSC.SearchOutput = eSC.SearchOutput + " | " + eSC.FirstName;
                                            }
                                        }

                                        if (eSC.SearchOutput.Length > 0)
                                        {
                                            eSC.SearchOutput = eSC.SearchOutput.Substring(3, eSC.SearchOutput.Length - 3).ToUpper();
                                        }


                                        eSC.DetailsViewedYN = false;
                                        eSC.Billable = false;
                                        eSC.CreatedByUser = eSe.CreatedByUser;
                                        eSC.CreatedOnDate = DateTime.Now;
                                        eSC.KeyID = int.Parse(r["ConsumerID"].ToString());
                                        eSC.KeyType = rp.ResponseKeyType;

                                        eSC.BillingTypeID = spr.BillingTypeID;
                                        eSC.BillingPrice = spr.UnitPrice;

                                        eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                        eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.S.ToString();

                                        dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                                    }
                                }
                            }

                        }
                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.
                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                        }
                    }
                    else
                    {
                        // When User want to Re - Open a report 
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = eSC.XMLData;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                        ExtractType = "A";
                    }

                    //DataTable dtBilling = new DataTable("SubscriberBillingInfo");
                    //dtBilling.Columns.Add("PricePerClick", typeof(String));
                    //dtBilling.Columns.Add("TotalPrice", typeof(String));

                    //DataRow drSubscriberBilling;
                    //drSubscriberBilling = dtBilling.NewRow();

                    //drSubscriberBilling["PricePerClick"] = sli.BillingPrice;
                    //drSubscriberBilling["TotalPrice"] = sli.TotalPrice;

                    //dtBilling.Rows.Add(drSubscriberBilling);

                    //ds.Tables.Add(dtBilling);

                    sli.PrimarySubscriberEnquiryID = intSubscriberEnquiryID;
                    sli.PrimarySubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                    DataSet dsHistory = dxdslog.GetInvestigationHistory(strlogcon, sli);


                    ds = new DataSet();


                    if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                    {
                        System.IO.StringReader SR = new System.IO.StringReader(eSC.XMLData);
                        ds.ReadXml(SR);
                    }



                    if (dsHistory.Tables.Count > 0)
                    {
                        ds.Tables.Add(dsHistory.Tables[0].Copy());
                    }

                    if (dsHistory.Tables.Contains("SubscriberBillingInfo"))
                    {
                        ds.Tables.Add(dsHistory.Tables["SubscriberBillingInfo"].Copy());
                    }

                    if (dsHistory.Tables.Contains("LinkageDetails") && ds.Tables.Contains("LinkageDetails"))
                    {
                        foreach (DataRow drH in dsHistory.Tables["LinkageDetails"].Rows)
                        {

                            //  File.AppendAllText(@"C:\Log\LinkagesComm.txt", "drH\n" + drH["EnquiryResultID"].ToString());
                            foreach (DataRow dr in ds.Tables["LinkageDetails"].Rows)
                            {
                                //File.AppendAllText(@"C:\Log\LinkagesComm.txt", "dr\n" + dr["EnquiryResultID"].ToString());
                                //File.AppendAllText(@"C:\Log\LinkagesComm.txt", "drH\n" + drH["EnquiryResultID"].ToString());
                                //File.AppendAllText(@"C:\Log\LinkagesComm.txt", "dr\n" + dr["EnquiryID"].ToString());
                                //File.AppendAllText(@"C:\Log\LinkagesComm.txt", "drH\n" + drH["EnquiryID"].ToString());
                                //File.AppendAllText(@"C:\Log\LinkagesComm.txt", "drH\n" + drH["DetailsViewed"].ToString());
                                if (drH["EnquiryID"].ToString() == dr["EnquiryID"].ToString() && drH["EnquiryResultID"].ToString() == dr["EnquiryResultID"].ToString() && bool.Parse(drH["DetailsViewed"].ToString()))
                                {
                                    // File.AppendAllText(@"C:\Log\LinkagesComm.txt", "Enter\n");
                                    ds.Tables["LinkageDetails"].Rows[ds.Tables["LinkageDetails"].Rows.IndexOf(dr)]["DetailsViewed"] = bool.Parse(drH["DetailsViewed"].ToString()) ? "1" : "0";
                                    break;
                                }
                            }
                        }
                    }

                    rp.ResponseData = ds.GetXml();

                    try
                    {
                        Entity.LinkageEvents ole = new Entity.LinkageEvents();

                        ole.InvestigationID = int.Parse(eSe.TelephoneNo.Trim());
                        ole.ProductID = eSe.ProductID;
                        ole.SubscriberEnquiryID = intSubscriberEnquiryID;
                        ole.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;

                        ole.Response = rp.ResponseData;
                        ole.Createdondate = DateTime.Now;
                        ole.CreatedbyUser = string.Empty;
                        ole.ExtractType = ExtractType;
                        ole.Billable = Billable;
                        ole.BillingPrice = BillingPrice;

                        dxdslog.InsertSubscriberLinkageEvents(strlogcon, ole);
                    }
                    catch (Exception ex)
                    {
                        File.AppendAllText(@"C:\Log\CommercialLinkages.txt", "GetCommercialReport:" + ex.Message + "\n");
                    }


                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;


                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response GetCommercialReport(SqlConnection con, SqlConnection AdminConnection, SqlConnection logConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, int intProductID, DataSet dsBonusDataSegment, bool bBonusChecking, string strVoucherCode)
        {
            string strAdminCon = AdminConnection.ConnectionString;
            string strlogcon = logConnection.ConnectionString;
            bool Billable = false;
            double BillingPrice = 0;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();


            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            Data.XDSLog dxdslog = new XDSPortalEnquiry.Data.XDSLog();


            Data.Reports dRe = new Data.Reports();
            Entity.Reports eRe = new Entity.Reports();

            Entity.SubscriberEnquiry eSeC = new Entity.SubscriberEnquiry();
            Entity.SubscriberEnquiryResult eSCC = new Entity.SubscriberEnquiryResult();

            Entity.LinkageEvents ole = new Entity.LinkageEvents();

            Entity.SubscriberLinkageInvestigationDetails sli = new Entity.SubscriberLinkageInvestigationDetails();
            

            try
            {

                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                ole.InvestigationID = int.Parse(eSe.TelephoneNo.Trim());
                ole.ProductID = intProductID;
                ole.SubscriberEnquiryID = intSubscriberEnquiryID;
                ole.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;

                ole = dxdslog.GetReportEnquiryInfo(strlogcon, ole);

                SqlConnection objConstring = new SqlConnection(eSe.ExtraVarInput1.ToString());

                xdsBilling xb = new xdsBilling();
                //Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, intProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                //  Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                // Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);


                //File.AppendAllText(@"C:\Log\CommercialLinkages.txt", "GetCommercialReport:Step1\n");

                if (!(ole.ResultSubscriberEnquiryID > 0 && ole.ResultSubscriberEnquiryResultID > 0))
                {

                    sli.InvestigationID = int.Parse(eSe.TelephoneNo);

                    sli = dxdslog.GetSubscriberLinkageInvestigation(strlogcon, sli.InvestigationID);



                    if (sli != null)
                    {
                        if (sli.InvestigationID != 0)
                        {
                            if (sli.InvestigationStatus == Entity.SubscriberLinkageInvestigationDetails.StatusInd.C)
                            {
                                throw new Exception("This Investigation Completed already, please initiate a new one");
                            }
                        }
                    }
                    //File.AppendAllText(@"C:\Log\CommercialLinkages.txt", "GetCommercialReport:Step2\n");

                    if (eSe.ProductID == 12 || eSe.ProductID == 40 || eSe.ProductID == 41 || eSe.ProductID == 69 || eSe.ProductID == 90)
                    {
                        eSe.ProductID = 12;
                    }
                    else
                    {
                        eSe.ProductID = intProductID;
                    }
                    eSe.EnquiryStatus = "P";
                    eSe.SubscriberEnquiryDate = DateTime.Now;
                    eSe.CreatedOnDate = DateTime.Now;
                    eSe.MaidenName = sli.InvestigationReference;
                    int cSubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);

                    // File.AppendAllText(@"C:\Log\CommercialLinkages.txt", "GetCommercialReport:" + cSubscriberEnquiryID .ToString()+ "\n");

                    // eSeC = dSe.GetSubscriberEnquiryObject(con, cSubscriberEnquiryID);

                    eSC.SubscriberEnquiryID = cSubscriberEnquiryID;
                    eSC.ProductID = intProductID;
                    eSC.EnquiryResult = "M";
                    eSC.XMLData = string.Empty;
                    eSC.DetailsViewedYN = false;
                    eSC.Billable = false;
                    eSC.CreatedOnDate = DateTime.Now;

                    int cSubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);

                    ole.ResultSubscriberEnquiryID = cSubscriberEnquiryID;
                    ole.ResultSubscriberEnquiryResultID = cSubscriberEnquiryResultID;

                    Billable = true;

                    // File.AppendAllText(@"C:\Log\CommercialLinkages.txt", "GetCommercialReport:" + cSubscriberEnquiryResultID .ToString()+ "\n");

                    // eSCC = dSC.GetSubscriberEnquiryResultObject(con, cSubscriberEnquiryResult);

                    //if (intProductID == 14)
                    //{
                    //    Business.MultipleCommercial_DirectorEnquiry dtMultipleDirectorTrace = new Business.MultipleCommercial_DirectorEnquiry();
                    //    rp = dtMultipleDirectorTrace.SubmitMulipleDirectorEnquiry(con, AdminConnection, cSubscriberEnquiryID, cSubscriberEnquiryResultID, dsBonusDataSegment, true, eSC.VoucherCode);

                    //}
                    //else if (intProductID == 12 || intProductID == 40 || intProductID == 41 || intProductID == 69 || intProductID == 90)
                    //{
                    //   // File.AppendAllText(@"C:\Log\CommercialLinkages.txt", "GetCommercialReport:Report\n");
                    //    Business.MultipleCommercial_BusinessEnquiry dtMultiplebusinessTrace = new Business.MultipleCommercial_BusinessEnquiry();
                    //    rp = dtMultiplebusinessTrace.SubmitMulipleBusinessEnquiry(con, AdminConnection, cSubscriberEnquiryID, cSubscriberEnquiryResultID,intProductID, dsBonusDataSegment, true, eSC.VoucherCode);

                    //}
                }
                //else
                //{
                if (intProductID == 14)
                {
                    Business.MultipleCommercial_DirectorEnquiry dtMultipleDirectorTrace = new Business.MultipleCommercial_DirectorEnquiry();
                    rp = dtMultipleDirectorTrace.SubmitMulipleDirectorEnquiry(con, AdminConnection, ole.ResultSubscriberEnquiryID, ole.ResultSubscriberEnquiryResultID, dsBonusDataSegment, true, eSC.VoucherCode);
                    ole.Billable = rp.Billable;
                    ole.BillingPrice = rp.BillingPrice;
                }
                else if (intProductID == 12 || intProductID == 40 || intProductID == 41 || intProductID == 69 || intProductID == 90)
                {
                    Business.MultipleCommercial_BusinessEnquiry dtMultiplebusinessTrace = new Business.MultipleCommercial_BusinessEnquiry();
                    rp = dtMultiplebusinessTrace.SubmitMulipleBusinessEnquiryWithStatus(con, AdminConnection, ole.ResultSubscriberEnquiryID, ole.ResultSubscriberEnquiryResultID, intProductID, dsBonusDataSegment, true, eSC.VoucherCode);
                    ole.Billable = rp.Billable;
                    ole.BillingPrice = rp.BillingPrice;
                }
                //  }

                System.IO.StringReader xmlSR = new System.IO.StringReader(rp.ResponseData);
                ds.ReadXml(xmlSR);

              //  Entity.SubscriberLinkageInvestigationDetails sli = new Entity.SubscriberLinkageInvestigationDetails();

                sli.PrimarySubscriberEnquiryID = ole.ResultSubscriberEnquiryID;
                sli.PrimarySubscriberEnquiryResultID = ole.ResultSubscriberEnquiryResultID;
                sli.CreatedbyUser = eSe.CreatedByUser;
                sli.EventID = 0;
                sli.InvestigationID = ole.InvestigationID;
                sli.Billable = ole.Billable;
                sli.BillingPrice = ole.BillingPrice;
                sli.DetailsViewedYN = ole.Billable;
                sli.TotalPrice = Math.Round((ole.TotalPrice + (ole.Billable ? ole.BillingPrice : 0)));
                sli = dxdslog.InsertSubscriberLinkageInvestigationBillingDetails(strlogcon, sli);



                DataTable dtBilling = new DataTable("SubscriberBillingInfo");
                dtBilling.Columns.Add("PricePerClick", typeof(String));
                dtBilling.Columns.Add("TotalPrice", typeof(String));

                DataRow drSubscriberBilling;
                drSubscriberBilling = dtBilling.NewRow();

                drSubscriberBilling["PricePerClick"] = string.Format("{0:N2}", sli.BillingPrice);
                drSubscriberBilling["TotalPrice"] = string.Format("{0:N2}", sli.TotalPrice) ;

                dtBilling.Rows.Add(drSubscriberBilling);

                if (ds.Tables.Contains(dtBilling.TableName))
                {
                    ds.Tables.Remove(dtBilling.TableName);
                }

                ds.Tables.Add(dtBilling);


                rp.ResponseData = ds.GetXml();
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, ole.ResultSubscriberEnquiryResultID);
                eSC.XMLData = rp.ResponseData;
                //File.AppendAllText(@"C:\Log\CommercialLinkages.txt", "Report:" + eSC.XMLData + "\n");
                dSC.UpdateSubscriberEnquiryResult(con, eSC);

                try
                {


                    ole.Response = rp.ResponseData;
                    ole.Createdondate = DateTime.Now;
                    ole.CreatedbyUser = string.Empty;
                    ole.ExtractType = string.Empty;


                    dxdslog.InsertSubscriberLinkageEvents(strlogcon, ole);
                }
                catch (Exception ex)
                {
                    File.AppendAllText(@"C:\Log\CommercialLinkages.txt", "GetCommercialReport:" + ex.Message + "\n");
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;


                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response GetLinkagedetectorReport(SqlConnection con, SqlConnection AdminConnection, SqlConnection logConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, int intProductID, DataSet dsBonusDataSegment, bool bBonusChecking, string strVoucherCode)
        {
            string strAdminCon = AdminConnection.ConnectionString;
            string strlogcon = logConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();


            string rXml = "";
            double Totalcost = 0;
            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            Data.XDSLog dxdslog = new XDSPortalEnquiry.Data.XDSLog();

            Entity.SubscriberLinkageInvestigationDetails sli = new Entity.SubscriberLinkageInvestigationDetails();
            try
            {

                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                SqlConnection objConstring = new SqlConnection(eSe.ExtraVarInput1.ToString());

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                if (spr.ReportID > 0)
                {
                    eoLinkagesEnquiryWseManager.BonusSegments = null;
                    if (dsBonusDataSegment != null)
                    {
                        if (dsBonusDataSegment.Tables.Count > 0 && dsBonusDataSegment.Tables[0].Rows.Count > 0)
                        {
                            dsBonusDataSegment.DataSetName = "BonusSegments";
                            dsBonusDataSegment.Tables[0].TableName = "Segment";
                            eoLinkagesEnquiryWseManager.BonusSegments = dsBonusDataSegment;
                        }
                    }

                    // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML 
                    // use this XML data for generating the Report 


                    if (!eSC.DetailsViewedYN && eSC.EnquiryResult.ToUpper() != "N")
                    {
                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }
                        else if (sub.PayAsYouGo == 1)
                        {
                            //Calculate The Total cost of the Report , including Bonus Segments

                            Totalcost = spr.UnitPrice;

                            if (eoLinkagesEnquiryWseManager.BonusSegments != null)
                            {

                                foreach (DataRow dr in eoLinkagesEnquiryWseManager.BonusSegments.Tables[0].Rows)
                                {
                                    if (dr["BonusViewed"].ToString().ToLower() == "true")
                                    {
                                        Totalcost = Totalcost + Convert.ToInt16(dr["BonusPrice"].ToString());
                                    }
                                }
                            }
                        }
                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {
                           
                            if (string.IsNullOrEmpty(eSC.XMLData))
                            {
                                throw new Exception("Enquiry is not in a valid state, unable to find the report");
                            }
                            else
                            {

                                sli.InvestigationID = int.Parse(eSe.TelephoneNo);

                                sli = dxdslog.GetSubscriberLinkageInvestigation(strlogcon, sli.InvestigationID);


                                if (sli != null)
                                {
                                    if (sli.InvestigationID != 0)
                                    {
                                        if (sli.InvestigationStatus == Entity.SubscriberLinkageInvestigationDetails.StatusInd.C)
                                        {
                                            throw new Exception("This Investigation Completed already, please initiate a new one");
                                        }
                                    }
                                }

                                Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                eSC.DetailsViewedDate = DateTime.Now;
                                eSC.DetailsViewedYN = true;
                                if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    eSC.Billable = false;
                                }
                                else
                                {
                                    eSC.Billable = true;
                                }
                                eSC.ChangedByUser = eSe.CreatedByUser;
                                eSC.ChangedOnDate = DateTime.Now;
                              //  eSC.SearchOutput = "";

                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                eSC.BillingTypeID = spr.BillingTypeID;
                                eSC.BillingPrice = spr.UnitPrice;


                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                eSC.ProductID = eSe.ProductID;
                                eSC.SearchOutput = eSe.SecondName;

                                //  File.AppendAllText(@"C:\Log\ComLinkages.txt", "StepX");

                                dSC.UpdateSubscriberEnquiryResult(con, eSC);

                                rp.Billable = eSC.Billable;
                                rp.BillingPrice = eSC.BillingPrice;

                                // File.AppendAllText(@"C:\Log\ComLinkages.txt", "StepY");

                                rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                rp.EnquiryID = eSC.SubscriberEnquiryID;

                                //  File.AppendAllText(@"C:\Log\ComLinkages.txt", "StepZ");

                                // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                if (sub.PayAsYouGo == 1)
                                {
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                }
                                if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                }

                                if (String.IsNullOrEmpty(eSC.VoucherCode))
                                {
                                    //Log FootPrint

                                    //   File.AppendAllText(@"C:\Log\ComLinkages.txt", "Step1");

                                    eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                    eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                    eSubscriberFootPrint.KeyID = eSC.KeyID;
                                    eSubscriberFootPrint.KeyType = eSC.KeyType;
                                    eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                    eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                    if (string.IsNullOrEmpty(sub.CompanyTelephoneCode) && string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                    {
                                        eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                    }
                                    eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                    eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                    eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                    eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                    eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                    eSubscriberFootPrint.CreatedByUser = sys.Username;
                                    eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                    eSubscriberFootPrint.ProductID = eSe.ProductID;
                                    eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                    int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);

                                    // File.AppendAllText(@"C:\Log\ComLinkages.txt", "Step2");

                                }

                                rp.ResponseData = eSC.XMLData;
                                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;

                            }

                        }
                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.
                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                        }
                    }
                    else
                    {
                        if (eSC.EnquiryResult.ToUpper() == "N")
                        {
                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                            rp.EnquiryID = eSC.SubscriberEnquiryID;
                            rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                            rp.Billable = false;
                            rp.BillingPrice = 0;
                        }
                        else if (eSC.EnquiryResult.ToUpper() == "M" || eSC.EnquiryResult.ToUpper() == "R")
                        {


                            // When User want to Re - Open a report 
                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                            rp.ResponseData = eSC.XMLData;
                            rp.EnquiryID = eSC.SubscriberEnquiryID;
                            rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                            rp.Billable = false;
                            rp.BillingPrice = 0;
                        }
                    }

                    if (eSC.EnquiryResult.ToUpper() != "N")
                    {
                        System.IO.StringReader xmlSR = new System.IO.StringReader(rp.ResponseData);
                        ds.ReadXml(xmlSR);



                        sli.PrimarySubscriberEnquiryID = rp.EnquiryID;
                        sli.PrimarySubscriberEnquiryResultID = rp.EnquiryResultID;
                        sli.CreatedbyUser = eSe.CreatedByUser;
                        sli.EventID = 0;
                        sli.InvestigationID = int.Parse(eSe.TelephoneNo);
                        sli.Billable = rp.Billable;
                        sli.BillingPrice = rp.BillingPrice;
                        sli.DetailsViewedYN = true;

                        sli = dxdslog.InsertSubscriberLinkageInvestigationBillingDetails(strlogcon, sli);

                        DataTable dtBilling = new DataTable("SubscriberBillingInfo");
                        dtBilling.Columns.Add("PricePerClick", typeof(String));
                        dtBilling.Columns.Add("TotalPrice", typeof(String));

                        DataRow drSubscriberBilling;
                        drSubscriberBilling = dtBilling.NewRow();

                        drSubscriberBilling["PricePerClick"] = string.Format("{0:N2}", sli.BillingPrice);
                        drSubscriberBilling["TotalPrice"] = string.Format("{0:N2}", sli.TotalPrice);

                        dtBilling.Rows.Add(drSubscriberBilling);

                        ds.Tables.Add(dtBilling);


                        rp.ResponseData = ds.GetXml();

                        try
                        {
                            Entity.LinkageEvents ole = new Entity.LinkageEvents();

                            ole.InvestigationID = int.Parse(eSe.TelephoneNo.Trim());
                            ole.ProductID = intProductID;
                            ole.SubscriberEnquiryID = intSubscriberEnquiryID;
                            ole.SubscriberEnquiryResultID = intSubscriberEnquiryResultID;
                            ole.Response = rp.ResponseData;
                            ole.Createdondate = DateTime.Now;
                            ole.CreatedbyUser = string.Empty;
                            ole.ExtractType = string.Empty;


                            dxdslog.InsertSubscriberLinkageEvents(strlogcon, ole);
                        }
                        catch (Exception ex)
                        {
                            File.AppendAllText(@"C:\Log\CommercialLinkages.txt", "GetCommercialReport:" + ex.Message + "\n");
                        }
                    }

                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;


                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response GetCommercialInvestigationHistoryReport(SqlConnection con, SqlConnection AdminConnection, SqlConnection logConnection, int intSystemuserID, int intInvestigationID, int intProductID, DataSet dsBonusDataSegment, bool bBonusChecking, string strVoucherCode)
        {

            //  File.AppendAllText(@"C:\Log\History.txt", "Here" + "\n");

            string strAdminCon = AdminConnection.ConnectionString;
            string strlogcon = logConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();


            string rXml = "";
            double Totalcost = 0;
            bool boolsubmitReport = false;
            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            Data.XDSLog dxdslog = new XDSPortalEnquiry.Data.XDSLog();
            Entity.LinkageEvents ole = new Entity.LinkageEvents();

            Entity.SubscriberLinkageInvestigationDetails sli = new Entity.SubscriberLinkageInvestigationDetails();

            try
            {
                //  File.AppendAllText(@"C:\Log\History.txt", "Enter123" + "\n");

                sli = dxdslog.GetSubscriberLinkageInvestigation(strlogcon, intInvestigationID);

                ole.InvestigationID = intInvestigationID;
                ole.ProductID = intProductID;
                ole.SubscriberEnquiryID = 0;
                ole.SubscriberEnquiryResultID = 0;

                //    File.AppendAllText(@"C:\Log\History.txt", "Enter"+"\n");

                ole = dxdslog.GetReportEnquiryInfo(strlogcon, ole);

                //File.AppendAllText(@"C:\Log\History.txt", "x\n");
                //File.AppendAllText(@"C:\Log\History.txt", ole.HistoryChanged.ToString() + "\n");
                //File.AppendAllText(@"C:\Log\History.txt", ole.ResultSubscriberEnquiryID.ToString() + "\n");
                //File.AppendAllText(@"C:\Log\History.txt", ole.ResultSubscriberEnquiryResultID.ToString() + "\n");

                if (ole.ResultSubscriberEnquiryID > 0 && ole.ResultSubscriberEnquiryResultID > 0 && !ole.HistoryChanged)
                {
                    File.AppendAllText(@"C:\Log\History.txt", "Enter3" + "\n");

                    eSe = dSe.GetSubscriberEnquiryObject(con, ole.ResultSubscriberEnquiryID);
                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, ole.ResultSubscriberEnquiryResultID);

                    if (!string.IsNullOrEmpty(eSC.XMLData))
                    {
                        boolsubmitReport = false;
                    }
                    else
                    {
                        boolsubmitReport = true;
                    }

                }
                else
                {
                    boolsubmitReport = true;
                }


                // File.AppendAllText(@"C:\Log\History.txt", sli.SubscriberID.ToString() + "\n"+ intProductID.ToString());

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, sli.SubscriberID, intProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, sli.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemuserID);

                if (spr.ReportID > 0)
                {
                    eoLinkagesEnquiryWseManager.BonusSegments = null;
                    if (dsBonusDataSegment != null)
                    {
                        if (dsBonusDataSegment.Tables.Count > 0 && dsBonusDataSegment.Tables[0].Rows.Count > 0)
                        {
                            dsBonusDataSegment.DataSetName = "BonusSegments";
                            dsBonusDataSegment.Tables[0].TableName = "Segment";
                            eoLinkagesEnquiryWseManager.BonusSegments = dsBonusDataSegment;
                        }
                    }

                    // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML 
                    // use this XML data for generating the Report 


                    if (boolsubmitReport)
                    {
                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }
                        else if (sub.PayAsYouGo == 1)
                        {
                            //Calculate The Total cost of the Report , including Bonus Segments

                            Totalcost = spr.UnitPrice;

                            if (eoLinkagesEnquiryWseManager.BonusSegments != null)
                            {

                                foreach (DataRow dr in eoLinkagesEnquiryWseManager.BonusSegments.Tables[0].Rows)
                                {
                                    if (dr["BonusViewed"].ToString().ToLower() == "true")
                                    {
                                        Totalcost = Totalcost + Convert.ToInt16(dr["BonusPrice"].ToString());
                                    }
                                }
                            }
                        }
                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {
                           // sli.InvestigationID = int.Parse(eSe.TelephoneNo);

                            sli = dxdslog.GetSubscriberLinkageInvestigation(strlogcon, sli.InvestigationID);



                            if (sli != null)
                            {
                                if (sli.InvestigationID != 0)
                                {
                                    if (sli.InvestigationStatus == Entity.SubscriberLinkageInvestigationDetails.StatusInd.C)
                                    {
                                        throw new Exception("This Investigation Completed already, please initiate a new one");
                                    }
                                }
                            }

                            eSe.EnquiryStatus = Entity.SubscriberEnquiry.EnquiryStatusInd.P.ToString();
                            eSe.SubscriberID = sys.SubscriberID;
                            eSe.ProductID = intProductID;
                            eSe.SubscriberEnquiryDate = DateTime.Now;
                            eSe.SubscriberName = sub.SubscriberName;
                            //eSe.SubscriberReference = ;
                            eSe.SystemUserID = sys.SystemUserID;
                            eSe.BusBusinessName = sli.InvestigationReference;
                            eSe.SystemUser = sys.SystemUserFullName;
                            eSe.CreatedByUser = sys.Username;
                            eSe.CreatedOnDate = DateTime.Now;
                            eSe.SearchInput = sli.InvestigationReference;
                            eSe.TelephoneNo = intInvestigationID.ToString();

                            //Generate serach input string


                            // Log the Current Enquiry to SubscriberEnquiry Table
                            int SubscriberEnquiryID = dSe.InsertSubscriberEnquiry(con, eSe);

                            eoLinkagesEnquiryWseManager.CommercialID = intInvestigationID;

                            // File.AppendAllText(@"C:\Log\History.txt", "abcd");

                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, strlogcon);
                            rp = ra.GetCommercialLinkageHistoryReport(eoLinkagesEnquiryWseManager);

                            // File.AppendAllText(@"C:\Log\History.txt", "abcd2");
                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.GetCommercialLinkageHistoryReport(eoLinkagesEnquiryWseManager);
                            }
                            // File.AppendAllText(@"C:\Log\History.txt", "abcd3");
                            rXml = rp.ResponseData;

                            // File.AppendAllText(@"C:\Log\History.txt", rXml);

                            int SubscriberEnquiryResultID = 0;

                            System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                            {
                                eSC.DetailsViewedYN = false;
                                eSC.Billable = false;
                                eSC.SubscriberEnquiryID = SubscriberEnquiryID;
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                                eSC.CreatedByUser = eSe.CreatedByUser;
                                eSC.CreatedOnDate = DateTime.Now;
                                eSC.KeyID = 0;
                                eSC.KeyType = string.Empty;

                                SubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);

                                rp.EnquiryResultID = SubscriberEnquiryResultID;
                                rp.EnquiryID = eSC.SubscriberEnquiryID;

                            }


                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {

                                eSC.EnquiryResult = "E";
                                eSe.ErrorDescription = rp.ResponseData.ToString();
                                dSe.UpdateSubscriberEnquiryError(con, eSe);


                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {

                                //  File.AppendAllText(@"C:\Log\History.txt", "xxxx");
                                ds.ReadXml(xmlSR);
                                // File.AppendAllText(@"C:\Log\History.txt", "yyyy");
                                ds.Tables[0].Columns.Add("EnquiryID");
                                ds.Tables[0].Columns.Add("EnquiryResultID");


                                Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();


                                DataTable dtSubscriberInput = new DataTable("InvestigationDetails");
                                dtSubscriberInput.Columns.Add("InvestigationStartDate", typeof(DateTime));
                                dtSubscriberInput.Columns.Add("ReportDate", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberName", typeof(String));
                                dtSubscriberInput.Columns.Add("SubscriberUserName", typeof(String));
                                dtSubscriberInput.Columns.Add("InvestigationName", typeof(String));

                                DataRow drSubscriberInput;
                                drSubscriberInput = dtSubscriberInput.NewRow();

                                drSubscriberInput["ReportDate"] = DateTime.Now;
                                drSubscriberInput["InvestigationStartDate"] = sli.Createdondate;
                                drSubscriberInput["SubscriberName"] = sub.SubscriberName;
                                drSubscriberInput["SubscriberUserName"] = sys.SystemUserFullName;
                                drSubscriberInput["InvestigationName"] = sli.InvestigationReference;


                                dtSubscriberInput.Rows.Add(drSubscriberInput);

                                ds.Tables.Add(dtSubscriberInput);
                                rXml = ds.GetXml();
                                rp.ResponseData = rXml;


                                eSC.SearchOutput = sli.InvestigationReference+"|"+DateTime.Now.ToString("yyyyMMddhhmmss");
                                eSC.BusBusinessName = "";
                                eSC.BusRegistrationNo = "";
                                eSC.IDNo = "";
                                eSC.PassportNo = "";
                                eSC.Surname = "";
                                eSC.FirstName = "";
                                eSC.BirthDate = DateTime.Parse("1900/01/01");
                                eSC.Gender = "";
                                eSC.CreatedByUser = eSe.CreatedByUser;
                                eSC.CreatedOnDate = DateTime.Now;
                              



                                eSC.KeyID = rp.ResponseKey;
                                eSC.KeyType = rp.ResponseKeyType;

                                //eSC.BillingTypeID = spr.BillingTypeID;
                                //eSC.BillingPrice = spr.UnitPrice;

                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                eSC.SubscriberEnquiryID = SubscriberEnquiryID;
                                eSC.DetailsViewedDate = DateTime.Now;
                                eSC.DetailsViewedYN = true;
                                if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    eSC.Billable = false;
                                }
                                else
                                {
                                    eSC.Billable = true;
                                }
                                eSC.BillingTypeID = spr.BillingTypeID;
                                eSC.BillingPrice = spr.UnitPrice;
                                eSC.BillingTypeID = spr.BillingTypeID;

                                eSC.ProductID = intProductID;
                                eSC.VoucherCode = strVoucherCode;



                                // Insert the match results to SubscriberEnquiryResult Table
                                SubscriberEnquiryResultID = dSC.InsertSubscriberEnquiryResult(con, eSC);

                                ole.Billable = eSC.Billable;
                                ole.BillingPrice = eSC.BillingPrice;

                                ds.Tables["ReportInformation"].Rows[0]["EnquiryID"] = SubscriberEnquiryID;
                                ds.Tables["ReportInformation"].Rows[0]["EnquiryResultID"] = SubscriberEnquiryResultID;

                                rp.ResponseData = ds.GetXml();
                                eSC.XMLData = rp.ResponseData;
                                eSC.SubscriberEnquiryResultID = SubscriberEnquiryResultID;

                                dSC.UpdateSubscriberEnquiryResult(con, eSC);

                                rp.EnquiryID = eSC.SubscriberEnquiryID;

                                // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                if (sub.PayAsYouGo == 1)
                                {
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                }
                                if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                }

                                if (String.IsNullOrEmpty(eSC.VoucherCode))
                                {
                                    //Log FootPrint

                                    eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                    eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                    eSubscriberFootPrint.KeyID = eSC.KeyID;
                                    eSubscriberFootPrint.KeyType = eSC.KeyType;
                                    eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                    eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                    if (string.IsNullOrEmpty(sub.CompanyTelephoneCode) && string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                    {
                                        eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                    }
                                    eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                    eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                    eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                    eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                    eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                    eSubscriberFootPrint.CreatedByUser = sys.Username;
                                    eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                    eSubscriberFootPrint.ProductID = eSe.ProductID;
                                    eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                    int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                }





                            }
                        }
                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.
                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                        }
                    }
                    else
                    {
                        // When User want to Re - Open a report 
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = eSC.XMLData;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                    }
                    ds = new DataSet();
                    System.IO.StringReader xmlS = new System.IO.StringReader(rp.ResponseData);
                    ds.ReadXml(xmlS);



                    sli.PrimarySubscriberEnquiryID = ole.ResultSubscriberEnquiryID;
                    sli.PrimarySubscriberEnquiryResultID = ole.ResultSubscriberEnquiryResultID;
                    sli.CreatedbyUser = eSe.CreatedByUser;
                    sli.EventID = 0;
                    sli.InvestigationID = ole.InvestigationID;
                    sli.Billable = ole.Billable;
                    sli.BillingPrice = ole.BillingPrice;
                    sli.DetailsViewedYN = ole.Billable;

                    sli = dxdslog.InsertSubscriberLinkageInvestigationBillingDetails(strlogcon, sli);

                    DataTable dtBilling = new DataTable("SubscriberBillingInfo");
                    dtBilling.Columns.Add("PricePerClick", typeof(String));
                    dtBilling.Columns.Add("TotalPrice", typeof(String));

                    DataRow drSubscriberBilling;
                    drSubscriberBilling = dtBilling.NewRow();

                    drSubscriberBilling["PricePerClick"] = string.Format("{0:N2}", ole.BillingPrice) ;
                    drSubscriberBilling["TotalPrice"] = string.Format("{0:N2}", Math.Round(ole.TotalPrice + (ole.Billable ? ole.BillingPrice : 0), 2));

                    dtBilling.Rows.Add(drSubscriberBilling);

                    ds.Tables.Add(dtBilling);

                    rp.ResponseData = ds.GetXml();

                    ole.ResultSubscriberEnquiryID = eSC.SubscriberEnquiryID;
                    ole.ResultSubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                    ole.SubscriberEnquiryID = 0;
                    ole.SubscriberEnquiryResultID = 0;

                    try
                    {
                        ole.Response = rp.ResponseData;
                        ole.Createdondate = DateTime.Now;
                        ole.CreatedbyUser = string.Empty;
                        ole.ExtractType = string.Empty;

                        dxdslog.InsertSubscriberLinkageEvents(strlogcon, ole);
                    }
                    catch (Exception ex)
                    {
                        File.AppendAllText(@"C:\Log\CommercialLinkages.txt", "GetCommercialInvestigationHistoryReport:" + ex.Message + "\n");
                    }


                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;


                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response GetSubscriberInvestigationHistory(SqlConnection con, SqlConnection AdminConnection, SqlConnection LogConnection, int intProductID, int intSubscriberID, int intSystemUserID)
        {
            string constr = LogConnection.ConnectionString;
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            DataSet ds = new DataSet("InvestigationHistory");

            try
            {
                //Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                //Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

                //Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                //Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                //Entity.IDbioMetricStats oIDbioMetricStats = new Entity.IDbioMetricStats();
                Data.XDSLog dXDSLog = new XDSPortalEnquiry.Data.XDSLog();
                //oIDbioMetricStats.IsMaster = string.IsNullOrEmpty(sub.Level2Desc) ? false : (sub.Level2Desc.ToString() == "1" ? true : false);
                //oIDbioMetricStats.Keyword = string.IsNullOrEmpty(sub.Level1Desc) ? string.Empty : sub.Level1Desc;
                //oIDbioMetricStats.SubscriberID = sub.SubscriberID;

                ds = dXDSLog.GetSubscriberInvestigationHistory(constr, intSubscriberID,intSystemUserID);                

                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                rp.ResponseData = ds.GetXml();
            }
            catch (Exception oException)
            {
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }

            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response GetSubscriberInvestigationDetail(SqlConnection con, SqlConnection AdminConnection, SqlConnection LogConnection, int intProductID, int intSubscriberID, int intSystemUserID, int InvestigationID)
        {
            string constr = LogConnection.ConnectionString;
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            DataSet ds = new DataSet("Linkages");

            try
            {
                //Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                //Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

                //Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                //Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                //Entity.IDbioMetricStats oIDbioMetricStats = new Entity.IDbioMetricStats();
                Data.XDSLog dXDSLog = new XDSPortalEnquiry.Data.XDSLog();
                //oIDbioMetricStats.IsMaster = string.IsNullOrEmpty(sub.Level2Desc) ? false : (sub.Level2Desc.ToString() == "1" ? true : false);
                //oIDbioMetricStats.Keyword = string.IsNullOrEmpty(sub.Level1Desc) ? string.Empty : sub.Level1Desc;
                //oIDbioMetricStats.SubscriberID = sub.SubscriberID;

                ds = dXDSLog.GetInvestigationDetails(constr, InvestigationID);

                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                rp.ResponseData = ds.GetXml();
            }
            catch (Exception oException)
            {
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }

            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response GetSubscriberlinkageDetectorReport(SqlConnection con, SqlConnection AdminConnection, SqlConnection LogConnection, int intProductID, int intSubscriberID, int intSystemUserID, int InvestigationID)
        {
            string constr = LogConnection.ConnectionString;
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            DataSet ds = new DataSet("Linkages");

            Entity.SubscriberLinkageDetectorEnquiry ole = new Entity.SubscriberLinkageDetectorEnquiry();

            try
            {
                ole.InvestigationID = InvestigationID;
                ole.ProductID = intProductID;
                ole.LinkSubscriberEnquiryID = string.Empty;
                ole.LinkSubscriberEnquiryResultID = string.Empty;
                Data.XDSLog dxdslog = new XDSPortalEnquiry.Data.XDSLog();

                ole = dxdslog.GetLinkageDetectorReport(constr, ole);

                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                rp.ResponseData = ole.Xmldata;

                if (string.IsNullOrEmpty (ole.Xmldata))
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                }
            }
            catch (Exception oException)
            {
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }

            ds.Dispose();
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response GetlinkagesLatestReportExtract(SqlConnection con, SqlConnection AdminConnection, SqlConnection LogConnection, int intProductID, int intSubscriberID, int intSystemUserID, int InvestigationID)
        {
            string constr = LogConnection.ConnectionString;
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            DataSet ds = new DataSet("Linkages");

            Entity.LinkageEvents ole = new Entity.LinkageEvents();

            try
            {
                ole.InvestigationID = InvestigationID;
                
              
                Data.XDSLog dxdslog = new XDSPortalEnquiry.Data.XDSLog();

                ds = dxdslog.GetLatestReportExtract(constr, ole);

                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                rp.ResponseData = ds.GetXml();
            }
            catch (Exception oException)
            {
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }

            ds.Dispose();
            return rp;
        }
    }
}
