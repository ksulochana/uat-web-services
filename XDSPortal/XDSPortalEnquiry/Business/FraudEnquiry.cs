﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.Xml;
using XDSPortalAuthentication;

namespace XDSPortalEnquiry.Business
{
    public class FraudEnquiry
    {
        XDSPortalLibrary.Entity_Layer.ConsumerTrace eoConsumerTraceWseManager;
        XDSPortalLibrary.Entity_Layer.ConsumerIDVerificationTrace eoIDVerificationTraceWseManager;
        XDSPortalLibrary.Entity_Layer.AuthenticationSummary eoAuthenticationSummary;
        Data.XDSSettings oXDSSettings = new XDSPortalEnquiry.Data.XDSSettings();

        public string AuthAdminConnection = string.Empty;
        public int productID = 1;
        

        public FraudEnquiry(SqlConnection AdminConnection)
        {
            eoConsumerTraceWseManager = new XDSPortalLibrary.Entity_Layer.ConsumerTrace();
            eoIDVerificationTraceWseManager = new XDSPortalLibrary.Entity_Layer.ConsumerIDVerificationTrace();
        }

        public XDSPortalLibrary.Entity_Layer.AuthenticationSummary GetSummary(SqlConnection con, SqlConnection AdminConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID)
        {
            string strAdminCon = AdminConnection.ConnectionString;
            string strValidationStatus = string.Empty;
            double Totalcost = 0;
            string rXml = "";
            DataSet ds = new DataSet();
            DataSet IDVds = new DataSet();

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            SqlConnection AuthAdminCon = new SqlConnection(AuthAdminConnection);

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            XDSPortalLibrary.Entity_Layer.Response IDVrp = new XDSPortalLibrary.Entity_Layer.Response();
            XDSPortalLibrary.Entity_Layer.AuthenticationSummary eoAuthenticationSummary = new XDSPortalLibrary.Entity_Layer.AuthenticationSummary();

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                SqlConnection objConstring = new SqlConnection(eSe.ExtraVarInput1.ToString());

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);
                Entity.SubscriberProductReports sprSMS = xb.GetPrice(AdminConnection, eSe.SubscriberID, 55);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                Data.DefaultAlerts oDefaultAlerts = new XDSPortalEnquiry.Data.DefaultAlerts();

                //XDSPortalEnquiry.Data.SubscriberEnquiryResultBonus odSubscriberEnquiryResultBonus = new XDSPortalEnquiry.Data.SubscriberEnquiryResultBonus();
                //eoDefaultAlertListing.BonusSegments = odSubscriberEnquiryResultBonus.GetSubscriberEnquiryResultBonusDataSet(con, intSubscriberEnquiryResultID);

                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {

                    if (!string.IsNullOrEmpty(eSC.VoucherCode))
                    {

                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }


                    }
                    else if (sub.PayAsYouGo == 1)
                    {
                        //Calculate The Total cost of the Report , including Bonus Segments

                        Totalcost = spr.UnitPrice;

                    }


                    if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                    {
                        XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);

                        eoIDVerificationTraceWseManager.ReferenceNo = intSubscriberEnquiryID.ToString();
                        eoIDVerificationTraceWseManager.ConfirmationChkBox = true;
                        eoIDVerificationTraceWseManager.ExternalReference = eSe.SubscriberReference;
                        eoIDVerificationTraceWseManager.IDno = eSe.IDNo;
                        eoIDVerificationTraceWseManager.ProductID = eSe.ProductID;
                        eoIDVerificationTraceWseManager.subscriberID = eSe.SubscriberID;

                        IDVrp = ra.ConsumerIDVerificationMatch(eoIDVerificationTraceWseManager);

                        //If error raise one last call with a different connection
                        if (IDVrp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                            ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            IDVrp = ra.ConsumerIDVerificationMatch(eoIDVerificationTraceWseManager);
                        }

                        //IDVrp = moIDVerificationTraceWseManager.search(eoIDVerificationTraceWseManager);
                        string IdVrXml = IDVrp.ResponseData;

                        if (IDVrp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple)
                        {
                            System.IO.StringReader IDVxmlSR = new System.IO.StringReader(IdVrXml);
                            IDVds.ReadXml(IDVxmlSR);

                            if (IDVrp.ResponseKeyType == "H")
                            {
                                if (IDVds.Tables.Contains("ConsumerDetails"))
                                {
                                    foreach (DataRow r in IDVds.Tables["ConsumerDetails"].Rows)
                                    {
                                        eSC.Ha_IDNo = "";
                                        eSC.Ha_FirstName = "";
                                        eSC.Ha_SecondName = "";
                                        eSC.Ha_Surname = "";
                                        eSC.Ha_DeceasedStatus = "";
                                        eSC.Ha_DeceasedDate = "";
                                        eSC.Ha_CauseOfDeath = "";


                                        if (r.Table.Columns.Contains("IDNo"))
                                        {
                                            if (!string.IsNullOrEmpty(r["IDNo"].ToString()))
                                            {
                                                eSC.Ha_IDNo = r["IDNo"].ToString().Replace("'", "''");
                                            }
                                        }

                                        if (r.Table.Columns.Contains("Surname"))
                                        {
                                            if (!string.IsNullOrEmpty(r["Surname"].ToString()))
                                            {
                                                eSC.Ha_Surname = r["Surname"].ToString().Replace("'", "''");
                                            }
                                        }

                                        if (r.Table.Columns.Contains("FirstName"))
                                        {
                                            if (!string.IsNullOrEmpty(r["FirstName"].ToString()))
                                            {
                                                eSC.Ha_FirstName = r["FirstName"].ToString().Replace("'", "''");
                                            }
                                        }

                                        if (r.Table.Columns.Contains("SecondName"))
                                        {
                                            if (!string.IsNullOrEmpty(r["SecondName"].ToString()))
                                            {
                                                eSC.Ha_SecondName = r["SecondName"].ToString().Replace("'", "''");
                                            }
                                        }

                                        if (r.Table.Columns.Contains("DeceasedStatus"))
                                        {
                                            if (!string.IsNullOrEmpty(r["DeceasedStatus"].ToString()))
                                            {
                                                eSC.Ha_DeceasedStatus = r["DeceasedStatus"].ToString().Replace("'", "''");
                                            }
                                        }

                                        if (r.Table.Columns.Contains("DeceasedDate"))
                                        {
                                            if (!string.IsNullOrEmpty(r["DeceasedDate"].ToString()))
                                            {
                                                eSC.Ha_DeceasedDate = r["DeceasedDate"].ToString().Replace("'", "''");
                                            }
                                        }

                                        if (r.Table.Columns.Contains("CauseOfDeath"))
                                        {
                                            if (!string.IsNullOrEmpty(r["CauseOfDeath"].ToString()))
                                            {
                                                eSC.Ha_CauseOfDeath = r["CauseOfDeath"].ToString().Replace("'", "''");
                                            }
                                        }


                                    }
                                }

                            }

                            dSC.UpdateSubscriberEnquiryResult(con, eSC);
                        }


                        dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                        eoAuthenticationSummary.IDNo = eSC.IDNo;
                        eoAuthenticationSummary.PassportNo = eSC.PassportNo;
                        eoAuthenticationSummary.Firstname = eSC.FirstName;
                        eoAuthenticationSummary.Surname = eSC.Surname;
                        eoAuthenticationSummary.Gender = eSC.Gender;
                        eoAuthenticationSummary.EnquiryID = intSubscriberEnquiryID;
                        eoAuthenticationSummary.EnquiryResultID = intSubscriberEnquiryResultID;
                        eoAuthenticationSummary.Birthdate = eSC.BirthDate;
                        eoAuthenticationSummary.Reference = eSe.SubscriberReference;
                        eoAuthenticationSummary.Ha_IDno = eSC.Ha_IDNo;
                        eoAuthenticationSummary.Ha_Firstname = eSC.Ha_FirstName;
                        eoAuthenticationSummary.Ha_Surname = eSC.Ha_Surname;
                        eoAuthenticationSummary.Ha_DeceasedStatus = eSC.Ha_DeceasedStatus;

                        ArrayList oPreviousAuthentication = new ArrayList();

                        XDSPortalLibrary.Entity_Layer.PreviousAuthentication oPreviousAuthenticationDetails = new XDSPortalLibrary.Entity_Layer.PreviousAuthentication();
                        oPreviousAuthenticationDetails.SubscriberAuthenticationID = 1;
                        oPreviousAuthenticationDetails.SubscriberAuthenticationDate = DateTime.Now.AddDays(-1).ToString("yyyy/MM/dd");
                        oPreviousAuthenticationDetails.SubscriberAuthenticationTime = DateTime.Now.TimeOfDay.ToString();
                        oPreviousAuthenticationDetails.Location = "XDS";
                        oPreviousAuthenticationDetails.Status = "Authenticated";

                        oPreviousAuthentication.Add(oPreviousAuthenticationDetails);
                        eoAuthenticationSummary.AuthenticationDetails = oPreviousAuthentication.ToArray(typeof(XDSPortalLibrary.Entity_Layer.PreviousAuthentication)) as XDSPortalLibrary.Entity_Layer.PreviousAuthentication[];
                    }


                    else
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                    }

                }


                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";

                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }

            catch (Exception oException)
            {
                eoAuthenticationSummary.IsSuccess = false;
                eoAuthenticationSummary.ErrorMessage = oException.Message;

                eSC.EnquiryResult = "E";
                eSe.ErrorDescription = rp.ResponseData.ToString();
                dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            ds.Dispose();


            return eoAuthenticationSummary;
        }

        //public XDSPortalLibrary.Entity_Layer.AuthenticationQuestionDocument GetQuestions(SqlConnection con, SqlConnection AdminConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID)
        //{
        //    string strAdminCon = AdminConnection.ConnectionString;
        //    string strValidationStatus = string.Empty;
        //    double Totalcost = 0;
        //    DataSet dsQuestion = new DataSet();
        //    DataSet dsQuestionAnswer = new DataSet();
        //    int SubscriberAuthenticationID = 0;


        //    if (con.State == ConnectionState.Closed)
        //        con.Open();
        //    if (AdminConnection.State == ConnectionState.Closed)
        //        AdminConnection.Open();

        //    Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
        //    Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

        //    Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
        //    Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

        //    Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
        //    Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

        //    Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

        //    XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

        //    XDSPortalLibrary.Entity_Layer.AuthenticationQuestionDocument eoAuthenticationDocument = new XDSPortalLibrary.Entity_Layer.AuthenticationQuestionDocument();

        //    Data.Authentication dAuthentication = new XDSPortalEnquiry.Data.Authentication();

        //    try
        //    {
        //        eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
        //        eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

        //        xdsBilling xb = new xdsBilling();
        //        Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

        //        Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
        //        Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

        //        Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
        //        Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);


        //        // Check if this subscriber is authorized to use the current product
        //        if (spr.ReportID > 0)
        //        {


        //            if (!string.IsNullOrEmpty(eSC.VoucherCode))
        //            {

        //                SubscriberVoucher sv = new SubscriberVoucher();
        //                strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

        //                if (strValidationStatus == "")
        //                {
        //                    throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
        //                }
        //                else if (!(strValidationStatus == "1"))
        //                {
        //                    throw new Exception(strValidationStatus);
        //                }


        //            }
        //            else if (sub.PayAsYouGo == 1)
        //            {
        //                //Calculate The Total cost of the Report , including Bonus Segments

        //                Totalcost = spr.UnitPrice;

        //            }


        //            if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
        //            {

        //                SubscriberAuthenticationID = dAuthentication.GetSubscriberAuthenticationID(AdminConnection);
        //                dsQuestion = dAuthentication.GetSubscriberAuthenticationQuestion(AdminConnection, SubscriberAuthenticationID);
        //                dsQuestionAnswer = dAuthentication.GetSubscriberAuthenticationQuestionAnswer(AdminConnection, SubscriberAuthenticationID);


        //                XDSPortalLibrary.Entity_Layer.AuthenticationQuestion oQuestionDocument = new XDSPortalLibrary.Entity_Layer.AuthenticationQuestion();


        //                ArrayList oQuestions = new ArrayList();

        //                eoAuthenticationDocument.EnquiryID = intSubscriberEnquiryID;
        //                eoAuthenticationDocument.EnquiryresultID = intSubscriberEnquiryResultID;
        //                eoAuthenticationDocument.SubscriberAuthenticationID = SubscriberAuthenticationID;



        //                foreach (DataRow dr in dsQuestion.Tables[0].Rows)
        //                {

        //                    oQuestionDocument.ProductAuthenticationQuestionID = int.Parse(dr["ProductAuthenticationQuestionID"].ToString()) ;
        //                    oQuestionDocument.Question = dr["Question"].ToString();
        //                    oQuestionDocument.QuestionPointValue = int.Parse(dr["QuestionPointValue"].ToString());
        //                    oQuestionDocument.RequiredNoOfAnswers = int.Parse(dr["RequiredNoOfAnswers"].ToString());
        //                    oQuestionDocument.AnswerStatusInd = dr["AnswerStatusInd"].ToString();

        //                    XDSPortalLibrary.Entity_Layer.AuthenticationAnswer oAnswerDocument = new XDSPortalLibrary.Entity_Layer.AuthenticationAnswer();
        //                    ArrayList oAnswers = new ArrayList();

        //                    foreach (DataRow drAnswer in dsQuestionAnswer.Tables[0].Rows)
        //                    {
        //                        if (drAnswer["ProductAuthenticationQuestionID"].ToString() == oQuestionDocument.ProductAuthenticationQuestionID.ToString())
        //                        {
        //                            oAnswerDocument.Answer = drAnswer["Answer"].ToString();
        //                            oAnswerDocument.AnswerID = int.Parse(drAnswer["AnswerID"].ToString());
        //                            oAnswerDocument.IsEnteredAnswerYN = false;
        //                        }

        //                        oAnswers.Add(oAnswerDocument);
        //                        oQuestionDocument.Answers = oAnswers.ToArray(typeof(XDSPortalLibrary.Entity_Layer.AuthenticationAnswer)) as XDSPortalLibrary.Entity_Layer.AuthenticationAnswer[];
        //                    }


        //                    oQuestions.Add(oQuestionDocument);
        //                    eoAuthenticationDocument.Questions = oQuestions.ToArray(typeof(XDSPortalLibrary.Entity_Layer.AuthenticationQuestion)) as XDSPortalLibrary.Entity_Layer.AuthenticationQuestion[];
        //                }
        //            }


        //            else
        //            {
        //                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //                rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
        //            }


        //        }


        //        else
        //        {
        //            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //            rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";

        //        }

        //        con.Close();
        //        AdminConnection.Close();
        //        SqlConnection.ClearPool(con);
        //        SqlConnection.ClearPool(AdminConnection);


        //    }

        //    catch (Exception oException)
        //    {
        //        eoAuthenticationDocument.IsSuccess = false;
        //        eoAuthenticationDocument.ErrorMessage = oException.Message;

        //        eSC.EnquiryResult = "E";
        //        eSe.ErrorDescription = rp.ResponseData.ToString();
        //        dSe.UpdateSubscriberEnquiryError(con, eSe);
        //        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
        //        rp.ResponseData = oException.Message;

        //        if (con.State == ConnectionState.Open)
        //            con.Close();
        //        if (AdminConnection.State == ConnectionState.Open)
        //            AdminConnection.Close();
        //        SqlConnection.ClearPool(con);
        //        SqlConnection.ClearPool(AdminConnection);

        //    }
        //    dsQuestion.Dispose();
        //    dsQuestionAnswer.Dispose();


        //    return eoAuthenticationDocument;
        //}

        public XDSPortalAuthentication.AuthenticationProcess GetQuestions(SqlConnection con, SqlConnection AdminConnection, SqlConnection SMSConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, string strsmtpServer, string strsmtpUsername, string strsmtpPassword, int intport)
        {
            #region Initialize Variables

            string strAdminCon = AdminConnection.ConnectionString;
            string strValidationStatus = string.Empty;
            double Totalcost = 0;
            DataSet dsBlockSettings = new DataSet();
            DataSet dsQuestion = new DataSet();
            DataSet dsQuestionAnswer = new DataSet();
            DataSet IDVds = new DataSet();
            DataSet dsAuthHis = new DataSet();
            DataSet dsVoidReasons = new DataSet();
            DataSet dsFraudReasons = new DataSet();
            int SubscriberAuthenticationID = 0;
            bool ISBlocked = false;
            string BlockingReason = string.Empty;
            int EmailCategoryID = 0;
            int SMSProductID = 75;
            string SAFPSMessage = string.Empty;


            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            xdsBilling xb = new xdsBilling();
            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();


            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            XDSPortalAuthentication.AuthenticationProcess oAuthenticationProcess = new XDSPortalAuthentication.AuthenticationProcess();
            XDSPortalLibrary.Entity_Layer.Authentication eAuthentication = new XDSPortalLibrary.Entity_Layer.Authentication();
            XDSPortalAuthentication.AuthenticationDocument oAuthenticationDocument = new XDSPortalAuthentication.AuthenticationDocument();

            XDSPortalLibrary.Entity_Layer.SubscriberAuthentication oSa = new XDSPortalLibrary.Entity_Layer.SubscriberAuthentication();
            XDSPortalLibrary.Entity_Layer.SubscriberAuthenticationProfile oSubscriberAuthenticationProfile = new XDSPortalLibrary.Entity_Layer.SubscriberAuthenticationProfile();
            XDSPortalLibrary.Entity_Layer.BlockedConsumers ebConsumer = new XDSPortalLibrary.Entity_Layer.BlockedConsumers();

            Data.Authentication dAuthentication = new XDSPortalEnquiry.Data.Authentication();

            XDSPortalEnquiry.Data.Authentication dAuth = new XDSPortalEnquiry.Data.Authentication();
            XDSPortalLibrary.Entity_Layer.SubscriberAuthenticationProfile oProfile = new XDSPortalLibrary.Entity_Layer.SubscriberAuthenticationProfile();
            XDSPortalEnquiry.Data.XDSSettings dsettings = new XDSPortalEnquiry.Data.XDSSettings();

            #endregion

            try
            {
                oAuthenticationDocument.ErrorMessage = string.Empty;

                if (con.State == ConnectionState.Closed)
                    con.Open();
                if (AdminConnection.State == ConnectionState.Closed)
                    AdminConnection.Open();

                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                AuthAdminConnection = oXDSSettings.GetConnection(AdminConnection, eSe.ProductID);

                SqlConnection AuthAdminCon = new SqlConnection(AuthAdminConnection);

                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);
                Entity.SubscriberProductReports sprSMS = xb.GetPrice(AdminConnection, eSe.SubscriberID, SMSProductID);

                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);


                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {
                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }
                        }
                        else if (sub.PayAsYouGo == 1)
                        {
                            //Calculate The Total cost of the Report , including Bonus Segments

                            Totalcost = spr.UnitPrice;
                        }

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {
                            // Check Blocking Status

                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);

                            XDSPortalEnquiry.Data.Consumer oEnquiryConsumer = new XDSPortalEnquiry.Data.Consumer();
                            oEnquiryConsumer.CheckBlockedStatus(AdminConnection, eSe.SubscriberID, eSC.KeyID, eSe.ProductID, sub.SubscriberAssociationCode, spr.ReportID);

                            // Insert into Subscriber Authentication

                            eAuthentication.ActionedBySystemUserID = sys.SystemUserID;
                            eAuthentication.SubscriberID = sub.SubscriberID;
                            eAuthentication.ConsumerID = eSC.KeyID;
                            eAuthentication.ProductID = eSC.ProductID;
                            eAuthentication.BranchCode = eSe.BranchCode;
                            eAuthentication.PurposeID = eSe.PurposeID;
                            eAuthentication.CreatedbyUser = sys.Username;
                            eAuthentication.Subscribername = sub.SubscriberName;
                            eAuthentication.SubscribergroupCode = sub.SubscriberGroupCode;
                            eAuthentication.cellularnumber = eSe.TelephoneNo;
                            eAuthentication.SAFPSFlag = sub.SAFPSIndicator;
                            eAuthentication.OverrideOTP = bool.Parse(eSe.BusRegistrationNo);
                            eAuthentication.OverrideOTPReason = eSe.BusBusinessName;
                            eAuthentication.EmailAddress = eSe.BusVatNumber;

                            oSa = dAuthentication.GetSubscriberAuthenticationID(AuthAdminCon, eAuthentication, intSubscriberEnquiryID, intSubscriberEnquiryResultID);

                            SubscriberAuthenticationID = oSa.SubscriberAuthenticationID;
                            SAFPSMessage = oSa.SAFPSMessage;
                            // Get SubscriberAuthentication Object
                            oSa = dAuthentication.GetSubscriberAuthenticationObject(AuthAdminCon, SubscriberAuthenticationID);

                            // Get the Subscriber Profile settings
                            oSubscriberAuthenticationProfile = dAuthentication.GetSubscriberAuthenticationProfile(AuthAdminCon, oSa.SubscriberID, "PENDING");

                            if (oSubscriberAuthenticationProfile.NotificationAlert == true)
                            {
                                if (!String.IsNullOrEmpty(oSa.CellularNumber))
                                {
                                    XDSPortalLibrary.Entity_Layer.SMSNotification eSMSNotification = new XDSPortalLibrary.Entity_Layer.SMSNotification();
                                    XDSPortalLibrary.Entity_Layer.Response rpSMS = new XDSPortalLibrary.Entity_Layer.Response();
                                    SMSNotification oSMSNotification = new SMSNotification();

                                    eSMSNotification.ClientContactDetails = eSe.SubscriberReference;
                                    eSMSNotification.ContactNo = oSa.CellularNumber;
                                    eSMSNotification.SubscriberName = sub.SubscriberName;
                                    eSMSNotification.IsCommercial = false;
                                    eSMSNotification.AccountNo = "";

                                    string AlertType = string.IsNullOrEmpty(eSe.SubscriberReference) ? "AUTHNOTIFY" : "AUTHNOTIFY1"; ;
                                    eSMSNotification.AlertType = AlertType;

                                    rpSMS = oSMSNotification.SendSMSNotification(con, AdminConnection, SMSConnection, 55, eSe.SubscriberID, eSe.SystemUserID, oSa.SubscriberAuthenticationID, AlertType, eSMSNotification);

                                    if (rpSMS.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                                    {
                                        oAuthenticationDocument.OTPStatus = false;
                                        oAuthenticationDocument.OTPNotGeneratedReason = "Internal Error Generating OTP";

                                    }

                                    string ContactNo = string.Empty;

                                    rp = dAuthentication.GetLatestPhoneNo(AuthAdminCon, oSa);
                                    ContactNo = rp.ResponseData;

                                    if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                                    {
                                        oAuthenticationDocument.OTPStatus = false;
                                        oAuthenticationDocument.OTPNotGeneratedReason = rp.ResponseData;
                                    }
                                    else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                                    {
                                        if (!String.IsNullOrEmpty(oSa.CellularNumber) && oSa.CellularNumber != ContactNo)
                                        {
                                            oSMSNotification = new SMSNotification();

                                            eSMSNotification.ClientContactDetails = eSe.SubscriberReference;
                                            eSMSNotification.ContactNo = ContactNo;
                                            eSMSNotification.SubscriberName = sub.SubscriberName;
                                            eSMSNotification.IsCommercial = false;
                                            eSMSNotification.AccountNo = "";
                                            string ALertType = string.IsNullOrEmpty(eSe.SubscriberReference) ? "AUTHNOTIFY" : "AUTHNOTIFY1"; ;
                                            eSMSNotification.AlertType = ALertType;

                                            rpSMS = oSMSNotification.SendSMSNotification(con, AdminConnection, SMSConnection, 55, eSe.SubscriberID, eSe.SystemUserID, oSa.SubscriberAuthenticationID, ALertType, eSMSNotification);

                                            if (rpSMS.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                                            {
                                                oAuthenticationDocument.OTPStatus = false;
                                                oAuthenticationDocument.OTPNotGeneratedReason = "Internal Error Generating OTP";

                                            }

                                        }
                                    }
                                }
                            }

                            // Check if user has the blocking functionality enabled
                            if (oSubscriberAuthenticationProfile.BlockUser)
                            {
                                // Get the Blocking categories
                                dsBlockSettings = dAuthentication.GetSubscriberBlockingSettings(AuthAdminCon, oSubscriberAuthenticationProfile.SubscriberAuthenticationProfileID, oSa.SubscriberID);


                                // Check if Consuemr satisfies any of the enabled categories
                                if (dsBlockSettings.Tables.Count > 0)
                                {
                                    foreach (DataRow dr in dsBlockSettings.Tables[0].Rows)
                                    {
                                        if (dr["Category"].ToString().ToLower() == "safps" && oSa.SafpsIndicator)
                                        {
                                            ISBlocked = true;
                                            BlockingReason = "Fraud Indicator";
                                            EmailCategoryID = int.Parse(dr["EmailcategoryID"].ToString());
                                            oSa.AuthenticationStatusInd = "SB";
                                            oSa.Reason = BlockingReason;
                                            oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.FraudIndBlocked;
                                            break;
                                        }
                                        else if (dr["Category"].ToString().ToLower() == "deceased" && !string.IsNullOrEmpty(eSC.Ha_DeceasedStatus))
                                        {
                                            if (eSC.Ha_DeceasedStatus.ToLower().Contains("deceased"))
                                            {
                                                ISBlocked = true;
                                                BlockingReason = "Deceased on Home Affairs";
                                                EmailCategoryID = int.Parse(dr["EmailcategoryID"].ToString());
                                                oSa.AuthenticationStatusInd = "DB";
                                                oSa.Reason = BlockingReason;
                                                oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.DeceasedBlocked;
                                                break;
                                            }
                                        }
                                        else if (dr["Category"].ToString().ToLower() == "pending")
                                        {
                                            XDSPortalLibrary.Entity_Layer.SubscriberAuthenticationProfile oSAP = dAuthentication.GetSubscriberAuthenticationProfile(AuthAdminCon, oSa.SubscriberID, "pending");

                                            if (oSa.RetryCount > oSAP.NoOfRetries)
                                            {
                                                ISBlocked = true;
                                                BlockingReason = "Pending attempts exceeded";
                                                EmailCategoryID = int.Parse(dr["EmailcategoryID"].ToString());
                                                oSa.AuthenticationStatusInd = "PB";
                                                oSa.Reason = BlockingReason;
                                                oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.PendingBlocked;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }

                            // Genrate Questions if user is not blocked
                            if (!ISBlocked)
                            {
                                if (!string.IsNullOrEmpty(eSC.ExtraVarOutput1))
                                {
                                    DateTime issueDate;
                                    DateTime.TryParse(eSC.ExtraVarOutput1, out issueDate);
                                    oSa.IDIssuedDate = issueDate.ToString("yyyy-MM-dd");
                                }
                                else
                                    oSa.IDIssuedDate = "";
                               
                                // Generate Questions, Authentication History and Void Reasons
                                dAuthentication.GenerateQuestions(AuthAdminCon, oSa);
                                oSa = dAuthentication.GetSubscriberAuthenticationObject(AuthAdminCon, SubscriberAuthenticationID);
                                dsQuestion = dAuthentication.GetSubscriberAuthenticationQuestion(AuthAdminCon, SubscriberAuthenticationID);
                                dsQuestionAnswer = dAuthentication.GetSubscriberAuthenticationQuestionAnswer(AuthAdminCon, SubscriberAuthenticationID);
                                dsAuthHis = dAuthentication.GetConsumerAuthenticationHistory(AuthAdminCon, SubscriberAuthenticationID, oSa.ConsumerID);
                                dsVoidReasons = dAuthentication.GetSubscriberVoidReasons(AuthAdminCon, SubscriberAuthenticationID, oSa.SubscriberID);

                                // Populate Questions, Authentication History and Void Reason Documents

                                ArrayList oQuestions = new ArrayList();

                                foreach (DataRow dr in dsQuestion.Tables[0].Rows)
                                {

                                    XDSPortalAuthentication.QuestionDocument oQuestionDocument = new XDSPortalAuthentication.QuestionDocument();

                                    oQuestionDocument.ProductAuthenticationQuestionID = int.Parse(dr["ProductAuthenticationQuestionID"].ToString());
                                    oQuestionDocument.Question = dr["Question"].ToString();
                                    oQuestionDocument.QuestionPointValue = int.Parse(dr["QuestionPointValue"].ToString());
                                    oQuestionDocument.RequiredNoOfAnswers = int.Parse(dr["RequiredNoOfAnswers"].ToString());
                                    oQuestionDocument.AnswerStatusInd = dr["AnswerStatusInd"].ToString();
                                    oQuestionDocument.RequiredNoOfAnswers = int.Parse(dr["RequiredNoOfAnswers"].ToString());


                                    ArrayList oAnswers = new ArrayList();

                                    foreach (DataRow drAnswer in dsQuestionAnswer.Tables[0].Rows)
                                    {
                                        XDSPortalAuthentication.AnswerDocument oAnswerDocument = new XDSPortalAuthentication.AnswerDocument();

                                        if (drAnswer["ProductAuthenticationQuestionID"].ToString() == oQuestionDocument.ProductAuthenticationQuestionID.ToString())
                                        {

                                            oAnswerDocument.Answer = drAnswer["Answer"].ToString();
                                            oAnswerDocument.AnswerID = int.Parse(drAnswer["AnswerID"].ToString());
                                            oAnswerDocument.IsEnteredAnswerYN = false;
                                            oAnswers.Add(oAnswerDocument);
                                        }


                                    }
                                    oQuestionDocument.Answers = oAnswers.ToArray(typeof(XDSPortalAuthentication.AnswerDocument)) as XDSPortalAuthentication.AnswerDocument[];

                                    oQuestions.Add(oQuestionDocument);
                                }
                                oAuthenticationDocument.Questions = oQuestions.ToArray(typeof(XDSPortalAuthentication.QuestionDocument)) as XDSPortalAuthentication.QuestionDocument[];

                                ArrayList oAuthHistory = new ArrayList();

                                if (dsAuthHis.Tables.Count > 0)
                                {
                                    foreach (DataRow drAuthHis in dsAuthHis.Tables[0].Rows)
                                    {
                                        XDSPortalAuthentication.AuthenticationHistoryDocument oHistoryDocument = new XDSPortalAuthentication.AuthenticationHistoryDocument();
                                        oHistoryDocument.BureauDate = drAuthHis["BureauDate"].ToString();
                                        oHistoryDocument.SubscriberName = drAuthHis["SubscriberName"].ToString();
                                        oHistoryDocument.cellularNumber = drAuthHis["Cellularnumber"].ToString();
                                        oHistoryDocument.AuthenticationStatus = drAuthHis["AuthenticationStatusInd"].ToString();
                                        oHistoryDocument.AuthenticationPerc = drAuthHis["AuthenticatedPerc"].ToString();
                                        oHistoryDocument.EmailAddress = drAuthHis["EmailAddress"].ToString();

                                        oAuthHistory.Add(oHistoryDocument);
                                    }
                                }

                                oAuthenticationDocument.AuthenticationHistory = oAuthHistory.ToArray(typeof(XDSPortalAuthentication.AuthenticationHistoryDocument)) as XDSPortalAuthentication.AuthenticationHistoryDocument[];

                                ArrayList oVoidReasons = new ArrayList();

                                if (dsVoidReasons.Tables.Count > 0)
                                {
                                    foreach (DataRow drVoidReason in dsVoidReasons.Tables[0].Rows)
                                    {
                                        XDSPortalAuthentication.VoidReasonsDocument oVoidReasonDocument = new XDSPortalAuthentication.VoidReasonsDocument();
                                        oVoidReasonDocument.VoidReasonID = long.Parse(drVoidReason["ID"].ToString());
                                        oVoidReasonDocument.VoidReason = drVoidReason["VoidReason"].ToString();
                                        oVoidReasonDocument.IsEnteredReasonYN = false;

                                        oVoidReasons.Add(oVoidReasonDocument);
                                    }
                                }

                                oAuthenticationDocument.VoidReasons = oVoidReasons.ToArray(typeof(XDSPortalAuthentication.VoidReasonsDocument)) as XDSPortalAuthentication.VoidReasonsDocument[];
                                
                                if (oSubscriberAuthenticationProfile.ReferToFraud)
                                {
                                    dsFraudReasons = dAuthentication.GetSubscriberFraudReasons(AuthAdminCon, SubscriberAuthenticationID, oSa.SubscriberID);

                                    ArrayList oFraudReasons = new ArrayList();

                                    if (dsFraudReasons.Tables.Count > 0)
                                    {
                                        foreach (DataRow drFraudReason in dsFraudReasons.Tables[0].Rows)
                                        {
                                            XDSPortalAuthentication.FraudReasonsDocument oFraudReasonDocument = new XDSPortalAuthentication.FraudReasonsDocument();
                                            oFraudReasonDocument.FraudReasonID = long.Parse(drFraudReason["ID"].ToString());
                                            oFraudReasonDocument.FraudReason = drFraudReason["FraudReason"].ToString();
                                            oFraudReasonDocument.IsEnteredReasonYN = false;

                                            oFraudReasons.Add(oFraudReasonDocument);
                                        }
                                    }

                                    oAuthenticationDocument.FraudReasons = oFraudReasons.ToArray(typeof(XDSPortalAuthentication.FraudReasonsDocument)) as XDSPortalAuthentication.FraudReasonsDocument[];

                                }

                                if (sprSMS.ReportID > 0 && !oSa.OverrideOTP)
                                {
                                    string ContactNo = string.Empty;
                                    string OTP = string.Empty;
                                    oAuthenticationDocument.OTPEnabled = true;
                                    rp = dAuthentication.GetLatestPhoneNo(AuthAdminCon, oSa);
                                    ContactNo = rp.ResponseData;

                                    if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                                    {
                                        oAuthenticationDocument.OTPStatus = false;
                                        oAuthenticationDocument.OTPNotGeneratedReason = rp.ResponseData;
                                    }
                                    else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report && !oSubscriberAuthenticationProfile.ExcludeLatestDBcellNo)
                                    {

                                        rp = dAuthentication.GenerateOTP(AuthAdminCon, oSa, eSC);
                                        OTP = rp.ResponseData;

                                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                                        {
                                            oAuthenticationDocument.OTPStatus = false;
                                            oAuthenticationDocument.OTPNotGeneratedReason = rp.ResponseData;
                                        }
                                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                                        {
                                            XDSPortalLibrary.Entity_Layer.SMSNotification eSMSNotification = new XDSPortalLibrary.Entity_Layer.SMSNotification();
                                            XDSPortalLibrary.Entity_Layer.Response rpSMS = new XDSPortalLibrary.Entity_Layer.Response();

                                            SMSNotification oSMSNotification = new SMSNotification();

                                            eSMSNotification.ClientContactDetails = string.Empty;
                                            eSMSNotification.ContactNo = ContactNo;
                                            eSMSNotification.SubscriberName = sub.SubscriberName;
                                            eSMSNotification.IsCommercial = false;
                                            eSMSNotification.AccountNo = OTP;
                                            eSMSNotification.AlertType = "AUTHOTP";

                                            rpSMS = oSMSNotification.SendSMSNotification(con, AdminConnection, SMSConnection, SMSProductID, eSe.SubscriberID, eSe.SystemUserID, oSa.SubscriberAuthenticationID, "AUTHOTP", eSMSNotification);

                                            if (rpSMS.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                                            {
                                                oAuthenticationDocument.OTPStatus = false;
                                                oAuthenticationDocument.OTPNotGeneratedReason = "Internal Error Generating OTP";

                                            }
                                            else if (rpSMS.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                                            {
                                                int QueueID = int.Parse(rpSMS.ResponseReferenceNo);
                                                rp = dAuthentication.LogOTP(AuthAdminCon, oSa, QueueID, OTP, XDSPortalEnquiry.Data.OTPAction.Insert);

                                                if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                                                {
                                                    oAuthenticationDocument.OTPStatus = false;
                                                    oAuthenticationDocument.OTPNotGeneratedReason = rp.ResponseData;
                                                }
                                                else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                                                {
                                                    oAuthenticationDocument.OTPStatus = true;
                                                    oAuthenticationDocument.OTPNotGeneratedReason = string.Empty;

                                                    if (String.IsNullOrEmpty(eSC.VoucherCode))
                                                    {
                                                        Totalcost = Totalcost + sprSMS.UnitPrice;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if (oSa.CellularNumber != ContactNo && !string.IsNullOrEmpty(oSa.CellularNumber.Trim()) && !oSubscriberAuthenticationProfile.ExcludeInputcellNo)
                                    {
                                        XDSPortalLibrary.Entity_Layer.SMSNotification eISMSNotification = new XDSPortalLibrary.Entity_Layer.SMSNotification();
                                        XDSPortalLibrary.Entity_Layer.Response rpISMS = new XDSPortalLibrary.Entity_Layer.Response();

                                        SMSNotification oISMSNotification = new SMSNotification();

                                        eISMSNotification.ClientContactDetails = string.Empty;
                                        eISMSNotification.ContactNo = oSa.CellularNumber;
                                        eISMSNotification.SubscriberName = sub.SubscriberName;
                                        eISMSNotification.IsCommercial = false;
                                        eISMSNotification.AlertType = "AUTHOTP";
                                        if (string.IsNullOrEmpty(OTP))
                                        {
                                            rp = dAuthentication.GenerateOTP(AuthAdminCon, oSa, eSC);
                                            OTP = rp.ResponseData;

                                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                                            {
                                                oAuthenticationDocument.OTPStatus = false;
                                                oAuthenticationDocument.OTPNotGeneratedReason = rp.ResponseData;
                                            }
                                        }

                                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                                        {

                                            eISMSNotification.AccountNo = OTP;


                                            rpISMS = oISMSNotification.SendSMSNotification(con, AdminConnection, SMSConnection, SMSProductID, eSe.SubscriberID, eSe.SystemUserID, oSa.SubscriberAuthenticationID, "AUTHOTP", eISMSNotification);

                                            if (rpISMS.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                                            {
                                                oAuthenticationDocument.OTPStatus = false;
                                                oAuthenticationDocument.OTPNotGeneratedReason = "Internal Error Generating OTP";

                                            }
                                            else if (rpISMS.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                                            {
                                                int IQueueID = int.Parse(rpISMS.ResponseReferenceNo);
                                                rp = dAuthentication.LogOTP(AuthAdminCon, oSa, IQueueID, OTP, XDSPortalEnquiry.Data.OTPAction.Insert);

                                                if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                                                {
                                                    oAuthenticationDocument.OTPStatus = false;
                                                    oAuthenticationDocument.OTPNotGeneratedReason = rp.ResponseData;
                                                }
                                                else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                                                {
                                                    oAuthenticationDocument.OTPStatus = true;
                                                    oAuthenticationDocument.OTPNotGeneratedReason = string.Empty;

                                                    if (String.IsNullOrEmpty(eSC.VoucherCode))
                                                    {
                                                        Totalcost = Totalcost + sprSMS.UnitPrice;
                                                    }
                                                }
                                            }

                                        }
                                    }
                                    if (!string.IsNullOrEmpty(oSa.EmailAddress.Trim()) && !oSubscriberAuthenticationProfile.ExcludeEmailAddress)
                                    {
                                        XDSPortalLibrary.Entity_Layer.SMSNotification eISMSNotification = new XDSPortalLibrary.Entity_Layer.SMSNotification();

                                        eISMSNotification.SubscriberID = sys.SubscriberID;
                                        eISMSNotification.SubscriberName = sub.SubscriberName;
                                        if (string.IsNullOrEmpty(OTP))
                                        {
                                            rp = dAuthentication.GenerateOTP(AuthAdminCon, oSa, eSC);
                                            OTP = rp.ResponseData;

                                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                                            {
                                                oAuthenticationDocument.OTPStatus = false;
                                                oAuthenticationDocument.OTPNotGeneratedReason = rp.ResponseData;
                                            }
                                        }

                                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                                        {
                                            XDSPortalEnquiry.Data.DefaultAlerts oOTP = new XDSPortalEnquiry.Data.DefaultAlerts();
                                            eISMSNotification.AccountNo = OTP;
                                            string strMessage = oOTP.GetOTPMessage(AdminConnection, eISMSNotification);

                                            try
                                            {
                                                ProcessEmailOTP(AuthAdminCon, "Q", oSa, strsmtpServer, strsmtpUsername, strsmtpPassword, intport, oAuthenticationProcess, eSe, "Authentication - OTP", strMessage);
                                                oAuthenticationDocument.OTPStatus = true;
                                                oAuthenticationDocument.OTPNotGeneratedReason = string.Empty;
                                            }
                                            catch (Exception ex)
                                            {
                                                oAuthenticationDocument.OTPStatus = false;
                                                oAuthenticationDocument.OTPNotGeneratedReason = rp.ResponseData;
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                // Block the user if user satisfies any of the blocking conditions

                                ebConsumer.BlockedByUser = eSC.CreatedByUser;
                                ebConsumer.BlockedOnDate = DateTime.Now.ToString("yyyy/MM/dd");
                                ebConsumer.ConsumerID = eSC.KeyID;
                                ebConsumer.DateofBirth = eSC.BirthDate;
                                ebConsumer.FirstName = eSC.FirstName;
                                ebConsumer.IDNo = eSC.IDNo;
                                ebConsumer.IsBlocked = true;
                                ebConsumer.PassportNo = eSC.PassportNo;
                                ebConsumer.ProductID = eSC.ProductID;
                                ebConsumer.SecondName = eSC.SecondName;
                                ebConsumer.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                ebConsumer.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                ebConsumer.SubscriberID = eSe.SubscriberID;
                                ebConsumer.SurName = eSC.Surname;
                                ebConsumer.SystemUserID = eSe.SystemUserID;
                                ebConsumer.BlockingReason = BlockingReason;
                                ebConsumer.SubscriberName = eSe.SubscriberName;

                                rp = dAuthentication.BlockUnblockConsumer(AdminConnection, ebConsumer);

                                if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                                {
                                    oAuthenticationDocument.IsUserBlocked = true;
                                    oAuthenticationDocument.blockid = rp.ResponseKey;
                                    oAuthenticationDocument.BlockingReason = ebConsumer.BlockingReason;
                                }
                                else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                                {
                                    throw new Exception(rp.ResponseData);
                                }

                                // Send Blocking email alert

                                ProcessEmail(AuthAdminCon, "B", "Q", oSa, strsmtpServer, strsmtpUsername, strsmtpPassword, intport, oAuthenticationProcess, EmailCategoryID, eSe);

                                oSa.EncryptedReferenceNo = dAuthentication.GetEncryptedReferenceNo(oSa, eSC);

                                // Update the Encrypted reference no to database
                                dAuthentication.UpdateSubscriberAuthentication(AuthAdminCon, oSa);

                            }


                            // Populate Authentication Document with info

                            oAuthenticationDocument.ReferToFraud = oSubscriberAuthenticationProfile.ReferToFraud;
                            oAuthenticationDocument.QuestionTimeout = oSubscriberAuthenticationProfile.QuestionTimeout;
                            oAuthenticationDocument.PersonalQuestionsenabled = oSubscriberAuthenticationProfile.PersonalQuestionsCheck;
                            oAuthenticationDocument.AuthenticatedPerc = oSa.AuthenticatedPerc;
                            oAuthenticationDocument.AuthenticationComment = oSa.AuthenticationComment;
                            oAuthenticationDocument.ActionedBySystemUserID = oSa.ActionedBySystemUserID;
                            oAuthenticationDocument.AuthenticationDate = oSa.AuthenticationDate;
                            oAuthenticationDocument.AuthenticationStatusInd = oSa.AuthenticationStatusInd;
                            oAuthenticationDocument.AuthenticationTypeInd = oSa.AuthenticationTypeInd;
                            oAuthenticationDocument.BirthDate = eSC.BirthDate.ToString("yyyy/MM/dd");
                            oAuthenticationDocument.ConsumerAccountAgeMessage = oSa.ConsumerAccountAgeMessage;
                            oAuthenticationDocument.EncryptedReferenceNo = string.Empty;
                            oAuthenticationDocument.FirstName = eSC.FirstName;
                            oAuthenticationDocument.Gender = eSC.Gender;
                            oAuthenticationDocument.Ha_CauseOfDeath = eSC.Ha_CauseOfDeath;
                            oAuthenticationDocument.Ha_DeaseasedDate = eSC.Ha_DeceasedDate;
                            oAuthenticationDocument.Ha_DeaseasedStatus = eSC.Ha_DeceasedStatus;
                            if (!string.IsNullOrEmpty(eSC.ExtraVarOutput1))
                            {
                                DateTime issueDate;
                                DateTime.TryParse(eSC.ExtraVarOutput1, out issueDate);
                                oAuthenticationDocument.Ha_IDIssuedDate = issueDate.ToString("yyyy/MM/dd");
                            }
                            else
                                oAuthenticationDocument.Ha_IDIssuedDate = "";
                            oAuthenticationDocument.Ha_FirstName = eSC.Ha_FirstName;
                            oAuthenticationDocument.Ha_IDNo = eSC.Ha_IDNo;
                            oAuthenticationDocument.Ha_SecondName = eSC.Ha_SecondName;
                            oAuthenticationDocument.Ha_Surname = eSC.Ha_Surname;
                            oAuthenticationDocument.IDNo = eSC.IDNo;
                            oAuthenticationDocument.PassportNo = eSC.PassportNo;
                            oAuthenticationDocument.SAFPSIndicator = oSa.SafpsIndicator;
                            oAuthenticationDocument.ReferenceNo = oSa.ReferenceNo;
                            oAuthenticationDocument.RepeatAuthenticationMessage = oSa.RepeatAuthenticationMessage;
                            oAuthenticationDocument.RequiredAuthenticatedPerc = oSa.RequiredAuthenticatedPerc;
                            oAuthenticationDocument.SecondName = eSC.SecondName;
                            oAuthenticationDocument.SubscriberAuthenticationID = oSa.SubscriberAuthenticationID;
                            oAuthenticationDocument.SubscriberID = oSa.SubscriberID;
                            oAuthenticationDocument.Surname = eSC.Surname;
                            oAuthenticationDocument.TotalQuestionPointValue = int.Parse(oSa.TotalQuestionPointValue.ToString());
                            oAuthenticationDocument.EncryptedReferenceNo = oSa.EncryptedReferenceNo;
                            oAuthenticationDocument.SAFPSMessage = SAFPSMessage;
                            oAuthenticationDocument.NoofAttemptsRemaining = (oSubscriberAuthenticationProfile.NoOfRetries - oSa.RetryCount);
                            // Sync the Authentication Document with AuthenticationProcess object
                            oAuthenticationProcess.GetAuthenticationProcessObject(oAuthenticationDocument);

                            // Set the values to be updated to subscriberEnquiryResult table

                            eSC.ExtraIntOutput1 = SubscriberAuthenticationID;
                            eSC.EnquiryResult = "A";
                            //eSC.Billable = true;
                            if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                            {
                                eSC.Billable = false;
                            }
                            else
                            {
                                eSC.Billable = true;
                            }
                            eSC.DetailsViewedDate = DateTime.Now;
                            eSC.DetailsViewedYN = true;
                            eSC.BillingTypeID = spr.BillingTypeID;
                            eSC.BillingPrice = spr.UnitPrice;

                            // Convert the AuthenticationProcess object to XML , so that it can be updated to 
                            XmlSerializer serializer = new XmlSerializer(oAuthenticationProcess.GetType());
                            System.IO.StringWriter sw = new System.IO.StringWriter();
                            serializer.Serialize(sw, oAuthenticationProcess);
                            System.IO.StringReader reader = new System.IO.StringReader(sw.ToString());

                            eSC.XMLData = reader.ReadToEnd();

                            dSC.UpdateSubscriberEnquiryResult(con, eSC);

                            rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                            rp.EnquiryID = eSC.SubscriberEnquiryID;

                            // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                            if (sub.PayAsYouGo == 1)
                            {
                                dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                            }
                            if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                            {
                                dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                            }
                        }
                        else
                        {
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                            oAuthenticationDocument.ErrorMessage = rp.ResponseData;
                            oAuthenticationProcess.GetAuthenticationProcessObject(oAuthenticationDocument);
                            throw new Exception(rp.ResponseData);
                        }

                    }
                    else
                    {

                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        oAuthenticationDocument.ErrorMessage = "Questions were already generated for this Authentication Request";
                        oAuthenticationProcess.GetAuthenticationProcessObject(oAuthenticationDocument);
                        //XmlSerializer deserialize = new XmlSerializer(oAuthenticationProcess.GetType());
                        //StringReader sr = new StringReader(eSC.XMLData);
                        //XmlTextReader xmlTextReader = new XmlTextReader(sr);
                        //XmlDataDocument xdoc1 = new XmlDataDocument();
                        //xdoc1.DataSet.ReadXml(xmlTextReader, XmlReadMode.InferSchema);
                        //MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(xdoc1.DataSet.GetXml())); 
                        //oAuthenticationProcess = (XDSPortalAuthentication.AuthenticationProcess)deserialize.`ialize(ms);
                    }


                }
                else
                {
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                    oAuthenticationDocument.ErrorMessage = rp.ResponseData;
                    oAuthenticationProcess.GetAuthenticationProcessObject(oAuthenticationDocument);
                    throw new Exception(rp.ResponseData);
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }

            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                dSe.UpdateSubscriberEnquiryError(con, eSe);

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

                oAuthenticationDocument.ErrorMessage = oException.Message;

                oAuthenticationProcess.GetAuthenticationProcessObject(oAuthenticationDocument);

                throw new Exception(oException.Message);

            }

            dsQuestion.Dispose();
            dsQuestionAnswer.Dispose();
            dsAuthHis.Dispose();
            dsBlockSettings.Dispose();
            dsVoidReasons.Dispose();

            return oAuthenticationProcess;
        }

        public XDSPortalAuthentication.AuthenticationProcess SubmitAuthentication(SqlConnection con, SqlConnection AdminConnection, SqlConnection SMSConnection, XDSPortalAuthentication.AuthenticationProcess oAuthenticationProcess, string strsmtpServer, string strsmtpUsername, string strsmtpPassword, int intport)
        {

            try
            {
                oAuthenticationProcess.SyncAuthenticationDocument();

                AuthenticationProcess(con, AdminConnection, SMSConnection, oAuthenticationProcess, strsmtpServer, strsmtpUsername, strsmtpPassword, intport);

            }

            catch (Exception oException)
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

                throw new Exception(oException.Message);

            }


            return oAuthenticationProcess;
        }

        public XDSPortalAuthentication.AuthenticationProcess AuthenticationProcess(SqlConnection con, SqlConnection AdminConnection, SqlConnection SMSConnection, XDSPortalAuthentication.AuthenticationProcess oAuthenticationProcess, string strsmtpServer, string strsmtpUsername, string strsmtpPassword, int intport)
        {
            #region Intialize variables

            string strAdminCon = AdminConnection.ConnectionString;
            string strValidationStatus = string.Empty;
            bool OTPMatch = false;
            DataSet dsPersonalQuestions = new DataSet();
            DataSet dsAnswers = new DataSet();
            DataSet dsBlockSettings = new DataSet();
            XDSPortalLibrary.Entity_Layer.SubscriberAuthenticationProfile oSubscriberAuthenticationProfile = new XDSPortalLibrary.Entity_Layer.SubscriberAuthenticationProfile();
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            int SubscriberAuthenticationID = 0;

            XDSPortalLibrary.Entity_Layer.Authentication eAuthentication = new XDSPortalLibrary.Entity_Layer.Authentication();

            Data.Authentication dAuthentication = new XDSPortalEnquiry.Data.Authentication();

            XDSPortalAuthentication.AuthenticationDocument oAuthenticationDocument = oAuthenticationProcess.CurrentObjectState.AuthenticationDocument;
            BlockedConsumers obConsumer = new BlockedConsumers();
            XDSPortalLibrary.Entity_Layer.BlockedConsumers ebConsumer = new XDSPortalLibrary.Entity_Layer.BlockedConsumers();

            XDSPortalLibrary.Entity_Layer.SubscriberAuthentication oSa = new XDSPortalLibrary.Entity_Layer.SubscriberAuthentication();
            xdsBilling xb = new xdsBilling();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();


            #endregion

            try
            {

                oAuthenticationDocument.ErrorMessage = string.Empty;

                if (con.State == ConnectionState.Closed)
                    con.Open();
                if (AdminConnection.State == ConnectionState.Closed)
                    AdminConnection.Open();


                // Get Subscriber Authentication ID

                SubscriberAuthenticationID = int.Parse(oAuthenticationDocument.SubscriberAuthenticationID.ToString());

                // Get the Authentication connection string

                AuthAdminConnection = oXDSSettings.GetConnection(AdminConnection, productID);

                SqlConnection AuthAdminCon = new SqlConnection(AuthAdminConnection);

                // Get the Subscriber Authentication Object

                oSa = dAuthentication.GetSubscriberAuthenticationObject(AuthAdminCon, SubscriberAuthenticationID);
                oAuthenticationDocument.RequiredAuthenticatedPerc = oSa.RequiredAuthenticatedPerc;

                // Get the Subscriber Enquiry object
                eSe = dSe.GetSubscriberEnquiryObject(con, oSa.EnquiryID);

                Entity.SubscriberProductReports sprSMS = xb.GetPrice(AdminConnection, eSe.SubscriberID, 75);

                //string strSelect = string.Format("Insert AuthLog(AuthObject,SubscriberAuthenticationID) Values ('{0}','{1}')",
                //    oSa.AuthenticationStatusInd + " -- " + oAuthenticationProcess.CurrentObjectState.AuthenticationDocument.AuthenticationStatusInd
                //     + " -- " + oAuthenticationProcess.CurrentObjectState.AuthenticationDocument.AuthenticationStatusReason.ToString()
                //      + " -- " + oAuthenticationProcess.CurrentObjectState.AuthenticationDocument.SubscriberAuthenticationID.ToString(), 
                //    SubscriberAuthenticationID.ToString());
                //SqlCommand authlog = new SqlCommand(strSelect, AdminConnection);

                //if (AdminConnection.State == ConnectionState.Closed)
                //    AdminConnection.Open();

                //authlog.ExecuteNonQuery();

                //if (AdminConnection.State == ConnectionState.Open)
                //    AdminConnection.Close();

                // Check if the Authentication is already Processed
                if (oSa.AuthenticationStatusInd != "P")
                {
                    throw new Exception("This Authentication request is already Processed");
                }

                // Check if OTP Matches

                if (sprSMS.ReportID > 0)
                {
                    rp = dAuthentication.LogOTP(AuthAdminCon, oSa, 0, oAuthenticationProcess.OTPValue.ToString(), XDSPortalEnquiry.Data.OTPAction.Evaluate);

                    if (rp.ResponseKey == 0)
                    {
                        OTPMatch = false;
                        //throw new Exception("Invalid OTP supplied");
                    }
                    else
                    {
                        OTPMatch = true;
                    }
                }
                else
                {
                    OTPMatch = true;
                }

                // Check if all the questions were answered

                if (oAuthenticationDocument.Questions != null)
                {
                    // Convert the AuthenticationProcess object to XML , so that it can be updated to 
                    XmlSerializer serializer1 = new XmlSerializer(oAuthenticationProcess.GetType());
                    System.IO.StringWriter sw1 = new System.IO.StringWriter();
                    serializer1.Serialize(sw1, oAuthenticationProcess);
                    System.IO.StringReader reader1 = new System.IO.StringReader(sw1.ToString());


                    SqlCommand ocom1 = new SqlCommand("Insert into AuthLog(AuthObject,SubscriberAuthenticationID) Values (@AuthObject, @SubscriberAuthenticationID)", AdminConnection);
                    ocom1.Parameters.AddWithValue("@AuthObject", reader1.ReadToEnd());
                    ocom1.Parameters.AddWithValue("@SubscriberAuthenticationID", SubscriberAuthenticationID);

                    if (AdminConnection.State == ConnectionState.Closed)
                        AdminConnection.Open();

                    ocom1.ExecuteNonQuery();

                    if (AdminConnection.State == ConnectionState.Open)
                        AdminConnection.Close();


                    foreach (XDSPortalAuthentication.QuestionDocument oQuestionDocument in oAuthenticationDocument.Questions)
                    {
                        bool IsEnteredAnswer = false;

                        foreach (XDSPortalAuthentication.AnswerDocument oAnswerDocument in oQuestionDocument.Answers)
                        {
                            if (oAnswerDocument.IsEnteredAnswerYN)
                            {
                                IsEnteredAnswer = oAnswerDocument.IsEnteredAnswerYN;
                            }
                        }

                        if (!IsEnteredAnswer)
                        {
                            throw new Exception("Please answer all questions before trying to authenticate! ");
                        }
                    }

                    // Read the Answers supplied to Data Set

                    XmlSerializer serializer = new XmlSerializer(oAuthenticationProcess.CurrentObjectState.AuthenticationDocument.Questions.GetType());
                    System.IO.StringWriter sw = new System.IO.StringWriter();
                    serializer.Serialize(sw, oAuthenticationProcess.CurrentObjectState.AuthenticationDocument.Questions);
                    System.IO.StringReader reader = new System.IO.StringReader(sw.ToString());
                    dsAnswers.ReadXml(reader);


                    // Update answers to data base

                    if (dsAnswers.Tables.Count > 0 && dsAnswers.Tables.Contains("AnswerDocument"))
                    {
                        dAuthentication.UpdateSubscriberAuthenticationQuestionAnswer(AuthAdminCon, int.Parse(oAuthenticationProcess.CurrentObjectState.AuthenticationDocument.SubscriberID.ToString()), SubscriberAuthenticationID, 0, dsAnswers.Tables["AnswerDocument"]);
                    }

                    // Validate Answers
                    dAuthentication.ValidateAnswers(AuthAdminCon, oAuthenticationProcess, oSa.CreatedByUser, OTPMatch);


                    // Get the Authentication percentage and Status
                    oSa = dAuthentication.GetSubscriberAuthenticationObject(AuthAdminCon, SubscriberAuthenticationID);

                }
                else
                {
                    throw new Exception("No Questions received to validate");
                }

                // Get the Subscriber Enquiry Result object

                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, oSa.EnquiryResultID);

                // Get the Subscriber Authentication Profile

                oSubscriberAuthenticationProfile = dAuthentication.GetSubscriberAuthenticationProfile(AuthAdminCon, oSa.SubscriberID, "failedauthentication");

                //OTP Retry - V2.2
                if (sprSMS.ReportID > 0 && !OTPMatch)
                {
                    if (oSubscriberAuthenticationProfile.ResendOTP && (oSa.OTPRetryCount < oSubscriberAuthenticationProfile.NoOfOTPRetries))
                    {
                        oSa.OTPRetryCount++;
                        dAuthentication.UpdateSubscriberAuthenticationOTPRetryCount(AuthAdminCon, oSa);

                        throw new Exception("Invalid OTP. Please Retry");
                    }
                }


                bool BlockUser = false;

                // Check if user should be blocked
                if (oSa.AuthenticationStatusInd.ToLower() == "n" && oSa.NoofmandatoryQuestions > 0 && oSa.MandatoryCriteriaMet)
                {
                    // Get the Blocking categories
                    dsBlockSettings = dAuthentication.GetSubscriberBlockingSettings(AuthAdminCon, oSubscriberAuthenticationProfile.SubscriberAuthenticationProfileID, oSa.SubscriberID);

                    // Check if Consumer satisfies any of the enabled categories
                    if (dsBlockSettings.Tables.Count > 0)
                    {
                        foreach (DataRow dr in dsBlockSettings.Tables[0].Rows)
                        {
                            if (dr["Category"].ToString().ToLower() == "mandatory")
                            {
                                BlockUser = true;
                                break;
                            }
                        }
                    }

                    oSubscriberAuthenticationProfile.BlockUser = BlockUser;

                    if (BlockUser)
                    {
                        // Block consumer

                        ebConsumer.BlockedByUser = oSa.CreatedByUser;
                        ebConsumer.BlockedOnDate = DateTime.Now.ToString("yyyy/MM/dd");
                        ebConsumer.ConsumerID = oSa.ConsumerID;

                        ebConsumer.DateofBirth = eSC.BirthDate;
                        ebConsumer.FirstName = eSC.FirstName;
                        ebConsumer.IDNo = eSC.IDNo;
                        ebConsumer.IsBlocked = true;
                        ebConsumer.PassportNo = eSC.PassportNo;
                        ebConsumer.ProductID = eSC.ProductID;
                        ebConsumer.SecondName = eSC.SecondName;
                        ebConsumer.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                        ebConsumer.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                        ebConsumer.SubscriberID = oSa.SubscriberID;
                        ebConsumer.SurName = eSC.Surname;
                        ebConsumer.SystemUserID = oSa.ActionedBySystemUserID;

                        ebConsumer.BlockingReason = "Blocked due to Mandatory criteria not met";
                        oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.NAInvalidAnswersB;

                        ebConsumer.SubscriberName = eSe.SubscriberName;

                        rp = dAuthentication.BlockUnblockConsumer(AdminConnection, ebConsumer);

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            oAuthenticationDocument.IsUserBlocked = true;
                            oAuthenticationDocument.blockid = rp.ResponseKey;
                            oAuthenticationDocument.BlockingReason = ebConsumer.BlockingReason;
                            oSa.AuthenticationStatusInd = "NB";
                            oSa.Reason = ebConsumer.BlockingReason;
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            throw new Exception(rp.ResponseData);
                        }

                        // Send Email

                       // ProcessEmail(AuthAdminCon, "B", "Q", oSa, strsmtpServer, strsmtpUsername, strsmtpPassword, intport, oAuthenticationProcess, 0, eSe);
                    }
                    else
                    {
                        if (oSa.AuthenticatedPerc < oSa.RequiredAuthenticatedPerc && OTPMatch)
                        {
                            oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.NAInvalidAnswers;
                        }
                        else if (oSa.AuthenticatedPerc >= oSa.RequiredAuthenticatedPerc && !OTPMatch)
                        {
                            oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.NAIncorrectOTP;
                        }
                        else if (oSa.AuthenticatedPerc < oSa.RequiredAuthenticatedPerc && !OTPMatch)
                        {
                            oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.NAInvalidOTPAnswers;
                        }
                    }

                }

                else if (oSubscriberAuthenticationProfile.BlockUser && oSa.AuthenticationStatusInd.ToLower() == "n" && oSa.RetryCount >= oSubscriberAuthenticationProfile.NoOfRetries)
                {

                    // Get the Blocking categories
                    dsBlockSettings = dAuthentication.GetSubscriberBlockingSettings(AuthAdminCon, oSubscriberAuthenticationProfile.SubscriberAuthenticationProfileID, oSa.SubscriberID);

                    // Check if Consumer satisfies any of the enabled categories
                    if (dsBlockSettings.Tables.Count > 0)
                    {
                        foreach (DataRow dr in dsBlockSettings.Tables[0].Rows)
                        {
                            if (dr["Category"].ToString().ToLower() == "failedauthentication")
                            {
                                BlockUser = true;
                                break;
                            }
                        }
                    }

                    oSubscriberAuthenticationProfile.BlockUser = BlockUser;

                    if (BlockUser)
                    {
                        // Block consumer

                        ebConsumer.BlockedByUser = oSa.CreatedByUser;
                        ebConsumer.BlockedOnDate = DateTime.Now.ToString("yyyy/MM/dd");
                        ebConsumer.ConsumerID = oSa.ConsumerID;

                        ebConsumer.DateofBirth = eSC.BirthDate;
                        ebConsumer.FirstName = eSC.FirstName;
                        ebConsumer.IDNo = eSC.IDNo;
                        ebConsumer.IsBlocked = true;
                        ebConsumer.PassportNo = eSC.PassportNo;
                        ebConsumer.ProductID = eSC.ProductID;
                        ebConsumer.SecondName = eSC.SecondName;
                        ebConsumer.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                        ebConsumer.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                        ebConsumer.SubscriberID = oSa.SubscriberID;
                        ebConsumer.SurName = eSC.Surname;
                        ebConsumer.SystemUserID = oSa.ActionedBySystemUserID;

                        if (oSa.AuthenticatedPerc < oSa.RequiredAuthenticatedPerc && OTPMatch)
                        {
                            ebConsumer.BlockingReason = "Failed Authentication as Questions were incorrectly answered";
                            oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.NAInvalidAnswersB;
                        }
                        else if (oSa.AuthenticatedPerc >= oSa.RequiredAuthenticatedPerc && !OTPMatch)
                        {
                            ebConsumer.BlockingReason = "Failed Authentication due to Invalid OTP";
                            oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.NAIncorrectOTPB;
                        }
                        else if (oSa.AuthenticatedPerc < oSa.RequiredAuthenticatedPerc && !OTPMatch)
                        {
                            ebConsumer.BlockingReason = "Failed Authentication: Invalid OTP and Incorrect Answers";
                            oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.NAInvalidOTPAnswersB;
                        }

                        ebConsumer.SubscriberName = eSe.SubscriberName;

                        rp = dAuthentication.BlockUnblockConsumer(AdminConnection, ebConsumer);

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            oAuthenticationDocument.IsUserBlocked = true;
                            oAuthenticationDocument.blockid = rp.ResponseKey;
                            oAuthenticationDocument.BlockingReason = ebConsumer.BlockingReason;
                            oSa.AuthenticationStatusInd = "NB";
                            oSa.Reason = ebConsumer.BlockingReason;
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            throw new Exception(rp.ResponseData);
                        }

                        // Send Email

                        ProcessEmail(AuthAdminCon, "B", "Q", oSa, strsmtpServer, strsmtpUsername, strsmtpPassword, intport, oAuthenticationProcess, 0, eSe);
                    }
                }

                else if (oSa.AuthenticationStatusInd.ToLower() == "a")
                {
                    oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.Authenticated;

                    if (oSubscriberAuthenticationProfile.PersonalQuestionsCheck)
                    {
                        // Get the Personal Questions set for the consumer
                        dsPersonalQuestions = dAuthentication.GetPersonalQuestions(AuthAdminCon, oSa.SubscriberID, oSa.ConsumerID, oSa.SubscriberAuthenticationID, oSa.CreatedByUser);

                        // Populate Authentication Documnet with Personal Questions

                        ArrayList oPersonalQuestionsList = new ArrayList();

                        if (dsPersonalQuestions.Tables.Count > 0)
                        {
                            oAuthenticationDocument.PersonalQuestionsenabled = true;
                            foreach (DataRow dr in dsPersonalQuestions.Tables[0].Rows)
                            {
                                XDSPortalAuthentication.PersonalQuestionsDocument oPQuestionDocument = new XDSPortalAuthentication.PersonalQuestionsDocument();
                                oPQuestionDocument.PQuestionID = long.Parse(dr["AuthenticationPersonalQuestionsID"].ToString());
                                oPQuestionDocument.PQuestion = dr["Question"].ToString();

                                oPersonalQuestionsList.Add(oPQuestionDocument);
                            }

                            oAuthenticationDocument.PersonalQuestions = oPersonalQuestionsList.ToArray(typeof(XDSPortalAuthentication.PersonalQuestionsDocument)) as XDSPortalAuthentication.PersonalQuestionsDocument[];


                        }
                    }
                }
                else if (oSa.AuthenticationStatusInd.ToLower() == "n")
                {
                    if (oSa.AuthenticatedPerc < oSa.RequiredAuthenticatedPerc && OTPMatch)
                    {
                        oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.NAInvalidAnswers;
                        oSa.Reason = "Invalid Answers";
                    }
                    else if (oSa.AuthenticatedPerc >= oSa.RequiredAuthenticatedPerc && !OTPMatch)
                    {
                        oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.NAIncorrectOTP;
                        oSa.Reason = "Invalid OTP";
                    }
                    else if (oSa.AuthenticatedPerc < oSa.RequiredAuthenticatedPerc && !OTPMatch)
                    {
                        oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.NAInvalidOTPAnswers;
                        oSa.Reason = "Invalid Answers and OTP";
                    }
                }

                oSa.EncryptedReferenceNo = dAuthentication.GetEncryptedReferenceNo(oSa, eSC);

                // Update the Encrypted reference no to database
                dAuthentication.UpdateSubscriberAuthentication(AuthAdminCon, oSa);

                oAuthenticationDocument.AuthenticatedPerc = oSa.AuthenticatedPerc;
                oAuthenticationDocument.RequiredAuthenticatedPerc = oSa.RequiredAuthenticatedPerc;
                oAuthenticationDocument.AuthenticationStatusInd = oSa.AuthenticationStatusInd;
                oAuthenticationDocument.EncryptedReferenceNo = oSa.EncryptedReferenceNo;
                oAuthenticationDocument.BlockingEnabledFlag = oSubscriberAuthenticationProfile.BlockUser;
                oAuthenticationDocument.NoofAttemptsRemaining = (oSubscriberAuthenticationProfile.NoOfRetries - oSa.RetryCount);

                oAuthenticationProcess.GetAuthenticationProcessObject(oAuthenticationDocument);


                // Convert the AuthenticationProcess object to XML , so that it can be updated to 
                XmlSerializer serializerout = new XmlSerializer(oAuthenticationProcess.GetType());
                System.IO.StringWriter swout = new System.IO.StringWriter();
                serializerout.Serialize(swout, oAuthenticationProcess);
                System.IO.StringReader readerout = new System.IO.StringReader(swout.ToString());

                eSC.XMLData = readerout.ReadToEnd();

                dSC.UpdateSubscriberEnquiryResult(con, eSC);

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }

            catch (Exception oException)
            {
                eSe.ErrorDescription = oException.Message;
                dSe.UpdateSubscriberEnquiryError(con, eSe);

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

                oAuthenticationDocument.ErrorMessage = oException.Message;

                oAuthenticationProcess.GetAuthenticationProcessObject(oAuthenticationDocument);
                throw new Exception(oException.Message);

            }

            dsAnswers.Dispose();
            dsBlockSettings.Dispose();
            dsPersonalQuestions.Dispose();

            return oAuthenticationProcess;
        }

        public XDSPortalAuthentication.AuthenticationProcess AuthenticationProcessBinary(SqlConnection con, SqlConnection AdminConnection, SqlConnection SMSConnection, XDSPortalAuthentication.AuthenticationProcess oAuthenticationProcess, string strsmtpServer, string strsmtpUsername, string strsmtpPassword, int intport)
        {
            #region Intialize variables

            string strAdminCon = AdminConnection.ConnectionString;
            string strValidationStatus = string.Empty;
            bool OTPMatch = false;
            DataSet dsPersonalQuestions = new DataSet();
            DataSet dsAnswers = new DataSet();
            DataSet dsBlockSettings = new DataSet();
            XDSPortalLibrary.Entity_Layer.SubscriberAuthenticationProfile oSubscriberAuthenticationProfile = new XDSPortalLibrary.Entity_Layer.SubscriberAuthenticationProfile();
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            int SubscriberAuthenticationID = 0;

            XDSPortalLibrary.Entity_Layer.Authentication eAuthentication = new XDSPortalLibrary.Entity_Layer.Authentication();

            Data.Authentication dAuthentication = new XDSPortalEnquiry.Data.Authentication();

            XDSPortalAuthentication.AuthenticationDocument oAuthenticationDocument = oAuthenticationProcess.CurrentObjectState.AuthenticationDocument;
            BlockedConsumers obConsumer = new BlockedConsumers();
            XDSPortalLibrary.Entity_Layer.BlockedConsumers ebConsumer = new XDSPortalLibrary.Entity_Layer.BlockedConsumers();

            XDSPortalLibrary.Entity_Layer.SubscriberAuthentication oSa = new XDSPortalLibrary.Entity_Layer.SubscriberAuthentication();
            xdsBilling xb = new xdsBilling();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();


            #endregion

            try
            {

                oAuthenticationDocument.ErrorMessage = string.Empty;

                if (con.State == ConnectionState.Closed)
                    con.Open();
                if (AdminConnection.State == ConnectionState.Closed)
                    AdminConnection.Open();


                // Get Subscriber Authentication ID

                SubscriberAuthenticationID = int.Parse(oAuthenticationDocument.SubscriberAuthenticationID.ToString());

                // Get the Authentication connection string

                AuthAdminConnection = oXDSSettings.GetConnection(AdminConnection, productID);

                SqlConnection AuthAdminCon = new SqlConnection(AuthAdminConnection);

                // Get the Subscriber Authentication Object

                oSa = dAuthentication.GetSubscriberAuthenticationObject(AuthAdminCon, SubscriberAuthenticationID);

                // Get the Subscriber Enquiry object
                eSe = dSe.GetSubscriberEnquiryObject(con, oSa.EnquiryID);

                Entity.SubscriberProductReports sprSMS = xb.GetPrice(AdminConnection, eSe.SubscriberID, 75);

                //string strSelect = string.Format("Insert AuthLog(AuthObject,SubscriberAuthenticationID) Values ('{0}','{1}')",
                //    oSa.AuthenticationStatusInd + " -- " + oAuthenticationProcess.CurrentObjectState.AuthenticationDocument.AuthenticationStatusInd
                //     + " -- " + oAuthenticationProcess.CurrentObjectState.AuthenticationDocument.AuthenticationStatusReason.ToString()
                //      + " -- " + oAuthenticationProcess.CurrentObjectState.AuthenticationDocument.SubscriberAuthenticationID.ToString(), 
                //    SubscriberAuthenticationID.ToString());
                //SqlCommand authlog = new SqlCommand(strSelect, AdminConnection);

                //if (AdminConnection.State == ConnectionState.Closed)
                //    AdminConnection.Open();

                //authlog.ExecuteNonQuery();

                //if (AdminConnection.State == ConnectionState.Open)
                //    AdminConnection.Close();

                // Check if the Authentication is already Processed
                if (oSa.AuthenticationStatusInd != "P")
                {
                    //throw new Exception("This Authentication request is already Processed");

                    oAuthenticationDocument.EncryptedReferenceNo = oSa.EncryptedReferenceNo;
                    oAuthenticationDocument.RequiredAuthenticatedPerc = oSa.RequiredAuthenticatedPerc;
                    oAuthenticationDocument.AuthenticatedPerc = oSa.AuthenticatedPerc;
                    oAuthenticationDocument.AuthenticationStatusInd = oSa.AuthenticationStatusInd;

                    oAuthenticationProcess.GetAuthenticationProcessObject(oAuthenticationDocument);

                    return oAuthenticationProcess;
                }

                // Check if OTP Matches

                if (sprSMS.ReportID > 0)
                {
                    rp = dAuthentication.LogOTP(AuthAdminCon, oSa, 0, oAuthenticationProcess.OTPValue.ToString(), XDSPortalEnquiry.Data.OTPAction.Evaluate);

                    if (rp.ResponseKey == 0)
                    {
                        OTPMatch = false;
                        //throw new Exception("Invalid OTP supplied");
                    }
                    else
                    {
                        OTPMatch = true;
                    }
                }
                else
                {
                    OTPMatch = true;
                }

                // Check if all the questions were answered

                if (oAuthenticationDocument.Questions != null)
                {
                    // Convert the AuthenticationProcess object to XML , so that it can be updated to 
                    XmlSerializer serializer1 = new XmlSerializer(oAuthenticationProcess.GetType());
                    System.IO.StringWriter sw1 = new System.IO.StringWriter();
                    serializer1.Serialize(sw1, oAuthenticationProcess);
                    System.IO.StringReader reader1 = new System.IO.StringReader(sw1.ToString());


                    SqlCommand ocom1 = new SqlCommand("Insert into AuthLog(AuthObject,SubscriberAuthenticationID) Values (@AuthObject, @SubscriberAuthenticationID)", AdminConnection);
                    ocom1.Parameters.AddWithValue("@AuthObject", reader1.ReadToEnd());
                    ocom1.Parameters.AddWithValue("@SubscriberAuthenticationID", SubscriberAuthenticationID);

                    if (AdminConnection.State == ConnectionState.Closed)
                        AdminConnection.Open();

                    ocom1.ExecuteNonQuery();

                    if (AdminConnection.State == ConnectionState.Open)
                        AdminConnection.Close();


                    foreach (XDSPortalAuthentication.QuestionDocument oQuestionDocument in oAuthenticationDocument.Questions)
                    {
                        bool IsEnteredAnswer = false;

                        foreach (XDSPortalAuthentication.AnswerDocument oAnswerDocument in oQuestionDocument.Answers)
                        {
                            if (oAnswerDocument.IsEnteredAnswerYN)
                            {
                                IsEnteredAnswer = oAnswerDocument.IsEnteredAnswerYN;
                            }
                        }

                        if (!IsEnteredAnswer)
                        {
                            throw new Exception("Please answer all questions before trying to authenticate! ");
                        }
                    }

                    // Read the Answers supplied to Data Set

                    XmlSerializer serializer = new XmlSerializer(oAuthenticationProcess.CurrentObjectState.AuthenticationDocument.Questions.GetType());
                    System.IO.StringWriter sw = new System.IO.StringWriter();
                    serializer.Serialize(sw, oAuthenticationProcess.CurrentObjectState.AuthenticationDocument.Questions);
                    System.IO.StringReader reader = new System.IO.StringReader(sw.ToString());
                    dsAnswers.ReadXml(reader);


                    // Update answers to data base

                    if (dsAnswers.Tables.Count > 0 && dsAnswers.Tables.Contains("AnswerDocument"))
                    {
                        dAuthentication.UpdateSubscriberAuthenticationQuestionAnswer(AuthAdminCon, int.Parse(oAuthenticationProcess.CurrentObjectState.AuthenticationDocument.SubscriberID.ToString()), SubscriberAuthenticationID, 0, dsAnswers.Tables["AnswerDocument"]);
                    }

                    // Validate Answers
                    dAuthentication.ValidateAnswers(AuthAdminCon, oAuthenticationProcess, oSa.CreatedByUser, OTPMatch);


                    // Get the Authentication percentage and Status
                    oSa = dAuthentication.GetSubscriberAuthenticationObject(AuthAdminCon, SubscriberAuthenticationID);

                }
                else
                {
                    throw new Exception("No Questions received to validate");
                }

                // Get the Subscriber Enquiry Result object

                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, oSa.EnquiryResultID);

                // Get the Subscriber Authentication Profile

                oSubscriberAuthenticationProfile = dAuthentication.GetSubscriberAuthenticationProfile(AuthAdminCon, oSa.SubscriberID, "failedauthentication");

                //OTP Retry - V2.2
                if (sprSMS.ReportID > 0 && !OTPMatch)
                {
                    if (oSubscriberAuthenticationProfile.ResendOTP && (oSa.OTPRetryCount < oSubscriberAuthenticationProfile.NoOfOTPRetries))
                    {
                        oSa.OTPRetryCount++;
                        dAuthentication.UpdateSubscriberAuthenticationOTPRetryCount(AuthAdminCon, oSa);

                        throw new Exception("Invalid OTP. Please Retry");
                    }
                }

                // Check if user should be blocked

                if (oSubscriberAuthenticationProfile.BlockUser && oSa.AuthenticationStatusInd.ToLower() == "n" && oSa.RetryCount >= oSubscriberAuthenticationProfile.NoOfRetries)
                {
                    bool BlockUser = false;

                    // Get the Blocking categories
                    dsBlockSettings = dAuthentication.GetSubscriberBlockingSettings(AuthAdminCon, oSubscriberAuthenticationProfile.SubscriberAuthenticationProfileID, oSa.SubscriberID);

                    // Check if Consumer satisfies any of the enabled categories
                    if (dsBlockSettings.Tables.Count > 0)
                    {
                        foreach (DataRow dr in dsBlockSettings.Tables[0].Rows)
                        {
                            if (dr["Category"].ToString().ToLower() == "failedauthentication")
                            {
                                BlockUser = true;
                                break;
                            }
                        }
                    }

                    oSubscriberAuthenticationProfile.BlockUser = BlockUser;

                    if (BlockUser)
                    {
                        // Block consumer

                        ebConsumer.BlockedByUser = oSa.CreatedByUser;
                        ebConsumer.BlockedOnDate = DateTime.Now.ToString("yyyy/MM/dd");
                        ebConsumer.ConsumerID = oSa.ConsumerID;

                        ebConsumer.DateofBirth = eSC.BirthDate;
                        ebConsumer.FirstName = eSC.FirstName;
                        ebConsumer.IDNo = eSC.IDNo;
                        ebConsumer.IsBlocked = true;
                        ebConsumer.PassportNo = eSC.PassportNo;
                        ebConsumer.ProductID = eSC.ProductID;
                        ebConsumer.SecondName = eSC.SecondName;
                        ebConsumer.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                        ebConsumer.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                        ebConsumer.SubscriberID = oSa.SubscriberID;
                        ebConsumer.SurName = eSC.Surname;
                        ebConsumer.SystemUserID = oSa.ActionedBySystemUserID;

                        if (oSa.AuthenticatedPerc < oSa.RequiredAuthenticatedPerc && OTPMatch)
                        {
                            ebConsumer.BlockingReason = "Failed Authentication as Questions were incorrectly answered";
                            oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.NAInvalidAnswersB;
                        }
                        else if (oSa.AuthenticatedPerc >= oSa.RequiredAuthenticatedPerc && !OTPMatch)
                        {
                            ebConsumer.BlockingReason = "Failed Authentication due to Invalid OTP";
                            oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.NAIncorrectOTPB;
                        }
                        else if (oSa.AuthenticatedPerc < oSa.RequiredAuthenticatedPerc && !OTPMatch)
                        {
                            ebConsumer.BlockingReason = "Failed Authentication: Invalid OTP and Incorrect Answers";
                            oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.NAInvalidOTPAnswersB;
                        }

                        ebConsumer.SubscriberName = eSe.SubscriberName;

                        rp = dAuthentication.BlockUnblockConsumer(AdminConnection, ebConsumer);

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            oAuthenticationDocument.IsUserBlocked = true;
                            oAuthenticationDocument.blockid = rp.ResponseKey;
                            oAuthenticationDocument.BlockingReason = ebConsumer.BlockingReason;
                            oSa.AuthenticationStatusInd = "NB";
                            oSa.Reason = ebConsumer.BlockingReason;
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            throw new Exception(rp.ResponseData);
                        }

                        // Send Email

                        ProcessEmail(AuthAdminCon, "B", "Q", oSa, strsmtpServer, strsmtpUsername, strsmtpPassword, intport, oAuthenticationProcess, 0, eSe);
                    }
                    else
                    {
                        if (oSa.AuthenticatedPerc < oSa.RequiredAuthenticatedPerc && OTPMatch)
                        {
                            oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.NAInvalidAnswers;
                        }
                        else if (oSa.AuthenticatedPerc >= oSa.RequiredAuthenticatedPerc && !OTPMatch)
                        {
                            oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.NAIncorrectOTP;
                        }
                        else if (oSa.AuthenticatedPerc < oSa.RequiredAuthenticatedPerc && !OTPMatch)
                        {
                            oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.NAInvalidOTPAnswers;
                        }
                    }

                }
                else if (oSa.AuthenticationStatusInd.ToLower() == "a")
                {
                    oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.Authenticated;

                    if (oSubscriberAuthenticationProfile.PersonalQuestionsCheck)
                    {
                        // Get the Personal Questions set for the consumer
                        dsPersonalQuestions = dAuthentication.GetPersonalQuestions(AuthAdminCon, oSa.SubscriberID, oSa.ConsumerID, oSa.SubscriberAuthenticationID, oSa.CreatedByUser);

                        // Populate Authentication Documnet with Personal Questions

                        ArrayList oPersonalQuestionsList = new ArrayList();

                        if (dsPersonalQuestions.Tables.Count > 0)
                        {
                            oAuthenticationDocument.PersonalQuestionsenabled = true;
                            foreach (DataRow dr in dsPersonalQuestions.Tables[0].Rows)
                            {
                                XDSPortalAuthentication.PersonalQuestionsDocument oPQuestionDocument = new XDSPortalAuthentication.PersonalQuestionsDocument();
                                oPQuestionDocument.PQuestionID = long.Parse(dr["AuthenticationPersonalQuestionsID"].ToString());
                                oPQuestionDocument.PQuestion = dr["Question"].ToString();

                                oPersonalQuestionsList.Add(oPQuestionDocument);
                            }

                            oAuthenticationDocument.PersonalQuestions = oPersonalQuestionsList.ToArray(typeof(XDSPortalAuthentication.PersonalQuestionsDocument)) as XDSPortalAuthentication.PersonalQuestionsDocument[];


                        }
                    }
                }
                else if (oSa.AuthenticationStatusInd.ToLower() == "n")
                {
                    if (oSa.AuthenticatedPerc < oSa.RequiredAuthenticatedPerc && OTPMatch)
                    {
                        oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.NAInvalidAnswers;
                        oSa.Reason = "Invalid Answers";
                    }
                    else if (oSa.AuthenticatedPerc >= oSa.RequiredAuthenticatedPerc && !OTPMatch)
                    {
                        oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.NAIncorrectOTP;
                        oSa.Reason = "Invalid OTP";
                    }
                    else if (oSa.AuthenticatedPerc < oSa.RequiredAuthenticatedPerc && !OTPMatch)
                    {
                        oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.NAInvalidOTPAnswers;
                        oSa.Reason = "Invalid Answers and OTP";
                    }
                }

                oSa.EncryptedReferenceNo = dAuthentication.GetEncryptedReferenceNo(oSa, eSC);

                // Update the Encrypted reference no to database
                dAuthentication.UpdateSubscriberAuthentication(AuthAdminCon, oSa);

                oAuthenticationDocument.AuthenticatedPerc = oSa.AuthenticatedPerc;
                oAuthenticationDocument.AuthenticationStatusInd = oSa.AuthenticationStatusInd;
                oAuthenticationDocument.EncryptedReferenceNo = oSa.EncryptedReferenceNo;
                oAuthenticationDocument.BlockingEnabledFlag = oSubscriberAuthenticationProfile.BlockUser;
                oAuthenticationDocument.NoofAttemptsRemaining = (oSubscriberAuthenticationProfile.NoOfRetries - oSa.RetryCount);

                oAuthenticationProcess.GetAuthenticationProcessObject(oAuthenticationDocument);


                // Convert the AuthenticationProcess object to XML , so that it can be updated to 
                XmlSerializer serializerout = new XmlSerializer(oAuthenticationProcess.GetType());
                System.IO.StringWriter swout = new System.IO.StringWriter();
                serializerout.Serialize(swout, oAuthenticationProcess);
                System.IO.StringReader readerout = new System.IO.StringReader(swout.ToString());

                eSC.XMLData = readerout.ReadToEnd();

                dSC.UpdateSubscriberEnquiryResult(con, eSC);

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }

            catch (Exception oException)
            {
                eSe.ErrorDescription = oException.Message;
                dSe.UpdateSubscriberEnquiryError(con, eSe);

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

                oAuthenticationDocument.ErrorMessage = oException.Message;

                oAuthenticationProcess.GetAuthenticationProcessObject(oAuthenticationDocument);
                throw new Exception(oException.Message);

            }

            dsAnswers.Dispose();
            dsBlockSettings.Dispose();
            dsPersonalQuestions.Dispose();

            return oAuthenticationProcess;
        }

        public XDSPortalLibrary.Entity_Layer.Response SavePersonalQuestions(SqlConnection con, SqlConnection AdminConnection, XDSPortalAuthentication.AuthenticationProcess oAuthenticationProcess)
        {
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                oAuthenticationProcess.SyncAuthenticationDocument();

                rp = AuthenticationProcessSavePersonalQuestions(con, AdminConnection, oAuthenticationProcess);

            }
            catch (Exception oException)
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                throw new Exception(oException.Message);
            }

            return rp;


        }

        public XDSPortalLibrary.Entity_Layer.Response AuthenticationProcessSavePersonalQuestions(SqlConnection con, SqlConnection AdminConnection, XDSPortalAuthentication.AuthenticationProcess oAuthenticationProcess)
        {
            #region Initialize Variables

            string strAdminCon = AdminConnection.ConnectionString;
            string strValidationStatus = string.Empty;
            string VoidReason = string.Empty;
            string result = string.Empty;
            bool IsQuestionAnswered = false;
            DataSet dsPersonalQuestions = new DataSet();
            int NoofQuestions = 0;

            int SubscriberAuthenticationID = 0;


            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();


            XDSPortalLibrary.Entity_Layer.Authentication eAuthentication = new XDSPortalLibrary.Entity_Layer.Authentication();

            Data.Authentication dAuthentication = new XDSPortalEnquiry.Data.Authentication();

            XDSPortalLibrary.Entity_Layer.SubscriberAuthentication oSa = new XDSPortalLibrary.Entity_Layer.SubscriberAuthentication();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();


            #endregion

            try
            {

                // Get the Subscriber Authentication ID
                SubscriberAuthenticationID = int.Parse(oAuthenticationProcess.CurrentObjectState.AuthenticationDocument.SubscriberAuthenticationID.ToString());


                // Get the Authentication Admin Connection string
                AuthAdminConnection = oXDSSettings.GetConnection(AdminConnection, productID);

                SqlConnection AuthAdminCon = new SqlConnection(AuthAdminConnection);

                // Get the Subscriber Authentication Object

                oSa = dAuthentication.GetSubscriberAuthenticationObject(AuthAdminCon, SubscriberAuthenticationID);

                // Get the Subscriber Enquiry object
                eSe = dSe.GetSubscriberEnquiryObject(con, oSa.EnquiryID);

                // Check the answers supplied 

                if (oAuthenticationProcess.CurrentObjectState.AuthenticationDocument.PersonalQuestions != null)
                {
                    foreach (XDSPortalAuthentication.PersonalQuestionsDocument oPQuestionDocument in oAuthenticationProcess.CurrentObjectState.AuthenticationDocument.PersonalQuestions)
                    {
                        NoofQuestions++;
                        if (!string.IsNullOrEmpty(oPQuestionDocument.PAnswer) && oPQuestionDocument.PAnswer.Trim().Length > 1)
                        {
                            IsQuestionAnswered = true;
                        }
                    }
                    if (NoofQuestions == 0)
                    {
                        throw new Exception("No Personal Questions exist for saving");
                    }

                    if (!IsQuestionAnswered)
                    {
                        throw new Exception("At least one question must be answered");
                    }

                    // Read the Personal question Answers to a Data table

                    XmlSerializer serializer = new XmlSerializer(oAuthenticationProcess.CurrentObjectState.AuthenticationDocument.PersonalQuestions.GetType());
                    System.IO.StringWriter sw = new System.IO.StringWriter();
                    serializer.Serialize(sw, oAuthenticationProcess.CurrentObjectState.AuthenticationDocument.PersonalQuestions);
                    System.IO.StringReader reader = new System.IO.StringReader(sw.ToString());
                    dsPersonalQuestions.ReadXml(reader);

                    // Save the personal Question answers to database

                    if (dsPersonalQuestions.Tables.Count > 0 && dsPersonalQuestions.Tables.Contains("PersonalQuestionsDocument"))
                    {
                        dAuthentication.savePersonalQuestions(AuthAdminCon, oSa.SubscriberID, oSa.ConsumerID, SubscriberAuthenticationID, dsPersonalQuestions.Tables["PersonalQuestionsDocument"], oSa.CreatedByUser);
                    }

                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                    rp.ResponseData = "Personal Questions Saved Successfully";
                }
                else
                {
                    throw new Exception("No Personal Questions exist for saving");
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }

            catch (Exception oException)
            {
                eSe.ErrorDescription = oException.Message;
                dSe.UpdateSubscriberEnquiryError(con, eSe);

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

                rp.ResponseData = "<Error>" + oException.Message + "</Error>";
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;

            }

            return rp;
        }

        public XDSPortalAuthentication.AuthenticationProcess VoidAuthentication(SqlConnection con, SqlConnection AdminConnection, XDSPortalAuthentication.AuthenticationProcess oAuthenticationProcess, string Comments, string strsmtpServer, string strsmtpUsername, string strsmtpPassword, int intport)
        {

            try
            {
                oAuthenticationProcess.SyncAuthenticationDocument();
                AuthenticationProcessVoid(con, AdminConnection, oAuthenticationProcess, Comments, strsmtpServer, strsmtpUsername, strsmtpPassword, intport);
            }

            catch (Exception oException)
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
                throw new Exception(oException.Message);
            }


            return oAuthenticationProcess;
        }

        public XDSPortalAuthentication.AuthenticationProcess AuthenticationProcessVoid(SqlConnection con, SqlConnection AdminConnection, XDSPortalAuthentication.AuthenticationProcess oAuthenticationProcess, string Comments, string strsmtpServer, string strsmtpUsername, string strsmtpPassword, int intport)
        {
            #region Intialize Variables

            string strAdminCon = AdminConnection.ConnectionString;
            string strValidationStatus = string.Empty;
            string VoidReason = string.Empty;
            int NoofReasons = 0;

            int SubscriberAuthenticationID = 0;

            XDSPortalLibrary.Entity_Layer.Authentication eAuthentication = new XDSPortalLibrary.Entity_Layer.Authentication();

            Data.Authentication dAuthentication = new XDSPortalEnquiry.Data.Authentication();
            XDSPortalAuthentication.AuthenticationDocument oAuthenticationDocument = oAuthenticationProcess.CurrentObjectState.AuthenticationDocument;

            XDSPortalLibrary.Entity_Layer.SubscriberAuthentication oSa = new XDSPortalLibrary.Entity_Layer.SubscriberAuthentication();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();

            BlockedConsumers obConsumer = new BlockedConsumers();
            XDSPortalLibrary.Entity_Layer.BlockedConsumers ebConsumer = new XDSPortalLibrary.Entity_Layer.BlockedConsumers();

            DataSet dsBlockSettings = new DataSet();
            XDSPortalLibrary.Entity_Layer.SubscriberAuthenticationProfile oSubscriberAuthenticationProfile = new XDSPortalLibrary.Entity_Layer.SubscriberAuthenticationProfile();
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            #endregion

            try
            {
                oAuthenticationDocument.ErrorMessage = string.Empty;

                if (con.State == ConnectionState.Closed)
                    con.Open();
                if (AdminConnection.State == ConnectionState.Closed)
                    AdminConnection.Open();

                // Get Authentication admin Connection string

                AuthAdminConnection = oXDSSettings.GetConnection(AdminConnection, productID);

                SqlConnection AuthAdminCon = new SqlConnection(AuthAdminConnection);

                // Get Subscriber AuthenticationID

                SubscriberAuthenticationID = int.Parse(oAuthenticationProcess.CurrentObjectState.AuthenticationDocument.SubscriberAuthenticationID.ToString());

                // Get Subscriber Authentication object
                oSa = dAuthentication.GetSubscriberAuthenticationObject(AuthAdminCon, SubscriberAuthenticationID);

                // Get the Subscriber Enquiry object
                eSe = dSe.GetSubscriberEnquiryObject(con, oSa.EnquiryID);

                // Check if the Authentication is already Processed
                if (oSa.AuthenticationStatusInd != "P")
                {
                    throw new Exception("This Authentication request is already Processed");
                }

                // Get the Subscriber Enquiry Result object

                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, oSa.EnquiryResultID);

                bool IsReasonSelected = false;

                if (oAuthenticationDocument.VoidReasons != null)
                {
                    // Check if the void Reason is selected
                    foreach (XDSPortalAuthentication.VoidReasonsDocument oVoidReasonDocument in oAuthenticationDocument.VoidReasons)
                    {
                        NoofReasons++;
                        if (oVoidReasonDocument.IsEnteredReasonYN)
                        {
                            IsReasonSelected = oVoidReasonDocument.IsEnteredReasonYN;
                            if (oVoidReasonDocument.VoidReason.ToLower() == "other")
                            {
                                if (string.IsNullOrEmpty(oAuthenticationDocument.VoidorFraudReason))
                                {
                                    throw new Exception("Void Reason is mandatory for the Void Reason Type OTHER");
                                }
                                else
                                {
                                    VoidReason = oAuthenticationDocument.VoidorFraudReason;
                                }
                            }
                            else
                            {
                                VoidReason = oVoidReasonDocument.VoidReason;

                            }

                        }

                    }
                    if (NoofReasons > 0 && !IsReasonSelected)
                    {
                        throw new Exception("Please select a valid Void Reason ");
                    }

                    // Update Void Attempts
                    dAuthentication.UpdateVoidAttempts(AuthAdminCon, oAuthenticationProcess, oSa.CreatedByUser);

                    // Get the Authentication percentage and Status
                    oSa = dAuthentication.GetSubscriberAuthenticationObject(AuthAdminCon, SubscriberAuthenticationID);
                }

                // Get the Subscriber Enquiry Result object

                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, oSa.EnquiryResultID);

                // Get the Subscriber Authentication Profile

                oSubscriberAuthenticationProfile = dAuthentication.GetSubscriberAuthenticationProfile(AuthAdminCon, oSa.SubscriberID, "voidauthentication");

                // Check if user should be blocked

                if (oSubscriberAuthenticationProfile.BlockUser && oSa.RetryCount >= oSubscriberAuthenticationProfile.NoOfRetries)
                {
                    bool BlockUser = false;

                    // Get the Blocking categories
                    dsBlockSettings = dAuthentication.GetSubscriberBlockingSettings(AuthAdminCon, oSubscriberAuthenticationProfile.SubscriberAuthenticationProfileID, oSa.SubscriberID);

                    // Check if Consumer satisfies any of the enabled categories
                    if (dsBlockSettings.Tables.Count > 0)
                    {
                        foreach (DataRow dr in dsBlockSettings.Tables[0].Rows)
                        {
                            if (dr["Category"].ToString().ToLower() == "voidauthentication")
                            {
                                BlockUser = true;
                                break;
                            }
                        }
                    }

                    oSubscriberAuthenticationProfile.BlockUser = BlockUser;

                    if (BlockUser)
                    {
                        // Block consumer

                        ebConsumer.BlockedByUser = oSa.CreatedByUser;
                        ebConsumer.BlockedOnDate = DateTime.Now.ToString("yyyy/MM/dd");
                        ebConsumer.ConsumerID = oSa.ConsumerID;

                        ebConsumer.DateofBirth = eSC.BirthDate;
                        ebConsumer.FirstName = eSC.FirstName;
                        ebConsumer.IDNo = eSC.IDNo;
                        ebConsumer.IsBlocked = true;
                        ebConsumer.PassportNo = eSC.PassportNo;
                        ebConsumer.ProductID = eSC.ProductID;
                        ebConsumer.SecondName = eSC.SecondName;
                        ebConsumer.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                        ebConsumer.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                        ebConsumer.SubscriberID = oSa.SubscriberID;
                        ebConsumer.SurName = eSC.Surname;
                        ebConsumer.SystemUserID = oSa.ActionedBySystemUserID;

                        ebConsumer.BlockingReason = "Number Of Void Attempts Exceeded";
                        oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.VoidBlocked;

                        ebConsumer.SubscriberName = eSe.SubscriberName;

                        rp = dAuthentication.BlockUnblockConsumer(AdminConnection, ebConsumer);

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            oAuthenticationDocument.IsUserBlocked = true;
                            oAuthenticationDocument.blockid = rp.ResponseKey;
                            oAuthenticationDocument.BlockingReason = ebConsumer.BlockingReason;
                            oAuthenticationDocument.AuthenticationStatusInd = "VB";
                            oSa.AuthenticationStatusInd = "VB";
                            oSa.Reason = VoidReason;
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            throw new Exception(rp.ResponseData);
                        }

                        // Send Email

                        ProcessEmail(AuthAdminCon, "B", "Q", oSa, strsmtpServer, strsmtpUsername, strsmtpPassword, intport, oAuthenticationProcess, 0, eSe);
                    }
                    else
                    {
                        oSa.AuthenticationStatusInd = "V";
                        oAuthenticationDocument.AuthenticationStatusInd = "V";
                        oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.Void;
                        // Send email
                        ProcessEmail(AuthAdminCon, "V", "Q", oSa, strsmtpServer, strsmtpUsername, strsmtpPassword, intport, oAuthenticationProcess, 0, eSe);
                    }
                }
                else
                {
                    oSa.AuthenticationStatusInd = "V";
                    oAuthenticationDocument.AuthenticationStatusInd = "V";
                    oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.Void;
                    // Send email
                    ProcessEmail(AuthAdminCon, "V", "Q", oSa, strsmtpServer, strsmtpUsername, strsmtpPassword, intport, oAuthenticationProcess, 0, eSe);
                }

                oSa.AuthenticationComment = Comments;
                oSa.Reason = VoidReason;
                oSa.EncryptedReferenceNo = dAuthentication.GetEncryptedReferenceNo(oSa, eSC);

                // Update the Void Status and ReferenceNo to database
                dAuthentication.UpdateSubscriberAuthentication(AuthAdminCon, oSa);

                oAuthenticationDocument.AuthenticationStatusInd = oSa.AuthenticationStatusInd;
                oAuthenticationDocument.EncryptedReferenceNo = oSa.EncryptedReferenceNo;
                oAuthenticationDocument.BlockingEnabledFlag = oSubscriberAuthenticationProfile.BlockUser;
                oAuthenticationDocument.NoofAttemptsRemaining = (oSubscriberAuthenticationProfile.NoOfRetries - oSa.RetryCount);

                // Populate the Authentication Status in Authentication Document
                oAuthenticationProcess.GetAuthenticationProcessObject(oAuthenticationDocument);

                // Convert the AuthenticationProcess object to XML , so that it can be updated to 
                XmlSerializer serializerout = new XmlSerializer(oAuthenticationProcess.GetType());
                System.IO.StringWriter swout = new System.IO.StringWriter();
                serializerout.Serialize(swout, oAuthenticationProcess);
                System.IO.StringReader readerout = new System.IO.StringReader(swout.ToString());

                eSC.XMLData = readerout.ReadToEnd();

                dSC.UpdateSubscriberEnquiryResult(con, eSC);

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            catch (Exception oException)
            {
                eSe.ErrorDescription = oException.Message;
                dSe.UpdateSubscriberEnquiryError(con, eSe);

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

                oAuthenticationDocument.ErrorMessage = oException.Message;
                oAuthenticationProcess.GetAuthenticationProcessObject(oAuthenticationDocument);

                throw new Exception(oException.Message);
            }

            return oAuthenticationProcess;
        }

        public void ProcessEmailOTP(SqlConnection AdminCon, string EmailStatus, XDSPortalLibrary.Entity_Layer.SubscriberAuthentication oSa, string strsmtpServer, string strsmtpUsername, string strsmtpPassword, int intport, XDSPortalAuthentication.AuthenticationProcess oAuthenticationProcess, Entity.SubscriberEnquiry eSe, string strSubject, string strMessage)
        {
            DataSet dsEmail = new DataSet();
            Data.Email dEmail = new Data.Email();
            XDSPortalLibrary.Entity_Layer.Email eEmail = new XDSPortalLibrary.Entity_Layer.Email();

            // Get the Email settings if enabled
            dsEmail = dEmail.GetEmailSettings(AdminCon, oSa.SubscriberID, "", oSa.ProductID, 6);

            if (dsEmail.Tables.Count > 0)
            {
                if (dsEmail.Tables[0].Rows.Count > 0)
                {
                    eEmail.BCCEmailaddress = "";
                    eEmail.CCEmailAddress = "";
                    eEmail.ToEmailAddress = oSa.EmailAddress;
                    eEmail.TemplatePath = dsEmail.Tables[0].Rows[0]["TemplatePath"].ToString();
                    eEmail.EmailStatus = EmailStatus;
                    eEmail.FromEmailAddress = dsEmail.Tables[0].Rows[0]["FromEmailAddress"].ToString();
                    eEmail.FromName = dsEmail.Tables[0].Rows[0]["FromName"].ToString();
                    eEmail.IsBodyhtml = bool.Parse(dsEmail.Tables[0].Rows[0]["Isbodyhtml"].ToString());
                    eEmail.Subject = dsEmail.Tables[0].Rows[0]["Subject"].ToString();
                    eEmail.SubscriberEmailSettingsID = 0;
                    eEmail.SubsriberEnquiryResultID = oSa.EnquiryResultID;
                    eEmail.MessageBody = dsEmail.Tables[0].Rows[0]["MessageBody"].ToString();
                    eEmail.IsBodyTemplate = bool.Parse(dsEmail.Tables[0].Rows[0]["IsBodyTemplate"].ToString());
                    eEmail.ChangedByUser = oSa.ChangedByUser;
                    eEmail.ChangedonDate = DateTime.Now;
                    eEmail.SMTPServerName = strsmtpServer;
                    eEmail.SMTPUsername = strsmtpUsername;
                    eEmail.SMTPPassword = strsmtpPassword;
                    eEmail.Port = intport;

                    string DirPath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                    if (eEmail.IsBodyhtml && eEmail.IsBodyTemplate)
                    {
                        eEmail.IncludeAttachment = false;
                        eEmail.NoOfAttachments = 0;

                        System.Collections.Hashtable mailtemplate = new System.Collections.Hashtable();
                        TemplateParser.Parser tparser = null;
                        mailtemplate.Add("Message", strMessage);
                        tparser = new TemplateParser.Parser((DirPath + eEmail.TemplatePath), mailtemplate);

                        eEmail.BodyStream = Encoding.ASCII.GetBytes(tparser.Parse());
                        eEmail.MessageBody = tparser.Parse();
                    }
                    eEmail.EmailLogID = dEmail.InsertEmailLog(AdminCon, eEmail);
                    dEmail.ProcessEmail(AdminCon, eEmail);
                }
            }
        }

        public void ProcessEmail(SqlConnection AdminCon, string CategoryInd, string EmailStatus, XDSPortalLibrary.Entity_Layer.SubscriberAuthentication oSa, string strsmtpServer, string strsmtpUsername, string strsmtpPassword, int intport, XDSPortalAuthentication.AuthenticationProcess oAuthenticationProcess, int EmailCategoryID, Entity.SubscriberEnquiry eSe)
        {
            DataSet dsEmail = new DataSet();
            Data.Email dEmail = new Data.Email();
            XDSPortalLibrary.Entity_Layer.Email eEmail = new XDSPortalLibrary.Entity_Layer.Email();

            // Get the Email settings if enabled
            dsEmail = dEmail.GetEmailSettings(AdminCon, oSa.SubscriberID, CategoryInd, oSa.ProductID, EmailCategoryID);

            if (dsEmail.Tables.Count > 0)
            {
                if (dsEmail.Tables[0].Rows.Count > 0)
                {
                    eEmail.BCCEmailaddress = dsEmail.Tables[0].Rows[0]["BCCEmailaddress"].ToString();
                    eEmail.CCEmailAddress = dsEmail.Tables[0].Rows[0]["CCEmailaddress"].ToString();
                    eEmail.ToEmailAddress = dsEmail.Tables[0].Rows[0]["ToEmailaddress"].ToString();
                    eEmail.TemplatePath = dsEmail.Tables[0].Rows[0]["TemplatePath"].ToString();
                    eEmail.EmailStatus = EmailStatus;
                    eEmail.FromEmailAddress = dsEmail.Tables[0].Rows[0]["FromEmailAddress"].ToString();
                    eEmail.FromName = dsEmail.Tables[0].Rows[0]["FromName"].ToString();
                    eEmail.IsBodyhtml = bool.Parse(dsEmail.Tables[0].Rows[0]["Isbodyhtml"].ToString());
                    eEmail.Subject = dsEmail.Tables[0].Rows[0]["Subject"].ToString();
                    eEmail.SubscriberEmailSettingsID = int.Parse(dsEmail.Tables[0].Rows[0]["SubscriberEmailSettingsID"].ToString());
                    eEmail.SubsriberEnquiryResultID = oSa.EnquiryResultID;
                    eEmail.MessageBody = dsEmail.Tables[0].Rows[0]["MessageBody"].ToString();
                    eEmail.IsBodyTemplate = bool.Parse(dsEmail.Tables[0].Rows[0]["IsBodyTemplate"].ToString());
                    eEmail.ChangedByUser = oSa.ChangedByUser;
                    eEmail.ChangedonDate = DateTime.Now;
                    eEmail.SMTPServerName = strsmtpServer;
                    eEmail.SMTPUsername = strsmtpUsername;
                    eEmail.SMTPPassword = strsmtpPassword;
                    eEmail.Port = intport;

                    EmailCategoryID = int.Parse(dsEmail.Tables[0].Rows[0]["EmailCategoryID"].ToString());
                    string DirPath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;

                    // Blocking email template
                    if (eEmail.IsBodyhtml && eEmail.IsBodyTemplate)
                    {
                        if (EmailCategoryID == 1)//(CategoryInd.ToLower() == "b")
                        {
                            eEmail.IncludeAttachment = false;
                            eEmail.NoOfAttachments = 0;
                            System.Collections.Hashtable mailtemplate = new System.Collections.Hashtable();
                            TemplateParser.Parser tparser = null;
                            mailtemplate.Add("IDNumber", string.IsNullOrEmpty(eSe.IDNo) ? eSe.PassportNo : eSe.IDNo);
                            mailtemplate.Add("Reference", oSa.ReferenceNo);
                            mailtemplate.Add("Reason", oAuthenticationProcess.BlockingReason);
                            tparser = new TemplateParser.Parser((DirPath + eEmail.TemplatePath), mailtemplate);

                            eEmail.BodyStream = Encoding.ASCII.GetBytes(tparser.Parse());
                            eEmail.MessageBody = tparser.Parse();
                        }
                        // Void email Template
                        else if (EmailCategoryID == 2)// (CategoryInd.ToLower() == "v")
                        {
                            eEmail.IncludeAttachment = false;
                            eEmail.NoOfAttachments = 0;
                            System.Collections.Hashtable mailtemplate = new System.Collections.Hashtable();
                            TemplateParser.Parser tparser = null;
                            mailtemplate.Add("IDNumber", string.IsNullOrEmpty(eSe.IDNo) ? eSe.PassportNo : eSe.IDNo);
                            tparser = new TemplateParser.Parser((DirPath + eEmail.TemplatePath), mailtemplate);

                            eEmail.BodyStream = Encoding.ASCII.GetBytes(tparser.Parse());
                            eEmail.MessageBody = tparser.Parse();
                        }
                        else if (EmailCategoryID == 3)
                        {
                            eEmail.IncludeAttachment = false;
                            eEmail.NoOfAttachments = 0;
                            System.Collections.Hashtable mailtemplate = new System.Collections.Hashtable();
                            TemplateParser.Parser tparser = null;
                            mailtemplate.Add("IDNumber", string.IsNullOrEmpty(eSe.IDNo) ? eSe.PassportNo : eSe.IDNo);
                            mailtemplate.Add("Reference", oSa.ReferenceNo);
                            mailtemplate.Add("Reason", oSa.Reason);
                            tparser = new TemplateParser.Parser((DirPath + eEmail.TemplatePath), mailtemplate);

                            eEmail.BodyStream = Encoding.ASCII.GetBytes(tparser.Parse());
                            eEmail.MessageBody = tparser.Parse();
                        }

                        else if (EmailCategoryID == 4)
                        {
                            eEmail.IncludeAttachment = false;
                            eEmail.NoOfAttachments = 0;
                            System.Collections.Hashtable mailtemplate = new System.Collections.Hashtable();
                            TemplateParser.Parser tparser = null;
                            mailtemplate.Add("IDNumber", string.IsNullOrEmpty(eSe.IDNo) ? eSe.PassportNo : eSe.IDNo);
                            mailtemplate.Add("Reference", oSa.ReferenceNo);
                            mailtemplate.Add("Reason", oSa.Reason);
                            tparser = new TemplateParser.Parser((DirPath + eEmail.TemplatePath), mailtemplate);

                            eEmail.BodyStream = Encoding.ASCII.GetBytes(tparser.Parse());
                            eEmail.MessageBody = tparser.Parse();
                        }
                    }

                    eEmail.EmailLogID = dEmail.InsertEmailLog(AdminCon, eEmail);
                    dEmail.ProcessEmail(AdminCon, eEmail);
                }
            }
        }

        public XDSPortalLibrary.Entity_Layer.Response GetBlockedConsumers(SqlConnection AdminCon, int SystemUserID)
        {
            #region Intialize Variables
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            Data.Authentication dAuthentication = new XDSPortalEnquiry.Data.Authentication();

            XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
            XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(AdminCon, SystemUserID);

            #endregion
            try
            {

                if (dAuthentication.GetUnblockUser(AdminCon, SystemUserID))
                {
                    rp = dAuthentication.GetBlockedConsumers(AdminCon, oSystemUser.SubscriberID);

                }
                else
                {
                    throw new Exception("User doesn't have access to get this information");
                }
            }
            catch (Exception ex)
            {
                rp.ResponseData = "<Error>" + ex.Message + "</Error>";
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            }

            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response UnblockConsumer(SqlConnection AdminCon, int BlockID, int SystemUserID)
        {
            #region Intialize Variables
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            XDSPortalLibrary.Entity_Layer.BlockedConsumers obConsumer = new XDSPortalLibrary.Entity_Layer.BlockedConsumers();
            Data.Authentication dAuthentication = new XDSPortalEnquiry.Data.Authentication();

            XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
            XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(AdminCon, SystemUserID);

            #endregion
            try
            {
                if (dAuthentication.GetUnblockUser(AdminCon, SystemUserID))
                {
                    obConsumer.BlockID = BlockID;
                    obConsumer.UnblockedByUser = oSystemUser.Username;
                    obConsumer.UnblockedOnDate = DateTime.Now.ToString("yyyy/MM/dd");

                    rp = dAuthentication.BlockUnblockConsumer(AdminCon, obConsumer);

                    if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        rp.ResponseData = "Consumer Unblocked Successfully";
                    else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        rp.ResponseData = "<Error>" + rp.ResponseData + "</Error>";
                }
                else
                {
                    throw new Exception("User doesn't have access to unblocking functionality");
                }
            }
            catch (Exception ex)
            {
                rp.ResponseData = "<Error>" + ex.Message + "</Error>";
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            }
            return rp;

        }

        public XDSPortalAuthentication.AuthenticationProcess ReferToFraud(SqlConnection con, SqlConnection AdminConnection, XDSPortalAuthentication.AuthenticationProcess oAuthenticationProcess, string Comments, string strsmtpServer, string strsmtpUsername, string strsmtpPassword, int intport)
        {

            try
            {
                oAuthenticationProcess.SyncAuthenticationDocument();
                AuthenticationProcessReferToFraud(con, AdminConnection, oAuthenticationProcess, Comments, strsmtpServer, strsmtpUsername, strsmtpPassword, intport);
            }

            catch (Exception oException)
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

                throw new Exception(oException.Message);

            }

            return oAuthenticationProcess;
        }

        public XDSPortalAuthentication.AuthenticationProcess AuthenticationProcessReferToFraud(SqlConnection con, SqlConnection AdminConnection, XDSPortalAuthentication.AuthenticationProcess oAuthenticationProcess, string Comments, string strsmtpServer, string strsmtpUsername, string strsmtpPassword, int intport)
        {
            #region intialize Variables
            string strAdminCon = AdminConnection.ConnectionString;
            string strValidationStatus = string.Empty;
            DataSet dsPersonalQuestions = new DataSet();
            DataSet dsAnswers = new DataSet();
            bool IsReasonSelected = false;
            string FraudReason = string.Empty;
            int NoofReasons = 0;

            XDSPortalLibrary.Entity_Layer.SubscriberAuthenticationProfile oSubscriberAuthenticationProfile = new XDSPortalLibrary.Entity_Layer.SubscriberAuthenticationProfile();
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            int SubscriberAuthenticationID = 0;

            XDSPortalLibrary.Entity_Layer.Authentication eAuthentication = new XDSPortalLibrary.Entity_Layer.Authentication();

            Data.Authentication dAuthentication = new XDSPortalEnquiry.Data.Authentication();

            XDSPortalAuthentication.AuthenticationDocument oAuthenticationDocument = oAuthenticationProcess.CurrentObjectState.AuthenticationDocument;
            XDSPortalLibrary.Entity_Layer.BlockedConsumers ebConsumer = new XDSPortalLibrary.Entity_Layer.BlockedConsumers();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            XDSPortalLibrary.Entity_Layer.SubscriberAuthentication oSa = new XDSPortalLibrary.Entity_Layer.SubscriberAuthentication();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();


            #endregion

            try
            {
                oAuthenticationDocument.ErrorMessage = string.Empty;
                if (con.State == ConnectionState.Closed)
                    con.Open();
                if (AdminConnection.State == ConnectionState.Closed)
                    AdminConnection.Open();

                // Get the Authentication Admin Connection string

                AuthAdminConnection = oXDSSettings.GetConnection(AdminConnection, productID);

                SqlConnection AuthAdminCon = new SqlConnection(AuthAdminConnection);

                // Get Subscriber Authentication ID

                SubscriberAuthenticationID = int.Parse(oAuthenticationDocument.SubscriberAuthenticationID.ToString());

                // Get Subscriber Authentication object
                oSa = dAuthentication.GetSubscriberAuthenticationObject(AuthAdminCon, SubscriberAuthenticationID);

                // Check if the Authentication is already Processed
                if (oSa.AuthenticationStatusInd != "P")
                {
                    throw new Exception("This Authentication request is already Processed");
                }

                // Get the Subscriber Profile settings
                oSubscriberAuthenticationProfile = dAuthentication.GetSubscriberAuthenticationProfile(AuthAdminCon, oSa.SubscriberID, "safps");

                // Get the Subscriber Enquiry object
                eSe = dSe.GetSubscriberEnquiryObject(con, oSa.EnquiryID);


                if (oSubscriberAuthenticationProfile.ReferToFraud)
                {

                    if (oAuthenticationDocument.FraudReasons != null)
                    {
                        // Check if the Fraud Reason is selected
                        foreach (XDSPortalAuthentication.FraudReasonsDocument oFraudReasonDocument in oAuthenticationDocument.FraudReasons)
                        {
                            NoofReasons++;
                            if (oFraudReasonDocument.IsEnteredReasonYN)
                            {
                                IsReasonSelected = oFraudReasonDocument.IsEnteredReasonYN;
                                if (oFraudReasonDocument.FraudReason.ToLower() == "other")
                                {
                                    if (string.IsNullOrEmpty(oAuthenticationDocument.VoidorFraudReason))
                                    {
                                        throw new Exception("Fraud Reason is mandatory for the Fraud Reason Type OTHER");
                                    }
                                    else
                                    {
                                        FraudReason = oAuthenticationDocument.VoidorFraudReason;
                                    }
                                }
                                else
                                {
                                    FraudReason = oFraudReasonDocument.FraudReason;

                                }
                            }

                        }
                        if (NoofReasons > 0 && !IsReasonSelected)
                        {
                            throw new Exception("Please select a valid Fraud Reason ");
                        }
                    }

                    // Get Subscriber Enquiry Result object
                    eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, oSa.EnquiryResultID);

                    oSa.AuthenticationComment = Comments;
                    oSa.Reason = FraudReason;
                    oSa.AuthenticationStatusInd = "F";
                    oSa.EncryptedReferenceNo = dAuthentication.GetEncryptedReferenceNo(oSa, eSC);

                    // Update the Fraud Status to database
                    dAuthentication.UpdateSubscriberAuthentication(AuthAdminCon, oSa);

                    ebConsumer.BlockedByUser = eSC.CreatedByUser;
                    ebConsumer.BlockedOnDate = DateTime.Now.ToString("yyyy/MM/dd");
                    ebConsumer.ConsumerID = eSC.KeyID;
                    ebConsumer.DateofBirth = eSC.BirthDate;
                    ebConsumer.FirstName = eSC.FirstName;
                    ebConsumer.IDNo = eSC.IDNo;
                    ebConsumer.IsBlocked = true;
                    ebConsumer.PassportNo = eSC.PassportNo;
                    ebConsumer.ProductID = eSC.ProductID;
                    ebConsumer.SecondName = eSC.SecondName;
                    ebConsumer.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                    ebConsumer.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                    ebConsumer.SubscriberID = eSe.SubscriberID;
                    ebConsumer.SurName = eSC.Surname;
                    ebConsumer.SystemUserID = eSe.SystemUserID;
                    ebConsumer.BlockingReason = "Referred To Fraud";
                    ebConsumer.SubscriberName = eSe.SubscriberName;

                    rp = dAuthentication.BlockUnblockConsumer(AdminConnection, ebConsumer);

                    if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                    {
                        oAuthenticationDocument.IsUserBlocked = true;
                        oAuthenticationDocument.blockid = rp.ResponseKey;
                        oAuthenticationDocument.BlockingReason = ebConsumer.BlockingReason;
                    }
                    else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                    {
                        throw new Exception(rp.ResponseData);
                    }

                    oAuthenticationDocument.AuthenticationStatusInd = "F";
                    oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.ReferredToFraudBlocked;

                    oAuthenticationProcess.GetAuthenticationProcessObject(oAuthenticationDocument);

                    // Convert the AuthenticationProcess object to XML , so that it can be updated to 
                    XmlSerializer serializerout = new XmlSerializer(oAuthenticationProcess.GetType());
                    System.IO.StringWriter swout = new System.IO.StringWriter();
                    serializerout.Serialize(swout, oAuthenticationProcess);
                    System.IO.StringReader readerout = new System.IO.StringReader(swout.ToString());

                    eSC.XMLData = readerout.ReadToEnd();

                    dSC.UpdateSubscriberEnquiryResult(con, eSC);

                    // Send Email
                    ProcessEmail(AuthAdminCon, "F", "Q", oSa, strsmtpServer, strsmtpUsername, strsmtpPassword, intport, oAuthenticationProcess, 0, eSe);


                }
                else
                {
                    throw new Exception("This functionality is not enabled on your profile");
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }

            catch (Exception oException)
            {
                eSe.ErrorDescription = oException.Message;
                dSe.UpdateSubscriberEnquiryError(con, eSe);

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

                oAuthenticationDocument.ErrorMessage = oException.Message;
                oAuthenticationProcess.GetAuthenticationProcessObject(oAuthenticationDocument);

                throw new Exception(oException.Message);

            }


            return oAuthenticationProcess;
        }

        public XDSPortalAuthentication.AuthenticationProcess SubmitAuthenticationConsumerDirect(SqlConnection con, SqlConnection AdminConnection, SqlConnection SMSConnection, XDSPortalAuthentication.AuthenticationProcess oAuthenticationProcess, string strsmtpServer, string strsmtpUsername, string strsmtpPassword, int intport)
        {

            try
            {
                oAuthenticationProcess.SyncAuthenticationDocument();

                AuthenticationProcessConsumerDirect(con, AdminConnection, SMSConnection, oAuthenticationProcess, strsmtpServer, strsmtpUsername, strsmtpPassword, intport);

            }

            catch (Exception oException)
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

                throw new Exception(oException.Message);

            }


            return oAuthenticationProcess;
        }

        public XDSPortalAuthentication.AuthenticationProcess AuthenticationProcessConsumerDirect(SqlConnection con, SqlConnection AdminConnection, SqlConnection SMSConnection, XDSPortalAuthentication.AuthenticationProcess oAuthenticationProcess, string strsmtpServer, string strsmtpUsername, string strsmtpPassword, int intport)
        {
            #region Intialize variables

            string strAdminCon = AdminConnection.ConnectionString;
            string strValidationStatus = string.Empty;
            bool OTPMatch = false;
            DataSet dsPersonalQuestions = new DataSet();
            DataSet dsAnswers = new DataSet();
            DataSet dsBlockSettings = new DataSet();
            XDSPortalLibrary.Entity_Layer.SubscriberAuthenticationProfile oSubscriberAuthenticationProfile = new XDSPortalLibrary.Entity_Layer.SubscriberAuthenticationProfile();
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            int SubscriberAuthenticationID = 0;

            XDSPortalLibrary.Entity_Layer.Authentication eAuthentication = new XDSPortalLibrary.Entity_Layer.Authentication();

            Data.Authentication dAuthentication = new XDSPortalEnquiry.Data.Authentication();

            XDSPortalAuthentication.AuthenticationDocument oAuthenticationDocument = oAuthenticationProcess.CurrentObjectState.AuthenticationDocument;
            BlockedConsumers obConsumer = new BlockedConsumers();
            XDSPortalLibrary.Entity_Layer.BlockedConsumers ebConsumer = new XDSPortalLibrary.Entity_Layer.BlockedConsumers();

            XDSPortalLibrary.Entity_Layer.SubscriberAuthentication oSa = new XDSPortalLibrary.Entity_Layer.SubscriberAuthentication();
            xdsBilling xb = new xdsBilling();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();


            #endregion

            try
            {

                oAuthenticationDocument.ErrorMessage = string.Empty;

                if (con.State == ConnectionState.Closed)
                    con.Open();
                if (AdminConnection.State == ConnectionState.Closed)
                    AdminConnection.Open();


                // Get Subscriber Authentication ID

                SubscriberAuthenticationID = int.Parse(oAuthenticationDocument.SubscriberAuthenticationID.ToString());

                // Get the Authentication connection string

                AuthAdminConnection = oXDSSettings.GetConnection(AdminConnection, productID);

                SqlConnection AuthAdminCon = new SqlConnection(AuthAdminConnection);

                // Get the Subscriber Authentication Object

                oSa = dAuthentication.GetSubscriberAuthenticationObject(AuthAdminCon, SubscriberAuthenticationID);

                // Get the Subscriber Enquiry object
                eSe = dSe.GetSubscriberEnquiryObject(con, oSa.EnquiryID);

                Entity.SubscriberProductReports sprSMS = xb.GetPrice(AdminConnection, eSe.SubscriberID, 75);

                // Check if the Authentication is already Processed
                if (oSa.AuthenticationStatusInd != "P")
                {
                    throw new Exception("This Authentication request is already Processed");
                }

                // Check if OTP Matches

                if (sprSMS.ReportID > 0)
                {
                    rp = dAuthentication.LogOTP(AuthAdminCon, oSa, 0, oAuthenticationProcess.OTPValue.ToString(), XDSPortalEnquiry.Data.OTPAction.Evaluate);

                    if (rp.ResponseKey == 0)
                    {
                        OTPMatch = false;
                        //throw new Exception("Invalid OTP supplied");
                    }
                    else
                    {
                        OTPMatch = true;
                    }
                }
                else
                {
                    OTPMatch = true;
                }

                // Check if all the questions were answered

                if (oAuthenticationDocument.Questions != null)
                {
                    // Convert the AuthenticationProcess object to XML , so that it can be updated to 
                    XmlSerializer serializer1 = new XmlSerializer(oAuthenticationProcess.GetType());
                    System.IO.StringWriter sw1 = new System.IO.StringWriter();
                    serializer1.Serialize(sw1, oAuthenticationProcess);
                    System.IO.StringReader reader1 = new System.IO.StringReader(sw1.ToString());


                    SqlCommand ocom1 = new SqlCommand("Insert into AuthLog(AuthObject) Values (@AuthObject)", AdminConnection);
                    ocom1.Parameters.AddWithValue("@AuthObject", reader1.ReadToEnd());

                    if (AdminConnection.State == ConnectionState.Closed)
                        AdminConnection.Open();

                    ocom1.ExecuteNonQuery();

                    if (AdminConnection.State == ConnectionState.Open)
                        AdminConnection.Close();


                    foreach (XDSPortalAuthentication.QuestionDocument oQuestionDocument in oAuthenticationDocument.Questions)
                    {
                        bool IsEnteredAnswer = false;

                        foreach (XDSPortalAuthentication.AnswerDocument oAnswerDocument in oQuestionDocument.Answers)
                        {
                            if (oAnswerDocument.IsEnteredAnswerYN)
                            {
                                IsEnteredAnswer = oAnswerDocument.IsEnteredAnswerYN;
                            }
                        }

                        if (!IsEnteredAnswer)
                        {
                            throw new Exception("Please answer all questions before trying to authenticate! ");
                        }
                    }

                    // Read the Answers supplied to Data Set

                    XmlSerializer serializer = new XmlSerializer(oAuthenticationProcess.CurrentObjectState.AuthenticationDocument.Questions.GetType());
                    System.IO.StringWriter sw = new System.IO.StringWriter();
                    serializer.Serialize(sw, oAuthenticationProcess.CurrentObjectState.AuthenticationDocument.Questions);
                    System.IO.StringReader reader = new System.IO.StringReader(sw.ToString());
                    dsAnswers.ReadXml(reader);


                    // Update answers to data base

                    if (dsAnswers.Tables.Count > 0 && dsAnswers.Tables.Contains("AnswerDocument"))
                    {
                        dAuthentication.UpdateSubscriberAuthenticationQuestionAnswer(AuthAdminCon, int.Parse(oAuthenticationProcess.CurrentObjectState.AuthenticationDocument.SubscriberID.ToString()), SubscriberAuthenticationID, 0, dsAnswers.Tables["AnswerDocument"]);
                    }

                    // Validate Answers
                    dAuthentication.ValidateAnswers(AuthAdminCon, oAuthenticationProcess, oSa.CreatedByUser, OTPMatch);


                    // Get the Authentication percentage and Status
                    oSa = dAuthentication.GetSubscriberAuthenticationObject(AuthAdminCon, SubscriberAuthenticationID);

                }
                else
                {
                    throw new Exception("No Questions received to validate");
                }

                // Get the Subscriber Enquiry Result object

                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, oSa.EnquiryResultID);

                // Get the Subscriber Authentication Profile

                oSubscriberAuthenticationProfile = dAuthentication.GetSubscriberAuthenticationProfile(AuthAdminCon, oSa.SubscriberID, "failedauthentication");

                // Check if user should be blocked

                if (oSubscriberAuthenticationProfile.BlockUser && oSa.AuthenticationStatusInd.ToLower() == "n" && oSa.RetryCount >= oSubscriberAuthenticationProfile.NoOfRetries)
                {
                    bool BlockUser = false;

                    // Get the Blocking categories
                    dsBlockSettings = dAuthentication.GetSubscriberBlockingSettings(AuthAdminCon, oSubscriberAuthenticationProfile.SubscriberAuthenticationProfileID, oSa.SubscriberID);

                    // Check if Consumer satisfies any of the enabled categories
                    if (dsBlockSettings.Tables.Count > 0)
                    {
                        foreach (DataRow dr in dsBlockSettings.Tables[0].Rows)
                        {
                            if (dr["Category"].ToString().ToLower() == "failedauthentication")
                            {
                                BlockUser = true;
                                break;
                            }
                        }
                    }

                    oSubscriberAuthenticationProfile.BlockUser = BlockUser;

                    if (BlockUser)
                    {
                        // Block consumer

                        ebConsumer.BlockedByUser = oSa.CreatedByUser;
                        ebConsumer.BlockedOnDate = DateTime.Now.ToString("yyyy/MM/dd");
                        ebConsumer.ConsumerID = oSa.ConsumerID;

                        ebConsumer.DateofBirth = eSC.BirthDate;
                        ebConsumer.FirstName = eSC.FirstName;
                        ebConsumer.IDNo = eSC.IDNo;
                        ebConsumer.IsBlocked = true;
                        ebConsumer.PassportNo = eSC.PassportNo;
                        ebConsumer.ProductID = eSC.ProductID;
                        ebConsumer.SecondName = eSC.SecondName;
                        ebConsumer.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                        ebConsumer.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                        ebConsumer.SubscriberID = oSa.SubscriberID;
                        ebConsumer.SurName = eSC.Surname;
                        ebConsumer.SystemUserID = oSa.ActionedBySystemUserID;

                        if (oSa.AuthenticatedPerc < oSa.RequiredAuthenticatedPerc && OTPMatch)
                        {
                            ebConsumer.BlockingReason = "Failed Authentication as Questions were incorrectly answered";
                            oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.NAInvalidAnswersB;
                        }
                        else if (oSa.AuthenticatedPerc >= oSa.RequiredAuthenticatedPerc && !OTPMatch)
                        {
                            ebConsumer.BlockingReason = "Failed Authentication due to Invalid OTP";
                            oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.NAIncorrectOTPB;
                        }
                        else if (oSa.AuthenticatedPerc < oSa.RequiredAuthenticatedPerc && !OTPMatch)
                        {
                            ebConsumer.BlockingReason = "Failed Authentication: Invalid OTP and Incorrect Answers";
                            oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.NAInvalidOTPAnswersB;
                        }

                        ebConsumer.SubscriberName = eSe.SubscriberName;

                        rp = dAuthentication.BlockUnblockConsumer(AdminConnection, ebConsumer);

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            oAuthenticationDocument.IsUserBlocked = true;
                            oAuthenticationDocument.blockid = rp.ResponseKey;
                            oAuthenticationDocument.BlockingReason = ebConsumer.BlockingReason;
                            oSa.AuthenticationStatusInd = "NB";
                            oSa.Reason = ebConsumer.BlockingReason;
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            throw new Exception(rp.ResponseData);
                        }

                        // Send Email

                        ProcessEmail(AuthAdminCon, "B", "Q", oSa, strsmtpServer, strsmtpUsername, strsmtpPassword, intport, oAuthenticationProcess, 0, eSe);
                    }
                    else
                    {
                        if (oSa.AuthenticatedPerc < oSa.RequiredAuthenticatedPerc && OTPMatch)
                        {
                            oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.NAInvalidAnswers;
                        }
                        else if (oSa.AuthenticatedPerc >= oSa.RequiredAuthenticatedPerc && !OTPMatch)
                        {
                            oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.NAIncorrectOTP;
                        }
                        else if (oSa.AuthenticatedPerc < oSa.RequiredAuthenticatedPerc && !OTPMatch)
                        {
                            oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.NAInvalidOTPAnswers;
                        }
                    }

                }
                else if (oSa.AuthenticationStatusInd.ToLower() == "n")
                {
                    if (oSa.AuthenticatedPerc < oSa.RequiredAuthenticatedPerc && OTPMatch)
                    {
                        oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.NAInvalidAnswers;
                        oSa.Reason = "Invalid Answers";
                    }
                    else if (oSa.AuthenticatedPerc >= oSa.RequiredAuthenticatedPerc && !OTPMatch)
                    {
                        oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.NAIncorrectOTP;
                        oSa.Reason = "Invalid OTP";
                    }
                    else if (oSa.AuthenticatedPerc < oSa.RequiredAuthenticatedPerc && !OTPMatch)
                    {
                        oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.NAInvalidOTPAnswers;
                        oSa.Reason = "Invalid Answers and OTP";
                    }
                }

                if (oSubscriberAuthenticationProfile.PersonalQuestionsCheck)
                {
                    oAuthenticationDocument.AuthenticationStatusReason = XDSPortalAuthentication.AuthenticationDocument.AuthStatusReaon.Authenticated;
                    // Get the Personal Questions set for the consumer
                    dsPersonalQuestions = dAuthentication.GetPersonalQuestions(AuthAdminCon, oSa.SubscriberID, oSa.ConsumerID, oSa.SubscriberAuthenticationID, oSa.CreatedByUser);

                    // Populate Authentication Documnet with Personal Questions

                    ArrayList oPersonalQuestionsList = new ArrayList();

                    if (dsPersonalQuestions.Tables.Count > 0)
                    {
                        oAuthenticationDocument.PersonalQuestionsenabled = true;
                        foreach (DataRow dr in dsPersonalQuestions.Tables[0].Rows)
                        {
                            XDSPortalAuthentication.PersonalQuestionsDocument oPQuestionDocument = new XDSPortalAuthentication.PersonalQuestionsDocument();
                            oPQuestionDocument.PQuestionID = long.Parse(dr["AuthenticationPersonalQuestionsID"].ToString());
                            oPQuestionDocument.PQuestion = dr["Question"].ToString();

                            oPersonalQuestionsList.Add(oPQuestionDocument);
                        }

                        oAuthenticationDocument.PersonalQuestions = oPersonalQuestionsList.ToArray(typeof(XDSPortalAuthentication.PersonalQuestionsDocument)) as XDSPortalAuthentication.PersonalQuestionsDocument[];


                    }
                }

                oSa.EncryptedReferenceNo = dAuthentication.GetEncryptedReferenceNo(oSa, eSC);

                // Update the Encrypted reference no to database
                dAuthentication.UpdateSubscriberAuthentication(AuthAdminCon, oSa);

                oAuthenticationDocument.AuthenticatedPerc = oSa.AuthenticatedPerc;
                oAuthenticationDocument.AuthenticationStatusInd = oSa.AuthenticationStatusInd;
                oAuthenticationDocument.EncryptedReferenceNo = oSa.EncryptedReferenceNo;
                oAuthenticationDocument.BlockingEnabledFlag = oSubscriberAuthenticationProfile.BlockUser;
                oAuthenticationDocument.NoofAttemptsRemaining = (oSubscriberAuthenticationProfile.NoOfRetries - oSa.RetryCount);

                oAuthenticationProcess.GetAuthenticationProcessObject(oAuthenticationDocument);


                // Convert the AuthenticationProcess object to XML , so that it can be updated to 
                XmlSerializer serializerout = new XmlSerializer(oAuthenticationProcess.GetType());
                System.IO.StringWriter swout = new System.IO.StringWriter();
                serializerout.Serialize(swout, oAuthenticationProcess);
                System.IO.StringReader readerout = new System.IO.StringReader(swout.ToString());

                eSC.XMLData = readerout.ReadToEnd();

                dSC.UpdateSubscriberEnquiryResult(con, eSC);

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);


            }

            catch (Exception oException)
            {
                eSe.ErrorDescription = oException.Message;
                dSe.UpdateSubscriberEnquiryError(con, eSe);

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

                oAuthenticationDocument.ErrorMessage = oException.Message;

                oAuthenticationProcess.GetAuthenticationProcessObject(oAuthenticationDocument);
                throw new Exception(oException.Message);

            }

            dsAnswers.Dispose();
            dsBlockSettings.Dispose();
            dsPersonalQuestions.Dispose();

            return oAuthenticationProcess;
        }

        public XDSPortalLibrary.Entity_Layer.Response GetPersonalQuestionAnswers(SqlConnection AdminConnection, int SubscriberID, int ConsumerID)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();

            string strAdminCon = AdminConnection.ConnectionString;
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();
            try
            {
                //Get the Authentication connection string
                AuthAdminConnection = oXDSSettings.GetConnection(AdminConnection, productID);
                SqlConnection AuthAdminCon = new SqlConnection(AuthAdminConnection);

                DataSet dsPersonalQuestions = new DataSet();
                XDSPortalEnquiry.Data.Authentication dAuthentication = new XDSPortalEnquiry.Data.Authentication();

                //Get the Personal Questions set for the consumer
                dsPersonalQuestions = dAuthentication.GetPersonalQuestionsConsumerDirect(AuthAdminCon, SubscriberID, ConsumerID);

                ObjResponse.ResponseKey = 0;
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = dsPersonalQuestions.GetXml();
            }
            catch (Exception ex)
            {
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(AdminConnection);

                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message;
            }

            return ObjResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response SavePersonalQuestionsConsumerDirect(SqlConnection con, SqlConnection AdminConnection, int SubscriberID, int ConsumerID, string sPersonalQuestions, string CreatedByUser)
        {
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            DataSet dsPersonalQuestions = new DataSet();

            try
            {
                if (!String.IsNullOrEmpty(sPersonalQuestions))
                {
                    StringReader srXML = new StringReader(sPersonalQuestions);
                    dsPersonalQuestions.ReadXml(srXML);
                }
                rp = AuthenticationProcessSavePersonalQuestionsConsumerDirect(con, AdminConnection, SubscriberID, ConsumerID, dsPersonalQuestions, CreatedByUser);
            }
            catch (Exception oException)
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                throw new Exception(oException.Message);
            }

            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response AuthenticationProcessSavePersonalQuestionsConsumerDirect(SqlConnection con, SqlConnection AdminConnection, int SubscriberID, int ConsumerID, DataSet dsPersonalQuestions, string CreatedByUser)
        {
            #region Initialize Variables

            string strAdminCon = AdminConnection.ConnectionString;
            string strValidationStatus = string.Empty;
            string VoidReason = string.Empty;
            string result = string.Empty;
            bool IsQuestionAnswered = false;
            int NoofQuestions = 0;

            int SubscriberAuthenticationID = 0;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            XDSPortalLibrary.Entity_Layer.Authentication eAuthentication = new XDSPortalLibrary.Entity_Layer.Authentication();

            Data.Authentication dAuthentication = new XDSPortalEnquiry.Data.Authentication();

            XDSPortalLibrary.Entity_Layer.SubscriberAuthentication oSa = new XDSPortalLibrary.Entity_Layer.SubscriberAuthentication();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            #endregion

            try
            {
                // Get the Authentication Admin Connection string
                AuthAdminConnection = oXDSSettings.GetConnection(AdminConnection, productID);
                SqlConnection AuthAdminCon = new SqlConnection(AuthAdminConnection);

                // Save the personal Question answers to database
                if (dsPersonalQuestions.Tables.Count > 0 && dsPersonalQuestions.Tables.Contains("PersonalQuestionsDocument"))
                {
                    dAuthentication.savePersonalQuestionsConsumerDirect(AuthAdminCon, SubscriberID, ConsumerID, dsPersonalQuestions.Tables["PersonalQuestionsDocument"], CreatedByUser);
                }

                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                rp.ResponseData = "Personal Questions Saved Successfully";

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);
            }
            catch (Exception oException)
            {
                eSe.ErrorDescription = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

                rp.ResponseData = "<Error>" + oException.Message + "</Error>";
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            }

            return rp;
        }



        //V2.2 
        public XDSPortalLibrary.Entity_Layer.Response UnblockConsumerV22(SqlConnection AdminCon, int BlockID, int SystemUserID, string UnblockReason)
        {
            #region Intialize Variables
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            XDSPortalLibrary.Entity_Layer.BlockedConsumers obConsumer = new XDSPortalLibrary.Entity_Layer.BlockedConsumers();
            Data.Authentication dAuthentication = new XDSPortalEnquiry.Data.Authentication();

            XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
            XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(AdminCon, SystemUserID);

            #endregion
            try
            {
                if (!string.IsNullOrEmpty(UnblockReason))
                {
                    if (dAuthentication.GetUnblockUser(AdminCon, SystemUserID))
                    {
                        obConsumer.BlockID = BlockID;
                        obConsumer.UnblockedByUser = oSystemUser.Username;
                        obConsumer.UnblockedOnDate = DateTime.Now.ToString("yyyy/MM/dd");
                        obConsumer.UnblockingReason = UnblockReason;

                        rp = dAuthentication.BlockUnblockConsumerV22(AdminCon, obConsumer);

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            rp.ResponseData = "Consumer Unblocked Successfully";
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            rp.ResponseData = "<Error>" + rp.ResponseData + "</Error>";
                    }
                    else
                    {
                        throw new Exception("User doesn't have access to unblocking functionality");
                    }
                }
                else
                {
                    throw new Exception("Please provide an unblock reason");
                }
            }
            catch (Exception ex)
            {
                rp.ResponseData = "<Error>" + ex.Message + "</Error>";
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            }
            return rp;

        }

        public XDSPortalLibrary.Entity_Layer.Response GetUnblockReasons(SqlConnection AdminConnection, int SubscriberID)
        {
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            Data.Authentication dAuthentication = new XDSPortalEnquiry.Data.Authentication();
            Data.XDSSettings oXDSSettings = new XDSPortalEnquiry.Data.XDSSettings();

            DataSet dsUnblockReasons = new DataSet();
            string AuthAdminConnection = string.Empty;

            try
            {
                AuthAdminConnection = oXDSSettings.GetConnection(AdminConnection, productID);

                SqlConnection AuthAdminCon = new SqlConnection(AuthAdminConnection);

                dsUnblockReasons = dAuthentication.GetSubscriberUnblockReasons(AuthAdminCon, SubscriberID);

                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                rp.ResponseData = dsUnblockReasons.GetXml();
            }
            catch (Exception oException)
            {
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(AdminConnection);

                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                throw new Exception(oException.Message);
            }

            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.ConsumerInfo GetConsumerInfo(SqlConnection AdminConnection, int SubscriberID, long ConsumerID)
        {
            #region Intialize Variables
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            XDSPortalLibrary.Entity_Layer.ConsumerInfo oConsumerInfo = new XDSPortalLibrary.Entity_Layer.ConsumerInfo();
            Data.Authentication dAuthentication = new XDSPortalEnquiry.Data.Authentication();

            XDSPortalLibrary.Entity_Layer.SubscriberAuthenticationProfile oSubscriberAuthenticationProfile = new XDSPortalLibrary.Entity_Layer.SubscriberAuthenticationProfile();
            Data.XDSSettings oXDSSettings = new XDSPortalEnquiry.Data.XDSSettings();

            string AuthAdminConnection = string.Empty;

            DataSet dsConsumerInfo = new DataSet();
            #endregion

            try
            {
                if (ConsumerID < 1)
                {
                    oConsumerInfo.ConsumerID = 0;
                    oConsumerInfo.ErrorMessage = "Please provide a valid Consumer ID";

                    return oConsumerInfo;
                }

                AuthAdminConnection = oXDSSettings.GetConnection(AdminConnection, productID);
                SqlConnection AuthAdminCon = new SqlConnection(AuthAdminConnection);

                // Get the Subscriber Profile settings
                oSubscriberAuthenticationProfile = dAuthentication.GetSubscriberAuthenticationProfile(AuthAdminCon, SubscriberID, "");

                if (oSubscriberAuthenticationProfile.FraudScore)
                {
                    dsConsumerInfo = dAuthentication.GetConsumerInfo(AdminConnection, ConsumerID);

                    if (dsConsumerInfo.Tables.Count > 0 && dsConsumerInfo.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in dsConsumerInfo.Tables[0].Rows)
                        {
                            oConsumerInfo.ConsumerID = ConsumerID;
                            oConsumerInfo.Address1 = dr["Address1"].ToString();
                            oConsumerInfo.Address2 = dr["Address2"].ToString(); ;
                            oConsumerInfo.Address3 = dr["Address3"].ToString(); ;
                            oConsumerInfo.Address4 = dr["Address4"].ToString(); ;
                            oConsumerInfo.PostalCode = dr["PostalCode"].ToString(); ;
                            oConsumerInfo.ErrorMessage = string.Empty;
                        }
                    }
                }
                else
                {
                    oConsumerInfo.ConsumerID = 0;
                    oConsumerInfo.ErrorMessage = "User doesn't have access to Fraud Score functionality";
                }
            }
            catch (Exception ex)
            {
                oConsumerInfo.ConsumerID = 0;
                oConsumerInfo.ErrorMessage = ex.Message;
            }

            return oConsumerInfo;
        }

        public XDSPortalLibrary.Entity_Layer.Response UpdateConsumerInfo(SqlConnection AdminConnection, int SystemUserID, int SubscriberID,
            long ConsumerID, int SubscriberEnquiryID, int SubscriberEnquiryResultID, string Address1, string Address2, string Address3, string Address4, string PostalCode)
        {
            #region Intialize Variables
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            XDSPortalLibrary.Entity_Layer.ConsumerInfo oConsumerInfo = new XDSPortalLibrary.Entity_Layer.ConsumerInfo();

            Data.Authentication dAuthentication = new XDSPortalEnquiry.Data.Authentication();

            XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
            XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(AdminConnection, SystemUserID);

            XDSPortalLibrary.Entity_Layer.SubscriberAuthenticationProfile oSubscriberAuthenticationProfile = new XDSPortalLibrary.Entity_Layer.SubscriberAuthenticationProfile();
            Data.XDSSettings oXDSSettings = new XDSPortalEnquiry.Data.XDSSettings();

            string AuthAdminConnection = string.Empty;

            DataSet dsConsumerInfo = new DataSet();
            #endregion

            try
            {
                if (ConsumerID < 1 ||
                    SubscriberEnquiryID < 1 ||
                    string.IsNullOrEmpty((Address1 + Address2 + Address3 + Address4).Trim()))
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "<Error>" + "ConsumerID, EnquiryID, EnquiryResultID and at least one address are required" + "</Error>";

                    return rp;
                }

                AuthAdminConnection = oXDSSettings.GetConnection(AdminConnection, productID);
                SqlConnection AuthAdminCon = new SqlConnection(AuthAdminConnection);

                // Get the Subscriber Profile settings
                oSubscriberAuthenticationProfile = dAuthentication.GetSubscriberAuthenticationProfile(AuthAdminCon, SubscriberID, "");

                if (oSubscriberAuthenticationProfile.FraudScore)
                {

                    oConsumerInfo.ConsumerID = ConsumerID;
                    oConsumerInfo.SubscriberEnquiryID = SubscriberEnquiryID;
                    oConsumerInfo.SubscriberEnquiryResultID = SubscriberEnquiryResultID;
                    oConsumerInfo.Address1 = Address1;
                    oConsumerInfo.Address2 = Address2;
                    oConsumerInfo.Address3 = Address3;
                    oConsumerInfo.Address4 = Address4;
                    oConsumerInfo.PostalCode = PostalCode;
                    oConsumerInfo.User = oSystemUser.Username;

                    rp = dAuthentication.UpdateConsumerInfo(AdminConnection, oConsumerInfo);

                    if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        rp.ResponseData = "Consumer Unblocked Successfully";
                    else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        rp.ResponseData = "<Error>" + rp.ResponseData + "</Error>";
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "<Error> User doesn't have access to Fraud Score functionality </Error>";
                }
            }
            catch (Exception ex)
            {
                rp.ResponseData = "<Error>" + ex.Message + "</Error>";
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            }
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response GetAuthenticationOTPNumbers(SqlConnection AdminConnection, int SubscriberID, long SubscriberAuthenticationID)
        {
            #region Intialize Variables
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            Data.Authentication dAuthentication = new XDSPortalEnquiry.Data.Authentication();
            XDSPortalLibrary.Entity_Layer.SubscriberAuthentication oSa = new XDSPortalLibrary.Entity_Layer.SubscriberAuthentication();
            XDSPortalLibrary.Entity_Layer.SubscriberAuthenticationProfile oSubscriberAuthenticationProfile = new XDSPortalLibrary.Entity_Layer.SubscriberAuthenticationProfile();
            Data.XDSSettings oXDSSettings = new XDSPortalEnquiry.Data.XDSSettings();
            DataSet dsConsumerInfo = new DataSet();

            string AuthAdminConnection = string.Empty;
            string ProvidedContactNumber = string.Empty;
            string LatestContactNumber = string.Empty;
            #endregion

            try
            {
                AuthAdminConnection = oXDSSettings.GetConnection(AdminConnection, productID);
                SqlConnection AuthAdminCon = new SqlConnection(AuthAdminConnection);

                // Get the Subscriber Profile settings
                oSubscriberAuthenticationProfile = dAuthentication.GetSubscriberAuthenticationProfile(AuthAdminCon, SubscriberID, "");

                // Get SubscriberAuthentication Object
                oSa = dAuthentication.GetSubscriberAuthenticationObject(AuthAdminCon, int.Parse(SubscriberAuthenticationID.ToString()));

                ProvidedContactNumber = oSa.CellularNumber;

                if (!oSubscriberAuthenticationProfile.ExcludeLatestDBcellNo)
                {
                    rp = dAuthentication.GetLatestPhoneNo(AuthAdminCon, oSa);

                    if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        LatestContactNumber = rp.ResponseData;
                }

                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                rp.ResponseData = "<ProvidedContactNumber>" + ProvidedContactNumber + "</ProvidedContactNumber><LatestContactNumber>" + LatestContactNumber + "</LatestContactNumber>";
            }
            catch (Exception ex)
            {
                rp.ResponseData = "<Error>" + ex.Message + "</Error>";
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            }

            return rp;
        }

        public XDSPortalAuthentication.AuthenticationProcess ResendOTP(SqlConnection con, SqlConnection AdminConnection, SqlConnection SMSConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, string strsmtpServer, string strsmtpUsername, string strsmtpPassword, int intport, XDSPortalAuthentication.AuthenticationProcess oAuthenticationProcess)
        {
            #region Initialize Variables

            string strAdminCon = AdminConnection.ConnectionString;
            string strValidationStatus = string.Empty;
            double Totalcost = 0;
            int SMSProductID = 75;
            int SubscriberAuthenticationID = 0;

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            xdsBilling xb = new xdsBilling();
            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            XDSPortalLibrary.Entity_Layer.SubscriberAuthentication oSa = new XDSPortalLibrary.Entity_Layer.SubscriberAuthentication();
            XDSPortalLibrary.Entity_Layer.SubscriberAuthenticationProfile oSubscriberAuthenticationProfile = new XDSPortalLibrary.Entity_Layer.SubscriberAuthenticationProfile();

            XDSPortalEnquiry.Data.Authentication dAuth = new XDSPortalEnquiry.Data.Authentication();
            XDSPortalEnquiry.Data.XDSSettings dsettings = new XDSPortalEnquiry.Data.XDSSettings();

            Data.Authentication dAuthentication = new XDSPortalEnquiry.Data.Authentication();

            XDSPortalLibrary.Entity_Layer.Authentication eAuthentication = new XDSPortalLibrary.Entity_Layer.Authentication();
            XDSPortalAuthentication.AuthenticationDocument oAuthenticationDocument = oAuthenticationProcess.CurrentObjectState.AuthenticationDocument;

            #endregion

            try
            {
                oAuthenticationDocument.ErrorMessage = string.Empty;

                if (con.State == ConnectionState.Closed)
                    con.Open();
                if (AdminConnection.State == ConnectionState.Closed)
                    AdminConnection.Open();

                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                AuthAdminConnection = oXDSSettings.GetConnection(AdminConnection, eSe.ProductID);

                SqlConnection AuthAdminCon = new SqlConnection(AuthAdminConnection);

                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);
                Entity.SubscriberProductReports sprSMS = xb.GetPrice(AdminConnection, eSe.SubscriberID, SMSProductID);

                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                SubscriberAuthenticationID = int.Parse(oAuthenticationDocument.SubscriberAuthenticationID.ToString());

                oSa = dAuthentication.GetSubscriberAuthenticationObject(AuthAdminCon, SubscriberAuthenticationID);
                oSubscriberAuthenticationProfile = dAuthentication.GetSubscriberAuthenticationProfile(AuthAdminCon, oSa.SubscriberID, "failedauthentication");

                if (sprSMS.ReportID > 0 && !oSa.OverrideOTP)
                {
                    string ContactNo = string.Empty;
                    string OTP = string.Empty;
                    oAuthenticationDocument.OTPEnabled = true;
                    rp = dAuthentication.GetLatestPhoneNo(AuthAdminCon, oSa);
                    ContactNo = rp.ResponseData;

                    if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                    {
                        oAuthenticationDocument.OTPStatus = false;
                        oAuthenticationDocument.OTPNotGeneratedReason = rp.ResponseData;
                    }
                    else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report && !oSubscriberAuthenticationProfile.ExcludeLatestDBcellNo)
                    {
                        rp = dAuthentication.GenerateOTP(AuthAdminCon, oSa, eSC);
                        OTP = rp.ResponseData;

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                        {
                            oAuthenticationDocument.OTPStatus = false;
                            oAuthenticationDocument.OTPNotGeneratedReason = rp.ResponseData;
                        }
                        else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            XDSPortalLibrary.Entity_Layer.SMSNotification eSMSNotification = new XDSPortalLibrary.Entity_Layer.SMSNotification();
                            XDSPortalLibrary.Entity_Layer.Response rpSMS = new XDSPortalLibrary.Entity_Layer.Response();

                            SMSNotification oSMSNotification = new SMSNotification();

                            eSMSNotification.ClientContactDetails = string.Empty;
                            eSMSNotification.ContactNo = ContactNo;
                            eSMSNotification.SubscriberName = sub.SubscriberName;
                            eSMSNotification.IsCommercial = false;
                            eSMSNotification.AccountNo = OTP;
                            eSMSNotification.AlertType = "AUTHOTP";

                            rpSMS = oSMSNotification.SendSMSNotification(con, AdminConnection, SMSConnection, SMSProductID, eSe.SubscriberID, eSe.SystemUserID, oSa.SubscriberAuthenticationID, "AUTHOTP", eSMSNotification);

                            if (rpSMS.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                oAuthenticationDocument.OTPStatus = false;
                                oAuthenticationDocument.OTPNotGeneratedReason = "Internal Error Generating OTP";

                            }
                            else if (rpSMS.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                int QueueID = int.Parse(rpSMS.ResponseReferenceNo);
                                rp = dAuthentication.LogOTP(AuthAdminCon, oSa, QueueID, OTP, XDSPortalEnquiry.Data.OTPAction.Insert);

                                if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                                {
                                    oAuthenticationDocument.OTPStatus = false;
                                    oAuthenticationDocument.OTPNotGeneratedReason = rp.ResponseData;
                                }
                                else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                                {
                                    oAuthenticationDocument.OTPStatus = true;
                                    oAuthenticationDocument.OTPNotGeneratedReason = string.Empty;

                                    if (String.IsNullOrEmpty(eSC.VoucherCode))
                                    {
                                        Totalcost = Totalcost + sprSMS.UnitPrice;
                                    }
                                }
                            }
                        }
                    }

                    if (oSa.CellularNumber != ContactNo && !string.IsNullOrEmpty(oSa.CellularNumber.Trim()) && !oSubscriberAuthenticationProfile.ExcludeInputcellNo)
                    {
                        XDSPortalLibrary.Entity_Layer.SMSNotification eISMSNotification = new XDSPortalLibrary.Entity_Layer.SMSNotification();
                        XDSPortalLibrary.Entity_Layer.Response rpISMS = new XDSPortalLibrary.Entity_Layer.Response();

                        SMSNotification oISMSNotification = new SMSNotification();

                        eISMSNotification.ClientContactDetails = string.Empty;
                        eISMSNotification.ContactNo = oSa.CellularNumber;
                        eISMSNotification.SubscriberName = sub.SubscriberName;
                        eISMSNotification.IsCommercial = false;
                        eISMSNotification.AlertType = "AUTHOTP";
                        if (string.IsNullOrEmpty(OTP))
                        {
                            rp = dAuthentication.GenerateOTP(AuthAdminCon, oSa, eSC);
                            OTP = rp.ResponseData;

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                oAuthenticationDocument.OTPStatus = false;
                                oAuthenticationDocument.OTPNotGeneratedReason = rp.ResponseData;
                            }
                        }

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {

                            eISMSNotification.AccountNo = OTP;


                            rpISMS = oISMSNotification.SendSMSNotification(con, AdminConnection, SMSConnection, SMSProductID, eSe.SubscriberID, eSe.SystemUserID, oSa.SubscriberAuthenticationID, "AUTHOTP", eISMSNotification);

                            if (rpISMS.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                oAuthenticationDocument.OTPStatus = false;
                                oAuthenticationDocument.OTPNotGeneratedReason = "Internal Error Generating OTP";

                            }
                            else if (rpISMS.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                int IQueueID = int.Parse(rpISMS.ResponseReferenceNo);
                                rp = dAuthentication.LogOTP(AuthAdminCon, oSa, IQueueID, OTP, XDSPortalEnquiry.Data.OTPAction.Insert);

                                if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                                {
                                    oAuthenticationDocument.OTPStatus = false;
                                    oAuthenticationDocument.OTPNotGeneratedReason = rp.ResponseData;
                                }
                                else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                                {
                                    oAuthenticationDocument.OTPStatus = true;
                                    oAuthenticationDocument.OTPNotGeneratedReason = string.Empty;

                                    if (String.IsNullOrEmpty(eSC.VoucherCode))
                                    {
                                        Totalcost = Totalcost + sprSMS.UnitPrice;
                                    }
                                }
                            }

                        }
                    }
                    if (!string.IsNullOrEmpty(oSa.EmailAddress.Trim()) && !oSubscriberAuthenticationProfile.ExcludeEmailAddress)
                    {
                        XDSPortalLibrary.Entity_Layer.SMSNotification eISMSNotification = new XDSPortalLibrary.Entity_Layer.SMSNotification();

                        eISMSNotification.SubscriberID = sys.SubscriberID;
                        eISMSNotification.SubscriberName = sub.SubscriberName;
                        if (string.IsNullOrEmpty(OTP))
                        {
                            rp = dAuthentication.GenerateOTP(AuthAdminCon, oSa, eSC);
                            OTP = rp.ResponseData;

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                oAuthenticationDocument.OTPStatus = false;
                                oAuthenticationDocument.OTPNotGeneratedReason = rp.ResponseData;
                            }
                        }

                        if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                        {
                            XDSPortalEnquiry.Data.DefaultAlerts oOTP = new XDSPortalEnquiry.Data.DefaultAlerts();
                            eISMSNotification.AccountNo = OTP;
                            string strMessage = oOTP.GetOTPMessage(AdminConnection, eISMSNotification);

                            try
                            {
                                ProcessEmailOTP(AuthAdminCon, "Q", oSa, strsmtpServer, strsmtpUsername, strsmtpPassword, intport, oAuthenticationProcess, eSe, "Authentication - OTP", strMessage);
                                oAuthenticationDocument.OTPStatus = true;
                                oAuthenticationDocument.OTPNotGeneratedReason = string.Empty;
                            }
                            catch (Exception ex)
                            {
                                oAuthenticationDocument.OTPStatus = false;
                                oAuthenticationDocument.OTPNotGeneratedReason = rp.ResponseData;
                            }
                        }
                    }
                }
            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                dSe.UpdateSubscriberEnquiryError(con, eSe);

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

                oAuthenticationDocument.ErrorMessage = oException.Message;

                oAuthenticationProcess.GetAuthenticationProcessObject(oAuthenticationDocument);

                throw new Exception(oException.Message);
            }

            oAuthenticationProcess.CurrentObjectState.AuthenticationDocument = oAuthenticationDocument;

            return oAuthenticationProcess;
        }

        public DataSet GetDataSegments(SqlConnection EnquiryCon, int EnquiryResultID, string BonusXML)
        {
            DataSet ds = new DataSet();

            string rXml = "";
            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();

            XDSPortalEnquiry.Data.SubscriberEnquiryResultBonus odSubscriberEnquiryResultBonus = new XDSPortalEnquiry.Data.SubscriberEnquiryResultBonus();

            DataSet dsDataSegments = odSubscriberEnquiryResultBonus.GetSubscriberEnquiryResultBonusDataSet(EnquiryCon, EnquiryResultID);

            if (!string.IsNullOrEmpty(BonusXML))
            {
                try
                {
                    System.IO.StringReader xmlSR = new System.IO.StringReader(BonusXML);
                    ds.ReadXml(xmlSR);
                    if (ds.Tables.Count < 1)
                    {
                        ds = null;
                    }
                    else if (ds.Tables[0].Rows.Count < 1)
                    {
                        ds = null;
                    }
                    else
                    {
                        if (ds.Tables[0].Columns.Contains("DataSegmentID") && ds.Tables[0].Columns.Contains("BonusViewed"))
                        {
                            foreach (DataRow dr in ds.Tables[0].Rows)
                            {
                                if (dr["BonusViewed"].ToString().ToLower() == "true")
                                {
                                    foreach (DataRow r in dsDataSegments.Tables[0].Rows)
                                    {
                                        if (dr["DataSegmentID"].ToString() == r["DataSegmentID"].ToString())
                                        {
                                            dr["BonusPrice"] = r["BillingPrice"].ToString();
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            rXml = "<Error>" + "Invalid Trace Plus XML Supplied" + "</Error>";
                            rsXml.LoadXml(rXml);
                            dsDataSegments.ReadXml(rsXml.OuterXml);
                        }
                    }


                }
                catch
                {
                    rXml = "<Error>" + "Invalid Trace Plus XML Supplied" + "</Error>";
                    rsXml.LoadXml(rXml);
                    dsDataSegments.ReadXml(rsXml.OuterXml);
                }
            }
            else
            {
                ds = null;
            }

            dsDataSegments.Clear();
            dsDataSegments = ds;

            return dsDataSegments;
        }

        public XDSPortalLibrary.Entity_Layer.Response GetFraudScore(SqlConnection con, SqlConnection AdminConnection, string strTitle, string strChannel, string strStore, int decSalary, int intMonthsEmployed, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID)
        {
            #region Intialize Variables
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();

            Data.Authentication dAuthentication = new XDSPortalEnquiry.Data.Authentication();
            XDSPortalLibrary.Entity_Layer.SubscriberAuthenticationProfile oSubscriberAuthenticationProfile = new XDSPortalLibrary.Entity_Layer.SubscriberAuthenticationProfile();

            Data.XDSSettings oXDSSettings = new XDSPortalEnquiry.Data.XDSSettings();
            DataSet dsFraudScore = new DataSet();

            string AuthAdminConnection = string.Empty;
            string ProvidedContactNumber = string.Empty;
            string LatestContactNumber = string.Empty;
            #endregion

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                AuthAdminConnection = oXDSSettings.GetConnection(AdminConnection, productID);
                SqlConnection AuthAdminCon = new SqlConnection(AuthAdminConnection);

                // Get the Subscriber Profile settings
                oSubscriberAuthenticationProfile = dAuthentication.GetSubscriberAuthenticationProfile(AuthAdminCon, sub.SubscriberID, "");

                if (oSubscriberAuthenticationProfile.FraudScore)
                {
                    //Get Fraud Score
                    dsFraudScore = dAuthentication.GetFraudScore(AdminConnection, sub.SubscriberID, eSC.ConsumerID, eSC.SubscriberEnquiryID, eSC.SubscriberEnquiryResultID, eSe.ProductID, spr.ReportID, eSC.CreatedByUser,
                        sub.AssociationTypeCode, sub.SubscriberAssociationCode, strStore, strTitle, strChannel, decSalary, intMonthsEmployed);

                    if (dsFraudScore.Tables.Count > 0 && dsFraudScore.Tables[0].Rows.Count > 0)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = dsFraudScore.GetXml();
                    }
                    else
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "<Error>Profile doesn't have access to Fraud Score</Error>";
                }
            }
            catch (Exception ex)
            {
                rp.ResponseData = "<Error>" + ex.Message + "</Error>";
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            }

            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response GetIDPhoto(SqlConnection con, SqlConnection AdminConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, string strPBSAUsername, string strPBSAPassword, string strPBSAMemberKey, string strPBSATerminalID, string strPBSAURL, string BonusXML)
        {
            #region Intialize Variables
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            XDSPortalLibrary.Entity_Layer.Response rpMatch = new XDSPortalLibrary.Entity_Layer.Response();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();

            XDSPortalEnquiry.Data.SubscriberEnquiryResult odSubscriberEnquiryResult = new XDSPortalEnquiry.Data.SubscriberEnquiryResult();
            XDSPortalEnquiry.Entity.SubscriberEnquiryResult oSubscriberEnquiryResult = new XDSPortalEnquiry.Entity.SubscriberEnquiryResult();

            Data.Authentication dAuthentication = new XDSPortalEnquiry.Data.Authentication();
            XDSPortalLibrary.Entity_Layer.SubscriberAuthenticationProfile oSubscriberAuthenticationProfile = new XDSPortalLibrary.Entity_Layer.SubscriberAuthenticationProfile();

            XDSPortalEnquiry.Business.ConsumerTrace_IdentityVerification dtConsumertrace = new XDSPortalEnquiry.Business.ConsumerTrace_IdentityVerification();

            Data.XDSSettings oXDSSettings = new XDSPortalEnquiry.Data.XDSSettings();
            XDSPortalEnquiry.Business.MultipleTrace_IdentityVerification dtMultipleIDPhoto = new XDSPortalEnquiry.Business.MultipleTrace_IdentityVerification();

            DataSet dsFraudScore = new DataSet();
            DataSet dsDataSegments = new DataSet();
            XmlDocument xmlDoc = new XmlDocument();

            int enquiryResultId = 0;
            int enquiryId = 0;

            string AuthAdminConnection = string.Empty;
            string ProvidedContactNumber = string.Empty;
            string LatestContactNumber = string.Empty;
            #endregion

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                XDSPortalEnquiry.Data.Consumer oConsumer = new XDSPortalEnquiry.Data.Consumer();

                AuthAdminConnection = oXDSSettings.GetConnection(AdminConnection, productID);
                SqlConnection AuthAdminCon = new SqlConnection(AuthAdminConnection);

                // Get the Subscriber Profile settings
                oSubscriberAuthenticationProfile = dAuthentication.GetSubscriberAuthenticationProfile(AuthAdminCon, sub.SubscriberID, "");

                if (oSubscriberAuthenticationProfile.IDPhoto)
                {
                    //Get Id Photo
                    rpMatch = dtConsumertrace.SubmitIdentityVerification(con, AdminConnection, sub.SubscriberID, sys.SystemUserID, 101, sub.SubscriberName, eSe.IDNo, eSe.Surname,
                        eSe.FirstName, eSe.SecondName, eSe.BirthDate, eSe.SubscriberEnquiryID.ToString(), true, false, eSC.VoucherCode);

                    xmlDoc.LoadXml(rpMatch.ResponseData);

                    if (xmlDoc.FirstChild.ChildNodes.Count > 0)
                    {
                        foreach (XmlNode oXmlNode in xmlDoc.FirstChild.ChildNodes)
                        {
                            int consumerId = 0;
                            int homeAffairsId = 0;

                            int.TryParse(oXmlNode["ConsumerID"].InnerText.Trim(), out consumerId);
                            int.TryParse(oXmlNode["HomeAffairsID"].InnerText.Trim(), out homeAffairsId);

                            if (consumerId == 0)
                                consumerId = oConsumer.GetConsumerID(AdminConnection, 0, homeAffairsId);

                            if (consumerId.Equals(eSC.KeyID))
                            {
                                enquiryResultId = int.Parse(oXmlNode["EnquiryResultID"].InnerText);
                                enquiryId = int.Parse(oXmlNode["EnquiryID"].InnerText);

                                //Get Result
                                oSubscriberEnquiryResult = odSubscriberEnquiryResult.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, enquiryResultId);
                                dsDataSegments = GetDataSegments(con, enquiryResultId, BonusXML);

                                rp = dtMultipleIDPhoto.SubmitMultipleIdPhotoVerification(con, AdminConnection, oSubscriberEnquiryResult,
                                    dsDataSegments, true, oSubscriberEnquiryResult.VoucherCode,
                                    strPBSAUsername, strPBSAPassword, strPBSAMemberKey, strPBSATerminalID, strPBSAURL);
                            }
                            else
                                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                        }
                    }
                    else
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "<Error>Profile doesn't have access to ID Photo</Error>";
                }
            }
            catch (Exception ex)
            {
                rp.ResponseData = "<Error>" + ex.Message + "</Error>";
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            }

            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response GetCreditReport(SqlConnection con, SqlConnection AdminConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, string BonusXML)
        {
            #region Intialize Variables
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            XDSPortalLibrary.Entity_Layer.Response rpMatch = new XDSPortalLibrary.Entity_Layer.Response();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();

            XDSPortalEnquiry.Data.SubscriberEnquiryResult odSubscriberEnquiryResult = new XDSPortalEnquiry.Data.SubscriberEnquiryResult();
            XDSPortalEnquiry.Entity.SubscriberEnquiryResult oSubscriberEnquiryResult = new XDSPortalEnquiry.Entity.SubscriberEnquiryResult();

            Data.Authentication dAuthentication = new XDSPortalEnquiry.Data.Authentication();
            XDSPortalLibrary.Entity_Layer.SubscriberAuthenticationProfile oSubscriberAuthenticationProfile = new XDSPortalLibrary.Entity_Layer.SubscriberAuthenticationProfile();

            XDSPortalEnquiry.Business.ConsumerCreditGrantor_CreditEnquiry dtConsumerCredit = new XDSPortalEnquiry.Business.ConsumerCreditGrantor_CreditEnquiry();
            XDSPortalEnquiry.Business.MultipleCreditGrantor_CreditEnquiry dtMultipleCredit = new XDSPortalEnquiry.Business.MultipleCreditGrantor_CreditEnquiry();

            DataSet dsFraudScore = new DataSet();
            DataSet dsDataSegments = new DataSet();
            XmlDocument xmlDoc = new XmlDocument();

            int enquiryResultId = 0;
            int enquiryId = 0;
            #endregion

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                AuthAdminConnection = oXDSSettings.GetConnection(AdminConnection, productID);
                SqlConnection AuthAdminCon = new SqlConnection(AuthAdminConnection);

                // Get the Subscriber Profile settings
                oSubscriberAuthenticationProfile = dAuthentication.GetSubscriberAuthenticationProfile(AuthAdminCon, sub.SubscriberID, "");

                if (oSubscriberAuthenticationProfile.CreditReport)
                {
                    //Get Credit Report
                    rpMatch = dtConsumerCredit.SubmitCreditEnquiry(con, AdminConnection, sub.SubscriberID, sys.SystemUserID, 15, sub.SubscriberName, eSe.IDNo,
                        eSe.PassportNo, eSe.Surname, "", eSe.FirstName, "", "", "", eSe.BirthDate,
                        XDSPortalEnquiry.Entity.SubscriberEnquiry.GenderInd.M, eSe.SubscriberEnquiryID.ToString(), eSe.EnquiryReason, true, 0, 0, 0, true, 0, 0, 0, 0, true, true, eSC.VoucherCode);

                    xmlDoc.LoadXml(rpMatch.ResponseData);

                    if (xmlDoc.FirstChild.ChildNodes.Count > 0)
                    {
                        foreach (XmlNode oXmlNode in xmlDoc.FirstChild.ChildNodes)
                        {
                            int consumerId = 0;

                            int.TryParse(oXmlNode["ConsumerID"].InnerText.Trim(), out consumerId);

                            if (consumerId.Equals(eSC.KeyID))
                            {
                                enquiryResultId = int.Parse(oXmlNode["EnquiryResultID"].InnerText);
                                enquiryId = int.Parse(oXmlNode["EnquiryID"].InnerText);

                                //Get Result
                                oSubscriberEnquiryResult = odSubscriberEnquiryResult.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, enquiryResultId);
                                dsDataSegments = GetDataSegments(con, enquiryResultId, BonusXML);

                                rp = dtMultipleCredit.SubmitMulipleCreditEnquiry(con, AdminConnection, enquiryId, enquiryResultId, dsDataSegments, true, oSubscriberEnquiryResult.VoucherCode);
                            }
                            else
                                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                        }
                    }
                    else
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "<Error>Profile doesn't have access to Credit Report</Error>";
                }
            }
            catch (Exception ex)
            {
                rp.ResponseData = "<Error>" + ex.Message + "</Error>";
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            }

            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response GetAccountVerification(SqlConnection con, SqlConnection AVSEnquiryCon, SqlConnection AdminConnection, SqlConnection AVSCon,
            int intSubscriberEnquiryID, int intSubscriberEnquiryResultID,
            int avsProductID, string VerificationType, string SurName, string Initials, string AccNo, string BranchCode, string Acctype, string BankName, string source)
        {
            #region Intialize Variables
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();

            Data.Authentication dAuthentication = new XDSPortalEnquiry.Data.Authentication();
            XDSPortalLibrary.Entity_Layer.SubscriberAuthenticationProfile oSubscriberAuthenticationProfile = new XDSPortalLibrary.Entity_Layer.SubscriberAuthenticationProfile();

            XDSPortalEnquiry.Business.BankingEnquiry_AccountVerificationNed dtAccountVerification = new XDSPortalEnquiry.Business.BankingEnquiry_AccountVerificationNed();
            #endregion

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                AuthAdminConnection = oXDSSettings.GetConnection(AdminConnection, productID);
                SqlConnection AuthAdminCon = new SqlConnection(AuthAdminConnection);

                // Get the Subscriber Profile settings
                oSubscriberAuthenticationProfile = dAuthentication.GetSubscriberAuthenticationProfile(AuthAdminCon, sub.SubscriberID, "");

                if (oSubscriberAuthenticationProfile.AccountVerification)
                {
                    //Get AVS
                    int[] vendorCode = dtAccountVerification.GetSubscriberVendorCode(AVSCon, sub.SubscriberID);

                    switch (vendorCode[1])
                    {
                        case 3:
                            rp = dtAccountVerification.SubmitAccountVerificationRealTime(AVSEnquiryCon, AdminConnection, AVSCon, sub.SubscriberID,
                                         sys.SystemUserID, avsProductID, sub.SubscriberName, VerificationType, "",
                                         "", "", "", "",
                                         !string.IsNullOrEmpty(eSe.IDNo) ? eSe.IDNo : eSe.PassportNo,
                                         !string.IsNullOrEmpty(eSe.IDNo) ? "SID" : "FPP",
                                         SurName, Initials, AccNo, BranchCode, Acctype, BankName,
                                         eSC.SubscriberEnquiryID.ToString(), eSC.VoucherCode, source, "");
                            break;
                        default:
                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "<NoResult><Error>Your profile has not been configured to use this Product. Please contact XDS Helpdesk</Error></NoResult>";
                            break;
                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "<Error>Profile doesn't have access to Account Verification</Error>";
                }
            }
            catch (Exception ex)
            {
                rp.ResponseData = "<Error>" + ex.Message + "</Error>";
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            }

            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response GetAccountVerificationResult(SqlConnection AVSEnquiryCon, SqlConnection AdminConnection, int EnquiryLogID)
        {
            #region Intialize Variables
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            XDSPortalEnquiry.Data.SubscriberEnquiryLog odSubscriberEnquiryLog = new XDSPortalEnquiry.Data.SubscriberEnquiryLog();
            XDSPortalEnquiry.Entity.SubscriberEnquiryLog oSubscriberEnquiryLog = new XDSPortalEnquiry.Entity.SubscriberEnquiryLog();

            Data.Authentication dAuthentication = new XDSPortalEnquiry.Data.Authentication();
            XDSPortalLibrary.Entity_Layer.SubscriberAuthenticationProfile oSubscriberAuthenticationProfile = new XDSPortalLibrary.Entity_Layer.SubscriberAuthenticationProfile();
            #endregion

            try
            {
                //Get AVS Result   
                oSubscriberEnquiryLog = odSubscriberEnquiryLog.GetSubscriberEnquiryLogObject(AVSEnquiryCon, EnquiryLogID);

                // Get the Subscriber Profile settings
                AuthAdminConnection = oXDSSettings.GetConnection(AdminConnection, productID);
                SqlConnection AuthAdminCon = new SqlConnection(AuthAdminConnection);

                oSubscriberAuthenticationProfile = dAuthentication.GetSubscriberAuthenticationProfile(AuthAdminCon, oSubscriberEnquiryLog.SubscriberID, "");

                if (oSubscriberAuthenticationProfile.AccountVerification)
                {
                    if (oSubscriberEnquiryLog.EnquiryResult == XDSPortalEnquiry.Entity.SubscriberEnquiryLog.EnquiryResultInd.R.ToString())
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = oSubscriberEnquiryLog.XMLData;
                        rp.ResponseKey = oSubscriberEnquiryLog.SubscriberEnquiryLogID;
                    }
                    else if ((oSubscriberEnquiryLog.EnquiryResult == XDSPortalEnquiry.Entity.SubscriberEnquiryLog.EnquiryResultInd.N.ToString())
                        || oSubscriberEnquiryLog.EnquiryStatus == XDSPortalEnquiry.Entity.SubscriberEnquiryLog.EnquiryStatusInd.P.ToString())
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                        rp.ResponseKey = oSubscriberEnquiryLog.SubscriberEnquiryLogID;
                    }
                    else if (oSubscriberEnquiryLog.EnquiryResult == XDSPortalEnquiry.Entity.SubscriberEnquiryLog.EnquiryResultInd.E.ToString())
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseKey = oSubscriberEnquiryLog.SubscriberEnquiryLogID;
                        rp.ResponseData = oSubscriberEnquiryLog.ErrorDescription;
                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "<Error>Profile doesn't have access to Account Verification</Error>";
                }
            }
            catch (Exception ex)
            {
                rp.ResponseData = "<Error>" + ex.Message + "</Error>";
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            }

            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response GetBlockedConsumersbyID(SqlConnection AdminCon, int SystemUserID, string IDno)
        {
            #region Intialize Variables
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            Data.Authentication dAuthentication = new XDSPortalEnquiry.Data.Authentication();

            XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
            XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(AdminCon, SystemUserID);

            #endregion
            try
            {

                if (dAuthentication.GetUnblockUser(AdminCon, SystemUserID))
                {
                    rp = dAuthentication.GetBlockedConsumersbyID(AdminCon, oSystemUser.SubscriberID, IDno);

                }
                else
                {
                    throw new Exception("User doesn't have access to get this information");
                }
            }
            catch (Exception ex)
            {
                rp.ResponseData = "<Error>" + ex.Message + "</Error>";
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
            }

            return rp;
        }

    }
}