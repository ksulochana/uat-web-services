﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;

namespace XDSPortalEnquiry.Business
{
    public class BaberekiCustomScoring
    {

        private XDSPortalLibrary.Business_Layer.ConsumerCreditEnquiry moConsumerCreditGrantorWseManager;
        private XDSPortalLibrary.Entity_Layer.ConsumerCreditEnquiry eoConsumerCreditGrantorWseManager;
        private XDSPortalLibrary.Entity_Layer.ConsumerIDVerificationTrace eoConsumerTraceWseManager;

        Data.XDSSettings oXDSSettings = new XDSPortalEnquiry.Data.XDSSettings();
        public string AuthAdminConnection = string.Empty;

        public BaberekiCustomScoring()
        {
            moConsumerCreditGrantorWseManager = new XDSPortalLibrary.Business_Layer.ConsumerCreditEnquiry();
            eoConsumerCreditGrantorWseManager = new XDSPortalLibrary.Entity_Layer.ConsumerCreditEnquiry();
            eoConsumerTraceWseManager = new XDSPortalLibrary.Entity_Layer.ConsumerIDVerificationTrace();
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitMulipleCreditEnquiry(SqlConnection con, SqlConnection AdminConnection, int intSubscriberEnquiryID, int intSubscriberEnquiryResultID, bool bBonusChecking, string strVoucherCode)
        {
            string strAdminCon = AdminConnection.ConnectionString;

            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            string rXml = "";
            double Totalcost = 0;

            DataSet ds = new DataSet();

            Data.SubscriberEnquiry dSe = new Data.SubscriberEnquiry();
            Entity.SubscriberEnquiry eSe = new Entity.SubscriberEnquiry();

            Data.SubscriberEnquiryResult dSC = new Data.SubscriberEnquiryResult();
            Data.SubscriberEnquiryResultBonus dSB = new Data.SubscriberEnquiryResultBonus();

            Entity.SubscriberEnquiryResult eSC = new Entity.SubscriberEnquiryResult();
            Entity.SubscriberEnquiryResultBonus eSB = new Entity.SubscriberEnquiryResultBonus();

            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                eSe = dSe.GetSubscriberEnquiryObject(con, intSubscriberEnquiryID);
                eSC = dSC.GetSubscriberEnquiryResultObject(con, eSe.SubscriberID, eSe.CreatedByUser, eSe.SubscriberEnquiryID, intSubscriberEnquiryResultID);

                xdsBilling xb = new xdsBilling();
                Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, eSe.SubscriberID, eSe.ProductID);

                Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
                Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, eSe.SubscriberID);

                Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
                Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, eSe.SystemUserID);

                //if (spr.ReportID > 0)
                //{
                //    eoConsumerCreditGrantorWseManager.BonusSegments = null;
                //    if (dsBonusDataSegment != null)
                //    {
                //        if (dsBonusDataSegment.Tables.Count > 0 && dsBonusDataSegment.Tables[0].Rows.Count > 0)
                //        {
                //            dsBonusDataSegment.DataSetName = "BonusSegments";
                //            dsBonusDataSegment.Tables[0].TableName = "Segment";
                //            eoConsumerCreditGrantorWseManager.BonusSegments = dsBonusDataSegment;
                //        }
                //    }

                if (spr.ProductID > 0)
                {
                    // Check if the XMLData in SubscriberEnquiryResult table is populated. If it is Populated no need to regenrate the ReportXML 
                    // use this XML data for generating the Report 

                    if (string.IsNullOrEmpty(eSC.XMLData) || eSC.XMLData == string.Empty)
                    {

                        string strValidationStatus = "";
                        if (!string.IsNullOrEmpty(eSC.VoucherCode))
                        {
                            SubscriberVoucher sv = new SubscriberVoucher();
                            strValidationStatus = sv.ValidationStatus(AdminConnection, eSe.SubscriberID, eSC.VoucherCode);

                            if (strValidationStatus == "")
                            {
                                throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                            }
                            else if (!(strValidationStatus == "1"))
                            {
                                throw new Exception(strValidationStatus);
                            }

                        }
                        else if (sub.PayAsYouGo == 1)
                        {
                            //Calculate The Total cost of the Report , including Bonus Segments

                            Totalcost = spr.UnitPrice;

                            if (eoConsumerCreditGrantorWseManager.BonusSegments != null)
                            {

                                foreach (DataRow dr in eoConsumerCreditGrantorWseManager.BonusSegments.Tables[0].Rows)
                                {
                                    if (dr["BonusViewed"].ToString().ToLower() == "true")
                                    {
                                        Totalcost = Totalcost + Convert.ToInt16(dr["BonusPrice"].ToString());
                                    }
                                }
                            }
                        }

                        // Check if the Subscriber Has enough PayAsYouGoEnquiryLimit to view the report

                        if ((sub.PayAsYouGo == 1 && sub.PayAsyouGoEnquiryLimit >= (Totalcost)) || sub.PayAsYouGo == 0)
                        {

                            eoConsumerCreditGrantorWseManager.ConsumerID = eSC.KeyID;
                            eoConsumerCreditGrantorWseManager.EnquiryReason = eSe.EnquiryReason;
                            eoConsumerCreditGrantorWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerCreditGrantorWseManager.ReferenceNo = eSC.Reference;
                            eoConsumerCreditGrantorWseManager.subscriberID = eSe.SubscriberID;
                            eoConsumerCreditGrantorWseManager.ProductID = eSe.ProductID;
                            eoConsumerCreditGrantorWseManager.ExternalReference = eSe.SubscriberReference;
                            eoConsumerCreditGrantorWseManager.TmpReference = eSC.ExtraVarOutput1;

                            eoConsumerCreditGrantorWseManager.DataSegments = eSe.ExtraVarInput2;
                            eoConsumerCreditGrantorWseManager.SubscriberAssociationCode = sub.SubscriberAssociationCode;
                            eoConsumerCreditGrantorWseManager.AssociationTypeCode = sub.AssociationTypeCode;
                            eoConsumerCreditGrantorWseManager.EnquiryID = intSubscriberEnquiryID;
                            eoConsumerCreditGrantorWseManager.EnquiryResultID = intSubscriberEnquiryResultID;
                            eoConsumerCreditGrantorWseManager.Username = eSe.CreatedByUser;
                            eoConsumerCreditGrantorWseManager.SAFPSIndicator = sub.SAFPSIndicator;
                            eoConsumerCreditGrantorWseManager.DisplayUnusableInformation = sub.DisplayUnusableInformation;

                            XDSPortalEnquiry.Data.Consumer oEnquiryConsumer = new XDSPortalEnquiry.Data.Consumer();
                            oEnquiryConsumer.CheckBlockedStatus(AdminConnection, int.Parse(eSe.SubscriberID.ToString()), int.Parse(eSC.KeyID.ToString()), eSe.ProductID, sub.SubscriberAssociationCode, eSe.ProductID);


                            XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                            rp = ra.GetCreditEnquiryReportBabereki(eoConsumerCreditGrantorWseManager);

                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSe = dSe.ErrorGetSubscriberEnquiryObject(con, eSe.SubscriberEnquiryID);
                                ra = new XDSDataLibrary.ReportAccess(strAdminCon, eSe.ExtraVarInput1);
                                rp = ra.GetCreditEnquiryReportBabereki(eoConsumerCreditGrantorWseManager);
                            }

                            rXml = rp.ResponseData;

                            System.IO.StringReader xmlSR = new System.IO.StringReader(rXml);


                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None)
                            {

                                eSC.DetailsViewedYN = false;
                                eSC.Billable = false;
                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.N.ToString();
                                eSC.CreatedByUser = eSe.CreatedByUser;
                                eSC.CreatedOnDate = DateTime.Now;

                                dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                rp.EnquiryID = eSC.SubscriberEnquiryID;

                            }
                            else if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error)
                            {
                                eSC.EnquiryResult = "E";
                                eSe.ErrorDescription = rp.ResponseData.ToString();
                                dSe.UpdateSubscriberEnquiryError(con, eSe);

                            }


                            if (rp.ResponseStatus == XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report)
                            {
                                Data.SubscriberFootPrint dSubscriberFootPrint = new XDSPortalEnquiry.Data.SubscriberFootPrint();
                                Entity.SubscriberFootPrint eSubscriberFootPrint = new XDSPortalEnquiry.Entity.SubscriberFootPrint();

                                eSC.DetailsViewedDate = DateTime.Now;
                                eSC.DetailsViewedYN = true;
                                if (sub.PayAsYouGo == 1 || !(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    eSC.Billable = false;
                                }
                                else
                                {
                                    eSC.Billable = true;
                                }
                                eSC.ChangedByUser = eSe.CreatedByUser;
                                eSC.ChangedOnDate = DateTime.Now;

                                eSC.KeyID = rp.ResponseKey;
                                eSC.KeyType = rp.ResponseKeyType;
                                eSC.SubscriberEnquiryID = intSubscriberEnquiryID;

                                eSC.BillingTypeID = spr.BillingTypeID;
                                eSC.BillingPrice = spr.UnitPrice;

                                eSC.XMLData = rXml.Replace("'", "''");
                                eSC.EnquiryResult = Entity.SubscriberEnquiry.EnquiryResultInd.R.ToString();
                                eSC.ProductID = eSe.ProductID;

                                dSC.UpdateSubscriberEnquiryResult(con, eSC);
                                rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;
                                rp.EnquiryID = eSC.SubscriberEnquiryID;

                                // Reduce the subscriber's PayAsYouGoEnquiryLimit by the Report Cost
                                if (sub.PayAsYouGo == 1)
                                {
                                    dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, eSe.SubscriberID, Totalcost);
                                }
                                if (!(string.IsNullOrEmpty(eSC.VoucherCode)))
                                {
                                    dSV.UpdateSubscriberVoucher(AdminConnection, eSC.VoucherCode, eSe.CreatedByUser);
                                }

                                if (String.IsNullOrEmpty(eSC.VoucherCode))
                                {
                                    //Log FootPrint

                                    eSubscriberFootPrint.CreatedByUser = eSC.CreatedByUser;
                                    eSubscriberFootPrint.CreatedOnDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.EnquiryReason = eSe.EnquiryReason;
                                    eSubscriberFootPrint.KeyID = eSC.KeyID;
                                    eSubscriberFootPrint.KeyType = eSC.KeyType;
                                    eSubscriberFootPrint.ExternalReference = eSe.SubscriberReference;
                                    eSubscriberFootPrint.SubscriberBusinessType = sub.SubscriberBusinessType;
                                    if (!string.IsNullOrEmpty(sub.CompanyTelephoneCode) && !string.IsNullOrEmpty(sub.CompanyTelephoneNo))
                                    {
                                        eSubscriberFootPrint.SubscriberContact = sub.CompanyTelephoneCode + sub.CompanyTelephoneNo;
                                    }
                                    eSubscriberFootPrint.SubscriberEnquiryDate = eSC.CreatedOnDate;
                                    eSubscriberFootPrint.SubscriberEnquiryID = eSC.SubscriberEnquiryID;
                                    eSubscriberFootPrint.SubscriberEnquiryResultID = eSC.SubscriberEnquiryResultID;
                                    eSubscriberFootPrint.SubscriberID = eSe.SubscriberID;
                                    eSubscriberFootPrint.SubscriberName = sub.SubscriberName;
                                    eSubscriberFootPrint.SubscriberUser = eSe.SystemUser;
                                    eSubscriberFootPrint.CreatedByUser = sys.Username;
                                    eSubscriberFootPrint.CreatedOnDate = DateTime.Now;
                                    eSubscriberFootPrint.AssociationTypeCode = sub.AssociationTypeCode;
                                    eSubscriberFootPrint.ProductID = eSe.ProductID;
                                    eSubscriberFootPrint.FootprintActive = spr.FootprintActive;

                                    int intSubscriberFootPrint = dSubscriberFootPrint.insertSubscriberFootPrint(AdminConnection, eSubscriberFootPrint);
                                }
                            }

                            rXml = rp.ResponseData;

                        }

                        else
                        {
                            // When Subscriberdoesn't have enough credit Limit.

                            rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                            rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";

                        }
                    }
                    else
                    {
                        // When User want to Re - Open a report 
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = eSC.XMLData;
                        rp.EnquiryID = eSC.SubscriberEnquiryID;
                        rp.EnquiryResultID = eSC.SubscriberEnquiryResultID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }
                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            catch (Exception oException)
            {
                eSe.EnquiryResult = "E";
                eSe.ErrorDescription = oException.Message;
                eSe.SubscriberEnquiryID = dSe.UpdateSubscriberEnquiryError(con, eSe);
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;


                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();

                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }
            ds.Dispose();
            return rp;
        }







    }
}
