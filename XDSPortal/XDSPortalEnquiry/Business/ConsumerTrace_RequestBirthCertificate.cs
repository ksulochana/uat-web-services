﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace XDSPortalEnquiry.Business
{
    public  class ConsumerTrace_RequestBirthCertificate
    {
        public ConsumerTrace_RequestBirthCertificate()
        {
        }
        public void ValidateInput(Entity.SubscriberEnquiryLog Validate)
        {
            if (string.IsNullOrEmpty(Validate.FirstName) || (string.IsNullOrEmpty(Validate.SurName)) || (string.IsNullOrEmpty(Validate.IDNo)) || (string.IsNullOrEmpty(Validate.EmailAddress)))
            {
                throw new Exception("IDNo ,FirstName, SurName and Email Address are Mandatory");
            }
        }

        public XDSPortalLibrary.Entity_Layer.Response SubmitBirthorDeathCertificateRequest(SqlConnection con, SqlConnection AdminConnection, int intSubscriberID, int intSystemUserID, int intProductId, string strSubscriberName, string strIDNo, string strSurName, string strFirstName, string strClientFirstname, string strClientSurname, string strClientCompany, string strContactNumber, string strCilentEmailAddress, bool bConfirmationChkBox, string strExtRef, bool bBonusChecking, string strVoucherCode)
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
            if (AdminConnection.State == ConnectionState.Closed)
                AdminConnection.Open();

            bBonusChecking = false;
            int intSubscriberEnquiryLogID = 0;

            int MatchResult = 0;
            DataSet ds = new DataSet();
            string strValidationStatus = "";
            Boolean boolSubmitEnquiry = true;


            Data.Subscriber dsub = new XDSPortalEnquiry.Data.Subscriber();
            Entity.Subscriber sub = dsub.GetSubscriberRecord(AdminConnection, intSubscriberID);

            Data.SystemUser dsys = new XDSPortalEnquiry.Data.SystemUser();
            Entity.SystemUser sys = dsys.GetSystemUserRecord(AdminConnection, intSystemUserID);

            Entity.SubscriberEnquiryLog sel = new XDSPortalEnquiry.Entity.SubscriberEnquiryLog();
            Data.SubscriberEnquiryLog dsel = new XDSPortalEnquiry.Data.SubscriberEnquiryLog();


            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            xdsBilling xb = new xdsBilling();
            Entity.SubscriberProductReports spr = xb.GetPrice(AdminConnection, intSubscriberID, intProductId);


            Data.SubscriberVoucher dSV = new XDSPortalEnquiry.Data.SubscriberVoucher();

            try
            {

                // Check if this subscriber is authorized to use the current product
                if (spr.ReportID > 0)
                {

                    if (!string.IsNullOrEmpty(strVoucherCode))
                    {
                        SubscriberVoucher sv = new SubscriberVoucher();
                        strValidationStatus = sv.ValidationStatus(AdminConnection, intSubscriberID, strVoucherCode);

                        if (strValidationStatus == "")
                        {
                            throw new System.ArgumentException("Invalid Voucher Code", "InvalidVoucherCode");
                        }
                        else if (!(strValidationStatus == "1"))
                        {
                            throw new Exception(strValidationStatus);
                        }


                    }
                    else if (sub.PayAsYouGo == 1 && spr.UnitPrice > sub.PayAsyouGoEnquiryLimit)
                    {
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                        boolSubmitEnquiry = false;

                    }
                    if (boolSubmitEnquiry)
                    {
                        sel.FirstName = strFirstName;
                        sel.SurName = strSurName;
                        sel.EmailAddress = strCilentEmailAddress;
                        sel.VoucherCode = strVoucherCode;
                        sel.SubscriberReference = strExtRef;
                        sel.SubscriberID = intSubscriberID;
                        sel.SubscriberName = strSubscriberName;
                        sel.SystemUserID = intSystemUserID;
                        sel.SystemUserName = sys.SystemUserFullName;
                        sel.CreatedByUser = sys.Username;
                        sel.CreatedOnDate = DateTime.Now;
                        sel.SubscriberEnquiryDate = DateTime.Now;
                        sel.EnquiryStatus = XDSPortalEnquiry.Entity.SubscriberEnquiryLog.EnquiryStatusInd.P.ToString();
                        sel.IDNo = strIDNo;
                        sel.BillingTypeID = spr.BillingTypeID;
                        sel.CompanyName = strClientCompany;
                        sel.ContactNo = strContactNumber;
                        sel.AccHolder = strClientFirstname;
                        sel.BusBusinessName = strClientSurname;



                        ValidateInput(sel);

                        // Generate serach input string

                        if (!string.IsNullOrEmpty(sel.IDNo))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.IDNo;
                        }
                        if (!string.IsNullOrEmpty(sel.FirstName))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.FirstName;
                        }
                        if (!string.IsNullOrEmpty(sel.SurName))
                        {
                            sel.Searchinput = sel.Searchinput + " | " + sel.SurName;
                        }

                        if (sel.Searchinput.Length > 0)
                        {
                            sel.Searchinput = sel.Searchinput.Substring(3, sel.Searchinput.Length - 3).ToUpper();
                        }

                        sel.KeyType = "H";
                        sel.ProductID = intProductId;
                        sel.BillingPrice = spr.UnitPrice;

                        if (sub.PayAsYouGo == 1 || !(strVoucherCode == string.Empty))
                        {
                            sel.Billable = false;
                        }
                        else
                        {
                            sel.Billable = true;
                        }

                        intSubscriberEnquiryLogID = dsel.InsertSubscriberEnquiryLog(con, sel);

                        if (sub.PayAsYouGo == 1)
                        {
                            dsub.UpdatePayAsYouGoEnquiryLimit(AdminConnection, intSubscriberID, spr.UnitPrice);
                        }
                        if (!(strVoucherCode == string.Empty))
                        {
                            dSV.UpdateSubscriberVoucher(AdminConnection, strVoucherCode, sys.Username);
                        }

                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        rp.ResponseData = "Enquiry Submitted successfully";
                        rp.EnquiryLogID = intSubscriberEnquiryLogID;

                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                }

                con.Close();
                AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

            }

            catch (Exception oException)
            {
                sel.EnquiryResult = "E";
                sel.ErrorDescription = oException.Message;
                sel.SubscriberEnquiryLogID = dsel.UpdateSubscriberEnquiryLogError(con, sel);

                MatchResult = sel.SubscriberEnquiryLogID;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

                if (con.State == ConnectionState.Open)
                    con.Close();
                if (AdminConnection.State == ConnectionState.Open)
                    AdminConnection.Close();
                SqlConnection.ClearPool(con);
                SqlConnection.ClearPool(AdminConnection);

                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;
            }

            ds.Dispose();
            return rp;
        }

    }
}
