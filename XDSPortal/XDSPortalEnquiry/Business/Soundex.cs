﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XDSPortalEnquiry.Business
{
    public static class Soundex
    {
        public static string FnCustomSoundex(string input)
        {
            string Key = string.Empty, character = string.Empty, Chars = string.Empty, FirstChar = string.Empty, Result = string.Empty;
            string[] Vowels = { "A", "E", "I", "O", "U" };
            int Counter = 0;

            if (string.IsNullOrEmpty(input))
            {
                input = string.Empty;
                return null;
            }

            input = input.Trim().ToUpper();

            if (!string.IsNullOrEmpty(input))
            {
                /* trim all spaces */
                input = input.Replace(" ", "");

                /* save first char */
                FirstChar = input.Substring(0, 1);

                /* (1) remove all 'S' and 'Z' chars from the end of the surname */
                Counter = input.Length;

                while (FnSubstring(input, Counter - 1, 1).ToUpper() == ("S") || FnSubstring(input, Counter - 1, 1).ToUpper() == ("Z"))
                {
                    Counter = Counter - 1;
                }

                input = FnSubstring(input, 0, Counter);

                /* (2) transcode initial strings */
                /*      MAC => MC                */
                /*      PF => F                  */
                if (FnSubstring(input, 0, 3).ToUpper() == "MAC")
                    input = "MC" + FnSubstring(input, 2, input.Length);
                else if (FnSubstring(input, 0, 2).ToUpper() == "PF")
                    input = "F" + FnSubstring(input, 2, input.Length);

                /* (3) Transcode trailing strings as follows */
                /*      IX       => IC                       */
                /*      EX       => EC                       */
                /*      YE,EE,IE => Y                        */
                /*      NT,ND    => D                        */
                if (input.ToUpper().EndsWith("IX"))
                    input = FnSubstring(input, 0, input.Length - 2) + "IC";
                else if (input.ToUpper().EndsWith("EX"))
                    input = FnSubstring(input, 0, input.Length - 2) + "EC";
                else if (input.ToUpper().EndsWith("YE"))
                    input = FnSubstring(input, 0, input.Length - 2) + "Y";
                else if (input.ToUpper().EndsWith("EE"))
                    input = FnSubstring(input, 0, input.Length - 2) + "Y";
                else if (input.ToUpper().EndsWith("IE"))
                    input = FnSubstring(input, 0, input.Length - 2) + "Y";
                else if (input.ToUpper().EndsWith("NT"))
                    input = FnSubstring(input, 0, input.Length - 2) + "D";
                else if (input.ToUpper().EndsWith("ND"))
                    input = FnSubstring(input, 0, input.Length - 2) + "D";

                /* the step (4) I moved to begining of while ... end below */

                /* (5) use first character of name as first character of key */
                /* select Key = left(input, 1) */
                /* don't now, what they thing with this, but with Key = '' it seems to be working */
                Counter = 0;
                /* while not end of input */
                while (Counter < input.Length)
                {
                    Chars = FnSubstring(input, Counter, input.Length - Counter >= 3 ? 3 : input.Length - Counter);

                    Result = Counter > 0 && Chars.ToUpper().StartsWith("EV") ? "AF" :
                        (Chars.ToUpper().StartsWith("W") && Vowels.Contains(FnSubstring(input, Counter - 1, 1).ToUpper()) ? FnSubstring(input, Counter - 1, 1) :
                        (Vowels.Contains(Chars.Substring(0, 1)) ? "A" :
                        (Chars.ToUpper().StartsWith("GHT") ? "GGG" :
                        (Chars.ToUpper().StartsWith("DG") ? "G" :
                        (Chars.ToUpper().StartsWith("PH") ? "F" :
                        (Chars.ToUpper().StartsWith("H") && Counter > 1 && (Vowels.Contains(FnSubstring(input, Counter - 1, 1).ToUpper()) || (Counter + 1 < input.Length && Vowels.Contains(FnSubstring(input, Counter + 1, 1).ToUpper()))) ? FnSubstring(input, Counter - 1, 1).ToUpper() :
                        (Chars.ToUpper().StartsWith("KN") ? "N" :
                        (Chars.ToUpper().StartsWith("K") ? "C" :
                        (Counter > 1 && Chars.StartsWith("M") ? "N" :
                        (Counter > 1 && Chars.StartsWith("Q") ? "G" :
                        (Chars.ToUpper().StartsWith("SH") ? "S" :
                        (Chars.ToUpper().StartsWith("SCH") ? "SSS" :
                        (Chars.ToUpper().StartsWith("YW") ? "Y" :
                        (Counter > 1 && Counter < input.Length && input.ToUpper().StartsWith("Y") ? "A" :
                        (Chars.ToUpper().StartsWith("WR") ? "R" :
                        (Counter > 1 && Chars.ToUpper().StartsWith("Z") ? "S" : Chars.Substring(0, 1)))))))))))))))));

                    input = input.Replace(input.Substring(Counter, Result.Length), Result);

                    //    /* Add current to key if current <> last key character */
                    if (FnRight(Key, 1) != FnLeft(Result, 1))
                    {
                        Key = Key + Result;
                    }

                    Counter = Counter + 1;
                }

                /* (21) transcode terminal 'AY' to 'Y' */
                if (FnRight(Key, 2).ToUpper() == "AY")
                {
                    Key = FnLeft(Key, Key.Length - 2) + "Y";
                }

                /* (22) remove traling vowels */
                /*      start vowels */
                Counter = 0;
                if (!string.IsNullOrEmpty(Key))
                {
                    while (Vowels.Contains(FnSubstring(Key, Counter, 1).ToUpper()))
                    {
                        /* replace vowels with spaces */
                        //Key = Key.Replace(Key.Substring(Counter, 1), " ");
                        Key = FnLeft(Key, Counter) + " " + FnRight(Key, Key.Length - Counter - 1);
                        Counter = Counter + 1;
                    }
                }
                /*     end vowels */
                Counter = Key.Length - 1;
                if (!string.IsNullOrEmpty(Key))
                {
                    while (Vowels.Contains(FnSubstring(Key, Counter, 1).ToUpper()))
                    {
                        /* replace vowels with spaces */
                        // Key = Key.Replace(Key.Substring(Counter, 1), " ");
                        Key = FnLeft(Key, Counter) + " " + FnRight(Key, Key.Length - Counter - 1);
                        Counter = Counter - 1;
                    }
                }

                /*     remove spaces */
                Key = Key.Replace(" ", "");

                /* (23) collapse all strings of repeated characters */
                /* not needed, see 'Add current to key if current <> last key character' before step (21) */

                /* (24) if first char of original surname was a vowel, append it to the start of code */
                if (Vowels.Contains(FirstChar.ToUpper()))
                {
                    Key = FirstChar + Key;
                }

                return Key;
            }

            return input;
        }

        public static string FnRight(string input, int numberOfCharacters)
        {
            if (string.IsNullOrEmpty(input))
            {
                return string.Empty;
            }
            else
            {
                return input.Substring(numberOfCharacters > input.Length ? 0 : input.Length - numberOfCharacters);
            }
        }

        public static string FnLeft(string input, int numberOfCharacters)
        {
            if (string.IsNullOrEmpty(input))
            {
                return string.Empty;
            }
            else
            {
                return input.Substring(0, numberOfCharacters);
            }
        }

        public static string FnSubstring(string input, int startIndex, int numberOfCharacters)
        {

            if (string.IsNullOrEmpty(input) || numberOfCharacters + startIndex > input.Length || startIndex < 0)
            {
                return string.Empty;
            }
            else
            {
                return input.Substring(startIndex, numberOfCharacters).ToUpper();
            }
        }
    }
}
