﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDSPortalEnquiry.Entity
{
    public class IDbioMetricStats
    {
        public IDbioMetricStats() { }

        public int IDBioMetricStatsID { get; set; }
        public int SubscriberEnquiryID { get; set; }
        public int SubscriberEnquiryResultID { get; set; }
        public int SubscriberID { get; set; }
        public int SystemuserID { get; set; }
        public string Region { get; set; }
        public int ConsumerID { get; set; }
        public int HomeAffairsID { get; set; }
        public string IDNumber { get; set; }
        public string Name { get; set; }
        public string ApplicationErrors { get; set; }
        public string VerificationErrors { get; set; }
        public string Base64StringJpeg2000Image { get; set; }
        public string VerificationResult { get; set; }
        public string VerificationResultDescription { get; set; }
        public string VerificationColorIndicator { get; set; }
        public DateTime TmStamp { get; set; }
        public long TransactionNumber { get; set; }
        public bool HasImage { get; set; }
        public string FingerColor { get; set; }
        public int ProductID { get; set; }
        public int ReportID { get; set; }
        public string SubscriberReference { get; set; }
        public string CreatedbyUser { get; set; }
        public string SubscriberName { get; set; }
        public DateTime SubscriberEnquiryDate { get; set; }
        public string Systemusername { get; set; }
        public string SystemUserNameInfo { get; set; }
        public bool IsMaster { get; set; }
        public string Keyword { get; set; }
        public string Comments { get; set; }

    }
}
