﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDSPortalEnquiry.Entity
{
    public class SubscriberLinkageInvestigationDetails
    {

        public enum StatusInd { P, C, F,N };
        public int SubscriberLinkageInvestigationDetailsID { get; set; }
        public int HeadSubscriberEnquiryID { get; set; }
        public int HeadSubscriberEnquiryresultID { get; set; }
        public int EventID { get; set; }
        public int InvestigationID { get; set; }
        public int SecondarySubscriberEnquiryID { get; set; }
        public int SecondarySubscriberEnquiryResultID { get; set; }
        public string KeyType { get; set; }
        public int KeyID { get; set; }
        public string KeyName { get; set; }
        public string KeyNo { get; set; }
        public int LinkedID { get; set; }
        public string LinkedName { get; set; }
        public string LinkedNo { get; set; }
        public bool IstheNodeExpanded { get; set; }
        public string LinkType { get; set; }
        public string CreatedbyUser { get; set; }
        public System.DateTime Createdondate { get; set; }
        public string ChangedbyUser { get; set; }
        public Nullable<System.DateTime> Changedondate { get; set; }
        public int PrimarySubscriberEnquiryID { get; set; }
        public int PrimarySubscriberEnquiryResultID { get; set; }
        public int SubscriberID { get; set; }
        public string SubscriberName { get; set; }
        public int SystemuserID { get; set; }
        public string SystemUsername { get; set; }
        public string InvestigationReference { get; set; }
        public StatusInd InvestigationStatus { get; set; }
        public string SubscriberReference { get; set; }
        public string ErrorDescription { get; set; }
        public int ProductID { get; set; }
        public string LinkSubscriberEnquiryID { get; set; }
        public string LinkSubscriberEnquiryResultID { get; set; }
        public string LinkID { get; set; }
        public int SubscriberEnquiryID { get; set; }
        public string SubscriberEnquiryResultID { get; set; }
        public string Status { get; set; }
        public string Xmldata { get; set; }
        public string EntityType { get; set; }
        public string LinkedEntityStatus { get; set; }
        public string EntityStatus { get; set; }
        public bool Billable { get; set; }
        public bool DetailsViewedYN { get; set; }
        public bool DetailsViewedDate { get; set; }
        public double BillingPrice { get; set; }
        public double TotalPrice { get; set; }
        public int BillingTypeID { get; set; }
        public string LinkedEntityCategory { get; set; }

    }

    public class EntityList
    {
       public int InvestigationID;
       public List<EntityReference> Entities ;

     //  public SortedList<int,int> SortedEntities = new SortedList<int,int>();
    }
    public class EntityReference
    {
       public int EnquiryID;
       public int EnquiryResultID;
    }

    public class SubscriberLinkageDetectorEnquiry
    {

        public enum StatusInd { P, C, F, N };
        public int InvestigationID { get; set; }
        public string CreatedbyUser { get; set; }
        public System.DateTime Createdondate { get; set; }
        public string ChangedbyUser { get; set; }
        public Nullable<System.DateTime> Changedondate { get; set; }
        public int SubscriberID { get; set; }
        public string SubscriberName { get; set; }
        public int SystemuserID { get; set; }
        public string SystemUsername { get; set; }
        public string SubscriberReference { get; set; }
        public string ErrorDescription { get; set; }
        public int ProductID { get; set; }
        public string LinkSubscriberEnquiryID { get; set; }
        public string LinkSubscriberEnquiryResultID { get; set; }
        public string LinkID { get; set; }
        public int SubscriberEnquiryID { get; set; }
        public int SubscriberEnquiryResultID { get; set; }
        public string Status { get; set; }
        public string Xmldata { get; set; }
        public int PrimaryEnquiryResultID { get; set; }
        public int ResultPrimaryEnquiryResultID { get; set; }
    }

    public class LinkageEvents
    {
        public int InvestigationID { get; set; }
        public int SubscriberEnquiryID { get; set; }
        public int SubscriberEnquiryResultID { get; set; }
        public int ProductID { get; set; }
        public int ResultSubscriberEnquiryID { get; set; }
        public int ResultSubscriberEnquiryResultID { get; set; }
        public string Response { get; set; }
        public string CreatedbyUser { get; set; }
        public System.DateTime Createdondate { get; set; }
        public string ChangedbyUser { get; set; }
        public Nullable<System.DateTime> Changedondate { get; set; }
        public string ExtractType { get; set; }
        public bool HistoryChanged { get; set; }
        public double BillingPrice { get; set; }
        public bool Billable { get; set; }
        public double TotalPrice { get; set; }
       
    }

    public class SubscriberInputDetails
    {
        public int InvestigationID { get; set; }
        public int SubscriberID { get; set; }
        public string SubscriberName { get; set; }
        public int SystemuserID { get; set; }
        public string SystemUsername { get; set; }
        public string SubscriberReference { get; set; }
        public DateTime EnquiryDate { get; set; }
        public string EnquiryType { get; set; }
        public string EnquiryInput { get; set; }
        public int EnquiryID { get; set; }
        public int EnquiryResultID { get; set; }
        public string EntityType { get; set; }
        public string EntityNo { get; set; }
        public string EntityName { get; set; }
        public int ProductID { get; set; }
        public string CreatedbyUser { get; set; }

    }
}
