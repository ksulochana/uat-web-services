using System;
using System.Collections.Generic;
using System.Text;

namespace XDSPortalEnquiry.Entity
{
    [Serializable]
    public class SubscriberFootPrint
    {
       
        public SubscriberFootPrint() { }

        private int _SubscriberFootPrintID;
        private int _SubscriberEnquiryID;
        private int _SubscriberEnquiryResultID;
        private string _KeyType;
        private int _KeyID;
        private DateTime _SubscriberEnquiryDate = DateTime.Parse("1900/01/01");
        private string _EnquiryReason;
        private int _SubscriberID;
        private string _SubscriberName;
        private string _SubscriberContact;
        private string _SubscriberUser;
        private string _SubscriberBusinessType;
        private string _CreatedByUser;
        private DateTime _CreatedOnDate = DateTime.Parse("1900/01/01");
        private string _ChangedByUser;
        private DateTime _ChangedOnDate = DateTime.Parse("1900/01/01");
        private string _ExternalReference;
        private string _AssociationTypeCode;
        private int _ProductID;
        private bool _FootprintActive;

        public string ExternalReference
        {
            get { return _ExternalReference; }
            set { _ExternalReference = value; }
        }

        public string CreatedByUser
        {
            get { return _CreatedByUser; }
            set { _CreatedByUser = value; }
        }

        public DateTime CreatedOnDate
        {
            get { return _CreatedOnDate; }
            set { _CreatedOnDate = value; }
        }

        public string ChangedByUser
        {
            get { return _ChangedByUser; }
            set { _ChangedByUser = value; }
        }

        public DateTime ChangedOnDate
        {
            get { return _ChangedOnDate; }
            set { _ChangedOnDate = value; }
        }

        public int SubscriberFootPrintID
        {
            get { return _SubscriberFootPrintID; }
            set { _SubscriberFootPrintID = value; }
        }

        public string EnquiryReason
        {
            get { return _EnquiryReason; }
            set { _EnquiryReason = value; }
        }

        public int SubscriberID
        {
            get { return _SubscriberID; }
            set { _SubscriberID = value; }
        }

        public string SubscriberName
        {
            get { return _SubscriberName; }
            set { _SubscriberName = value; }
        }

        public string SubscriberContact
        {
            get { return _SubscriberContact; }
            set { _SubscriberContact = value; }
        }

        public string SubscriberUser
        {
            get { return _SubscriberUser; }
            set { _SubscriberUser = value; }
        }

        public string SubscriberBusinessType
        {
            get { return _SubscriberBusinessType; }
            set { _SubscriberBusinessType = value; }
        }


        public string KeyType
        {
            get { return _KeyType; }
            set { _KeyType = value; }
        }

        public int KeyID
        {
            get { return _KeyID; }
            set { _KeyID = value; }
        }

        public int SubscriberEnquiryID
        {
            get { return _SubscriberEnquiryID; }
            set { _SubscriberEnquiryID = value; }
        }

        public int SubscriberEnquiryResultID
        {
            get { return _SubscriberEnquiryResultID; }
            set { _SubscriberEnquiryResultID = value; }
        }


        public DateTime SubscriberEnquiryDate
        {
            get { return _SubscriberEnquiryDate; }
            set { _SubscriberEnquiryDate = value; }
        }

        public string AssociationTypeCode
        {
            get { return _AssociationTypeCode; }
            set { _AssociationTypeCode = value; }
        }

        public int ProductID
        {
            get { return _ProductID; }
            set { _ProductID = value; }
        }

        public bool FootprintActive
        {
            get { return _FootprintActive; }
            set { _FootprintActive = value; }
        }

    }
}
