﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;

namespace XDSPortalEnquiry.Entity
{
    [Serializable]
    public class SubscriberSanction
    {

        public SubscriberSanction() { }

        public string IdNumber { get; set; }
        public string PassportNo { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string Nationality { get; set; }
        public string Citizenship { get; set; }
        public string Country { get; set; }
    }
}
