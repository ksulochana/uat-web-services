using System;
using System.Collections.Generic;
using System.Text;

namespace XDSPortalEnquiry.Entity
{
    [Serializable]
    public class SubscriberEnquiryResultBonus
    {
        
        public SubscriberEnquiryResultBonus() { }

        private int _SubscriberEnquiryResultBonusID;
        private int _SubscriberEnquiryResultID;
        private int _DataSegmentID;
        private string _DataSegmentName;
        private string _DataSegmentDisplayText;
        private bool _BonusViewed;
        private double _BillingPrice;
        private bool _Billable;
        private string _CreatedByUser;
        private DateTime _CreatedOnDate = DateTime.Parse("1900/01/01");
        private string _ChangedByUser;
        private DateTime _ChangedOnDate = DateTime.Parse("1900/01/01");

        public bool Billable
        {
            get { return _Billable; }
            set { _Billable = value; }
        }

        public string CreatedByUser
        {
            get { return _CreatedByUser; }
            set { _CreatedByUser = value; }
        }

        public DateTime CreatedOnDate
        {
            get { return _CreatedOnDate; }
            set { _CreatedOnDate = value; }
        }

        public string ChangedByUser
        {
            get { return _ChangedByUser; }
            set { _ChangedByUser = value; }
        }

        public DateTime ChangedOnDate
        {
            get { return _ChangedOnDate; }
            set { _ChangedOnDate = value; }
        }

        public bool BonusViewed
        {
            get { return _BonusViewed; }
            set { _BonusViewed = value; }
        }
        
        public double BillingPrice
        {
            get { return _BillingPrice; }
            set { _BillingPrice = value; }
        }

        public int SubscriberEnquiryResultBonusID
        {
            get { return _SubscriberEnquiryResultBonusID; }
            set { _SubscriberEnquiryResultBonusID = value; }
        }

        public string DataSegmentName
        {
            get { return _DataSegmentName; }
            set { _DataSegmentName = value; }
        }

        public int SubscriberEnquiryResultID
        {
            get { return _SubscriberEnquiryResultID; }
            set { _SubscriberEnquiryResultID = value; }
        }

        public int DataSegmentID
        {
            get { return _DataSegmentID; }
            set { _DataSegmentID = value; }
        }

        public string DataSegmentDisplayText
        {
            get { return _DataSegmentDisplayText; }
            set { _DataSegmentDisplayText = value; }
        }

    }
}
