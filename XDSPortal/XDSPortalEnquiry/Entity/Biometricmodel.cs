﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDSPortalEnquiry.Entity
{
    public class Biometricmodel
    {

        public class VerifyUserResult
        {          

            public string Message { get; set; }
            public int ResultCode { get; set; }
        }

        public class VerifyUserInfo
        {

            public string UserId { get; set; }
            public string IsoTemplateString { get; set; }
            public string WsqBufferString { get; set; }
            public int FingerIndex { get; set; }
        }

        public class FingerPrintSequenceInfo
        {
            public string IsoTemplateString { get; set; }
            public int FingerIndex { get; set; }
        }



    }
}
