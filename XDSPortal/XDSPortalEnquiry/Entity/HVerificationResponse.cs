﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDSPortalEnquiry.Entity
{
    public class HVerificationResponse
    {
        public int SubscriberEnquiryID { get; set; }
        public int SubscriberEnquiryResultID { get; set; }
        public int BioMetricID { get; set; }
        public string ApplicationErrors { get; set; }
        public string Base64StringJpeg2000Image { get; set; }
        public string FingerColor { get; set; }
        public bool HasImage { get; set; }
        public DateTime TmStamp { get; set; }
        public long TransactionNumber { get; set; }
        public string VerificationColorIndicator { get; set; }
        public string VerificationErrors { get; set; }
        public string VerificationResult { get; set; }
        public string VerificationResultDescription { get; set; }
        public string CreatedbyUser { get; set; }
        public string UrlRequested { get; set; }
        public bool Sync { get; set; }
    }
}
