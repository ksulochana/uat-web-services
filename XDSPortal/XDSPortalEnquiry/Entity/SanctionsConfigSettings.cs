﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;

namespace XDSPortalEnquiry.Entity
{
    public class SanctionsConfigSettings
    {
        public string MemberKey { get; set; }

        public string Password { get; set; }

        public string URL1 { get; set; }
        public string URL2 { get; set; }

    }
}
