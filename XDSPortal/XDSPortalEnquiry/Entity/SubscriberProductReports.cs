﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XDSPortalEnquiry.Entity
{
    [Serializable]
   public class SubscriberProductReports
    {
       
        private int _SubscriberProductReportID;
        private int _SubscriberID;
        private int _ProductID;
        private int _ReportID =0;
        private int _BillingTypeID;
        private double _UnitPrice;
        private bool _FootprintActive;
        private bool _CountryofBirthActive = false;
        private bool _ActiveUnKnownAccountType = false;
        public SubscriberProductReports()
        {
        }



        private string _ReducedWatermarkInd;
     
        public string ReducedWatermarkInd
        {
            get
            {
                return this._ReducedWatermarkInd;
            }
            set
            {
                this._ReducedWatermarkInd = value;
            }
        }
        public bool ActiveUnKnownAccountType
        {
            get { return _ActiveUnKnownAccountType; }
            set { _ActiveUnKnownAccountType = value; }
        }
        public int SubscriberProductReportID
        {
            get
            {
                return this._SubscriberProductReportID;
            }
            set
            {
                this._SubscriberProductReportID = value;
            }
        }

        public int SubscriberID
        {
            get
            {
                return this._SubscriberID;
            }
            set
            {
                this._SubscriberID = value;
            }
        }

        public int ProductID
        {
            get
            {
                return this._ProductID;
            }
            set
            {
                this._ProductID = value;
            }
        }


        public int ReportID
        {
            get
            {
                return this._ReportID;
            }
            set
            {
                this._ReportID = value;
            }
        }

        public int BillingTypeID
        {
            get
            {
                return this._BillingTypeID;
            }
            set
            {
                this._BillingTypeID = value;
            }
        }

        public double UnitPrice
        {
            get
            {
                return this._UnitPrice;
            }
            set
            {
                this._UnitPrice = value;
            }
        }

        public bool FootprintActive
        {
            get { return _FootprintActive; }
            set { _FootprintActive = value; }
        }
        public bool CountryofBirthActive
        {
            get { return _CountryofBirthActive; }
            set { _CountryofBirthActive = value; }
        }

        

    }
}
