﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDSPortalEnquiry.Entity
{
    public class DOVSetting
    {
        public string Username { get; set; }

        public string Password { get; set; }

        public string Institution { get; set; }
    }
}
