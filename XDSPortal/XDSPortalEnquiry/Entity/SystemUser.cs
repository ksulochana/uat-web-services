﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XDSPortalEnquiry.Entity
{
    [Serializable]
    public class SystemUser
    {
        
        public SystemUser()
        {

        }
        private int subscriberIDField;
        private int SystemUserIDField;
        private string SystemUserTypeIndField;
        private string FirstNameField = "";
        private string SurnameField  = ""; 
        private string IDNoField;
        private string JobPositionField;
        private string CellularCodeField;
        private string CellularNoField;
        private string EmailAddressField;
        private bool ActiveYNField;
        private string UsernameField;
        private string SystemUserRoleField;

        private string _CreatedByUser;
        private DateTime _CreatedOnDate = DateTime.Parse("1900/01/01");
        private string _ChangedByUser;
        private DateTime _ChangedOnDate = DateTime.Parse("1900/01/01");
        
        private int _StayAlive;

        public string SystemUserFullName
        {
            get { return FirstNameField + " " + SurnameField; }

        }

        public string SystemUserRole
        {
            get { return SystemUserRoleField; }
            set { SystemUserRoleField = value; }
        }


        public string Username
        {
            get { return UsernameField; }
            set { UsernameField = value; }
        }

        public bool ActiveYN
        {
            get { return ActiveYNField; }
            set { ActiveYNField = value; }
        }

        public string EmailAddress
        {
            get { return EmailAddressField; }
            set { EmailAddressField = value; }
        }

        public string CellularNo
        {
            get { return CellularNoField; }
            set { CellularNoField = value; }
        }

        public string CellularCode
        {
            get { return CellularCodeField; }
            set { CellularCodeField = value; }
        }

        public string JobPosition
        {
            get { return JobPositionField; }
            set { JobPositionField = value; }
        }

        public string IDNo
        {
            get { return IDNoField; }
            set { IDNoField = value; }
        }

        public string Surname
        {
            get { return SurnameField; }
            set { SurnameField = value; }
        }

        public string FirstName
        {
            get { return FirstNameField; }
            set { FirstNameField = value; }
        }
        
        public string SystemUserTypeInd
        {
            get { return SystemUserTypeIndField; }
            set { SystemUserTypeIndField = value; }
        }

        public int SystemUserID
        {
            get { return SystemUserIDField; }
            set { SystemUserIDField = value; }
        }

        public string CreatedByUser
        {
            get { return _CreatedByUser; }
            set { _CreatedByUser = value; }
        }

        public DateTime CreatedOnDate
        {
            get { return _CreatedOnDate; }
            set { _CreatedOnDate = value; }
        }

        public string ChangedByUser
        {
            get { return _ChangedByUser; }
            set { _ChangedByUser = value; }
        }

        public DateTime ChangedOnDate
        {
            get { return _ChangedOnDate; }
            set { _ChangedOnDate = value; }
        }

        public int SubscriberID
        {
            get
            {
                return this.subscriberIDField;
            }
            set
            {
                this.subscriberIDField = value;
            }
        }

        public int StayAlive
        {
            get
            {
                return this._StayAlive;
            }
            set
            {
                this._StayAlive = value;
            }
        }
        
    }
}
