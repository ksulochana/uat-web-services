﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDSPortalEnquiry.Entity
{
    public class BulkReport
    {
        public string IDno { get; set; }
        public string PassportNo { get; set; }
        public string SubscriberReference { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string EnquiryStatusInd { get; set; }
        public string EnquiryResultInd { get; set; }
        public string XMLData { get; set; }
        public string ErrorDescription { get; set; }
    }

    public class BulkReportInfo
    {
        public BulkReport[] BulkReportInfoData { get; set; }
        public string Error { get; set; }
    }
}
