using System;
using System.Collections.Generic;
using System.Text;

namespace XDSPortalEnquiry.Entity
{
    [Serializable]
    public class SubscriberVoucher
    {
        
        public SubscriberVoucher() { }

        private int _VoucherID;
        private string _VoucherCode;
        private DateTime _ExpiryDate = DateTime.Parse("1900/01/01");
        private int _SubscriberID;
        private int _Quantity;
        private string _VoucherType;
        private string _RequestedBy;
        private string _ApprovedBy;
        private string _Note;
        private string _CreatedByUser;
        private DateTime _CreatedOnDate = DateTime.Parse("1900/01/01");
        private string _ChangedByUser;
        private DateTime _ChangedOnDate = DateTime.Parse("1900/01/01");
        private string _ExternalReference;

        public string CreatedByUser
        {
            get { return _CreatedByUser; }
            set { _CreatedByUser = value; }
        }

        public DateTime CreatedOnDate
        {
            get { return _CreatedOnDate; }
            set { _CreatedOnDate = value; }
        }

        public string ChangedByUser
        {
            get { return _ChangedByUser; }
            set { _ChangedByUser = value; }
        }

        public DateTime ChangedOnDate
        {
            get { return _ChangedOnDate; }
            set { _ChangedOnDate = value; }
        }

        public int VoucherID
        {
            get { return _VoucherID; }
            set { _VoucherID = value; }
        }

        public int SubscriberID
        {
            get { return _SubscriberID; }
            set { _SubscriberID = value; }
        }

        public string VoucherCode
        {
            get { return _VoucherCode; }
            set { _VoucherCode = value; }
        }

        public string VoucherType
        {
            get { return _VoucherType; }
            set { _VoucherType = value; }
        }

        public string RequestedBy
        {
            get { return _RequestedBy; }
            set { _RequestedBy = value; }
        }

        public string ApprovedBy
        {
            get { return _ApprovedBy; }
            set { _ApprovedBy = value; }
        }
        
        public string Note
        {
            get { return _Note; }
            set { _Note = value; }
        }

        public int Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }
        
        public DateTime ExpiryDate
        {
            get { return _ExpiryDate; }
            set { _ExpiryDate = value; }
        }
    }
}
