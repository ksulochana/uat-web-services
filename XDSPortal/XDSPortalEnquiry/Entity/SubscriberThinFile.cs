﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;

namespace XDSPortalEnquiry.Entity
{
    [Serializable]
    public class SubscriberThinFile
    {

        public SubscriberThinFile() { }

        public string IdNumber { get; set; }
        public string PassportNo { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public int Age { get; set; }
        public string Gender { get; set; }
        public DateTime BirthDate { get; set; }
        public string ResidentialAddressLine1 { get; set; }
        public string ResidentialAddressLine2 { get; set; }
        public string ResidentialAddressLine3 { get; set; }
        public string ResidentialAddressLine4 { get; set; }
        public string ResidentialPostalCode { get; set; }
        public string PostalAddressLine1 { get; set; }
        public string PostalAddressLine2 { get; set; }
        public string PostalAddressLine3 { get; set; }
        public string PostalAddressLine4 { get; set; }
        public string PostalPostalCode { get; set; }
        public string CellNo1 { get; set; }
        public string CellNo2 { get; set; }
        public string CellNo3 { get; set; }
        public string EmailAddress { get; set; }
    }

}
