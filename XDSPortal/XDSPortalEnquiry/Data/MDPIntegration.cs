﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDSPortalEnquiry.Data
{
   public class MDPIntegration
    {
        public void GetMDPIntegrationinfo(SqlConnection cn,string ProductName)
        {
           
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            string strSQL = @"Select mp.ProductName,mpu.FunctionName,mp.MDPBaseURL,mpu.url,mpup.ParameterName,mpup.ParameterValue from MDPProducts Mp inner join MDPProductURLS Mpu left outer join MDPProductURLParameters mpup on mpu.URLID = mpup.URLID
 on mp.MDPProductID = Mpu.ProductID where mp.StatusInd = 'A' and mpu.StatusInd = 'A' and isnull(mpup.StatusInd,'A')= 'A' and mp.ProductName = '" + ProductName.ToString()+ "' order by FunctionName";

            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            string functionname = string.Empty;
            Dictionary<string, string> param = new Dictionary<string, string>();

            XDSPortalLibrary.Entity_Layer.URLInfo oUinfo = new XDSPortalLibrary.Entity_Layer.URLInfo();
            XDSPortalLibrary.Entity_Layer.MDPURLInfo.oURLinfo = new Dictionary<string, XDSPortalLibrary.Entity_Layer.URLInfo>();
            int i = 0;

            foreach (DataRow r in ds.Tables[0].Rows)
            {

                if (!string.IsNullOrEmpty(functionname) && functionname != r["FunctionName"].ToString())
                {

                    XDSPortalLibrary.Entity_Layer.MDPURLInfo.oURLinfo.Add(functionname, oUinfo);

                    functionname = r["FunctionName"].ToString();

                    oUinfo = new XDSPortalLibrary.Entity_Layer.URLInfo();
                    oUinfo.MDPBaseURL = r["MDPBaseURL"].ToString();
                    oUinfo.URL = r["url"].ToString();

                    param.Add(r["ParameterName"].ToString(), r["ParameterValue"].ToString());
                    oUinfo.Parameter = param;

                }
                else
                {
                    functionname = r["FunctionName"].ToString();
                    oUinfo.MDPBaseURL = r["MDPBaseURL"].ToString();
                    oUinfo.URL = r["url"].ToString();

                    param.Add(r["ParameterName"].ToString(), r["ParameterValue"].ToString());
                    oUinfo.Parameter = param;
                }


                if (i== (ds.Tables[0].Rows.Count )-1)
                {
                    XDSPortalLibrary.Entity_Layer.MDPURLInfo.oURLinfo.Add(functionname, oUinfo);
                }
                i++;

            }
            sqlda.Dispose();
            sqlcmd.Dispose();
            ds.Dispose();
           // File.AppendAllText(@"C:\Log\Responsejd.txt", "load:"+XDSPortalLibrary.Entity_Layer.MDPURLInfo.oURLinfo.Count().ToString());
            cn.Close();


        }

    }
}
