﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDSPortalEnquiry.Data
{
    public class XDSLog
    {


        public int InsertSubscriberLinkageInvestigationDetails(string sqlcon, Entity.SubscriberLinkageInvestigationDetails olinkagedetails)
        {
            int i = 0;

            SqlConnection ocon = new SqlConnection(sqlcon);
            SqlCommand ocmd = new SqlCommand("sp_I_SubscriberLinkageInvestigationDetails", ocon);
            ocmd.CommandType = System.Data.CommandType.StoredProcedure;

            ocmd.Parameters.AddWithValue("@HeadSubscriberEnquiryID", olinkagedetails.HeadSubscriberEnquiryID);
            ocmd.Parameters.AddWithValue("@HeadSubscriberEnquiryresultID", olinkagedetails.HeadSubscriberEnquiryresultID);
            ocmd.Parameters.AddWithValue("@EventID", olinkagedetails.EventID);
            ocmd.Parameters.AddWithValue("@SecondarySubscriberEnquiryID", olinkagedetails.SecondarySubscriberEnquiryID);
            ocmd.Parameters.AddWithValue("@SecondarySubscriberEnquiryResultID", olinkagedetails.SecondarySubscriberEnquiryResultID);
            ocmd.Parameters.AddWithValue("@KeyType", olinkagedetails.KeyType);
            ocmd.Parameters.AddWithValue("@KeyID", olinkagedetails.KeyID);
            ocmd.Parameters.AddWithValue("@LinkedID", olinkagedetails.LinkedID);
            ocmd.Parameters.AddWithValue("@LinkedName", olinkagedetails.LinkedName);
            ocmd.Parameters.AddWithValue("@LinkedNo", olinkagedetails.LinkedNo);
            ocmd.Parameters.AddWithValue("@IstheNodeExpanded", olinkagedetails.IstheNodeExpanded);
            ocmd.Parameters.AddWithValue("@LinkType", olinkagedetails.LinkType);
            ocmd.Parameters.AddWithValue("@CreatedbyUser", olinkagedetails.CreatedbyUser);
            ocmd.Parameters.AddWithValue("@Createdondate", DateTime.Now);
            ocmd.Parameters.AddWithValue("@PrimarySubscriberEnquiryID", olinkagedetails.PrimarySubscriberEnquiryID);
            ocmd.Parameters.AddWithValue("@PrimarySubscriberEnquiryResultID", olinkagedetails.PrimarySubscriberEnquiryResultID);
            ocmd.Parameters.AddWithValue("@KeyNo", olinkagedetails.KeyNo);
            ocmd.Parameters.AddWithValue("@KeyName", olinkagedetails.KeyName);
            ocmd.Parameters.AddWithValue("@InvestigationID", olinkagedetails.InvestigationID);
            ocmd.Parameters.AddWithValue("@EntityType", olinkagedetails.EntityType);
            ocmd.Parameters.AddWithValue("@LinkedEntityStatus", olinkagedetails.LinkedEntityStatus);
            ocmd.Parameters.AddWithValue("@EntityStatus", olinkagedetails.EntityStatus);
            ocmd.Parameters.AddWithValue("@LinkedEntityCategory", olinkagedetails.LinkedEntityCategory);

            SqlParameter oparam = new SqlParameter();
            oparam.DbType = System.Data.DbType.Int16;
            oparam.Direction = System.Data.ParameterDirection.ReturnValue;
            ocmd.Parameters.Add(oparam);

            if (ocon.State == System.Data.ConnectionState.Closed)
                ocon.Open();

            ocmd.ExecuteNonQuery();

            if (ocon.State == System.Data.ConnectionState.Open)
                ocon.Close();

            i = int.Parse(oparam.Value.ToString());

            return i;
        }



        public void UpdateSubscriberLinkageInvestigationDetails(string sqlcon, Entity.SubscriberLinkageInvestigationDetails olinkagedetails)
        {

            SqlConnection ocon = new SqlConnection(sqlcon);
            SqlCommand ocmd = new SqlCommand("sp_U_SubscriberLinkageInvestigationDetails", ocon);
            ocmd.CommandType = System.Data.CommandType.StoredProcedure;

            ocmd.Parameters.AddWithValue("@SubscriberLinkageInvestigationDetailsID", olinkagedetails.SubscriberLinkageInvestigationDetailsID);
            ocmd.Parameters.AddWithValue("@HeadSubscriberEnquiryID", olinkagedetails.HeadSubscriberEnquiryID);
            ocmd.Parameters.AddWithValue("@HeadSubscriberEnquiryresultID", olinkagedetails.HeadSubscriberEnquiryresultID);
            ocmd.Parameters.AddWithValue("@EventID", olinkagedetails.EventID);
            ocmd.Parameters.AddWithValue("@SecondarySubscriberEnquiryID", olinkagedetails.SecondarySubscriberEnquiryID);
            ocmd.Parameters.AddWithValue("@SecondarySubscriberEnquiryResultID", olinkagedetails.SecondarySubscriberEnquiryResultID);
            ocmd.Parameters.AddWithValue("@IstheNodeExpanded", olinkagedetails.IstheNodeExpanded);
            ocmd.Parameters.AddWithValue("@Username", olinkagedetails.CreatedbyUser);
            ocmd.Parameters.AddWithValue("@Transactiondate", olinkagedetails.Createdondate);
            ocmd.Parameters.AddWithValue("@PrimarySubscriberEnquiryID", olinkagedetails.PrimarySubscriberEnquiryID);
            ocmd.Parameters.AddWithValue("@PrimarySubscriberEnquiryResultID", olinkagedetails.PrimarySubscriberEnquiryResultID);

            if (ocon.State == System.Data.ConnectionState.Closed)
                ocon.Open();

            ocmd.ExecuteNonQuery();

            if (ocon.State == System.Data.ConnectionState.Open)
                ocon.Close();
        }


        public Entity.SubscriberLinkageInvestigationDetails GetEventInfo(string sqlcon, Entity.SubscriberLinkageInvestigationDetails olinkagedetails)
        {
            SqlConnection ocon = new SqlConnection(sqlcon);
            SqlCommand ocmd = new SqlCommand("sp_GetEvent_SubscriberLinkageInvestigationDetails", ocon);
            ocmd.CommandType = System.Data.CommandType.StoredProcedure;

            ocmd.Parameters.AddWithValue("@SecondarySubscriberEnquiryID", olinkagedetails.SecondarySubscriberEnquiryID);
            ocmd.Parameters.AddWithValue("@SecondarySubscriberEnquiryResultID", olinkagedetails.SecondarySubscriberEnquiryResultID);
            ocmd.Parameters.AddWithValue("@PrimarySubscriberEnquiryID", olinkagedetails.PrimarySubscriberEnquiryID);
            ocmd.Parameters.AddWithValue("@PrimarySubscriberEnquiryResultID", olinkagedetails.PrimarySubscriberEnquiryResultID);
            ocmd.Parameters.AddWithValue("@InvestigationID", olinkagedetails.InvestigationID);

            SqlParameter oparam = new SqlParameter("@HeadSubscriberEnquiryID", System.Data.SqlDbType.Int);
            oparam.Direction = System.Data.ParameterDirection.Output;
            ocmd.Parameters.Add(oparam);

            SqlParameter oparam1 = new SqlParameter("@HeadSubscriberEnquiryresultID", System.Data.SqlDbType.Int);
            oparam1.Direction = System.Data.ParameterDirection.Output;
            ocmd.Parameters.Add(oparam1);

            SqlParameter oparam2 = new SqlParameter("@EventID", System.Data.SqlDbType.Int);
            oparam2.Direction = System.Data.ParameterDirection.Output;
            ocmd.Parameters.Add(oparam2);


            //SqlParameter oparam3 = new SqlParameter("@InvestigationID", System.Data.SqlDbType.Int);
            //oparam3.Direction = System.Data.ParameterDirection.Output;
            //ocmd.Parameters.Add(oparam3);

            if (ocon.State == System.Data.ConnectionState.Closed)
                ocon.Open();

            ocmd.ExecuteNonQuery();

            if (ocon.State == System.Data.ConnectionState.Open)
                ocon.Close();


            olinkagedetails.HeadSubscriberEnquiryID = int.Parse(oparam.Value.ToString());
            olinkagedetails.HeadSubscriberEnquiryresultID = int.Parse(oparam1.Value.ToString());
            olinkagedetails.EventID = int.Parse(oparam2.Value.ToString());
            //olinkagedetails.InvestigationID = int.Parse(oparam3.Value.ToString());


            return olinkagedetails;

        }


        public DataSet GetInvestigationHistory(string sqlcon, Entity.SubscriberLinkageInvestigationDetails olinkagedetails)
        {

            //  DataTable dt = new DataTable();
            SqlConnection ocon = new SqlConnection(sqlcon);
            SqlCommand ocmd = new SqlCommand("sp_Get_SubscriberLinkageInvestigationDetails", ocon);
            ocmd.CommandType = System.Data.CommandType.StoredProcedure;

            ocmd.Parameters.AddWithValue("@SubscriberEnquiryID", olinkagedetails.PrimarySubscriberEnquiryID);
            ocmd.Parameters.AddWithValue("@SubscriberEnquiryresultID", olinkagedetails.PrimarySubscriberEnquiryResultID);

            SqlDataAdapter oDa = new SqlDataAdapter(ocmd);
            DataSet ds = new DataSet("InvestigationHistory");
            oDa.Fill(ds);

            if (ds.Tables.Count > 0)
            {
                ds.Tables[0].TableName = "InvestigationInfo";

                if (ds.Tables.Count > 1)
                {
                    ds.Tables[1].TableName = "LinkageDetails";
                }

                if (ds.Tables.Count > 2)
                {
                    ds.Tables[2].TableName = "SubscriberBillingInfo";
                }
                // dt = ds.Tables[0].Copy();
            }


            return ds;

        }

        public int InsertSubscriberLinkageInvestigation(string sqlcon, Entity.SubscriberLinkageInvestigationDetails olinkagedetails)
        {
            int i = 0;

            SqlConnection ocon = new SqlConnection(sqlcon);
            SqlCommand ocmd = new SqlCommand("sp_I_SubscriberCommercialLinkageInvestigation", ocon);
            ocmd.CommandType = System.Data.CommandType.StoredProcedure;

            ocmd.Parameters.AddWithValue("@SubscriberID", olinkagedetails.SubscriberID);
            ocmd.Parameters.AddWithValue("@SubscriberName", olinkagedetails.SubscriberName);
            ocmd.Parameters.AddWithValue("@SystemuserID", olinkagedetails.SystemuserID);
            ocmd.Parameters.AddWithValue("@SystemUsername", olinkagedetails.SystemUsername);
            ocmd.Parameters.AddWithValue("@SubscriberReference", olinkagedetails.SubscriberReference);
            ocmd.Parameters.AddWithValue("@InvestigationReference", olinkagedetails.InvestigationReference);
            ocmd.Parameters.AddWithValue("@InvestigationStatus", olinkagedetails.InvestigationStatus.ToString());
            ocmd.Parameters.AddWithValue("@CreatedbyUser", olinkagedetails.CreatedbyUser);
            ocmd.Parameters.AddWithValue("@Createdondate", olinkagedetails.Createdondate);


            SqlParameter oparam = new SqlParameter();
            oparam.DbType = System.Data.DbType.Int16;
            oparam.Direction = System.Data.ParameterDirection.ReturnValue;
            ocmd.Parameters.Add(oparam);

            if (ocon.State == System.Data.ConnectionState.Closed)
                ocon.Open();

            ocmd.ExecuteNonQuery();

            if (ocon.State == System.Data.ConnectionState.Open)
                ocon.Close();

            i = int.Parse(oparam.Value.ToString());

            return i;
        }

        public int InsertSubscriberLinkageInvestigationError(string sqlcon, Entity.SubscriberLinkageInvestigationDetails olinkagedetails)
        {
            int i = 0;

            SqlConnection ocon = new SqlConnection(sqlcon);
            SqlCommand ocmd = new SqlCommand("sp_I_SubscriberCommercialLinkageInvestigationError", ocon);
            ocmd.CommandType = System.Data.CommandType.StoredProcedure;

            ocmd.Parameters.AddWithValue("@SubscriberID", olinkagedetails.SubscriberID);
            ocmd.Parameters.AddWithValue("@SubscriberName", olinkagedetails.SubscriberName);
            ocmd.Parameters.AddWithValue("@SystemuserID", olinkagedetails.SystemuserID);
            ocmd.Parameters.AddWithValue("@SystemUsername", olinkagedetails.SystemUsername);
            ocmd.Parameters.AddWithValue("@InvestigationReference", olinkagedetails.InvestigationReference);
            ocmd.Parameters.AddWithValue("@InvestigationStatus", olinkagedetails.InvestigationStatus.ToString());
            ocmd.Parameters.AddWithValue("@CreatedbyUser", olinkagedetails.CreatedbyUser);
            ocmd.Parameters.AddWithValue("@Createdondate", olinkagedetails.Createdondate);
            ocmd.Parameters.AddWithValue("@ErrorDescription", olinkagedetails.ErrorDescription);
            ocmd.Parameters.AddWithValue("@SubscriberReference", olinkagedetails.SubscriberReference);


            SqlParameter oparam = new SqlParameter();
            oparam.DbType = System.Data.DbType.Int16;
            oparam.Direction = System.Data.ParameterDirection.ReturnValue;
            ocmd.Parameters.Add(oparam);

            if (ocon.State == System.Data.ConnectionState.Closed)
                ocon.Open();

            ocmd.ExecuteNonQuery();

            if (ocon.State == System.Data.ConnectionState.Open)
                ocon.Close();

            i = int.Parse(oparam.Value.ToString());

            return i;
        }

        public void UpdateSubscriberLinkageInvestigation(string sqlcon, Entity.SubscriberLinkageInvestigationDetails olinkagedetails)
        {


            SqlConnection ocon = new SqlConnection(sqlcon);
            SqlCommand ocmd = new SqlCommand("sp_U_SubscriberCommercialLinkageInvestigation", ocon);
            ocmd.CommandType = System.Data.CommandType.StoredProcedure;

            ocmd.Parameters.AddWithValue("@InvestigationID", olinkagedetails.InvestigationID);
            ocmd.Parameters.AddWithValue("@InvestigationStatus", olinkagedetails.InvestigationStatus.ToString());
            ocmd.Parameters.AddWithValue("@CreatedbyUser", olinkagedetails.CreatedbyUser);
            ocmd.Parameters.AddWithValue("@Createdondate", olinkagedetails.Createdondate);
            ocmd.Parameters.AddWithValue("@ErrorDescription", olinkagedetails.ErrorDescription);


            if (ocon.State == System.Data.ConnectionState.Closed)
                ocon.Open();

            ocmd.ExecuteNonQuery();

            if (ocon.State == System.Data.ConnectionState.Open)
                ocon.Close();


        }


        public Entity.SubscriberLinkageInvestigationDetails GetSubscriberLinkageInvestigation(string sqlcon, int investigationid)
        {

            Entity.SubscriberLinkageInvestigationDetails olinkagedetails = new Entity.SubscriberLinkageInvestigationDetails();

            SqlConnection ocon = new SqlConnection(sqlcon);
            SqlCommand ocmd = new SqlCommand("Select * from SubscriberCommercialLinkageInvestigation nolock where InvestigationID = @InvestigationID", ocon);
            ocmd.CommandType = System.Data.CommandType.Text;

            ocmd.Parameters.AddWithValue("@InvestigationID", investigationid);

            DataSet ds = new DataSet();
            SqlDataAdapter oda = new SqlDataAdapter(ocmd);
            oda.Fill(ds);

            if (ds != null)
            {
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            olinkagedetails.InvestigationID = int.Parse(dr["InvestigationID"].ToString());
                            olinkagedetails.SystemuserID = int.Parse(dr["SystemuserID"].ToString());
                            olinkagedetails.SubscriberID = int.Parse(dr["SubscriberID"].ToString());
                            olinkagedetails.SubscriberName = dr["SubscriberName"].ToString();
                            olinkagedetails.SystemUsername = dr["SystemUsername"].ToString();
                            olinkagedetails.InvestigationReference = dr["InvestigationReference"].ToString();
                            olinkagedetails.InvestigationStatus = dr["InvestigationStatus"].ToString() == "C" ? Entity.SubscriberLinkageInvestigationDetails.StatusInd.C : (dr["InvestigationStatus"].ToString() == "P" ? Entity.SubscriberLinkageInvestigationDetails.StatusInd.P : (dr["InvestigationStatus"].ToString() == "F" ? Entity.SubscriberLinkageInvestigationDetails.StatusInd.F : Entity.SubscriberLinkageInvestigationDetails.StatusInd.N));
                            olinkagedetails.SubscriberReference = dr["SubscriberReference"].ToString();
                            olinkagedetails.Createdondate = DateTime.Parse(dr["Createdondate"].ToString(), System.Globalization.CultureInfo.InvariantCulture,System.Globalization.DateTimeStyles.AssumeLocal);
                        }
                    }
                }
            }

            return olinkagedetails;

        }

        public int InsertSubscriberLinkageDetectorEnquiry(string sqlcon, Entity.SubscriberLinkageDetectorEnquiry olinkagedetails)
        {
            int i = 0;

            SqlConnection ocon = new SqlConnection(sqlcon);
            SqlCommand ocmd = new SqlCommand("sp_I_SubscriberLinkageDetectorEnquiry", ocon);
            ocmd.CommandType = System.Data.CommandType.StoredProcedure;

            ocmd.Parameters.AddWithValue("@InvestigationID", olinkagedetails.InvestigationID);
            ocmd.Parameters.AddWithValue("@LinkSubscriberEnquiryID", olinkagedetails.LinkSubscriberEnquiryID);
            ocmd.Parameters.AddWithValue("@LinkSubscriberEnquiryresultID", olinkagedetails.LinkSubscriberEnquiryResultID);
            ocmd.Parameters.AddWithValue("@LinkID", olinkagedetails.LinkID);
            ocmd.Parameters.AddWithValue("@ProductID", olinkagedetails.ProductID);
            ocmd.Parameters.AddWithValue("@SubscriberEnquiryID", olinkagedetails.SubscriberEnquiryID);
            ocmd.Parameters.AddWithValue("@SubscriberEnquiryResultID", olinkagedetails.SubscriberEnquiryResultID);
            ocmd.Parameters.AddWithValue("@Status", olinkagedetails.Status);
            ocmd.Parameters.AddWithValue("@XMLData", olinkagedetails.Xmldata);
            ocmd.Parameters.AddWithValue("@CreatedbyUser", olinkagedetails.CreatedbyUser);
            ocmd.Parameters.AddWithValue("@Createdondate", olinkagedetails.Createdondate);
            ocmd.Parameters.AddWithValue("@PrimaryEnquiryResultID", olinkagedetails.PrimaryEnquiryResultID);


            SqlParameter oparam = new SqlParameter();
            oparam.DbType = System.Data.DbType.Int16;
            oparam.Direction = System.Data.ParameterDirection.ReturnValue;
            ocmd.Parameters.Add(oparam);

            if (ocon.State == System.Data.ConnectionState.Closed)
                ocon.Open();

            ocmd.ExecuteNonQuery();

            if (ocon.State == System.Data.ConnectionState.Open)
                ocon.Close();

            i = int.Parse(oparam.Value.ToString());

            return i;
        }

        public int InsertSubscriberLinkageEvents(string sqlcon, Entity.LinkageEvents olinkageevents)
        {
            int i = 0;
            // string strsql = string.Format("sp_I_LinkageEvents {0},{1},{2},{3},'{4}','{5}','{6}'", olinkageevents.InvestigationID, olinkageevents.SubscriberEnquiryID, olinkageevents.SubscriberEnquiryResultID, olinkageevents.ProductID, olinkageevents.Response.Replace("'", "''"), olinkageevents.CreatedbyUser.Replace("'", "''"), olinkageevents.Createdondate);

            // File.AppendAllText(@"C:\Log\CommercialLinkages.txt", "GetCommercialReport:" + strsql + "\n");

            SqlConnection ocon = new SqlConnection(sqlcon);
            //SqlCommand ocmd = new SqlCommand(strsql, ocon);
            //ocmd.CommandType = CommandType.Text;
            SqlCommand ocmd = new SqlCommand("sp_I_LinkageEvents", ocon);
            ocmd.CommandType = System.Data.CommandType.StoredProcedure;

            ocmd.Parameters.AddWithValue("@InvestigationID", olinkageevents.InvestigationID);
            ocmd.Parameters.AddWithValue("@SubscriberEnquiryID", olinkageevents.SubscriberEnquiryID);
            ocmd.Parameters.AddWithValue("@SubscriberEnquiryResultID", olinkageevents.SubscriberEnquiryResultID);
            ocmd.Parameters.AddWithValue("@ProductID", olinkageevents.ProductID);
            ocmd.Parameters.AddWithValue("@ResultSubscriberEnquiryID", olinkageevents.ResultSubscriberEnquiryID);
            ocmd.Parameters.AddWithValue("@ResultSubscriberEnquiryResultID", olinkageevents.ResultSubscriberEnquiryResultID);
            ocmd.Parameters.AddWithValue("@Response", olinkageevents.Response);
            ocmd.Parameters.AddWithValue("@ExtractType", olinkageevents.ExtractType);
            ocmd.Parameters.AddWithValue("@CreatedbyUser", olinkageevents.CreatedbyUser);
            ocmd.Parameters.AddWithValue("@Createdondate", olinkageevents.Createdondate);
            ocmd.Parameters.AddWithValue("@Billable", olinkageevents.Billable);
            ocmd.Parameters.AddWithValue("@BillingPrice", olinkageevents.BillingPrice);

            SqlParameter oparam = new SqlParameter();
            oparam.DbType = System.Data.DbType.Int16;
            oparam.Direction = System.Data.ParameterDirection.ReturnValue;
            ocmd.Parameters.Add(oparam);

            if (ocon.State == System.Data.ConnectionState.Closed)
                ocon.Open();

            ocmd.ExecuteNonQuery();

            if (ocon.State == System.Data.ConnectionState.Open)
                ocon.Close();

            i = int.Parse(oparam.Value.ToString());

            return i;
        }

        public Entity.LinkageEvents GetReportEnquiryInfo(string sqlcon, Entity.LinkageEvents olinkageevents)
        {
            SqlConnection ocon = new SqlConnection(sqlcon);
            SqlCommand ocmd = new SqlCommand("sp_Get_LinkageEvents", ocon);
            ocmd.CommandType = System.Data.CommandType.StoredProcedure;

            ocmd.Parameters.AddWithValue("@InvestigationID", olinkageevents.InvestigationID);
            ocmd.Parameters.AddWithValue("@SubscriberEnquiryID", olinkageevents.SubscriberEnquiryID);
            ocmd.Parameters.AddWithValue("@SubscriberEnquiryResultID", olinkageevents.SubscriberEnquiryResultID);
            ocmd.Parameters.AddWithValue("@ProductID", olinkageevents.ProductID);

            SqlParameter oparam = new SqlParameter("@ResultSubscriberEnquiryID", System.Data.SqlDbType.Int);
            oparam.Direction = System.Data.ParameterDirection.Output;
            ocmd.Parameters.Add(oparam);

            SqlParameter oparam1 = new SqlParameter("@ResultSubscriberEnquiryResultID", System.Data.SqlDbType.Int);
            oparam1.Direction = System.Data.ParameterDirection.Output;
            ocmd.Parameters.Add(oparam1);

            SqlParameter oparam2 = new SqlParameter("@HistoryChanged", System.Data.SqlDbType.Bit);
            oparam2.Direction = System.Data.ParameterDirection.Output;
            ocmd.Parameters.Add(oparam2);

            if (ocon.State == System.Data.ConnectionState.Closed)
                ocon.Open();

            //ocmd.ExecuteNonQuery();

            SqlDataReader odr = ocmd.ExecuteReader();

            odr.Read();
            olinkageevents.TotalPrice = Convert.ToDouble(odr.GetValue(0));
            odr.Close();

            if (ocon.State == System.Data.ConnectionState.Open)
                ocon.Close();

            //File.AppendAllText(@"C:\Log\History.txt", oparam2.Value.ToString());

            //File.AppendAllText(@"C:\Log\History.txt", olinkageevents.HistoryChanged.ToString());

            olinkageevents.ResultSubscriberEnquiryID = int.Parse(oparam.Value.ToString());
            olinkageevents.ResultSubscriberEnquiryResultID = int.Parse(oparam1.Value.ToString());
            olinkageevents.HistoryChanged = bool.Parse(oparam2.Value.ToString());

            if (olinkageevents.ProductID == 12 || olinkageevents.ProductID == 40 || olinkageevents.ProductID == 41 || olinkageevents.ProductID == 69 || olinkageevents.ProductID == 90 || olinkageevents.ProductID == 14)
            {
                if (olinkageevents.ResultSubscriberEnquiryID > 0 && olinkageevents.ResultSubscriberEnquiryResultID > 0)
                {
                    olinkageevents.Billable = false;
                    olinkageevents.BillingPrice = 0;
                }
            }

            if (olinkageevents.ProductID == 135)
            {
                if (olinkageevents.ResultSubscriberEnquiryID > 0 && olinkageevents.ResultSubscriberEnquiryResultID > 0 && !olinkageevents.HistoryChanged)
                {
                    olinkageevents.Billable = false;
                    olinkageevents.BillingPrice = 0;
                }
            }
            return olinkageevents;

        }


        public Entity.SubscriberLinkageDetectorEnquiry GetLinkageDetectorReport(string sqlcon, Entity.SubscriberLinkageDetectorEnquiry olinkagedetectorevents)
        {
            SqlConnection ocon = new SqlConnection(sqlcon);
            SqlCommand ocmd = new SqlCommand("sp_Get_LinkageDetectorReport", ocon);
            ocmd.CommandType = System.Data.CommandType.StoredProcedure;

            ocmd.Parameters.AddWithValue("@InvestigationID", olinkagedetectorevents.InvestigationID);
            ocmd.Parameters.AddWithValue("@SubscriberEnquiryID", olinkagedetectorevents.LinkSubscriberEnquiryID);
            ocmd.Parameters.AddWithValue("@SubscriberEnquiryResultID", olinkagedetectorevents.LinkSubscriberEnquiryResultID);
            ocmd.Parameters.AddWithValue("@ProductID", olinkagedetectorevents.ProductID);
            ocmd.Parameters.AddWithValue("@PrimaryEnquiryResultID", olinkagedetectorevents.PrimaryEnquiryResultID);

            SqlParameter oparam = new SqlParameter("@ResultSubscriberEnquiryID", System.Data.SqlDbType.Int);
            oparam.Direction = System.Data.ParameterDirection.Output;
            ocmd.Parameters.Add(oparam);

            SqlParameter oparam1 = new SqlParameter("@ResultSubscriberEnquiryResultID", System.Data.SqlDbType.Int);
            oparam1.Direction = System.Data.ParameterDirection.Output;
            ocmd.Parameters.Add(oparam1);

            SqlParameter oparam2 = new SqlParameter("@XMLData", System.Data.SqlDbType.VarChar, -1);
            oparam2.Direction = System.Data.ParameterDirection.Output;
            ocmd.Parameters.Add(oparam2);

            SqlParameter oparam3 = new SqlParameter("@ResultPrimaryEnquiryResultID", System.Data.SqlDbType.Int);
            oparam3.Direction = System.Data.ParameterDirection.Output;
            ocmd.Parameters.Add(oparam3);

            if (ocon.State == System.Data.ConnectionState.Closed)
                ocon.Open();

            ocmd.ExecuteNonQuery();

            if (ocon.State == System.Data.ConnectionState.Open)
                ocon.Close();


            olinkagedetectorevents.SubscriberEnquiryID = int.Parse(oparam.Value.ToString());
            olinkagedetectorevents.SubscriberEnquiryResultID = int.Parse(oparam1.Value.ToString());
            olinkagedetectorevents.Xmldata = oparam2.Value.ToString();
            olinkagedetectorevents.ResultPrimaryEnquiryResultID = int.Parse(oparam3.Value.ToString());

            return olinkagedetectorevents;

        }

        public Entity.SubscriberLinkageInvestigationDetails InsertSubscriberLinkageInvestigationBillingDetails(string sqlcon, Entity.SubscriberLinkageInvestigationDetails olinkagedetails)
        {

            SqlConnection ocon = new SqlConnection(sqlcon);
            SqlCommand ocmd = new SqlCommand("sp_I_SubscriberLinkageInvestigationBillingDetails", ocon);
            ocmd.CommandType = System.Data.CommandType.StoredProcedure;

            ocmd.Parameters.AddWithValue("@EventID", olinkagedetails.EventID);
            ocmd.Parameters.AddWithValue("@CreatedbyUser", olinkagedetails.CreatedbyUser);
            ocmd.Parameters.AddWithValue("@Createdondate", DateTime.Now);
            ocmd.Parameters.AddWithValue("@PrimarySubscriberEnquiryID", olinkagedetails.PrimarySubscriberEnquiryID);
            ocmd.Parameters.AddWithValue("@PrimarySubscriberEnquiryResultID", olinkagedetails.PrimarySubscriberEnquiryResultID);
            ocmd.Parameters.AddWithValue("@InvestigationID", olinkagedetails.InvestigationID);
            ocmd.Parameters.AddWithValue("@Billable", olinkagedetails.Billable ? "1" : "0");
            ocmd.Parameters.AddWithValue("@DetailsViewedYN", olinkagedetails.DetailsViewedYN ? "1" : "0");
            ocmd.Parameters.AddWithValue("@DetailsViewedDate", DateTime.Now);
            ocmd.Parameters.AddWithValue("@BillingTypeID", olinkagedetails.BillingTypeID);
            ocmd.Parameters.AddWithValue("@BillingPrice", olinkagedetails.BillingPrice);



            //SqlParameter oparam = new SqlParameter("@TotalPrice", System.Data.DbType.String);
            ////oparam.Precision = 18;
            ////oparam.Scale = 2;
            ////oparam.DbType = System.Data.DbType.Int16;
            //oparam.Direction = System.Data.ParameterDirection.Output;
            //ocmd.Parameters.Add(oparam);

            if (ocon.State == System.Data.ConnectionState.Closed)
                ocon.Open();

            // ocmd.ExecuteNonQuery();

            SqlDataReader odr = ocmd.ExecuteReader();
            odr.Read();
            olinkagedetails.TotalPrice = Convert.ToDouble(odr.GetValue(0));
            odr.Close();

            if (ocon.State == System.Data.ConnectionState.Open)
                ocon.Close();
            // File.AppendAllText(@"C:\log\link.txt", ocmd.Parameters["@TotalPrice"].Value.ToString() );
            //File.AppendAllText(@"C:\log\link.txt", oparam.Value.ToString());
            // olinkagedetails.TotalPrice = double.Parse(oparam.Value.ToString());

            return olinkagedetails;
        }

        public DataSet GetSubscriberInvestigationHistory(string sqlcon, int subscriberID, int systemUserID)
        {

            //  DataTable dt = new DataTable();
            SqlConnection ocon = new SqlConnection(sqlcon);
            SqlCommand ocmd = new SqlCommand("sp_Get_SubscriberInvestigationHistory", ocon);
            ocmd.CommandType = System.Data.CommandType.StoredProcedure;

            ocmd.Parameters.AddWithValue("@SubscriberID", subscriberID);
            ocmd.Parameters.AddWithValue("@SystemUserID", systemUserID);

            SqlDataAdapter oDa = new SqlDataAdapter(ocmd);
            DataSet ds = new DataSet("SubscriberInvestigationHistory");
            oDa.Fill(ds);

            if (ds.Tables.Count > 0)
            {
                ds.Tables[0].TableName = "Investigation";
            }


            return ds;

        }

        public DataSet GetInvestigationDetails(string sqlcon, int InvestigationID)
        {

            //  DataTable dt = new DataTable();
            SqlConnection ocon = new SqlConnection(sqlcon);
            SqlCommand ocmd = new SqlCommand("sp_Get_SubscriberInvestigationInfo", ocon);
            ocmd.CommandType = System.Data.CommandType.StoredProcedure;

            ocmd.Parameters.AddWithValue("@InvestigationID", InvestigationID);

            SqlDataAdapter oDa = new SqlDataAdapter(ocmd);
            DataSet ds = new DataSet("Linkages");
            oDa.Fill(ds);

            if (ds.Tables.Count > 0)
            {
                ds.Tables[0].TableName = "InvestigationInfo";

                if (ds.Tables.Count > 1)
                {
                    ds.Tables[1].TableName = "LinkageDetails";
                }

                if (ds.Tables.Count > 2)
                {
                    ds.Tables[2].TableName = "SubscriberBillingInfo";
                }
                if (ds.Tables.Count > 3)
                {
                    ds.Tables[3].TableName = "SubscriberInputDetails";
                }
                // dt = ds.Tables[0].Copy();
            }


            return ds;

        }

        public int InsertSubscriberInputDetails(string sqlcon, Entity.SubscriberInputDetails oSubscriberInputDetails)
        {
            int i = 0;
            // string strsql = string.Format("sp_I_LinkageEvents {0},{1},{2},{3},'{4}','{5}','{6}'", olinkageevents.InvestigationID, olinkageevents.SubscriberEnquiryID, olinkageevents.SubscriberEnquiryResultID, olinkageevents.ProductID, olinkageevents.Response.Replace("'", "''"), olinkageevents.CreatedbyUser.Replace("'", "''"), olinkageevents.Createdondate);

            // File.AppendAllText(@"C:\Log\CommercialLinkages.txt", "GetCommercialReport:" + strsql + "\n");

            SqlConnection ocon = new SqlConnection(sqlcon);
            //SqlCommand ocmd = new SqlCommand(strsql, ocon);
            //ocmd.CommandType = CommandType.Text;
            SqlCommand ocmd = new SqlCommand("sp_I_SubscriberCommercialLinkageInvestigationSubscriberInputDetails", ocon);
            ocmd.CommandType = System.Data.CommandType.StoredProcedure;

            ocmd.Parameters.AddWithValue("@InvestigationID", oSubscriberInputDetails.InvestigationID);
            ocmd.Parameters.AddWithValue("@SubscriberID", oSubscriberInputDetails.SubscriberID);
            ocmd.Parameters.AddWithValue("@SubscriberName", oSubscriberInputDetails.SubscriberName);
            ocmd.Parameters.AddWithValue("@SystemuserID", oSubscriberInputDetails.SystemuserID);
            ocmd.Parameters.AddWithValue("@SystemUsername", oSubscriberInputDetails.SystemUsername);
            ocmd.Parameters.AddWithValue("@SubscriberReference", oSubscriberInputDetails.SubscriberReference);
            ocmd.Parameters.AddWithValue("@EnquiryDate", oSubscriberInputDetails.EnquiryDate);
            ocmd.Parameters.AddWithValue("@EnquiryType", oSubscriberInputDetails.EnquiryType);
            ocmd.Parameters.AddWithValue("@EnquiryInput", oSubscriberInputDetails.EnquiryInput);
            ocmd.Parameters.AddWithValue("@EnquiryID", oSubscriberInputDetails.EnquiryID);
            ocmd.Parameters.AddWithValue("@EnquiryResultID", oSubscriberInputDetails.EnquiryResultID);
            ocmd.Parameters.AddWithValue("@EntityType", oSubscriberInputDetails.EntityType);
            ocmd.Parameters.AddWithValue("@EntityNo", oSubscriberInputDetails.EntityNo);
            ocmd.Parameters.AddWithValue("@EntityName", oSubscriberInputDetails.EntityName);
            ocmd.Parameters.AddWithValue("@ProductID", oSubscriberInputDetails.ProductID);
            ocmd.Parameters.AddWithValue("@CreatedbyUser", oSubscriberInputDetails.CreatedbyUser);

            SqlParameter oparam = new SqlParameter();
            oparam.DbType = System.Data.DbType.Int16;
            oparam.Direction = System.Data.ParameterDirection.ReturnValue;
            ocmd.Parameters.Add(oparam);

            if (ocon.State == System.Data.ConnectionState.Closed)
                ocon.Open();

            ocmd.ExecuteNonQuery();

            if (ocon.State == System.Data.ConnectionState.Open)
                ocon.Close();

            i = int.Parse(oparam.Value.ToString());

            return i;
        }

        public DataSet GetLatestReportExtract(string sqlcon, Entity.LinkageEvents oevents)
        {
            SqlConnection ocon = new SqlConnection(sqlcon);
            SqlCommand ocmd = new SqlCommand("sp_Get_LatestReport", ocon);
            ocmd.CommandType = System.Data.CommandType.StoredProcedure;

            ocmd.Parameters.AddWithValue("@InvestigationID", oevents.InvestigationID);

            SqlParameter oparam = new SqlParameter("@SubscriberEnquiryID", System.Data.SqlDbType.Int);
            oparam.Direction = System.Data.ParameterDirection.Output;
            ocmd.Parameters.Add(oparam);

            SqlParameter oparam1 = new SqlParameter("@SubscriberEnquiryResultID", System.Data.SqlDbType.Int);
            oparam1.Direction = System.Data.ParameterDirection.Output;
            ocmd.Parameters.Add(oparam1);

            SqlParameter oparam2 = new SqlParameter("@XMLData", System.Data.SqlDbType.VarChar, -1);
            oparam2.Direction = System.Data.ParameterDirection.Output;
            ocmd.Parameters.Add(oparam2);

            SqlParameter oparam3 = new SqlParameter("@ProductID", System.Data.SqlDbType.Int);
            oparam3.Direction = System.Data.ParameterDirection.Output;
            ocmd.Parameters.Add(oparam3);

            if (ocon.State == System.Data.ConnectionState.Closed)
                ocon.Open();

            ocmd.ExecuteNonQuery();

            if (ocon.State == System.Data.ConnectionState.Open)
                ocon.Close();

            DataSet ds = new DataSet("Report");

            DataTable dt = new DataTable("ReportDetail");

            DataColumn dc1 = new DataColumn("ProductID", typeof(string));
            DataColumn dc2 = new DataColumn("EnquiryID", typeof(string));
            DataColumn dc3 = new DataColumn("EnquiryResultID", typeof(string));
            DataColumn dc4 = new DataColumn("ReportData", typeof(string));

            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);
            dt.Columns.Add(dc3);
            dt.Columns.Add(dc4);

            DataRow dr = dt.NewRow();



            oevents.SubscriberEnquiryID = int.Parse(oparam.Value.ToString());
            oevents.SubscriberEnquiryResultID = int.Parse(oparam1.Value.ToString());
            oevents.Response = oparam2.Value.ToString();
            oevents.ProductID = int.Parse(oparam3.Value.ToString());

            dr["EnquiryID"] = oevents.SubscriberEnquiryID;
            dr["EnquiryResultID"] = oevents.SubscriberEnquiryResultID;
            dr["ProductID"] = oevents.ProductID;
            dr["ReportData"] = oevents.Response;


            dt.Rows.Add(dr);
            ds.Tables.Add(dt);

            return ds;

        }

    }
}
