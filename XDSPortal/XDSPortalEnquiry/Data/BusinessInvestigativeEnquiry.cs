﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Xml.Serialization;

namespace XDSPortalEnquiry.Data
{
    public class BusinessInvestigativeEnquiry
    {
        public BusinessInvestigativeEnquiry()
        {
        }

        public DataSet GetBICountryCode(SqlConnection con)
        {
            //if (Enquirycon.State == ConnectionState.Closed)
            //    Enquirycon.Open();

            //string strSQL = "SELECT top 1  ServerConnectionString nolock FROM dbo.EnquiryServers where Active = 1";
            //SqlCommand cmdconstr = new SqlCommand(strSQL, Enquirycon);
            //SqlDataReader dr = cmdconstr.ExecuteReader();
            //dr.Read();
            //string strconstring = dr.GetValue(0).ToString();
            //dr.Close();

            //Enquirycon.Close();

            //SqlConnection con = new SqlConnection(strconstring);

            if (con.State == ConnectionState.Closed)
            con.Open();

            DataSet ds = new DataSet("BICountryCodes");
            string strSQL = "Select CountryID,CountryDescription from BICountryCodes where RecordStatusInd = 'A'";
            SqlCommand cmd = new SqlCommand(strSQL, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);

            foreach (DataTable dt in ds.Tables)
            {
                dt.TableName = "Country";
            }

            con.Close();
            return ds;
        }

        public DataSet GetBIReport(SqlConnection con, int CountryID)
        {
            //if (Enquirycon.State == ConnectionState.Closed)
            //    Enquirycon.Open();

            //string strSQL = "SELECT top 1  ServerConnectionString nolock FROM dbo.EnquiryServers where Active = 1";
            //SqlCommand cmdconstr = new SqlCommand(strSQL, Enquirycon);
            //SqlDataReader dr = cmdconstr.ExecuteReader();
            //dr.Read();
            //string strconstring = dr.GetValue(0).ToString();
            //dr.Close();

            //Enquirycon.Close();

            //SqlConnection con = new SqlConnection(strconstring);

            if (con.State == ConnectionState.Closed)
            con.Open();

            DataSet ds = new DataSet("BIReportTypes");
            string strSQL = "Select a.CountryReportID ReportID, b.ReportName from BICountryReports a (nolock) inner join BIReports b (nolock) on a.ReportID = b.ReportID where a.CountryID = " + CountryID.ToString() + " and a.Active = 1 and b.RecordStatusInd = 'A'";
            SqlCommand cmd = new SqlCommand(strSQL, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            foreach (DataTable dt in ds.Tables)
            {

                dt.TableName = "Reports";
            }
            con.Close();
            return ds;
        }

        public DataSet GetBIReportTimeFrames(SqlConnection con, int ReportID)
        {
            //if (Enquirycon.State == ConnectionState.Closed)
            //    Enquirycon.Open();

            //string strSQL = "SELECT top 1  ServerConnectionString nolock FROM dbo.EnquiryServers where Active = 1";
            //SqlCommand cmdconstr = new SqlCommand(strSQL, Enquirycon);
            //SqlDataReader dr = cmdconstr.ExecuteReader();
            //dr.Read();
            //string strconstring = dr.GetValue(0).ToString();
            //dr.Close();

            //Enquirycon.Close();

            //SqlConnection con = new SqlConnection(strconstring);

            if (con.State == ConnectionState.Closed)
            con.Open();

            DataSet ds = new DataSet("BIReportTimeFrames");
            //string strSQL = "Select a.BIReportTimeFrameID, b.TimeFrameDesc from BIReportTimeFrame a (nolock) inner join BITimeFrames b (nolock) on a.TimeFrameID = b.TimeFrameID where a.ReportID = " + ReportID.ToString() + " and a.Active = 1 and b.RecordStatusInd = 'A'";
            //string strSQL = "Select a.BICountryReportTimeFrameID, b.TimeFrameDesc from BICountryReportTimeFrame a (nolock) 	inner join BITimeFrames b (nolock) on a.TimeFrameID = b.TimeFrameID where 	a.CountryID = " + CountryID.ToString() + " and	a.ReportID = " + ReportID.ToString() + " and a.Active = 1 and b.RecordStatusInd = 'A'";
            string strSQL = "Select a.BICountryReportTimeFrameID, b.TimeFrameDesc from BICountryReportTimeFrame a (nolock) inner join BITimeFrames b (nolock) on a.TimeFrameID = b.TimeFrameID inner join BIReports C (nolock) on a.ReportID = c.ReportID inner join BICountryCodes d (nolock) on a.CountryID = d.CountryID inner join BICountryReports e on a.CountryID = e.CountryID and a.ReportID = e.ReportID where e.CountryreportID =  " + ReportID.ToString() + " and a.Active = 1 and b.RecordStatusInd = 'A'";
            SqlCommand cmd = new SqlCommand(strSQL, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            foreach (DataTable dt in ds.Tables)
            {
                dt.TableName = "TimeFrames";
            }
            con.Close();
            return ds;
        }

        public int GetBICommercialID(SqlConnection con, int EnquiryLogID)
        {
            if (con.State == ConnectionState.Closed)
                con.Open();

            DataSet ds = new DataSet();
            string strSQL = "select distinct isnull(CommercialID,0) CommercialID from TraceMatch where EnquiryLogID=" + EnquiryLogID + " and Status='Viewed'";
            SqlCommand cmd = new SqlCommand(strSQL, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);

            con.Close();

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count == 1)
                return Convert.ToInt32(ds.Tables[0].Rows[0]["CommercialID"].ToString());
            else
                return 0;
        }

        public int InsertCustomisedReportAttachment(SqlConnection con, int SubscriberEnquiryLogID, string Attachment, string AttachmentFilename)
        {
            int added = 0;

            try
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();

                DataSet ds = new DataSet();
                string strSQL = "sp_InsertCommercialCustomisedReportAttachment";
                SqlCommand cmd = new SqlCommand(strSQL, con);
                cmd.CommandType = CommandType.StoredProcedure;
                byte[] file = Convert.FromBase64String(Attachment);

                cmd.Parameters.AddWithValue("@SubscriberEnquiryLogID", SubscriberEnquiryLogID);
                cmd.Parameters.AddWithValue("@Attachment", file);
                cmd.Parameters.AddWithValue("@AttachmentFilename", AttachmentFilename);

                cmd.ExecuteNonQuery();

                added = 1;
            }
            catch (Exception ex)
            {
                added = -1;
            }

            return added;
        }

        public int InsertCustomisedReportBankCode(SqlConnection con, int SubscriberEnquiryLogID, string Bank, string Branch, string BranchNo, string AccountNo, string AccountName)
        {
            int added = 0;

            try
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();

                DataSet ds = new DataSet();
                string strSQL = "sp_InsertCommercialCustomisedReportBankCode";
                SqlCommand cmd = new SqlCommand(strSQL, con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@SubscriberEnquiryLogID", SubscriberEnquiryLogID);
                cmd.Parameters.AddWithValue("@Bank", Bank);
                cmd.Parameters.AddWithValue("@Branch", Branch);
                cmd.Parameters.AddWithValue("@BranchNo", BranchNo);
                cmd.Parameters.AddWithValue("@AccountNo", AccountNo);
                cmd.Parameters.AddWithValue("@AccountName", AccountName);

                cmd.ExecuteNonQuery();

                added = 1;
            }
            catch (Exception ex)
            {
                added = -1;
            }

            return added;
        }

        public int InsertCustomisedReportTradeReferences(SqlConnection con, int SubscriberEnquiryLogID, string Name, string Contact)
        {
            int added = 0;

            try
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();

                DataSet ds = new DataSet();
                string strSQL = "sp_InsertCommercialCustomisedReportTradeReferences";
                SqlCommand cmd = new SqlCommand(strSQL, con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@SubscriberEnquiryLogID", SubscriberEnquiryLogID);
                cmd.Parameters.AddWithValue("@Name", Name);
                cmd.Parameters.AddWithValue("@Contact", Contact);

                cmd.ExecuteNonQuery();

                added = 1;
            }
            catch (Exception ex)
            {
                added = -1;
            }

            return added;
        }

        public int InsertCustomisedReportModules(SqlConnection con, int SubscriberEnquiryLogID, string ReportModule, int SubscriberCustomisedTemplateID)
        {
            int added = 0;

            try
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();

                DataSet ds = new DataSet();
                string strSQL = "sp_InsertCommercialCustomisedReportModules";
                SqlCommand cmd = new SqlCommand(strSQL, con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@SubscriberEnquiryLogID", SubscriberEnquiryLogID);
                cmd.Parameters.AddWithValue("@ReportModule", ReportModule);
                cmd.Parameters.AddWithValue("@SubscriberCustomisedTemplateID", SubscriberCustomisedTemplateID);

                cmd.ExecuteNonQuery();

                added = 1;
            }
            catch (Exception ex)
            {
                added = -1;
            }

            return added;
        }

        public long InsertSubscriberCustomisedTemplate(SqlConnection con, int SubscriberID, string TemplateName, string RecordStatusInd, string CreatedByUser)
        {
            long SubscriberCustomisedTemplateID = 0;

            try
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();

                DataSet ds = new DataSet();
                string strSQL = "sp_InsertCommercialSubscriberCustomisedTemplate";
                SqlCommand cmd = new SqlCommand(strSQL, con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@SubscriberID", SubscriberID);
                cmd.Parameters.AddWithValue("@TemplateName", TemplateName);
                cmd.Parameters.AddWithValue("@RecordStatusInd", RecordStatusInd);
                cmd.Parameters.AddWithValue("@CreatedbyUser", CreatedByUser);

                SubscriberCustomisedTemplateID = Convert.ToInt64(cmd.ExecuteScalar());
            }
            catch (Exception ex)
            {
                SubscriberCustomisedTemplateID = -1;
            }

            return SubscriberCustomisedTemplateID;
        }

        public long InsertSubscriberCustomisedTemplateModule(SqlConnection con, long SubscriberCustomisedTemplateID, string ReportModule, string RecordStatusInd, string CreatedByUser)
        {
            long ReportModuleID = 0;

            try
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();

                DataSet ds = new DataSet();
                string strSQL = "sp_InsertCommercialSubscriberCustomisedTemplateModules";
                SqlCommand cmd = new SqlCommand(strSQL, con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@SubscriberCustomisedTemplateID", SubscriberCustomisedTemplateID);
                cmd.Parameters.AddWithValue("@ReportModule", ReportModule);
                cmd.Parameters.AddWithValue("@RecordStatusInd", RecordStatusInd);
                cmd.Parameters.AddWithValue("@CreatedbyUser", CreatedByUser);

                ReportModuleID = Convert.ToInt64(cmd.ExecuteScalar());
            }
            catch (Exception ex)
            {
                ReportModuleID = -1;
            }

            return ReportModuleID;
        }

        public int DeleteSubscriberCustomisedTemplate(SqlConnection con, int SubscriberCustomisedTemplateID, string ChangedByUser)
        {
            int added = 0;

            try
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();

                DataSet ds = new DataSet();
                string strSQL = "sp_DeleteCommercialSubscriberCustomisedTemplate";
                SqlCommand cmd = new SqlCommand(strSQL, con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@SubscriberCustomisedTemplateID", SubscriberCustomisedTemplateID);
                cmd.Parameters.AddWithValue("@ChangedbyUser", ChangedByUser);

                cmd.ExecuteNonQuery();

                added = 1;
            }
            catch (Exception ex)
            {
                added = -1;
            }

            return added;
        }

        public string GetSubscriberCustomisedTemplates(SqlConnection con, int SubscriberID)
        {
            DataSet ds = new DataSet();
            string rXml = "";

            try
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();

                
                string strSQL = "sp_GetCommercialSubscriberCustomisedTemplates";
                SqlCommand cmd = new SqlCommand(strSQL, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SubscriberID", SubscriberID);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds, "SubscriberTemplates");
                foreach (DataTable dt in ds.Tables)
                {
                    dt.TableName = "SubscriberTemplates";
                }
                con.Close();

                if (ds.Tables[0].Rows.Count == 0)
                {
                    rXml = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                }
                else
                {
                    DataSet ds1 = new DataSet();
                    ds1.DataSetName = "ConnectTemplates";
                    DataTable dt = new DataTable();
                    dt.TableName = "SubscriberTemplates";

                    dt.Columns.Add("TemplateID", typeof(int));
                    dt.Columns.Add("Template", typeof(string));
                    dt.Columns.Add("RecordStatusInd", typeof(string));

                    foreach (DataRow r in ds.Tables[0].Rows)
                    {
                        DataRow dr = dt.NewRow();
                        dr["TemplateID"] = r["TemplateID"];
                        dr["Template"] = r["Template"];
                        dr["RecordStatusInd"] = r["RecordStatusInd"];
 
                        dt.Rows.Add(dr);
                    }
                    ds1.Tables.Add(dt);
                    rXml = ds1.GetXml();
                }
            }
            catch (Exception ex)
            {
                return "<Error>" + ex.Message + "</Error>";
            }

            return rXml;
        }

        public string GetTemplateModules(SqlConnection con, int SubscriberCustomisedTemplateID)
        {
            DataSet ds = new DataSet();
            string rXml = "";

            try
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();

                string strSQL = "sp_GetCommercialCustomisedTemplateModules";
                SqlCommand cmd = new SqlCommand(strSQL, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SubscriberCustomisedTemplateID", SubscriberCustomisedTemplateID);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds, "TemplateModules");
                ds.DataSetName = "ConnectTemplateModules";

                foreach (DataTable dt in ds.Tables)
                {
                    dt.TableName = "SubscriberTemplateModules";
                }
                con.Close();

                if (ds.Tables[0].Rows.Count == 0)
                {
                    rXml = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                }
                else
                {
                    rXml = ds.GetXml();
                }
            }
            catch (Exception ex)
            {
                return "<Error>" + ex.Message + "</Error>";
            }

            return rXml;
        }

        public int LogCommercialQuestionnaire(SqlConnection con, XDSPortalLibrary.Entity_Layer.CommercialQuestionnaire oCommercialQuestionnaire,
            int subscriberID, string regNo, string companyName, string questionnaireInfo ,string subscriberReference, string subscriberFirstname,
            string subscriberSurname, string subscriberEmail, string subscriberContactno, string clientName, string clientContact, string createdByUser)
        {
            if (con.State == ConnectionState.Closed)
                con.Open();

           

            DataSet ds = new DataSet();
            string strSQL = "sp_I_Questionnaire";
            SqlCommand cmd = new SqlCommand(strSQL, con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@SubscriberID",subscriberID);
            cmd.Parameters.AddWithValue("@ReceivedDate", DateTime.Now);
            cmd.Parameters.AddWithValue("@RegNo", regNo);
            cmd.Parameters.AddWithValue("@CompanyName", companyName);
            cmd.Parameters.AddWithValue("@QuestionnaireInfo", questionnaireInfo);
            
            cmd.Parameters.AddWithValue("@SubscriberReference", subscriberReference);
            cmd.Parameters.AddWithValue("@SubscriberFirstName", subscriberFirstname);
            cmd.Parameters.AddWithValue("@SubscriberSurname", subscriberSurname);
            cmd.Parameters.AddWithValue("@SubscriberEmail", subscriberEmail);
            cmd.Parameters.AddWithValue("@SubscriberContactNo", subscriberContactno);
            cmd.Parameters.AddWithValue("@ClientName", clientName);
            cmd.Parameters.AddWithValue("@ClientContact", clientContact);
            cmd.Parameters.AddWithValue("@CreatedByUser", createdByUser);


            SqlParameter oreturnvalue = new SqlParameter();
            oreturnvalue.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(oreturnvalue);

            int added = 0;
            cmd.ExecuteNonQuery();

            added = int.Parse(oreturnvalue.Value.ToString()); 

            return added;
          
        }

        public int LogCommercialEnquiry(SqlConnection con, string ReportType, string TimeFrame, string Country, string EmailAddress, string FirstName, string Surname, string CompanyName, string TelNo, string Reg1, string Reg2, string Reg3, string BusinessName, double Amount)
        {
            if (con.State == ConnectionState.Closed)
                con.Open();

            string strSQL = "";

            try
            {
                strSQL = "sp_I_PortalEnquiryWS";
                SqlCommand cmd = new SqlCommand(strSQL, con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Product", ReportType);
                cmd.Parameters.AddWithValue("@ReportTimeFrame", TimeFrame);
                cmd.Parameters.AddWithValue("@Country", Country);
                cmd.Parameters.AddWithValue("@ClientEmail", EmailAddress);
                cmd.Parameters.AddWithValue("@ClentFirstName", FirstName);
                cmd.Parameters.AddWithValue("@ClientSurname", Surname);
                cmd.Parameters.AddWithValue("@ClientCompany", CompanyName);
                cmd.Parameters.AddWithValue("@ClientContactNumber", TelNo);
                cmd.Parameters.AddWithValue("@RegistrationNumber", Reg1 + Reg2 + Reg3);
                cmd.Parameters.AddWithValue("@BusinessName", BusinessName);
                cmd.Parameters.AddWithValue("@Amount", Amount);
                cmd.Parameters.AddWithValue("@CreatedByUser", "admin");

                SqlParameter oreturnvalue = new SqlParameter();
                oreturnvalue.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(oreturnvalue);

                int EnquiryID = 0;
                cmd.ExecuteNonQuery();

                EnquiryID = int.Parse(oreturnvalue.Value.ToString());

                //Save Contact Details
                strSQL = "sp_I_PortalEnquiryContactInfo";
                cmd = new SqlCommand(strSQL, con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@EnquiryID", EnquiryID);
                cmd.Parameters.AddWithValue("@ContactPerson", FirstName + " " + Surname);
                cmd.Parameters.AddWithValue("@ContactNumber", TelNo);
                cmd.Parameters.AddWithValue("@CreatedByUser", "admin");

                cmd.ExecuteNonQuery();

                return EnquiryID;
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText("C:\\Log\\Customised.txt", ex.Message);
                return 0;
            }
        }

        public string GetCommercialReport(SqlConnection con, int EnquiryID)
        {
            DataSet ds = new DataSet();
            DataSet ds2 = new DataSet("InvestigativeReport");
            string rXml = "";

            try
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();

                string strSQL = "Reports_Investigative";
                SqlCommand cmd = new SqlCommand(strSQL, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@EnquiryID", EnquiryID);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds, "InvestigativeReport");

                con.Close();

                if (ds.Tables[0].Rows.Count == 0)
                {
                    rXml = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                }
                else
                {
                    if (ds.Tables.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables.Count; i++)
                        {
                            if (ds.Tables[i].Rows.Count > 0)
                            {
                                if (ds.Tables[i].Columns.Contains("TableName"))
                                {
                                    ds.Tables[i].TableName = ds.Tables[i].Rows[0]["TableName"].ToString();
                                    ds.Tables[i].Columns.Remove("TableName");
                                }
                                ds2.Tables.Add(ds.Tables[i].Copy());
                            }
                        }
                    }

                    rXml = ds2.GetXml();
                }
            }
            catch (Exception ex)
            {
                return "<Error>" + ex.Message + "</Error>";
            }

            return rXml;
        }
    }
}
