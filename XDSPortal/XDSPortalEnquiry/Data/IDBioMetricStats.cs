﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDSPortalEnquiry.Data
{
    public class IDBioMetricStats
    {
        public IDBioMetricStats() { }

        public int InsertIDBioMetricStats(string sqlcon, Entity.IDbioMetricStats oIDbioMetricStats)
        {
            int i = 0;

            SqlConnection ocon = new SqlConnection(sqlcon);
            SqlCommand ocmd = new SqlCommand("sp_I_IDBioMetricStats", ocon);
            ocmd.CommandType = System.Data.CommandType.StoredProcedure;

            ocmd.Parameters.AddWithValue("@SubscriberEnquiryID", oIDbioMetricStats.SubscriberEnquiryID);
            ocmd.Parameters.AddWithValue("@SubscriberEnquiryResultID", oIDbioMetricStats.SubscriberEnquiryResultID);
            ocmd.Parameters.AddWithValue("@SubscriberID", oIDbioMetricStats.SubscriberID);
            ocmd.Parameters.AddWithValue("@SystemuserID", oIDbioMetricStats.SystemuserID);
            ocmd.Parameters.AddWithValue("@Region", oIDbioMetricStats.Region);
            ocmd.Parameters.AddWithValue("@ConsumerID", oIDbioMetricStats.ConsumerID);
            ocmd.Parameters.AddWithValue("@HomeAffairsID", oIDbioMetricStats.HomeAffairsID);
            ocmd.Parameters.AddWithValue("@IDNumber", oIDbioMetricStats.IDNumber);
            ocmd.Parameters.AddWithValue("@Name", oIDbioMetricStats.Name);
            ocmd.Parameters.AddWithValue("@ApplicationErrors", oIDbioMetricStats.ApplicationErrors);
            ocmd.Parameters.AddWithValue("@VerificationErrors", oIDbioMetricStats.VerificationErrors);
            ocmd.Parameters.AddWithValue("@Base64StringJpeg2000Image", oIDbioMetricStats.Base64StringJpeg2000Image);
            ocmd.Parameters.AddWithValue("@VerificationResult", oIDbioMetricStats.VerificationResult);            
            ocmd.Parameters.AddWithValue("@VerificationResultDescription", oIDbioMetricStats.VerificationResultDescription);
            ocmd.Parameters.AddWithValue("@VerificationColorIndicator", oIDbioMetricStats.VerificationColorIndicator);
            ocmd.Parameters.AddWithValue("@TmStamp", oIDbioMetricStats.TmStamp);
            ocmd.Parameters.AddWithValue("@TransactionNumber", oIDbioMetricStats.TransactionNumber);
            ocmd.Parameters.AddWithValue("@HasImage", oIDbioMetricStats.HasImage);
            ocmd.Parameters.AddWithValue("@FingerColor", oIDbioMetricStats.FingerColor);
            ocmd.Parameters.AddWithValue("@ProductID", oIDbioMetricStats.ProductID);
            ocmd.Parameters.AddWithValue("@ReportID", oIDbioMetricStats.ReportID);
            ocmd.Parameters.AddWithValue("@SubscriberReference", oIDbioMetricStats.SubscriberReference);
            ocmd.Parameters.AddWithValue("@CreatedbyUser", oIDbioMetricStats.CreatedbyUser);
            ocmd.Parameters.AddWithValue("@Createdondate", DateTime.Now);
            ocmd.Parameters.AddWithValue("@SubscriberName", oIDbioMetricStats.SubscriberName);
            ocmd.Parameters.AddWithValue("@subscriberEnquiryDate", oIDbioMetricStats.SubscriberEnquiryDate);
            ocmd.Parameters.AddWithValue("@Systemusername", oIDbioMetricStats.Systemusername);
            ocmd.Parameters.AddWithValue("@SystemUserNameInfo", oIDbioMetricStats.SystemUserNameInfo);

            System.IO.File.AppendAllText(@"C:\Log\Product149.txt", ocmd.CommandText);
            System.IO.File.AppendAllText(@"C:\Log\Product149.txt", oIDbioMetricStats.TmStamp.ToString());
            System.IO.File.AppendAllText(@"C:\Log\Product149.txt", oIDbioMetricStats.SubscriberEnquiryDate.ToString());
            

            SqlParameter oparam = new SqlParameter();
            oparam.DbType = System.Data.DbType.Int16;
            oparam.Direction = System.Data.ParameterDirection.ReturnValue;
            ocmd.Parameters.Add(oparam);

            if (ocon.State == System.Data.ConnectionState.Closed)
                ocon.Open();

            ocmd.ExecuteNonQuery();

            if (ocon.State == System.Data.ConnectionState.Open)
                ocon.Close();

            i = int.Parse(oparam.Value.ToString());

            return i;
        }

        public DataSet GetIDBioMetricStats(string sqlcon, Entity.IDbioMetricStats oIDbioMetricStats)
        {
            int i = 0;

            SqlConnection ocon = new SqlConnection(sqlcon);
            SqlCommand ocmd = new SqlCommand("sp_Get_IDBioMetricStats", ocon);
            ocmd.CommandType = System.Data.CommandType.StoredProcedure;


            ocmd.Parameters.AddWithValue("@SubscriberID", oIDbioMetricStats.SubscriberID);
            ocmd.Parameters.AddWithValue("@IsMaster", oIDbioMetricStats.IsMaster);
            ocmd.Parameters.AddWithValue("@Keyword", oIDbioMetricStats.Keyword);
           

            //if (ocon.State == System.Data.ConnectionState.Closed)
            //    ocon.Open();

            SqlDataAdapter oDa = new SqlDataAdapter(ocmd);
            DataSet ds = new DataSet();
            oDa.Fill(ds);

            if (ocon.State == System.Data.ConnectionState.Open)
                ocon.Close();

         

            return ds;
        }

        public void UpdateIDBioMetricStats(string sqlcon, Entity.IDbioMetricStats oIDbioMetricStats)
        {
            int i = 0;

            SqlConnection ocon = new SqlConnection(sqlcon);
            SqlCommand ocmd = new SqlCommand("sp_U_IDBioMetricStats", ocon);
            ocmd.CommandType = System.Data.CommandType.StoredProcedure;


            ocmd.Parameters.AddWithValue("@EnquiryID", oIDbioMetricStats.SubscriberEnquiryID);
            ocmd.Parameters.AddWithValue("@EnquiryResultID", oIDbioMetricStats.SubscriberEnquiryResultID);
            ocmd.Parameters.AddWithValue("@Comments", oIDbioMetricStats.Comments);
            ocmd.Parameters.AddWithValue("@Username", oIDbioMetricStats.CreatedbyUser);

            
            if (ocon.State == System.Data.ConnectionState.Closed)
                ocon.Open();

            ocmd.ExecuteNonQuery();

            if (ocon.State == System.Data.ConnectionState.Open)
                ocon.Close();
        }
    }
}
