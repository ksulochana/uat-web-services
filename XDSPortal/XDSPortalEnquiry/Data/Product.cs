﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace XDSPortalEnquiry.Data
{
    public class Product
    {
        public Product()
        {
        }
        public XDSPortalEnquiry.Entity.Product GetProductRecord(string conString, int productID)
        {
            XDSPortalEnquiry.Entity.Product objProduct = new XDSPortalEnquiry.Entity.Product();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = "select * from product where productID = " + productID;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objProduct.ProductID = Convert.ToInt32(Dr["ProductID"]);
                objProduct.ProductDesc = Dr["ProductDesc"].ToString();
                objProduct.DefaultPointValue = Convert.ToDecimal(Dr["DefaultPointValue"]);
                objProduct.ProductDetails = Dr["ProductDetails"].ToString();
                objProduct.EnableReportingServicesReportYN = Convert.ToBoolean(Dr["EnableReportingServicesReportYN"]);
                
            }
            ds.Dispose();
            Objsqlcom.Dispose();
            objsqladp.Dispose();
            ObjConstring.Close();

            return objProduct;
        }

        public XDSPortalEnquiry.Entity.Product GetProductRecord(SqlConnection ObjConstring, int productID)
        {
            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();

            XDSPortalEnquiry.Entity.Product objProduct = new XDSPortalEnquiry.Entity.Product();

            string sqlSelect = "select * from product where productID = " + productID;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objProduct.ProductID = Convert.ToInt32(Dr["ProductID"]);
                objProduct.ProductDesc = Dr["ProductDesc"].ToString();
                objProduct.DefaultPointValue = Convert.ToDecimal(Dr["DefaultPointValue"]);
                objProduct.ProductDetails = Dr["ProductDetails"].ToString();
                objProduct.EnableReportingServicesReportYN = Convert.ToBoolean(Dr["EnableReportingServicesReportYN"]);

            }

            ds.Dispose();
            Objsqlcom.Dispose();
            objsqladp.Dispose();
            ObjConstring.Close();

            return objProduct;
        }

        public DataSet GetProductList(string conString)
        {
            XDSPortalEnquiry.Entity.Product objProduct = new XDSPortalEnquiry.Entity.Product();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = "select * from product where enablereportingservicesreportyn = 1";

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            ObjConstring.Close();

            return ds;
        }

    }
}
