﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace XDSPortalEnquiry.Data
{
    public class SubscriberChecks
    {
        public XDSPortalLibrary.Entity_Layer.Response Response { get; set; } = new XDSPortalLibrary.Entity_Layer.Response();
        readonly Entity.SystemUser _systemUser;
        readonly Entity.Subscriber _subscriber;
        readonly SqlConnection _adminConnection;
        readonly Entity.SubscriberProductReports _subscriberProductReports;

        public SubscriberChecks(Entity.SystemUser systemUser, Entity.Subscriber subscriber, Entity.SubscriberProductReports subscriberProductReports, SqlConnection adminConnection)
        {
            _systemUser = systemUser;
            _subscriber = subscriber;
            _adminConnection = adminConnection;
            _subscriberProductReports = subscriberProductReports;
            Response.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
        }


        public bool IsProfileActive()
        {
            Response = new XDSPortalLibrary.Entity_Layer.Response();

            if (_systemUser != null && _subscriber != null && _systemUser.ActiveYN && _subscriber.StatusInd == "A")
                return true;
            else
            {
                Response.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                Response.ResponseData = "User or Subscriber Profile not active";

                return false;
            }

        }

        public bool IsValidVoucher(string voucherCode)
        {
            Response = new XDSPortalLibrary.Entity_Layer.Response();
            if (!string.IsNullOrEmpty(voucherCode))
            {
                Business.SubscriberVoucher sv = new Business.SubscriberVoucher();
                string strValidationStatus = sv.ValidationStatus(_adminConnection, _subscriber.SubscriberID, voucherCode);

                if (strValidationStatus == "")
                {
                    Response.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    Response.ResponseData = "Invalid Voucher Code";
                    return false;
                }
                else if (strValidationStatus != "1")
                {
                    Response.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    Response.ResponseData = strValidationStatus;
                    return false;
                }
            }

            return true;
        }

        public bool Hasproductaccess(int productId)
        {
            if (_subscriberProductReports == null || _subscriberProductReports.ReportID <= 0)
            {
                Response.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                Response.ResponseData = "This enquiry Product is not activated in your profile. Please contact XDS to activate the product";
                return false;
            }

            return true;
        }


        public bool GotEnoughCredit(DataSet bonusSegments)
        {
           double Totalcost = _subscriberProductReports.UnitPrice + GetBonusSegmnetsCost(bonusSegments);
            if ((_subscriber.PayAsYouGo == 1 && _subscriber.PayAsyouGoEnquiryLimit >= (Totalcost)) || _subscriber.PayAsYouGo == 0)
            {
                return true;
            }
            else
            {
                Response.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                Response.ResponseData = "Your profile has insufficient fund for this enquiry. Please contact XDS to top up your account";
                return false;
            }
        }


        private static double GetBonusSegmnetsCost(DataSet bonusSegments)
        {
            double Totalcost = 0;
            if (bonusSegments != null)
            {
                foreach (DataRow dr in bonusSegments.Tables[0].Rows)
                {
                    if (dr["BonusViewed"].ToString().ToLower() == "true")
                    {
                        Totalcost = Totalcost + Convert.ToDouble(dr["BonusPrice"].ToString());
                    }
                }
            }

            return Totalcost;
        }


    }
}
