﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace XDSPortalEnquiry.Data
{
   public class SubscriberEnquiryLog
    {
       public int InsertSubscriberEnquiryLog(SqlConnection cn, Entity.SubscriberEnquiryLog SEL)
       {           
            if (cn.State == ConnectionState.Closed)
                cn.Open();

           string strSQL;
           if (!(string.IsNullOrEmpty(SEL.SubscriberName)))
               SEL.SubscriberName = SEL.SubscriberName.Replace("'", "''");
           if (!(string.IsNullOrEmpty(SEL.SystemUserName)))
               SEL.SystemUserName = SEL.SystemUserName.Replace("',", "''");
           if (!(string.IsNullOrEmpty(SEL.BusBusinessName)))
               SEL.BusBusinessName = SEL.BusBusinessName.Replace("'", "''");
           if (!(string.IsNullOrEmpty(SEL.BusRegistrationNo)))
               SEL.BusRegistrationNo = SEL.BusRegistrationNo.Replace("'", "''");
           if (!(string.IsNullOrEmpty(SEL.FirstName)))
               SEL.FirstName = SEL.FirstName.Replace("'", "''");
           if (!(string.IsNullOrEmpty(SEL.SurName)))
               SEL.SurName = SEL.SurName.Replace("'", "''");
           if (!(string.IsNullOrEmpty(SEL.CompanyName)))
               SEL.CompanyName = SEL.CompanyName.Replace("'", "''");
           if (!(string.IsNullOrEmpty(SEL.SubscriberReference)))
               SEL.SubscriberReference = SEL.SubscriberReference.Replace("'", "''");
           if (!(string.IsNullOrEmpty(SEL.EmailAddress)))
               SEL.EmailAddress = SEL.EmailAddress.Replace("'", "''");
           if (!(string.IsNullOrEmpty(SEL.BankName)))
               SEL.BankName = SEL.BankName.Replace("'", "''");
           if (!(string.IsNullOrEmpty(SEL.BranchName)))
               SEL.BranchName = SEL.BranchName.Replace("'", "''");
           if (!(string.IsNullOrEmpty(SEL.BranchCode)))
               SEL.BranchCode = SEL.BranchCode.Replace("'", "''");
           if (!(string.IsNullOrEmpty(SEL.AccountNo)))
               SEL.AccountNo = SEL.AccountNo.Replace("'", "''");
           if (!(string.IsNullOrEmpty(SEL.AccHolder)))
               SEL.AccHolder = SEL.AccHolder.Replace("'", "''");
           if (!(string.IsNullOrEmpty(SEL.Terms)))
               SEL.Terms = SEL.Terms.Replace("'", "''");
           if (!(string.IsNullOrEmpty(SEL.ExtraVarInput1)))
               SEL.ExtraVarInput1 = SEL.ExtraVarInput1.Replace("'", "''");
           if (!(string.IsNullOrEmpty(SEL.Searchinput)))
               SEL.Searchinput = SEL.Searchinput.Replace("'", "''");
           if (!(string.IsNullOrEmpty(SEL.EntityType)))
               SEL.EntityType = SEL.EntityType.Replace("'", "''");
           if (!(string.IsNullOrEmpty(SEL.RegNumber)))
               SEL.RegNumber = SEL.RegNumber.Replace("'", "''");


           strSQL = "Exec sp_I_SubscriberEnquiryLog " + SEL.SubscriberEnquiryID + ",'" + SEL.KeyType + "','" + SEL.SubscriberReference + "'," + SEL.SubscriberID + ",'" + SEL.SubscriberName + "'," + SEL.SystemUserID + ",'" + SEL.SystemUserName + "','" + SEL.BusRegistrationNo + "','" + SEL.BusBusinessName + "','" + SEL.FirstName + "','" + SEL.SurName + "','" + SEL.CompanyName + "','" + SEL.ContactNo + "','" + SEL.EmailAddress + "','" + SEL.VoucherCode + "','" + SEL.ExtraVarInput1 + "','" + SEL.ExtraVarInput2 + "','" + SEL.ExtraVarInput3 + "'," + SEL.ExtraintInput1 + "," + SEL.ExtraintInput2 + ",'" + SEL.CreatedByUser + "','" + SEL.CreatedOnDate + "','" + SEL.EnquiryStatus + "','" + SEL.EnquiryResult + "','" + SEL.ReportType + "','" + SEL.TimeFrame + "','" + SEL.BankName + "','" + SEL.BranchName + "','" + SEL.BranchCode + "','" + SEL.AccountNo + "','" + SEL.AccHolder + "','" + SEL.Amount + "','" + SEL.Terms + "'," + SEL.Billable + "," + SEL.ProductID + ",'" + SEL.BillingPrice + "','" + SEL.IDNo + "','" + SEL.CompanyContactNo + "'," + SEL.KeyID + ",'" + SEL.Searchinput + "','" + SEL.SubscriberEnquiryDate + "'," + SEL.BillingTypeID + ",'" + SEL.EntityType + "','" + SEL.Trust + "','" + SEL.PassportNo + "','" + SEL.InternationalBusRegistrationNumber + "','" + SEL.VerificationType + "'";
           SqlCommand objcmd = new SqlCommand(strSQL, cn);
           int RowsEffected;
           RowsEffected = objcmd.ExecuteNonQuery();

           strSQL = "SELECT @@IDENTITY";
           SqlCommand Imycommand = new SqlCommand(strSQL, cn);
           SqlDataReader Idr = Imycommand.ExecuteReader();
           Idr.Read();
           int intSubscriberEnquiryLogID = Convert.ToInt32(Idr.GetValue(0));
           Idr.Close();

           strSQL = "Exec spGetDataSegments " + SEL.SubscriberID + "," + SEL.ProductID + ",'" + SEL.SubscriberReference + "'";
           objcmd = new SqlCommand(strSQL, cn);
           objcmd.CommandTimeout = 0;
           SqlDataAdapter da = new SqlDataAdapter(objcmd);
           DataSet ds = new DataSet("DataSegments");
           da.Fill(ds);
           ds.Tables[0].TableName = "Segments";
           string xml = string.Empty;
           xml = ds.GetXml();

           strSQL = "Update SubscriberEnquiryLog set ExtraVarInput3 = '" + xml + "' where SubscriberEnquiryLogID = " + intSubscriberEnquiryLogID;
           Imycommand = new SqlCommand(strSQL, cn);

           RowsEffected = Imycommand.ExecuteNonQuery();

           objcmd.Dispose();
           Imycommand.Dispose();
           Idr.Dispose();
           cn.Close();
           ds.Dispose();

           return intSubscriberEnquiryLogID;

       }

       public int InsertSubscriberEnquiryLogAVSRealTime(SqlConnection cn, Entity.SubscriberEnquiryLog SEL)
       {
           if (cn.State == ConnectionState.Closed)
               cn.Open();

           string strSQL;

           if (!(string.IsNullOrEmpty(SEL.SubscriberName)))
               SEL.SubscriberName = SEL.SubscriberName.Replace("'", "''");
           if (!(string.IsNullOrEmpty(SEL.SystemUserName)))
               SEL.SystemUserName = SEL.SystemUserName.Replace("',", "''");
           if (!(string.IsNullOrEmpty(SEL.BusBusinessName)))
               SEL.BusBusinessName = SEL.BusBusinessName.Replace("'", "''");
           if (!(string.IsNullOrEmpty(SEL.BusRegistrationNo)))
               SEL.BusRegistrationNo = SEL.BusRegistrationNo.Replace("'", "''");
           if (!(string.IsNullOrEmpty(SEL.FirstName)))
               SEL.FirstName = SEL.FirstName.Replace("'", "''");
           if (!(string.IsNullOrEmpty(SEL.SurName)))
               SEL.SurName = SEL.SurName.Replace("'", "''");
           if (!(string.IsNullOrEmpty(SEL.CompanyName)))
               SEL.CompanyName = SEL.CompanyName.Replace("'", "''");
           if (!(string.IsNullOrEmpty(SEL.SubscriberReference)))
               SEL.SubscriberReference = SEL.SubscriberReference.Replace("'", "''");
           if (!(string.IsNullOrEmpty(SEL.EmailAddress)))
               SEL.EmailAddress = SEL.EmailAddress.Replace("'", "''");
           if (!(string.IsNullOrEmpty(SEL.BankName)))
               SEL.BankName = SEL.BankName.Replace("'", "''");
           if (!(string.IsNullOrEmpty(SEL.BranchName)))
               SEL.BranchName = SEL.BranchName.Replace("'", "''");
           if (!(string.IsNullOrEmpty(SEL.BranchCode)))
               SEL.BranchCode = SEL.BranchCode.Replace("'", "''");
           if (!(string.IsNullOrEmpty(SEL.AccountNo)))
               SEL.AccountNo = SEL.AccountNo.Replace("'", "''");
           if (!(string.IsNullOrEmpty(SEL.AccHolder)))
               SEL.AccHolder = SEL.AccHolder.Replace("'", "''");
           if (!(string.IsNullOrEmpty(SEL.Terms)))
               SEL.Terms = SEL.Terms.Replace("'", "''");
           if (!(string.IsNullOrEmpty(SEL.ExtraVarInput1)))
               SEL.ExtraVarInput1 = SEL.ExtraVarInput1.Replace("'", "''");
           if (!(string.IsNullOrEmpty(SEL.Searchinput)))
               SEL.Searchinput = SEL.Searchinput.Replace("'", "''");
           if (!(string.IsNullOrEmpty(SEL.EntityType)))
               SEL.EntityType = SEL.EntityType.Replace("'", "''");
           if (!(string.IsNullOrEmpty(SEL.RegNumber)))
               SEL.RegNumber = SEL.RegNumber.Replace("'", "''");


           strSQL = "Exec sp_i_SubscriberEnquiryLog_AVSRealTime " + SEL.SubscriberEnquiryID + ",'" + SEL.KeyType + "','" + SEL.SubscriberReference + "'," + SEL.SubscriberID + ",'" + SEL.SubscriberName + "'," + SEL.SystemUserID + ",'" + SEL.SystemUserName + "','" + SEL.BusRegistrationNo + "','" + SEL.BusBusinessName + "','" + SEL.FirstName + "','" + SEL.SurName + "','" + SEL.CompanyName + "','" + SEL.ContactNo + "','" + SEL.EmailAddress + "','" + SEL.VoucherCode + "','" + SEL.ExtraVarInput1 + "','" + SEL.ExtraVarInput2 + "','" + SEL.ExtraVarInput3 + "'," + SEL.ExtraintInput1 + "," + SEL.ExtraintInput2 + ",'" + SEL.CreatedByUser + "','" + SEL.CreatedOnDate + "','" + SEL.EnquiryStatus + "','" + SEL.EnquiryResult + "','" + SEL.ReportType + "','" + SEL.TimeFrame + "','" + SEL.BankName + "','" + SEL.BranchName + "','" + SEL.BranchCode + "','" + SEL.AccountNo + "','" + SEL.AccHolder + "','" + SEL.Amount + "','" + SEL.Terms + "'," + SEL.Billable + "," + SEL.ProductID + ",'" + SEL.BillingPrice + "','" + SEL.IDNo + "','" + SEL.CompanyContactNo + "'," + SEL.KeyID + ",'" + SEL.Searchinput + "','" + SEL.SubscriberEnquiryDate + "'," + SEL.BillingTypeID + ",'" + SEL.EntityType + "','" + SEL.Trust + "'";
           SqlCommand objcmd = new SqlCommand(strSQL, cn);
           int RowsEffected;
           RowsEffected = objcmd.ExecuteNonQuery();

           strSQL = "SELECT @@IDENTITY";
           SqlCommand Imycommand = new SqlCommand(strSQL, cn);
           SqlDataReader Idr = Imycommand.ExecuteReader();
           Idr.Read();
           int intSubscriberEnquiryLogID = Convert.ToInt32(Idr.GetValue(0));
           Idr.Close();

           strSQL = "Exec spGetDataSegments " + SEL.SubscriberID + "," + SEL.ProductID + ",'" + SEL.SubscriberReference + "'";
           objcmd = new SqlCommand(strSQL, cn);
           objcmd.CommandTimeout = 0;
           SqlDataAdapter da = new SqlDataAdapter(objcmd);
           DataSet ds = new DataSet("DataSegments");
           da.Fill(ds);
           ds.Tables[0].TableName = "Segments";
           string xml = string.Empty;
           xml = ds.GetXml();

           strSQL = "Update SubscriberEnquiryLog set ExtraVarInput3 = '" + xml + "' where SubscriberEnquiryLogID = " + intSubscriberEnquiryLogID;
           Imycommand = new SqlCommand(strSQL, cn);

           RowsEffected = Imycommand.ExecuteNonQuery();

           objcmd.Dispose();
           Imycommand.Dispose();
           Idr.Dispose();
           cn.Close();
           ds.Dispose();

           return intSubscriberEnquiryLogID;

       }

       public int UpdateSubscriberEnquiryLog(SqlConnection cn, Entity.SubscriberEnquiryLog SEL)
       {
           if (cn.State == ConnectionState.Closed)
               cn.Open();

         //  string strSQL;

           //if (!(string.IsNullOrEmpty(SEL.SubscriberName)))
           //    SEL.SubscriberName = SEL.SubscriberName.Replace("'", "''");
           //if (!(string.IsNullOrEmpty(SEL.SystemUserName)))
           //    SEL.SystemUserName = SEL.SystemUserName.Replace("',", "''");
           //if (!(string.IsNullOrEmpty(SEL.BusBusinessName)))
           //    SEL.BusBusinessName = SEL.BusBusinessName.Replace("'", "''");
           //if (!(string.IsNullOrEmpty(SEL.FirstName)))
           //    SEL.FirstName = SEL.FirstName.Replace("'", "''");
           //if (!(string.IsNullOrEmpty(SEL.SurName)))
           //    SEL.SurName = SEL.SurName.Replace("'", "''");
           //if (!(string.IsNullOrEmpty(SEL.CompanyName)))
           //    SEL.CompanyName = SEL.CompanyName.Replace("'", "''");
           //if (!(string.IsNullOrEmpty(SEL.SubscriberReference)))
           //    SEL.SubscriberReference = SEL.SubscriberReference.Replace("'", "''");
           //if (!(string.IsNullOrEmpty(SEL.EmailAddress)))
           //    SEL.EmailAddress = SEL.EmailAddress.Replace("'", "''");
           //if (!(string.IsNullOrEmpty(SEL.BankName)))
           //    SEL.BankName = SEL.BankName.Replace("'", "''");
           //if (!(string.IsNullOrEmpty(SEL.BranchName)))
           //    SEL.BranchName = SEL.BranchName.Replace("'", "''");
           //if (!(string.IsNullOrEmpty(SEL.BranchCode)))
           //    SEL.BranchCode = SEL.BranchCode.Replace("'", "''");
           //if (!(string.IsNullOrEmpty(SEL.AccountNo)))
           //    SEL.AccountNo = SEL.AccountNo.Replace("'", "''");
           //if (!(string.IsNullOrEmpty(SEL.AccHolder)))
           //    SEL.AccHolder = SEL.AccHolder.Replace("'", "''");
           //if (!(string.IsNullOrEmpty(SEL.Terms)))
           //    SEL.Terms = SEL.Terms.Replace("'", "''");
           //if (!(string.IsNullOrEmpty(SEL.ExtraVarInput1)))
           //    SEL.ExtraVarInput1 = SEL.ExtraVarInput1.Replace("'", "''");
           //if (!(string.IsNullOrEmpty(SEL.XMLData)))
           //    SEL.XMLData = SEL.XMLData.Replace("'", "''");
           //if (!(string.IsNullOrEmpty(SEL.Searchinput)))
           //    SEL.Searchinput = SEL.Searchinput.Replace("'", "''");
           //if (!(string.IsNullOrEmpty(SEL.SearchOutput)))
           //    SEL.SearchOutput = SEL.SearchOutput.Replace("'", "''");
           //if (!(string.IsNullOrEmpty(SEL.EntityType)))
           //    SEL.EntityType = SEL.EntityType.Replace("'", "''");
           //if (!(string.IsNullOrEmpty(SEL.RegNumber)))
           //    SEL.RegNumber = SEL.RegNumber.Replace("'", "''");

           /*
           SqlCommand cmd = new SqlCommand();
           cmd.CommandText = "UPDATE SubscriberEnquiryLog SET ReportFile = @ReportFile where SubscriberEnquiryLogID = 72";
           cmd.CommandType = CommandType.Text;
           cmd.Connection = cn;

   
           SqlParameter UploadedImage = new SqlParameter
                         ("@ReportFile", SqlDbType.Image, SEL.ReportFile.Length);
           UploadedImage.Value = SEL.ReportFile;
           cmd.Parameters.Add(UploadedImage);
           
           int RowsEffected = cmd.ExecuteNonQuery();
           cn.Close();

           */

           //strSQL = "Exec sp_u_SubscriberEnquiryLog " + SEL.SubscriberEnquiryLogID + "," + SEL.SubscriberEnquiryID + ",'" + SEL.KeyType + "','" + SEL.BusRegistrationNo + "','" + SEL.BusBusinessName + "','" + SEL.FirstName + "','" + SEL.SurName + "','" + SEL.CompanyName + "','" + SEL.ContactNo + "','" + SEL.EmailAddress + "','" + SEL.VoucherCode + "','" + SEL.ExtraVarInput1 + "','" + SEL.ExtraVarInput2 + "','" + SEL.ExtraVarInput3 + "'," + SEL.ExtraintInput1 + "," + SEL.ExtraintInput2 + ",'" + SEL.CreatedByUser + "','" + SEL.CreatedOnDate + "','" + SEL.EnquiryStatus + "','" + SEL.EnquiryResult + "','" + SEL.ReportType + "','" + SEL.TimeFrame + "','" + SEL.BankName + "','" + SEL.BranchName + "','" + SEL.BranchCode + "','" + SEL.AccountNo + "','" + SEL.AccHolder + "','" + SEL.Amount + "','" + SEL.Terms + "'," + SEL.Billable + "," + SEL.ProductID + ",'" + SEL.BillingPrice + "','" + SEL.IDNo + "','" + SEL.CompanyContactNo + "'," + SEL.KeyID + ",'" + SEL.XMLData + "','" + SEL.Searchinput + "','" + SEL.SearchOutput + "','" + SEL.DetailsViewedDate + "','" + SEL.DetailsViewedYN + "','" + SEL.FileName +"', " + SEL.ReportFile +"";

           int RowsEffected;



           if (!(SEL.IsAHVRequest))
           {
               SqlCommand objcmd = new SqlCommand("sp_u_SubscriberEnquiryLog", cn);
               objcmd.CommandType = CommandType.StoredProcedure;
               objcmd.Parameters.AddWithValue("@SubscriberEnquiryLogID", SEL.SubscriberEnquiryLogID);
               objcmd.Parameters.AddWithValue("@SubscriberEnquiryID", SEL.SubscriberEnquiryID);
               objcmd.Parameters.AddWithValue("@KeyType", SEL.KeyType);
               objcmd.Parameters.AddWithValue("@BusRegistrationNo", SEL.BusRegistrationNo);
               objcmd.Parameters.AddWithValue("@BusBusinessName", SEL.BusBusinessName);
               objcmd.Parameters.AddWithValue("@FirstName", SEL.FirstName);
               objcmd.Parameters.AddWithValue("@Surname", SEL.SurName);
               objcmd.Parameters.AddWithValue("@CompanyName", SEL.CompanyName);
               objcmd.Parameters.AddWithValue("@ContactNo", SEL.ContactNo);
               objcmd.Parameters.AddWithValue("@Email", SEL.EmailAddress);
               objcmd.Parameters.AddWithValue("@VoucherCode", SEL.VoucherCode);
               objcmd.Parameters.AddWithValue("@ExtraVarInput1", SEL.ExtraVarInput1);
               objcmd.Parameters.AddWithValue("@ExtraVarInput2", SEL.ExtraVarInput2);
               objcmd.Parameters.AddWithValue("@ExtraVarInput3", SEL.ExtraVarInput3);
               objcmd.Parameters.AddWithValue("@ExtraintInput1", SEL.ExtraintInput1);
               objcmd.Parameters.AddWithValue("@ExtraintInput2", SEL.ExtraintInput2);
               objcmd.Parameters.AddWithValue("@CreatedByUser", SEL.CreatedByUser);
               objcmd.Parameters.AddWithValue("@Createdondate", SEL.CreatedOnDate);
               objcmd.Parameters.AddWithValue("@EnquiryStatusInd", SEL.EnquiryStatus);
               objcmd.Parameters.AddWithValue("@EnquiryResultInd", SEL.EnquiryResult);
               objcmd.Parameters.AddWithValue("@ReportType", SEL.ReportType);
               objcmd.Parameters.AddWithValue("@TimeFrame", SEL.TimeFrame);
               objcmd.Parameters.AddWithValue("@BankName", SEL.BankName);
               objcmd.Parameters.AddWithValue("@BranchName", SEL.BranchName);
               objcmd.Parameters.AddWithValue("@BranchCode", SEL.BranchCode);
               objcmd.Parameters.AddWithValue("@AccountNo", SEL.AccountNo);
               objcmd.Parameters.AddWithValue("@AccHolder", SEL.AccHolder);
               objcmd.Parameters.AddWithValue("@Amount", SEL.Amount);
               objcmd.Parameters.AddWithValue("@Terms", SEL.Terms);
               objcmd.Parameters.AddWithValue("@Billable", SEL.Billable);
               objcmd.Parameters.AddWithValue("@ProductID", SEL.ProductID);
               objcmd.Parameters.AddWithValue("@BillingPrice", SEL.BillingPrice);
               objcmd.Parameters.AddWithValue("@IDNo", SEL.IDNo);
               objcmd.Parameters.AddWithValue("@PassportNO", SEL.PassportNo);
               objcmd.Parameters.AddWithValue("@CompanyContactNo", SEL.CompanyContactNo);
               objcmd.Parameters.AddWithValue("@KeyID", SEL.KeyID);
               objcmd.Parameters.AddWithValue("@XmlData", SEL.XMLData);
               objcmd.Parameters.AddWithValue("@SearchInput", SEL.Searchinput);
               objcmd.Parameters.AddWithValue("@SearchOutput", SEL.SearchOutput);
               objcmd.Parameters.AddWithValue("@DetailsViewedDate", SEL.DetailsViewedDate);
               objcmd.Parameters.AddWithValue("@DetailsViewedYN", SEL.DetailsViewedYN);
               objcmd.Parameters.AddWithValue("@FileName", SEL.FileName);
               objcmd.Parameters.AddWithValue("@InternationalBusRegistrationNumber", SEL.InternationalBusRegistrationNumber);
               objcmd.Parameters.AddWithValue("@VerificationType", SEL.VerificationType);

                //SqlParameter pReportFile = new SqlParameter("@ReportFile", SqlDbType.Image, SEL.ReportFile.Length);
                SqlParameter pReportFile = new SqlParameter("@ReportFile", SqlDbType.Image, SEL.ReportFile == null ? 1 : SEL.ReportFile.Length); 
               byte[] bImg = new byte[0];
               pReportFile.Value = SEL.ReportFile == null ? bImg : SEL.ReportFile;
               objcmd.Parameters.Add(pReportFile);
               RowsEffected = objcmd.ExecuteNonQuery();
           }
           else
           {
               SqlCommand objcmd = new SqlCommand("sp_u_SubscriberEnquiryLogNoFile", cn);
               objcmd.CommandType = CommandType.StoredProcedure;
               objcmd.Parameters.AddWithValue("@SubscriberEnquiryLogID", SEL.SubscriberEnquiryLogID);
               objcmd.Parameters.AddWithValue("@SubscriberEnquiryID", SEL.SubscriberEnquiryID);
               objcmd.Parameters.AddWithValue("@KeyType", SEL.KeyType);
               objcmd.Parameters.AddWithValue("@BusRegistrationNo", SEL.BusRegistrationNo);
               objcmd.Parameters.AddWithValue("@BusBusinessName", SEL.BusBusinessName);
               objcmd.Parameters.AddWithValue("@FirstName", SEL.FirstName);
               objcmd.Parameters.AddWithValue("@Surname", SEL.SurName);
               objcmd.Parameters.AddWithValue("@CompanyName", SEL.CompanyName);
               objcmd.Parameters.AddWithValue("@ContactNo", SEL.ContactNo);
               objcmd.Parameters.AddWithValue("@Email", SEL.EmailAddress);
               objcmd.Parameters.AddWithValue("@VoucherCode", SEL.VoucherCode);
               objcmd.Parameters.AddWithValue("@ExtraVarInput1", SEL.ExtraVarInput1);
               objcmd.Parameters.AddWithValue("@ExtraVarInput2", SEL.ExtraVarInput2);
               objcmd.Parameters.AddWithValue("@ExtraVarInput3", SEL.ExtraVarInput3);
               objcmd.Parameters.AddWithValue("@ExtraintInput1", SEL.ExtraintInput1);
               objcmd.Parameters.AddWithValue("@ExtraintInput2", SEL.ExtraintInput2);
               objcmd.Parameters.AddWithValue("@CreatedByUser", SEL.CreatedByUser);
               objcmd.Parameters.AddWithValue("@Createdondate", SEL.CreatedOnDate);
               objcmd.Parameters.AddWithValue("@EnquiryStatusInd", SEL.EnquiryStatus);
               objcmd.Parameters.AddWithValue("@EnquiryResultInd", SEL.EnquiryResult);
               objcmd.Parameters.AddWithValue("@ReportType", SEL.ReportType);
               objcmd.Parameters.AddWithValue("@TimeFrame", SEL.TimeFrame);
               objcmd.Parameters.AddWithValue("@BankName", SEL.BankName);
               objcmd.Parameters.AddWithValue("@BranchName", SEL.BranchName);
               objcmd.Parameters.AddWithValue("@BranchCode", SEL.BranchCode);
               objcmd.Parameters.AddWithValue("@AccountNo", SEL.AccountNo);
               objcmd.Parameters.AddWithValue("@AccHolder", SEL.AccHolder);
               objcmd.Parameters.AddWithValue("@Amount", SEL.Amount);
               objcmd.Parameters.AddWithValue("@Terms", SEL.Terms);
               objcmd.Parameters.AddWithValue("@Billable", SEL.Billable);
               objcmd.Parameters.AddWithValue("@ProductID", SEL.ProductID);
               objcmd.Parameters.AddWithValue("@BillingPrice", SEL.BillingPrice);
               objcmd.Parameters.AddWithValue("@IDNo", SEL.IDNo);
               objcmd.Parameters.AddWithValue("@PassportNo", SEL.PassportNo);
               objcmd.Parameters.AddWithValue("@InternationalBusRegistrationNumber", SEL.InternationalBusRegistrationNumber);
               objcmd.Parameters.AddWithValue("@VerificationType", SEL.VerificationType);
               objcmd.Parameters.AddWithValue("@CompanyContactNo", SEL.CompanyContactNo);
               objcmd.Parameters.AddWithValue("@KeyID", SEL.KeyID);
               objcmd.Parameters.AddWithValue("@XmlData", SEL.XMLData);
               objcmd.Parameters.AddWithValue("@SearchInput", SEL.Searchinput);
               objcmd.Parameters.AddWithValue("@SearchOutput", SEL.SearchOutput);
               objcmd.Parameters.AddWithValue("@DetailsViewedDate", SEL.DetailsViewedDate);
               objcmd.Parameters.AddWithValue("@DetailsViewedYN", SEL.DetailsViewedYN);
               objcmd.Parameters.AddWithValue("@FileName", SEL.FileName);
             
               RowsEffected = objcmd.ExecuteNonQuery();
           }

           cn.Close();


           return RowsEffected;

       }

       public int UpdateSubscriberEnquiryLogError(SqlConnection cn, Entity.SubscriberEnquiryLog SEL)
       {

           if (cn.State == ConnectionState.Closed)
               cn.Open();

           string insertSQL = "UPDATE SubscriberEnquiryLog SET EnquiryStatusInd = 'F', EnquiryResultInd = 'E', ErrorDescription = '" + SEL.ErrorDescription.Replace("'", "''") + "' WHERE SubscriberEnquiryLogID = " + SEL.SubscriberEnquiryLogID.ToString();
           SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

           int added = 0;
           added = sqlcmd.ExecuteNonQuery();

           sqlcmd.Dispose();
           cn.Close();

           return added;
       }

       public int UpdateSubscriberEnquiryLogErrorAccv(SqlConnection cn, Entity.SubscriberEnquiryLog SEL)
       {

           if (cn.State == ConnectionState.Closed)
               cn.Open();

           string insertSQL = "UPDATE SubscriberEnquiryLog SET EnquiryStatusInd = '"+SEL.EnquiryStatus+"', EnquiryResultInd = '"+SEL.EnquiryResult+"', ErrorDescription = '" + SEL.ErrorDescription.Replace("'", "''") + "' WHERE SubscriberEnquiryLogID = " + SEL.SubscriberEnquiryLogID.ToString();
           SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

           int added = 0;
           added = sqlcmd.ExecuteNonQuery();

           sqlcmd.Dispose();
           cn.Close();

           return added;
       }

       public Entity.SubscriberEnquiryLog GetSubscriberEnquiryLogObject(SqlConnection cn, int intSubscriberEnquiryLogID)
       {
           // System.IO.File.AppendAllText(@"C:\Log\AVSResponse.txt", "Get result\n");
            if (cn.State == ConnectionState.Closed)
               cn.Open();

           Entity.SubscriberEnquiryLog se = new Entity.SubscriberEnquiryLog();


           string strSQL = "Select *, case EnquiryStatusInd when 'C' then 'Completed' when 'F' then 'Failed' when 'P' then 'Pending' end as 'EnquiryStatusDesc' from SubscriberEnquiryLog nolock Where SubscriberEnquiryLogID = " + intSubscriberEnquiryLogID.ToString();
           SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
           SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
           DataSet ds = new DataSet();
           sqlda.Fill(ds);

           foreach (DataRow r in ds.Tables[0].Rows)
           {
               se.SubscriberEnquiryLogID = int.Parse(r["SubscriberEnquiryLogID"].ToString());
               se.SubscriberEnquiryID = int.Parse(r["SubscriberEnquiryID"].ToString());
               se.EnquiryStatus = r["EnquiryStatusInd"].ToString();
               se.EnquiryResult = r["EnquiryResultInd"].ToString();
               se.ErrorDescription = r["ErrorDescription"].ToString();
               se.ProductID = int.Parse(r["ProductID"].ToString());
               se.SubscriberID = int.Parse(r["SubscriberID"].ToString());
               se.SystemUserID = int.Parse(r["SystemUserID"].ToString());
               se.SystemUserName = r["SystemUser"].ToString();
               se.CreatedByUser = r["CreatedByUser"].ToString();
               se.CreatedOnDate = DateTime.Parse(r["CreatedOnDate"].ToString());
               se.Searchinput = r["SearchInput"].ToString();
               se.ExtraintInput1 = int.Parse(r["ExtraIntInput1"].ToString());
               se.ExtraintInput2 = int.Parse(r["ExtraIntInput2"].ToString());
               se.ExtraVarInput1 = r["ExtraVarInput1"].ToString();
               se.ExtraVarInput2 = r["ExtraVarInput2"].ToString();
               se.ExtraVarInput3 = r["ExtraVarInput3"].ToString();
               se.BranchCode = r["BranchCode"].ToString();
               se.SubscriberReference = r["SubscriberReference"].ToString();
               se.IDNo = r["IDNo"].ToString();
               se.SurName = r["Surname"].ToString();
               se.BusBusinessName = r["BusBusinessName"].ToString();
               se.FirstName = r["FirstName"].ToString();
               se.AccountNo = r["AccountNo"].ToString();
               se.BranchName = r["BranchName"].ToString();
               se.BranchCode = r["BranchCode"].ToString();
               se.BankName = r["BankName"].ToString();
               se.BusRegistrationNo = r["BusRegistrationNo"].ToString();
               se.EmailAddress = r["Email"].ToString();
               se.KeyID = int.Parse(r["KeyID"].ToString());
               se.FileName = r["FileName"].ToString();                            
               se.Trust = r["TrustNo"].ToString();
               se.Terms = r["Terms"].ToString();
               if (!DBNull.Value.Equals(r["ReportFile"]))
               {
                   se.ReportFile = (byte[])r["ReportFile"];
               }
               se.Reference = r["Reference"].ToString();
               se.SubscriberReference = r["SubscriberReference"].ToString();
               se.AccHolder = r["AccHolder"].ToString();
               se.BusBusinessName = r["BusBusinessName"].ToString();
               se.XMLData = r["XMLData"].ToString();
               se.EntityType = r["EntityType"].ToString();
               se.SubscriberEnquiryDate = DateTime.Parse(r["SubscriberEnquiryDate"].ToString());

           }
           sqlda.Dispose();
           sqlcmd.Dispose();
           ds.Dispose();
           cn.Close();

           return se;
       }

       public DataSet GetSubscriberEnquiryLogDataSet(SqlConnection cn, int intSubscriberEnquiryLogID)
       {
           if (cn.State == ConnectionState.Closed)
               cn.Open();

           string strSQL = "Select *, case EnquiryStatusInd when 'C' then 'Completed' when 'F' then 'Failed' when 'P' then 'Pending' end as 'EnquiryStatusDesc' from SubscriberEnquiryLog nolock Where SubscriberEnquiryLogID = " + intSubscriberEnquiryLogID.ToString() + " ";
           SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
           SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
           DataSet ds = new DataSet();
           sqlda.Fill(ds);

           cn.Close();

           return ds;
       }

       public DataSet GetEnquiryLogDataSet(SqlConnection cn, int intSubscriberEnquiryLogID, int SubscriberID)
       {
           if (cn.State == ConnectionState.Closed)
               cn.Open();

           string strSQL = "Select * from SubscriberEnquiryLog nolock Where SubscriberEnquiryLogID = " + intSubscriberEnquiryLogID.ToString() + " and SubscriberID = " + SubscriberID.ToString();
           SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
           SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
           DataSet ds = new DataSet();
           sqlda.Fill(ds);

           cn.Close();

           return ds;
       }

       public DataSet GetCommercialID(SqlConnection cn, int intSubscriberEnquiryLogID)
       {
           if (cn.State == ConnectionState.Closed)
               cn.Open();

           string strSQL = "Exec spGetCommercialID " + intSubscriberEnquiryLogID.ToString();
           SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
           SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
           DataSet ds = new DataSet();
           sqlda.Fill(ds);

           cn.Close();

           return ds;
       }

       public DataSet GetAccountVerificationLogDataSet(SqlConnection cn, int intProductID)
       {
           if (cn.State == ConnectionState.Closed)
               cn.Open();

           string strSQL = "Select * from SubscriberEnquiryLog nolock Where ProductID = " + intProductID.ToString() + " and EnquiryStatusInd = 'P' and ExtraIntInput1 <> 3 ";
           //string strSQL = "Select * from SubscriberEnquiryLog nolock Where ProductID = " + intProductID.ToString() + " and EnquiryStatusInd = 'P' and Datediff(n,isnull(ChangedOnDate, CreatedOnDate),getdate()) >= 90  and Datediff(n,CreatedOnDate,getdate()) <= 180";
           SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
           SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
           DataSet ds = new DataSet();
           sqlda.Fill(ds);

           cn.Close();

           return ds;
       }

       public DataSet GetBankCodesDataSet(SqlConnection cn, int intProductID)
       {
           if (cn.State == ConnectionState.Closed)
               cn.Open();

           string strSQL = "Select * from SubscriberEnquiryLog nolock Where ProductID = " + intProductID.ToString() + " and EnquiryStatusInd = 'P' and KeyID > 0";
           //string strSQL = "Select * from SubscriberEnquiryLog nolock Where ProductID = " + intProductID.ToString() + " and EnquiryStatusInd = 'P' and Datediff(n,isnull(ChangedOnDate, CreatedOnDate),getdate()) >= 90  and Datediff(n,CreatedOnDate,getdate()) <= 180";
           SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
           SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
           DataSet ds = new DataSet();
           sqlda.Fill(ds);

           cn.Close();

           return ds;
       }

       public DataSet GetSubscriberEnquiryLogDataSetperproduct(SqlConnection cn, int intProductID)
       {
           if (cn.State == ConnectionState.Closed)
               cn.Open();

           string strSQL = "Select * from SubscriberEnquiryLog nolock Where ProductID = " + intProductID.ToString() + " and EnquiryStatusInd = 'P'";
           SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
           SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
           DataSet ds = new DataSet();
           sqlda.Fill(ds);

           cn.Close();

           return ds;
       }

       public DataSet GetSubscriberEnquiryLogDataSetpersubscriber(SqlConnection cn, int intSubscriberID, int intProductID)
       {
           if (cn.State == ConnectionState.Closed)
               cn.Open();

           string strSQL = "Select *, case EnquiryStatusInd when 'C' then 'Completed' when 'F' then 'Failed' when 'P' then 'Pending' end as 'EnquiryStatusDesc' from SubscriberEnquiryLog nolock Where SubscriberID = " + intSubscriberID.ToString() + " and ProductID = " + intProductID.ToString() + " order by SubscriberEnquiryLogID desc";
           SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
           SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
           DataSet ds = new DataSet();
           sqlda.Fill(ds);

           cn.Close();

           return ds;
       }

       public DataSet GetSubscriberEnquiryLogDataSetpersubscriber(SqlConnection cn, int intSystemUserID, string strproductID, string strEnquiryStatusInd)
       {
           if (cn.State == ConnectionState.Closed)
               cn.Open();

           string strSQL = "Select *, case EnquiryStatusInd when 'C' then 'Completed' when 'F' then 'Failed' when 'P' then 'Pending' end as 'EnquiryStatusDesc' from SubscriberEnquiryLog nolock Where SystemUserID = " + intSystemUserID.ToString() + " and productID in (" + strproductID + ") and EnquiryStatusInd = '" + strEnquiryStatusInd + "'order by SubscriberEnquiryLogID desc";
           SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
           SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
           DataSet ds = new DataSet("RequestHistory");
           sqlda.Fill(ds,"HistoryItem");

           cn.Close();

           return ds;
       }

       public DataSet GetSubscriberEnquiryLogDataSetpersubscriber(SqlConnection cn, int intSystemUserID, string strproductID, string strEnquiryStatusInd, int intSubscriberID)
       {
           if (cn.State == ConnectionState.Closed)
               cn.Open();

           string strSQL = "Select top 100 *, case EnquiryStatusInd when 'C' then 'Completed' when 'F' then 'Failed' when 'P' then 'Pending' end as 'EnquiryStatusDesc' from SubscriberEnquiryLog nolock Where SubscriberID = " + intSubscriberID.ToString() + " and productID in (" + strproductID + ")";

            if (intSystemUserID > 0)
            {
                strSQL = strSQL + " and SystemUserID = " + intSystemUserID.ToString();
            }
            if (!(strEnquiryStatusInd == string.Empty))
            {
                strSQL = strSQL + " and EnquiryStatusInd = '" + strEnquiryStatusInd + "'";
            }
          
           strSQL = strSQL + " order by SubscriberEnquiryLogID desc";
           
           SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
           SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
           DataSet ds = new DataSet("RequestHistory");
           sqlda.Fill(ds,"HistoryItem");

           cn.Close();

           return ds;
       }

       public DataSet GetSubscriberEnquiryLogDataSetpersubscriber(SqlConnection cn, int intSystemUserID, string strproductID, string strEnquiryStatusInd, int intSubscriberID, int intEnquiryLogID)
       {
           if (cn.State == ConnectionState.Closed)
               cn.Open();

           string strSQL = "Select top 100 *, case EnquiryStatusInd when 'C' then 'Completed' when 'F' then 'Failed' when 'P' then 'Pending' end as 'EnquiryStatusDesc' from SubscriberEnquiryLog nolock Where SubscriberEnquiryLogID = " + intEnquiryLogID.ToString() + " and SubscriberID = " + intSubscriberID.ToString() + " and productID in (" + strproductID + ")";

           if (intSystemUserID > 0)
           {
               strSQL = strSQL + " and SystemUserID = " + intSystemUserID.ToString();
           }
           if (!(strEnquiryStatusInd == string.Empty))
           {
               strSQL = strSQL + " and EnquiryStatusInd = '" + strEnquiryStatusInd + "'";
           }

           strSQL = strSQL + " order by SubscriberEnquiryLogID desc";

           SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
           SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
           DataSet ds = new DataSet("RequestHistory");
           sqlda.Fill(ds, "HistoryItem");

           cn.Close();

           return ds;
       }

       public DataSet GetRealTimeSubscriberEnquiryLogDataSet(SqlConnection cn, int intSystemUserID, string strproductID, string strEnquiryStatusInd, int intSubscriberID)
       {
           if (cn.State == ConnectionState.Closed)
               cn.Open();

           string strSQL = "Select top 100 *, case EnquiryStatusInd when 'C' then 'Completed' when 'F' then 'Failed' when 'P' then 'Pending' end as 'EnquiryStatusDesc' from SubscriberEnquiryLog nolock Where SubscriberID = " + intSubscriberID.ToString() + " and productID = " + strproductID + " and ExtraIntInput1 = 3";

           if (intSystemUserID > 0)
           {
               strSQL = strSQL + " and SystemUserID = " + intSystemUserID.ToString();
           }
           if (!(strEnquiryStatusInd == string.Empty))
           {
               strSQL = strSQL + " and EnquiryStatusInd = '" + strEnquiryStatusInd + "'";
           }

           strSQL = strSQL + " order by SubscriberEnquiryLogID desc";

           SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
           SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
           DataSet ds = new DataSet("RequestHistory");
           sqlda.Fill(ds,"HistoryItem");

           cn.Close();

           return ds;
       }

       public DataSet GetRealTimeSubscriberEnquiryLogDataSet(SqlConnection cn, int intSystemUserID, string strproductID, string strEnquiryStatusInd, int intSubscriberID, int EnquiryLogID)
       {
           if (cn.State == ConnectionState.Closed)
               cn.Open();

           string strSQL = "Select top 100 *, case EnquiryStatusInd when 'C' then 'Completed' when 'F' then 'Failed' when 'P' then 'Pending' end as 'EnquiryStatusDesc' from SubscriberEnquiryLog nolock Where SubscriberEnquiryLogID = " + EnquiryLogID.ToString() + " and SubscriberID = " + intSubscriberID.ToString() + " and productID = " + strproductID + " and ExtraIntInput1 = 3";

           if (intSystemUserID > 0)
           {
               strSQL = strSQL + " and SystemUserID = " + intSystemUserID.ToString();
           }
           if (!(strEnquiryStatusInd == string.Empty))
           {
               strSQL = strSQL + " and EnquiryStatusInd = '" + strEnquiryStatusInd + "'";
           }

           strSQL = strSQL + " order by SubscriberEnquiryLogID desc";

           SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
           SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
           DataSet ds = new DataSet("RequestHistory");
           sqlda.Fill(ds, "HistoryItem");

           cn.Close();

           return ds;
       }

       public DataSet GetRealTimeSubscriberEnquiryLogDataSet(SqlConnection cn, int intSystemUserID, string strproductID, string strEnquiryStatusInd)
       {
           if (cn.State == ConnectionState.Closed)
               cn.Open();

           string strSQL = "Select *, case EnquiryStatusInd when 'C' then 'Completed' when 'F' then 'Failed' when 'P' then 'Pending' end as 'EnquiryStatusDesc' from SubscriberEnquiryLog nolock Where SystemUserID = " + intSystemUserID.ToString() + " and productID = " + strproductID + " and EnquiryStatusInd = '" + strEnquiryStatusInd + "' and ExtraIntInput1 = 3 order by SubscriberEnquiryLogID desc";
           SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
           SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
           DataSet ds = new DataSet("RequestHistory");
           sqlda.Fill(ds,"HistoryItem");

           cn.Close();

           return ds;
       }

       public DataSet GetPendingEmailRequests(SqlConnection cn, int intProductID)
       {
           if (cn.State == ConnectionState.Closed)
               cn.Open();

           string strSQL = "Select * from SubscriberEnquiryLog nolock Where ProductID = " + intProductID.ToString() + " and EnquiryStatusInd = 'C' and KeyID > 0 and EmailSent = 0 order by retrycount ";
           //string strSQL = "Select * from SubscriberEnquiryLog nolock Where ProductID = " + intProductID.ToString() + " and EnquiryStatusInd = 'P' and Datediff(n,isnull(ChangedOnDate, CreatedOnDate),getdate()) >= 90  and Datediff(n,CreatedOnDate,getdate()) <= 180";
           SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
           SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
           DataSet ds = new DataSet();
           sqlda.Fill(ds);

           cn.Close();

           return ds;
       }

       public DataSet GetSubscriberLogo(SqlConnection cn, int intProductID, int intReportID, int intSubscriberID)
       {
           if (cn.State == ConnectionState.Closed)
               cn.Open();

           string strSQL = "select SubscriberID, ProductID,ReportID, LogoImage as LogoImage from SubscriberLogo nolock where SubscriberID = " + intSubscriberID + " and ProductID = " + intProductID + " and ReportID = " + intReportID;
           SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
           SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
           DataSet ds = new DataSet();
           sqlda.Fill(ds);

           cn.Close();

           if (ds.Tables.Count > 0)
               ds.Tables[0].TableName = "SubscriberLogo";

           return ds;
       }

       public int SetEmailFlag(SqlConnection cn,int intSubscriberEnquiryLogID,bool bEmailSent, int intRetryCount)
       {
           if (cn.State == ConnectionState.Closed)
               cn.Open();

           string strSQL = "Update SubscriberEnquiryLog Set EmailSent = '" + bEmailSent.ToString() + "' ,RetryCount=RetryCount+" + intRetryCount.ToString() + " Where SubscriberEnquiryLogID = " + intSubscriberEnquiryLogID.ToString() + " and EnquiryStatusInd = 'C' and KeyID > 0 and EmailSent = 0";
           //string strSQL = "Select * from SubscriberEnquiryLog nolock Where ProductID = " + intProductID.ToString() + " and EnquiryStatusInd = 'P' and Datediff(n,isnull(ChangedOnDate, CreatedOnDate),getdate()) >= 90  and Datediff(n,CreatedOnDate,getdate()) <= 180";
           SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
           int rowseffected = sqlcmd.ExecuteNonQuery();

           return rowseffected;
       }

       public Entity.SubscriberEnquiryLog ErrorGetSubscriberEnquiryLogObject(SqlConnection cn, int intSubscriberEnquiryLogID)
       {

           Entity.SubscriberEnquiryLog se = new Entity.SubscriberEnquiryLog();

           if (cn.State == ConnectionState.Closed)
               cn.Open();

           string strSQL = "EXEC [sp_u_ErrorSubscriberEnquiryLog] " + intSubscriberEnquiryLogID.ToString();
           SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
           SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
           DataSet ds = new DataSet();
           sqlda.Fill(ds);

           foreach (DataRow r in ds.Tables[0].Rows)
           {
               se.SubscriberEnquiryLogID = int.Parse(r["SubscriberEnquiryLogID"].ToString());
               se.EnquiryStatus = r["EnquiryStatusInd"].ToString();
               se.EnquiryResult = r["EnquiryResultInd"].ToString();
               se.ErrorDescription = r["ErrorDescription"].ToString();
               se.ProductID = int.Parse(r["ProductID"].ToString());
               se.SubscriberID = int.Parse(r["SubscriberID"].ToString());
               se.SystemUserID = int.Parse(r["SystemUserID"].ToString());
               //se.SystemUser = r["SystemUser"].ToString();
               //se.EnquiryReason = r["EnquiryReason"].ToString();
               se.CreatedByUser = r["CreatedByUser"].ToString();
               se.CreatedOnDate = DateTime.Parse(r["CreatedOnDate"].ToString());
               //se.SearchInput = r["SearchInput"].ToString();
               //se.ExtraIntInput1 = int.Parse(r["ExtraIntInput1"].ToString());
               //se.ExtraIntInput2 = int.Parse(r["ExtraIntInput2"].ToString());
               se.ExtraVarInput1 = r["ExtraVarInput1"].ToString();
               se.ExtraVarInput2 = r["ExtraVarInput2"].ToString();
               se.ExtraVarInput3 = r["ExtraVarInput3"].ToString();
               se.BranchCode = r["BranchCode"].ToString();
               //se.PurposeID = int.Parse(r["PurposeID"].ToString());
               se.SubscriberReference = r["SubscriberReference"].ToString();
               se.IDNo = r["IDNo"].ToString();
               //se.Surname = r["Surname"].ToString();
               se.BusRegistrationNo = r["BusRegistrationNo"].ToString();
               se.BusBusinessName = r["BusBusinessName"].ToString();
               //se.MaidenName = r["MaidenName"].ToString();
               se.AccountNo = r["AccountNo"].ToString();
               se.FirstName = r["FirstName"].ToString();
               //se.NetMonthlyIncomeAmt = decimal.Parse(r["NetMonthlyIncomeAmt"].ToString());
           }
           sqlda.Dispose();
           sqlcmd.Dispose();
           ds.Dispose();

           cn.Close();
           return se;
       }

    }
}
