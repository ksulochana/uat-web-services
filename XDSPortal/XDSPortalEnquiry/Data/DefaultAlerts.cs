﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace XDSPortalEnquiry.Data
{
   public  class DefaultAlerts
    {
       public DefaultAlerts()
       {
       }

       public DataSet GetConsumerDefaultAlertHistory(SqlConnection Enquirycon,int SubscriberID)
       {
           if (Enquirycon.State == ConnectionState.Closed)
               Enquirycon.Open();

           string strSQL = "SELECT top 1  ServerConnectionString nolock FROM dbo.EnquiryServers where Active = 1";
           SqlCommand cmdconstr = new SqlCommand(strSQL, Enquirycon);
           SqlDataReader dr = cmdconstr.ExecuteReader();
           dr.Read();
           string strconstring = dr.GetValue(0).ToString();
           dr.Close();
           
           Enquirycon.Close();

           SqlConnection con = new SqlConnection(strconstring);
           
           con.Open();
           DataSet ds = new DataSet("ConsumerDefaultAlerts");

           SqlCommand cmd = new SqlCommand("sp_GetOnlineConsumerDefaultAlerts", con);
           cmd.CommandType = CommandType.StoredProcedure;
           cmd.Parameters.AddWithValue("@SubscriberID", SubscriberID);

           SqlDataAdapter da = new SqlDataAdapter(cmd);
           da.Fill(ds);

           foreach (DataTable dt in ds.Tables)
           {
               if (!(ds.Tables.Contains("DefaultAlerts")))
               {
                   dt.TableName = "DefaultAlerts";
               }
           }

           con.Close();
           return ds;
       }

       public DataSet GetConsumerDefaultAlertDataSet(SqlConnection Enquirycon, int ConsumerDefaultAlertID)
       {
           if (Enquirycon.State == ConnectionState.Closed)
               Enquirycon.Open();

           string strSQL = "SELECT top 1  ServerConnectionString nolock FROM dbo.EnquiryServers where Active = 1";
           SqlCommand cmdconstr = new SqlCommand(strSQL, Enquirycon);
           SqlDataReader dr = cmdconstr.ExecuteReader();
           dr.Read();
           string strconstring = dr.GetValue(0).ToString();
           dr.Close();
           
           Enquirycon.Close();

           SqlConnection con = new SqlConnection(strconstring);
           
           con.Open();           

           DataSet ds = new DataSet("ConsumerDefaultAlerts");

           SqlCommand cmd = new SqlCommand("sp_GetConsumerAlertAccno", con);
           cmd.CommandType = CommandType.StoredProcedure;
           cmd.Parameters.AddWithValue("@ID", ConsumerDefaultAlertID);

           SqlDataAdapter da = new SqlDataAdapter(cmd);
           da.Fill(ds);

           foreach (DataTable dt in ds.Tables)
           {
               if (!(ds.Tables.Contains("DefaultAlerts")))
               {
                   dt.TableName = "DefaultAlerts";
               }
           }

           con.Close();
           return ds;
       }

       public int RemoveConsumerDefaultAlert(SqlConnection Enquirycon, int OnlineConsumerDefaultAlertID, string Username, int SystemuserID, string ClientResponsibleToNotify)
       {
           if (Enquirycon.State == ConnectionState.Closed)
               Enquirycon.Open();

           string strSQL = "SELECT top 1  ServerConnectionString nolock FROM dbo.EnquiryServers where Active = 1";
           SqlCommand cmdconstr = new SqlCommand(strSQL, Enquirycon);
           SqlDataReader dr = cmdconstr.ExecuteReader();
           dr.Read();
           string strconstring = dr.GetValue(0).ToString();
           dr.Close();

           Enquirycon.Close();

           SqlConnection con = new SqlConnection(strconstring);

           con.Open();
           
           SqlCommand cmd = new SqlCommand("sp_RemoveOnlineConsumerDefaultAlerts", con);
           cmd.CommandType = CommandType.StoredProcedure;
           cmd.Parameters.AddWithValue("@ReffNo", OnlineConsumerDefaultAlertID);
           cmd.Parameters.AddWithValue("@UserName", Username);
           cmd.Parameters.AddWithValue("@SystemUserID", SystemuserID);
           cmd.Parameters.AddWithValue("@ResponsibleToNotifyClientForDeListing", ClientResponsibleToNotify);

           int rowseffected = cmd.ExecuteNonQuery();
           con.Close();
           return rowseffected;
       }

       public int MoveConsumerPaymentNotifications(SqlConnection Enquirycon, int OnlineConsumerDefaultAlertID, string Username, int SystemuserID, string ContantNo,string EmailAddress, string ClientResponsibleToNotify)
       {
           if (Enquirycon.State == ConnectionState.Closed)
               Enquirycon.Open();

           string strSQL = "SELECT top 1  ServerConnectionString nolock FROM dbo.EnquiryServers where Active = 1";
           SqlCommand cmdconstr = new SqlCommand(strSQL, Enquirycon);
           SqlDataReader dr = cmdconstr.ExecuteReader();
           dr.Read();
           string strconstring = dr.GetValue(0).ToString();
           dr.Close();

           Enquirycon.Close();

           SqlConnection con = new SqlConnection(strconstring);

           con.Open();

           SqlCommand cmd = new SqlCommand("sp_MoveConsumerListingToDefaultAlerts", con);
           cmd.CommandType = CommandType.StoredProcedure;
           cmd.Parameters.AddWithValue("@ReffNo", OnlineConsumerDefaultAlertID);
           cmd.Parameters.AddWithValue("@UserName", Username);
           cmd.Parameters.AddWithValue("@SystemUserID", SystemuserID);
           cmd.Parameters.AddWithValue("@CellNo", ContantNo);
           cmd.Parameters.AddWithValue("@MailID", EmailAddress);
           cmd.Parameters.AddWithValue("@ResponsibleToNotifyClientForMove", ClientResponsibleToNotify);

           int rowseffected = cmd.ExecuteNonQuery();
           con.Close();
           return rowseffected;
       }

       public int InsertSMSNotifications(SqlConnection con, XDSPortalLibrary.Entity_Layer.SMSNotification oSMSNotification)
       {
           if (con.State == ConnectionState.Closed)
           con.Open();
           
           SqlCommand cmd = new SqlCommand("sp_I_Notifications", con);
           cmd.CommandType = CommandType.StoredProcedure;
           cmd.Parameters.AddWithValue("@Subscribername", oSMSNotification.SubscriberName);
           cmd.Parameters.AddWithValue("@MobileNumber", oSMSNotification.ContactNo);
           cmd.Parameters.AddWithValue("@ClientContactDetails", oSMSNotification.ClientContactDetails);
           cmd.Parameters.AddWithValue("@IsCommercial", oSMSNotification.IsCommercial);
           cmd.Parameters.AddWithValue("@Accountno", oSMSNotification.AccountNo);
           cmd.Parameters.AddWithValue("@AlertType", oSMSNotification.AlertType);
           cmd.Parameters.AddWithValue("@SubscriberID", oSMSNotification.SubscriberID);
           cmd.Parameters.AddWithValue("@NotificationMessage", oSMSNotification.Message);

           SqlParameter oparam = new SqlParameter("@QueueID", SqlDbType.Int);
           oparam.Direction = ParameterDirection.ReturnValue;
           cmd.Parameters.Add(oparam);

           cmd.ExecuteNonQuery();
           con.Close();

           int QueueID = int.Parse(oparam.Value.ToString());
           return QueueID;
       }

       public string GetOTPMessage(SqlConnection con, XDSPortalLibrary.Entity_Layer.SMSNotification oSMSNotification)
       {
           if (con.State == ConnectionState.Closed)
               con.Open();

           SqlCommand cmd = new SqlCommand("spAuthentication_GetOTPMessage", con);
           cmd.CommandType = CommandType.StoredProcedure;
           cmd.Parameters.AddWithValue("@SubscriberID", oSMSNotification.SubscriberID);
           cmd.Parameters.AddWithValue("@Subscribername", oSMSNotification.SubscriberName);
           cmd.Parameters.AddWithValue("@Accountno", oSMSNotification.AccountNo);

           SqlParameter oparam = new SqlParameter("@OTPMessage", SqlDbType.VarChar, 5000);
           oparam.Direction = ParameterDirection.Output;
           cmd.Parameters.Add(oparam);

           cmd.ExecuteNonQuery();
           con.Close();

           string OTPMessage = oparam.Value.ToString();
           return OTPMessage;
       }

       public int InsertCredit4LifeNotifications(SqlConnection con, XDSPortalLibrary.Entity_Layer.SMSNotification oSMSNotification)
       {
           if (con.State == ConnectionState.Closed)
               con.Open();

           SqlCommand cmd = new SqlCommand("sp_I_Notifications", con);
           cmd.CommandType = CommandType.StoredProcedure;
           cmd.Parameters.AddWithValue("@Subscribername", oSMSNotification.SubscriberName);
           cmd.Parameters.AddWithValue("@MobileNumber", oSMSNotification.ContactNo);
           cmd.Parameters.AddWithValue("@ClientContactDetails", oSMSNotification.Message);
           cmd.Parameters.AddWithValue("@IsCommercial", oSMSNotification.IsCommercial);
           cmd.Parameters.AddWithValue("@Accountno", oSMSNotification.AccountNo);
           cmd.Parameters.AddWithValue("@AlertType", oSMSNotification.AlertType);
           cmd.Parameters.AddWithValue("@SubscriberID", oSMSNotification.SubscriberID);

           SqlParameter oparam = new SqlParameter("@QueueID", SqlDbType.Int);
           oparam.Direction = ParameterDirection.ReturnValue;
           cmd.Parameters.Add(oparam);

           cmd.ExecuteNonQuery();
           con.Close();

           int QueueID = int.Parse(oparam.Value.ToString());
           return QueueID;
       }

       public DataSet GetCommercialDefaultAlertHistory(SqlConnection Enquirycon, int SubscriberID)
       {
           if (Enquirycon.State == ConnectionState.Closed)
               Enquirycon.Open();

           string strSQL = "SELECT top 1  ServerConnectionString nolock FROM dbo.EnquiryServers where Active = 1";
           SqlCommand cmdconstr = new SqlCommand(strSQL, Enquirycon);
           SqlDataReader dr = cmdconstr.ExecuteReader();
           dr.Read();
           string strconstring = dr.GetValue(0).ToString();
           dr.Close();

           Enquirycon.Close();

           SqlConnection con = new SqlConnection(strconstring);

           con.Open();
           DataSet ds = new DataSet("CommercialDefaultAlerts");

           SqlCommand cmd = new SqlCommand("sp_GetOnlineCommercialDefaultAlerts", con);
           cmd.CommandType = CommandType.StoredProcedure;
           cmd.Parameters.AddWithValue("@SubscriberID", SubscriberID);

           SqlDataAdapter da = new SqlDataAdapter(cmd);
           da.Fill(ds);
           
           foreach (DataTable dt in ds.Tables)
           {
               if (!(ds.Tables.Contains("DefaultAlerts")))
               {
                   dt.TableName = "DefaultAlerts";
               }
           }

           con.Close();
           return ds;
       }

       public DataSet GetCommercialDefaultAlertDataSet(SqlConnection Enquirycon, int CommercialDefaultAlertID)
       {
           if (Enquirycon.State == ConnectionState.Closed)
               Enquirycon.Open();

           string strSQL = "SELECT top 1  ServerConnectionString nolock FROM dbo.EnquiryServers where Active = 1";
           SqlCommand cmdconstr = new SqlCommand(strSQL, Enquirycon);
           SqlDataReader dr = cmdconstr.ExecuteReader();
           dr.Read();
           string strconstring = dr.GetValue(0).ToString();
           dr.Close();

           Enquirycon.Close();

           SqlConnection con = new SqlConnection(strconstring);

           con.Open();

           DataSet ds = new DataSet("CommercialDefaultAlerts");

           SqlCommand cmd = new SqlCommand("sp_GetComercialAlertAccno", con);
           cmd.CommandType = CommandType.StoredProcedure;
           cmd.Parameters.AddWithValue("@ID", CommercialDefaultAlertID);

           SqlDataAdapter da = new SqlDataAdapter(cmd);
           da.Fill(ds);

           foreach (DataTable dt in ds.Tables)
           {
               if (!(ds.Tables.Contains("DefaultAlerts")))
               {
                   dt.TableName = "DefaultAlerts";
               }
           }

           con.Close();
           return ds;
       }

       public int RemoveCommercialDefaultAlert(SqlConnection Enquirycon, int OnlineCommercialDefaultAlertID, string Username, int SystemuserID, string ClientResponsibleToNotify)
       {
           if (Enquirycon.State == ConnectionState.Closed)
               Enquirycon.Open();

           string strSQL = "SELECT top 1  ServerConnectionString nolock FROM dbo.EnquiryServers where Active = 1";
           SqlCommand cmdconstr = new SqlCommand(strSQL, Enquirycon);
           SqlDataReader dr = cmdconstr.ExecuteReader();
           dr.Read();
           string strconstring = dr.GetValue(0).ToString();
           dr.Close();

           Enquirycon.Close();

           SqlConnection con = new SqlConnection(strconstring);

           con.Open();
           
           SqlCommand cmd = new SqlCommand("sp_RemoveOnlineCommercialDefaultAlerts", con);
           cmd.CommandType = CommandType.StoredProcedure;
           cmd.Parameters.AddWithValue("@ReffNo", OnlineCommercialDefaultAlertID);
           cmd.Parameters.AddWithValue("@UserName", Username);
           cmd.Parameters.AddWithValue("@ResponsibleToNotifyClientForDeListing", ClientResponsibleToNotify);

           int rowseffected = cmd.ExecuteNonQuery();
           con.Close();
           return rowseffected;
       }

       public int MoveCommercialDefaultAlert(SqlConnection Enquirycon, int OnlineCommercialDefaultAlertID, string Username, int SystemuserID, string ContantNo, string EmailAddress, string ClientResponsibleToNotify)
       {
           if (Enquirycon.State == ConnectionState.Closed)
               Enquirycon.Open();

           string strSQL = "SELECT top 1  ServerConnectionString nolock FROM dbo.EnquiryServers where Active = 1";
           SqlCommand cmdconstr = new SqlCommand(strSQL, Enquirycon);
           SqlDataReader dr = cmdconstr.ExecuteReader();
           dr.Read();
           string strconstring = dr.GetValue(0).ToString();
           dr.Close();

           Enquirycon.Close();

           SqlConnection con = new SqlConnection(strconstring);

           con.Open();

           SqlCommand cmd = new SqlCommand("sp_MoveCommercialListingToDefaultAlerts", con);
           cmd.CommandType = CommandType.StoredProcedure;
           cmd.Parameters.AddWithValue("@ReffNo", OnlineCommercialDefaultAlertID);
           cmd.Parameters.AddWithValue("@UserName", Username);
           cmd.Parameters.AddWithValue("@CellNo", ContantNo);
           cmd.Parameters.AddWithValue("@MailID", EmailAddress);
           cmd.Parameters.AddWithValue("@ResponsibleToNotifyClientForMove", ClientResponsibleToNotify);
           

           int rowseffected = cmd.ExecuteNonQuery();
           con.Close();
           return rowseffected;
       }

     

       public DataSet GetStatusCodesDataSet(SqlConnection con, string ListingType)
       {
           if (con.State == ConnectionState.Closed)
               con.Open();
           DataSet ds = new DataSet("DefaultAlertStatusCodes");
           string strSQL = string.Empty;

           if (ListingType != string.Empty)
           {
               strSQL = "Select (StatusCode +' - '+Description) StatuscodeDesc, * from DefaultAlertStatusCode nolock where RecordStatusInd = 'A' and StatusCodeType = '" + ListingType + "'";
           }
           else
           
           {
               throw new Exception("Please enter a valid Listing type");
           }


           SqlCommand cmd = new SqlCommand(strSQL, con);

           SqlDataAdapter da = new SqlDataAdapter(cmd);
           da.Fill(ds);

           foreach (DataTable dt in ds.Tables)
           {
               if (!(ds.Tables.Contains("StatusCodes")))
               {
                   dt.TableName = "StatusCodes";
               }
           }

           con.Close();

           return ds;
       }

       public DataSet GetAccountTypesDataSet(SqlConnection con, string ListingType)
       {
           if (con.State == ConnectionState.Closed)
               con.Open();
           DataSet ds = new DataSet("DefaultAlertAccountTypes");
           string strSQL = string.Empty;
           if (ListingType.ToLower() == "consumer")
           {
               strSQL = "Select (AccountType +' - '+ Description) AccountTypeDesc,* from DefaultAlertAccountTypes nolock where RecordStatusInd = 'A' and consumer = 1";
           }
           else if (ListingType.ToLower() == "commercial")
           {
               strSQL = "Select (AccountType +' - '+ Description) AccountTypeDesc,* from DefaultAlertAccountTypes nolock where RecordStatusInd = 'A' and commercial = 1";
           }
           else
           {
               throw new Exception("Please enter a valid Listing type");
           }

           SqlCommand cmd = new SqlCommand(strSQL, con);

           SqlDataAdapter da = new SqlDataAdapter(cmd);
           da.Fill(ds);
           foreach (DataTable dt in ds.Tables)
           {
               if (!(ds.Tables.Contains("AccountTypes")))
               {
                   dt.TableName = "AccountTypes";
               }
           }
           con.Close();

           return ds;
       }

       public int GetStatusCodes(SqlConnection con, string strStatusCode)
       {
           if (con.State == ConnectionState.Closed)
               con.Open();
           DataSet ds = new DataSet();
           string strSQL = "Select (StatusCode +' - '+Description) StatuscodeDesc, * from DefaultAlertStatusCode nolock where RecordStatusInd = 'A' and StatusCode = '"+ strStatusCode +"'";
           SqlCommand cmd = new SqlCommand(strSQL, con);
           
           SqlDataAdapter da = new SqlDataAdapter(cmd);
           da.Fill(ds);

           int RowsEffected = ds.Tables[0].Rows.Count;

           con.Close();
           ds.Dispose();

           return RowsEffected;
       }

       public int GetAccountTypes(SqlConnection con,string strAccountType)
       {
           if (con.State == ConnectionState.Closed)
               con.Open();
           DataSet ds = new DataSet();
           string strSQL = "Select (AccountType +' - '+ Description) AccountTypeDesc,* from DefaultAlertAccountTypes nolock where RecordStatusInd = 'A' and AccountType = '" + strAccountType +"'";
           SqlCommand cmd = new SqlCommand(strSQL, con);

           SqlDataAdapter da = new SqlDataAdapter(cmd);
           da.Fill(ds);

           int RowsEffected = ds.Tables[0].Rows.Count;

           con.Close();
           ds.Dispose();

           return RowsEffected;
       }
     

    }
}
