
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using XDSPortalLibrary.Entity_Layer;

namespace XDSPortalEnquiry.Data
{
    public class SubscriberEnquiry
    {

        public SubscriberEnquiry() { }

        public bool CheckEnquiryLimit(SqlConnection cn, int SubscriberID, int ProductID, int EnquiryLimit)
        {

            if (cn.State == ConnectionState.Closed)
                cn.Open();


            SqlCommand sqlcmd = new SqlCommand("sp_check_EnquiryLimit_WS", cn);
            sqlcmd.CommandType = CommandType.StoredProcedure;
            sqlcmd.CommandTimeout = 0;

            sqlcmd.Parameters.AddWithValue("@SubscriberID", SubscriberID);
            sqlcmd.Parameters.AddWithValue("@ProductID", ProductID);
            sqlcmd.Parameters.AddWithValue("@EnquiryLimit", EnquiryLimit);

            SqlParameter oparam = new SqlParameter();
            oparam.Direction = ParameterDirection.ReturnValue;

            sqlcmd.Parameters.Add(oparam);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();


            bool value = (oparam.Value.ToString() == "0" ? false : true);


            return value;

        }
        public int InsertSubscriberEnquiry(string DBConnection, Entity.SubscriberEnquiry S)
        {


            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = DBConnection;
            cn.Open();
            if (!string.IsNullOrEmpty(S.SearchInput))
                S.SearchInput = S.SearchInput.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.Surname))
                S.Surname = S.Surname.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.MaidenName))
                S.MaidenName = S.MaidenName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.FirstName))
                S.FirstName = S.FirstName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SecondName))
                S.SecondName = S.SecondName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.BusRegistrationNo))
                S.BusRegistrationNo = S.BusRegistrationNo.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.BusBusinessName))
                S.BusBusinessName = S.BusBusinessName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.ErrorDescription))
                S.ErrorDescription = S.ErrorDescription.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SubscriberName))
                S.SubscriberName = S.SubscriberName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SystemUser))
                S.SystemUser = S.SystemUser.Replace("'", "''");

            string insertSQL = "EXEC sp_i_SubscriberEnquiry " + S.SubscriberID + " , '" + S.SubscriberName + "' , " + S.SystemUserID + " , '" + S.SystemUser + "' , '" + S.EnquiryReason + "' , '" + S.SubscriberEnquiryDate + "' , '" + S.SubscriberReference + "' , '" + S.EnquiryStatus + "' , '" + S.EnquiryResult + "' , " + S.ProductID + " , '" + S.SearchInput + "' , '" + S.IDNo + "' , '" + S.PassportNo + "' , '" + S.Surname + "' , '" + S.FirstInitial + "' , '" + S.SecondInitial + "' , '" + S.MaidenName + "' , '" + S.Gender + "' , '" + S.FirstName + "' , '" + S.SecondName + "' , '" + S.BirthDate + "' , '" + S.TelephoneCode + "' , '" + S.TelephoneNo + "' , '" + S.AccountNo + "' , '" + S.SubAccountNo + "' , '" + S.BusRegistrationNo + "' , '" + S.BusBusinessName + "' , '" + S.BusVatNumber + "' , '" + S.AgeSearchTypeInd + "' , " + S.Age + " , " + S.AgeBirthYear + " , " + S.AgeDeviation + " , '" + S.ErrorDescription + "' , '" + S.ExtraVarInput1 + "' , '" + S.ExtraVarInput2 + "' , '|" + XDSPortalEnquiry.Entity.SystemName.name.ToString()+"|"+S.ExtraVarInput3.Trim() + "' , " + S.ExtraIntInput1 + " , " + S.ExtraIntInput2 + " , '" + S.BranchCode + "' , " + S.PurposeID + " , '" + S.CreatedByUser + "' , '" + S.CreatedOnDate + "' , '" + S.ChangedByUser + "' , '" + S.ChangedOnDate + "' ";
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();

            string strSQL = "SELECT @@IDENTITY";
            SqlCommand Imycommand = new SqlCommand(strSQL, cn);
            SqlDataReader Idr = Imycommand.ExecuteReader();
            Idr.Read();
            int intSubscriberEnquiryID = Convert.ToInt32(Idr.GetValue(0));
            Idr.Close();
            cn.Close();

            sqlcmd.Dispose();
            Imycommand.Dispose();
            Idr.Dispose();
            return intSubscriberEnquiryID;
        }

        //public int InsertSubscriberEnquiry(SqlConnection cn, Entity.SubscriberEnquiry S)
        //{
        //    if (cn.State == ConnectionState.Closed)
        //        cn.Open();

        //    if (!string.IsNullOrEmpty(S.SearchInput))
        //        S.SearchInput = S.SearchInput.Replace("'", "''");
        //    if (!string.IsNullOrEmpty(S.Surname))
        //        S.Surname = S.Surname.Replace("'", "''");
        //    if (!string.IsNullOrEmpty(S.MaidenName))
        //        S.MaidenName = S.MaidenName.Replace("'", "''");
        //    if (!string.IsNullOrEmpty(S.FirstName))
        //        S.FirstName = S.FirstName.Replace("'", "''");
        //    if (!string.IsNullOrEmpty(S.SecondName))
        //        S.SecondName = S.SecondName.Replace("'", "''");
        //    if (!string.IsNullOrEmpty(S.BusBusinessName))
        //        S.BusBusinessName = S.BusBusinessName.Replace("'", "''");
        //    if (!string.IsNullOrEmpty(S.ErrorDescription))
        //        S.ErrorDescription = S.ErrorDescription.Replace("'", "''");
        //    if (!string.IsNullOrEmpty(S.SubscriberName))
        //        S.SubscriberName = S.SubscriberName.Replace("'", "''");
        //    if (!string.IsNullOrEmpty(S.SystemUser))
        //        S.SystemUser = S.SystemUser.Replace("'", "''");
        //    if (!string.IsNullOrEmpty(S.SubscriberReference))
        //        S.SubscriberReference = S.SubscriberReference.Replace("'", "''");
        //    if (!string.IsNullOrEmpty(S.EnquiryReason))
        //        S.EnquiryReason = S.EnquiryReason.Replace("'", "''");

        //    string insertSQL = "EXEC sp_i_SubscriberEnquiry " + S.SubscriberID + " , '" + S.SubscriberName + "' , " + S.SystemUserID + " , '" + S.SystemUser + "' , '" + S.EnquiryReason + "' , '" + S.SubscriberEnquiryDate + "' , '" + S.SubscriberReference + "' , '" + S.EnquiryStatus + "' , '" + S.EnquiryResult + "' , " + S.ProductID + " , '" + S.SearchInput + "' , '" + S.IDNo + "' , '" + S.PassportNo + "' , '" + S.Surname + "' , '" + S.FirstInitial + "' , '" + S.SecondInitial + "' , '" + S.MaidenName + "' , '" + S.Gender + "' , '" + S.FirstName + "' , '" + S.SecondName + "' , '" + S.BirthDate + "' , '" + S.TelephoneCode + "' , '" + S.TelephoneNo + "' , '" + S.AccountNo + "' , '" + S.SubAccountNo + "' , '" + S.BusRegistrationNo + "' , '" + S.BusBusinessName + "' , '" + S.BusVatNumber + "' , '" + S.AgeSearchTypeInd + "' , " + S.Age + " , " + S.AgeBirthYear + " , " + S.AgeDeviation + " , '" + S.ErrorDescription + "' , '" + S.ExtraVarInput1 + "' , '" + S.ExtraVarInput2 + "' , '" + S.ExtraVarInput3 + "' , " + S.ExtraIntInput1 + " , " + S.ExtraIntInput2 + " , '" + S.BranchCode + "' , " + S.PurposeID + " , '" + S.CreatedByUser + "' , '" + S.CreatedOnDate + "' , '" + S.ChangedByUser + "' , '" + S.ChangedOnDate + "' ";
        //    SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

        //    int added = 0;
        //    added = sqlcmd.ExecuteNonQuery();

        //    string strSQL = "SELECT @@IDENTITY";
        //    SqlCommand Imycommand = new SqlCommand(strSQL, cn);
        //    SqlDataReader Idr = Imycommand.ExecuteReader();
        //    Idr.Read();
        //    int intSubscriberEnquiryID = Convert.ToInt32(Idr.GetValue(0));
        //    Idr.Close();

        //    sqlcmd.Dispose();
        //    Imycommand.Dispose();
        //    Idr.Dispose();
        //    cn.Close();

        //    return intSubscriberEnquiryID;
        //}

        public int InsertSubscriberEnquiry(SqlConnection cn, Entity.SubscriberEnquiry S)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            if (!string.IsNullOrEmpty(S.SearchInput))
                S.SearchInput = S.SearchInput.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.Surname))
                S.Surname = S.Surname.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.MaidenName))
                S.MaidenName = S.MaidenName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.FirstName))
                S.FirstName = S.FirstName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SecondName))
                S.SecondName = S.SecondName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.BusRegistrationNo))
                S.BusRegistrationNo = S.BusRegistrationNo.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.BusBusinessName))
                S.BusBusinessName = S.BusBusinessName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.ErrorDescription))
                S.ErrorDescription = S.ErrorDescription.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SubscriberName))
                S.SubscriberName = S.SubscriberName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SystemUser))
                S.SystemUser = S.SystemUser.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SubscriberReference))
                S.SubscriberReference = S.SubscriberReference.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.EnquiryReason))
                S.EnquiryReason = S.EnquiryReason.Replace("'", "''");

            string insertSQL = "EXEC sp_i_SubscriberEnquiry " + S.SubscriberID + " , '" + S.SubscriberName + "' , " + S.SystemUserID + " , '" + S.SystemUser + "' , '" + S.EnquiryReason + "' , '" + S.SubscriberEnquiryDate + "' , '" + S.SubscriberReference + "' , '" + S.EnquiryStatus + "' , '" + S.EnquiryResult + "' , " + S.ProductID + " , '" + S.SearchInput + "' , '" + S.IDNo + "' , '" + S.PassportNo + "' , '" + S.Surname + "' , '" + S.FirstInitial + "' , '" + S.SecondInitial + "' , '" + S.MaidenName + "' , '" + S.Gender + "' , '" + S.FirstName + "' , '" + S.SecondName + "' , '" + S.BirthDate + "' , '" + S.TelephoneCode + "' , '" + S.TelephoneNo + "' , '" + S.AccountNo + "' , '" + S.SubAccountNo + "' , '" + S.BusRegistrationNo + "' , '" + S.BusBusinessName + "' , '" + S.BusVatNumber + "' , '" + S.AgeSearchTypeInd + "' , " + S.Age + " , " + S.AgeBirthYear + " , " + S.AgeDeviation + " , '" + S.ErrorDescription + "' , '" + S.ExtraVarInput1 + "' , '" + S.ExtraVarInput2 + "' , '|"  + Entity.SystemName.name.ToString() + "|" + S.ExtraVarInput3  +"' , " + S.ExtraIntInput1 + " , " + S.ExtraIntInput2 + " , '" + S.BranchCode + "' , " + S.PurposeID + " , '" + S.CreatedByUser + "' , '" + S.CreatedOnDate + "' , '" + S.ChangedByUser + "' , '" + S.ChangedOnDate + "','" + S.NetMonthlyIncomeAmt + "'";



            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);
            sqlcmd.CommandTimeout = 0;

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();

            string strSQL = "SELECT @@IDENTITY";
            SqlCommand Imycommand = new SqlCommand(strSQL, cn);
            SqlDataReader Idr = Imycommand.ExecuteReader();
            Idr.Read();
            int intSubscriberEnquiryID = Convert.ToInt32(Idr.GetValue(0));
            Idr.Close();

            strSQL = "Exec spGetDataSegments " + S.SubscriberID + "," + S.ProductID + ",'" + S.SubscriberReference + "'";
            Imycommand = new SqlCommand(strSQL, cn);
            Imycommand.CommandTimeout = 0;
            SqlDataAdapter da = new SqlDataAdapter(Imycommand);
            DataSet ds = new DataSet("DataSegments");
            da.Fill(ds);
            ds.Tables[0].TableName = "Segments";
            string xml = string.Empty;
            xml = ds.GetXml();


            insertSQL = "Update SubscriberEnquiry set ExtraVarInput2 = '" + xml + "' where SubscriberEnquiryID = " + intSubscriberEnquiryID;
            sqlcmd = new SqlCommand(insertSQL, cn);

            added = sqlcmd.ExecuteNonQuery();

            sqlcmd.Dispose();
            Imycommand.Dispose();
            Idr.Dispose();
            cn.Close();
            ds.Dispose();

            return intSubscriberEnquiryID;
        }

        public int InsertSubscriberEnquiryBioMetric(SqlConnection cn, XDSPortalLibrary.Entity_Layer.ConsumerIDVerificationTrace oSearch)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();


            string insertSQL = "sp_I_SubscriberEnquiryIDBioMetric";
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);
            sqlcmd.CommandTimeout = 0;
            sqlcmd.CommandType = CommandType.StoredProcedure;


            sqlcmd.Parameters.AddWithValue("@SubscriberEnquiryID", oSearch.EnquiryID);
            sqlcmd.Parameters.AddWithValue("@SubscriberEnquiryResultID", oSearch.EnquiryResultID);
            sqlcmd.Parameters.AddWithValue("@VendorID", oSearch.VendorID);
            sqlcmd.Parameters.AddWithValue("@LastUsedTimeStamp", oSearch.LastUsedTimeStamp);
            sqlcmd.Parameters.AddWithValue("@Errors", oSearch.Errors);
            sqlcmd.Parameters.AddWithValue("@Finger1Index", oSearch.Finger1Index);
            sqlcmd.Parameters.AddWithValue("@Finger2Index", oSearch.Finger2Index);
            sqlcmd.Parameters.AddWithValue("@Finger1WsqImage", oSearch.Finger1WsqImage);
            sqlcmd.Parameters.AddWithValue("@Finger2WsqImage", oSearch.Finger2WsqImage);
            sqlcmd.Parameters.AddWithValue("@DeviceFirmwareVersion", oSearch.DeviceFirmwareVersion);
            sqlcmd.Parameters.AddWithValue("@DeviceModel", oSearch.DeviceModel);
            sqlcmd.Parameters.AddWithValue("@DeviceSerialNumber", oSearch.DeviceSerialNumber);
            sqlcmd.Parameters.AddWithValue("@WorkstationLoggedInUserName", oSearch.WorkstationLoggedInUserName);
            sqlcmd.Parameters.AddWithValue("@WorkStationName", oSearch.WorkStationName);
            sqlcmd.Parameters.AddWithValue("@CreatedbyUser", oSearch.CreatedbyUser);
            sqlcmd.Parameters.AddWithValue("@Createdondate", DateTime.Now);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();

            string strSQL = "SELECT @@IDENTITY";
            SqlCommand Imycommand = new SqlCommand(strSQL, cn);
            SqlDataReader Idr = Imycommand.ExecuteReader();
            Idr.Read();
            int intSubscriberEnquiryBiometricID = Convert.ToInt32(Idr.GetValue(0));
            Idr.Close();


            return intSubscriberEnquiryBiometricID;
        }

        public void InsertSubscriberCreditAssesmentEnquiry(SqlConnection cn, Entity.SubscriberEnquiry S, string SubscriberEnquiryID, string strMaritalStatus, string strSpouseFirstName, string strSpouseSurname, string strResAddress1, string strResAddress2, string strResAddress3, string strResAddress4, string strResAddressPC, string strPostAddress1, string strPostAddress2, string strPostAddress3, string strPostAddress4, string strPostAddressPC, string strHomeTelCode, string strHomeTelNo, string strWorkTelCode, string strWorkTelNo, string strCellCode, string strCellNo, string strEmail, decimal strTotalNetMonthlyIncome, string strEmployer, string strJobTitle)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            if (!string.IsNullOrEmpty(S.SearchInput))
                S.SearchInput = S.SearchInput.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.Surname))
                S.Surname = S.Surname.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.MaidenName))
                S.MaidenName = S.MaidenName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.FirstName))
                S.FirstName = S.FirstName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SecondName))
                S.SecondName = S.SecondName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.BusRegistrationNo))
                S.BusRegistrationNo = S.BusRegistrationNo.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.BusBusinessName))
                S.BusBusinessName = S.BusBusinessName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.ErrorDescription))
                S.ErrorDescription = S.ErrorDescription.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SubscriberName))
                S.SubscriberName = S.SubscriberName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SystemUser))
                S.SystemUser = S.SystemUser.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SubscriberReference))
                S.SubscriberReference = S.SubscriberReference.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.EnquiryReason))
                S.EnquiryReason = S.EnquiryReason.Replace("'", "''");

            string insertSQL = "EXEC sp_i_SubscriberCreditAssesmentEnquiry " + SubscriberEnquiryID + ",  " + S.SubscriberID + ", '" + S.SubscriberName + "' , " + S.SystemUserID + " , '" + S.SystemUser + "' , '" + S.EnquiryReason + "' , '" + S.SubscriberEnquiryDate + "' , '" + S.SubscriberReference + "' , " + S.ProductID + " , '" + S.SearchInput + "' , '" + S.IDNo + "' , '" + S.PassportNo + "' , '" + S.Surname + "' , '" + S.FirstInitial + "' , '" + S.SecondInitial + "' , '" + S.MaidenName + "' , '" + S.Gender + "' , '" + S.FirstName + "' , '" + S.SecondName + "' , '" + S.BirthDate + "' , '" + S.TelephoneCode + "' , '" + S.TelephoneNo + "' , '" + strMaritalStatus.Replace("'", "''") + "', '" + strSpouseFirstName.Replace("'", "''") + "', '" + strSpouseSurname.Replace("'", "''") + "', '" + strResAddress1.Replace("'", "''") + "', '" + strResAddress2.Replace("'", "''") + "', '" + strResAddress3.Replace("'", "''") + "', '" + strResAddress4.Replace("'", "''") + "', '" + strResAddressPC.Replace("'", "''") + "', '" + strPostAddress1.Replace("'", "''") + "', '" + strPostAddress2.Replace("'", "''") + "', '" + strPostAddress3.Replace("'", "''") + "', '" + strPostAddress4.Replace("'", "''") + "', '" + strPostAddressPC.Replace("'", "''") + "', '" + strHomeTelCode.Replace("'", "''") + "', '" + strHomeTelNo.Replace("'", "''") + "', '" + strWorkTelCode.Replace("'", "''") + "', '" + strWorkTelNo.Replace("'", "''") + "', '" + strCellCode.Replace("'", "''") + "', '" + strCellNo.Replace("'", "''") + "', '" + strEmail.Replace("'", "''") + "', " + strTotalNetMonthlyIncome + ", '" + strEmployer.Replace("'", "''") + "', '" + strJobTitle.Replace("'", "''") + "', '" + S.CreatedByUser + "' , '" + S.CreatedOnDate + "' , '" + S.ChangedByUser + "' , '" + S.ChangedOnDate + "'";
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);
            sqlcmd.CommandTimeout = 0;

            sqlcmd.ExecuteNonQuery();

            sqlcmd.Dispose();
            cn.Close();
        }

        public void InsertSubscriberConsumerComplianceEnquiry(SqlConnection cn, string SubscriberEnquiryID, string SystemUser,
            string strIDNo, string strPassportNo, string strFirstName, string strSurname, DateTime dtBirthDate, string strGender,
            string strAddressLine1, string strAddressLine2, string strCity, string strProvince, string strPostalCode, string strCountry,
            string strCitizenship, string strCellNumber)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            if (strAddressLine1 == null)
                strAddressLine1 = "";

            if (strAddressLine2 == null)
                strAddressLine2 = "";

            if (strCity == null)
                strCity = "";

            if (strProvince == null)
                strProvince = "";

            if (strCountry == null)
                strCountry = "";

            if (strCitizenship == null)
                strCitizenship = "";

            string insertSQL = "EXEC sp_i_SubscriberConsumerComplianceEnquiry " + SubscriberEnquiryID + ",  '" + strIDNo + "', '" +
                strPassportNo + "', '" + strFirstName.Replace("'", "''") + "', '" + strSurname.Replace("'", "''") + "', '" +
                dtBirthDate + "', '" + strGender + "', '" + strAddressLine1.Replace("'", "''") + "', '" +
                strAddressLine2.Replace("'", "''") + "', '" + strCity.Replace("'", "''") + "', '" +
                strProvince.Replace("'", "''") + "', '" + strPostalCode + "', '" + strCountry.Replace("'", "''") + "', '" +
                strCitizenship.Replace("'", "''") + "', '" + strCellNumber + "', '" + SystemUser + "', '" + DateTime.Now + "'";
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);
            sqlcmd.CommandTimeout = 0;

            sqlcmd.ExecuteNonQuery();

            sqlcmd.Dispose();
            cn.Close();
        }

        public int UpdateSubscriberEnquiry(string DBConnection, Entity.SubscriberEnquiry S)
        {
            if (!string.IsNullOrEmpty(S.SearchInput))
                S.SearchInput = S.SearchInput.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.Surname))
                S.Surname = S.Surname.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.MaidenName))
                S.MaidenName = S.MaidenName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.FirstName))
                S.FirstName = S.FirstName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SecondName))
                S.SecondName = S.SecondName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.BusRegistrationNo))
                S.BusRegistrationNo = S.BusRegistrationNo.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.BusBusinessName))
                S.BusBusinessName = S.BusBusinessName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.ErrorDescription))
                S.ErrorDescription = S.ErrorDescription.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SubscriberName))
                S.SubscriberName = S.SubscriberName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SystemUser))
                S.SystemUser = S.SystemUser.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SubscriberReference))
                S.SubscriberReference = S.SubscriberReference.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.EnquiryReason))
                S.EnquiryReason = S.EnquiryReason.Replace("'", "''");

            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = DBConnection;
            cn.Open();

            string insertSQL = "EXEC spUpdateSubscriberEnquiry " + S.SubscriberEnquiryID + " , " + S.SubscriberID + " , '" + S.SubscriberName + "' , " + S.SystemUserID + " , '" + S.SubscriberEnquiryDate + "' , '" + S.SubscriberReference + "' , '" + S.EnquiryStatus + "' , '" + S.EnquiryResult + "' , " + S.ProductID + " , '" + S.IDNo + "' , '" + S.PassportNo + "' , '" + S.Surname + "' , '" + S.FirstInitial + "' , '" + S.SecondInitial + "' , '" + S.MaidenName + "' , '" + S.Gender + "' , '" + S.FirstName + "' , '" + S.SecondName + "' , '" + S.BirthDate + "' , '" + S.TelephoneCode + "' , '" + S.TelephoneNo + "' , '" + S.AccountNo + "' , '" + S.SubAccountNo + "'";
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();
            cn.Close();

            sqlcmd.Dispose();
            return added;
        }


        public int UpdateSubscriberEnquiry(SqlConnection cn, Entity.SubscriberEnquiry S)
        {

            if (cn.State == ConnectionState.Closed)
                cn.Open();

            if (!string.IsNullOrEmpty(S.SearchInput))
                S.SearchInput = S.SearchInput.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.Surname))
                S.Surname = S.Surname.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.MaidenName))
                S.MaidenName = S.MaidenName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.FirstName))
                S.FirstName = S.FirstName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SecondName))
                S.SecondName = S.SecondName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.BusRegistrationNo))
                S.BusRegistrationNo = S.BusRegistrationNo.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.BusBusinessName))
                S.BusBusinessName = S.BusBusinessName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.ErrorDescription))
                S.ErrorDescription = S.ErrorDescription.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SubscriberName))
                S.SubscriberName = S.SubscriberName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SystemUser))
                S.SystemUser = S.SystemUser.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SubscriberReference))
                S.SubscriberReference = S.SubscriberReference.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.EnquiryReason))
                S.EnquiryReason = S.EnquiryReason.Replace("'", "''");

            string insertSQL = "EXEC sp_u_SubscriberEnquiry " + S.SubscriberEnquiryID + " , " + S.SubscriberID + " , '" + S.SubscriberName + "' , " + S.SystemUserID + " , '" + S.SystemUser + "' , '" + S.EnquiryReason + "' , '" + S.SubscriberEnquiryDate + "' , '" + S.SubscriberReference + "' , '" + S.EnquiryStatus + "' , '" + S.EnquiryResult + "' , " + S.ProductID + " , '" + S.SearchInput + "' , '" + S.IDNo + "' , '" + S.PassportNo + "' , '" + S.Surname + "' , '" + S.FirstInitial + "' , '" + S.SecondInitial + "' , '" + S.MaidenName + "' , '" + S.Gender + "' , '" + S.FirstName + "' , '" + S.SecondName + "' , '" + S.BirthDate + "' , '" + S.TelephoneCode + "' , '" + S.TelephoneNo + "' , '" + S.AccountNo + "' , '" + S.SubAccountNo + "' , '" + S.BusRegistrationNo + "' , '" + S.BusBusinessName + "' , '" + S.BusVatNumber + "' , '" + S.AgeSearchTypeInd + "' , " + S.Age + " , " + S.AgeBirthYear + " , " + S.AgeDeviation + " , '" + S.ErrorDescription + "' , '" + S.ExtraVarInput1 + "' , '" + S.ExtraVarInput2 + "' , '" + S.ExtraVarInput3 + "' , " + S.ExtraIntInput1 + " , " + S.ExtraIntInput2 + " , '" + S.BranchCode + "' , " + S.PurposeID + " , '" + S.CreatedByUser + "' , '" + S.CreatedOnDate + "' , '" + S.ChangedByUser + "' , '" + S.ChangedOnDate + "'";
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();

            sqlcmd.Dispose();

            cn.Close();
            return added;

        }


        public int UpdateSubscriberEnquiryError(string DBConnection, Entity.SubscriberEnquiry S)
        {

            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = DBConnection;
            cn.Open();

            string insertSQL = "UPDATE SubscriberEnquiry SET EnquiryStatusInd = 'F', EnquiryResultInd = 'E', ErrorDescription = '" + S.ErrorDescription + "' WHERE SubscriberEnquiryID = " + S.SubscriberEnquiryID.ToString();
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();
            cn.Close();

            sqlcmd.Dispose();
            return added;
        }


        public int UpdateSubscriberEnquiryError(SqlConnection cn, Entity.SubscriberEnquiry S)
        {

            if (cn.State == ConnectionState.Closed)
                cn.Open();

            string insertSQL = "UPDATE SubscriberEnquiry SET EnquiryStatusInd = 'F', EnquiryResultInd = 'E', ErrorDescription = '" + S.ErrorDescription.Replace("'", "''") + "' WHERE SubscriberEnquiryID = " + S.SubscriberEnquiryID.ToString();

            //File.AppendAllText(@"C:\Log\Rewardsco.txt", insertSQL);
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();

            sqlcmd.Dispose();
            cn.Close();

            return added;
        }

        public int UpdateSubscriberEnquiryErrorJD(SqlConnection cn, Entity.SubscriberEnquiry S)
        {

            if (cn.State == ConnectionState.Closed)
                cn.Open();

            string insertSQL = "UPDATE SubscriberEnquiry SET EnquiryStatusInd = 'F', EnquiryResultInd = 'E', ErrorDescription = '" + S.ErrorDescription.Replace("'", "''") + "', ExtraVarInput2 = '" + S.ExtraVarInput2.Replace("'", "''") + "' WHERE SubscriberEnquiryID = " + S.SubscriberEnquiryID.ToString();

            //File.AppendAllText(@"C:\Log\Rewardsco.txt", insertSQL);
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();

            sqlcmd.Dispose();
            cn.Close();

            return added;
        }

        public DataSet GetSubscriberEnquiryDataSet(string DBConnection, int intSubscriberEnquiryID)
        {

            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = DBConnection;
            cn.Open();

            string strSQL = "Select * from SubscriberEnquiry nolock Where SubscriberEnquiryID = " + intSubscriberEnquiryID.ToString();
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            sqlcmd.Dispose();
            sqlda.Dispose();
            cn.Close();
            return ds;
        }

        public DataSet GetSubscriberEnquiryDataSet(SqlConnection cn, int intSubscriberEnquiryID)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            string strSQL = "Select * from SubscriberEnquiry nolock Where SubscriberEnquiryID = " + intSubscriberEnquiryID.ToString();
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            sqlda.Dispose();
            sqlcmd.Dispose();
            cn.Close();

            return ds;
        }

        public Entity.SubscriberEnquiry GetSubscriberEnquiryObject(string DBConnection, int intSubscriberEnquiryID)
        {

            Entity.SubscriberEnquiry se = new Entity.SubscriberEnquiry();

            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = DBConnection;
            cn.Open();

            string strSQL = "Select * from SubscriberEnquiry nolock Where SubscriberEnquiryID = " + intSubscriberEnquiryID.ToString();
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                se.SubscriberEnquiryID = int.Parse(r["SubscriberEnquiryID"].ToString());
                se.EnquiryStatus = r["EnquiryStatusInd"].ToString();
                se.EnquiryResult = r["EnquiryResultInd"].ToString();
                se.ErrorDescription = r["ErrorDescription"].ToString();
                se.ProductID = int.Parse(r["ProductID"].ToString());
                se.SubscriberID = int.Parse(r["SubscriberID"].ToString());
                se.SystemUserID = int.Parse(r["SystemUserID"].ToString());
                se.SystemUser = r["SystemUser"].ToString();
                se.EnquiryReason = r["EnquiryReason"].ToString();
                se.CreatedByUser = r["CreatedByUser"].ToString();
                se.CreatedOnDate = DateTime.Parse(r["CreatedOnDate"].ToString());
                se.SearchInput = r["SearchInput"].ToString();
                se.ExtraIntInput1 = int.Parse(r["ExtraIntInput1"].ToString());
                se.ExtraIntInput2 = int.Parse(r["ExtraIntInput2"].ToString());
                se.ExtraVarInput1 = r["ExtraVarInput1"].ToString();
                se.ExtraVarInput2 = r["ExtraVarInput2"].ToString();
                se.ExtraVarInput3 = r["ExtraVarInput3"].ToString();
                se.BranchCode = r["BranchCode"].ToString();
                se.PurposeID = int.Parse(r["PurposeID"].ToString());
                se.SubscriberReference = r["SubscriberReference"].ToString();
                se.IDNo = r["IDNo"].ToString();
                se.Surname = r["Surname"].ToString();
                se.BusRegistrationNo = r["BusRegistrationNo"].ToString();
                se.BusBusinessName = r["BusBusinessName"].ToString();
                se.MaidenName = r["MaidenName"].ToString();
                se.AccountNo = r["AccountNo"].ToString();
                se.FirstName = r["FirstName"].ToString();
                se.NetMonthlyIncomeAmt = decimal.Parse(r["NetMonthlyIncomeAmt"].ToString());
            }
            sqlcmd.Dispose();
            sqlda.Dispose();
            ds.Dispose();
            cn.Close();
            return se;
        }

        public Entity.SubscriberEnquiry GetSubscriberEnquiryObject(SqlConnection cn, int intSubscriberEnquiryID)
        {

            Entity.SubscriberEnquiry se = new Entity.SubscriberEnquiry();

            if (cn.State == ConnectionState.Closed)
                cn.Open();

            string strSQL = "Select * from SubscriberEnquiry nolock Where SubscriberEnquiryID = " + intSubscriberEnquiryID.ToString();
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                se.SubscriberEnquiryID = int.Parse(r["SubscriberEnquiryID"].ToString());
                se.EnquiryStatus = r["EnquiryStatusInd"].ToString();
                se.EnquiryResult = r["EnquiryResultInd"].ToString();
                se.ErrorDescription = r["ErrorDescription"].ToString();
                se.ProductID = int.Parse(r["ProductID"].ToString());
                se.SubscriberID = int.Parse(r["SubscriberID"].ToString());
                se.SystemUserID = int.Parse(r["SystemUserID"].ToString());
                se.SystemUser = r["SystemUser"].ToString();
                se.EnquiryReason = r["EnquiryReason"].ToString();
                se.CreatedByUser = r["CreatedByUser"].ToString();
                se.CreatedOnDate = DateTime.Parse(r["CreatedOnDate"].ToString());
                se.SearchInput = r["SearchInput"].ToString();
                se.ExtraIntInput1 = int.Parse(r["ExtraIntInput1"].ToString());
                se.ExtraIntInput2 = int.Parse(r["ExtraIntInput2"].ToString());
                se.ExtraVarInput1 = r["ExtraVarInput1"].ToString();
                se.ExtraVarInput2 = r["ExtraVarInput2"].ToString();
                se.ExtraVarInput3 = r["ExtraVarInput3"].ToString();
                se.BranchCode = r["BranchCode"].ToString();
                se.PurposeID = int.Parse(r["PurposeID"].ToString());
                se.SubscriberReference = r["SubscriberReference"].ToString();
                se.IDNo = r["IDNo"].ToString();
                se.PassportNo = r["PassportNo"].ToString();
                se.BusRegistrationNo = r["BusRegistrationNo"].ToString(); se.Surname = r["Surname"].ToString();
                se.BusBusinessName = r["BusBusinessName"].ToString();
                se.MaidenName = r["MaidenName"].ToString();
                se.AccountNo = r["AccountNo"].ToString();
                se.FirstName = r["FirstName"].ToString();
                se.SecondName = r["SecondName"].ToString();
                se.NetMonthlyIncomeAmt = decimal.Parse(r["NetMonthlyIncomeAmt"].ToString());
                se.SubscriberEnquiryDate = DateTime.Parse(r["SubscriberEnquiryDate"].ToString());
                se.SubscriberName = r["Subscribername"].ToString();
                se.AccountNo = r["Accountno"].ToString();
                se.BusRegistrationNo = r["BusRegistrationNo"].ToString();
                se.BusBusinessName = r["BusBusinessName"].ToString();
                se.BusVatNumber = r["BusVatNumber"].ToString();
                se.TelephoneNo = r["TelephoneNo"].ToString();
                se.TelephoneCode = r["TelephoneCode"].ToString();
                DateTime dt = new DateTime();
                DateTime.TryParse(r["BirthDate"].ToString(), out  dt);
                se.BirthDate = dt;

            }
            sqlda.Dispose();
            sqlcmd.Dispose();
            ds.Dispose();

            cn.Close();
            return se;
        }

        public Entity.SubscriberEnquiry ErrorGetSubscriberEnquiryObject(SqlConnection cn, int intSubscriberEnquiryID)
        {

            Entity.SubscriberEnquiry se = new Entity.SubscriberEnquiry();

            if (cn.State == ConnectionState.Closed)
                cn.Open();

            string strSQL = "EXEC [sp_u_ErrorSubscriberEnquiry] " + intSubscriberEnquiryID.ToString();
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                se.SubscriberEnquiryID = int.Parse(r["SubscriberEnquiryID"].ToString());
                se.EnquiryStatus = r["EnquiryStatusInd"].ToString();
                se.EnquiryResult = r["EnquiryResultInd"].ToString();
                se.ErrorDescription = r["ErrorDescription"].ToString();
                se.ProductID = int.Parse(r["ProductID"].ToString());
                se.SubscriberID = int.Parse(r["SubscriberID"].ToString());
                se.SystemUserID = int.Parse(r["SystemUserID"].ToString());
                se.SystemUser = r["SystemUser"].ToString();
                se.EnquiryReason = r["EnquiryReason"].ToString();
                se.CreatedByUser = r["CreatedByUser"].ToString();
                se.CreatedOnDate = DateTime.Parse(r["CreatedOnDate"].ToString());
                se.SearchInput = r["SearchInput"].ToString();
                se.ExtraIntInput1 = int.Parse(r["ExtraIntInput1"].ToString());
                se.ExtraIntInput2 = int.Parse(r["ExtraIntInput2"].ToString());
                se.ExtraVarInput1 = r["ExtraVarInput1"].ToString();
                se.ExtraVarInput2 = r["ExtraVarInput2"].ToString();
                se.ExtraVarInput3 = r["ExtraVarInput3"].ToString();
                se.BranchCode = r["BranchCode"].ToString();
                se.PurposeID = int.Parse(r["PurposeID"].ToString());
                se.SubscriberReference = r["SubscriberReference"].ToString();
                se.IDNo = r["IDNo"].ToString();
                se.Surname = r["Surname"].ToString();
                se.BusRegistrationNo = r["BusRegistrationNo"].ToString(); se.BusRegistrationNo = r["BusRegistrationNo"].ToString();
                se.BusBusinessName = r["BusBusinessName"].ToString();
                se.MaidenName = r["MaidenName"].ToString();
                se.AccountNo = r["AccountNo"].ToString();
                se.FirstName = r["FirstName"].ToString();
                se.NetMonthlyIncomeAmt = decimal.Parse(r["NetMonthlyIncomeAmt"].ToString());
            }
            sqlda.Dispose();
            sqlcmd.Dispose();
            ds.Dispose();

            cn.Close();
            return se;
        }

        public DataSet GetSubscriberEnquirySearchHistory(SqlConnection cn, int SubscriberID, int ProductID, int SystemUserID)
        {
            DataSet ds = new DataSet("SearchHistory");

            using (cn)
            {
                StringBuilder oStringBuilder = new StringBuilder();

                oStringBuilder.Append(string.Format("SELECT TOP 50 * FROM SubscriberEnquirySearchResults WHERE SubscriberID = {0} ", SubscriberID));

                if (SystemUserID > 0)
                    oStringBuilder.Append(string.Format("AND SystemUserID = {0} ", SystemUserID));

                if (ProductID > 0)
                    oStringBuilder.Append(string.Format("AND ProductID = {0} ", ProductID));

                oStringBuilder.Append("ORDER BY DetailsViewedDate DESC");

                if (cn.State == ConnectionState.Closed)
                    cn.Open();

                SqlCommand sqlcmd = new SqlCommand(oStringBuilder.ToString(), cn);
                SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);

                sqlda.Fill(ds,"HistoryItem");

                sqlda.Dispose();
                sqlcmd.Dispose();
                cn.Close();
            }

            return ds;
        }

        public DataSet GetSubscriberEnquiryReport(SqlConnection cn, int SubscriberEnquiryResultID)
        {
            DataSet ds = new DataSet("SearchHistory");

            using (cn)
            {
                StringBuilder oStringBuilder = new StringBuilder();

                oStringBuilder.Append(string.Format("SELECT XMLData,isnull(ExtraVarOutput1,'') as ExtraVarOutput1,Isnull(ProductID,0) ProductID FROM SubscriberEnquiryResult nolock WHERE SubscriberEnquiryResultId = {0}", SubscriberEnquiryResultID));

                if (cn.State == ConnectionState.Closed)
                    cn.Open();

                SqlCommand sqlcmd = new SqlCommand(oStringBuilder.ToString(), cn);
                SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);

                sqlda.Fill(ds, "ReportItem");

                sqlda.Dispose();
                sqlcmd.Dispose();
                cn.Close();
            }

            return ds;
        }

        public DataSet GetSubscriberEnquiryKYCReport(SqlConnection cn, int ProductID, string SystemUser, string StartDate, string EndDate)
        {
            DataSet ds = new DataSet("SearchHistory");

            using (cn)
            {
                StringBuilder oStringBuilder = new StringBuilder();

                oStringBuilder.Append(string.Format("SELECT XMLData FROM SubscriberEnquiryResult SER INNER JOIN SubscriberEnquiry SE ON SER.SubscriberEnquiryID = SE.SubscriberEnquiryID WHERE SE.ProductID = {0} AND SE.CreatedByUser = '{1}' AND SE.SubscriberEnquiryDate >= '{2}' AND SE.SubscriberEnquiryDate <= '{3}' AND EnquiryStatusInd = 'C' AND XMLData NOT LIKE ''", ProductID, SystemUser, StartDate, EndDate));

                if (cn.State == ConnectionState.Closed)
                    cn.Open();

                SqlCommand sqlcmd = new SqlCommand(oStringBuilder.ToString(), cn);
                SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);

                sqlda.Fill(ds, "ReportItem");

                sqlda.Dispose();
                sqlcmd.Dispose();
                cn.Close();
            }

            return ds;
        }

        public Entity.SubscriberEnquiry GetSubscriberEnquiryObjectByBranchCode(SqlConnection cn, string BranchCode)
        {

            Entity.SubscriberEnquiry se = new Entity.SubscriberEnquiry();

            if (cn.State == ConnectionState.Closed)
                cn.Open();

            string strSQL = "Select * from SubscriberEnquiry nolock Where BranchCode = '" + BranchCode + "'";
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                se.SubscriberEnquiryID = int.Parse(r["SubscriberEnquiryID"].ToString());
                se.EnquiryStatus = r["EnquiryStatusInd"].ToString();
                se.EnquiryResult = r["EnquiryResultInd"].ToString();
                se.ErrorDescription = r["ErrorDescription"].ToString();
                se.ProductID = int.Parse(r["ProductID"].ToString());
                se.SubscriberID = int.Parse(r["SubscriberID"].ToString());
                se.SystemUserID = int.Parse(r["SystemUserID"].ToString());
                se.SystemUser = r["SystemUser"].ToString();
                se.EnquiryReason = r["EnquiryReason"].ToString();
                se.CreatedByUser = r["CreatedByUser"].ToString();
                se.CreatedOnDate = DateTime.Parse(r["CreatedOnDate"].ToString());
                se.SearchInput = r["SearchInput"].ToString();
                se.ExtraIntInput1 = int.Parse(r["ExtraIntInput1"].ToString());
                se.ExtraIntInput2 = int.Parse(r["ExtraIntInput2"].ToString());
                se.ExtraVarInput1 = r["ExtraVarInput1"].ToString();
                se.ExtraVarInput2 = r["ExtraVarInput2"].ToString();
                se.ExtraVarInput3 = r["ExtraVarInput3"].ToString();
                se.BranchCode = r["BranchCode"].ToString();
                se.PurposeID = int.Parse(r["PurposeID"].ToString());
                se.SubscriberReference = r["SubscriberReference"].ToString();
                se.IDNo = r["IDNo"].ToString();
                se.PassportNo = r["PassportNo"].ToString();
                se.BusRegistrationNo = r["BusRegistrationNo"].ToString(); se.Surname = r["Surname"].ToString();
                se.BusBusinessName = r["BusBusinessName"].ToString();
                se.MaidenName = r["MaidenName"].ToString();
                se.AccountNo = r["AccountNo"].ToString();
                se.FirstName = r["FirstName"].ToString();
                se.SecondName = r["SecondName"].ToString();
                se.NetMonthlyIncomeAmt = decimal.Parse(r["NetMonthlyIncomeAmt"].ToString());
                se.SubscriberEnquiryDate = DateTime.Parse(r["SubscriberEnquiryDate"].ToString());
                se.SubscriberName = r["Subscribername"].ToString();
                se.AccountNo = r["Accountno"].ToString();
                se.BusRegistrationNo = r["BusRegistrationNo"].ToString();
                se.BusBusinessName = r["BusBusinessName"].ToString();
                se.BusVatNumber = r["BusVatNumber"].ToString();
                se.TelephoneNo = r["TelephoneNo"].ToString();
                se.TelephoneCode = r["TelephoneCode"].ToString();
                DateTime dt = new DateTime();
                DateTime.TryParse(r["BirthDate"].ToString(), out dt);
                se.BirthDate = dt;

            }
            sqlda.Dispose();
            sqlcmd.Dispose();
            ds.Dispose();

            cn.Close();
            return se;
        }

        public int UpdateSubscriberEnquiryBranchCode(SqlConnection cn, int SubscriberEnquiryID, string BranchCode)
        {

            if (cn.State == ConnectionState.Closed)
                cn.Open();

            string insertSQL = "UPDATE SubscriberEnquiry SET BranchCode = '" + BranchCode + "' WHERE SubscriberEnquiryID = " + SubscriberEnquiryID.ToString();
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();

            sqlcmd.Dispose();

            cn.Close();
            return added;

        }

        public DataSet GetDOVSubscriberEnquiryLogDataSet(SqlConnection cn, int intSystemUserID, string strproductID, string strEnquiryStatusInd, int intSubscriberID)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            string strSQL = "Select top 100 se.SubscriberEnquiryID, ser.SubscriberEnquiryResultID, SubscriberEnquiryDate, SearchInput, se.IDNo, ser.FirstName, ser.Surname, TelephoneNo, ErrorDescription, case EnquiryStatusInd when 'C' then 'Completed' when 'F' then 'Failed' when 'P' then 'Pending' end as 'EnquiryStatusDesc' from SubscriberEnquiry (nolock) se inner join SubscriberEnquiryResult (nolock) ser on se.SubscriberEnquiryID = ser.SubscriberEnquiryID Where se.SubscriberID = " + intSubscriberID.ToString() + " and se.productID = " + strproductID;

            if (intSystemUserID > 0)
            {
                strSQL = strSQL + " and se.SystemUserID = " + intSystemUserID.ToString();
            }
            if (!(strEnquiryStatusInd == string.Empty))
            {
                strSQL = strSQL + " and se.EnquiryStatusInd = '" + strEnquiryStatusInd + "'";
            }

            strSQL = strSQL + " order by se.SubscriberEnquiryID desc";

            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            cn.Close();

            return ds;
        }

        public DataSet GetDOVSubscriberEnquiryLogDataSet(SqlConnection cn, int intSystemUserID, string strproductID, string strEnquiryStatusInd)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            string strSQL = "Select se.SubscriberEnquiryID, ser.SubscriberEnquiryResultID, SubscriberEnquiryDate, SearchInput, se.IDNo, ser.FirstName, ser.Surname, TelephoneNo, ErrorDescription, case EnquiryStatusInd when 'C' then 'Completed' when 'F' then 'Failed' when 'P' then 'Pending' end as 'EnquiryStatusDesc' from SubscriberEnquiry (nolock) se inner join SubscriberEnquiryResult (nolock) ser on se.SubscriberEnquiryID = ser.SubscriberEnquiryID Where se.SystemUserID = " + intSystemUserID.ToString() + " and se.productID = " + strproductID + " and se.EnquiryStatusInd = '" + strEnquiryStatusInd + "' order by se.SubscriberEnquiryID desc";
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            cn.Close();

            return ds;
        }

        public void InsertSubscriberAddressAssesmentEnquiry(SqlConnection cn, Entity.SubscriberEnquiry S, string SubscriberEnquiryID, string strResAddress1, string strResAddress2, string strResAddress3, string strResAddress4, string strResAddressPC)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            if (!string.IsNullOrEmpty(S.SearchInput))
                S.SearchInput = S.SearchInput.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.Surname))
                S.Surname = S.Surname.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.FirstName))
                S.FirstName = S.FirstName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.ErrorDescription))
                S.ErrorDescription = S.ErrorDescription.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SubscriberName))
                S.SubscriberName = S.SubscriberName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SystemUser))
                S.SystemUser = S.SystemUser.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SubscriberReference))
                S.SubscriberReference = S.SubscriberReference.Replace("'", "''");

            if (string.IsNullOrEmpty(strResAddress3))
            {
                strResAddress3 = "";
            }

            if (string.IsNullOrEmpty(strResAddress4))
            {
                strResAddress4 = "";
            }

            string insertSQL = "EXEC sp_i_SubscriberAddressAssesmentEnquiry " + SubscriberEnquiryID + ",  " + S.SubscriberID + ", '" + S.SubscriberName + "' , " + S.SystemUserID + " , '" + S.SystemUser + "' , '" + S.SubscriberEnquiryDate + "' , '" + S.SubscriberReference + "' , " + S.ProductID + " , '" + S.SearchInput + "' , '" + S.IDNo + "' , '" + S.PassportNo + "' , '" + S.Surname + "' , '" + S.Gender + "' , '" + S.FirstName + "' , '" + S.BirthDate + "' , '" + strResAddress1.Replace("'", "''") + "', '" + strResAddress2.Replace("'", "''") + "', '" + strResAddress3.Replace("'", "''") + "', '" + strResAddress4.Replace("'", "''") + "', '" + strResAddressPC.Replace("'", "''") + "', '" + S.CreatedByUser + "' , '" + S.CreatedOnDate + "' , '" + S.ChangedByUser + "' , '" + S.ChangedOnDate + "'";
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);
            sqlcmd.CommandTimeout = 0;

            sqlcmd.ExecuteNonQuery();

            sqlcmd.Dispose();
            cn.Close();
        }

        public void InsertSubscriberEnquiryMDP(SqlConnection cn, int SubscriberEnquiryID, string ProductTransactionID, string ErrorMessage, string CreatedByUser, DateTime CreatedOnDate)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            if (!string.IsNullOrEmpty(ErrorMessage))
                ErrorMessage = ErrorMessage.Replace("'", "''");

            string insertSQL = "EXEC sp_i_SubscriberEnquiryMDP " + SubscriberEnquiryID + ", '" + ProductTransactionID + "', '" + ErrorMessage + "', '" + CreatedByUser + "', '" + CreatedOnDate + "'";
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);
            sqlcmd.CommandTimeout = 0;

            sqlcmd.ExecuteNonQuery();

            sqlcmd.Dispose();
            cn.Close();
        }

        public Entity.SimSwap GetSimSwapObject(SqlConnection cn, string IDNo, string TelephoneNo)
        {

            Entity.SimSwap ss = new Entity.SimSwap();

            if (cn.State == ConnectionState.Closed)
                cn.Open();

            string strSQL = "EXEC spGetRecentSimSwapEnquiry '" + IDNo + "' , '" + TelephoneNo + "'";
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                ss.CellphoneNumber = r["CellphoneNumber"].ToString();
                ss.SimSwapResult = r["SimSwapResult"].ToString();
                ss.RiskFlag = r["RiskFlag"].ToString();
            }

            sqlda.Dispose();
            sqlcmd.Dispose();
            ds.Dispose();
            cn.Close();

            return ss;
        }

        public void InsertSubscriberThinFileEnquiry(SqlConnection cn, Entity.SubscriberEnquiry S, string SubscriberEnquiryID, string strAge, string strGender, DateTime dtBirthDate, string strResidentialAddressLine1, string strResidentialAddressLine2, string strResidentialAddressLine3, string strResidentialAddressLine4, string strResidentialPostalCode, string strPostalAddressLine1, string strPostalAddressLine2, string strPostalAddressLine3, string strPostalAddressLine4, string strPostalPostalCode, string strCellNo1, string strCellNo2, string strCellNo3, string strEmailAddress)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            if (!string.IsNullOrEmpty(S.SearchInput))
                S.SearchInput = S.SearchInput.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.Surname))
                S.Surname = S.Surname.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.MaidenName))
                S.MaidenName = S.MaidenName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.FirstName))
                S.FirstName = S.FirstName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SecondName))
                S.SecondName = S.SecondName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.BusRegistrationNo))
                S.BusRegistrationNo = S.BusRegistrationNo.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.BusBusinessName))
                S.BusBusinessName = S.BusBusinessName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.ErrorDescription))
                S.ErrorDescription = S.ErrorDescription.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SubscriberName))
                S.SubscriberName = S.SubscriberName.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SystemUser))
                S.SystemUser = S.SystemUser.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.SubscriberReference))
                S.SubscriberReference = S.SubscriberReference.Replace("'", "''");
            if (!string.IsNullOrEmpty(S.EnquiryReason))
                S.EnquiryReason = S.EnquiryReason.Replace("'", "''");

            string insertSQL = "EXEC sp_i_SubscriberThinFileEnquiry " + SubscriberEnquiryID + ",  " + S.SubscriberID + ", '" + S.SubscriberName + "' , " + S.SystemUserID + " , '" + S.SystemUser + "' , '" + S.EnquiryReason + "' , '" + S.SubscriberEnquiryDate + "' , '" + S.SubscriberReference + "' , " + S.ProductID + " , '" + S.SearchInput + "' , '" + S.IDNo + "' , '" + S.PassportNo + "' , '" + S.FirstName + "' , '" + S.Surname + "', " + strAge + " , '" + strGender + "' , '" + S.BirthDate + "' , '" + strResidentialAddressLine1.Replace("'", "''") + "', '" + strResidentialAddressLine2.Replace("'", "''") + "', '" + strResidentialAddressLine3.Replace("'", "''") + "', '" + strResidentialAddressLine4.Replace("'", "''") + "', '" + strResidentialPostalCode.Replace("'", "''") + "', '" + strPostalAddressLine1.Replace("'", "''") + "', '" + strPostalAddressLine2.Replace("'", "''") + "', '" + strPostalAddressLine3.Replace("'", "''") + "', '" + strPostalAddressLine4.Replace("'", "''") + "', '" + strPostalPostalCode.Replace("'", "''") + "', '" + strCellNo1.Replace("'", "''") + "', '" + strCellNo2.Replace("'", "''") + "', '" + strCellNo3.Replace("'", "''") + "', '" + strEmailAddress.Replace("'", "''") + "', '" + S.CreatedByUser + "' , '" + S.CreatedOnDate + "' , '" + S.ChangedByUser + "' , '" + S.ChangedOnDate + "'";
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);
            sqlcmd.CommandTimeout = 0;

            sqlcmd.ExecuteNonQuery();

            sqlcmd.Dispose();
            cn.Close();
        }

        public Entity.SubscriberThinFile GetSubscriberThinFileObject(SqlConnection cn, int intSubscriberEnquiryID)
        {

            Entity.SubscriberThinFile se = new Entity.SubscriberThinFile();

            if (cn.State == ConnectionState.Closed)
                cn.Open();

            string strSQL = "Select * from SubscriberThinFileEnquiry nolock Where SubscriberEnquiryID = " + intSubscriberEnquiryID.ToString();
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                se.IdNumber = r["IDNo"].ToString();
                se.PassportNo = r["PassportNo"].ToString();
                se.FirstName = r["FirstName"].ToString();
                se.Surname = r["Surname"].ToString();
                se.Age = int.Parse(r["Age"].ToString());
                se.Gender = r["Gender"].ToString();
                se.BirthDate = DateTime.Parse(r["BirthDate"].ToString());
                se.ResidentialAddressLine1 = r["ResidentialAddressLine1"].ToString();
                se.ResidentialAddressLine2 = r["ResidentialAddressLine2"].ToString();
                se.ResidentialAddressLine3 = r["ResidentialAddressLine3"].ToString();
                se.ResidentialAddressLine4 = r["ResidentialAddressLine4"].ToString();
                se.ResidentialPostalCode = r["ResidentialPostalCode"].ToString();
                se.PostalAddressLine1 = r["PostalAddressLine1"].ToString();
                se.PostalAddressLine2 = r["PostalAddressLine2"].ToString();
                se.PostalAddressLine3 = r["PostalAddressLine3"].ToString();
                se.PostalAddressLine4 = r["PostalAddressLine4"].ToString();
                se.PostalPostalCode = r["PostalPostalCode"].ToString();
                se.CellNo1 = r["CellNo1"].ToString();
                se.CellNo2 = r["CellNo2"].ToString();
                se.CellNo3 = r["CellNo3"].ToString();
                se.EmailAddress = r["EmailAddress"].ToString();
            }
            sqlda.Dispose();
            sqlcmd.Dispose();
            ds.Dispose();

            cn.Close();
            return se;
        }

        public int GetDHARunMode(SqlConnection cn)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            string strSQL = "Select top 1 RunMode from DHACurrentRunMode nolock order by CreatedOnDate desc";
            SqlCommand Imycommand = new SqlCommand(strSQL, cn);
            SqlDataReader Idr = Imycommand.ExecuteReader();
            Idr.Read();
            int intRunMode = Convert.ToInt32(Idr.GetValue(0));
            Idr.Close();

            return intRunMode;
        }

        public void InsertDHAFailedLog(SqlConnection cn, int RIDVSubscriberEnquiryID, int NormalSubscriberEnquiryID, string CreatedByUser, DateTime CreatedOnDate)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            if (!string.IsNullOrEmpty(CreatedByUser))
                CreatedByUser = CreatedByUser.Replace("'", "''");

            string insertSQL = "EXEC sp_i_DHAFailedLog " + RIDVSubscriberEnquiryID + ",  " + NormalSubscriberEnquiryID + ", '" + CreatedByUser + "' , '" + CreatedOnDate + "'";
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);
            sqlcmd.CommandTimeout = 0;

            sqlcmd.ExecuteNonQuery();

            sqlcmd.Dispose();
            cn.Close();
        }

        public Entity.SubscriberSanction GetSubscriberSanctionObject(SqlConnection cn, int intSubscriberEnquiryID)
        {

            Entity.SubscriberSanction se = new Entity.SubscriberSanction();

            if (cn.State == ConnectionState.Closed)
                cn.Open();

            string strSQL = "Select * from SubscriberConsumerComplianceEnquiry nolock Where SubscriberEnquiryID = " + intSubscriberEnquiryID.ToString();
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                se.IdNumber = r["IDNo"].ToString();
                se.PassportNo = r["PassportNo"].ToString();
                se.FirstName = r["FirstName"].ToString();
                se.Surname = r["Surname"].ToString();
                se.Nationality = r["Province"].ToString();
                se.Citizenship = r["Citizenship"].ToString();
                se.Country = r["Country"].ToString();
            }
            sqlda.Dispose();
            sqlcmd.Dispose();
            ds.Dispose();

            cn.Close();
            return se;
        }
    }

}
