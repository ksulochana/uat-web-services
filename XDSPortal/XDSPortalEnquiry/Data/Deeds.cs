﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace XDSPortalEnquiry.Data
{
   public  class Deeds
    {
       public Deeds()
       {
       }
       public DataSet GetTownshipNames(SqlConnection Enquirycon,string RegistrarName,string TownshipName)
       {
           if (Enquirycon.State == ConnectionState.Closed)
               Enquirycon.Open();

           string strSQL = "SELECT top 1  ServerConnectionString nolock FROM dbo.EnquiryServers where Active = 1";
           SqlCommand cmdconstr = new SqlCommand(strSQL, Enquirycon);
           SqlDataReader dr = cmdconstr.ExecuteReader();
           dr.Read();
           string strconstring = dr.GetValue(0).ToString();
           dr.Close();

           Enquirycon.Close();

           SqlConnection cn = new SqlConnection(strconstring);

           if (cn.State == ConnectionState.Closed)
               cn.Open();
                      
           SqlCommand sqlcmd = new SqlCommand("spGetTownships", cn);
           sqlcmd.CommandType = CommandType.StoredProcedure;
           sqlcmd.Parameters.AddWithValue("@DeedsOffice", RegistrarName);
           sqlcmd.Parameters.AddWithValue("@TownshipName", TownshipName);
           SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
           DataSet ds = new DataSet("Result");
                      
           sqlda.Fill(ds);

           foreach (DataTable dt in ds.Tables)
           {
               if (!(ds.Tables.Contains("Deeds")))
               dt.TableName = "Deeds";
           }

           sqlda.Dispose();
           sqlcmd.Dispose();
           cn.Close();
           return ds;
       }
    }
}
