﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.IO;

namespace XDSPortalEnquiry.Data
{
    public class SystemUser
    {
        public SystemUser()
        {

        }
        public Entity.SystemUser GetSystemUserRecord(SqlConnection conString, int SystemUserID)
        {
            if (conString.State == ConnectionState.Closed)
                conString.Open();

            Entity.SystemUser objSystemUser = new Entity.SystemUser();

            //SqlConnection ObjConstring = new SqlConnection(conString);
            //ObjConstring.Open();

            string sqlSelect = "SELECT [SystemUserID],isnull([SystemUserTypeInd],'') AS SystemUserTypeInd ,isnull([FirstName],'') AS FirstName,isnull([Surname],'') AS Surname,isnull([IDNo],'') AS IDNo,isnull([JobPosition],'') AS JobPosition,isnull([CellularCode], '') AS CellularCode,isnull([CellularNo], '')  AS CellularNo,isnull([EmailAddress], '') AS EmailAddress,isnull([ActiveYN],0) AS ActiveYN,isnull([Username],'') AS Username,isnull([SubscriberID],0) AS SubscriberID,isnull([CreatedByUser], '') AS CreatedByUser,isnull([CreatedOnDate], '') AS CreatedOnDate,isnull([ChangedByUser], '') AS ChangedByUser,isnull([ChangedOnDate], '') AS ChangedOnDate,isnull([SystemUserRole], '') AS SystemUserRole from SystemUser (NOLOCK) where SystemUserID = " + SystemUserID;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, conString);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objSystemUser.SubscriberID = Convert.ToInt32(Dr["SubscriberID"]);
                objSystemUser.ActiveYN = bool.Parse(Dr["ActiveYN"].ToString());
                objSystemUser.CellularCode = Dr["CellularCode"].ToString();
                objSystemUser.CellularNo = Dr["CellularNo"].ToString();
                objSystemUser.ChangedByUser = Dr["ChangedByUser"].ToString();
                objSystemUser.ChangedOnDate = DateTime.Parse(Dr["ChangedOnDate"].ToString());
                objSystemUser.CreatedByUser = Dr["CreatedByUser"].ToString();
                objSystemUser.CreatedOnDate = DateTime.Parse(Dr["CreatedOnDate"].ToString());
                objSystemUser.EmailAddress = Dr["EmailAddress"].ToString();
                objSystemUser.FirstName = Dr["FirstName"].ToString();
                objSystemUser.IDNo = Dr["IDNo"].ToString();
                objSystemUser.JobPosition = Dr["JobPosition"].ToString();
                objSystemUser.Surname = Dr["Surname"].ToString();

                objSystemUser.SystemUserID = int.Parse(Dr["SystemUserID"].ToString());
                objSystemUser.SystemUserRole = Dr["SystemUserRole"].ToString();
                objSystemUser.SystemUserTypeInd = Dr["SystemUserTypeInd"].ToString();
                objSystemUser.Username = Dr["Username"].ToString();
               
            }

            objsqladp.Dispose();
            Objsqlcom.Dispose();
            ds.Dispose();
            conString.Close();

           return objSystemUser;
        }

        public void GetPasswordResetStatus(SqlConnection conString, int SystemUserID, out int PasswordResetStatus, out int ResetGraceDays, out int Noofdaystoreset)
        {
            /*
                1 Reset Password
                2 deactivate user Access
                3 grant access 
            */

            // int intPasswordResetStatus = 0;


            SqlCommand sqlCommand = new SqlCommand("spGetPasswordResetStatus", conString);
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandTimeout = 0;
            sqlCommand.Parameters.AddWithValue("@SystemUserID", SystemUserID);

            SqlParameter oResult = new SqlParameter("@Result", SqlDbType.Int);
            oResult.Direction = ParameterDirection.Output;
            sqlCommand.Parameters.Add(oResult);


            SqlParameter oResetGraceDays = new SqlParameter("@ResetGraceDays", SqlDbType.Int);
            oResetGraceDays.Direction = ParameterDirection.Output;
            sqlCommand.Parameters.Add(oResetGraceDays);

            SqlParameter onoofdayssincepasswordreset = new SqlParameter("@noofdaystoreset", SqlDbType.Int);
            onoofdayssincepasswordreset.Direction = ParameterDirection.Output;
            sqlCommand.Parameters.Add(onoofdayssincepasswordreset);


            SqlDataAdapter odataAdapter = new SqlDataAdapter(sqlCommand);

            if (conString.State == ConnectionState.Closed)
                conString.Open();

            sqlCommand.ExecuteNonQuery();

            PasswordResetStatus = int.Parse(oResult.Value.ToString());
            ResetGraceDays = int.Parse(oResetGraceDays.Value.ToString());
            Noofdaystoreset = int.Parse(onoofdayssincepasswordreset.Value.ToString());

            //string sqlSelect = "SELECT dbo.fnGetPasswordResetStatus_V1(" + SystemUserID + ")";

            //if (conString.State == ConnectionState.Closed)
            //    conString.Open(); 

            //SqlCommand Objsqlcom = new SqlCommand(sqlSelect, conString);
            //intPasswordResetStatus = (int)Objsqlcom.ExecuteScalar();

            //if (conString.State == ConnectionState.Open)
            //    conString.Close();

            //return intPasswordResetStatus;
        }

        public void UpdateLastResetDate(SqlConnection conString,string Username, int SystemUserID)
        { 
            if (conString.State == ConnectionState.Closed)
                conString.Open();

            string sqlSelect = "Update SystemUserResetPeriod Set LastResetDate = GETDATE(),ChangedByUser = '" + Username + "',ChangedOnDate = GETDATE() Where SystemUserID = " + SystemUserID;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, conString);
            Objsqlcom.ExecuteNonQuery(); 
        }

        public void UpdateChangepasswordStatus(SqlConnection conString, string Username, int SystemUserID)
        {
            if (conString.State == ConnectionState.Closed)
                conString.Open();

            string sqlSelect = "Update SystemUserResetPeriod Set LastResetDate=Getdate(), RecordStatusInd = 1,ChangedByUser = '" + Username + "',ChangedOnDate = GETDATE(),LastResetDate = Getdate() Where SystemUserID = " + SystemUserID;
           // File.AppendAllText(@"C:\Log\Log.txt", sqlSelect);
            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, conString);
            Objsqlcom.ExecuteNonQuery();
        }
        
        public Entity.SystemUser GetSystemUser(SqlConnection conString, string strUserName)
        {
            if (conString.State == ConnectionState.Closed)
                conString.Open();

            Entity.SystemUser objSystemUser = new Entity.SystemUser();

            //SqlConnection ObjConstring = new SqlConnection(conString);
            //ObjConstring.Open();

            string sqlSelect = string.Format("SELECT [SystemUserID],isnull([SystemUserTypeInd],'') AS SystemUserTypeInd ,isnull([FirstName],'') AS FirstName,isnull([Surname],'') AS Surname,isnull([IDNo],'') AS IDNo,isnull([JobPosition],'') AS JobPosition,isnull([CellularCode], '') AS CellularCode,isnull([CellularNo], '')  AS CellularNo,isnull([EmailAddress], '') AS EmailAddress,isnull([ActiveYN],0) AS ActiveYN,isnull([Username],'') AS Username,isnull([SubscriberID],0) AS SubscriberID,isnull([CreatedByUser], '') AS CreatedByUser,isnull([CreatedOnDate], '') AS CreatedOnDate,isnull([ChangedByUser], '') AS ChangedByUser,isnull([ChangedOnDate], '') AS ChangedOnDate,isnull([SystemUserRole], '') AS SystemUserRole from SystemUser (NOLOCK) where UserName ='{0}' ", strUserName);

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, conString);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objSystemUser.SubscriberID = Convert.ToInt32(Dr["SubscriberID"]);
                objSystemUser.ActiveYN = bool.Parse(Dr["ActiveYN"].ToString());
                objSystemUser.CellularCode = Dr["CellularCode"].ToString();
                objSystemUser.CellularNo = Dr["CellularNo"].ToString();
                objSystemUser.ChangedByUser = Dr["ChangedByUser"].ToString();
                objSystemUser.ChangedOnDate = DateTime.Parse(Dr["ChangedOnDate"].ToString());
                objSystemUser.CreatedByUser = Dr["CreatedByUser"].ToString();
                objSystemUser.CreatedOnDate = DateTime.Parse(Dr["CreatedOnDate"].ToString());
                objSystemUser.EmailAddress = Dr["EmailAddress"].ToString();
                objSystemUser.FirstName = Dr["FirstName"].ToString();
                objSystemUser.IDNo = Dr["IDNo"].ToString();
                objSystemUser.JobPosition = Dr["JobPosition"].ToString();
                objSystemUser.Surname = Dr["Surname"].ToString();

                objSystemUser.SystemUserID = int.Parse(Dr["SystemUserID"].ToString());
                objSystemUser.SystemUserRole = Dr["SystemUserRole"].ToString();
                objSystemUser.SystemUserTypeInd = Dr["SystemUserTypeInd"].ToString();
                objSystemUser.Username = Dr["Username"].ToString();

            }
            objsqladp.Dispose();
            Objsqlcom.Dispose();
            ds.Dispose();
            conString.Close();

            return objSystemUser;
        }

        public Entity.SystemUser GetSystemUser(string conString, string strUserName)
        {
            Entity.SystemUser objSystemUser = new Entity.SystemUser();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = string.Format("SELECT [SystemUserID],isnull([SystemUserTypeInd],'') AS SystemUserTypeInd ,isnull([FirstName],'') AS FirstName,isnull([Surname],'') AS Surname,isnull([IDNo],'') AS IDNo,isnull([JobPosition],'') AS JobPosition,isnull([CellularCode], '') AS CellularCode,isnull([CellularNo], '')  AS CellularNo,isnull([EmailAddress], '') AS EmailAddress,isnull([ActiveYN],0) AS ActiveYN,isnull([Username],'') AS Username,isnull([SubscriberID],0) AS SubscriberID,isnull([CreatedByUser], '') AS CreatedByUser,isnull([CreatedOnDate], '') AS CreatedOnDate,isnull([ChangedByUser], '') AS ChangedByUser,isnull([ChangedOnDate], '') AS ChangedOnDate,isnull([SystemUserRole], '') AS SystemUserRole from SystemUser (NOLOCK) where UserName ='{0}' ", strUserName);

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objSystemUser.SubscriberID = Convert.ToInt32(Dr["SubscriberID"]);
                objSystemUser.ActiveYN = bool.Parse(Dr["ActiveYN"].ToString());
                objSystemUser.CellularCode = Dr["CellularCode"].ToString();
                objSystemUser.CellularNo = Dr["CellularNo"].ToString();
                objSystemUser.ChangedByUser = Dr["ChangedByUser"].ToString();
                objSystemUser.ChangedOnDate = DateTime.Parse(Dr["ChangedOnDate"].ToString());
                objSystemUser.CreatedByUser = Dr["CreatedByUser"].ToString();
                objSystemUser.CreatedOnDate = DateTime.Parse(Dr["CreatedOnDate"].ToString());
                objSystemUser.EmailAddress = Dr["EmailAddress"].ToString();
                objSystemUser.FirstName = Dr["FirstName"].ToString();
                objSystemUser.IDNo = Dr["IDNo"].ToString();
                objSystemUser.JobPosition = Dr["JobPosition"].ToString();
                objSystemUser.Surname = Dr["Surname"].ToString();

                objSystemUser.SystemUserID = int.Parse(Dr["SystemUserID"].ToString());
                objSystemUser.SystemUserRole = Dr["SystemUserRole"].ToString();
                objSystemUser.SystemUserTypeInd = Dr["SystemUserTypeInd"].ToString();
                objSystemUser.Username = Dr["Username"].ToString();

            }
            objsqladp.Dispose();
            Objsqlcom.Dispose();
            ds.Dispose();
            ObjConstring.Close();
            return objSystemUser;
        }

        public Entity.SystemUser GetSystemUserRecord(string conString, int SystemUserID)
        {
            Entity.SystemUser objSystemUser = new Entity.SystemUser();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            //,isnull([StayAlive], 0) AS StayAlive

            string sqlSelect = "SELECT [SystemUserID],isnull([SystemUserTypeInd],'') AS SystemUserTypeInd ,isnull([FirstName],'') AS FirstName,isnull([Surname],'') AS Surname,isnull([IDNo],'') AS IDNo,isnull([JobPosition],'') AS JobPosition,isnull([CellularCode], '') AS CellularCode,isnull([CellularNo], '')  AS CellularNo,isnull([EmailAddress], '') AS EmailAddress,isnull([ActiveYN],0) AS ActiveYN,isnull([Username],'') AS Username,isnull([SubscriberID],0) AS SubscriberID,isnull([CreatedByUser], '') AS CreatedByUser,isnull([CreatedOnDate], '') AS CreatedOnDate,isnull([ChangedByUser], '') AS ChangedByUser,isnull([ChangedOnDate], '') AS ChangedOnDate,isnull([SystemUserRole], '') AS SystemUserRole from SystemUser (NOLOCK) where SystemUserID = " + SystemUserID;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objSystemUser.SubscriberID = Convert.ToInt32(Dr["SubscriberID"]);
                objSystemUser.ActiveYN = bool.Parse(Dr["ActiveYN"].ToString());
                objSystemUser.CellularCode = Dr["CellularCode"].ToString();
                objSystemUser.CellularNo = Dr["CellularNo"].ToString();
                objSystemUser.ChangedByUser = Dr["ChangedByUser"].ToString();
                objSystemUser.ChangedOnDate = DateTime.Parse(Dr["ChangedOnDate"].ToString());
                objSystemUser.CreatedByUser = Dr["CreatedByUser"].ToString();
                objSystemUser.CreatedOnDate = DateTime.Parse(Dr["CreatedOnDate"].ToString());
                objSystemUser.EmailAddress = Dr["EmailAddress"].ToString();
                objSystemUser.FirstName = Dr["FirstName"].ToString();
                objSystemUser.IDNo = Dr["IDNo"].ToString();
                objSystemUser.JobPosition = Dr["JobPosition"].ToString();
                objSystemUser.Surname = Dr["Surname"].ToString();

                objSystemUser.SystemUserID = int.Parse(Dr["SystemUserID"].ToString());
                objSystemUser.SystemUserRole = Dr["SystemUserRole"].ToString();
                objSystemUser.SystemUserTypeInd = Dr["SystemUserTypeInd"].ToString();
                objSystemUser.Username = Dr["Username"].ToString();

                //if (!string.IsNullOrEmpty(Dr["StayAlive"].ToString()))
                //    objSystemUser.StayAlive = Convert.ToInt32(Dr["StayAlive"]);
                //else
                //    objSystemUser.StayAlive = 0;

            }
            objsqladp.Dispose();
            Objsqlcom.Dispose();
            ds.Dispose();
            ObjConstring.Close();
            return objSystemUser;
        }

        public DataSet GetSystemUserList(string conString, int SubscriberID)
        {
            Entity.SystemUser objSystemUser = new Entity.SystemUser();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            //,isnull([StayAlive], 0) AS StayAlive
            string sqlSelect = "SELECT [SystemUserID],isnull([SystemUserTypeInd],'') AS SystemUserTypeInd ,isnull([FirstName],'') AS FirstName,isnull([Surname],'') AS Surname,isnull([IDNo],'') AS IDNo,isnull([JobPosition],'') AS JobPosition,isnull([CellularCode], '') AS CellularCode,isnull([CellularNo], '')  AS CellularNo,isnull([EmailAddress], '') AS EmailAddress,isnull([ActiveYN],0) AS ActiveYN,isnull([Username],'') AS Username,isnull([SubscriberID],0) AS SubscriberID,isnull([CreatedByUser], '') AS CreatedByUser,isnull([CreatedOnDate], '') AS CreatedOnDate,isnull([ChangedByUser], '') AS ChangedByUser,isnull([ChangedOnDate], '') AS ChangedOnDate,isnull([SystemUserRole], '') AS SystemUserRole from SystemUser (NOLOCK) where SubscriberID = " + SubscriberID;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);
            ObjConstring.Close();
            return ds;
        }

        public List<Entity.SystemUser> GetSystemUserRecord(string conString, string whereclause)
        {
            List<Entity.SystemUser> objSystemUserList = new List<Entity.SystemUser>();
            Entity.SystemUser objSystemUser = new Entity.SystemUser();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = "select * from Subscriber " + whereclause;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objSystemUser.SubscriberID = Convert.ToInt32(Dr["SubscriberID"]);
                objSystemUser.ActiveYN = bool.Parse(Dr["ActiveYN"].ToString());
                objSystemUser.CellularCode = Dr["CellularCode"].ToString();
                objSystemUser.CellularNo = Dr["CellularNo"].ToString();
                objSystemUser.ChangedByUser = Dr["ChangedByUser"].ToString();
                objSystemUser.ChangedOnDate = DateTime.Parse(Dr["ChangedOnDate"].ToString());
                objSystemUser.CreatedByUser = Dr["CreatedByUser"].ToString();
                objSystemUser.CreatedOnDate = DateTime.Parse(Dr["CreatedOnDate"].ToString());
                objSystemUser.EmailAddress = Dr["EmailAddress"].ToString();
                objSystemUser.FirstName = Dr["FirstName"].ToString();
                objSystemUser.IDNo = Dr["IDNo"].ToString();
                objSystemUser.JobPosition = Dr["JobPosition"].ToString();
                objSystemUser.Surname = Dr["Surname"].ToString();

                objSystemUser.SystemUserID = int.Parse(Dr["SystemUserID"].ToString());
                objSystemUser.SystemUserRole = Dr["SystemUserRole"].ToString();
                objSystemUser.SystemUserTypeInd = Dr["SystemUserTypeInd"].ToString();
                objSystemUser.Username = Dr["Username"].ToString();
                objSystemUserList.Add(objSystemUser);
            }
            objsqladp.Dispose();
            Objsqlcom.Dispose();
            ds.Dispose();
            ObjConstring.Close();
            return objSystemUserList;
        }

        public int UpdateSystemUserRecord(string conString, int SystemUserID, string FirstName, string Surname, string Email, string JobPosition, string Role, bool isActive)
        {
            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            StringBuilder oStringBuilder = new StringBuilder();

            oStringBuilder.Append("UPDATE SystemUser SET ");
            oStringBuilder.Append("FirstName = '" + FirstName + "',");
            oStringBuilder.Append("Surname = '" + Surname + "',");
            //oStringBuilder.Append("EmailAddress = '" + Email + "',");
            oStringBuilder.Append("JobPosition = '" + JobPosition + "' ");
            //oStringBuilder.Append("SystemUserRole = '" + Role + "',");
            //oStringBuilder.Append("ActiveYN = " + Convert.ToInt32(isActive) + " ");
            oStringBuilder.Append("WHERE SystemUserID = " + SystemUserID);

            SqlCommand Objsqlcom = new SqlCommand(oStringBuilder.ToString(), ObjConstring);
            int result = Objsqlcom.ExecuteNonQuery();

            Objsqlcom.Dispose();
            ObjConstring.Close();

            return result;
        }
    }
}

