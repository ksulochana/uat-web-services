﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Threading;

namespace XDSPortalEnquiry.Data

{
    public class ThreadProcess
    {
         DataSet dataset = new DataSet();
        DataTable dtError;

        public enum executiontype
        {
            filladapter,
            nonquery
        }

        public  DataSet Processthreaddataset(SqlConnection ObjConstring, string SyncServers,string commandtext,CommandType ocommandtype, Dictionary<string, string> parametercollection, bool sort,string sortColumn, int topnrec,bool distinct,string[] distinctcolumnnames, executiontype exectype)
        {
            string[] syncsvrlist = SyncServers.Split(new char[] { '|' });

            int cnt = 0;

            foreach (string s in syncsvrlist)
            {
                if (!string.IsNullOrEmpty(s.Trim()))
                {
                    cnt++;
                }
            }

                object[] connections = new object[cnt + 1];

            System.IO.File.AppendAllText(@"C:\Log\AuthLog.txt", (syncsvrlist.Count() + 1).ToString() + "\n");
            System.IO.File.AppendAllText(@"C:\Log\AuthLog.txt", commandtext.ToString() + "\n");

            connections[0] = ObjConstring;
            int i = 1;

            if (syncsvrlist.Count() > 0)
            {
                foreach (string s in syncsvrlist)
                {
                    //System.IO.File.AppendAllText(@"C:\Log\AuthLog.txt", "inside loop" + "\n");
                    if (!string.IsNullOrEmpty(s.Trim()))
                    {
                        SqlConnection osqlConnection = new SqlConnection(s);
                        connections[i] = osqlConnection;
                        i++;
                    }
                }
            }

            List<Thread> threads = new List<Thread>();

            

            int threadcount = 0;

            dtError = new DataTable("Error");
            dtError.Columns.Add("ErrorDescription");

            foreach (object connection in connections)
            {
                object[] inputparameters = new object[2];
                SqlConnection ocon = (SqlConnection)connection;
                //SqlCommand osqlcommand = ocmd;
                //osqlcommand.Connection = ocon;

                SqlCommand osqlcommand = new SqlCommand(commandtext, ocon);
                osqlcommand.CommandType = ocommandtype;
                osqlcommand.CommandTimeout = 0;

                foreach (KeyValuePair<string,string> okeyValuePair in parametercollection)
                {
                    osqlcommand.Parameters.AddWithValue(okeyValuePair.Key, okeyValuePair.Value);
                }

                if (exectype == executiontype.filladapter)
                {
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(osqlcommand);
                    inputparameters[0] = sqlDataAdapter;
                }
                else if (exectype == executiontype.nonquery)
                {
                    if (ocon.State == ConnectionState.Closed)
                        ocon.Open();

                    inputparameters[0] = osqlcommand;
                }
                inputparameters[1] = exectype;

                //inputparameters[0] = connection;
                //inputparameters[1] = ocmd;


                Thread thread = new Thread(processsql);
                //{
                //    Name = string.Format("{0}", connection.)
                //};
               // System.IO.File.AppendAllText(@"C:\Log\AuthLog.txt", ((SqlConnection)connection).ConnectionString + "\n");
                thread.Start(inputparameters);
                threads.Add(thread);
                threadcount++;

                //  processsql(inputparameters);
            }

            threads.ForEach(t => t.Join());


            foreach (Thread thread2 in threads)
            {
                if (thread2.IsAlive)
                    thread2.Abort();
            }
            int x = 0;

            while (x < threadcount)
            {
                threads.RemoveAt(0);
                x++;
            }
            foreach (object connection in connections)
            {
                SqlConnection ocon = (SqlConnection)connection;
                if (ocon.State == ConnectionState.Open)
                    ocon.Close();
            }

            if (dtError.Rows.Count > 0)
            {
                throw new Exception(dtError.Rows[0]["ErrorDescription"].ToString());
            }

            DataTable dtoutput = new DataTable();

            if (dataset != null)
            {
                if (dataset.Tables.Count > 0)
                {
                    if (dataset.Tables[0].Rows.Count > 0)
                    {
                        if (sort)
                        {
                            dataset.Tables[0].DefaultView.Sort = sortColumn + " desc";
                        }

                        if (topnrec > 0)
                        {
                            dtoutput = dataset.Tables[0].DefaultView.ToTable(distinct, distinctcolumnnames).AsEnumerable().Take(topnrec).CopyToDataTable();
                        }
                        else
                        {
                            dtoutput = dataset.Tables[0].DefaultView.ToTable(distinct, distinctcolumnnames);
                        }
                    }
                }
            }
            DataSet ds = new DataSet();
            ds.Tables.Add(dtoutput);

            return ds;
        }

        //public void processsql(object parameters)
        //{

        //        object[] inputparameters = (object[])parameters;

        //        SqlConnection ocon = (SqlConnection)inputparameters[0];
        //        SqlCommand ocmd = (SqlCommand)inputparameters[1];

        //    try
        //    {

        //        ocmd.Connection = ocon;

        //        if (ocon.State == ConnectionState.Closed)
        //            ocon.Open();

        //        //System.IO.File.AppendAllText(@"C:\Log\AuthLog.txt", "After opening the connection" + "\n");
        //        //System.IO.File.AppendAllText(@"C:\Log\AuthLog.txt", ocon.State.ToString() + "\n");


        //        SqlDataAdapter objsqladp = new SqlDataAdapter(ocmd);

        //        objsqladp.Fill(dataset);

        //       // System.IO.File.AppendAllText(@"C:\Log\AuthLog.txt", "done" + "\n");

        //        if (ocon.State == ConnectionState.Open)
        //            ocon.Close();

        //        objsqladp.Dispose();

        //      //  System.IO.File.AppendAllText(@"C:\Log\AuthLog.txt", "connection closed" + "\n");
        //    }
        //    catch(Exception ex)
        //    {
        //        System.IO.File.AppendAllText(@"C:\Log\AuthLog.txt", ex.Message+ex.StackTrace + ocon.ConnectionString+ "\n");
        //    }

        //}

        public void processsql(object parameters)
        {

            object[] inputparameters = (object[])parameters;

            
            executiontype exectype = (executiontype)inputparameters[1];

            try
            {


                //System.IO.File.AppendAllText(@"C:\Log\AuthLog.txt", "After opening the connection" + "\n");
                //System.IO.File.AppendAllText(@"C:\Log\AuthLog.txt", ocon.State.ToString() + "\n");

                if (exectype == executiontype.filladapter)
                {
                    SqlDataAdapter objsqladp = (SqlDataAdapter)inputparameters[0];
                    objsqladp.Fill(dataset);

                    objsqladp.Dispose();
                }
                else if (exectype == executiontype.nonquery)
                {
                    SqlCommand objsqlcmd = (SqlCommand)inputparameters[0];
                    objsqlcmd.ExecuteNonQuery();
                    objsqlcmd.Dispose();

                }

                // System.IO.File.AppendAllText(@"C:\Log\AuthLog.txt", "done" + "\n");

                


                //  System.IO.File.AppendAllText(@"C:\Log\AuthLog.txt", "connection closed" + "\n");
            }
            catch (Exception ex)
            {

                System.IO.File.AppendAllText(@"C:\Log\AuthLog.txt", ex.Message + ex.StackTrace + "\n");
                DataRow drError = dtError.NewRow();
                drError["ErrorDescription"] = ex.Message;
                dtError.Rows.Add(drError);

            }

        }
    }
}
