﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XDSPortalEnquiry.Entity;

namespace XDSPortalEnquiry.Data
{
    public class BatchReports
    {
        public int CompareColumns(string strFileLocation, string type)// out string ProductID)
        {
            //ProductID = "0";
            try
            {

                DataTable dtUploadedFile = new DataTable();
                DataTable dtTemplate = new DataTable();

                dtTemplate.Columns.Add("IDNo");
                dtTemplate.Columns.Add("PassportNo");
                dtTemplate.Columns.Add("FirstName");
                dtTemplate.Columns.Add("Surname");
                dtTemplate.Columns.Add("SubscriberReference");



                switch (type)
                {
                    case "Excel":
                        string ConnectionString = "Provider = Microsoft.ACE.OLEDB.12.0; Data Source=" + strFileLocation + "; Extended Properties=Excel 12.0;";
                        using (OleDbConnection cn = new OleDbConnection(ConnectionString))
                        {
                            cn.Open();

                            DataTable dbSchema = cn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                            if (dbSchema == null || dbSchema.Rows.Count < 1)
                            {
                                return 0;
                            }

                            string WorkSheetName = dbSchema.Rows[0]["TABLE_NAME"].ToString();

                            OleDbDataAdapter da = new OleDbDataAdapter("SELECT * FROM [" + WorkSheetName + "]", cn);
                            dtUploadedFile = new DataTable(WorkSheetName);

                            da.Fill(dtUploadedFile);

                            //DataTable dtProductID=  dtUploadedFile.DefaultView.ToTable(true, "ProductID");

                            // if (dtProductID.Rows.Count > 1)
                            // {
                            //     throw new Exception("More than one productID supplied in the file, which is not supported");
                            // }
                            // else if (dtProductID.Rows.Count == 1 && dtProductID.Rows[0][0].ToString() == "0")
                            // {
                            //     throw new Exception("Invalid productID supplied in the file");
                            // }

                            // else if (dtProductID.Rows.Count == 1 && dtProductID.Rows[0][0].ToString() != "0")
                            // {
                            //     ProductID = dtProductID.Rows[0][0].ToString();
                            // }
                        }

                        if (dtTemplate.Columns.Count != dtUploadedFile.Columns.Count)
                            return 0;

                        foreach (DataColumn column in dtUploadedFile.Columns)
                        {
                            if (!dtTemplate.Columns.Contains(column.ColumnName.Replace(" ", "")))
                                return 0;
                        }

                        break;
                    case "CSV":
                        using (StreamReader reader = new StreamReader(strFileLocation))
                        {
                            string strLine = reader.ReadLine();
                            int i = 0;
                            int indexProductID = 0;

                            List<string> ListProductID = new List<string>();

                            if (!string.IsNullOrEmpty(strLine))
                            {
                                string[] values = strLine.Split(new char[] { ';' });

                                //if (i == 0)
                                //{
                                indexProductID = (values.Count()) - 1;
                                DataTable dtTemplateHeader = new DataTable("Headers");

                                foreach (string v in values)
                                {
                                    dtTemplateHeader.Columns.Add(v);
                                }

                                if (dtTemplate.Columns.Count != dtTemplateHeader.Columns.Count)
                                    return 0;

                                foreach (DataColumn column in dtTemplateHeader.Columns)
                                {
                                    if (!dtTemplate.Columns.Contains(column.ColumnName.Replace(" ", "")))
                                        return 0;
                                }
                                //}
                                //else if (i >0)
                                //{

                                //    if (!ListProductID.Contains(values[indexProductID]))
                                //    {
                                //        ListProductID.Add(values[indexProductID]);
                                //    }
                                //}
                                //i++;
                                //strLine = reader.ReadLine();
                            }

                            //if (ListProductID.Count > 1)
                            //{
                            //    throw new Exception("More than one productID supplied in the file, which is not supported");
                            //}
                            //else if (ListProductID.Count == 1 && ListProductID[0] == "0")
                            //{
                            //    throw new Exception("Invalid productID supplied in the file");
                            //}

                            //else if (ListProductID.Count == 1 && ListProductID[0] != "0")
                            //{
                            //    ProductID = ListProductID[0].ToString();
                            //}
                        }
                        break;
                }

                return 1;

            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public string LogRequest(SqlConnection cn, int intSubscriberID, string strFileName, string strSubscribername, string strStagingFileName, string strFileLocation, string strSystemUsername, string username, string password, int productID, bool GeneratePDF)
        {
            string result = "0";

            try
            {
                SqlCommand ocmd = new SqlCommand("sp_I_ReportRequest", cn);
                ocmd.CommandType = CommandType.StoredProcedure;
                ocmd.Parameters.AddWithValue("@SubscriberID", intSubscriberID.ToString());
                ocmd.Parameters.AddWithValue("@Subscribername", strSubscribername.ToString());
                ocmd.Parameters.AddWithValue("@FileName", strFileName);
                ocmd.Parameters.AddWithValue("@ProductID", productID);
                ocmd.Parameters.AddWithValue("@WSUsername", username);
                ocmd.Parameters.AddWithValue("@WSPassword", password);
                ocmd.Parameters.AddWithValue("@Requestedby", strSystemUsername);
                ocmd.Parameters.AddWithValue("@CustomizedReportName", string.Empty);
                ocmd.Parameters.AddWithValue("@GeneratePDF", GeneratePDF);

                if (cn.State == ConnectionState.Closed)
                {
                    cn.Open();
                }

                SqlDataReader oReader = ocmd.ExecuteReader();



                if (oReader.HasRows)
                {
                    while (oReader.Read())
                    {
                        result = oReader.GetValue(20).ToString();
                    }
                }
                else
                    result = "0";

                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                return result;
            }
            catch (Exception ex)
            {
                return "0";
            }
        }

        public int LogDownloadRequest(SqlConnection cn, string Reference, int intSubscriberID, int intsystemuserID, int intSkipToken, string username)
        {
            int result = 0;

            try
            {
                SqlCommand ocmd = new SqlCommand("sp_I_ReportRequestDownloadLog", cn);
                ocmd.CommandType = CommandType.StoredProcedure;
                ocmd.Parameters.AddWithValue("Reference", Reference);
                ocmd.Parameters.AddWithValue("@SubscriberID", intSubscriberID);
                ocmd.Parameters.AddWithValue("@systemuserID", intsystemuserID);
                ocmd.Parameters.AddWithValue("@SkipToken",intSkipToken);
                ocmd.Parameters.AddWithValue("@Createdbyuser", username);
            
                if (cn.State == ConnectionState.Closed)
                {
                    cn.Open();
                }

                SqlDataReader oReader = ocmd.ExecuteReader();


                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                return result;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public DataSet GetFileStatus(SqlConnection cn, string ReferenceNo)
        {
            string status = string.Empty;
            DataSet ds = new DataSet();
            try
            {
                SqlCommand ocmd = new SqlCommand("sp_Get_RequestStatus", cn);
                ocmd.CommandType = CommandType.StoredProcedure;
                ocmd.Parameters.AddWithValue("@ReferenceNo", ReferenceNo);

                if (cn.State == ConnectionState.Closed)
                {
                    cn.Open();
                }
                SqlDataAdapter oda = new SqlDataAdapter(ocmd);
                
                oda.Fill(ds);


                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
               // return 0;
            }


        }

        //   public async Task<BulkReport> Getdata(SqlConnection cn, string StagingName)
        //   {
        //       int result = 0;
        //       BulkReport oB = new BulkReport();
        //       try
        //       {

        //           using (SqlConnection connection = cn)
        //           {
        //               await connection.OpenAsync();
        //               using (SqlCommand command = new SqlCommand("SELECT isnull(SubscriberReference,'')SubscriberReference,isnull(FirstName,'')FirstName,isnull(Surname,'') Surname, isnull(IDno,'')IDno,isnull(PassportNo,'')PassportNo,isnull(EnquiryStatusInd,'') EnquiryStatusInd, isnull(EnquiryResultInd,'') EnquiryResultInd,isnull(XMLData,'') XMLData  FROM [Staging_29] (nolock)", connection))
        //               {

        //                   // The reader needs to be executed with the SequentialAccess behavior to enable network streaming  
        //                   // Otherwise ReadAsync will buffer the entire text document into memory which can cause scalability issues or even OutOfMemoryExceptions  
        //                   using (SqlDataReader reader = await command.ExecuteReaderAsync(CommandBehavior.SequentialAccess))
        //                   {
        //                       while (await reader.ReadAsync())
        //                       {
        //                          oB = new BulkReport();
        //                           oB.SubscriberReference = reader.GetValue(0).ToString();
        //                           oB.FirstName = reader.GetValue(1).ToString();
        //                           oB.Surname = reader.GetValue(2).ToString();
        //                           oB.IDno = reader.GetValue(3).ToString();
        //                           oB.PassportNo = reader.GetValue(4).ToString();
        //                           oB.EnquiryStatusInd = reader.GetValue(5).ToString();
        //                           oB.EnquiryResultInd = reader.GetValue(6).ToString();
        //                           oB.XMLData = reader.GetValue(7).ToString();                                  
        //                       }                           

        //                   }
        //                   return await Task.FromResult(oB);
        //               }
        //           }  


        //   }


        //}
    }
}
