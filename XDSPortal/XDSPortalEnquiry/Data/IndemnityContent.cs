﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDSPortalEnquiry.Data
{
   public  class IndemnityContent
    {
       public IndemnityContent() { }

     

       public DataSet GetContent(string sqlcon, int SubscriberID, int SystemUserID, int ProductID, string SubscriberName, string Username)
       {

           //  DataTable dt = new DataTable();
           SqlConnection ocon = new SqlConnection(sqlcon);
           SqlCommand ocmd = new SqlCommand("sp_Get_IndemnityContent", ocon);
           ocmd.CommandType = System.Data.CommandType.StoredProcedure;

           ocmd.Parameters.AddWithValue("@SubscriberID", SubscriberID);
           ocmd.Parameters.AddWithValue("@SystemuserID", SystemUserID);
           ocmd.Parameters.AddWithValue("@ProductID", ProductID);
           ocmd.Parameters.AddWithValue("@SubscriberName", SubscriberName);
           ocmd.Parameters.AddWithValue("@Username", Username);


           SqlDataAdapter oDa = new SqlDataAdapter(ocmd);
           DataSet ds = new DataSet();
           oDa.Fill(ds);

           ds.DataSetName = "IndemnityContent";

           if (ds.Tables.Count > 0)
           {
               ds.Tables[0].TableName = "Content";
           }
           
           return ds;        

       }

       public void UpdateAcknowledgement(string sqlcon, int SubscriberID, int SystemUserID, int ProductID,  string Username,bool Agree,int ReferenceNo)
       {

           //  DataTable dt = new DataTable();
           SqlConnection ocon = new SqlConnection(sqlcon);
           SqlCommand ocmd = new SqlCommand("sp_U_IndemnityAcceptance", ocon);
           ocmd.CommandType = System.Data.CommandType.StoredProcedure;

           ocmd.Parameters.AddWithValue("@SubscriberID", SubscriberID);
           ocmd.Parameters.AddWithValue("@SystemuserID", SystemUserID);
           ocmd.Parameters.AddWithValue("@ProductID", ProductID);
           ocmd.Parameters.AddWithValue("@ReferenceNo", ReferenceNo);
           ocmd.Parameters.AddWithValue("@Username", Username);
           ocmd.Parameters.AddWithValue("@Agree", Agree);

           if (ocon.State == System.Data.ConnectionState.Closed)
               ocon.Open();

           ocmd.ExecuteNonQuery();

           if (ocon.State == System.Data.ConnectionState.Open)
               ocon.Close();

       }

       public bool CheckIndemnityAccepted(string sqlcon, int SubscriberID, int SystemUserID, int ProductID)
       {
           bool accepted = false;

           //  DataTable dt = new DataTable();
           SqlConnection ocon = new SqlConnection(sqlcon);
           SqlCommand ocmd = new SqlCommand("Select 1 from SystemUserProductIndemnity (nolock) where SubscriberID = @SubscriberID and SystemUserID = @SystemuserID and ProductID= @ProductID and Statusind = 'A' and Agreed = 1", ocon);
           ocmd.CommandType = System.Data.CommandType.Text;

           ocmd.Parameters.AddWithValue("@SubscriberID", SubscriberID);
           ocmd.Parameters.AddWithValue("@SystemuserID", SystemUserID);
           ocmd.Parameters.AddWithValue("@ProductID", ProductID);
         


           SqlDataAdapter oDa = new SqlDataAdapter(ocmd);
           DataSet ds = new DataSet();
           oDa.Fill(ds);
          
           if (ds.Tables.Count > 0)
           {
               if (ds.Tables[0].Rows.Count > 0)
               {
                   accepted = true;
               }
           }

           return accepted;
       }
    }
}
