﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using sembleWare.Security;
using XDSPortalAuthentication;
using System.Threading;

namespace XDSPortalEnquiry.Data
{
    public enum OTPAction
    {
        Insert = 1,
        Evaluate = 2
    }

    public class Authentication
    {

        public XDSPortalLibrary.Entity_Layer.SubscriberAuthentication GetSubscriberAuthenticationID(SqlConnection ObjConstring, XDSPortalLibrary.Entity_Layer.Authentication oAuth, int EnquiryID, int EnquiryResultID)
        {
            XDSPortalLibrary.Entity_Layer.SubscriberAuthentication oSubAuth = new XDSPortalLibrary.Entity_Layer.SubscriberAuthentication();

            int AuthenticationID = 0;
            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();

            SqlCommand Objsqlcom = new SqlCommand("spAuthentication_S_SubscriberAuthentication", ObjConstring);
            Objsqlcom.CommandType = CommandType.StoredProcedure;
            Objsqlcom.CommandTimeout = 0;

            Objsqlcom.Parameters.AddWithValue("@SubscriberID", oAuth.SubscriberID);
            Objsqlcom.Parameters.AddWithValue("@ConsumerID", oAuth.ConsumerID);
            Objsqlcom.Parameters.AddWithValue("@ActionedBySystemUserID", oAuth.ActionedBySystemUserID);
            Objsqlcom.Parameters.AddWithValue("@ProductID", oAuth.ProductID);
            Objsqlcom.Parameters.AddWithValue("@BarnchCode", oAuth.BranchCode);
            Objsqlcom.Parameters.AddWithValue("@PurposeID", oAuth.PurposeID);
            Objsqlcom.Parameters.AddWithValue("@CreatedbyUser", oAuth.CreatedbyUser);
            Objsqlcom.Parameters.AddWithValue("@SubscriberName", oAuth.Subscribername);
            Objsqlcom.Parameters.AddWithValue("@SubscriberGroupCode", oAuth.SubscribergroupCode == null ? string.Empty : oAuth.SubscribergroupCode);
            Objsqlcom.Parameters.AddWithValue("@CellularNumber", oAuth.cellularnumber == null ? string.Empty : oAuth.cellularnumber);
            Objsqlcom.Parameters.AddWithValue("@EnquiryID", EnquiryID);
            Objsqlcom.Parameters.AddWithValue("@EnquiryresultID", EnquiryResultID);
            Objsqlcom.Parameters.AddWithValue("@SAFPSFlag", oAuth.SAFPSFlag);
            Objsqlcom.Parameters.AddWithValue("@OverrideOTP",oAuth.OverrideOTP);
            Objsqlcom.Parameters.AddWithValue("@OverrideOTPReason", oAuth.OverrideOTPReason);
            Objsqlcom.Parameters.AddWithValue("@EmailAddress", oAuth.EmailAddress);

            SqlParameter parAuthenticationID = new SqlParameter("@SubscriberAuthenticationID", SqlDbType.Int);
            parAuthenticationID.Direction = ParameterDirection.ReturnValue;

            Objsqlcom.Parameters.Add(parAuthenticationID);

            SqlParameter parSAFPSMessage = new SqlParameter("@SAFPSMessage",SqlDbType.VarChar,500);
            parSAFPSMessage.Direction = ParameterDirection.Output;

            Objsqlcom.Parameters.Add(parSAFPSMessage);

            Objsqlcom.ExecuteNonQuery();

            ObjConstring.Close();

            oSubAuth.SubscriberAuthenticationID = int.Parse(parAuthenticationID.Value.ToString());
            oSubAuth.SAFPSMessage = parSAFPSMessage.Value.ToString();

            return oSubAuth;
        }

        public DataSet GetSubscriberAuthenticationQuestion(SqlConnection ObjConstring, int AuthenticationID)
        {
            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();

            string sqlSelect = "select * from SubscriberAuthenticationQuestion (NOLOCK) where SubscriberAuthenticationID = " + AuthenticationID.ToString() + " order by newid()";

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            Objsqlcom.CommandTimeout = 0;
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            ObjConstring.Close();

            return ds;
        }


        public DataSet GetSubscriberAuthenticationQuestionAnswer(SqlConnection ObjConstring, int AuthenticationID)
        {
            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();

            //string sqlSelect = "select * from SubscriberAuthenticationQuestionAnswer (NOLOCK) where SubscriberAuthenticationID = " + AuthenticationID.ToString() + " order by newid() ";
            string sqlSelect = "select * from SubscriberAuthenticationQuestionAnswer (NOLOCK) where SubscriberAuthenticationID = " + AuthenticationID.ToString();

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            Objsqlcom.CommandTimeout = 0;
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            ObjConstring.Close();

            return ds;
        }

        public XDSPortalLibrary.Entity_Layer.SubscriberAuthentication GetSubscriberAuthenticationObject(SqlConnection ObjConstring, int SubscriberAuthenticationID)
        {
            XDSPortalLibrary.Entity_Layer.SubscriberAuthentication oSubscriberAuthentication = new XDSPortalLibrary.Entity_Layer.SubscriberAuthentication();

            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();

            string sqlSelect = "select * from SubscriberAuthentication (NOLOCK) where SubscriberAuthenticationID = " + SubscriberAuthenticationID.ToString() + " ";

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            Objsqlcom.CommandTimeout = 0;
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            ObjConstring.Close();


            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                oSubscriberAuthentication.SubscriberID = int.Parse(dr["SubscriberID"].ToString());
                oSubscriberAuthentication.SubscriberAuthenticationID = int.Parse(dr["SubscriberAuthenticationID"].ToString());
                oSubscriberAuthentication.AuthenticationTypeInd = dr["AuthenticationTypeInd"].ToString();
                oSubscriberAuthentication.AuthenticationDate = DateTime.Parse(dr["AuthenticationDate"].ToString());
                oSubscriberAuthentication.ReferenceNo = dr["ReferenceNo"].ToString();
                oSubscriberAuthentication.AuthenticationStatusInd = dr["AuthenticationStatusInd"].ToString();
                oSubscriberAuthentication.RequiredAuthenticatedPerc = decimal.Parse(dr["RequiredAuthenticatedPerc"].ToString());
                oSubscriberAuthentication.AuthenticatedPerc = decimal.Parse(dr["AuthenticatedPerc"].ToString());
                oSubscriberAuthentication.TotalQuestionPointValue = decimal.Parse(dr["TotalQuestionPointValue"].ToString());
                oSubscriberAuthentication.IDNoVerificationYN = bool.Parse(dr["IDNoVerificationYN"].ToString());
                oSubscriberAuthentication.RepeatAuthenticationMessage = dr["RepeatAuthenticationMessage"].ToString();
                oSubscriberAuthentication.AuthenticationComment = dr["AuthenticationComment"].ToString();
                oSubscriberAuthentication.ConsumerID = int.Parse(dr["ConsumerID"].ToString());
                oSubscriberAuthentication.ActionedBySystemUserID = int.Parse(dr["ActionedBySystemUserID"].ToString());
                oSubscriberAuthentication.SubscriberBranchCode = dr["SubscriberBranchCode"].ToString();
                oSubscriberAuthentication.AuthenticationPurposeID = int.Parse(dr["AuthenticationPurposeID"].ToString());
                oSubscriberAuthentication.PreviousSubscriberAuthenticationID = int.Parse(string.IsNullOrEmpty(dr["PreviousSubscriberAuthenticationID"].ToString()) ? "0" : dr["PreviousSubscriberAuthenticationID"].ToString());
                oSubscriberAuthentication.CreatedByUser = dr["CreatedByUser"].ToString();
                oSubscriberAuthentication.CreatedOnDate = DateTime.Parse(dr["CreatedOnDate"].ToString());
                oSubscriberAuthentication.ChangedByUser = dr["ChangedByUser"].ToString();
                oSubscriberAuthentication.ChangedOnDate = DateTime.Parse(string.IsNullOrEmpty(dr["ChangedOnDate"].ToString()) ? "1900/01/01" : dr["ChangedOnDate"].ToString());
                oSubscriberAuthentication.ConsumerAccountAgeMessage = dr["ConsumerAccountAgeMessage"].ToString();
                oSubscriberAuthentication.PreviousSubscriberID = int.Parse(string.IsNullOrEmpty(dr["PreviousSubscriberID"].ToString()) ? "0" : dr["PreviousSubscriberID"].ToString());
                oSubscriberAuthentication.ProductID = int.Parse(dr["ProductID"].ToString());
                oSubscriberAuthentication.EnquiryID = int.Parse(dr["EnquiryID"].ToString());
                oSubscriberAuthentication.EnquiryResultID = int.Parse(dr["EnquiryResultID"].ToString());
                oSubscriberAuthentication.RetryCount = int.Parse(string.IsNullOrEmpty(dr["RetryCount"].ToString()) ? "0" : dr["RetryCount"].ToString());
                oSubscriberAuthentication.SafpsIndicator = bool.Parse(dr["SAFPSIndicator"].ToString());
                oSubscriberAuthentication.Reason = dr["Reason"].ToString();
                oSubscriberAuthentication.EncryptedReferenceNo = dr["EncryptedReferenceNo"].ToString();
                oSubscriberAuthentication.CellularNumber = dr["CellularNumber"].ToString();
                oSubscriberAuthentication.OverrideOTP = Convert.ToBoolean(dr["OverrideOTP"]);
                oSubscriberAuthentication.OverrideOTPReason = dr["OverrideOTPReason"].ToString();
                oSubscriberAuthentication.EmailAddress = dr["EmailAddress"].ToString();
            
                
                oSubscriberAuthentication.NoofmandatoryQuestions = int.Parse(dr["SubscriberHomeAffairsEnquiryID"] == null ? "0": dr["SubscriberHomeAffairsEnquiryID"].ToString());
                oSubscriberAuthentication.MandatoryCriteriaMet = bool.Parse(dr["SubscriberConsumerEnquiryID"] == null ? "true":(dr["SubscriberConsumerEnquiryID"].ToString() == "1"?"true":"false"));
            }

            return oSubscriberAuthentication;
        }
        
        public void UpdateSubscriberAuthenticationQuestionAnswer(SqlConnection ObjConstring, int SubscriberID, int SubscriberAuthenticationID, int ProductAuthenticationID, DataTable dtAnswers)
        {

            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();

            SqlCommand Objsqlcom = new SqlCommand("spAuthentication_U_Answers", ObjConstring);
            Objsqlcom.CommandType = CommandType.StoredProcedure;
            Objsqlcom.CommandTimeout = 0;

            Objsqlcom.Parameters.AddWithValue("@SubscriberID", SubscriberID);
            Objsqlcom.Parameters.AddWithValue("@SubscriberAuthenticationID", SubscriberAuthenticationID);
            Objsqlcom.Parameters.AddWithValue("@AnswerDocument", dtAnswers);

            Objsqlcom.ExecuteNonQuery();

            ObjConstring.Close();

        }

        public void ValidateAnswers(SqlConnection ObjConstring, XDSPortalAuthentication.AuthenticationProcess oAuthProcess, string Username, bool OTPMatch)
        {

            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();

            SqlCommand Objsqlcom = new SqlCommand("spAuthentication_U_ValidateAnswers", ObjConstring);
            Objsqlcom.CommandType = CommandType.StoredProcedure;
            Objsqlcom.CommandTimeout = 0;


            Objsqlcom.Parameters.AddWithValue("@SubscriberID", oAuthProcess.CurrentObjectState.AuthenticationDocument.SubscriberID);
            Objsqlcom.Parameters.AddWithValue("@SubscriberAuthenticationID", oAuthProcess.CurrentObjectState.AuthenticationDocument.SubscriberAuthenticationID);
            Objsqlcom.Parameters.AddWithValue("@Username", Username);
            Objsqlcom.Parameters.AddWithValue("@OTPMatch", OTPMatch);

            Objsqlcom.ExecuteNonQuery();


            ObjConstring.Close();
        }

        public void UpdateVoidAttempts(SqlConnection ObjConstring, XDSPortalAuthentication.AuthenticationProcess oAuthProcess, string Username)
        {
            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();

            SqlCommand Objsqlcom = new SqlCommand("spAuthentication_U_Void", ObjConstring);
            Objsqlcom.CommandType = CommandType.StoredProcedure;
            Objsqlcom.CommandTimeout = 0;

            Objsqlcom.Parameters.AddWithValue("@SubscriberID", oAuthProcess.CurrentObjectState.AuthenticationDocument.SubscriberID);
            Objsqlcom.Parameters.AddWithValue("@SubscriberAuthenticationID", oAuthProcess.CurrentObjectState.AuthenticationDocument.SubscriberAuthenticationID);
            Objsqlcom.Parameters.AddWithValue("@Username", Username);

            Objsqlcom.ExecuteNonQuery();

            ObjConstring.Close();
        }

        public void UpdateSubscriberAuthentication(SqlConnection ObjConstring, XDSPortalLibrary.Entity_Layer.SubscriberAuthentication oSa)
        {

            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();

            SqlCommand Objsqlcom = new SqlCommand("spAuthentication_U_SubscriberAuthentication", ObjConstring);
            Objsqlcom.CommandType = CommandType.StoredProcedure;
            Objsqlcom.CommandTimeout = 0;

            Objsqlcom.Parameters.AddWithValue("@SubscriberID", oSa.SubscriberID);
            Objsqlcom.Parameters.AddWithValue("@SubscriberAuthenticationID", oSa.SubscriberAuthenticationID);
            Objsqlcom.Parameters.AddWithValue("@AuthenticationStatusInd", oSa.AuthenticationStatusInd);
            Objsqlcom.Parameters.AddWithValue("@Comments", oSa.AuthenticationComment);
            Objsqlcom.Parameters.AddWithValue("@Username", oSa.CreatedByUser);
            Objsqlcom.Parameters.AddWithValue("@Reason", oSa.Reason);
            Objsqlcom.Parameters.AddWithValue("@EncryptedReferenceNo", oSa.EncryptedReferenceNo);

            Objsqlcom.ExecuteNonQuery();


            ObjConstring.Close();
        }

        public void AuthenticationError(SqlConnection ObjConstring, int SubscriberID, int SubscriberAuthenticationID, int EnquiryID, int EnquiryResultID, string errormessage, string Level)
        {

            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();

            SqlCommand Objsqlcom = new SqlCommand("spAuthentication_I_Error", ObjConstring);
            Objsqlcom.CommandType = CommandType.StoredProcedure;
            Objsqlcom.CommandTimeout = 0;

            Objsqlcom.Parameters.AddWithValue("@SubscriberID", SubscriberID);
            Objsqlcom.Parameters.AddWithValue("@SubscriberAuthenticationID", SubscriberAuthenticationID);
            Objsqlcom.Parameters.AddWithValue("@Level", Level);
            Objsqlcom.Parameters.AddWithValue("@ErrorMessage", errormessage);
            Objsqlcom.Parameters.AddWithValue("@EnquiryID", EnquiryID);
            Objsqlcom.Parameters.AddWithValue("@EnquiryResultID", EnquiryResultID);

            Objsqlcom.ExecuteNonQuery();

            ObjConstring.Close();
        }

        public DataSet GetSubscriberAuthenticationDataset(SqlConnection ObjConstring, int SubscriberAuthenticationID)
        {

            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();

            string sqlSelect = "select 'SubscriberAuthentication' as Tablename, * from SubscriberAuthentication (NOLOCK) where SubscriberAuthenticationID = " + SubscriberAuthenticationID.ToString() + " ";

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            Objsqlcom.CommandTimeout = 0;
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            ObjConstring.Close();

            if (ds.Tables[0].Columns.Contains("TableName"))
            {
                ds.Tables[0].TableName = ds.Tables[0].Rows[0]["TableName"].ToString();
                ds.Tables[0].Columns.Remove("TableName");
            }

            return ds;
        }

        public DataSet GetConsumerAuthenticationHistory(SqlConnection ObjConstring, int AuthenticationID, int ConsumerID)
        {

            Dictionary<string, string> oparametercollection = new Dictionary<string, string>();

            oparametercollection.Add("@SubscriberAuthenticationID", AuthenticationID.ToString());
            oparametercollection.Add("@ConsumerID", ConsumerID.ToString());

            
            ThreadProcess threadProcess = new ThreadProcess();

            DataSet ds = threadProcess.Processthreaddataset(ObjConstring,XDSPortalEnquiry.Entity.SystemName.authSyncserver, "spAuthentication_Get_AuthenticationHistory",CommandType.StoredProcedure, oparametercollection, true, "BureauDate",5,true,new string[] { "BureauDate", "SubscriberName", "Cellularnumber", "AuthenticationStatusInd", "AuthenticatedPerc", "EmailAddress" }, ThreadProcess.executiontype.filladapter);
           // System.IO.File.AppendAllText(@"C:\Log\AuthLog.txt", "Exit" + "\n");
            return ds;
        }

        public DataSet GetSubscriberVoidReasons(SqlConnection ObjConstring, int AuthenticationID, int SubscriberID)
        {
            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();



            SqlCommand Objsqlcom = new SqlCommand("spAuthentication_Get_VoidReasons", ObjConstring);
            Objsqlcom.CommandType = CommandType.StoredProcedure;
            Objsqlcom.CommandTimeout = 0;
            Objsqlcom.Parameters.AddWithValue("@SubscriberAuthenticationID", AuthenticationID);
            Objsqlcom.Parameters.AddWithValue("@SubscriberID", SubscriberID);

            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            ObjConstring.Close();

            return ds;
        }

        public DataSet GetPersonalQuestions(SqlConnection ObjConstring, int subscriberID, int ConsumerID, int SubscriberAuthenticationID, string CreatedbyUser)
        {
            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();


            SqlCommand Objsqlcom = new SqlCommand("spAuthentication_Get_PersonalQuestions", ObjConstring);
            Objsqlcom.CommandType = CommandType.StoredProcedure;
            Objsqlcom.CommandTimeout = 0;
            Objsqlcom.Parameters.AddWithValue("@SubscriberAuthenticationID", SubscriberAuthenticationID);
            Objsqlcom.Parameters.AddWithValue("@SubscriberID", subscriberID);
            Objsqlcom.Parameters.AddWithValue("@CreatedbyUser", CreatedbyUser);
            Objsqlcom.Parameters.AddWithValue("@ConsumerID", ConsumerID);

            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            ObjConstring.Close();

            if (ObjConstring.State == ConnectionState.Open)
                ObjConstring.Close();

            return ds;
        }

        public DataSet GetPersonalQuestionsConsumerDirect(SqlConnection ObjConstring, int subscriberID, int ConsumerID)
        {
            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();

            SqlCommand Objsqlcom = new SqlCommand("spConsumerDirect_Get_PersonalQuestionAnswers", ObjConstring);
            Objsqlcom.CommandType = CommandType.StoredProcedure;
            Objsqlcom.CommandTimeout = 0;
            Objsqlcom.Parameters.AddWithValue("@SubscriberID", subscriberID);
            Objsqlcom.Parameters.AddWithValue("@ConsumerID", ConsumerID);

            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            ObjConstring.Close();

            if (ObjConstring.State == ConnectionState.Open)
                ObjConstring.Close();

            return ds;
        }
        
        public XDSPortalLibrary.Entity_Layer.SubscriberAuthenticationProfile GetSubscriberAuthenticationProfile(SqlConnection ObjConstring, int subscriberID, string Category)
        {
            XDSPortalLibrary.Entity_Layer.SubscriberAuthenticationProfile oSubscriberAuthenticationProfile = new XDSPortalLibrary.Entity_Layer.SubscriberAuthenticationProfile();

            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();

            //SqlCommand Objsqlcom = new SqlCommand("Select * from Auth_S_Profile a inner join Auth_S_BlockSettings b on a.ProfileID=b.ProfileID where SubscriberID = @SubscriberID and a.StatusInd = 'A' and b.StatusInd = 'A'", ObjConstring);
            SqlCommand Objsqlcom = new SqlCommand("Select * from Auth_S_Profile where SubscriberID = @SubscriberID and StatusInd = 'A'", ObjConstring);
            Objsqlcom.CommandTimeout = 0;
            Objsqlcom.Parameters.AddWithValue("@SubscriberID", subscriberID);

            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            ObjConstring.Close();

            if (ds.Tables.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    oSubscriberAuthenticationProfile.SubscriberAuthenticationProfileID = int.Parse(dr["ProfileID"].ToString());
                    oSubscriberAuthenticationProfile.SubscriberID = int.Parse(dr["SubscriberID"].ToString());
                    //oSubscriberAuthenticationProfile.SubscriberGroupCode = dr["SubscriberGroupCode"].ToString();
                    oSubscriberAuthenticationProfile.BlockUser = bool.Parse(dr["BlockUser"].ToString());
                    //oSubscriberAuthenticationProfile.NoOfRetries = int.Parse(dr["NoOfRetries"].ToString());
                    //oSubscriberAuthenticationProfile.RetryPeriod = int.Parse(dr["RetryPeriod"].ToString());
                    oSubscriberAuthenticationProfile.TotalNoOfQuestions = int.Parse(dr["TotalNoOfQuestions"].ToString());
                    oSubscriberAuthenticationProfile.PersonalQuestionsCheck = bool.Parse(dr["PersonalQuestionsCheck"].ToString());
                    oSubscriberAuthenticationProfile.UnblockAccess = bool.Parse(dr["UnblockAccess"].ToString());
                    oSubscriberAuthenticationProfile.ReferToFraud = bool.Parse(dr["ReferToFraud"].ToString());
                    oSubscriberAuthenticationProfile.QuestionTimeout = long.Parse(dr["QuestionTimeOut"].ToString());
                    oSubscriberAuthenticationProfile.CellNoMandatory = bool.Parse(dr["CellNoMandatory"].ToString());
                    oSubscriberAuthenticationProfile.ExcludeInputcellNo = bool.Parse(dr["ExcludeInputcellNo"].ToString());
                    oSubscriberAuthenticationProfile.ExcludeLatestDBcellNo = bool.Parse(dr["ExcludeLatestDBcellNo"].ToString());
                    oSubscriberAuthenticationProfile.OverrideOTP = bool.Parse(dr["OverrideOTP"].ToString());
                    oSubscriberAuthenticationProfile.ExcludeEmailAddress = bool.Parse(dr["ExcludeEmailAddress"].ToString());
                    oSubscriberAuthenticationProfile.EmailMandatory = bool.Parse(dr["EmailMandatory"].ToString());

                    //V2.2
                    oSubscriberAuthenticationProfile.FraudScore = bool.Parse(dr["FraudScore"].ToString());
                    oSubscriberAuthenticationProfile.AccountVerification = bool.Parse(dr["AccountVerification"].ToString());
                    oSubscriberAuthenticationProfile.IDPhoto = bool.Parse(dr["IDPhoto"].ToString());
                    oSubscriberAuthenticationProfile.CreditReport = bool.Parse(dr["CreditReport"].ToString());
                    oSubscriberAuthenticationProfile.ResendOTP = bool.Parse(dr["ResendOTP"].ToString());
                    oSubscriberAuthenticationProfile.NoOfOTPRetries = int.Parse(dr["NoOfOTPRetries"].ToString());
                    if (dr["NotificationAlert"] != DBNull.Value)
                    {
                        oSubscriberAuthenticationProfile.NotificationAlert = Boolean.Parse(dr["NotificationAlert"].ToString());
                    }
                    else
                    {
                        oSubscriberAuthenticationProfile.NotificationAlert = false;
                    }
                }
            }
            ds.Dispose();

            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();

            SqlCommand Objsqlcom1 = new SqlCommand("Select * from Auth_S_Profile a inner join Auth_S_BlockSettings b on a.ProfileID=b.ProfileID inner join Auth_L_BlockCategories c on b.BlockCategoriesID=c.BlockCategoryID where SubscriberID = @SubscriberID and a.StatusInd = 'A' and b.StatusInd='A' and c.Category='" + Category.ToUpper() + "'", ObjConstring);
            Objsqlcom1.CommandTimeout = 0;
            Objsqlcom1.Parameters.AddWithValue("@SubscriberID", subscriberID);

            SqlDataAdapter objsqladp1 = new SqlDataAdapter(Objsqlcom1);
            DataSet ds1 = new DataSet();
            objsqladp1.Fill(ds1);

            ObjConstring.Close();

            if (ds1.Tables.Count > 0)
            {
                foreach (DataRow dr in ds1.Tables[0].Rows)
                {
                    oSubscriberAuthenticationProfile.NoOfRetries = int.Parse(dr["NoOfRetries"].ToString());
                    oSubscriberAuthenticationProfile.RetryPeriod = int.Parse(dr["RetryPeriod"].ToString());
                }
            }
            ds1.Dispose();

            return oSubscriberAuthenticationProfile;
        }

        public void savePersonalQuestions(SqlConnection ObjConstring, int subscriberID, int ConsumerID, int SubscriberAuthenticationID, DataTable dtPersonalQuestions, string CreatedbyUser)
        {
            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();

            SqlCommand Objsqlcom = new SqlCommand("spAuthentication_Save_PersonalQuestions", ObjConstring);
            Objsqlcom.CommandType = CommandType.StoredProcedure;
            Objsqlcom.CommandTimeout = 0;
            Objsqlcom.Parameters.AddWithValue("@SubscriberAuthenticationID", SubscriberAuthenticationID);
            Objsqlcom.Parameters.AddWithValue("@SubscriberID", subscriberID);
            Objsqlcom.Parameters.AddWithValue("@CreatedbyUser", CreatedbyUser);
            Objsqlcom.Parameters.AddWithValue("@ConsumerID", ConsumerID);
            Objsqlcom.Parameters.AddWithValue("@PersonalQuestions", dtPersonalQuestions);

            Objsqlcom.ExecuteNonQuery();
        }

        public void savePersonalQuestionsConsumerDirect(SqlConnection ObjConstring, int subscriberID, int ConsumerID, DataTable dtPersonalQuestions, string CreatedbyUser)
        {
            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();

            SqlCommand Objsqlcom = new SqlCommand("spConsumerDirect_Save_PersonalQuestions", ObjConstring);
            Objsqlcom.CommandType = CommandType.StoredProcedure;
            Objsqlcom.CommandTimeout = 0;
            Objsqlcom.Parameters.AddWithValue("@SubscriberID", subscriberID);
            Objsqlcom.Parameters.AddWithValue("@ConsumerID", ConsumerID);
            Objsqlcom.Parameters.AddWithValue("@CreatedbyUser", CreatedbyUser);
            Objsqlcom.Parameters.AddWithValue("@PersonalQuestions", dtPersonalQuestions);

            Objsqlcom.ExecuteNonQuery();
        }

        public XDSPortalLibrary.Entity_Layer.Response BlockUnblockConsumer(SqlConnection ObjConstring, XDSPortalLibrary.Entity_Layer.BlockedConsumers obConsumer)
        {
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            int iMode = 101;
            if (obConsumer.BlockID > 0)
                iMode = 102;

            try
            {
                if (ObjConstring.State == ConnectionState.Closed)
                    ObjConstring.Open();

                SqlCommand objcmd = new SqlCommand("USP_IU_BlockedConsumers_V2", ObjConstring);
                objcmd.CommandType = CommandType.StoredProcedure;
                objcmd.CommandTimeout = 0;

                objcmd.Parameters.AddWithValue("@inMode", iMode);
                objcmd.Parameters.AddWithValue("@inBlockID", obConsumer.BlockID);
                objcmd.Parameters.AddWithValue("@inConsumerID", obConsumer.ConsumerID);
                objcmd.Parameters.AddWithValue("@ivIDNo", obConsumer.IDNo);
                objcmd.Parameters.AddWithValue("@ivPassportNo", obConsumer.PassportNo);
                objcmd.Parameters.AddWithValue("@ivSurname", obConsumer.SurName);
                objcmd.Parameters.AddWithValue("@ivFirstName", obConsumer.FirstName);
                objcmd.Parameters.AddWithValue("@ivSecondName", obConsumer.SecondName);
                objcmd.Parameters.AddWithValue("@idtBirthDate", obConsumer.DateofBirth);
                objcmd.Parameters.AddWithValue("@inSystemUserID", obConsumer.SystemUserID);
                objcmd.Parameters.AddWithValue("@inSubscriberID", obConsumer.SubscriberID);
                objcmd.Parameters.AddWithValue("@inSubscriberEnquiryID", obConsumer.SubscriberEnquiryID);
                objcmd.Parameters.AddWithValue("@inSubscriberEnquiryResultID", obConsumer.SubscriberEnquiryResultID);
                objcmd.Parameters.AddWithValue("@inProductID", obConsumer.ProductID);
                objcmd.Parameters.AddWithValue("@ibIsBlocked", obConsumer.IsBlocked);
                objcmd.Parameters.AddWithValue("@ivBlockedByUser", obConsumer.BlockedByUser);
                objcmd.Parameters.AddWithValue("@ivUnblockedByUser", obConsumer.UnblockedByUser);
                objcmd.Parameters.AddWithValue("@ivBlockingReason", obConsumer.BlockingReason);
                objcmd.Parameters.AddWithValue("@ivSubscriberName", obConsumer.SubscriberName);

                SqlParameter osqlparameter = new SqlParameter("BlockID", SqlDbType.Int);
                osqlparameter.Direction = ParameterDirection.ReturnValue;
                objcmd.Parameters.Add(osqlparameter);

                objcmd.ExecuteNonQuery();

                obConsumer.BlockID = int.Parse(osqlparameter.Value.ToString());

                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                rp.ResponseKey = obConsumer.BlockID;
            }
            catch (Exception oException)
            {
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;
            }
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response GetBlockedConsumers(SqlConnection ObjConstring, int SubscriberID)
        {
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                if (ObjConstring.State == ConnectionState.Closed)
                    ObjConstring.Open();

                SqlCommand objcmd = new SqlCommand("spAuthentication_Get_BlockedConsumers", ObjConstring);
                objcmd.CommandType = CommandType.StoredProcedure;
                objcmd.CommandTimeout = 0;
                objcmd.Parameters.AddWithValue("@SubscriberID", SubscriberID);
                objcmd.CommandTimeout = 0;
                DataSet dsBC = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(objcmd);
                da.Fill(dsBC);
                dsBC.DataSetName = "BlockedConsumer";
                if (dsBC.Tables.Count > 0)
                {
                    dsBC.Tables[0].TableName = "Consumer";
                }

                rp.ResponseKey = 0;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                rp.ResponseData = dsBC.GetXml();

                if (ObjConstring.State == ConnectionState.Open)
                    ObjConstring.Close();

                dsBC.Dispose();

            }
            catch (Exception oException)
            {
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = "<Error>" + oException.Message + "</Error>";
            }
            return rp;
        }

        public bool GetUnblockUser(SqlConnection ObjConstring, int intSystemUserID)
        {
            try
            {
                if (ObjConstring.State == ConnectionState.Closed)
                    ObjConstring.Open();

                SqlCommand objcmd = new SqlCommand("Select * from SystemUserUnblock Where SystemUserID=" + intSystemUserID.ToString() + " and RecordStatusInd='A'", ObjConstring);
                objcmd.CommandType = CommandType.Text;
                objcmd.CommandTimeout = 0;

                DataSet dsUser = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(objcmd);
                da.Fill(dsUser);

                if (ObjConstring.State == ConnectionState.Open)
                    ObjConstring.Close();

                if (dsUser.Tables.Count > 0 && dsUser.Tables[0].Rows.Count > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public DataSet GetSubscriberBlockingSettings(SqlConnection ObjConstring, int ProfileID, int SubscriberID)
        {
            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();

            SqlCommand Objsqlcom = new SqlCommand("spAuthentication_Get_BlockSettings", ObjConstring);
            Objsqlcom.CommandType = CommandType.StoredProcedure;
            Objsqlcom.CommandTimeout = 0;
            Objsqlcom.Parameters.AddWithValue("@ProfileID", ProfileID);
            Objsqlcom.Parameters.AddWithValue("@SubscriberID", SubscriberID);

            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            ObjConstring.Close();

            return ds;
        }

        public void GenerateQuestions(SqlConnection ObjConstring, XDSPortalLibrary.Entity_Layer.SubscriberAuthentication oAuth)
        {
            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();

            SqlCommand Objsqlcom = new SqlCommand("spAuthentication_I_DefaultQuestions", ObjConstring);
            Objsqlcom.CommandType = CommandType.StoredProcedure;
            Objsqlcom.CommandTimeout = 0;
            Objsqlcom.Parameters.AddWithValue("@SubscriberID", oAuth.SubscriberID);
            Objsqlcom.Parameters.AddWithValue("@SubscriberAuthenticationID", oAuth.SubscriberAuthenticationID);
            Objsqlcom.Parameters.AddWithValue("@Username", oAuth.CreatedByUser);
            Objsqlcom.Parameters.AddWithValue("@AuthenitcationTypeInd", oAuth.AuthenticationTypeInd);
            Objsqlcom.Parameters.AddWithValue("@IDIssuedDate", oAuth.IDIssuedDate);

            Objsqlcom.ExecuteNonQuery();

            ObjConstring.Close();


        }

        public DataSet GetSubscriberFraudReasons(SqlConnection ObjConstring, int AuthenticationID, int SubscriberID)
        {
            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();



            SqlCommand Objsqlcom = new SqlCommand("spAuthentication_Get_FraudReasons", ObjConstring);
            Objsqlcom.CommandType = CommandType.StoredProcedure;
            Objsqlcom.CommandTimeout = 0;
            Objsqlcom.Parameters.AddWithValue("@SubscriberAuthenticationID", AuthenticationID);
            Objsqlcom.Parameters.AddWithValue("@SubscriberID", SubscriberID);

            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            ObjConstring.Close();

            return ds;
        }

        public string GetEncryptedReferenceNo(XDSPortalLibrary.Entity_Layer.SubscriberAuthentication oSa, XDSPortalEnquiry.Entity.SubscriberEnquiryResult eSc)
        {
            string EncryptedReferenceNo = string.Empty, Seperator = "|", ReferenceNo = string.Empty;
            int CheckSumeven = 0, CheckSumOdd = 0, CheckSumTotal = 0, Counter = 0;
            string IDNo = string.IsNullOrEmpty(eSc.IDNo) ? eSc.PassportNo : eSc.IDNo;

            ReferenceNo = string.Format("{0}{1}{2}{3}{4}", IDNo, Seperator, oSa.AuthenticationStatusInd.ToString(), Seperator, oSa.AuthenticatedPerc.ToString());

            char[] IDArray = new char[IDNo.Length];

            if (!(string.IsNullOrEmpty(IDNo)))
            {
                IDArray = IDNo.ToCharArray();

                foreach (Char s in IDArray)
                {
                    if (Counter % 2 == 0)
                        CheckSumeven += int.Parse(char.GetNumericValue(s).ToString());
                    else
                        CheckSumOdd += int.Parse(char.GetNumericValue(s).ToString());

                    Counter++;
                }
            }

            CheckSumTotal = CheckSumeven + CheckSumOdd + 100;

            ReferenceNo = string.Format("{0}{1}{2}{3}{4}", ReferenceNo, Seperator, CheckSumeven.ToString(), CheckSumOdd.ToString(), CheckSumTotal.ToString());

            Encryption sEncryption = new Encryption("XDS.Auth.Reference");
            EncryptedReferenceNo = sEncryption.Encrypt(ReferenceNo);

            return EncryptedReferenceNo;
        }

        public DataSet GetSubscriberAuthenticationprecheckSettings(SqlConnection ObjConstring, int SubscriberID, int ProductID, int AuthenticationProductID)
        {

            XDSSettings oSettings = new XDSSettings();
            string Constr = oSettings.GetConnection(ObjConstring, AuthenticationProductID);
            ObjConstring.ConnectionString = Constr;

            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();

            SqlCommand Objsqlcom = new SqlCommand("spAuthentication_Get_PrecheckSettings", ObjConstring);
            Objsqlcom.CommandType = CommandType.StoredProcedure;
            Objsqlcom.CommandTimeout = 0;
            Objsqlcom.Parameters.AddWithValue("@ProductID", ProductID);
            Objsqlcom.Parameters.AddWithValue("@SubscriberID", SubscriberID);

            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            ObjConstring.Close();

            return ds;
        }

        public XDSPortalLibrary.Entity_Layer.Response GenerateOTP(SqlConnection ObjConstring, XDSPortalLibrary.Entity_Layer.SubscriberAuthentication oSa, XDSPortalEnquiry.Entity.SubscriberEnquiryResult eSc)
        {
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            string OTP = string.Empty;
            try
            {
                string IDNo = string.IsNullOrEmpty(eSc.IDNo) ? eSc.PassportNo : eSc.IDNo, Cstring = string.Empty;
                long NoofTicks = DateTime.Now.Ticks;
                int SubscriberID = oSa.SubscriberID, SubscriberAuthenticationID = oSa.SubscriberAuthenticationID;
                int CheckSumeven = 0, CheckSumOdd = 0, CheckSumTotal = 0, Counter = 0;

                Cstring = IDNo + NoofTicks.ToString() + SubscriberID.ToString() + SubscriberAuthenticationID.ToString();

                byte[] bArray = null;

                if (!(string.IsNullOrEmpty(Cstring)))
                {
                    bArray = Encoding.Unicode.GetBytes(Cstring);

                    foreach (byte s in bArray)
                    {
                        Random R = new Random();

                        CheckSumTotal = Math.Abs(CheckSumTotal + s + R.Next(100000, 999999));
                    }
                }


                OTP = CheckSumTotal.ToString().Substring(0, 6);

                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                rp.ResponseData = OTP;
            }
            catch (Exception ex)
            {
                rp.ResponseData = "Internal Error Generating OTP";
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                AuthenticationError(ObjConstring, oSa.SubscriberID, oSa.SubscriberAuthenticationID, oSa.EnquiryID, oSa.EnquiryResultID, ex.Message, "Generate OTP");
            }

            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response LogOTP(SqlConnection ObjConstring, XDSPortalLibrary.Entity_Layer.SubscriberAuthentication osa, int SMSLogID, string OTP, OTPAction Action)
        {
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                if (ObjConstring.State == ConnectionState.Closed)
                    ObjConstring.Open();

                SqlCommand Objsqlcom = new SqlCommand("spAuthentication_OTPLog", ObjConstring);
                Objsqlcom.CommandType = CommandType.StoredProcedure;
                Objsqlcom.CommandTimeout = 0;
                Objsqlcom.Parameters.AddWithValue("@SubscriberID", osa.SubscriberID);
                Objsqlcom.Parameters.AddWithValue("@SubscriberAuthenticationID", osa.SubscriberAuthenticationID);
                Objsqlcom.Parameters.AddWithValue("@SMSQueueID", SMSLogID);
                Objsqlcom.Parameters.AddWithValue("@OTP", OTP);
                Objsqlcom.Parameters.AddWithValue("@Username", osa.CreatedByUser);
                Objsqlcom.Parameters.AddWithValue("@Action", Action.ToString());
                SqlParameter oparam = new SqlParameter("@LogID", SqlDbType.Int);
                oparam.Direction = ParameterDirection.ReturnValue;

                Objsqlcom.Parameters.Add(oparam);

                SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
                DataSet ds = new DataSet();
                objsqladp.Fill(ds);

                ObjConstring.Close();

                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                rp.ResponseKey = int.Parse(oparam.Value.ToString());
            }
            catch (Exception ex)
            {
                rp.ResponseData = "Internal Error Generating OTP";
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                AuthenticationError(ObjConstring, osa.SubscriberID, osa.SubscriberAuthenticationID, osa.EnquiryID, osa.EnquiryResultID, ex.Message, "LogOTP " + Action.ToString());
            }

            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response GetLatestPhoneNo(SqlConnection ObjConstring, XDSPortalLibrary.Entity_Layer.SubscriberAuthentication oSa)
        {
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                if (ObjConstring.State == ConnectionState.Closed)
                    ObjConstring.Open();

                SqlCommand Objsqlcom = new SqlCommand("spAuthentication_Get_LatestCellNo", ObjConstring);
                Objsqlcom.CommandType = CommandType.StoredProcedure;
                Objsqlcom.CommandTimeout = 0;
                Objsqlcom.Parameters.AddWithValue("@ConsumerID", oSa.ConsumerID);
                Objsqlcom.Parameters.AddWithValue("@InputNumber", oSa.CellularNumber);
                Objsqlcom.Parameters.AddWithValue("@TelephoneTypeInd", "C");
                Objsqlcom.Parameters.AddWithValue("@SubscriberID", oSa.SubscriberID);
                Objsqlcom.Parameters.AddWithValue("@SubscriberAuthenticationID", oSa.SubscriberAuthenticationID);
                SqlParameter oparam = new SqlParameter("@FinalNumber", SqlDbType.VarChar, 20);
                oparam.Direction = ParameterDirection.Output;
                Objsqlcom.Parameters.Add(oparam);

                SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
                DataSet ds = new DataSet();
                objsqladp.Fill(ds);

                ObjConstring.Close();

                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                rp.ResponseData = oparam.Value.ToString();

                if (string.IsNullOrEmpty(rp.ResponseData))
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    rp.ResponseData = "No CellPhone No recorded at Bureau";
                }
            }
            catch (Exception ex)
            {
                rp.ResponseData = "Internal Error Generating OTP";
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                AuthenticationError(ObjConstring, oSa.SubscriberID, oSa.SubscriberAuthenticationID, oSa.EnquiryID, oSa.EnquiryResultID, ex.Message, "GetLatestPhoneNo");
            }

            return rp;
        }

        public string GetSAFPSMessage(SqlConnection ObjConstring, int SubscriberID, int ConsumerID, bool SAFPSFlag)
        {
            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();

            SqlCommand Objsqlcom = new SqlCommand("spAuthentication_GetSAFPSMessage", ObjConstring);
            Objsqlcom.CommandType = CommandType.StoredProcedure;
            Objsqlcom.CommandTimeout = 0;

            Objsqlcom.Parameters.AddWithValue("@SubscriberID", SubscriberID);
            Objsqlcom.Parameters.AddWithValue("@ConsumerID", ConsumerID);
            Objsqlcom.Parameters.AddWithValue("@SAFPSFlag", SAFPSFlag);

            SqlParameter parSAFPSMessage = new SqlParameter("@SAFPSMessage", SqlDbType.VarChar, 500);
            parSAFPSMessage.Direction = ParameterDirection.Output;

            Objsqlcom.Parameters.Add(parSAFPSMessage);

            Objsqlcom.ExecuteNonQuery();

            ObjConstring.Close();

            return parSAFPSMessage.Value.ToString();
        }

        //V2.2
        public DataSet GetSubscriberUnblockReasons(SqlConnection ObjConstring, int SubscriberID)
        {
            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();

            SqlCommand Objsqlcom = new SqlCommand("spAuthentication_Get_UnblockReasons", ObjConstring);
            Objsqlcom.CommandType = CommandType.StoredProcedure;
            Objsqlcom.CommandTimeout = 0;
            Objsqlcom.Parameters.AddWithValue("@SubscriberID", SubscriberID);

            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet("UnblockReasons");
            objsqladp.Fill(ds, "UnblockReason");

            ObjConstring.Close();

            return ds;
        }

        public DataSet GetConsumerInfo(SqlConnection ObjConstring, long ConsumerID)
        {
            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();


            SqlCommand Objsqlcom = new SqlCommand("spAuthentication_GetConsumerInfo", ObjConstring);

            Objsqlcom.CommandType = CommandType.StoredProcedure;
            Objsqlcom.CommandTimeout = 0;

            Objsqlcom.Parameters.AddWithValue("@ConsumerID", ConsumerID);

            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            ObjConstring.Close();

            if (ObjConstring.State == ConnectionState.Open)
                ObjConstring.Close();

            return ds;
        }

        public XDSPortalLibrary.Entity_Layer.Response UpdateConsumerInfo(SqlConnection ObjConstring, XDSPortalLibrary.Entity_Layer.ConsumerInfo oConsumerInfo)
        {
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                if (ObjConstring.State == ConnectionState.Closed)
                    ObjConstring.Open();

                SqlCommand objcmd = new SqlCommand("spAuthentication_UpdateConsumerInfo", ObjConstring);
                objcmd.CommandType = CommandType.StoredProcedure;
                objcmd.CommandTimeout = 0;

                objcmd.Parameters.AddWithValue("@ConsumerID", oConsumerInfo.ConsumerID);
                objcmd.Parameters.AddWithValue("@SubscriberEnquiryID", oConsumerInfo.SubscriberEnquiryID);
                objcmd.Parameters.AddWithValue("@SubscriberEnquiryResultID", oConsumerInfo.SubscriberEnquiryResultID);
                objcmd.Parameters.AddWithValue("@Address1", oConsumerInfo.Address1);
                objcmd.Parameters.AddWithValue("@Address2", oConsumerInfo.Address2);
                objcmd.Parameters.AddWithValue("@Address3", oConsumerInfo.Address3);
                objcmd.Parameters.AddWithValue("@Address4", oConsumerInfo.Address4);
                objcmd.Parameters.AddWithValue("@PostalCode", oConsumerInfo.PostalCode);
                objcmd.Parameters.AddWithValue("@User", oConsumerInfo.User);

                int intRes = objcmd.ExecuteNonQuery();

                if (intRes > 0)
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                else
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
            }
            catch (Exception oException)
            {
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;
            }
            return rp;
        }

        public DataSet GetFraudScore(SqlConnection ObjConstring, int subscriberID, int ConsumerID, int EnquiryID, int EnquiryResultID,
            int ProductID, int ReportID, string CreatedbyUser, string AssociationTypeCode, string SubscriberAssociationCode, string Store, string Title, string Channel, decimal Salary, int MonthsEmployed)
        {
            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();

            SqlCommand Objsqlcom = new SqlCommand("spFraudScoring", ObjConstring);
            Objsqlcom.CommandType = CommandType.StoredProcedure;
            Objsqlcom.CommandTimeout = 0;
            Objsqlcom.Parameters.AddWithValue("ConsumerID", ConsumerID);
            Objsqlcom.Parameters.AddWithValue("SubscriberID", subscriberID);
            Objsqlcom.Parameters.AddWithValue("@EnquiryID", EnquiryID);
            Objsqlcom.Parameters.AddWithValue("@EnquiryResultID", EnquiryResultID);
            Objsqlcom.Parameters.AddWithValue("@ProductID", ProductID);
            Objsqlcom.Parameters.AddWithValue("@ReportID", ReportID);
            Objsqlcom.Parameters.AddWithValue("@UserName", CreatedbyUser);
            Objsqlcom.Parameters.AddWithValue("@AssociationTypeCode", AssociationTypeCode);
            Objsqlcom.Parameters.AddWithValue("@SubscriberAssociationCode", SubscriberAssociationCode);
            Objsqlcom.Parameters.AddWithValue("@Store", Store);
            Objsqlcom.Parameters.AddWithValue("@Title", Title);
            Objsqlcom.Parameters.AddWithValue("@Salary", Salary);
            Objsqlcom.Parameters.AddWithValue("@Channel", Channel);
            Objsqlcom.Parameters.AddWithValue("@MonthsEmployed", MonthsEmployed);

            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet("FraudScore");
            objsqladp.Fill(ds, "ScoreDetails");

            ObjConstring.Close();

            if (ObjConstring.State == ConnectionState.Open)
                ObjConstring.Close();

            return ds;
        }

        public void UpdateSubscriberAuthenticationOTPRetryCount(SqlConnection ObjConstring, XDSPortalLibrary.Entity_Layer.SubscriberAuthentication oSa)
        {

            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();

            SqlCommand Objsqlcom = new SqlCommand("spAuthentication_U_SubscriberAuthentication_OTP", ObjConstring);
            Objsqlcom.CommandType = CommandType.StoredProcedure;
            Objsqlcom.CommandTimeout = 0;

            Objsqlcom.Parameters.AddWithValue("@SubscriberID", oSa.SubscriberID);
            Objsqlcom.Parameters.AddWithValue("@SubscriberAuthenticationID", oSa.SubscriberAuthenticationID);
            Objsqlcom.Parameters.AddWithValue("@Username", oSa.CreatedByUser);
            Objsqlcom.Parameters.AddWithValue("@OTPRetryCount", oSa.OTPRetryCount);

            Objsqlcom.ExecuteNonQuery();


            ObjConstring.Close();
        }

        public XDSPortalLibrary.Entity_Layer.Response BlockUnblockConsumerV22(SqlConnection ObjConstring, XDSPortalLibrary.Entity_Layer.BlockedConsumers obConsumer)
        {
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            int iMode = 101;
            if (obConsumer.BlockID > 0)
                iMode = 102;

            try
            {
                if (ObjConstring.State == ConnectionState.Closed)
                    ObjConstring.Open();

                SqlCommand objcmd = new SqlCommand("USP_IU_BlockedConsumers_V22", ObjConstring);
                objcmd.CommandType = CommandType.StoredProcedure;
                objcmd.CommandTimeout = 0;

                objcmd.Parameters.AddWithValue("@inMode", iMode);
                objcmd.Parameters.AddWithValue("@inBlockID", obConsumer.BlockID);
                objcmd.Parameters.AddWithValue("@inConsumerID", obConsumer.ConsumerID);
                objcmd.Parameters.AddWithValue("@ivIDNo", obConsumer.IDNo);
                objcmd.Parameters.AddWithValue("@ivPassportNo", obConsumer.PassportNo);
                objcmd.Parameters.AddWithValue("@ivSurname", obConsumer.SurName);
                objcmd.Parameters.AddWithValue("@ivFirstName", obConsumer.FirstName);
                objcmd.Parameters.AddWithValue("@ivSecondName", obConsumer.SecondName);
                objcmd.Parameters.AddWithValue("@idtBirthDate", obConsumer.DateofBirth);
                objcmd.Parameters.AddWithValue("@inSystemUserID", obConsumer.SystemUserID);
                objcmd.Parameters.AddWithValue("@inSubscriberID", obConsumer.SubscriberID);
                objcmd.Parameters.AddWithValue("@inSubscriberEnquiryID", obConsumer.SubscriberEnquiryID);
                objcmd.Parameters.AddWithValue("@inSubscriberEnquiryResultID", obConsumer.SubscriberEnquiryResultID);
                objcmd.Parameters.AddWithValue("@inProductID", obConsumer.ProductID);
                objcmd.Parameters.AddWithValue("@ibIsBlocked", obConsumer.IsBlocked);
                objcmd.Parameters.AddWithValue("@ivBlockedByUser", obConsumer.BlockedByUser);
                objcmd.Parameters.AddWithValue("@ivUnblockedByUser", obConsumer.UnblockedByUser);
                objcmd.Parameters.AddWithValue("@ivBlockingReason", obConsumer.BlockingReason);
                objcmd.Parameters.AddWithValue("@ivUnblockingReason", obConsumer.UnblockingReason);
                objcmd.Parameters.AddWithValue("@ivSubscriberName", obConsumer.SubscriberName);

                SqlParameter osqlparameter = new SqlParameter("BlockID", SqlDbType.Int);
                osqlparameter.Direction = ParameterDirection.ReturnValue;
                objcmd.Parameters.Add(osqlparameter);

                objcmd.ExecuteNonQuery();

                obConsumer.BlockID = int.Parse(osqlparameter.Value.ToString());

                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                rp.ResponseKey = obConsumer.BlockID;
            }
            catch (Exception oException)
            {
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;
            }
            return rp;
        }

        public XDSPortalLibrary.Entity_Layer.Response GetBlockedConsumersbyID(SqlConnection ObjConstring, int SubscriberID, string IDno)
        {
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                //if (ObjConstring.State == ConnectionState.Closed)
                //    ObjConstring.Open();

                Dictionary<string, string> oparametercollection = new Dictionary<string, string>();

                oparametercollection.Add("@SubscriberID", SubscriberID.ToString());
                oparametercollection.Add("@IDno", IDno);
                
                

                ThreadProcess threadProcess = new ThreadProcess();

                DataSet dsBC = threadProcess.Processthreaddataset(ObjConstring, XDSPortalEnquiry.Entity.SystemName.authSyncserver, "spAuthentication_Get_BlockedConsumers_ID", CommandType.StoredProcedure, oparametercollection, true, "BlockID", 0, true, new string[] { "BlockID","ConsumerID","IDNo","PassportNo","Surname","FirstName","SecondName","BirthDate" ,"BlockingReason","BlockedDate" }, ThreadProcess.executiontype.filladapter);

                dsBC.DataSetName = "BlockedConsumer";
                if (dsBC.Tables.Count > 0)
                {
                    dsBC.Tables[0].TableName = "Consumer";
                }

                rp.ResponseKey = 0;
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                rp.ResponseData = dsBC.GetXml();

                if (ObjConstring.State == ConnectionState.Open)
                    ObjConstring.Close();

                dsBC.Dispose();

            }
            catch (Exception oException)
            {
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = "<Error>" + oException.Message + "</Error>";
            }
            return rp;
        }

    }
}
