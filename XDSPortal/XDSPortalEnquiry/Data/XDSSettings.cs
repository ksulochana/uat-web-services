﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using XDSPortalEnquiry.Entity;

namespace XDSPortalEnquiry.Data
{
    public class XDSSettings
    {
        public string GetConnection(SqlConnection AdminCon, int productID)
        {
            string Connectionstring = string.Empty;

            if (AdminCon.State == ConnectionState.Closed)
                AdminCon.Open();

            string strSQL = "SELECT Connectionstring from XDS_Settings_ConnectionString nolock Where productID = " + productID.ToString() + " and isnull(Active,0) = 1";
            SqlCommand Imycommand = new SqlCommand(strSQL, AdminCon);
            SqlDataReader Idr = Imycommand.ExecuteReader();
            Idr.Read();
            Connectionstring = Idr.GetValue(0).ToString();
            Idr.Close();


            if (AdminCon.State == ConnectionState.Open)
                AdminCon.Close();

            return Connectionstring;
        }

		public DOVSetting GetDOVSSettings(SqlConnection AdminCon, int SubscriberID)
		{
			try
			{
				DOVSetting dOVSetting = new DOVSetting();
				string strSQL = "SELECT Username, Password, Institution FROM [CDO_DOVS_Settings] WHERE SubscriberID = " + SubscriberID;

				if (AdminCon.State == ConnectionState.Closed)
					AdminCon.Open();

				SqlCommand sqlcmd = new SqlCommand(strSQL, AdminCon);

				using (SqlDataReader oReader = sqlcmd.ExecuteReader())
				{
					while (oReader.Read())
					{
						dOVSetting.Username = oReader.GetString(0);
						dOVSetting.Password = oReader.GetString(1);
						dOVSetting.Institution = oReader.GetString(2);
					}
				}

				if (AdminCon.State == ConnectionState.Open)
					AdminCon.Close();

				return dOVSetting;
			}
			catch (Exception)
			{
				throw;
			}
		}

        public Entity.SanctionsConfigSettings GetSanctionStandardSettings(SqlConnection AdminCon)
        {
            try
            {
                Entity.SanctionsConfigSettings objSettings = new Entity.SanctionsConfigSettings();

                if (AdminCon.State == ConnectionState.Closed)
                    AdminCon.Open();

                string sqlSelect = "SELECT MemberKey, Password, URL1, URL2 FROM SanctionsConfigSettings (NOLOCK) WHERE Id = 1";

                SqlCommand Objsqlcom = new SqlCommand(sqlSelect, AdminCon);
                SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
                DataSet ds = new DataSet();
                objsqladp.Fill(ds);

                foreach (DataRow Dr in ds.Tables[0].Rows)
                {
                    objSettings.MemberKey = Dr["MemberKey"].ToString();
                    objSettings.Password = Dr["Password"].ToString();
                    objSettings.URL1 = Dr["URL1"].ToString();
                    objSettings.URL2 = Dr["URL2"].ToString();
                }

                ds.Dispose();
                Objsqlcom.Dispose();
                objsqladp.Dispose();
                AdminCon.Close();

                SqlConnection.ClearPool(AdminCon);

                return objSettings;
            }
            catch (Exception)
            {

                throw;
            }

        }
    }
}
