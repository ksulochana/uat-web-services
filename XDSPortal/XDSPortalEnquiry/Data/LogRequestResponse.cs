﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace XDSPortalEnquiry.Data
{
    public class LogRequestResponse
    {
        readonly SqlConnection _sqlConnection;
        public LogRequestResponse(SqlConnection sqlConnection)
        {
            _sqlConnection = sqlConnection;
        }
        public void InsertLog(string request, string response, string user)
        {
            SqlCommand cmd = new SqlCommand("Insert into CustomVettingLog (Request,Response,CreatedOnDate,Createdbyuser) values(@Request,@Response,getdate(),@User)", _sqlConnection);
            cmd.CommandType = System.Data.CommandType.Text;

            cmd.Parameters.AddWithValue("@Request", request);
            cmd.Parameters.AddWithValue("@Response", response);
            cmd.Parameters.AddWithValue("@User", user);

            if (_sqlConnection.State == ConnectionState.Closed)
                _sqlConnection.Open();

            cmd.ExecuteNonQuery();

           _sqlConnection.Close();
        }
    }
}
