﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.IO;

namespace XDSPortalEnquiry.Data
{
    public class Subscriber
    {
        public Subscriber()
        {
        }
        public void Checkblockrestriction(SqlConnection conString, int SubscriberID, int ProductID, out int EnquiryLimit, out bool EnquiryLimitcheckenabled)
        {
            if (conString.State == ConnectionState.Closed)
                conString.Open();

            SqlCommand Objsqlcom = new SqlCommand("spUser_check_blockrestrictions_WS", conString);
            Objsqlcom.CommandType = CommandType.StoredProcedure;
            Objsqlcom.CommandTimeout = 0;

            Objsqlcom.Parameters.AddWithValue("@SubscriberID", SubscriberID);
            Objsqlcom.Parameters.AddWithValue("@ProductID", ProductID);

            SqlParameter enqlimitparam = new SqlParameter("@EnquiryLimit", SqlDbType.Int);
            enqlimitparam.Direction = ParameterDirection.Output;

            SqlParameter EnquiryLimitcheckenabledparam = new SqlParameter("@EnquiryLimitcheckenabled", SqlDbType.Bit);
            EnquiryLimitcheckenabledparam.Direction = ParameterDirection.Output;

            Objsqlcom.Parameters.Add(enqlimitparam);
            Objsqlcom.Parameters.Add(EnquiryLimitcheckenabledparam);

            Objsqlcom.ExecuteNonQuery();

            EnquiryLimit = int.Parse(enqlimitparam.Value.ToString());
            EnquiryLimitcheckenabled = bool.Parse(EnquiryLimitcheckenabledparam.Value.ToString());

            if (conString.State == ConnectionState.Open)
                conString.Close();

            Objsqlcom.Dispose();

            SqlConnection.ClearPool(conString);
        }
        public XDSPortalLibrary.Entity_Layer.Response GetUserBlockingStatus(SqlConnection EnquiryConnection, SqlConnection AdminConnection, XDSPortalEnquiry.Entity.Subscriber oSubscriber, XDSPortalEnquiry.Entity.SystemUser oSystemuser, int ProductID)
        {
            XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
            int enquiryLimit = 0;
            bool EnquiryLimitcheckenabled = false;
            DateTime LastunblockedtimeonEnquiryLimit = DateTime.Now;
            bool triggerEnquiryLimitalert = false;

            try
            {
                Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();

                odSubscriber.Checkblockrestriction(AdminConnection, oSystemuser.SubscriberID, ProductID, out enquiryLimit, out EnquiryLimitcheckenabled);

                rp.ResponseReferenceNo = enquiryLimit.ToString();

                XDSPortalEnquiry.Data.SubscriberEnquiry oEnquiry = new XDSPortalEnquiry.Data.SubscriberEnquiry();

                if (EnquiryLimitcheckenabled)
                {
                    triggerEnquiryLimitalert = oEnquiry.CheckEnquiryLimit(EnquiryConnection, oSystemuser.SubscriberID, ProductID, enquiryLimit);
                    if (triggerEnquiryLimitalert)
                    {
                        rp.ResponseData = "Number of transactions exceed client threshold. Please contact XDS";
                        rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                        rp.ResponseKeyType = "ELM04";
                    }
                }
                else
                {
                    rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                }
            }
            catch (Exception oException)
            {
                rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                rp.ResponseData = oException.Message;

            }

            return rp;

        }

        public Entity.Subscriber GetSubscriberRecord(SqlConnection conString, int SubscriberID)
        {
            Entity.Subscriber objSubscriber = new Entity.Subscriber();

            //SqlConnection ObjConstring = new SqlConnection(conString);
            //ObjConstring.Open();

            if (conString.State == ConnectionState.Closed)

                conString.Open();

            string sqlSelect = "SELECT   Subscriber.* , SubscriberBusinessTypeDesc AS SubscriberBusinessType, SubscriberIndustryDesc AS Industry  FROM Subscriber (NOLOCK) LEFT OUTER JOIN SubscriberBusinessType (NOLOCK) ON Subscriber.SubscriberBusinessTypeCode = SubscriberBusinessType.SubscriberBusinessTypeCode LEFT OUTER JOIN SubscriberIndustry (NOLOCK) ON Subscriber.SubscriberIndustryID = SubscriberIndustry.SubscriberIndustryID where SubscriberID = " + SubscriberID;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, conString);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objSubscriber.SubscriberID = Convert.ToInt32(Dr["SubscriberID"]);
                objSubscriber.SubscriberName = Dr["SubscriberName"].ToString();
                objSubscriber.SubscriberBusinessType = Dr["SubscriberBusinessType"].ToString();
                objSubscriber.SubscriberTypeInd = Dr["SubscriberTypeInd"].ToString();
                objSubscriber.StatusInd = Dr["StatusInd"].ToString();
                if (Convert.ToBoolean(Dr["PayAsYouGo"].ToString())) { objSubscriber.PayAsYouGo = 1; } else {objSubscriber.PayAsYouGo = 0;}
                objSubscriber.PayAsyouGoEnquiryLimit = Convert.ToDouble(Dr["PayAsYouGoEnquiryLimit"].ToString());
                if (!string.IsNullOrEmpty(Dr["CompanyTelephoneCode"].ToString())) { objSubscriber.CompanyTelephoneCode = Dr["CompanyTelephoneCode"].ToString(); }
                if (!string.IsNullOrEmpty(Dr["CompanyTelephoneNo"].ToString())) { objSubscriber.CompanyTelephoneNo = Dr["CompanyTelephoneNo"].ToString(); }
                if (!string.IsNullOrEmpty(Dr["SubscriberBusinessTypeCode"].ToString())) { objSubscriber.SubscriberBusinessTypeCode = Dr["SubscriberBusinessTypeCode"].ToString(); }
                if (!string.IsNullOrEmpty(Dr["SubscriberAssociationCode"].ToString())) { objSubscriber.SubscriberAssociationCode = Dr["SubscriberAssociationCode"].ToString(); }
                if (!string.IsNullOrEmpty(Dr["AssociationTypeCode"].ToString())) { objSubscriber.AssociationTypeCode = Dr["AssociationTypeCode"].ToString(); }
                if (!string.IsNullOrEmpty(Dr["SubscriberGroupCode"].ToString())) { objSubscriber.SubscriberGroupCode = Dr["SubscriberGroupCode"].ToString(); }
                if (!string.IsNullOrEmpty(Dr["SAFPSEnabled"].ToString()))
                {
                    bool SAFPSInd = false;
                    if (bool.TryParse(Dr["SAFPSEnabled"].ToString(), out SAFPSInd))
                    {
                        objSubscriber.SAFPSIndicator = SAFPSInd;
                    }
                }
                if (!string.IsNullOrEmpty(Dr["Level5SubscriberID"].ToString()))
                {
                    bool NewAccTypeInd = false;
                    int newAccType = Convert.ToInt32(Dr["Level5SubscriberID"]);

                    if (newAccType == 1)
                        NewAccTypeInd = true;

                    objSubscriber.NewAccountTypeIndicator = NewAccTypeInd;
                }
                if (!string.IsNullOrEmpty(Dr["Level4SubscriberID"].ToString()))
                {
                    bool DisplayInd = false;
                    int display = Convert.ToInt32(Dr["Level4SubscriberID"]);

                    if (display == 1)
                        DisplayInd = true;

                    objSubscriber.DisplayUnusableInformation = DisplayInd;
                }
                if (!string.IsNullOrEmpty(Dr["Level5Desc"].ToString()))
                {
                    bool boverride = false;
                    int ioverride = Convert.ToInt32(Dr["Level5Desc"]);

                    if (ioverride == 1)
                        boverride = true;

                    objSubscriber.RealTimeIDVOverride = boverride;
                }
                if (!string.IsNullOrEmpty(Dr["Industry"].ToString())) { objSubscriber.Industry = Dr["Industry"].ToString(); }
                if (ds.Tables[0].Columns.Contains("EnablePhotoFailover"))
                {
                    objSubscriber.EnablePhotoFailover = (Dr["EnablePhotoFailover"] == DBNull.Value ? false : (Dr["EnablePhotoFailover"].ToString() == string.Empty ? false : bool.Parse(Dr["EnablePhotoFailover"].ToString())));
                }
                if (ds.Tables[0].Columns.Contains("SanctionsMinScore"))
                {
                    objSubscriber.SanctionsMinScore = (Dr["SanctionsMinScore"] == DBNull.Value ? 0 : (Dr["SanctionsMinScore"].ToString() == string.Empty ? 0 : int.Parse(Dr["SanctionsMinScore"].ToString())));
                }
            }
            //ObjConstring.Close();

            ds.Dispose();
            Objsqlcom.Dispose();
            objsqladp.Dispose();
            conString.Close();

            SqlConnection.ClearPool(conString);

            return objSubscriber;
       
        }

        public Entity.Subscriber GetSubscriberRecord(string conString, int SubscriberID)
        {
            Entity.Subscriber objSubscriber = new Entity.Subscriber();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = "SELECT   Subscriber.* , SubscriberBusinessTypeDesc AS SubscriberBusinessType  FROM Subscriber (NOLOCK) LEFT OUTER JOIN SubscriberBusinessType (NOLOCK) ON Subscriber.SubscriberBusinessTypeCode = SubscriberBusinessType.SubscriberBusinessTypeCode  where SubscriberID = " + SubscriberID;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objSubscriber.SubscriberID = Convert.ToInt32(Dr["SubscriberID"]);
                objSubscriber.SubscriberName = Dr["SubscriberName"].ToString();
                objSubscriber.SubscriberBusinessType = Dr["SubscriberBusinessType"].ToString();
                objSubscriber.SubscriberTypeInd = Dr["SubscriberTypeInd"].ToString();
                objSubscriber.StatusInd = Dr["StatusInd"].ToString();
                if (Convert.ToBoolean(Dr["PayAsYouGo"].ToString())) { objSubscriber.PayAsYouGo = 1; } else { objSubscriber.PayAsYouGo = 0; }
                objSubscriber.PayAsyouGoEnquiryLimit = Convert.ToDouble(Dr["PayAsYouGoEnquiryLimit"].ToString());
                if (!string.IsNullOrEmpty(Dr["CompanyTelephoneCode"].ToString())) { objSubscriber.CompanyTelephoneCode = Dr["CompanyTelephoneCode"].ToString(); }
                if (!string.IsNullOrEmpty(Dr["CompanyTelephoneNo"].ToString())) { objSubscriber.CompanyTelephoneNo = Dr["CompanyTelephoneNo"].ToString(); }
                if (!string.IsNullOrEmpty(Dr["SubscriberBusinessTypeCode"].ToString())) { objSubscriber.SubscriberBusinessTypeCode = Dr["SubscriberBusinessTypeCode"].ToString(); }
                if (!string.IsNullOrEmpty(Dr["SubscriberAssociationCode"].ToString())) { objSubscriber.SubscriberAssociationCode = Dr["SubscriberAssociationCode"].ToString(); }
                 { objSubscriber.Level1Desc =  Dr["Level1Desc"]!=null ? Dr["Level1Desc"].ToString():string.Empty ; }
                if (!string.IsNullOrEmpty(Dr["SAFPSEnabled"].ToString()))
                {
                    bool SAFPSInd = false;
                    if (bool.TryParse(Dr["SAFPSEnabled"].ToString(), out SAFPSInd))
                    {
                        objSubscriber.SAFPSIndicator = SAFPSInd;
                    }
                }
                if (!string.IsNullOrEmpty(Dr["Level5SubscriberID"].ToString()))
                {
                    bool NewAccTypeInd = false;
                    int newAccType = Convert.ToInt32(Dr["Level5SubscriberID"]);

                    if (newAccType == 1)
                        NewAccTypeInd = true;

                    objSubscriber.NewAccountTypeIndicator = NewAccTypeInd;
                }
                if (!string.IsNullOrEmpty(Dr["Level4SubscriberID"].ToString()))
                {
                    bool DisplayInd = false;
                    int display = Convert.ToInt32(Dr["Level4SubscriberID"]);

                    if (display == 1)
                        DisplayInd = true;

                    objSubscriber.DisplayUnusableInformation = DisplayInd;
                }
                if (!string.IsNullOrEmpty(Dr["Level5Desc"].ToString()))
                {
                    bool boverride = false;
                    int ioverride = Convert.ToInt32(Dr["Level5Desc"]);

                    if (ioverride == 1)
                        boverride = true;

                    objSubscriber.RealTimeIDVOverride = boverride;
                }
            }
            //ObjConstring.Close();
            ds.Dispose();
            Objsqlcom.Dispose();
            objsqladp.Dispose();
            ObjConstring.Close();
            SqlConnection.ClearPool(ObjConstring);
            return objSubscriber;

        }

        public DataSet GetSubscriberProfile(string conString, int SubscriberID)
        {
            Entity.Subscriber objSubscriber = new Entity.Subscriber();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            //string sqlSelect = "SELECT SubscriberID, AuthenticationSearchOnIDNoYN, AuthenticationSearchOnCellPhoneNoYN, AuthenticationSearchOnAccountNoYN from SubscriberProfile (NOLOCK) where SubscriberID = " + SubscriberID;
            string sqlSelect = "spAuthentication_Get_SubscriberProfile";

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            Objsqlcom.CommandType = CommandType.StoredProcedure;
            Objsqlcom.Parameters.AddWithValue("@SubscriberID",SubscriberID);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            
            //ObjConstring.Close();
            ds.Dispose();
            Objsqlcom.Dispose();
            objsqladp.Dispose();
            ObjConstring.Close();

            return ds;

        }

        public DataSet GetBranches(string conString, int SubscriberID)
        {
            XDSSettings oXDSSettings = new XDSSettings();
            string AdminConnectionString = string.Empty;
           
            
            SqlConnection ObjConstring = new SqlConnection(conString);

            AdminConnectionString = oXDSSettings.GetConnection(ObjConstring, 1);

            SqlConnection oAuthCon = new SqlConnection(AdminConnectionString);

            if (oAuthCon.State == ConnectionState.Closed)
                oAuthCon.Open();

//            string sqlSelect = "SELECT SubscriberID, SubscriberBranchCode as BranchCode, SubscriberBranchDesc as BranchName from SubscriberProfileProductAuthenticationBranch (NOLOCK) where SubscriberID = " + SubscriberID;

            string sqlSelect = "SELECT SubscriberID, SubscriberBranchCode as BranchCode, SubscriberBranchDesc as BranchName from Auth_S_Branch (NOLOCK) where SubscriberID = " + SubscriberID + " and Statusind ='A'";

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, oAuthCon);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            if (oAuthCon.State == ConnectionState.Open)
                oAuthCon.Close();

            //ObjConstring.Close();
            ds.Dispose();
            Objsqlcom.Dispose();
            objsqladp.Dispose();
            ObjConstring.Close();

            return ds;

        }

        public DataSet GetPurposes(string conString, int SubscriberID)
        {
            XDSSettings oXDSSettings = new XDSSettings();
            string AdminConnectionString = string.Empty;
            
            SqlConnection ObjConstring = new SqlConnection(conString);
            AdminConnectionString = oXDSSettings.GetConnection(ObjConstring, 1);

            SqlConnection oAuthCon = new SqlConnection(AdminConnectionString);

            if (oAuthCon.State == ConnectionState.Closed)
                oAuthCon.Open();

//            string sqlSelect = "SELECT SubscriberID, AuthenticationPurposeID as PurposeID, AuthenticationPurposeDesc as Purpose from SubscriberProfileProductAuthenticationPurpose (NOLOCK) where SubscriberID = " + SubscriberID;

            string sqlSelect = "SELECT SubscriberID, AuthenticationPurposeID as PurposeID, AuthenticationPurposeDesc as Purpose from Auth_S_PurPose (NOLOCK) where SubscriberID = " + SubscriberID + " and StatusInd = 'A'";

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, oAuthCon);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            if (oAuthCon.State == ConnectionState.Open)
                oAuthCon.Close();

            //ObjConstring.Close();
            ds.Dispose();
            Objsqlcom.Dispose();
            objsqladp.Dispose();
            ObjConstring.Close();

            return ds;

        }

        public List<Entity.Subscriber> GetSubscriberRecord(string conString, string whereclause)
        {
            List<Entity.Subscriber> ObjSubscriberList = new List<Entity.Subscriber>();
            Entity.Subscriber objSubscriber = new Entity.Subscriber();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = "select * from Subscriber " + whereclause;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objSubscriber.SubscriberID = Convert.ToInt32(Dr["SubscriberID"]);
                objSubscriber.SubscriberName = Dr["SubscriberName"].ToString();
                objSubscriber.SubscriberBusinessType = Dr["SubscriberBusinessType"].ToString();
                objSubscriber.SubscriberTypeInd = Dr["SubscriberTypeInd"].ToString();
                objSubscriber.StatusInd = Dr["StatusInd"].ToString();
                if (!string.IsNullOrEmpty(Dr["CompanyTelephoneCode"].ToString())) { objSubscriber.CompanyTelephoneCode = Dr["CompanyTelephoneCode"].ToString(); }
                if (!string.IsNullOrEmpty(Dr["CompanyTelephoneNo"].ToString())) { objSubscriber.CompanyTelephoneNo = Dr["CompanyTelephoneNo"].ToString(); }
                if (!string.IsNullOrEmpty(Dr["SubscriberBusinessTypeCode"].ToString())) { objSubscriber.SubscriberBusinessTypeCode = Dr["SubscriberBusinessTypeCode"].ToString(); }
                if (!string.IsNullOrEmpty(Dr["SubscriberAssociationCode"].ToString())) { objSubscriber.SubscriberAssociationCode = Dr["SubscriberAssociationCode"].ToString(); }

                ObjSubscriberList.Add(objSubscriber);
            }

            ObjConstring.Close();
            ds.Dispose();
            Objsqlcom.Dispose();
            objsqladp.Dispose();
            return ObjSubscriberList;
        }

        public XDSPortalEnquiry.Entity.Subscriber GetPayAsYouGoEnquiryLimit(string conString, int SubscriberID)
        {
            XDSPortalEnquiry.Entity.Subscriber objSubscriberPayAsYouGo = new XDSPortalEnquiry.Entity.Subscriber();
            SqlConnection objConstring = new SqlConnection(conString);
            string sqlSelect = "select SubscriberID,PayAsYouGo,PayAsYouGoEnquiryLimit from Subscriber nolock where SubscriberID = " + SubscriberID;
            objConstring.Open();

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, objConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objSubscriberPayAsYouGo.SubscriberID = Convert.ToInt32(Dr["SubscriberID"]);
                objSubscriberPayAsYouGo.PayAsYouGo = Convert.ToInt32(Dr["PayAsYouGo"]);
                objSubscriberPayAsYouGo.PayAsyouGoEnquiryLimit = Convert.ToDouble(Dr["PayAsYouGoEnquiryLimit"]);

            }
            objConstring.Close();
            ds.Dispose();
            Objsqlcom.Dispose();
            objsqladp.Dispose();
            return objSubscriberPayAsYouGo;
        }

        public int UpdatePayAsYouGoEnquiryLimit(string conString, int SubscriberID, double unitPrice)
        {
            int Status = 0;
            SqlConnection objConstring = new SqlConnection(conString);
            string sqlSelect = "Exec sp_u_Subscriber " + SubscriberID + "," + unitPrice;
            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, objConstring);
            objConstring.Open();
            Status = Objsqlcom.ExecuteNonQuery();
            objConstring.Close();

            Objsqlcom.Dispose();

            return Status;
        }

        public int UpdatePayAsYouGoEnquiryLimit(SqlConnection objConstring, int SubscriberID, double unitPrice)
        {

            if (objConstring.State == ConnectionState.Closed)
                objConstring.Open();
            

            int Status = 0;
           
            string sqlSelect = "Exec sp_u_Subscriber " + SubscriberID + "," + unitPrice;
            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, objConstring);
                        
            Status = Objsqlcom.ExecuteNonQuery();
            Objsqlcom.Dispose();
            objConstring.Close();
            return Status;
        }

        public int GetAccountVerificationProductID(string conString, int SubscriberID)
        {
            SqlConnection objConstring = new SqlConnection(conString);

            if (objConstring.State == ConnectionState.Closed)
                objConstring.Open();


            int avsProductID = 0;

            string sqlSelect = "Select ProductID from SubscriberProductReports nolock Where SubscriberID = " + SubscriberID + " and Active = 1 and ProductID in (45,84)";
            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, objConstring);
             
            SqlDataReader oReader = Objsqlcom.ExecuteReader();

            while(oReader.Read())
                avsProductID = oReader.GetInt32(0);

            oReader.Close();
            oReader.Dispose();
            Objsqlcom.Dispose();
            objConstring.Close();
            return avsProductID;
        }

        public DataSet GetSubscriberProductList(SqlConnection objConstring, int SubscriberID)
        { 
            if (objConstring.State == ConnectionState.Closed)
                objConstring.Open();

            string sqlSelect = "select a.ProductID,b.ReportID,a.ProductDesc as ProductDescription from Product a (nolock) inner join SubscriberProductReports b (nolock) on a.ProductID = b.ProductID where b.SubscriberID = " + SubscriberID + " and b.Active = 1";

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, objConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet("Products");
            objsqladp.Fill(ds,"ProductItem");

            if (objConstring.State == ConnectionState.Open)
                objConstring.Close();

            ds.Dispose();
            Objsqlcom.Dispose();
            objsqladp.Dispose();
            objConstring.Close();

            return ds;
        }

        public DataSet GetSubscriberAuthenticationProfile(string conString, int SubscriberID)
        {
            Entity.Subscriber objSubscriber = new Entity.Subscriber();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = "spAuthentication_Get_SubscriberAuthenticationProfile";

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            Objsqlcom.CommandType = CommandType.StoredProcedure;
            Objsqlcom.Parameters.AddWithValue("@SubscriberID", SubscriberID);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            ds.Dispose();
            Objsqlcom.Dispose();
            objsqladp.Dispose();
            ObjConstring.Close();

            return ds;
        }

        public DataSet GetSubscriberMenu(string conString, int SubscriberID, int SystemuserID)
        {
            

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            //SqlConnection ObjConstring = new SqlConnection(conString);
            //ObjConstring.Open();


            string sqlSelect = " Execute spGetSubscriberMenuInfo " + SubscriberID.ToString() + " ," + SystemuserID.ToString();

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

          

            ds.Dispose();
            Objsqlcom.Dispose();
            objsqladp.Dispose();
            ObjConstring.Close();

            SqlConnection.ClearPool(ObjConstring);

            return ds;

        }

        public bool CheckIPAllowed(SqlConnection conString, int SubscriberID, int SystemuserID,string IPAddress)
        {
            bool ipallowed = false;


            if (conString.State == ConnectionState.Closed)
                conString.Open();
           // File.AppendAllText(@"C:\Log\IPcheck.txt", conString.ConnectionString.ToString());

            SqlCommand Objsqlcom = new SqlCommand("spLogin_IPCheck", conString);
            Objsqlcom.CommandType = CommandType.StoredProcedure;

            Objsqlcom.Parameters.AddWithValue("@SubscriberID", SubscriberID);
            Objsqlcom.Parameters.AddWithValue("@SystemuserID", SystemuserID);
            Objsqlcom.Parameters.AddWithValue("@IPAddress", IPAddress);

            SqlParameter ipallowedparam = new SqlParameter("@IPAllowed", SqlDbType.Bit);
            ipallowedparam.Direction = ParameterDirection.Output;

            Objsqlcom.Parameters.Add(ipallowedparam);

            Objsqlcom.ExecuteNonQuery();


            

            ipallowed = bool.Parse(ipallowedparam.Value.ToString());

            if (conString.State == ConnectionState.Open)
                conString.Close();

            Objsqlcom.Dispose();            

            SqlConnection.ClearPool(conString);

            return ipallowed;

        }

        public DataSet IsProductIDActive(SqlConnection objConstring, int SubscriberID, int ProductID)
        {
            if (objConstring.State == ConnectionState.Closed)
                objConstring.Open();

            string sqlSelect = "SELECT a.ProductID,b.ReportID,a.ProductDesc as ProductDescription from Product a (nolock) inner join SubscriberProductReports b (nolock) on a.ProductID = b.ProductID where b.SubscriberID = " + SubscriberID + " and b.ProductID= " + ProductID + " and b.Active = 1";

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, objConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet("Products");
            objsqladp.Fill(ds, "ProductItem");

            if (objConstring.State == ConnectionState.Open)
                objConstring.Close();

            ds.Dispose();
            Objsqlcom.Dispose();
            objsqladp.Dispose();
            objConstring.Close();

            return ds;
        }


        public string GetPressageVersion(SqlConnection conString, int SubscriberID)
        {
            string PressageScoreVersion = string.Empty;

            //SqlConnection ObjConstring = new SqlConnection(conString);
            //ObjConstring.Open();

            if (conString.State == ConnectionState.Closed)

                conString.Open();

            string sqlSelect = "Select  ISNULL([Description],'') Description from dbo.SubscriberProfile a inner join XX_ADMINDB.dbo.LookUpPresageScore b on a.PresageScoreID = b.LookUpPresageScoreID where a.SubscriberID = " + SubscriberID;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, conString);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                PressageScoreVersion = Dr["Description"].ToString();

            }
            //ObjConstring.Close();

            ds.Dispose();
            Objsqlcom.Dispose();
            objsqladp.Dispose();
            conString.Close();

            SqlConnection.ClearPool(conString);

            return PressageScoreVersion;

        }
    }
}

