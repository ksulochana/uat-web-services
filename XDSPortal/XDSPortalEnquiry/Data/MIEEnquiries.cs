﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Xml;

namespace XDSPortalEnquiry.Data
{
    public class MIEEnquiries
    {

        public DataSet GetMieTypeCodes(SqlConnection AdminConnection, int SubscriberID)
        {
            AdminConnection.Open();

            string strSQL = "select Code,Name,CategoryKey,TypeKey,CopyAskType,AllowMultiple,IndemnityType,IndemnityText, IndemnityPrompt from MieTypes where subscriberID  =" + SubscriberID;
            SqlCommand sqlcmd = new SqlCommand(strSQL, AdminConnection);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet("XDSTypeCodes");
            sqlda.Fill(ds, "TypeCode");

            AdminConnection.Close();

            return ds;

        }

        public string PutMieEnquiry(string FirstName, string SecondName, string Surname, string MaidenName, string IdNumber, string Passport, DateTime DOB, string TypeCode, string EmailAddress, string Telephone, string Qualification, SqlConnection cn, int EnquiryLogID, string Reference)
        {
            string insertSQL;
            SqlCommand sqlcmd = new SqlCommand();
            sqlcmd.CommandTimeout = 0;

            cn.Open();

            insertSQL = "EXEC SP_I_MIEenquiries '" + FirstName + "' , '" + Surname + "' , '" + MaidenName + "' , '" + SecondName + "' , '" + IdNumber + "' , '" + Passport + "' , '" + DOB + "' , '" + EmailAddress + "' , '" + Telephone + "' , " + EnquiryLogID + " , '" + Reference + "'";
            SqlCommand sqlcmnd = new SqlCommand(insertSQL, cn);
            sqlcmnd.ExecuteNonQuery();
            sqlcmd.CommandTimeout = 0;

            cn.Close();

            if (!string.IsNullOrEmpty(TypeCode))
            {


                XmlDocument xmlTypeCodes = new XmlDocument();
                xmlTypeCodes.LoadXml(TypeCode);

                XmlNodeReader xmlrTypecodes = new XmlNodeReader(xmlTypeCodes);
                DataSet dsTypecodes = new DataSet();
                //XmlNodeList xnResultList = xmlDoc.SelectNodes("xml/TypeCodes");

                dsTypecodes.ReadXml(xmlrTypecodes);



                foreach (DataRow drTypeCodes in dsTypecodes.Tables[0].Rows)
                {
                    if (cn.State == ConnectionState.Open)
                        cn.Close();
                    cn.Open();

                    SqlCommand cmdTypecodes = new SqlCommand("sp_I_EnquiryTypeCode", cn);
                    cmdTypecodes.CommandType = CommandType.StoredProcedure;

                    cmdTypecodes.Parameters.AddWithValue("@Code", drTypeCodes.Table.Columns.Contains("Code") ? (object)drTypeCodes["Code"].ToString() : DBNull.Value);
                    cmdTypecodes.Parameters.AddWithValue("@Indemnity", drTypeCodes.Table.Columns.Contains("Indemnity") ? (object)drTypeCodes["Indemnity"].ToString() : 0);
                    cmdTypecodes.Parameters.AddWithValue("@SubscriberEnquiryID", EnquiryLogID.ToString());
                    cmdTypecodes.Parameters.AddWithValue("@Reference", Reference);

                    cmdTypecodes.ExecuteNonQuery();

                    cmdTypecodes.CommandTimeout = 0;

                    SqlCommand InsertDocs = new SqlCommand("SP_I_MieDocuments", cn);
                    InsertDocs.CommandType = CommandType.StoredProcedure;

                    if (drTypeCodes.Table.Columns.Contains("Document") == true)
                    {
                        string document = drTypeCodes["Document"].ToString();
                        string ConsentDocument = drTypeCodes["ConsentDocument"].ToString();

                        if (!string.IsNullOrEmpty(document))
                        {

                            byte[] documentarray = System.Text.Encoding.UTF8.GetBytes(document);                            

                            InsertDocs.Parameters.AddWithValue("@SubscriberEnquiryID", EnquiryLogID.ToString());
                            if (!string.IsNullOrEmpty(ConsentDocument))
                                
                            {
                                byte[] ConsentDocumentarray = System.Text.Encoding.UTF8.GetBytes(ConsentDocument);
                                InsertDocs.Parameters.AddWithValue("@ConsentDocument", ConsentDocumentarray);
                            }
                            else
                            {
                                InsertDocs.Parameters.AddWithValue("@ConsentDocument", DBNull.Value);
                            }
                            InsertDocs.Parameters.AddWithValue("@Document", documentarray);
                            InsertDocs.Parameters.AddWithValue("@DocumentName", drTypeCodes.Table.Columns.Contains("DocumentName") ? (object)drTypeCodes["DocumentName"].ToString() : DBNull.Value);

                            InsertDocs.ExecuteNonQuery();
                            InsertDocs.CommandTimeout = 0;

                            cn.Close();
                        }
                    }
                }

                if (!string.IsNullOrEmpty(Qualification))
                {

                    XmlDocument xmlQual = new XmlDocument();
                    xmlQual.LoadXml(Qualification);

                    XmlNodeReader xmxrQual = new XmlNodeReader(xmlQual);
                    DataSet dsQual = new DataSet();

                    dsQual.ReadXml(xmxrQual);

                    foreach (DataRow drQual in dsQual.Tables[0].Rows)
                    {
                        if (cn.State == ConnectionState.Open)
                            cn.Close();

                        cn.Open();

                        SqlCommand cmdQuali = new SqlCommand("sp_i_InquiryQualification", cn);
                        cmdQuali.CommandType = CommandType.StoredProcedure;

                        cmdQuali.Parameters.AddWithValue("@Qualification", drQual.Table.Columns.Contains("Qualification") ? (object)drQual["Qualification"].ToString() : DBNull.Value);
                        cmdQuali.Parameters.AddWithValue("@Institution", drQual.Table.Columns.Contains("Institution") ? (object)drQual["Institution"].ToString() : DBNull.Value);
                        cmdQuali.Parameters.AddWithValue("@YearCompleted", drQual.Table.Columns.Contains("YearCompleted") ? (object)drQual["YearCompleted"].ToString() : DBNull.Value);
                        //cmdQuali.Parameters.AddWithValue("@Reason", drQual.Table.Columns.Contains("Reason") ? (object)drQual["Reason"].ToString() : DBNull.Value);
                        cmdQuali.Parameters.AddWithValue("@SubscriberEnquiryID", EnquiryLogID.ToString());

                        cmdQuali.ExecuteNonQuery();
                        cmdQuali.CommandTimeout = 0;

                        SqlCommand cmdDocQuali = new SqlCommand("SP_I_MieDocuments", cn);
                        cmdDocQuali.CommandType = CommandType.StoredProcedure;

                        if (drQual.Table.Columns.Contains("Certificate") == true)
                        {
                            string document = drQual["Certificate"].ToString();

                            if (!string.IsNullOrEmpty(document))
                            {
                                byte[] documentarray = System.Text.Encoding.UTF8.GetBytes(document);

                                cmdDocQuali.Parameters.AddWithValue("@SubscriberEnquiryID", EnquiryLogID.ToString());
                                //cmdDocQuali.Parameters.AddWithValue("@Document", drQual.Table.Columns.Contains("Document") ? (object)drQual["Document"].ToString() : DBNull.Value);
                                cmdDocQuali.Parameters.AddWithValue("@Document", documentarray);
                                cmdDocQuali.Parameters.AddWithValue("@DocumentName", drQual.Table.Columns.Contains("CertificateName") ? (object)drQual["CertificateName"].ToString() : DBNull.Value);


                                cmdDocQuali.ExecuteNonQuery();
                                cmdDocQuali.CommandTimeout = 0;

                                cn.Close();
                            }
                        }
                    }

                }
            }

            return insertSQL;
        }

        //public string UploadDoc(int Reference, string Document, string DocName, SqlConnection cn)
        //{
        //    string insertSQL;

        //    cn.Open();           

        //    insertSQL = "Exec sp_GetDoc '" + Reference + "' , '" + Document + "' , '" + DocName + "'";
        //    SqlCommand slqCommands = new SqlCommand(insertSQL, cn);
        //    slqCommands.ExecuteNonQuery();
        //    slqCommands.CommandTimeout = 0;

        //    cn.Close();

        //    return insertSQL;

        //}

        public int AdditionalInfo(SqlConnection AdminConnection,string Feedback, int ReferenceNo, string status)
        {
            AdminConnection.Open();

            int requestKey = 0, credentialKey = 0, addInfoTypeKey = 0, addInfoKey = 0, RemoteKey = 0, rows = 0;

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(Feedback);

            //XmlNode oFeedbackNode = xmlDoc.SelectSingleNode("/xml/feedback");
            XmlNodeList oFeedbackItemNode = xmlDoc.SelectNodes("/xml/feedback/feedbackitem");

            if (oFeedbackItemNode != null)
            {
                foreach (XmlNode oFeedbackItem in oFeedbackItemNode)
                {
                    if (oFeedbackItem["ia0_key"] != null)
                        addInfoTypeKey = int.Parse(oFeedbackItem["ia0_key"].InnerText);

                    if (oFeedbackItem["iai_key"] != null)
                        addInfoKey = int.Parse(oFeedbackItem["iai_key"].InnerText);

                    string strSQL = string.Format("Update a set a.status = '{0}',a.FeedBack = '{1}' from MIEEnquiryResult a (nolock) inner join MIEEnquiry b (nolock) on a.RemoteRequest = b.RemoteRequest where a.ia0_key = {2} and iai_key = {3} and b.SubscriberenquirylogID = {4}", status, oFeedbackItem.OuterXml, addInfoTypeKey, addInfoKey, ReferenceNo);
                    SqlCommand sqlcmd = new SqlCommand(strSQL, AdminConnection);

                    rows = sqlcmd.ExecuteNonQuery(); 
                }
            }

            if (AdminConnection.State == ConnectionState.Open)
            {
                AdminConnection.Close();
                AdminConnection.Dispose();
            }

            return rows;
        }
    }
}