﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace XDSPortalEnquiry.Data
{
    public class Collections
    {
        public int InsertDemographic(SqlConnection cn, Entity.OnlineDemographicLog oDemographiclog)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            SqlCommand objcmd = new SqlCommand("sp_I_OnlineDemographicLog", cn);
            objcmd.CommandType = CommandType.StoredProcedure;

            objcmd.Parameters.AddWithValue("@EnquiryID",oDemographiclog.enquiryID);
            objcmd.Parameters.AddWithValue("@EnquiryResultID", oDemographiclog.enquiryResultID);
            objcmd.Parameters.AddWithValue("@KeyID", oDemographiclog.keyID);
            objcmd.Parameters.AddWithValue("@KeyType", oDemographiclog.keyType);
            objcmd.Parameters.AddWithValue("@SubscriberID", oDemographiclog.subscriberID);
            objcmd.Parameters.AddWithValue("@SubscriberName", oDemographiclog.subscriberName);
            objcmd.Parameters.AddWithValue("@SystemUserID", oDemographiclog.systemUserID);
            objcmd.Parameters.AddWithValue("@SystemUserName", oDemographiclog.systemUsername);
            objcmd.Parameters.AddWithValue("@SubscriberReference", oDemographiclog.subscriberReference);
            objcmd.Parameters.AddWithValue("@EnquiryStatusInd", oDemographiclog.enquiryStatusind);
            objcmd.Parameters.AddWithValue("@EnquiryResultInd", oDemographiclog.enquiryResultInd);
            objcmd.Parameters.AddWithValue("@HomePhoneNumber", oDemographiclog.homephonenumber);
            objcmd.Parameters.AddWithValue("@WorkPhoneNumber", oDemographiclog.workphonenumber);
            objcmd.Parameters.AddWithValue("@CellPhoneNumber", oDemographiclog.cellphonenumber);
            objcmd.Parameters.AddWithValue("@PhysicalAddress1", oDemographiclog.physicalAddress1);
            objcmd.Parameters.AddWithValue("@PhysicalAddress2", oDemographiclog.physicalAddress2);
            objcmd.Parameters.AddWithValue("@PhysicalAddress3", oDemographiclog.physicalAddress3);
            objcmd.Parameters.AddWithValue("@PhysicalAddress4", oDemographiclog.physicalAddress4);
            objcmd.Parameters.AddWithValue("@PhysicalPostCode", oDemographiclog.physicalPostCode);
            objcmd.Parameters.AddWithValue("@PostalAddress1", oDemographiclog.postalAddress1);
            objcmd.Parameters.AddWithValue("@PostalAddress2", oDemographiclog.postalAddress2);
            objcmd.Parameters.AddWithValue("@PostalAddress3", oDemographiclog.postalAddress3);
            objcmd.Parameters.AddWithValue("@PostalAddress4", oDemographiclog.postalAddress4);
            objcmd.Parameters.AddWithValue("@PostalPostCode", oDemographiclog.postalPostCode);
            objcmd.Parameters.AddWithValue("@EmailAddress", oDemographiclog.emailAddress);
            objcmd.Parameters.AddWithValue("@CreatedbyUser", oDemographiclog.username);

            SqlParameter ologID = new SqlParameter("@LogID",SqlDbType.Int);
            ologID.Direction = ParameterDirection.ReturnValue;
            objcmd.Parameters.Add(ologID);

            objcmd.ExecuteNonQuery();

            int intLogID = Convert.ToInt32(ologID.Value);
            
            if (cn.State == ConnectionState.Open)
                cn.Close();
            return intLogID;

        }

        public int InsertEmployment(SqlConnection cn, Entity.OnlineEmploymentLog oEmploymentLog)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            SqlCommand objcmd = new SqlCommand("sp_I_OnlineEmploymentLog", cn);
            objcmd.CommandType = CommandType.StoredProcedure;

            objcmd.Parameters.AddWithValue("@EnquiryID", oEmploymentLog.enquiryID);
            objcmd.Parameters.AddWithValue("@EnquiryResultID", oEmploymentLog.enquiryResultID);
            objcmd.Parameters.AddWithValue("@KeyID", oEmploymentLog.keyID);
            objcmd.Parameters.AddWithValue("@KeyType", oEmploymentLog.keyType);
            objcmd.Parameters.AddWithValue("@SubscriberID", oEmploymentLog.subscriberID);
            objcmd.Parameters.AddWithValue("@SubscriberName", oEmploymentLog.subscriberName);
            objcmd.Parameters.AddWithValue("@SystemUserID", oEmploymentLog.systemUserID);
            objcmd.Parameters.AddWithValue("@SystemUserName", oEmploymentLog.systemUsername);
            objcmd.Parameters.AddWithValue("@SubscriberReference", oEmploymentLog.subscriberReference);
            objcmd.Parameters.AddWithValue("@EnquiryStatusInd", oEmploymentLog.enquiryStatusind);
            objcmd.Parameters.AddWithValue("@EnquiryResultInd", oEmploymentLog.enquiryResultInd);
            objcmd.Parameters.AddWithValue("@EmployerName", oEmploymentLog.employerName);
            objcmd.Parameters.AddWithValue("@EmployerTelephoneNumber", oEmploymentLog.employerTelephoneNumber);
            objcmd.Parameters.AddWithValue("@CreatedbyUser", oEmploymentLog.username);
            objcmd.Parameters.AddWithValue("@EmploymentDate", oEmploymentLog.employmentDate);

            SqlParameter ologID = new SqlParameter("@LogID", SqlDbType.Int);
            ologID.Direction = ParameterDirection.ReturnValue;
            objcmd.Parameters.Add(ologID);

            objcmd.ExecuteNonQuery();

            int intLogID = Convert.ToInt32(ologID.Value);

            if (cn.State == ConnectionState.Open)
                cn.Close();
            return intLogID;

        }

        public void UpdateDemographic(SqlConnection cn, Entity.OnlineDemographicLog oDemographiclog)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            SqlCommand objcmd = new SqlCommand("sp_u_OnlineDemopgraphicLog", cn);
            objcmd.CommandType = CommandType.StoredProcedure;

            objcmd.Parameters.AddWithValue("@DemographicLogID", oDemographiclog.demographicLogID);
            objcmd.Parameters.AddWithValue("@EnquiryStatusInd", oDemographiclog.enquiryStatusind);
            objcmd.Parameters.AddWithValue("@EnquiryResultInd", oDemographiclog.enquiryResultInd);
            objcmd.Parameters.AddWithValue("@ErrorDescription", oDemographiclog.errordescription);
            objcmd.Parameters.AddWithValue("@ChangedbyUser", oDemographiclog.username);

            objcmd.ExecuteNonQuery();

            
            if (cn.State == ConnectionState.Open)
                cn.Close();            

        }

        public void UpdateEmployment(SqlConnection cn, Entity.OnlineEmploymentLog oEmploymentLog)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            SqlCommand objcmd = new SqlCommand("sp_u_OnlineEmploymentLog", cn);
            objcmd.CommandType = CommandType.StoredProcedure;

            objcmd.Parameters.AddWithValue("@EmploymentLogID", oEmploymentLog.employmentLogID);
            objcmd.Parameters.AddWithValue("@EnquiryStatusInd", oEmploymentLog.enquiryStatusind);
            objcmd.Parameters.AddWithValue("@EnquiryResultInd", oEmploymentLog.enquiryResultInd);
            objcmd.Parameters.AddWithValue("@ErrorDescription", oEmploymentLog.errordescription);
            objcmd.Parameters.AddWithValue("@ChangedbyUser", oEmploymentLog.username);

            objcmd.ExecuteNonQuery();

            if (cn.State == ConnectionState.Open)
                cn.Close();
        }

        public void validateDemographic(SqlConnection cn, Entity.OnlineDemographicLog oDemographiclog)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            SqlCommand objcmd = new SqlCommand("sp_Validate_OnlineDemographicLog", cn);
            objcmd.CommandType = CommandType.StoredProcedure;

            objcmd.Parameters.AddWithValue("@EnquiryID", oDemographiclog.enquiryID);
            objcmd.Parameters.AddWithValue("@EnquiryResultID", oDemographiclog.enquiryResultID);
            objcmd.Parameters.AddWithValue("@KeyID", oDemographiclog.keyID);
            objcmd.Parameters.AddWithValue("@KeyType", oDemographiclog.keyType);
            objcmd.Parameters.AddWithValue("@SubscriberID", oDemographiclog.subscriberID);
            objcmd.Parameters.AddWithValue("@SubscriberName", oDemographiclog.subscriberName);
            objcmd.Parameters.AddWithValue("@SystemUserID", oDemographiclog.systemUserID);
            objcmd.Parameters.AddWithValue("@SystemUserName", oDemographiclog.systemUsername);
            objcmd.Parameters.AddWithValue("@SubscriberReference", oDemographiclog.subscriberReference);
            objcmd.Parameters.AddWithValue("@EnquiryStatusInd", oDemographiclog.enquiryStatusind);
            objcmd.Parameters.AddWithValue("@EnquiryResultInd", oDemographiclog.enquiryResultInd);
            objcmd.Parameters.AddWithValue("@HomePhoneNumber", oDemographiclog.homephonenumber);
            objcmd.Parameters.AddWithValue("@WorkPhoneNumber", oDemographiclog.workphonenumber);
            objcmd.Parameters.AddWithValue("@CellPhoneNumber", oDemographiclog.cellphonenumber);
            objcmd.Parameters.AddWithValue("@PhysicalAddress1", oDemographiclog.physicalAddress1);
            objcmd.Parameters.AddWithValue("@PhysicalAddress2", oDemographiclog.physicalAddress2);
            objcmd.Parameters.AddWithValue("@PhysicalAddress3", oDemographiclog.physicalAddress3);
            objcmd.Parameters.AddWithValue("@PhysicalAddress4", oDemographiclog.physicalAddress4);
            objcmd.Parameters.AddWithValue("@PhysicalPostCode", oDemographiclog.physicalPostCode);
            objcmd.Parameters.AddWithValue("@PostalAddress1", oDemographiclog.postalAddress1);
            objcmd.Parameters.AddWithValue("@PostalAddress2", oDemographiclog.postalAddress2);
            objcmd.Parameters.AddWithValue("@PostalAddress3", oDemographiclog.postalAddress3);
            objcmd.Parameters.AddWithValue("@PostalAddress4", oDemographiclog.postalAddress4);
            objcmd.Parameters.AddWithValue("@PostalPostCode", oDemographiclog.postalPostCode);
            objcmd.Parameters.AddWithValue("@EmailAddress", oDemographiclog.emailAddress);
            objcmd.Parameters.AddWithValue("@CreatedbyUser", oDemographiclog.username);
            objcmd.Parameters.AddWithValue("@DemographicLogID", oDemographiclog.demographicLogID);

            objcmd.ExecuteNonQuery();

            if (cn.State == ConnectionState.Open)
                cn.Close();           

        }

        public void validateEmployment(SqlConnection cn, Entity.OnlineEmploymentLog oEmploymentLog)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            SqlCommand objcmd = new SqlCommand("sp_Validate_OnlineEmploymentLog", cn);
            objcmd.CommandType = CommandType.StoredProcedure;

            objcmd.Parameters.AddWithValue("@EnquiryID", oEmploymentLog.enquiryID);
            objcmd.Parameters.AddWithValue("@EnquiryResultID", oEmploymentLog.enquiryResultID);
            objcmd.Parameters.AddWithValue("@KeyID", oEmploymentLog.keyID);
            objcmd.Parameters.AddWithValue("@KeyType", oEmploymentLog.keyType);
            objcmd.Parameters.AddWithValue("@SubscriberID", oEmploymentLog.subscriberID);
            objcmd.Parameters.AddWithValue("@SubscriberName", oEmploymentLog.subscriberName);
            objcmd.Parameters.AddWithValue("@SystemUserID", oEmploymentLog.systemUserID);
            objcmd.Parameters.AddWithValue("@SystemUserName", oEmploymentLog.systemUsername);
            objcmd.Parameters.AddWithValue("@SubscriberReference", oEmploymentLog.subscriberReference);
            objcmd.Parameters.AddWithValue("@EnquiryStatusInd", oEmploymentLog.enquiryStatusind);
            objcmd.Parameters.AddWithValue("@EnquiryResultInd", oEmploymentLog.enquiryResultInd);
            objcmd.Parameters.AddWithValue("@EmployerName", oEmploymentLog.employerName);
            objcmd.Parameters.AddWithValue("@EmployerTelephoneNumber", oEmploymentLog.employerTelephoneNumber);
            objcmd.Parameters.AddWithValue("@CreatedbyUser", oEmploymentLog.username);
            objcmd.Parameters.AddWithValue("@EmploymentDate", oEmploymentLog.employmentDate);
            objcmd.Parameters.AddWithValue("@EmploymentLogID", oEmploymentLog.employmentLogID);

            objcmd.ExecuteNonQuery();

            if (cn.State == ConnectionState.Open)
                cn.Close();
            
        }

    }
}
