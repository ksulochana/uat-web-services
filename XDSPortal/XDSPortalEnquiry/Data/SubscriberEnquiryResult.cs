
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace XDSPortalEnquiry.Data
{
    public class SubscriberEnquiryResult
    {

        public SubscriberEnquiryResult() {}

        public int InsertSubscriberEnquiryResult(string DBConnection, Entity.SubscriberEnquiryResult SC)
        {

            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = DBConnection;
            cn.Open();

            string insertSQL = "sp_i_SubscriberEnquiryResult " + SC.SubscriberEnquiryID + " , '" + SC.KeyType + "' , " + SC.KeyID + " , '" + SC.EnquiryResult + "' , '" + SC.SearchOutput + "' , '" + SC.IDNo + "' , '" + SC.PassportNo + "' , '" + SC.FirstName + "' , '" + SC.SecondName + "' , '" + SC.Surname + "' , '" + SC.BirthDate + "' , '" + SC.Gender + "' , '" + SC.FirstInitial + "' , '" + SC.SecondInitial + "' , '" + SC.BusRegistrationNo + "' , '" + SC.BusBusinessName + "' , '" + SC.Ha_IDNo + "' , '" + SC.Ha_FirstName + "' , '" + SC.Ha_SecondName + "' , '" + SC.Ha_Surname + "' , '" + SC.Ha_DeceasedStatus + "' , '" + SC.Ha_DeceasedDate + "' , '" + SC.Ha_CauseOfDeath + "' , " + SC.BillingTypeID + " , " + SC.BillingPrice + " , '" + SC.VoucherCode + "' , " + SC.DetailsViewedYN + " , '" + SC.DetailsViewedDate + "' , " + SC.Billable + " , '" + SC.XMLData + "' , '" + SC.XMLBonus + "' , " + SC.BonusIncluded + " , '" + SC.ExtraVarOutput1 + "' , '" + SC.ExtraVarOutput2 + "' , '" + SC.ExtraVarOutput3 + "' , " + SC.ExtraIntOutput1 + " , " + SC.ExtraIntOutput2 + " , '" + SC.CreatedByUser + "' , '" + SC.CreatedOnDate + "' , '" + SC.ChangedByUser + "' , '" + SC.ChangedOnDate + "' ";
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();

            string strSQL = "SELECT @@IDENTITY";
            SqlCommand Imycommand = new SqlCommand(strSQL, cn);
            SqlDataReader Idr = Imycommand.ExecuteReader();
            Idr.Read();
            int intSubscriberEnquiryResultID = Convert.ToInt32(Idr.GetValue(0));
            Idr.Close();
            cn.Close();

            sqlcmd.Dispose();
            Imycommand.Dispose();
            Idr.Dispose();
            return intSubscriberEnquiryResultID;
        }

        public int InsertSubscriberEnquiryResult(SqlConnection cn, Entity.SubscriberEnquiryResult SC)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            if (!string.IsNullOrEmpty(SC.Surname))
                SC.Surname = SC.Surname.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.FirstName))
                SC.FirstName = SC.FirstName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.SecondName))
                SC.SecondName = SC.SecondName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.Ha_FirstName))
                SC.Ha_FirstName = SC.Ha_FirstName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.Ha_SecondName))
                SC.Ha_SecondName = SC.Ha_SecondName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.Ha_Surname))
                SC.Ha_Surname = SC.Ha_Surname.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.BusBusinessName))
                SC.BusBusinessName = SC.BusBusinessName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.Ha_CauseOfDeath))
                SC.Ha_CauseOfDeath = SC.Ha_CauseOfDeath.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.XMLBonus))
                SC.XMLBonus = SC.XMLBonus.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.XMLData))
                SC.XMLData = SC.XMLData.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.SearchOutput))
                SC.SearchOutput = SC.SearchOutput.Replace("'", "''");
   
            string insertSQL = "sp_i_SubscriberEnquiryResult " + SC.SubscriberEnquiryID + " , '" + SC.KeyType + "' , " + SC.KeyID + " , '" + SC.EnquiryResult + "' , '" + SC.SearchOutput + "' , '" + SC.IDNo + "' , '" + SC.PassportNo + "' , '" + SC.FirstName + "' , '" + SC.SecondName + "' , '" + SC.Surname + "' , '" + SC.BirthDate + "' , '" + SC.Gender + "' , '" + SC.FirstInitial + "' , '" + SC.SecondInitial + "' , '" + SC.BusRegistrationNo + "' , '" + SC.BusBusinessName + "' , '" + SC.Ha_IDNo + "' , '" + SC.Ha_FirstName + "' , '" + SC.Ha_SecondName + "' , '" + SC.Ha_Surname + "' , '" + SC.Ha_DeceasedStatus + "' , '" + SC.Ha_DeceasedDate + "' , '" + SC.Ha_CauseOfDeath + "' , " + SC.BillingTypeID + " , " + SC.BillingPrice + " , '" + SC.VoucherCode + "' , " +  SC.DetailsViewedYN + " , '" + SC.DetailsViewedDate + "' , " + SC.Billable + " , '" + SC.XMLData + "' , '" + SC.XMLBonus + "' , " + SC.BonusIncluded + " , '" + SC.ExtraVarOutput1 + "' , '" + SC.ExtraVarOutput2 + "' , '" + SC.ExtraVarOutput3 + "' , " + SC.ExtraIntOutput1 + " , " + SC.ExtraIntOutput2 + " , '" + SC.CreatedByUser + "' , '" + SC.CreatedOnDate + "' , '" + SC.ChangedByUser + "' , '" + SC.ChangedOnDate + "',"+SC.ProductID;
            
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();

            string strSQL = "SELECT @@IDENTITY";
            SqlCommand Imycommand = new SqlCommand(strSQL, cn);
            SqlDataReader Idr = Imycommand.ExecuteReader();
            Idr.Read();
            int intSubscriberEnquiryResultID = Convert.ToInt32(Idr.GetValue(0));
            Idr.Close();

            sqlcmd.Dispose();
            Idr.Dispose();
            Imycommand.Dispose();
            cn.Close();
            return intSubscriberEnquiryResultID;
        }

        public int InsertSubscriberEnquiryResultBioMetric(SqlConnection cn, XDSPortalEnquiry.Entity.HVerificationResponse oSearch, XDSPortalLibrary.Entity_Layer.ConsumerIDVerificationTrace ObjSearch)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();


            string insertSQL = "sp_I_SubscriberEnquiryResultIDBioMetric";
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);
            sqlcmd.CommandTimeout = 0;
            sqlcmd.CommandType = CommandType.StoredProcedure;


            sqlcmd.Parameters.AddWithValue("@SubscriberEnquiryIDBioMetric", oSearch.BioMetricID);
            sqlcmd.Parameters.AddWithValue("@SubscriberEnquiryID", oSearch.SubscriberEnquiryID);
            sqlcmd.Parameters.AddWithValue("@SubscriberEnquiryResultID", oSearch.SubscriberEnquiryResultID);
            sqlcmd.Parameters.AddWithValue("@ApplicationErrors", oSearch.ApplicationErrors);
            sqlcmd.Parameters.AddWithValue("@VerificationErrors", oSearch.VerificationErrors);
            sqlcmd.Parameters.AddWithValue("@Base64StringJpeg2000Image", oSearch.Base64StringJpeg2000Image);
            sqlcmd.Parameters.AddWithValue("@VerificationResult", oSearch.VerificationResult);
            sqlcmd.Parameters.AddWithValue("@VerificationResultDescription", oSearch.VerificationResultDescription);
            sqlcmd.Parameters.AddWithValue("@VerificationColorIndicator", oSearch.VerificationColorIndicator);
            sqlcmd.Parameters.AddWithValue("@TmStamp", oSearch.TmStamp);
            sqlcmd.Parameters.AddWithValue("@TransactionNumber", oSearch.TransactionNumber);
            sqlcmd.Parameters.AddWithValue("@HasImage", oSearch.HasImage);
            sqlcmd.Parameters.AddWithValue("@FingerColor", oSearch.FingerColor);
            sqlcmd.Parameters.AddWithValue("@CreatedbyUser", oSearch.CreatedbyUser);
            sqlcmd.Parameters.AddWithValue("@Createdondate", DateTime.Now);
            sqlcmd.Parameters.AddWithValue("@URLrequested", oSearch.UrlRequested);

            int added = 0;
            SqlParameter oparam = new SqlParameter();
            oparam.Direction = ParameterDirection.ReturnValue;
            sqlcmd.Parameters.Add(oparam);

            added = sqlcmd.ExecuteNonQuery();

            //  File.AppendAllText(@"C:\Log\oparam.txt", oparam.Value.ToString());

            if (oSearch.Sync)
            {
                ObjSearch.EnquiryID = oSearch.SubscriberEnquiryID;
                ObjSearch.EnquiryResultID = oSearch.SubscriberEnquiryResultID;
                ObjSearch.ApplicationErrors = oSearch.ApplicationErrors;
                ObjSearch.VerificationErrors = oSearch.VerificationErrors;
                ObjSearch.Base64StringJpeg2000Image = oSearch.Base64StringJpeg2000Image;
                ObjSearch.VerificationResult = oSearch.VerificationResult;
                ObjSearch.VerificationResultDescription = oSearch.VerificationResultDescription;
                ObjSearch.VerificationColorIndicator = oSearch.VerificationColorIndicator;
                ObjSearch.HanisTimeStamp = oSearch.TmStamp;
                ObjSearch.TransactionNumber = long.Parse(oSearch.TransactionNumber.ToString());
                ObjSearch.HasImage = oSearch.HasImage;
                ObjSearch.FingerColor = oSearch.FingerColor;
                ObjSearch.CreatedbyUser = oSearch.CreatedbyUser;
                

                insertSQL = "Insert into DIAResultSynclog (SubscriberEnquiryResultIDBiometric,CreatedbyUser,Createdondate) values (@SubscriberEnquiryResultIDBiometricID,@CreatedbyUser,@Createdondate) Select @@Identity";
                SqlCommand cmd1 = new SqlCommand(insertSQL, cn);
                cmd1.CommandTimeout = 0;
                cmd1.CommandType = CommandType.Text;


                cmd1.Parameters.AddWithValue("@SubscriberEnquiryResultIDBiometricID", oparam.Value.ToString());
                cmd1.Parameters.AddWithValue("@CreatedbyUser", oSearch.CreatedbyUser);
                cmd1.Parameters.AddWithValue("@Createdondate", DateTime.Now);

                SqlDataReader or = cmd1.ExecuteReader();

                if (or.HasRows)
                {
                    while (or.Read())
                    {
                        added = int.Parse(or.GetValue(0).ToString());
                    }
                }
                or.Close();


                try
                {
                    XDSDataLibrary.ReportAccess ra = new XDSDataLibrary.ReportAccess(string.Empty, string.Empty);
                    ObjSearch.XDSRef = int.Parse(oparam.Value.ToString());

                    XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

                    rp = ra.DIADataSync(ObjSearch);

                    insertSQL = "Insert into DIAResultSynclogStatus (LogID,SubscriberEnquiryResultIDBiometric,SyncStatus,CreatedbyUser,Createdondate,Response) values (@LogID,@SubscriberEnquiryResultIDBiometric,@SyncStatus,@CreatedbyUser,@Createdondate,@Response) Select @@Identity";
                    sqlcmd = new SqlCommand(insertSQL, cn);
                    sqlcmd.CommandTimeout = 0;
                    sqlcmd.CommandType = CommandType.Text;


                     File.AppendAllText(@"C:\Log\realtimesync.txt", rp.ResponseData);

                    File.AppendAllText(@"C:\Log\realtimesync.txt","Status"+ rp.ResponseStatus.ToString().Substring(0, 1));

                    sqlcmd.Parameters.AddWithValue("@LogID", added);
                    sqlcmd.Parameters.AddWithValue("@SubscriberEnquiryResultIDBiometric", ObjSearch.XDSRef);
                    sqlcmd.Parameters.AddWithValue("@SyncStatus", rp.ResponseStatus.ToString().Substring(0,1));
                    sqlcmd.Parameters.AddWithValue("@CreatedbyUser", oSearch.CreatedbyUser);
                    sqlcmd.Parameters.AddWithValue("@Createdondate", DateTime.Now);
                    sqlcmd.Parameters.AddWithValue("@Response", rp.ResponseData);

                    sqlcmd.ExecuteNonQuery();

                }
                catch(Exception ex)
                {
                    try
                    {
                        File.AppendAllText(@"C:\Log\realtimesync.txt", ex.Message + ex.StackTrace + ex.InnerException);

                        insertSQL = "Insert into DIASyncServiceLog (StatusDescription,CreatedbyUser,Createdondate) values (@StatusDescription,@CreatedbyUser,@Createdondate) Select @@Identity";
                        sqlcmd = new SqlCommand(insertSQL, cn);
                        sqlcmd.CommandTimeout = 0;
                        sqlcmd.CommandType = CommandType.Text;

                        sqlcmd.Parameters.AddWithValue("@StatusDescription", "WebService-" + ex.Message + ex.StackTrace + ex.InnerException);
                        sqlcmd.Parameters.AddWithValue("@CreatedbyUser", oSearch.CreatedbyUser);
                        sqlcmd.Parameters.AddWithValue("@Createdondate", DateTime.Now);

                        sqlcmd.ExecuteNonQuery();

                        insertSQL = "Insert into DIAResultSynclogStatus (LogID,SubscriberEnquiryResultIDBiometric,SyncStatus,CreatedbyUser,Createdondate) values (@LogID,@SubscriberEnquiryResultIDBiometric,@SyncStatus,@CreatedbyUser,@Createdondate) Select @@Identity";
                        sqlcmd = new SqlCommand(insertSQL, cn);
                        sqlcmd.CommandTimeout = 0;
                        sqlcmd.CommandType = CommandType.Text;

                        sqlcmd.Parameters.AddWithValue("@LogID", added);
                        sqlcmd.Parameters.AddWithValue("@SubscriberEnquiryResultIDBiometric", ObjSearch.XDSRef);
                        sqlcmd.Parameters.AddWithValue("@SyncStatus", "E");
                        sqlcmd.Parameters.AddWithValue("@CreatedbyUser", oSearch.CreatedbyUser);
                        sqlcmd.Parameters.AddWithValue("@Createdondate", DateTime.Now);

                        sqlcmd.ExecuteNonQuery();
                    }

                    catch (Exception e)
                    {

                        File.AppendAllText(@"C:\Log\realtimesync.txt","Step 2:"+ ex.Message + ex.StackTrace + ex.InnerException);
                    }

                }
            }

            return added;
        }

      

        public int InsertDIAVerifyimagesResponse(SqlConnection cn, XDSPortalEnquiry.Entity.DIAVerifyFaces oSearch)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();


            string insertSQL = "Insert into  [DIAVerifyimagesResponse](SubscriberEnquiryID  ,SubscriberEnquiryResultID  ,Score  ,Quality1  ,Sharpness1  ,Age1  ,Quality2  ,Sharpness2  ,Age2  ,Gender1  ,Gender2  ,MsgResp  ,DiaReference  ,CreatedbyUser  ,Createdondate  ,URLrequested) values (@SubscriberEnquiryID  ,@SubscriberEnquiryResultID  ,@Score  ,@Quality1  ,@Sharpness1  ,@Age1  ,@Quality2  ,@Sharpness2  ,@Age2  ,@Gender1  ,@Gender2  ,@MsgResp  ,@DiaReference  ,@CreatedbyUser  ,@Createdondate  ,@URLrequested)";
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);
            sqlcmd.CommandTimeout = 0;
            sqlcmd.CommandType = CommandType.Text;


            sqlcmd.Parameters.AddWithValue("@SubscriberEnquiryID", oSearch.SubscriberEnquiryID);
            sqlcmd.Parameters.AddWithValue("@SubscriberEnquiryResultID", oSearch.SubscriberEnquiryResultID);
            sqlcmd.Parameters.AddWithValue("@Score", oSearch.Score);
            sqlcmd.Parameters.AddWithValue("@Quality1", oSearch.Quality1);
            sqlcmd.Parameters.AddWithValue("@Sharpness1", oSearch.Sharpness1);
            sqlcmd.Parameters.AddWithValue("@Age1", oSearch.Age1);
            sqlcmd.Parameters.AddWithValue("@Quality2", oSearch.Quality2);
            sqlcmd.Parameters.AddWithValue("@Sharpness2", oSearch.Sharpness2);
            sqlcmd.Parameters.AddWithValue("@Age2", oSearch.Age2);
            sqlcmd.Parameters.AddWithValue("@Gender1", oSearch.Gender1);
            sqlcmd.Parameters.AddWithValue("@Gender2", oSearch.Gender2);
            sqlcmd.Parameters.AddWithValue("@MsgResp", oSearch.ResponseMsg);
            sqlcmd.Parameters.AddWithValue("@DiaReference", oSearch.DiaReference);
            sqlcmd.Parameters.AddWithValue("@CreatedbyUser", "ws");
            sqlcmd.Parameters.AddWithValue("@Createdondate", DateTime.Now);
            sqlcmd.Parameters.AddWithValue("@URLrequested", oSearch.URL);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();

            return added;
        }

        public int UpdateSubscriberEnquiryResult(string DBConnection, Entity.SubscriberEnquiryResult SC)
        {

            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = DBConnection;
            cn.Open();

            if (!string.IsNullOrEmpty(SC.Surname))
                SC.Surname = SC.Surname.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.FirstName))
                SC.FirstName = SC.FirstName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.SecondName))
                SC.SecondName = SC.SecondName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.Ha_FirstName))
                SC.Ha_FirstName = SC.Ha_FirstName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.Ha_SecondName))
                SC.Ha_SecondName = SC.Ha_SecondName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.Ha_Surname))
                SC.Ha_Surname = SC.Ha_Surname.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.BusBusinessName))
                SC.BusBusinessName = SC.BusBusinessName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.Ha_CauseOfDeath))
                SC.Ha_CauseOfDeath = SC.Ha_CauseOfDeath.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.XMLBonus))
                SC.XMLBonus = SC.XMLBonus.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.XMLData))
                SC.XMLData = SC.XMLData.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.SearchOutput))
                SC.SearchOutput = SC.SearchOutput.Replace("'", "''");

            string insertSQL = "EXEC sp_u_SubscriberEnquiryResult " + SC.SubscriberEnquiryResultID + " , " + SC.SubscriberEnquiryID + " , '" + SC.KeyType + "' , " + SC.KeyID + " , '" + SC.EnquiryResult + "' , '" + SC.SearchOutput + "' , '" + SC.IDNo + "' , '" + SC.PassportNo + "' , '" + SC.FirstName + "' , '" + SC.SecondName + "' , '" + SC.Surname + "' , '" + SC.BirthDate + "' , '" + SC.Gender + "' , '" + SC.FirstInitial + "' , '" + SC.SecondInitial + "' , '" + SC.BusRegistrationNo + "' , '" + SC.BusBusinessName + "' , '" + SC.Ha_IDNo + "' , '" + SC.Ha_FirstName + "' , '" + SC.Ha_SecondName + "' , '" + SC.Ha_Surname + "' , '" + SC.Ha_DeceasedStatus + "' , '" + SC.Ha_DeceasedDate + "' , '" + SC.Ha_CauseOfDeath + "' , " + SC.BillingTypeID + " , " + SC.BillingPrice + " , '" + SC.VoucherCode + "' , " + SC.DetailsViewedYN + " , '" + SC.DetailsViewedDate + "' , " + SC.Billable + " , '" + SC.XMLData + "' , '" + SC.XMLBonus + "' , " + SC.BonusIncluded + " , '" + SC.ExtraVarOutput1 + "' , '" + SC.ExtraVarOutput2 + "' , '" + SC.ExtraVarOutput3 + "' , " + SC.ExtraIntOutput1 + " , " + SC.ExtraIntOutput2 + " , '" + SC.CreatedByUser + "' , '" + SC.CreatedOnDate + "' , '" + SC.ChangedByUser + "' , '" + SC.ChangedOnDate + "'," + SC.ProductID + "," + SC.NLRDataIncluded;
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();
            cn.Close();

            sqlcmd.Dispose();

            return added;
        }

        public int UpdateSubscriberEnquiryResult(SqlConnection cn, Entity.SubscriberEnquiryResult SC)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            if (!string.IsNullOrEmpty(SC.Surname))
                SC.Surname = SC.Surname.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.FirstName))
                SC.FirstName = SC.FirstName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.SecondName))
                SC.SecondName = SC.SecondName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.Ha_FirstName))
                SC.Ha_FirstName = SC.Ha_FirstName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.Ha_SecondName))
                SC.Ha_SecondName = SC.Ha_SecondName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.Ha_Surname))
                SC.Ha_Surname = SC.Ha_Surname.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.BusBusinessName))
                SC.BusBusinessName = SC.BusBusinessName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.Ha_CauseOfDeath))
                SC.Ha_CauseOfDeath = SC.Ha_CauseOfDeath.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.XMLBonus))
                SC.XMLBonus = SC.XMLBonus.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.XMLData))
                SC.XMLData = SC.XMLData.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.SearchOutput))
                SC.SearchOutput = SC.SearchOutput.Replace("'", "''");

            string insertSQL = "EXEC sp_u_SubscriberEnquiryResult " + SC.SubscriberEnquiryResultID + " , " + SC.SubscriberEnquiryID + " , '" + SC.KeyType + "' , " + SC.KeyID + " , '" + SC.EnquiryResult + "' , '" + SC.SearchOutput + "' , '" + SC.IDNo + "' , '" + SC.PassportNo + "' , '" + SC.FirstName + "' , '" + SC.SecondName + "' , '" + SC.Surname + "' , '" + SC.BirthDate + "' , '" + SC.Gender + "' , '" + SC.FirstInitial + "' , '" + SC.SecondInitial + "' , '" + SC.BusRegistrationNo + "' , '" + SC.BusBusinessName + "' , '" + SC.Ha_IDNo + "' , '" + SC.Ha_FirstName + "' , '" + SC.Ha_SecondName + "' , '" + SC.Ha_Surname + "' , '" + SC.Ha_DeceasedStatus + "' , '" + SC.Ha_DeceasedDate + "' , '" + SC.Ha_CauseOfDeath + "' , " + SC.BillingTypeID + " , " + SC.BillingPrice + " , '" + SC.VoucherCode + "' , " + SC.DetailsViewedYN + " , '" + SC.DetailsViewedDate + "' , " + SC.Billable + " , '" + SC.XMLData + "' , '" + SC.XMLBonus + "' , " + SC.BonusIncluded + " , '" + SC.ExtraVarOutput1 + "' , '" + SC.ExtraVarOutput2 + "' , '" + SC.ExtraVarOutput3 + "' , " + SC.ExtraIntOutput1 + " , " + SC.ExtraIntOutput2 + " , '" + SC.CreatedByUser + "' , '" + SC.CreatedOnDate + "' , '" + SC.ChangedByUser + "' , '" + SC.ChangedOnDate + "'," + SC.ProductID + "," + SC.NLRDataIncluded;
            //File.AppendAllText(@"C:\Log\CommercialLinkages.txt", "Report:" + insertSQL + "\n");

            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();

            sqlcmd.Dispose();
            cn.Close();

            return added;
        }

        public int UpdateMulipleSubscriberEnquiryResult(string DBConnection, Entity.SubscriberEnquiryResult SC)
        {


            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = DBConnection;
            cn.Open();
            string insertSQL = "EXEC sp_u_SubscriberEnquiryResult " + SC.SubscriberEnquiryResultID + " , " + SC.SubscriberEnquiryID + " , '" + SC.KeyType + "' , " + SC.KeyID + " , '" + SC.EnquiryResult + "' , '" + SC.SearchOutput + "' , '" + SC.IDNo + "' , '" + SC.PassportNo + "' , '" + SC.FirstName + "' , '" + SC.SecondName + "' , '" + SC.Surname + "' , '" + SC.BirthDate + "' , '" + SC.Gender + "' , '" + SC.FirstInitial + "' , '" + SC.SecondInitial + "' , '" + SC.BusRegistrationNo + "' , '" + SC.BusBusinessName + "' , '" + SC.Ha_IDNo + "' , '" + SC.Ha_FirstName + "' , '" + SC.Ha_SecondName + "' , '" + SC.Ha_Surname + "' , '" + SC.Ha_DeceasedStatus + "' , '" + SC.Ha_DeceasedDate + "' , '" + SC.Ha_CauseOfDeath + "' , " + SC.BillingTypeID + " , " + SC.BillingPrice + " , '" + SC.VoucherCode + "' , " + SC.DetailsViewedYN + " , '" + SC.DetailsViewedDate + "' , " + SC.Billable + " , '" + SC.XMLData + "' , '" + SC.XMLBonus + "' , " + SC.BonusIncluded + " , '" + SC.ExtraVarOutput1 + "' , '" + SC.ExtraVarOutput2 + "' , '" + SC.ExtraVarOutput3 + "' , " + SC.ExtraIntOutput1 + " , " + SC.ExtraIntOutput2 + " , '" + SC.CreatedByUser + "' , '" + SC.CreatedOnDate + "' , '" + SC.ChangedByUser + "' , '" + SC.ChangedOnDate + "', " + SC.ProductID + "," + SC.NLRDataIncluded;
            //string insertSQL = "EXEC sp_u_SubscriberEnquiryResult " + SC.SubscriberEnquiryResultID + " , " + SC.SubscriberEnquiryID + " , '" + SC.KeyType + "' , " + SC.KeyID + " , '" + SC.EnquiryResult + "' , '" + SC.SearchOutput + "' , '" + SC.IDNo + "' , '" + SC.PassportNo + "' , '" + SC.FirstName + "' , '" + SC.Surname + "' , '" + SC.BirthDate + "' , '" + SC.Gender + "' , '" + SC.FirstInitial + "' , '" + SC.SecondInitial + "' , '" + SC.BusRegistrationNo + "' , '" + SC.BusBusinessName + "' , " + SC.BillingTypeID + " , " + SC.BillingPrice + " , '" + SC.VoucherCode + "' , " + SC.DetailsViewedYN + " , '" + SC.DetailsViewedDate + "' , " + SC.Billable + " , '" + SC.XMLData + "' , '" + SC.XMLBonus + "' , " + SC.BonusIncluded + " , '" + SC.ExtraVarOutput1 + "' , '" + SC.ExtraVarOutput2 + "' , '" + SC.ExtraVarOutput3 + "' , " + SC.ExtraIntOutput1 + " , " + SC.ExtraIntOutput2 + " , '" + SC.CreatedByUser + "' , '" + SC.CreatedOnDate + "' , '" + SC.ChangedByUser + "' , '" + SC.ChangedOnDate + "' ";
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();
            cn.Close();

            sqlcmd.Dispose();
            return added;
        }

        public int UpdateMulipleSubscriberEnquiryResult(SqlConnection cn, Entity.SubscriberEnquiryResult SC)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            if (!string.IsNullOrEmpty(SC.Surname))
                SC.Surname = SC.Surname.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.FirstName))
                SC.FirstName = SC.FirstName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.SecondName))
                SC.SecondName = SC.SecondName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.Ha_FirstName))
                SC.Ha_FirstName = SC.Ha_FirstName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.Ha_SecondName))
                SC.Ha_SecondName = SC.Ha_SecondName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.Ha_Surname))
                SC.Ha_Surname = SC.Ha_Surname.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.BusBusinessName))
                SC.BusBusinessName = SC.BusBusinessName.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.Ha_CauseOfDeath))
                SC.Ha_CauseOfDeath = SC.Ha_CauseOfDeath.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.XMLBonus))
                SC.XMLBonus = SC.XMLBonus.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.XMLData))
                SC.XMLData = SC.XMLData.Replace("'", "''");
            if (!string.IsNullOrEmpty(SC.SearchOutput))
                SC.SearchOutput = SC.SearchOutput.Replace("'", "''");
            
            string insertSQL = "EXEC sp_u_SubscriberEnquiryResult " + SC.SubscriberEnquiryResultID + " , " + SC.SubscriberEnquiryID + " , '" + SC.KeyType + "' , " + SC.KeyID + " , '" + SC.EnquiryResult + "' , '" + SC.SearchOutput + "' , '" + SC.IDNo + "' , '" + SC.PassportNo + "' , '" + SC.FirstName + "' , '" + SC.SecondName + "' , '" + SC.Surname + "' , '" + SC.BirthDate + "' , '" + SC.Gender + "' , '" + SC.FirstInitial + "' , '" + SC.SecondInitial + "' , '" + SC.BusRegistrationNo + "' , '" + SC.BusBusinessName + "' , '" + SC.Ha_IDNo + "' , '" + SC.Ha_FirstName + "' , '" + SC.Ha_SecondName + "' , '" + SC.Ha_Surname + "' , '" + SC.Ha_DeceasedStatus + "' , '" + SC.Ha_DeceasedDate + "' , '" + SC.Ha_CauseOfDeath + "' , " + SC.BillingTypeID + " , " + SC.BillingPrice + " , '" + SC.VoucherCode + "' , " + SC.DetailsViewedYN + " , '" + SC.DetailsViewedDate + "' , " + SC.Billable + " , '" + SC.XMLData + "' , '" + SC.XMLBonus + "' , " + SC.BonusIncluded + " , '" + SC.ExtraVarOutput1 + "' , '" + SC.ExtraVarOutput2 + "' , '" + SC.ExtraVarOutput3 + "' , " + SC.ExtraIntOutput1 + " , " + SC.ExtraIntOutput2 + " , '" + SC.CreatedByUser + "' , '" + SC.CreatedOnDate + "' , '" + SC.ChangedByUser + "' , '" + SC.ChangedOnDate + "', " + SC.ProductID + "," + SC.NLRDataIncluded;
            //string insertSQL = "EXEC sp_u_SubscriberEnquiryResult " + SC.SubscriberEnquiryResultID + " , " + SC.SubscriberEnquiryID + " , '" + SC.KeyType + "' , " + SC.KeyID + " , '" + SC.EnquiryResult + "' , '" + SC.SearchOutput + "' , '" + SC.IDNo + "' , '" + SC.PassportNo + "' , '" + SC.FirstName + "' , '" + SC.Surname + "' , '" + SC.BirthDate + "' , '" + SC.Gender + "' , '" + SC.FirstInitial + "' , '" + SC.SecondInitial + "' , '" + SC.BusRegistrationNo + "' , '" + SC.BusBusinessName + "' , " + SC.BillingTypeID + " , " + SC.BillingPrice + " , '" + SC.VoucherCode + "' , " + SC.DetailsViewedYN + " , '" + SC.DetailsViewedDate + "' , " + SC.Billable + " , '" + SC.XMLData + "' , '" + SC.XMLBonus + "' , " + SC.BonusIncluded + " , '" + SC.ExtraVarOutput1 + "' , '" + SC.ExtraVarOutput2 + "' , '" + SC.ExtraVarOutput3 + "' , " + SC.ExtraIntOutput1 + " , " + SC.ExtraIntOutput2 + " , '" + SC.CreatedByUser + "' , '" + SC.CreatedOnDate + "' , '" + SC.ChangedByUser + "' , '" + SC.ChangedOnDate + "' ";
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();

            sqlcmd.Dispose();
            cn.Close();

            return added;
        }

        public int UpdateBonusSubscriberEnquiryResult(string DBConnection, Entity.SubscriberEnquiryResult SC)
        {

            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = DBConnection;
            cn.Open();

            string insertSQL = "UPDATE SubscriberEnquiryResult SET DetailsViewedYN = 1,  DetailsViewedDate = GETDATE(), XMLData = '" + SC.XMLData + "' WHERE DetailsViewedYN = 0 AND SubscriberEnquiryResultID = " + SC.SubscriberEnquiryResultID.ToString();
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();
            cn.Close();

            sqlcmd.Dispose();
            cn.Close();

            return added;
        }

        public int UpdateBonusSubscriberEnquiryResult(SqlConnection cn, Entity.SubscriberEnquiryResult SC)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();
            
            string insertSQL = "UPDATE SubscriberEnquiryResult SET DetailsViewedYN = 1,  DetailsViewedDate = GETDATE(), XMLData = '" + SC.XMLData + "' WHERE DetailsViewedYN = 0 AND SubscriberEnquiryResultID = " + SC.SubscriberEnquiryResultID.ToString();
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();

            sqlcmd.Dispose();
            cn.Close();

            return added;
        }

        public DataSet GetSubscriberEnquiryResultDataSet(string DBConnection, int intSubscriberEnquiryID)
        {
            
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = DBConnection;
            cn.Open();

            string strSQL = "Select * from SubscriberEnquiryResult nolock Where SubscriberEnquiryID = " + intSubscriberEnquiryID.ToString();
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            sqlcmd.Dispose();
            sqlda.Dispose();
            cn.Close();

            return ds;
        }

        public DataSet GetSubscriberEnquiryResultDataSet(SqlConnection cn, int intSubscriberEnquiryID)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            string strSQL = "Select * from SubscriberEnquiryResult nolock Where SubscriberEnquiryID = " + intSubscriberEnquiryID.ToString();
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            sqlda.Dispose();
            sqlcmd.Dispose();
            cn.Close();

            return ds;
        }

        public DataSet GetSubscriberEnquiryResultDataSetProduct(SqlConnection cn, int SubscriberID, int ProductID)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            string strSQL = "Select top 50 * from SubscriberEnquirySearchResults where subscriberid = " + SubscriberID.ToString() + " and ProductID = " + ProductID.ToString() + " order by subscriberenquiryresultid desc";
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            sqlda.Dispose();
            sqlcmd.Dispose();

            cn.Close();

            return ds;
        }

        public DataSet GetSubscriberEnquiryResultDataSetSubscriber(SqlConnection cn, int SubscriberID)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            string strSQL = "Select top 50 * from SubscriberEnquirySearchResults where subscriberid = " + SubscriberID.ToString() + " order by subscriberenquiryresultid desc"; ;
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            sqlda.Dispose();
            sqlcmd.Dispose();
            cn.Close();
            return ds;
        }

        public DataSet GetSubscriberEnquiryResultDataSetUser(SqlConnection cn, int SystemUserID, int ProductID)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            string strSQL = "";

            if (ProductID == 0)
            {
                strSQL = "Select top 50 * from SubscriberEnquirySearchResults where systemuserid = " + SystemUserID.ToString() + " and ProductID = " + ProductID.ToString() + " order by subscriberenquiryresultid desc";
            }
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            sqlda.Dispose();
            sqlcmd.Dispose();
            cn.Close();

            return ds;
        }


        public List<Entity.SubscriberEnquiryResult> GetSubscriberEnquiryResultListObject(string DBConnection, int intSubscriberEnquiryID)
        {
            List<Entity.SubscriberEnquiryResult> Lsec = new List<Entity.SubscriberEnquiryResult>();
            Entity.SubscriberEnquiryResult sec = new Entity.SubscriberEnquiryResult();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = DBConnection;
            cn.Open();

            string strSQL = "Select * from SubscriberEnquiryResult nolock Where SubscriberEnquiryID = " + intSubscriberEnquiryID.ToString();
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                sec.SubscriberEnquiryResultID = int.Parse(r["SubscriberEnquiryResultID"].ToString());
                sec.SubscriberEnquiryID = int.Parse(r["SubscriberEnquiryID"].ToString());
                sec.Billable = bool.Parse(r["Billable"].ToString());
                sec.BillingPrice = double.Parse(r["BillingPrice"].ToString());
                if (!string.IsNullOrEmpty(r["BillingTypeID"].ToString())) { sec.BillingTypeID = int.Parse(r["BillingTypeID"].ToString()); }
                if (!string.IsNullOrEmpty(r["BirthDate"].ToString())) { sec.BirthDate = DateTime.Parse(r["BirthDate"].ToString()); }
                sec.BonusIncluded = bool.Parse(r["BonusIncluded"].ToString());
                if (!string.IsNullOrEmpty(r["BusBusinessName"].ToString())) { sec.BusBusinessName = r["BusBusinessName"].ToString(); }
                if (!string.IsNullOrEmpty(r["BusRegistrationNo"].ToString())) { sec.BusRegistrationNo = r["BusRegistrationNo"].ToString(); }
                sec.ChangedByUser = r["ChangedByUser"].ToString();
                if (!string.IsNullOrEmpty(r["ChangedOnDate"].ToString())) { sec.ChangedOnDate = DateTime.Parse(r["ChangedOnDate"].ToString()); }
                sec.CreatedByUser = r["CreatedByUser"].ToString();
                if (!string.IsNullOrEmpty(r["CreatedOnDate"].ToString())) { sec.CreatedOnDate = DateTime.Parse(r["CreatedOnDate"].ToString()); }
                if (!string.IsNullOrEmpty(r["DetailsViewedDate"].ToString())) {sec.DetailsViewedDate = DateTime.Parse(r["DetailsViewedDate"].ToString());}
                sec.DetailsViewedYN = bool.Parse(r["CreatedByUser"].ToString());
                sec.EnquiryResult = r["EnquiryResult"].ToString();
                sec.ExtraIntOutput1 = int.Parse(r["ExtraIntOutput1"].ToString());
                sec.ExtraIntOutput2 = int.Parse(r["ExtraIntOutput2"].ToString());
                if (!string.IsNullOrEmpty(r["ExtraVarOutput1"].ToString())) { sec.ExtraVarOutput1 = r["ExtraVarOutput1"].ToString(); }
                if (!string.IsNullOrEmpty(r["ExtraVarOutput2"].ToString())) {sec.ExtraVarOutput2 = r["ExtraVarOutput2"].ToString();}
                if (!string.IsNullOrEmpty(r["ExtraVarOutput3"].ToString())) {sec.ExtraVarOutput3 = r["ExtraVarOutput3"].ToString();}
                if (!string.IsNullOrEmpty(r["FirstInitial"].ToString())) {sec.FirstInitial = r["FirstInitial"].ToString();}
                if (!string.IsNullOrEmpty(r["FirstName"].ToString())) {sec.FirstName = r["FirstName"].ToString();}
                if (!string.IsNullOrEmpty(r["GenderInd"].ToString())) {sec.Gender = r["GenderInd"].ToString();}
                if (!string.IsNullOrEmpty(r["IDNo"].ToString())) { sec.IDNo = r["IDNo"].ToString(); }
                sec.KeyID = int.Parse(r["KeyID"].ToString());
                if (!string.IsNullOrEmpty(r["KeyType"].ToString())) {sec.KeyType = r["KeyType"].ToString();}
                if (!string.IsNullOrEmpty(r["PassportNo"].ToString())) {sec.PassportNo = r["PassportNo"].ToString();}
                if (!string.IsNullOrEmpty(r["Reference"].ToString())) {sec.Reference = r["Reference"].ToString();}
                if (!string.IsNullOrEmpty(r["SearchOutput"].ToString())) {sec.SearchOutput = r["SearchOutput"].ToString();}
                if (!string.IsNullOrEmpty(r["SecondInitial"].ToString())) {sec.SecondInitial = r["SecondInitial"].ToString();}
                if (!string.IsNullOrEmpty(r["SecondName"].ToString())) { sec.SecondName = r["SecondName"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_FirstName"].ToString())) { sec.Ha_FirstName = r["Ha_FirstName"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_IDNo"].ToString())) { sec.Ha_IDNo = r["Ha_IDNo"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_SecondName"].ToString())) { sec.Ha_SecondName = r["Ha_SecondName"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_Surname"].ToString())) { sec.Ha_Surname = r["Ha_Surname"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_DeceasedStatus"].ToString())) { sec.Ha_DeceasedStatus = r["Ha_DeceasedStatus"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_DeceasedDate"].ToString())) { sec.Ha_DeceasedDate = r["Ha_DeceasedDate"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_CauseOfDeath"].ToString())) { sec.Ha_CauseOfDeath = r["Ha_CauseOfDeath"].ToString(); }
                if (!string.IsNullOrEmpty(r["Surname"].ToString())) {sec.Surname = r["Surname"].ToString();}
                if (!string.IsNullOrEmpty(r["VoucherCode"].ToString())) {sec.VoucherCode = r["VoucherCode"].ToString();}
                if (!string.IsNullOrEmpty(r["XMLBonus"].ToString())) {sec.XMLBonus = r["XMLBonus"].ToString();}
                if (!string.IsNullOrEmpty(r["XMLData"].ToString())) { sec.XMLData = r["XMLData"].ToString(); }
                if (!string.IsNullOrEmpty(r["ProductID"].ToString())) { sec.ProductID = Convert.ToInt16(r["ProductID"].ToString()); }
                Lsec.Add(sec);
            }

            sqlcmd.Dispose();
            sqlda.Dispose();
            ds.Dispose();
            cn.Close();

            return Lsec;

           
        }

        public List<Entity.SubscriberEnquiryResult> GetSubscriberEnquiryResultListObject(SqlConnection cn, int intSubscriberEnquiryID)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            List<Entity.SubscriberEnquiryResult> Lsec = new List<Entity.SubscriberEnquiryResult>();
            Entity.SubscriberEnquiryResult sec = new Entity.SubscriberEnquiryResult();
            

            string strSQL = "Select * from SubscriberEnquiryResult nolock Where SubscriberEnquiryID = " + intSubscriberEnquiryID.ToString();
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                sec.SubscriberEnquiryResultID = int.Parse(r["SubscriberEnquiryResultID"].ToString());
                sec.SubscriberEnquiryID = int.Parse(r["SubscriberEnquiryID"].ToString());
                sec.Billable = bool.Parse(r["Billable"].ToString());
                sec.BillingPrice = double.Parse(r["BillingPrice"].ToString());
                if (!string.IsNullOrEmpty(r["BillingTypeID"].ToString())) { sec.BillingTypeID = int.Parse(r["BillingTypeID"].ToString()); }
                if (!string.IsNullOrEmpty(r["BirthDate"].ToString())) { sec.BirthDate = DateTime.Parse(r["BirthDate"].ToString()); }
                sec.BonusIncluded = bool.Parse(r["BonusIncluded"].ToString());
                if (!string.IsNullOrEmpty(r["BusBusinessName"].ToString())) { sec.BusBusinessName = r["BusBusinessName"].ToString(); }
                if (!string.IsNullOrEmpty(r["BusRegistrationNo"].ToString())) { sec.BusRegistrationNo = r["BusRegistrationNo"].ToString(); }
                sec.ChangedByUser = r["ChangedByUser"].ToString();
                if (!string.IsNullOrEmpty(r["ChangedOnDate"].ToString())) { sec.ChangedOnDate = DateTime.Parse(r["ChangedOnDate"].ToString()); }
                sec.CreatedByUser = r["CreatedByUser"].ToString();
                if (!string.IsNullOrEmpty(r["CreatedOnDate"].ToString())) { sec.CreatedOnDate = DateTime.Parse(r["CreatedOnDate"].ToString()); }
                if (!string.IsNullOrEmpty(r["DetailsViewedDate"].ToString())) { sec.DetailsViewedDate = DateTime.Parse(r["DetailsViewedDate"].ToString()); }
                sec.DetailsViewedYN = bool.Parse(r["CreatedByUser"].ToString());
                sec.EnquiryResult = r["EnquiryResult"].ToString();
                sec.ExtraIntOutput1 = int.Parse(r["ExtraIntOutput1"].ToString());
                sec.ExtraIntOutput2 = int.Parse(r["ExtraIntOutput2"].ToString());
                if (!string.IsNullOrEmpty(r["ExtraVarOutput1"].ToString())) { sec.ExtraVarOutput1 = r["ExtraVarOutput1"].ToString(); }
                if (!string.IsNullOrEmpty(r["ExtraVarOutput2"].ToString())) { sec.ExtraVarOutput2 = r["ExtraVarOutput2"].ToString(); }
                if (!string.IsNullOrEmpty(r["ExtraVarOutput3"].ToString())) { sec.ExtraVarOutput3 = r["ExtraVarOutput3"].ToString(); }
                if (!string.IsNullOrEmpty(r["FirstInitial"].ToString())) { sec.FirstInitial = r["FirstInitial"].ToString(); }
                if (!string.IsNullOrEmpty(r["FirstName"].ToString())) { sec.FirstName = r["FirstName"].ToString(); }
                if (!string.IsNullOrEmpty(r["GenderInd"].ToString())) { sec.Gender = r["GenderInd"].ToString(); }
                if (!string.IsNullOrEmpty(r["IDNo"].ToString())) { sec.IDNo = r["IDNo"].ToString(); }
                sec.KeyID = int.Parse(r["KeyID"].ToString());
                if (!string.IsNullOrEmpty(r["KeyType"].ToString())) { sec.KeyType = r["KeyType"].ToString(); }
                if (!string.IsNullOrEmpty(r["PassportNo"].ToString())) { sec.PassportNo = r["PassportNo"].ToString(); }
                if (!string.IsNullOrEmpty(r["Reference"].ToString())) { sec.Reference = r["Reference"].ToString(); }
                if (!string.IsNullOrEmpty(r["SearchOutput"].ToString())) { sec.SearchOutput = r["SearchOutput"].ToString(); }
                if (!string.IsNullOrEmpty(r["SecondInitial"].ToString())) { sec.SecondInitial = r["SecondInitial"].ToString(); }
                if (!string.IsNullOrEmpty(r["SecondName"].ToString())) { sec.SecondName = r["SecondName"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_FirstName"].ToString())) { sec.Ha_FirstName = r["Ha_FirstName"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_IDNo"].ToString())) { sec.Ha_IDNo = r["Ha_IDNo"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_SecondName"].ToString())) { sec.Ha_SecondName = r["Ha_SecondName"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_Surname"].ToString())) { sec.Ha_Surname = r["Ha_Surname"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_DeceasedStatus"].ToString())) { sec.Ha_DeceasedStatus = r["Ha_DeceasedStatus"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_DeceasedDate"].ToString())) { sec.Ha_DeceasedDate = r["Ha_DeceasedDate"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_CauseOfDeath"].ToString())) { sec.Ha_CauseOfDeath = r["Ha_CauseOfDeath"].ToString(); }
                if (!string.IsNullOrEmpty(r["Surname"].ToString())) { sec.Surname = r["Surname"].ToString(); }
                if (!string.IsNullOrEmpty(r["VoucherCode"].ToString())) { sec.VoucherCode = r["VoucherCode"].ToString(); }
                if (!string.IsNullOrEmpty(r["XMLBonus"].ToString())) { sec.XMLBonus = r["XMLBonus"].ToString(); }
                if (!string.IsNullOrEmpty(r["XMLData"].ToString())) { sec.XMLData = r["XMLData"].ToString(); }
                if (!string.IsNullOrEmpty(r["ProductID"].ToString())) { sec.ProductID = Convert.ToInt16(r["ProductID"].ToString()); }
                Lsec.Add(sec);
            }

            sqlcmd.Dispose();
            sqlda.Dispose();
            ds.Dispose();
            cn.Close();

            return Lsec;


        }

        public Entity.SubscriberEnquiryResult GetSubscriberEnquiryResultObject(string DBConnection, int intSubscriberEnquiryResultID)
        {
            Entity.SubscriberEnquiryResult sec = new Entity.SubscriberEnquiryResult();
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = DBConnection;
            cn.Open();

            string strSQL = "Select * from SubscriberEnquiryResult nolock Where SubscriberEnquiryResultID = " + intSubscriberEnquiryResultID.ToString();
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                sec.SubscriberEnquiryResultID = int.Parse(r["SubscriberEnquiryResultID"].ToString());
                sec.SubscriberEnquiryID = int.Parse(r["SubscriberEnquiryID"].ToString());
                sec.Billable = bool.Parse(r["Billable"].ToString());
                sec.BillingPrice = double.Parse(r["BillingPrice"].ToString());
                if (!string.IsNullOrEmpty(r["BillingTypeID"].ToString())) { sec.BillingTypeID = int.Parse(r["BillingTypeID"].ToString()); }
                if (!string.IsNullOrEmpty(r["BirthDate"].ToString())) { sec.BirthDate = DateTime.Parse(r["BirthDate"].ToString()); }
                sec.BonusIncluded = bool.Parse(r["BonusIncluded"].ToString());
                if (!string.IsNullOrEmpty(r["BusBusinessName"].ToString())) { sec.BusBusinessName = r["BusBusinessName"].ToString(); }
                if (!string.IsNullOrEmpty(r["BusRegistrationNo"].ToString())) { sec.BusRegistrationNo = r["BusRegistrationNo"].ToString(); }
                sec.ChangedByUser = r["ChangedByUser"].ToString();
                if (!string.IsNullOrEmpty(r["ChangedOnDate"].ToString())) { sec.ChangedOnDate = DateTime.Parse(r["ChangedOnDate"].ToString()); }
                sec.CreatedByUser = r["CreatedByUser"].ToString();
                if (!string.IsNullOrEmpty(r["CreatedOnDate"].ToString())) { sec.CreatedOnDate = DateTime.Parse(r["CreatedOnDate"].ToString()); }
                if (!string.IsNullOrEmpty(r["DetailsViewedDate"].ToString())) { sec.DetailsViewedDate = DateTime.Parse(r["DetailsViewedDate"].ToString()); }
                sec.DetailsViewedYN = bool.Parse(r["DetailsViewedYN"].ToString());
                sec.EnquiryResult = r["EnquiryResultInd"].ToString();
                sec.ExtraIntOutput1 = int.Parse(r["ExtraIntOutput1"].ToString());
                sec.ExtraIntOutput2 = int.Parse(r["ExtraIntOutput2"].ToString());
                if (!string.IsNullOrEmpty(r["ExtraVarOutput1"].ToString())) { sec.ExtraVarOutput1 = r["ExtraVarOutput1"].ToString(); }
                if (!string.IsNullOrEmpty(r["ExtraVarOutput2"].ToString())) { sec.ExtraVarOutput2 = r["ExtraVarOutput2"].ToString(); }
                if (!string.IsNullOrEmpty(r["ExtraVarOutput3"].ToString())) { sec.ExtraVarOutput3 = r["ExtraVarOutput3"].ToString(); }
                if (!string.IsNullOrEmpty(r["FirstInitial"].ToString())) { sec.FirstInitial = r["FirstInitial"].ToString(); }
                if (!string.IsNullOrEmpty(r["FirstName"].ToString())) { sec.FirstName = r["FirstName"].ToString(); }
                if (!string.IsNullOrEmpty(r["GenderInd"].ToString())) { sec.Gender = r["GenderInd"].ToString(); }
                if (!string.IsNullOrEmpty(r["IDNo"].ToString())) { sec.IDNo = r["IDNo"].ToString(); }
                sec.KeyID = int.Parse(r["KeyID"].ToString());
                if (!string.IsNullOrEmpty(r["KeyType"].ToString())) { sec.KeyType = r["KeyType"].ToString(); }
                if (!string.IsNullOrEmpty(r["PassportNo"].ToString())) { sec.PassportNo = r["PassportNo"].ToString(); }
                if (!string.IsNullOrEmpty(r["Reference"].ToString())) { sec.Reference = r["Reference"].ToString(); }
                if (!string.IsNullOrEmpty(r["SearchOutput"].ToString())) { sec.SearchOutput = r["SearchOutput"].ToString(); }
                if (!string.IsNullOrEmpty(r["SecondInitial"].ToString())) { sec.SecondInitial = r["SecondInitial"].ToString(); }
                if (!string.IsNullOrEmpty(r["SecondName"].ToString())) { sec.SecondName = r["SecondName"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_FirstName"].ToString())) { sec.Ha_FirstName = r["Ha_FirstName"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_IDNo"].ToString())) { sec.Ha_IDNo = r["Ha_IDNo"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_SecondName"].ToString())) { sec.Ha_SecondName = r["Ha_SecondName"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_Surname"].ToString())) { sec.Ha_Surname = r["Ha_Surname"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_DeceasedStatus"].ToString())) { sec.Ha_DeceasedStatus = r["Ha_DeceasedStatus"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_DeceasedDate"].ToString())) { sec.Ha_DeceasedDate = r["Ha_DeceasedDate"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_CauseOfDeath"].ToString())) { sec.Ha_CauseOfDeath = r["Ha_CauseOfDeath"].ToString(); }
                if (!string.IsNullOrEmpty(r["Surname"].ToString())) { sec.Surname = r["Surname"].ToString(); }
                if (!string.IsNullOrEmpty(r["VoucherCode"].ToString())) { sec.VoucherCode = r["VoucherCode"].ToString(); }
                if (!string.IsNullOrEmpty(r["XMLBonus"].ToString())) { sec.XMLBonus = r["XMLBonus"].ToString(); }
                if (!string.IsNullOrEmpty(r["XMLData"].ToString())) { sec.XMLData = r["XMLData"].ToString(); }
                if (!string.IsNullOrEmpty(r["ProductID"].ToString())) { sec.ProductID = Convert.ToInt16(r["ProductID"].ToString()); }
            }

            sqlcmd.Dispose();
            sqlda.Dispose();
            ds.Dispose();
            cn.Close();

            return sec;


        }

        public Entity.SubscriberEnquiryResult GetSubscriberEnquiryResultObject(SqlConnection cn, int intSubscriberEnquiryResultID)
        {

            if (cn.State == ConnectionState.Closed)
                cn.Open();

            Entity.SubscriberEnquiryResult sec = new Entity.SubscriberEnquiryResult();

            string strSQL = "Select * from SubscriberEnquiryResult nolock Where SubscriberEnquiryResultID = " + intSubscriberEnquiryResultID.ToString();
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                sec.SubscriberEnquiryResultID = int.Parse(r["SubscriberEnquiryResultID"].ToString());
                sec.SubscriberEnquiryID = int.Parse(r["SubscriberEnquiryID"].ToString());
                sec.Billable = bool.Parse(r["Billable"].ToString());
                sec.BillingPrice = double.Parse(r["BillingPrice"].ToString());
                if (!string.IsNullOrEmpty(r["BillingTypeID"].ToString())) { sec.BillingTypeID = int.Parse(r["BillingTypeID"].ToString()); }
                if (!string.IsNullOrEmpty(r["BirthDate"].ToString())) { sec.BirthDate = DateTime.Parse(r["BirthDate"].ToString()); }
                sec.BonusIncluded = bool.Parse(r["BonusIncluded"].ToString());
                if (!string.IsNullOrEmpty(r["BusBusinessName"].ToString())) { sec.BusBusinessName = r["BusBusinessName"].ToString(); }
                if (!string.IsNullOrEmpty(r["BusRegistrationNo"].ToString())) { sec.BusRegistrationNo = r["BusRegistrationNo"].ToString(); }
                sec.ChangedByUser = r["ChangedByUser"].ToString();
                if (!string.IsNullOrEmpty(r["ChangedOnDate"].ToString())) { sec.ChangedOnDate = DateTime.Parse(r["ChangedOnDate"].ToString()); }
                sec.CreatedByUser = r["CreatedByUser"].ToString();
                if (!string.IsNullOrEmpty(r["CreatedOnDate"].ToString())) { sec.CreatedOnDate = DateTime.Parse(r["CreatedOnDate"].ToString()); }
                if (!string.IsNullOrEmpty(r["DetailsViewedDate"].ToString())) { sec.DetailsViewedDate = DateTime.Parse(r["DetailsViewedDate"].ToString()); }
                sec.DetailsViewedYN = bool.Parse(r["DetailsViewedYN"].ToString());
                sec.EnquiryResult = r["EnquiryResultInd"].ToString();
                sec.ExtraIntOutput1 = int.Parse(r["ExtraIntOutput1"].ToString());
                sec.ExtraIntOutput2 = int.Parse(r["ExtraIntOutput2"].ToString());
                if (!string.IsNullOrEmpty(r["ExtraVarOutput1"].ToString())) { sec.ExtraVarOutput1 = r["ExtraVarOutput1"].ToString(); } else { sec.ExtraVarOutput1 = string.Empty; }
                if (!string.IsNullOrEmpty(r["ExtraVarOutput2"].ToString())) { sec.ExtraVarOutput2 = r["ExtraVarOutput2"].ToString(); } else { sec.ExtraVarOutput2 = string.Empty; }
                if (!string.IsNullOrEmpty(r["ExtraVarOutput3"].ToString())) { sec.ExtraVarOutput3 = r["ExtraVarOutput3"].ToString(); } else { sec.ExtraVarOutput3 = string.Empty; }
                if (!string.IsNullOrEmpty(r["FirstInitial"].ToString())) { sec.FirstInitial = r["FirstInitial"].ToString(); }
                if (!string.IsNullOrEmpty(r["FirstName"].ToString())) { sec.FirstName = r["FirstName"].ToString(); }
                if (!string.IsNullOrEmpty(r["GenderInd"].ToString())) { sec.Gender = r["GenderInd"].ToString(); }
                if (!string.IsNullOrEmpty(r["IDNo"].ToString())) { sec.IDNo = r["IDNo"].ToString(); }
                sec.KeyID = int.Parse(r["KeyID"].ToString());
                if (!string.IsNullOrEmpty(r["KeyType"].ToString())) { sec.KeyType = r["KeyType"].ToString(); }
                if (!string.IsNullOrEmpty(r["PassportNo"].ToString())) { sec.PassportNo = r["PassportNo"].ToString(); }
                if (!string.IsNullOrEmpty(r["Reference"].ToString())) { sec.Reference = r["Reference"].ToString(); }
                if (!string.IsNullOrEmpty(r["SearchOutput"].ToString())) { sec.SearchOutput = r["SearchOutput"].ToString(); }
                if (!string.IsNullOrEmpty(r["SecondInitial"].ToString())) { sec.SecondInitial = r["SecondInitial"].ToString(); }
                if (!string.IsNullOrEmpty(r["SecondName"].ToString())) { sec.SecondName = r["SecondName"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_FirstName"].ToString())) { sec.Ha_FirstName = r["Ha_FirstName"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_IDNo"].ToString())) { sec.Ha_IDNo = r["Ha_IDNo"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_SecondName"].ToString())) { sec.Ha_SecondName = r["Ha_SecondName"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_Surname"].ToString())) { sec.Ha_Surname = r["Ha_Surname"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_DeceasedStatus"].ToString())) { sec.Ha_DeceasedStatus = r["Ha_DeceasedStatus"].ToString(); }
                //if (!string.IsNullOrEmpty(r["Ha_DeceasedDate"].ToString())) { sec.Ha_DeceasedDate = r["Ha_DeceasedDate"].ToString(); }
                if (!String.IsNullOrEmpty(r["Ha_DeceasedDate"].ToString()))
                {
                    try
                    {
                        sec.Ha_DeceasedDate = Convert.ToDateTime(r["Ha_DeceasedDate"].ToString()).ToString("yyyy/MM/dd");
                    }
                    catch
                    {
                        sec.Ha_DeceasedDate = r["Ha_DeceasedDate"].ToString();
                    }
                }
                if (!string.IsNullOrEmpty(r["Ha_CauseOfDeath"].ToString())) { sec.Ha_CauseOfDeath = r["Ha_CauseOfDeath"].ToString(); }
                if (!string.IsNullOrEmpty(r["Surname"].ToString())) { sec.Surname = r["Surname"].ToString(); }
                if (!string.IsNullOrEmpty(r["VoucherCode"].ToString())) { sec.VoucherCode = r["VoucherCode"].ToString(); }
                if (!string.IsNullOrEmpty(r["XMLBonus"].ToString())) { sec.XMLBonus = r["XMLBonus"].ToString(); }
                if (!string.IsNullOrEmpty(r["XMLData"].ToString())) { sec.XMLData = r["XMLData"].ToString(); }
                if (!string.IsNullOrEmpty(r["ProductID"].ToString())) { sec.ProductID = Convert.ToInt16(r["ProductID"].ToString()); }
            }

            sqlcmd.Dispose();
            sqlda.Dispose();
            ds.Dispose();
            cn.Close();

            return sec;

        }

        public Entity.SubscriberEnquiryResult GetSubscriberEnquiryResultObject(SqlConnection cn, int SubscriberID, string Username, int SubscriberEnquiryID, int intSubscriberEnquiryResultID)
        {
            string Result = "0";

            if (cn.State == ConnectionState.Closed)
                cn.Open();

            Entity.SubscriberEnquiryResult sec = new Entity.SubscriberEnquiryResult();
            
            using (SqlCommand cmd = new SqlCommand("spValidateSearchHistoryReq", cn))
            {
                
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@SubscriberID", SqlDbType.BigInt).Value = SubscriberID;
                cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = Username;
                cmd.Parameters.Add("@SubscriberEnquiryID", SqlDbType.BigInt).Value = SubscriberEnquiryID;
                cmd.Parameters.Add("@SubscriberEnquiryResultID", SqlDbType.BigInt).Value = intSubscriberEnquiryResultID;
                
                Result = cmd.ExecuteScalar().ToString();
                
            }

            if (Result == "1")
            {
                string strSQL = "Select * from SubscriberEnquiryResult nolock Where SubscriberEnquiryResultID = " + intSubscriberEnquiryResultID.ToString();
                SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
                SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
                DataSet ds = new DataSet();
                sqlda.Fill(ds);

                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    sec.SubscriberEnquiryResultID = int.Parse(r["SubscriberEnquiryResultID"].ToString());
                    sec.SubscriberEnquiryID = int.Parse(r["SubscriberEnquiryID"].ToString());
                    sec.Billable = bool.Parse(r["Billable"].ToString());
                    sec.BillingPrice = double.Parse(r["BillingPrice"].ToString());
                    if (!string.IsNullOrEmpty(r["BillingTypeID"].ToString())) { sec.BillingTypeID = int.Parse(r["BillingTypeID"].ToString()); }
                    if (!string.IsNullOrEmpty(r["BirthDate"].ToString())) { sec.BirthDate = DateTime.Parse(r["BirthDate"].ToString()); }
                    sec.BonusIncluded = bool.Parse(r["BonusIncluded"].ToString());
                    if (!string.IsNullOrEmpty(r["BusBusinessName"].ToString())) { sec.BusBusinessName = r["BusBusinessName"].ToString(); }
                    if (!string.IsNullOrEmpty(r["BusRegistrationNo"].ToString())) { sec.BusRegistrationNo = r["BusRegistrationNo"].ToString(); }
                    sec.ChangedByUser = r["ChangedByUser"].ToString();
                    if (!string.IsNullOrEmpty(r["ChangedOnDate"].ToString())) { sec.ChangedOnDate = DateTime.Parse(r["ChangedOnDate"].ToString()); }
                    sec.CreatedByUser = r["CreatedByUser"].ToString();
                    if (!string.IsNullOrEmpty(r["CreatedOnDate"].ToString())) { sec.CreatedOnDate = DateTime.Parse(r["CreatedOnDate"].ToString()); }
                    if (!string.IsNullOrEmpty(r["DetailsViewedDate"].ToString())) { sec.DetailsViewedDate = DateTime.Parse(r["DetailsViewedDate"].ToString()); }
                    sec.DetailsViewedYN = bool.Parse(r["DetailsViewedYN"].ToString());
                    sec.EnquiryResult = r["EnquiryResultInd"].ToString();
                    sec.ExtraIntOutput1 = int.Parse(r["ExtraIntOutput1"].ToString());
                    sec.ExtraIntOutput2 = int.Parse(r["ExtraIntOutput2"].ToString());
                    if (!string.IsNullOrEmpty(r["ExtraVarOutput1"].ToString())) { sec.ExtraVarOutput1 = r["ExtraVarOutput1"].ToString(); } else { sec.ExtraVarOutput1 = string.Empty; }
                    if (!string.IsNullOrEmpty(r["ExtraVarOutput2"].ToString())) { sec.ExtraVarOutput2 = r["ExtraVarOutput2"].ToString(); } else { sec.ExtraVarOutput2 = string.Empty; }
                    if (!string.IsNullOrEmpty(r["ExtraVarOutput3"].ToString())) { sec.ExtraVarOutput3 = r["ExtraVarOutput3"].ToString(); } else { sec.ExtraVarOutput3 = string.Empty; }
                    if (!string.IsNullOrEmpty(r["FirstInitial"].ToString())) { sec.FirstInitial = r["FirstInitial"].ToString(); }
                    if (!string.IsNullOrEmpty(r["FirstName"].ToString())) { sec.FirstName = r["FirstName"].ToString(); }
                    if (!string.IsNullOrEmpty(r["GenderInd"].ToString())) { sec.Gender = r["GenderInd"].ToString(); }
                    if (!string.IsNullOrEmpty(r["IDNo"].ToString())) { sec.IDNo = r["IDNo"].ToString(); }
                    sec.KeyID = int.Parse(r["KeyID"].ToString());
                    if (!string.IsNullOrEmpty(r["KeyType"].ToString())) { sec.KeyType = r["KeyType"].ToString(); }
                    if (!string.IsNullOrEmpty(r["PassportNo"].ToString())) { sec.PassportNo = r["PassportNo"].ToString(); }
                    if (!string.IsNullOrEmpty(r["Reference"].ToString())) { sec.Reference = r["Reference"].ToString(); }
                    if (!string.IsNullOrEmpty(r["SearchOutput"].ToString())) { sec.SearchOutput = r["SearchOutput"].ToString(); }
                    if (!string.IsNullOrEmpty(r["SecondInitial"].ToString())) { sec.SecondInitial = r["SecondInitial"].ToString(); }
                    if (!string.IsNullOrEmpty(r["SecondName"].ToString())) { sec.SecondName = r["SecondName"].ToString(); }
                    if (!string.IsNullOrEmpty(r["Ha_FirstName"].ToString())) { sec.Ha_FirstName = r["Ha_FirstName"].ToString(); }
                    if (!string.IsNullOrEmpty(r["Ha_IDNo"].ToString())) { sec.Ha_IDNo = r["Ha_IDNo"].ToString(); }
                    if (!string.IsNullOrEmpty(r["Ha_SecondName"].ToString())) { sec.Ha_SecondName = r["Ha_SecondName"].ToString(); }
                    if (!string.IsNullOrEmpty(r["Ha_Surname"].ToString())) { sec.Ha_Surname = r["Ha_Surname"].ToString(); }
                    if (!string.IsNullOrEmpty(r["Ha_DeceasedStatus"].ToString())) { sec.Ha_DeceasedStatus = r["Ha_DeceasedStatus"].ToString(); }
                    //if (!string.IsNullOrEmpty(r["Ha_DeceasedDate"].ToString())) { sec.Ha_DeceasedDate = r["Ha_DeceasedDate"].ToString(); }
                    if (!String.IsNullOrEmpty(r["Ha_DeceasedDate"].ToString()))
                    {
                        try
                        {
                            sec.Ha_DeceasedDate = Convert.ToDateTime(r["Ha_DeceasedDate"].ToString()).ToString("yyyy/MM/dd");
                        }
                        catch
                        {
                            sec.Ha_DeceasedDate = r["Ha_DeceasedDate"].ToString();
                        }
                    }
                    if (!string.IsNullOrEmpty(r["Ha_CauseOfDeath"].ToString())) { sec.Ha_CauseOfDeath = r["Ha_CauseOfDeath"].ToString(); }
                    if (!string.IsNullOrEmpty(r["Surname"].ToString())) { sec.Surname = r["Surname"].ToString(); }
                    if (!string.IsNullOrEmpty(r["VoucherCode"].ToString())) { sec.VoucherCode = r["VoucherCode"].ToString(); }
                    if (!string.IsNullOrEmpty(r["XMLBonus"].ToString())) { sec.XMLBonus = r["XMLBonus"].ToString(); }
                    if (!string.IsNullOrEmpty(r["XMLData"].ToString())) { sec.XMLData = r["XMLData"].ToString(); }
                    if (!string.IsNullOrEmpty(r["ProductID"].ToString())) { sec.ProductID = Convert.ToInt16(r["ProductID"].ToString()); }
                }

                sqlcmd.Dispose();
                sqlda.Dispose();
                ds.Dispose();
                cn.Close();
            }

            return sec;

        }

        public Entity.SubscriberEnquiryResult GetSubscriberAuthenticationEnquiryResultObject(SqlConnection cn, int intSubscriberAuthenticationID)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();

            Entity.SubscriberEnquiryResult sec = new Entity.SubscriberEnquiryResult();

            string strSQL = "Select * from SubscriberEnquiryResult nolock Where ExtraIntOutput1 = " + intSubscriberAuthenticationID.ToString();
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                sec.SubscriberEnquiryResultID = int.Parse(r["SubscriberEnquiryResultID"].ToString());
                sec.SubscriberEnquiryID = int.Parse(r["SubscriberEnquiryID"].ToString());
                sec.Billable = bool.Parse(r["Billable"].ToString());
                sec.BillingPrice = double.Parse(r["BillingPrice"].ToString());
                if (!string.IsNullOrEmpty(r["BillingTypeID"].ToString())) { sec.BillingTypeID = int.Parse(r["BillingTypeID"].ToString()); }
                if (!string.IsNullOrEmpty(r["BirthDate"].ToString())) { sec.BirthDate = DateTime.Parse(r["BirthDate"].ToString()); }
                sec.BonusIncluded = bool.Parse(r["BonusIncluded"].ToString());
                if (!string.IsNullOrEmpty(r["BusBusinessName"].ToString())) { sec.BusBusinessName = r["BusBusinessName"].ToString(); }
                if (!string.IsNullOrEmpty(r["BusRegistrationNo"].ToString())) { sec.BusRegistrationNo = r["BusRegistrationNo"].ToString(); }
                sec.ChangedByUser = r["ChangedByUser"].ToString();
                if (!string.IsNullOrEmpty(r["ChangedOnDate"].ToString())) { sec.ChangedOnDate = DateTime.Parse(r["ChangedOnDate"].ToString()); }
                sec.CreatedByUser = r["CreatedByUser"].ToString();
                if (!string.IsNullOrEmpty(r["CreatedOnDate"].ToString())) { sec.CreatedOnDate = DateTime.Parse(r["CreatedOnDate"].ToString()); }
                if (!string.IsNullOrEmpty(r["DetailsViewedDate"].ToString())) { sec.DetailsViewedDate = DateTime.Parse(r["DetailsViewedDate"].ToString()); }
                sec.DetailsViewedYN = bool.Parse(r["DetailsViewedYN"].ToString());
                sec.EnquiryResult = r["EnquiryResultInd"].ToString();
                sec.ExtraIntOutput1 = int.Parse(r["ExtraIntOutput1"].ToString());
                sec.ExtraIntOutput2 = int.Parse(r["ExtraIntOutput2"].ToString());
                if (!string.IsNullOrEmpty(r["ExtraVarOutput1"].ToString())) { sec.ExtraVarOutput1 = r["ExtraVarOutput1"].ToString(); }
                if (!string.IsNullOrEmpty(r["ExtraVarOutput2"].ToString())) { sec.ExtraVarOutput2 = r["ExtraVarOutput2"].ToString(); }
                if (!string.IsNullOrEmpty(r["ExtraVarOutput3"].ToString())) { sec.ExtraVarOutput3 = r["ExtraVarOutput3"].ToString(); }
                if (!string.IsNullOrEmpty(r["FirstInitial"].ToString())) { sec.FirstInitial = r["FirstInitial"].ToString(); }
                if (!string.IsNullOrEmpty(r["FirstName"].ToString())) { sec.FirstName = r["FirstName"].ToString(); }
                if (!string.IsNullOrEmpty(r["GenderInd"].ToString())) { sec.Gender = r["GenderInd"].ToString(); }
                if (!string.IsNullOrEmpty(r["IDNo"].ToString())) { sec.IDNo = r["IDNo"].ToString(); }
                sec.KeyID = int.Parse(r["KeyID"].ToString());
                if (!string.IsNullOrEmpty(r["KeyType"].ToString())) { sec.KeyType = r["KeyType"].ToString(); }
                if (!string.IsNullOrEmpty(r["PassportNo"].ToString())) { sec.PassportNo = r["PassportNo"].ToString(); }
                if (!string.IsNullOrEmpty(r["Reference"].ToString())) { sec.Reference = r["Reference"].ToString(); }
                if (!string.IsNullOrEmpty(r["SearchOutput"].ToString())) { sec.SearchOutput = r["SearchOutput"].ToString(); }
                if (!string.IsNullOrEmpty(r["SecondInitial"].ToString())) { sec.SecondInitial = r["SecondInitial"].ToString(); }
                if (!string.IsNullOrEmpty(r["SecondName"].ToString())) { sec.SecondName = r["SecondName"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_FirstName"].ToString())) { sec.Ha_FirstName = r["Ha_FirstName"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_IDNo"].ToString())) { sec.Ha_IDNo = r["Ha_IDNo"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_SecondName"].ToString())) { sec.Ha_SecondName = r["Ha_SecondName"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_Surname"].ToString())) { sec.Ha_Surname = r["Ha_Surname"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_DeceasedStatus"].ToString())) { sec.Ha_DeceasedStatus = r["Ha_DeceasedStatus"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_DeceasedDate"].ToString())) { sec.Ha_DeceasedDate = r["Ha_DeceasedDate"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_CauseOfDeath"].ToString())) { sec.Ha_CauseOfDeath = r["Ha_CauseOfDeath"].ToString(); }
                if (!string.IsNullOrEmpty(r["Surname"].ToString())) { sec.Surname = r["Surname"].ToString(); }
                if (!string.IsNullOrEmpty(r["VoucherCode"].ToString())) { sec.VoucherCode = r["VoucherCode"].ToString(); }
                if (!string.IsNullOrEmpty(r["XMLBonus"].ToString())) { sec.XMLBonus = r["XMLBonus"].ToString(); }
                if (!string.IsNullOrEmpty(r["XMLData"].ToString())) { sec.XMLData = r["XMLData"].ToString(); }
                if (!string.IsNullOrEmpty(r["ProductID"].ToString())) { sec.ProductID = Convert.ToInt16(r["ProductID"].ToString()); }
            }

            sqlcmd.Dispose();
            sqlda.Dispose();
            ds.Dispose();
            cn.Close();

            return sec;

        }

        public int InsertSubscriberEnquiryResultRequest(SqlConnection cn, int SubscriberEnquiryID, int SubscriberEnquiryResultID, int ProductID, DateTime CreatedDate)
        {

            if (cn.State == ConnectionState.Closed)
                cn.Open();

            string insertSQL = "sp_InsertSubscriberEnquiryResultRequest " + SubscriberEnquiryID + " , " + SubscriberEnquiryResultID + " , " + ProductID + " , '" + CreatedDate + "' ";
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();

            string strSQL = "SELECT @@IDENTITY";
            SqlCommand Imycommand = new SqlCommand(strSQL, cn);
            SqlDataReader Idr = Imycommand.ExecuteReader();
            Idr.Read();
            int intSubscriberEnquiryResultID = Convert.ToInt32(Idr.GetValue(0));
            Idr.Close();
            cn.Close();

            sqlcmd.Dispose();
            Imycommand.Dispose();
            Idr.Dispose();
            return intSubscriberEnquiryResultID;
        }

        public Entity.SubscriberEnquiryResult GetSubscriberEnquiryResultObjectBySubscriberEnquiryID(SqlConnection cn, int intSubscriberEnquiryID)
        {

            if (cn.State == ConnectionState.Closed)
                cn.Open();

            Entity.SubscriberEnquiryResult sec = new Entity.SubscriberEnquiryResult();

            string strSQL = "Select * from SubscriberEnquiryResult nolock Where SubscriberEnquiryID = " + intSubscriberEnquiryID.ToString();
            SqlCommand sqlcmd = new SqlCommand(strSQL, cn);
            SqlDataAdapter sqlda = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            sqlda.Fill(ds);

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                sec.SubscriberEnquiryResultID = int.Parse(r["SubscriberEnquiryResultID"].ToString());
                sec.SubscriberEnquiryID = int.Parse(r["SubscriberEnquiryID"].ToString());
                sec.Billable = bool.Parse(r["Billable"].ToString());
                sec.BillingPrice = double.Parse(r["BillingPrice"].ToString());
                if (!string.IsNullOrEmpty(r["BillingTypeID"].ToString())) { sec.BillingTypeID = int.Parse(r["BillingTypeID"].ToString()); }
                if (!string.IsNullOrEmpty(r["BirthDate"].ToString())) { sec.BirthDate = DateTime.Parse(r["BirthDate"].ToString()); }
                sec.BonusIncluded = bool.Parse(r["BonusIncluded"].ToString());
                if (!string.IsNullOrEmpty(r["BusBusinessName"].ToString())) { sec.BusBusinessName = r["BusBusinessName"].ToString(); }
                if (!string.IsNullOrEmpty(r["BusRegistrationNo"].ToString())) { sec.BusRegistrationNo = r["BusRegistrationNo"].ToString(); }
                sec.ChangedByUser = r["ChangedByUser"].ToString();
                if (!string.IsNullOrEmpty(r["ChangedOnDate"].ToString())) { sec.ChangedOnDate = DateTime.Parse(r["ChangedOnDate"].ToString()); }
                sec.CreatedByUser = r["CreatedByUser"].ToString();
                if (!string.IsNullOrEmpty(r["CreatedOnDate"].ToString())) { sec.CreatedOnDate = DateTime.Parse(r["CreatedOnDate"].ToString()); }
                if (!string.IsNullOrEmpty(r["DetailsViewedDate"].ToString())) { sec.DetailsViewedDate = DateTime.Parse(r["DetailsViewedDate"].ToString()); }
                sec.DetailsViewedYN = bool.Parse(r["DetailsViewedYN"].ToString());
                sec.EnquiryResult = r["EnquiryResultInd"].ToString();
                sec.ExtraIntOutput1 = int.Parse(r["ExtraIntOutput1"].ToString());
                sec.ExtraIntOutput2 = int.Parse(r["ExtraIntOutput2"].ToString());
                if (!string.IsNullOrEmpty(r["ExtraVarOutput1"].ToString())) { sec.ExtraVarOutput1 = r["ExtraVarOutput1"].ToString(); } else { sec.ExtraVarOutput1 = string.Empty; }
                if (!string.IsNullOrEmpty(r["ExtraVarOutput2"].ToString())) { sec.ExtraVarOutput2 = r["ExtraVarOutput2"].ToString(); } else { sec.ExtraVarOutput2 = string.Empty; }
                if (!string.IsNullOrEmpty(r["ExtraVarOutput3"].ToString())) { sec.ExtraVarOutput3 = r["ExtraVarOutput3"].ToString(); } else { sec.ExtraVarOutput3 = string.Empty; }
                if (!string.IsNullOrEmpty(r["FirstInitial"].ToString())) { sec.FirstInitial = r["FirstInitial"].ToString(); }
                if (!string.IsNullOrEmpty(r["FirstName"].ToString())) { sec.FirstName = r["FirstName"].ToString(); }
                if (!string.IsNullOrEmpty(r["GenderInd"].ToString())) { sec.Gender = r["GenderInd"].ToString(); }
                if (!string.IsNullOrEmpty(r["IDNo"].ToString())) { sec.IDNo = r["IDNo"].ToString(); }
                sec.KeyID = int.Parse(r["KeyID"].ToString());
                if (!string.IsNullOrEmpty(r["KeyType"].ToString())) { sec.KeyType = r["KeyType"].ToString(); }
                if (!string.IsNullOrEmpty(r["PassportNo"].ToString())) { sec.PassportNo = r["PassportNo"].ToString(); }
                if (!string.IsNullOrEmpty(r["Reference"].ToString())) { sec.Reference = r["Reference"].ToString(); }
                if (!string.IsNullOrEmpty(r["SearchOutput"].ToString())) { sec.SearchOutput = r["SearchOutput"].ToString(); }
                if (!string.IsNullOrEmpty(r["SecondInitial"].ToString())) { sec.SecondInitial = r["SecondInitial"].ToString(); }
                if (!string.IsNullOrEmpty(r["SecondName"].ToString())) { sec.SecondName = r["SecondName"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_FirstName"].ToString())) { sec.Ha_FirstName = r["Ha_FirstName"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_IDNo"].ToString())) { sec.Ha_IDNo = r["Ha_IDNo"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_SecondName"].ToString())) { sec.Ha_SecondName = r["Ha_SecondName"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_Surname"].ToString())) { sec.Ha_Surname = r["Ha_Surname"].ToString(); }
                if (!string.IsNullOrEmpty(r["Ha_DeceasedStatus"].ToString())) { sec.Ha_DeceasedStatus = r["Ha_DeceasedStatus"].ToString(); }
                //if (!string.IsNullOrEmpty(r["Ha_DeceasedDate"].ToString())) { sec.Ha_DeceasedDate = r["Ha_DeceasedDate"].ToString(); }
                if (!String.IsNullOrEmpty(r["Ha_DeceasedDate"].ToString()))
                {
                    try
                    {
                        sec.Ha_DeceasedDate = Convert.ToDateTime(r["Ha_DeceasedDate"].ToString()).ToString("yyyy/MM/dd");
                    }
                    catch
                    {
                        sec.Ha_DeceasedDate = r["Ha_DeceasedDate"].ToString();
                    }
                }
                if (!string.IsNullOrEmpty(r["Ha_CauseOfDeath"].ToString())) { sec.Ha_CauseOfDeath = r["Ha_CauseOfDeath"].ToString(); }
                if (!string.IsNullOrEmpty(r["Surname"].ToString())) { sec.Surname = r["Surname"].ToString(); }
                if (!string.IsNullOrEmpty(r["VoucherCode"].ToString())) { sec.VoucherCode = r["VoucherCode"].ToString(); }
                if (!string.IsNullOrEmpty(r["XMLBonus"].ToString())) { sec.XMLBonus = r["XMLBonus"].ToString(); }
                if (!string.IsNullOrEmpty(r["XMLData"].ToString())) { sec.XMLData = r["XMLData"].ToString(); }
                if (!string.IsNullOrEmpty(r["ProductID"].ToString())) { sec.ProductID = Convert.ToInt16(r["ProductID"].ToString()); }
            }

            sqlcmd.Dispose();
            sqlda.Dispose();
            ds.Dispose();
            cn.Close();

            return sec;

        }



        public int InsertSubscriberEnquiryResultBioMetric(SqlConnection cn, XDSPortalEnquiry.Entity.HVerificationResponse oSearch)
        {
            if (cn.State == ConnectionState.Closed)
                cn.Open();


            string insertSQL = "sp_I_SubscriberEnquiryResultIDBioMetric";
            SqlCommand sqlcmd = new SqlCommand(insertSQL, cn);
            sqlcmd.CommandTimeout = 0;
            sqlcmd.CommandType = CommandType.StoredProcedure;


            sqlcmd.Parameters.AddWithValue("@SubscriberEnquiryIDBioMetric", oSearch.BioMetricID);
            sqlcmd.Parameters.AddWithValue("@SubscriberEnquiryID", oSearch.SubscriberEnquiryID);
            sqlcmd.Parameters.AddWithValue("@SubscriberEnquiryResultID", oSearch.SubscriberEnquiryResultID);
            sqlcmd.Parameters.AddWithValue("@ApplicationErrors", oSearch.ApplicationErrors);
            sqlcmd.Parameters.AddWithValue("@VerificationErrors", oSearch.VerificationErrors);
            sqlcmd.Parameters.AddWithValue("@Base64StringJpeg2000Image", oSearch.Base64StringJpeg2000Image);
            sqlcmd.Parameters.AddWithValue("@VerificationResult", oSearch.VerificationResult);
            sqlcmd.Parameters.AddWithValue("@VerificationResultDescription", oSearch.VerificationResultDescription);
            sqlcmd.Parameters.AddWithValue("@VerificationColorIndicator", oSearch.VerificationColorIndicator);
            sqlcmd.Parameters.AddWithValue("@TmStamp", oSearch.TmStamp);
            sqlcmd.Parameters.AddWithValue("@TransactionNumber", oSearch.TransactionNumber);
            sqlcmd.Parameters.AddWithValue("@HasImage", oSearch.HasImage);
            sqlcmd.Parameters.AddWithValue("@FingerColor", oSearch.FingerColor);
            sqlcmd.Parameters.AddWithValue("@CreatedbyUser", oSearch.CreatedbyUser);
            sqlcmd.Parameters.AddWithValue("@Createdondate", DateTime.Now);
            sqlcmd.Parameters.AddWithValue("@URLrequested", oSearch.UrlRequested);

            int added = 0;
            added = sqlcmd.ExecuteNonQuery();

            return added;
        }

    }
}
