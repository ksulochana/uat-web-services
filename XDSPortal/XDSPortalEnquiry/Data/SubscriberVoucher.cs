﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace XDSPortalEnquiry.Data
{
    public class SubscriberVoucher
    {
        public SubscriberVoucher()
        {
        }

        public Entity.SubscriberVoucher GetSubscriberVoucher(SqlConnection ObjConstring, string strVoucherCode)
        {
            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();
            
            Entity.SubscriberVoucher sv = new Entity.SubscriberVoucher();

            string sqlSelect = "select * from SubscriberVoucher (NOLOCK) WHERE VoucherCode = '" + strVoucherCode + "'";

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                sv.ApprovedBy = r["ApprovedBy"].ToString();
                //sv.ChangedByUser = r["ChangedByUser"].ToString();
                //sv.ChangedOnDate = DateTime.Parse(r["ChangedOnDate"].ToString());
                sv.CreatedByUser = r["CreatedByUser"].ToString();
                sv.CreatedOnDate = DateTime.Parse(r["CreatedOnDate"].ToString());
                DateTime isdate;
                if (DateTime.TryParse(r["ExpiryDate"].ToString(), out isdate))
                {
                    sv.ExpiryDate = DateTime.Parse(r["ExpiryDate"].ToString());
                }
                sv.Note = r["Note"].ToString();
                sv.Quantity = int.Parse(r["Quantity"].ToString());
                sv.RequestedBy = r["RequestedBy"].ToString();
                sv.SubscriberID = int.Parse(r["SubscriberID"].ToString());
                sv.VoucherCode = r["VoucherCode"].ToString();
                sv.VoucherID = int.Parse(r["VoucherID"].ToString());
                sv.VoucherType = r["VoucherType"].ToString();                    
            }
            
            objsqladp.Dispose();
            Objsqlcom.Dispose();
            ds.Dispose();
            ObjConstring.Close();
            return sv;
        }

        public int UpdateSubscriberVoucher(SqlConnection ObjConstring, string strVoucherCode,string ChangedByUser)
        {
            if (ObjConstring.State == ConnectionState.Closed)
                ObjConstring.Open();

            Entity.SubscriberVoucher sv = new Entity.SubscriberVoucher();

            string sqlSelect = "UPDATE SubscriberVoucher SET QUANTITY = QUANTITY - 1,ChangedByUser = '" + ChangedByUser + "',ChangedOnDate = getdate() where VoucherCode = '" + strVoucherCode + "' and VoucherType = 1";

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            int RowsAffected=0;
            RowsAffected = Objsqlcom.ExecuteNonQuery();
            

            //foreach (DataRow r in ds.Tables[0].Rows)
            //{
            //    sv.ApprovedBy = r["ApprovedBy"].ToString();
            //    sv.ChangedByUser = r["ChangedByUser"].ToString();
            //    sv.ChangedOnDate = DateTime.Parse(r["ChangedOnDate"].ToString());
            //    sv.CreatedByUser = r["CreatedByUser"].ToString();
            //    sv.CreatedOnDate = DateTime.Parse(r["CreatedOnDate"].ToString());
            //    sv.ExpiryDate = DateTime.Parse(r["ExpiryDate"].ToString());
            //    sv.Note = r["Note"].ToString();
            //    sv.Quantity = int.Parse(r["Quantity"].ToString());
            //    sv.RequestedBy = r["RequestedBy"].ToString();
            //    sv.SubscriberID = int.Parse(r["SubscriberID"].ToString());
            //    sv.VoucherCode = r["VoucherCode"].ToString();
            //    sv.VoucherID = int.Parse(r["VoucherID"].ToString());
            //    sv.VoucherType = "";
            //}
            //objsqladp.Dispose();
            //Objsqlcom.Dispose();
            //ds.Dispose();
            //return sv;
            ObjConstring.Close();
            return RowsAffected;
        }
    }
}

