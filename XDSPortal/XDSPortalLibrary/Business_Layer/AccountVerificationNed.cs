﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace XDSPortalLibrary.Business_Layer
{
    public class AccountVerificationNed
    {
        string Referenceno = string.Empty, keyType = string.Empty, xmlstring = string.Empty;

        SqlConnection constring = null;

        public SqlConnection ConnectionString
        {
            get
            {
                return this.constring;
            }
            set
            {
                this.constring = value;
            }
        }
        public AccountVerificationNed()
        {
            this.keyType = "F";
        }

        public XDSPortalLibrary.Entity_Layer.Response search(XDSPortalLibrary.Entity_Layer.AccountVerificationNed ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                SqlCommand ObjSQlCmd = new SqlCommand("InsertAccountVerifications", this.ConnectionString);
                ObjSQlCmd.CommandTimeout = 0;
                ObjSQlCmd.CommandType = CommandType.StoredProcedure;
                ObjSQlCmd.Parameters.AddWithValue("@BankName", ObjSearch.BankName);
                ObjSQlCmd.Parameters.AddWithValue("@BranchNumber", ObjSearch.BranchCode);
                ObjSQlCmd.Parameters.AddWithValue("@AccountNumber", ObjSearch.AccNo);
                ObjSQlCmd.Parameters.AddWithValue("@AccountType", ObjSearch.Acctype);
                ObjSQlCmd.Parameters.AddWithValue("@IDNumber", ObjSearch.IDNo);
                ObjSQlCmd.Parameters.AddWithValue("@IDType", ObjSearch.IDType);
                ObjSQlCmd.Parameters.AddWithValue("@Initials", ObjSearch.Initials);
                ObjSQlCmd.Parameters.AddWithValue("@Surname", ObjSearch.SurName);
                ObjSQlCmd.Parameters.AddWithValue("@TaxReferenceNo", string.Empty);
                ObjSQlCmd.Parameters.AddWithValue("@SubscriberID", ObjSearch.SubscriberID);

                if (ConnectionString.State == ConnectionState.Closed)
                    ConnectionString.Open();

                DataSet ObjDS = new DataSet();
                SqlDataAdapter ObjDA = new SqlDataAdapter(ObjSQlCmd);
                DataTable dt = new DataTable("AccountVerification");

                ObjDA.Fill(dt);

                ConnectionString.Close();

                ObjSearch.Key = Convert.ToInt32(dt.Rows[0]["VerificationID"].ToString());
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single;
                ObjResponse.ResponseKeyType = this.keyType;
                ObjResponse.ResponseKey = ObjSearch.Key;
                ObjResponse.ResponseData = "Enquiry Submitted Successfully";


            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message.ToString();
            }
            return ObjResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response InsertAVSRealTime(XDSPortalLibrary.Entity_Layer.AccountVerificationNed ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                SqlCommand ObjSQlCmd = new SqlCommand("InsertAccountVerifications_AVSRealTime", this.ConnectionString);
                ObjSQlCmd.CommandTimeout = 0;
                ObjSQlCmd.CommandType = CommandType.StoredProcedure;
                ObjSQlCmd.Parameters.AddWithValue("@BankName", ObjSearch.BankName);
                ObjSQlCmd.Parameters.AddWithValue("@BranchNumber", ObjSearch.BranchCode);
                ObjSQlCmd.Parameters.AddWithValue("@AccountNumber", ObjSearch.AccNo);
                ObjSQlCmd.Parameters.AddWithValue("@AccountType", ObjSearch.Acctype);
                ObjSQlCmd.Parameters.AddWithValue("@IDNumber", ObjSearch.IDNo);
                ObjSQlCmd.Parameters.AddWithValue("@IDType", ObjSearch.IDType);
                ObjSQlCmd.Parameters.AddWithValue("@Initials", ObjSearch.Initials);
                ObjSQlCmd.Parameters.AddWithValue("@Surname", ObjSearch.SurName);
                ObjSQlCmd.Parameters.AddWithValue("@TaxReferenceNo", string.Empty);
                ObjSQlCmd.Parameters.AddWithValue("@SubscriberID", ObjSearch.SubscriberID);

                if (ConnectionString.State == ConnectionState.Closed)
                    ConnectionString.Open();

                DataSet ObjDS = new DataSet();
                SqlDataAdapter ObjDA = new SqlDataAdapter(ObjSQlCmd);
                DataTable dt = new DataTable("AccountVerification");

                ObjDA.Fill(dt);

                ConnectionString.Close();

                ObjSearch.Key = Convert.ToInt32(dt.Rows[0]["VerificationID"].ToString());
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single;
                ObjResponse.ResponseKeyType = this.keyType;
                ObjResponse.ResponseKey = ObjSearch.Key;
                ObjResponse.ResponseData = "Enquiry Submitted Successfully";


            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message.ToString();
            }
            return ObjResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response InsertAVSRealTimeWithContacts(XDSPortalLibrary.Entity_Layer.AccountVerificationNed ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                SqlCommand ObjSQlCmd = new SqlCommand("InsertAccountVerifications_AVSRealTimeWithContacts", this.ConnectionString);
                ObjSQlCmd.CommandTimeout = 0;
                ObjSQlCmd.CommandType = CommandType.StoredProcedure;
                ObjSQlCmd.Parameters.AddWithValue("@BankName", ObjSearch.BankName);
                ObjSQlCmd.Parameters.AddWithValue("@BranchNumber", ObjSearch.BranchCode);
                ObjSQlCmd.Parameters.AddWithValue("@AccountNumber", ObjSearch.AccNo);
                ObjSQlCmd.Parameters.AddWithValue("@AccountType", ObjSearch.Acctype);
                ObjSQlCmd.Parameters.AddWithValue("@IDNumber", ObjSearch.IDNo);
                ObjSQlCmd.Parameters.AddWithValue("@IDType", ObjSearch.IDType);
                ObjSQlCmd.Parameters.AddWithValue("@Initials", ObjSearch.Initials);
                ObjSQlCmd.Parameters.AddWithValue("@Surname", ObjSearch.SurName);
                ObjSQlCmd.Parameters.AddWithValue("@TaxReferenceNo", string.Empty);
                ObjSQlCmd.Parameters.AddWithValue("@SubscriberID", ObjSearch.SubscriberID);
                ObjSQlCmd.Parameters.AddWithValue("@ContactNumber", ObjSearch.ContactNumber);
                ObjSQlCmd.Parameters.AddWithValue("@EmailAddress", ObjSearch.EmailAddress);

                if (ConnectionString.State == ConnectionState.Closed)
                    ConnectionString.Open();

                DataSet ObjDS = new DataSet();
                SqlDataAdapter ObjDA = new SqlDataAdapter(ObjSQlCmd);
                DataTable dt = new DataTable("AccountVerification");

                ObjDA.Fill(dt);

                ConnectionString.Close();

                ObjSearch.Key = Convert.ToInt32(dt.Rows[0]["VerificationID"].ToString());
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single;
                ObjResponse.ResponseKeyType = this.keyType;
                ObjResponse.ResponseKey = ObjSearch.Key;
                ObjResponse.ResponseData = "Enquiry Submitted Successfully";


            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message.ToString();
            }
            return ObjResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response GetData(XDSPortalLibrary.Entity_Layer.AccountVerificationNed ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                SqlCommand ObjSQlCmd = new SqlCommand("GetRecordXML", this.ConnectionString);
                ObjSQlCmd.CommandTimeout = 0;
                ObjSQlCmd.CommandType = CommandType.StoredProcedure;
                ObjSQlCmd.Parameters.AddWithValue("@VerificationID", ObjSearch.Key);

                if (ConnectionString.State == ConnectionState.Closed)
                    ConnectionString.Open();

                DataSet ObjDS = new DataSet("AccountVerification");
                SqlDataAdapter ObjDA = new SqlDataAdapter(ObjSQlCmd);
                DataTable dt = new DataTable("AccountVerificationResponse");

                ObjDA.Fill(dt);
                ObjDS.Tables.Add(dt);
                ConnectionString.Close();

                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                ObjResponse.ResponseKeyType = this.keyType;
                ObjResponse.ResponseKey = ObjSearch.Key;
                ObjResponse.ResponseData = ObjDS.GetXml();

            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message.ToString();
            }
            return ObjResponse;
        }

        public int[] GetSubscriberVendorCode(long SubscriberID)
        {
            int[] VendorCodes = new int[2];

            try
            {
                int normalVendorCode = 0, realTimeVendorCode = 0;

                SqlCommand ObjSQlCmd = new SqlCommand("GetSubscriberVendorCode", this.ConnectionString);
                ObjSQlCmd.CommandTimeout = 0;
                ObjSQlCmd.CommandType = CommandType.StoredProcedure;
                ObjSQlCmd.Parameters.AddWithValue("@SubscriberID", SubscriberID);

                if (this.ConnectionString.State == ConnectionState.Closed)
                    this.ConnectionString.Open();

                SqlDataReader oReader = ObjSQlCmd.ExecuteReader();

                while (oReader.Read())
                {
                    normalVendorCode = oReader.GetInt32(0);
                    realTimeVendorCode = oReader.GetInt32(1);
                }

                oReader.Close();
                oReader.Dispose();

                if (this.ConnectionString.State == ConnectionState.Open)
                {
                    this.ConnectionString.Close();
                    SqlConnection.ClearPool(this.ConnectionString);
                }

                VendorCodes[0] = normalVendorCode;
                VendorCodes[1] = realTimeVendorCode;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return VendorCodes;
        }
    }
}
