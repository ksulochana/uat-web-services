﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace XDSPortalLibrary.Business_Layer
{
    public class GetDataSegments
    {
        string xmlstring = string.Empty;
        SqlConnection constring = null;


        public SqlConnection ConnectionString
        {
            get
            {
                return this.constring;
            }
            set
            {
                this.constring = value;
            }
        }

        public GetDataSegments()
        {

        }
        private void validateinput(XDSPortalLibrary.Entity_Layer.GetDataSegments Validate)
        {
            if (String.IsNullOrEmpty(Validate.SubscriberID.ToString()))
            {
                throw new Exception("SubscriberID is not provided");
            }
            if (String.IsNullOrEmpty(Validate.ProductID.ToString()))
            {
                throw new Exception("ProductID is not provided");
            }
            else if (Validate.KeyType != "C" && Validate.KeyType != "D" && Validate.KeyType != "B" && Validate.KeyType != "H" && Validate.KeyType != "P")
            {
                throw new Exception("KeyType should be either C, D, H or B");
            }
            //if (Validate.ConsumerID <= 0 && Validate.CommercialID <= 0 && Validate.DirectorID <= 0 && Validate.HomeAffairsID <= 0 && Validate.PropertyDeedID<=0)
            //{
            //    throw new Exception("Either ConsumerID or CommercialID or DirectorID must be supplied");
            //}
            if (Validate.KeyID <= 0)
            {
                throw new Exception("Key ID is Mandatory");
            }
        }
        public XDSPortalLibrary.Entity_Layer.Response DataSegments(XDSPortalLibrary.Entity_Layer.GetDataSegments ObjBonus)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {

                ObjResponse.ResponseKeyType = ObjBonus.KeyType;
                ObjResponse.ResponseExternalReferenceNo = ObjBonus.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjBonus.ReferenceNo;

                validateinput(ObjBonus);


                SqlCommand ObjSQlCmd = new SqlCommand("spGetDataSegments", this.ConnectionString);
                ObjSQlCmd.CommandTimeout = 0;
                ObjSQlCmd.CommandType = CommandType.StoredProcedure;

                if (ConnectionString.State == ConnectionState.Closed)
                    ConnectionString.Open();

                ObjSQlCmd.Parameters.AddWithValue("@SubscriberID", ObjBonus.SubscriberID);
                ObjSQlCmd.Parameters.AddWithValue("@ProductID", ObjBonus.ProductID);
                //ObjSQlCmd.Parameters.AddWithValue("@ConsumerID", ObjBonus.ConsumerID);
                //ObjSQlCmd.Parameters.AddWithValue("@CommercialID", ObjBonus.CommercialID);
                //ObjSQlCmd.Parameters.AddWithValue("@DirectorID", ObjBonus.DirectorID);
                ObjSQlCmd.Parameters.AddWithValue("@KeyType", ObjBonus.KeyType);
                ObjSQlCmd.Parameters.AddWithValue("@ExternalReferenceNo", ObjBonus.ExternalReference);
                //ObjSQlCmd.Parameters.AddWithValue("@PropertyDeedID", ObjBonus.PropertyDeedID);
                //ObjSQlCmd.Parameters.AddWithValue("@HomeAffairsID", ObjBonus.HomeAffairsID);
                //ObjSQlCmd.Parameters.AddWithValue("@TempTable", string.Empty);
                ObjSQlCmd.Parameters.AddWithValue("@KeyID", ObjBonus.KeyID);
                

                DataSet ObjDS = new DataSet();
                SqlDataAdapter ObjDA = new SqlDataAdapter(ObjSQlCmd);

                //ObjDA.Fill(ObjDS);
                ObjDS.Clear();

                ObjDA.Fill(ObjDS);

                ConnectionString.Close();

                ObjDS.DataSetName = "DataSegments";
                ObjDS.Tables[0].TableName = "Segments";
                ObjResponse.TmpReference = ObjDS.Tables[1].Rows[0].Field<string>("TempReference").ToString();
                ObjDS.Tables.RemoveAt(2);
                ObjDS.Tables.RemoveAt(1);

                xmlstring = ObjDS.GetXml();
                xmlstring = xmlstring.Replace("_x0023_", "");

                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Bonus;
                ObjResponse.ResponseData = xmlstring;

                ObjSQlCmd.Dispose();
                ObjDA.Dispose();
                ObjDS.Dispose();


            }
            catch (Exception a)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = a.Message;

            }
            return ObjResponse;
        }
    }
}
