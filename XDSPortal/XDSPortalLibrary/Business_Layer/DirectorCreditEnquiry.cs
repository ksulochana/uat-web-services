using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace XDSPortalLibrary.Business_Layer
{
   public class DirectorCreditEnquiry
    {
        string DirectorID = string.Empty,Referenceno = string.Empty,  keyType=string.Empty,xmlstring =string.Empty;

        SqlConnection constring = null;
        public SqlConnection ConnectionString
        {
            get
            {
                return this.constring;
            }
            set
            {
                this.constring = value;
            }
        }

        public DirectorCreditEnquiry()
        {
            //Assign the class variables 
          this.keyType = "D";
        }
        private void validateinput(XDSPortalLibrary.Entity_Layer.DirectorCreditEnquiry Validate)
        {
            if (String.IsNullOrEmpty(Validate.IDno))
            {
                throw new Exception("Your search input must have an Id Number");
            }
        }
        
        public XDSPortalLibrary.Entity_Layer.Response search(XDSPortalLibrary.Entity_Layer.DirectorCreditEnquiry ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            
            try
            {
                validateinput(ObjSearch);
                SqlCommand ObjSQlCmd = new SqlCommand("spDirector_S_Match_Standard", this.ConnectionString);

                ObjSQlCmd.CommandTimeout = 0;
                ObjSQlCmd.CommandType = CommandType.StoredProcedure;

                if (ConnectionString.State == ConnectionState.Closed)
                    ConnectionString.Open();

                ObjSQlCmd.Parameters.AddWithValue("@IDNo", ObjSearch.IDno);
                ObjSQlCmd.Parameters.AddWithValue("@FirstInitial", ObjSearch.FirstInitial);
                ObjSQlCmd.Parameters.AddWithValue("@FirstName", ObjSearch.Firstname);
                ObjSQlCmd.Parameters.AddWithValue("@SecondName", ObjSearch.SecondName);
                ObjSQlCmd.Parameters.AddWithValue("@Surname", ObjSearch.Surname);
                if (ObjSearch.DOB.Year > 1753)
                {
                    ObjSQlCmd.Parameters.AddWithValue("@BirthDate", ObjSearch.DOB);
                }
                else
                {
                    ObjSQlCmd.Parameters.AddWithValue("@BirthDate", "");
                }

                ObjSQlCmd.Parameters.AddWithValue("@SecondInitial", ObjSearch.SecondInitial);


                
                DataSet ObjDS = new DataSet();
                SqlDataAdapter ObjDA = new SqlDataAdapter(ObjSQlCmd);

                DataTable dt = new DataTable("ConsumerDetails");

                ObjDA.Fill(dt);

                ConnectionString.Close();

                DataColumn dcBonusSeg = new DataColumn("BonusXML");
                DataColumn dcTempReference = new DataColumn("TempReference");

                dt.Columns.Add(dcBonusSeg);
                dt.Columns.Add(dcTempReference);

                if (ObjSearch.BonusCheck)
                {

                    // Genrate error message if the search gives more than 20 results 

                    if (dt.Rows.Count < 21)
                    {
                        XDSPortalLibrary.Entity_Layer.Response ObjBonusResponse = new XDSPortalLibrary.Entity_Layer.Response();

                        // Get the Bonus XML for each of the Consumers / Companies matched

                        foreach (DataRow dr in dt.Rows)
                        {

                            ObjSearch.DirectorID = Convert.ToInt32(dr["DirectorID"]);
                            ObjSearch.ConsumerID = Convert.ToInt32(dr["ConsumerID"]);
                            ObjSearch.IsConsumerMatching = true;
                            ObjBonusResponse = GetData(ObjSearch);
                            dr["BonusXML"] = "";
                            if (ObjBonusResponse.ResponseData.ToString() != String.Empty && ObjBonusResponse.ResponseData.ToString() != "<BonusSegments />")
                            {
                                dr["BonusXML"] = ObjBonusResponse.ResponseData;
                            }
                            dr["TempReference"] = ObjBonusResponse.TmpReference;

                        }
                        dt.AcceptChanges();
                    }
                    else
                    {
                        throw new Exception("There are too many results from this search. Please refine your search by providing a surname and try again");
                    }
                }
                else
                {
                    foreach (DataRow dr in dt.Rows)
                    {

                        dr["BonusXML"] = "";
                        dr["TempReference"] = "";

                    }
                    dt.AcceptChanges();
                }
                ObjDS.Tables.Add(dt);
                ObjDS.DataSetName = "ListOfDirectors";
                ObjDS.Tables[0].TableName = "DirectorDetails";

                if (ObjDS.Tables[0].Rows.Count == 1)
                {
                    //Single Match
                    if (ObjSearch.ConfirmationChkBox == false)
                    {
                        // Single Match --> No Bonus --> ConfirmationCheckbox is not Checked --> Generate the Report XML

                        if (ObjDS.Tables[0].Rows[0].Field<string>("BonusXML").ToString() == string.Empty)
                        {

                            ObjResponse = GetReportXMLForSingleMAtch(ObjSearch);
                            ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        }
                        else
                        {
                            ObjResponse.ResponseData = ObjDS.GetXml();
                            ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single;
                        }

                        ObjResponse.ResponseKeyType = this.keyType;
                        ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.DirectorID);

                    }
                    else if (ObjSearch.ConfirmationChkBox == true)
                    {
                        //Display user info

                        xmlstring = ObjDS.GetXml();
                        ObjSearch.ConsumerID = Convert.ToInt32(ObjDS.Tables[0].Rows[0]["ConsumerID"]);
                        ObjSearch.DirectorID = Convert.ToInt32(ObjDS.Tables[0].Rows[0]["DirectorID"]);
                        ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single;
                        ObjResponse.ResponseKeyType = this.keyType;
                        ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.DirectorID);
                        ObjResponse.ResponseData = xmlstring;
                    }
                }
                else if (ObjDS.Tables[0].Rows.Count > 1)
                {
                    //Return User Info XML
                    xmlstring = ObjDS.GetXml();
                    ObjResponse.ResponseKeyType = this.keyType;
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple;
                    ObjResponse.ResponseData = xmlstring;
                    // ObjResponse.ResponseKey = Convert.ToInt16(this.DirectorID);
                }

                else if (ObjDS.Tables[0].Rows.Count == 0)
                {
                    // No match
                    xmlstring = ObjDS.GetXml();
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                    ObjResponse.ResponseData = xmlstring;
                    //  ObjResponse.ResponseKey = Convert.ToInt16(this.DirectorID);
                }
                dcBonusSeg.Dispose();
                dcTempReference.Dispose();

                dt.Dispose();
                ObjSQlCmd.Dispose();
                ObjDS.Dispose();
                ObjDA.Dispose();
            }

            catch (Exception a)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = a.Message;
             }

            return ObjResponse;
        }
        public XDSPortalLibrary.Entity_Layer.Response GetData(XDSPortalLibrary.Entity_Layer.DirectorCreditEnquiry ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                if (ObjSearch.DirectorID > 0)
                {
                    if (ObjSearch.IsConsumerMatching == true)
                    {
                        // Get the Bonus Segments for each of the matching results during matching
                        XDSPortalLibrary.Entity_Layer.GetBonusSegments ObjBonus = new XDSPortalLibrary.Entity_Layer.GetBonusSegments();

                        //ObjBonus.SubscriberID = ObjSearch.subscriberID;
                        ObjBonus.ProductID = ObjSearch.ProductID;
                        if (ObjSearch.DirectorID > 0)
                        {
                            ObjBonus.KeyID = Convert.ToInt32(ObjSearch.DirectorID);
                            ObjBonus.KeyType = "D";
                        }
                        else if (ObjSearch.ConsumerID > 0)
                        {
                            ObjBonus.KeyID = Convert.ToInt32(ObjSearch.ConsumerID);
                            ObjBonus.KeyType = "C";
                        }
                        ObjBonus.ExternalReference = ObjSearch.ExternalReference;
                        //ObjBonus.ReferenceNo = ObjSearch.ReferenceNo;
                        ObjBonus.DataSegments = ObjSearch.DataSegments;

                        GetBonusSegments ObjGetBonus = new GetBonusSegments();
                        ObjGetBonus.ConnectionString = this.ConnectionString;

                        ObjResponse = ObjGetBonus.BonusSegments(ObjBonus);
                        ObjResponse.ResponseKeyType = this.keyType;
                        ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.DirectorID);
                    }
                    else
                    {
                        // Generate the Report XML when user Submit Multipletrace 
                        ObjResponse = GetReportXMLForSingleMAtch(ObjSearch);
                    }
                }
                else
                {
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    ObjResponse.ResponseData = "Insufficient data supplied";
                }
            }
            catch (Exception a)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = a.Message;
            }

            return ObjResponse;
        }
        private XDSPortalLibrary.Entity_Layer.Response GetReportXMLForSingleMAtch(XDSPortalLibrary.Entity_Layer.DirectorCreditEnquiry ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                XDSPortalLibrary.Entity_Layer.GenerateReportXML ObjReportParam = new XDSPortalLibrary.Entity_Layer.GenerateReportXML();
                ObjReportParam.SubscriberID = ObjSearch.subscriberID;
                ObjReportParam.ProductID = ObjSearch.ProductID;
                if (ObjSearch.DirectorID > 0)
                {
                    ObjReportParam.KeyID = Convert.ToInt32(ObjSearch.DirectorID);
                    ObjReportParam.KeyType = "D";
                }
                else if (ObjSearch.ConsumerID > 0)
                {
                    ObjReportParam.KeyID = Convert.ToInt32(ObjSearch.ConsumerID);
                    ObjReportParam.KeyType = "C";
                }
                ObjReportParam.ReferenceNo = ObjSearch.ReferenceNo;
                ObjReportParam.ExternalReference = ObjSearch.ExternalReference;
                ObjReportParam.TmpReference = ObjSearch.TmpReference;

                if (!(ObjSearch.BonusSegments == null))
                {
                    ObjReportParam.BonusSegments = ObjSearch.BonusSegments;
                }
                else
                {
                    DataSet ds = new DataSet();
                    DataTable dt = new DataTable();
                    ds.DataSetName = "BonusSegments";
                    dt.TableName = "Segment";
                    ds.Tables.Add(dt);
                    ObjReportParam.BonusSegments = ds;

                    ds.Dispose();
                    dt.Dispose();
                }
                ObjReportParam.DataSegments = ObjSearch.DataSegments;

                GenerateReportXML ObjReport = new GenerateReportXML();
                ObjReport.ConnectionString = this.ConnectionString;
                ObjResponse = ObjReport.GenerateXML(ObjReportParam);

                ObjResponse.ResponseKeyType = this.keyType;
                ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.ConsumerID);
            }

            catch (Exception a)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = a.Message;
            }

            return ObjResponse;
        }
    }
}
