﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace XDSPortalLibrary.Business_Layer
{
    public class DeedsEnquiry
    {
        string Referenceno = string.Empty, keyType = string.Empty, xmlstring = string.Empty, PropertyDeedID = string.Empty;
        SqlConnection constring = null;


        public SqlConnection ConnectionString
        {
            get
            {
                return this.constring;
            }
            set
            {
                this.constring = value;
            }
        }

        public DeedsEnquiry()
        {
            this.keyType = "P";
        }
        private void validateinput(XDSPortalLibrary.Entity_Layer.DeedsEnquiry Validate)
        {
            if (string.IsNullOrEmpty(Validate.DeedsOffice))
            {
                throw new Exception("Please enter a valid Deeds Office");
            }
            if (Validate.SearchTypeInd == 0)
            {
                throw new Exception("Invalid Search Type Indicator");
            }
            else if (Validate.SearchTypeInd == 1 && String.IsNullOrEmpty(Validate.IDno))
            {
                throw new Exception("IDNo is Mandatory");
            }
            else if ((Validate.SearchTypeInd == 2 || Validate.SearchTypeInd == 3 )&& String.IsNullOrEmpty(Validate.BusinessName) && String.IsNullOrEmpty(Validate.IDno))
            {
                throw new Exception("Company Registartion No or Business Name is Mandatory");
            }
            else if (Validate.SearchTypeInd == 6 && String.IsNullOrEmpty(Validate.ErfNo) && String.IsNullOrEmpty(Validate.TownshipName))
            {
                throw new Exception("ErfNo and TownshipName  are Mandatory");
            }
            else if ((Validate.SearchTypeInd == 4 || Validate.SearchTypeInd == 5 )&& String.IsNullOrEmpty(Validate.BondAccNo) && String.IsNullOrEmpty(Validate.TitleDeedNo))
            {
                throw new Exception("Title Deed Number or Bond Account No are Mandatory");
            }
//            if (String.IsNullOrEmpty(Validate.IDno) && String.IsNullOrEmpty(Validate.BusinessName) && String.IsNullOrEmpty(Validate.ErfNo) && String.IsNullOrEmpty(Validate.TitleDeedNo) && String.IsNullOrEmpty(Validate.BondAccNo))
//            {
//                throw new Exception(@"Your search input must include either of following mandatory fields 
//		                               1. IDno 
//		                               2. Company RegistrationNO or BusinessName
//		                               3. Erf No
//		                               4. Title Deed No
//		                               4. BondAccountno");
//             }
        }
        public XDSPortalLibrary.Entity_Layer.Response search(XDSPortalLibrary.Entity_Layer.DeedsEnquiry ObjSearch)
        {
            //Executes the Matching procedure for Consumertrace and generates the ouput XML
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();

            ObjResponse.ResponseKeyType = this.keyType;
            ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
            ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

            try
            {
                validateinput(ObjSearch);
                //string SQl;
                //SQl = "Exec spProperty_S_Match_Standard '" + ObjSearch.DeedsOffice + "','" + ObjSearch.IDno + "','" + ObjSearch.FirstName + "','" + ObjSearch.SurName + "','" + ObjSearch.BusinessName + "','" + ObjSearch.TownshipName + "','" + ObjSearch.ErfNo + "','" + ObjSearch.TitleDeedNo + "','" + ObjSearch.BondAccNo + "','" + ObjSearch.PortionNo + "'";
                //SqlCommand ObjSQlCmd = new SqlCommand(SQl, this.ConnectionString);
                SqlCommand ObjSQlCmd = new SqlCommand("spProperty_S_Match_Standard", this.ConnectionString);
                ObjSQlCmd.CommandTimeout = 0;


                ObjSQlCmd.CommandType = CommandType.StoredProcedure;

                if (ConnectionString.State == ConnectionState.Closed)
                    ConnectionString.Open();

                ObjSQlCmd.Parameters.AddWithValue("@DeedsOffice", ObjSearch.DeedsOffice);
                ObjSQlCmd.Parameters.AddWithValue("@IDNo", ObjSearch.IDno);
                ObjSQlCmd.Parameters.AddWithValue("@TownshipName", ObjSearch.TownshipName);
                ObjSQlCmd.Parameters.AddWithValue("@ErfNo", ObjSearch.ErfNo);
                ObjSQlCmd.Parameters.AddWithValue("@TitleDeedNo", ObjSearch.TitleDeedNo);
                ObjSQlCmd.Parameters.AddWithValue("@BonDAccNo", ObjSearch.BondAccNo);
                ObjSQlCmd.Parameters.AddWithValue("@FirstName", ObjSearch.FirstName);
                ObjSQlCmd.Parameters.AddWithValue("@SurName", ObjSearch.SurName);
                ObjSQlCmd.Parameters.AddWithValue("@Businessname", ObjSearch.BusinessName);
                ObjSQlCmd.Parameters.AddWithValue("@PortionNo", ObjSearch.PortionNo);
                //int rowsaffexted;
                //rowsaffexted = ObjSQlCmd.ExecuteNonQuery();

                DataSet ObjDS = new DataSet();
                SqlDataAdapter ObjDA = new SqlDataAdapter(ObjSQlCmd);
                DataTable dt = new DataTable("PropertyDetails");

                ObjDA.Fill(dt);

                ConnectionString.Close();

                DataColumn dcBonusSeg = new DataColumn("BonusXML");
                DataColumn dcTempReference = new DataColumn("TempReference");

                dt.Columns.Add(dcBonusSeg);
                dt.Columns.Add(dcTempReference);


                if (ObjSearch.BonusCheck)
                {
                    // Genrate error message if the search gives more than 20 results 

                    if (dt.Rows.Count < 21)
                    {

                        XDSPortalLibrary.Entity_Layer.Response ObjBonusResponse = new XDSPortalLibrary.Entity_Layer.Response();

                        // Get the Bonus XML for each of the Consumers / Companies matched

                        foreach (DataRow dr in dt.Rows)
                        {

                            ObjSearch.PropertyDeedID = Convert.ToInt32(dr["PropertyDeedID"]);
                            ObjSearch.IsConsumerMatching = true;
                            ObjBonusResponse = GetData(ObjSearch);
                            dr["BonusXML"] = "";
                            if (ObjBonusResponse.ResponseData.ToString() != String.Empty && ObjBonusResponse.ResponseData.ToString() != "<BonusSegments />")
                            {
                                dr["BonusXML"] = ObjBonusResponse.ResponseData;
                            }
                            dr["TempReference"] = ObjBonusResponse.TmpReference;

                        }
                        dt.AcceptChanges();

                    }
                    else
                    {
                        throw new Exception("There are too many results from this search. Please refine your search and try again");
                    }
                }
                else
                {
                    foreach (DataRow dr in dt.Rows)
                    {

                        dr["BonusXML"] = "";
                        dr["TempReference"] = "";

                    }
                    dt.AcceptChanges();
                }
                ObjDS.Tables.Add(dt);
                ObjDS.DataSetName = "ListOfProperties";


                if (ObjDS.Tables[0].Rows.Count == 1)
                {
                    //Single Match

                    if (ObjSearch.ConfirmationChkBox == false)
                    {
                        // Single Match --> No Bonus --> ConfirmationCheckbox is not Checked --> Generate the Report XML


                        if (ObjDS.Tables[0].Rows[0].Field<string>("BonusXML").ToString() == string.Empty)
                        {

                            ObjResponse = GetReportXMLForSingleMAtch(ObjSearch);
                            ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        }
                        else
                        {
                            ObjResponse.ResponseData = ObjDS.GetXml();
                            ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single;
                        }
                        ObjResponse.ResponseKeyType = this.keyType;
                        ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.PropertyDeedID);
                    }
                    else if (ObjSearch.ConfirmationChkBox == true)
                    {
                        //Display user info
                        xmlstring = ObjDS.GetXml();
                        ObjSearch.PropertyDeedID = Convert.ToInt32(ObjDS.Tables[0].Rows[0]["PropertyDeedID"]);
                        ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single;
                        ObjResponse.ResponseKeyType = this.keyType;
                        ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.PropertyDeedID);
                        ObjResponse.ResponseData = xmlstring;
                    }
                }
                else if (ObjDS.Tables[0].Rows.Count > 1)
                {
                    //Return User Info XML
                    xmlstring = ObjDS.GetXml();
                    ObjResponse.ResponseKeyType = this.keyType;
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple;
                    ObjResponse.ResponseData = xmlstring;
                    //ObjResponse.ResponseKey = Convert.ToInt16(this.consumerid);
                }

                else if (ObjDS.Tables[0].Rows.Count == 0)
                {
                    // No match
                    xmlstring = ObjDS.GetXml();
                    ObjResponse.ResponseKeyType = this.keyType;
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                    ObjResponse.ResponseData = xmlstring;
                    //ObjResponse.ResponseKey = Convert.ToInt16(this.consumerid);
                }

                dcBonusSeg.Dispose();
                dcTempReference.Dispose();

                dt.Dispose();
                ObjSQlCmd.Dispose();
                ObjDS.Dispose();
                ObjDA.Dispose();

            }

            catch (Exception a)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = a.Message;
            }

            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response GetData(XDSPortalLibrary.Entity_Layer.DeedsEnquiry ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                if (ObjSearch.PropertyDeedID > 0)
                {
                    if (ObjSearch.IsConsumerMatching == true)
                    {
                        // Get the Bonus Segments for each of the matching results during matching
                        XDSPortalLibrary.Entity_Layer.GetBonusSegments ObjBonus = new XDSPortalLibrary.Entity_Layer.GetBonusSegments();

                        //ObjBonus.SubscriberID = ObjSearch.subscriberID;
                        ObjBonus.ProductID = ObjSearch.ProductID;
                        ObjBonus.KeyID = Convert.ToInt32(ObjSearch.PropertyDeedID);
                        ObjBonus.KeyType = this.keyType;
                        ObjBonus.ExternalReference = ObjSearch.ExternalReference;
                        //ObjBonus.ReferenceNo = ObjSearch.ReferenceNo;
                        ObjBonus.DataSegments = ObjSearch.DataSegments;

                        GetBonusSegments ObjGetBonus = new GetBonusSegments();
                        ObjGetBonus.ConnectionString = this.ConnectionString;

                        ObjResponse = ObjGetBonus.BonusSegments(ObjBonus);
                        ObjResponse.ResponseKeyType = this.keyType;
                        ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.PropertyDeedID);
                    }
                    else
                    {
                        // Generate the Report XML when user Submit Multipletrace 
                        ObjResponse = GetReportXMLForSingleMAtch(ObjSearch);
                    }
                }
                else
                {
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    ObjResponse.ResponseData = "Insufficient data supplied";
                }
            }
            catch (Exception a)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = a.Message;
            }

            return ObjResponse;
        }
        private XDSPortalLibrary.Entity_Layer.Response GetReportXMLForSingleMAtch(XDSPortalLibrary.Entity_Layer.DeedsEnquiry ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                XDSPortalLibrary.Entity_Layer.GenerateReportXML ObjReportParam = new XDSPortalLibrary.Entity_Layer.GenerateReportXML();
                ObjReportParam.SubscriberID = ObjSearch.subscriberID;
                ObjReportParam.ProductID = ObjSearch.ProductID;
                ObjReportParam.KeyID = ObjSearch.PropertyDeedID;
                ObjReportParam.KeyType = this.keyType;
                ObjReportParam.ReferenceNo = ObjSearch.ReferenceNo;
                ObjReportParam.ExternalReference = ObjSearch.ExternalReference;
                ObjReportParam.TmpReference = ObjSearch.TmpReference;

                if (!(ObjSearch.BonusSegments == null))
                {
                    ObjReportParam.BonusSegments = ObjSearch.BonusSegments;
                }
                else
                {
                    DataSet ds = new DataSet();
                    DataTable dt = new DataTable();
                    ds.DataSetName = "BonusSegments";
                    dt.TableName = "Segment";
                    ds.Tables.Add(dt);
                    ObjReportParam.BonusSegments = ds;

                    ds.Dispose();
                    dt.Dispose();
                }
                ObjReportParam.DataSegments = ObjSearch.DataSegments;


                GenerateReportXML ObjReport = new GenerateReportXML();
                ObjReport.ConnectionString = this.ConnectionString;
                ObjResponse = ObjReport.GenerateXML(ObjReportParam);

                ObjResponse.ResponseKeyType = this.keyType;
                ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.PropertyDeedID);
            }

            catch (Exception a)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = a.Message;
            }

            return ObjResponse;
        }
    }
}
