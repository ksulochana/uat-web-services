﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;


namespace XDSPortalLibrary.Business_Layer
{
   public class AccountVerificationIdeco
    {
        string Referenceno = string.Empty,  keyType=string.Empty,xmlstring =string.Empty,lresult = string.Empty, Result = string.Empty;
        int returncode = 0;

        wsideco.xmldata objwsideco = new XDSPortalLibrary.wsideco.xmldata();
        public AccountVerificationIdeco()
        {
            //Assign the class variables 
            this.keyType = "F";
        }

        private void validateinput(XDSPortalLibrary.Entity_Layer.AccountVerification Validate)
        {
            if (String.IsNullOrEmpty(Validate.IDNo) || String.IsNullOrEmpty(Validate.SurName) || String.IsNullOrEmpty(Validate.AccNo) || String.IsNullOrEmpty(Validate.BranchCode) || String.IsNullOrEmpty(Validate.EmailAddress) || String.IsNullOrEmpty(Validate.Acctype) || String.IsNullOrEmpty(Validate.BankName) || String.IsNullOrEmpty(Validate.RequesterFirstName) || String.IsNullOrEmpty(Validate.RequesterSurName))
            {
                throw new Exception("Registration No/ IDNo, Company Name/ SurName, Account No, Account Type, Barnch Code,Firstname, Surname, Email Address are mandatory");
            }
        }
        public XDSPortalLibrary.Entity_Layer.Response search(XDSPortalLibrary.Entity_Layer.AccountVerification ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            validateinput(ObjSearch);
            try
            {
                Login(ObjSearch);
                Result = AHVRequest(ObjSearch);

                ObjResponse.ResponseData = Result;
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single;
                ObjResponse.ResponseKeyType = this.keyType;
                ObjResponse.TmpReference = ObjSearch.SessionID;
                ObjResponse.ResponseKey = ObjSearch.Key;

                objwsideco.logoff(ObjSearch.SessionID);  
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message.ToString();
            }
            return ObjResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response GetData(XDSPortalLibrary.Entity_Layer.AccountVerification ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                Login(ObjSearch);
                ObjResponse.ResponseData = AHVResult(ObjSearch);

                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseKeyType = this.keyType;
                ObjResponse.TmpReference = ObjSearch.SessionID;
                ObjResponse.ResponseKey = ObjSearch.Key;

                objwsideco.logoff(ObjSearch.SessionID);
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message.ToString();
            }
            return ObjResponse;
        }

        public void Login(XDSPortalLibrary.Entity_Layer.AccountVerification OAccountVerification)
        {
            string strresult;
            DataSet DS = new DataSet();
            strresult = objwsideco.login(OAccountVerification.UserName, OAccountVerification.Password, OAccountVerification.AccountRef);

            System.IO.StringReader sr = new System.IO.StringReader(strresult);

            DS.ReadXml(sr);

            OAccountVerification.SessionID = DS.Tables[0].Rows[0].Field<string>("sessionid").ToString();

            if (Convert.ToInt16(DS.Tables[0].Rows[0].Field<string>("code").ToString()) != 0)
            {
                throw new Exception("Login - " + DS.Tables[0].Rows[0].Field<string>("code").ToString());
            }
        }

        public string AHVRequest(XDSPortalLibrary.Entity_Layer.AccountVerification OAccountVerification)
        {
            string strresult;
            DataSet DS = new DataSet();
            strresult = objwsideco.ahvrequest(OAccountVerification.SessionID, OAccountVerification.IDNo, OAccountVerification.SurName, OAccountVerification.Initials, OAccountVerification.AccNo, OAccountVerification.BranchCode, OAccountVerification.Acctype, OAccountVerification.BankName);

            System.IO.StringReader sr = new System.IO.StringReader(strresult);

            DS.ReadXml(sr);

            OAccountVerification.Key = Convert.ToInt16(DS.Tables["return"].Rows[0].Field<string>("refnumber").ToString());

            if (Convert.ToInt16(DS.Tables[0].Rows[0].Field<string>("code").ToString()) != 0)
            {
                throw new Exception("AHVRequest - " + DS.Tables[0].Rows[0].Field<string>("code").ToString());
            }

            return strresult;
        }
        public string AHVResult(XDSPortalLibrary.Entity_Layer.AccountVerification OAccountVerification)
        {
            string strresult;
            DataSet DS = new DataSet();
            strresult = objwsideco.ahvresult(OAccountVerification.SessionID,OAccountVerification.Key.ToString());

            System.IO.StringReader sr = new System.IO.StringReader(strresult);

            DS.ReadXml(sr);

            if (Convert.ToInt16(DS.Tables[0].Rows[0].Field<string>("code").ToString()) != 0)
            {
                throw new Exception("AHVResult - " + DS.Tables[0].Rows[0].Field<string>("code").ToString());
            }

            return strresult;
        }

    }
}
