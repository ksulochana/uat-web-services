﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


namespace XDSPortalLibrary.Business_Layer
{
   class GenerateReportXML
   {
       SqlConnection constring = null;
       public SqlConnection ConnectionString
       {
           get
           {
               return this.constring;
           }
           set
           {
               this.constring = value;
           }
       }

       public GenerateReportXML()
       {
         
       }
       private void validateinput(XDSPortalLibrary.Entity_Layer.GenerateReportXML Validate)
       {
           if (Validate.SubscriberID<=0)
           {
               throw new Exception("SubscriberID must be a Positive integer");
           }
           if (Validate.ProductID<=0)
           {
               throw new Exception("ProductID is not supplied");
           }
           if (String.IsNullOrEmpty(Validate.KeyType))
           {
               throw new Exception("KeyType is mandatory");
           }
           else if (Validate.KeyType != "C" && Validate.KeyType != "D" && Validate.KeyType != "B" && Validate.KeyType != "H" && Validate.KeyType != "P")
           {
               throw new Exception("KeyType should be either C,D,H,B or P");
           }
           //if (Validate.ConsumerID <= 0 && Validate.CommercialID <= 0 && Validate.DirectorID <= 0 && Validate.HomeAffairsID <= 0 && Validate.PropertyDeedID<=0)
           //{
           //    throw new Exception("Either ConsumerID or CommercialID or DirectorID or HomeAffairsID or PropertyDeedID should be supplied");
           //}
           if (Validate.KeyID <= 0)
           {
               throw new Exception("Key ID is Mandatory");
           }
       }
        public XDSPortalLibrary.Entity_Layer.Response GenerateXML(XDSPortalLibrary.Entity_Layer.GenerateReportXML ObjReport)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
           try
            {
                validateinput(ObjReport);
                ObjResponse.ResponseKeyType = ObjReport.KeyType;
                ObjResponse.ResponseExternalReferenceNo = ObjReport.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjReport.ReferenceNo;


                SqlCommand ObjReportProcCMd = new SqlCommand("spReport_FillReport", this.ConnectionString);
                
                ObjReportProcCMd.CommandTimeout = 0;
               
                ObjReportProcCMd.CommandType = CommandType.StoredProcedure;

                if (ConnectionString.State == ConnectionState.Closed)
                    ConnectionString.Open();

                ObjReportProcCMd.Parameters.AddWithValue("@Reference", ObjReport.TmpReference);
                //ObjReportProcCMd.Parameters.AddWithValue("@ConsumerID", ObjReport.ConsumerID);
                //ObjReportProcCMd.Parameters.AddWithValue("@CommercialID", ObjReport.CommercialID);
                //ObjReportProcCMd.Parameters.AddWithValue("@DirectorID", ObjReport.DirectorID);
                ObjReportProcCMd.Parameters.AddWithValue("@ReferenceNo", ObjReport.ReferenceNo);
                ObjReportProcCMd.Parameters.AddWithValue("@KeyType", ObjReport.KeyType);
                string str = ObjReport.BonusSegments.GetXml();
                ObjReportProcCMd.Parameters.AddWithValue("@BonusSegments", ObjReport.BonusSegments.GetXml());
                ObjReportProcCMd.Parameters.AddWithValue("@DataSegments", ObjReport.DataSegments.ToString());
                //ObjReportProcCMd.Parameters.AddWithValue("@HomeAffairsID", ObjReport.HomeAffairsID);
                ObjReportProcCMd.Parameters.AddWithValue("@SubscriberID", ObjReport.SubscriberID);
                ObjReportProcCMd.Parameters.AddWithValue("@ProductID", ObjReport.ProductID);
                ObjReportProcCMd.Parameters.AddWithValue("@ExternalReferenceNo", ObjReport.ExternalReference);
                //ObjReportProcCMd.Parameters.AddWithValue("@PropertyDeedID", ObjReport.PropertyDeedID);
                ObjReportProcCMd.Parameters.AddWithValue("@KeyID", ObjReport.KeyID);
                
                   
                DataSet ObjDSReport = new DataSet();
                
                SqlDataAdapter ObjDAReport = new SqlDataAdapter(ObjReportProcCMd);

                ObjDAReport.Fill(ObjDSReport);

                ConnectionString.Close();

                for (int i = 0; i < ObjDSReport.Tables.Count; i++)
                {
                    if (ObjDSReport.Tables[i].Columns.Contains("TableName"))
                    {
                        foreach (DataRow row in ObjDSReport.Tables[i].Rows)
                        {
                            ObjDSReport.Tables[i].TableName = row["TableName"].ToString();
                        }
                        ObjDSReport.Tables[i].Columns.Remove("TableName");
                    }
                    else
                    {
                        ObjDSReport.Tables.RemoveAt(i);
                        i--;
                    }
                }

                if (ObjReport.KeyType == "C")
                {
                    ObjDSReport.DataSetName = "Consumer";
                }
                else if (ObjReport.KeyType == "D")
                {
                    ObjDSReport.DataSetName = "Director";
                }
                else if (ObjReport.KeyType == "B")
                {
                    ObjDSReport.DataSetName = "Commercial";
                }
                else if (ObjReport.KeyType == "H")
                {
                    ObjDSReport.DataSetName = "HomeAffairs";
                }
                else if (ObjReport.KeyType == "P")
                {
                    ObjDSReport.DataSetName = "Property";
                }
                string xml = ObjDSReport.GetXml();
                xml = xml.Replace("_x0036_", "");
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = xml;

                ObjDSReport.Dispose();
                ObjReportProcCMd.Dispose();
                ObjDAReport.Dispose();

                
            }
            catch (Exception a)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = a.Message;

            }
            return ObjResponse;
            
        }
    }
}
