using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace XDSPortalLibrary.Business_Layer
{
    public class ConsumerDocumentVerificationTraceco
    {
        string Referenceno = string.Empty, keyType = string.Empty, xmlstring = string.Empty, lresult = string.Empty, Result = string.Empty;
        int returncode = 0;

      SqlConnection constring = null;

      public SqlConnection ConnectionString
      {
          get
          {
              return this.constring;
          }
          set
          {
              this.constring = value;
          }
      }
        wsideco.xmldata objwsideco = new XDSPortalLibrary.wsideco.xmldata();
        public ConsumerDocumentVerificationTraceco()
        {
            //Assign the class variables 
            this.keyType = "F";
        }

        private void validateinput(XDSPortalLibrary.Entity_Layer.ConsumerDocumentVerificationTrace Validate)
        {
            //if (String.IsNullOrEmpty(Validate.IDNo) || String.IsNullOrEmpty(Validate.SurName) || String.IsNullOrEmpty(Validate.EmailAddress) )
            //{
            //    throw new Exception("IDNo,  SurName,Firstname, Surname, Email Address are mandatory");
            //}
        }

     
        public XDSPortalLibrary.Entity_Layer.Response search(XDSPortalLibrary.Entity_Layer.ConsumerDocumentVerificationTrace ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            validateinput(ObjSearch);
            try
            {
                //Login(ObjSearch);
                Result = DVRequest(ObjSearch);

                ObjResponse.ResponseData = Result;
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single;
                ObjResponse.ResponseKeyType = this.keyType;
                ObjResponse.TmpReference = ObjSearch.SessionID;
                ObjResponse.ResponseKey = ObjSearch.Key;

                objwsideco.logoff(ObjSearch.SessionID);  
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message.ToString();
            }
            return ObjResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response GetData(XDSPortalLibrary.Entity_Layer.ConsumerDocumentVerificationTrace ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                Login(ObjSearch);
                ObjResponse.ResponseData = DVResult(ObjSearch);

                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseKeyType = this.keyType;
                ObjResponse.TmpReference = ObjSearch.SessionID;
                ObjResponse.ResponseKey = ObjSearch.Key;

                objwsideco.logoff(ObjSearch.SessionID);
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message.ToString();
            }
            return ObjResponse;
        }

        public void Login(XDSPortalLibrary.Entity_Layer.ConsumerDocumentVerificationTrace ODocVerification)
        {
            string strresult;
            DataSet DS = new DataSet();
            strresult = objwsideco.login(ODocVerification.UserName, ODocVerification.Password, ODocVerification.AccountRef);

            System.IO.StringReader sr = new System.IO.StringReader(strresult);

            DS.ReadXml(sr);

            ODocVerification.SessionID = DS.Tables[0].Rows[0].Field<string>("sessionid").ToString();

            if (Convert.ToInt16(DS.Tables[0].Rows[0].Field<string>("code").ToString()) != 0)
            {
                throw new Exception("Login - " + DS.Tables[0].Rows[0].Field<string>("code").ToString());
            }
        }

        public string DVRequest(XDSPortalLibrary.Entity_Layer.ConsumerDocumentVerificationTrace ODocVerification)
        {
            string strresult;
            DataSet DS = new DataSet();
            strresult = objwsideco.ahvrequest(ODocVerification.SessionID, ODocVerification.IDNo, ODocVerification.SurName, ODocVerification.Initials, ODocVerification.AccNo, ODocVerification.BranchCode, ODocVerification.Acctype, ODocVerification.BankName);

            System.IO.StringReader sr = new System.IO.StringReader(strresult);

            DS.ReadXml(sr);

            ODocVerification.Key = Convert.ToInt16(DS.Tables["return"].Rows[0].Field<string>("refnumber").ToString());

            if (Convert.ToInt16(DS.Tables[0].Rows[0].Field<string>("code").ToString()) != 0)
            {
                throw new Exception("DVRequest - " + DS.Tables[0].Rows[0].Field<string>("code").ToString());
            }

            return strresult;
        }
        public string DVResult(XDSPortalLibrary.Entity_Layer.ConsumerDocumentVerificationTrace ODocVerification)
        {
            string strresult;
            DataSet DS = new DataSet();
            strresult = objwsideco.ahvresult(ODocVerification.SessionID, ODocVerification.Key.ToString());

            System.IO.StringReader sr = new System.IO.StringReader(strresult);

            DS.ReadXml(sr);

            if (Convert.ToInt16(DS.Tables[0].Rows[0].Field<string>("code").ToString()) != 0)
            {
                throw new Exception("DVResult - " + DS.Tables[0].Rows[0].Field<string>("code").ToString());
            }

            return strresult;
        }



        }
     
}
