﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace XDSPortalLibrary.Business_Layer
{
    public class IDVerificationIdeco
    {
        string Referenceno = string.Empty, keyType = string.Empty, xmlstring = string.Empty,result = string.Empty, returncode = string.Empty;

        wsideco.xmldata objwsideco = new XDSPortalLibrary.wsideco.xmldata();

        public IDVerificationIdeco()
        {
            this.keyType = "H";
        }

        private void validateinput(XDSPortalLibrary.Entity_Layer.IDVerificationIdeco Validate)
        {
            if (String.IsNullOrEmpty(Validate.IDno))
            {
                throw new Exception("IDno is mandatory");
            }
        }

        public void Login(XDSPortalLibrary.Entity_Layer.IDVerificationIdeco OIDVerification)
        {
            string strresult;
            DataSet DS = new DataSet();
            strresult = objwsideco.login(OIDVerification.UserName, OIDVerification.Password, OIDVerification.AccountRef);

            System.IO.StringReader sr = new System.IO.StringReader(strresult);

            DS.ReadXml(sr);

            OIDVerification.SessionID = DS.Tables[0].Rows[0].Field<string>("sessionid").ToString();

            if (Convert.ToInt16(DS.Tables[0].Rows[0].Field<string>("code").ToString()) != 0)
            {
                throw new Exception("Login - " + DS.Tables[0].Rows[0].Field<string>("code").ToString());
            }
        }

        public XDSPortalLibrary.Entity_Layer.Response VerifyID(XDSPortalLibrary.Entity_Layer.IDVerificationIdeco OIDVerification)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            validateinput(OIDVerification);
            try
            {
                Login(OIDVerification);

                string strresult;
                DataSet DS = new DataSet();

                strresult = objwsideco.verifyid(OIDVerification.SessionID, OIDVerification.IDno);

                System.IO.StringReader sr = new System.IO.StringReader(strresult);

                DS.ReadXml(sr);

                if ((Convert.ToInt16(DS.Tables[0].Rows[0].Field<string>("code").ToString()) != 0) && (Convert.ToInt16(DS.Tables[0].Rows[0].Field<string>("code").ToString()) != 1) && (Convert.ToInt16(DS.Tables[0].Rows[0].Field<string>("code").ToString()) != 105))
                {
                    throw new Exception(DS.Tables[0].Rows[0].Field<string>("message").ToString());
                }

                
                ObjResponse.ResponseData = strresult;
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseKeyType = this.keyType;
                ObjResponse.TmpReference = OIDVerification.SessionID;

                objwsideco.logoff(OIDVerification.SessionID);  
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message.ToString();
            }
            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response ProfileID(XDSPortalLibrary.Entity_Layer.IDVerificationIdeco OIDVerification)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            validateinput(OIDVerification);
            try
            {
                Login(OIDVerification);

                string strresult;
                DataSet DS = new DataSet();

                strresult = objwsideco.profileid(OIDVerification.SessionID, OIDVerification.IDno);

                System.IO.StringReader sr = new System.IO.StringReader(strresult);

                DS.ReadXml(sr);

                if ((Convert.ToInt16(DS.Tables[0].Rows[0].Field<string>("code").ToString()) != 0) && (Convert.ToInt16(DS.Tables[0].Rows[0].Field<string>("code").ToString()) != 1) && (Convert.ToInt16(DS.Tables[0].Rows[0].Field<string>("code").ToString()) != 105))
                {
                    throw new Exception(DS.Tables[0].Rows[0].Field<string>("message").ToString());
                }


                ObjResponse.ResponseData = strresult;
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseKeyType = this.keyType;
                ObjResponse.TmpReference = OIDVerification.SessionID;

                objwsideco.logoff(OIDVerification.SessionID);
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message.ToString();
            }
            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response IndividualID(XDSPortalLibrary.Entity_Layer.IDVerificationIdeco OIDVerification)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            validateinput(OIDVerification);
            try
            {
                Login(OIDVerification);

                string strresult;
                DataSet DS = new DataSet();

                strresult = objwsideco.getindividualfile(OIDVerification.SessionID, OIDVerification.IDno);

                System.IO.StringReader sr = new System.IO.StringReader(strresult);

                DS.ReadXml(sr);

                if ((Convert.ToInt16(DS.Tables[0].Rows[0].Field<string>("code").ToString()) != 0) && (Convert.ToInt16(DS.Tables[0].Rows[0].Field<string>("code").ToString()) != 1) && (Convert.ToInt16(DS.Tables[0].Rows[0].Field<string>("code").ToString()) != 105))
                {
                    throw new Exception(DS.Tables[0].Rows[0].Field<string>("message").ToString());
                }


                ObjResponse.ResponseData = strresult;
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseKeyType = this.keyType;
                ObjResponse.TmpReference = OIDVerification.SessionID;

                objwsideco.logoff(OIDVerification.SessionID);
            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message.ToString();
            }
            return ObjResponse;

        }

    }
}
