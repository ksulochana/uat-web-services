﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;


namespace XDSPortalLibrary.Business_Layer
{
   public class DefaultAlertListing
   {
       SqlConnection constring = null;


       public SqlConnection con
       {
           get
           {
               return this.constring;
           }
           set
           {
               this.constring = value;
           }
       }

       public DefaultAlertListing()
        {
           
        }

       public XDSPortalLibrary.Entity_Layer.Response LoadConsumerDefaultAlert(XDSPortalLibrary.Entity_Layer.ConsumerDefaultAlertListing oDefaultAlertListing)
       {
           XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();
           try
           {
               if (String.IsNullOrEmpty(oDefaultAlertListing.Surname) && String.IsNullOrEmpty(oDefaultAlertListing.FirstName) && String.IsNullOrEmpty(oDefaultAlertListing.AccountNo) && oDefaultAlertListing.Amount == 0 && String.IsNullOrEmpty(oDefaultAlertListing.StatusCode))
               {
                   throw new Exception("Your input must have a SurName, FirstName, Account Number, Amount and Status Code");
               }
               //else if (String.IsNullOrEmpty(oDefaultAlertListing.idno) || (String.IsNullOrEmpty(oDefaultAlertListing.PassportNo) || (oDefaultAlertListing.BirthDate.Year <= 1900)))
               //{
               //    throw new Exception("Either ID Number or the combination of passportno and date of Birth is mandatory");
               //}
               else if ((oDefaultAlertListing.SMSNotification == true) && (oDefaultAlertListing.Mobile == string.Empty))
               {
                   throw new Exception("Please enter a valid Mobile number of the consumer to send the SMS Notification");
               }
               else if ((oDefaultAlertListing.EmailNotification == true) && (oDefaultAlertListing.EmailId == string.Empty))
               {
                   throw new Exception("Please enter a valid Email address of the consumer to send the Email Notification");
               }
               else if (oDefaultAlertListing.Amount < 100)
               {
                   throw new Exception("Amount must be Greaterthan 100");
               }


               //DateTime dDate = DateTime.Now;
               //if (!string.IsNullOrEmpty(oDefaultAlertListing.BirthDate.ToShortDateString())) dDate = DateTime.Parse(oDefaultAlertListing.BirthDate.ToShortDateString());
               con.Open();
               SqlCommand cmd = new SqlCommand("sp_InsertOnlineConsumerDefaultAlert", con);
               cmd.CommandType = CommandType.StoredProcedure;
               cmd.Parameters.AddWithValue("@IDNo", oDefaultAlertListing.idno);
               cmd.Parameters.AddWithValue("@PassportNo", oDefaultAlertListing.PassportNo);
               cmd.Parameters.AddWithValue("@Surname", oDefaultAlertListing.Surname);
               cmd.Parameters.AddWithValue("@Firstname", oDefaultAlertListing.FirstName);
               cmd.Parameters.AddWithValue("@Secondname ", oDefaultAlertListing.SecondName);
               cmd.Parameters.AddWithValue("@BirthDate", oDefaultAlertListing.BirthDate);
               cmd.Parameters.AddWithValue("@Gender", oDefaultAlertListing.Gender);
               cmd.Parameters.AddWithValue("@AccountNo", oDefaultAlertListing.AccountNo);
               cmd.Parameters.AddWithValue("@SubaccountNo", oDefaultAlertListing.SubAccountNo);
               cmd.Parameters.AddWithValue("@AccountType", oDefaultAlertListing.Accountype);
               cmd.Parameters.AddWithValue("@Statuscode", oDefaultAlertListing.StatusCode);
               cmd.Parameters.AddWithValue("@Effectivedate", oDefaultAlertListing.Effectivedate);
               cmd.Parameters.AddWithValue("@Amount", oDefaultAlertListing.Amount);
               cmd.Parameters.AddWithValue("@Comments", oDefaultAlertListing.comments);
               cmd.Parameters.AddWithValue("@HTelePhone", oDefaultAlertListing.Hometelephone);
               cmd.Parameters.AddWithValue("@WTelePhone", oDefaultAlertListing.WorkTelephone);
               cmd.Parameters.AddWithValue("@CellPhone", oDefaultAlertListing.Mobile);
               cmd.Parameters.AddWithValue("@Address1", oDefaultAlertListing.Address1);
               cmd.Parameters.AddWithValue("@Address2", oDefaultAlertListing.Address2);
               cmd.Parameters.AddWithValue("@Address3", oDefaultAlertListing.Address3);
               cmd.Parameters.AddWithValue("@Address4", oDefaultAlertListing.Address4);
               cmd.Parameters.AddWithValue("@Email", oDefaultAlertListing.EmailId);
               cmd.Parameters.AddWithValue("@PostalCode", oDefaultAlertListing.Postalcode);
               cmd.Parameters.AddWithValue("@CreatedByUser", oDefaultAlertListing.Createdbyuser);
               //cmd.Parameters.AddWithValue("@SystemUserID", oDefaultAlertListing.SystemUserID);              
               //cmd.Parameters.AddWithValue("@ConsumerID", oDefaultAlertListing.ConsumerID);              
              
               int intProcessed = cmd.ExecuteNonQuery();

               if (intProcessed > 0)
               {
                   rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                   rp.ResponseData = "Enquiry Submitted successfully";
               }
               else
               {
                   rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                   rp.ResponseData = "This listing is not added to the consumer. Please contact XDS";
               }
           }
           catch (Exception ex)
           {
               rp.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
               rp.ResponseData = ex.Message;
           }


            return rp;
       }

       public DataSet GetConsumerDefaultAlertHistory(int SubscriberID, string UserName)
       {
           SqlCommand cmd;
           string SystemUser = UserName;
           con.Open();
           DataSet ds = new DataSet();

           cmd = new SqlCommand("sp_GetOnlineConsumerDefaultAlerts", con);
           cmd.CommandType = CommandType.StoredProcedure;
           cmd.Parameters.AddWithValue("@SystemUserName", UserName);
           cmd.Parameters.AddWithValue("@SubscriberID", SubscriberID);

           SqlDataAdapter da = new SqlDataAdapter("sp_GetOnlineConsumerDefaultAlerts", con);
           da.SelectCommand = cmd;
           da.Fill(ds);
           return ds;

       }

       public int RemoveConsumerDefaultAlert(string ReferenceNo, string UserName)
       {
           con.Open();
           SqlCommand cmd = new SqlCommand("sp_RemoveOnlineConsumerDefaultAlerts", con);
           cmd.CommandType = CommandType.StoredProcedure;
           cmd.Parameters.AddWithValue("@ReffNo", ReferenceNo);
           cmd.Parameters.AddWithValue("@UserName", UserName);
           int intProcesed = cmd.ExecuteNonQuery();
           return intProcesed;
       }

       public int LoadCommercialDefaultAlert(XDSPortalLibrary.Entity_Layer.ConsumerDefaultAlertListing oDefaultAlertListing)
       {
           //con.Open();
           //SqlCommand cmd = new SqlCommand("sp_InsertOnlineCommercialDefaultAlert", con);
           //cmd.CommandType = CommandType.StoredProcedure;
           //cmd.Parameters.AddWithValue("@RegistrationNo", oDefaultAlertListing.RegistrationNumber);
           //cmd.Parameters.AddWithValue("@CommercialName", oDefaultAlertListing.CommercialName);
           //cmd.Parameters.AddWithValue("@VatNumber", oDefaultAlertListing.VatNumber);
           //cmd.Parameters.AddWithValue("@AccountNo", oDefaultAlertListing.AccountNo);
           //cmd.Parameters.AddWithValue("@SubaccountNo", oDefaultAlertListing.SubAccountNo);
           //cmd.Parameters.AddWithValue("@AccountType", oDefaultAlertListing.Accountype);
           //cmd.Parameters.AddWithValue("@Statuscode", oDefaultAlertListing.StatusCode);
           //cmd.Parameters.AddWithValue("@Effectivedate", oDefaultAlertListing.Effectivedate);
           //cmd.Parameters.AddWithValue("@Amount", oDefaultAlertListing.Amount);
           //cmd.Parameters.AddWithValue("@Comments", oDefaultAlertListing.comments);
           //cmd.Parameters.AddWithValue("@HTelePhone", oDefaultAlertListing.Hometelephone);
           //cmd.Parameters.AddWithValue("@WTelePhone", oDefaultAlertListing.WorkTelephone);
           //cmd.Parameters.AddWithValue("@CellPhone", oDefaultAlertListing.Mobile);
           //cmd.Parameters.AddWithValue("@Address1", oDefaultAlertListing.Address1);
           //cmd.Parameters.AddWithValue("@Address2", oDefaultAlertListing.Address2);
           //cmd.Parameters.AddWithValue("@Address3", oDefaultAlertListing.Address3);
           //cmd.Parameters.AddWithValue("@Address4", oDefaultAlertListing.Address4);
           //cmd.Parameters.AddWithValue("@Email", oDefaultAlertListing.EmailId);
           //cmd.Parameters.AddWithValue("@PostalCode", oDefaultAlertListing.Postalcode);
           //cmd.Parameters.AddWithValue("@CreatedByUser", oDefaultAlertListing.Createdbyuser);

           //if (String.IsNullOrEmpty(oDefaultAlertListing.RegistrationNumber) || String.IsNullOrEmpty(oDefaultAlertListing.AccountNo) || (oDefaultAlertListing.Amount == 0) || String.IsNullOrEmpty(oDefaultAlertListing.StatusCode))
           //{
           //    throw new Exception("RegistrationNumber , Account No, Account Type, Amount, StatusCode are mandatory");
           //}
           //if (Convert.ToDecimal(oDefaultAlertListing.Amount) < 100)
           //{
           //    throw new Exception("Amount must be Greaterthan 100");
           //}
           ////if (!string.IsNullOrEmpty(oDefaultAlertListing.Effectivedate))
           ////{
           ////    try
           ////    {
           ////        Convert.ToDateTime(oDefaultAlertListing.Effectivedate);
           ////    }
           ////    catch
           ////    {
           ////        throw new Exception("Date Supplied is Invalid");
           ////    }
           ////}
           //int intProcesed = cmd.ExecuteNonQuery();
           return 1;

       }

       public DataSet GetCommercialDefaultAlertHistory(int SubscriberID, string User)
       {
           SqlCommand cmd;
           con.Open();
           DataSet ds = new DataSet();
           cmd = new SqlCommand("sp_GetOnlineCommercialDefaultAlerts", con);
           cmd.CommandType = CommandType.StoredProcedure;
           cmd.Parameters.AddWithValue("@SystemUserName", User);
           cmd.Parameters.AddWithValue("@SubscriberID", SubscriberID);

           SqlDataAdapter da = new SqlDataAdapter("sp_GetOnlineCommercialDefaultAlerts", con);
           da.SelectCommand = cmd;
           ds = new DataSet();
           da.Fill(ds);
           return ds;
       }

       public int RemoveCommercialDefaultAlert(string ReferenceNo, string Username)
       {


           // string username = "XDS";
           con.Open();
           SqlCommand cmd = new SqlCommand("sp_RemoveOnlineCommercialDefaultAlerts", con);
           cmd.CommandType = CommandType.StoredProcedure;
           cmd.Parameters.AddWithValue("@ReffNo", ReferenceNo);
           cmd.Parameters.AddWithValue("@UserName", Username);
           int intProcesed = cmd.ExecuteNonQuery();
           return intProcesed;


       }

    }
}
