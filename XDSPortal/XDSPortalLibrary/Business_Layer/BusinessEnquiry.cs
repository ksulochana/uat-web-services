using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace XDSPortalLibrary.Business_Layer
{
    public class BusinessEnquiry
    {
          string CommercialID = string.Empty,Referenceno = string.Empty,  keyType=string.Empty,xmlstring =string.Empty;

          SqlConnection constring = null;

        public SqlConnection ConnectionString
        {
            get
            {
                return this.constring;
            }
            set
            {
                this.constring = value;
            }
        }

        public BusinessEnquiry()
        {
            //Assign the class variables 
            this.keyType = "B";
        }
        private void validateinput(XDSPortalLibrary.Entity_Layer.BusinessEnquiry Validate)
        {
            if (String.IsNullOrEmpty(Validate.RegistrationNo) && String.IsNullOrEmpty(Validate.BusinessName))
            {
                throw new Exception("Registration No or Business Name are mandatory");
            }
        }
      
        public XDSPortalLibrary.Entity_Layer.Response search(XDSPortalLibrary.Entity_Layer.BusinessEnquiry ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                ObjResponse.ResponseKeyType = this.keyType;
                ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;
                validateinput(ObjSearch);

                SqlCommand ObjSQlCmd = new SqlCommand("spCommercial_S_Match_Standard", this.ConnectionString);
                ObjSQlCmd.CommandTimeout = 0;
                ObjSQlCmd.CommandType = CommandType.StoredProcedure;
                ObjSQlCmd.Parameters.AddWithValue("@RegistrationNo", ObjSearch.RegistrationNo);
                ObjSQlCmd.Parameters.AddWithValue("@Businessname", ObjSearch.BusinessName);
                ObjSQlCmd.Parameters.AddWithValue("@vatNumber", ObjSearch.VATNumber);
                ObjSQlCmd.Parameters.AddWithValue("@SolePropIDNo", ObjSearch.SolePropIDNo);
                ObjSQlCmd.Parameters.AddWithValue("@TrustNo", ObjSearch.TrustNo);

                if (ConnectionString.State == ConnectionState.Closed)
                    ConnectionString.Open();

                DataSet ObjDS = new DataSet();
                SqlDataAdapter ObjDA = new SqlDataAdapter(ObjSQlCmd);
                DataTable dt = new DataTable("CommercialDetails");

                ObjDA.Fill(dt);

                ConnectionString.Close();
                XDSPortalLibrary.Entity_Layer.Response ObjBonusResponse = new XDSPortalLibrary.Entity_Layer.Response();

                DataColumn dcBonusSeg = new DataColumn("BonusXML");
                DataColumn dcTempReference = new DataColumn("TempReference");

                dt.Columns.Add(dcBonusSeg);
                dt.Columns.Add(dcTempReference);

                if (ObjSearch.BonusCheck)
                {
                    // Genrate error message if the search gives more than 20 results 
                    if (dt.Rows.Count < 21)
                    {

                        // Get the Bonus XML for each of the Consumers / Companies matched

                        foreach (DataRow dr in dt.Rows)
                        {

                            ObjSearch.CommercialID = Convert.ToInt32(dr["CommercialID"]);
                            ObjSearch.IsMatching = true;
                            ObjBonusResponse = GetData(ObjSearch);
                            dr["BonusXML"] = "";
                            if (ObjBonusResponse.ResponseData.ToString() != String.Empty && ObjBonusResponse.ResponseData.ToString() != "<BonusSegments />")
                            {
                                dr["BonusXML"] = ObjBonusResponse.ResponseData;
                            }
                            dr["TempReference"] = ObjBonusResponse.TmpReference;

                        }
                        dt.AcceptChanges();
                    }
                    else
                    {
                        throw new Exception("There are too many results from this search. Please refine your search and try again");
                    }
                }
                else
                {
                    foreach (DataRow dr in dt.Rows)
                    {           
                        dr["BonusXML"] = "";
                        dr["TempReference"] = "";

                    }
                    dt.AcceptChanges();
                }

                ObjDS.Tables.Add(dt);
                ObjDS.DataSetName = "DsCommercialMatch";


                if (ObjDS.Tables[0].Rows.Count == 1)
                {
                    //Single Match
                    if (ObjSearch.ConfirmationChkBox == false)
                    {
                        // Single Match --> No Bonus --> ConfirmationCheckbox is not Checked --> Generate the Report XML

                        if (ObjDS.Tables[0].Rows[0].Field<string>("BonusXML").ToString() == string.Empty)
                        {

                            ObjResponse = GetReportXMLForSingleMAtch(ObjSearch);
                            ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        }
                        else
                        {
                            ObjResponse.ResponseData = ObjDS.GetXml();
                            ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single;
                        }
                        ObjResponse.ResponseKeyType = this.keyType;
                        ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.CommercialID);
                    }
                    else if (ObjSearch.ConfirmationChkBox == true)
                    {
                        //Display user info
                        xmlstring = ObjDS.GetXml();
                        CommercialID = ObjDS.Tables[0].Rows[0]["CommercialID"].ToString();
                        ObjSearch.CommercialID = Convert.ToInt32(ObjDS.Tables[0].Rows[0]["CommercialID"]);
                        ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single;
                        ObjResponse.ResponseData = xmlstring;
                        ObjResponse.ResponseKeyType = this.keyType;
                        ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.CommercialID);
                    }
                }
                else if (ObjDS.Tables[0].Rows.Count > 1)
                {
                    //Return User Info XML
                    xmlstring = ObjDS.GetXml();
                    ObjResponse.ResponseKeyType = this.keyType;
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple;
                    ObjResponse.ResponseData = xmlstring;

                }
                else if (ObjDS.Tables[0].Rows.Count == 0)
                {
                    // No match
                    xmlstring = ObjDS.GetXml();
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                    ObjResponse.ResponseData = xmlstring;

                }

                dcBonusSeg.Dispose();
                dcTempReference.Dispose();
                dt.Dispose();
                ObjSQlCmd.Dispose();
                ObjDS.Dispose();
                ObjDA.Dispose();
                
            }

            catch (Exception a)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = a.Message;
            }
            return ObjResponse;
        }
      
        public XDSPortalLibrary.Entity_Layer.Response GetData(XDSPortalLibrary.Entity_Layer.BusinessEnquiry ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                if (ObjSearch.CommercialID > 0)
                {

                    if (ObjSearch.IsMatching == true)
                    {
                        // Get the Bonus Segments for each of the matching results during matching

                        XDSPortalLibrary.Entity_Layer.GetBonusSegments ObjBonus = new XDSPortalLibrary.Entity_Layer.GetBonusSegments();

                       // ObjBonus.SubscriberID = ObjSearch.subscriberID;
                        ObjBonus.ProductID = ObjSearch.ProductID;
                        ObjBonus.KeyID = Convert.ToInt32(ObjSearch.CommercialID);
                        ObjBonus.KeyType = this.keyType;
                        ObjBonus.ExternalReference = ObjSearch.ExternalReference;
                       // ObjBonus.ReferenceNo = ObjSearch.ReferenceNo;
                        ObjBonus.DataSegments = ObjSearch.DataSegments;

                        GetBonusSegments ObjGetBonus = new GetBonusSegments();
                        ObjGetBonus.ConnectionString = this.ConnectionString;

                        ObjResponse = ObjGetBonus.BonusSegments(ObjBonus);
                        ObjResponse.ResponseKeyType = this.keyType;
                        ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.CommercialID);
                    }
                    else
                    {
                        // Generate the Report XML when user Submit Multipletrace
                        ObjResponse = GetReportXMLForSingleMAtch(ObjSearch);
                    }
                }

                
                else
                {
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    ObjResponse.ResponseData = "Insufficient data supplied";
                }
            }
            catch (Exception a)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = a.Message;
            }

            return ObjResponse;
        }

        private XDSPortalLibrary.Entity_Layer.Response GetReportXMLForSingleMAtch(XDSPortalLibrary.Entity_Layer.BusinessEnquiry ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                XDSPortalLibrary.Entity_Layer.GenerateReportXML ObjReportParam = new XDSPortalLibrary.Entity_Layer.GenerateReportXML();
                ObjReportParam.SubscriberID = ObjSearch.subscriberID;
                ObjReportParam.ProductID = ObjSearch.ProductID;
                ObjReportParam.KeyID = ObjSearch.CommercialID;
                ObjReportParam.KeyType = this.keyType;
                ObjReportParam.ReferenceNo = ObjSearch.ReferenceNo;
                ObjReportParam.ExternalReference = ObjSearch.ExternalReference;
                ObjReportParam.TmpReference = ObjSearch.TmpReference;
                ObjReportParam.DataSegments = ObjSearch.DataSegments;

                if (!(ObjSearch.BonusSegments == null))
                {
                    ObjReportParam.BonusSegments = ObjSearch.BonusSegments;
                }
                else
                {
                    DataSet ds = new DataSet();
                    DataTable dt = new DataTable();
                    ds.DataSetName = "BonusSegments";
                    dt.TableName = "Segment";
                    ds.Tables.Add(dt);
                    ObjReportParam.BonusSegments = ds;
                }

                GenerateReportXML ObjReport = new GenerateReportXML();
                ObjReport.ConnectionString = this.ConnectionString;
                ObjResponse = ObjReport.GenerateXML(ObjReportParam);

                ObjResponse.ResponseKeyType = this.keyType;
                ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.CommercialID);

                
            }

            catch (Exception a)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = a.Message;
            }

            return ObjResponse;
        }

    }
}
