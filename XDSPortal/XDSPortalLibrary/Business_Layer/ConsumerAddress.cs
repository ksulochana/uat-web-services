﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace XDSPortalLibrary.Business_Layer
{
  public  class ConsumerAddress
    {
        string  consumerid = string.Empty, Referenceno = string.Empty, keyType = string.Empty, xmlstring = string.Empty;

        SqlConnection constring = null;

        public SqlConnection ConnectionString
        {
            get
            {
                return this.constring;
            }
            set
            {
                this.constring = value;
            }
        }

        public ConsumerAddress()
        {
            //Assign the class variables 
             this.keyType = "C";
        }
        private void validateinput(XDSPortalLibrary.Entity_Layer.ConsumerAddress Validate)
        {
            if (String.IsNullOrEmpty(Validate.Province) || String.IsNullOrEmpty(Validate.City) || String.IsNullOrEmpty(Validate.Suburb) || String.IsNullOrEmpty(Validate.PostalCode) || String.IsNullOrEmpty(Validate.StreetName))
            {
                throw new Exception("The combination of Province, City, Suburb, PostalCode, StreetName or Province, City, Suburb, PostalCode, Postal Number is Mandatory");
            }
        }
      
        public XDSPortalLibrary.Entity_Layer.Response search(XDSPortalLibrary.Entity_Layer.ConsumerAddress ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                validateinput(ObjSearch);

                ObjResponse.ResponseKeyType = this.keyType;
                ObjResponse.ResponseExternalReferenceNo = string.Empty;
                ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

                DataSet ObjDS = new DataSet();

                DataTable dt = SearchingAddress(this.ConnectionString, ObjSearch.Country, ObjSearch.Province, ObjSearch.City, ObjSearch.Suburb, ObjSearch.PostalCode, ObjSearch.StreetName, ObjSearch.StreetNo, ObjSearch.BuildingName);
                dt.TableName = "ConsumerDetails";

                DataColumn dcBonusSeg = new DataColumn("BonusXML");
                DataColumn dcTempReference = new DataColumn("TempReference");

                dt.Columns.Add(dcBonusSeg);
                dt.Columns.Add(dcTempReference);


                if (ObjSearch.BonusCheck)
                {
                    // Genrate error message if the search gives more than 20 results 

                    if (dt.Rows.Count < 21)
                    {

                        XDSPortalLibrary.Entity_Layer.Response ObjBonusResponse = new XDSPortalLibrary.Entity_Layer.Response();

                        // Get the Bonus XML for each of the Consumers / Companies matched


                        foreach (DataRow dr in dt.Rows)
                        {

                            ObjSearch.ConsumerID = Convert.ToInt32(dr["ConsumerID"]);
                            ObjSearch.IsConsumerMatching = true;
                            ObjBonusResponse = GetData(ObjSearch);

                            dr["BonusXML"] = "";
                            if (ObjBonusResponse.ResponseData.ToString() != String.Empty && ObjBonusResponse.ResponseData.ToString() != "<BonusSegments />")
                            {
                                dr["BonusXML"] = ObjBonusResponse.ResponseData;

                            }
                            dr["TempReference"] = ObjBonusResponse.TmpReference;

                        }
                        dt.AcceptChanges();

                    }
                    else
                    {
                        throw new Exception("There are too many results from this search. Please refine your search by providing a surname and try again");
                    }
                }
                else
                {
                    foreach (DataRow dr in dt.Rows)
                    {

                        dr["BonusXML"] = "";
                        dr["TempReference"] = "";

                    }
                    dt.AcceptChanges();

                }
                ObjDS.Tables.Add(dt);
                ObjDS.DataSetName = "ListOfConsumers";


                if (ObjDS.Tables[0].Rows.Count == 1)
                {
                    //Single Match

                    if (ObjSearch.ConfirmationChkBox == false)
                    {
                        // Single Match --> No Bonus --> ConfirmationCheckbox is not Checked --> Generate the Report XML


                        if (ObjDS.Tables[0].Rows[0].Field<string>("BonusXML").ToString() == string.Empty)
                        {

                            ObjResponse = GetReportXMLForSingleMAtch(ObjSearch);
                            ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                        }
                        else
                        {
                            ObjResponse.ResponseData = ObjDS.GetXml();
                            ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single;
                        }

                        ObjResponse.ResponseKeyType = this.keyType;
                        ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.ConsumerID);
                    }
                    else if (ObjSearch.ConfirmationChkBox == true)
                    {
                        //Display user info
                        xmlstring = ObjDS.GetXml();
                        ObjSearch.ConsumerID = Convert.ToInt32(ObjDS.Tables[0].Rows[0]["ConsumerID"].ToString());
                        ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single;
                        ObjResponse.ResponseData = xmlstring;
                        ObjResponse.ResponseKeyType = this.keyType;
                        ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.ConsumerID);
                    }
                }
                else if (ObjDS.Tables[0].Rows.Count > 1)
                {
                    //Return User Info XML
                    xmlstring = ObjDS.GetXml();
                    ObjResponse.ResponseKeyType = this.keyType;
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple;
                    ObjResponse.ResponseData = xmlstring;
                    //ObjResponse.ResponseKey = Convert.ToInt16(this.consumerid);
                }

                else if (ObjDS.Tables[0].Rows.Count == 0)
                {
                    // No match

                    xmlstring = ObjDS.GetXml();
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                    ObjResponse.ResponseData = xmlstring;
                    //ObjResponse.ResponseKey = Convert.ToInt16(this.consumerid);
                }
                dcBonusSeg.Dispose();
                dcTempReference.Dispose();

                dt.Dispose();
                ObjDS.Dispose();
            }

            catch (Exception a)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = a.Message;
                // ObjResponse.ResponseKey = Convert.ToInt16(this.consumerid);
            }

            return ObjResponse;
        }
        public XDSPortalLibrary.Entity_Layer.Response GetData(XDSPortalLibrary.Entity_Layer.ConsumerAddress ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                if (ObjSearch.ConsumerID > 0 )
                {
                    if (ObjSearch.IsConsumerMatching == true)
                    {
                        // Get the Bonus Segments for each of the matching results during matching

                        XDSPortalLibrary.Entity_Layer.GetBonusSegments ObjBonus = new XDSPortalLibrary.Entity_Layer.GetBonusSegments();

                       // ObjBonus.SubscriberID = ObjSearch.subscriberID;
                        ObjBonus.ProductID = ObjSearch.ProductID;
                        ObjBonus.KeyID = Convert.ToInt32(ObjSearch.ConsumerID);
                        ObjBonus.KeyType = this.keyType;
                        ObjBonus.ExternalReference = ObjSearch.ExternalReference;
                        //ObjBonus.ReferenceNo = ObjSearch.ReferenceNo;
                        ObjBonus.DataSegments = ObjSearch.DataSegments;

                        GetBonusSegments ObjGetBonus = new GetBonusSegments();
                        ObjGetBonus.ConnectionString = this.ConnectionString;

                        ObjResponse = ObjGetBonus.BonusSegments(ObjBonus);
                        ObjResponse.ResponseKeyType = this.keyType;
                        ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.ConsumerID);
                    }
                    else
                    {
                        // Generate the Report XML when user Submit Multipletrace

                        ObjResponse = GetReportXMLForSingleMAtch(ObjSearch);
                    }
                }
                else
                {
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    ObjResponse.ResponseData = "Insufficient data supplied";
                }
            }
            catch (Exception a)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = a.Message;
            }

            return ObjResponse;
        }

        private DataTable SearchingAddress(System.Data.SqlClient.SqlConnection conn, string countryName, string provinceName, string cityName, string suburbName, string postalCode, string streetName, string streetNo, string buildingName)
        {
            try
            {

                string dupeCode = "";

                System.Data.SqlClient.SqlCommand comm = null;
                DataTable dt = new DataTable();

                comm = new System.Data.SqlClient.SqlCommand("Search", conn);
                comm.CommandType = CommandType.StoredProcedure;
                comm.CommandTimeout = 0;

                if (conn.State == ConnectionState.Closed)
                    conn.Open();

                // To Run DupeCode Here

                System.Data.SqlClient.SqlParameter param1 = new System.Data.SqlClient.SqlParameter("@DupCode", SqlDbType.VarChar, 50);
                param1.Value = dupeCode;
                comm.Parameters.Add(param1);

                System.Data.SqlClient.SqlParameter param2 = new System.Data.SqlClient.SqlParameter("@CountryName", SqlDbType.VarChar, 50);
                param2.Value = countryName;
                comm.Parameters.Add(param2);

                System.Data.SqlClient.SqlParameter param3 = new System.Data.SqlClient.SqlParameter("@ProvinceName", SqlDbType.VarChar, 50);
                param3.Value = provinceName;
                comm.Parameters.Add(param3);


                System.Data.SqlClient.SqlParameter param4 = new System.Data.SqlClient.SqlParameter("@CityName", SqlDbType.VarChar, 50);
                param4.Value = cityName;
                comm.Parameters.Add(param4);

                System.Data.SqlClient.SqlParameter param5 = new System.Data.SqlClient.SqlParameter("@SuburbName", SqlDbType.VarChar, 50);
                param5.Value = suburbName;
                comm.Parameters.Add(param5);

                System.Data.SqlClient.SqlParameter param6 = new System.Data.SqlClient.SqlParameter("@Postalcode", SqlDbType.VarChar, 10);
                param6.Value = postalCode;
                comm.Parameters.Add(param6);

                System.Data.SqlClient.SqlParameter param7 = new System.Data.SqlClient.SqlParameter("@StreetName", SqlDbType.VarChar, 50);
                param7.Value = streetName;
                comm.Parameters.Add(param7);

                System.Data.SqlClient.SqlParameter param8 = new System.Data.SqlClient.SqlParameter("@StreetNo", SqlDbType.VarChar, 50);
                param8.Value = streetNo;
                comm.Parameters.Add(param8);

                System.Data.SqlClient.SqlParameter param9 = new System.Data.SqlClient.SqlParameter("@BuildingName", SqlDbType.VarChar, 50);
                param9.Value = buildingName;
                comm.Parameters.Add(param9);

                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(comm);
                da.Fill(dt);

                conn.Close();

                return dt;
            }
            catch (Exception ex)
            {  
                throw new Exception(ex.Message);
            }
            
        }
        private XDSPortalLibrary.Entity_Layer.Response GetReportXMLForSingleMAtch(XDSPortalLibrary.Entity_Layer.ConsumerAddress ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                XDSPortalLibrary.Entity_Layer.GenerateReportXML ObjReportParam = new XDSPortalLibrary.Entity_Layer.GenerateReportXML();
                ObjReportParam.SubscriberID = ObjSearch.subscriberID;
                ObjReportParam.ProductID = ObjSearch.ProductID;
                ObjReportParam.KeyID = ObjSearch.ConsumerID;
                ObjReportParam.KeyType = this.keyType;
                ObjReportParam.ReferenceNo = ObjSearch.ReferenceNo;
                ObjReportParam.ExternalReference = ObjSearch.ExternalReference;
                ObjReportParam.TmpReference = ObjSearch.TmpReference;
                ObjReportParam.DataSegments = ObjSearch.DataSegments;

                if (!(ObjSearch.BonusSegments == null))
                {
                    ObjReportParam.BonusSegments = ObjSearch.BonusSegments;
                }
                else
                {
                    DataSet ds = new DataSet();
                    DataTable dt = new DataTable();
                    ds.DataSetName = "BonusSegments";
                    dt.TableName = "Segment";
                    ds.Tables.Add(dt);
                    ObjReportParam.BonusSegments = ds;

                    ds.Dispose();
                    dt.Dispose();
                }

                GenerateReportXML ObjReport = new GenerateReportXML();
                ObjReport.ConnectionString = this.ConnectionString;
                ObjResponse = ObjReport.GenerateXML(ObjReportParam);

                ObjResponse.ResponseKeyType = this.keyType;
                ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.ConsumerID);
            }

            catch (Exception a)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = a.Message;
            }

            return ObjResponse;
        }


    }
}
