﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace XDSPortalLibrary.Business_Layer
{
    public class BankCodeEnquiry
    {
        string Referenceno = string.Empty, keyType = string.Empty, xmlstring = string.Empty;
        SqlConnection constring = null;

        public SqlConnection ConnectionString
        {
            get
            {
                return this.constring;
            }
            set
            {
                this.constring = value;
            }
        }
        public BankCodeEnquiry()
        {
            this.keyType = "F";
        }
        public XDSPortalLibrary.Entity_Layer.Response search(XDSPortalLibrary.Entity_Layer.BankCodes ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                SqlCommand ObjSQlCmd = new SqlCommand("InsertBankCodes", this.ConnectionString);
                ObjSQlCmd.CommandTimeout = 0;
                ObjSQlCmd.CommandType = CommandType.StoredProcedure;
                ObjSQlCmd.Parameters.AddWithValue("@RequestType", ObjSearch.RequestType);
                ObjSQlCmd.Parameters.AddWithValue("@Region",ObjSearch.Region);
                ObjSQlCmd.Parameters.AddWithValue("@ClientName", ObjSearch.AccHolder);
                ObjSQlCmd.Parameters.AddWithValue("@Bank", ObjSearch.BankName);
                ObjSQlCmd.Parameters.AddWithValue("@BranchCode", ObjSearch.BranchCode);
                ObjSQlCmd.Parameters.AddWithValue("@BranchName", ObjSearch.BranchName);
                ObjSQlCmd.Parameters.AddWithValue("@AccountNumber", ObjSearch.AccNo);                
                ObjSQlCmd.Parameters.AddWithValue("@Amount", ObjSearch.Amount);
                ObjSQlCmd.Parameters.AddWithValue("@Terms", ObjSearch.Terms);
                ObjSQlCmd.Parameters.AddWithValue("@CountryShortCode", ObjSearch.CountryCode);
                ObjSQlCmd.Parameters.AddWithValue("@RegistrationNo", ObjSearch.RegistrationNo);
               


                if (ConnectionString.State == ConnectionState.Closed)
                    ConnectionString.Open();

                DataSet ObjDS = new DataSet();
                SqlDataAdapter ObjDA = new SqlDataAdapter(ObjSQlCmd);
                DataTable dt = new DataTable("BankCodes");

                ObjDA.Fill(dt);

                ConnectionString.Close();

                ObjSearch.Key = Convert.ToInt32(dt.Rows[0]["BankCodeID"].ToString());
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single;
                ObjResponse.ResponseKeyType = this.keyType;
                ObjResponse.ResponseKey = ObjSearch.Key;
                ObjResponse.ResponseData = "Enquiry Submitted Successfully";


            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message.ToString();
            }
            return ObjResponse;
        }

        public XDSPortalLibrary.Entity_Layer.Response GetData(XDSPortalLibrary.Entity_Layer.BankCodes ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();

            try
            {
                SqlCommand ObjSQlCmd = new SqlCommand("GetRecordXML", this.ConnectionString);
                ObjSQlCmd.CommandTimeout = 0;
                ObjSQlCmd.CommandType = CommandType.StoredProcedure;
                ObjSQlCmd.Parameters.AddWithValue("@BankCodeID", ObjSearch.Key);

                if (ConnectionString.State == ConnectionState.Closed)
                    ConnectionString.Open();

                DataSet ObjDS = new DataSet("BankCodes");
                SqlDataAdapter ObjDA = new SqlDataAdapter(ObjSQlCmd);
                DataTable dt = new DataTable("BankCodeResponse");

                ObjDA.Fill(dt);
                ObjDS.Tables.Add(dt);
                ConnectionString.Close();

                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None;
                ObjResponse.ResponseKeyType = this.keyType;
                ObjResponse.ResponseKey = ObjSearch.Key;
                ObjResponse.ResponseData = ObjDS.GetXml();

            }
            catch (Exception ex)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = ex.Message.ToString();
            }
            return ObjResponse;
        }
    }
}
