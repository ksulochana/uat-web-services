﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Drawing;
namespace XDSPortalLibrary.Business_Layer
{
    public class CustomVetting
    {
     
         string Referenceno = string.Empty,  keyType=string.Empty,xmlstring =string.Empty,ComemrcialID=string.Empty,DirectorID=string.Empty;

         SqlConnection constring = null;


         public SqlConnection ConnectionString
        {
            get
            {
                return this.constring;
            }
            set
            {
                this.constring = value;
            }
        }

         public CustomVetting()
        {
            //Assign the class variables 
            this.keyType = "C";
        }
        private void validateinput(XDSPortalLibrary.Entity_Layer.ConsumerTrace Validate)
        {
            if (String.IsNullOrEmpty(Validate.IDno) && String.IsNullOrEmpty(Validate.Passportno) && (String.IsNullOrEmpty(Validate.Surname) || (Validate.DOB.Year <= 1900) || String.IsNullOrEmpty(Validate.FirstName)))
            {
                throw new Exception("Your search input must have an Id Number or a Passport Number or a combination of Surname, date of birth and First name");
            }
        }
        
     
        public XDSPortalLibrary.Entity_Layer.Response search(XDSPortalLibrary.Entity_Layer.ConsumerTrace ObjSearch)
        {
            //Executes the Matching procedure for Consumertrace and generates the ouput XML
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            

            ObjResponse.ResponseKeyType = this.keyType;
            ObjResponse.ResponseExternalReferenceNo = ObjSearch.ExternalReference;
            ObjResponse.ResponseReferenceNo = ObjSearch.ReferenceNo;

            try
            {
                validateinput(ObjSearch);
                SqlCommand ObjSQlCmd = new SqlCommand("spCustomVettingC", this.ConnectionString);
                ObjSQlCmd.CommandTimeout = 0;

                ObjSQlCmd.CommandTimeout = 0;
                ObjSQlCmd.CommandType = CommandType.StoredProcedure;

                if (ConnectionString.State == ConnectionState.Closed)
                    ConnectionString.Open();

                ObjSQlCmd.Parameters.AddWithValue("@IDNo", ObjSearch.IDno);
                ObjSQlCmd.Parameters.AddWithValue("@ConsumerID", ObjSearch.ConsumerID);
                ObjSQlCmd.Parameters.AddWithValue("@FirstName", ObjSearch.FirstName);
                ObjSQlCmd.Parameters.AddWithValue("@GrossMonthlyIncome", ObjSearch.GrossMonthlyIncome);
                ObjSQlCmd.Parameters.AddWithValue("@Surname", ObjSearch.Surname);
                ObjSQlCmd.Parameters.AddWithValue("@ReferenceNo", ObjSearch.ReferenceNo);
                ObjSQlCmd.Parameters.AddWithValue("@ExternalReference", ObjSearch.ExternalReference);

                DataSet ObjDSReport = new DataSet();

                SqlDataAdapter ObjDAReport = new SqlDataAdapter(ObjSQlCmd);

                ObjDAReport.Fill(ObjDSReport);

                ConnectionString.Close();

                for (int i = 0; i < ObjDSReport.Tables.Count; i++)
                {
                    if (ObjDSReport.Tables[i].Columns.Contains("DisplayText"))
                    {
                        foreach (DataRow row in ObjDSReport.Tables[i].Rows)
                        {
                            ObjDSReport.Tables[i].TableName = row["DisplayText"].ToString();
                        }
                        ObjDSReport.Tables[i].Columns.Remove("DisplayText");
                    }
                    else
                    {
                        ObjDSReport.Tables.RemoveAt(i);
                        i--;
                    }
                }
                ObjDSReport.DataSetName = "ConnectCustomVettingC";

                string xml = ObjDSReport.GetXml();
                xml = xml.Replace("_x0036_", "");
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report;
                ObjResponse.ResponseData = xml;

                ObjDSReport.Dispose();
                ObjSQlCmd.Dispose();
                ObjDAReport.Dispose();

            }

            catch (Exception a)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = a.Message;
             }

            return ObjResponse;

        }

        public XDSPortalLibrary.Entity_Layer.Response GetData(XDSPortalLibrary.Entity_Layer.ConsumerTrace ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                if (ObjSearch.ConsumerID > 0)
                {
                    if (ObjSearch.IsConsumerMatching == true)
                    {
                        // Get the Bonus Segments for each of the matching results during matching
                        XDSPortalLibrary.Entity_Layer.GetBonusSegments ObjBonus = new XDSPortalLibrary.Entity_Layer.GetBonusSegments();

                        //ObjBonus.SubscriberID = ObjSearch.subscriberID;
                        ObjBonus.ProductID = ObjSearch.ProductID;
                        ObjBonus.KeyID = Convert.ToInt32(ObjSearch.ConsumerID);
                        ObjBonus.KeyType = this.keyType;
                        ObjBonus.ExternalReference = ObjSearch.ExternalReference;
                        //ObjBonus.ReferenceNo = ObjSearch.ReferenceNo;
                        ObjBonus.DataSegments = ObjSearch.DataSegments;

                        GetBonusSegments ObjGetBonus = new GetBonusSegments();
                        ObjGetBonus.ConnectionString = this.ConnectionString;

                        ObjResponse = ObjGetBonus.BonusSegments(ObjBonus);
                        ObjResponse.ResponseKeyType = this.keyType;
                        ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.ConsumerID);
                    }
                    else
                    {
                        // Generate the Report XML when user Submit Multipletrace 
                        ObjResponse = GetReportXMLForSingleMAtch(ObjSearch);
                    }
                }
                else
                {
                    ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                    ObjResponse.ResponseData = "Insufficient data supplied";
                }
            }
            catch (Exception a)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = a.Message;
            }

            return ObjResponse;
        }
        private XDSPortalLibrary.Entity_Layer.Response GetReportXMLForSingleMAtch(XDSPortalLibrary.Entity_Layer.ConsumerTrace ObjSearch)
        {
            XDSPortalLibrary.Entity_Layer.Response ObjResponse = new XDSPortalLibrary.Entity_Layer.Response();
            try
            {
                XDSPortalLibrary.Entity_Layer.GenerateReportXML ObjReportParam = new XDSPortalLibrary.Entity_Layer.GenerateReportXML();
                ObjReportParam.SubscriberID = ObjSearch.subscriberID;
                ObjReportParam.ProductID = ObjSearch.ProductID;
                ObjReportParam.KeyID = ObjSearch.ConsumerID;
                ObjReportParam.KeyType = this.keyType;
                ObjReportParam.ReferenceNo = ObjSearch.ReferenceNo;
                ObjReportParam.ExternalReference = ObjSearch.ExternalReference;
                ObjReportParam.TmpReference = ObjSearch.TmpReference;
                ObjReportParam.DataSegments = ObjSearch.DataSegments;

                if (!(ObjSearch.BonusSegments == null))
                {
                    ObjReportParam.BonusSegments = ObjSearch.BonusSegments;
                }
                else
                {
                    DataSet ds = new DataSet();
                    DataTable dt = new DataTable();
                    ds.DataSetName = "BonusSegments";
                    dt.TableName = "Segment";
                    ds.Tables.Add(dt);
                    ObjReportParam.BonusSegments = ds;

                    ds.Dispose();
                    dt.Dispose();
                }

                GenerateReportXML ObjReport = new GenerateReportXML();
                ObjReport.ConnectionString = this.ConnectionString;
                ObjResponse = ObjReport.GenerateXML(ObjReportParam);

                ObjResponse.ResponseKeyType = this.keyType;
                ObjResponse.ResponseKey = Convert.ToInt32(ObjSearch.ConsumerID);
            }

            catch (Exception a)
            {
                ObjResponse.ResponseStatus = XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error;
                ObjResponse.ResponseData = a.Message;
            }
            
            return ObjResponse;
        }

    }
}
