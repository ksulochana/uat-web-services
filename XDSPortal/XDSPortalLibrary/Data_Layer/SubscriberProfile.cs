﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace XDSPortalLibrary.Data_Layer
{
   public class SubscriberProfile
    {
        public SubscriberProfile()
        {
        }
        public XDSPortalLibrary.Entity_Layer.SubscriberProfile GetProductReportsRecord(string conString, int SubscriberID)
        {
            XDSPortalLibrary.Entity_Layer.SubscriberProfile objSubProfile = new XDSPortalLibrary.Entity_Layer.SubscriberProfile();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = "select * from subscriberProfile where SubscriberID = " + SubscriberID;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objSubProfile.SubscriberID = Convert.ToInt16(Dr["SubscriberID"]);
                objSubProfile.AuthenticationSearchOnIDNoYN = Convert.ToBoolean(Dr["AuthenticationSearchOnIDNoYN"]);
                objSubProfile.AuthenticationSearchOnCellPhoneNoYN = Convert.ToBoolean(Dr["AuthenticationSearchOnCellPhoneNoYN"]);
                objSubProfile.AuthenticationSearchOnAccountNoYN = Convert.ToBoolean(Dr["AuthenticationSearchOnAccountNoYN"]);
                objSubProfile.AuthenticationDefaultIDVerificationSelectionInd = Dr["AuthenticationDefaultIDVerificationSelectionInd"].ToString();
                objSubProfile.AuthenticationPrimaryNoOfPositiveHighPriorityQuestions = Convert.ToInt16(Dr["AuthenticationPrimaryNoOfPositiveHighPriorityQuestions"]);
                objSubProfile.AuthenticationPrimaryNoOfPositiveMediumPriorityQuestions = Convert.ToInt16(Dr["AuthenticationPrimaryNoOfPositiveMediumPriorityQuestions"]);
                objSubProfile.AuthenticationPrimaryNoOfPositiveLowPriorityQuestions = Convert.ToInt16(Dr["AuthenticationPrimaryNoOfPositiveLowPriorityQuestions"]);
                objSubProfile.AuthenticationPrimaryNoOfNegativeHighPriorityQuestions = Convert.ToInt16(Dr["AuthenticationPrimaryNoOfNegativeHighPriorityQuestions"]);
                objSubProfile.AuthenticationPrimaryNoOfNegativeMediumPriorityQuestions = Convert.ToInt16(Dr["AuthenticationPrimaryNoOfNegativeMediumPriorityQuestions"]);
                objSubProfile.AuthenticationPrimaryNoOfNegativeLowPriorityQuestions = Convert.ToInt16(Dr["AuthenticationPrimaryNoOfNegativeLowPriorityQuestions"]);
                objSubProfile.AuthenticationPrimaryNoOfYesNoQuestions = Convert.ToInt16(Dr["AuthenticationPrimaryNoOfYesNoQuestions"]);
                objSubProfile.AuthenticationPrimaryNoOfAnswersPerQuestion = Convert.ToInt16(Dr["AuthenticationPrimaryNoOfAnswersPerQuestion"]);
                objSubProfile.AuthenticationPrimaryRequiredAuthenticatedPerc = Convert.ToInt16(Dr["AuthenticationPrimaryRequiredAuthenticatedPerc"]);
                objSubProfile.AuthenticationPrimaryEnsureRequiredNoOfQuestionsYN = Convert.ToBoolean(Dr["AuthenticationPrimaryEnsureRequiredNoOfQuestionsYN"]);
                objSubProfile.AuthenticationSecondaryNoOfPositiveHighPriorityQuestions = Convert.ToInt16(Dr["AuthenticationSecondaryNoOfPositiveHighPriorityQuestions"]);
                objSubProfile.AuthenticationSecondaryNoOfPositiveMediumPriorityQuestions = Convert.ToInt16(Dr["AuthenticationSecondaryNoOfPositiveMediumPriorityQuestions"]);
                objSubProfile.AuthenticationSecondaryNoOfPositiveLowPriorityQuestions = Convert.ToInt16(Dr["AuthenticationSecondaryNoOfPositiveLowPriorityQuestions"]);
                objSubProfile.AuthenticationSecondaryNoOfNegativeHighPriorityQuestions = Convert.ToInt16(Dr["AuthenticationSecondaryNoOfNegativeHighPriorityQuestions"]);
                objSubProfile.AuthenticationSecondaryNoOfNegativeMediumPriorityQuestions = Convert.ToInt16(Dr["AuthenticationSecondaryNoOfNegativeMediumPriorityQuestions"]);
                objSubProfile.AuthenticationSecondaryNoOfNegativeLowPriorityQuestions = Convert.ToInt16(Dr["AuthenticationSecondaryNoOfNegativeLowPriorityQuestions"]);
                objSubProfile.AuthenticationSecondaryNoOfYesNoQuestions = Convert.ToInt16(Dr["AuthenticationSecondaryNoOfYesNoQuestions"]);
                objSubProfile.AuthenticationSecondaryNoOfAnswersPerQuestion = Convert.ToInt16(Dr["AuthenticationSecondaryNoOfAnswersPerQuestion"]);
                objSubProfile.AuthenticationSecondaryRequiredAuthenticatedPerc = Convert.ToInt16(Dr["AuthenticationSecondaryRequiredAuthenticatedPerc"]);
                objSubProfile.AuthenticationSecondaryEnsureRequiredNoOfQuestionsYN = Convert.ToBoolean(Dr["AuthenticationSecondaryEnsureRequiredNoOfQuestionsYN"]);
                objSubProfile.CreatedByUser = Dr["CreatedByUser"].ToString();
                objSubProfile.CreatedOnDate = Convert.ToDateTime(Dr["CreatedOnDate"]);
                objSubProfile.ChangedByUser = Dr["ChangedByUser"].ToString();
                objSubProfile.ChangedOnDate = Convert.ToDateTime(Dr["ChangedOnDate"]);
            }

            return objSubProfile;
        }

        public List<XDSPortalLibrary.Entity_Layer.SubscriberProfile> GetProductReportsRecord(string conString, string whereClause)
        {
            List<XDSPortalLibrary.Entity_Layer.SubscriberProfile> objSubProfileList = new List<XDSPortalLibrary.Entity_Layer.SubscriberProfile>();
            XDSPortalLibrary.Entity_Layer.SubscriberProfile objSubProfile = new XDSPortalLibrary.Entity_Layer.SubscriberProfile();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = "select * from subscriberProfile " + whereClause;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objSubProfile.SubscriberID = Convert.ToInt16(Dr["SubscriberID"]);
                objSubProfile.AuthenticationSearchOnIDNoYN = Convert.ToBoolean(Dr["AuthenticationSearchOnIDNoYN"]);
                objSubProfile.AuthenticationSearchOnCellPhoneNoYN = Convert.ToBoolean(Dr["AuthenticationSearchOnCellPhoneNoYN"]);
                objSubProfile.AuthenticationSearchOnAccountNoYN = Convert.ToBoolean(Dr["AuthenticationSearchOnAccountNoYN"]);
                objSubProfile.AuthenticationDefaultIDVerificationSelectionInd = Dr["AuthenticationDefaultIDVerificationSelectionInd"].ToString();
                objSubProfile.AuthenticationPrimaryNoOfPositiveHighPriorityQuestions = Convert.ToInt16(Dr["AuthenticationPrimaryNoOfPositiveHighPriorityQuestions"]);
                objSubProfile.AuthenticationPrimaryNoOfPositiveMediumPriorityQuestions = Convert.ToInt16(Dr["AuthenticationPrimaryNoOfPositiveMediumPriorityQuestions"]);
                objSubProfile.AuthenticationPrimaryNoOfPositiveLowPriorityQuestions = Convert.ToInt16(Dr["AuthenticationPrimaryNoOfPositiveLowPriorityQuestions"]);
                objSubProfile.AuthenticationPrimaryNoOfNegativeHighPriorityQuestions = Convert.ToInt16(Dr["AuthenticationPrimaryNoOfNegativeHighPriorityQuestions"]);
                objSubProfile.AuthenticationPrimaryNoOfNegativeMediumPriorityQuestions = Convert.ToInt16(Dr["AuthenticationPrimaryNoOfNegativeMediumPriorityQuestions"]);
                objSubProfile.AuthenticationPrimaryNoOfNegativeLowPriorityQuestions = Convert.ToInt16(Dr["AuthenticationPrimaryNoOfNegativeLowPriorityQuestions"]);
                objSubProfile.AuthenticationPrimaryNoOfYesNoQuestions = Convert.ToInt16(Dr["AuthenticationPrimaryNoOfYesNoQuestions"]);
                objSubProfile.AuthenticationPrimaryNoOfAnswersPerQuestion = Convert.ToInt16(Dr["AuthenticationPrimaryNoOfAnswersPerQuestion"]);
                objSubProfile.AuthenticationPrimaryRequiredAuthenticatedPerc = Convert.ToInt16(Dr["AuthenticationPrimaryRequiredAuthenticatedPerc"]);
                objSubProfile.AuthenticationPrimaryEnsureRequiredNoOfQuestionsYN = Convert.ToBoolean(Dr["AuthenticationPrimaryEnsureRequiredNoOfQuestionsYN"]);
                objSubProfile.AuthenticationSecondaryNoOfPositiveHighPriorityQuestions = Convert.ToInt16(Dr["AuthenticationSecondaryNoOfPositiveHighPriorityQuestions"]);
                objSubProfile.AuthenticationSecondaryNoOfPositiveMediumPriorityQuestions = Convert.ToInt16(Dr["AuthenticationSecondaryNoOfPositiveMediumPriorityQuestions"]);
                objSubProfile.AuthenticationSecondaryNoOfPositiveLowPriorityQuestions = Convert.ToInt16(Dr["AuthenticationSecondaryNoOfPositiveLowPriorityQuestions"]);
                objSubProfile.AuthenticationSecondaryNoOfNegativeHighPriorityQuestions = Convert.ToInt16(Dr["AuthenticationSecondaryNoOfNegativeHighPriorityQuestions"]);
                objSubProfile.AuthenticationSecondaryNoOfNegativeMediumPriorityQuestions = Convert.ToInt16(Dr["AuthenticationSecondaryNoOfNegativeMediumPriorityQuestions"]);
                objSubProfile.AuthenticationSecondaryNoOfNegativeLowPriorityQuestions = Convert.ToInt16(Dr["AuthenticationSecondaryNoOfNegativeLowPriorityQuestions"]);
                objSubProfile.AuthenticationSecondaryNoOfYesNoQuestions = Convert.ToInt16(Dr["AuthenticationSecondaryNoOfYesNoQuestions"]);
                objSubProfile.AuthenticationSecondaryNoOfAnswersPerQuestion = Convert.ToInt16(Dr["AuthenticationSecondaryNoOfAnswersPerQuestion"]);
                objSubProfile.AuthenticationSecondaryRequiredAuthenticatedPerc = Convert.ToInt16(Dr["AuthenticationSecondaryRequiredAuthenticatedPerc"]);
                objSubProfile.AuthenticationSecondaryEnsureRequiredNoOfQuestionsYN = Convert.ToBoolean(Dr["AuthenticationSecondaryEnsureRequiredNoOfQuestionsYN"]);
                objSubProfile.CreatedByUser = Dr["CreatedByUser"].ToString();
                objSubProfile.CreatedOnDate = Convert.ToDateTime(Dr["CreatedOnDate"]);
                objSubProfile.ChangedByUser = Dr["ChangedByUser"].ToString();
                objSubProfile.ChangedOnDate = Convert.ToDateTime(Dr["ChangedOnDate"]);
                objSubProfileList.Add(objSubProfile);
            }

            return objSubProfileList;
        }

        public XDSPortalLibrary.Entity_Layer.SubscriberProfile GetSubscriberProfile(SqlConnection conString, int SubscriberID)
        {
            //  List<XDSPortalLibrary.Entity_Layer.SubscriberProfile> objSubProfileList = new List<XDSPortalLibrary.Entity_Layer.SubscriberProfile>();
            XDSPortalLibrary.Entity_Layer.SubscriberProfile objSubProfile = new XDSPortalLibrary.Entity_Layer.SubscriberProfile();

            //  SqlConnection ObjConstring = new SqlConnection(conString);
            conString.Open();

            string sqlSelect = "select * from subscriberProfile where SubscriberID=" + SubscriberID;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, conString);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objSubProfile.SubscriberID = Convert.ToInt32(Dr["SubscriberID"]);
                objSubProfile.AuthenticationSearchOnIDNoYN = Convert.ToBoolean(Dr["AuthenticationSearchOnIDNoYN"]);
                objSubProfile.AuthenticationSearchOnCellPhoneNoYN = Convert.ToBoolean(Dr["AuthenticationSearchOnCellPhoneNoYN"]);
                objSubProfile.AuthenticationSearchOnAccountNoYN = Convert.ToBoolean(Dr["AuthenticationSearchOnAccountNoYN"]);
                objSubProfile.AuthenticationDefaultIDVerificationSelectionInd = Dr["AuthenticationDefaultIDVerificationSelectionInd"].ToString();
                objSubProfile.AuthenticationPrimaryNoOfPositiveHighPriorityQuestions = Convert.ToInt16(Dr["AuthenticationPrimaryNoOfPositiveHighPriorityQuestions"]);
                objSubProfile.AuthenticationPrimaryNoOfPositiveMediumPriorityQuestions = Convert.ToInt16(Dr["AuthenticationPrimaryNoOfPositiveMediumPriorityQuestions"]);
                objSubProfile.AuthenticationPrimaryNoOfPositiveLowPriorityQuestions = Convert.ToInt16(Dr["AuthenticationPrimaryNoOfPositiveLowPriorityQuestions"]);
                objSubProfile.AuthenticationPrimaryNoOfNegativeHighPriorityQuestions = Convert.ToInt16(Dr["AuthenticationPrimaryNoOfNegativeHighPriorityQuestions"]);
                objSubProfile.AuthenticationPrimaryNoOfNegativeMediumPriorityQuestions = Convert.ToInt16(Dr["AuthenticationPrimaryNoOfNegativeMediumPriorityQuestions"]);
                objSubProfile.AuthenticationPrimaryNoOfNegativeLowPriorityQuestions = Convert.ToInt16(Dr["AuthenticationPrimaryNoOfNegativeLowPriorityQuestions"]);
                objSubProfile.AuthenticationPrimaryNoOfYesNoQuestions = Convert.ToInt16(Dr["AuthenticationPrimaryNoOfYesNoQuestions"]);
                objSubProfile.AuthenticationPrimaryNoOfAnswersPerQuestion = Convert.ToInt16(Dr["AuthenticationPrimaryNoOfAnswersPerQuestion"]);
                objSubProfile.AuthenticationPrimaryRequiredAuthenticatedPerc = Convert.ToInt16(Dr["AuthenticationPrimaryRequiredAuthenticatedPerc"]);
                objSubProfile.AuthenticationPrimaryEnsureRequiredNoOfQuestionsYN = Convert.ToBoolean(Dr["AuthenticationPrimaryEnsureRequiredNoOfQuestionsYN"]);
                objSubProfile.AuthenticationSecondaryNoOfPositiveHighPriorityQuestions = Convert.ToInt16(Dr["AuthenticationSecondaryNoOfPositiveHighPriorityQuestions"]);
                objSubProfile.AuthenticationSecondaryNoOfPositiveMediumPriorityQuestions = Convert.ToInt16(Dr["AuthenticationSecondaryNoOfPositiveMediumPriorityQuestions"]);
                objSubProfile.AuthenticationSecondaryNoOfPositiveLowPriorityQuestions = Convert.ToInt16(Dr["AuthenticationSecondaryNoOfPositiveLowPriorityQuestions"]);
                objSubProfile.AuthenticationSecondaryNoOfNegativeHighPriorityQuestions = Convert.ToInt16(Dr["AuthenticationSecondaryNoOfNegativeHighPriorityQuestions"]);
                objSubProfile.AuthenticationSecondaryNoOfNegativeMediumPriorityQuestions = Convert.ToInt16(Dr["AuthenticationSecondaryNoOfNegativeMediumPriorityQuestions"]);
                objSubProfile.AuthenticationSecondaryNoOfNegativeLowPriorityQuestions = Convert.ToInt16(Dr["AuthenticationSecondaryNoOfNegativeLowPriorityQuestions"]);
                objSubProfile.AuthenticationSecondaryNoOfYesNoQuestions = Convert.ToInt16(Dr["AuthenticationSecondaryNoOfYesNoQuestions"]);
                objSubProfile.AuthenticationSecondaryNoOfAnswersPerQuestion = Convert.ToInt16(Dr["AuthenticationSecondaryNoOfAnswersPerQuestion"]);
                objSubProfile.AuthenticationSecondaryRequiredAuthenticatedPerc = Convert.ToInt16(Dr["AuthenticationSecondaryRequiredAuthenticatedPerc"]);
                objSubProfile.AuthenticationSecondaryEnsureRequiredNoOfQuestionsYN = Convert.ToBoolean(Dr["AuthenticationSecondaryEnsureRequiredNoOfQuestionsYN"]);

                objSubProfile.CommercialScoreID = Convert.ToInt16(Dr["CommercialScoreID"]);
                //  objSubProfileList.Add(objSubProfile);
            }

            return objSubProfile;
        }



    }
}
