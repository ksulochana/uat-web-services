﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace XDSPortalLibrary.Data_Layer
{
    class SubscriberProductReports
    {
        public SubscriberProductReports()
        {
        }
        public XDSPortalLibrary.Entity_Layer.SubscriberProductReports GetProductReportsRecord(string conString, int SubProdReportID)
        {
            XDSPortalLibrary.Entity_Layer.SubscriberProductReports objSubproductreport = new XDSPortalLibrary.Entity_Layer.SubscriberProductReports();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = "select * from subscriberproductreports where SubscriberProductReportID = " + SubProdReportID;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objSubproductreport.SubscriberProductReportID = Convert.ToInt16(Dr["SubscriberProductReportID"]);
                objSubproductreport.SubscriberID = Convert.ToInt16(Dr["SubscriberID"]);
                objSubproductreport.ProductID = Convert.ToInt16(Dr["ProductID"]);
                objSubproductreport.ReportID = Convert.ToInt16(Dr["ReportID"]);
                objSubproductreport.OverrideReport = Convert.ToBoolean(Dr["OverrideReport"]);
                objSubproductreport.OverrideDataSegment = Convert.ToBoolean(Dr["OverrideDataSegment"]);
                objSubproductreport.Active = Convert.ToBoolean(Dr["Active"]);
                objSubproductreport.CreatedByUser = Dr["CreatedByUser"].ToString();
                objSubproductreport.CreatedOnDate = Convert.ToDateTime(Dr["CreatedOnDate"]);
                objSubproductreport.ChangedByUser = Dr["ChangedByUser"].ToString();
                objSubproductreport.ChangedOnDate = Convert.ToDateTime(Dr["ChangedOnDate"]);
            }

            return objSubproductreport;
        }
        public List<XDSPortalLibrary.Entity_Layer.SubscriberProductReports> GetProductReportsRecord(string conString, string whereClause)
        {
            List<XDSPortalLibrary.Entity_Layer.SubscriberProductReports> objSubproductreportList = new List<XDSPortalLibrary.Entity_Layer.SubscriberProductReports>();
            XDSPortalLibrary.Entity_Layer.SubscriberProductReports objSubproductreport = new XDSPortalLibrary.Entity_Layer.SubscriberProductReports();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = "select * from subscriberproductreports  " + whereClause;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objSubproductreport.SubscriberProductReportID = Convert.ToInt16(Dr["SubscriberProductReportID"]);
                objSubproductreport.SubscriberID = Convert.ToInt16(Dr["SubscriberID"]);
                objSubproductreport.ProductID = Convert.ToInt16(Dr["ProductID"]);
                objSubproductreport.ReportID = Convert.ToInt16(Dr["ReportID"]);
                objSubproductreport.OverrideReport = Convert.ToBoolean(Dr["OverrideReport"]);
                objSubproductreport.OverrideDataSegment = Convert.ToBoolean(Dr["OverrideDataSegment"]);
                objSubproductreport.Active = Convert.ToBoolean(Dr["Active"]);
                objSubproductreport.CreatedByUser = Dr["CreatedByUser"].ToString();
                objSubproductreport.CreatedOnDate = Convert.ToDateTime(Dr["CreatedOnDate"]);
                objSubproductreport.ChangedByUser = Dr["ChangedByUser"].ToString();
                objSubproductreport.ChangedOnDate = Convert.ToDateTime(Dr["ChangedOnDate"]);
                objSubproductreportList.Add(objSubproductreport);
            }

            return objSubproductreportList;
        }
    }
}
