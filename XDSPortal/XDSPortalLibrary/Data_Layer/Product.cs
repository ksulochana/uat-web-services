﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace XDSPortalLibrary.Data_Layer
{
    class Product
    {
        public Product()
        {
        }
        public XDSPortalLibrary.Entity_Layer.Product GetProductRecord(string conString, int productID)
        {
            XDSPortalLibrary.Entity_Layer.Product objProduct = new XDSPortalLibrary.Entity_Layer.Product();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = "select * from product where productID = " + productID;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objProduct.ProductID = Convert.ToInt32(Dr["ProductID"]);
                objProduct.ProductDesc = Dr["ProductDesc"].ToString();
                objProduct.DefaultPointValue = Convert.ToDecimal(Dr["DefaultPointValue"]);
                objProduct.ProductDetails = Dr["ProductDetails"].ToString();
                objProduct.EnableReportingServicesReportYN = Convert.ToBoolean(Dr["EnableReportingServicesReportYN"]);
                objProduct.ReportingServicesReportDisplayInd = Dr["ReportingServicesReportDisplayInd"].ToString();
                objProduct.ProductTypeID = Convert.ToInt32(Dr["ProductTypeID"]);
                objProduct.ReportingServicesReportID = Convert.ToInt32(Dr["ReportingServicesReportID"]);
                objProduct.CreatedByUser = Dr["CreatedByUser"].ToString();
                objProduct.CreatedOnDate = Convert.ToDateTime(Dr["CreatedOnDate"]);
                objProduct.ChangedByUser = Dr["ChangedByUser"].ToString();
                objProduct.ChangedOnDate = Convert.ToDateTime(Dr["ChangedOnDate"]);
            }

            return objProduct;
        }
        public List<XDSPortalLibrary.Entity_Layer.Product> GetProductRecord(string conString, string whereClause)
        {
            List<XDSPortalLibrary.Entity_Layer.Product> ObjProductList = new List<XDSPortalLibrary.Entity_Layer.Product>();
            XDSPortalLibrary.Entity_Layer.Product objProduct = new XDSPortalLibrary.Entity_Layer.Product();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = "select * from product" + whereClause;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objProduct.ProductID = Convert.ToInt32(Dr["ProductID"]);
                objProduct.ProductDesc = Dr["ProductDesc"].ToString();
                objProduct.DefaultPointValue = Convert.ToDecimal(Dr["DefaultPointValue"]);
                objProduct.ProductDetails = Dr["ProductDetails"].ToString();
                objProduct.EnableReportingServicesReportYN = Convert.ToBoolean(Dr["EnableReportingServicesReportYN"]);
                objProduct.ReportingServicesReportDisplayInd = Dr["ReportingServicesReportDisplayInd"].ToString();
                objProduct.ProductTypeID = Convert.ToInt32(Dr["ProductTypeID"]);
                objProduct.ReportingServicesReportID = Convert.ToInt32(Dr["ReportingServicesReportID"]);
                objProduct.CreatedByUser = Dr["CreatedByUser"].ToString();
                objProduct.CreatedOnDate = Convert.ToDateTime(Dr["CreatedOnDate"]);
                objProduct.ChangedByUser = Dr["ChangedByUser"].ToString();
                objProduct.ChangedOnDate = Convert.ToDateTime(Dr["ChangedOnDate"]);
                ObjProductList.Add(objProduct);
            }

            return ObjProductList;
        }
    }
}
