﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace XDSPortalLibrary.Data_Layer
{
    public class DataSegment
    {
        public DataSegment()
        {

        }

        public XDSPortalLibrary.Entity_Layer.DataSegment GetDataSegmentrecord(string conString, int DataSegmentID)
        {
            XDSPortalLibrary.Entity_Layer.DataSegment objDataSegment = new XDSPortalLibrary.Entity_Layer.DataSegment();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = "select * from DataSegment where datasegmentid = " + DataSegmentID;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                objDataSegment.DataSegmentID = Convert.ToInt32(r["DataSegmentID"]);
                objDataSegment.DataSegmentDesc = r["DataSegmentDesc"].ToString();
                objDataSegment.CreatedOnDate =Convert.ToDateTime(r["CreatedOnDate"]);
                objDataSegment.CreatedByUser = r["CreatedByUser"].ToString();
                objDataSegment.ChangedOnDate = Convert.ToDateTime(r["ChangedOnDate"]);
                objDataSegment.ChangedByUser = r["ChangedByUser"].ToString();
                objDataSegment.Active = Convert.ToBoolean(r["Active"]);
            }

            return objDataSegment;
            
        }


        public List<XDSPortalLibrary.Entity_Layer.DataSegment> GetDataSegmentrecord(string constring, string whereClause)
        {
            List<XDSPortalLibrary.Entity_Layer.DataSegment> objDataSegmentList = new List<XDSPortalLibrary.Entity_Layer.DataSegment>();
            XDSPortalLibrary.Entity_Layer.DataSegment objDataSegment = new XDSPortalLibrary.Entity_Layer.DataSegment();

            SqlConnection ObjConstring = new SqlConnection(constring);
            ObjConstring.Open();

            string sqlSelect = "select * from DataSegment " + whereClause;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter Objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            Objsqladp.Fill(ds);

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                objDataSegment.DataSegmentID = Convert.ToInt32(r["DataSegmentID"]);
                objDataSegment.DataSegmentDesc = r["DataSegmentDesc"].ToString();
                objDataSegment.CreatedOnDateSpecified = Convert.ToBoolean(r["CreatedOnDateSpecified"]);
                objDataSegment.CreatedOnDate = Convert.ToDateTime(r["CreatedOnDate"]);
                objDataSegment.CreatedByUser = r["CreatedByUser"].ToString();
                objDataSegment.ChangedOnDateSpecified = Convert.ToBoolean(r["ChangedOnDateSpecified"]);
                objDataSegment.ChangedOnDate = Convert.ToDateTime(r["ChangedOnDate"]);
                objDataSegment.ChangedByUser = r["ChangedByUser"].ToString();
                objDataSegment.Active = Convert.ToBoolean(r["Active"]);

                objDataSegmentList.Add(objDataSegment);
            }

            return objDataSegmentList;

        }

    }
}
