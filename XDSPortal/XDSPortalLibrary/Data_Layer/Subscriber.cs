﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace XDSPortalLibrary.Data_Layer
{
    class Subscriber
    {
        public Subscriber()
        {
        }
        public XDSPortalLibrary.Entity_Layer.Subscriber GetProductReportsRecord(string conString, int SubscriberID)
        {
            XDSPortalLibrary.Entity_Layer.Subscriber objSubscriber = new XDSPortalLibrary.Entity_Layer.Subscriber();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = "select * from Subscriber where SubscriberID = " + SubscriberID;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objSubscriber.SubscriberID = Convert.ToInt32(Dr["SubscriberID"]);
                objSubscriber.SubscriberName = Dr["SubscriberName"].ToString();
                objSubscriber.AuthenticationSubscriberName = Dr["AuthenticationSubscriberName"].ToString();
                objSubscriber.SubscriberTypeInd = Dr["SubscriberTypeInd"].ToString();
                objSubscriber.StatusInd = Dr["StatusInd"].ToString();
                objSubscriber.CompanyRegistrationNo = Dr["CompanyRegistrationNo"].ToString();
                objSubscriber.CompanyVATNo = Dr["CompanyVATNo"].ToString();
                objSubscriber.CompanyDetails = Dr["CompanyDetails"].ToString();
                objSubscriber.CompanyTradingName1 = Dr["CompanyTradingName1"].ToString();
                objSubscriber.CompanyTradingName2 = Dr["CompanyTradingName2"].ToString();
                objSubscriber.CompanyTelephoneCode = Dr["CompanyTelephoneCode"].ToString();
                objSubscriber.CompanyTelephoneNo = Dr["CompanyTelephoneNo"].ToString();
                objSubscriber.CompanyFaxCode = Dr["CompanyFaxCode"].ToString();
                objSubscriber.CompanyFaxNo = Dr["CompanyFaxNo"].ToString();
                objSubscriber.CompanyWebsiteUrl = Dr["CompanyWebsiteUrl"].ToString();
                objSubscriber.CompanyPhysicalAddress1 = Dr["CompanyPhysicalAddress1"].ToString();
                objSubscriber.CompanyPhysicalAddress2 = Dr["CompanyPhysicalAddress2"].ToString();
                objSubscriber.CompanyPhysicalAddress3 = Dr["CompanyPhysicalAddress3"].ToString();
                objSubscriber.CompanyPhysicalPostalCode = Dr["CompanyPhysicalPostalCode"].ToString();
                objSubscriber.CompanyPostalAddress1 = Dr["CompanyPostalAddress1"].ToString();
                objSubscriber.CompanyPostalAddress2 = Dr["CompanyPostalAddress2"].ToString();
                objSubscriber.CompanyPostalAddress3 = Dr["CompanyPostalAddress3"].ToString();
                objSubscriber.CompanyPostalPostalCode = Dr["CompanyPostalPostalCode"].ToString();
                objSubscriber.LevelNo = Convert.ToInt16(Dr["CompanyPostalPostalCode"]);
                objSubscriber.Level1Desc = Dr["Level1Desc"].ToString();
                objSubscriber.Level2Desc = Dr["Level2Desc"].ToString();
                objSubscriber.Level3Desc = Dr["Level3Desc"].ToString();
                objSubscriber.Level4Desc = Dr["Level4Desc"].ToString();
                objSubscriber.Level5Desc = Dr["Level5Desc"].ToString();
                objSubscriber.SubscriberRegistrationID = Convert.ToInt16(Dr["SubscriberRegistrationID"]);
                objSubscriber.Level1SubscriberID = Convert.ToInt16(Dr["Level1SubscriberID"]);
                objSubscriber.Level2SubscriberID = Convert.ToInt16(Dr["Level2SubscriberID"]);
                objSubscriber.Level3SubscriberID = Convert.ToInt16(Dr["Level3SubscriberID"]);
                objSubscriber.Level4SubscriberID = Convert.ToInt16(Dr["Level4SubscriberID"]);
                objSubscriber.Level5SubscriberID = Convert.ToInt16(Dr["Level5SubscriberID"]);
                objSubscriber.SubscriberBillingGroupCode = Dr["SubscriberBillingGroupCode"].ToString();
                objSubscriber.SubscriberBusinessTypeCode = Dr["SubscriberBusinessTypeCode"].ToString();
                objSubscriber.SubscriberAssociationCode = Dr["SubscriberAssociationCode"].ToString();
                objSubscriber.SubscriberGroupCode = Dr["SubscriberGroupCode"].ToString();
                objSubscriber.Level5SubscriberID = Convert.ToInt16(Dr["Level5SubscriberID"]);
                objSubscriber.Level5SubscriberID = Convert.ToInt16(Dr["Level5SubscriberID"]);
                objSubscriber.CreatedByUser = Dr["CreatedByUser"].ToString();
                objSubscriber.CreatedOnDate = Convert.ToDateTime(Dr["CreatedOnDate"]);
                objSubscriber.ChangedByUser = Dr["ChangedByUser"].ToString();
                objSubscriber.ChangedOnDate = Convert.ToDateTime(Dr["ChangedOnDate"]);
                objSubscriber.OLDSUBSID = Convert.ToInt16(Dr["OLDSUBSID"]);
            }

            return objSubscriber;
        }

        public List<XDSPortalLibrary.Entity_Layer.Subscriber> GetProductReportsRecord(string conString, string whereclause)
        {
            List<XDSPortalLibrary.Entity_Layer.Subscriber> ObjSubscriberList = new List<XDSPortalLibrary.Entity_Layer.Subscriber>();
            XDSPortalLibrary.Entity_Layer.Subscriber objSubscriber = new XDSPortalLibrary.Entity_Layer.Subscriber();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = "select * from Subscriber " + whereclause;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objSubscriber.SubscriberID = Convert.ToInt32(Dr["SubscriberID"]);
                objSubscriber.SubscriberName = Dr["SubscriberName"].ToString();
                objSubscriber.AuthenticationSubscriberName = Dr["AuthenticationSubscriberName"].ToString();
                objSubscriber.SubscriberTypeInd = Dr["SubscriberTypeInd"].ToString();
                objSubscriber.StatusInd = Dr["StatusInd"].ToString();
                objSubscriber.CompanyRegistrationNo = Dr["CompanyRegistrationNo"].ToString();
                objSubscriber.CompanyVATNo = Dr["CompanyVATNo"].ToString();
                objSubscriber.CompanyDetails = Dr["CompanyDetails"].ToString();
                objSubscriber.CompanyTradingName1 = Dr["CompanyTradingName1"].ToString();
                objSubscriber.CompanyTradingName2 = Dr["CompanyTradingName2"].ToString();
                objSubscriber.CompanyTelephoneCode = Dr["CompanyTelephoneCode"].ToString();
                objSubscriber.CompanyTelephoneNo = Dr["CompanyTelephoneNo"].ToString();
                objSubscriber.CompanyFaxCode = Dr["CompanyFaxCode"].ToString();
                objSubscriber.CompanyFaxNo = Dr["CompanyFaxNo"].ToString();
                objSubscriber.CompanyWebsiteUrl = Dr["CompanyWebsiteUrl"].ToString();
                objSubscriber.CompanyPhysicalAddress1 = Dr["CompanyPhysicalAddress1"].ToString();
                objSubscriber.CompanyPhysicalAddress2 = Dr["CompanyPhysicalAddress2"].ToString();
                objSubscriber.CompanyPhysicalAddress3 = Dr["CompanyPhysicalAddress3"].ToString();
                objSubscriber.CompanyPhysicalPostalCode = Dr["CompanyPhysicalPostalCode"].ToString();
                objSubscriber.CompanyPostalAddress1 = Dr["CompanyPostalAddress1"].ToString();
                objSubscriber.CompanyPostalAddress2 = Dr["CompanyPostalAddress2"].ToString();
                objSubscriber.CompanyPostalAddress3 = Dr["CompanyPostalAddress3"].ToString();
                objSubscriber.CompanyPostalPostalCode = Dr["CompanyPostalPostalCode"].ToString();
                objSubscriber.LevelNo = Convert.ToInt16(Dr["CompanyPostalPostalCode"]);
                objSubscriber.Level1Desc = Dr["Level1Desc"].ToString();
                objSubscriber.Level2Desc = Dr["Level2Desc"].ToString();
                objSubscriber.Level3Desc = Dr["Level3Desc"].ToString();
                objSubscriber.Level4Desc = Dr["Level4Desc"].ToString();
                objSubscriber.Level5Desc = Dr["Level5Desc"].ToString();
                objSubscriber.SubscriberRegistrationID = Convert.ToInt16(Dr["SubscriberRegistrationID"]);
                objSubscriber.Level1SubscriberID = Convert.ToInt16(Dr["Level1SubscriberID"]);
                objSubscriber.Level2SubscriberID = Convert.ToInt16(Dr["Level2SubscriberID"]);
                objSubscriber.Level3SubscriberID = Convert.ToInt16(Dr["Level3SubscriberID"]);
                objSubscriber.Level4SubscriberID = Convert.ToInt16(Dr["Level4SubscriberID"]);
                objSubscriber.Level5SubscriberID = Convert.ToInt16(Dr["Level5SubscriberID"]);
                objSubscriber.SubscriberBillingGroupCode = Dr["SubscriberBillingGroupCode"].ToString();
                objSubscriber.SubscriberBusinessTypeCode = Dr["SubscriberBusinessTypeCode"].ToString();
                objSubscriber.SubscriberAssociationCode = Dr["SubscriberAssociationCode"].ToString();
                objSubscriber.SubscriberGroupCode = Dr["SubscriberGroupCode"].ToString();
                objSubscriber.Level5SubscriberID = Convert.ToInt16(Dr["Level5SubscriberID"]);
                objSubscriber.Level5SubscriberID = Convert.ToInt16(Dr["Level5SubscriberID"]);
                objSubscriber.CreatedByUser = Dr["CreatedByUser"].ToString();
                objSubscriber.CreatedOnDate = Convert.ToDateTime(Dr["CreatedOnDate"]);
                objSubscriber.ChangedByUser = Dr["ChangedByUser"].ToString();
                objSubscriber.ChangedOnDate = Convert.ToDateTime(Dr["ChangedOnDate"]);
                objSubscriber.OLDSUBSID = Convert.ToInt16(Dr["OLDSUBSID"]);
                ObjSubscriberList.Add(objSubscriber);
            }

            return ObjSubscriberList;
        }
    }
}

