﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace XDSPortalLibrary.Data_Layer
{
    class ReportsDataSegment
    {
        public ReportsDataSegment()
        {
        }
        public XDSPortalLibrary.Entity_Layer.ReportsDataSegment GetProductReportsRecord(string conString, int ReportDataSegmentID)
        {
            XDSPortalLibrary.Entity_Layer.ReportsDataSegment objReportDataSegment = new XDSPortalLibrary.Entity_Layer.ReportsDataSegment();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = "select * from ReportsDataSegment where ReportDataSegmentID = " + ReportDataSegmentID;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objReportDataSegment.ReportDataSegmentID = Convert.ToInt32(Dr["ReportDataSegmentID"]);
                objReportDataSegment.ReportID = Convert.ToInt32(Dr["ReportID"]);
                objReportDataSegment.DataSegmentID = Convert.ToInt32(Dr["DataSegmentID"]);
                objReportDataSegment.Active = Convert.ToBoolean(Dr["Active"]);
                objReportDataSegment.CreatedByUser = Dr["CreatedByUser"].ToString();
                objReportDataSegment.CreatedOnDate = Convert.ToDateTime(Dr["CreatedOnDate"]);
                objReportDataSegment.ChangedByUser = Dr["ChangedByUser"].ToString();
                objReportDataSegment.ChangedOnDate = Convert.ToDateTime(Dr["ChangedOnDate"]);
            }

            return objReportDataSegment;
        }
        public List<XDSPortalLibrary.Entity_Layer.ReportsDataSegment> GetProductReportsRecord(string conString, string whereClause)
        {
            List<XDSPortalLibrary.Entity_Layer.ReportsDataSegment> ObjReportDataSegmentList = new List<XDSPortalLibrary.Entity_Layer.ReportsDataSegment>();
            XDSPortalLibrary.Entity_Layer.ReportsDataSegment objReportDataSegment = new XDSPortalLibrary.Entity_Layer.ReportsDataSegment();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = "select * from ReportsDataSegment  " + whereClause;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objReportDataSegment.ReportDataSegmentID = Convert.ToInt32(Dr["ReportDataSegmentID"]);
                objReportDataSegment.ReportID = Convert.ToInt32(Dr["ReportID"]);
                objReportDataSegment.DataSegmentID = Convert.ToInt32(Dr["DataSegmentID"]);
                objReportDataSegment.Active = Convert.ToBoolean(Dr["Active"]);
                objReportDataSegment.CreatedByUser = Dr["CreatedByUser"].ToString();
                objReportDataSegment.CreatedOnDate = Convert.ToDateTime(Dr["CreatedOnDate"]);
                objReportDataSegment.ChangedByUser = Dr["ChangedByUser"].ToString();
                objReportDataSegment.ChangedOnDate = Convert.ToDateTime(Dr["ChangedOnDate"]);
                ObjReportDataSegmentList.Add(objReportDataSegment);
            }

            return ObjReportDataSegmentList;
        }
    }
}
