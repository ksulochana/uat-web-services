﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace XDSPortalLibrary.Data_Layer
{
    class SubscriberAssociation
    {
        public SubscriberAssociation()
        {
        }
        public XDSPortalLibrary.Entity_Layer.SubscriberAssociation GetProductReportsRecord(string conString, int SubscriberID)
        {
            XDSPortalLibrary.Entity_Layer.SubscriberAssociation objSubscriberAss = new XDSPortalLibrary.Entity_Layer.SubscriberAssociation();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = "select * from SubscriberAssociation where SubscriberAssociationCode = " + SubscriberID;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objSubscriberAss.SubscriberAssociationCode = Dr["SubscriberAssociationCode"].ToString();
                objSubscriberAss.SubscriberAssociationDesc = Dr["SubscriberName"].ToString();
            }

            return objSubscriberAss;
        }
        public List<XDSPortalLibrary.Entity_Layer.SubscriberAssociation> GetProductReportsRecord(string conString, string whereClause)
        {
            List<XDSPortalLibrary.Entity_Layer.SubscriberAssociation> objSubscriberAssList = new List<XDSPortalLibrary.Entity_Layer.SubscriberAssociation>();
            XDSPortalLibrary.Entity_Layer.SubscriberAssociation objSubscriberAss = new XDSPortalLibrary.Entity_Layer.SubscriberAssociation();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = "select * from SubscriberAssociation  " + whereClause;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objSubscriberAss.SubscriberAssociationCode = Dr["SubscriberAssociationCode"].ToString();
                objSubscriberAss.SubscriberAssociationDesc = Dr["SubscriberName"].ToString();
                objSubscriberAssList.Add(objSubscriberAss);
            }

            return objSubscriberAssList;
        }
    }
}
