﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace XDSPortalLibrary.Data_Layer
{
    class SubscriberAssociationProductReportsDataSegment
    {
        public SubscriberAssociationProductReportsDataSegment()
        {
        }
        public XDSPortalLibrary.Entity_Layer.SubscriberAssociationProductReportsDataSegment GetProductReportsRecord(string conString, int SubAssProdReportDSID)
        {
            XDSPortalLibrary.Entity_Layer.SubscriberAssociationProductReportsDataSegment objSubAssproductreportDS = new XDSPortalLibrary.Entity_Layer.SubscriberAssociationProductReportsDataSegment();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = "select * from subscriberAssociationproductreportsdatasegment where SubscriberAssociationProductReportDataSegmentID = " + SubAssProdReportDSID;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objSubAssproductreportDS.SubscriberAssociationProductReportDataSegmentID = Convert.ToInt16(Dr["SubscriberAssociationProductReportDataSegmentID"]);
                objSubAssproductreportDS.SubscriberAssociationProductReportID = Convert.ToInt16(Dr["SubscriberAssociationProductReportID"]);
                objSubAssproductreportDS.DataSegmentID = Convert.ToInt16(Dr["DataSegmentID"]);
                objSubAssproductreportDS.Active = Convert.ToBoolean(Dr["Active"]);
                objSubAssproductreportDS.CreatedByUser = Dr["CreatedByUser"].ToString();
                objSubAssproductreportDS.CreatedOnDate = Convert.ToDateTime(Dr["CreatedOnDate"]);
                objSubAssproductreportDS.ChangedByUser = Dr["ChangedByUser"].ToString();
                objSubAssproductreportDS.ChangedOnDate = Convert.ToDateTime(Dr["ChangedOnDate"]);
            }

            return objSubAssproductreportDS;
        }
        public List<XDSPortalLibrary.Entity_Layer.SubscriberAssociationProductReportsDataSegment> GetProductReportsRecord(string conString, string whereClause)
        {
            List<XDSPortalLibrary.Entity_Layer.SubscriberAssociationProductReportsDataSegment> objSubAssproductreportDSList = new List<XDSPortalLibrary.Entity_Layer.SubscriberAssociationProductReportsDataSegment>();
            XDSPortalLibrary.Entity_Layer.SubscriberAssociationProductReportsDataSegment objSubAssproductreportDS = new XDSPortalLibrary.Entity_Layer.SubscriberAssociationProductReportsDataSegment();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = "select * from subscriberAssociationproductreportsdatasegment  " + whereClause;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objSubAssproductreportDS.SubscriberAssociationProductReportDataSegmentID = Convert.ToInt16(Dr["SubscriberAssociationProductReportDataSegmentID"]);
                objSubAssproductreportDS.SubscriberAssociationProductReportID = Convert.ToInt16(Dr["SubscriberAssociationProductReportID"]);
                objSubAssproductreportDS.DataSegmentID = Convert.ToInt16(Dr["DataSegmentID"]);
                objSubAssproductreportDS.Active = Convert.ToBoolean(Dr["Active"]);
                objSubAssproductreportDS.CreatedByUser = Dr["CreatedByUser"].ToString();
                objSubAssproductreportDS.CreatedOnDate = Convert.ToDateTime(Dr["CreatedOnDate"]);
                objSubAssproductreportDS.ChangedByUser = Dr["ChangedByUser"].ToString();
                objSubAssproductreportDS.ChangedOnDate = Convert.ToDateTime(Dr["ChangedOnDate"]);
                objSubAssproductreportDSList.Add(objSubAssproductreportDS);
            }

            return objSubAssproductreportDSList;
        }
    }
}
