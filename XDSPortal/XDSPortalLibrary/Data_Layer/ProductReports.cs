﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace XDSPortalLibrary.Data_Layer
{
    class ProductReports
    {
        public ProductReports()
        {
        }
        public XDSPortalLibrary.Entity_Layer.ProductReports GetProductReportsRecord(string conString, int productReportID)
        {
            XDSPortalLibrary.Entity_Layer.ProductReports objProductReport = new XDSPortalLibrary.Entity_Layer.ProductReports();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = "select * from productReports where productReportID = " + productReportID;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objProductReport.ProductReportID = Convert.ToInt32(Dr["ProductReportID"]);
                objProductReport.ProductID = Convert.ToInt32(Dr["ProductID"]);
                objProductReport.ReportID = Convert.ToInt32(Dr["ReportID"]);
                objProductReport.DefaultReport = Convert.ToBoolean(Dr["DefaultReport"]);
                objProductReport.CreatedByUser = Dr["CreatedByUser"].ToString();
                objProductReport.CreatedOnDate = Convert.ToDateTime(Dr["CreatedOnDate"]);
                objProductReport.ChangedByUser = Dr["ChangedByUser"].ToString();
                objProductReport.ChangedOnDate = Convert.ToDateTime(Dr["ChangedOnDate"]);
            }

            return objProductReport;
        }
        public List<XDSPortalLibrary.Entity_Layer.ProductReports> GetProductReportsRecord(string conString, string whereClause)
        {
            List<XDSPortalLibrary.Entity_Layer.ProductReports> objProductReportsList = new List<XDSPortalLibrary.Entity_Layer.ProductReports>();
            XDSPortalLibrary.Entity_Layer.ProductReports objProductReport = new XDSPortalLibrary.Entity_Layer.ProductReports();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = "select * from productReports " + whereClause;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objProductReport.ProductReportID = Convert.ToInt32(Dr["ProductReportID"]);
                objProductReport.ProductID = Convert.ToInt32(Dr["ProductID"]);
                objProductReport.ReportID = Convert.ToInt32(Dr["ReportID"]);
                objProductReport.DefaultReport = Convert.ToBoolean(Dr["DefaultReport"]);
                objProductReport.CreatedByUser = Dr["CreatedByUser"].ToString();
                objProductReport.CreatedOnDate = Convert.ToDateTime(Dr["CreatedOnDate"]);
                objProductReport.ChangedByUser = Dr["ChangedByUser"].ToString();
                objProductReport.ChangedOnDate = Convert.ToDateTime(Dr["ChangedOnDate"]);
                objProductReportsList.Add(objProductReport);
            }

            return objProductReportsList;
        }
    }
}
