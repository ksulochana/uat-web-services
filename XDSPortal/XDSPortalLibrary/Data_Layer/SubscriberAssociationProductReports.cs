﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace XDSPortalLibrary.Data_Layer
{
    class SubscriberAssociationProductReports
    {
        public SubscriberAssociationProductReports()
        {
        }
        public XDSPortalLibrary.Entity_Layer.SubscriberAssociationProductReports GetProductReportsRecord(string conString, int SubscriberAssociationProductReportID)
        {
            XDSPortalLibrary.Entity_Layer.SubscriberAssociationProductReports objSubAssproductreport = new XDSPortalLibrary.Entity_Layer.SubscriberAssociationProductReports();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = "select * from subscriberAssociationproductreports where SubscriberAssociationProductReportID = " + SubscriberAssociationProductReportID;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objSubAssproductreport.SubscriberAssociationProductReportID = Convert.ToInt16(Dr["SubscriberAssociationProductReportID"]);
                objSubAssproductreport.SubscriberAssociationCode = Dr["SubscriberAssociationCode"].ToString();
                objSubAssproductreport.ProductID = Convert.ToInt16(Dr["ProductID"]);
                objSubAssproductreport.ReportID = Convert.ToInt16(Dr["ReportID"]);
                objSubAssproductreport.Active = Convert.ToBoolean(Dr["Active"]);
                objSubAssproductreport.CreatedByUser = Dr["CreatedByUser"].ToString();
                objSubAssproductreport.CreatedOnDate = Convert.ToDateTime(Dr["CreatedOnDate"]);
                objSubAssproductreport.ChangedByUser = Dr["ChangedByUser"].ToString();
                objSubAssproductreport.ChangedOnDate = Convert.ToDateTime(Dr["ChangedOnDate"]);
            }

            return objSubAssproductreport;
        }
        public List<XDSPortalLibrary.Entity_Layer.SubscriberAssociationProductReports> GetProductReportsRecord(string conString, string whereClause)
        {
            List<XDSPortalLibrary.Entity_Layer.SubscriberAssociationProductReports> objSubAssproductreportList = new List<XDSPortalLibrary.Entity_Layer.SubscriberAssociationProductReports>();
            XDSPortalLibrary.Entity_Layer.SubscriberAssociationProductReports objSubAssproductreport = new XDSPortalLibrary.Entity_Layer.SubscriberAssociationProductReports();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = "select * from subscriberAssociationproductreports  " + whereClause;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objSubAssproductreport.SubscriberAssociationProductReportID = Convert.ToInt16(Dr["SubscriberAssociationProductReportID"]);
                objSubAssproductreport.SubscriberAssociationCode = Dr["SubscriberAssociationCode"].ToString();
                objSubAssproductreport.ProductID = Convert.ToInt16(Dr["ProductID"]);
                objSubAssproductreport.ReportID = Convert.ToInt16(Dr["ReportID"]);
                objSubAssproductreport.Active = Convert.ToBoolean(Dr["Active"]);
                objSubAssproductreport.CreatedByUser = Dr["CreatedByUser"].ToString();
                objSubAssproductreport.CreatedOnDate = Convert.ToDateTime(Dr["CreatedOnDate"]);
                objSubAssproductreport.ChangedByUser = Dr["ChangedByUser"].ToString();
                objSubAssproductreport.ChangedOnDate = Convert.ToDateTime(Dr["ChangedOnDate"]);
                objSubAssproductreportList.Add(objSubAssproductreport);
            }

            return objSubAssproductreportList;
        }
    }
}
