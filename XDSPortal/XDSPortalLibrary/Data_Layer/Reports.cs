﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace XDSPortalLibrary.Data_Layer
{
    class Reports
    {
        public Reports()
        {
        }
        public XDSPortalLibrary.Entity_Layer.Reports GetProductReportsRecord(string conString, int ReportID)
        {
            XDSPortalLibrary.Entity_Layer.Reports objReport = new XDSPortalLibrary.Entity_Layer.Reports();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = "select * from Reports where ReportID = " + ReportID;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objReport.ReportID = Convert.ToInt32(Dr["ReportID"]);
                objReport.ReportName = Dr["ReportName"].ToString();
                objReport.ReportPath = Dr["ReportPath"].ToString();
                byte[] ObjReportPath=null;
                ObjReportPath.SetValue(Dr["ReportPath"],0);
                objReport.ReportFile = ObjReportPath;
                objReport.CreatedByUser = Dr["CreatedByUser"].ToString();
                objReport.CreatedOnDate = Convert.ToDateTime(Dr["CreatedOnDate"]);
                objReport.ChangedByUser = Dr["ChangedByUser"].ToString();
                objReport.ChangedOnDate = Convert.ToDateTime(Dr["ChangedOnDate"]);
            }

            return objReport;
        }
        public List<XDSPortalLibrary.Entity_Layer.Reports> GetProductReportsRecord(string conString, string whereclause)
        {
            List<XDSPortalLibrary.Entity_Layer.Reports> ObjReportList = new List<XDSPortalLibrary.Entity_Layer.Reports>();
            XDSPortalLibrary.Entity_Layer.Reports objReport = new XDSPortalLibrary.Entity_Layer.Reports();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = "select * from Reports " + whereclause;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objReport.ReportID = Convert.ToInt32(Dr["ReportID"]);
                objReport.ReportName = Dr["ReportName"].ToString();
                objReport.ReportPath = Dr["ReportPath"].ToString();
                byte[] ObjReportPath=null;
                ObjReportPath.SetValue(Dr["ReportPath"], 0);
                objReport.ReportFile = ObjReportPath;
                objReport.CreatedByUser = Dr["CreatedByUser"].ToString();
                objReport.CreatedOnDate = Convert.ToDateTime(Dr["CreatedOnDate"]);
                objReport.ChangedByUser = Dr["ChangedByUser"].ToString();
                objReport.ChangedOnDate = Convert.ToDateTime(Dr["ChangedOnDate"]);

                ObjReportList.Add(objReport);
            }

            return ObjReportList;
        }
    }
}
