﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace XDSPortalLibrary.Data_Layer
{
    class SubscriberProductReportsDataSegment
    {
        public SubscriberProductReportsDataSegment()
        {
        }
        public XDSPortalLibrary.Entity_Layer.SubscriberProductReportsDataSegment GetProductReportsRecord(string conString, int SubProdReportDSID)
        {
            XDSPortalLibrary.Entity_Layer.SubscriberProductReportsDataSegment objSubproductreportDS = new XDSPortalLibrary.Entity_Layer.SubscriberProductReportsDataSegment();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = "select * from subscriberproductreportsDataSegment where SubscriberProductReportDataSegmentID = " + SubProdReportDSID;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objSubproductreportDS.SubscriberProductReportDataSegmentID = Convert.ToInt16(Dr["SubscriberProductReportDataSegmentID"]);
                objSubproductreportDS.SubscriberProductReportID = Convert.ToInt16(Dr["SubscriberProductReportID"]);
                objSubproductreportDS.DataSegmentID = Convert.ToInt16(Dr["DataSegmentID"]);
                objSubproductreportDS.Active = Convert.ToBoolean(Dr["Active"]);
                objSubproductreportDS.CreatedByUser = Dr["CreatedByUser"].ToString();
                objSubproductreportDS.CreatedOnDate = Convert.ToDateTime(Dr["CreatedOnDate"]);
                objSubproductreportDS.ChangedByUser = Dr["ChangedByUser"].ToString();
                objSubproductreportDS.ChangedOnDate = Convert.ToDateTime(Dr["ChangedOnDate"]);
            }

            return objSubproductreportDS;
        }
        public List<XDSPortalLibrary.Entity_Layer.SubscriberProductReportsDataSegment> GetProductReportsRecord(string conString, string whereClause)
        {
            List<XDSPortalLibrary.Entity_Layer.SubscriberProductReportsDataSegment> objSubproductreportDSList = new List<XDSPortalLibrary.Entity_Layer.SubscriberProductReportsDataSegment>();
            XDSPortalLibrary.Entity_Layer.SubscriberProductReportsDataSegment objSubproductreportDS = new XDSPortalLibrary.Entity_Layer.SubscriberProductReportsDataSegment();

            SqlConnection ObjConstring = new SqlConnection(conString);
            ObjConstring.Open();

            string sqlSelect = "select * from subscriberproductreportsDataSegment  " + whereClause;

            SqlCommand Objsqlcom = new SqlCommand(sqlSelect, ObjConstring);
            SqlDataAdapter objsqladp = new SqlDataAdapter(Objsqlcom);
            DataSet ds = new DataSet();
            objsqladp.Fill(ds);

            foreach (DataRow Dr in ds.Tables[0].Rows)
            {
                objSubproductreportDS.SubscriberProductReportDataSegmentID = Convert.ToInt16(Dr["SubscriberProductReportDataSegmentID"]);
                objSubproductreportDS.SubscriberProductReportID = Convert.ToInt16(Dr["SubscriberProductReportID"]);
                objSubproductreportDS.DataSegmentID = Convert.ToInt16(Dr["DataSegmentID"]);
                objSubproductreportDS.Active = Convert.ToBoolean(Dr["Active"]);
                objSubproductreportDS.CreatedByUser = Dr["CreatedByUser"].ToString();
                objSubproductreportDS.CreatedOnDate = Convert.ToDateTime(Dr["CreatedOnDate"]);
                objSubproductreportDS.ChangedByUser = Dr["ChangedByUser"].ToString();
                objSubproductreportDS.ChangedOnDate = Convert.ToDateTime(Dr["ChangedOnDate"]);
                objSubproductreportDSList.Add(objSubproductreportDS);
            }

            return objSubproductreportDSList;
        }
    }
}
