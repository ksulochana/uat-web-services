﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XDSPortalLibrary.Entity_Layer
{
    class Product
    {
        private int productIDField;

        private string productDescField;

        private decimal defaultPointValueField;

        private string productDetailsField;

        private bool enableReportingServicesReportYNField;

        private string reportingServicesReportDisplayIndField;

        private int productTypeIDField;

        private int reportingServicesReportIDField;

        private bool reportingServicesReportIDFieldSpecified;

        private string createdByUserField;

        private System.DateTime createdOnDateField;

        private bool createdOnDateFieldSpecified;

        private string changedByUserField;

        private System.DateTime changedOnDateField;

        public Product()
        {
        }
       
        private bool changedOnDateFieldSpecified;

        /// <remarks/>
        public int ProductID
        {
            get
            {
                return this.productIDField;
            }
            set
            {
                this.productIDField = value;
            }
        }

        /// <remarks/>
        public string ProductDesc
        {
            get
            {
                return this.productDescField;
            }
            set
            {
                this.productDescField = value;
            }
        }

        /// <remarks/>
        public decimal DefaultPointValue
        {
            get
            {
                return this.defaultPointValueField;
            }
            set
            {
                this.defaultPointValueField = value;
            }
        }

        /// <remarks/>
        public string ProductDetails
        {
            get
            {
                return this.productDetailsField;
            }
            set
            {
                this.productDetailsField = value;
            }
        }

        /// <remarks/>
        public bool EnableReportingServicesReportYN
        {
            get
            {
                return this.enableReportingServicesReportYNField;
            }
            set
            {
                this.enableReportingServicesReportYNField = value;
            }
        }

        /// <remarks/>
        public string ReportingServicesReportDisplayInd
        {
            get
            {
                return this.reportingServicesReportDisplayIndField;
            }
            set
            {
                this.reportingServicesReportDisplayIndField = value;
            }
        }

        /// <remarks/>
        public int ProductTypeID
        {
            get
            {
                return this.productTypeIDField;
            }
            set
            {
                this.productTypeIDField = value;
            }
        }

        /// <remarks/>
        public int ReportingServicesReportID
        {
            get
            {
                return this.reportingServicesReportIDField;
            }
            set
            {
                this.reportingServicesReportIDField = value;
            }
        }
         [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool ReportingServicesReportIDSpecified {
        get {
            return this.reportingServicesReportIDFieldSpecified;
        }
        set {
            this.reportingServicesReportIDFieldSpecified = value;
        }
    }
    
    /// <remarks/>
    public string CreatedByUser {
        get {
            return this.createdByUserField;
        }
        set {
            this.createdByUserField = value;
        }
    }
    
    /// <remarks/>
    public System.DateTime CreatedOnDate {
        get {
            return this.createdOnDateField;
        }
        set {
            this.createdOnDateField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool CreatedOnDateSpecified {
        get {
            return this.createdOnDateFieldSpecified;
        }
        set {
            this.createdOnDateFieldSpecified = value;
        }
    }
    
    /// <remarks/>
    public string ChangedByUser {
        get {
            return this.changedByUserField;
        }
        set {
            this.changedByUserField = value;
        }
    }
    
    /// <remarks/>
    public System.DateTime ChangedOnDate {
        get {
            return this.changedOnDateField;
        }
        set {
            this.changedOnDateField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool ChangedOnDateSpecified {
        get {
            return this.changedOnDateFieldSpecified;
        }
        set {
            this.changedOnDateFieldSpecified = value;
        }
    }
}
    }