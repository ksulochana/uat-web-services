﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XDSPortalLibrary.Entity_Layer
{
   public  class GetBonusSegments
    {
        public GetBonusSegments()
        {
        }
        int subscriberIDValue = 0, ProductIDvalue = 0;//, ConsumeridValue = 0, CommercialIDValue = 0, DirectorIDValue = 0,HomeAffairsIDValue = 0,propertyDeedIDValue = 0;
        int KeyIDValue = 0;
        string ReferenceNoValue = string.Empty, ExternalReferenceNoValue = string.Empty,KeyTypeValue = string.Empty;
        private string strdataSegments = string.Empty;

        public string DataSegments
        {
            get { return this.strdataSegments; }
            set { this.strdataSegments = value; }
        }

        public string KeyType
        {
            get
            {
                if (String.IsNullOrEmpty(this.KeyTypeValue))
                    return DBNull.Value.ToString();
                else
                    return this.KeyTypeValue;
            }
            set
            {
                this.KeyTypeValue = value;
            }
        }


        public string ReferenceNo
        {
            get
            {
                if (String.IsNullOrEmpty(this.ReferenceNoValue))
                    return DBNull.Value.ToString();
                else
                    return this.ReferenceNoValue;
            }
            set
            {
                this.ReferenceNoValue = value;
            }
        }

        public string ExternalReference
        {
            get
            {
                if (String.IsNullOrEmpty(this.ExternalReferenceNoValue))
                    return DBNull.Value.ToString();
                else
                    return this.ExternalReferenceNoValue;
            }
            set
            {
                this.ExternalReferenceNoValue = value;
            }
        }

        public int SubscriberID
        {
            get
            {
                return this.subscriberIDValue;                
            }
            set
            {
                this.subscriberIDValue = value;
            }
        }
        public int ProductID
        {
            get
            {
                return this.ProductIDvalue;
            }
            set
            {
                this.ProductIDvalue = value;
            }
        }
        //public int ConsumerID
        //{
        //    get
        //    {
        //        return this.ConsumeridValue;
        //    }
        //    set
        //    {
        //        this.ConsumeridValue = value;
        //    }
        //}
        //public int CommercialID
        //{
        //    get
        //    {
        //        return this.CommercialIDValue;
        //    }
        //    set
        //    {
        //        this.CommercialIDValue = value;
        //    }
        //}
        //public int DirectorID
        //{
        //    get
        //    {
        //        return this.DirectorIDValue;
        //    }
        //    set
        //    {
        //        this.DirectorIDValue = value;
        //    }
        //}
        //public int HomeAffairsID
        //{
        //    get
        //    {
        //        return this.HomeAffairsIDValue;
        //    }
        //    set
        //    {
        //        this.HomeAffairsIDValue = value;
        //    }
        //}
        //public int PropertyDeedID
        //{
        //    get
        //    {
        //        return this.propertyDeedIDValue;
        //    }
        //    set
        //    {
        //        this.propertyDeedIDValue = value;
        //    }
        //}

        public int KeyID
        {
            get
            {
                return this.KeyIDValue;
            }
            set
            {
                this.KeyIDValue = value;
            }
        }

    }
}
