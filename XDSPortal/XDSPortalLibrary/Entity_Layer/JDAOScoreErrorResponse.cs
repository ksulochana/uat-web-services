﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDSPortalLibrary.Entity_Layer
{
    public class Error
    {
        public string message { get; set; }
    }

    public class Detail
    {
        public string path { get; set; }
        public string providedValue { get; set; }
        public List<Error> errors { get; set; }
    }

    public class JDAOScoreErrorResponse
    {
        public string name { get; set; }
        public int status { get; set; }
        public string message { get; set; }
        public List<Detail> details { get; set; }
    }
}
