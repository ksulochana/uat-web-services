using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace XDSPortalLibrary.Entity_Layer
{
    public class ConsumerIDVerificationTrace
    {
        int subscriberIDvalue, productIDvalue, ConsumerIDValue = 0, HomeAffairsIDValue = 0;
        string IDnovalue = string.Empty, strTmpReference = string.Empty, Surnamevalue = string.Empty, Firstnamevalue = string.Empty, Secondnamevalue = string.Empty, externalreferencevalue = string.Empty, ReferenceNoValue = string.Empty;
        bool ConfirmationChkBoxvalue, IsConsumerMatchingvalue = false;
        private DateTime DOBvalue;
        DataSet BonusSegmentsDS;
        private bool boolBonusCheck = false;
        private string strdataSegments = string.Empty;
        private string strAssociationCode = string.Empty;
        private int intEnquiryID = 0;
        private int intEnquiryResultID = 0;
        private string strCreatedbyUser = string.Empty;
        private string strIDIssuedDate = string.Empty;
        private string strVendorID = string.Empty;
        private string strErrors = string.Empty;
        private string strFinger1WsqImage = string.Empty;
        private string strFinger2WsqImage = string.Empty;
        private bool bHasErrors = false;
        private int intFinger1Index = 0;
        private int intFinger2Index = 0;
        private string strFinger1Name = string.Empty;
        private string strFinger2Name = string.Empty;
        private string strDeviceSerialNumber = string.Empty;
        private string strDeviceFirmwareVersion = string.Empty;
        private string strDeviceModel = string.Empty;
        private string strWorkStationName = string.Empty;
        private string strWorkstationLoggedInUserName = string.Empty;
        private DateTime dtLastUsedTimeStamp = DateTime.Now;
        private string strFinger1NameString = string.Empty;
        private string strFinger2NameString = string.Empty;
        private string strAssociationTypeCode = string.Empty;
        private string strIVSURl = string.Empty;
        private string strHanisURl1 = string.Empty;
        private string strHanisURl2 = string.Empty;
        private string strHanisSiteID = string.Empty;
        private string strHanisWorkStationID = string.Empty;
        private string strHanisType = string.Empty;
        private string strJ2KURl = string.Empty;
        private string strDHAURl = string.Empty;
        private string strDIAURl = string.Empty;
        private string strDIAUsername = string.Empty;
        private string strDIAPassword = string.Empty;
        private bool bGetImage = false;
        private DateTime dtHanisTimeStamp = DateTime.Now;
        private string strApplicationErrors = string.Empty;
        private string strVerificationErrors = string.Empty;
        private string strBase64StringJpeg2000Image = string.Empty;
        private string strVerificationResult = string.Empty;
        private string strVerificationResultDescription = string.Empty;
        private string strVerificationColorIndicator = string.Empty;
        private long iTransactionNumber = 0;
        private bool bHasImage = false;
        private string strFingerColor = string.Empty;
        private int intXDSRef = 0;
        private string strValidateIDMessage = string.Empty;
        private string strRelationshipName = string.Empty;

        public string RelationshipName
        {
            get { return this.strRelationshipName; }
            set { this.strRelationshipName = value; }
        }
        public string ValidateIDMessage
        {
            get { return this.strValidateIDMessage; }
            set { this.strValidateIDMessage = value; }
        }
        private string strchkTmpRTIDV = string.Empty;
        public string chkTmpRTIDV
        {
            get { return this.strchkTmpRTIDV; }
            set { this.strchkTmpRTIDV = value; }
        }
        public string SubscriberAssociationCode
        {
            get { return this.strAssociationCode; }
            set { this.strAssociationCode = value; }
        }

        public string AssociationTypeCode
        {
            get { return this.strAssociationTypeCode; }
            set { this.strAssociationTypeCode = value; }
        }

        public string IDIssuedDate
        {
            get { return this.strIDIssuedDate; }
            set { this.strIDIssuedDate = value; }
        }

        public string DataSegments
        {
            get { return this.strdataSegments; }
            set { this.strdataSegments = value; }
        }

        public bool BonusCheck
        {
            get { return this.boolBonusCheck; }
            set { this.boolBonusCheck = value; }
        }

        public ConsumerIDVerificationTrace()
        {
        }
        public int ConsumerID
        {
            get
            {
                return this.ConsumerIDValue;
            }
            set
            {
                this.ConsumerIDValue = value;
            }
        }
        public DataSet BonusSegments
        {
            get
            {
                return this.BonusSegmentsDS;
            }
            set
            {
                this.BonusSegmentsDS = value;
            }
        }
        public string ReferenceNo
        {
            get
            {
                if (String.IsNullOrEmpty(this.ReferenceNoValue))
                    return DBNull.Value.ToString();
                else
                    return this.ReferenceNoValue;
            }
            set
            {
                this.ReferenceNoValue = value;
            }
        }

        public int subscriberID
        {
            get
            {
                if (String.IsNullOrEmpty(this.subscriberIDvalue.ToString()))
                    return Convert.ToInt16(DBNull.Value);
                else
                    return this.subscriberIDvalue;
            }
            set
            {
                this.subscriberIDvalue = value;
            }
        }
        public int ProductID
        {
            get
            {
                if (String.IsNullOrEmpty(this.subscriberIDvalue.ToString()))
                    return Convert.ToInt16(DBNull.Value);
                else
                    return this.productIDvalue;
            }
            set
            {
                this.productIDvalue = value;
            }
        }

        public string IDno
        {
            get
            {
                if (String.IsNullOrEmpty(IDnovalue))
                    return DBNull.Value.ToString();
                else
                    return this.IDnovalue;
            }
            set
            {
                this.IDnovalue = value;
            }
        }
        public string Surname
        {
            get
            {
                if (String.IsNullOrEmpty(Surnamevalue))
                    return DBNull.Value.ToString();
                else
                    return this.Surnamevalue;
            }
            set
            {
                this.Surnamevalue = value;
            }
        }
        public string Firstname
        {
            get
            {
                if (String.IsNullOrEmpty(Firstnamevalue))
                    return DBNull.Value.ToString();
                else
                    return this.Firstnamevalue;
            }
            set
            {
                this.Firstnamevalue = value;
            }
        }
        public string SecondName
        {
            get
            {
                if (String.IsNullOrEmpty(Secondnamevalue))
                    return DBNull.Value.ToString();
                else
                    return this.Secondnamevalue;
            }
            set
            {
                this.Secondnamevalue = value;
            }
        }
        public DateTime DOB
        {
            get
            {
                if (this.DOBvalue == null)
                    return Convert.ToDateTime(DBNull.Value);
                else
                    return this.DOBvalue;
            }
            set
            {
                this.DOBvalue = value;
            }
        }
        public string ExternalReference
        {
            get
            {
                if (String.IsNullOrEmpty(externalreferencevalue))
                    return DBNull.Value.ToString();
                else
                    return this.externalreferencevalue;
            }
            set
            {
                this.externalreferencevalue = value;
            }
        }
        public bool ConfirmationChkBox
        {
            get
            {
                return this.ConfirmationChkBoxvalue;
            }
            set
            {
                this.ConfirmationChkBoxvalue = value;
            }
        }
        public int HomeAffairsID
        {
            get
            {
                return this.HomeAffairsIDValue;
            }
            set
            {
                this.HomeAffairsIDValue = value;
            }
        }

        public bool IsConsumerMatching
        {
            get
            {
                return this.IsConsumerMatchingvalue;
            }
            set
            {
                this.IsConsumerMatchingvalue = value;
            }
        }
        public string TmpReference
        {
            get
            {
                if (String.IsNullOrEmpty(this.strTmpReference))
                    return DBNull.Value.ToString();
                else
                    return this.strTmpReference;
            }
            set
            {
                this.strTmpReference = value;
            }
        }

        public int EnquiryID
        {
            get { return this.intEnquiryID; }
            set { this.intEnquiryID = value; }
        }
        public int EnquiryResultID
        {
            get { return this.intEnquiryResultID; }
            set { this.intEnquiryResultID = value; }
        }

        public string CreatedbyUser
        {
            get { return this.strCreatedbyUser; }
            set { this.strCreatedbyUser = value; }
        }

        public string VendorID
        {
            get { return this.strVendorID; }
            set { this.strVendorID = value; }
        }

        public string Errors
        {
            get { return this.strErrors; }
            set { this.strErrors = value; }
        }

        public string Finger1WsqImage
        {
            get { return this.strFinger1WsqImage; }
            set { this.strFinger1WsqImage = value; }
        }

        public string Finger2WsqImage
        {
            get { return this.strFinger2WsqImage; }
            set { this.strFinger2WsqImage = value; }
        }

        public bool HasErrors
        {
            get { return this.bHasErrors; }
            set { this.bHasErrors = value; }
        }
     
        public int Finger1Index
        {
            get { return this.intFinger1Index; }
            set { this.intFinger1Index = value; }
        }

        public int Finger2Index
        {
            get { return this.intFinger2Index; }
            set { this.intFinger2Index = value; }
        }
        public string Finger1Name
        {
            get { return this.strFinger1Name; }
            set { this.strFinger1Name = value; }
        }

        public string Finger2Name
        {
            get { return this.strFinger2Name; }
            set { this.strFinger2Name = value; }
        }
        public string DeviceSerialNumber
        {
            get { return this.strDeviceSerialNumber; }
            set { this.strDeviceSerialNumber = value; }
        }

        public string DeviceFirmwareVersion
        {
            get { return this.strDeviceFirmwareVersion; }
            set { this.strDeviceFirmwareVersion = value; }
        }

        public string DeviceModel
        {
            get { return this.strDeviceModel; }
            set { this.strDeviceModel = value; }
        }
        public string WorkStationName
        {
            get { return this.strWorkStationName; }
            set { this.strWorkStationName = value; }
        }

        public string WorkstationLoggedInUserName
        {
            get { return this.strWorkstationLoggedInUserName; }
            set { this.strWorkstationLoggedInUserName = value; }
        }
        public DateTime LastUsedTimeStamp
        {
            get { return this.dtLastUsedTimeStamp; }
            set { this.dtLastUsedTimeStamp = value; }
        }
        public string Finger1NameString
        {
            get { return this.strFinger1NameString; }
            set { this.strFinger1NameString = value; }
        }
        public string Finger2NameString
        {
            get { return this.strFinger2NameString; }
            set { this.strFinger2NameString = value; }
        }
        public string IVSURL
        {
            get { return this.strIVSURl; }
            set { this.strIVSURl = value; }
        }

        public string HanisURl
        {
            get { return this.strHanisURl1; }
            set { this.strHanisURl1 = value; }
        }

        public string HanisUR2
        {
            get { return this.strHanisURl2; }
            set { this.strHanisURl2 = value; }
        }
       
        public string HanisSiteID
        {
            get { return this.strHanisSiteID; }
            set { this.strHanisSiteID = value; }
        }
        public string HanisWorkStationID
        {
            get { return this.strHanisWorkStationID; }
            set { this.strHanisWorkStationID = value; }
        }

        public string HanisType
        {
            get { return this.strHanisType; }
            set { this.strHanisType = value; }
        }

        public string J2KURl
        {
            get { return this.strJ2KURl; }
            set { this.strJ2KURl = value; }
        }

        public string DHAURl
        {
            get { return this.strDHAURl; }
            set { this.strDHAURl = value; }
        }

        public string DIAURl
        {
            get { return this.strDIAURl; }
            set { this.strDIAURl = value; }
        }

        public string DIAUsername
        {
            get { return this.strDIAUsername; }
            set { this.strDIAUsername = value; }
        }

        public string DIAPassword
        {
            get { return this.strDIAPassword; }
            set { this.strDIAPassword = value; }
        }

        public bool Getimage
        {
            get { return this.bGetImage; }
            set { this.bGetImage = value; }
        }

        public string ApplicationErrors
        {
            get { return this.strApplicationErrors; }
            set
            {
                this.strApplicationErrors = value;
            }
        }

        public string VerificationErrors
        {
            get { return this.strVerificationErrors; }
            set
            {
                this.strVerificationErrors = value;
            }
        }

        public string Base64StringJpeg2000Image
        {
            get { return this.strBase64StringJpeg2000Image; }
            set
            {
                this.strBase64StringJpeg2000Image = value;
            }
        }

        public string VerificationResult
        {
            get { return this.strVerificationResult; }
            set
            {
                this.strVerificationResult = value;
            }
        }

        public string VerificationResultDescription
        {
            get { return this.strVerificationResultDescription; }
            set
            {
                this.strVerificationResultDescription = value;
            }
        }

        public string VerificationColorIndicator
        {
            get { return this.strVerificationColorIndicator; }
            set
            {
                this.strVerificationColorIndicator = value;
            }
        }

        

        public long TransactionNumber
        {
            get { return this.iTransactionNumber; }
            set
            {
                this.iTransactionNumber = value;
            }
        }

        public bool HasImage
        {
            get { return this.bHasImage; }
            set
            {
                this.bHasImage = value;
            }
        }

        public string FingerColor
        {
            get { return this.strFingerColor; }
            set
            {
                this.strFingerColor = value;
            }
        }

        
             public DateTime HanisTimeStamp
        {
            get { return this.dtHanisTimeStamp; }
            set
            {
                this.dtHanisTimeStamp = value;
            }
        }

        public int XDSRef
        {
            get { return this.intXDSRef; }
            set
            {
                this.intXDSRef = value;
            }
        }


    }
}
