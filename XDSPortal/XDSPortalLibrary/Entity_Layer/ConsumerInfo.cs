﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace XDSPortalLibrary.Entity_Layer
{
    public class ConsumerInfo
    {
        public ConsumerInfo()
        {

        }

        private long _ConsumerID;
        private int _SubscriberEnquiryID;
        private int _SubscriberEnquiryResultID;
        private string _Address1;
        private string _Address2;
        private string _Address3;
        private string _Address4;
        private string _PostalCode;
        private string _User;
        private string _ErrorMessage;

        public long ConsumerID
        {
            get { return this._ConsumerID; }
            set { this._ConsumerID = value; }
        }

        public int SubscriberEnquiryID
        {
            get { return this._SubscriberEnquiryID; }
            set { this._SubscriberEnquiryID = value; }
        }

        public int SubscriberEnquiryResultID
        {
            get { return this._SubscriberEnquiryResultID; }
            set { this._SubscriberEnquiryResultID = value; }
        }

        public string Address1
        {
            get { return this._Address1; }
            set { this._Address1 = value; }
        }

        public string Address2
        {
            get { return this._Address2; }
            set { this._Address2 = value; }
        }

        public string Address3
        {
            get { return this._Address3; }
            set { this._Address3 = value; }
        }

        public string Address4
        {
            get { return this._Address4; }
            set { this._Address4 = value; }
        }

        public string PostalCode
        {
            get { return this._PostalCode; }
            set { this._PostalCode = value; }
        }

        public string User
        {
            get { return this._User; }
            set { this._User = value; }
        }

        public string ErrorMessage
        {
            get { return this._ErrorMessage; }
            set { this._ErrorMessage = value; }
        }
    }
}
