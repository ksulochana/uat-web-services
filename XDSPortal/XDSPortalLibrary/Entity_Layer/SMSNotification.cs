﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XDSPortalLibrary.Entity_Layer
{
    public  class SMSNotification
    {
        public SMSNotification()
        {
        }

        private string strSubscriberName = string.Empty, strContactNo = string.Empty,
            strClientContactDetails = string.Empty, strAccountNo = string.Empty, strAlertType = string.Empty, strMessage = string.Empty, strReferenceNo = string.Empty,strQueueID = string.Empty;
        private bool bIsCommercial = false;
        private int intSubscriberID = 0;

        public string SubscriberName
        {
            get { return this.strSubscriberName; }
            set { this.strSubscriberName = value; }
        }
        public string ContactNo
        {
            get { return this.strContactNo; }
            set { this.strContactNo = value; }
        }
        public string ClientContactDetails
        {
            get { return this.strClientContactDetails; }
            set { this.strClientContactDetails = value; }
        }
        public string AccountNo
        {
            get { return this.strAccountNo; }
            set { this.strAccountNo = value; }
        }
        public string AlertType
        {
            get { return this.strAlertType; }
            set { this.strAlertType = value; }
        }

        public bool IsCommercial
        {
            get { return this.bIsCommercial; }
            set { this.bIsCommercial = value; }
        }

        public int SubscriberID
        {
            get { return this.intSubscriberID; }
            set { this.intSubscriberID = value; }
        }

        public string Message
        {
            get { return this.strMessage; }
            set { this.strMessage = value; }
        }

        public string ReferenceNo
        {
            get { return this.strReferenceNo; }
            set { this.strReferenceNo = value; }
        }

        public string QueueID
        {
            get { return this.strQueueID; }
            set { this.strQueueID = value; }
        }
    }
}
