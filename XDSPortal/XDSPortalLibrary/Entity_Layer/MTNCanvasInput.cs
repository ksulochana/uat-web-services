﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDSPortalLibrary.Entity_Layer
{
    public class MTNCanvasInput
    {

        private string companyField;

        private string ruleSetField;

        private string branchField;

        private string userField;

        private string passwordField;

        private string isExistingClientField;

        private string grossMonthlyIncomeField;

        private string surnameField;

        private string firstNameField;

        private string iDTypeField;

        private string birthDateField;

        private string identityNumberField;

        private string genderField;

        private string maritalStatusField;

        private string addressLine1Field;

        private string addressLine2Field;

        private string suburbField;

        private string cityField;

        private string postalCodeField;

        private string homeTelephoneCodeField;

        private string homeTelephoneNumberField;

        private string workTelephoneCodeField;

        private string workTelephoneNumberField;

        private string cellNumberField;

        private string occupationField;

        private string employerField;

        private string numberOfYearsAtEmployerField;

        private string bankNameField;

        private string bankAccountTypeField;

        private string bankBranchCodeField;

        private string bankAccountNumberField;

        private string highestQualificationField;

        private string dealerCodeField;

        private string customerNameField;

        private string channelField;

        private string alternatecontactnumberField;
        private string emailaddressField;
        private bool overwriteAVSResultField;
        private string initialsField;
        private string accountHolderNameField;

        /// <remarks/>
        public string Company
        {
            get
            {
                return this.companyField;
            }
            set
            {
                this.companyField = value;
            }
        }

        /// <remarks/>
        public string RuleSet
        {
            get
            {
                return this.ruleSetField;
            }
            set
            {
                this.ruleSetField = value;
            }
        }

        /// <remarks/>
        public string Branch
        {
            get
            {
                return this.branchField;
            }
            set
            {
                this.branchField = value;
            }
        }

        /// <remarks/>
        public string User
        {
            get
            {
                return this.userField;
            }
            set
            {
                this.userField = value;
            }
        }

        /// <remarks/>
        public string Password
        {
            get
            {
                return this.passwordField;
            }
            set
            {
                this.passwordField = value;
            }
        }

        /// <remarks/>
        public string IsExistingClient
        {
            get
            {
                return this.isExistingClientField;
            }
            set
            {
                this.isExistingClientField = value;
            }
        }

        /// <remarks/>
        public string GrossMonthlyIncome
        {
            get
            {
                return this.grossMonthlyIncomeField;
            }
            set
            {
                this.grossMonthlyIncomeField = value;
            }
        }

        /// <remarks/>
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        public string FirstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string IDType
        {
            get
            {
                return this.iDTypeField;
            }
            set
            {
                this.iDTypeField = value;
            }
        }

        /// <remarks/>
        public string BirthDate
        {
            get
            {
                return this.birthDateField;
            }
            set
            {
                this.birthDateField = value;
            }
        }

        /// <remarks/>
        public string IdentityNumber
        {
            get
            {
                return this.identityNumberField;
            }
            set
            {
                this.identityNumberField = value;
            }
        }

        /// <remarks/>
        public string Gender
        {
            get
            {
                return this.genderField;
            }
            set
            {
                this.genderField = value;
            }
        }

        /// <remarks/>
        public string MaritalStatus
        {
            get
            {
                return this.maritalStatusField;
            }
            set
            {
                this.maritalStatusField = value;
            }
        }

        /// <remarks/>
        public string AddressLine1
        {
            get
            {
                return this.addressLine1Field;
            }
            set
            {
                this.addressLine1Field = value;
            }
        }

        /// <remarks/>
        public string AddressLine2
        {
            get
            {
                return this.addressLine2Field;
            }
            set
            {
                this.addressLine2Field = value;
            }
        }

        /// <remarks/>
        public string Suburb
        {
            get
            {
                return this.suburbField;
            }
            set
            {
                this.suburbField = value;
            }
        }

        /// <remarks/>
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        public string PostalCode
        {
            get
            {
                return this.postalCodeField;
            }
            set
            {
                this.postalCodeField = value;
            }
        }

        /// <remarks/>
        public string HomeTelephoneCode
        {
            get
            {
                return this.homeTelephoneCodeField;
            }
            set
            {
                this.homeTelephoneCodeField = value;
            }
        }

        /// <remarks/>
        public string HomeTelephoneNumber
        {
            get
            {
                return this.homeTelephoneNumberField;
            }
            set
            {
                this.homeTelephoneNumberField = value;
            }
        }

        /// <remarks/>
        public string WorkTelephoneCode
        {
            get
            {
                return this.workTelephoneCodeField;
            }
            set
            {
                this.workTelephoneCodeField = value;
            }
        }

        /// <remarks/>
        public string WorkTelephoneNumber
        {
            get
            {
                return this.workTelephoneNumberField;
            }
            set
            {
                this.workTelephoneNumberField = value;
            }
        }

        /// <remarks/>
        public string CellNumber
        {
            get
            {
                return this.cellNumberField;
            }
            set
            {
                this.cellNumberField = value;
            }
        }

        /// <remarks/>
        public string Occupation
        {
            get
            {
                return this.occupationField;
            }
            set
            {
                this.occupationField = value;
            }
        }

        /// <remarks/>
        public string Employer
        {
            get
            {
                return this.employerField;
            }
            set
            {
                this.employerField = value;
            }
        }

        /// <remarks/>
        public string NumberOfYearsAtEmployer
        {
            get
            {
                return this.numberOfYearsAtEmployerField;
            }
            set
            {
                this.numberOfYearsAtEmployerField = value;
            }
        }

        /// <remarks/>
        public string BankName
        {
            get
            {
                return this.bankNameField;
            }
            set
            {
                this.bankNameField = value;
            }
        }

        /// <remarks/>
        public string BankAccountType
        {
            get
            {
                return this.bankAccountTypeField;
            }
            set
            {
                this.bankAccountTypeField = value;
            }
        }

        /// <remarks/>
        public string BankBranchCode
        {
            get
            {
                return this.bankBranchCodeField;
            }
            set
            {
                this.bankBranchCodeField = value;
            }
        }

        /// <remarks/>
        public string BankAccountNumber
        {
            get
            {
                return this.bankAccountNumberField;
            }
            set
            {
                this.bankAccountNumberField = value;
            }
        }

        /// <remarks/>
        public string HighestQualification
        {
            get
            {
                return this.highestQualificationField;
            }
            set
            {
                this.highestQualificationField = value;
            }
        }

        /// <remarks/>
        public string DealerCode
        {
            get
            {
                return this.dealerCodeField;
            }
            set
            {
                this.dealerCodeField = value;
            }
        }

        /// <remarks/>
        public string CustomerName
        {
            get
            {
                return this.customerNameField;
            }
            set
            {
                this.customerNameField = value;
            }
        }

        /// <remarks/>
        public string Channel
        {
            get
            {
                return this.channelField;
            }
            set
            {
                this.channelField = value;
            }
        }

        /// <remarks/>
        public string AlternateContactNumber
        {
            get
            {
                return this.alternatecontactnumberField;
            }
            set
            {
                this.alternatecontactnumberField = value;
            }
        }

        /// <remarks/>
        public string EmailAddress
        {
            get
            {
                return this.emailaddressField;
            }
            set
            {
                this.emailaddressField = value;
            }
        }

        public bool OverwriteAVSResult
        {
            get
            {
                return this.overwriteAVSResultField;
            }
            set
            {
                this.overwriteAVSResultField = value;
            }
        }

        public string Initials
        {
            get
            {
                return this.initialsField;
            }
            set
            {
                this.initialsField = value;
            }
        }

        public string AccountHolderName
        {
            get
            {
                return this.accountHolderNameField;
            }
            set
            {
                this.accountHolderNameField = value;
            }
        }
    }
}
