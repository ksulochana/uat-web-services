using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace XDSPortalLibrary.Entity_Layer
{
  public class BusinessEnquiry
    {
      private string RegistrationNovalue = string.Empty, strTmpReference = string.Empty, Businessnamevalue = string.Empty, ExternalReferencevalue = string.Empty, ReferenceNoValue = string.Empty;
        bool ConfirmationChkBoxvalue,IsMatchingValue = false;
        private int subscriberIDvalue, productIDvalue,CommercialIDValue =0, _ReportID=0, CommercialScoreIDField =0;
        DataSet BonusSegmentsDS;
        private string VATNumberValue = string.Empty, SolePropIDNoValue = string.Empty, TrustNoValue = string.Empty, _ReportName = string.Empty;
        private bool boolBonusCheck = false;
        private string strdataSegments = string.Empty;

        private int intSubscriberEnquiryID = 0;
        private int intSubscriberEnquiryResultID = 0;

        private string strUsername = string.Empty;
        private string strAssociationTypeCode = string.Empty;
        private string strAssociationCode = string.Empty;

        private string isExistingClientField = string.Empty;
        private string productindicatorField = string.Empty;
        private DataTable dtSolepropConsumerlist = new DataTable();
        public int CommercialScoreID
        {
            get
            {
                return this.CommercialScoreIDField;
            }
            set
            {
                this.CommercialScoreIDField = value;
            }
        }
        public string IsExistingClient
        {
            get { return this.isExistingClientField; }
            set { this.isExistingClientField = value; }
        }

        public string ProductIndicator
        {
            get { return this.productindicatorField; }
            set { this.productindicatorField = value; }
        }


        public int ReportID
        {
            get { return this._ReportID; }
            set { this._ReportID = value; }
        }

        public string ReportName
        {
            get { return this._ReportName; }
            set { this._ReportName = value; }
        }

        public string DataSegments
        {
            get { return this.strdataSegments; }
            set { this.strdataSegments = value; }
        }

        public bool BonusCheck
        {
            get { return this.boolBonusCheck; }
            set { this.boolBonusCheck = value; }
        }

        public BusinessEnquiry()
        {
        }
        public int CommercialID
        {
            get
            {
                return this.CommercialIDValue;
            }
            set
            {
                this.CommercialIDValue = value;
            }
        }
        public DataSet BonusSegments
        {
            get
            {
                return this.BonusSegmentsDS;
            }
            set
            {
                this.BonusSegmentsDS = value;
            }
        }
        public string ReferenceNo
        {
            get
            {
                if (String.IsNullOrEmpty(this.ReferenceNoValue))
                    return DBNull.Value.ToString();
                else
                    return this.ReferenceNoValue;
            }
            set
            {
                this.ReferenceNoValue = value;
            }
        }

        public int subscriberID
        {
            get
            {
                if (String.IsNullOrEmpty(this.subscriberIDvalue.ToString()))
                    return Convert.ToInt16(DBNull.Value);
                else
                    return this.subscriberIDvalue;
            }
            set
            {
                this.subscriberIDvalue = value;
            }
        }
        public int ProductID
        {
            get
            {
                if (String.IsNullOrEmpty(this.subscriberIDvalue.ToString()))
                    return Convert.ToInt16(DBNull.Value);
                else
                    return this.productIDvalue;
            }
            set
            {
                this.productIDvalue = value;
            }
        }
        public string RegistrationNo
        {
            get
            {
                if (String.IsNullOrEmpty(this.RegistrationNovalue))
                    return DBNull.Value.ToString();
                else
                    return this.RegistrationNovalue;
            }
            set
            {
                this.RegistrationNovalue = value;
            }
        }
        public string BusinessName
        {
            get
            {
                if (String.IsNullOrEmpty(this.Businessnamevalue))
                    return DBNull.Value.ToString();
                else
                    return this.Businessnamevalue;
            }
            set
            {
                this.Businessnamevalue = value;
            }
        }
        public string ExternalReference
        {
            get
            {
                if (String.IsNullOrEmpty(this.ExternalReferencevalue))
                    return DBNull.Value.ToString();
                else
                    return this.ExternalReferencevalue;
            }
            set
            {
                this.ExternalReferencevalue = value;
            }
        }
        public bool ConfirmationChkBox
        {
            get
            {
                return this.ConfirmationChkBoxvalue;
            }
            set
            {
                this.ConfirmationChkBoxvalue = value;
            }
        }
        public bool IsMatching
        {
            get
            {
                return this.IsMatchingValue;
            }
            set
            {
                this.IsMatchingValue = value;
            }
        }
        public string TmpReference
        {
            get
            {
                if (String.IsNullOrEmpty(this.strTmpReference))
                    return DBNull.Value.ToString();
                else
                    return this.strTmpReference;
            }
            set
            {
                this.strTmpReference = value;
            }
        }
        public string VATNumber
        {
            get
            {
                return this.VATNumberValue;
            }
            set
            {
                this.VATNumberValue = value;
            }
        }
        public string SolePropIDNo
        {
            get
            {
                return this.SolePropIDNoValue;
            }
            set
            {
                this.SolePropIDNoValue = value;
            }
        }
        public string TrustNo
        {
            get
            {
                return this.TrustNoValue;
            }
            set
            {
                this.TrustNoValue = value;
            }
        }

        public int SubscriberEnquiryID
        {
            get
            {
                return this.intSubscriberEnquiryID;
            }
            set
            {
                this.intSubscriberEnquiryID = value;
            }
        }

        public int SubscriberEnquiryResultID
        {
            get
            {
                return this.intSubscriberEnquiryResultID;
            }
            set
            {
                this.intSubscriberEnquiryResultID = value;
            }
        }

      public string Username
        {
            get { return this.strUsername; }
            set { this.strUsername = value; }
        }

      public string AssociationTypeCode
      {
          get { return this.strAssociationTypeCode; }
          set { this.strAssociationTypeCode = value; }
      }

      public string AssociationCode
      {
          get { return this.strAssociationCode; }
          set { this.strAssociationCode = value; }
      }

        public DataTable SolepropConsumerlist
        {
            get { return this.dtSolepropConsumerlist; }
            set { this.dtSolepropConsumerlist = value; }
        }
    }
}
