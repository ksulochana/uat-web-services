﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XDSPortalLibrary.Entity_Layer
{
    public class SubscriberAuthenticationProfile
    {
        public SubscriberAuthenticationProfile()
        {
        }

        private int _SubscriberAuthenticationProfileID = 0;
        private int _SubscriberID = 0;
        private string _SubscriberGroupCode = string.Empty;
        private bool _BlockUser = false;
        private int _NoOfRetries = 0;
        private int _RetryPeriod = 0;
        private int _TotalNoofQuestions = 0;
        private bool _PersonalQuestionsCheck = false;
        private bool _UnblockAccess = false;
        private bool _ReferToFraud = false;
        private bool _Splitscreen = false;
        private long _QuestionTimeout = 0;
        private bool _CellNoMandatory = false;
        private bool _ExcludeInputcellNo = false;
        private bool _ExcludeLatestDBcellNo = false;
        private bool _OverrideOTP = false;
        private bool _ExcludeEmailAddress = false;
        private bool _EmailMandatory = false;

        //V2.2
        private bool _FraudScore = false;
        private bool _AccountVerification = false;
        private bool _IDPhoto = false;
        private bool _CreditReport = false;
        private bool _ResendOTP = false;
        private int _NoOfOTPRetries = 0;
        private bool _NotificationAlert = false;

        public int SubscriberAuthenticationProfileID
        {
            get { return this._SubscriberAuthenticationProfileID; }
            set { this._SubscriberAuthenticationProfileID = value; }
        }
        public int SubscriberID
        {
            get {return this._SubscriberID; }
            set { this._SubscriberID = value; }
        }
        public string SubscriberGroupCode
        {
            get { return this._SubscriberGroupCode; }
            set { this._SubscriberGroupCode = value; }
        }
        public bool BlockUser
        {
            get { return this._BlockUser; }
            set { this._BlockUser = value; }
        }
        public int NoOfRetries
        {
            get { return this._NoOfRetries; }
            set { this._NoOfRetries = value; }
        }
        public int RetryPeriod
        {
            get { return this._RetryPeriod; }
            set { this._RetryPeriod = value; }
        }
        public int TotalNoOfQuestions
        {
            get { return this._TotalNoofQuestions; }
            set { this._TotalNoofQuestions = value; }
        }
        public bool PersonalQuestionsCheck
        {
            get { return this._PersonalQuestionsCheck; }
            set { this._PersonalQuestionsCheck = value; }
        }
        public bool UnblockAccess
        {
            get { return this._UnblockAccess; }
            set { this._UnblockAccess = value; }
        }
        public bool ReferToFraud
        {
            get { return this._ReferToFraud; }
            set { this._ReferToFraud = value; }
        }
        public bool Splitscreen
        {
            get { return this._Splitscreen; }
            set { this._Splitscreen = value; }
        }
        public long QuestionTimeout
        {
            get { return this._QuestionTimeout; }
            set { this._QuestionTimeout = value; }
        }
        public bool CellNoMandatory
        {
            get { return this._CellNoMandatory; }
            set { this._CellNoMandatory = value; }
        }

        public bool ExcludeLatestDBcellNo
        {
            get { return this._ExcludeLatestDBcellNo; }
            set { this._ExcludeLatestDBcellNo = value; }
        }

        public bool ExcludeInputcellNo
        {
            get { return this._ExcludeInputcellNo; }
            set { this._ExcludeInputcellNo = value; }
        }

        public bool OverrideOTP
        {
            get { return this._OverrideOTP; }
            set { this._OverrideOTP = value; }
        }

        public bool ExcludeEmailAddress
        {
            get { return this._ExcludeEmailAddress; }
            set { this._ExcludeEmailAddress = value; }
        }

        public bool EmailMandatory
        {
            get { return this._EmailMandatory; }
            set { this._EmailMandatory = value; }
        }

        //V2.2
        public bool FraudScore
        {
            get { return this._FraudScore; }
            set { this._FraudScore = value; }
        }

        public bool AccountVerification
        {
            get { return this._AccountVerification; }
            set { this._AccountVerification = value; }
        }

        public bool IDPhoto
        {
            get { return this._IDPhoto; }
            set { this._IDPhoto = value; }
        }

        public bool CreditReport
        {
            get { return this._CreditReport; }
            set { this._CreditReport = value; }
        }

        public bool ResendOTP
        {
            get { return this._ResendOTP; }
            set { this._ResendOTP = value; }
        }

        public int NoOfOTPRetries
        {
            get { return this._NoOfOTPRetries; }
            set { this._NoOfOTPRetries = value; }
        }

        public bool NotificationAlert
        {
            get { return this._NotificationAlert; }
            set { this._NotificationAlert = value; }
        }
    }
}
