﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDSPortalLibrary.Entity_Layer
{
  

    //public class IdDocument
    //{
    //    public string type { get; set; }
    //    public string value { get; set; }
    //    public DateTime dateOfBirth { get; set; }
    //}

    public class Characteristic
    {
        public string name { get; set; }
        public int? value { get; set; }
        public double? weighting { get; set; }
    }

    public class ModelResults
    {
        public int aoScore { get; set; }
        public object exclusionReason { get; set; }
        public List<Characteristic> characteristics { get; set; }
    }

    public class JDAOScoreResponse
    {
        public string submissionId { get; set; }
        public string clientReference { get; set; }
        public bool matched { get; set; }
        //public XDSPortalLibrary.Entity_Layer.idDocument idDocument { get; set; }
        public int consumerId { get; set; }
        public ModelResults modelResults { get; set; }
    }


}
