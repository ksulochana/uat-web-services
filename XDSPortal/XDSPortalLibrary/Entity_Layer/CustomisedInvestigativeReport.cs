﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDSPortalLibrary.Entity_Layer
{
    public class CustomisedInvestigativeReport
    {
        public string Country { get; set; }
        public string ReportType { get; set; }
        public string TimeFrame { get; set; }
        public string Reg1 { get; set; }
        public string Reg2 { get; set; }
        public string Reg3 { get; set; }
        public string BusinessName { get; set; }
        public string IDNo { get; set; }
        public string AdditionalInfo { get; set; }
        public string CompanyContactNo { get; set; }
        public string Terms { get; set; }
        public double Amount { get; set; }
        public string FirstName { get; set; }
        public string SurName { get; set; }
        public string TelNo { get; set; }
        public string EmailAddress { get; set; }
        public string CompanyName { get; set; }
        public bool SendEmail { get; set; }
        public string VoucherCode { get; set; }
        public string YourReference { get; set; }
        public BankCode BankCode { get; set; }
        public TradeReference[] TradeReferences { get; set; }
    }

    public partial class TradeReference
    {
        public string Supplier { get; set; }
        public string SupplierContact { get; set; }
    }

    public partial class BankCode
    {
        public string BankName { get; set; }
        public string Branch { get; set; }
        public string BranchNumber { get; set; }
        public string AccountNumber { get; set; }
        public string AccountName { get; set; }
    }

}
