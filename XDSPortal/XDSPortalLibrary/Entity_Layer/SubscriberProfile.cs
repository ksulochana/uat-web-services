﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XDSPortalLibrary.Entity_Layer
{
   public class SubscriberProfile
    {
        public SubscriberProfile()
        {
        }
        private int subscriberIDField;

        private bool authenticationSearchOnIDNoYNField;

        private bool authenticationSearchOnCellPhoneNoYNField;

        private bool authenticationSearchOnAccountNoYNField;

        private string authenticationDefaultIDVerificationSelectionIndField;

        private int authenticationPrimaryNoOfPositiveHighPriorityQuestionsField;

        private int authenticationPrimaryNoOfPositiveMediumPriorityQuestionsField;

        private int authenticationPrimaryNoOfPositiveLowPriorityQuestionsField;

        private int authenticationPrimaryNoOfNegativeHighPriorityQuestionsField;

        private int authenticationPrimaryNoOfNegativeMediumPriorityQuestionsField;

        private int authenticationPrimaryNoOfNegativeLowPriorityQuestionsField;

        private int authenticationPrimaryNoOfYesNoQuestionsField;

        private int authenticationPrimaryNoOfAnswersPerQuestionField;

        private decimal authenticationPrimaryRequiredAuthenticatedPercField;

        private bool authenticationPrimaryEnsureRequiredNoOfQuestionsYNField;

        private int authenticationSecondaryNoOfPositiveHighPriorityQuestionsField;

        private int authenticationSecondaryNoOfPositiveMediumPriorityQuestionsField;

        private int authenticationSecondaryNoOfPositiveLowPriorityQuestionsField;

        private int authenticationSecondaryNoOfNegativeHighPriorityQuestionsField;

        private int authenticationSecondaryNoOfNegativeMediumPriorityQuestionsField;

        private int authenticationSecondaryNoOfNegativeLowPriorityQuestionsField;

        private int authenticationSecondaryNoOfYesNoQuestionsField;

        private int authenticationSecondaryNoOfAnswersPerQuestionField;

        private decimal authenticationSecondaryRequiredAuthenticatedPercField;

        private bool authenticationSecondaryEnsureRequiredNoOfQuestionsYNField;

        private string createdByUserField;

        private System.DateTime createdOnDateField;

        private bool createdOnDateFieldSpecified;

        private string changedByUserField;

        private System.DateTime changedOnDateField;

        private bool changedOnDateFieldSpecified;

        private bool excludeFromCreditEnquiryYNField;

        private bool excludeFromCreditEnquiryYNFieldSpecified;

        private int CommercialScoreIDField;

        public int CommercialScoreID
        {
            get
            {
                return this.CommercialScoreIDField;
            }
            set
            {
                this.CommercialScoreIDField = value;
            }
        }
        /// <remarks/>
        public int SubscriberID
        {
            get
            {
                return this.subscriberIDField;
            }
            set
            {
                this.subscriberIDField = value;
            }
        }

        /// <remarks/>
        public bool AuthenticationSearchOnIDNoYN
        {
            get
            {
                return this.authenticationSearchOnIDNoYNField;
            }
            set
            {
                this.authenticationSearchOnIDNoYNField = value;
            }
        }

        /// <remarks/>
        public bool AuthenticationSearchOnCellPhoneNoYN
        {
            get
            {
                return this.authenticationSearchOnCellPhoneNoYNField;
            }
            set
            {
                this.authenticationSearchOnCellPhoneNoYNField = value;
            }
        }

        /// <remarks/>
        public bool AuthenticationSearchOnAccountNoYN
        {
            get
            {
                return this.authenticationSearchOnAccountNoYNField;
            }
            set
            {
                this.authenticationSearchOnAccountNoYNField = value;
            }
        }

        /// <remarks/>
        public string AuthenticationDefaultIDVerificationSelectionInd
        {
            get
            {
                return this.authenticationDefaultIDVerificationSelectionIndField;
            }
            set
            {
                this.authenticationDefaultIDVerificationSelectionIndField = value;
            }
        }

        /// <remarks/>
        public int AuthenticationPrimaryNoOfPositiveHighPriorityQuestions
        {
            get
            {
                return this.authenticationPrimaryNoOfPositiveHighPriorityQuestionsField;
            }
            set
            {
                this.authenticationPrimaryNoOfPositiveHighPriorityQuestionsField = value;
            }
        }

        /// <remarks/>
        public int AuthenticationPrimaryNoOfPositiveMediumPriorityQuestions
        {
            get
            {
                return this.authenticationPrimaryNoOfPositiveMediumPriorityQuestionsField;
            }
            set
            {
                this.authenticationPrimaryNoOfPositiveMediumPriorityQuestionsField = value;
            }
        }

        /// <remarks/>
        public int AuthenticationPrimaryNoOfPositiveLowPriorityQuestions
        {
            get
            {
                return this.authenticationPrimaryNoOfPositiveLowPriorityQuestionsField;
            }
            set
            {
                this.authenticationPrimaryNoOfPositiveLowPriorityQuestionsField = value;
            }
        }

        /// <remarks/>
        public int AuthenticationPrimaryNoOfNegativeHighPriorityQuestions
        {
            get
            {
                return this.authenticationPrimaryNoOfNegativeHighPriorityQuestionsField;
            }
            set
            {
                this.authenticationPrimaryNoOfNegativeHighPriorityQuestionsField = value;
            }
        }

        /// <remarks/>
        public int AuthenticationPrimaryNoOfNegativeMediumPriorityQuestions
        {
            get
            {
                return this.authenticationPrimaryNoOfNegativeMediumPriorityQuestionsField;
            }
            set
            {
                this.authenticationPrimaryNoOfNegativeMediumPriorityQuestionsField = value;
            }
        }

        /// <remarks/>
        public int AuthenticationPrimaryNoOfNegativeLowPriorityQuestions
        {
            get
            {
                return this.authenticationPrimaryNoOfNegativeLowPriorityQuestionsField;
            }
            set
            {
                this.authenticationPrimaryNoOfNegativeLowPriorityQuestionsField = value;
            }
        }

        /// <remarks/>
        public int AuthenticationPrimaryNoOfYesNoQuestions
        {
            get
            {
                return this.authenticationPrimaryNoOfYesNoQuestionsField;
            }
            set
            {
                this.authenticationPrimaryNoOfYesNoQuestionsField = value;
            }
        }

        /// <remarks/>
        public int AuthenticationPrimaryNoOfAnswersPerQuestion
        {
            get
            {
                return this.authenticationPrimaryNoOfAnswersPerQuestionField;
            }
            set
            {
                this.authenticationPrimaryNoOfAnswersPerQuestionField = value;
            }
        }

        /// <remarks/>
        public decimal AuthenticationPrimaryRequiredAuthenticatedPerc
        {
            get
            {
                return this.authenticationPrimaryRequiredAuthenticatedPercField;
            }
            set
            {
                this.authenticationPrimaryRequiredAuthenticatedPercField = value;
            }
        }

        /// <remarks/>
        public bool AuthenticationPrimaryEnsureRequiredNoOfQuestionsYN
        {
            get
            {
                return this.authenticationPrimaryEnsureRequiredNoOfQuestionsYNField;
            }
            set
            {
                this.authenticationPrimaryEnsureRequiredNoOfQuestionsYNField = value;
            }
        }

        /// <remarks/>
        public int AuthenticationSecondaryNoOfPositiveHighPriorityQuestions
        {
            get
            {
                return this.authenticationSecondaryNoOfPositiveHighPriorityQuestionsField;
            }
            set
            {
                this.authenticationSecondaryNoOfPositiveHighPriorityQuestionsField = value;
            }
        }

        /// <remarks/>
        public int AuthenticationSecondaryNoOfPositiveMediumPriorityQuestions
        {
            get
            {
                return this.authenticationSecondaryNoOfPositiveMediumPriorityQuestionsField;
            }
            set
            {
                this.authenticationSecondaryNoOfPositiveMediumPriorityQuestionsField = value;
            }
        }

        /// <remarks/>
        public int AuthenticationSecondaryNoOfPositiveLowPriorityQuestions
        {
            get
            {
                return this.authenticationSecondaryNoOfPositiveLowPriorityQuestionsField;
            }
            set
            {
                this.authenticationSecondaryNoOfPositiveLowPriorityQuestionsField = value;
            }
        }

        /// <remarks/>
        public int AuthenticationSecondaryNoOfNegativeHighPriorityQuestions
        {
            get
            {
                return this.authenticationSecondaryNoOfNegativeHighPriorityQuestionsField;
            }
            set
            {
                this.authenticationSecondaryNoOfNegativeHighPriorityQuestionsField = value;
            }
        }

        /// <remarks/>
        public int AuthenticationSecondaryNoOfNegativeMediumPriorityQuestions
        {
            get
            {
                return this.authenticationSecondaryNoOfNegativeMediumPriorityQuestionsField;
            }
            set
            {
                this.authenticationSecondaryNoOfNegativeMediumPriorityQuestionsField = value;
            }
        }

        /// <remarks/>
        public int AuthenticationSecondaryNoOfNegativeLowPriorityQuestions
        {
            get
            {
                return this.authenticationSecondaryNoOfNegativeLowPriorityQuestionsField;
            }
            set
            {
                this.authenticationSecondaryNoOfNegativeLowPriorityQuestionsField = value;
            }
        }

        /// <remarks/>
        public int AuthenticationSecondaryNoOfYesNoQuestions
        {
            get
            {
                return this.authenticationSecondaryNoOfYesNoQuestionsField;
            }
            set
            {
                this.authenticationSecondaryNoOfYesNoQuestionsField = value;
            }
        }

        /// <remarks/>
        public int AuthenticationSecondaryNoOfAnswersPerQuestion
        {
            get
            {
                return this.authenticationSecondaryNoOfAnswersPerQuestionField;
            }
            set
            {
                this.authenticationSecondaryNoOfAnswersPerQuestionField = value;
            }
        }

        /// <remarks/>
        public decimal AuthenticationSecondaryRequiredAuthenticatedPerc
        {
            get
            {
                return this.authenticationSecondaryRequiredAuthenticatedPercField;
            }
            set
            {
                this.authenticationSecondaryRequiredAuthenticatedPercField = value;
            }
        }

        /// <remarks/>
        public bool AuthenticationSecondaryEnsureRequiredNoOfQuestionsYN
        {
            get
            {
                return this.authenticationSecondaryEnsureRequiredNoOfQuestionsYNField;
            }
            set
            {
                this.authenticationSecondaryEnsureRequiredNoOfQuestionsYNField = value;
            }
        }

        /// <remarks/>
        public string CreatedByUser
        {
            get
            {
                return this.createdByUserField;
            }
            set
            {
                this.createdByUserField = value;
            }
        }

        /// <remarks/>
        public System.DateTime CreatedOnDate
        {
            get
            {
                return this.createdOnDateField;
            }
            set
            {
                this.createdOnDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CreatedOnDateSpecified
        {
            get
            {
                return this.createdOnDateFieldSpecified;
            }
            set
            {
                this.createdOnDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string ChangedByUser
        {
            get
            {
                return this.changedByUserField;
            }
            set
            {
                this.changedByUserField = value;
            }
        }

        /// <remarks/>
        public System.DateTime ChangedOnDate
        {
            get
            {
                return this.changedOnDateField;
            }
            set
            {
                this.changedOnDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ChangedOnDateSpecified
        {
            get
            {
                return this.changedOnDateFieldSpecified;
            }
            set
            {
                this.changedOnDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool ExcludeFromCreditEnquiryYN
        {
            get
            {
                return this.excludeFromCreditEnquiryYNField;
            }
            set
            {
                this.excludeFromCreditEnquiryYNField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ExcludeFromCreditEnquiryYNSpecified
        {
            get
            {
                return this.excludeFromCreditEnquiryYNFieldSpecified;
            }
            set
            {
                this.excludeFromCreditEnquiryYNFieldSpecified = value;
            }
        }
    }
}
