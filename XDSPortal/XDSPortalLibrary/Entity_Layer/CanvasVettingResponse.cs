﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDSPortalLibrary.Entity_Layer
{
  

    public class CanvasVettingResult
    {       
        public CanvasVettingResponse VettingResult { get; set; }
        public AVSResponse AVSResult { get; set; }
        public FMPResponse FMPResult { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public class CanvasVettingResponse
    {
        private string enquiryIDField;
        private string uniqueIDField;
        private string name1Field;
        private string name2Field;
        private string iDentityNumberField;
        private string birthDateField;
        private string capturedByField;
        private string incomeField;
        private string alternateContactNoField;
        private string cellularNoField;
        private string emailAddressField;
        private string referenceNoField;
        private string applicationStatusField;
        private string portfolioField;
        private string dealerIDField;
        private string promoCodeField;
        private string createdOnDateField;
        private string applicationDealerID;
        private string score;
        private string predictedIncome;
        private string predictedAvailableInstalment;
        private string reasonDescription;
        private string applicationPromotionDeal;
        private string reasonCode;
        private string accountNo;
        private string sourceInd;
        private string mTNResponse;
        private string mTNResponseType;
        private string emailStatus;
        private string regionField;
        private string statusField;
        private string errorDescriptionField;
        private string errorCodeField;
        private string finalScoreField;
        private string modelIDField;
        private string cleanResultField;
        private string riskBandField;
        private string creditLimitField;
        private string maxDiaposableIncomeField;
        private string bureauResponseField;
        private string internalReasonField;
        private string externalReasonField;
        private string codeField;
        private string campaignValidityField;
        private string decisionField;
        private string affordabilityOverrideField;
        private string affordabilityOverrideEndDateField;
        private string campaignFilenameField;
        private string campaignNameField;
        private string preApprovedFlag;

        /// <remarks/>
        public string EnquiryID
        {
            get
            {
                return this.enquiryIDField;
            }
            set
            {
                this.enquiryIDField = value;
            }
        }


        /// <remarks/>
        public string UniqueID
        {
            get
            {
                return this.uniqueIDField;
            }
            set
            {
                this.uniqueIDField = value;
            }
        }

        /// <remarks/>
        public string Name1
        {
            get
            {
                return this.name1Field;
            }
            set
            {
                this.name1Field = value;
            }
        }

        /// <remarks/>
        public string Name2
        {
            get
            {
                return this.name2Field;
            }
            set
            {
                this.name2Field = value;
            }
        }

        /// <remarks/>
        public string IdentityNumber
        {
            get
            {
                return this.iDentityNumberField;
            }
            set
            {
                this.iDentityNumberField = value;
            }
        }

        /// <remarks/>
        //[System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        //public System.DateTime? BirthDate
        public string BirthDate
        {
            get
            {
                return this.birthDateField;

                //return BirthDate.HasValue ? BirthDate.Value : new DateTime(9999, 12, 31);
                //return string.IsNullOrEmpty(BirthDate.Value.ToString()) ? new DateTime(9999, 12, 31) : BirthDate.Value;
                //return BirthDate.HasValue ? BirthDate.Value : null;

            }
            set
            {
                this.birthDateField = value;
                //this.birthDateField = string.IsNullOrEmpty(BirthDate.Value.ToString()) ? new DateTime(9999, 12, 31) : value;
            }
        }


        /// <remarks/> 
        public string CellularNo
        {
            get
            {
                return this.cellularNoField;
            }
            set
            {
                this.cellularNoField = value;
            }
        }

        /// <remarks/>
        public string EmailAddress
        {
            get
            {
                return this.emailAddressField;
            }
            set
            {
                this.emailAddressField = value;
            }
        }

        /// <remarks/>
        public string CapturedBy
        {
            get
            {
                return this.capturedByField;
            }
            set
            {
                this.capturedByField = value;
            }
        }

        /// <remarks/>
        public string Income
        {
            get
            {
                return this.incomeField;
            }
            set
            {
                this.incomeField = value;
            }
        }

        /// <remarks/>
        public string AlternateContactNo
        {
            get
            {
                return this.alternateContactNoField;
            }
            set
            {
                this.alternateContactNoField = value;
            }
        }

        /// <remarks/>
        public string ApplicationStatus
        {
            get
            {
                return this.applicationStatusField;
            }
            set
            {
                this.applicationStatusField = value;
            }
        }

        public string Portfolio
        {
            get
            {
                return this.portfolioField; ;
            }
            set
            {
                this.portfolioField = value;
            }
        }

        public string DealerID
        {
            get
            {
                return this.dealerIDField;
            }
            set
            {
                this.dealerIDField = value;
            }
        }

        public string PromoCode
        {
            get
            {
                return this.promoCodeField;
            }
            set
            {
                this.promoCodeField = value;
            }
        }

        public string ReferenceNo
        {
            get
            {
                return this.referenceNoField;
            }
            set
            {
                this.referenceNoField = value;
            }
        }

        public string CreatedOnDate
        {
            get
            {
                return createdOnDateField;
            }

            set
            {
                createdOnDateField = value;
            }
        }

        public string ApplicationDealerID
        {
            get
            {
                return applicationDealerID;
            }

            set
            {
                applicationDealerID = value;
            }
        }

        public string Score
        {
            get
            {
                return score;
            }

            set
            {
                score = value;
            }
        }

        public string PredictedIncome
        {
            get
            {
                return predictedIncome;
            }

            set
            {
                predictedIncome = value;
            }
        }

        public string PredictedAvailableInstalment
        {
            get
            {
                return predictedAvailableInstalment;
            }

            set
            {
                predictedAvailableInstalment = value;
            }
        }

        public string ReasonDescription
        {
            get
            {
                return reasonDescription;
            }

            set
            {
                reasonDescription = value;
            }
        }

        public string ApplicationPromotionDeal
        {
            get
            {
                return applicationPromotionDeal;
            }

            set
            {
                applicationPromotionDeal = value;
            }
        }

        public string ReasonCode
        {
            get
            {
                return reasonCode;
            }

            set
            {
                reasonCode = value;
            }
        }

        public string AccountNo
        {
            get
            {
                return accountNo;
            }

            set
            {
                accountNo = value;
            }
        }

        public string SourceInd
        {
            get
            {
                return sourceInd;
            }

            set
            {
                sourceInd = value;
            }
        }

        public string MTNResponse
        {
            get
            {
                return this.mTNResponse;
            }

            set
            {
                this.mTNResponse = value;
            }
        }

        public string MTNResponseType
        {
            get
            {
                return this.mTNResponseType;
            }

            set
            {
                this.mTNResponseType = value;
            }
        }
        public string EmailStatus
        {
            get
            {
                return emailStatus;
            }

            set
            {
                emailStatus = value;
            }
        }
        /// <remarks/>
        public string Region
        {
            get
            {
                return this.regionField;
            }
            set
            {
                this.regionField = value;
            }
        }

        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        public string ErrorDescription
        {
            get
            {
                return this.errorDescriptionField;
            }
            set
            {
                this.errorDescriptionField = value;
            }
        }
        public string ErrorCode
        {
            get
            {
                return this.errorCodeField;
            }
            set
            {
                this.errorCodeField = value;
            }
        }
        public string FinalScore
        {
            get
            {
                return this.finalScoreField;
            }
            set
            {
                this.finalScoreField = value;
            }
        }
        public string ModelID
        {
            get
            {
                return this.modelIDField;
            }
            set
            {
                this.modelIDField = value;
            }
        }

        public string CleanResult
        {
            get
            {
                return this.cleanResultField;
            }
            set
            {
                this.cleanResultField = value;
            }
        }

        public string RiskBand
        {
            get
            {
                return this.riskBandField;
            }
            set
            {
                this.riskBandField = value;
            }
        }

        public string CreditLimit
        {
            get
            {
                return this.creditLimitField;
            }
            set
            {
                this.creditLimitField = value;
            }
        }

        public string MaxDisposableIncome
        {
            get
            {
                return this.maxDiaposableIncomeField;
            }
            set
            {
                this.maxDiaposableIncomeField = value;
            }
        }

        public string BureauResponse
        {
            get
            {
                return this.bureauResponseField;
            }
            set
            {
                this.bureauResponseField = value;
            }
        }

        public string InternalReason
        {
            get
            {
                return this.internalReasonField;
            }
            set
            {
                this.internalReasonField = value;
            }
        }

        public string ExternalReason
        {
            get
            {
                return this.externalReasonField;
            }
            set
            {
                this.externalReasonField = value;
            }
        }

        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }
        public string CampaignValidity
        {
            get
            {
                return this.campaignValidityField;
            }
            set
            {
                this.campaignValidityField = value;
            }
        }
        public string Decision
        {
            get
            {
                return this.decisionField;
            }
            set
            {
                this.decisionField = value;
            }
        }
        public string AffordabilityOverride
        {
            get
            {
                return this.affordabilityOverrideField;
            }
            set
            {
                this.affordabilityOverrideField = value;
            }
        }
        public string AffordabilityOverrideEndDate
        {
            get
            {
                return this.affordabilityOverrideEndDateField;
            }
            set
            {
                this.affordabilityOverrideEndDateField = value;
            }
        }
        public string CampaignFilename
        {
            get
            {
                return this.campaignFilenameField;
            }
            set
            {
                this.campaignFilenameField = value;
            }
        }
        public string CampaignName
        {
            get
            {
                return this.campaignNameField;
            }
            set
            {
                this.campaignNameField = value;
            }
        }
        public string PreApproved
        {
            get
            {
                return this.preApprovedFlag;
            }
            set
            {
                this.preApprovedFlag = value;
            }
        }

      

    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public class AVSResponse
    {
        private string errorCodeField;
        private string errorDescriptionField;
        private Canvas.AccountVerificationResultResultFile accountVerificationResultField;

        /// <remarks/>
        public string ErrorCode
        {
            get
            {
                return this.errorCodeField;
            }
            set
            {
                this.errorCodeField = value;
            }
        }


        /// <remarks/>
        public string ErrorDescription
        {
            get
            {
                return this.errorDescriptionField;
            }
            set
            {
                this.errorDescriptionField = value;
            }
        }

        /// <remarks/>
        public Canvas.AccountVerificationResultResultFile ResultFile
        {
            get
            {
                return this.accountVerificationResultField;
            }
            set
            {
                this.accountVerificationResultField = value;
            }
        }
    }

    public class FMPResponse
    {
        private string fmpScoreField;
        private string fmpCharactersticsField;
        private string clientMessageField;
        private string[] reasonCodeField;
        private string[] reasonDescriptionField;

        public string FMPScore
        {
            set { this.fmpScoreField = value; }
            get { return this.fmpScoreField; }
        }
        public string FMPCharacterstics
        {
            set { this.fmpCharactersticsField = value; }
            get { return this.fmpCharactersticsField; }
        }
        public string ClientMessage
        {
            set { this.clientMessageField = value; }
            get { return this.clientMessageField; }
        }
        public string[] ReasonCode
        {
            set { this.reasonCodeField = value; }
            get { return this.reasonCodeField; }
        }
        public string[] ReasonDescription
        {
            set { this.reasonDescriptionField = value; }
            get { return this.reasonDescriptionField; }
        }
    }
}

