using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace XDSPortalLibrary.Entity_Layer
{
    public class DirectorCreditEnquiry
    {
        int subscriberIDvalue, productIDvalue, DirectorIDValue = 0, ConsumerIDvalue = 0;
        string IDnovalue = string.Empty, strTmpReference = string.Empty, Surnamevalue = string.Empty, Firstnamevalue = string.Empty, Secondnamevalue = string.Empty, externalreferencevalue = string.Empty, FirstInitialvalue = string.Empty, SecondInitialvalue = string.Empty, ReferenceNoValue = string.Empty;
        DateTime DOBvalue;
        bool ConfirmationChkBoxvalue, IsConsumerMatchingvalue = false;
        DataSet BonusSegmentsDS;
        private bool boolBonusCheck = false;
        private string strdataSegments = string.Empty;
        private string strAssociationCode = string.Empty;
        private bool _SAFPSIndicator = false;
        private bool _DisplayUnusableInformation = false;


        public string SubscriberAssociationCode
        {
            get { return this.strAssociationCode; }
            set { this.strAssociationCode = value; }
        }

        public string DataSegments
        {
            get { return this.strdataSegments; }
            set { this.strdataSegments = value; }
        }

        public bool BonusCheck
        {
            get { return this.boolBonusCheck; }
            set { this.boolBonusCheck = value; }
        }

        public DirectorCreditEnquiry()
        {
        }
        public int DirectorID
        {
            get
            {
                return this.DirectorIDValue;
            }
            set
            {
                this.DirectorIDValue = value;
            }
        }
        public int ConsumerID
        {
            get
            {
                return this.ConsumerIDvalue;
            }
            set
            {
                this.ConsumerIDvalue = value;
            }
        }
        public DataSet BonusSegments
        {
            get
            {
                return this.BonusSegmentsDS;
            }
            set
            {
                this.BonusSegmentsDS = value;
            }
        }
        public string ReferenceNo
        {
            get
            {
                if (String.IsNullOrEmpty(this.ReferenceNoValue))
                    return DBNull.Value.ToString();
                else
                    return this.ReferenceNoValue;
            }
            set
            {
                this.ReferenceNoValue = value;
            }
        }
        public int subscriberID
        {
            get
            {
                if (String.IsNullOrEmpty(this.subscriberIDvalue.ToString()))
                    return Convert.ToInt16(DBNull.Value);
                else
                    return this.subscriberIDvalue;
            }
            set
            {
                this.subscriberIDvalue = value;
            }
        }
        public int ProductID
        {
            get
            {
                if (String.IsNullOrEmpty(this.subscriberIDvalue.ToString()))
                    return Convert.ToInt16(DBNull.Value);
                else
                    return this.productIDvalue;
            }
            set
            {
                this.productIDvalue = value;
            }
        }
        public string IDno
        {
            get
            {
                if (String.IsNullOrEmpty(IDnovalue))
                    return DBNull.Value.ToString();
                else
                    return this.IDnovalue;
            }
            set
            {
                this.IDnovalue = value;
            }
        }
        public string Surname
        {
            get
            {
                if (String.IsNullOrEmpty(Surnamevalue))
                    return DBNull.Value.ToString();
                else
                    return this.Surnamevalue;
            }
            set
            {
                this.Surnamevalue = value;
            }
        }
        public string Firstname
        {
            get
            {
                if (String.IsNullOrEmpty(Firstnamevalue))
                    return DBNull.Value.ToString();
                else
                    return this.Firstnamevalue;
            }
            set
            {
                this.Firstnamevalue = value;
            }
        }
        public string SecondName
        {
            get
            {
                if (String.IsNullOrEmpty(Secondnamevalue))
                    return DBNull.Value.ToString();
                else
                    return this.Secondnamevalue;
            }
            set
            {
                this.Secondnamevalue = value;
            }
        }
        public DateTime DOB
        {
            get
            {
                if (this.DOBvalue == null)
                    return Convert.ToDateTime(DBNull.Value);
                else
                    return this.DOBvalue;
            }
            set
            {
                this.DOBvalue = value;
            }
        }
        public string ExternalReference
        {
            get
            {
                if (String.IsNullOrEmpty(externalreferencevalue))
                    return DBNull.Value.ToString();
                else
                    return this.externalreferencevalue;
            }
            set
            {
                this.externalreferencevalue = value;
            }
        }
        public string FirstInitial
        {
            get
            {
                if (String.IsNullOrEmpty(FirstInitialvalue))
                    return DBNull.Value.ToString();
                else
                    return this.FirstInitialvalue;
            }
            set
            {
                this.FirstInitialvalue = value;
            }
        }
        public string SecondInitial
        {
            get
            {
                if (String.IsNullOrEmpty(SecondInitialvalue))
                    return DBNull.Value.ToString();
                else
                    return this.SecondInitialvalue;
            }
            set
            {
                this.SecondInitialvalue = value;
            }
        }
        public bool ConfirmationChkBox
        {
            get
            {
                return this.ConfirmationChkBoxvalue;
            }
            set
            {
                this.ConfirmationChkBoxvalue = value;
            }
        }
        public bool IsConsumerMatching
        {
            get
            {
                return this.IsConsumerMatchingvalue;
            }
            set
            {
                this.IsConsumerMatchingvalue = value;
            }
        }
        public string TmpReference
        {
            get
            {
                if (String.IsNullOrEmpty(this.strTmpReference))
                    return DBNull.Value.ToString();
                else
                    return this.strTmpReference;
            }
            set
            {
                this.strTmpReference = value;
            }
        }

        public bool SAFPSIndicator
        {
            get { return this._SAFPSIndicator; }
            set { this._SAFPSIndicator = value; }
        }

        public bool DisplayUnusableInformation
        {
            get { return this._DisplayUnusableInformation; }
            set { this._DisplayUnusableInformation = value; }
        }
    }
}
