﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace XDSPortalLibrary.Entity_Layer
{
    public class BlockedConsumers
    {
        public BlockedConsumers()
        {

        }

        private int iBlockID = 0, iConsumerID = 0, iSystemUserID = 0, iSubscriberID = 0, iSubscriberEnquiryID = 0, iSubscriberEnquiryResultID = 0, iProductID = 0;
        private bool bIsBlocked = false;
        private string sBlockedByUser = string.Empty, sBlockedOnDate = string.Empty, sUnblockedByUser = string.Empty, sUnblockedOnDate = string.Empty;
        private string sIDNo = string.Empty, sPassportNo = string.Empty, sFirstname = string.Empty, sSecondname = string.Empty, sSurname = string.Empty;
        private DateTime sDateofBirth = DateTime.Parse("1900/01/01");
        private string sBlockingReason = string.Empty;
        private string sUnblockingReason = string.Empty;
        private string _SubscriberName = string.Empty;

        public int BlockID
        {
            get { return this.iBlockID; }
            set { this.iBlockID = value; }
        }

        public int ConsumerID
        {
            get { return this.iConsumerID; }
            set { this.iConsumerID = value; }
        }

        public int SystemUserID
        {
            get { return this.iSystemUserID; }
            set { this.iSystemUserID = value; }
        }

        public int SubscriberID
        {
            get { return this.iSubscriberID; }
            set { this.iSubscriberID = value; }
        }

        public int SubscriberEnquiryID
        {
            get { return this.iSubscriberEnquiryID; }
            set { this.iSubscriberEnquiryID = value; }
        }

        public int SubscriberEnquiryResultID
        {
            get { return this.iSubscriberEnquiryResultID; }
            set { this.iSubscriberEnquiryResultID = value; }
        }

        public int ProductID
        {
            get { return this.iProductID; }
            set { this.iProductID = value; }
        }

        public bool IsBlocked
        {
            get { return this.bIsBlocked; }
            set { this.bIsBlocked = value; }
        }

        public string BlockedByUser
        {
            get { return this.sBlockedByUser; }
            set { this.sBlockedByUser = value; }
        }

        public string BlockedOnDate        
        {
            get { return this.sBlockedOnDate; }
            set { this.sBlockedOnDate = value; }
        }

        public string UnblockedByUser
        {
            get { return this.sUnblockedByUser; }
            set { this.sUnblockedByUser = value; }
        }

        public string UnblockedOnDate
        {
            get { return this.sUnblockedOnDate; }
            set { this.sUnblockedOnDate = value; }
        }

        public string IDNo
        {
            get { return this.sIDNo; }
            set { this.sIDNo = value; }
        }

        public string PassportNo
        {
            get { return this.sPassportNo; }
            set { this.sPassportNo = value; }
        }

        public string FirstName
        {
            get { return this.sFirstname; }
            set { this.sFirstname = value; }
        }

        public string SecondName
        {
            get { return this.sSecondname; }
            set { this.sSecondname = value; }
        }

        public string SurName
        {
            get { return this.sSurname; }
            set { this.sSurname = value; }
        }

        public DateTime DateofBirth
        {
            get { return this.sDateofBirth; }
            set { this.sDateofBirth = value; }
        }
        public string BlockingReason
        {
            get { return this.sBlockingReason; }
            set { this.sBlockingReason = value; }
        }
        public string SubscriberName
        {
            get { return this._SubscriberName; }
            set { this._SubscriberName = value; }
        }

        //V2.2
        public string UnblockingReason
        {
            get { return this.sBlockingReason; }
            set { this.sBlockingReason = value; }
        }
    }
}
