﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDSPortalEnquiry.Entity
{
    public class DIAVerifyFaces
    {   
        public int SubscriberEnquiryID { get; set; }
        public int SubscriberEnquiryResultID { get; set; }
        public int BioMetricID { get; set; }
        public string Age1 { get; set; }
        public string Age2 { get; set; }
        public string DiaReference { get; set; }
        public string Gender1 { get; set; }
        public string Gender2 { get; set; }
        public string Quality1 { get; set; }
        public string Quality2 { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseMsg { get; set; }
        public string Score { get; set; }
        public string Sharpness1 { get; set; }
        public string Sharpness2 { get; set; }
        public string URL { get; set; }
        public string CreatedbyUser { get; set; }
    }
}
