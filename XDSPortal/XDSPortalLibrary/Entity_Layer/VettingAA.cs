﻿using System;
using System.Collections.Generic;
using static DevExpress.Xpo.DB.DataStoreLongrunnersWatch;

namespace XDSPortalLibrary.Entity_Layer.VettingAA
{
    public enum eCustomerType { Consumer, Business }
    public enum eIdType { Said, Passport }
    public class VettingAARequest
    {
        public VettingAARequest() { }
        public string Username { get; set; }
        public string Password { get; set; }
        public DateTime Transactiondatetime { get; set; }
        public long Transactionnumber { get; set; }
        public eCustomerType Customertype { get; set; }
        public bool Existingcustomer { get; set; }
        public string Firstname { get; set; }
        public string Surname { get; set; }
        public eIdType Idtype { get; set; }
        public string Idorpassportnumber { get; set; }
        public DateTime Dateofbirth { get; set; }
        public double Grossmonthlyincome { get; set; }
        public int Numberofservicesappliedfor { get; set; }

        public VettingAARequest(VettingAARequest iRequest)
        {
            Username = iRequest.Username;
            Password = iRequest.Password;
            Transactiondatetime = iRequest.Transactiondatetime;
            Transactionnumber = iRequest.Transactionnumber;
            Customertype = iRequest.Customertype;
            Existingcustomer = iRequest.Existingcustomer;
            Firstname = iRequest.Firstname;
            Surname = iRequest.Surname;
            Idtype = iRequest.Idtype;
            Idorpassportnumber = iRequest.Idorpassportnumber;
            Dateofbirth = iRequest.Dateofbirth;
            Grossmonthlyincome = iRequest.Grossmonthlyincome;
            Numberofservicesappliedfor = iRequest.Numberofservicesappliedfor;
        }

        public List<string> Validate()
        {
            List<string> validationMessage = new List<string>();



            if (Username == null)
            {
                //validationMessage.Add("Username is mandatory");
                if (!validationMessage.Contains("All mandatory fields should be populated"))
                { validationMessage.Add("All mandatory fields should be populated"); }

            }


            if (Password == null)
            {
                //validationMessage.Add("Password is mandatory");
                if (!validationMessage.Contains("All mandatory fields should be populated"))
                {
                    validationMessage.Add("All mandatory fields should be populated");
                }

            }

            if (Customertype != eCustomerType.Consumer && Customertype != eCustomerType.Business)
            {
                validationMessage.Add("CustomerType can be either Consumer or Business");
            }

            if (string.IsNullOrEmpty(Firstname))
            { //validationMessage.Add("Firstname is mandatory");
                if (!validationMessage.Contains("All mandatory fields should be populated"))
                { validationMessage.Add("All mandatory fields should be populated"); }
            }


            if (string.IsNullOrEmpty(Surname))
            { //validationMessage.Add("Surname is mandatory");
                if (!validationMessage.Contains("All mandatory fields should be populated"))
                { validationMessage.Add("All mandatory fields should be populated"); }
            }
            if (string.IsNullOrEmpty(Idorpassportnumber))
            { //validationMessage.Add("Idnumber is mandatory");
                if (!validationMessage.Contains("All mandatory fields should be populated"))
                { validationMessage.Add("All mandatory fields should be populated"); }
            }
                if (Idtype == eIdType.Said && !string.IsNullOrEmpty(Idorpassportnumber) && Idorpassportnumber.Length < 13)
            {
                validationMessage.Add("Invalid idno supplied");
            }
            else if (Idtype == eIdType.Passport && !string.IsNullOrEmpty(Idorpassportnumber) && Idorpassportnumber.Length > 16)
            {
                validationMessage.Add("Invalid passportno supplied");
            }

            if (Grossmonthlyincome <= 0)
            {
                validationMessage.Add("Invalid grossmonthlyincome supplied");
            }

            return validationMessage;
        }

    }

    public class VettingAAResponse
    {
        public VettingAAResponse() { }

        public string Status { get; set; }
        public string ErrorDescription { get; set; }
        public DateTime Agilitytransactiondatetime { get; set; }
        public long Agilitytransactionnumber { get; set; }
        public DateTime XDStransactiondatetime { get; set; }
        public string XDStransactionnumber { get; set; }
        public Int64 XDSconsumernumber { get; set; }
        public eCustomerType AgilityCustomertype { get; set; }
        public bool AgilityExistingcustomer { get; set; }
        public int Numberofservicesrequested { get; set; }
        public eIdType IDtype { get; set; }
        public string IDorpassportnumber { get; set; }
        public DateTime Dateofbirth { get; set; }
        public string Scorecardname { get; set; }
        public string Recommendeddecision { get; set; }
        public int RiskGrade { get; set; } = 1;
        public double Bureauscore { get; set; }
        public double Applicationscore { get; set; }
        public double Creditlimit { get; set; }
        public int Maximumcontractperiod { get; set; }
        public double Maximumdealvalue { get; set; }
        public double Payinvalue { get; set; } = 0;
        public int Maximumnumberofservicesallowed { get; set; }
        public double Estimatedmonthlyincome { get; set; }
        public List<PolicyRules> Policyrulestriggered { get; set; }
        public bool BureauMatched { get; set; } = false;
        public bool IDissued { get; set; } = false;
        public bool IDverified { get; set; } = false;
        public bool Deceased { get; set; } = false;
        public bool FraudDatabaseMatch { get; set; } = false;
        public bool UnderDebtReview { get; set; } = false;
        public DateTime Debtreviewdate { get; set; }
        public string Debtreviewstatus { get; set; }
        public int Numberofcourtnotices { get; set; }
        public DateTime Dateofmostrecentcourtnotice { get; set; }
        public double Valueofcourtnotices { get; set; }
        public int Numberofjudgements { get; set; }
        public double TotalJudgementvalue { get; set; }
        public DateTime Dateofmostrecentjudgement { get; set; }
        public double Valueofmostrecentjudgement { get; set; }
        public int Numberofadverses { get; set; }
        public double Totaladversevalue { get; set; }
        public DateTime Dateofmostrecentadverse { get; set; }
        public double Valueofmostrecentadverse { get; set; }
        public int Totalnumberofactiveaccounts { get; set; }
        public int Numberofaccountsinarrears { get; set; }
        public int Numberofpaidupaccounts { get; set; }
        public int Numberofclosedaccounts { get; set; }
        public int Numberofaccountsopenedinlast45days { get; set; }
        public int Numberofaccountswithanegativestatuscode { get; set; }
        public int Numberofaccounts3plusinarrears { get; set; }
        public int Numberofaccounts3plusinarrearsinpast60days { get; set; }
        public int Numberofaccounts3plusinarrearsinpast90days { get; set; }
        public int Numberofaccounts3plusinarrearsinpast120days { get; set; }
        public int Worstarrearsinlast3months { get; set; }
        public int Worstarrearsinlast6months { get; set; }
        public int Worstarrearsinlast12months { get; set; }
        public int Worstarrearsinlast24months { get; set; }
        public int Totalownaccountsopen { get; set; }
        public int Totalownaccountspaid { get; set; }
        public int Totalownaccountsclosed { get; set; }
        public int Totalownaccountsinarrears { get; set; }
        public int Totalownaccountswithnegativestatuscode { get; set; }
        public int TotalactiveNLR { get; set; }
        public int ToalNLRaccountsinarrears { get; set; }
        public int TotalNLRaccountspaidup { get; set; }
        public int TotalNLRaccountsclosed { get; set; }
        public int TotalNLRaccountsopeninginlast45days { get; set; }
        public double Totalmonthlyinstalments { get; set; }
        public double Totaloutstandingdebt { get; set; }
        public double Totalarrearsamount { get; set; }
        public int Totalenquiriesinlast90days { get; set; }
        public int Totalownenquiriesinlast90days { get; set; }
        public int Totalenquiriesinlast30days { get; set; }
        public int Totalownenquiriesinlast30days { get; set; }
        public int Numberofpropertyinterests { get; set; }
        public double Valueofpropertyinterests { get; set; }
        public int Numberofprincipallinks { get; set; }
        public int Numberofprincipallinksingoodstanding { get; set; }

        public void GetErrorResponse(string message)
        {
            Status = "Error";
            ErrorDescription = message ;

        }
    }

    public class PolicyRules
    {
        public string Rulenumber { get; set; }
        public string Decision { get; set; }
    }
}