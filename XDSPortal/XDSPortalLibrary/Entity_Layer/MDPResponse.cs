﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XDSPortalLibrary.Entity_Layer
{
    public class MDPResponse
    {
        public ResponseValues response { get; set; }
    }

    public class ResponseValues
    {
        public string EnquiryId { get; set; }
        public string Message { get; set; }
        public string ProductTransactionId { get; set; }
    }
}
