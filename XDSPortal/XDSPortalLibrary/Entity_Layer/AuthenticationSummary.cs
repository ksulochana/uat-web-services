﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace XDSPortalLibrary.Entity_Layer
{
    public class AuthenticationSummary
    {
        public AuthenticationSummary()
        {
        }
         public string IDNo = string.Empty;
         public string PassportNo = string.Empty;
         public string Firstname = string.Empty;
         public string Surname = string.Empty;
         public DateTime Birthdate = DateTime.Parse("1900/01/01");
         public string Gender = string.Empty;
         public int EnquiryID = 0;
         public int EnquiryResultID = 0;
         public string Reference = string.Empty;
         public string Ha_IDno = string.Empty;
         public string Ha_Firstname = string.Empty;
         public string Ha_Surname = string.Empty;
         public DateTime Ha_birthdate = DateTime.Parse("1900/01/01");
         public string Ha_DeceasedStatus = string.Empty;
         public string creditUsage = string.Empty;
         public string AuthenticationUsage = string.Empty;
         public string AccountVerifications = string.Empty;
         public string Identityverification = string.Empty;
         public string SAFPS = string.Empty;
         public string Closedusergroup = string.Empty;
         public string IsBlocked = string.Empty;
         public string AuthenticationMessage = string.Empty;
         public string AccountageMessage = string.Empty;
         public string ErrorMessage = string.Empty;
         public bool IsSuccess = true;

         [XmlArray("AuthenticationDetails")]
         [XmlArrayItem("PreviousAuthentication")]
         public PreviousAuthentication[] AuthenticationDetails;
    }

    public class PreviousAuthentication
    {
        //public PreviousAuthentication()
        //{
        //}

        public int SubscriberAuthenticationID = 0;
        public string SubscriberAuthenticationDate = string.Empty;
        public string SubscriberAuthenticationTime = string.Empty;
        public string Location = string.Empty;
        public string Status = string.Empty;
    }


}
