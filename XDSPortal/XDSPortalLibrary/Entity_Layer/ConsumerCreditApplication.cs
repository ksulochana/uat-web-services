using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace XDSPortalLibrary.Entity_Layer
{
   public class ConsumerCreditApplication
    {
       int subscriberIDvalue, productIDvalue, ConsumerIDValue = 0;
       string EnquiryReasonvalue = string.Empty, strTmpReference = string.Empty, ExternalReferencevalue = string.Empty, IDnovalue = string.Empty, passportNovalue = string.Empty, SurNamevalue = string.Empty, MaidenNamevalue = string.Empty, FirstNamevalue = string.Empty, SecondNamevalue = string.Empty, Gendervalue = string.Empty, MaritalStatusvalue = string.Empty, SpouseFirstnamevalue = string.Empty, SpouseSurNamevalue = string.Empty, ResAddress1value = string.Empty, ResAddress2value = string.Empty, ResAddress3value = string.Empty, ResAddress4value = string.Empty, Respostalcodevalue = string.Empty, PostalAddress1value = string.Empty, PostalAddress2value = string.Empty, PostalAddress3value = string.Empty, PostalAddress4value = string.Empty, Postalpostalcodevalue = string.Empty, HomeTelephoneCodevalue = string.Empty, HomeTelePhoneNovalue = string.Empty, WorkTelephoneCodevalue = string.Empty, workTelephoneNovalue = string.Empty, Cellcodevalue = string.Empty, cellNovalue = string.Empty, Emailaddressvalue = string.Empty, TotalMonthlyIncomevalue = string.Empty, EmployerDetailvalue = string.Empty, OccupationTitlevalue = string.Empty, ReferenceNoValue = string.Empty;
        DateTime DOBvalue;
        bool ConfirmationChkBoxvalue, IsConsumerMatchingvalue=false;
        DataSet BonusSegmentsDS;
        private bool boolBonusCheck = false;
        private string strdataSegments = string.Empty;
        private string strAssociationCode = string.Empty;

        private bool _SAFPSIndicator = false;

        public string SubscriberAssociationCode
        {
            get { return this.strAssociationCode; }
            set { this.strAssociationCode = value; }
        }

        public string DataSegments
        {
            get { return this.strdataSegments; }
            set { this.strdataSegments = value; }
        }

        public bool BonusCheck
        {
            get { return this.boolBonusCheck; }
            set { this.boolBonusCheck = value; }
        }

        public ConsumerCreditApplication()
        {
        }

        public string ExternalReference
        {
            get
            {
                if (String.IsNullOrEmpty(this.ExternalReferencevalue))
                    return DBNull.Value.ToString();
                else
                    return this.ExternalReferencevalue;
            }
            set
            {
                this.ExternalReferencevalue = value;
            }
        }
        public int ConsumerID
        {
            get
            {
                return this.ConsumerIDValue;
            }
            set
            {
                this.ConsumerIDValue = value;
            }
        }
        public DataSet BonusSegments
        {
            get
            {
                return this.BonusSegmentsDS;
            }
            set
            {
                this.BonusSegmentsDS = value;
            }
        }
        public string ReferenceNo
        {
            get
            {
                if (String.IsNullOrEmpty(this.ReferenceNoValue))
                    return DBNull.Value.ToString();
                else
                    return this.ReferenceNoValue;
            }
            set
            {
                this.ReferenceNoValue = value;
            }
        }
        public int subscriberID
        {
            get
            {
                if (String.IsNullOrEmpty(this.subscriberIDvalue.ToString()))
                    return Convert.ToInt16(DBNull.Value);
                else
                    return this.subscriberIDvalue;
            }
            set
            {
                this.subscriberIDvalue = value;
            }
        }
        public int ProductID
        {
            get
            {
                if (String.IsNullOrEmpty(this.subscriberIDvalue.ToString()))
                    return Convert.ToInt16(DBNull.Value);
                else
                    return this.productIDvalue;
            }
            set
            {
                this.productIDvalue = value;
            }
        }
        public string EnquiryReason
        {
            get
            {
                if (String.IsNullOrEmpty(this.EnquiryReasonvalue))
                    return DBNull.Value.ToString();
                else
                    return this.EnquiryReasonvalue;
            }
            set
            {
                this.EnquiryReasonvalue = value;
            }
        }
        public string IDNo
        {
            get
            {
                if (String.IsNullOrEmpty(this.IDnovalue))
                    return DBNull.Value.ToString();
                else
                    return this.IDnovalue;
            }
            set
            {
                this.IDnovalue = value;
            }
        }
        public string PassportNo
        {
            get
            {
                if (String.IsNullOrEmpty(this.passportNovalue))
                    return DBNull.Value.ToString();
                else
                    return this.passportNovalue;
            }
            set
            {
                this.passportNovalue = value;
            }
        }
        public string SurName
        {
            get
            {
                if (String.IsNullOrEmpty(this.SurNamevalue))
                    return DBNull.Value.ToString();
                else
                    return this.SurNamevalue;
            }
            set
            {
                this.SurNamevalue = value;
            }
        }
        public string MaidenName
        {
            get
            {
                if (String.IsNullOrEmpty(this.MaidenNamevalue))
                    return DBNull.Value.ToString();
                else
                    return this.MaidenNamevalue;
            }
            set
            {
                this.MaidenNamevalue = value;
            }
        }
        public string FirstName
        {
            get
            {
                if (String.IsNullOrEmpty(this.FirstNamevalue))
                    return DBNull.Value.ToString();
                else
                    return this.FirstNamevalue;
            }
            set
            {
                this.FirstNamevalue = value;
            }
        }
        public string SecondName
        {
            get
            {
                if (String.IsNullOrEmpty(this.SecondNamevalue))
                    return DBNull.Value.ToString();
                else
                    return this.SecondNamevalue;
            }
            set
            {
                this.SecondNamevalue = value;
            }
        }
        public DateTime DOB
        {
            get
            {
                if (this.DOBvalue == null)
                    return Convert.ToDateTime(DBNull.Value);
                else
                    return this.DOBvalue;
            }
            set
            {
                this.DOBvalue = value;
            }
        }
        public string Gender
        {
            get
            {
                if (String.IsNullOrEmpty(this.Gendervalue))
                    return DBNull.Value.ToString();
                else
                    return this.Gendervalue;
            }
            set
            {
                this.Gendervalue = value;
            }
        }
        public string MaritalStatus
        {
            get
            {
                if (String.IsNullOrEmpty(this.MaritalStatusvalue))
                    return DBNull.Value.ToString();
                else
                    return this.MaritalStatusvalue;
            }
            set
            {
                this.MaritalStatusvalue = value;
            }
        }
        public string SposeFirstName
        {
            get
            {
                if (String.IsNullOrEmpty(this.SpouseFirstnamevalue))
                    return DBNull.Value.ToString();
                else
                    return this.SpouseFirstnamevalue;
            }
            set
            {
                this.SpouseFirstnamevalue = value;
            }
        }
        public string SpouseSurName
        {
            get
            {
                if (String.IsNullOrEmpty(this.SpouseSurNamevalue))
                    return DBNull.Value.ToString();
                else
                    return this.SpouseSurNamevalue;
            }
            set
            {
                this.SpouseSurNamevalue = value;
            }
        }
        public string ResAddress1
        {
            get
            {
                if (String.IsNullOrEmpty(this.ResAddress1value))
                    return DBNull.Value.ToString();
                else
                    return this.ResAddress1value;
            }
            set
            {
                this.ResAddress1value = value;
            }
        }
        public string ResAddress2
        {
            get
            {
                if (String.IsNullOrEmpty(this.ResAddress2value))
                    return DBNull.Value.ToString();
                else
                    return this.ResAddress2value;
            }
            set
            {
                this.ResAddress2value = value;
            }
        }
        public string ResAddress3
        {
            get
            {
                if (String.IsNullOrEmpty(this.ResAddress3value))
                    return DBNull.Value.ToString();
                else
                    return this.ResAddress3value;
            }
            set
            {
                this.ResAddress3value = value;
            }
        }
        public string ResAddress4
        {
            get
            {
                if (String.IsNullOrEmpty(this.ResAddress4value))
                    return DBNull.Value.ToString();
                else
                    return this.ResAddress4value;
            }
            set
            {
                this.ResAddress4value = value;
            }
        }
        public string ResPostalcode
        {
            get
            {
                if (String.IsNullOrEmpty(this.Respostalcodevalue))
                    return DBNull.Value.ToString();
                else
                    return this.Respostalcodevalue;
            }
            set
            {
                this.Respostalcodevalue = value;
            }
        }
        public string PostalAddress1
        {
            get
            {
                if (String.IsNullOrEmpty(this.PostalAddress1value))
                    return DBNull.Value.ToString();
                else
                    return this.PostalAddress1value;
            }
            set
            {
                this.PostalAddress1value = value;
            }
        }
        public string PostalAddress2
        {
            get
            {
                if (String.IsNullOrEmpty(this.PostalAddress2value))
                    return DBNull.Value.ToString();
                else
                    return this.PostalAddress2value;
            }
            set
            {
                this.PostalAddress2value = value;
            }
        }
        public string PostalAddress3
        {
            get
            {
                if (String.IsNullOrEmpty(this.PostalAddress3value))
                    return DBNull.Value.ToString();
                else
                    return this.PostalAddress3value;
            }
            set
            {
                this.PostalAddress3value = value;
            }
        }
        public string PostalAddress4
        {
            get
            {
                if (String.IsNullOrEmpty(this.PostalAddress4value))
                    return DBNull.Value.ToString();
                else
                    return this.PostalAddress4value;
            }
            set
            {
                this.PostalAddress4value = value;
            }
        }
        public string PostalPostCode
        {
            get
            {
                if (String.IsNullOrEmpty(this.Postalpostalcodevalue))
                    return DBNull.Value.ToString();
                else
                    return this.Postalpostalcodevalue;
            }
            set
            {
                this.Postalpostalcodevalue = value;
            }
        }
        public string HomeTelePhoneCode
        {
            get
            {
                if (String.IsNullOrEmpty(this.HomeTelephoneCodevalue))
                    return DBNull.Value.ToString();
                else
                    return this.HomeTelephoneCodevalue;
            }
            set
            {
                this.HomeTelephoneCodevalue = value;
            }
        }
        public string HomeTelephoneNo
        {
            get
            {
                if (String.IsNullOrEmpty(this.HomeTelePhoneNovalue))
                    return DBNull.Value.ToString();
                else
                    return this.HomeTelePhoneNovalue;
            }
            set
            {
                this.HomeTelePhoneNovalue = value;
            }
        }
        public string WorkTelephoneCode
        {
            get
            {
                if (String.IsNullOrEmpty(this.WorkTelephoneCodevalue))
                    return DBNull.Value.ToString();
                else
                    return this.WorkTelephoneCodevalue;
            }
            set
            {
                this.WorkTelephoneCodevalue = value;
            }
        }
        public string WorkTelephoneNo
        {
            get
            {
                if (String.IsNullOrEmpty(this.workTelephoneNovalue))
                    return DBNull.Value.ToString();
                else
                    return this.workTelephoneNovalue;
            }
            set
            {
                this.workTelephoneNovalue = value;
            }
        }
        public string CellCode
        {
            get
            {
                if (String.IsNullOrEmpty(this.Cellcodevalue))
                    return DBNull.Value.ToString();
                else
                    return this.Cellcodevalue;
            }
            set
            {
                this.Cellcodevalue = value;
            }
        }
        public string CellNo
        {
            get
            {
                if (String.IsNullOrEmpty(this.cellNovalue))
                    return DBNull.Value.ToString();
                else
                    return this.cellNovalue;
            }
            set
            {
                this.cellNovalue = value;
            }
        }
        public string EmailAddress
        {
            get
            {
                if (String.IsNullOrEmpty(this.Emailaddressvalue))
                    return DBNull.Value.ToString();
                else
                    return this.Emailaddressvalue;
            }
            set
            {
                this.Emailaddressvalue = value;
            }
        }
        public string MonthlyIncome
        {
            get
            {
                if (String.IsNullOrEmpty(this.TotalMonthlyIncomevalue))
                    return DBNull.Value.ToString();
                else
                    return this.TotalMonthlyIncomevalue;
            }
            set
            {
                this.TotalMonthlyIncomevalue = value;
            }

        }
        public string EmployerDetail
        {
            get
            {
                if (String.IsNullOrEmpty(this.EmployerDetailvalue))
                    return DBNull.Value.ToString();
                else
                    return this.EmployerDetailvalue;
            }
            set
            {
                this.EmployerDetailvalue = value;
            }
        }

        public string OccupationTitle
        {
            get
            {
                if (String.IsNullOrEmpty(this.OccupationTitlevalue))
                    return DBNull.Value.ToString();
                else
                    return this.OccupationTitlevalue;
            }
            set
            {
                this.OccupationTitlevalue = value;
            }
        }
        public bool ConfirmationChkBox
        {
            get
            {
                return this.ConfirmationChkBoxvalue;
            }
            set
            {
                this.ConfirmationChkBoxvalue = value;
            }
        }
        public bool IsConsumerMatching
        {
            get
            {
                return this.IsConsumerMatchingvalue;
            }
            set
            {
                this.IsConsumerMatchingvalue = value;
            }
        }
        public string TmpReference
        {
            get
            {
                if (String.IsNullOrEmpty(this.strTmpReference))
                    return DBNull.Value.ToString();
                else
                    return this.strTmpReference;
            }
            set
            {
                this.strTmpReference = value;
            }
        }
        public bool SAFPSIndicator
        {
            get { return this._SAFPSIndicator; }
            set { this._SAFPSIndicator = value; }
        }
    }
}
