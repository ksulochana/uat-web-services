﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace XDSPortalLibrary.Entity_Layer
{
    public class Business
    {

        public enum BusinessSearchType { NAME, REGNO, /*ITNO, DUNSNO,*/ VATNO/*, BANKACCOUNTNO, TRADINGNO*/ };
        public enum BusinessResponseStatus { SUCCESS, FAILURE };

    }

    /// <remarks/>
    public partial class BusinessSearch
    {

        private BusinessSearchBusinessSearch businessSearchField;
        private BusinessSearchBusinessSearchNR businessSearchFieldNR;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("BusinessSearch")]
        public BusinessSearchBusinessSearch BusinessSearch1
        {
            get
            {
                return this.businessSearchField;
            }
            set
            {
                this.businessSearchField = value;
            }
        }

        [System.Xml.Serialization.XmlElementAttribute("BusinessSearchNR")]
        public BusinessSearchBusinessSearchNR BusinessSearchNR
        {
            get
            {
                return this.businessSearchFieldNR;
            }
            set
            {
                this.businessSearchFieldNR = value;
            }
        }
    }

    public partial class BusinessSearchBusinessSearchNR
    {
        private string searchTypeField;

        private string subjectNameField;

        private string registrationNoField;

        private string iTNumberField;

        private string dunsNumberField;

        private string vatNumberField;

        private string bankAccountNumberField;

        private string tradingNumberField;

        private string userField;

        private string passwordField;

        private string clientReferenceField;

        private string DesignationField;
        private string GrossMonthlyIncomeField;
        private string SurnameField;
        private string FirstNameField;
        private string IDTypeField;
        private string BirthDateField;
        private string IdentityNumberField;
        private string GenderField;
        private string MaritalStatusField;
        private string AddressLine1Field;
        private string AddressLine2Field;
        private string SuburbField;
        private string CityField;
        private string PostalCodeField;
        private string HomeTelephoneCodeField;
        private string HomeTelephoneNumberField;
        private string WorkTelephoneCodeField;
        private string WorkTelephoneNumberField;
        private string CellNumberField;
        private string OccupationField;
        private string EmployerField;
        private string NumberOfYearsAtEmployerField;
        private string BankNameField;
        private string BankAccountTypeField;
        private string BankBranchCodeField;
        private string BankAccountNumberPrincipalField;
        private string HighestQualificationField;

        public string Designation
        {
            get { return DesignationField; }
            set { DesignationField = value; }
        }

        public string GrossMonthlyIncome
        {
            get { return GrossMonthlyIncomeField; }
            set { GrossMonthlyIncomeField = value; }
        }

        public string Surname
        {
            get { return SurnameField; }
            set { SurnameField = value; }
        }
        public string FirstName
        {
            get { return FirstNameField; }
            set { FirstNameField = value; }
        }
        public string IDType
        {
            get { return IDTypeField; }
            set { IDTypeField = value; }
        }
        public string BirthDate
        {
            get { return BirthDateField; }
            set { BirthDateField = value; }
        }
        public string IdentityNumber
        {
            get { return IdentityNumberField; }
            set { IdentityNumberField = value; }
        }
        public string Gender
        {
            get { return GenderField; }
            set { GenderField = value; }
        }
        public string MaritalStatus
        {
            get { return MaritalStatusField; }
            set { MaritalStatusField = value; }
        }
        public string AddressLine1
        {
            get { return AddressLine1Field; }
            set { AddressLine1Field = value; }
        }
        public string AddressLine2
        {
            get { return AddressLine2Field; }
            set { AddressLine2Field = value; }
        }
        public string Suburb
        {
            get { return SuburbField; }
            set { SuburbField = value; }
        }
        public string City
        {
            get { return CityField; }
            set { CityField = value; }
        }
        public string PostalCode
        {
            get { return PostalCodeField; }
            set { PostalCodeField = value; }
        }
        public string HomeTelephoneCode
        {
            get { return HomeTelephoneCodeField; }
            set { HomeTelephoneCodeField = value; }
        }
        public string HomeTelephoneNumber
        {
            get { return HomeTelephoneNumberField; }
            set { HomeTelephoneNumberField = value; }
        }
        public string WorkTelephoneCode
        {
            get { return WorkTelephoneCodeField; }
            set { WorkTelephoneCodeField = value; }
        }
        public string WorkTelephoneNumber
        {
            get { return WorkTelephoneNumberField; }
            set { WorkTelephoneNumberField = value; }
        }
        public string CellNumber
        {
            get { return CellNumberField; }
            set { CellNumberField = value; }
        }
        public string Occupation
        {
            get { return OccupationField; }
            set { OccupationField = value; }
        }
        public string Employer
        {
            get { return EmployerField; }
            set { EmployerField = value; }
        }
        public string NumberOfYearsAtEmployer
        {
            get { return NumberOfYearsAtEmployerField; }
            set { NumberOfYearsAtEmployerField = value; }
        }
        public string BankName
        {
            get { return BankNameField; }
            set { BankNameField = value; }
        }
        public string BankAccountType
        {
            get { return BankAccountTypeField; }
            set { BankAccountTypeField = value; }
        }
        public string BankBranchCode
        {
            get { return BankBranchCodeField; }
            set { BankBranchCodeField = value; }
        }
        public string BankAccountNumberPrincipal
        {
            get { return BankAccountNumberPrincipalField; }
            set { BankAccountNumberPrincipalField = value; }
        }
        public string HighestQualification
        {
            get { return HighestQualificationField; }
            set { HighestQualificationField = value; }
        }
    }

     /// <remarks/>
    public partial class BusinessSearchBusinessSearch
    {

        private string searchTypeField;

        private string subjectNameField;

        private string registrationNoField;

        private string iTNumberField;

        private string dunsNumberField;

        private string vatNumberField;

        private string bankAccountNumberField;

        private string tradingNumberField;

        private string userField;

        private string passwordField;

        private string clientReferenceField;

        /// <remarks/>
        public string SearchType
        {
            get
            {
                return this.searchTypeField;
            }
            set
            {
                this.searchTypeField = value;
            }
        }

        /// <remarks/>
        public string SubjectName
        {
            get
            {
                return this.subjectNameField;
            }
            set
            {
                this.subjectNameField = value;
            }
        }

        /// <remarks/>
        public string RegistrationNo
        {
            get
            {
                return this.registrationNoField;
            }
            set
            {
                this.registrationNoField = value;
            }
        }

        /// <remarks/>
        public string ITNumber
        {
            get
            {
                return this.iTNumberField;
            }
            set
            {
                this.iTNumberField = value;
            }
        }

        /// <remarks/>
        public string DunsNumber
        {
            get
            {
                return this.dunsNumberField;
            }
            set
            {
                this.dunsNumberField = value;
            }
        }

        /// <remarks/>
        public string VatNumber
        {
            get
            {
                return this.vatNumberField;
            }
            set
            {
                this.vatNumberField = value;
            }
        }

        /// <remarks/>
        public string BankAccountNumber
        {
            get
            {
                return this.bankAccountNumberField;
            }
            set
            {
                this.bankAccountNumberField = value;
            }
        }

        /// <remarks/>
        public string TradingNumber
        {
            get
            {
                return this.tradingNumberField;
            }
            set
            {
                this.tradingNumberField = value;
            }
        }

        public string User
        {
            get
            {
                return this.userField;
            }
            set
            {
                this.userField = value;
            }
        }

        public string Password
        {
            get
            {
                return this.passwordField;
            }
            set
            {
                this.passwordField = value;
            }
        }

        public string ClientReference
        {
            get
            {
                return this.clientReferenceField;
            }
            set
            {
                this.clientReferenceField = value;
            }
        }

       
    }

     /// <remarks/>
  //  [System.Xml.Serialization.XmlElement("SearchResponse")]
    public partial class BusinessSearchResponse
    {

        private BusinessSearchResponseBusinessSearchResult businessSearchResultField;

        /// <remarks/>
        public BusinessSearchResponseBusinessSearchResult BusinessSearchResult
        {
            get
            {
                return this.businessSearchResultField;
            }
            set
            {
                this.businessSearchResultField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class BusinessSearchResponseBusinessSearchResult
    {
       

        private string rawDataField;

        private XDSPortalLibrary.Entity_Layer.Business.BusinessResponseStatus responseStatusField;

        private DateTime processingStartDateField = DateTime.Now;

        private float processingTimeSecsField;

        private string uniqueRefGuidField;

        private string errorDescriptionField;

        private BusinessSearchResponseBusinessSearchResultSearchResponse searchResponseField;

        /// <remarks/>
        public string RawData
        {
            get
            {
                return this.rawDataField;
            }
            set
            {
                this.rawDataField = value;
            }
        }

        /// <remarks/>
        public XDSPortalLibrary.Entity_Layer.Business.BusinessResponseStatus ResponseStatus
        {
            get
            {
                return this.responseStatusField;
            }
            set
            {
                this.responseStatusField = value;
            }
        }

        /// <remarks/>
        public DateTime ProcessingStartDate
        {
            get
            {
                return this.processingStartDateField;
            }
            set
            {
                this.processingStartDateField = value;
            }
        }

        /// <remarks/>
        public float ProcessingTimeSecs
        {
            get
            {
                return this.processingTimeSecsField;
            }
            set
            {
                this.processingTimeSecsField = value;
            }
        }

        /// <remarks/>
        public string UniqueRefGuid
        {
            get
            {
                return this.uniqueRefGuidField;
            }
            set
            {
                this.uniqueRefGuidField = value;
            }
        }

        public string ErrorDescription
        {
            get
            {
                return this.errorDescriptionField;
            }
            set
            {
                this.errorDescriptionField = value;
            }
        }

        
        /// <remarks/>
        [XmlElement(Namespace = "http://www.web.xds.co.za/XDSConnectWS")]
        public BusinessSearchResponseBusinessSearchResultSearchResponse SearchResponse
        {
            get
            {
                return this.searchResponseField;
            }
            set
            {
                this.searchResponseField = value;
            }
        }
    }

    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class BusinessSearchResponseBusinessSearchResultSearchResponse
    {

        private BusinessSearchResponseBusinessSearchResultSearchResponseSearchResponse[] searchResponseField;
        [System.Xml.Serialization.XmlElement("SearchResponse")]
        /// <remarks/>
        public BusinessSearchResponseBusinessSearchResultSearchResponseSearchResponse[] SearchResponse
        {
            get
            {
                return this.searchResponseField;
            }
            set
            {
                this.searchResponseField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class BusinessSearchResponseBusinessSearchResultSearchResponseSearchResponse
    {

        //private string enquiryIDField;

        //private string enquiryResultIDField;

        private string nameField;

        private string nameTypeField;

        private string businessNameField;

        private string physicalAddressField;

        private string suburbField;

        private string townField;

        private string countryField;

        private string postCodeField;

        private string postalAddressField;

        private string postalSuburbField;

        private string postalTownField;

        private string postalCountryField;

        private string postalPostCodeField;

        private string phoneNoField;

        private string faxNoField;

        private string regNoField;

        private string regStatusField;

        private string regStatusCodeField;

        private string tradingNumberField;

        private string tUNumberField;

        /// <remarks/>
        //public string EnquiryID
        //{
        //    get
        //    {
        //        return this.enquiryIDField;
        //    }
        //    set
        //    {
        //        this.enquiryIDField = value;
        //    }
        //}

        public string TUNumber
        {
            get
            {
                return this.tUNumberField;
            }
            set
            {
                this.tUNumberField = value;
            }
        }

        /// <remarks/>
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public string NameType
        {
            get
            {
                return this.nameTypeField;
            }
            set
            {
                this.nameTypeField = value;
            }
        }

        /// <remarks/>
        public string BusinessName
        {
            get
            {
                return this.businessNameField;
            }
            set
            {
                this.businessNameField = value;
            }
        }

        /// <remarks/>
        public string PhysicalAddress
        {
            get
            {
                return this.physicalAddressField;
            }
            set
            {
                this.physicalAddressField = value;
            }
        }

        /// <remarks/>
        public string Suburb
        {
            get
            {
                return this.suburbField;
            }
            set
            {
                this.suburbField = value;
            }
        }

        /// <remarks/>
        public string Town
        {
            get
            {
                return this.townField;
            }
            set
            {
                this.townField = value;
            }
        }

        /// <remarks/>
        public string Country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        /// <remarks/>
        public string PostCode
        {
            get
            {
                return this.postCodeField;
            }
            set
            {
                this.postCodeField = value;
            }
        }

        /// <remarks/>
        public string PostalAddress
        {
            get
            {
                return this.postalAddressField;
            }
            set
            {
                this.postalAddressField = value;
            }
        }

        /// <remarks/>
        public string PostalSuburb
        {
            get
            {
                return this.postalSuburbField;
            }
            set
            {
                this.postalSuburbField = value;
            }
        }

        /// <remarks/>
        public string PostalTown
        {
            get
            {
                return this.postalTownField;
            }
            set
            {
                this.postalTownField = value;
            }
        }

        /// <remarks/>
        public string PostalCountry
        {
            get
            {
                return this.postalCountryField;
            }
            set
            {
                this.postalCountryField = value;
            }
        }

        /// <remarks/>
        public string PostalPostCode
        {
            get
            {
                return this.postalPostCodeField;
            }
            set
            {
                this.postalPostCodeField = value;
            }
        }

        /// <remarks/>
        public string PhoneNo
        {
            get
            {
                return this.phoneNoField;
            }
            set
            {
                this.phoneNoField = value;
            }
        }

        /// <remarks/>
        public string FaxNo
        {
            get
            {
                return this.faxNoField;
            }
            set
            {
                this.faxNoField = value;
            }
        }

        /// <remarks/>
        public string RegNo
        {
            get
            {
                return this.regNoField;
            }
            set
            {
                this.regNoField = value;
            }
        }

        /// <remarks/>
        public string RegStatus
        {
            get
            {
                return this.regStatusField;
            }
            set
            {
                this.regStatusField = value;
            }
        }

        /// <remarks/>
        public string RegStatusCode
        {
            get
            {
                return this.regStatusCodeField;
            }
            set
            {
                this.regStatusCodeField = value;
            }
        }

        /// <remarks/>
        public string TradingNumber
        {
            get
            {
                return this.tradingNumberField;
            }
            set
            {
                this.tradingNumberField = value;
            }
        }
    }
    
    public partial class ModuleRequest
    {

        private ModuleRequestModuleRequest moduleRequest1Field;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ModuleRequest")]
        public ModuleRequestModuleRequest ModuleRequest1
        {
            get
            {
                return this.moduleRequest1Field;
            }
            set
            {
                this.moduleRequest1Field = value;
            }
        }
    }

    public class CompanyInformationRequest
    {
        private int employeecountField;
        private decimal turnoverField;
        private string tradeindustryField;
        private DirectorInformation[] directorsField;
        

        public int EmployeeCount
        {
            get
            {
                return this.employeecountField;
            }
            set
            {
                this.employeecountField = value;
            }
        }

        public Decimal Turnover
        {
            get
            {
                return this.turnoverField;
            }
            set
            {
                this.turnoverField = value;
            }
        }

        public string TradeIndustry
        {
            get
            {
                return this.tradeindustryField;
            }
            set
            {
                this.tradeindustryField = value;
            }
        }

        public DirectorInformation[] Directors
        {
            get
            {
                return this.directorsField;
            }
            set
            {
                this.directorsField = value;
            }
        }
    }


    public class DirectorInformation
    {
        private string directoridnumberField;
        private decimal directorincomeField;

      
        public string ID
        {
            get
            {
                return this.directoridnumberField;
            }
            set
            {
                this.directoridnumberField = value;
            }
        }

        public Decimal Income
        {
            get
            {
                return this.directorincomeField;
            }
            set
            {
                this.directorincomeField = value;
            }
        }
    }
    /// <remarks/>
    public partial class ModuleRequestModuleRequest
    {

        public enum enumComEnquiryReason { CreditRisk, CreditLimitManagement, InsuranceApplication, Employment, FraudCorruptionTheftInv, FraudDetectionFraudPrev, SettingLimit, UnclaimedFundsDistribution, AffordabilityAssessment, PrescreeningMarketing, Other, DebtorBookAssessment, TracingByCreditProvider, ModelDevelopment, Contactibility, CreditScoringModel, CreditInformationQuery, DebtCounseling, CreditApplication, UpdateRecords, Supplier, DefaultCheck, FactualCheck, ReferanceCheck, Tracing, Lease, Rental, Surety, InternalEnquiry, Marketing, UnknownCPUtoCPU, InternationalEnquiry, SubscriberConcern, AccessCheck, InsuranceClaim, EmploymentCheck, FraudInvestigation, FraudDetection, ProvisionLimit, CreditScoreDevelopment, Affordability, PropensityToRepay };

        private string userField;
        private string passwordField;

        private string tUNumberField;

        private string[] moduleProductCodesField;

        private string enquiryReasonField;

        private string enquiryAmountField;

        private string termsField;

        private string userOnTicketField;

        private string contactForenameField;

        private string contactSurnameField;

        private string contactPhoneCodeField;

        private string contactPhoneNoField;

        private string contactFaxCodeField;

        private string contactFaxNoField;

        private string additionalClientReferenceField;

        private string subjectPhoneCodeField;

        private string subjectPhoneNoField;

        private string subjectNameOnTicketField;

        private string subjectAddressField;

        private string subjectSuburbField;

        private string subjectCityField;

        private string subjectPostCodeField;

        private string investigateOptionField;

        private string bankAccountNoField;

        private string bankAbbreviationField;

        private string bankBranchField;

        private string bankBranchCodeField;

        private string specialInstructionsField;

        private string bankCreditAmountField;

        private string bankTermsGivenField;

        private string bankAccountHolderField;
        private string uniquereferencenumberField;

        private CompanyInformationRequest companyInformationRequestField;

        public string User
        {
            get
            {
                return this.userField;
            }
            set
            {
                this.userField = value;
            }
        }

        public string Password
        {
            get
            {
                return this.passwordField;
            }
            set
            {
                this.passwordField = value;
            }
        }

        /// <remarks/>
        public string TUNumber
        {
            get
            {
                return this.tUNumberField;
            }
            set
            {
                this.tUNumberField = value;
            }
        }
       

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public string[] ModuleProductCodes
        {
            get
            {
                return this.moduleProductCodesField;
            }
            set
            {
                this.moduleProductCodesField = value;
            }
        }

        /// <remarks/>
        public string EnquiryReason
        {
            get
            {
                return this.enquiryReasonField;
            }
            set
            {
                this.enquiryReasonField = value;
            }
        }

        /// <remarks/>
        public string EnquiryAmount
        {
            get
            {
                return this.enquiryAmountField;
            }
            set
            {
                this.enquiryAmountField = value;
            }
        }

        /// <remarks/>
        public string Terms
        {
            get
            {
                return this.termsField;
            }
            set
            {
                this.termsField = value;
            }
        }

        /// <remarks/>
        public string UserOnTicket
        {
            get
            {
                return this.userOnTicketField;
            }
            set
            {
                this.userOnTicketField = value;
            }
        }

        /// <remarks/>
        public string ContactForename
        {
            get
            {
                return this.contactForenameField;
            }
            set
            {
                this.contactForenameField = value;
            }
        }

        /// <remarks/>
        public string ContactSurname
        {
            get
            {
                return this.contactSurnameField;
            }
            set
            {
                this.contactSurnameField = value;
            }
        }

        /// <remarks/>
        public string ContactPhoneCode
        {
            get
            {
                return this.contactPhoneCodeField;
            }
            set
            {
                this.contactPhoneCodeField = value;
            }
        }

        /// <remarks/>
        public string ContactPhoneNo
        {
            get
            {
                return this.contactPhoneNoField;
            }
            set
            {
                this.contactPhoneNoField = value;
            }
        }

        /// <remarks/>
        public string ContactFaxCode
        {
            get
            {
                return this.contactFaxCodeField;
            }
            set
            {
                this.contactFaxCodeField = value;
            }
        }

        /// <remarks/>
        public string ContactFaxNo
        {
            get
            {
                return this.contactFaxNoField;
            }
            set
            {
                this.contactFaxNoField = value;
            }
        }

        /// <remarks/>
        public string AdditionalClientReference
        {
            get
            {
                return this.additionalClientReferenceField;
            }
            set
            {
                this.additionalClientReferenceField = value;
            }
        }

        /// <remarks/>
        public string SubjectPhoneCode
        {
            get
            {
                return this.subjectPhoneCodeField;
            }
            set
            {
                this.subjectPhoneCodeField = value;
            }
        }

        /// <remarks/>
        public string SubjectPhoneNo
        {
            get
            {
                return this.subjectPhoneNoField;
            }
            set
            {
                this.subjectPhoneNoField = value;
            }
        }

        /// <remarks/>
        public string SubjectNameOnTicket
        {
            get
            {
                return this.subjectNameOnTicketField;
            }
            set
            {
                this.subjectNameOnTicketField = value;
            }
        }

        /// <remarks/>
        public string SubjectAddress
        {
            get
            {
                return this.subjectAddressField;
            }
            set
            {
                this.subjectAddressField = value;
            }
        }

        /// <remarks/>
        public string SubjectSuburb
        {
            get
            {
                return this.subjectSuburbField;
            }
            set
            {
                this.subjectSuburbField = value;
            }
        }

        /// <remarks/>
        public string SubjectCity
        {
            get
            {
                return this.subjectCityField;
            }
            set
            {
                this.subjectCityField = value;
            }
        }

        /// <remarks/>
        public string SubjectPostCode
        {
            get
            {
                return this.subjectPostCodeField;
            }
            set
            {
                this.subjectPostCodeField = value;
            }
        }

        /// <remarks/>
        public string InvestigateOption
        {
            get
            {
                return this.investigateOptionField;
            }
            set
            {
                this.investigateOptionField = value;
            }
        }

        /// <remarks/>
        public string BankAccountNo
        {
            get
            {
                return this.bankAccountNoField;
            }
            set
            {
                this.bankAccountNoField = value;
            }
        }

        /// <remarks/>
        public string BankAbbreviation
        {
            get
            {
                return this.bankAbbreviationField;
            }
            set
            {
                this.bankAbbreviationField = value;
            }
        }

        /// <remarks/>
        public string BankBranch
        {
            get
            {
                return this.bankBranchField;
            }
            set
            {
                this.bankBranchField = value;
            }
        }

        /// <remarks/>
        public string BankBranchCode
        {
            get
            {
                return this.bankBranchCodeField;
            }
            set
            {
                this.bankBranchCodeField = value;
            }
        }

        /// <remarks/>
        public string SpecialInstructions
        {
            get
            {
                return this.specialInstructionsField;
            }
            set
            {
                this.specialInstructionsField = value;
            }
        }

        /// <remarks/>
        public string BankCreditAmount
        {
            get
            {
                return this.bankCreditAmountField;
            }
            set
            {
                this.bankCreditAmountField = value;
            }
        }

        /// <remarks/>
        public string BankTermsGiven
        {
            get
            {
                return this.bankTermsGivenField;
            }
            set
            {
                this.bankTermsGivenField = value;
            }
        }

        /// <remarks/>
        public string BankAccountHolder
        {
            get
            {
                return this.bankAccountHolderField;
            }
            set
            {
                this.bankAccountHolderField = value;
            }
        }
        public string UniqueReferenceNumber
        {
            get
            {
                return this.uniquereferencenumberField;
            }
            set
            {
                this.uniquereferencenumberField = value;
            }
        }
        public CompanyInformationRequest CompanyInformation
        {
            get
            {
                return this.companyInformationRequestField;
            }
            set
            {
                this.companyInformationRequestField = value;
            }
        }

    }

    /// <remarks/>

    public partial class ModuleRequestNR
    {

        private ModuleRequestModuleRequestNR moduleRequest1FieldNR;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ModuleRequest")]
        public ModuleRequestModuleRequestNR ModuleRequest1
        {
            get
            {
                return this.moduleRequest1FieldNR;
            }
            set
            {
                this.moduleRequest1FieldNR = value;
            }
        }
    }

    /// <remarks/>
    public partial class ModuleRequestModuleRequestNR
    {

        //public enum enumComEnquiryReason { CreditRisk, CreditLimitManagement, InsuranceApplication, Employment, FraudCorruptionTheftInv, FraudDetectionFraudPrev, SettingLimit, UnclaimedFundsDistribution, AffordabilityAssessment, PrescreeningMarketing, Other, DebtorBookAssessment, TracingByCreditProvider, ModelDevelopment, Contactibility, CreditScoringModel, CreditInformationQuery, DebtCounseling, CreditApplication, UpdateRecords, Supplier, DefaultCheck, FactualCheck, ReferanceCheck, Tracing, Lease, Rental, Surety, InternalEnquiry, Marketing, UnknownCPUtoCPU, InternationalEnquiry, SubscriberConcern, AccessCheck, InsuranceClaim, EmploymentCheck, FraudInvestigation, FraudDetection, ProvisionLimit, CreditScoreDevelopment, Affordability, PropensityToRepay };

        private string searchTypeField;

        private string subjectNameField;

        private string registrationNoField;

        private string iTNumberField;

        private string dunsNumberField;

        private string vatNumberField;

        private string bankAccountNumberField;

        private string tradingNumberField;

        private string userField;

        private string passwordField;

        private string clientReferenceField;

        private string DesignationField;
        private string GrossMonthlyIncomeField;
        private string SurnameField;
        private string FirstNameField;
        private string IDTypeField;
        private string BirthDateField;
        private string IdentityNumberField;
        private string GenderField;
        private string MaritalStatusField;
        private string AddressLine1Field;
        private string AddressLine2Field;
        private string SuburbField;
        private string CityField;
        private string PostalCodeField;
        private string HomeTelephoneCodeField;
        private string HomeTelephoneNumberField;
        private string WorkTelephoneCodeField;
        private string WorkTelephoneNumberField;
        private string CellNumberField;
        private string OccupationField;
        private string EmployerField;
        private string NumberOfYearsAtEmployerField;
        private string BankNameField;
        private string BankAccountTypeField;
        private string BankBranchCodeField;
        private string BankAccountNumberPrincipalField;
        private string HighestQualificationField;

        public string Designation
        {
            get { return DesignationField; }
            set { DesignationField = value; }
        }

        public string GrossMonthlyIncome
        {
            get { return GrossMonthlyIncomeField; }
            set { GrossMonthlyIncomeField = value; }
        }

        public string Surname
        {
            get { return SurnameField; }
            set { SurnameField = value; }
        }
        public string FirstName
        {
            get { return FirstNameField; }
            set { FirstNameField = value; }
        }
        public string IDType
        {
            get { return IDTypeField; }
            set { IDTypeField = value; }
        }
        public string BirthDate
        {
            get { return BirthDateField; }
            set { BirthDateField = value; }
        }
        public string IdentityNumber
        {
            get { return IdentityNumberField; }
            set { IdentityNumberField = value; }
        }
        public string Gender
        {
            get { return GenderField; }
            set { GenderField = value; }
        }
        public string MaritalStatus
        {
            get { return MaritalStatusField; }
            set { MaritalStatusField = value; }
        }
        public string AddressLine1
        {
            get { return AddressLine1Field; }
            set { AddressLine1Field = value; }
        }
        public string AddressLine2
        {
            get { return AddressLine2Field; }
            set { AddressLine2Field = value; }
        }
        public string Suburb
        {
            get { return SuburbField; }
            set { SuburbField = value; }
        }
        public string City
        {
            get { return CityField; }
            set { CityField = value; }
        }
        public string PostalCode
        {
            get { return PostalCodeField; }
            set { PostalCodeField = value; }
        }
        public string HomeTelephoneCode
        {
            get { return HomeTelephoneCodeField; }
            set { HomeTelephoneCodeField = value; }
        }
        public string HomeTelephoneNumber
        {
            get { return HomeTelephoneNumberField; }
            set { HomeTelephoneNumberField = value; }
        }
        public string WorkTelephoneCode
        {
            get { return WorkTelephoneCodeField; }
            set { WorkTelephoneCodeField = value; }
        }
        public string WorkTelephoneNumber
        {
            get { return WorkTelephoneNumberField; }
            set { WorkTelephoneNumberField = value; }
        }
        public string CellNumber
        {
            get { return CellNumberField; }
            set { CellNumberField = value; }
        }
        public string Occupation
        {
            get { return OccupationField; }
            set { OccupationField = value; }
        }
        public string Employer
        {
            get { return EmployerField; }
            set { EmployerField = value; }
        }
        public string NumberOfYearsAtEmployer
        {
            get { return NumberOfYearsAtEmployerField; }
            set { NumberOfYearsAtEmployerField = value; }
        }
        public string BankName
        {
            get { return BankNameField; }
            set { BankNameField = value; }
        }
        public string BankAccountType
        {
            get { return BankAccountTypeField; }
            set { BankAccountTypeField = value; }
        }
        public string BankBranchCode
        {
            get { return BankBranchCodeField; }
            set { BankBranchCodeField = value; }
        }
        public string BankAccountNumberPrincipal
        {
            get { return BankAccountNumberPrincipalField; }
            set { BankAccountNumberPrincipalField = value; }
        }
        public string HighestQualification
        {
            get { return HighestQualificationField; }
            set { HighestQualificationField = value; }
        }


        /// <remarks/>
        public string SearchType
        {
            get
            {
                return this.searchTypeField;
            }
            set
            {
                this.searchTypeField = value;
            }
        }

        /// <remarks/>
        public string SubjectName
        {
            get
            {
                return this.subjectNameField;
            }
            set
            {
                this.subjectNameField = value;
            }
        }

        /// <remarks/>
        public string RegistrationNo
        {
            get
            {
                return this.registrationNoField;
            }
            set
            {
                this.registrationNoField = value;
            }
        }

        /// <remarks/>
        public string ITNumber
        {
            get
            {
                return this.iTNumberField;
            }
            set
            {
                this.iTNumberField = value;
            }
        }

        /// <remarks/>
        public string DunsNumber
        {
            get
            {
                return this.dunsNumberField;
            }
            set
            {
                this.dunsNumberField = value;
            }
        }

        /// <remarks/>
        public string VatNumber
        {
            get
            {
                return this.vatNumberField;
            }
            set
            {
                this.vatNumberField = value;
            }
        }

        /// <remarks/>
        public string BankAccountNumber
        {
            get
            {
                return this.bankAccountNumberField;
            }
            set
            {
                this.bankAccountNumberField = value;
            }
        }

        /// <remarks/>
        public string TradingNumber
        {
            get
            {
                return this.tradingNumberField;
            }
            set
            {
                this.tradingNumberField = value;
            }
        }

        public string User
        {
            get
            {
                return this.userField;
            }
            set
            {
                this.userField = value;
            }
        }

        public string Password
        {
            get
            {
                return this.passwordField;
            }
            set
            {
                this.passwordField = value;
            }
        }

        public string ClientReference
        {
            get
            {
                return this.clientReferenceField;
            }
            set
            {
                this.clientReferenceField = value;
            }
        }
    }

    public partial class ModuleRequestResponse
    {

        private ModuleRequestResponseModuleRequestResult moduleRequestResultField;

        /// <remarks/>
        public ModuleRequestResponseModuleRequestResult ModuleRequestResult
        {
            get
            {
                return this.moduleRequestResultField;
            }
            set
            {
                this.moduleRequestResultField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResult
    {

        public enum enumRResultResponseStatus { Success, Failure };

        private string rawDataField;

        private string responseStatusField;

        private string errorCodeField;

        private string errorMessageField;

        private DateTime processingStartDateField = DateTime.Now;

        private float processingTimeSecsField;

        private string uniqueRefGuidField;

                
        private ModuleRequestResponseModuleRequestResultHeader headerField;

        private ModuleRequestResponseModuleRequestResultNames namesField;

        private ModuleRequestResponseModuleRequestResultRegistrationDetails registrationDetailsField;

        private ModuleRequestResponseModuleRequestResultFinanceHeader financeHeaderField;

        private ModuleRequestResponseModuleRequestResultEmpOfCapital empOfCapitalField;

        private ModuleRequestResponseModuleRequestResultCurrentAsset currentAssetField;

        private ModuleRequestResponseModuleRequestResultCurrentLiabilities currentLiabilitiesField;

        private ModuleRequestResponseModuleRequestResultVehicles[] vehiclesField;

        private ModuleRequestResponseModuleRequestResultOperations operationsField;

        private ModuleRequestResponseModuleRequestResultAffiliations affiliationsField;

        private ModuleRequestResponseModuleRequestResultBankHistory bankHistoryField;

        private ModuleRequestResponseModuleRequestResultGeneralBankingInfo generalBankingInfoField;

        private ModuleRequestResponseModuleRequestResultBankCodes bankCodesField;

        private ModuleRequestResponseModuleRequestResultTradeHistory tradeHistoryField;

        private ModuleRequestResponseModuleRequestResultBusinessDeedsSummaryDA businessDeedsSummaryDAField;

        private ModuleRequestResponseModuleRequestResultCourtRecords courtRecordsField;

        private ModuleRequestResponseModuleRequestResultDefaults defaultsField;

        private ModuleRequestResponseModuleRequestResultNotarialBonds notarialBondsField;

        private ModuleRequestResponseModuleRequestResultPrincipals principalsField;

        private ModuleRequestResponseModuleRequestResultPrincipalClearances principalClearancesField;

        private ModuleRequestResponseModuleRequestResultConsumerJudgements consumerJudgementsField;

        private ModuleRequestResponseModuleRequestResultConsumerDefaults consumerDefaultsField;

        private ModuleRequestResponseModuleRequestResultConsumerNotices consumerNoticesField;

        private ModuleRequestResponseModuleRequestResultConsumerNotarialBonds consumerNotarialBondsField;

        private ModuleRequestResponseModuleRequestResultPrincipalLinks principalLinksField;

        private ModuleRequestResponseModuleRequestResultConsumerInfoNO04 consumerInfoNO04Field;

        private ModuleRequestResponseModuleRequestResultBCCCX01 bCCCX01Field;

        private ModuleRequestResponseModuleRequestResultEnquiryHistoryEnquiryHistory enquiryHistoryField;

        /// <remarks/>
        public string RawData
        {
            get
            {
                return this.rawDataField;
            }
            set
            {
                this.rawDataField = value;
            }
        }

        /// <remarks/>
        public string ResponseStatus
        {
            get
            {
                return this.responseStatusField;
            }
            set
            {
                this.responseStatusField = value;
            }
        }

        /// <remarks/>
        public string ErrorCode
        {
            get
            {
                return this.errorCodeField;
            }
            set
            {
                this.errorCodeField = value;
            }
        }

        /// <remarks/>
        public string ErrorMessage
        {
            get
            {
                return this.errorMessageField;
            }
            set
            {
                this.errorMessageField = value;
            }
        }

        /// <remarks/>
        public DateTime ProcessingStartDate
        {
            get
            {
                return this.processingStartDateField;
            }
            set
            {
                this.processingStartDateField = value;
            }
        }

        /// <remarks/>
        public float ProcessingTimeSecs
        {
            get
            {
                return this.processingTimeSecsField;
            }
            set
            {
                this.processingTimeSecsField = value;
            }
        }

        /// <remarks/>
        public string UniqueRefGuid
        {
            get
            {
                return this.uniqueRefGuidField;
            }
            set
            {
                this.uniqueRefGuidField = value;
            }
        }

        /// <remarks/>
         [System.Xml.Serialization.XmlElement("Header")]
        public ModuleRequestResponseModuleRequestResultHeader Header
        {
            get
            {
                return this.headerField;
            }
            set
            {
                this.headerField = value;
            }
        }
         [System.Xml.Serialization.XmlElement("Names")]
        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultNames Names
        {
            get
            {
                return this.namesField;
            }
            set
            {
                this.namesField = value;
            }
        }
        [System.Xml.Serialization.XmlElement("RegistrationDetails")]
        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultRegistrationDetails RegistrationDetails
        {
            get
            {
                return this.registrationDetailsField;
            }
            set
            {
                this.registrationDetailsField = value;
            }
        }
        [System.Xml.Serialization.XmlElement("FinanceHeader")]
        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultFinanceHeader FinanceHeader
        {
            get
            {
                return this.financeHeaderField;
            }
            set
            {
                this.financeHeaderField = value;
            }
        }
        [System.Xml.Serialization.XmlElement("EmpOfCapital")]
        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultEmpOfCapital EmpOfCapital
        {
            get
            {
                return this.empOfCapitalField;
            }
            set
            {
                this.empOfCapitalField = value;
            }
        }
        [System.Xml.Serialization.XmlElement("CurrentAsset")]
        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultCurrentAsset CurrentAsset
        {
            get
            {
                return this.currentAssetField;
            }
            set
            {
                this.currentAssetField = value;
            }
        }
        [System.Xml.Serialization.XmlElement("CurrentLiabilities")]
        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultCurrentLiabilities CurrentLiabilities
        {
            get
            {
                return this.currentLiabilitiesField;
            }
            set
            {
                this.currentLiabilitiesField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Vehicles", IsNullable = false)]
        public ModuleRequestResponseModuleRequestResultVehicles[] Vehicles
        {
            get
            {
                return this.vehiclesField;
            }
            set
            {
                this.vehiclesField = value;
            }
        }

        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultOperations Operations
        {
            get
            {
                return this.operationsField;
            }
            set
            {
                this.operationsField = value;
            }
        }

        /// <remarks/>
         [System.Xml.Serialization.XmlElement("Affiliations")]
        public ModuleRequestResponseModuleRequestResultAffiliations Affiliations
        {
            get
            {
                return this.affiliationsField;
            }
            set
            {
                this.affiliationsField = value;
            }
        }
        [System.Xml.Serialization.XmlElement("BankHistory")]
        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultBankHistory BankHistory
        {
            get
            {
                return this.bankHistoryField;
            }
            set
            {
                this.bankHistoryField = value;
            }
        }
        [System.Xml.Serialization.XmlElement("GeneralBankingInfo")]
        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultGeneralBankingInfo GeneralBankingInfo
        {
            get
            {
                return this.generalBankingInfoField;
            }
            set
            {
                this.generalBankingInfoField = value;
            }
        }
        [System.Xml.Serialization.XmlElement("BankCodes")]
        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultBankCodes BankCodes
        {
            get
            {
                return this.bankCodesField;
            }
            set
            {
                this.bankCodesField = value;
            }
        }

        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultTradeHistory TradeHistory
        {
            get
            {
                return this.tradeHistoryField;
            }
            set
            {
                this.tradeHistoryField = value;
            }
        }

        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultBusinessDeedsSummaryDA BusinessDeedsSummaryDA
        {
            get
            {
                return this.businessDeedsSummaryDAField;
            }
            set
            {
                this.businessDeedsSummaryDAField = value;
            }
        }

        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultCourtRecords CourtRecords
        {
            get
            {
                return this.courtRecordsField;
            }
            set
            {
                this.courtRecordsField = value;
            }
        }

        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultDefaults Defaults
        {
            get
            {
                return this.defaultsField;
            }
            set
            {
                this.defaultsField = value;
            }
        }

        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultNotarialBonds NotarialBonds
        {
            get
            {
                return this.notarialBondsField;
            }
            set
            {
                this.notarialBondsField = value;
            }
        }
        
        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultPrincipals Principals
        {
            get
            {
                return this.principalsField;
            }
            set
            {
                this.principalsField = value;
            }
        }
        [System.Xml.Serialization.XmlElement("PrincipalClearances")]
        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultPrincipalClearances PrincipalClearances
        {
            get
            {
                return this.principalClearancesField;
            }
            set
            {
                this.principalClearancesField = value;
            }
        }
        [System.Xml.Serialization.XmlElement("ConsumerJudgements")]

        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultConsumerJudgements ConsumerJudgements
        {
            get
            {
                return this.consumerJudgementsField;
            }
            set
            {
                this.consumerJudgementsField = value;
            }
        }

        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultConsumerDefaults ConsumerDefaults
        {
            get
            {
                return this.consumerDefaultsField;
            }
            set
            {
                this.consumerDefaultsField = value;
            }
        }

        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultConsumerNotices ConsumerNotices
        {
            get
            {
                return this.consumerNoticesField;
            }
            set
            {
                this.consumerNoticesField = value;
            }
        }

        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultConsumerNotarialBonds ConsumerNotarialBonds
        {
            get
            {
                return this.consumerNotarialBondsField;
            }
            set
            {
                this.consumerNotarialBondsField = value;
            }
        }
        
        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultPrincipalLinks PrincipalLinks
        {
            get
            {
                return this.principalLinksField;
            }
            set
            {
                this.principalLinksField = value;
            }
        }

        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultConsumerInfoNO04 ConsumerInfoNO04
        {
            get
            {
                return this.consumerInfoNO04Field;
            }
            set
            {
                this.consumerInfoNO04Field = value;
            }
        }

        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultBCCCX01 BCCCX01
        {
            get
            {
                return this.bCCCX01Field;
            }
            set
            {
                this.bCCCX01Field = value;
            }
        }

        public ModuleRequestResponseModuleRequestResultEnquiryHistoryEnquiryHistory EnquiryHistory
        {
            get
            {
                return this.enquiryHistoryField;
            }
            set
            {
                this.enquiryHistoryField = value;
            }
        }

        
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultHeader
    {

        private string majorProductField;

        private string dateOfHeaderField;

        private string startDateField;

        private string businessTypeField;

        private string dunsNumberField;

        private string startingCapitalField;

        private string purchasePriceField;

        private string industryField;

        private string businessFunctionField;

        private string businessCategoryField;

        private string physicalAddressField;

        private string suburbField;

        private string cityField;

        private string countryField;

        private string postCodeField;

        private string postalAddressField;

        private string postalSuburbField;

        private string postalCityField;

        private string postalCountryField;

        private string postalPostCodeField;

        private string phoneField;

        private string faxField;

        private string[] vATNumbersField;

        private string websiteField;

        private string emailField;

        private string taxNumberField;

        private string tradingNumberField;

        /// <remarks/>
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        public string DateOfHeader
        {
            get
            {
                return this.dateOfHeaderField;
            }
            set
            {
                this.dateOfHeaderField = value;
            }
        }

        /// <remarks/>
        public string StartDate
        {
            get
            {
                return this.startDateField;
            }
            set
            {
                this.startDateField = value;
            }
        }

        /// <remarks/>
        public string BusinessType
        {
            get
            {
                return this.businessTypeField;
            }
            set
            {
                this.businessTypeField = value;
            }
        }

        /// <remarks/>
        public string DunsNumber
        {
            get
            {
                return this.dunsNumberField;
            }
            set
            {
                this.dunsNumberField = value;
            }
        }

        /// <remarks/>
        public string StartingCapital
        {
            get
            {
                return this.startingCapitalField;
            }
            set
            {
                this.startingCapitalField = value;
            }
        }

        /// <remarks/>
        public string PurchasePrice
        {
            get
            {
                return this.purchasePriceField;
            }
            set
            {
                this.purchasePriceField = value;
            }
        }

        /// <remarks/>
        public string Industry
        {
            get
            {
                return this.industryField;
            }
            set
            {
                this.industryField = value;
            }
        }

        /// <remarks/>
        public string BusinessFunction
        {
            get
            {
                return this.businessFunctionField;
            }
            set
            {
                this.businessFunctionField = value;
            }
        }

        /// <remarks/>
        public string BusinessCategory
        {
            get
            {
                return this.businessCategoryField;
            }
            set
            {
                this.businessCategoryField = value;
            }
        }

        /// <remarks/>
        public string PhysicalAddress
        {
            get
            {
                return this.physicalAddressField;
            }
            set
            {
                this.physicalAddressField = value;
            }
        }

        /// <remarks/>
        public string Suburb
        {
            get
            {
                return this.suburbField;
            }
            set
            {
                this.suburbField = value;
            }
        }

        /// <remarks/>
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        public string Country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        /// <remarks/>
        public string PostCode
        {
            get
            {
                return this.postCodeField;
            }
            set
            {
                this.postCodeField = value;
            }
        }

        /// <remarks/>
        public string PostalAddress
        {
            get
            {
                return this.postalAddressField;
            }
            set
            {
                this.postalAddressField = value;
            }
        }

        /// <remarks/>
        public string PostalSuburb
        {
            get
            {
                return this.postalSuburbField;
            }
            set
            {
                this.postalSuburbField = value;
            }
        }

        /// <remarks/>
        public string PostalCity
        {
            get
            {
                return this.postalCityField;
            }
            set
            {
                this.postalCityField = value;
            }
        }

        /// <remarks/>
        public string PostalCountry
        {
            get
            {
                return this.postalCountryField;
            }
            set
            {
                this.postalCountryField = value;
            }
        }

        /// <remarks/>
        public string PostalPostCode
        {
            get
            {
                return this.postalPostCodeField;
            }
            set
            {
                this.postalPostCodeField = value;
            }
        }

        /// <remarks/>
        public string Phone
        {
            get
            {
                return this.phoneField;
            }
            set
            {
                this.phoneField = value;
            }
        }

        /// <remarks/>
        public string Fax
        {
            get
            {
                return this.faxField;
            }
            set
            {
                this.faxField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public string[] VATNumbers
        {
            get
            {
                return this.vATNumbersField;
            }
            set
            {
                this.vATNumbersField = value;
            }
        }

        /// <remarks/>
        public string Website
        {
            get
            {
                return this.websiteField;
            }
            set
            {
                this.websiteField = value;
            }
        }

        /// <remarks/>
        public string Email
        {
            get
            {
                return this.emailField;
            }
            set
            {
                this.emailField = value;
            }
        }

        /// <remarks/>
        public string TaxNumber
        {
            get
            {
                return this.taxNumberField;
            }
            set
            {
                this.taxNumberField = value;
            }
        }

        /// <remarks/>
        public string TradingNumber
        {
            get
            {
                return this.tradingNumberField;
            }
            set
            {
                this.tradingNumberField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultEnquiryHistory
    {

        private string majorProductField;

        private string dateOfEnquiryField;

        private string reasonField;

        private string subscriberField;

        private string amountField;

        private string contactNameField;

        private string contactPhoneField;

        private string subscriberNumberField;

        /// <remarks/>
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        public string DateOfEnquiry
        {
            get
            {
                return this.dateOfEnquiryField;
            }
            set
            {
                this.dateOfEnquiryField = value;
            }
        }

        /// <remarks/>
        public string Reason
        {
            get
            {
                return this.reasonField;
            }
            set
            {
                this.reasonField = value;
            }
        }

        /// <remarks/>
        public string Subscriber
        {
            get
            {
                return this.subscriberField;
            }
            set
            {
                this.subscriberField = value;
            }
        }

        /// <remarks/>
        public string Amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        public string ContactName
        {
            get
            {
                return this.contactNameField;
            }
            set
            {
                this.contactNameField = value;
            }
        }

        public string ContactPhone
        {
            get
            {
                return this.contactPhoneField;
            }
            set
            {
                this.contactPhoneField = value;
            }
        }

        /// <remarks/>
        public string SubscriberNumber
        {
            get
            {
                return this.subscriberNumberField;
            }
            set
            {
                this.subscriberNumberField = value;
            }
        }

    }


    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultEnquiryHistoryEnquiryHistory
    {

        private ModuleRequestResponseModuleRequestResultEnquiryHistory[] enquiryHistoryField;

        [System.Xml.Serialization.XmlElement("EnquiryHistory")]
        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultEnquiryHistory[] EnquiryHistory
        {
            get
            {
                return this.enquiryHistoryField;
            }
            set
            {
                this.enquiryHistoryField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultNames
    {

        private string majorProductField;

        private string businessNameField;

        private string aKANameField;

        private string[] divisionalNamesField;

        private string[] previousNamesField;

        private string[] previousNameDatesField;

        private string[] tradeStylesField;

        /// <remarks/>
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        public string BusinessName
        {
            get
            {
                return this.businessNameField;
            }
            set
            {
                this.businessNameField = value;
            }
        }

        /// <remarks/>
        public string AKAName
        {
            get
            {
                return this.aKANameField;
            }
            set
            {
                this.aKANameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public string[] DivisionalNames
        {
            get
            {
                return this.divisionalNamesField;
            }
            set
            {
                this.divisionalNamesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public string[] PreviousNames
        {
            get
            {
                return this.previousNamesField;
            }
            set
            {
                this.previousNamesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public string[] PreviousNameDates
        {
            get
            {
                return this.previousNameDatesField;
            }
            set
            {
                this.previousNameDatesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public string[] TradeStyles
        {
            get
            {
                return this.tradeStylesField;
            }
            set
            {
                this.tradeStylesField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultRegistrationDetails
    {

        private string majorProductField;

        private string registrationDateField;

        private string registrationStatusCodeField;

        private string registrationStatusDescField;

        private string liquidationDateField;

        private string registrationNoField;

        private string companyTypeCodeField;

        private string companyTypeDescField;

        private string registrationCountryField;

        private string[] previousRegistrationNoField;

        private string auditorsField;

        private string registeredAddressField;

        private string suburbField;

        private string cityField;

        private string countryField;

        private string postCodeField;

        private string authorisedCapitalField;

        private object[] shareInfoField;

        private string issuedCapitalField;

        private string statedCapitalField;

        private object[] capitalInfoField;

        private string reg_Status_ReasonField;

        private string reg_Info_DateField;

        private string[] postalAddressField;

        private string postalPostCodeField;

        private string financialYearEndField;

        /// <remarks/>
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        public string RegistrationDate
        {
            get
            {
                return this.registrationDateField;
            }
            set
            {
                this.registrationDateField = value;
            }
        }

        /// <remarks/>
        public string RegistrationStatusCode
        {
            get
            {
                return this.registrationStatusCodeField;
            }
            set
            {
                this.registrationStatusCodeField = value;
            }
        }

        /// <remarks/>
        public string RegistrationStatusDesc
        {
            get
            {
                return this.registrationStatusDescField;
            }
            set
            {
                this.registrationStatusDescField = value;
            }
        }

        /// <remarks/>
        public string LiquidationDate
        {
            get
            {
                return this.liquidationDateField;
            }
            set
            {
                this.liquidationDateField = value;
            }
        }

        /// <remarks/>
        public string RegistrationNo
        {
            get
            {
                return this.registrationNoField;
            }
            set
            {
                this.registrationNoField = value;
            }
        }

        /// <remarks/>
        public string CompanyTypeCode
        {
            get
            {
                return this.companyTypeCodeField;
            }
            set
            {
                this.companyTypeCodeField = value;
            }
        }

        /// <remarks/>
        public string CompanyTypeDesc
        {
            get
            {
                return this.companyTypeDescField;
            }
            set
            {
                this.companyTypeDescField = value;
            }
        }

        /// <remarks/>
        public string RegistrationCountry
        {
            get
            {
                return this.registrationCountryField;
            }
            set
            {
                this.registrationCountryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public string[] PreviousRegistrationNo
        {
            get
            {
                return this.previousRegistrationNoField;
            }
            set
            {
                this.previousRegistrationNoField = value;
            }
        }

        /// <remarks/>
        public string Auditors
        {
            get
            {
                return this.auditorsField;
            }
            set
            {
                this.auditorsField = value;
            }
        }

        /// <remarks/>
        public string RegisteredAddress
        {
            get
            {
                return this.registeredAddressField;
            }
            set
            {
                this.registeredAddressField = value;
            }
        }

        /// <remarks/>
        public string Suburb
        {
            get
            {
                return this.suburbField;
            }
            set
            {
                this.suburbField = value;
            }
        }

        /// <remarks/>
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        public string Country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        /// <remarks/>
        public string PostCode
        {
            get
            {
                return this.postCodeField;
            }
            set
            {
                this.postCodeField = value;
            }
        }

        /// <remarks/>
        public string AuthorisedCapital
        {
            get
            {
                return this.authorisedCapitalField;
            }
            set
            {
                this.authorisedCapitalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("ShareInfo", IsNullable = false)]
        public object[] ShareInfo
        {
            get
            {
                return this.shareInfoField;
            }
            set
            {
                this.shareInfoField = value;
            }
        }

        /// <remarks/>
        public string IssuedCapital
        {
            get
            {
                return this.issuedCapitalField;
            }
            set
            {
                this.issuedCapitalField = value;
            }
        }

        /// <remarks/>
        public string StatedCapital
        {
            get
            {
                return this.statedCapitalField;
            }
            set
            {
                this.statedCapitalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("CapitalInfo", IsNullable = false)]
        public object[] CapitalInfo
        {
            get
            {
                return this.capitalInfoField;
            }
            set
            {
                this.capitalInfoField = value;
            }
        }

        /// <remarks/>
        public string Reg_Status_Reason
        {
            get
            {
                return this.reg_Status_ReasonField;
            }
            set
            {
                this.reg_Status_ReasonField = value;
            }
        }

        /// <remarks/>
        public string Reg_Info_Date
        {
            get
            {
                return this.reg_Info_DateField;
            }
            set
            {
                this.reg_Info_DateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public string[] PostalAddress
        {
            get
            {
                return this.postalAddressField;
            }
            set
            {
                this.postalAddressField = value;
            }
        }

        /// <remarks/>
        public string PostalPostCode
        {
            get
            {
                return this.postalPostCodeField;
            }
            set
            {
                this.postalPostCodeField = value;
            }
        }

        /// <remarks/>
        public string FinancialYearEnd
        {
            get
            {
                return this.financialYearEndField;
            }
            set
            {
                this.financialYearEndField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultFinanceHeader
    {

        private string majorProductField;

        private string dateField;

        private string sourceField;

        private string sourceTitleField;

        private string commentField;

        private string turnOverDateYear1Field;

        private string turnOverDateYear2Field;

        private string turnOverAmount1Field;

        private string turnOverAmount2Field;

        private string turnOverYear1TypeField;

        private string turnOverYear2TypeField;

        private string balanceSheetDateField;

        private string balanceSheetUnitField;

        private string balanceSheetTypeField;

        private string finYearMonthField;

        private string dRCededIndField;

        private string dRCededNameField;

        private string dRFactoredField;

        private string dRFactoredNameField;

        /// <remarks/>
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        public string Date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>
        public string Source
        {
            get
            {
                return this.sourceField;
            }
            set
            {
                this.sourceField = value;
            }
        }

        /// <remarks/>
        public string SourceTitle
        {
            get
            {
                return this.sourceTitleField;
            }
            set
            {
                this.sourceTitleField = value;
            }
        }

        /// <remarks/>
        public string Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        public string TurnOverDateYear1
        {
            get
            {
                return this.turnOverDateYear1Field;
            }
            set
            {
                this.turnOverDateYear1Field = value;
            }
        }

        /// <remarks/>
        public string TurnOverDateYear2
        {
            get
            {
                return this.turnOverDateYear2Field;
            }
            set
            {
                this.turnOverDateYear2Field = value;
            }
        }

        /// <remarks/>
        public string TurnOverAmount1
        {
            get
            {
                return this.turnOverAmount1Field;
            }
            set
            {
                this.turnOverAmount1Field = value;
            }
        }

        /// <remarks/>
        public string TurnOverAmount2
        {
            get
            {
                return this.turnOverAmount2Field;
            }
            set
            {
                this.turnOverAmount2Field = value;
            }
        }

        /// <remarks/>
        public string TurnOverYear1Type
        {
            get
            {
                return this.turnOverYear1TypeField;
            }
            set
            {
                this.turnOverYear1TypeField = value;
            }
        }

        /// <remarks/>
        public string TurnOverYear2Type
        {
            get
            {
                return this.turnOverYear2TypeField;
            }
            set
            {
                this.turnOverYear2TypeField = value;
            }
        }

        /// <remarks/>
        public string BalanceSheetDate
        {
            get
            {
                return this.balanceSheetDateField;
            }
            set
            {
                this.balanceSheetDateField = value;
            }
        }

        /// <remarks/>
        public string BalanceSheetUnit
        {
            get
            {
                return this.balanceSheetUnitField;
            }
            set
            {
                this.balanceSheetUnitField = value;
            }
        }

        /// <remarks/>
        public string BalanceSheetType
        {
            get
            {
                return this.balanceSheetTypeField;
            }
            set
            {
                this.balanceSheetTypeField = value;
            }
        }

        /// <remarks/>
        public string FinYearMonth
        {
            get
            {
                return this.finYearMonthField;
            }
            set
            {
                this.finYearMonthField = value;
            }
        }

        /// <remarks/>
        public string DRCededInd
        {
            get
            {
                return this.dRCededIndField;
            }
            set
            {
                this.dRCededIndField = value;
            }
        }

        /// <remarks/>
        public string DRCededName
        {
            get
            {
                return this.dRCededNameField;
            }
            set
            {
                this.dRCededNameField = value;
            }
        }

        /// <remarks/>
        public string DRFactored
        {
            get
            {
                return this.dRFactoredField;
            }
            set
            {
                this.dRFactoredField = value;
            }
        }

        /// <remarks/>
        public string DRFactoredName
        {
            get
            {
                return this.dRFactoredNameField;
            }
            set
            {
                this.dRFactoredNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultEmpOfCapital
    {

        private string majorProductField;

        private string empOfCapTotalField;

        private string[] empOfCapItemField;

        private string[] empOfCapAmtField;

        /// <remarks/>
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        public string EmpOfCapTotal
        {
            get
            {
                return this.empOfCapTotalField;
            }
            set
            {
                this.empOfCapTotalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public string[] EmpOfCapItem
        {
            get
            {
                return this.empOfCapItemField;
            }
            set
            {
                this.empOfCapItemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public string[] EmpOfCapAmt
        {
            get
            {
                return this.empOfCapAmtField;
            }
            set
            {
                this.empOfCapAmtField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultCurrentAsset
    {

        private string majorProductField;

        private string currentAssTotalField;

        private string[] currentAssItemField;

        private string[] currentAssItemAmtField;

        /// <remarks/>
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        public string CurrentAssTotal
        {
            get
            {
                return this.currentAssTotalField;
            }
            set
            {
                this.currentAssTotalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public string[] CurrentAssItem
        {
            get
            {
                return this.currentAssItemField;
            }
            set
            {
                this.currentAssItemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public string[] CurrentAssItemAmt
        {
            get
            {
                return this.currentAssItemAmtField;
            }
            set
            {
                this.currentAssItemAmtField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultCurrentLiabilities
    {

        private string majorProductField;

        private string currentLiabilityTotalField;

        private string[] currentLiabilityItemField;

        private string[] currentLiabilityItemAmtField;

        /// <remarks/>
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        public string CurrentLiabilityTotal
        {
            get
            {
                return this.currentLiabilityTotalField;
            }
            set
            {
                this.currentLiabilityTotalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public string[] CurrentLiabilityItem
        {
            get
            {
                return this.currentLiabilityItemField;
            }
            set
            {
                this.currentLiabilityItemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public string[] CurrentLiabilityItemAmt
        {
            get
            {
                return this.currentLiabilityItemAmtField;
            }
            set
            {
                this.currentLiabilityItemAmtField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultVehicles
    {

        private string majorProductField;

        private string commentField;

        private object vehicleListField;

        private string dateInfoField;

        /// <remarks/>
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        public string Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        public object VehicleList
        {
            get
            {
                return this.vehicleListField;
            }
            set
            {
                this.vehicleListField = value;
            }
        }

        /// <remarks/>
        public string DateInfo
        {
            get
            {
                return this.dateInfoField;
            }
            set
            {
                this.dateInfoField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultOperations
    {

        private ModuleRequestResponseModuleRequestResultOperationsOperation operationField;

        [System.Xml.Serialization.XmlElement("Operation")]
        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultOperationsOperation Operation
        {
            get
            {
                return this.operationField;
            }
            set
            {
                this.operationField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultOperationsOperation
    {

        private string majorProductField;

        private string dateField;

        private string sicCodeField;

        private string sicDescriptionField;

        private string salesAreaField;

        private string divisionalNameField;

        private string creditSalesField;

        private string creditSalesPercField;

        private string termsFromField;

        private string termsToField;

        private string aveCollPeriodField;

        private string cashSalesField;

        private string cashSalesPercField;

        private object opCommentField;

        private string importField;

        private string importCommentField;

        private string exportField;

        private string exportCommentField;

        private string numberSalesStaffField;

        private string numberWageStaffField;

        private string totalNumberStaffField;

        private string premisesOwnerField;

        private string premisesLessorField;

        private string premisesSizeField;

        private string leasePeriodFromField;

        private string leasePeriodToField;

        private string premisesRentalField;

        private string sASicCodeField;

        private string sASicDescriptionField;

        /// <remarks/>
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        public string Date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>
        public string SicCode
        {
            get
            {
                return this.sicCodeField;
            }
            set
            {
                this.sicCodeField = value;
            }
        }

        /// <remarks/>
        public string SicDescription
        {
            get
            {
                return this.sicDescriptionField;
            }
            set
            {
                this.sicDescriptionField = value;
            }
        }

        /// <remarks/>
        public string SalesArea
        {
            get
            {
                return this.salesAreaField;
            }
            set
            {
                this.salesAreaField = value;
            }
        }

        /// <remarks/>
        public string DivisionalName
        {
            get
            {
                return this.divisionalNameField;
            }
            set
            {
                this.divisionalNameField = value;
            }
        }

        /// <remarks/>
        public string CreditSales
        {
            get
            {
                return this.creditSalesField;
            }
            set
            {
                this.creditSalesField = value;
            }
        }

        /// <remarks/>
        public string CreditSalesPerc
        {
            get
            {
                return this.creditSalesPercField;
            }
            set
            {
                this.creditSalesPercField = value;
            }
        }

        /// <remarks/>
        public string TermsFrom
        {
            get
            {
                return this.termsFromField;
            }
            set
            {
                this.termsFromField = value;
            }
        }

        /// <remarks/>
        public string TermsTo
        {
            get
            {
                return this.termsToField;
            }
            set
            {
                this.termsToField = value;
            }
        }

        /// <remarks/>
        public string AveCollPeriod
        {
            get
            {
                return this.aveCollPeriodField;
            }
            set
            {
                this.aveCollPeriodField = value;
            }
        }

        /// <remarks/>
        public string CashSales
        {
            get
            {
                return this.cashSalesField;
            }
            set
            {
                this.cashSalesField = value;
            }
        }

        /// <remarks/>
        public string CashSalesPerc
        {
            get
            {
                return this.cashSalesPercField;
            }
            set
            {
                this.cashSalesPercField = value;
            }
        }

        /// <remarks/>
        public object OpComment
        {
            get
            {
                return this.opCommentField;
            }
            set
            {
                this.opCommentField = value;
            }
        }

        /// <remarks/>
        public string Import
        {
            get
            {
                return this.importField;
            }
            set
            {
                this.importField = value;
            }
        }

        /// <remarks/>
        public string ImportComment
        {
            get
            {
                return this.importCommentField;
            }
            set
            {
                this.importCommentField = value;
            }
        }

        /// <remarks/>
        public string Export
        {
            get
            {
                return this.exportField;
            }
            set
            {
                this.exportField = value;
            }
        }

        /// <remarks/>
        public string ExportComment
        {
            get
            {
                return this.exportCommentField;
            }
            set
            {
                this.exportCommentField = value;
            }
        }

        /// <remarks/>
        public string NumberSalesStaff
        {
            get
            {
                return this.numberSalesStaffField;
            }
            set
            {
                this.numberSalesStaffField = value;
            }
        }

        /// <remarks/>
        public string NumberWageStaff
        {
            get
            {
                return this.numberWageStaffField;
            }
            set
            {
                this.numberWageStaffField = value;
            }
        }

        /// <remarks/>
        public string TotalNumberStaff
        {
            get
            {
                return this.totalNumberStaffField;
            }
            set
            {
                this.totalNumberStaffField = value;
            }
        }

        /// <remarks/>
        public string PremisesOwner
        {
            get
            {
                return this.premisesOwnerField;
            }
            set
            {
                this.premisesOwnerField = value;
            }
        }

        /// <remarks/>
        public string PremisesLessor
        {
            get
            {
                return this.premisesLessorField;
            }
            set
            {
                this.premisesLessorField = value;
            }
        }

        /// <remarks/>
        public string PremisesSize
        {
            get
            {
                return this.premisesSizeField;
            }
            set
            {
                this.premisesSizeField = value;
            }
        }

        /// <remarks/>
        public string LeasePeriodFrom
        {
            get
            {
                return this.leasePeriodFromField;
            }
            set
            {
                this.leasePeriodFromField = value;
            }
        }

        /// <remarks/>
        public string LeasePeriodTo
        {
            get
            {
                return this.leasePeriodToField;
            }
            set
            {
                this.leasePeriodToField = value;
            }
        }

        /// <remarks/>
        public string PremisesRental
        {
            get
            {
                return this.premisesRentalField;
            }
            set
            {
                this.premisesRentalField = value;
            }
        }

        /// <remarks/>
        public string SASicCode
        {
            get
            {
                return this.sASicCodeField;
            }
            set
            {
                this.sASicCodeField = value;
            }
        }

        /// <remarks/>
        public string SASicDescription
        {
            get
            {
                return this.sASicDescriptionField;
            }
            set
            {
                this.sASicDescriptionField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultAffiliations
    {

        private ModuleRequestResponseModuleRequestResultAffiliationsAffiliations affiliationsField;

        [System.Xml.Serialization.XmlElement("Affiliations")]
        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultAffiliationsAffiliations Affiliations
        {
            get
            {
                return this.affiliationsField;
            }
            set
            {
                this.affiliationsField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultAffiliationsAffiliations
    {

        private string majorProductField;

        private string relationshipTypeField;

        private string startDateField;

        private string endDateField;

        private string percentShareField;

        private string commentField;

        private string objectNameField;

        private string objectTownField;

        private string objectCountryField;

        private string iTNumberField;

        private string dunsNumberField;

        private string infoDateField;

        private string registrationNumberField;

        /// <remarks/>
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        public string RelationshipType
        {
            get
            {
                return this.relationshipTypeField;
            }
            set
            {
                this.relationshipTypeField = value;
            }
        }

        /// <remarks/>
        public string StartDate
        {
            get
            {
                return this.startDateField;
            }
            set
            {
                this.startDateField = value;
            }
        }

        /// <remarks/>
        public string EndDate
        {
            get
            {
                return this.endDateField;
            }
            set
            {
                this.endDateField = value;
            }
        }

        /// <remarks/>
        public string PercentShare
        {
            get
            {
                return this.percentShareField;
            }
            set
            {
                this.percentShareField = value;
            }
        }

        /// <remarks/>
        public string Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        public string ObjectName
        {
            get
            {
                return this.objectNameField;
            }
            set
            {
                this.objectNameField = value;
            }
        }

        /// <remarks/>
        public string ObjectTown
        {
            get
            {
                return this.objectTownField;
            }
            set
            {
                this.objectTownField = value;
            }
        }

        /// <remarks/>
        public string ObjectCountry
        {
            get
            {
                return this.objectCountryField;
            }
            set
            {
                this.objectCountryField = value;
            }
        }

        /// <remarks/>
        public string ITNumber
        {
            get
            {
                return this.iTNumberField;
            }
            set
            {
                this.iTNumberField = value;
            }
        }

        /// <remarks/>
        public string DunsNumber
        {
            get
            {
                return this.dunsNumberField;
            }
            set
            {
                this.dunsNumberField = value;
            }
        }

        /// <remarks/>
        public string InfoDate
        {
            get
            {
                return this.infoDateField;
            }
            set
            {
                this.infoDateField = value;
            }
        }

        /// <remarks/>
        public string RegistrationNumber
        {
            get
            {
                return this.registrationNumberField;
            }
            set
            {
                this.registrationNumberField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultBankHistory
    {

        private ModuleRequestResponseModuleRequestResultBankHistoryBankHistory[] bankHistoryField;

        [System.Xml.Serialization.XmlElement("BankHistory")]
        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultBankHistoryBankHistory[] BankHistory
        {
            get
            {
                return this.bankHistoryField;
            }
            set
            {
                this.bankHistoryField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultBankHistoryBankHistory
    {

        private string majorProductField;

        private string accountNoField;

        private string bankNameField;

        private string branchField;

        private string bankCodeField;

        private string bankCodeDescField;

        private object commentField;

        private string amountField;

        private string startDateField;

        private string termsField;

        private string dateField;

        /// <remarks/>
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        public string AccountNo
        {
            get
            {
                return this.accountNoField;
            }
            set
            {
                this.accountNoField = value;
            }
        }

        /// <remarks/>
        public string BankName
        {
            get
            {
                return this.bankNameField;
            }
            set
            {
                this.bankNameField = value;
            }
        }

        /// <remarks/>
        public string Branch
        {
            get
            {
                return this.branchField;
            }
            set
            {
                this.branchField = value;
            }
        }

        /// <remarks/>
        public string BankCode
        {
            get
            {
                return this.bankCodeField;
            }
            set
            {
                this.bankCodeField = value;
            }
        }

        /// <remarks/>
        public string BankCodeDesc
        {
            get
            {
                return this.bankCodeDescField;
            }
            set
            {
                this.bankCodeDescField = value;
            }
        }

        /// <remarks/>
        public object Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        public string Amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        public string StartDate
        {
            get
            {
                return this.startDateField;
            }
            set
            {
                this.startDateField = value;
            }
        }

        /// <remarks/>
        public string Terms
        {
            get
            {
                return this.termsField;
            }
            set
            {
                this.termsField = value;
            }
        }

        /// <remarks/>
        public string Date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultGeneralBankingInfo
    {

        private ModuleRequestResponseModuleRequestResultGeneralBankingInfoGeneralBankingInfo generalBankingInfoField;

        [System.Xml.Serialization.XmlElement("GeneralBankingInfo")]
        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultGeneralBankingInfoGeneralBankingInfo GeneralBankingInfo
        {
            get
            {
                return this.generalBankingInfoField;
            }
            set
            {
                this.generalBankingInfoField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultGeneralBankingInfoGeneralBankingInfo
    {

        private string majorProductField;

        private string accountNoField;

        private string bankNameField;

        private string branchField;

        private object commentField;

        private string startDateField;

        private string infoDateField;

        /// <remarks/>
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        public string AccountNo
        {
            get
            {
                return this.accountNoField;
            }
            set
            {
                this.accountNoField = value;
            }
        }

        /// <remarks/>
        public string BankName
        {
            get
            {
                return this.bankNameField;
            }
            set
            {
                this.bankNameField = value;
            }
        }

        /// <remarks/>
        public string Branch
        {
            get
            {
                return this.branchField;
            }
            set
            {
                this.branchField = value;
            }
        }

        /// <remarks/>
        public object Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        public string StartDate
        {
            get
            {
                return this.startDateField;
            }
            set
            {
                this.startDateField = value;
            }
        }

        /// <remarks/>
        public string InfoDate
        {
            get
            {
                return this.infoDateField;
            }
            set
            {
                this.infoDateField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultBankCodes
    {

        private ModuleRequestResponseModuleRequestResultBankCodesBankCodes bankCodesField;

        [System.Xml.Serialization.XmlElement("BankCodes")]
        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultBankCodesBankCodes BankCodes
        {
            get
            {
                return this.bankCodesField;
            }
            set
            {
                this.bankCodesField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultBankCodesBankCodes
    {

        private string majorProductField;

        private string accountNoField;

        private string bankNameField;

        private string branchField;

        private string bankCodeField;

        private string bankCodeDescField;

        private object commentField;

        private string amountField;

        private string startDateField;

        private string termsField;

        private string dateField;

        /// <remarks/>
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        public string AccountNo
        {
            get
            {
                return this.accountNoField;
            }
            set
            {
                this.accountNoField = value;
            }
        }

        /// <remarks/>
        public string BankName
        {
            get
            {
                return this.bankNameField;
            }
            set
            {
                this.bankNameField = value;
            }
        }

        /// <remarks/>
        public string Branch
        {
            get
            {
                return this.branchField;
            }
            set
            {
                this.branchField = value;
            }
        }

        /// <remarks/>
        public string BankCode
        {
            get
            {
                return this.bankCodeField;
            }
            set
            {
                this.bankCodeField = value;
            }
        }

        /// <remarks/>
        public string BankCodeDesc
        {
            get
            {
                return this.bankCodeDescField;
            }
            set
            {
                this.bankCodeDescField = value;
            }
        }

        /// <remarks/>
        public object Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        public string Amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        public string StartDate
        {
            get
            {
                return this.startDateField;
            }
            set
            {
                this.startDateField = value;
            }
        }

        /// <remarks/>
        public string Terms
        {
            get
            {
                return this.termsField;
            }
            set
            {
                this.termsField = value;
            }
        }

        /// <remarks/>
        public string Date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultTradeHistory
    {

        private ModuleRequestResponseModuleRequestResultTradeHistoryTradeHistory[] tradeHistoryField;

        [System.Xml.Serialization.XmlElement("TradeHistory")]
        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultTradeHistoryTradeHistory[] TradeHistory
        {
            get
            {
                return this.tradeHistoryField;
            }
            set
            {
                this.tradeHistoryField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultTradeHistoryTradeHistory
    {

        private string majorProductField;

        private string dateOfRefField;

        private string yearsKnownField;

        private string monthsKnownField;

        private string creditLimitField;

        private string unlimitedField;

        private string purchasesField;

        private string termsTakenField;

        private string termsGivenField;

        private string discountField;

        private string referenceNameField;

        private string insuranceField;

        private string insuranceDescField;

        private string commentField;

        private string assuredValueField;

        private string obtainedField;

        /// <remarks/>
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        public string DateOfRef
        {
            get
            {
                return this.dateOfRefField;
            }
            set
            {
                this.dateOfRefField = value;
            }
        }

        /// <remarks/>
        public string YearsKnown
        {
            get
            {
                return this.yearsKnownField;
            }
            set
            {
                this.yearsKnownField = value;
            }
        }

        /// <remarks/>
        public string MonthsKnown
        {
            get
            {
                return this.monthsKnownField;
            }
            set
            {
                this.monthsKnownField = value;
            }
        }

        /// <remarks/>
        public string CreditLimit
        {
            get
            {
                return this.creditLimitField;
            }
            set
            {
                this.creditLimitField = value;
            }
        }

        /// <remarks/>
        public string Unlimited
        {
            get
            {
                return this.unlimitedField;
            }
            set
            {
                this.unlimitedField = value;
            }
        }

        /// <remarks/>
        public string Purchases
        {
            get
            {
                return this.purchasesField;
            }
            set
            {
                this.purchasesField = value;
            }
        }

        /// <remarks/>
        public string TermsTaken
        {
            get
            {
                return this.termsTakenField;
            }
            set
            {
                this.termsTakenField = value;
            }
        }

        /// <remarks/>
        public string TermsGiven
        {
            get
            {
                return this.termsGivenField;
            }
            set
            {
                this.termsGivenField = value;
            }
        }

        /// <remarks/>
        public string Discount
        {
            get
            {
                return this.discountField;
            }
            set
            {
                this.discountField = value;
            }
        }

        /// <remarks/>
        public string ReferenceName
        {
            get
            {
                return this.referenceNameField;
            }
            set
            {
                this.referenceNameField = value;
            }
        }

        /// <remarks/>
        public string Insurance
        {
            get
            {
                return this.insuranceField;
            }
            set
            {
                this.insuranceField = value;
            }
        }

        /// <remarks/>
        public string InsuranceDesc
        {
            get
            {
                return this.insuranceDescField;
            }
            set
            {
                this.insuranceDescField = value;
            }
        }

        /// <remarks/>
        public string Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        public string AssuredValue
        {
            get
            {
                return this.assuredValueField;
            }
            set
            {
                this.assuredValueField = value;
            }
        }

        /// <remarks/>
        public string Obtained
        {
            get
            {
                return this.obtainedField;
            }
            set
            {
                this.obtainedField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultBusinessDeedsSummaryDA
    {

        private ModuleRequestResponseModuleRequestResultBusinessDeedsSummaryDABusinessDeedsSummaryDA businessDeedsSummaryDAField;
        [System.Xml.Serialization.XmlElement("BusinessDeedsSummaryDA")]
        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultBusinessDeedsSummaryDABusinessDeedsSummaryDA BusinessDeedsSummaryDA
        {
            get
            {
                return this.businessDeedsSummaryDAField;
            }
            set
            {
                this.businessDeedsSummaryDAField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultBusinessDeedsSummaryDABusinessDeedsSummaryDA
    {

        private string majorProductField;

        private string deedsOfficeField;

        private string numberPropertiesField;

        private string totalValueField;

        private string totalBondField;

        private string totalOwnedField;

        private string commentField;

        private string oldestPropertyDateField;

        private string oldestPropertyValueField;

        /// <remarks/>
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        public string DeedsOffice
        {
            get
            {
                return this.deedsOfficeField;
            }
            set
            {
                this.deedsOfficeField = value;
            }
        }

        /// <remarks/>
        public string NumberProperties
        {
            get
            {
                return this.numberPropertiesField;
            }
            set
            {
                this.numberPropertiesField = value;
            }
        }

        /// <remarks/>
        public string TotalValue
        {
            get
            {
                return this.totalValueField;
            }
            set
            {
                this.totalValueField = value;
            }
        }

        /// <remarks/>
        public string TotalBond
        {
            get
            {
                return this.totalBondField;
            }
            set
            {
                this.totalBondField = value;
            }
        }

        /// <remarks/>
        public string TotalOwned
        {
            get
            {
                return this.totalOwnedField;
            }
            set
            {
                this.totalOwnedField = value;
            }
        }

        /// <remarks/>
        public string Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        public string OldestPropertyDate
        {
            get
            {
                return this.oldestPropertyDateField;
            }
            set
            {
                this.oldestPropertyDateField = value;
            }
        }

        /// <remarks/>
        public string OldestPropertyValue
        {
            get
            {
                return this.oldestPropertyValueField;
            }
            set
            {
                this.oldestPropertyValueField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultCourtRecords
    {

        private ModuleRequestResponseModuleRequestResultCourtRecordsCourtRecord[] courtRecordField;
        [System.Xml.Serialization.XmlElement("CourtRecord")]
        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultCourtRecordsCourtRecord[] CourtRecord
        {
            get
            {
                return this.courtRecordField;
            }
            set
            {
                this.courtRecordField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultCourtRecordsCourtRecord
    {

        private string majorProductField;

        private string numberFoundField;

        private string actionDateField;

        private string typeCodeField;

        private string claimAmountField;

        private string typeDescField;

        private string defendantNameField;

        private string defendantTradeStyleField;

        private string courtRecordAddressField;

        private string suburbField;

        private string cityField;

        private string countryField;

        private string postCodeField;

        private string natureOfDebtField;

        private string defendantDistrictField;

        private string caseNumberField;

        private string courtDistrictField;

        private string courtTypeField;

        private string plaintiffNameField;

        private string attorneyField;

        private string commentField;

        private string abandonDateField;

        private string returnDateField;

        private string messageField;

        private string serialNumberField;

        /// <remarks/>
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        public string NumberFound
        {
            get
            {
                return this.numberFoundField;
            }
            set
            {
                this.numberFoundField = value;
            }
        }

        /// <remarks/>
        public string ActionDate
        {
            get
            {
                return this.actionDateField;
            }
            set
            {
                this.actionDateField = value;
            }
        }

        /// <remarks/>
        public string TypeCode
        {
            get
            {
                return this.typeCodeField;
            }
            set
            {
                this.typeCodeField = value;
            }
        }

        /// <remarks/>
        public string ClaimAmount
        {
            get
            {
                return this.claimAmountField;
            }
            set
            {
                this.claimAmountField = value;
            }
        }

        /// <remarks/>
        public string TypeDesc
        {
            get
            {
                return this.typeDescField;
            }
            set
            {
                this.typeDescField = value;
            }
        }

        /// <remarks/>
        public string DefendantName
        {
            get
            {
                return this.defendantNameField;
            }
            set
            {
                this.defendantNameField = value;
            }
        }

        /// <remarks/>
        public string DefendantTradeStyle
        {
            get
            {
                return this.defendantTradeStyleField;
            }
            set
            {
                this.defendantTradeStyleField = value;
            }
        }

        /// <remarks/>
        public string CourtRecordAddress
        {
            get
            {
                return this.courtRecordAddressField;
            }
            set
            {
                this.courtRecordAddressField = value;
            }
        }

        /// <remarks/>
        public string Suburb
        {
            get
            {
                return this.suburbField;
            }
            set
            {
                this.suburbField = value;
            }
        }

        /// <remarks/>
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        public string Country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        /// <remarks/>
        public string PostCode
        {
            get
            {
                return this.postCodeField;
            }
            set
            {
                this.postCodeField = value;
            }
        }

        /// <remarks/>
        public string NatureOfDebt
        {
            get
            {
                return this.natureOfDebtField;
            }
            set
            {
                this.natureOfDebtField = value;
            }
        }

        /// <remarks/>
        public string DefendantDistrict
        {
            get
            {
                return this.defendantDistrictField;
            }
            set
            {
                this.defendantDistrictField = value;
            }
        }

        /// <remarks/>
        public string CaseNumber
        {
            get
            {
                return this.caseNumberField;
            }
            set
            {
                this.caseNumberField = value;
            }
        }

        /// <remarks/>
        public string CourtDistrict
        {
            get
            {
                return this.courtDistrictField;
            }
            set
            {
                this.courtDistrictField = value;
            }
        }

        /// <remarks/>
        public string CourtType
        {
            get
            {
                return this.courtTypeField;
            }
            set
            {
                this.courtTypeField = value;
            }
        }

        /// <remarks/>
        public string PlaintiffName
        {
            get
            {
                return this.plaintiffNameField;
            }
            set
            {
                this.plaintiffNameField = value;
            }
        }

        /// <remarks/>
        public string Attorney
        {
            get
            {
                return this.attorneyField;
            }
            set
            {
                this.attorneyField = value;
            }
        }

        /// <remarks/>
        public string Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        public string AbandonDate
        {
            get
            {
                return this.abandonDateField;
            }
            set
            {
                this.abandonDateField = value;
            }
        }

        /// <remarks/>
        public string ReturnDate
        {
            get
            {
                return this.returnDateField;
            }
            set
            {
                this.returnDateField = value;
            }
        }

        /// <remarks/>
        public string Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }

        /// <remarks/>
        public string SerialNumber
        {
            get
            {
                return this.serialNumberField;
            }
            set
            {
                this.serialNumberField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultDefaults
    {

        private ModuleRequestResponseModuleRequestResultDefaultsDefault[] defaultField;
        [System.Xml.Serialization.XmlElement("Default")]
        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultDefaultsDefault[] Default
        {
            get
            {
                return this.defaultField;
            }
            set
            {
                this.defaultField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultDefaultsDefault
    {

        private string majorProductField;

        private string numberFoundField;

        private string defaultDateField;

        private string defaultNameField;

        private string defaultTradeStyleField;

        private string defaultAddressField;

        private string suburbField;

        private string cityField;

        private string countryField;

        private string postCodeField;

        private string amountField;

        private string commentField;

        private string subscriberNameField;

        private string supplierNameField;

        private string messageField;

        private string serialNoField;

        private string onBehalfOfField;

        private string statusField;

        /// <remarks/>
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        public string NumberFound
        {
            get
            {
                return this.numberFoundField;
            }
            set
            {
                this.numberFoundField = value;
            }
        }

        /// <remarks/>
        public string DefaultDate
        {
            get
            {
                return this.defaultDateField;
            }
            set
            {
                this.defaultDateField = value;
            }
        }

        /// <remarks/>
        public string DefaultName
        {
            get
            {
                return this.defaultNameField;
            }
            set
            {
                this.defaultNameField = value;
            }
        }

        /// <remarks/>
        public string DefaultTradeStyle
        {
            get
            {
                return this.defaultTradeStyleField;
            }
            set
            {
                this.defaultTradeStyleField = value;
            }
        }

        /// <remarks/>
        public string DefaultAddress
        {
            get
            {
                return this.defaultAddressField;
            }
            set
            {
                this.defaultAddressField = value;
            }
        }

        /// <remarks/>
        public string Suburb
        {
            get
            {
                return this.suburbField;
            }
            set
            {
                this.suburbField = value;
            }
        }

        /// <remarks/>
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        public string Country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        /// <remarks/>
        public string PostCode
        {
            get
            {
                return this.postCodeField;
            }
            set
            {
                this.postCodeField = value;
            }
        }

        /// <remarks/>
        public string Amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        public string Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        public string SubscriberName
        {
            get
            {
                return this.subscriberNameField;
            }
            set
            {
                this.subscriberNameField = value;
            }
        }

        /// <remarks/>
        public string SupplierName
        {
            get
            {
                return this.supplierNameField;
            }
            set
            {
                this.supplierNameField = value;
            }
        }

        /// <remarks/>
        public string Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }

        /// <remarks/>
        public string SerialNo
        {
            get
            {
                return this.serialNoField;
            }
            set
            {
                this.serialNoField = value;
            }
        }

        /// <remarks/>
        public string OnBehalfOf
        {
            get
            {
                return this.onBehalfOfField;
            }
            set
            {
                this.onBehalfOfField = value;
            }
        }

        /// <remarks/>
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultNotarialBonds
    {

        private ModuleRequestResponseModuleRequestResultNotarialBondsNotarialBond notarialBondField;
        [System.Xml.Serialization.XmlElement("NotarialBond")]
        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultNotarialBondsNotarialBond NotarialBond
        {
            get
            {
                return this.notarialBondField;
            }
            set
            {
                this.notarialBondField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultNotarialBondsNotarialBond
    {

        private string majorProductField;

        private string numberFoundField;

        private string bondDateField;

        private string mortgagorField;

        private string tradeStyleField;

        private string addressField;

        private string suburbField;

        private string cityField;

        private string countryField;

        private string postCodeField;

        private string bondNumberField;

        private string bondAmountField;

        private string bondPercentField;

        private string bondDistrictField;

        private string mortgageeField;

        private string dateCancelledField;

        private string messageField;

        private string serialNumberField;

        private string mortgagorDistrictField;

        /// <remarks/>
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        public string NumberFound
        {
            get
            {
                return this.numberFoundField;
            }
            set
            {
                this.numberFoundField = value;
            }
        }

        /// <remarks/>
        public string BondDate
        {
            get
            {
                return this.bondDateField;
            }
            set
            {
                this.bondDateField = value;
            }
        }

        /// <remarks/>
        public string Mortgagor
        {
            get
            {
                return this.mortgagorField;
            }
            set
            {
                this.mortgagorField = value;
            }
        }

        /// <remarks/>
        public string TradeStyle
        {
            get
            {
                return this.tradeStyleField;
            }
            set
            {
                this.tradeStyleField = value;
            }
        }

        /// <remarks/>
        public string Address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>
        public string Suburb
        {
            get
            {
                return this.suburbField;
            }
            set
            {
                this.suburbField = value;
            }
        }

        /// <remarks/>
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        public string Country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        /// <remarks/>
        public string PostCode
        {
            get
            {
                return this.postCodeField;
            }
            set
            {
                this.postCodeField = value;
            }
        }

        /// <remarks/>
        public string BondNumber
        {
            get
            {
                return this.bondNumberField;
            }
            set
            {
                this.bondNumberField = value;
            }
        }

        /// <remarks/>
        public string BondAmount
        {
            get
            {
                return this.bondAmountField;
            }
            set
            {
                this.bondAmountField = value;
            }
        }

        /// <remarks/>
        public string BondPercent
        {
            get
            {
                return this.bondPercentField;
            }
            set
            {
                this.bondPercentField = value;
            }
        }

        /// <remarks/>
        public string BondDistrict
        {
            get
            {
                return this.bondDistrictField;
            }
            set
            {
                this.bondDistrictField = value;
            }
        }

        /// <remarks/>
        public string Mortgagee
        {
            get
            {
                return this.mortgageeField;
            }
            set
            {
                this.mortgageeField = value;
            }
        }

        /// <remarks/>
        public string DateCancelled
        {
            get
            {
                return this.dateCancelledField;
            }
            set
            {
                this.dateCancelledField = value;
            }
        }

        /// <remarks/>
        public string Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }

        /// <remarks/>
        public string SerialNumber
        {
            get
            {
                return this.serialNumberField;
            }
            set
            {
                this.serialNumberField = value;
            }
        }

        /// <remarks/>
        public string MortgagorDistrict
        {
            get
            {
                return this.mortgagorDistrictField;
            }
            set
            {
                this.mortgagorDistrictField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultPrincipals
    {

        private ModuleRequestResponseModuleRequestResultPrincipalsPrincipals[] principalsField;
        [System.Xml.Serialization.XmlElement("Principals")]
        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultPrincipalsPrincipals[] Principals
        {
            get
            {
                return this.principalsField;
            }
            set
            {
                this.principalsField = value;
            }
        }
    }

    /// <remarks/>
    public partial class ModuleRequestResponseModuleRequestResultPrincipalsPrincipals
    {

        private string majorProductField;

        private string surnameField;

        private string forename1Field;

        private string forename2Field;

        private string forename3Field;

        private string iDNumberField;

        private string dateOfBirthField;

        private string commentField;

        private string civilCourtRecordField;

        private string defaultDataField;

        private string noticesOrNotarialBondsField;

        private string confirmedByRegistrarField;

        private string positionField;

        private string dateStartedField;

        private string dateDisputedField;

        private string debtCouncilDateField;

        private string debtCouncilDescField;

        private string deedsField;

        private string infoDateField;

        private string assetNotarialBondField;

        private string empiricaScoreField;

        private object empiricaReasonCodeField;

        private object empiricaReasonDescriptionField;

        private string empiricaExclusionCodeField;

        private string empiricaExclusionDescriptionField;

        /// <remarks/>
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        public string Forename1
        {
            get
            {
                return this.forename1Field;
            }
            set
            {
                this.forename1Field = value;
            }
        }

        /// <remarks/>
        public string Forename2
        {
            get
            {
                return this.forename2Field;
            }
            set
            {
                this.forename2Field = value;
            }
        }

        /// <remarks/>
        public string Forename3
        {
            get
            {
                return this.forename3Field;
            }
            set
            {
                this.forename3Field = value;
            }
        }

        /// <remarks/>
        public string IDNumber
        {
            get
            {
                return this.iDNumberField;
            }
            set
            {
                this.iDNumberField = value;
            }
        }

        /// <remarks/>
        public string DateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>
        public string Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        public string CivilCourtRecord
        {
            get
            {
                return this.civilCourtRecordField;
            }
            set
            {
                this.civilCourtRecordField = value;
            }
        }

        /// <remarks/>
        public string DefaultData
        {
            get
            {
                return this.defaultDataField;
            }
            set
            {
                this.defaultDataField = value;
            }
        }

        /// <remarks/>
        public string NoticesOrNotarialBonds
        {
            get
            {
                return this.noticesOrNotarialBondsField;
            }
            set
            {
                this.noticesOrNotarialBondsField = value;
            }
        }

        /// <remarks/>
        public string ConfirmedByRegistrar
        {
            get
            {
                return this.confirmedByRegistrarField;
            }
            set
            {
                this.confirmedByRegistrarField = value;
            }
        }

        /// <remarks/>
        public string Position
        {
            get
            {
                return this.positionField;
            }
            set
            {
                this.positionField = value;
            }
        }

        /// <remarks/>
        public string DateStarted
        {
            get
            {
                return this.dateStartedField;
            }
            set
            {
                this.dateStartedField = value;
            }
        }

        /// <remarks/>
        public string DateDisputed
        {
            get
            {
                return this.dateDisputedField;
            }
            set
            {
                this.dateDisputedField = value;
            }
        }

        /// <remarks/>
        public string DebtCouncilDate
        {
            get
            {
                return this.debtCouncilDateField;
            }
            set
            {
                this.debtCouncilDateField = value;
            }
        }

        /// <remarks/>
        public string DebtCouncilDesc
        {
            get
            {
                return this.debtCouncilDescField;
            }
            set
            {
                this.debtCouncilDescField = value;
            }
        }

        /// <remarks/>
        public string Deeds
        {
            get
            {
                return this.deedsField;
            }
            set
            {
                this.deedsField = value;
            }
        }

        /// <remarks/>
        public string InfoDate
        {
            get
            {
                return this.infoDateField;
            }
            set
            {
                this.infoDateField = value;
            }
        }

        /// <remarks/>
        public string AssetNotarialBond
        {
            get
            {
                return this.assetNotarialBondField;
            }
            set
            {
                this.assetNotarialBondField = value;
            }
        }

        /// <remarks/>
        public string EmpiricaScore
        {
            get
            {
                return this.empiricaScoreField;
            }
            set
            {
                this.empiricaScoreField = value;
            }
        }

        /// <remarks/>
        public object EmpiricaReasonCode
        {
            get
            {
                return this.empiricaReasonCodeField;
            }
            set
            {
                this.empiricaReasonCodeField = value;
            }
        }

        /// <remarks/>
        public object EmpiricaReasonDescription
        {
            get
            {
                return this.empiricaReasonDescriptionField;
            }
            set
            {
                this.empiricaReasonDescriptionField = value;
            }
        }

        /// <remarks/>
        public string EmpiricaExclusionCode
        {
            get
            {
                return this.empiricaExclusionCodeField;
            }
            set
            {
                this.empiricaExclusionCodeField = value;
            }
        }

        /// <remarks/>
        public string EmpiricaExclusionDescription
        {
            get
            {
                return this.empiricaExclusionDescriptionField;
            }
            set
            {
                this.empiricaExclusionDescriptionField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultPrincipalClearances
    {

        private ModuleRequestResponseModuleRequestResultPrincipalClearancesPrincipalClearance principalClearanceField;
        [System.Xml.Serialization.XmlElement("PrincipalClearance")]
        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultPrincipalClearancesPrincipalClearance PrincipalClearance
        {
            get
            {
                return this.principalClearanceField;
            }
            set
            {
                this.principalClearanceField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultPrincipalClearancesPrincipalClearance
    {

        private string majorProductField;

        private string surnameField;

        private string forename1Field;

        private string forename2Field;

        private string forename3Field;

        private string iDNumberField;

        private string dateOfBirthField;

        private string establishedField;

        private string civilCourtCountField;

        private string defaultCountField;

        private string noticeNotarialCountField;

        private string disputeDateField;

        private string debtCouncilDateField;

        private string debtCouncilDescField;

        /// <remarks/>
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        public string Forename1
        {
            get
            {
                return this.forename1Field;
            }
            set
            {
                this.forename1Field = value;
            }
        }

        /// <remarks/>
        public string Forename2
        {
            get
            {
                return this.forename2Field;
            }
            set
            {
                this.forename2Field = value;
            }
        }

        /// <remarks/>
        public string Forename3
        {
            get
            {
                return this.forename3Field;
            }
            set
            {
                this.forename3Field = value;
            }
        }

        /// <remarks/>
        public string IDNumber
        {
            get
            {
                return this.iDNumberField;
            }
            set
            {
                this.iDNumberField = value;
            }
        }

        /// <remarks/>
        public string DateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>
        public string Established
        {
            get
            {
                return this.establishedField;
            }
            set
            {
                this.establishedField = value;
            }
        }

        /// <remarks/>
        public string CivilCourtCount
        {
            get
            {
                return this.civilCourtCountField;
            }
            set
            {
                this.civilCourtCountField = value;
            }
        }

        /// <remarks/>
        public string DefaultCount
        {
            get
            {
                return this.defaultCountField;
            }
            set
            {
                this.defaultCountField = value;
            }
        }

        /// <remarks/>
        public string NoticeNotarialCount
        {
            get
            {
                return this.noticeNotarialCountField;
            }
            set
            {
                this.noticeNotarialCountField = value;
            }
        }

        /// <remarks/>
        public string DisputeDate
        {
            get
            {
                return this.disputeDateField;
            }
            set
            {
                this.disputeDateField = value;
            }
        }

        /// <remarks/>
        public string DebtCouncilDate
        {
            get
            {
                return this.debtCouncilDateField;
            }
            set
            {
                this.debtCouncilDateField = value;
            }
        }

        /// <remarks/>
        public string DebtCouncilDesc
        {
            get
            {
                return this.debtCouncilDescField;
            }
            set
            {
                this.debtCouncilDescField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultConsumerJudgements
    {

        private ModuleRequestResponseModuleRequestResultConsumerJudgementsConsumerJudgement[] consumerJudgementField;
        [System.Xml.Serialization.XmlElement("ConsumerJudgement")]
        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultConsumerJudgementsConsumerJudgement[] ConsumerJudgement
        {
            get
            {
                return this.consumerJudgementField;
            }
            set
            {
                this.consumerJudgementField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultLinkedCompanies
    {

        private ModuleRequestResponseModuleRequestResultLinkedCompany[] linkedCompanyField;
        [System.Xml.Serialization.XmlElement("LinkedCompanies")]
        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultLinkedCompany[] LinkedCompanies
        {
            get
            {
                return this.linkedCompanyField;
            }
            set
            {
                this.linkedCompanyField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultLinkedBusinessDefaults
    {

        private ModuleRequestResponseModuleRequestResultLinkedBusinessDefault[] linkedbusinessdefaultsField;
        [System.Xml.Serialization.XmlElement("LinkedBusinessDefaults")]
        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultLinkedBusinessDefault[] LinkedBusinessDefaults
        {
            get
            {
                return this.linkedbusinessdefaultsField;
            }
            set
            {
                this.linkedbusinessdefaultsField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultConsumerJudgementsConsumerJudgement
    {

        private string majorProductField;

        private string consumerNumberField;

        private string defendantNameField;

        private string defNoField;

        private string tradeStyleField;

        private string address1Field;

        private string dateOfBirthField;

        private string iDNoField;

        private string plaintiffNameField;

        private string debtCodeField;

        private string debtDescriptionField;

        private string judgementCodeField;

        private string judgementDescriptionField;

        private string courtTypeCodeField;

        private string courtTypeField;

        private string courtNameCodeField;

        private string courtNameField;

        private string caseNumberField;

        private string amountField;

        private string judgementDateField;

        private string captureDateField;

        private string attorneyNameField;

        private string attorneyReferenceField;

        private string attorneyPhoneField;

        private string adminMonthlyAmountField;

        private string adminNoMonthsField;

        private string adminStartDateField;

        private string masterNumberField;

        private string remarksField;

        private string transTypeField;

        private string messageField;

        /// <remarks/>
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        public string ConsumerNumber
        {
            get
            {
                return this.consumerNumberField;
            }
            set
            {
                this.consumerNumberField = value;
            }
        }

        /// <remarks/>
        public string DefendantName
        {
            get
            {
                return this.defendantNameField;
            }
            set
            {
                this.defendantNameField = value;
            }
        }

        /// <remarks/>
        public string DefNo
        {
            get
            {
                return this.defNoField;
            }
            set
            {
                this.defNoField = value;
            }
        }

        /// <remarks/>
        public string TradeStyle
        {
            get
            {
                return this.tradeStyleField;
            }
            set
            {
                this.tradeStyleField = value;
            }
        }

        /// <remarks/>
        public string Address1
        {
            get
            {
                return this.address1Field;
            }
            set
            {
                this.address1Field = value;
            }
        }

        /// <remarks/>
        public string DateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>
        public string IDNo
        {
            get
            {
                return this.iDNoField;
            }
            set
            {
                this.iDNoField = value;
            }
        }

        /// <remarks/>
        public string PlaintiffName
        {
            get
            {
                return this.plaintiffNameField;
            }
            set
            {
                this.plaintiffNameField = value;
            }
        }

        /// <remarks/>
        public string DebtCode
        {
            get
            {
                return this.debtCodeField;
            }
            set
            {
                this.debtCodeField = value;
            }
        }

        /// <remarks/>
        public string DebtDescription
        {
            get
            {
                return this.debtDescriptionField;
            }
            set
            {
                this.debtDescriptionField = value;
            }
        }

        /// <remarks/>
        public string JudgementCode
        {
            get
            {
                return this.judgementCodeField;
            }
            set
            {
                this.judgementCodeField = value;
            }
        }

        /// <remarks/>
        public string JudgementDescription
        {
            get
            {
                return this.judgementDescriptionField;
            }
            set
            {
                this.judgementDescriptionField = value;
            }
        }

        /// <remarks/>
        public string CourtTypeCode
        {
            get
            {
                return this.courtTypeCodeField;
            }
            set
            {
                this.courtTypeCodeField = value;
            }
        }

        /// <remarks/>
        public string CourtType
        {
            get
            {
                return this.courtTypeField;
            }
            set
            {
                this.courtTypeField = value;
            }
        }

        /// <remarks/>
        public string CourtNameCode
        {
            get
            {
                return this.courtNameCodeField;
            }
            set
            {
                this.courtNameCodeField = value;
            }
        }

        /// <remarks/>
        public string CourtName
        {
            get
            {
                return this.courtNameField;
            }
            set
            {
                this.courtNameField = value;
            }
        }

        /// <remarks/>
        public string CaseNumber
        {
            get
            {
                return this.caseNumberField;
            }
            set
            {
                this.caseNumberField = value;
            }
        }

        /// <remarks/>
        public string Amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        public string JudgementDate
        {
            get
            {
                return this.judgementDateField;
            }
            set
            {
                this.judgementDateField = value;
            }
        }

        /// <remarks/>
        public string CaptureDate
        {
            get
            {
                return this.captureDateField;
            }
            set
            {
                this.captureDateField = value;
            }
        }

        /// <remarks/>
        public string AttorneyName
        {
            get
            {
                return this.attorneyNameField;
            }
            set
            {
                this.attorneyNameField = value;
            }
        }

        /// <remarks/>
        public string AttorneyReference
        {
            get
            {
                return this.attorneyReferenceField;
            }
            set
            {
                this.attorneyReferenceField = value;
            }
        }

        /// <remarks/>
        public string AttorneyPhone
        {
            get
            {
                return this.attorneyPhoneField;
            }
            set
            {
                this.attorneyPhoneField = value;
            }
        }

        /// <remarks/>
        public string AdminMonthlyAmount
        {
            get
            {
                return this.adminMonthlyAmountField;
            }
            set
            {
                this.adminMonthlyAmountField = value;
            }
        }

        /// <remarks/>
        public string AdminNoMonths
        {
            get
            {
                return this.adminNoMonthsField;
            }
            set
            {
                this.adminNoMonthsField = value;
            }
        }

        /// <remarks/>
        public string AdminStartDate
        {
            get
            {
                return this.adminStartDateField;
            }
            set
            {
                this.adminStartDateField = value;
            }
        }

        /// <remarks/>
        public string MasterNumber
        {
            get
            {
                return this.masterNumberField;
            }
            set
            {
                this.masterNumberField = value;
            }
        }

        /// <remarks/>
        public string Remarks
        {
            get
            {
                return this.remarksField;
            }
            set
            {
                this.remarksField = value;
            }
        }

        /// <remarks/>
        public string TransType
        {
            get
            {
                return this.transTypeField;
            }
            set
            {
                this.transTypeField = value;
            }
        }

        /// <remarks/>
        public string Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultConsumerDefaults
    {

        private ModuleRequestResponseModuleRequestResultConsumerDefaultsConsumerDefault[] consumerDefaultField;
        [System.Xml.Serialization.XmlElement("ConsumerDefault")]
        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultConsumerDefaultsConsumerDefault[] ConsumerDefault
        {
            get
            {
                return this.consumerDefaultField;
            }
            set
            {
                this.consumerDefaultField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultConsumerDefaultsConsumerDefault
    {

        private string majorProductField;

        private string consumerNoField;

        private string nameField;

        private string fnDefaultField;

        private string defaultDateField;

        private string contactNameField;

        private string contactPhoneCodeField;

        private string contactPhoneNoField;

        private string subscriberNameField;

        private string addressField;

        private string accountField;

        private string subAccountField;

        private string branchCodeField;

        private string amountField;

        private string iDNoField;

        private string dateOfBirthField;

        private string debtCodeField;

        private string debtDescField;

        private string remarksField;

        private string remarksXField;

        private string remarksYField;

        private string transTypeField;

        private string messageField;

        private string amountF700Field;

        private string accountF700Field;

        /// <remarks/>
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        public string ConsumerNo
        {
            get
            {
                return this.consumerNoField;
            }
            set
            {
                this.consumerNoField = value;
            }
        }

        /// <remarks/>
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public string FnDefault
        {
            get
            {
                return this.fnDefaultField;
            }
            set
            {
                this.fnDefaultField = value;
            }
        }

        /// <remarks/>
        public string DefaultDate
        {
            get
            {
                return this.defaultDateField;
            }
            set
            {
                this.defaultDateField = value;
            }
        }

        /// <remarks/>
        public string ContactName
        {
            get
            {
                return this.contactNameField;
            }
            set
            {
                this.contactNameField = value;
            }
        }

        /// <remarks/>
        public string ContactPhoneCode
        {
            get
            {
                return this.contactPhoneCodeField;
            }
            set
            {
                this.contactPhoneCodeField = value;
            }
        }

        /// <remarks/>
        public string ContactPhoneNo
        {
            get
            {
                return this.contactPhoneNoField;
            }
            set
            {
                this.contactPhoneNoField = value;
            }
        }

        /// <remarks/>
        public string SubscriberName
        {
            get
            {
                return this.subscriberNameField;
            }
            set
            {
                this.subscriberNameField = value;
            }
        }

        /// <remarks/>
        public string Address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>
        public string Account
        {
            get
            {
                return this.accountField;
            }
            set
            {
                this.accountField = value;
            }
        }

        /// <remarks/>
        public string SubAccount
        {
            get
            {
                return this.subAccountField;
            }
            set
            {
                this.subAccountField = value;
            }
        }

        /// <remarks/>
        public string BranchCode
        {
            get
            {
                return this.branchCodeField;
            }
            set
            {
                this.branchCodeField = value;
            }
        }

        /// <remarks/>
        public string Amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        public string IDNo
        {
            get
            {
                return this.iDNoField;
            }
            set
            {
                this.iDNoField = value;
            }
        }

        /// <remarks/>
        public string DateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>
        public string DebtCode
        {
            get
            {
                return this.debtCodeField;
            }
            set
            {
                this.debtCodeField = value;
            }
        }

        /// <remarks/>
        public string DebtDesc
        {
            get
            {
                return this.debtDescField;
            }
            set
            {
                this.debtDescField = value;
            }
        }

        /// <remarks/>
        public string Remarks
        {
            get
            {
                return this.remarksField;
            }
            set
            {
                this.remarksField = value;
            }
        }

        /// <remarks/>
        public string RemarksX
        {
            get
            {
                return this.remarksXField;
            }
            set
            {
                this.remarksXField = value;
            }
        }

        /// <remarks/>
        public string RemarksY
        {
            get
            {
                return this.remarksYField;
            }
            set
            {
                this.remarksYField = value;
            }
        }

        /// <remarks/>
        public string TransType
        {
            get
            {
                return this.transTypeField;
            }
            set
            {
                this.transTypeField = value;
            }
        }

        /// <remarks/>
        public string Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }

        /// <remarks/>
        public string AmountF700
        {
            get
            {
                return this.amountF700Field;
            }
            set
            {
                this.amountF700Field = value;
            }
        }

        /// <remarks/>
        public string AccountF700
        {
            get
            {
                return this.accountF700Field;
            }
            set
            {
                this.accountF700Field = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultConsumerNotices
    {

        private ModuleRequestResponseModuleRequestResultConsumerNoticesConsumerNotice[] consumerNoticeField;
        [System.Xml.Serialization.XmlElement("ConsumerNotice")]
        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultConsumerNoticesConsumerNotice[] ConsumerNotice
        {
            get
            {
                return this.consumerNoticeField;
            }
            set
            {
                this.consumerNoticeField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultConsumerNoticesConsumerNotice
    {

        private string majorProductField;

        private string consumerNumberField;

        private string defendantNameField;

        private string defNoField;

        private string address1Field;

        private string dateOfBirthField;

        private string iDNoField;

        private string plaintiffNameField;

        private string noticeCodeField;

        private string noticeDescriptionField;

        private string courtTypeCodeField;

        private string courtTypeDescField;

        private string courtCodeField;

        private string courtNameField;

        private string amountField;

        private string caseNumberField;

        private string noticeDateField;

        private string captureDateField;

        private string masterNoField;

        private string attorneyNameField;

        private string attorneyReferenceField;

        private string attorneyPhoneField;

        private string remarksField;

        private string transTypeField;

        private string messageField;

        /// <remarks/>
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        public string ConsumerNumber
        {
            get
            {
                return this.consumerNumberField;
            }
            set
            {
                this.consumerNumberField = value;
            }
        }

        /// <remarks/>
        public string DefendantName
        {
            get
            {
                return this.defendantNameField;
            }
            set
            {
                this.defendantNameField = value;
            }
        }

        /// <remarks/>
        public string DefNo
        {
            get
            {
                return this.defNoField;
            }
            set
            {
                this.defNoField = value;
            }
        }

        /// <remarks/>
        public string Address1
        {
            get
            {
                return this.address1Field;
            }
            set
            {
                this.address1Field = value;
            }
        }

        /// <remarks/>
        public string DateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>
        public string IDNo
        {
            get
            {
                return this.iDNoField;
            }
            set
            {
                this.iDNoField = value;
            }
        }

        /// <remarks/>
        public string PlaintiffName
        {
            get
            {
                return this.plaintiffNameField;
            }
            set
            {
                this.plaintiffNameField = value;
            }
        }

        /// <remarks/>
        public string NoticeCode
        {
            get
            {
                return this.noticeCodeField;
            }
            set
            {
                this.noticeCodeField = value;
            }
        }

        /// <remarks/>
        public string NoticeDescription
        {
            get
            {
                return this.noticeDescriptionField;
            }
            set
            {
                this.noticeDescriptionField = value;
            }
        }

        /// <remarks/>
        public string CourtTypeCode
        {
            get
            {
                return this.courtTypeCodeField;
            }
            set
            {
                this.courtTypeCodeField = value;
            }
        }

        /// <remarks/>
        public string CourtTypeDesc
        {
            get
            {
                return this.courtTypeDescField;
            }
            set
            {
                this.courtTypeDescField = value;
            }
        }

        /// <remarks/>
        public string CourtCode
        {
            get
            {
                return this.courtCodeField;
            }
            set
            {
                this.courtCodeField = value;
            }
        }

        /// <remarks/>
        public string CourtName
        {
            get
            {
                return this.courtNameField;
            }
            set
            {
                this.courtNameField = value;
            }
        }

        /// <remarks/>
        public string Amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        public string CaseNumber
        {
            get
            {
                return this.caseNumberField;
            }
            set
            {
                this.caseNumberField = value;
            }
        }

        /// <remarks/>
        public string NoticeDate
        {
            get
            {
                return this.noticeDateField;
            }
            set
            {
                this.noticeDateField = value;
            }
        }

        /// <remarks/>
        public string CaptureDate
        {
            get
            {
                return this.captureDateField;
            }
            set
            {
                this.captureDateField = value;
            }
        }

        /// <remarks/>
        public string MasterNo
        {
            get
            {
                return this.masterNoField;
            }
            set
            {
                this.masterNoField = value;
            }
        }

        /// <remarks/>
        public string AttorneyName
        {
            get
            {
                return this.attorneyNameField;
            }
            set
            {
                this.attorneyNameField = value;
            }
        }

        /// <remarks/>
        public string AttorneyReference
        {
            get
            {
                return this.attorneyReferenceField;
            }
            set
            {
                this.attorneyReferenceField = value;
            }
        }

        /// <remarks/>
        public string AttorneyPhone
        {
            get
            {
                return this.attorneyPhoneField;
            }
            set
            {
                this.attorneyPhoneField = value;
            }
        }

        /// <remarks/>
        public string Remarks
        {
            get
            {
                return this.remarksField;
            }
            set
            {
                this.remarksField = value;
            }
        }

        /// <remarks/>
        public string TransType
        {
            get
            {
                return this.transTypeField;
            }
            set
            {
                this.transTypeField = value;
            }
        }

        /// <remarks/>
        public string Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultConsumerNotarialBonds
    {

        private ModuleRequestResponseModuleRequestResultConsumerNotarialBondsConsumerNotarialBonds consumerNotarialBondsField;
        [System.Xml.Serialization.XmlElement("ConsumerNotarialBonds")]
        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultConsumerNotarialBondsConsumerNotarialBonds ConsumerNotarialBonds
        {
            get
            {
                return this.consumerNotarialBondsField;
            }
            set
            {
                this.consumerNotarialBondsField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultConsumerNotarialBondsConsumerNotarialBonds
    {

        private string majorProductField;

        private string consumerNumberField;

        private string defendantNameField;

        private string defendantNumberField;

        private string addressField;

        private string dateOfBirthField;

        private string iDNoField;

        private string plaintiffField;

        private string judgementCodeField;

        private string judgementDescField;

        private string courtTypeField;

        private string courtTypeDescField;

        private string courtCodeField;

        private string courtDescField;

        private string caseNoField;

        private string notarialDateField;

        private string captureDateField;

        private string amountField;

        private string bondPercField;

        private string masterNoField;

        private string remarksField;

        private string transTypeField;

        private string messageField;

        /// <remarks/>
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        public string ConsumerNumber
        {
            get
            {
                return this.consumerNumberField;
            }
            set
            {
                this.consumerNumberField = value;
            }
        }

        /// <remarks/>
        public string DefendantName
        {
            get
            {
                return this.defendantNameField;
            }
            set
            {
                this.defendantNameField = value;
            }
        }

        /// <remarks/>
        public string DefendantNumber
        {
            get
            {
                return this.defendantNumberField;
            }
            set
            {
                this.defendantNumberField = value;
            }
        }

        /// <remarks/>
        public string Address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>
        public string DateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>
        public string IDNo
        {
            get
            {
                return this.iDNoField;
            }
            set
            {
                this.iDNoField = value;
            }
        }

        /// <remarks/>
        public string Plaintiff
        {
            get
            {
                return this.plaintiffField;
            }
            set
            {
                this.plaintiffField = value;
            }
        }

        /// <remarks/>
        public string JudgementCode
        {
            get
            {
                return this.judgementCodeField;
            }
            set
            {
                this.judgementCodeField = value;
            }
        }

        /// <remarks/>
        public string JudgementDesc
        {
            get
            {
                return this.judgementDescField;
            }
            set
            {
                this.judgementDescField = value;
            }
        }

        /// <remarks/>
        public string CourtType
        {
            get
            {
                return this.courtTypeField;
            }
            set
            {
                this.courtTypeField = value;
            }
        }

        /// <remarks/>
        public string CourtTypeDesc
        {
            get
            {
                return this.courtTypeDescField;
            }
            set
            {
                this.courtTypeDescField = value;
            }
        }

        /// <remarks/>
        public string CourtCode
        {
            get
            {
                return this.courtCodeField;
            }
            set
            {
                this.courtCodeField = value;
            }
        }

        /// <remarks/>
        public string CourtDesc
        {
            get
            {
                return this.courtDescField;
            }
            set
            {
                this.courtDescField = value;
            }
        }

        /// <remarks/>
        public string CaseNo
        {
            get
            {
                return this.caseNoField;
            }
            set
            {
                this.caseNoField = value;
            }
        }

        /// <remarks/>
        public string NotarialDate
        {
            get
            {
                return this.notarialDateField;
            }
            set
            {
                this.notarialDateField = value;
            }
        }

        /// <remarks/>
        public string CaptureDate
        {
            get
            {
                return this.captureDateField;
            }
            set
            {
                this.captureDateField = value;
            }
        }

        /// <remarks/>
        public string Amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        public string BondPerc
        {
            get
            {
                return this.bondPercField;
            }
            set
            {
                this.bondPercField = value;
            }
        }

        /// <remarks/>
        public string MasterNo
        {
            get
            {
                return this.masterNoField;
            }
            set
            {
                this.masterNoField = value;
            }
        }

        /// <remarks/>
        public string Remarks
        {
            get
            {
                return this.remarksField;
            }
            set
            {
                this.remarksField = value;
            }
        }

        /// <remarks/>
        public string TransType
        {
            get
            {
                return this.transTypeField;
            }
            set
            {
                this.transTypeField = value;
            }
        }

        /// <remarks/>
        public string Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
 ////<remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultPrincipalLinks
    {

        private ModuleRequestResponseModuleRequestResultPrincipalLinksPrincipalLink[] principalLinkField;
        
          [System.Xml.Serialization.XmlElement("PrincipalLink")]
        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultPrincipalLinksPrincipalLink[] PrincipalLink
        {
            get
            {
                return this.principalLinkField;
            }
            set
            {
                this.principalLinkField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultLinkedCompany
    {
        private string majorProductField;

        private string businessnameField;

        private string businessStatusField;

        private string registeredDateField;
       

        /// <remarks/>
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        public string BusinessName
        {
            get
            {
                return this.businessnameField;
            }
            set
            {
                this.businessnameField = value;
            }
        }

        /// <remarks/>
        public string BusinessStatus
        {
            get
            {
                return this.businessStatusField;
            }
            set
            {
                this.businessStatusField = value;
            }
        }

        /// <remarks/>
        public string RegisteredDate
        {
            get
            {
                return this.registeredDateField;
            }
            set
            {
                this.registeredDateField = value;
            }
        }
        /// <remarks/>
     
       
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultLinkedBusinessDefault
    {
        private string majorProductField;

        private int iTNumberField;

        private string registrationNoField;

        private int judgmentCountField;
        private int defaultCountField;


        /// <remarks/>
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        public int ITNumber
        {
            get
            {
                return this.iTNumberField;
            }
            set
            {
                this.iTNumberField = value;
            }
        }

        /// <remarks/>
        public string RegistrationNo
        {
            get
            {
                return this.registrationNoField;
            }
            set
            {
                this.registrationNoField = value;
            }
        }

        /// <remarks/>
        public int JudgementCount
        {
            get
            {
                return this.judgmentCountField;
            }
            set
            {
                this.judgmentCountField = value;
            }
        }
        /// <remarks/>
        public int DefaultCount
        {
            get
            {
                return this.defaultCountField;
            }
            set
            {
                this.defaultCountField = value;
            }
        }

    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultPrincipalLinksPrincipalLink
    {

        private string majorProductField;

        private string surnameField;

        private string forename1Field;

        private string forename2Field;

        private string forename3Field;

        private string iDNumberField;

        private string dateOfBirthField;

        private string establishedField;

        private string consumerNoField;

        private ModuleRequestResponseModuleRequestResultLinkedCompanies linkedCompaniesField;

        private ModuleRequestResponseModuleRequestResultLinkedBusinessDefaults linkedBusinessDefaultsField;

        /// <remarks/>
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        public string Forename1
        {
            get
            {
                return this.forename1Field;
            }
            set
            {
                this.forename1Field = value;
            }
        }

        /// <remarks/>
        public string Forename2
        {
            get
            {
                return this.forename2Field;
            }
            set
            {
                this.forename2Field = value;
            }
        }

        /// <remarks/>
        public string Forename3
        {
            get
            {
                return this.forename3Field;
            }
            set
            {
                this.forename3Field = value;
            }
        }

        /// <remarks/>
        public string IDNumber
        {
            get
            {
                return this.iDNumberField;
            }
            set
            {
                this.iDNumberField = value;
            }
        }

        /// <remarks/>
        public string DateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>
        public string Established
        {
            get
            {
                return this.establishedField;
            }
            set
            {
                this.establishedField = value;
            }
        }

        /// <remarks/>
        public string ConsumerNo
        {
            get
            {
                return this.consumerNoField;
            }
            set
            {
                this.consumerNoField = value;
            }
        }

         
         //[System.Xml.Serialization.XmlArrayItemAttribute("LinkedCompanies")]
        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultLinkedCompanies LinkedCompanies
        {
            get
            {
                return this.linkedCompaniesField;
            }
            set
            {
                this.linkedCompaniesField = value;
            }
        }
        
        //[System.Xml.Serialization.XmlArrayItemAttribute("LinkedBusinessDefaults")]
        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultLinkedBusinessDefaults LinkedBusinessDefaults
        {
            get
            {
                return this.linkedBusinessDefaultsField;
            }
            set
            {
                this.linkedBusinessDefaultsField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultConsumerInfoNO04
    {

        private string recordSeqField;

        private string partField;

        private string partSeqField;

        private string consumerNoField;

        private string surnameField;

        private string forename1Field;

        private string forename2Field;

        private string forename3Field;

        private string titleField;

        private string genderField;

        private string nameInfoDateField;

        private string dateOfBirthField;

        private string identityNo1Field;

        private string identityNo2Field;

        private string maritalStatusCodeField;

        private string maritalStatusDescField;

        private string dependantsField;

        private string spouseName1Field;

        private string spouseName2Field;

        private string telephoneNumbersField;

        private string deceasedDateField;

        /// <remarks/>
        public string RecordSeq
        {
            get
            {
                return this.recordSeqField;
            }
            set
            {
                this.recordSeqField = value;
            }
        }

        /// <remarks/>
        public string Part
        {
            get
            {
                return this.partField;
            }
            set
            {
                this.partField = value;
            }
        }

        /// <remarks/>
        public string PartSeq
        {
            get
            {
                return this.partSeqField;
            }
            set
            {
                this.partSeqField = value;
            }
        }

        /// <remarks/>
        public string ConsumerNo
        {
            get
            {
                return this.consumerNoField;
            }
            set
            {
                this.consumerNoField = value;
            }
        }

        /// <remarks/>
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        public string Forename1
        {
            get
            {
                return this.forename1Field;
            }
            set
            {
                this.forename1Field = value;
            }
        }

        /// <remarks/>
        public string Forename2
        {
            get
            {
                return this.forename2Field;
            }
            set
            {
                this.forename2Field = value;
            }
        }

        /// <remarks/>
        public string Forename3
        {
            get
            {
                return this.forename3Field;
            }
            set
            {
                this.forename3Field = value;
            }
        }

        /// <remarks/>
        public string Title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>
        public string Gender
        {
            get
            {
                return this.genderField;
            }
            set
            {
                this.genderField = value;
            }
        }

        /// <remarks/>
        public string NameInfoDate
        {
            get
            {
                return this.nameInfoDateField;
            }
            set
            {
                this.nameInfoDateField = value;
            }
        }

        /// <remarks/>
        public string DateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>
        public string IdentityNo1
        {
            get
            {
                return this.identityNo1Field;
            }
            set
            {
                this.identityNo1Field = value;
            }
        }

        /// <remarks/>
        public string IdentityNo2
        {
            get
            {
                return this.identityNo2Field;
            }
            set
            {
                this.identityNo2Field = value;
            }
        }

        /// <remarks/>
        public string MaritalStatusCode
        {
            get
            {
                return this.maritalStatusCodeField;
            }
            set
            {
                this.maritalStatusCodeField = value;
            }
        }

        /// <remarks/>
        public string MaritalStatusDesc
        {
            get
            {
                return this.maritalStatusDescField;
            }
            set
            {
                this.maritalStatusDescField = value;
            }
        }

        /// <remarks/>
        public string Dependants
        {
            get
            {
                return this.dependantsField;
            }
            set
            {
                this.dependantsField = value;
            }
        }

        /// <remarks/>
        public string SpouseName1
        {
            get
            {
                return this.spouseName1Field;
            }
            set
            {
                this.spouseName1Field = value;
            }
        }

        /// <remarks/>
        public string SpouseName2
        {
            get
            {
                return this.spouseName2Field;
            }
            set
            {
                this.spouseName2Field = value;
            }
        }

        /// <remarks/>
        public string TelephoneNumbers
        {
            get
            {
                return this.telephoneNumbersField;
            }
            set
            {
                this.telephoneNumbersField = value;
            }
        }

        /// <remarks/>
        public string DeceasedDate
        {
            get
            {
                return this.deceasedDateField;
            }
            set
            {
                this.deceasedDateField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultBCCCX01
    {

        private ModuleRequestResponseModuleRequestResultBCCCX01BCCCX01 bCCCX01Field;
        [System.Xml.Serialization.XmlElement("BCCCX01")]
        /// <remarks/>
        public ModuleRequestResponseModuleRequestResultBCCCX01BCCCX01 BCCCX01
        {
            get
            {
                return this.bCCCX01Field;
            }
            set
            {
                this.bCCCX01Field = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ModuleRequestResponseModuleRequestResultBCCCX01BCCCX01
    {

        private string consumerNoField;

        private object bCCsField;

        /// <remarks/>
        public string ConsumerNo
        {
            get
            {
                return this.consumerNoField;
            }
            set
            {
                this.consumerNoField = value;
            }
        }

        /// <remarks/>
        public object BCCs
        {
            get
            {
                return this.bCCsField;
            }
            set
            {
                this.bCCsField = value;
            }
        }
    }

    

}
