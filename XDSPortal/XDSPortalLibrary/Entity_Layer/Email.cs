﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace XDSPortalLibrary.Entity_Layer
{
    public  class Email
    {
        public Email() { }

        private int _EmailLogID = 0;
        private int _SubsriberEnquiryResultID = 0;
        private int _SubscriberEmailSettingsID = 0;
        private string _ToEmailAddress = string.Empty;
        private string _FromEmailAddress = string.Empty;
        private string _CCEmailAddress = string.Empty;
        private string _BCCEmailaddress = string.Empty;
        private string _FromName = string.Empty;
        private string _Toname = string.Empty;
        private string _Subject = string.Empty;
        private bool _IncludeAttachment = false;
        private int _NoOfAttachments = 0;
        private string _MessageBody = string.Empty;
        private bool _IsBodyhtml = false;
        private bool _IsBodyTemplate = false;
        private byte[] _BodyStream = null;
        private string _EmailStatus = "Q";
        private string _ErrorDescription = string.Empty;
        private string _StatusInd = string.Empty;
        private string _CreatedbyUser = string.Empty;
        private DateTime _CreatedOnDate = DateTime.Now;
        private string _ChangedByUser = string.Empty;
        private DateTime _ChangedonDate = DateTime.Now;
        private string _TemplatePath = string.Empty;
        private string _smtpservername = string.Empty;
        private string _smtpusername = string.Empty;
        private string _smtpPassword = string.Empty;
        private int _port = 0;

        public int EmailLogID {get { return this._EmailLogID; } set { this._EmailLogID = value; } }
        public int SubsriberEnquiryResultID { get { return this._SubsriberEnquiryResultID; } set { this._SubsriberEnquiryResultID = value; } }
        public int SubscriberEmailSettingsID { get { return this._SubscriberEmailSettingsID; } set { this._SubscriberEmailSettingsID = value; } }
        public string ToEmailAddress { get { return this._ToEmailAddress; } set { this._ToEmailAddress = value; } }
        public string FromEmailAddress { get { return this._FromEmailAddress; } set { this._FromEmailAddress = value; } }
        public string CCEmailAddress { get { return this._CCEmailAddress; } set { this._CCEmailAddress = value; } }
        public string BCCEmailaddress { get { return this._BCCEmailaddress; } set { this._BCCEmailaddress = value; } }
        public string FromName { get { return this._FromName; } set { this._FromName = value; } }
        public string Toname { get { return this._Toname; } set { this._Toname = value; } }
        public string Subject { get { return this._Subject; } set { this._Subject = value; } }
        public bool IncludeAttachment { get { return this._IncludeAttachment; } set { this._IncludeAttachment = value; } }
        public int NoOfAttachments { get { return this._NoOfAttachments; } set { this._NoOfAttachments = value; } }
        public string MessageBody { get { return this._MessageBody; } set { this._MessageBody = value; } }
        public bool IsBodyhtml { get { return this._IsBodyhtml; } set { this._IsBodyhtml = value; } }
        public bool IsBodyTemplate { get { return this._IsBodyTemplate; } set { this._IsBodyTemplate = value; } }
        public byte[] BodyStream { get { return this._BodyStream; } set { this._BodyStream = value; } }
        public string EmailStatus { get { return this._EmailStatus; } set { this._EmailStatus = value; } }
        public string ErrorDescription { get { return this._ErrorDescription; } set { this._ErrorDescription = value; } }
        public string StatusInd { get { return this._StatusInd; } set { this._StatusInd = value; } }
        public string CreatedbyUser { get { return this._CreatedbyUser; } set { this._CreatedbyUser = value; } }
        public DateTime CreatedOnDate { get { return this._CreatedOnDate; } set { this._CreatedOnDate = value; } }
        public string ChangedByUser { get { return this._ChangedByUser; } set { this._ChangedByUser = value; } }
        public DateTime ChangedonDate { get { return this._ChangedonDate; } set { this._ChangedonDate = value; } }
        public string TemplatePath { get { return this._TemplatePath; } set { this._TemplatePath = value; } }
        public string SMTPServerName { get { return this._smtpservername; } set { this._smtpservername = value; } }
        public string SMTPPassword { get { return this._smtpPassword; } set { this._smtpPassword = value; } }
        public string SMTPUsername { get { return this._smtpusername; } set { this._smtpusername = value; } }
        public int Port { get { return this._port; } set { this._port = value; } }


        [XmlArray("PersonalQuestions")]
        [XmlArrayItem("PersonalQuestionsDocument")]
        public AttachmentList[] Attachmets;
        
    }

    public class AttachmentList
    {
        public string AttachmentName = string.Empty;
        public string AttachmentType = string.Empty;
        public byte[] AttachemnetContent = null;
        public string CreatedbyUser = string.Empty;
        public string ChangedbyUser = string.Empty;
    }
}
