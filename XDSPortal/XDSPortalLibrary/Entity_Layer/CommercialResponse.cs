﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDSPortalLibrary.Entity_Layer.MTN
{
    public partial class Extension
    {

        private string nameField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>





    public partial class Aggregate
    {

        private string nameField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>





    public partial class CX01
    {

        private string nameField;

        private string valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>





    public partial class BusinessBenchmarkDetail
    {

        private string bCA_CategoryField;

        private string bCA_Var_IdField;

        private string bCA_Subj_valField;

        private string bCA_Ltd_ValField;

        private string bCA_PtyLtd_ValField;

        private string bCA_CC_ValField;

        private string bCA_Other_valField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string BCA_Category
        {
            get
            {
                return this.bCA_CategoryField;
            }
            set
            {
                this.bCA_CategoryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string BCA_Var_Id
        {
            get
            {
                return this.bCA_Var_IdField;
            }
            set
            {
                this.bCA_Var_IdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string BCA_Subj_val
        {
            get
            {
                return this.bCA_Subj_valField;
            }
            set
            {
                this.bCA_Subj_valField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string BCA_Ltd_Val
        {
            get
            {
                return this.bCA_Ltd_ValField;
            }
            set
            {
                this.bCA_Ltd_ValField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string BCA_PtyLtd_Val
        {
            get
            {
                return this.bCA_PtyLtd_ValField;
            }
            set
            {
                this.bCA_PtyLtd_ValField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string BCA_CC_Val
        {
            get
            {
                return this.bCA_CC_ValField;
            }
            set
            {
                this.bCA_CC_ValField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string BCA_Other_val
        {
            get
            {
                return this.bCA_Other_valField;
            }
            set
            {
                this.bCA_Other_valField = value;
            }
        }
    }

    /// <remarks/>





    public partial class FinanceAmounts
    {

        private string amountCurrentField;

        private string amountBeforeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string AmountCurrent
        {
            get
            {
                return this.amountCurrentField;
            }
            set
            {
                this.amountCurrentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string AmountBefore
        {
            get
            {
                return this.amountBeforeField;
            }
            set
            {
                this.amountBeforeField = value;
            }
        }
    }

    /// <remarks/>





    public partial class DebtorInfo
    {

        private string debtorNameField;

        private string debtorDescriptionField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string DebtorName
        {
            get
            {
                return this.debtorNameField;
            }
            set
            {
                this.debtorNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string DebtorDescription
        {
            get
            {
                return this.debtorDescriptionField;
            }
            set
            {
                this.debtorDescriptionField = value;
            }
        }
    }

    /// <remarks/>





    public partial class Module
    {

        private string productCodeField;

        private string productDescField;

        private ProductType productTypeField;

        private float hoursField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string ProductCode
        {
            get
            {
                return this.productCodeField;
            }
            set
            {
                this.productCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string ProductDesc
        {
            get
            {
                return this.productDescField;
            }
            set
            {
                this.productDescField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public ProductType ProductType
        {
            get
            {
                return this.productTypeField;
            }
            set
            {
                this.productTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public float Hours
        {
            get
            {
                return this.hoursField;
            }
            set
            {
                this.hoursField = value;
            }
        }
    }

    /// <remarks/>



    public enum ProductType
    {

        /// <remarks/>
        View,

        /// <remarks/>
        Investigate,
    }

    /// <remarks/>





    public partial class SicDetails
    {

        private string sASicCodeField;

        private string sicCodeField;

        private string sicDescriptionField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string SASicCode
        {
            get
            {
                return this.sASicCodeField;
            }
            set
            {
                this.sASicCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string SicCode
        {
            get
            {
                return this.sicCodeField;
            }
            set
            {
                this.sicCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string SicDescription
        {
            get
            {
                return this.sicDescriptionField;
            }
            set
            {
                this.sicDescriptionField = value;
            }
        }
    }

    /// <remarks/>





    public partial class CapitalInfo
    {

        private string capitalChangedFromField;

        private string capitalChangedToField;

        private string capitalChangedDateField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string CapitalChangedFrom
        {
            get
            {
                return this.capitalChangedFromField;
            }
            set
            {
                this.capitalChangedFromField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string CapitalChangedTo
        {
            get
            {
                return this.capitalChangedToField;
            }
            set
            {
                this.capitalChangedToField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string CapitalChangedDate
        {
            get
            {
                return this.capitalChangedDateField;
            }
            set
            {
                this.capitalChangedDateField = value;
            }
        }
    }

    /// <remarks/>





    public partial class ShareInfo
    {

        private string shareTypeField;

        private string shareNumberField;

        private string shareValueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string ShareType
        {
            get
            {
                return this.shareTypeField;
            }
            set
            {
                this.shareTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string ShareNumber
        {
            get
            {
                return this.shareNumberField;
            }
            set
            {
                this.shareNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string ShareValue
        {
            get
            {
                return this.shareValueField;
            }
            set
            {
                this.shareValueField = value;
            }
        }
    }

    /// <remarks/>





    public partial class SegmentDescription
    {

        private string infoTypeField;

        private string infoDescriptionField;

        private string dateField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string InfoType
        {
            get
            {
                return this.infoTypeField;
            }
            set
            {
                this.infoTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string InfoDescription
        {
            get
            {
                return this.infoDescriptionField;
            }
            set
            {
                this.infoDescriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string Date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }
    }

    /// <remarks/>





    public partial class AccountSynopsisDetail
    {

        private string supplierField;

        private string termField;

        private string termExtField;

        private string statusField;

        private string accountNumberField;

        private string creditLimitField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string Supplier
        {
            get
            {
                return this.supplierField;
            }
            set
            {
                this.supplierField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string Term
        {
            get
            {
                return this.termField;
            }
            set
            {
                this.termField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string TermExt
        {
            get
            {
                return this.termExtField;
            }
            set
            {
                this.termExtField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string AccountNumber
        {
            get
            {
                return this.accountNumberField;
            }
            set
            {
                this.accountNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string CreditLimit
        {
            get
            {
                return this.creditLimitField;
            }
            set
            {
                this.creditLimitField = value;
            }
        }
    }

    /// <remarks/>





    public partial class AgeAnalysisDetail
    {

        private string supplierField;

        private string termField;

        private string creditLimitField;

        private string totalCreditLimitField;

        private string totalOverduePercentageField;

        private string totalCurrentPercentageField;

        private string days30PercentageField;

        private string days60PercentageField;

        private string days90PercentageField;

        private string days120PercentageField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string Supplier
        {
            get
            {
                return this.supplierField;
            }
            set
            {
                this.supplierField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string Term
        {
            get
            {
                return this.termField;
            }
            set
            {
                this.termField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string CreditLimit
        {
            get
            {
                return this.creditLimitField;
            }
            set
            {
                this.creditLimitField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string TotalCreditLimit
        {
            get
            {
                return this.totalCreditLimitField;
            }
            set
            {
                this.totalCreditLimitField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string TotalOverduePercentage
        {
            get
            {
                return this.totalOverduePercentageField;
            }
            set
            {
                this.totalOverduePercentageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string TotalCurrentPercentage
        {
            get
            {
                return this.totalCurrentPercentageField;
            }
            set
            {
                this.totalCurrentPercentageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string Days30Percentage
        {
            get
            {
                return this.days30PercentageField;
            }
            set
            {
                this.days30PercentageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string Days60Percentage
        {
            get
            {
                return this.days60PercentageField;
            }
            set
            {
                this.days60PercentageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string Days90Percentage
        {
            get
            {
                return this.days90PercentageField;
            }
            set
            {
                this.days90PercentageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string Days120Percentage
        {
            get
            {
                return this.days120PercentageField;
            }
            set
            {
                this.days120PercentageField = value;
            }
        }
    }

    /// <remarks/>





    public partial class PaymentHistoryDetail
    {

        private string periodField;

        private string numberSuppliersField;

        private string creditLimitField;

        private string totalCreditLimitField;

        private string totalOverduePercentageField;

        private string totalCurrentPercentageField;

        private string days30PercentageField;

        private string days60PercentageField;

        private string days90PercentageField;

        private string days120PercentageField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string Period
        {
            get
            {
                return this.periodField;
            }
            set
            {
                this.periodField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string NumberSuppliers
        {
            get
            {
                return this.numberSuppliersField;
            }
            set
            {
                this.numberSuppliersField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string CreditLimit
        {
            get
            {
                return this.creditLimitField;
            }
            set
            {
                this.creditLimitField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string TotalCreditLimit
        {
            get
            {
                return this.totalCreditLimitField;
            }
            set
            {
                this.totalCreditLimitField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string TotalOverduePercentage
        {
            get
            {
                return this.totalOverduePercentageField;
            }
            set
            {
                this.totalOverduePercentageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string TotalCurrentPercentage
        {
            get
            {
                return this.totalCurrentPercentageField;
            }
            set
            {
                this.totalCurrentPercentageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string Days30Percentage
        {
            get
            {
                return this.days30PercentageField;
            }
            set
            {
                this.days30PercentageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string Days60Percentage
        {
            get
            {
                return this.days60PercentageField;
            }
            set
            {
                this.days60PercentageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string Days90Percentage
        {
            get
            {
                return this.days90PercentageField;
            }
            set
            {
                this.days90PercentageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string Days120Percentage
        {
            get
            {
                return this.days120PercentageField;
            }
            set
            {
                this.days120PercentageField = value;
            }
        }
    }

    /// <remarks/>





    public partial class CreditBand
    {

        private string invoiceAmountField;

        private string noOfInvoicesField;

        private string withinTermsField;

        private string days1to10Field;

        private string days11to30Field;

        private string days31to60Field;

        private string days61to90Field;

        private string days91to120Field;

        private string days120toPlusField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string InvoiceAmount
        {
            get
            {
                return this.invoiceAmountField;
            }
            set
            {
                this.invoiceAmountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string NoOfInvoices
        {
            get
            {
                return this.noOfInvoicesField;
            }
            set
            {
                this.noOfInvoicesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string WithinTerms
        {
            get
            {
                return this.withinTermsField;
            }
            set
            {
                this.withinTermsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string Days1to10
        {
            get
            {
                return this.days1to10Field;
            }
            set
            {
                this.days1to10Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string Days11to30
        {
            get
            {
                return this.days11to30Field;
            }
            set
            {
                this.days11to30Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string Days31to60
        {
            get
            {
                return this.days31to60Field;
            }
            set
            {
                this.days31to60Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string Days61to90
        {
            get
            {
                return this.days61to90Field;
            }
            set
            {
                this.days61to90Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string Days91to120
        {
            get
            {
                return this.days91to120Field;
            }
            set
            {
                this.days91to120Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string Days120toPlus
        {
            get
            {
                return this.days120toPlusField;
            }
            set
            {
                this.days120toPlusField = value;
            }
        }
    }

    /// <remarks/>





    public partial class Vehicle
    {

        private string quantityField;

        private string descriptionField;

        private string noLeasedField;

        private string noOwnedField;

        private string noHPField;

        private string infoDateField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string Quantity
        {
            get
            {
                return this.quantityField;
            }
            set
            {
                this.quantityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string NoLeased
        {
            get
            {
                return this.noLeasedField;
            }
            set
            {
                this.noLeasedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string NoOwned
        {
            get
            {
                return this.noOwnedField;
            }
            set
            {
                this.noOwnedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string NoHP
        {
            get
            {
                return this.noHPField;
            }
            set
            {
                this.noHPField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string InfoDate
        {
            get
            {
                return this.infoDateField;
            }
            set
            {
                this.infoDateField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(AccountVerificationVH))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(AggregateCW))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Affiliations))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BankCodes))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BankingDetailSummary))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BankHistory))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BankReport))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Branch))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BCCCX01))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BusinessAdverseSummary))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BusinessDeedsDI))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BusinessDeedsComprehensivePB))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BusinessDeedsSummaryDA))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BusinessDeedsComprehensivePW))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BusinessBenchmarkBB))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BusinessNotarialBonds))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BusinessPlusPrincipalSummary))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(CancelledTicket))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(CapitalEmployed))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(CommercialDisputeResponse))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ConsumerBusinessEnquiry))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ConsumerDefault))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ConsumerEnquiry))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ConsumerHeader))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ConsumerHeaderC1))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ConsumerInfoNO04))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ConsumerJudgement))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ConsumerNotarialBonds))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ConsumerNotice))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ConsumerTraceAlert))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(CourtRecord))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(CurrentAsset))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(CurrentLiabilities))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(DeedHistory))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(DeedsMultipleBond))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Default))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(DynamicRating))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Empirica))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(EmpiricaE1))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(EmpiricaEM04))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(EmpOfCapital))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(EnquiryHistory))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(EnquirySummary))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(FirstResponse))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(FinanceHeader))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(FinanceData))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(FinanceDataFE))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(FinanceDataFF))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(FinancialRatios))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(GeneralBankingInfo))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Header))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(LinkedBusinessHeader))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(MailboxRetrieveList))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ModuleAvailabilityResponse))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Names))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(NotarialBond))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Observation))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ObservationCont))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Operation))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(AdditionalOperations))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(OtherOperation))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Principal))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PrincipalDetailEmpirica))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PrincipalArchive))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PrincipalArchiveP5))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PrincipalClearance))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PrincipalDeedsComprehensiveCA))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PrincipalDeedsComprehensiveCV))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PrincipalDeedsSummaryCO))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PrincipalDeedsSummaryP8))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PrincipalFirstResponse))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PrincipalIDV))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PrincipalLink))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(LinkedCompany))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(LinkedBusinessDefault))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(PrincipalNotarialBonds))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(RegisteredPrincipal))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(RegistrationDetails))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(RegistrationDetailsExtended))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(BusinessRescueDetail))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(SearchResponse))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(SMEAssessment))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(SegmentDescriptions))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(TradeAccountSynopsis))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(TradeAgeAnalysis))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(TradeHistory))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(TradePaymentHistory))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(TradeReference))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(TradeReferenceSummary))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Tradex))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(TicketStatus))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(UnmatchedCourtRecord))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(UnmatchedDefault))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(UnmatchedNotarialBond))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(Vehicles))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(VeriCheque))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ComplianceIndicatorLI))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(AccountVerificationVA))]





    public partial class BureauSegment
    {
    }

    /// <remarks/>





    public partial class AccountVerificationVH : BureauSegment
    {

        private string majorProductField;

        private string bankNameField;

        private string branchCodeField;

        private string branchNameField;

        private string accountNumberField;

        private string accountTypeField;

        private string accountHolderField;

        private string startDateField;

        private string verifiedDatedField;

        private string accountDormantField;

        private string accountOpen3MonthsField;

        private string accountAcceptsDebitsField;

        private string accountAcceptsCreditsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string BankName
        {
            get
            {
                return this.bankNameField;
            }
            set
            {
                this.bankNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string BranchCode
        {
            get
            {
                return this.branchCodeField;
            }
            set
            {
                this.branchCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string BranchName
        {
            get
            {
                return this.branchNameField;
            }
            set
            {
                this.branchNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string AccountNumber
        {
            get
            {
                return this.accountNumberField;
            }
            set
            {
                this.accountNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string AccountType
        {
            get
            {
                return this.accountTypeField;
            }
            set
            {
                this.accountTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string AccountHolder
        {
            get
            {
                return this.accountHolderField;
            }
            set
            {
                this.accountHolderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string StartDate
        {
            get
            {
                return this.startDateField;
            }
            set
            {
                this.startDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string VerifiedDated
        {
            get
            {
                return this.verifiedDatedField;
            }
            set
            {
                this.verifiedDatedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string AccountDormant
        {
            get
            {
                return this.accountDormantField;
            }
            set
            {
                this.accountDormantField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string AccountOpen3Months
        {
            get
            {
                return this.accountOpen3MonthsField;
            }
            set
            {
                this.accountOpen3MonthsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string AccountAcceptsDebits
        {
            get
            {
                return this.accountAcceptsDebitsField;
            }
            set
            {
                this.accountAcceptsDebitsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string AccountAcceptsCredits
        {
            get
            {
                return this.accountAcceptsCreditsField;
            }
            set
            {
                this.accountAcceptsCreditsField = value;
            }
        }
    }

    /// <remarks/>





    public partial class AggregateCW : BureauSegment
    {

        private string majorProductField;

        private Aggregate[] aggregatesField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 1)]
        public Aggregate[] Aggregates
        {
            get
            {
                return this.aggregatesField;
            }
            set
            {
                this.aggregatesField = value;
            }
        }
    }

    /// <remarks/>





    public partial class Affiliations : BureauSegment
    {

        private string majorProductField;

        private string relationshipTypeField;

        private string startDateField;

        private string endDateField;

        private string percentShareField;

        private string commentField;

        private string objectNameField;

        private string objectTownField;

        private string objectCountryField;

        private string iTNumberField;

        private string dunsNumberField;

        private string infoDateField;

        private string registrationNumberField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string RelationshipType
        {
            get
            {
                return this.relationshipTypeField;
            }
            set
            {
                this.relationshipTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string StartDate
        {
            get
            {
                return this.startDateField;
            }
            set
            {
                this.startDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string EndDate
        {
            get
            {
                return this.endDateField;
            }
            set
            {
                this.endDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string PercentShare
        {
            get
            {
                return this.percentShareField;
            }
            set
            {
                this.percentShareField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string ObjectName
        {
            get
            {
                return this.objectNameField;
            }
            set
            {
                this.objectNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string ObjectTown
        {
            get
            {
                return this.objectTownField;
            }
            set
            {
                this.objectTownField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string ObjectCountry
        {
            get
            {
                return this.objectCountryField;
            }
            set
            {
                this.objectCountryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string ITNumber
        {
            get
            {
                return this.iTNumberField;
            }
            set
            {
                this.iTNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string DunsNumber
        {
            get
            {
                return this.dunsNumberField;
            }
            set
            {
                this.dunsNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string InfoDate
        {
            get
            {
                return this.infoDateField;
            }
            set
            {
                this.infoDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string RegistrationNumber
        {
            get
            {
                return this.registrationNumberField;
            }
            set
            {
                this.registrationNumberField = value;
            }
        }
    }

    /// <remarks/>





    public partial class BankCodes : BureauSegment
    {

        private string majorProductField;

        private string accountNoField;

        private string bankNameField;

        private string branchField;

        private string bankCodeField;

        private string bankCodeDescField;

        private string[] commentField;

        private string amountField;

        private string startDateField;

        private string termsField;

        private string dateField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string AccountNo
        {
            get
            {
                return this.accountNoField;
            }
            set
            {
                this.accountNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string BankName
        {
            get
            {
                return this.bankNameField;
            }
            set
            {
                this.bankNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string Branch
        {
            get
            {
                return this.branchField;
            }
            set
            {
                this.branchField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string BankCode
        {
            get
            {
                return this.bankCodeField;
            }
            set
            {
                this.bankCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string BankCodeDesc
        {
            get
            {
                return this.bankCodeDescField;
            }
            set
            {
                this.bankCodeDescField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 6)]
        public string[] Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string Amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string StartDate
        {
            get
            {
                return this.startDateField;
            }
            set
            {
                this.startDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string Terms
        {
            get
            {
                return this.termsField;
            }
            set
            {
                this.termsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string Date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }
    }

    /// <remarks/>





    public partial class BankingDetailSummary : BureauSegment
    {

        private string majorProductField;

        private string accountNumberField;

        private string bankNameField;

        private string bankBranchField;

        private string bankCodeField;

        private string bankCodeDescriptionField;

        private string amountField;

        private string startDateField;

        private string termsField;

        private string dateField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string AccountNumber
        {
            get
            {
                return this.accountNumberField;
            }
            set
            {
                this.accountNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string BankName
        {
            get
            {
                return this.bankNameField;
            }
            set
            {
                this.bankNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string BankBranch
        {
            get
            {
                return this.bankBranchField;
            }
            set
            {
                this.bankBranchField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string BankCode
        {
            get
            {
                return this.bankCodeField;
            }
            set
            {
                this.bankCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string BankCodeDescription
        {
            get
            {
                return this.bankCodeDescriptionField;
            }
            set
            {
                this.bankCodeDescriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string Amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string StartDate
        {
            get
            {
                return this.startDateField;
            }
            set
            {
                this.startDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string Terms
        {
            get
            {
                return this.termsField;
            }
            set
            {
                this.termsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string Date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }
    }

    /// <remarks/>





    public partial class BankHistory : BureauSegment
    {

        private string majorProductField;

        private string accountNoField;

        private string bankNameField;

        private string branchField;

        private string bankCodeField;

        private string bankCodeDescField;

        private string[] commentField;

        private string amountField;

        private string startDateField;

        private string termsField;

        private string dateField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string AccountNo
        {
            get
            {
                return this.accountNoField;
            }
            set
            {
                this.accountNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string BankName
        {
            get
            {
                return this.bankNameField;
            }
            set
            {
                this.bankNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string Branch
        {
            get
            {
                return this.branchField;
            }
            set
            {
                this.branchField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string BankCode
        {
            get
            {
                return this.bankCodeField;
            }
            set
            {
                this.bankCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string BankCodeDesc
        {
            get
            {
                return this.bankCodeDescField;
            }
            set
            {
                this.bankCodeDescField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 6)]
        public string[] Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string Amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string StartDate
        {
            get
            {
                return this.startDateField;
            }
            set
            {
                this.startDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string Terms
        {
            get
            {
                return this.termsField;
            }
            set
            {
                this.termsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string Date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }
    }

    /// <remarks/>





    public partial class BankReport : BureauSegment
    {

        private string majorProductField;

        private string accountNoField;

        private string bankNameField;

        private string branchField;

        private string bankCodeField;

        private string bankCodeDescField;

        private string[] commentField;

        private string amountField;

        private string startDateField;

        private string termsField;

        private string dateField;

        private string accHolderField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string AccountNo
        {
            get
            {
                return this.accountNoField;
            }
            set
            {
                this.accountNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string BankName
        {
            get
            {
                return this.bankNameField;
            }
            set
            {
                this.bankNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string Branch
        {
            get
            {
                return this.branchField;
            }
            set
            {
                this.branchField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string BankCode
        {
            get
            {
                return this.bankCodeField;
            }
            set
            {
                this.bankCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string BankCodeDesc
        {
            get
            {
                return this.bankCodeDescField;
            }
            set
            {
                this.bankCodeDescField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 6)]
        public string[] Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string Amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string StartDate
        {
            get
            {
                return this.startDateField;
            }
            set
            {
                this.startDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string Terms
        {
            get
            {
                return this.termsField;
            }
            set
            {
                this.termsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string Date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string AccHolder
        {
            get
            {
                return this.accHolderField;
            }
            set
            {
                this.accHolderField = value;
            }
        }
    }

    /// <remarks/>





    public partial class Branch : BureauSegment
    {

        private string sGIndexField;

        private string majorProductField;

        private string dunsNoField;

        private string physicalAddressField;

        private string physicalSuburbField;

        private string physicalTownField;

        private string physicalCountryField;

        private string physicalPostCodeField;

        private string postBoxField;

        private string phoneNumberField;

        private string faxNumberField;

        private string websiteField;

        private string emailField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string SGIndex
        {
            get
            {
                return this.sGIndexField;
            }
            set
            {
                this.sGIndexField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string DunsNo
        {
            get
            {
                return this.dunsNoField;
            }
            set
            {
                this.dunsNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string PhysicalAddress
        {
            get
            {
                return this.physicalAddressField;
            }
            set
            {
                this.physicalAddressField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string PhysicalSuburb
        {
            get
            {
                return this.physicalSuburbField;
            }
            set
            {
                this.physicalSuburbField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string PhysicalTown
        {
            get
            {
                return this.physicalTownField;
            }
            set
            {
                this.physicalTownField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string PhysicalCountry
        {
            get
            {
                return this.physicalCountryField;
            }
            set
            {
                this.physicalCountryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string PhysicalPostCode
        {
            get
            {
                return this.physicalPostCodeField;
            }
            set
            {
                this.physicalPostCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string PostBox
        {
            get
            {
                return this.postBoxField;
            }
            set
            {
                this.postBoxField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string PhoneNumber
        {
            get
            {
                return this.phoneNumberField;
            }
            set
            {
                this.phoneNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string FaxNumber
        {
            get
            {
                return this.faxNumberField;
            }
            set
            {
                this.faxNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string Website
        {
            get
            {
                return this.websiteField;
            }
            set
            {
                this.websiteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string Email
        {
            get
            {
                return this.emailField;
            }
            set
            {
                this.emailField = value;
            }
        }
    }

    /// <remarks/>





    public partial class BCCCX01 : BureauSegment
    {

        private string consumerNoField;

        private CX01[] bCCsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string ConsumerNo
        {
            get
            {
                return this.consumerNoField;
            }
            set
            {
                this.consumerNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 1)]
        public CX01[] BCCs
        {
            get
            {
                return this.bCCsField;
            }
            set
            {
                this.bCCsField = value;
            }
        }
    }

    /// <remarks/>





    public partial class BusinessAdverseSummary : BureauSegment
    {

        private string majorProductField;

        private string judgementLast6MonthsCountField;

        private string judgementLast6MonthsTotalField;

        private string judgementLast12MonthsCountField;

        private string judgementLast12MonthsTotalField;

        private string judgementLast24MonthsCountField;

        private string judgementLast24MonthsTotalField;

        private string judgement24MonthsPlusCountField;

        private string judgement24MonthsPlusTotalField;

        private string judgementTotalCountField;

        private string judgementTotalAmountField;

        private string adverseLast6MonthsCountField;

        private string adverseLast6MonthsTotalField;

        private string adverseLast12MonthsCountField;

        private string adverseLast12MonthsTotalField;

        private string adverseLast24MonthsCountField;

        private string adverseLast24MonthsTotalField;

        private string adverse24MonthsPlusCountField;

        private string adverse24MonthsPlusTotalField;

        private string adverseTotalCountField;

        private string adverseTotalAmountField;

        private string notarialBondLast6MonthsCountField;

        private string notarialBondLast6MonthsTotalField;

        private string notarialBondLast12MonthsCountField;

        private string notarialBondLast12MonthsTotalField;

        private string notarialBondLast24MonthsCountField;

        private string notarialBondLast24MonthsTotalField;

        private string notarialBond24MonthsPlusCountField;

        private string notarialBond24MonthsPlusTotalField;

        private string notarialBondTotalCountField;

        private string notarialBondTotalAmountField;

        private string messageField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string JudgementLast6MonthsCount
        {
            get
            {
                return this.judgementLast6MonthsCountField;
            }
            set
            {
                this.judgementLast6MonthsCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string JudgementLast6MonthsTotal
        {
            get
            {
                return this.judgementLast6MonthsTotalField;
            }
            set
            {
                this.judgementLast6MonthsTotalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string JudgementLast12MonthsCount
        {
            get
            {
                return this.judgementLast12MonthsCountField;
            }
            set
            {
                this.judgementLast12MonthsCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string JudgementLast12MonthsTotal
        {
            get
            {
                return this.judgementLast12MonthsTotalField;
            }
            set
            {
                this.judgementLast12MonthsTotalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string JudgementLast24MonthsCount
        {
            get
            {
                return this.judgementLast24MonthsCountField;
            }
            set
            {
                this.judgementLast24MonthsCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string JudgementLast24MonthsTotal
        {
            get
            {
                return this.judgementLast24MonthsTotalField;
            }
            set
            {
                this.judgementLast24MonthsTotalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string Judgement24MonthsPlusCount
        {
            get
            {
                return this.judgement24MonthsPlusCountField;
            }
            set
            {
                this.judgement24MonthsPlusCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string Judgement24MonthsPlusTotal
        {
            get
            {
                return this.judgement24MonthsPlusTotalField;
            }
            set
            {
                this.judgement24MonthsPlusTotalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string JudgementTotalCount
        {
            get
            {
                return this.judgementTotalCountField;
            }
            set
            {
                this.judgementTotalCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string JudgementTotalAmount
        {
            get
            {
                return this.judgementTotalAmountField;
            }
            set
            {
                this.judgementTotalAmountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string AdverseLast6MonthsCount
        {
            get
            {
                return this.adverseLast6MonthsCountField;
            }
            set
            {
                this.adverseLast6MonthsCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string AdverseLast6MonthsTotal
        {
            get
            {
                return this.adverseLast6MonthsTotalField;
            }
            set
            {
                this.adverseLast6MonthsTotalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string AdverseLast12MonthsCount
        {
            get
            {
                return this.adverseLast12MonthsCountField;
            }
            set
            {
                this.adverseLast12MonthsCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string AdverseLast12MonthsTotal
        {
            get
            {
                return this.adverseLast12MonthsTotalField;
            }
            set
            {
                this.adverseLast12MonthsTotalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public string AdverseLast24MonthsCount
        {
            get
            {
                return this.adverseLast24MonthsCountField;
            }
            set
            {
                this.adverseLast24MonthsCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 16)]
        public string AdverseLast24MonthsTotal
        {
            get
            {
                return this.adverseLast24MonthsTotalField;
            }
            set
            {
                this.adverseLast24MonthsTotalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 17)]
        public string Adverse24MonthsPlusCount
        {
            get
            {
                return this.adverse24MonthsPlusCountField;
            }
            set
            {
                this.adverse24MonthsPlusCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 18)]
        public string Adverse24MonthsPlusTotal
        {
            get
            {
                return this.adverse24MonthsPlusTotalField;
            }
            set
            {
                this.adverse24MonthsPlusTotalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 19)]
        public string AdverseTotalCount
        {
            get
            {
                return this.adverseTotalCountField;
            }
            set
            {
                this.adverseTotalCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 20)]
        public string AdverseTotalAmount
        {
            get
            {
                return this.adverseTotalAmountField;
            }
            set
            {
                this.adverseTotalAmountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 21)]
        public string NotarialBondLast6MonthsCount
        {
            get
            {
                return this.notarialBondLast6MonthsCountField;
            }
            set
            {
                this.notarialBondLast6MonthsCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 22)]
        public string NotarialBondLast6MonthsTotal
        {
            get
            {
                return this.notarialBondLast6MonthsTotalField;
            }
            set
            {
                this.notarialBondLast6MonthsTotalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 23)]
        public string NotarialBondLast12MonthsCount
        {
            get
            {
                return this.notarialBondLast12MonthsCountField;
            }
            set
            {
                this.notarialBondLast12MonthsCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 24)]
        public string NotarialBondLast12MonthsTotal
        {
            get
            {
                return this.notarialBondLast12MonthsTotalField;
            }
            set
            {
                this.notarialBondLast12MonthsTotalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 25)]
        public string NotarialBondLast24MonthsCount
        {
            get
            {
                return this.notarialBondLast24MonthsCountField;
            }
            set
            {
                this.notarialBondLast24MonthsCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 26)]
        public string NotarialBondLast24MonthsTotal
        {
            get
            {
                return this.notarialBondLast24MonthsTotalField;
            }
            set
            {
                this.notarialBondLast24MonthsTotalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 27)]
        public string NotarialBond24MonthsPlusCount
        {
            get
            {
                return this.notarialBond24MonthsPlusCountField;
            }
            set
            {
                this.notarialBond24MonthsPlusCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 28)]
        public string NotarialBond24MonthsPlusTotal
        {
            get
            {
                return this.notarialBond24MonthsPlusTotalField;
            }
            set
            {
                this.notarialBond24MonthsPlusTotalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 29)]
        public string NotarialBondTotalCount
        {
            get
            {
                return this.notarialBondTotalCountField;
            }
            set
            {
                this.notarialBondTotalCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 30)]
        public string NotarialBondTotalAmount
        {
            get
            {
                return this.notarialBondTotalAmountField;
            }
            set
            {
                this.notarialBondTotalAmountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 31)]
        public string Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }
    }

    /// <remarks/>





    public partial class BusinessDeedsDI : BureauSegment
    {

        private string majorProductField;

        private string dateField;

        private string commentField;

        private string purchasePriceField;

        private string purchaseDateField;

        private string propertySizeField;

        private string bondNumberField;

        private string bondHolderField;

        private string bondAmountField;

        private string bondDateField;

        private string dateOfBirthOrIDNumberField;

        private string eRFField;

        private string farmField;

        private string buyerNameField;

        private string portionField;

        private string titleField;

        private string townshipField;

        private string deedsOfficeField;

        private string rowIDField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string Date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string PurchasePrice
        {
            get
            {
                return this.purchasePriceField;
            }
            set
            {
                this.purchasePriceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string PurchaseDate
        {
            get
            {
                return this.purchaseDateField;
            }
            set
            {
                this.purchaseDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string PropertySize
        {
            get
            {
                return this.propertySizeField;
            }
            set
            {
                this.propertySizeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string BondNumber
        {
            get
            {
                return this.bondNumberField;
            }
            set
            {
                this.bondNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string BondHolder
        {
            get
            {
                return this.bondHolderField;
            }
            set
            {
                this.bondHolderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string BondAmount
        {
            get
            {
                return this.bondAmountField;
            }
            set
            {
                this.bondAmountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string BondDate
        {
            get
            {
                return this.bondDateField;
            }
            set
            {
                this.bondDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string DateOfBirthOrIDNumber
        {
            get
            {
                return this.dateOfBirthOrIDNumberField;
            }
            set
            {
                this.dateOfBirthOrIDNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string ERF
        {
            get
            {
                return this.eRFField;
            }
            set
            {
                this.eRFField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string Farm
        {
            get
            {
                return this.farmField;
            }
            set
            {
                this.farmField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string BuyerName
        {
            get
            {
                return this.buyerNameField;
            }
            set
            {
                this.buyerNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string Portion
        {
            get
            {
                return this.portionField;
            }
            set
            {
                this.portionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public string Title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 16)]
        public string Township
        {
            get
            {
                return this.townshipField;
            }
            set
            {
                this.townshipField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 17)]
        public string DeedsOffice
        {
            get
            {
                return this.deedsOfficeField;
            }
            set
            {
                this.deedsOfficeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 18)]
        public string RowID
        {
            get
            {
                return this.rowIDField;
            }
            set
            {
                this.rowIDField = value;
            }
        }
    }

    /// <remarks/>





    public partial class BusinessDeedsComprehensivePB : BureauSegment
    {

        private string majorProductField;

        private string dateField;

        private string commentField;

        private string purchasePriceField;

        private string purchaseDateField;

        private string propertySizeField;

        private string bondNumberField;

        private string bondHolderField;

        private string bondAmountField;

        private string bondDateField;

        private string registrationNumberField;

        private string eRFField;

        private string farmField;

        private string propertyNameField;

        private string portionField;

        private string titleField;

        private string townshipField;

        private string deedsOfficeField;

        private string rowIDField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string Date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string PurchasePrice
        {
            get
            {
                return this.purchasePriceField;
            }
            set
            {
                this.purchasePriceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string PurchaseDate
        {
            get
            {
                return this.purchaseDateField;
            }
            set
            {
                this.purchaseDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string PropertySize
        {
            get
            {
                return this.propertySizeField;
            }
            set
            {
                this.propertySizeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string BondNumber
        {
            get
            {
                return this.bondNumberField;
            }
            set
            {
                this.bondNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string BondHolder
        {
            get
            {
                return this.bondHolderField;
            }
            set
            {
                this.bondHolderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string BondAmount
        {
            get
            {
                return this.bondAmountField;
            }
            set
            {
                this.bondAmountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string BondDate
        {
            get
            {
                return this.bondDateField;
            }
            set
            {
                this.bondDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string RegistrationNumber
        {
            get
            {
                return this.registrationNumberField;
            }
            set
            {
                this.registrationNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string ERF
        {
            get
            {
                return this.eRFField;
            }
            set
            {
                this.eRFField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string Farm
        {
            get
            {
                return this.farmField;
            }
            set
            {
                this.farmField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string PropertyName
        {
            get
            {
                return this.propertyNameField;
            }
            set
            {
                this.propertyNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string Portion
        {
            get
            {
                return this.portionField;
            }
            set
            {
                this.portionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public string Title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 16)]
        public string Township
        {
            get
            {
                return this.townshipField;
            }
            set
            {
                this.townshipField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 17)]
        public string DeedsOffice
        {
            get
            {
                return this.deedsOfficeField;
            }
            set
            {
                this.deedsOfficeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 18)]
        public string RowID
        {
            get
            {
                return this.rowIDField;
            }
            set
            {
                this.rowIDField = value;
            }
        }
    }

    /// <remarks/>





    public partial class BusinessDeedsSummaryDA : BureauSegment
    {

        private string majorProductField;

        private string deedsOfficeField;

        private string numberPropertiesField;

        private string totalValueField;

        private string totalBondField;

        private string totalOwnedField;

        private string commentField;

        private string oldestPropertyDateField;

        private string oldestPropertyValueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string DeedsOffice
        {
            get
            {
                return this.deedsOfficeField;
            }
            set
            {
                this.deedsOfficeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string NumberProperties
        {
            get
            {
                return this.numberPropertiesField;
            }
            set
            {
                this.numberPropertiesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string TotalValue
        {
            get
            {
                return this.totalValueField;
            }
            set
            {
                this.totalValueField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string TotalBond
        {
            get
            {
                return this.totalBondField;
            }
            set
            {
                this.totalBondField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string TotalOwned
        {
            get
            {
                return this.totalOwnedField;
            }
            set
            {
                this.totalOwnedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string OldestPropertyDate
        {
            get
            {
                return this.oldestPropertyDateField;
            }
            set
            {
                this.oldestPropertyDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string OldestPropertyValue
        {
            get
            {
                return this.oldestPropertyValueField;
            }
            set
            {
                this.oldestPropertyValueField = value;
            }
        }
    }

    /// <remarks/>





    public partial class BusinessDeedsComprehensivePW : BureauSegment
    {

        private string majorProductField;

        private string dateField;

        private string commentField;

        private string purchasePriceField;

        private string purchaseDateField;

        private string propertySizeField;

        private string bondNumberField;

        private string mortgageeField;

        private string bondAmountField;

        private string bondDateField;

        private string multipleField;

        private string shareField;

        private string dateOfBirthIDField;

        private string eRFField;

        private string propertyTypeField;

        private string farmField;

        private string buyerNameField;

        private string schemeNameField;

        private string schemeNumberField;

        private string portionField;

        private string titleField;

        private string streetField;

        private string townshipField;

        private string provinceField;

        private string deedsOfficeField;

        private string rowIDField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string Date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string PurchasePrice
        {
            get
            {
                return this.purchasePriceField;
            }
            set
            {
                this.purchasePriceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string PurchaseDate
        {
            get
            {
                return this.purchaseDateField;
            }
            set
            {
                this.purchaseDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string PropertySize
        {
            get
            {
                return this.propertySizeField;
            }
            set
            {
                this.propertySizeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string BondNumber
        {
            get
            {
                return this.bondNumberField;
            }
            set
            {
                this.bondNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string Mortgagee
        {
            get
            {
                return this.mortgageeField;
            }
            set
            {
                this.mortgageeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string BondAmount
        {
            get
            {
                return this.bondAmountField;
            }
            set
            {
                this.bondAmountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string BondDate
        {
            get
            {
                return this.bondDateField;
            }
            set
            {
                this.bondDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string Multiple
        {
            get
            {
                return this.multipleField;
            }
            set
            {
                this.multipleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string Share
        {
            get
            {
                return this.shareField;
            }
            set
            {
                this.shareField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string DateOfBirthID
        {
            get
            {
                return this.dateOfBirthIDField;
            }
            set
            {
                this.dateOfBirthIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string ERF
        {
            get
            {
                return this.eRFField;
            }
            set
            {
                this.eRFField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string PropertyType
        {
            get
            {
                return this.propertyTypeField;
            }
            set
            {
                this.propertyTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public string Farm
        {
            get
            {
                return this.farmField;
            }
            set
            {
                this.farmField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 16)]
        public string BuyerName
        {
            get
            {
                return this.buyerNameField;
            }
            set
            {
                this.buyerNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 17)]
        public string SchemeName
        {
            get
            {
                return this.schemeNameField;
            }
            set
            {
                this.schemeNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 18)]
        public string SchemeNumber
        {
            get
            {
                return this.schemeNumberField;
            }
            set
            {
                this.schemeNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 19)]
        public string Portion
        {
            get
            {
                return this.portionField;
            }
            set
            {
                this.portionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 20)]
        public string Title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 21)]
        public string Street
        {
            get
            {
                return this.streetField;
            }
            set
            {
                this.streetField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 22)]
        public string Township
        {
            get
            {
                return this.townshipField;
            }
            set
            {
                this.townshipField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 23)]
        public string Province
        {
            get
            {
                return this.provinceField;
            }
            set
            {
                this.provinceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 24)]
        public string DeedsOffice
        {
            get
            {
                return this.deedsOfficeField;
            }
            set
            {
                this.deedsOfficeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 25)]
        public string RowID
        {
            get
            {
                return this.rowIDField;
            }
            set
            {
                this.rowIDField = value;
            }
        }
    }

    /// <remarks/>





    public partial class BusinessBenchmarkBB : BureauSegment
    {

        private string majorProductField;

        private string rPT_DateField;

        private string statusField;

        private string sME_ScoreField;

        private string avg_ScoreField;

        private string ratingField;

        private string uSSicCodeField;

        private string sASicCodeField;

        private string sicDescField;

        private BusinessBenchmarkDetail[] businessBenchmarkDetailField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string RPT_Date
        {
            get
            {
                return this.rPT_DateField;
            }
            set
            {
                this.rPT_DateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string SME_Score
        {
            get
            {
                return this.sME_ScoreField;
            }
            set
            {
                this.sME_ScoreField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string Avg_Score
        {
            get
            {
                return this.avg_ScoreField;
            }
            set
            {
                this.avg_ScoreField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string Rating
        {
            get
            {
                return this.ratingField;
            }
            set
            {
                this.ratingField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string USSicCode
        {
            get
            {
                return this.uSSicCodeField;
            }
            set
            {
                this.uSSicCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string SASicCode
        {
            get
            {
                return this.sASicCodeField;
            }
            set
            {
                this.sASicCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string SicDesc
        {
            get
            {
                return this.sicDescField;
            }
            set
            {
                this.sicDescField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 9)]
        public BusinessBenchmarkDetail[] BusinessBenchmarkDetail
        {
            get
            {
                return this.businessBenchmarkDetailField;
            }
            set
            {
                this.businessBenchmarkDetailField = value;
            }
        }
    }

    /// <remarks/>





    public partial class BusinessNotarialBonds : BureauSegment
    {

        private string majorProductField;

        private string deedsOfficeField;

        private string multipleOwnersField;

        private string bondRegDateField;

        private string bondNumberField;

        private string bondAmountField;

        private string microfilemRefField;

        private string bondTypeField;

        private string bondOwnerField;

        private string ownerIDField;

        private string ownerTypeField;

        private string bondPercentageField;

        private string[] heldOverField;

        private string hOAboveThresholdField;

        private string parentChildIndField;

        private string messageField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string DeedsOffice
        {
            get
            {
                return this.deedsOfficeField;
            }
            set
            {
                this.deedsOfficeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string MultipleOwners
        {
            get
            {
                return this.multipleOwnersField;
            }
            set
            {
                this.multipleOwnersField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string BondRegDate
        {
            get
            {
                return this.bondRegDateField;
            }
            set
            {
                this.bondRegDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string BondNumber
        {
            get
            {
                return this.bondNumberField;
            }
            set
            {
                this.bondNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string BondAmount
        {
            get
            {
                return this.bondAmountField;
            }
            set
            {
                this.bondAmountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string MicrofilemRef
        {
            get
            {
                return this.microfilemRefField;
            }
            set
            {
                this.microfilemRefField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string BondType
        {
            get
            {
                return this.bondTypeField;
            }
            set
            {
                this.bondTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string BondOwner
        {
            get
            {
                return this.bondOwnerField;
            }
            set
            {
                this.bondOwnerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string OwnerID
        {
            get
            {
                return this.ownerIDField;
            }
            set
            {
                this.ownerIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string OwnerType
        {
            get
            {
                return this.ownerTypeField;
            }
            set
            {
                this.ownerTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string BondPercentage
        {
            get
            {
                return this.bondPercentageField;
            }
            set
            {
                this.bondPercentageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 12)]
        public string[] HeldOver
        {
            get
            {
                return this.heldOverField;
            }
            set
            {
                this.heldOverField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string HOAboveThreshold
        {
            get
            {
                return this.hOAboveThresholdField;
            }
            set
            {
                this.hOAboveThresholdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string ParentChildInd
        {
            get
            {
                return this.parentChildIndField;
            }
            set
            {
                this.parentChildIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public string Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }
    }

    /// <remarks/>





    public partial class BusinessPlusPrincipalSummary : BureauSegment
    {

        private string majorProductField;

        private string surnameField;

        private string forename1Field;

        private string forename2Field;

        private string forename3Field;

        private string iDNumberField;

        private string judgementCountField;

        private string adverseCountField;

        private string noticesCountField;

        private string debtCounselingField;

        private string numberOfNonAdverseNotorialBondsField;

        private string deedsCountField;

        private string numberOfBusinessesField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string Forename1
        {
            get
            {
                return this.forename1Field;
            }
            set
            {
                this.forename1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string Forename2
        {
            get
            {
                return this.forename2Field;
            }
            set
            {
                this.forename2Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string Forename3
        {
            get
            {
                return this.forename3Field;
            }
            set
            {
                this.forename3Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string IDNumber
        {
            get
            {
                return this.iDNumberField;
            }
            set
            {
                this.iDNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string JudgementCount
        {
            get
            {
                return this.judgementCountField;
            }
            set
            {
                this.judgementCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string AdverseCount
        {
            get
            {
                return this.adverseCountField;
            }
            set
            {
                this.adverseCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string NoticesCount
        {
            get
            {
                return this.noticesCountField;
            }
            set
            {
                this.noticesCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string DebtCounseling
        {
            get
            {
                return this.debtCounselingField;
            }
            set
            {
                this.debtCounselingField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string NumberOfNonAdverseNotorialBonds
        {
            get
            {
                return this.numberOfNonAdverseNotorialBondsField;
            }
            set
            {
                this.numberOfNonAdverseNotorialBondsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string DeedsCount
        {
            get
            {
                return this.deedsCountField;
            }
            set
            {
                this.deedsCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string NumberOfBusinesses
        {
            get
            {
                return this.numberOfBusinessesField;
            }
            set
            {
                this.numberOfBusinessesField = value;
            }
        }
    }

    /// <remarks/>





    public partial class CancelledTicket : BureauSegment
    {

        private string[] ticketField;

        private string[] subjectNameField;

        private string[] statusField;

        private string[] cancelledStatusField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 0)]
        public string[] Ticket
        {
            get
            {
                return this.ticketField;
            }
            set
            {
                this.ticketField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 1)]
        public string[] SubjectName
        {
            get
            {
                return this.subjectNameField;
            }
            set
            {
                this.subjectNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 2)]
        public string[] Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 3)]
        public string[] CancelledStatus
        {
            get
            {
                return this.cancelledStatusField;
            }
            set
            {
                this.cancelledStatusField = value;
            }
        }
    }

    /// <remarks/>





    public partial class CapitalEmployed : BureauSegment
    {

        private string majorProductField;

        private string capitalEmployedTotalField;

        private string[] capitalEmployedItemField;

        private string[] capitalEmployedItemAmountField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string CapitalEmployedTotal
        {
            get
            {
                return this.capitalEmployedTotalField;
            }
            set
            {
                this.capitalEmployedTotalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 2)]
        public string[] CapitalEmployedItem
        {
            get
            {
                return this.capitalEmployedItemField;
            }
            set
            {
                this.capitalEmployedItemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 3)]
        public string[] CapitalEmployedItemAmount
        {
            get
            {
                return this.capitalEmployedItemAmountField;
            }
            set
            {
                this.capitalEmployedItemAmountField = value;
            }
        }
    }

    /// <remarks/>





    public partial class CommercialDisputeResponse : BureauSegment
    {

        private string responseCodeField;

        private string responseDescField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string ResponseCode
        {
            get
            {
                return this.responseCodeField;
            }
            set
            {
                this.responseCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string ResponseDesc
        {
            get
            {
                return this.responseDescField;
            }
            set
            {
                this.responseDescField = value;
            }
        }
    }

    /// <remarks/>





    public partial class ConsumerBusinessEnquiry : BureauSegment
    {

        private string majorProductField;

        private string consumerNoField;

        private string enquiryDateField;

        private string subscriberField;

        private string contactField;

        private string typeField;

        private string transTypeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string ConsumerNo
        {
            get
            {
                return this.consumerNoField;
            }
            set
            {
                this.consumerNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string EnquiryDate
        {
            get
            {
                return this.enquiryDateField;
            }
            set
            {
                this.enquiryDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string Subscriber
        {
            get
            {
                return this.subscriberField;
            }
            set
            {
                this.subscriberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string Contact
        {
            get
            {
                return this.contactField;
            }
            set
            {
                this.contactField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string Type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string TransType
        {
            get
            {
                return this.transTypeField;
            }
            set
            {
                this.transTypeField = value;
            }
        }
    }

    /// <remarks/>





    public partial class ConsumerDefault : BureauSegment
    {

        private string majorProductField;

        private string consumerNoField;

        private string nameField;

        private string fnDefaultField;

        private string defaultDateField;

        private string contactNameField;

        private string contactPhoneCodeField;

        private string contactPhoneNoField;

        private string subscriberNameField;

        private string addressField;

        private string accountField;

        private string subAccountField;

        private string branchCodeField;

        private string amountField;

        private string iDNoField;

        private string dateOfBirthField;

        private string debtCodeField;

        private string debtDescField;

        private string remarksField;

        private string remarksXField;

        private string remarksYField;

        private string transTypeField;

        private string messageField;

        private string amountF700Field;

        private string accountF700Field;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string ConsumerNo
        {
            get
            {
                return this.consumerNoField;
            }
            set
            {
                this.consumerNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string FnDefault
        {
            get
            {
                return this.fnDefaultField;
            }
            set
            {
                this.fnDefaultField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string DefaultDate
        {
            get
            {
                return this.defaultDateField;
            }
            set
            {
                this.defaultDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string ContactName
        {
            get
            {
                return this.contactNameField;
            }
            set
            {
                this.contactNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string ContactPhoneCode
        {
            get
            {
                return this.contactPhoneCodeField;
            }
            set
            {
                this.contactPhoneCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string ContactPhoneNo
        {
            get
            {
                return this.contactPhoneNoField;
            }
            set
            {
                this.contactPhoneNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string SubscriberName
        {
            get
            {
                return this.subscriberNameField;
            }
            set
            {
                this.subscriberNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string Address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string Account
        {
            get
            {
                return this.accountField;
            }
            set
            {
                this.accountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string SubAccount
        {
            get
            {
                return this.subAccountField;
            }
            set
            {
                this.subAccountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string BranchCode
        {
            get
            {
                return this.branchCodeField;
            }
            set
            {
                this.branchCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string Amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string IDNo
        {
            get
            {
                return this.iDNoField;
            }
            set
            {
                this.iDNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public string DateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 16)]
        public string DebtCode
        {
            get
            {
                return this.debtCodeField;
            }
            set
            {
                this.debtCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 17)]
        public string DebtDesc
        {
            get
            {
                return this.debtDescField;
            }
            set
            {
                this.debtDescField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 18)]
        public string Remarks
        {
            get
            {
                return this.remarksField;
            }
            set
            {
                this.remarksField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 19)]
        public string RemarksX
        {
            get
            {
                return this.remarksXField;
            }
            set
            {
                this.remarksXField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 20)]
        public string RemarksY
        {
            get
            {
                return this.remarksYField;
            }
            set
            {
                this.remarksYField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 21)]
        public string TransType
        {
            get
            {
                return this.transTypeField;
            }
            set
            {
                this.transTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 22)]
        public string Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 23)]
        public string AmountF700
        {
            get
            {
                return this.amountF700Field;
            }
            set
            {
                this.amountF700Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 24)]
        public string AccountF700
        {
            get
            {
                return this.accountF700Field;
            }
            set
            {
                this.accountF700Field = value;
            }
        }
    }

    /// <remarks/>





    public partial class ConsumerEnquiry : BureauSegment
    {

        private string majorProductField;

        private string consumerNoField;

        private string enquiryDateField;

        private string subscriberField;

        private string contactField;

        private string typeField;

        private string transTypeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string ConsumerNo
        {
            get
            {
                return this.consumerNoField;
            }
            set
            {
                this.consumerNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string EnquiryDate
        {
            get
            {
                return this.enquiryDateField;
            }
            set
            {
                this.enquiryDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string Subscriber
        {
            get
            {
                return this.subscriberField;
            }
            set
            {
                this.subscriberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string Contact
        {
            get
            {
                return this.contactField;
            }
            set
            {
                this.contactField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string Type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string TransType
        {
            get
            {
                return this.transTypeField;
            }
            set
            {
                this.transTypeField = value;
            }
        }
    }

    /// <remarks/>





    public partial class ConsumerHeader : BureauSegment
    {

        private string majorProductField;

        private string consumerNumberField;

        private string surnameField;

        private string forename1Field;

        private string forename2Field;

        private string forename3Field;

        private string aKAName1Field;

        private string aKADate1Field;

        private string aKAName2Field;

        private string aKADate2Field;

        private string aKAName3Field;

        private string aKADate3Field;

        private string spouseForenameField;

        private string addressDate1Field;

        private string address1Field;

        private string addressDate2Field;

        private string address2Field;

        private string addressDate3Field;

        private string address3Field;

        private string addressDate4Field;

        private string address4Field;

        private string employmentDate1Field;

        private string occupation1Field;

        private string employer1Field;

        private string employmentDate2Field;

        private string occupation2Field;

        private string employer2Field;

        private string employmentDate3Field;

        private string occupation3Field;

        private string employer3Field;

        private string remarksField;

        private string iDNo1Field;

        private string iDNo2Field;

        private string dateOfBirthField;

        private string workPhoneCodeField;

        private string workPhoneNumberField;

        private string homePhoneCodeField;

        private string homePhoneNumberField;

        private string ticketNumberField;

        private string messageField;

        private string matchField;

        private string disputeDateField;

        private string debtCouncilDateField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string ConsumerNumber
        {
            get
            {
                return this.consumerNumberField;
            }
            set
            {
                this.consumerNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string Forename1
        {
            get
            {
                return this.forename1Field;
            }
            set
            {
                this.forename1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string Forename2
        {
            get
            {
                return this.forename2Field;
            }
            set
            {
                this.forename2Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string Forename3
        {
            get
            {
                return this.forename3Field;
            }
            set
            {
                this.forename3Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string AKAName1
        {
            get
            {
                return this.aKAName1Field;
            }
            set
            {
                this.aKAName1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string AKADate1
        {
            get
            {
                return this.aKADate1Field;
            }
            set
            {
                this.aKADate1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string AKAName2
        {
            get
            {
                return this.aKAName2Field;
            }
            set
            {
                this.aKAName2Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string AKADate2
        {
            get
            {
                return this.aKADate2Field;
            }
            set
            {
                this.aKADate2Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string AKAName3
        {
            get
            {
                return this.aKAName3Field;
            }
            set
            {
                this.aKAName3Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string AKADate3
        {
            get
            {
                return this.aKADate3Field;
            }
            set
            {
                this.aKADate3Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string SpouseForename
        {
            get
            {
                return this.spouseForenameField;
            }
            set
            {
                this.spouseForenameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string AddressDate1
        {
            get
            {
                return this.addressDate1Field;
            }
            set
            {
                this.addressDate1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string Address1
        {
            get
            {
                return this.address1Field;
            }
            set
            {
                this.address1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public string AddressDate2
        {
            get
            {
                return this.addressDate2Field;
            }
            set
            {
                this.addressDate2Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 16)]
        public string Address2
        {
            get
            {
                return this.address2Field;
            }
            set
            {
                this.address2Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 17)]
        public string AddressDate3
        {
            get
            {
                return this.addressDate3Field;
            }
            set
            {
                this.addressDate3Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 18)]
        public string Address3
        {
            get
            {
                return this.address3Field;
            }
            set
            {
                this.address3Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 19)]
        public string AddressDate4
        {
            get
            {
                return this.addressDate4Field;
            }
            set
            {
                this.addressDate4Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 20)]
        public string Address4
        {
            get
            {
                return this.address4Field;
            }
            set
            {
                this.address4Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 21)]
        public string EmploymentDate1
        {
            get
            {
                return this.employmentDate1Field;
            }
            set
            {
                this.employmentDate1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 22)]
        public string Occupation1
        {
            get
            {
                return this.occupation1Field;
            }
            set
            {
                this.occupation1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 23)]
        public string Employer1
        {
            get
            {
                return this.employer1Field;
            }
            set
            {
                this.employer1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 24)]
        public string EmploymentDate2
        {
            get
            {
                return this.employmentDate2Field;
            }
            set
            {
                this.employmentDate2Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 25)]
        public string Occupation2
        {
            get
            {
                return this.occupation2Field;
            }
            set
            {
                this.occupation2Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 26)]
        public string Employer2
        {
            get
            {
                return this.employer2Field;
            }
            set
            {
                this.employer2Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 27)]
        public string EmploymentDate3
        {
            get
            {
                return this.employmentDate3Field;
            }
            set
            {
                this.employmentDate3Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 28)]
        public string Occupation3
        {
            get
            {
                return this.occupation3Field;
            }
            set
            {
                this.occupation3Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 29)]
        public string Employer3
        {
            get
            {
                return this.employer3Field;
            }
            set
            {
                this.employer3Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 30)]
        public string Remarks
        {
            get
            {
                return this.remarksField;
            }
            set
            {
                this.remarksField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 31)]
        public string IDNo1
        {
            get
            {
                return this.iDNo1Field;
            }
            set
            {
                this.iDNo1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 32)]
        public string IDNo2
        {
            get
            {
                return this.iDNo2Field;
            }
            set
            {
                this.iDNo2Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 33)]
        public string DateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 34)]
        public string WorkPhoneCode
        {
            get
            {
                return this.workPhoneCodeField;
            }
            set
            {
                this.workPhoneCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 35)]
        public string WorkPhoneNumber
        {
            get
            {
                return this.workPhoneNumberField;
            }
            set
            {
                this.workPhoneNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 36)]
        public string HomePhoneCode
        {
            get
            {
                return this.homePhoneCodeField;
            }
            set
            {
                this.homePhoneCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 37)]
        public string HomePhoneNumber
        {
            get
            {
                return this.homePhoneNumberField;
            }
            set
            {
                this.homePhoneNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 38)]
        public string TicketNumber
        {
            get
            {
                return this.ticketNumberField;
            }
            set
            {
                this.ticketNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 39)]
        public string Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 40)]
        public string Match
        {
            get
            {
                return this.matchField;
            }
            set
            {
                this.matchField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 41)]
        public string DisputeDate
        {
            get
            {
                return this.disputeDateField;
            }
            set
            {
                this.disputeDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 42)]
        public string DebtCouncilDate
        {
            get
            {
                return this.debtCouncilDateField;
            }
            set
            {
                this.debtCouncilDateField = value;
            }
        }
    }

    /// <remarks/>





    public partial class ConsumerHeaderC1 : BureauSegment
    {

        private string segmentCodeField;

        private string telWorkCode1Field;

        private string telWorkNumber1Field;

        private string telHomeCode1Field;

        private string telHomeNumber1Field;

        private string telWorkCode2Field;

        private string telWorkNumber2Field;

        private string telHomeCode2Field;

        private string telHomeNumber2Field;

        private string maritalStatusField;

        private string deceasedDateField;

        private string entryDateField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string SegmentCode
        {
            get
            {
                return this.segmentCodeField;
            }
            set
            {
                this.segmentCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string TelWorkCode1
        {
            get
            {
                return this.telWorkCode1Field;
            }
            set
            {
                this.telWorkCode1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string TelWorkNumber1
        {
            get
            {
                return this.telWorkNumber1Field;
            }
            set
            {
                this.telWorkNumber1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string TelHomeCode1
        {
            get
            {
                return this.telHomeCode1Field;
            }
            set
            {
                this.telHomeCode1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string TelHomeNumber1
        {
            get
            {
                return this.telHomeNumber1Field;
            }
            set
            {
                this.telHomeNumber1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string TelWorkCode2
        {
            get
            {
                return this.telWorkCode2Field;
            }
            set
            {
                this.telWorkCode2Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string TelWorkNumber2
        {
            get
            {
                return this.telWorkNumber2Field;
            }
            set
            {
                this.telWorkNumber2Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string TelHomeCode2
        {
            get
            {
                return this.telHomeCode2Field;
            }
            set
            {
                this.telHomeCode2Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string TelHomeNumber2
        {
            get
            {
                return this.telHomeNumber2Field;
            }
            set
            {
                this.telHomeNumber2Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string MaritalStatus
        {
            get
            {
                return this.maritalStatusField;
            }
            set
            {
                this.maritalStatusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string DeceasedDate
        {
            get
            {
                return this.deceasedDateField;
            }
            set
            {
                this.deceasedDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string EntryDate
        {
            get
            {
                return this.entryDateField;
            }
            set
            {
                this.entryDateField = value;
            }
        }
    }

    /// <remarks/>





    public partial class ConsumerInfoNO04 : BureauSegment
    {

        private string recordSeqField;

        private string partField;

        private string partSeqField;

        private string consumerNoField;

        private string surnameField;

        private string forename1Field;

        private string forename2Field;

        private string forename3Field;

        private string titleField;

        private string genderField;

        private string nameInfoDateField;

        private string dateOfBirthField;

        private string identityNo1Field;

        private string identityNo2Field;

        private string maritalStatusCodeField;

        private string maritalStatusDescField;

        private string dependantsField;

        private string spouseName1Field;

        private string spouseName2Field;

        private string telephoneNumbersField;

        private string deceasedDateField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string RecordSeq
        {
            get
            {
                return this.recordSeqField;
            }
            set
            {
                this.recordSeqField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string Part
        {
            get
            {
                return this.partField;
            }
            set
            {
                this.partField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string PartSeq
        {
            get
            {
                return this.partSeqField;
            }
            set
            {
                this.partSeqField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string ConsumerNo
        {
            get
            {
                return this.consumerNoField;
            }
            set
            {
                this.consumerNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string Forename1
        {
            get
            {
                return this.forename1Field;
            }
            set
            {
                this.forename1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string Forename2
        {
            get
            {
                return this.forename2Field;
            }
            set
            {
                this.forename2Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string Forename3
        {
            get
            {
                return this.forename3Field;
            }
            set
            {
                this.forename3Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string Title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string Gender
        {
            get
            {
                return this.genderField;
            }
            set
            {
                this.genderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string NameInfoDate
        {
            get
            {
                return this.nameInfoDateField;
            }
            set
            {
                this.nameInfoDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string DateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string IdentityNo1
        {
            get
            {
                return this.identityNo1Field;
            }
            set
            {
                this.identityNo1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string IdentityNo2
        {
            get
            {
                return this.identityNo2Field;
            }
            set
            {
                this.identityNo2Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string MaritalStatusCode
        {
            get
            {
                return this.maritalStatusCodeField;
            }
            set
            {
                this.maritalStatusCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public string MaritalStatusDesc
        {
            get
            {
                return this.maritalStatusDescField;
            }
            set
            {
                this.maritalStatusDescField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 16)]
        public string Dependants
        {
            get
            {
                return this.dependantsField;
            }
            set
            {
                this.dependantsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 17)]
        public string SpouseName1
        {
            get
            {
                return this.spouseName1Field;
            }
            set
            {
                this.spouseName1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 18)]
        public string SpouseName2
        {
            get
            {
                return this.spouseName2Field;
            }
            set
            {
                this.spouseName2Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 19)]
        public string TelephoneNumbers
        {
            get
            {
                return this.telephoneNumbersField;
            }
            set
            {
                this.telephoneNumbersField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 20)]
        public string DeceasedDate
        {
            get
            {
                return this.deceasedDateField;
            }
            set
            {
                this.deceasedDateField = value;
            }
        }
    }

    /// <remarks/>





    public partial class ConsumerJudgement : BureauSegment
    {

        private string majorProductField;

        private string consumerNumberField;

        private string defendantNameField;

        private string defNoField;

        private string tradeStyleField;

        private string address1Field;

        private string dateOfBirthField;

        private string iDNoField;

        private string plaintiffNameField;

        private string debtCodeField;

        private string debtDescriptionField;

        private string judgementCodeField;

        private string judgementDescriptionField;

        private string courtTypeCodeField;

        private string courtTypeField;

        private string courtNameCodeField;

        private string courtNameField;

        private string caseNumberField;

        private string amountField;

        private string judgementDateField;

        private string captureDateField;

        private string attorneyNameField;

        private string attorneyReferenceField;

        private string attorneyPhoneField;

        private string adminMonthlyAmountField;

        private string adminNoMonthsField;

        private string adminStartDateField;

        private string masterNumberField;

        private string remarksField;

        private string transTypeField;

        private string messageField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string ConsumerNumber
        {
            get
            {
                return this.consumerNumberField;
            }
            set
            {
                this.consumerNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string DefendantName
        {
            get
            {
                return this.defendantNameField;
            }
            set
            {
                this.defendantNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string DefNo
        {
            get
            {
                return this.defNoField;
            }
            set
            {
                this.defNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string TradeStyle
        {
            get
            {
                return this.tradeStyleField;
            }
            set
            {
                this.tradeStyleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string Address1
        {
            get
            {
                return this.address1Field;
            }
            set
            {
                this.address1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string DateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string IDNo
        {
            get
            {
                return this.iDNoField;
            }
            set
            {
                this.iDNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string PlaintiffName
        {
            get
            {
                return this.plaintiffNameField;
            }
            set
            {
                this.plaintiffNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string DebtCode
        {
            get
            {
                return this.debtCodeField;
            }
            set
            {
                this.debtCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string DebtDescription
        {
            get
            {
                return this.debtDescriptionField;
            }
            set
            {
                this.debtDescriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string JudgementCode
        {
            get
            {
                return this.judgementCodeField;
            }
            set
            {
                this.judgementCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string JudgementDescription
        {
            get
            {
                return this.judgementDescriptionField;
            }
            set
            {
                this.judgementDescriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string CourtTypeCode
        {
            get
            {
                return this.courtTypeCodeField;
            }
            set
            {
                this.courtTypeCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string CourtType
        {
            get
            {
                return this.courtTypeField;
            }
            set
            {
                this.courtTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public string CourtNameCode
        {
            get
            {
                return this.courtNameCodeField;
            }
            set
            {
                this.courtNameCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 16)]
        public string CourtName
        {
            get
            {
                return this.courtNameField;
            }
            set
            {
                this.courtNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 17)]
        public string CaseNumber
        {
            get
            {
                return this.caseNumberField;
            }
            set
            {
                this.caseNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 18)]
        public string Amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 19)]
        public string JudgementDate
        {
            get
            {
                return this.judgementDateField;
            }
            set
            {
                this.judgementDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 20)]
        public string CaptureDate
        {
            get
            {
                return this.captureDateField;
            }
            set
            {
                this.captureDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 21)]
        public string AttorneyName
        {
            get
            {
                return this.attorneyNameField;
            }
            set
            {
                this.attorneyNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 22)]
        public string AttorneyReference
        {
            get
            {
                return this.attorneyReferenceField;
            }
            set
            {
                this.attorneyReferenceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 23)]
        public string AttorneyPhone
        {
            get
            {
                return this.attorneyPhoneField;
            }
            set
            {
                this.attorneyPhoneField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 24)]
        public string AdminMonthlyAmount
        {
            get
            {
                return this.adminMonthlyAmountField;
            }
            set
            {
                this.adminMonthlyAmountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 25)]
        public string AdminNoMonths
        {
            get
            {
                return this.adminNoMonthsField;
            }
            set
            {
                this.adminNoMonthsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 26)]
        public string AdminStartDate
        {
            get
            {
                return this.adminStartDateField;
            }
            set
            {
                this.adminStartDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 27)]
        public string MasterNumber
        {
            get
            {
                return this.masterNumberField;
            }
            set
            {
                this.masterNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 28)]
        public string Remarks
        {
            get
            {
                return this.remarksField;
            }
            set
            {
                this.remarksField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 29)]
        public string TransType
        {
            get
            {
                return this.transTypeField;
            }
            set
            {
                this.transTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 30)]
        public string Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }
    }

    /// <remarks/>





    public partial class ConsumerNotarialBonds : BureauSegment
    {

        private string majorProductField;

        private string consumerNumberField;

        private string defendantNameField;

        private string defendantNumberField;

        private string addressField;

        private string dateOfBirthField;

        private string iDNoField;

        private string plaintiffField;

        private string judgementCodeField;

        private string judgementDescField;

        private string courtTypeField;

        private string courtTypeDescField;

        private string courtCodeField;

        private string courtDescField;

        private string caseNoField;

        private string notarialDateField;

        private string captureDateField;

        private string amountField;

        private string bondPercField;

        private string masterNoField;

        private string remarksField;

        private string transTypeField;

        private string messageField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string ConsumerNumber
        {
            get
            {
                return this.consumerNumberField;
            }
            set
            {
                this.consumerNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string DefendantName
        {
            get
            {
                return this.defendantNameField;
            }
            set
            {
                this.defendantNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string DefendantNumber
        {
            get
            {
                return this.defendantNumberField;
            }
            set
            {
                this.defendantNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string Address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string DateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string IDNo
        {
            get
            {
                return this.iDNoField;
            }
            set
            {
                this.iDNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string Plaintiff
        {
            get
            {
                return this.plaintiffField;
            }
            set
            {
                this.plaintiffField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string JudgementCode
        {
            get
            {
                return this.judgementCodeField;
            }
            set
            {
                this.judgementCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string JudgementDesc
        {
            get
            {
                return this.judgementDescField;
            }
            set
            {
                this.judgementDescField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string CourtType
        {
            get
            {
                return this.courtTypeField;
            }
            set
            {
                this.courtTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string CourtTypeDesc
        {
            get
            {
                return this.courtTypeDescField;
            }
            set
            {
                this.courtTypeDescField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string CourtCode
        {
            get
            {
                return this.courtCodeField;
            }
            set
            {
                this.courtCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string CourtDesc
        {
            get
            {
                return this.courtDescField;
            }
            set
            {
                this.courtDescField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string CaseNo
        {
            get
            {
                return this.caseNoField;
            }
            set
            {
                this.caseNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public string NotarialDate
        {
            get
            {
                return this.notarialDateField;
            }
            set
            {
                this.notarialDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 16)]
        public string CaptureDate
        {
            get
            {
                return this.captureDateField;
            }
            set
            {
                this.captureDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 17)]
        public string Amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 18)]
        public string BondPerc
        {
            get
            {
                return this.bondPercField;
            }
            set
            {
                this.bondPercField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 19)]
        public string MasterNo
        {
            get
            {
                return this.masterNoField;
            }
            set
            {
                this.masterNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 20)]
        public string Remarks
        {
            get
            {
                return this.remarksField;
            }
            set
            {
                this.remarksField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 21)]
        public string TransType
        {
            get
            {
                return this.transTypeField;
            }
            set
            {
                this.transTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 22)]
        public string Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }
    }

    /// <remarks/>





    public partial class ConsumerNotice : BureauSegment
    {

        private string majorProductField;

        private string consumerNumberField;

        private string defendantNameField;

        private string defNoField;

        private string address1Field;

        private string dateOfBirthField;

        private string iDNoField;

        private string plaintiffNameField;

        private string noticeCodeField;

        private string noticeDescriptionField;

        private string courtTypeCodeField;

        private string courtTypeDescField;

        private string courtCodeField;

        private string courtNameField;

        private string amountField;

        private string caseNumberField;

        private string noticeDateField;

        private string captureDateField;

        private string masterNoField;

        private string attorneyNameField;

        private string attorneyReferenceField;

        private string attorneyPhoneField;

        private string remarksField;

        private string transTypeField;

        private string messageField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string ConsumerNumber
        {
            get
            {
                return this.consumerNumberField;
            }
            set
            {
                this.consumerNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string DefendantName
        {
            get
            {
                return this.defendantNameField;
            }
            set
            {
                this.defendantNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string DefNo
        {
            get
            {
                return this.defNoField;
            }
            set
            {
                this.defNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string Address1
        {
            get
            {
                return this.address1Field;
            }
            set
            {
                this.address1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string DateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string IDNo
        {
            get
            {
                return this.iDNoField;
            }
            set
            {
                this.iDNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string PlaintiffName
        {
            get
            {
                return this.plaintiffNameField;
            }
            set
            {
                this.plaintiffNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string NoticeCode
        {
            get
            {
                return this.noticeCodeField;
            }
            set
            {
                this.noticeCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string NoticeDescription
        {
            get
            {
                return this.noticeDescriptionField;
            }
            set
            {
                this.noticeDescriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string CourtTypeCode
        {
            get
            {
                return this.courtTypeCodeField;
            }
            set
            {
                this.courtTypeCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string CourtTypeDesc
        {
            get
            {
                return this.courtTypeDescField;
            }
            set
            {
                this.courtTypeDescField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string CourtCode
        {
            get
            {
                return this.courtCodeField;
            }
            set
            {
                this.courtCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string CourtName
        {
            get
            {
                return this.courtNameField;
            }
            set
            {
                this.courtNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string Amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public string CaseNumber
        {
            get
            {
                return this.caseNumberField;
            }
            set
            {
                this.caseNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 16)]
        public string NoticeDate
        {
            get
            {
                return this.noticeDateField;
            }
            set
            {
                this.noticeDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 17)]
        public string CaptureDate
        {
            get
            {
                return this.captureDateField;
            }
            set
            {
                this.captureDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 18)]
        public string MasterNo
        {
            get
            {
                return this.masterNoField;
            }
            set
            {
                this.masterNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 19)]
        public string AttorneyName
        {
            get
            {
                return this.attorneyNameField;
            }
            set
            {
                this.attorneyNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 20)]
        public string AttorneyReference
        {
            get
            {
                return this.attorneyReferenceField;
            }
            set
            {
                this.attorneyReferenceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 21)]
        public string AttorneyPhone
        {
            get
            {
                return this.attorneyPhoneField;
            }
            set
            {
                this.attorneyPhoneField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 22)]
        public string Remarks
        {
            get
            {
                return this.remarksField;
            }
            set
            {
                this.remarksField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 23)]
        public string TransType
        {
            get
            {
                return this.transTypeField;
            }
            set
            {
                this.transTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 24)]
        public string Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }
    }

    /// <remarks/>





    public partial class ConsumerTraceAlert : BureauSegment
    {

        private string majorProductField;

        private string consumerNoField;

        private string informationDateField;

        private string traceTypeCodeField;

        private string traceTypeField;

        private string contactNameField;

        private string contactPhoneField;

        private string subscriberNameField;

        private string comment1Field;

        private string comment2Field;

        private string transactionTypeField;

        private string messageField;

        private string subscriberNumberField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string ConsumerNo
        {
            get
            {
                return this.consumerNoField;
            }
            set
            {
                this.consumerNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string InformationDate
        {
            get
            {
                return this.informationDateField;
            }
            set
            {
                this.informationDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string TraceTypeCode
        {
            get
            {
                return this.traceTypeCodeField;
            }
            set
            {
                this.traceTypeCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string TraceType
        {
            get
            {
                return this.traceTypeField;
            }
            set
            {
                this.traceTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string ContactName
        {
            get
            {
                return this.contactNameField;
            }
            set
            {
                this.contactNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string ContactPhone
        {
            get
            {
                return this.contactPhoneField;
            }
            set
            {
                this.contactPhoneField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string SubscriberName
        {
            get
            {
                return this.subscriberNameField;
            }
            set
            {
                this.subscriberNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string Comment1
        {
            get
            {
                return this.comment1Field;
            }
            set
            {
                this.comment1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string Comment2
        {
            get
            {
                return this.comment2Field;
            }
            set
            {
                this.comment2Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string TransactionType
        {
            get
            {
                return this.transactionTypeField;
            }
            set
            {
                this.transactionTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string SubscriberNumber
        {
            get
            {
                return this.subscriberNumberField;
            }
            set
            {
                this.subscriberNumberField = value;
            }
        }
    }

    /// <remarks/>





    public partial class CourtRecord : BureauSegment
    {

        private string majorProductField;

        private string numberFoundField;

        private string actionDateField;

        private string typeCodeField;

        private string claimAmountField;

        private string typeDescField;

        private string defendantNameField;

        private string defendantTradeStyleField;

        private string courtRecordAddressField;

        private string suburbField;

        private string cityField;

        private string countryField;

        private string postCodeField;

        private string natureOfDebtField;

        private string defendantDistrictField;

        private string caseNumberField;

        private string courtDistrictField;

        private string courtTypeField;

        private string plaintiffNameField;

        private string attorneyField;

        private string commentField;

        private string abandonDateField;

        private string returnDateField;

        private string messageField;

        private string serialNumberField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string NumberFound
        {
            get
            {
                return this.numberFoundField;
            }
            set
            {
                this.numberFoundField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string ActionDate
        {
            get
            {
                return this.actionDateField;
            }
            set
            {
                this.actionDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string TypeCode
        {
            get
            {
                return this.typeCodeField;
            }
            set
            {
                this.typeCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string ClaimAmount
        {
            get
            {
                return this.claimAmountField;
            }
            set
            {
                this.claimAmountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string TypeDesc
        {
            get
            {
                return this.typeDescField;
            }
            set
            {
                this.typeDescField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string DefendantName
        {
            get
            {
                return this.defendantNameField;
            }
            set
            {
                this.defendantNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string DefendantTradeStyle
        {
            get
            {
                return this.defendantTradeStyleField;
            }
            set
            {
                this.defendantTradeStyleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string CourtRecordAddress
        {
            get
            {
                return this.courtRecordAddressField;
            }
            set
            {
                this.courtRecordAddressField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string Suburb
        {
            get
            {
                return this.suburbField;
            }
            set
            {
                this.suburbField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string Country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string PostCode
        {
            get
            {
                return this.postCodeField;
            }
            set
            {
                this.postCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string NatureOfDebt
        {
            get
            {
                return this.natureOfDebtField;
            }
            set
            {
                this.natureOfDebtField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string DefendantDistrict
        {
            get
            {
                return this.defendantDistrictField;
            }
            set
            {
                this.defendantDistrictField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public string CaseNumber
        {
            get
            {
                return this.caseNumberField;
            }
            set
            {
                this.caseNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 16)]
        public string CourtDistrict
        {
            get
            {
                return this.courtDistrictField;
            }
            set
            {
                this.courtDistrictField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 17)]
        public string CourtType
        {
            get
            {
                return this.courtTypeField;
            }
            set
            {
                this.courtTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 18)]
        public string PlaintiffName
        {
            get
            {
                return this.plaintiffNameField;
            }
            set
            {
                this.plaintiffNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 19)]
        public string Attorney
        {
            get
            {
                return this.attorneyField;
            }
            set
            {
                this.attorneyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 20)]
        public string Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 21)]
        public string AbandonDate
        {
            get
            {
                return this.abandonDateField;
            }
            set
            {
                this.abandonDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 22)]
        public string ReturnDate
        {
            get
            {
                return this.returnDateField;
            }
            set
            {
                this.returnDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 23)]
        public string Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 24)]
        public string SerialNumber
        {
            get
            {
                return this.serialNumberField;
            }
            set
            {
                this.serialNumberField = value;
            }
        }
    }

    /// <remarks/>





    public partial class CurrentAsset : BureauSegment
    {

        private string majorProductField;

        private string currentAssTotalField;

        private string[] currentAssItemField;

        private string[] currentAssItemAmtField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string CurrentAssTotal
        {
            get
            {
                return this.currentAssTotalField;
            }
            set
            {
                this.currentAssTotalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 2)]
        public string[] CurrentAssItem
        {
            get
            {
                return this.currentAssItemField;
            }
            set
            {
                this.currentAssItemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 3)]
        public string[] CurrentAssItemAmt
        {
            get
            {
                return this.currentAssItemAmtField;
            }
            set
            {
                this.currentAssItemAmtField = value;
            }
        }
    }

    /// <remarks/>





    public partial class CurrentLiabilities : BureauSegment
    {

        private string majorProductField;

        private string currentLiabilityTotalField;

        private string[] currentLiabilityItemField;

        private string[] currentLiabilityItemAmtField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string CurrentLiabilityTotal
        {
            get
            {
                return this.currentLiabilityTotalField;
            }
            set
            {
                this.currentLiabilityTotalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 2)]
        public string[] CurrentLiabilityItem
        {
            get
            {
                return this.currentLiabilityItemField;
            }
            set
            {
                this.currentLiabilityItemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 3)]
        public string[] CurrentLiabilityItemAmt
        {
            get
            {
                return this.currentLiabilityItemAmtField;
            }
            set
            {
                this.currentLiabilityItemAmtField = value;
            }
        }
    }

    /// <remarks/>





    public partial class DeedHistory : BureauSegment
    {

        private string majorProductField;

        private string dateField;

        private string commentField;

        private string purchasePriceField;

        private string purchaseDateField;

        private string propertySizeField;

        private string bondNumberField;

        private string mortgageeField;

        private string bondAmountField;

        private string bondDateField;

        private string dateOfBirthIDField;

        private string eRFField;

        private string farmField;

        private string propertyNameField;

        private string portionField;

        private string titleField;

        private string townshipField;

        private string deedsOfficeField;

        private string rowIDField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string Date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string PurchasePrice
        {
            get
            {
                return this.purchasePriceField;
            }
            set
            {
                this.purchasePriceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string PurchaseDate
        {
            get
            {
                return this.purchaseDateField;
            }
            set
            {
                this.purchaseDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string PropertySize
        {
            get
            {
                return this.propertySizeField;
            }
            set
            {
                this.propertySizeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string BondNumber
        {
            get
            {
                return this.bondNumberField;
            }
            set
            {
                this.bondNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string Mortgagee
        {
            get
            {
                return this.mortgageeField;
            }
            set
            {
                this.mortgageeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string BondAmount
        {
            get
            {
                return this.bondAmountField;
            }
            set
            {
                this.bondAmountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string BondDate
        {
            get
            {
                return this.bondDateField;
            }
            set
            {
                this.bondDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string DateOfBirthID
        {
            get
            {
                return this.dateOfBirthIDField;
            }
            set
            {
                this.dateOfBirthIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string ERF
        {
            get
            {
                return this.eRFField;
            }
            set
            {
                this.eRFField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string Farm
        {
            get
            {
                return this.farmField;
            }
            set
            {
                this.farmField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string PropertyName
        {
            get
            {
                return this.propertyNameField;
            }
            set
            {
                this.propertyNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string Portion
        {
            get
            {
                return this.portionField;
            }
            set
            {
                this.portionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public string Title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 16)]
        public string Township
        {
            get
            {
                return this.townshipField;
            }
            set
            {
                this.townshipField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 17)]
        public string DeedsOffice
        {
            get
            {
                return this.deedsOfficeField;
            }
            set
            {
                this.deedsOfficeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 18)]
        public string RowID
        {
            get
            {
                return this.rowIDField;
            }
            set
            {
                this.rowIDField = value;
            }
        }
    }

    /// <remarks/>





    public partial class DeedsMultipleBond : BureauSegment
    {

        private string majorProductField;

        private string actionDateField;

        private string commentField;

        private string bondNumberField;

        private string bondHolderField;

        private string bondAmountField;

        private string bondDateField;

        private string bondBuyerIDField;

        private string bondBuyerNameField;

        private string rowIDField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string ActionDate
        {
            get
            {
                return this.actionDateField;
            }
            set
            {
                this.actionDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string BondNumber
        {
            get
            {
                return this.bondNumberField;
            }
            set
            {
                this.bondNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string BondHolder
        {
            get
            {
                return this.bondHolderField;
            }
            set
            {
                this.bondHolderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string BondAmount
        {
            get
            {
                return this.bondAmountField;
            }
            set
            {
                this.bondAmountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string BondDate
        {
            get
            {
                return this.bondDateField;
            }
            set
            {
                this.bondDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string BondBuyerID
        {
            get
            {
                return this.bondBuyerIDField;
            }
            set
            {
                this.bondBuyerIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string BondBuyerName
        {
            get
            {
                return this.bondBuyerNameField;
            }
            set
            {
                this.bondBuyerNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string RowID
        {
            get
            {
                return this.rowIDField;
            }
            set
            {
                this.rowIDField = value;
            }
        }
    }

    /// <remarks/>





    public partial class Default : BureauSegment
    {

        private string majorProductField;

        private string numberFoundField;

        private string defaultDateField;

        private string defaultNameField;

        private string defaultTradeStyleField;

        private string defaultAddressField;

        private string suburbField;

        private string cityField;

        private string countryField;

        private string postCodeField;

        private string amountField;

        private string commentField;

        private string subscriberNameField;

        private string supplierNameField;

        private string messageField;

        private string serialNoField;

        private string onBehalfOfField;

        private string statusField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string NumberFound
        {
            get
            {
                return this.numberFoundField;
            }
            set
            {
                this.numberFoundField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string DefaultDate
        {
            get
            {
                return this.defaultDateField;
            }
            set
            {
                this.defaultDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string DefaultName
        {
            get
            {
                return this.defaultNameField;
            }
            set
            {
                this.defaultNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string DefaultTradeStyle
        {
            get
            {
                return this.defaultTradeStyleField;
            }
            set
            {
                this.defaultTradeStyleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string DefaultAddress
        {
            get
            {
                return this.defaultAddressField;
            }
            set
            {
                this.defaultAddressField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string Suburb
        {
            get
            {
                return this.suburbField;
            }
            set
            {
                this.suburbField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string Country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string PostCode
        {
            get
            {
                return this.postCodeField;
            }
            set
            {
                this.postCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string Amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string SubscriberName
        {
            get
            {
                return this.subscriberNameField;
            }
            set
            {
                this.subscriberNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string SupplierName
        {
            get
            {
                return this.supplierNameField;
            }
            set
            {
                this.supplierNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public string SerialNo
        {
            get
            {
                return this.serialNoField;
            }
            set
            {
                this.serialNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 16)]
        public string OnBehalfOf
        {
            get
            {
                return this.onBehalfOfField;
            }
            set
            {
                this.onBehalfOfField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 17)]
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }
    }

    /// <remarks/>





    public partial class DynamicRating : BureauSegment
    {

        private string majorProductField;

        private string ratingField;

        private string trendField;

        private string conditionField;

        private string[] commentField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string Rating
        {
            get
            {
                return this.ratingField;
            }
            set
            {
                this.ratingField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string Trend
        {
            get
            {
                return this.trendField;
            }
            set
            {
                this.trendField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string Condition
        {
            get
            {
                return this.conditionField;
            }
            set
            {
                this.conditionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 4)]
        public string[] Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(EmpiricaE1))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(EmpiricaEM04))]





    public partial class Empirica : BureauSegment
    {
    }

    /// <remarks/>





    public partial class EmpiricaE1 : Empirica
    {

        private string majorProductField;

        private string surnameField;

        private string forename1Field;

        private string forename2Field;

        private string forename3Field;

        private string identityNoField;

        private string dateOfBirthField;

        private string consumerNoField;

        private string empiricaScoreField;

        private string exclusionCodeField;

        private string exclusionCodeDescriptionField;

        private string[] reasonCodeField;

        private string[] reasonDescriptionField;

        private string expansionCodeField;

        private string errorMessageField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string Forename1
        {
            get
            {
                return this.forename1Field;
            }
            set
            {
                this.forename1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string Forename2
        {
            get
            {
                return this.forename2Field;
            }
            set
            {
                this.forename2Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string Forename3
        {
            get
            {
                return this.forename3Field;
            }
            set
            {
                this.forename3Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string IdentityNo
        {
            get
            {
                return this.identityNoField;
            }
            set
            {
                this.identityNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string DateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string ConsumerNo
        {
            get
            {
                return this.consumerNoField;
            }
            set
            {
                this.consumerNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string EmpiricaScore
        {
            get
            {
                return this.empiricaScoreField;
            }
            set
            {
                this.empiricaScoreField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string ExclusionCode
        {
            get
            {
                return this.exclusionCodeField;
            }
            set
            {
                this.exclusionCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string ExclusionCodeDescription
        {
            get
            {
                return this.exclusionCodeDescriptionField;
            }
            set
            {
                this.exclusionCodeDescriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 11)]
        public string[] ReasonCode
        {
            get
            {
                return this.reasonCodeField;
            }
            set
            {
                this.reasonCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 12)]
        public string[] ReasonDescription
        {
            get
            {
                return this.reasonDescriptionField;
            }
            set
            {
                this.reasonDescriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string ExpansionCode
        {
            get
            {
                return this.expansionCodeField;
            }
            set
            {
                this.expansionCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string ErrorMessage
        {
            get
            {
                return this.errorMessageField;
            }
            set
            {
                this.errorMessageField = value;
            }
        }
    }

    /// <remarks/>





    public partial class EmpiricaEM04 : Empirica
    {

        private string consumerNoField;

        private string empiricaScoreField;

        private string exclusionCodeField;

        private string exclusionCodeDescriptionField;

        private string[] reasonCodeField;

        private string[] reasonDescriptionField;

        private string expansionScoreField;

        private string expansionScoreDescriptionField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string ConsumerNo
        {
            get
            {
                return this.consumerNoField;
            }
            set
            {
                this.consumerNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string EmpiricaScore
        {
            get
            {
                return this.empiricaScoreField;
            }
            set
            {
                this.empiricaScoreField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string ExclusionCode
        {
            get
            {
                return this.exclusionCodeField;
            }
            set
            {
                this.exclusionCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string ExclusionCodeDescription
        {
            get
            {
                return this.exclusionCodeDescriptionField;
            }
            set
            {
                this.exclusionCodeDescriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 4)]
        public string[] ReasonCode
        {
            get
            {
                return this.reasonCodeField;
            }
            set
            {
                this.reasonCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 5)]
        public string[] ReasonDescription
        {
            get
            {
                return this.reasonDescriptionField;
            }
            set
            {
                this.reasonDescriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string ExpansionScore
        {
            get
            {
                return this.expansionScoreField;
            }
            set
            {
                this.expansionScoreField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string ExpansionScoreDescription
        {
            get
            {
                return this.expansionScoreDescriptionField;
            }
            set
            {
                this.expansionScoreDescriptionField = value;
            }
        }
    }

    /// <remarks/>





    public partial class EmpOfCapital : BureauSegment
    {

        private string majorProductField;

        private string empOfCapTotalField;

        private string[] empOfCapItemField;

        private string[] empOfCapAmtField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string EmpOfCapTotal
        {
            get
            {
                return this.empOfCapTotalField;
            }
            set
            {
                this.empOfCapTotalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 2)]
        public string[] EmpOfCapItem
        {
            get
            {
                return this.empOfCapItemField;
            }
            set
            {
                this.empOfCapItemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 3)]
        public string[] EmpOfCapAmt
        {
            get
            {
                return this.empOfCapAmtField;
            }
            set
            {
                this.empOfCapAmtField = value;
            }
        }
    }

    /// <remarks/>





    public partial class EnquiryHistory : BureauSegment
    {

        private string majorProductField;

        private string dateOfEnquiryField;

        private string reasonField;

        private string subscriberField;

        private string amountField;

        private string contactNameField;

        private string contactPhoneField;

        private string subscriberNumberField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string DateOfEnquiry
        {
            get
            {
                return this.dateOfEnquiryField;
            }
            set
            {
                this.dateOfEnquiryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string Reason
        {
            get
            {
                return this.reasonField;
            }
            set
            {
                this.reasonField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string Subscriber
        {
            get
            {
                return this.subscriberField;
            }
            set
            {
                this.subscriberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string Amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string ContactName
        {
            get
            {
                return this.contactNameField;
            }
            set
            {
                this.contactNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string ContactPhone
        {
            get
            {
                return this.contactPhoneField;
            }
            set
            {
                this.contactPhoneField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string SubscriberNumber
        {
            get
            {
                return this.subscriberNumberField;
            }
            set
            {
                this.subscriberNumberField = value;
            }
        }
    }

    /// <remarks/>





    public partial class EnquirySummary : BureauSegment
    {

        private string majorProductField;

        private string last3MonthTotalField;

        private string last6MonthTotalField;

        private string last12MonthTotalField;

        private string last24MonthTotalField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string Last3MonthTotal
        {
            get
            {
                return this.last3MonthTotalField;
            }
            set
            {
                this.last3MonthTotalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string Last6MonthTotal
        {
            get
            {
                return this.last6MonthTotalField;
            }
            set
            {
                this.last6MonthTotalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string Last12MonthTotal
        {
            get
            {
                return this.last12MonthTotalField;
            }
            set
            {
                this.last12MonthTotalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string Last24MonthTotal
        {
            get
            {
                return this.last24MonthTotalField;
            }
            set
            {
                this.last24MonthTotalField = value;
            }
        }
    }

    /// <remarks/>





    public partial class FirstResponse : BureauSegment
    {

        private string ticketNumberField;

        private string iTNumberField;

        private string dunsNumberField;

        private string registrationNumberField;

        private string businessNameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string TicketNumber
        {
            get
            {
                return this.ticketNumberField;
            }
            set
            {
                this.ticketNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string ITNumber
        {
            get
            {
                return this.iTNumberField;
            }
            set
            {
                this.iTNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string DunsNumber
        {
            get
            {
                return this.dunsNumberField;
            }
            set
            {
                this.dunsNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string RegistrationNumber
        {
            get
            {
                return this.registrationNumberField;
            }
            set
            {
                this.registrationNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string BusinessName
        {
            get
            {
                return this.businessNameField;
            }
            set
            {
                this.businessNameField = value;
            }
        }
    }

    /// <remarks/>





    public partial class FinanceHeader : BureauSegment
    {

        private string majorProductField;

        private string dateField;

        private string sourceField;

        private string sourceTitleField;

        private string commentField;

        private string turnOverDateYear1Field;

        private string turnOverDateYear2Field;

        private string turnOverAmount1Field;

        private string turnOverAmount2Field;

        private string turnOverYear1TypeField;

        private string turnOverYear2TypeField;

        private string balanceSheetDateField;

        private string balanceSheetUnitField;

        private string balanceSheetTypeField;

        private string finYearMonthField;

        private string dRCededIndField;

        private string dRCededNameField;

        private string dRFactoredField;

        private string dRFactoredNameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string Date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string Source
        {
            get
            {
                return this.sourceField;
            }
            set
            {
                this.sourceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string SourceTitle
        {
            get
            {
                return this.sourceTitleField;
            }
            set
            {
                this.sourceTitleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string TurnOverDateYear1
        {
            get
            {
                return this.turnOverDateYear1Field;
            }
            set
            {
                this.turnOverDateYear1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string TurnOverDateYear2
        {
            get
            {
                return this.turnOverDateYear2Field;
            }
            set
            {
                this.turnOverDateYear2Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string TurnOverAmount1
        {
            get
            {
                return this.turnOverAmount1Field;
            }
            set
            {
                this.turnOverAmount1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string TurnOverAmount2
        {
            get
            {
                return this.turnOverAmount2Field;
            }
            set
            {
                this.turnOverAmount2Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string TurnOverYear1Type
        {
            get
            {
                return this.turnOverYear1TypeField;
            }
            set
            {
                this.turnOverYear1TypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string TurnOverYear2Type
        {
            get
            {
                return this.turnOverYear2TypeField;
            }
            set
            {
                this.turnOverYear2TypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string BalanceSheetDate
        {
            get
            {
                return this.balanceSheetDateField;
            }
            set
            {
                this.balanceSheetDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string BalanceSheetUnit
        {
            get
            {
                return this.balanceSheetUnitField;
            }
            set
            {
                this.balanceSheetUnitField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string BalanceSheetType
        {
            get
            {
                return this.balanceSheetTypeField;
            }
            set
            {
                this.balanceSheetTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string FinYearMonth
        {
            get
            {
                return this.finYearMonthField;
            }
            set
            {
                this.finYearMonthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public string DRCededInd
        {
            get
            {
                return this.dRCededIndField;
            }
            set
            {
                this.dRCededIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 16)]
        public string DRCededName
        {
            get
            {
                return this.dRCededNameField;
            }
            set
            {
                this.dRCededNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 17)]
        public string DRFactored
        {
            get
            {
                return this.dRFactoredField;
            }
            set
            {
                this.dRFactoredField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 18)]
        public string DRFactoredName
        {
            get
            {
                return this.dRFactoredNameField;
            }
            set
            {
                this.dRFactoredNameField = value;
            }
        }
    }

    /// <remarks/>





    public partial class FinanceData : BureauSegment
    {

        private string majorProductField;

        private string dateField;

        private string balanceSheetDateCurrentField;

        private string balanceSheetDateBeforeField;

        private string financialFactorField;

        private string currencyCurrentField;

        private string currencyBeforeField;

        private string finYearMonthCurrentField;

        private string finYearMonthBeforeField;

        private string yearCurrentField;

        private string yearBeforeField;

        private string[] turnoverBandCurrentField;

        private string[] turnoverBandBeforeField;

        private string turnoverApproximationCurrentField;

        private string turnoverApproximationBeforeField;

        private string netProfitApproximationCurrentField;

        private string netProfitApproximationBeforeField;

        private string balanceSheetTypeCurrentField;

        private string balanceSheetTypeBeforeField;

        private string infoSourceField;

        private string commentField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string Date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string BalanceSheetDateCurrent
        {
            get
            {
                return this.balanceSheetDateCurrentField;
            }
            set
            {
                this.balanceSheetDateCurrentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string BalanceSheetDateBefore
        {
            get
            {
                return this.balanceSheetDateBeforeField;
            }
            set
            {
                this.balanceSheetDateBeforeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string FinancialFactor
        {
            get
            {
                return this.financialFactorField;
            }
            set
            {
                this.financialFactorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string CurrencyCurrent
        {
            get
            {
                return this.currencyCurrentField;
            }
            set
            {
                this.currencyCurrentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string CurrencyBefore
        {
            get
            {
                return this.currencyBeforeField;
            }
            set
            {
                this.currencyBeforeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string FinYearMonthCurrent
        {
            get
            {
                return this.finYearMonthCurrentField;
            }
            set
            {
                this.finYearMonthCurrentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string FinYearMonthBefore
        {
            get
            {
                return this.finYearMonthBeforeField;
            }
            set
            {
                this.finYearMonthBeforeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string YearCurrent
        {
            get
            {
                return this.yearCurrentField;
            }
            set
            {
                this.yearCurrentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string YearBefore
        {
            get
            {
                return this.yearBeforeField;
            }
            set
            {
                this.yearBeforeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 11)]
        public string[] TurnoverBandCurrent
        {
            get
            {
                return this.turnoverBandCurrentField;
            }
            set
            {
                this.turnoverBandCurrentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 12)]
        public string[] TurnoverBandBefore
        {
            get
            {
                return this.turnoverBandBeforeField;
            }
            set
            {
                this.turnoverBandBeforeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string TurnoverApproximationCurrent
        {
            get
            {
                return this.turnoverApproximationCurrentField;
            }
            set
            {
                this.turnoverApproximationCurrentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string TurnoverApproximationBefore
        {
            get
            {
                return this.turnoverApproximationBeforeField;
            }
            set
            {
                this.turnoverApproximationBeforeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public string NetProfitApproximationCurrent
        {
            get
            {
                return this.netProfitApproximationCurrentField;
            }
            set
            {
                this.netProfitApproximationCurrentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 16)]
        public string NetProfitApproximationBefore
        {
            get
            {
                return this.netProfitApproximationBeforeField;
            }
            set
            {
                this.netProfitApproximationBeforeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 17)]
        public string BalanceSheetTypeCurrent
        {
            get
            {
                return this.balanceSheetTypeCurrentField;
            }
            set
            {
                this.balanceSheetTypeCurrentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 18)]
        public string BalanceSheetTypeBefore
        {
            get
            {
                return this.balanceSheetTypeBeforeField;
            }
            set
            {
                this.balanceSheetTypeBeforeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 19)]
        public string InfoSource
        {
            get
            {
                return this.infoSourceField;
            }
            set
            {
                this.infoSourceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 20)]
        public string Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }
    }

    /// <remarks/>





    public partial class FinanceDataFE : BureauSegment
    {

        private string majorProductField;

        private string dateField;

        private FinanceAmounts[] amountsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string Date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 2)]
        public FinanceAmounts[] Amounts
        {
            get
            {
                return this.amountsField;
            }
            set
            {
                this.amountsField = value;
            }
        }
    }

    /// <remarks/>





    public partial class FinanceDataFF : BureauSegment
    {

        private string majorProductField;

        private string dateField;

        private DebtorInfo[] debtorsField;

        private string debtorNameField;

        private string debtorDescriptionField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string Date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 2)]
        public DebtorInfo[] Debtors
        {
            get
            {
                return this.debtorsField;
            }
            set
            {
                this.debtorsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string DebtorName
        {
            get
            {
                return this.debtorNameField;
            }
            set
            {
                this.debtorNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string DebtorDescription
        {
            get
            {
                return this.debtorDescriptionField;
            }
            set
            {
                this.debtorDescriptionField = value;
            }
        }
    }

    /// <remarks/>





    public partial class FinancialRatios : BureauSegment
    {

        private string majorProductField;

        private string infoDateField;

        private string[] currencyField;

        private string[] balanceSheetDateField;

        private string[] profitabilityRatioField;

        private string[] gearingRatioField;

        private string[] interestCoverRatioField;

        private string[] currentRatioField;

        private string[] acidTestRatioField;

        private string[] returnOnEquityRatioField;

        private string[] turnoverField;

        private string[] profitBeforeInterestTaxField;

        private string[] interestPaidField;

        private string[] ownersEquityField;

        private string[] currentAssetsField;

        private string[] currentLiabilitiesField;

        private string[] longTermLiabilitiesField;

        private string[] inventoryField;

        private string commentField;

        private string[] balanceSheetTypeField;

        private string[] financialYearEndDateField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string InfoDate
        {
            get
            {
                return this.infoDateField;
            }
            set
            {
                this.infoDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 2)]
        public string[] Currency
        {
            get
            {
                return this.currencyField;
            }
            set
            {
                this.currencyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 3)]
        public string[] BalanceSheetDate
        {
            get
            {
                return this.balanceSheetDateField;
            }
            set
            {
                this.balanceSheetDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 4)]
        public string[] ProfitabilityRatio
        {
            get
            {
                return this.profitabilityRatioField;
            }
            set
            {
                this.profitabilityRatioField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 5)]
        public string[] GearingRatio
        {
            get
            {
                return this.gearingRatioField;
            }
            set
            {
                this.gearingRatioField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 6)]
        public string[] InterestCoverRatio
        {
            get
            {
                return this.interestCoverRatioField;
            }
            set
            {
                this.interestCoverRatioField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 7)]
        public string[] CurrentRatio
        {
            get
            {
                return this.currentRatioField;
            }
            set
            {
                this.currentRatioField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 8)]
        public string[] AcidTestRatio
        {
            get
            {
                return this.acidTestRatioField;
            }
            set
            {
                this.acidTestRatioField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 9)]
        public string[] ReturnOnEquityRatio
        {
            get
            {
                return this.returnOnEquityRatioField;
            }
            set
            {
                this.returnOnEquityRatioField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 10)]
        public string[] Turnover
        {
            get
            {
                return this.turnoverField;
            }
            set
            {
                this.turnoverField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 11)]
        public string[] ProfitBeforeInterestTax
        {
            get
            {
                return this.profitBeforeInterestTaxField;
            }
            set
            {
                this.profitBeforeInterestTaxField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 12)]
        public string[] InterestPaid
        {
            get
            {
                return this.interestPaidField;
            }
            set
            {
                this.interestPaidField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 13)]
        public string[] OwnersEquity
        {
            get
            {
                return this.ownersEquityField;
            }
            set
            {
                this.ownersEquityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 14)]
        public string[] CurrentAssets
        {
            get
            {
                return this.currentAssetsField;
            }
            set
            {
                this.currentAssetsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 15)]
        public string[] CurrentLiabilities
        {
            get
            {
                return this.currentLiabilitiesField;
            }
            set
            {
                this.currentLiabilitiesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 16)]
        public string[] LongTermLiabilities
        {
            get
            {
                return this.longTermLiabilitiesField;
            }
            set
            {
                this.longTermLiabilitiesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 17)]
        public string[] Inventory
        {
            get
            {
                return this.inventoryField;
            }
            set
            {
                this.inventoryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 18)]
        public string Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 19)]
        public string[] BalanceSheetType
        {
            get
            {
                return this.balanceSheetTypeField;
            }
            set
            {
                this.balanceSheetTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 20)]
        public string[] FinancialYearEndDate
        {
            get
            {
                return this.financialYearEndDateField;
            }
            set
            {
                this.financialYearEndDateField = value;
            }
        }
    }

    /// <remarks/>





    public partial class GeneralBankingInfo : BureauSegment
    {

        private string majorProductField;

        private string accountNoField;

        private string bankNameField;

        private string branchField;

        private string[] commentField;

        private string startDateField;

        private string infoDateField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string AccountNo
        {
            get
            {
                return this.accountNoField;
            }
            set
            {
                this.accountNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string BankName
        {
            get
            {
                return this.bankNameField;
            }
            set
            {
                this.bankNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string Branch
        {
            get
            {
                return this.branchField;
            }
            set
            {
                this.branchField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 4)]
        public string[] Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string StartDate
        {
            get
            {
                return this.startDateField;
            }
            set
            {
                this.startDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string InfoDate
        {
            get
            {
                return this.infoDateField;
            }
            set
            {
                this.infoDateField = value;
            }
        }
    }

    /// <remarks/>





    public partial class Header : BureauSegment
    {

        private string majorProductField;

        private string dateOfHeaderField;

        private string startDateField;

        private string businessTypeField;

        private string dunsNumberField;

        private string startingCapitalField;

        private string purchasePriceField;

        private string industryField;

        private string businessFunctionField;

        private string businessCategoryField;

        private string physicalAddressField;

        private string suburbField;

        private string cityField;

        private string countryField;

        private string postCodeField;

        private string postalAddressField;

        private string postalSuburbField;

        private string postalCityField;

        private string postalCountryField;

        private string postalPostCodeField;

        private string phoneField;

        private string faxField;

        private string[] vATNumbersField;

        private string websiteField;

        private string emailField;

        private string taxNumberField;

        private string tradingNumberField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string DateOfHeader
        {
            get
            {
                return this.dateOfHeaderField;
            }
            set
            {
                this.dateOfHeaderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string StartDate
        {
            get
            {
                return this.startDateField;
            }
            set
            {
                this.startDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string BusinessType
        {
            get
            {
                return this.businessTypeField;
            }
            set
            {
                this.businessTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string DunsNumber
        {
            get
            {
                return this.dunsNumberField;
            }
            set
            {
                this.dunsNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string StartingCapital
        {
            get
            {
                return this.startingCapitalField;
            }
            set
            {
                this.startingCapitalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string PurchasePrice
        {
            get
            {
                return this.purchasePriceField;
            }
            set
            {
                this.purchasePriceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string Industry
        {
            get
            {
                return this.industryField;
            }
            set
            {
                this.industryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string BusinessFunction
        {
            get
            {
                return this.businessFunctionField;
            }
            set
            {
                this.businessFunctionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string BusinessCategory
        {
            get
            {
                return this.businessCategoryField;
            }
            set
            {
                this.businessCategoryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string PhysicalAddress
        {
            get
            {
                return this.physicalAddressField;
            }
            set
            {
                this.physicalAddressField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string Suburb
        {
            get
            {
                return this.suburbField;
            }
            set
            {
                this.suburbField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string Country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string PostCode
        {
            get
            {
                return this.postCodeField;
            }
            set
            {
                this.postCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public string PostalAddress
        {
            get
            {
                return this.postalAddressField;
            }
            set
            {
                this.postalAddressField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 16)]
        public string PostalSuburb
        {
            get
            {
                return this.postalSuburbField;
            }
            set
            {
                this.postalSuburbField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 17)]
        public string PostalCity
        {
            get
            {
                return this.postalCityField;
            }
            set
            {
                this.postalCityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 18)]
        public string PostalCountry
        {
            get
            {
                return this.postalCountryField;
            }
            set
            {
                this.postalCountryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 19)]
        public string PostalPostCode
        {
            get
            {
                return this.postalPostCodeField;
            }
            set
            {
                this.postalPostCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 20)]
        public string Phone
        {
            get
            {
                return this.phoneField;
            }
            set
            {
                this.phoneField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 21)]
        public string Fax
        {
            get
            {
                return this.faxField;
            }
            set
            {
                this.faxField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 22)]
        public string[] VATNumbers
        {
            get
            {
                return this.vATNumbersField;
            }
            set
            {
                this.vATNumbersField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 23)]
        public string Website
        {
            get
            {
                return this.websiteField;
            }
            set
            {
                this.websiteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 24)]
        public string Email
        {
            get
            {
                return this.emailField;
            }
            set
            {
                this.emailField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 25)]
        public string TaxNumber
        {
            get
            {
                return this.taxNumberField;
            }
            set
            {
                this.taxNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 26)]
        public string TradingNumber
        {
            get
            {
                return this.tradingNumberField;
            }
            set
            {
                this.tradingNumberField = value;
            }
        }
    }

    /// <remarks/>





    public partial class LinkedBusinessHeader : BureauSegment
    {

        private string majorProductField;

        private string consumerNoField;

        private string iTNumberField;

        private string registeredNumberField;

        private string registeredStatusField;

        private string businessNameField;

        private string registeredDateField;

        private string judgementCounterField;

        private string defaultCounterField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string ConsumerNo
        {
            get
            {
                return this.consumerNoField;
            }
            set
            {
                this.consumerNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string ITNumber
        {
            get
            {
                return this.iTNumberField;
            }
            set
            {
                this.iTNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string RegisteredNumber
        {
            get
            {
                return this.registeredNumberField;
            }
            set
            {
                this.registeredNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string RegisteredStatus
        {
            get
            {
                return this.registeredStatusField;
            }
            set
            {
                this.registeredStatusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string BusinessName
        {
            get
            {
                return this.businessNameField;
            }
            set
            {
                this.businessNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string RegisteredDate
        {
            get
            {
                return this.registeredDateField;
            }
            set
            {
                this.registeredDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string JudgementCounter
        {
            get
            {
                return this.judgementCounterField;
            }
            set
            {
                this.judgementCounterField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string DefaultCounter
        {
            get
            {
                return this.defaultCounterField;
            }
            set
            {
                this.defaultCounterField = value;
            }
        }
    }

    /// <remarks/>





    public partial class MailboxRetrieveList : BureauSegment
    {

        private string[] ticketsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 0)]
        public string[] Tickets
        {
            get
            {
                return this.ticketsField;
            }
            set
            {
                this.ticketsField = value;
            }
        }
    }

    /// <remarks/>





    public partial class ModuleAvailabilityResponse : BureauSegment
    {

        private string iTNumberField;

        private Module[] modulesField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string ITNumber
        {
            get
            {
                return this.iTNumberField;
            }
            set
            {
                this.iTNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 1)]
        public Module[] Modules
        {
            get
            {
                return this.modulesField;
            }
            set
            {
                this.modulesField = value;
            }
        }
    }

    /// <remarks/>





    public partial class Names : BureauSegment
    {

        private string majorProductField;

        private string businessNameField;

        private string aKANameField;

        private string[] divisionalNamesField;

        private string[] previousNamesField;

        private string[] previousNameDatesField;

        private string[] tradeStylesField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string BusinessName
        {
            get
            {
                return this.businessNameField;
            }
            set
            {
                this.businessNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string AKAName
        {
            get
            {
                return this.aKANameField;
            }
            set
            {
                this.aKANameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 3)]
        public string[] DivisionalNames
        {
            get
            {
                return this.divisionalNamesField;
            }
            set
            {
                this.divisionalNamesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 4)]
        public string[] PreviousNames
        {
            get
            {
                return this.previousNamesField;
            }
            set
            {
                this.previousNamesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 5)]
        public string[] PreviousNameDates
        {
            get
            {
                return this.previousNameDatesField;
            }
            set
            {
                this.previousNameDatesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 6)]
        public string[] TradeStyles
        {
            get
            {
                return this.tradeStylesField;
            }
            set
            {
                this.tradeStylesField = value;
            }
        }
    }

    /// <remarks/>





    public partial class NotarialBond : BureauSegment
    {

        private string majorProductField;

        private string numberFoundField;

        private string bondDateField;

        private string mortgagorField;

        private string tradeStyleField;

        private string addressField;

        private string suburbField;

        private string cityField;

        private string countryField;

        private string postCodeField;

        private string bondNumberField;

        private string bondAmountField;

        private string bondPercentField;

        private string bondDistrictField;

        private string mortgageeField;

        private string dateCancelledField;

        private string messageField;

        private string serialNumberField;

        private string mortgagorDistrictField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string NumberFound
        {
            get
            {
                return this.numberFoundField;
            }
            set
            {
                this.numberFoundField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string BondDate
        {
            get
            {
                return this.bondDateField;
            }
            set
            {
                this.bondDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string Mortgagor
        {
            get
            {
                return this.mortgagorField;
            }
            set
            {
                this.mortgagorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string TradeStyle
        {
            get
            {
                return this.tradeStyleField;
            }
            set
            {
                this.tradeStyleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string Address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string Suburb
        {
            get
            {
                return this.suburbField;
            }
            set
            {
                this.suburbField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string Country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string PostCode
        {
            get
            {
                return this.postCodeField;
            }
            set
            {
                this.postCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string BondNumber
        {
            get
            {
                return this.bondNumberField;
            }
            set
            {
                this.bondNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string BondAmount
        {
            get
            {
                return this.bondAmountField;
            }
            set
            {
                this.bondAmountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string BondPercent
        {
            get
            {
                return this.bondPercentField;
            }
            set
            {
                this.bondPercentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string BondDistrict
        {
            get
            {
                return this.bondDistrictField;
            }
            set
            {
                this.bondDistrictField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string Mortgagee
        {
            get
            {
                return this.mortgageeField;
            }
            set
            {
                this.mortgageeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public string DateCancelled
        {
            get
            {
                return this.dateCancelledField;
            }
            set
            {
                this.dateCancelledField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 16)]
        public string Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 17)]
        public string SerialNumber
        {
            get
            {
                return this.serialNumberField;
            }
            set
            {
                this.serialNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 18)]
        public string MortgagorDistrict
        {
            get
            {
                return this.mortgagorDistrictField;
            }
            set
            {
                this.mortgagorDistrictField = value;
            }
        }
    }

    /// <remarks/>





    public partial class Observation : BureauSegment
    {

        private string majorProductField;

        private string[] commentField;

        private string commentDateField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 1)]
        public string[] Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string CommentDate
        {
            get
            {
                return this.commentDateField;
            }
            set
            {
                this.commentDateField = value;
            }
        }
    }

    /// <remarks/>





    public partial class ObservationCont : BureauSegment
    {

        private string majorProductField;

        private string[] commentField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 1)]
        public string[] Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }
    }

    /// <remarks/>





    public partial class Operation : BureauSegment
    {

        private string majorProductField;

        private string dateField;

        private string sicCodeField;

        private string sicDescriptionField;

        private string salesAreaField;

        private string divisionalNameField;

        private string creditSalesField;

        private string creditSalesPercField;

        private string termsFromField;

        private string termsToField;

        private string aveCollPeriodField;

        private string cashSalesField;

        private string cashSalesPercField;

        private string[] opCommentField;

        private string importField;

        private string importCommentField;

        private string exportField;

        private string exportCommentField;

        private string numberSalesStaffField;

        private string numberWageStaffField;

        private string totalNumberStaffField;

        private string premisesOwnerField;

        private string premisesLessorField;

        private string premisesSizeField;

        private string leasePeriodFromField;

        private string leasePeriodToField;

        private string premisesRentalField;

        private string sASicCodeField;

        private string sASicDescriptionField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string Date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string SicCode
        {
            get
            {
                return this.sicCodeField;
            }
            set
            {
                this.sicCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string SicDescription
        {
            get
            {
                return this.sicDescriptionField;
            }
            set
            {
                this.sicDescriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string SalesArea
        {
            get
            {
                return this.salesAreaField;
            }
            set
            {
                this.salesAreaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string DivisionalName
        {
            get
            {
                return this.divisionalNameField;
            }
            set
            {
                this.divisionalNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string CreditSales
        {
            get
            {
                return this.creditSalesField;
            }
            set
            {
                this.creditSalesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string CreditSalesPerc
        {
            get
            {
                return this.creditSalesPercField;
            }
            set
            {
                this.creditSalesPercField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string TermsFrom
        {
            get
            {
                return this.termsFromField;
            }
            set
            {
                this.termsFromField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string TermsTo
        {
            get
            {
                return this.termsToField;
            }
            set
            {
                this.termsToField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string AveCollPeriod
        {
            get
            {
                return this.aveCollPeriodField;
            }
            set
            {
                this.aveCollPeriodField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string CashSales
        {
            get
            {
                return this.cashSalesField;
            }
            set
            {
                this.cashSalesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string CashSalesPerc
        {
            get
            {
                return this.cashSalesPercField;
            }
            set
            {
                this.cashSalesPercField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 13)]
        public string[] OpComment
        {
            get
            {
                return this.opCommentField;
            }
            set
            {
                this.opCommentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string Import
        {
            get
            {
                return this.importField;
            }
            set
            {
                this.importField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public string ImportComment
        {
            get
            {
                return this.importCommentField;
            }
            set
            {
                this.importCommentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 16)]
        public string Export
        {
            get
            {
                return this.exportField;
            }
            set
            {
                this.exportField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 17)]
        public string ExportComment
        {
            get
            {
                return this.exportCommentField;
            }
            set
            {
                this.exportCommentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 18)]
        public string NumberSalesStaff
        {
            get
            {
                return this.numberSalesStaffField;
            }
            set
            {
                this.numberSalesStaffField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 19)]
        public string NumberWageStaff
        {
            get
            {
                return this.numberWageStaffField;
            }
            set
            {
                this.numberWageStaffField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 20)]
        public string TotalNumberStaff
        {
            get
            {
                return this.totalNumberStaffField;
            }
            set
            {
                this.totalNumberStaffField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 21)]
        public string PremisesOwner
        {
            get
            {
                return this.premisesOwnerField;
            }
            set
            {
                this.premisesOwnerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 22)]
        public string PremisesLessor
        {
            get
            {
                return this.premisesLessorField;
            }
            set
            {
                this.premisesLessorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 23)]
        public string PremisesSize
        {
            get
            {
                return this.premisesSizeField;
            }
            set
            {
                this.premisesSizeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 24)]
        public string LeasePeriodFrom
        {
            get
            {
                return this.leasePeriodFromField;
            }
            set
            {
                this.leasePeriodFromField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 25)]
        public string LeasePeriodTo
        {
            get
            {
                return this.leasePeriodToField;
            }
            set
            {
                this.leasePeriodToField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 26)]
        public string PremisesRental
        {
            get
            {
                return this.premisesRentalField;
            }
            set
            {
                this.premisesRentalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 27)]
        public string SASicCode
        {
            get
            {
                return this.sASicCodeField;
            }
            set
            {
                this.sASicCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 28)]
        public string SASicDescription
        {
            get
            {
                return this.sASicDescriptionField;
            }
            set
            {
                this.sASicDescriptionField = value;
            }
        }
    }

    /// <remarks/>





    public partial class AdditionalOperations : BureauSegment
    {

        private string majorProductField;

        private SicDetails[] sicDetailsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 1)]
        public SicDetails[] SicDetails
        {
            get
            {
                return this.sicDetailsField;
            }
            set
            {
                this.sicDetailsField = value;
            }
        }
    }

    /// <remarks/>





    public partial class OtherOperation : BureauSegment
    {

        private string majorProductField;

        private string[] branchNumberField;

        private string[] branchLocationField;

        private string[] agentNumberField;

        private string[] agentLocationField;

        private string[] agencyNameField;

        private string[] agencyProductField;

        private string[] agencyCountryField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 1)]
        public string[] BranchNumber
        {
            get
            {
                return this.branchNumberField;
            }
            set
            {
                this.branchNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 2)]
        public string[] BranchLocation
        {
            get
            {
                return this.branchLocationField;
            }
            set
            {
                this.branchLocationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 3)]
        public string[] AgentNumber
        {
            get
            {
                return this.agentNumberField;
            }
            set
            {
                this.agentNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 4)]
        public string[] AgentLocation
        {
            get
            {
                return this.agentLocationField;
            }
            set
            {
                this.agentLocationField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 5)]
        public string[] AgencyName
        {
            get
            {
                return this.agencyNameField;
            }
            set
            {
                this.agencyNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 6)]
        public string[] AgencyProduct
        {
            get
            {
                return this.agencyProductField;
            }
            set
            {
                this.agencyProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 7)]
        public string[] AgencyCountry
        {
            get
            {
                return this.agencyCountryField;
            }
            set
            {
                this.agencyCountryField = value;
            }
        }
    }

    /// <remarks/>





    public partial class Principal : BureauSegment
    {

        private string majorProductField;

        private string surnameField;

        private string forename1Field;

        private string forename2Field;

        private string forename3Field;

        private string iDNumberField;

        private string dateOfBirthField;

        private string commentField;

        private string civilCourtRecordField;

        private string defaultDataField;

        private string noticesOrNotarialBondsField;

        private string confirmedByRegistrarField;

        private string positionField;

        private string dateStartedField;

        private string dateDisputedField;

        private string debtCouncilDateField;

        private string debtCouncilDescField;

        private string deedsField;

        private string infoDateField;

        private string assetNotarialBondField;

        private string empiricaScoreField;

        private string[] empiricaReasonCodeField;

        private string[] empiricaReasonDescriptionField;

        private string empiricaExclusionCodeField;

        private string empiricaExclusionDescriptionField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string Forename1
        {
            get
            {
                return this.forename1Field;
            }
            set
            {
                this.forename1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string Forename2
        {
            get
            {
                return this.forename2Field;
            }
            set
            {
                this.forename2Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string Forename3
        {
            get
            {
                return this.forename3Field;
            }
            set
            {
                this.forename3Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string IDNumber
        {
            get
            {
                return this.iDNumberField;
            }
            set
            {
                this.iDNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string DateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string CivilCourtRecord
        {
            get
            {
                return this.civilCourtRecordField;
            }
            set
            {
                this.civilCourtRecordField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string DefaultData
        {
            get
            {
                return this.defaultDataField;
            }
            set
            {
                this.defaultDataField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string NoticesOrNotarialBonds
        {
            get
            {
                return this.noticesOrNotarialBondsField;
            }
            set
            {
                this.noticesOrNotarialBondsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string ConfirmedByRegistrar
        {
            get
            {
                return this.confirmedByRegistrarField;
            }
            set
            {
                this.confirmedByRegistrarField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string Position
        {
            get
            {
                return this.positionField;
            }
            set
            {
                this.positionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string DateStarted
        {
            get
            {
                return this.dateStartedField;
            }
            set
            {
                this.dateStartedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string DateDisputed
        {
            get
            {
                return this.dateDisputedField;
            }
            set
            {
                this.dateDisputedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public string DebtCouncilDate
        {
            get
            {
                return this.debtCouncilDateField;
            }
            set
            {
                this.debtCouncilDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 16)]
        public string DebtCouncilDesc
        {
            get
            {
                return this.debtCouncilDescField;
            }
            set
            {
                this.debtCouncilDescField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 17)]
        public string Deeds
        {
            get
            {
                return this.deedsField;
            }
            set
            {
                this.deedsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 18)]
        public string InfoDate
        {
            get
            {
                return this.infoDateField;
            }
            set
            {
                this.infoDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 19)]
        public string AssetNotarialBond
        {
            get
            {
                return this.assetNotarialBondField;
            }
            set
            {
                this.assetNotarialBondField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 20)]
        public string EmpiricaScore
        {
            get
            {
                return this.empiricaScoreField;
            }
            set
            {
                this.empiricaScoreField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 21)]
        public string[] EmpiricaReasonCode
        {
            get
            {
                return this.empiricaReasonCodeField;
            }
            set
            {
                this.empiricaReasonCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 22)]
        public string[] EmpiricaReasonDescription
        {
            get
            {
                return this.empiricaReasonDescriptionField;
            }
            set
            {
                this.empiricaReasonDescriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 23)]
        public string EmpiricaExclusionCode
        {
            get
            {
                return this.empiricaExclusionCodeField;
            }
            set
            {
                this.empiricaExclusionCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 24)]
        public string EmpiricaExclusionDescription
        {
            get
            {
                return this.empiricaExclusionDescriptionField;
            }
            set
            {
                this.empiricaExclusionDescriptionField = value;
            }
        }
    }

    /// <remarks/>





    public partial class PrincipalDetailEmpirica : BureauSegment
    {

        private string majorProductField;

        private string surnameField;

        private string forename1Field;

        private string forename2Field;

        private string forename3Field;

        private string iDNumberField;

        private string dateOfBirthField;

        private string commentField;

        private string civilCourtRecordField;

        private string defaultDataField;

        private string noticesOrNotarialBondsField;

        private string confirmedByRegistrarField;

        private string positionField;

        private string dateStartedField;

        private string dateDisputedField;

        private string debtCouncilDateField;

        private string debtCouncilDescField;

        private string deedsField;

        private string infoDateField;

        private string assetNotarialBondField;

        private string empiricaScoreField;

        private string[] empiricaReasonCodeField;

        private string[] empiricaReasonDescriptionField;

        private string empiricaExclusionCodeField;

        private string empiricaExclusionDescriptionField;

        private string expansionCodeField;

        private string expansionCodeDescriptionField;

        private string empiricaVersionField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string Forename1
        {
            get
            {
                return this.forename1Field;
            }
            set
            {
                this.forename1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string Forename2
        {
            get
            {
                return this.forename2Field;
            }
            set
            {
                this.forename2Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string Forename3
        {
            get
            {
                return this.forename3Field;
            }
            set
            {
                this.forename3Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string IDNumber
        {
            get
            {
                return this.iDNumberField;
            }
            set
            {
                this.iDNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string DateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string CivilCourtRecord
        {
            get
            {
                return this.civilCourtRecordField;
            }
            set
            {
                this.civilCourtRecordField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string DefaultData
        {
            get
            {
                return this.defaultDataField;
            }
            set
            {
                this.defaultDataField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string NoticesOrNotarialBonds
        {
            get
            {
                return this.noticesOrNotarialBondsField;
            }
            set
            {
                this.noticesOrNotarialBondsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string ConfirmedByRegistrar
        {
            get
            {
                return this.confirmedByRegistrarField;
            }
            set
            {
                this.confirmedByRegistrarField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string Position
        {
            get
            {
                return this.positionField;
            }
            set
            {
                this.positionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string DateStarted
        {
            get
            {
                return this.dateStartedField;
            }
            set
            {
                this.dateStartedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string DateDisputed
        {
            get
            {
                return this.dateDisputedField;
            }
            set
            {
                this.dateDisputedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public string DebtCouncilDate
        {
            get
            {
                return this.debtCouncilDateField;
            }
            set
            {
                this.debtCouncilDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 16)]
        public string DebtCouncilDesc
        {
            get
            {
                return this.debtCouncilDescField;
            }
            set
            {
                this.debtCouncilDescField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 17)]
        public string Deeds
        {
            get
            {
                return this.deedsField;
            }
            set
            {
                this.deedsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 18)]
        public string InfoDate
        {
            get
            {
                return this.infoDateField;
            }
            set
            {
                this.infoDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 19)]
        public string AssetNotarialBond
        {
            get
            {
                return this.assetNotarialBondField;
            }
            set
            {
                this.assetNotarialBondField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 20)]
        public string EmpiricaScore
        {
            get
            {
                return this.empiricaScoreField;
            }
            set
            {
                this.empiricaScoreField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 21)]
        public string[] EmpiricaReasonCode
        {
            get
            {
                return this.empiricaReasonCodeField;
            }
            set
            {
                this.empiricaReasonCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 22)]
        public string[] EmpiricaReasonDescription
        {
            get
            {
                return this.empiricaReasonDescriptionField;
            }
            set
            {
                this.empiricaReasonDescriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 23)]
        public string EmpiricaExclusionCode
        {
            get
            {
                return this.empiricaExclusionCodeField;
            }
            set
            {
                this.empiricaExclusionCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 24)]
        public string EmpiricaExclusionDescription
        {
            get
            {
                return this.empiricaExclusionDescriptionField;
            }
            set
            {
                this.empiricaExclusionDescriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 25)]
        public string ExpansionCode
        {
            get
            {
                return this.expansionCodeField;
            }
            set
            {
                this.expansionCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 26)]
        public string ExpansionCodeDescription
        {
            get
            {
                return this.expansionCodeDescriptionField;
            }
            set
            {
                this.expansionCodeDescriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 27)]
        public string EmpiricaVersion
        {
            get
            {
                return this.empiricaVersionField;
            }
            set
            {
                this.empiricaVersionField = value;
            }
        }
    }

    /// <remarks/>





    public partial class PrincipalArchive : BureauSegment
    {

        private string registrationNumberField;

        private string surnameField;

        private string initialsField;

        private string iDNumberField;

        private string dateOfBirthField;

        private string positionField;

        private string dateStartedField;

        private string dateResignedField;

        private string businessNameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string RegistrationNumber
        {
            get
            {
                return this.registrationNumberField;
            }
            set
            {
                this.registrationNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string Initials
        {
            get
            {
                return this.initialsField;
            }
            set
            {
                this.initialsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string IDNumber
        {
            get
            {
                return this.iDNumberField;
            }
            set
            {
                this.iDNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string DateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string Position
        {
            get
            {
                return this.positionField;
            }
            set
            {
                this.positionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string DateStarted
        {
            get
            {
                return this.dateStartedField;
            }
            set
            {
                this.dateStartedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string DateResigned
        {
            get
            {
                return this.dateResignedField;
            }
            set
            {
                this.dateResignedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string BusinessName
        {
            get
            {
                return this.businessNameField;
            }
            set
            {
                this.businessNameField = value;
            }
        }
    }

    /// <remarks/>





    public partial class PrincipalArchiveP5 : BureauSegment
    {

        private string majorProductField;

        private string registrationNumberField;

        private string surnameField;

        private string initialsField;

        private string iDNumberField;

        private string dateOfBirthField;

        private string positionField;

        private string dateStartedField;

        private string dateResignedField;

        private string statusField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string RegistrationNumber
        {
            get
            {
                return this.registrationNumberField;
            }
            set
            {
                this.registrationNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string Initials
        {
            get
            {
                return this.initialsField;
            }
            set
            {
                this.initialsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string IDNumber
        {
            get
            {
                return this.iDNumberField;
            }
            set
            {
                this.iDNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string DateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string Position
        {
            get
            {
                return this.positionField;
            }
            set
            {
                this.positionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string DateStarted
        {
            get
            {
                return this.dateStartedField;
            }
            set
            {
                this.dateStartedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string DateResigned
        {
            get
            {
                return this.dateResignedField;
            }
            set
            {
                this.dateResignedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }
    }

    /// <remarks/>





    public partial class PrincipalClearance : BureauSegment
    {

        private string majorProductField;

        private string surnameField;

        private string forename1Field;

        private string forename2Field;

        private string forename3Field;

        private string iDNumberField;

        private string dateOfBirthField;

        private Established establishedField;

        private string civilCourtCountField;

        private string defaultCountField;

        private string noticeNotarialCountField;

        private string disputeDateField;

        private string debtCouncilDateField;

        private string debtCouncilDescField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string Forename1
        {
            get
            {
                return this.forename1Field;
            }
            set
            {
                this.forename1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string Forename2
        {
            get
            {
                return this.forename2Field;
            }
            set
            {
                this.forename2Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string Forename3
        {
            get
            {
                return this.forename3Field;
            }
            set
            {
                this.forename3Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string IDNumber
        {
            get
            {
                return this.iDNumberField;
            }
            set
            {
                this.iDNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string DateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public Established Established
        {
            get
            {
                return this.establishedField;
            }
            set
            {
                this.establishedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string CivilCourtCount
        {
            get
            {
                return this.civilCourtCountField;
            }
            set
            {
                this.civilCourtCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string DefaultCount
        {
            get
            {
                return this.defaultCountField;
            }
            set
            {
                this.defaultCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string NoticeNotarialCount
        {
            get
            {
                return this.noticeNotarialCountField;
            }
            set
            {
                this.noticeNotarialCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string DisputeDate
        {
            get
            {
                return this.disputeDateField;
            }
            set
            {
                this.disputeDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string DebtCouncilDate
        {
            get
            {
                return this.debtCouncilDateField;
            }
            set
            {
                this.debtCouncilDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string DebtCouncilDesc
        {
            get
            {
                return this.debtCouncilDescField;
            }
            set
            {
                this.debtCouncilDescField = value;
            }
        }
    }

    /// <remarks/>



    public enum Established
    {

        /// <remarks/>
        No,

        /// <remarks/>
        Yes,
    }

    /// <remarks/>





    public partial class PrincipalDeedsComprehensiveCA : BureauSegment
    {

        private string majorProductField;

        private string dateField;

        private string commentField;

        private string purchasePriceField;

        private string purchaseDateField;

        private string propertySizeField;

        private string bondNumberField;

        private string bondHolderField;

        private string bondAmountField;

        private string bondDateField;

        private string multipleOwnersField;

        private string shareField;

        private string dateOfBirthOrIDNumberField;

        private string eRFField;

        private string propertyTypeField;

        private string farmField;

        private string propertyNameField;

        private string schemeNameField;

        private string schemeNumberField;

        private string portionField;

        private string titleField;

        private string townshipField;

        private string deedsOfficeField;

        private string rowIDField;

        private string streetField;

        private string provinceField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string Date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string PurchasePrice
        {
            get
            {
                return this.purchasePriceField;
            }
            set
            {
                this.purchasePriceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string PurchaseDate
        {
            get
            {
                return this.purchaseDateField;
            }
            set
            {
                this.purchaseDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string PropertySize
        {
            get
            {
                return this.propertySizeField;
            }
            set
            {
                this.propertySizeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string BondNumber
        {
            get
            {
                return this.bondNumberField;
            }
            set
            {
                this.bondNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string BondHolder
        {
            get
            {
                return this.bondHolderField;
            }
            set
            {
                this.bondHolderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string BondAmount
        {
            get
            {
                return this.bondAmountField;
            }
            set
            {
                this.bondAmountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string BondDate
        {
            get
            {
                return this.bondDateField;
            }
            set
            {
                this.bondDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string MultipleOwners
        {
            get
            {
                return this.multipleOwnersField;
            }
            set
            {
                this.multipleOwnersField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string Share
        {
            get
            {
                return this.shareField;
            }
            set
            {
                this.shareField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string DateOfBirthOrIDNumber
        {
            get
            {
                return this.dateOfBirthOrIDNumberField;
            }
            set
            {
                this.dateOfBirthOrIDNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string ERF
        {
            get
            {
                return this.eRFField;
            }
            set
            {
                this.eRFField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string PropertyType
        {
            get
            {
                return this.propertyTypeField;
            }
            set
            {
                this.propertyTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public string Farm
        {
            get
            {
                return this.farmField;
            }
            set
            {
                this.farmField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 16)]
        public string PropertyName
        {
            get
            {
                return this.propertyNameField;
            }
            set
            {
                this.propertyNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 17)]
        public string SchemeName
        {
            get
            {
                return this.schemeNameField;
            }
            set
            {
                this.schemeNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 18)]
        public string SchemeNumber
        {
            get
            {
                return this.schemeNumberField;
            }
            set
            {
                this.schemeNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 19)]
        public string Portion
        {
            get
            {
                return this.portionField;
            }
            set
            {
                this.portionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 20)]
        public string Title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 21)]
        public string Township
        {
            get
            {
                return this.townshipField;
            }
            set
            {
                this.townshipField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 22)]
        public string DeedsOffice
        {
            get
            {
                return this.deedsOfficeField;
            }
            set
            {
                this.deedsOfficeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 23)]
        public string RowID
        {
            get
            {
                return this.rowIDField;
            }
            set
            {
                this.rowIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 24)]
        public string Street
        {
            get
            {
                return this.streetField;
            }
            set
            {
                this.streetField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 25)]
        public string Province
        {
            get
            {
                return this.provinceField;
            }
            set
            {
                this.provinceField = value;
            }
        }
    }

    /// <remarks/>





    public partial class PrincipalDeedsComprehensiveCV : BureauSegment
    {

        private string majorProductField;

        private string dateField;

        private string commentField;

        private string purchasePriceField;

        private string purchaseDateField;

        private string propertySizeField;

        private string bondNumberField;

        private string bondHolderField;

        private string bondAmountField;

        private string bondDateField;

        private string dateOfBirthOrIDNumberField;

        private string eRFField;

        private string farmField;

        private string propertyNameField;

        private string schemeNameField;

        private string schemeNumberField;

        private string portionField;

        private string titleField;

        private string townshipField;

        private string deedsOfficeField;

        private string multipleOwnersField;

        private string shareField;

        private string propertyTypeField;

        private string rowIDField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string Date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string PurchasePrice
        {
            get
            {
                return this.purchasePriceField;
            }
            set
            {
                this.purchasePriceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string PurchaseDate
        {
            get
            {
                return this.purchaseDateField;
            }
            set
            {
                this.purchaseDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string PropertySize
        {
            get
            {
                return this.propertySizeField;
            }
            set
            {
                this.propertySizeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string BondNumber
        {
            get
            {
                return this.bondNumberField;
            }
            set
            {
                this.bondNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string BondHolder
        {
            get
            {
                return this.bondHolderField;
            }
            set
            {
                this.bondHolderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string BondAmount
        {
            get
            {
                return this.bondAmountField;
            }
            set
            {
                this.bondAmountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string BondDate
        {
            get
            {
                return this.bondDateField;
            }
            set
            {
                this.bondDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string DateOfBirthOrIDNumber
        {
            get
            {
                return this.dateOfBirthOrIDNumberField;
            }
            set
            {
                this.dateOfBirthOrIDNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string ERF
        {
            get
            {
                return this.eRFField;
            }
            set
            {
                this.eRFField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string Farm
        {
            get
            {
                return this.farmField;
            }
            set
            {
                this.farmField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string PropertyName
        {
            get
            {
                return this.propertyNameField;
            }
            set
            {
                this.propertyNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string SchemeName
        {
            get
            {
                return this.schemeNameField;
            }
            set
            {
                this.schemeNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public string SchemeNumber
        {
            get
            {
                return this.schemeNumberField;
            }
            set
            {
                this.schemeNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 16)]
        public string Portion
        {
            get
            {
                return this.portionField;
            }
            set
            {
                this.portionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 17)]
        public string Title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 18)]
        public string Township
        {
            get
            {
                return this.townshipField;
            }
            set
            {
                this.townshipField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 19)]
        public string DeedsOffice
        {
            get
            {
                return this.deedsOfficeField;
            }
            set
            {
                this.deedsOfficeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 20)]
        public string MultipleOwners
        {
            get
            {
                return this.multipleOwnersField;
            }
            set
            {
                this.multipleOwnersField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 21)]
        public string Share
        {
            get
            {
                return this.shareField;
            }
            set
            {
                this.shareField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 22)]
        public string PropertyType
        {
            get
            {
                return this.propertyTypeField;
            }
            set
            {
                this.propertyTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 23)]
        public string RowID
        {
            get
            {
                return this.rowIDField;
            }
            set
            {
                this.rowIDField = value;
            }
        }
    }

    /// <remarks/>





    public partial class PrincipalDeedsSummaryCO : BureauSegment
    {

        private string majorProductField;

        private string principalNameField;

        private string principalSurnameField;

        private string dateOfBirthOrIDNumberField;

        private string numberOfPropertiesField;

        private string purchasePriceField;

        private string bondAmountField;

        private string bondFreeAmountField;

        private string deedsOfficeField;

        private string deedsDateField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string PrincipalName
        {
            get
            {
                return this.principalNameField;
            }
            set
            {
                this.principalNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string PrincipalSurname
        {
            get
            {
                return this.principalSurnameField;
            }
            set
            {
                this.principalSurnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string DateOfBirthOrIDNumber
        {
            get
            {
                return this.dateOfBirthOrIDNumberField;
            }
            set
            {
                this.dateOfBirthOrIDNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string NumberOfProperties
        {
            get
            {
                return this.numberOfPropertiesField;
            }
            set
            {
                this.numberOfPropertiesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string PurchasePrice
        {
            get
            {
                return this.purchasePriceField;
            }
            set
            {
                this.purchasePriceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string BondAmount
        {
            get
            {
                return this.bondAmountField;
            }
            set
            {
                this.bondAmountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string BondFreeAmount
        {
            get
            {
                return this.bondFreeAmountField;
            }
            set
            {
                this.bondFreeAmountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string DeedsOffice
        {
            get
            {
                return this.deedsOfficeField;
            }
            set
            {
                this.deedsOfficeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string DeedsDate
        {
            get
            {
                return this.deedsDateField;
            }
            set
            {
                this.deedsDateField = value;
            }
        }
    }

    /// <remarks/>





    public partial class PrincipalDeedsSummaryP8 : BureauSegment
    {

        private string majorProductField;

        private string businessNameField;

        private string registrationNumberField;

        private string principalNameField;

        private string dateOfBirthOrIDNumberField;

        private string numberOfPropertiesField;

        private string purchasePriceField;

        private string bondAmountField;

        private string bondFreeAmountField;

        private string deedsOfficeField;

        private string messageField;

        private string dateField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string BusinessName
        {
            get
            {
                return this.businessNameField;
            }
            set
            {
                this.businessNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string RegistrationNumber
        {
            get
            {
                return this.registrationNumberField;
            }
            set
            {
                this.registrationNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string PrincipalName
        {
            get
            {
                return this.principalNameField;
            }
            set
            {
                this.principalNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string DateOfBirthOrIDNumber
        {
            get
            {
                return this.dateOfBirthOrIDNumberField;
            }
            set
            {
                this.dateOfBirthOrIDNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string NumberOfProperties
        {
            get
            {
                return this.numberOfPropertiesField;
            }
            set
            {
                this.numberOfPropertiesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string PurchasePrice
        {
            get
            {
                return this.purchasePriceField;
            }
            set
            {
                this.purchasePriceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string BondAmount
        {
            get
            {
                return this.bondAmountField;
            }
            set
            {
                this.bondAmountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string BondFreeAmount
        {
            get
            {
                return this.bondFreeAmountField;
            }
            set
            {
                this.bondFreeAmountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string DeedsOffice
        {
            get
            {
                return this.deedsOfficeField;
            }
            set
            {
                this.deedsOfficeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string Date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }
    }

    /// <remarks/>





    public partial class PrincipalFirstResponse : BureauSegment
    {

        private string ticketNumberField;

        private string consumerNumberField;

        private string principalNameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string TicketNumber
        {
            get
            {
                return this.ticketNumberField;
            }
            set
            {
                this.ticketNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string ConsumerNumber
        {
            get
            {
                return this.consumerNumberField;
            }
            set
            {
                this.consumerNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string PrincipalName
        {
            get
            {
                return this.principalNameField;
            }
            set
            {
                this.principalNameField = value;
            }
        }
    }

    /// <remarks/>





    public partial class PrincipalIDV : BureauSegment
    {

        private string recordSequenceField;

        private string partField;

        private string partSequenceField;

        private string hawkNumberField;

        private string hawkCodeField;

        private string hawkDescriptionField;

        private string iDVerifiedCodeField;

        private string iDDescriptionField;

        private string oldIDWarningField;

        private string oldIDDescriptionField;

        private string surnameField;

        private string forename1Field;

        private string forename2Field;

        private string ticketNumberField;

        private string subscriberNumberField;

        private string subscriberNameField;

        private string subscriberReferenceField;

        private string iDNumberField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string RecordSequence
        {
            get
            {
                return this.recordSequenceField;
            }
            set
            {
                this.recordSequenceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string Part
        {
            get
            {
                return this.partField;
            }
            set
            {
                this.partField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string PartSequence
        {
            get
            {
                return this.partSequenceField;
            }
            set
            {
                this.partSequenceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string HawkNumber
        {
            get
            {
                return this.hawkNumberField;
            }
            set
            {
                this.hawkNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string HawkCode
        {
            get
            {
                return this.hawkCodeField;
            }
            set
            {
                this.hawkCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string HawkDescription
        {
            get
            {
                return this.hawkDescriptionField;
            }
            set
            {
                this.hawkDescriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string IDVerifiedCode
        {
            get
            {
                return this.iDVerifiedCodeField;
            }
            set
            {
                this.iDVerifiedCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string IDDescription
        {
            get
            {
                return this.iDDescriptionField;
            }
            set
            {
                this.iDDescriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string OldIDWarning
        {
            get
            {
                return this.oldIDWarningField;
            }
            set
            {
                this.oldIDWarningField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string OldIDDescription
        {
            get
            {
                return this.oldIDDescriptionField;
            }
            set
            {
                this.oldIDDescriptionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string Forename1
        {
            get
            {
                return this.forename1Field;
            }
            set
            {
                this.forename1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string Forename2
        {
            get
            {
                return this.forename2Field;
            }
            set
            {
                this.forename2Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string TicketNumber
        {
            get
            {
                return this.ticketNumberField;
            }
            set
            {
                this.ticketNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string SubscriberNumber
        {
            get
            {
                return this.subscriberNumberField;
            }
            set
            {
                this.subscriberNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public string SubscriberName
        {
            get
            {
                return this.subscriberNameField;
            }
            set
            {
                this.subscriberNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 16)]
        public string SubscriberReference
        {
            get
            {
                return this.subscriberReferenceField;
            }
            set
            {
                this.subscriberReferenceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 17)]
        public string IDNumber
        {
            get
            {
                return this.iDNumberField;
            }
            set
            {
                this.iDNumberField = value;
            }
        }
    }

    /// <remarks/>





    public partial class PrincipalLink : BureauSegment
    {

        private string majorProductField;

        private string surnameField;

        private string forename1Field;

        private string forename2Field;

        private string forename3Field;

        private string iDNumberField;

        private string dateOfBirthField;

        private Established establishedField;

        private string consumerNoField;

        private LinkedCompany[] linkedCompaniesField;

        private LinkedBusinessDefault[] linkedBusinessDefaultsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string Forename1
        {
            get
            {
                return this.forename1Field;
            }
            set
            {
                this.forename1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string Forename2
        {
            get
            {
                return this.forename2Field;
            }
            set
            {
                this.forename2Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string Forename3
        {
            get
            {
                return this.forename3Field;
            }
            set
            {
                this.forename3Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string IDNumber
        {
            get
            {
                return this.iDNumberField;
            }
            set
            {
                this.iDNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string DateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public Established Established
        {
            get
            {
                return this.establishedField;
            }
            set
            {
                this.establishedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string ConsumerNo
        {
            get
            {
                return this.consumerNoField;
            }
            set
            {
                this.consumerNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 9)]
        public LinkedCompany[] LinkedCompanies
        {
            get
            {
                return this.linkedCompaniesField;
            }
            set
            {
                this.linkedCompaniesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 10)]
        public LinkedBusinessDefault[] LinkedBusinessDefaults
        {
            get
            {
                return this.linkedBusinessDefaultsField;
            }
            set
            {
                this.linkedBusinessDefaultsField = value;
            }
        }
    }

    /// <remarks/>





    public partial class LinkedCompany : BureauSegment
    {

        private string[] businessNameField;

        private string[] businessStatusField;

        private string[] registeredDateField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 0)]
        public string[] BusinessName
        {
            get
            {
                return this.businessNameField;
            }
            set
            {
                this.businessNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 1)]
        public string[] BusinessStatus
        {
            get
            {
                return this.businessStatusField;
            }
            set
            {
                this.businessStatusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 2)]
        public string[] RegisteredDate
        {
            get
            {
                return this.registeredDateField;
            }
            set
            {
                this.registeredDateField = value;
            }
        }
    }

    /// <remarks/>





    public partial class LinkedBusinessDefault : BureauSegment
    {

        private string moduleProductField;

        private string[] iTNumberField;

        private string[] registrationNumberField;

        private string[] judgementCountField;

        private string[] adverseCountField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string ModuleProduct
        {
            get
            {
                return this.moduleProductField;
            }
            set
            {
                this.moduleProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 1)]
        public string[] ITNumber
        {
            get
            {
                return this.iTNumberField;
            }
            set
            {
                this.iTNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 2)]
        public string[] RegistrationNumber
        {
            get
            {
                return this.registrationNumberField;
            }
            set
            {
                this.registrationNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 3)]
        public string[] JudgementCount
        {
            get
            {
                return this.judgementCountField;
            }
            set
            {
                this.judgementCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 4)]
        public string[] AdverseCount
        {
            get
            {
                return this.adverseCountField;
            }
            set
            {
                this.adverseCountField = value;
            }
        }
    }

    /// <remarks/>





    public partial class PrincipalNotarialBonds : BureauSegment
    {

        private string majorProductField;

        private string deedsOfficeField;

        private string multipleOwnersField;

        private string bondRegDateField;

        private string bondNumberField;

        private string bondAmountField;

        private string microfilemRefField;

        private string bondTypeField;

        private string bondOwnerField;

        private string ownerIDField;

        private string ownerTypeField;

        private string bondPercentageField;

        private string[] heldOverField;

        private string hOAboveThresholdField;

        private string parentChildIndField;

        private string messageField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string DeedsOffice
        {
            get
            {
                return this.deedsOfficeField;
            }
            set
            {
                this.deedsOfficeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string MultipleOwners
        {
            get
            {
                return this.multipleOwnersField;
            }
            set
            {
                this.multipleOwnersField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string BondRegDate
        {
            get
            {
                return this.bondRegDateField;
            }
            set
            {
                this.bondRegDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string BondNumber
        {
            get
            {
                return this.bondNumberField;
            }
            set
            {
                this.bondNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string BondAmount
        {
            get
            {
                return this.bondAmountField;
            }
            set
            {
                this.bondAmountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string MicrofilemRef
        {
            get
            {
                return this.microfilemRefField;
            }
            set
            {
                this.microfilemRefField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string BondType
        {
            get
            {
                return this.bondTypeField;
            }
            set
            {
                this.bondTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string BondOwner
        {
            get
            {
                return this.bondOwnerField;
            }
            set
            {
                this.bondOwnerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string OwnerID
        {
            get
            {
                return this.ownerIDField;
            }
            set
            {
                this.ownerIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string OwnerType
        {
            get
            {
                return this.ownerTypeField;
            }
            set
            {
                this.ownerTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string BondPercentage
        {
            get
            {
                return this.bondPercentageField;
            }
            set
            {
                this.bondPercentageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 12)]
        public string[] HeldOver
        {
            get
            {
                return this.heldOverField;
            }
            set
            {
                this.heldOverField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string HOAboveThreshold
        {
            get
            {
                return this.hOAboveThresholdField;
            }
            set
            {
                this.hOAboveThresholdField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string ParentChildInd
        {
            get
            {
                return this.parentChildIndField;
            }
            set
            {
                this.parentChildIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public string Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }
    }

    /// <remarks/>





    public partial class RegisteredPrincipal : BureauSegment
    {

        private string majorProductField;

        private string surnameField;

        private string initialsField;

        private string iDNumberField;

        private string memberContributionField;

        private string memberContributionPercField;

        private string rPDateField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string Initials
        {
            get
            {
                return this.initialsField;
            }
            set
            {
                this.initialsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string IDNumber
        {
            get
            {
                return this.iDNumberField;
            }
            set
            {
                this.iDNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string MemberContribution
        {
            get
            {
                return this.memberContributionField;
            }
            set
            {
                this.memberContributionField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string MemberContributionPerc
        {
            get
            {
                return this.memberContributionPercField;
            }
            set
            {
                this.memberContributionPercField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string RPDate
        {
            get
            {
                return this.rPDateField;
            }
            set
            {
                this.rPDateField = value;
            }
        }
    }

    /// <remarks/>





    public partial class RegistrationDetails : BureauSegment
    {

        private string majorProductField;

        private string registrationDateField;

        private string registrationStatusCodeField;

        private string registrationStatusDescField;

        private string liquidationDateField;

        private string registrationNoField;

        private string companyTypeCodeField;

        private string companyTypeDescField;

        private string registrationCountryField;

        private string[] previousRegistrationNoField;

        private string auditorsField;

        private string registeredAddressField;

        private string suburbField;

        private string cityField;

        private string countryField;

        private string postCodeField;

        private string authorisedCapitalField;

        private ShareInfo[] shareInfoField;

        private string issuedCapitalField;

        private string statedCapitalField;

        private CapitalInfo[] capitalInfoField;

        private string reg_Status_ReasonField;

        private string reg_Info_DateField;

        private string[] postalAddressField;

        private string postalPostCodeField;

        private string financialYearEndField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string RegistrationDate
        {
            get
            {
                return this.registrationDateField;
            }
            set
            {
                this.registrationDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string RegistrationStatusCode
        {
            get
            {
                return this.registrationStatusCodeField;
            }
            set
            {
                this.registrationStatusCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string RegistrationStatusDesc
        {
            get
            {
                return this.registrationStatusDescField;
            }
            set
            {
                this.registrationStatusDescField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string LiquidationDate
        {
            get
            {
                return this.liquidationDateField;
            }
            set
            {
                this.liquidationDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string RegistrationNo
        {
            get
            {
                return this.registrationNoField;
            }
            set
            {
                this.registrationNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string CompanyTypeCode
        {
            get
            {
                return this.companyTypeCodeField;
            }
            set
            {
                this.companyTypeCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string CompanyTypeDesc
        {
            get
            {
                return this.companyTypeDescField;
            }
            set
            {
                this.companyTypeDescField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string RegistrationCountry
        {
            get
            {
                return this.registrationCountryField;
            }
            set
            {
                this.registrationCountryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 9)]
        public string[] PreviousRegistrationNo
        {
            get
            {
                return this.previousRegistrationNoField;
            }
            set
            {
                this.previousRegistrationNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string Auditors
        {
            get
            {
                return this.auditorsField;
            }
            set
            {
                this.auditorsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string RegisteredAddress
        {
            get
            {
                return this.registeredAddressField;
            }
            set
            {
                this.registeredAddressField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string Suburb
        {
            get
            {
                return this.suburbField;
            }
            set
            {
                this.suburbField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string Country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public string PostCode
        {
            get
            {
                return this.postCodeField;
            }
            set
            {
                this.postCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 16)]
        public string AuthorisedCapital
        {
            get
            {
                return this.authorisedCapitalField;
            }
            set
            {
                this.authorisedCapitalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 17)]
        public ShareInfo[] ShareInfo
        {
            get
            {
                return this.shareInfoField;
            }
            set
            {
                this.shareInfoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 18)]
        public string IssuedCapital
        {
            get
            {
                return this.issuedCapitalField;
            }
            set
            {
                this.issuedCapitalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 19)]
        public string StatedCapital
        {
            get
            {
                return this.statedCapitalField;
            }
            set
            {
                this.statedCapitalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 20)]
        public CapitalInfo[] CapitalInfo
        {
            get
            {
                return this.capitalInfoField;
            }
            set
            {
                this.capitalInfoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 21)]
        public string Reg_Status_Reason
        {
            get
            {
                return this.reg_Status_ReasonField;
            }
            set
            {
                this.reg_Status_ReasonField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 22)]
        public string Reg_Info_Date
        {
            get
            {
                return this.reg_Info_DateField;
            }
            set
            {
                this.reg_Info_DateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 23)]
        public string[] PostalAddress
        {
            get
            {
                return this.postalAddressField;
            }
            set
            {
                this.postalAddressField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 24)]
        public string PostalPostCode
        {
            get
            {
                return this.postalPostCodeField;
            }
            set
            {
                this.postalPostCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 25)]
        public string FinancialYearEnd
        {
            get
            {
                return this.financialYearEndField;
            }
            set
            {
                this.financialYearEndField = value;
            }
        }
    }

    /// <remarks/>





    public partial class RegistrationDetailsExtended : BureauSegment
    {

        private string majorProductField;

        private string businessRescueDateField;

        private string businessRescueCommentField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string BusinessRescueDate
        {
            get
            {
                return this.businessRescueDateField;
            }
            set
            {
                this.businessRescueDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string BusinessRescueComment
        {
            get
            {
                return this.businessRescueCommentField;
            }
            set
            {
                this.businessRescueCommentField = value;
            }
        }
    }

    /// <remarks/>





    public partial class BusinessRescueDetail : BureauSegment
    {

        private string majorProductField;

        private string registrationInfoDateField;

        private string registrationStatusCodeField;

        private string businessRescueTypeField;

        private string practitionerNameField;

        private string practitionerDateField;

        private string practitionerContactDetailsField;

        private string rescuePlanField;

        private string endDateField;

        private string businessRescueDateField;

        private string businessRescueCommentField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string RegistrationInfoDate
        {
            get
            {
                return this.registrationInfoDateField;
            }
            set
            {
                this.registrationInfoDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string RegistrationStatusCode
        {
            get
            {
                return this.registrationStatusCodeField;
            }
            set
            {
                this.registrationStatusCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string BusinessRescueType
        {
            get
            {
                return this.businessRescueTypeField;
            }
            set
            {
                this.businessRescueTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string PractitionerName
        {
            get
            {
                return this.practitionerNameField;
            }
            set
            {
                this.practitionerNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string PractitionerDate
        {
            get
            {
                return this.practitionerDateField;
            }
            set
            {
                this.practitionerDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string PractitionerContactDetails
        {
            get
            {
                return this.practitionerContactDetailsField;
            }
            set
            {
                this.practitionerContactDetailsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string RescuePlan
        {
            get
            {
                return this.rescuePlanField;
            }
            set
            {
                this.rescuePlanField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string EndDate
        {
            get
            {
                return this.endDateField;
            }
            set
            {
                this.endDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string BusinessRescueDate
        {
            get
            {
                return this.businessRescueDateField;
            }
            set
            {
                this.businessRescueDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string BusinessRescueComment
        {
            get
            {
                return this.businessRescueCommentField;
            }
            set
            {
                this.businessRescueCommentField = value;
            }
        }
    }

    /// <remarks/>





    public partial class SearchResponse : BureauSegment
    {

        private string iTNumberField;

        private string nameField;

        private string nameTypeField;

        private string businessNameField;

        private string physicalAddressField;

        private string suburbField;

        private string townField;

        private string countryField;

        private string postCodeField;

        private string postalAddressField;

        private string postalSuburbField;

        private string postalTownField;

        private string postalCountryField;

        private string postalPostCodeField;

        private string phoneNoField;

        private string faxNoField;

        private string regNoField;

        private string regStatusField;

        private string regStatusCodeField;

        private string tradingNumberField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string ITNumber
        {
            get
            {
                return this.iTNumberField;
            }
            set
            {
                this.iTNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string NameType
        {
            get
            {
                return this.nameTypeField;
            }
            set
            {
                this.nameTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string BusinessName
        {
            get
            {
                return this.businessNameField;
            }
            set
            {
                this.businessNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string PhysicalAddress
        {
            get
            {
                return this.physicalAddressField;
            }
            set
            {
                this.physicalAddressField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string Suburb
        {
            get
            {
                return this.suburbField;
            }
            set
            {
                this.suburbField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string Town
        {
            get
            {
                return this.townField;
            }
            set
            {
                this.townField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string Country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string PostCode
        {
            get
            {
                return this.postCodeField;
            }
            set
            {
                this.postCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string PostalAddress
        {
            get
            {
                return this.postalAddressField;
            }
            set
            {
                this.postalAddressField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string PostalSuburb
        {
            get
            {
                return this.postalSuburbField;
            }
            set
            {
                this.postalSuburbField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string PostalTown
        {
            get
            {
                return this.postalTownField;
            }
            set
            {
                this.postalTownField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string PostalCountry
        {
            get
            {
                return this.postalCountryField;
            }
            set
            {
                this.postalCountryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string PostalPostCode
        {
            get
            {
                return this.postalPostCodeField;
            }
            set
            {
                this.postalPostCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string PhoneNo
        {
            get
            {
                return this.phoneNoField;
            }
            set
            {
                this.phoneNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public string FaxNo
        {
            get
            {
                return this.faxNoField;
            }
            set
            {
                this.faxNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 16)]
        public string RegNo
        {
            get
            {
                return this.regNoField;
            }
            set
            {
                this.regNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 17)]
        public string RegStatus
        {
            get
            {
                return this.regStatusField;
            }
            set
            {
                this.regStatusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 18)]
        public string RegStatusCode
        {
            get
            {
                return this.regStatusCodeField;
            }
            set
            {
                this.regStatusCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 19)]
        public string TradingNumber
        {
            get
            {
                return this.tradingNumberField;
            }
            set
            {
                this.tradingNumberField = value;
            }
        }
    }

    /// <remarks/>





    public partial class SMEAssessment : BureauSegment
    {

        private string majorProductField;

        private string typeOfScoreField;

        private string scoreDateField;

        private string scoreField;

        private string scoreBandField;

        private string bandTrendField;

        private string bandDescField;

        private string oddsOfFailureField;

        private string failureRateField;

        private string sizeField;

        private string sMECommentCodeField;

        private string[] commentsField;

        private string[] keyContrCodesField;

        private string[] subjectGraphValueField;

        private string[] averageGraphValueField;

        private string[] recomActionCodeField;

        private string[] historyDateField;

        private string[] historyScoreField;

        private string[] historyAverageField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string TypeOfScore
        {
            get
            {
                return this.typeOfScoreField;
            }
            set
            {
                this.typeOfScoreField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string ScoreDate
        {
            get
            {
                return this.scoreDateField;
            }
            set
            {
                this.scoreDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string Score
        {
            get
            {
                return this.scoreField;
            }
            set
            {
                this.scoreField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string ScoreBand
        {
            get
            {
                return this.scoreBandField;
            }
            set
            {
                this.scoreBandField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string BandTrend
        {
            get
            {
                return this.bandTrendField;
            }
            set
            {
                this.bandTrendField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string BandDesc
        {
            get
            {
                return this.bandDescField;
            }
            set
            {
                this.bandDescField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string OddsOfFailure
        {
            get
            {
                return this.oddsOfFailureField;
            }
            set
            {
                this.oddsOfFailureField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string FailureRate
        {
            get
            {
                return this.failureRateField;
            }
            set
            {
                this.failureRateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string Size
        {
            get
            {
                return this.sizeField;
            }
            set
            {
                this.sizeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string SMECommentCode
        {
            get
            {
                return this.sMECommentCodeField;
            }
            set
            {
                this.sMECommentCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 11)]
        public string[] Comments
        {
            get
            {
                return this.commentsField;
            }
            set
            {
                this.commentsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 12)]
        public string[] KeyContrCodes
        {
            get
            {
                return this.keyContrCodesField;
            }
            set
            {
                this.keyContrCodesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 13)]
        public string[] SubjectGraphValue
        {
            get
            {
                return this.subjectGraphValueField;
            }
            set
            {
                this.subjectGraphValueField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 14)]
        public string[] AverageGraphValue
        {
            get
            {
                return this.averageGraphValueField;
            }
            set
            {
                this.averageGraphValueField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 15)]
        public string[] RecomActionCode
        {
            get
            {
                return this.recomActionCodeField;
            }
            set
            {
                this.recomActionCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 16)]
        public string[] HistoryDate
        {
            get
            {
                return this.historyDateField;
            }
            set
            {
                this.historyDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 17)]
        public string[] HistoryScore
        {
            get
            {
                return this.historyScoreField;
            }
            set
            {
                this.historyScoreField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 18)]
        public string[] HistoryAverage
        {
            get
            {
                return this.historyAverageField;
            }
            set
            {
                this.historyAverageField = value;
            }
        }
    }

    /// <remarks/>





    public partial class SegmentDescriptions : BureauSegment
    {

        private SegmentDescription[] segmentInfoDescriptionsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 0)]
        public SegmentDescription[] SegmentInfoDescriptions
        {
            get
            {
                return this.segmentInfoDescriptionsField;
            }
            set
            {
                this.segmentInfoDescriptionsField = value;
            }
        }
    }

    /// <remarks/>





    public partial class TradeAccountSynopsis : BureauSegment
    {

        private string majorProductField;

        private AccountSynopsisDetail[] accountSynopsisDetailsField;

        private string messageField;

        private string dateField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 1)]
        public AccountSynopsisDetail[] AccountSynopsisDetails
        {
            get
            {
                return this.accountSynopsisDetailsField;
            }
            set
            {
                this.accountSynopsisDetailsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string Date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }
    }

    /// <remarks/>





    public partial class TradeAgeAnalysis : BureauSegment
    {

        private string majorProductField;

        private AgeAnalysisDetail[] ageAnalysisDetailsField;

        private string messageField;

        private string dateField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 1)]
        public AgeAnalysisDetail[] AgeAnalysisDetails
        {
            get
            {
                return this.ageAnalysisDetailsField;
            }
            set
            {
                this.ageAnalysisDetailsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string Date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }
    }

    /// <remarks/>





    public partial class TradeHistory : BureauSegment
    {

        private string majorProductField;

        private string dateOfRefField;

        private string yearsKnownField;

        private string monthsKnownField;

        private string creditLimitField;

        private string unlimitedField;

        private string purchasesField;

        private string termsTakenField;

        private string termsGivenField;

        private string discountField;

        private string referenceNameField;

        private string insuranceField;

        private string insuranceDescField;

        private string commentField;

        private string assuredValueField;

        private string obtainedField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string DateOfRef
        {
            get
            {
                return this.dateOfRefField;
            }
            set
            {
                this.dateOfRefField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string YearsKnown
        {
            get
            {
                return this.yearsKnownField;
            }
            set
            {
                this.yearsKnownField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string MonthsKnown
        {
            get
            {
                return this.monthsKnownField;
            }
            set
            {
                this.monthsKnownField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string CreditLimit
        {
            get
            {
                return this.creditLimitField;
            }
            set
            {
                this.creditLimitField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string Unlimited
        {
            get
            {
                return this.unlimitedField;
            }
            set
            {
                this.unlimitedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string Purchases
        {
            get
            {
                return this.purchasesField;
            }
            set
            {
                this.purchasesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string TermsTaken
        {
            get
            {
                return this.termsTakenField;
            }
            set
            {
                this.termsTakenField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string TermsGiven
        {
            get
            {
                return this.termsGivenField;
            }
            set
            {
                this.termsGivenField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string Discount
        {
            get
            {
                return this.discountField;
            }
            set
            {
                this.discountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string ReferenceName
        {
            get
            {
                return this.referenceNameField;
            }
            set
            {
                this.referenceNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string Insurance
        {
            get
            {
                return this.insuranceField;
            }
            set
            {
                this.insuranceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string InsuranceDesc
        {
            get
            {
                return this.insuranceDescField;
            }
            set
            {
                this.insuranceDescField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string AssuredValue
        {
            get
            {
                return this.assuredValueField;
            }
            set
            {
                this.assuredValueField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public string Obtained
        {
            get
            {
                return this.obtainedField;
            }
            set
            {
                this.obtainedField = value;
            }
        }
    }

    /// <remarks/>





    public partial class TradePaymentHistory : BureauSegment
    {

        private string majorProductField;

        private PaymentHistoryDetail[] paymentHistoryDetailsField;

        private string messageField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 1)]
        public PaymentHistoryDetail[] PaymentHistoryDetails
        {
            get
            {
                return this.paymentHistoryDetailsField;
            }
            set
            {
                this.paymentHistoryDetailsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }
    }

    /// <remarks/>





    public partial class TradeReference : BureauSegment
    {

        private string majorProductField;

        private string dateOfRefField;

        private string yearsKnownField;

        private string monthsKnownField;

        private string creditLimitField;

        private string unlimitedField;

        private string purchasesField;

        private string termsTakenField;

        private string termsGivenField;

        private string discountField;

        private string referenceNameField;

        private string insuranceField;

        private string insuranceDescField;

        private string commentField;

        private string obtainedField;

        private string tradeAccountNumberField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string DateOfRef
        {
            get
            {
                return this.dateOfRefField;
            }
            set
            {
                this.dateOfRefField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string YearsKnown
        {
            get
            {
                return this.yearsKnownField;
            }
            set
            {
                this.yearsKnownField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string MonthsKnown
        {
            get
            {
                return this.monthsKnownField;
            }
            set
            {
                this.monthsKnownField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string CreditLimit
        {
            get
            {
                return this.creditLimitField;
            }
            set
            {
                this.creditLimitField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string Unlimited
        {
            get
            {
                return this.unlimitedField;
            }
            set
            {
                this.unlimitedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string Purchases
        {
            get
            {
                return this.purchasesField;
            }
            set
            {
                this.purchasesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string TermsTaken
        {
            get
            {
                return this.termsTakenField;
            }
            set
            {
                this.termsTakenField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string TermsGiven
        {
            get
            {
                return this.termsGivenField;
            }
            set
            {
                this.termsGivenField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string Discount
        {
            get
            {
                return this.discountField;
            }
            set
            {
                this.discountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string ReferenceName
        {
            get
            {
                return this.referenceNameField;
            }
            set
            {
                this.referenceNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string Insurance
        {
            get
            {
                return this.insuranceField;
            }
            set
            {
                this.insuranceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string InsuranceDesc
        {
            get
            {
                return this.insuranceDescField;
            }
            set
            {
                this.insuranceDescField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string Obtained
        {
            get
            {
                return this.obtainedField;
            }
            set
            {
                this.obtainedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public string TradeAccountNumber
        {
            get
            {
                return this.tradeAccountNumberField;
            }
            set
            {
                this.tradeAccountNumberField = value;
            }
        }
    }

    /// <remarks/>





    public partial class TradeReferenceSummary : BureauSegment
    {

        private string majorProductField;

        private string threeMonthTotalField;

        private string sixMonthTotalField;

        private string twelveMonthTotalField;

        private string twentyFourMonthTotalField;

        private string threeMonthAverageField;

        private string sixMonthAverageField;

        private string twelveMonthAverageField;

        private string twentyFourMonthAverageField;

        private string threeMonthNumberWithinTermsField;

        private string sixMonthNumberWithinTermsField;

        private string twelveMonthNumberWithinTermsField;

        private string twentyFourMonthNumberWithinTermsField;

        private string threeMonthNumberOutsideTermsField;

        private string sixMonthNumberOutsideTermsField;

        private string twelveMonthNumberOutsideTermsField;

        private string twentyFourMonthNumberOutsideTermsField;

        private string threeMonthWeightedAverageField;

        private string sixMonthWeightedAverageField;

        private string twelveMonthWeightedAverageField;

        private string twentyFourMonthWeightedAverageField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string ThreeMonthTotal
        {
            get
            {
                return this.threeMonthTotalField;
            }
            set
            {
                this.threeMonthTotalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string SixMonthTotal
        {
            get
            {
                return this.sixMonthTotalField;
            }
            set
            {
                this.sixMonthTotalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string TwelveMonthTotal
        {
            get
            {
                return this.twelveMonthTotalField;
            }
            set
            {
                this.twelveMonthTotalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string TwentyFourMonthTotal
        {
            get
            {
                return this.twentyFourMonthTotalField;
            }
            set
            {
                this.twentyFourMonthTotalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string ThreeMonthAverage
        {
            get
            {
                return this.threeMonthAverageField;
            }
            set
            {
                this.threeMonthAverageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string SixMonthAverage
        {
            get
            {
                return this.sixMonthAverageField;
            }
            set
            {
                this.sixMonthAverageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string TwelveMonthAverage
        {
            get
            {
                return this.twelveMonthAverageField;
            }
            set
            {
                this.twelveMonthAverageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string TwentyFourMonthAverage
        {
            get
            {
                return this.twentyFourMonthAverageField;
            }
            set
            {
                this.twentyFourMonthAverageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string ThreeMonthNumberWithinTerms
        {
            get
            {
                return this.threeMonthNumberWithinTermsField;
            }
            set
            {
                this.threeMonthNumberWithinTermsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string SixMonthNumberWithinTerms
        {
            get
            {
                return this.sixMonthNumberWithinTermsField;
            }
            set
            {
                this.sixMonthNumberWithinTermsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string TwelveMonthNumberWithinTerms
        {
            get
            {
                return this.twelveMonthNumberWithinTermsField;
            }
            set
            {
                this.twelveMonthNumberWithinTermsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string TwentyFourMonthNumberWithinTerms
        {
            get
            {
                return this.twentyFourMonthNumberWithinTermsField;
            }
            set
            {
                this.twentyFourMonthNumberWithinTermsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string ThreeMonthNumberOutsideTerms
        {
            get
            {
                return this.threeMonthNumberOutsideTermsField;
            }
            set
            {
                this.threeMonthNumberOutsideTermsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string SixMonthNumberOutsideTerms
        {
            get
            {
                return this.sixMonthNumberOutsideTermsField;
            }
            set
            {
                this.sixMonthNumberOutsideTermsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public string TwelveMonthNumberOutsideTerms
        {
            get
            {
                return this.twelveMonthNumberOutsideTermsField;
            }
            set
            {
                this.twelveMonthNumberOutsideTermsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 16)]
        public string TwentyFourMonthNumberOutsideTerms
        {
            get
            {
                return this.twentyFourMonthNumberOutsideTermsField;
            }
            set
            {
                this.twentyFourMonthNumberOutsideTermsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 17)]
        public string ThreeMonthWeightedAverage
        {
            get
            {
                return this.threeMonthWeightedAverageField;
            }
            set
            {
                this.threeMonthWeightedAverageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 18)]
        public string SixMonthWeightedAverage
        {
            get
            {
                return this.sixMonthWeightedAverageField;
            }
            set
            {
                this.sixMonthWeightedAverageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 19)]
        public string TwelveMonthWeightedAverage
        {
            get
            {
                return this.twelveMonthWeightedAverageField;
            }
            set
            {
                this.twelveMonthWeightedAverageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 20)]
        public string TwentyFourMonthWeightedAverage
        {
            get
            {
                return this.twentyFourMonthWeightedAverageField;
            }
            set
            {
                this.twentyFourMonthWeightedAverageField = value;
            }
        }
    }

    /// <remarks/>





    public partial class Tradex : BureauSegment
    {

        private string majorProductField;

        private string debtorNameField;

        private string calculationPeriodField;

        private string totalInvoicesField;

        private string totalInvoiceAmountField;

        private string averageDaysEarlyLateField;

        private CreditBand[] creditBandsField;

        private string dateCreatedField;

        private string globalScoreField;

        private string totalOverdueInvoicesField;

        private string totalOverdueAmountField;

        private string scoreCriteriaField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string DebtorName
        {
            get
            {
                return this.debtorNameField;
            }
            set
            {
                this.debtorNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string CalculationPeriod
        {
            get
            {
                return this.calculationPeriodField;
            }
            set
            {
                this.calculationPeriodField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string TotalInvoices
        {
            get
            {
                return this.totalInvoicesField;
            }
            set
            {
                this.totalInvoicesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string TotalInvoiceAmount
        {
            get
            {
                return this.totalInvoiceAmountField;
            }
            set
            {
                this.totalInvoiceAmountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string AverageDaysEarlyLate
        {
            get
            {
                return this.averageDaysEarlyLateField;
            }
            set
            {
                this.averageDaysEarlyLateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 6)]
        public CreditBand[] CreditBands
        {
            get
            {
                return this.creditBandsField;
            }
            set
            {
                this.creditBandsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string DateCreated
        {
            get
            {
                return this.dateCreatedField;
            }
            set
            {
                this.dateCreatedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string GlobalScore
        {
            get
            {
                return this.globalScoreField;
            }
            set
            {
                this.globalScoreField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string TotalOverdueInvoices
        {
            get
            {
                return this.totalOverdueInvoicesField;
            }
            set
            {
                this.totalOverdueInvoicesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string TotalOverdueAmount
        {
            get
            {
                return this.totalOverdueAmountField;
            }
            set
            {
                this.totalOverdueAmountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string ScoreCriteria
        {
            get
            {
                return this.scoreCriteriaField;
            }
            set
            {
                this.scoreCriteriaField = value;
            }
        }
    }

    /// <remarks/>





    public partial class TicketStatus : BureauSegment
    {

        private string[] ticketField;

        private string[] subjectNameField;

        private string[] statusField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 0)]
        public string[] Ticket
        {
            get
            {
                return this.ticketField;
            }
            set
            {
                this.ticketField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 1)]
        public string[] SubjectName
        {
            get
            {
                return this.subjectNameField;
            }
            set
            {
                this.subjectNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 2)]
        public string[] Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }
    }

    /// <remarks/>





    public partial class UnmatchedCourtRecord : BureauSegment
    {

        private string majorProductField;

        private string numberFoundField;

        private string actionDateField;

        private string typeCodeField;

        private string claimAmountField;

        private string typeDescField;

        private string defendantNameField;

        private string defendantTradeStyleField;

        private string courtRecordAddressField;

        private string suburbField;

        private string cityField;

        private string countryField;

        private string postCodeField;

        private string natureOfDebtField;

        private string defendantDistrictField;

        private string caseNumberField;

        private string courtDistrictField;

        private string courtTypeField;

        private string plaintiffNameField;

        private string attorneyField;

        private string commentField;

        private string abandonDateField;

        private string returnDateField;

        private string messageField;

        private string serialNumberField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string NumberFound
        {
            get
            {
                return this.numberFoundField;
            }
            set
            {
                this.numberFoundField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string ActionDate
        {
            get
            {
                return this.actionDateField;
            }
            set
            {
                this.actionDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string TypeCode
        {
            get
            {
                return this.typeCodeField;
            }
            set
            {
                this.typeCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string ClaimAmount
        {
            get
            {
                return this.claimAmountField;
            }
            set
            {
                this.claimAmountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string TypeDesc
        {
            get
            {
                return this.typeDescField;
            }
            set
            {
                this.typeDescField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string DefendantName
        {
            get
            {
                return this.defendantNameField;
            }
            set
            {
                this.defendantNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string DefendantTradeStyle
        {
            get
            {
                return this.defendantTradeStyleField;
            }
            set
            {
                this.defendantTradeStyleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string CourtRecordAddress
        {
            get
            {
                return this.courtRecordAddressField;
            }
            set
            {
                this.courtRecordAddressField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string Suburb
        {
            get
            {
                return this.suburbField;
            }
            set
            {
                this.suburbField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string Country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string PostCode
        {
            get
            {
                return this.postCodeField;
            }
            set
            {
                this.postCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string NatureOfDebt
        {
            get
            {
                return this.natureOfDebtField;
            }
            set
            {
                this.natureOfDebtField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string DefendantDistrict
        {
            get
            {
                return this.defendantDistrictField;
            }
            set
            {
                this.defendantDistrictField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public string CaseNumber
        {
            get
            {
                return this.caseNumberField;
            }
            set
            {
                this.caseNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 16)]
        public string CourtDistrict
        {
            get
            {
                return this.courtDistrictField;
            }
            set
            {
                this.courtDistrictField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 17)]
        public string CourtType
        {
            get
            {
                return this.courtTypeField;
            }
            set
            {
                this.courtTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 18)]
        public string PlaintiffName
        {
            get
            {
                return this.plaintiffNameField;
            }
            set
            {
                this.plaintiffNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 19)]
        public string Attorney
        {
            get
            {
                return this.attorneyField;
            }
            set
            {
                this.attorneyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 20)]
        public string Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 21)]
        public string AbandonDate
        {
            get
            {
                return this.abandonDateField;
            }
            set
            {
                this.abandonDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 22)]
        public string ReturnDate
        {
            get
            {
                return this.returnDateField;
            }
            set
            {
                this.returnDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 23)]
        public string Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 24)]
        public string SerialNumber
        {
            get
            {
                return this.serialNumberField;
            }
            set
            {
                this.serialNumberField = value;
            }
        }
    }

    /// <remarks/>





    public partial class UnmatchedDefault : BureauSegment
    {

        private string majorProductField;

        private string numberFoundField;

        private string defaultDateField;

        private string defaultNameField;

        private string defaultTradeStyleField;

        private string defaultAddressField;

        private string suburbField;

        private string cityField;

        private string countryField;

        private string postCodeField;

        private string amountField;

        private string commentField;

        private string subscriberNameField;

        private string supplierNameField;

        private string messageField;

        private string serialNoField;

        private string onBehalfOfField;

        private string statusField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string NumberFound
        {
            get
            {
                return this.numberFoundField;
            }
            set
            {
                this.numberFoundField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string DefaultDate
        {
            get
            {
                return this.defaultDateField;
            }
            set
            {
                this.defaultDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string DefaultName
        {
            get
            {
                return this.defaultNameField;
            }
            set
            {
                this.defaultNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string DefaultTradeStyle
        {
            get
            {
                return this.defaultTradeStyleField;
            }
            set
            {
                this.defaultTradeStyleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string DefaultAddress
        {
            get
            {
                return this.defaultAddressField;
            }
            set
            {
                this.defaultAddressField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string Suburb
        {
            get
            {
                return this.suburbField;
            }
            set
            {
                this.suburbField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string Country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string PostCode
        {
            get
            {
                return this.postCodeField;
            }
            set
            {
                this.postCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string Amount
        {
            get
            {
                return this.amountField;
            }
            set
            {
                this.amountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string SubscriberName
        {
            get
            {
                return this.subscriberNameField;
            }
            set
            {
                this.subscriberNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string SupplierName
        {
            get
            {
                return this.supplierNameField;
            }
            set
            {
                this.supplierNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public string SerialNo
        {
            get
            {
                return this.serialNoField;
            }
            set
            {
                this.serialNoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 16)]
        public string OnBehalfOf
        {
            get
            {
                return this.onBehalfOfField;
            }
            set
            {
                this.onBehalfOfField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 17)]
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }
    }

    /// <remarks/>





    public partial class UnmatchedNotarialBond : BureauSegment
    {

        private string majorProductField;

        private string numberFoundField;

        private string bondDateField;

        private string mortgagorField;

        private string tradeStyleField;

        private string addressField;

        private string suburbField;

        private string cityField;

        private string countryField;

        private string postCodeField;

        private string bondNumberField;

        private string bondAmountField;

        private string bondPercentField;

        private string bondDistrictField;

        private string mortgageeField;

        private string dateCancelledField;

        private string messageField;

        private string serialNumberField;

        private string mortgagorDistrictField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string NumberFound
        {
            get
            {
                return this.numberFoundField;
            }
            set
            {
                this.numberFoundField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string BondDate
        {
            get
            {
                return this.bondDateField;
            }
            set
            {
                this.bondDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string Mortgagor
        {
            get
            {
                return this.mortgagorField;
            }
            set
            {
                this.mortgagorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string TradeStyle
        {
            get
            {
                return this.tradeStyleField;
            }
            set
            {
                this.tradeStyleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string Address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string Suburb
        {
            get
            {
                return this.suburbField;
            }
            set
            {
                this.suburbField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string Country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string PostCode
        {
            get
            {
                return this.postCodeField;
            }
            set
            {
                this.postCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string BondNumber
        {
            get
            {
                return this.bondNumberField;
            }
            set
            {
                this.bondNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string BondAmount
        {
            get
            {
                return this.bondAmountField;
            }
            set
            {
                this.bondAmountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string BondPercent
        {
            get
            {
                return this.bondPercentField;
            }
            set
            {
                this.bondPercentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string BondDistrict
        {
            get
            {
                return this.bondDistrictField;
            }
            set
            {
                this.bondDistrictField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string Mortgagee
        {
            get
            {
                return this.mortgageeField;
            }
            set
            {
                this.mortgageeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public string DateCancelled
        {
            get
            {
                return this.dateCancelledField;
            }
            set
            {
                this.dateCancelledField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 16)]
        public string Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 17)]
        public string SerialNumber
        {
            get
            {
                return this.serialNumberField;
            }
            set
            {
                this.serialNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 18)]
        public string MortgagorDistrict
        {
            get
            {
                return this.mortgagorDistrictField;
            }
            set
            {
                this.mortgagorDistrictField = value;
            }
        }
    }

    /// <remarks/>





    public partial class Vehicles : BureauSegment
    {

        private string majorProductField;

        private string commentField;

        private Vehicle[] vehicleListField;

        private string dateInfoField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 2)]
        public Vehicle[] VehicleList
        {
            get
            {
                return this.vehicleListField;
            }
            set
            {
                this.vehicleListField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string DateInfo
        {
            get
            {
                return this.dateInfoField;
            }
            set
            {
                this.dateInfoField = value;
            }
        }
    }

    /// <remarks/>





    public partial class VeriCheque : BureauSegment
    {

        private string majorProductField;

        private string numberFoundField;

        private string veriRegisterNameField;

        private string addressField;

        private string suburbField;

        private string cityField;

        private string countryField;

        private string postCodeField;

        private string reasonField;

        private string veriDateField;

        private string veriAmountField;

        private string messageField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string NumberFound
        {
            get
            {
                return this.numberFoundField;
            }
            set
            {
                this.numberFoundField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string VeriRegisterName
        {
            get
            {
                return this.veriRegisterNameField;
            }
            set
            {
                this.veriRegisterNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string Address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string Suburb
        {
            get
            {
                return this.suburbField;
            }
            set
            {
                this.suburbField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string Country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string PostCode
        {
            get
            {
                return this.postCodeField;
            }
            set
            {
                this.postCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string Reason
        {
            get
            {
                return this.reasonField;
            }
            set
            {
                this.reasonField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string VeriDate
        {
            get
            {
                return this.veriDateField;
            }
            set
            {
                this.veriDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string VeriAmount
        {
            get
            {
                return this.veriAmountField;
            }
            set
            {
                this.veriAmountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string Message
        {
            get
            {
                return this.messageField;
            }
            set
            {
                this.messageField = value;
            }
        }
    }

    /// <remarks/>





    public partial class ComplianceIndicatorLI : BureauSegment
    {

        private string majorProductField;

        private string iTNumberField;

        private string segmentTypeField;

        private string nCAComplianceIndField;

        private string cPAComplianceIndField;

        private string dateField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string ITNumber
        {
            get
            {
                return this.iTNumberField;
            }
            set
            {
                this.iTNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string SegmentType
        {
            get
            {
                return this.segmentTypeField;
            }
            set
            {
                this.segmentTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string NCAComplianceInd
        {
            get
            {
                return this.nCAComplianceIndField;
            }
            set
            {
                this.nCAComplianceIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string CPAComplianceInd
        {
            get
            {
                return this.cPAComplianceIndField;
            }
            set
            {
                this.cPAComplianceIndField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string Date
        {
            get
            {
                return this.dateField;
            }
            set
            {
                this.dateField = value;
            }
        }
    }

    /// <remarks/>





    public partial class AccountVerificationVA : BureauSegment
    {

        private string majorProductField;

        private string bankNameField;

        private string branchCodeField;

        private string branchNameField;

        private string accountNumberField;

        private string accountTypeField;

        private string accountHolderField;

        private string startDateField;

        private string verifiedField;

        private string verifiedDatedField;

        private string accountFoundField;

        private string iDMatchField;

        private string surnameMatchField;

        private string accountOpenField;

        private string accountDormantField;

        private string accountOpen3MonthsField;

        private string accountAcceptsDebitsField;

        private string accountAcceptsCreditsField;

        private string errorReasonField;

        private string commentField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string MajorProduct
        {
            get
            {
                return this.majorProductField;
            }
            set
            {
                this.majorProductField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public string BankName
        {
            get
            {
                return this.bankNameField;
            }
            set
            {
                this.bankNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string BranchCode
        {
            get
            {
                return this.branchCodeField;
            }
            set
            {
                this.branchCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string BranchName
        {
            get
            {
                return this.branchNameField;
            }
            set
            {
                this.branchNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public string AccountNumber
        {
            get
            {
                return this.accountNumberField;
            }
            set
            {
                this.accountNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public string AccountType
        {
            get
            {
                return this.accountTypeField;
            }
            set
            {
                this.accountTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public string AccountHolder
        {
            get
            {
                return this.accountHolderField;
            }
            set
            {
                this.accountHolderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 7)]
        public string StartDate
        {
            get
            {
                return this.startDateField;
            }
            set
            {
                this.startDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 8)]
        public string Verified
        {
            get
            {
                return this.verifiedField;
            }
            set
            {
                this.verifiedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public string VerifiedDated
        {
            get
            {
                return this.verifiedDatedField;
            }
            set
            {
                this.verifiedDatedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 10)]
        public string AccountFound
        {
            get
            {
                return this.accountFoundField;
            }
            set
            {
                this.accountFoundField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 11)]
        public string IDMatch
        {
            get
            {
                return this.iDMatchField;
            }
            set
            {
                this.iDMatchField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 12)]
        public string SurnameMatch
        {
            get
            {
                return this.surnameMatchField;
            }
            set
            {
                this.surnameMatchField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 13)]
        public string AccountOpen
        {
            get
            {
                return this.accountOpenField;
            }
            set
            {
                this.accountOpenField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 14)]
        public string AccountDormant
        {
            get
            {
                return this.accountDormantField;
            }
            set
            {
                this.accountDormantField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 15)]
        public string AccountOpen3Months
        {
            get
            {
                return this.accountOpen3MonthsField;
            }
            set
            {
                this.accountOpen3MonthsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 16)]
        public string AccountAcceptsDebits
        {
            get
            {
                return this.accountAcceptsDebitsField;
            }
            set
            {
                this.accountAcceptsDebitsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 17)]
        public string AccountAcceptsCredits
        {
            get
            {
                return this.accountAcceptsCreditsField;
            }
            set
            {
                this.accountAcceptsCreditsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 18)]
        public string ErrorReason
        {
            get
            {
                return this.errorReasonField;
            }
            set
            {
                this.errorReasonField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 19)]
        public string Comment
        {
            get
            {
                return this.commentField;
            }
            set
            {
                this.commentField = value;
            }
        }
    }



    public partial class ModuleRequestResponse
    {

        private ModuleRequestResult moduleRequestResultField;

        /// <remarks/>
        public ModuleRequestResult ModuleRequestResult
        {
            get
            {
                return this.moduleRequestResultField;
            }
            set
            {
                this.moduleRequestResultField = value;
            }
        }
    }



    public partial class ModuleRequestResult
    {

        private string rawDataField;

        private ResponseStatus responseStatusField;

        private string errorCodeField;

        private string errorMessageField;

        private System.DateTime processingStartDateField;

        private double processingTimeSecsField;

        private System.Guid uniqueRefGuidField;

        private AccountVerificationVA[] accountVerificationVAField;

        private AccountVerificationVH[] accountVerificationVHField;

        private AggregateCW aggregateCWField;

        private Affiliations[] affiliationsField;

        private BankCodes[] bankCodesField;

        private BankingDetailSummary[] bankingDetailsSummaryField;

        private BankHistory[] bankHistoryField;

        private BankReport[] bankReportField;

        private Branch[] branchField;

        private BCCCX01[] bCCCX01Field;

        private BusinessAdverseSummary businessAdverseSummaryField;

        private BusinessDeedsDI[] businessDeedsDIField;

        private BusinessDeedsComprehensivePB[] businessDeedsComprehensivePBField;

        private BusinessDeedsSummaryDA[] businessDeedsSummaryDAField;

        private BusinessDeedsComprehensivePW[] businessDeedsComprehensivePWField;

        private BusinessBenchmarkBB[] businessbenchmarkField;

        private BusinessNotarialBonds[] businessNotarialBondsField;

        private BusinessPlusPrincipalSummary[] businessPlusPrincipalsSummaryField;

        private CancelledTicket[] cancelledTicketsField;

        private CapitalEmployed capitalEmployedField;

        private CommercialDisputeResponse commercialDisputeResponseField;

        private ConsumerBusinessEnquiry[] consumerBusinessEnquiriesField;

        private ConsumerDefault[] consumerDefaultsField;

        private ConsumerEnquiry[] consumerEnquiriesField;

        private ConsumerHeader[] consumerHeaderField;

        private ConsumerHeaderC1[] consumerHeaderC1Field;

        private ConsumerInfoNO04[] consumerInfoNO04Field;

        private ConsumerJudgement[] consumerJudgementsField;

        private ConsumerNotarialBonds[] consumerNotarialBondsField;

        private ConsumerNotice[] consumerNoticesField;

        private ConsumerTraceAlert[] consumerTraceAlertsField;

        private CourtRecord[] courtRecordsField;

        private CurrentAsset currentAssetField;

        private CurrentLiabilities currentLiabilitiesField;

        private DeedHistory[] deedHistoryField;

        private DeedsMultipleBond[] deedsMultipleBondField;

        private Default[] defaultsField;

        private DynamicRating dynamicRatingField;

        private EmpiricaE1[] empiricaE1Field;

        private EmpiricaEM04 empiricaEM04Field;

        private EmpOfCapital empOfCapitalField;

        private EnquiryHistory[] enquiryHistoryField;

        private EnquirySummary enquirySummaryField;

        private FirstResponse firstResponseField;

        private FinanceHeader financeHeaderField;

        private FinanceData[] financeDataField;

        private FinanceDataFE[] financeDataFEField;

        private FinanceDataFF[] financeDataFFField;

        private FinancialRatios[] financialRatiosField;

        private GeneralBankingInfo[] generalBankingInfoField;

        private Header headerField;

        private LinkedBusinessHeader[] linkedBusinessHeadersField;

        private MailboxRetrieveList mailboxRetrieveListField;

        private ModuleAvailabilityResponse moduleAvailabilityResponseField;

        private Names namesField;

        private NotarialBond[] notarialBondsField;

        private Observation[] observationsField;

        private ObservationCont[] observationContField;

        private Operation[] operationsField;

        private AdditionalOperations[] operationsHeaderField;

        private OtherOperation[] otherOperationsField;

        private Principal[] principalsField;

        private PrincipalDetailEmpirica[] principalsDetailEmpiricaField;

        private PrincipalArchive[] principalArchivesField;

        private PrincipalArchiveP5[] principalArchivesP5Field;

        private PrincipalClearance[] principalClearancesField;

        private PrincipalDeedsComprehensiveCA[] principalDeedsCAField;

        private PrincipalDeedsComprehensiveCV[] principalDeedsComprehensiveCVField;

        private PrincipalDeedsSummaryCO[] principalDeedsSummaryCOField;

        private PrincipalDeedsSummaryP8[] principalDeedsSummaryP8Field;

        private PrincipalFirstResponse principalFirstResponseField;

        private PrincipalIDV[] principalIDVsField;

        private PrincipalLink[] principalLinksField;

        private PrincipalNotarialBonds[] principalNotarialBondsField;

        private RegisteredPrincipal[] registeredPrincipalsField;

        private RegistrationDetails registrationDetailsField;

        private RegistrationDetailsExtended registrationDetailsExtendedField;

        private BusinessRescueDetail businessRescueDetailField;

        private SearchResponse[] searchResponseField;

        private SMEAssessment sMEAssessmentField;

        private SegmentDescriptions[] segmentDescriptionsField;

        private TradeAccountSynopsis[] tradeAccountSynopsisField;

        private TradeAgeAnalysis[] tradeAgeAnalysisField;

        private TradeHistory[] tradeHistoryField;

        private TradePaymentHistory[] tradePaymentHistoryField;

        private TradeReference[] tradeReferencesField;

        private TradeReferenceSummary tradeReferenceSummaryField;

        private Tradex[] tradexField;

        private TicketStatus[] ticketsStatusField;

        private UnmatchedCourtRecord[] unmatchedCourtRecordsField;

        private UnmatchedDefault[] unmatchedDefaultsField;

        private UnmatchedNotarialBond[] unmatchedNotarialBondsField;

        private Vehicles[] vehiclesField;

        private VeriCheque[] veriChequesField;

        private ComplianceIndicatorLI[] complianceIndicatorLIField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        public string RawData
        {
            get
            {
                return this.rawDataField;
            }
            set
            {
                this.rawDataField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        public ResponseStatus ResponseStatus
        {
            get
            {
                return this.responseStatusField;
            }
            set
            {
                this.responseStatusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        public string ErrorCode
        {
            get
            {
                return this.errorCodeField;
            }
            set
            {
                this.errorCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        public string ErrorMessage
        {
            get
            {
                return this.errorMessageField;
            }
            set
            {
                this.errorMessageField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 4)]
        public System.DateTime ProcessingStartDate
        {
            get
            {
                return this.processingStartDateField;
            }
            set
            {
                this.processingStartDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 5)]
        public double ProcessingTimeSecs
        {
            get
            {
                return this.processingTimeSecsField;
            }
            set
            {
                this.processingTimeSecsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 6)]
        public System.Guid UniqueRefGuid
        {
            get
            {
                return this.uniqueRefGuidField;
            }
            set
            {
                this.uniqueRefGuidField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 7)]
        public AccountVerificationVA[] AccountVerificationVA
        {
            get
            {
                return this.accountVerificationVAField;
            }
            set
            {
                this.accountVerificationVAField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 8)]
        public AccountVerificationVH[] AccountVerificationVH
        {
            get
            {
                return this.accountVerificationVHField;
            }
            set
            {
                this.accountVerificationVHField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 9)]
        public AggregateCW AggregateCW
        {
            get
            {
                return this.aggregateCWField;
            }
            set
            {
                this.aggregateCWField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 10)]
        public Affiliations[] Affiliations
        {
            get
            {
                return this.affiliationsField;
            }
            set
            {
                this.affiliationsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 11)]
        public BankCodes[] BankCodes
        {
            get
            {
                return this.bankCodesField;
            }
            set
            {
                this.bankCodesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 12)]
        public BankingDetailSummary[] BankingDetailsSummary
        {
            get
            {
                return this.bankingDetailsSummaryField;
            }
            set
            {
                this.bankingDetailsSummaryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 13)]
        public BankHistory[] BankHistory
        {
            get
            {
                return this.bankHistoryField;
            }
            set
            {
                this.bankHistoryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 14)]
        public BankReport[] BankReport
        {
            get
            {
                return this.bankReportField;
            }
            set
            {
                this.bankReportField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 15)]
        public Branch[] Branch
        {
            get
            {
                return this.branchField;
            }
            set
            {
                this.branchField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 16)]
        public BCCCX01[] BCCCX01
        {
            get
            {
                return this.bCCCX01Field;
            }
            set
            {
                this.bCCCX01Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 17)]
        public BusinessAdverseSummary BusinessAdverseSummary
        {
            get
            {
                return this.businessAdverseSummaryField;
            }
            set
            {
                this.businessAdverseSummaryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 18)]
        public BusinessDeedsDI[] BusinessDeedsDI
        {
            get
            {
                return this.businessDeedsDIField;
            }
            set
            {
                this.businessDeedsDIField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 19)]
        public BusinessDeedsComprehensivePB[] BusinessDeedsComprehensivePB
        {
            get
            {
                return this.businessDeedsComprehensivePBField;
            }
            set
            {
                this.businessDeedsComprehensivePBField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 20)]
        public BusinessDeedsSummaryDA[] BusinessDeedsSummaryDA
        {
            get
            {
                return this.businessDeedsSummaryDAField;
            }
            set
            {
                this.businessDeedsSummaryDAField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 21)]
        public BusinessDeedsComprehensivePW[] BusinessDeedsComprehensivePW
        {
            get
            {
                return this.businessDeedsComprehensivePWField;
            }
            set
            {
                this.businessDeedsComprehensivePWField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 22)]
        public BusinessBenchmarkBB[] Businessbenchmark
        {
            get
            {
                return this.businessbenchmarkField;
            }
            set
            {
                this.businessbenchmarkField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 23)]
        public BusinessNotarialBonds[] BusinessNotarialBonds
        {
            get
            {
                return this.businessNotarialBondsField;
            }
            set
            {
                this.businessNotarialBondsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 24)]
        public BusinessPlusPrincipalSummary[] BusinessPlusPrincipalsSummary
        {
            get
            {
                return this.businessPlusPrincipalsSummaryField;
            }
            set
            {
                this.businessPlusPrincipalsSummaryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 25)]
        public CancelledTicket[] CancelledTickets
        {
            get
            {
                return this.cancelledTicketsField;
            }
            set
            {
                this.cancelledTicketsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 26)]
        public CapitalEmployed CapitalEmployed
        {
            get
            {
                return this.capitalEmployedField;
            }
            set
            {
                this.capitalEmployedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 27)]
        public CommercialDisputeResponse CommercialDisputeResponse
        {
            get
            {
                return this.commercialDisputeResponseField;
            }
            set
            {
                this.commercialDisputeResponseField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 28)]
        public ConsumerBusinessEnquiry[] ConsumerBusinessEnquiries
        {
            get
            {
                return this.consumerBusinessEnquiriesField;
            }
            set
            {
                this.consumerBusinessEnquiriesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 29)]
        public ConsumerDefault[] ConsumerDefaults
        {
            get
            {
                return this.consumerDefaultsField;
            }
            set
            {
                this.consumerDefaultsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 30)]
        public ConsumerEnquiry[] ConsumerEnquiries
        {
            get
            {
                return this.consumerEnquiriesField;
            }
            set
            {
                this.consumerEnquiriesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 31)]
        public ConsumerHeader[] ConsumerHeader
        {
            get
            {
                return this.consumerHeaderField;
            }
            set
            {
                this.consumerHeaderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 32)]
        public ConsumerHeaderC1[] ConsumerHeaderC1
        {
            get
            {
                return this.consumerHeaderC1Field;
            }
            set
            {
                this.consumerHeaderC1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 33)]
        public ConsumerInfoNO04[] ConsumerInfoNO04
        {
            get
            {
                return this.consumerInfoNO04Field;
            }
            set
            {
                this.consumerInfoNO04Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 34)]
        public ConsumerJudgement[] ConsumerJudgements
        {
            get
            {
                return this.consumerJudgementsField;
            }
            set
            {
                this.consumerJudgementsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 35)]
        public ConsumerNotarialBonds[] ConsumerNotarialBonds
        {
            get
            {
                return this.consumerNotarialBondsField;
            }
            set
            {
                this.consumerNotarialBondsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 36)]
        public ConsumerNotice[] ConsumerNotices
        {
            get
            {
                return this.consumerNoticesField;
            }
            set
            {
                this.consumerNoticesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 37)]
        public ConsumerTraceAlert[] ConsumerTraceAlerts
        {
            get
            {
                return this.consumerTraceAlertsField;
            }
            set
            {
                this.consumerTraceAlertsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 38)]
        public CourtRecord[] CourtRecords
        {
            get
            {
                return this.courtRecordsField;
            }
            set
            {
                this.courtRecordsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 39)]
        public CurrentAsset CurrentAsset
        {
            get
            {
                return this.currentAssetField;
            }
            set
            {
                this.currentAssetField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 40)]
        public CurrentLiabilities CurrentLiabilities
        {
            get
            {
                return this.currentLiabilitiesField;
            }
            set
            {
                this.currentLiabilitiesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 41)]
        public DeedHistory[] DeedHistory
        {
            get
            {
                return this.deedHistoryField;
            }
            set
            {
                this.deedHistoryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 42)]
        public DeedsMultipleBond[] DeedsMultipleBond
        {
            get
            {
                return this.deedsMultipleBondField;
            }
            set
            {
                this.deedsMultipleBondField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 43)]
        public Default[] Defaults
        {
            get
            {
                return this.defaultsField;
            }
            set
            {
                this.defaultsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 44)]
        public DynamicRating DynamicRating
        {
            get
            {
                return this.dynamicRatingField;
            }
            set
            {
                this.dynamicRatingField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 45)]
        public EmpiricaE1[] EmpiricaE1
        {
            get
            {
                return this.empiricaE1Field;
            }
            set
            {
                this.empiricaE1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 46)]
        public EmpiricaEM04 EmpiricaEM04
        {
            get
            {
                return this.empiricaEM04Field;
            }
            set
            {
                this.empiricaEM04Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 47)]
        public EmpOfCapital EmpOfCapital
        {
            get
            {
                return this.empOfCapitalField;
            }
            set
            {
                this.empOfCapitalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 48)]
        public EnquiryHistory[] EnquiryHistory
        {
            get
            {
                return this.enquiryHistoryField;
            }
            set
            {
                this.enquiryHistoryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 49)]
        public EnquirySummary EnquirySummary
        {
            get
            {
                return this.enquirySummaryField;
            }
            set
            {
                this.enquirySummaryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 50)]
        public FirstResponse FirstResponse
        {
            get
            {
                return this.firstResponseField;
            }
            set
            {
                this.firstResponseField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 51)]
        public FinanceHeader FinanceHeader
        {
            get
            {
                return this.financeHeaderField;
            }
            set
            {
                this.financeHeaderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 52)]
        public FinanceData[] FinanceData
        {
            get
            {
                return this.financeDataField;
            }
            set
            {
                this.financeDataField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 53)]
        public FinanceDataFE[] FinanceDataFE
        {
            get
            {
                return this.financeDataFEField;
            }
            set
            {
                this.financeDataFEField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 54)]
        public FinanceDataFF[] FinanceDataFF
        {
            get
            {
                return this.financeDataFFField;
            }
            set
            {
                this.financeDataFFField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 55)]
        public FinancialRatios[] FinancialRatios
        {
            get
            {
                return this.financialRatiosField;
            }
            set
            {
                this.financialRatiosField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 56)]
        public GeneralBankingInfo[] GeneralBankingInfo
        {
            get
            {
                return this.generalBankingInfoField;
            }
            set
            {
                this.generalBankingInfoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 57)]
        public Header Header
        {
            get
            {
                return this.headerField;
            }
            set
            {
                this.headerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 58)]
        public LinkedBusinessHeader[] LinkedBusinessHeaders
        {
            get
            {
                return this.linkedBusinessHeadersField;
            }
            set
            {
                this.linkedBusinessHeadersField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 59)]
        public MailboxRetrieveList MailboxRetrieveList
        {
            get
            {
                return this.mailboxRetrieveListField;
            }
            set
            {
                this.mailboxRetrieveListField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 60)]
        public ModuleAvailabilityResponse ModuleAvailabilityResponse
        {
            get
            {
                return this.moduleAvailabilityResponseField;
            }
            set
            {
                this.moduleAvailabilityResponseField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 61)]
        public Names Names
        {
            get
            {
                return this.namesField;
            }
            set
            {
                this.namesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 62)]
        public NotarialBond[] NotarialBonds
        {
            get
            {
                return this.notarialBondsField;
            }
            set
            {
                this.notarialBondsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 63)]
        public Observation[] Observations
        {
            get
            {
                return this.observationsField;
            }
            set
            {
                this.observationsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 64)]
        public ObservationCont[] ObservationCont
        {
            get
            {
                return this.observationContField;
            }
            set
            {
                this.observationContField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 65)]
        public Operation[] Operations
        {
            get
            {
                return this.operationsField;
            }
            set
            {
                this.operationsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 66)]
        public AdditionalOperations[] OperationsHeader
        {
            get
            {
                return this.operationsHeaderField;
            }
            set
            {
                this.operationsHeaderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 67)]
        public OtherOperation[] OtherOperations
        {
            get
            {
                return this.otherOperationsField;
            }
            set
            {
                this.otherOperationsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 68)]
        public Principal[] Principals
        {
            get
            {
                return this.principalsField;
            }
            set
            {
                this.principalsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 69)]
        public PrincipalDetailEmpirica[] PrincipalsDetailEmpirica
        {
            get
            {
                return this.principalsDetailEmpiricaField;
            }
            set
            {
                this.principalsDetailEmpiricaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 70)]
        public PrincipalArchive[] PrincipalArchives
        {
            get
            {
                return this.principalArchivesField;
            }
            set
            {
                this.principalArchivesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 71)]
        public PrincipalArchiveP5[] PrincipalArchivesP5
        {
            get
            {
                return this.principalArchivesP5Field;
            }
            set
            {
                this.principalArchivesP5Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 72)]
        public PrincipalClearance[] PrincipalClearances
        {
            get
            {
                return this.principalClearancesField;
            }
            set
            {
                this.principalClearancesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 73)]
        public PrincipalDeedsComprehensiveCA[] PrincipalDeedsCA
        {
            get
            {
                return this.principalDeedsCAField;
            }
            set
            {
                this.principalDeedsCAField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 74)]
        public PrincipalDeedsComprehensiveCV[] PrincipalDeedsComprehensiveCV
        {
            get
            {
                return this.principalDeedsComprehensiveCVField;
            }
            set
            {
                this.principalDeedsComprehensiveCVField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 75)]
        public PrincipalDeedsSummaryCO[] PrincipalDeedsSummaryCO
        {
            get
            {
                return this.principalDeedsSummaryCOField;
            }
            set
            {
                this.principalDeedsSummaryCOField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 76)]
        public PrincipalDeedsSummaryP8[] PrincipalDeedsSummaryP8
        {
            get
            {
                return this.principalDeedsSummaryP8Field;
            }
            set
            {
                this.principalDeedsSummaryP8Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 77)]
        public PrincipalFirstResponse PrincipalFirstResponse
        {
            get
            {
                return this.principalFirstResponseField;
            }
            set
            {
                this.principalFirstResponseField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 78)]
        public PrincipalIDV[] PrincipalIDVs
        {
            get
            {
                return this.principalIDVsField;
            }
            set
            {
                this.principalIDVsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 79)]
        public PrincipalLink[] PrincipalLinks
        {
            get
            {
                return this.principalLinksField;
            }
            set
            {
                this.principalLinksField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 80)]
        public PrincipalNotarialBonds[] PrincipalNotarialBonds
        {
            get
            {
                return this.principalNotarialBondsField;
            }
            set
            {
                this.principalNotarialBondsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 81)]
        public RegisteredPrincipal[] RegisteredPrincipals
        {
            get
            {
                return this.registeredPrincipalsField;
            }
            set
            {
                this.registeredPrincipalsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 82)]
        public RegistrationDetails RegistrationDetails
        {
            get
            {
                return this.registrationDetailsField;
            }
            set
            {
                this.registrationDetailsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 83)]
        public RegistrationDetailsExtended RegistrationDetailsExtended
        {
            get
            {
                return this.registrationDetailsExtendedField;
            }
            set
            {
                this.registrationDetailsExtendedField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 84)]
        public BusinessRescueDetail BusinessRescueDetail
        {
            get
            {
                return this.businessRescueDetailField;
            }
            set
            {
                this.businessRescueDetailField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 85)]
        public SearchResponse[] SearchResponse
        {
            get
            {
                return this.searchResponseField;
            }
            set
            {
                this.searchResponseField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 86)]
        public SMEAssessment SMEAssessment
        {
            get
            {
                return this.sMEAssessmentField;
            }
            set
            {
                this.sMEAssessmentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 87)]
        public SegmentDescriptions[] SegmentDescriptions
        {
            get
            {
                return this.segmentDescriptionsField;
            }
            set
            {
                this.segmentDescriptionsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 88)]
        public TradeAccountSynopsis[] TradeAccountSynopsis
        {
            get
            {
                return this.tradeAccountSynopsisField;
            }
            set
            {
                this.tradeAccountSynopsisField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 89)]
        public TradeAgeAnalysis[] TradeAgeAnalysis
        {
            get
            {
                return this.tradeAgeAnalysisField;
            }
            set
            {
                this.tradeAgeAnalysisField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 90)]
        public TradeHistory[] TradeHistory
        {
            get
            {
                return this.tradeHistoryField;
            }
            set
            {
                this.tradeHistoryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 91)]
        public TradePaymentHistory[] TradePaymentHistory
        {
            get
            {
                return this.tradePaymentHistoryField;
            }
            set
            {
                this.tradePaymentHistoryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 92)]
        public TradeReference[] TradeReferences
        {
            get
            {
                return this.tradeReferencesField;
            }
            set
            {
                this.tradeReferencesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order = 93)]
        public TradeReferenceSummary TradeReferenceSummary
        {
            get
            {
                return this.tradeReferenceSummaryField;
            }
            set
            {
                this.tradeReferenceSummaryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 94)]
        public Tradex[] Tradex
        {
            get
            {
                return this.tradexField;
            }
            set
            {
                this.tradexField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 95)]
        public TicketStatus[] TicketsStatus
        {
            get
            {
                return this.ticketsStatusField;
            }
            set
            {
                this.ticketsStatusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 96)]
        public UnmatchedCourtRecord[] UnmatchedCourtRecords
        {
            get
            {
                return this.unmatchedCourtRecordsField;
            }
            set
            {
                this.unmatchedCourtRecordsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 97)]
        public UnmatchedDefault[] UnmatchedDefaults
        {
            get
            {
                return this.unmatchedDefaultsField;
            }
            set
            {
                this.unmatchedDefaultsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 98)]
        public UnmatchedNotarialBond[] UnmatchedNotarialBonds
        {
            get
            {
                return this.unmatchedNotarialBondsField;
            }
            set
            {
                this.unmatchedNotarialBondsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 99)]
        public Vehicles[] Vehicles
        {
            get
            {
                return this.vehiclesField;
            }
            set
            {
                this.vehiclesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 100)]
        public VeriCheque[] VeriCheques
        {
            get
            {
                return this.veriChequesField;
            }
            set
            {
                this.veriChequesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Order = 101)]
        public ComplianceIndicatorLI[] ComplianceIndicatorLI
        {
            get
            {
                return this.complianceIndicatorLIField;
            }
            set
            {
                this.complianceIndicatorLIField = value;
            }
        }


    }




    //public enum ResponseStatus
    //{

    //    /// <remarks/>
    //    Success,

    //    /// <remarks/>
    //    Failure,
    //}
}
