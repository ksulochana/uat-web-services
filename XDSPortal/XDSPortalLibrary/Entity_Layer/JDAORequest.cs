﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDSPortalLibrary.Entity_Layer
{
    public class JDAOScoreRequest
    {
        public JDAOScoreRequest() { }
        public string clientReference = string.Empty;
        public object identity = new object();
    }

    public class idDocument
    {
        public idDocument() { }
        public enum IDtype { SA_ID = 1, PASSPORT = 2 };

        public string type = IDtype.SA_ID.ToString();
        public string value = string.Empty;
        public DateTime dateOfBirth = DateTime.Now;


    }
}
