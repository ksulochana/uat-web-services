﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XDSPortalLibrary.Entity_Layer
{
    public class DataSegment
    {
        public DataSegment()
        {

        }

    private int dataSegmentIDField;
    
    private string dataSegmentDescField;
    
    private bool extraDataField;
    
    private bool activeField;
    
    private string createdByUserField;
    
    private System.DateTime createdOnDateField;
    
    private bool createdOnDateFieldSpecified;
    
    private string changedByUserField;
    
    private System.DateTime changedOnDateField;
    
    private bool changedOnDateFieldSpecified;
    
    /// <remarks/>
    public int DataSegmentID {
        get {
            return this.dataSegmentIDField;
        }
        set {
            this.dataSegmentIDField = value;
        }
    }
    
    /// <remarks/>
    public string DataSegmentDesc {
        get {
            return this.dataSegmentDescField;
        }
        set {
            this.dataSegmentDescField = value;
        }
    }
    
    /// <remarks/>
    public bool ExtraData {
        get {
            return this.extraDataField;
        }
        set {
            this.extraDataField = value;
        }
    }
    
    /// <remarks/>
    public bool Active {
        get {
            return this.activeField;
        }
        set {
            this.activeField = value;
        }
    }
    
    /// <remarks/>
    public string CreatedByUser {
        get {
            return this.createdByUserField;
        }
        set {
            this.createdByUserField = value;
        }
    }
    
    /// <remarks/>
    public System.DateTime CreatedOnDate {
        get {
            return this.createdOnDateField;
        }
        set {
            this.createdOnDateField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool CreatedOnDateSpecified {
        get {
            return this.createdOnDateFieldSpecified;
        }
        set {
            this.createdOnDateFieldSpecified = value;
        }
    }
    
    /// <remarks/>
    public string ChangedByUser {
        get {
            return this.changedByUserField;
        }
        set {
            this.changedByUserField = value;
        }
    }
    
    /// <remarks/>
    public System.DateTime ChangedOnDate {
        get {
            return this.changedOnDateField;
        }
        set {
            this.changedOnDateField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool ChangedOnDateSpecified {
        get {
            return this.changedOnDateFieldSpecified;
        }
        set {
            this.changedOnDateFieldSpecified = value;
        }
    }
    }
}
