﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XDSPortalLibrary.Entity_Layer
{
    public class NLRDailyClosures
    {
        public NLRDailyClosures()
        {
        }
        private long nLRDailyClosuresIDField = 0;

        private string transactionIDField = string.Empty;

        private string transactionTypeField = string.Empty;

        private string subscriberDataFileCodeField = string.Empty;

        private string subscriberDataFilePasswordField = string.Empty;

        private string subscriberDataFileGroupIDField = string.Empty;

        private string subscriberDataFileOperatorIDField = string.Empty;

        private string subscriberDataFilesOperatorPasswordField = string.Empty;

        private string nLRLoanRegistrationNumberField = string.Empty;

        private string nLREnquiryReferenceNumberField = string.Empty;

        private string branchCodeField = string.Empty;

        private string accountNumberField = string.Empty;

        private string subAccountNumberField = string.Empty;

        private string sAIDNumberField = string.Empty;

        private string dateOfBirthField = string.Empty;

        private string surnameField = string.Empty;

        private string forename1Field = string.Empty;

        private string forename2Field = string.Empty;

        private string forename3Field = string.Empty;

        private string statusCodeField = string.Empty;

        private string dateOfClosureCancellationField = string.Empty;

        private string transactionStatusField = string.Empty;

        private string errorCodeField = string.Empty;

        private string closureDateField = string.Empty;

        private string closureTimeField = string.Empty;

        private string filler1Field = string.Empty;

        private string createdByUserField = string.Empty;

        private System.DateTime createdOnDateField ;

        private bool createdOnDateFieldSpecified=false;

        private System.DateTime changedOnDateField;

        private bool changedOnDateFieldSpecified=false;

        private string changedByUserField = string.Empty;

        private string recordStatusIndField = string.Empty;

        private int consumerIDField=0;

        private bool consumerIDFieldSpecified=false;

        private string nLRDailyFileDateField = string.Empty;

        private string subscriberNameField = string.Empty;

        private int subscriberIDField=0;

        private bool subscriberIDFieldSpecified=false;

        private int loaderIDField=0;

        private bool loaderIDFieldSpecified=false;

        private string nLRDailyClosureFileNameField = string.Empty;

        private bool hasBeenSyncedField=false;

        private bool hasBeenSyncedFieldSpecified=false;

        private System.DateTime syncDateField;

        private bool syncDateFieldSpecified=false;

        private string oldBranchCodeField = string.Empty;

        private string oldAccountNoField = string.Empty;

        private string oldSubAccountNoField = string.Empty;

        private int xDSSubscriberIDField=0;

        private bool xDSSubscriberIDFieldSpecified=false;

        private bool isMatchedAccountField=false;

        private bool isMatchedAccountFieldSpecified=false;

        private string PassportNofield = string.Empty;

        private int EnquirySubscriberIDfield = 0;

        private string Usernamefield = string.Empty;

        private int SystemUserIDfield = 0;

        private string StatusIndfield = string.Empty;

        private string ErrorDescriptionfield = string.Empty;

        private int NLRDailyRegistrationsLogIDfield = 0;

        private string SubscriberReferencefield = string.Empty;

        private DateTime subscriberEnquiryDatefield;

        private string VoucherCodefield = string.Empty;

        private int Payasyougofield = 0;

        private bool DetailsViewedYNfield = false;

        private DateTime Detailsvieweddatefield;

        private int productIDfield = 0;

        private int BillingtypeIdfield = 0;

        private bool Billablefield = false;

        private string ExtraVarInput1field = string.Empty;

        private string ExtraVarInput2field = string.Empty;

        private string ExtraVarInput3field = string.Empty;

        private int ExtraIntInput1field = 0;

        private int ExtraIntInput2field = 0;

        private string XMLDatafield = string.Empty;

        private string supplierRefNoField = string.Empty;

        /// <remarks/>
        public long NLRDailyClosuresID
        {
            get
            {
                return this.nLRDailyClosuresIDField;
            }
            set
            {
                this.nLRDailyClosuresIDField = value;
            }
        }

        /// <remarks/>
        public string TransactionID
        {
            get
            {
                return this.transactionIDField;
            }
            set
            {
                this.transactionIDField = value;
            }
        }

        /// <remarks/>
        public string TransactionType
        {
            get
            {
                return this.transactionTypeField;
            }
            set
            {
                this.transactionTypeField = value;
            }
        }

        /// <remarks/>
        public string SubscriberDataFileCode
        {
            get
            {
                return this.subscriberDataFileCodeField;
            }
            set
            {
                this.subscriberDataFileCodeField = value;
            }
        }

        /// <remarks/>
        public string SubscriberDataFilePassword
        {
            get
            {
                return this.subscriberDataFilePasswordField;
            }
            set
            {
                this.subscriberDataFilePasswordField = value;
            }
        }

        /// <remarks/>
        public string SubscriberDataFileGroupID
        {
            get
            {
                return this.subscriberDataFileGroupIDField;
            }
            set
            {
                this.subscriberDataFileGroupIDField = value;
            }
        }

        /// <remarks/>
        public string SubscriberDataFileOperatorID
        {
            get
            {
                return this.subscriberDataFileOperatorIDField;
            }
            set
            {
                this.subscriberDataFileOperatorIDField = value;
            }
        }

        /// <remarks/>
        public string SubscriberDataFilesOperatorPassword
        {
            get
            {
                return this.subscriberDataFilesOperatorPasswordField;
            }
            set
            {
                this.subscriberDataFilesOperatorPasswordField = value;
            }
        }

        /// <remarks/>
        public string NLRLoanRegistrationNumber
        {
            get
            {
                return this.nLRLoanRegistrationNumberField;
            }
            set
            {
                this.nLRLoanRegistrationNumberField = value;
            }
        }

        /// <remarks/>
        public string NLREnquiryReferenceNumber
        {
            get
            {
                return this.nLREnquiryReferenceNumberField;
            }
            set
            {
                this.nLREnquiryReferenceNumberField = value;
            }
        }

        /// <remarks/>
        public string BranchCode
        {
            get
            {
                return this.branchCodeField;
            }
            set
            {
                this.branchCodeField = value;
            }
        }

        /// <remarks/>
        public string AccountNumber
        {
            get
            {
                return this.accountNumberField;
            }
            set
            {
                this.accountNumberField = value;
            }
        }

        /// <remarks/>
        public string SubAccountNumber
        {
            get
            {
                return this.subAccountNumberField;
            }
            set
            {
                this.subAccountNumberField = value;
            }
        }

        /// <remarks/>
        public string SAIDNumber
        {
            get
            {
                return this.sAIDNumberField;
            }
            set
            {
                this.sAIDNumberField = value;
            }
        }

        /// <remarks/>
        public string DateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        public string Forename1
        {
            get
            {
                return this.forename1Field;
            }
            set
            {
                this.forename1Field = value;
            }
        }

        /// <remarks/>
        public string Forename2
        {
            get
            {
                return this.forename2Field;
            }
            set
            {
                this.forename2Field = value;
            }
        }

        /// <remarks/>
        public string Forename3
        {
            get
            {
                return this.forename3Field;
            }
            set
            {
                this.forename3Field = value;
            }
        }

        /// <remarks/>
        public string StatusCode
        {
            get
            {
                return this.statusCodeField;
            }
            set
            {
                this.statusCodeField = value;
            }
        }

        /// <remarks/>
        public string DateOfClosureCancellation
        {
            get
            {
                return this.dateOfClosureCancellationField;
            }
            set
            {
                this.dateOfClosureCancellationField = value;
            }
        }

        /// <remarks/>
        public string TransactionStatus
        {
            get
            {
                return this.transactionStatusField;
            }
            set
            {
                this.transactionStatusField = value;
            }
        }

        /// <remarks/>
        public string ErrorCode
        {
            get
            {
                return this.errorCodeField;
            }
            set
            {
                this.errorCodeField = value;
            }
        }

        /// <remarks/>
        public string ClosureDate
        {
            get
            {
                return this.closureDateField;
            }
            set
            {
                this.closureDateField = value;
            }
        }

        /// <remarks/>
        public string ClosureTime
        {
            get
            {
                return this.closureTimeField;
            }
            set
            {
                this.closureTimeField = value;
            }
        }

        /// <remarks/>
        public string Filler1
        {
            get
            {
                return this.filler1Field;
            }
            set
            {
                this.filler1Field = value;
            }
        }

        /// <remarks/>
        public string CreatedByUser
        {
            get
            {
                return this.createdByUserField;
            }
            set
            {
                this.createdByUserField = value;
            }
        }

        /// <remarks/>
        public System.DateTime CreatedOnDate
        {
            get
            {
                return this.createdOnDateField;
            }
            set
            {
                this.createdOnDateField = value;
            }
        }

        /// <remarks/>
        public bool CreatedOnDateSpecified
        {
            get
            {
                return this.createdOnDateFieldSpecified;
            }
            set
            {
                this.createdOnDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public System.DateTime ChangedOnDate
        {
            get
            {
                return this.changedOnDateField;
            }
            set
            {
                this.changedOnDateField = value;
            }
        }

        /// <remarks/>
        public bool ChangedOnDateSpecified
        {
            get
            {
                return this.changedOnDateFieldSpecified;
            }
            set
            {
                this.changedOnDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string ChangedByUser
        {
            get
            {
                return this.changedByUserField;
            }
            set
            {
                this.changedByUserField = value;
            }
        }

        /// <remarks/>
        public string RecordStatusInd
        {
            get
            {
                return this.recordStatusIndField;
            }
            set
            {
                this.recordStatusIndField = value;
            }
        }

        /// <remarks/>
        public int ConsumerID
        {
            get
            {
                return this.consumerIDField;
            }
            set
            {
                this.consumerIDField = value;
            }
        }

        /// <remarks/>
        public bool ConsumerIDSpecified
        {
            get
            {
                return this.consumerIDFieldSpecified;
            }
            set
            {
                this.consumerIDFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string NLRDailyFileDate
        {
            get
            {
                return this.nLRDailyFileDateField;
            }
            set
            {
                this.nLRDailyFileDateField = value;
            }
        }

        /// <remarks/>
        public string SubscriberName
        {
            get
            {
                return this.subscriberNameField;
            }
            set
            {
                this.subscriberNameField = value;
            }
        }

        /// <remarks/>
        public int SubscriberID
        {
            get
            {
                return this.subscriberIDField;
            }
            set
            {
                this.subscriberIDField = value;
            }
        }

        /// <remarks/>
        public bool SubscriberIDSpecified
        {
            get
            {
                return this.subscriberIDFieldSpecified;
            }
            set
            {
                this.subscriberIDFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int LoaderID
        {
            get
            {
                return this.loaderIDField;
            }
            set
            {
                this.loaderIDField = value;
            }
        }

        /// <remarks/>
        public bool LoaderIDSpecified
        {
            get
            {
                return this.loaderIDFieldSpecified;
            }
            set
            {
                this.loaderIDFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string NLRDailyClosureFileName
        {
            get
            {
                return this.nLRDailyClosureFileNameField;
            }
            set
            {
                this.nLRDailyClosureFileNameField = value;
            }
        }

        /// <remarks/>
        public bool HasBeenSynced
        {
            get
            {
                return this.hasBeenSyncedField;
            }
            set
            {
                this.hasBeenSyncedField = value;
            }
        }

        /// <remarks/>
        public bool HasBeenSyncedSpecified
        {
            get
            {
                return this.hasBeenSyncedFieldSpecified;
            }
            set
            {
                this.hasBeenSyncedFieldSpecified = value;
            }
        }

        /// <remarks/>
        public System.DateTime SyncDate
        {
            get
            {
                return this.syncDateField;
            }
            set
            {
                this.syncDateField = value;
            }
        }

        /// <remarks/>
        public bool SyncDateSpecified
        {
            get
            {
                return this.syncDateFieldSpecified;
            }
            set
            {
                this.syncDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string OldBranchCode
        {
            get
            {
                return this.oldBranchCodeField;
            }
            set
            {
                this.oldBranchCodeField = value;
            }
        }

        /// <remarks/>
        public string OldAccountNo
        {
            get
            {
                return this.oldAccountNoField;
            }
            set
            {
                this.oldAccountNoField = value;
            }
        }

        /// <remarks/>
        public string OldSubAccountNo
        {
            get
            {
                return this.oldSubAccountNoField;
            }
            set
            {
                this.oldSubAccountNoField = value;
            }
        }

        /// <remarks/>
        public int XDSSubscriberID
        {
            get
            {
                return this.xDSSubscriberIDField;
            }
            set
            {
                this.xDSSubscriberIDField = value;
            }
        }

        /// <remarks/>
        public bool XDSSubscriberIDSpecified
        {
            get
            {
                return this.xDSSubscriberIDFieldSpecified;
            }
            set
            {
                this.xDSSubscriberIDFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool IsMatchedAccount
        {
            get
            {
                return this.isMatchedAccountField;
            }
            set
            {
                this.isMatchedAccountField = value;
            }
        }

        /// <remarks/>
        public bool IsMatchedAccountSpecified
        {
            get
            {
                return this.isMatchedAccountFieldSpecified;
            }
            set
            {
                this.isMatchedAccountFieldSpecified = value;
            }
        }

        /// <remarks/>

        public string PassportNo
        {
            get
            {
                return this.PassportNofield;
            }
            set
            {
                this.PassportNofield = value;
            }
        }

        /// <remarks/>

        public int EnquirySubscriberID
        {
            get
            {
                return this.EnquirySubscriberIDfield;
            }
            set
            {
                this.EnquirySubscriberIDfield = value;
            }
        }

        /// <remarks/>

        public string Username
        {
            get
            {
                return this.Usernamefield;
            }
            set
            {
                this.Usernamefield = value;
            }
        }


        /// <remarks/>

        public int SystemUserID
        {
            get
            {
                return this.SystemUserIDfield;
            }
            set
            {
                this.SystemUserIDfield = value;
            }
        }


        /// <remarks/>

        public string StatusInd
        {
            get
            {
                return this.StatusIndfield;
            }
            set
            {
                this.StatusIndfield = value;
            }
        }

        
        /// <remarks/>

        public string ErrorDescription
        {
            get
            {
                return this.ErrorDescriptionfield;
            }
            set
            {
                this.ErrorDescriptionfield = value;
            }
        }

        /// <remarks/>

        public int NLRDailyRegistrationsLogID
        {
            get
            {
                return this.NLRDailyRegistrationsLogIDfield;
            }
            set
            {
                this.NLRDailyRegistrationsLogIDfield = value;
            }
        }


        /// <remarks/>

        public string SubscriberReference
        {
            get
            {
                return this.SubscriberReferencefield;
            }
            set
            {
                this.SubscriberReferencefield = value;
            }
        }

        /// <remarks/>

        public DateTime subscriberEnquiryDate
        {
            get
            {
                return this.subscriberEnquiryDatefield;
            }
            set
            {
                this.subscriberEnquiryDatefield = value;
            }
        }

        /// <remarks/>

        public string VoucherCode
        {
            get
            {
                return this.VoucherCodefield;
            }
            set
            {
                this.VoucherCodefield = value;
            }
        }


        /// <remarks/>

        public int Payasyougo
        {
            get
            {
                return this.Payasyougofield;
            }
            set
            {
                this.Payasyougofield = value;
            }
        }


        /// <remarks/>

        public bool DetailsViewedYN
        {
            get
            {
                return this.DetailsViewedYNfield;
            }
            set
            {
                this.DetailsViewedYNfield = value;
            }
        }

        /// <remarks/>

        public DateTime Detailsvieweddate
        {
            get
            {
                return this.Detailsvieweddatefield;
            }
            set
            {
                this.Detailsvieweddatefield = value;
            }
        }


        /// <remarks/>

        public int productID
        {
            get
            {
                return this.productIDfield;
            }
            set
            {
                this.productIDfield = value;
            }
        }

        /// <remarks/>

        public int BillingtypeId
        {
            get
            {
                return this.BillingtypeIdfield;
            }
            set
            {
                this.BillingtypeIdfield = value;
            }
        }

        /// <remarks/>

        public bool Billable
        {
            get
            {
                return this.Billablefield;
            }
            set
            {
                this.Billablefield = value;
            }
        }


        /// <remarks/>

        public string ExtraVarInput1
        {
            get
            {
                return this.ExtraVarInput1field;
            }
            set
            {
                this.ExtraVarInput1field = value;
            }
        }

        /// <remarks/>

        public string ExtraVarInput2
        {
            get
            {
                return this.ExtraVarInput2field;
            }
            set
            {
                this.ExtraVarInput2field = value;
            }
        }

        /// <remarks/>

        public string ExtraVarInput3
        {
            get
            {
                return this.ExtraVarInput3field;
            }
            set
            {
                this.ExtraVarInput3field = value;
            }
        }

        /// <remarks/>

        public int ExtraIntInput1
        {
            get
            {
                return this.ExtraIntInput1field;
            }
            set
            {
                this.ExtraIntInput1field = value;
            }
        }

        /// <remarks/>

        public int ExtraIntInput2
        {
            get
            {
                return this.ExtraIntInput2field;
            }
            set
            {
                this.ExtraIntInput2field = value;
            }
        }
        /// <remarks/>

        public string XMLData
        {
            get
            {
                return this.XMLDatafield;
            }
            set
            {
                this.XMLDatafield = value;
            }
        }

        /// <remarks/>
        public string SupplierRefNo
        {
            get
            {
                return this.supplierRefNoField;
            }
            set
            {
                this.supplierRefNoField = value;
            }
        }


    }
}
