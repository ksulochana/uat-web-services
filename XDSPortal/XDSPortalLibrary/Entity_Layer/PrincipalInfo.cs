﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDSPortalLibrary.Entity
{
    [Serializable]
    public class PrincipalInfo
    {
        public PrincipalInfo()
        {

        }
        

        private string DesignationField;
        private string GrossMonthlyIncomeField;
        private string SurnameField;
        private string FirstNameField;
        private string IDTypeField;
        private string BirthDateField;
        private string IdentityNumberField;
        private string GenderField;
        private string MaritalStatusField;
        private string AddressLine1Field;
        private string AddressLine2Field;
        private string SuburbField;
        private string CityField;
        private string PostalCodeField;
        private string HomeTelephoneCodeField;
        private string HomeTelephoneNumberField;
        private string WorkTelephoneCodeField;
        private string WorkTelephoneNumberField;
        private string CellNumberField;
        private string OccupationField;
        private string EmployerField;
        private string NumberOfYearsAtEmployerField;
        private string BankNameField;
        private string BankAccountTypeField;
        private string BankBranchCodeField;
        private string BankAccountNumberField;
        private string HighestQualificationField;

        public string Designation
        {
            get { return DesignationField; }
            set { DesignationField = value; }
        }

        public string GrossMonthlyIncome
        {
            get { return GrossMonthlyIncomeField; }
            set { GrossMonthlyIncomeField = value; }
        }

        public string Surname
        {
            get { return SurnameField; }
            set { SurnameField = value; }
        }
        public string FirstName
        {
            get { return FirstNameField; }
            set { FirstNameField = value; }
        }
        public string IDType
        {
            get { return IDTypeField; }
            set { IDTypeField = value; }
        }
        public string BirthDate
        {
            get { return BirthDateField; }
            set { BirthDateField = value; }
        }
        public string IdentityNumber
        {
            get { return IdentityNumberField; }
            set { IdentityNumberField = value; }
        }
        public string Gender
        {
            get { return GenderField; }
            set { GenderField = value; }
        }
        public string MaritalStatus
        {
            get { return MaritalStatusField; }
            set { MaritalStatusField = value; }
        }
        public string AddressLine1
        {
            get { return AddressLine1Field; }
            set { AddressLine1Field = value; }
        }
        public string AddressLine2
        {
            get { return AddressLine2Field; }
            set { AddressLine2Field = value; }
        }
        public string Suburb
        {
            get { return SuburbField; }
            set { SuburbField = value; }
        }
        public string City
        {
            get { return CityField; }
            set { CityField = value; }
        }
        public string PostalCode
        {
            get { return PostalCodeField; }
            set { PostalCodeField = value; }
        }
        public string HomeTelephoneCode
        {
            get { return HomeTelephoneCodeField; }
            set { HomeTelephoneCodeField = value; }
        }
        public string HomeTelephoneNumber
        {
            get { return HomeTelephoneNumberField; }
            set { HomeTelephoneNumberField = value; }
        }
        public string WorkTelephoneCode
        {
            get { return WorkTelephoneCodeField; }
            set { WorkTelephoneCodeField = value; }
        }
        public string WorkTelephoneNumber
        {
            get { return WorkTelephoneNumberField; }
            set { WorkTelephoneNumberField = value; }
        }
        public string CellNumber
        {
            get { return CellNumberField; }
            set { CellNumberField = value; }
        }
        public string Occupation
        {
            get { return OccupationField; }
            set { OccupationField = value; }
        }
        public string Employer
        {
            get { return EmployerField; }
            set { EmployerField = value; }
        }
        public string NumberOfYearsAtEmployer
        {
            get { return NumberOfYearsAtEmployerField; }
            set { NumberOfYearsAtEmployerField = value; }
        }
        public string BankName
        {
            get { return BankNameField; }
            set { BankNameField = value; }
        }
        public string BankAccountType
        {
            get { return BankAccountTypeField; }
            set { BankAccountTypeField = value; }
        }
        public string BankBranchCode
        {
            get { return BankBranchCodeField; }
            set { BankBranchCodeField = value; }
        }
        public string BankAccountNumber
        {
            get { return BankAccountNumberField; }
            set { BankAccountNumberField = value; }
        }
        public string HighestQualification
        {
            get { return HighestQualificationField; }
            set { HighestQualificationField = value; }
        }
    }
}
