﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace XDSPortalLibrary.Entity_Layer
{
   public class PublicDomainEnquiry
    {
        public PublicDomainEnquiry()
        {
        }
        int subscriberIDvalue, productIDvalue, ConsumerIDValue = 0;
        string IDnovalue = string.Empty, strTmpReference = string.Empty, passportNovalue = string.Empty, SurNamevalue = string.Empty, MaidenNamevalue = string.Empty, FirstNamevalue = string.Empty, SecondNamevalue = string.Empty, Gendervalue = string.Empty, ExternalReferencevalue = string.Empty, FirstInitialvalue = string.Empty, SecondInitialvalue = string.Empty, ReferenceNoValue = string.Empty;
        DateTime DOBvalue;
        bool ConfirmationChkBoxvalue, IsConsumerMatchingvalue = false;
        DataSet BonusSegmentsDS;
        private bool boolBonusCheck = false;
        private string strdataSegments = string.Empty;
        private string strAssociationCode = string.Empty;

        public string SubscriberAssociationCode
        {
            get { return this.strAssociationCode; }
            set { this.strAssociationCode = value; }
        }

        public string DataSegments
        {
            get { return this.strdataSegments; }
            set { this.strdataSegments = value; }
        }

        public bool BonusCheck
        {
            get { return this.boolBonusCheck; }
            set { this.boolBonusCheck = value; }
        }

        public int ConsumerID
        {
            get
            {
                return this.ConsumerIDValue;
            }
            set
            {
                this.ConsumerIDValue = value;
            }
        }

        public int subscriberID
        {
            get
            {
                if (String.IsNullOrEmpty(this.subscriberIDvalue.ToString()))
                    return Convert.ToInt16(DBNull.Value);
                else
                    return this.subscriberIDvalue;
            }
            set
            {
                this.subscriberIDvalue = value;
            }
        }
        public int ProductID
        {
            get
            {
                if (String.IsNullOrEmpty(this.subscriberIDvalue.ToString()))
                    return Convert.ToInt16(DBNull.Value);
                else
                    return this.productIDvalue;
            }
            set
            {
                this.productIDvalue = value;
            }
        }
        public string IDno
        {
            get
            {
                if (String.IsNullOrEmpty(this.IDnovalue))
                    return DBNull.Value.ToString();
                else
                    return this.IDnovalue;
            }
            set
            {
                this.IDnovalue = value;
            }
        }
        public string Passportno
        {
            get
            {
                if (String.IsNullOrEmpty(this.passportNovalue))
                    return DBNull.Value.ToString();
                else
                    return this.passportNovalue;
            }
            set
            {
                this.passportNovalue = value;
            }
        }
        public string Surname
        {
            get
            {
                if (String.IsNullOrEmpty(this.SurNamevalue))
                    return DBNull.Value.ToString();
                else
                    return this.SurNamevalue;
            }
            set
            {
                this.SurNamevalue = value;
            }
        }
        public string MaidenName
        {
            get
            {
                if (String.IsNullOrEmpty(this.MaidenNamevalue))
                    return DBNull.Value.ToString();
                else
                    return this.MaidenNamevalue;
            }
            set
            {
                this.MaidenNamevalue = value;
            }
        }
        public string FirstName
        {
            get
            {
                if (String.IsNullOrEmpty(this.FirstNamevalue))
                    return DBNull.Value.ToString();
                else
                    return this.FirstNamevalue;
            }
            set
            {
                this.FirstNamevalue = value;
            }
        }
        public string SecondName
        {
            get
            {
                if (String.IsNullOrEmpty(this.SecondNamevalue))
                    return DBNull.Value.ToString();
                else
                    return this.SecondNamevalue;
            }
            set
            {
                this.SecondNamevalue = value;
            }
        }
        public string Gender
        {
            get
            {
                if (String.IsNullOrEmpty(this.Gendervalue))
                    return DBNull.Value.ToString();
                else
                    return this.Gendervalue;
            }
            set
            {
                this.Gendervalue = value;
            }
        }
        public string ExternalReference
        {
            get
            {
                if (String.IsNullOrEmpty(this.ExternalReferencevalue))
                    return DBNull.Value.ToString();
                else
                    return this.ExternalReferencevalue;
            }
            set
            {
                this.ExternalReferencevalue = value;
            }
        }
        public DateTime DOB
        {
            get
            {
                if (this.DOBvalue == null)
                    return Convert.ToDateTime(DBNull.Value);
                else
                    return this.DOBvalue;
            }
            set
            {
                this.DOBvalue = value;
            }
        }
        public string FirstInitial
        {
            get
            {
                if (String.IsNullOrEmpty(this.FirstInitialvalue))
                    return DBNull.Value.ToString();
                else
                    return this.FirstInitialvalue;
            }
            set
            {
                this.FirstInitialvalue = value;
            }
        }
        public string SecondInitial
        {
            get
            {
                if (String.IsNullOrEmpty(this.SecondInitialvalue))
                    return DBNull.Value.ToString();
                else
                    return this.SecondInitialvalue;
            }
            set
            {
                this.SecondInitialvalue = value;
            }
        }
        public bool ConfirmationChkBox
        {
            get
            {
                return this.ConfirmationChkBoxvalue;
            }
            set
            {
                this.ConfirmationChkBoxvalue = value;
            }
        }
        public string ReferenceNo
        {
            get
            {
                if (String.IsNullOrEmpty(this.ReferenceNoValue))
                    return DBNull.Value.ToString();
                else
                    return this.ReferenceNoValue;
            }
            set
            {
                this.ReferenceNoValue = value;
            }
        }
        public DataSet BonusSegments
        {
            get
            {
                return this.BonusSegmentsDS;
            }
            set
            {
                this.BonusSegmentsDS = value;
            }
        }
        public bool IsConsumerMatching
        {
            get
            {
                return this.IsConsumerMatchingvalue;
            }
            set
            {
                this.IsConsumerMatchingvalue = value;
            }
        }
        public string TmpReference
        {
            get
            {
                if (String.IsNullOrEmpty(this.strTmpReference))
                    return DBNull.Value.ToString();
                else
                    return this.strTmpReference;
            }
            set
            {
                this.strTmpReference = value;
            }
        }
    }
}
