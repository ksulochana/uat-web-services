﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace XDSPortalLibrary.Entity_Layer
{

    public partial class MTNSoapReqResp
    {
        public MTNSoapReqResp() { }


        public enum IDType { SAID, PASSPORT, OTHER, NONE };






        public class AddressNA07
        {

            private string consumerNoField = string.Empty;

            private string informationDateField = string.Empty;

            private string line1Field = string.Empty;

            private string line2Field = string.Empty;

            private string suburbField = string.Empty;

            private string cityField = string.Empty;

            private string postalCodeField = string.Empty;

            private string provinceCodeField = string.Empty;

            private string provinceField = string.Empty;

            private string addressPeriodField = string.Empty;

            private string ownerTenantField = string.Empty;

            private string addressChangedField = string.Empty;

            // private AddressNA07[] addressNA071Field;

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string InformationDate
            {
                get
                {
                    return this.informationDateField;
                }
                set
                {
                    this.informationDateField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string Line1
            {
                get
                {
                    return this.line1Field;
                }
                set
                {
                    this.line1Field = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string Line2
            {
                get
                {
                    return this.line2Field;
                }
                set
                {
                    this.line2Field = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string Suburb
            {
                get
                {
                    return this.suburbField;
                }
                set
                {
                    this.suburbField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string City
            {
                get
                {
                    return this.cityField;
                }
                set
                {
                    this.cityField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string PostalCode
            {
                get
                {
                    return this.postalCodeField;
                }
                set
                {
                    this.postalCodeField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string ProvinceCode
            {
                get
                {
                    return this.provinceCodeField;
                }
                set
                {
                    this.provinceCodeField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string Province
            {
                get
                {
                    return this.provinceField;
                }
                set
                {
                    this.provinceField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string AddressPeriod
            {
                get
                {
                    return this.addressPeriodField;
                }
                set
                {
                    this.addressPeriodField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string OwnerTenant
            {
                get
                {
                    return this.ownerTenantField;
                }
                set
                {
                    this.ownerTenantField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string AddressChanged
            {
                get
                {
                    return this.addressChangedField;
                }
                set
                {
                    this.addressChangedField = value;
                }
            }

            /// <remarks/>
            ////[System.Xml.Serialization.XmlElementAttribute("AddressNA07")]
            //public AddressNA07[] AddressNA071
            //{
            //    get
            //    {
            //        return this.addressNA071Field;
            //    }
            //    set
            //    {
            //        this.addressNA071Field = value;
            //    }
            //}
        }



        public class PaymentProfileWMP01
        {

            private string worstPaymentProfileMaxPayNLRCurrent;

            private string worstPaymentProfileMaxPayNLR3M;

            private string worstPaymentProfileMaxPayNLR6M;

            private string worstPaymentProfileMaxPayNLR12M;

            private string worstPaymentProfileMaxPayNLR24M;

            private string worstPaymentProfileMaxPayCPACurrent;

            private string worstPaymentProfileMaxPayCPAL3M;

            private string worstPaymentProfileMaxPayCPAL6M;

            private string worstPaymentProfileMaxPayCPAL12M;

            private string worstPaymentProfileMaxPayCPAL24M;

            private string worstPaymentProfileStatusCurrent;

            private string worstPaymentProfileStatusL3M;

            private string worstPaymentProfileStatusL6M;

            private string worstPaymentProfileStatusL12M;

            private string worstPaymentProfileStatusL18M;

            private string worstPaymentProfileStatusL24M;

            // private AddressNA07[] addressNA071Field;

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string WorstPaymentProfileMaxPayNLRCurrent
            {
                get
                {
                    return this.worstPaymentProfileMaxPayNLRCurrent;
                }
                set
                {
                    this.worstPaymentProfileMaxPayNLRCurrent = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string WorstPaymentProfileMaxPayNLR3M
            {
                get
                {
                    return this.worstPaymentProfileMaxPayNLR3M;
                }
                set
                {
                    this.worstPaymentProfileMaxPayNLR3M = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string WorstPaymentProfileMaxPayNLR6M
            {
                get
                {
                    return this.worstPaymentProfileMaxPayNLR6M;
                }
                set
                {
                    this.worstPaymentProfileMaxPayNLR6M = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string WorstPaymentProfileMaxPayNLR12M
            {
                get
                {
                    return this.worstPaymentProfileMaxPayNLR12M;
                }
                set
                {
                    this.worstPaymentProfileMaxPayNLR12M = value;
                }
            }

            public string WorstPaymentProfileMaxPayNLR24M
            {
                get
                {
                    return this.worstPaymentProfileMaxPayNLR24M;
                }
                set
                {
                    this.worstPaymentProfileMaxPayNLR24M = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string WorstPaymentProfileMaxPayCPACurrent
            {
                get
                {
                    return this.worstPaymentProfileMaxPayCPACurrent;
                }
                set
                {
                    this.worstPaymentProfileMaxPayCPACurrent = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string WorstPaymentProfileMaxPayCPAL3M
            {
                get
                {
                    return this.worstPaymentProfileMaxPayCPAL3M;
                }
                set
                {
                    this.worstPaymentProfileMaxPayCPAL3M = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string WorstPaymentProfileMaxPayCPAL6M
            {
                get
                {
                    return this.worstPaymentProfileMaxPayCPAL6M;
                }
                set
                {
                    this.worstPaymentProfileMaxPayCPAL6M = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string WorstPaymentProfileMaxPayCPAL12M
            {
                get
                {
                    return this.worstPaymentProfileMaxPayCPAL12M;
                }
                set
                {
                    this.worstPaymentProfileMaxPayCPAL12M = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string WorstPaymentProfileMaxPayCPAL24M
            {
                get
                {
                    return this.worstPaymentProfileMaxPayCPAL24M;
                }
                set
                {
                    this.worstPaymentProfileMaxPayCPAL24M = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string WorstPaymentProfileStatusCurrent
            {
                get
                {
                    return this.worstPaymentProfileStatusCurrent;
                }
                set
                {
                    this.worstPaymentProfileStatusCurrent = value;
                }
            }



            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string WorstPaymentProfileStatusL3M
            {
                get
                {
                    return this.worstPaymentProfileStatusL3M;
                }
                set
                {
                    this.worstPaymentProfileStatusL3M = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string WorstPaymentProfileStatusL6M
            {
                get
                {
                    return this.worstPaymentProfileStatusL6M;
                }
                set
                {
                    this.worstPaymentProfileStatusL6M = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string WorstPaymentProfileStatusL12M
            {
                get
                {
                    return this.worstPaymentProfileStatusL12M;
                }
                set
                {
                    this.worstPaymentProfileStatusL12M = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string WorstPaymentProfileStatusL18M
            {
                get
                {
                    return this.worstPaymentProfileStatusL18M;
                }
                set
                {
                    this.worstPaymentProfileStatusL18M = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string WorstPaymentProfileStatusL24M
            {
                get
                {
                    return this.worstPaymentProfileStatusL24M;
                }
                set
                {
                    this.worstPaymentProfileStatusL24M = value;
                }
            }
        }




        public class AddressNA08
        {

            private AddressNA08[] addressNA081Field;


            //[System.Xml.Serialization.XmlElementAttribute("AddressNA08")]
            public AddressNA08[] AddressNA081
            {
                get
                {
                    return this.addressNA081Field;
                }
                set
                {
                    this.addressNA081Field = value;
                }
            }
        }







        public class AKANamesNK04
        {
            private string informationDateField = string.Empty;

            private string akaNameField = string.Empty;

            private AKANamesNK04[] aKANamesNK041Field;

            //[System.Xml.Serialization.XmlElementAttribute("AKANamesNK04")]
            public AKANamesNK04[] AKANamesNK041
            {
                get
                {
                    return this.aKANamesNK041Field;
                }
                set
                {
                    this.aKANamesNK041Field = value;
                }
            }

            public string AKAName
            {
                get
                {
                    return this.akaNameField;
                }
                set
                {
                    this.akaNameField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string InformationDate
            {
                get
                {
                    return this.informationDateField;
                }
                set
                {
                    this.informationDateField = value;
                }
            }
        }








        public class AlertDetailC3
        {

            private AlertDetailC3[] alertDetailC31Field;


            //[System.Xml.Serialization.XmlElementAttribute("AlertDetailC3")]
            public AlertDetailC3[] AlertDetailC31
            {
                get
                {
                    return this.alertDetailC31Field;
                }
                set
                {
                    this.alertDetailC31Field = value;
                }
            }
        }








        public class AlertDetailQM
        {

            private AlertDetailQM[] alertDetailQM1Field;

            //[System.Xml.Serialization.XmlElementAttribute("AlertDetailQM")]
            public AlertDetailQM[] AlertDetailQM1
            {
                get
                {
                    return this.alertDetailQM1Field;
                }
                set
                {
                    this.alertDetailQM1Field = value;
                }
            }
        }







        public class AlertDetailQE
        {

            private AlertDetailQE[] alertDetailQE1Field;


            //[System.Xml.Serialization.XmlElementAttribute("AlertDetailQE")]
            public AlertDetailQE[] AlertDetailQE1
            {
                get
                {
                    return this.alertDetailQE1Field;
                }
                set
                {
                    this.alertDetailQE1Field = value;
                }
            }
        }








        public class AlertDetailQC
        {

            private AlertDetailQC[] alertDetailQC1Field;


            //[System.Xml.Serialization.XmlElementAttribute("AlertDetailQC")]
            public AlertDetailQC[] AlertDetailQC1
            {
                get
                {
                    return this.alertDetailQC1Field;
                }
                set
                {
                    this.alertDetailQC1Field = value;
                }
            }
        }








        public class AlertDetailQD
        {

            private AlertDetailQD[] alertDetailQD1Field;


            //[System.Xml.Serialization.XmlElementAttribute("AlertDetailQD")]
            public AlertDetailQD[] AlertDetailQD1
            {
                get
                {
                    return this.alertDetailQD1Field;
                }
                set
                {
                    this.alertDetailQD1Field = value;
                }
            }
        }








        public class AlertDetailQW
        {

            private AlertDetailQW[] alertDetailQW1Field;


            //[System.Xml.Serialization.XmlElementAttribute("AlertDetailQW")]
            public AlertDetailQW[] AlertDetailQW1
            {
                get
                {
                    return this.alertDetailQW1Field;
                }
                set
                {
                    this.alertDetailQW1Field = value;
                }
            }
        }








        public class AlertDetailQP
        {

            private AlertDetailQP[] alertDetailQP1Field;


            //[System.Xml.Serialization.XmlElementAttribute("AlertDetailQP")]
            public AlertDetailQP[] AlertDetailQP1
            {
                get
                {
                    return this.alertDetailQP1Field;
                }
                set
                {
                    this.alertDetailQP1Field = value;
                }
            }
        }








        public class AlertDetailQN
        {

            private AlertDetailQN[] alertDetailQN1Field;


            //[System.Xml.Serialization.XmlElementAttribute("AlertDetailQN")]
            public AlertDetailQN[] AlertDetailQN1
            {
                get
                {
                    return this.alertDetailQN1Field;
                }
                set
                {
                    this.alertDetailQN1Field = value;
                }
            }
        }








        public class AlertDetailQV
        {

            private AlertDetailQV[] alertDetailQV1Field;


            //[System.Xml.Serialization.XmlElementAttribute("AlertDetailQV")]
            public AlertDetailQV[] AlertDetailQV1
            {
                get
                {
                    return this.alertDetailQV1Field;
                }
                set
                {
                    this.alertDetailQV1Field = value;
                }
            }
        }








        public class AlertDetailQJ
        {

            private AlertDetailQJ[] alertDetailQJ1Field;


            //[System.Xml.Serialization.XmlElementAttribute("AlertDetailQJ")]
            public AlertDetailQJ[] AlertDetailQJ1
            {
                get
                {
                    return this.alertDetailQJ1Field;
                }
                set
                {
                    this.alertDetailQJ1Field = value;
                }
            }
        }








        public class CellphoneValidationNZ01
        {

            private CellphoneValidationNZ01[] cellphoneValidationNZ011Field;


            //[System.Xml.Serialization.XmlElementAttribute("CellphoneValidationNZ01")]
            public CellphoneValidationNZ01[] CellphoneValidationNZ011
            {
                get
                {
                    return this.cellphoneValidationNZ011Field;
                }
                set
                {
                    this.cellphoneValidationNZ011Field = value;
                }
            }
        }








        public class ConsumerDeedsDD01
        {

            private ConsumerDeedsDD01[] consumerDeedsDD011Field;


            //[System.Xml.Serialization.XmlElementAttribute("ConsumerDeedsDD01")]
            public ConsumerDeedsDD01[] ConsumerDeedsDD011
            {
                get
                {
                    return this.consumerDeedsDD011Field;
                }
                set
                {
                    this.consumerDeedsDD011Field = value;
                }
            }
        }








        public class ConsumerMiscellaneousMS
        {

            private ConsumerMiscellaneousMS[] consumerMiscellaneousMS1Field;


            //[System.Xml.Serialization.XmlElementAttribute("ConsumerMiscellaneousMS")]
            public ConsumerMiscellaneousMS[] ConsumerMiscellaneousMS1
            {
                get
                {
                    return this.consumerMiscellaneousMS1Field;
                }
                set
                {
                    this.consumerMiscellaneousMS1Field = value;
                }
            }
        }








        public class ConsumerNumberFrequencyNY01
        {

            private ConsumerNumberFrequencyNY01[] consumerNumberFrequencyNY011Field;


            //[System.Xml.Serialization.XmlElementAttribute("ConsumerNumberFrequencyNY01")]
            public ConsumerNumberFrequencyNY01[] ConsumerNumberFrequencyNY011
            {
                get
                {
                    return this.consumerNumberFrequencyNY011Field;
                }
                set
                {
                    this.consumerNumberFrequencyNY011Field = value;
                }
            }
        }








        public class CPAAccountInformationExtendedP8
        {

            private CPAAccountInformationExtendedP8[] cPAAccountInformationExtendedP81Field;


            //[System.Xml.Serialization.XmlElementAttribute("CPAAccountInformationExtendedP8")]
            public CPAAccountInformationExtendedP8[] CPAAccountInformationExtendedP81
            {
                get
                {
                    return this.cPAAccountInformationExtendedP81Field;
                }
                set
                {
                    this.cPAAccountInformationExtendedP81Field = value;
                }
            }
        }








        public class DebtReviewDR01
        {

            private DebtReviewDR01[] debtReviewDR011Field;


            //[System.Xml.Serialization.XmlElementAttribute("DebtReviewDR01")]
            public DebtReviewDR01[] DebtReviewDR011
            {
                get
                {
                    return this.debtReviewDR011Field;
                }
                set
                {
                    this.debtReviewDR011Field = value;
                }
            }
        }








        public class DefaultsND07
        {

            private DefaultsND07[] defaultsND071Field;


            //[System.Xml.Serialization.XmlElementAttribute("DefaultsND07")]
            public DefaultsND07[] DefaultsND071
            {
                get
                {
                    return this.defaultsND071Field;
                }
                set
                {
                    this.defaultsND071Field = value;
                }
            }
        }








        public class DriversLicenceUY01
        {

            private DriversLicenceUY01[] driversLicenceUY011Field;


            //[System.Xml.Serialization.XmlElementAttribute("DriversLicenceUY01")]
            public DriversLicenceUY01[] DriversLicenceUY011
            {
                get
                {
                    return this.driversLicenceUY011Field;
                }
                set
                {
                    this.driversLicenceUY011Field = value;
                }
            }
        }








        public class EmpiricaEX
        {

            private EmpiricaEX[] empiricaEX1Field;


            //[System.Xml.Serialization.XmlElementAttribute("EmpiricaEX")]
            public EmpiricaEX[] EmpiricaEX1
            {
                get
                {
                    return this.empiricaEX1Field;
                }
                set
                {
                    this.empiricaEX1Field = value;
                }
            }
        }








        public class EmploymentNM04
        {
            private string informationDateField = string.Empty;
            private string occupationField = string.Empty;
            private string employerNameField = string.Empty;
            private string employmentPeriodField = string.Empty;


            public string InformationDate
            {
                get
                {
                    return this.informationDateField;
                }
                set
                {
                    this.informationDateField = value;
                }
            }

            public string Occupation
            {
                get
                {
                    return this.occupationField;
                }
                set
                {
                    this.occupationField = value;
                }
            }

            public string EmployerName
            {
                get
                {
                    return this.employerNameField;
                }
                set
                {
                    this.employerNameField = value;
                }
            }

            public string EmploymentPeriod
            {
                get
                {
                    return this.employmentPeriodField;
                }
                set
                {
                    this.employmentPeriodField = value;
                }
            }


            private EmploymentNM04[] employmentNM041Field;


            //[System.Xml.Serialization.XmlElementAttribute("EmploymentNM04")]
            public EmploymentNM04[] EmploymentNM041
            {
                get
                {
                    return this.employmentNM041Field;
                }
                set
                {
                    this.employmentNM041Field = value;
                }
            }
        }








        public class EnquiriesNE09
        {

            private EnquiriesNE09[] enquiriesNE091Field;


            //[System.Xml.Serialization.XmlElementAttribute("EnquiriesNE09")]
            public EnquiriesNE09[] EnquiriesNE091
            {
                get
                {
                    return this.enquiriesNE091Field;
                }
                set
                {
                    this.enquiriesNE091Field = value;
                }
            }
        }








        public class HawkHA01
        {

            private HawkHA01[] hawkHA011Field;


            //[System.Xml.Serialization.XmlElementAttribute("HawkHA01")]
            public HawkHA01[] HawkHA011
            {
                get
                {
                    return this.hawkHA011Field;
                }
                set
                {
                    this.hawkHA011Field = value;
                }
            }
        }








        public class HawkHI01
        {

            private HawkHI01[] hawkHI011Field;


            //[System.Xml.Serialization.XmlElementAttribute("HawkHI01")]
            public HawkHI01[] HawkHI011
            {
                get
                {
                    return this.hawkHI011Field;
                }
                set
                {
                    this.hawkHI011Field = value;
                }
            }
        }








        public class InsuranceClaimsUA01
        {

            private InsuranceClaimsUA01[] insuranceClaimsUA011Field;


            //[System.Xml.Serialization.XmlElementAttribute("InsuranceClaimsUA01")]
            public InsuranceClaimsUA01[] InsuranceClaimsUA011
            {
                get
                {
                    return this.insuranceClaimsUA011Field;
                }
                set
                {
                    this.insuranceClaimsUA011Field = value;
                }
            }
        }








        public class InsuranceClaimsUB01
        {

            private InsuranceClaimsUB01[] insuranceClaimsUB011Field;


            //[System.Xml.Serialization.XmlElementAttribute("InsuranceClaimsUB01")]
            public InsuranceClaimsUB01[] InsuranceClaimsUB011
            {
                get
                {
                    return this.insuranceClaimsUB011Field;
                }
                set
                {
                    this.insuranceClaimsUB011Field = value;
                }
            }
        }








        public class InsuranceClaimsUC01
        {

            private InsuranceClaimsUC01[] insuranceClaimsUC011Field;


            //[System.Xml.Serialization.XmlElementAttribute("InsuranceClaimsUC01")]
            public InsuranceClaimsUC01[] InsuranceClaimsUC011
            {
                get
                {
                    return this.insuranceClaimsUC011Field;
                }
                set
                {
                    this.insuranceClaimsUC011Field = value;
                }
            }
        }








        public class InsuranceClaimsUI01
        {

            private InsuranceClaimsUI01[] insuranceClaimsUI011Field;


            //[System.Xml.Serialization.XmlElementAttribute("InsuranceClaimsUI01")]
            public InsuranceClaimsUI01[] InsuranceClaimsUI011
            {
                get
                {
                    return this.insuranceClaimsUI011Field;
                }
                set
                {
                    this.insuranceClaimsUI011Field = value;
                }
            }
        }








        public class IndividualTrace04
        {

            private IndividualTrace04[] individualTrace041Field;


            //[System.Xml.Serialization.XmlElementAttribute("IndividualTrace04")]
            public IndividualTrace04[] IndividualTrace041
            {
                get
                {
                    return this.individualTrace041Field;
                }
                set
                {
                    this.individualTrace041Field = value;
                }
            }
        }








        public class IndividualTrace05
        {

            private IndividualTrace05[] individualTrace051Field;


            //[System.Xml.Serialization.XmlElementAttribute("IndividualTrace05")]
            public IndividualTrace05[] IndividualTrace051
            {
                get
                {
                    return this.individualTrace051Field;
                }
                set
                {
                    this.individualTrace051Field = value;
                }
            }
        }








        public class JudgementsNJ06
        {

            private JudgementsNJ06[] judgementsNJ061Field;


            //[System.Xml.Serialization.XmlElementAttribute("JudgementsNJ06")]
            public JudgementsNJ06[] JudgementsNJ061
            {
                get
                {
                    return this.judgementsNJ061Field;
                }
                set
                {
                    this.judgementsNJ061Field = value;
                }
            }
        }








        public class JudgementsNJ07
        {
            private string judgmentDateField = string.Empty;
            private string judgmentTypeDescField = string.Empty;
            private string judgmentTypeCodeField = string.Empty;
            private string plaintiffField = string.Empty;
            private string natureOfDebtDescField = string.Empty;
            private string amountField = string.Empty;
            private string caseNoField = string.Empty;
            private string courtNameDescField = string.Empty;
            private string courtTypeDescField = string.Empty;


            public string JudgmentDate
            {
                get
                {
                    return this.judgmentDateField;
                }
                set
                {
                    this.judgmentDateField = value;
                }
            }

            public string JudgmentTypeDesc
            {
                get
                {
                    return this.judgmentTypeDescField;
                }
                set
                {
                    this.judgmentTypeDescField = value;
                }
            }

            public string JudgmentTypeCode
            {
                get
                {
                    return this.judgmentTypeCodeField;
                }
                set
                {
                    this.judgmentTypeCodeField = value;
                }
            }

            public string Plaintiff
            {
                get
                {
                    return this.plaintiffField;
                }
                set
                {
                    this.plaintiffField = value;
                }
            }

            public string NatureOfDebtDesc
            {
                get
                {
                    return this.natureOfDebtDescField;
                }
                set
                {
                    this.natureOfDebtDescField = value;
                }
            }

            public string Amount
            {
                get
                {
                    return this.amountField;
                }
                set
                {
                    this.amountField = value;
                }
            }

            public string CaseNo
            {
                get
                {
                    return this.caseNoField;
                }
                set
                {
                    this.caseNoField = value;
                }
            }

            public string CourtNameDesc
            {
                get
                {
                    return this.courtNameDescField;
                }
                set
                {
                    this.courtNameDescField = value;
                }
            }

            public string CourtTypeDesc
            {
                get
                {
                    return this.courtTypeDescField;
                }
                set
                {
                    this.courtTypeDescField = value;
                }
            }



            private JudgementsNJ07[] judgementsNJ071Field;


            //[System.Xml.Serialization.XmlElementAttribute("JudgementsNJ07")]
            public JudgementsNJ07[] JudgementsNJ071
            {
                get
                {
                    return this.judgementsNJ071Field;
                }
                set
                {
                    this.judgementsNJ071Field = value;
                }
            }
        }








        public class NLRAccountInformationMP01
        {

            private NLRAccountInformationMP01[] nLRAccountInformationMP011Field;


            //[System.Xml.Serialization.XmlElementAttribute("NLRAccountInformationMP01")]
            public NLRAccountInformationMP01[] NLRAccountInformationMP011
            {
                get
                {
                    return this.nLRAccountInformationMP011Field;
                }
                set
                {
                    this.nLRAccountInformationMP011Field = value;
                }
            }
        }








        public class NLRAccountInformationMP03
        {

            private NLRAccountInformationMP03[] nLRAccountInformationMP031Field;


            //[System.Xml.Serialization.XmlElementAttribute("NLRAccountInformationMP03")]
            public NLRAccountInformationMP03[] NLRAccountInformationMP031
            {
                get
                {
                    return this.nLRAccountInformationMP031Field;
                }
                set
                {
                    this.nLRAccountInformationMP031Field = value;
                }
            }
        }








        public class NLRAccountInformationExtendedM8
        {

            private NLRAccountInformationExtendedM8[] nLRAccountInformationExtendedM81Field;


            //[System.Xml.Serialization.XmlElementAttribute("NLRAccountInformationExtendedM8")]
            public NLRAccountInformationExtendedM8[] NLRAccountInformationExtendedM81
            {
                get
                {
                    return this.nLRAccountInformationExtendedM81Field;
                }
                set
                {
                    this.nLRAccountInformationExtendedM81Field = value;
                }
            }
        }








        public class NLRCounterSeqmentMC01
        {

            //private NLRCounterSeqmentMC01[] nLRCounterSeqmentMC011Field;


            ////[System.Xml.Serialization.XmlElementAttribute("NLRCounterSeqmentMC01")]
            //public NLRCounterSeqmentMC01[] NLRCounterSeqmentMC011
            //{
            //    get
            //    {
            //        return this.nLRCounterSeqmentMC011Field;
            //    }
            //    set
            //    {
            //        this.nLRCounterSeqmentMC011Field = value;
            //    }
            //}

            private string segmentCode = string.Empty;


            public string SegmentCode
            {
                get
                {
                    return this.segmentCode;
                }
                set
                {
                    this.segmentCode = value;
                }
            }

            private string consumerNumber = string.Empty;


            public string ConsumerNumber
            {
                get
                {
                    return this.consumerNumber;
                }
                set
                {
                    this.consumerNumber = value;
                }
            }

            private string currentYearEnquiriesClient = string.Empty;


            public string CurrentYearEnquiriesClient
            {
                get
                {
                    return this.currentYearEnquiriesClient;
                }
                set
                {
                    this.currentYearEnquiriesClient = value;
                }
            }

            private string currentYearEnquiriesOtherSubscribers = string.Empty;


            public string CurrentYearEnquiriesOtherSubscribers
            {
                get
                {
                    return this.currentYearEnquiriesOtherSubscribers;
                }
                set
                {
                    this.currentYearEnquiriesOtherSubscribers = value;
                }
            }

            private string currentYearPositiveNLRLoans = string.Empty;


            public string CurrentYearPositiveNLRLoans
            {
                get
                {
                    return this.currentYearPositiveNLRLoans;
                }
                set
                {
                    this.currentYearPositiveNLRLoans = value;
                }
            }

            private string currentYearHighestMonthsInArrears = string.Empty;


            public string CurrentYearHighestMonthsInArrears
            {
                get
                {
                    return this.currentYearHighestMonthsInArrears;
                }
                set
                {
                    this.currentYearHighestMonthsInArrears = value;
                }
            }

            private string previousYearEnquiriesClient = string.Empty;


            public string PreviousYearEnquiriesClient
            {
                get
                {
                    return this.previousYearEnquiriesClient;
                }
                set
                {
                    this.previousYearEnquiriesClient = value;
                }
            }

            private string previousYearEnquiriesOtherSubscribers = string.Empty;


            public string PreviousYearEnquiriesOtherSubscribers
            {
                get
                {
                    return this.previousYearEnquiriesOtherSubscribers;
                }
                set
                {
                    this.previousYearEnquiriesOtherSubscribers = value;
                }
            }

            private string previousYearPositiveNLRLoans = string.Empty;


            public string PreviousYearPositiveNLRLoans
            {
                get
                {
                    return this.previousYearPositiveNLRLoans;
                }
                set
                {
                    this.previousYearPositiveNLRLoans = value;
                }
            }

            private string previousYearHighestMonthsInArrears = string.Empty;

            public string PreviousYearHighestMonthsInArrears
            {
                get
                {
                    return this.previousYearHighestMonthsInArrears;
                }
                set
                {
                    this.previousYearHighestMonthsInArrears = value;
                }
            }

            private string allOtherYearEnquiriesClient = string.Empty;

            public string AllOtherYearEnquiriesClient
            {
                get
                {
                    return this.allOtherYearEnquiriesClient;
                }
                set
                {
                    this.allOtherYearEnquiriesClient = value;
                }
            }

            private string allOtherYearEnquiriesOtherSubscribers = string.Empty;

            public string AllOtherYearEnquiriesOtherSubscribers
            {
                get
                {
                    return this.allOtherYearEnquiriesOtherSubscribers;
                }
                set
                {
                    this.allOtherYearEnquiriesOtherSubscribers = value;
                }
            }

            private string allOtherYearPositiveNLRLoans = string.Empty;

            public string AllOtherYearPositiveNLRLoans
            {
                get
                {
                    return this.allOtherYearPositiveNLRLoans;
                }
                set
                {
                    this.allOtherYearPositiveNLRLoans = value;
                }
            }

            private string allOtherYearsHighestMonthsInArrears = string.Empty;

            public string AllOtherYearsHighestMonthsInArrears
            {
                get
                {
                    return this.allOtherYearsHighestMonthsInArrears;
                }
                set
                {
                    this.allOtherYearsHighestMonthsInArrears = value;
                }
            }

        }








        public class NLREnquiriesME01
        {

            private NLREnquiriesME01[] nLREnquiriesME011Field;


            //[System.Xml.Serialization.XmlElementAttribute("NLREnquiriesME01")]
            public NLREnquiriesME01[] NLREnquiriesME011
            {
                get
                {
                    return this.nLREnquiriesME011Field;
                }
                set
                {
                    this.nLREnquiriesME011Field = value;
                }
            }
        }








        public class NoticesNN08
        {
            private string noticeTypeCodefield;
            private string noticeTypeCodevaluefield;

            private NoticesNN08[] noticesNN081Field;


            //[System.Xml.Serialization.XmlElementAttribute("NoticesNN08")]
            public NoticesNN08[] NoticesNN081
            {
                get
                {
                    return this.noticesNN081Field;
                }
                set
                {
                    this.noticesNN081Field = value;
                }
            }

            public string NoticeTypeCode
            {
                get { return this.noticeTypeCodefield; }
                set { this.noticeTypeCodefield = value; }
            }

            public string NoticeTypeCodevalue
            {
                get { return this.noticeTypeCodevaluefield; }
                set { this.noticeTypeCodevaluefield = value; }
            }
        }








        public class NoticesNN10
        {

            private NoticesNN10[] noticesNN101Field;


            //[System.Xml.Serialization.XmlElementAttribute("NoticesNN10")]
            public NoticesNN10[] NoticesNN101
            {
                get
                {
                    return this.noticesNN101Field;
                }
                set
                {
                    this.noticesNN101Field = value;
                }
            }
        }








        public class NoticesNN07
        {

            private NoticesNN07[] noticesNN071Field;


            //[System.Xml.Serialization.XmlElementAttribute("NoticesNN07")]
            public NoticesNN07[] NoticesNN071
            {
                get
                {
                    return this.noticesNN071Field;
                }
                set
                {
                    this.noticesNN071Field = value;
                }
            }
        }








        public class PaymentProfileNP09
        {

            private PaymentProfileNP09[] paymentProfileNP091Field;


            //[System.Xml.Serialization.XmlElementAttribute("PaymentProfileNP09")]
            public PaymentProfileNP09[] PaymentProfileNP091
            {
                get
                {
                    return this.paymentProfileNP091Field;
                }
                set
                {
                    this.paymentProfileNP091Field = value;
                }
            }
        }








        public class PaymentProfileNP11
        {

            private PaymentProfileNP11[] paymentProfileNP111Field;


            //[System.Xml.Serialization.XmlElementAttribute("PaymentProfileNP11")]
            public PaymentProfileNP11[] PaymentProfileNP111
            {
                get
                {
                    return this.paymentProfileNP111Field;
                }
                set
                {
                    this.paymentProfileNP111Field = value;
                }
            }
        }








        public class PaymentProfileNP15
        {

            private PaymentProfileNP15[] paymentProfileNP151Field;


            //[System.Xml.Serialization.XmlElementAttribute("PaymentProfileNP15")]
            public PaymentProfileNP15[] PaymentProfileNP151
            {
                get
                {
                    return this.paymentProfileNP151Field;
                }
                set
                {
                    this.paymentProfileNP151Field = value;
                }
            }
        }








        public class SAFPSNF01
        {
            private string fpsRefNoField = string.Empty;

            private string originatorRefNoField = string.Empty;
            private string fraudCategoryField = string.Empty;
            private string fraudCaseDateField = string.Empty;
            private string memberRegNoField = string.Empty;
            private string organisationNameField = string.Empty;


            public string FPSRefNo
            {
                get
                {
                    return this.fpsRefNoField;
                }
                set
                {
                    this.fpsRefNoField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string OriginatorRefNo
            {
                get
                {
                    return this.originatorRefNoField;
                }
                set
                {
                    this.originatorRefNoField = value;
                }
            }

            public string FraudCategory
            {
                get
                {
                    return this.fraudCategoryField;
                }
                set
                {
                    this.fraudCategoryField = value;
                }
            }

            public string FraudCaseDate
            {
                get
                {
                    return this.fraudCaseDateField;
                }
                set
                {
                    this.fraudCaseDateField = value;
                }
            }
            public string MemberRegNo
            {
                get
                {
                    return this.memberRegNoField;
                }
                set
                {
                    this.memberRegNoField = value;
                }
            }

            public string OrganisationName
            {
                get
                {
                    return this.organisationNameField;
                }
                set
                {
                    this.organisationNameField = value;
                }
            }



        }




        public class ScorecardS101
        {

            private ScorecardS101[] scorecardS1011Field;


            //[System.Xml.Serialization.XmlElementAttribute("ScorecardS101")]
            public ScorecardS101[] ScorecardS1011
            {
                get
                {
                    return this.scorecardS1011Field;
                }
                set
                {
                    this.scorecardS1011Field = value;
                }
            }
        }








        public class TraceAlertsNT04
        {

            private TraceAlertsNT04[] traceAlertsNT041Field;


            //[System.Xml.Serialization.XmlElementAttribute("TraceAlertsNT04")]
            public TraceAlertsNT04[] TraceAlertsNT041
            {
                get
                {
                    return this.traceAlertsNT041Field;
                }
                set
                {
                    this.traceAlertsNT041Field = value;
                }
            }
        }








        public class TraceAlertsNT06
        {

            private TraceAlertsNT06[] traceAlertsNT061Field;


            //[System.Xml.Serialization.XmlElementAttribute("TraceAlertsNT06")]
            public TraceAlertsNT06[] TraceAlertsNT061
            {
                get
                {
                    return this.traceAlertsNT061Field;
                }
                set
                {
                    this.traceAlertsNT061Field = value;
                }
            }
        }








        public class TraceAlertsNT07
        {

            private TraceAlertsNT07[] traceAlertsNT071Field;


            //[System.Xml.Serialization.XmlElementAttribute("TraceAlertsNT07")]
            public TraceAlertsNT07[] TraceAlertsNT071
            {
                get
                {
                    return this.traceAlertsNT071Field;
                }
                set
                {
                    this.traceAlertsNT071Field = value;
                }
            }
        }














        public class AddressVerificationNR01
        {

            private string last24HoursField;

            private string last48HoursField;

            private string last96HoursField;

            private string last30DaysField;

            private string addressMessageField;



            public string Last24Hours
            {
                get
                {
                    return this.last24HoursField;
                }
                set
                {
                    this.last24HoursField = value;
                }
            }



            public string Last48Hours
            {
                get
                {
                    return this.last48HoursField;
                }
                set
                {
                    this.last48HoursField = value;
                }
            }



            public string Last96Hours
            {
                get
                {
                    return this.last96HoursField;
                }
                set
                {
                    this.last96HoursField = value;
                }
            }



            public string Last30Days
            {
                get
                {
                    return this.last30DaysField;
                }
                set
                {
                    this.last30DaysField = value;
                }
            }



            public string AddressMessage
            {
                get
                {
                    return this.addressMessageField;
                }
                set
                {
                    this.addressMessageField = value;
                }
            }
        }







        public class AffordabilityAF01
        {

            private string consumerNoField;

            private string affordability_InstalField;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string Affordability_Instal
            {
                get
                {
                    return this.affordability_InstalField;
                }
                set
                {
                    this.affordability_InstalField = value;
                }
            }
        }







        public class AffordStandardBatchCharsXB01
        {

            private string consumerNoField;

            private string mL179Field;

            private string mL180Field;

            private string mL181Field;

            private string xC121Field;

            private string xC122Field;

            private string xC179Field;

            private string xC180Field;

            private string xC181Field;

            private string xH179Field;

            private string xH180Field;

            private string xH181Field;

            private string xI179Field;

            private string xI180Field;

            private string xI181Field;

            private string xL179Field;

            private string xL180Field;

            private string xL181Field;

            private string xO179Field;

            private string xO180Field;

            private string xO181Field;

            private string xP179Field;

            private string xP180Field;

            private string xP181Field;

            private string xR121Field;

            private string xR122Field;

            private string xR179Field;

            private string xR180Field;

            private string xR181Field;

            private string xS179Field;

            private string xS180Field;

            private string xS181Field;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string ML179
            {
                get
                {
                    return this.mL179Field;
                }
                set
                {
                    this.mL179Field = value;
                }
            }



            public string ML180
            {
                get
                {
                    return this.mL180Field;
                }
                set
                {
                    this.mL180Field = value;
                }
            }



            public string ML181
            {
                get
                {
                    return this.mL181Field;
                }
                set
                {
                    this.mL181Field = value;
                }
            }



            public string XC121
            {
                get
                {
                    return this.xC121Field;
                }
                set
                {
                    this.xC121Field = value;
                }
            }



            public string XC122
            {
                get
                {
                    return this.xC122Field;
                }
                set
                {
                    this.xC122Field = value;
                }
            }



            public string XC179
            {
                get
                {
                    return this.xC179Field;
                }
                set
                {
                    this.xC179Field = value;
                }
            }



            public string XC180
            {
                get
                {
                    return this.xC180Field;
                }
                set
                {
                    this.xC180Field = value;
                }
            }



            public string XC181
            {
                get
                {
                    return this.xC181Field;
                }
                set
                {
                    this.xC181Field = value;
                }
            }



            public string XH179
            {
                get
                {
                    return this.xH179Field;
                }
                set
                {
                    this.xH179Field = value;
                }
            }



            public string XH180
            {
                get
                {
                    return this.xH180Field;
                }
                set
                {
                    this.xH180Field = value;
                }
            }



            public string XH181
            {
                get
                {
                    return this.xH181Field;
                }
                set
                {
                    this.xH181Field = value;
                }
            }



            public string XI179
            {
                get
                {
                    return this.xI179Field;
                }
                set
                {
                    this.xI179Field = value;
                }
            }



            public string XI180
            {
                get
                {
                    return this.xI180Field;
                }
                set
                {
                    this.xI180Field = value;
                }
            }



            public string XI181
            {
                get
                {
                    return this.xI181Field;
                }
                set
                {
                    this.xI181Field = value;
                }
            }



            public string XL179
            {
                get
                {
                    return this.xL179Field;
                }
                set
                {
                    this.xL179Field = value;
                }
            }



            public string XL180
            {
                get
                {
                    return this.xL180Field;
                }
                set
                {
                    this.xL180Field = value;
                }
            }



            public string XL181
            {
                get
                {
                    return this.xL181Field;
                }
                set
                {
                    this.xL181Field = value;
                }
            }



            public string XO179
            {
                get
                {
                    return this.xO179Field;
                }
                set
                {
                    this.xO179Field = value;
                }
            }



            public string XO180
            {
                get
                {
                    return this.xO180Field;
                }
                set
                {
                    this.xO180Field = value;
                }
            }



            public string XO181
            {
                get
                {
                    return this.xO181Field;
                }
                set
                {
                    this.xO181Field = value;
                }
            }



            public string XP179
            {
                get
                {
                    return this.xP179Field;
                }
                set
                {
                    this.xP179Field = value;
                }
            }



            public string XP180
            {
                get
                {
                    return this.xP180Field;
                }
                set
                {
                    this.xP180Field = value;
                }
            }



            public string XP181
            {
                get
                {
                    return this.xP181Field;
                }
                set
                {
                    this.xP181Field = value;
                }
            }



            public string XR121
            {
                get
                {
                    return this.xR121Field;
                }
                set
                {
                    this.xR121Field = value;
                }
            }



            public string XR122
            {
                get
                {
                    return this.xR122Field;
                }
                set
                {
                    this.xR122Field = value;
                }
            }



            public string XR179
            {
                get
                {
                    return this.xR179Field;
                }
                set
                {
                    this.xR179Field = value;
                }
            }



            public string XR180
            {
                get
                {
                    return this.xR180Field;
                }
                set
                {
                    this.xR180Field = value;
                }
            }



            public string XR181
            {
                get
                {
                    return this.xR181Field;
                }
                set
                {
                    this.xR181Field = value;
                }
            }



            public string XS179
            {
                get
                {
                    return this.xS179Field;
                }
                set
                {
                    this.xS179Field = value;
                }
            }



            public string XS180
            {
                get
                {
                    return this.xS180Field;
                }
                set
                {
                    this.xS180Field = value;
                }
            }



            public string XS181
            {
                get
                {
                    return this.xS181Field;
                }
                set
                {
                    this.xS181Field = value;
                }
            }
        }







        public class AggregateAG01
        {

            private string consumerNoField;

            private string aggregatesField;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string Aggregates
            {
                get
                {
                    return this.aggregatesField;
                }
                set
                {
                    this.aggregatesField = value;
                }
            }
        }







        public class AggregateAG02
        {

            private string consumerNoField;

            private string aggregatesField;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string Aggregates
            {
                get
                {
                    return this.aggregatesField;
                }
                set
                {
                    this.aggregatesField = value;
                }
            }
        }







        public class AggregateNX05
        {

            private string consumerNoField;

            private string noJudgementsField;

            private string noWriteOffField;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string NoJudgements
            {
                get
                {
                    return this.noJudgementsField;
                }
                set
                {
                    this.noJudgementsField = value;
                }
            }



            public string NoWriteOff
            {
                get
                {
                    return this.noWriteOffField;
                }
                set
                {
                    this.noWriteOffField = value;
                }
            }
        }







        public class AggregateNX09
        {

            private string consumerNoField;

            private string pPWorstArrears6MonthsField;

            private string pPWorstArrears24MonthsField;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string PPWorstArrears6Months
            {
                get
                {
                    return this.pPWorstArrears6MonthsField;
                }
                set
                {
                    this.pPWorstArrears6MonthsField = value;
                }
            }



            public string PPWorstArrears24Months
            {
                get
                {
                    return this.pPWorstArrears24MonthsField;
                }
                set
                {
                    this.pPWorstArrears24MonthsField = value;
                }
            }
        }







        public class AggregateNX33
        {

            private string totalJudgmentsField;

            private string monthsSinceOldestCPAProfileField;

            private string percentage0ArrearsLast24MonthsField;

            private string worstNLRArrearsField;

            private string worstCPAArrearsLast90DaysField;

            private string percentageZeroArrearsLast3MonthsField;

            private string numberOfNLRPaymentProfilesOpenedLastYearField;

            private string totalJudgementsLastYearField;

            private string monthsSinceHighestNegativeField;

            private string percentage3PlusArrearsLast3MonthsField;

            private string percentage3PlusArrearsLast24MonthsField;

            private string worstArrearsLastYearField;

            private string totalAdverseEverField;

            private string totalNoticesEverField;

            private string totalAccountsCPAField;

            private string totalAccountsNLRField;

            private string iDVerificationField;

            private string enquiriesEverField;

            private string negativeEverField;

            private string totalInstallmentNLROpenAccountsField;

            private string totalInstallmentCPAOpenAccountsField;

            private string recentNegativeMonthsField;

            private string recentNegativeValueField;

            private string worstMPSAArrearsLast90DaysField;

            private string highestJudgementValueField;

            private string highestAdverseValueField;

            private string worstArrearsCPAProfileField;

            private string arrears0Last6MonthsField;

            private string percentage0Last12MonthsField;



            public string TotalJudgments
            {
                get
                {
                    return this.totalJudgmentsField;
                }
                set
                {
                    this.totalJudgmentsField = value;
                }
            }



            public string MonthsSinceOldestCPAProfile
            {
                get
                {
                    return this.monthsSinceOldestCPAProfileField;
                }
                set
                {
                    this.monthsSinceOldestCPAProfileField = value;
                }
            }



            public string Percentage0ArrearsLast24Months
            {
                get
                {
                    return this.percentage0ArrearsLast24MonthsField;
                }
                set
                {
                    this.percentage0ArrearsLast24MonthsField = value;
                }
            }



            public string WorstNLRArrears
            {
                get
                {
                    return this.worstNLRArrearsField;
                }
                set
                {
                    this.worstNLRArrearsField = value;
                }
            }



            public string WorstCPAArrearsLast90Days
            {
                get
                {
                    return this.worstCPAArrearsLast90DaysField;
                }
                set
                {
                    this.worstCPAArrearsLast90DaysField = value;
                }
            }



            public string PercentageZeroArrearsLast3Months
            {
                get
                {
                    return this.percentageZeroArrearsLast3MonthsField;
                }
                set
                {
                    this.percentageZeroArrearsLast3MonthsField = value;
                }
            }



            public string NumberOfNLRPaymentProfilesOpenedLastYear
            {
                get
                {
                    return this.numberOfNLRPaymentProfilesOpenedLastYearField;
                }
                set
                {
                    this.numberOfNLRPaymentProfilesOpenedLastYearField = value;
                }
            }



            public string TotalJudgementsLastYear
            {
                get
                {
                    return this.totalJudgementsLastYearField;
                }
                set
                {
                    this.totalJudgementsLastYearField = value;
                }
            }



            public string MonthsSinceHighestNegative
            {
                get
                {
                    return this.monthsSinceHighestNegativeField;
                }
                set
                {
                    this.monthsSinceHighestNegativeField = value;
                }
            }



            public string Percentage3PlusArrearsLast3Months
            {
                get
                {
                    return this.percentage3PlusArrearsLast3MonthsField;
                }
                set
                {
                    this.percentage3PlusArrearsLast3MonthsField = value;
                }
            }



            public string Percentage3PlusArrearsLast24Months
            {
                get
                {
                    return this.percentage3PlusArrearsLast24MonthsField;
                }
                set
                {
                    this.percentage3PlusArrearsLast24MonthsField = value;
                }
            }



            public string WorstArrearsLastYear
            {
                get
                {
                    return this.worstArrearsLastYearField;
                }
                set
                {
                    this.worstArrearsLastYearField = value;
                }
            }



            public string TotalAdverseEver
            {
                get
                {
                    return this.totalAdverseEverField;
                }
                set
                {
                    this.totalAdverseEverField = value;
                }
            }



            public string TotalNoticesEver
            {
                get
                {
                    return this.totalNoticesEverField;
                }
                set
                {
                    this.totalNoticesEverField = value;
                }
            }



            public string TotalAccountsCPA
            {
                get
                {
                    return this.totalAccountsCPAField;
                }
                set
                {
                    this.totalAccountsCPAField = value;
                }
            }



            public string TotalAccountsNLR
            {
                get
                {
                    return this.totalAccountsNLRField;
                }
                set
                {
                    this.totalAccountsNLRField = value;
                }
            }



            public string IDVerification
            {
                get
                {
                    return this.iDVerificationField;
                }
                set
                {
                    this.iDVerificationField = value;
                }
            }



            public string EnquiriesEver
            {
                get
                {
                    return this.enquiriesEverField;
                }
                set
                {
                    this.enquiriesEverField = value;
                }
            }



            public string NegativeEver
            {
                get
                {
                    return this.negativeEverField;
                }
                set
                {
                    this.negativeEverField = value;
                }
            }



            public string TotalInstallmentNLROpenAccounts
            {
                get
                {
                    return this.totalInstallmentNLROpenAccountsField;
                }
                set
                {
                    this.totalInstallmentNLROpenAccountsField = value;
                }
            }



            public string TotalInstallmentCPAOpenAccounts
            {
                get
                {
                    return this.totalInstallmentCPAOpenAccountsField;
                }
                set
                {
                    this.totalInstallmentCPAOpenAccountsField = value;
                }
            }



            public string RecentNegativeMonths
            {
                get
                {
                    return this.recentNegativeMonthsField;
                }
                set
                {
                    this.recentNegativeMonthsField = value;
                }
            }



            public string RecentNegativeValue
            {
                get
                {
                    return this.recentNegativeValueField;
                }
                set
                {
                    this.recentNegativeValueField = value;
                }
            }



            public string WorstMPSAArrearsLast90Days
            {
                get
                {
                    return this.worstMPSAArrearsLast90DaysField;
                }
                set
                {
                    this.worstMPSAArrearsLast90DaysField = value;
                }
            }



            public string HighestJudgementValue
            {
                get
                {
                    return this.highestJudgementValueField;
                }
                set
                {
                    this.highestJudgementValueField = value;
                }
            }



            public string HighestAdverseValue
            {
                get
                {
                    return this.highestAdverseValueField;
                }
                set
                {
                    this.highestAdverseValueField = value;
                }
            }



            public string WorstArrearsCPAProfile
            {
                get
                {
                    return this.worstArrearsCPAProfileField;
                }
                set
                {
                    this.worstArrearsCPAProfileField = value;
                }
            }



            public string Arrears0Last6Months
            {
                get
                {
                    return this.arrears0Last6MonthsField;
                }
                set
                {
                    this.arrears0Last6MonthsField = value;
                }
            }



            public string Percentage0Last12Months
            {
                get
                {
                    return this.percentage0Last12MonthsField;
                }
                set
                {
                    this.percentage0Last12MonthsField = value;
                }
            }
        }







        public class AlertDetailC4
        {

            private string resultField;



            public string Result
            {
                get
                {
                    return this.resultField;
                }
                set
                {
                    this.resultField = value;
                }
            }
        }







        public class BCCBC01
        {

            private string consumerNoField;

            private string dM003ALField;

            private string eQ001CLField;

            private string eQ002CLField;

            private string eQ003ALField;

            private string eQ003CLField;

            private string eQ003NLField;

            private string eQ005NLField;

            private string eQ007GRField;

            private string eQ007HWField;

            private string eQ007JWField;

            private string eQ008MBField;

            private string eQ008NLField;

            private string eQ008VFField;

            private string nG001ALField;

            private string nG002ALField;

            private string nG004ALField;

            private string nG006ALField;

            private string nG008ALField;

            private string nG022ALField;

            private string nG034ALField;

            private string pP001ALField;

            private string pP001CAField;

            private string pP001CLField;

            private string pP001NLField;

            private string pP002CCField;

            private string pP002CEField;

            private string pP002FCField;

            private string pP002FLField;

            private string pP002HWField;

            private string pP002INField;

            private string pP002JWField;

            private string pP002MBField;

            private string pP002TEField;

            private string pP002VFField;

            private string pP003ALField;

            private string pP004ALField;

            private string pP011ALField;

            private string pP012CLField;

            private string pP012HWField;

            private string pP012JWField;

            private string pP013CCField;

            private string pP013CLField;

            private string pP013FNField;

            private string pP013TEField;

            private string pP017ALField;

            private string pP018ALField;

            private string pP019ALField;

            private string pP020ALField;

            private string pP044CEField;

            private string pP044FNField;

            private string pP045ALField;

            private string pP046ALField;

            private string pP049CLField;

            private string pP054ALField;

            private string pP056ALField;

            private string pP057ALField;

            private string pP058ALField;

            private string pP060ALField;

            private string pP062ALField;

            private string pP064ALField;

            private string pP065ALField;

            private string pP066ALField;

            private string pP066CLField;

            private string pP067CEField;

            private string pP067FNField;

            private string pP068CCField;

            private string pP069ALField;

            private string pP070CCField;

            private string pP070CLField;

            private string pP070FNField;

            private string pP071ALField;

            private string pP076CLField;

            private string pP078CCField;

            private string pP082CLField;

            private string pP082NLField;

            private string pP099ALField;

            private string pP100NLField;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string DM003AL
            {
                get
                {
                    return this.dM003ALField;
                }
                set
                {
                    this.dM003ALField = value;
                }
            }



            public string EQ001CL
            {
                get
                {
                    return this.eQ001CLField;
                }
                set
                {
                    this.eQ001CLField = value;
                }
            }



            public string EQ002CL
            {
                get
                {
                    return this.eQ002CLField;
                }
                set
                {
                    this.eQ002CLField = value;
                }
            }



            public string EQ003AL
            {
                get
                {
                    return this.eQ003ALField;
                }
                set
                {
                    this.eQ003ALField = value;
                }
            }



            public string EQ003CL
            {
                get
                {
                    return this.eQ003CLField;
                }
                set
                {
                    this.eQ003CLField = value;
                }
            }



            public string EQ003NL
            {
                get
                {
                    return this.eQ003NLField;
                }
                set
                {
                    this.eQ003NLField = value;
                }
            }



            public string EQ005NL
            {
                get
                {
                    return this.eQ005NLField;
                }
                set
                {
                    this.eQ005NLField = value;
                }
            }



            public string EQ007GR
            {
                get
                {
                    return this.eQ007GRField;
                }
                set
                {
                    this.eQ007GRField = value;
                }
            }



            public string EQ007HW
            {
                get
                {
                    return this.eQ007HWField;
                }
                set
                {
                    this.eQ007HWField = value;
                }
            }



            public string EQ007JW
            {
                get
                {
                    return this.eQ007JWField;
                }
                set
                {
                    this.eQ007JWField = value;
                }
            }



            public string EQ008MB
            {
                get
                {
                    return this.eQ008MBField;
                }
                set
                {
                    this.eQ008MBField = value;
                }
            }



            public string EQ008NL
            {
                get
                {
                    return this.eQ008NLField;
                }
                set
                {
                    this.eQ008NLField = value;
                }
            }



            public string EQ008VF
            {
                get
                {
                    return this.eQ008VFField;
                }
                set
                {
                    this.eQ008VFField = value;
                }
            }



            public string NG001AL
            {
                get
                {
                    return this.nG001ALField;
                }
                set
                {
                    this.nG001ALField = value;
                }
            }



            public string NG002AL
            {
                get
                {
                    return this.nG002ALField;
                }
                set
                {
                    this.nG002ALField = value;
                }
            }



            public string NG004AL
            {
                get
                {
                    return this.nG004ALField;
                }
                set
                {
                    this.nG004ALField = value;
                }
            }



            public string NG006AL
            {
                get
                {
                    return this.nG006ALField;
                }
                set
                {
                    this.nG006ALField = value;
                }
            }



            public string NG008AL
            {
                get
                {
                    return this.nG008ALField;
                }
                set
                {
                    this.nG008ALField = value;
                }
            }



            public string NG022AL
            {
                get
                {
                    return this.nG022ALField;
                }
                set
                {
                    this.nG022ALField = value;
                }
            }



            public string NG034AL
            {
                get
                {
                    return this.nG034ALField;
                }
                set
                {
                    this.nG034ALField = value;
                }
            }



            public string PP001AL
            {
                get
                {
                    return this.pP001ALField;
                }
                set
                {
                    this.pP001ALField = value;
                }
            }



            public string PP001CA
            {
                get
                {
                    return this.pP001CAField;
                }
                set
                {
                    this.pP001CAField = value;
                }
            }



            public string PP001CL
            {
                get
                {
                    return this.pP001CLField;
                }
                set
                {
                    this.pP001CLField = value;
                }
            }



            public string PP001NL
            {
                get
                {
                    return this.pP001NLField;
                }
                set
                {
                    this.pP001NLField = value;
                }
            }



            public string PP002CC
            {
                get
                {
                    return this.pP002CCField;
                }
                set
                {
                    this.pP002CCField = value;
                }
            }



            public string PP002CE
            {
                get
                {
                    return this.pP002CEField;
                }
                set
                {
                    this.pP002CEField = value;
                }
            }



            public string PP002FC
            {
                get
                {
                    return this.pP002FCField;
                }
                set
                {
                    this.pP002FCField = value;
                }
            }



            public string PP002FL
            {
                get
                {
                    return this.pP002FLField;
                }
                set
                {
                    this.pP002FLField = value;
                }
            }



            public string PP002HW
            {
                get
                {
                    return this.pP002HWField;
                }
                set
                {
                    this.pP002HWField = value;
                }
            }



            public string PP002IN
            {
                get
                {
                    return this.pP002INField;
                }
                set
                {
                    this.pP002INField = value;
                }
            }



            public string PP002JW
            {
                get
                {
                    return this.pP002JWField;
                }
                set
                {
                    this.pP002JWField = value;
                }
            }



            public string PP002MB
            {
                get
                {
                    return this.pP002MBField;
                }
                set
                {
                    this.pP002MBField = value;
                }
            }



            public string PP002TE
            {
                get
                {
                    return this.pP002TEField;
                }
                set
                {
                    this.pP002TEField = value;
                }
            }



            public string PP002VF
            {
                get
                {
                    return this.pP002VFField;
                }
                set
                {
                    this.pP002VFField = value;
                }
            }



            public string PP003AL
            {
                get
                {
                    return this.pP003ALField;
                }
                set
                {
                    this.pP003ALField = value;
                }
            }



            public string PP004AL
            {
                get
                {
                    return this.pP004ALField;
                }
                set
                {
                    this.pP004ALField = value;
                }
            }



            public string PP011AL
            {
                get
                {
                    return this.pP011ALField;
                }
                set
                {
                    this.pP011ALField = value;
                }
            }



            public string PP012CL
            {
                get
                {
                    return this.pP012CLField;
                }
                set
                {
                    this.pP012CLField = value;
                }
            }



            public string PP012HW
            {
                get
                {
                    return this.pP012HWField;
                }
                set
                {
                    this.pP012HWField = value;
                }
            }



            public string PP012JW
            {
                get
                {
                    return this.pP012JWField;
                }
                set
                {
                    this.pP012JWField = value;
                }
            }



            public string PP013CC
            {
                get
                {
                    return this.pP013CCField;
                }
                set
                {
                    this.pP013CCField = value;
                }
            }



            public string PP013CL
            {
                get
                {
                    return this.pP013CLField;
                }
                set
                {
                    this.pP013CLField = value;
                }
            }



            public string PP013FN
            {
                get
                {
                    return this.pP013FNField;
                }
                set
                {
                    this.pP013FNField = value;
                }
            }



            public string PP013TE
            {
                get
                {
                    return this.pP013TEField;
                }
                set
                {
                    this.pP013TEField = value;
                }
            }



            public string PP017AL
            {
                get
                {
                    return this.pP017ALField;
                }
                set
                {
                    this.pP017ALField = value;
                }
            }



            public string PP018AL
            {
                get
                {
                    return this.pP018ALField;
                }
                set
                {
                    this.pP018ALField = value;
                }
            }



            public string PP019AL
            {
                get
                {
                    return this.pP019ALField;
                }
                set
                {
                    this.pP019ALField = value;
                }
            }



            public string PP020AL
            {
                get
                {
                    return this.pP020ALField;
                }
                set
                {
                    this.pP020ALField = value;
                }
            }



            public string PP044CE
            {
                get
                {
                    return this.pP044CEField;
                }
                set
                {
                    this.pP044CEField = value;
                }
            }



            public string PP044FN
            {
                get
                {
                    return this.pP044FNField;
                }
                set
                {
                    this.pP044FNField = value;
                }
            }



            public string PP045AL
            {
                get
                {
                    return this.pP045ALField;
                }
                set
                {
                    this.pP045ALField = value;
                }
            }



            public string PP046AL
            {
                get
                {
                    return this.pP046ALField;
                }
                set
                {
                    this.pP046ALField = value;
                }
            }



            public string PP049CL
            {
                get
                {
                    return this.pP049CLField;
                }
                set
                {
                    this.pP049CLField = value;
                }
            }



            public string PP054AL
            {
                get
                {
                    return this.pP054ALField;
                }
                set
                {
                    this.pP054ALField = value;
                }
            }



            public string PP056AL
            {
                get
                {
                    return this.pP056ALField;
                }
                set
                {
                    this.pP056ALField = value;
                }
            }



            public string PP057AL
            {
                get
                {
                    return this.pP057ALField;
                }
                set
                {
                    this.pP057ALField = value;
                }
            }



            public string PP058AL
            {
                get
                {
                    return this.pP058ALField;
                }
                set
                {
                    this.pP058ALField = value;
                }
            }



            public string PP060AL
            {
                get
                {
                    return this.pP060ALField;
                }
                set
                {
                    this.pP060ALField = value;
                }
            }



            public string PP062AL
            {
                get
                {
                    return this.pP062ALField;
                }
                set
                {
                    this.pP062ALField = value;
                }
            }



            public string PP064AL
            {
                get
                {
                    return this.pP064ALField;
                }
                set
                {
                    this.pP064ALField = value;
                }
            }



            public string PP065AL
            {
                get
                {
                    return this.pP065ALField;
                }
                set
                {
                    this.pP065ALField = value;
                }
            }



            public string PP066AL
            {
                get
                {
                    return this.pP066ALField;
                }
                set
                {
                    this.pP066ALField = value;
                }
            }



            public string PP066CL
            {
                get
                {
                    return this.pP066CLField;
                }
                set
                {
                    this.pP066CLField = value;
                }
            }



            public string PP067CE
            {
                get
                {
                    return this.pP067CEField;
                }
                set
                {
                    this.pP067CEField = value;
                }
            }



            public string PP067FN
            {
                get
                {
                    return this.pP067FNField;
                }
                set
                {
                    this.pP067FNField = value;
                }
            }



            public string PP068CC
            {
                get
                {
                    return this.pP068CCField;
                }
                set
                {
                    this.pP068CCField = value;
                }
            }



            public string PP069AL
            {
                get
                {
                    return this.pP069ALField;
                }
                set
                {
                    this.pP069ALField = value;
                }
            }



            public string PP070CC
            {
                get
                {
                    return this.pP070CCField;
                }
                set
                {
                    this.pP070CCField = value;
                }
            }



            public string PP070CL
            {
                get
                {
                    return this.pP070CLField;
                }
                set
                {
                    this.pP070CLField = value;
                }
            }



            public string PP070FN
            {
                get
                {
                    return this.pP070FNField;
                }
                set
                {
                    this.pP070FNField = value;
                }
            }



            public string PP071AL
            {
                get
                {
                    return this.pP071ALField;
                }
                set
                {
                    this.pP071ALField = value;
                }
            }



            public string PP076CL
            {
                get
                {
                    return this.pP076CLField;
                }
                set
                {
                    this.pP076CLField = value;
                }
            }



            public string PP078CC
            {
                get
                {
                    return this.pP078CCField;
                }
                set
                {
                    this.pP078CCField = value;
                }
            }



            public string PP082CL
            {
                get
                {
                    return this.pP082CLField;
                }
                set
                {
                    this.pP082CLField = value;
                }
            }



            public string PP082NL
            {
                get
                {
                    return this.pP082NLField;
                }
                set
                {
                    this.pP082NLField = value;
                }
            }



            public string PP099AL
            {
                get
                {
                    return this.pP099ALField;
                }
                set
                {
                    this.pP099ALField = value;
                }
            }



            public string PP100NL
            {
                get
                {
                    return this.pP100NLField;
                }
                set
                {
                    this.pP100NLField = value;
                }
            }
        }







        public class BCCBC03
        {

            private string consumerNoField;

            private string bCCsField;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string BCCs
            {
                get
                {
                    return this.bCCsField;
                }
                set
                {
                    this.bCCsField = value;
                }
            }
        }







        public class BCCBC04
        {

            private string consumerNoField;

            private string bCCsField;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string BCCs
            {
                get
                {
                    return this.bCCsField;
                }
                set
                {
                    this.bCCsField = value;
                }
            }
        }







        public class BCCC1
        {

            private string consumerNoField;

            private string bCCsField;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string BCCs
            {
                get
                {
                    return this.bCCsField;
                }
                set
                {
                    this.bCCsField = value;
                }
            }
        }







        public class BCCC2
        {

            private string consumerNoField;

            private string bCCsField;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string BCCs
            {
                get
                {
                    return this.bCCsField;
                }
                set
                {
                    this.bCCsField = value;
                }
            }
        }







        public class CCASummaryMX01
        {

            private string consumerNoField;

            private string totalActiveAccountsField;

            private string totalClosedAccounts24MthsField;

            private string totalAdverseAccounts24MthsField;

            private string highestActualMonths24MthsField;

            private string noRevolvingAccountsField;

            private string noCurrentInstallmentAccountsField;

            private string noCurrentOpenAccountsField;

            private string currentBalanceField;

            private string currentBalanceIndField;

            private string currentMonthlyInstallmentField;

            private string currentMonthlyInstallmentBalIndField;

            private string cumulativeArrearsAmountField;

            private string cumulativeArrearsAmountBalanceIndField;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string TotalActiveAccounts
            {
                get
                {
                    return this.totalActiveAccountsField;
                }
                set
                {
                    this.totalActiveAccountsField = value;
                }
            }



            public string TotalClosedAccounts24Mths
            {
                get
                {
                    return this.totalClosedAccounts24MthsField;
                }
                set
                {
                    this.totalClosedAccounts24MthsField = value;
                }
            }



            public string TotalAdverseAccounts24Mths
            {
                get
                {
                    return this.totalAdverseAccounts24MthsField;
                }
                set
                {
                    this.totalAdverseAccounts24MthsField = value;
                }
            }



            public string HighestActualMonths24Mths
            {
                get
                {
                    return this.highestActualMonths24MthsField;
                }
                set
                {
                    this.highestActualMonths24MthsField = value;
                }
            }



            public string NoRevolvingAccounts
            {
                get
                {
                    return this.noRevolvingAccountsField;
                }
                set
                {
                    this.noRevolvingAccountsField = value;
                }
            }



            public string NoCurrentInstallmentAccounts
            {
                get
                {
                    return this.noCurrentInstallmentAccountsField;
                }
                set
                {
                    this.noCurrentInstallmentAccountsField = value;
                }
            }



            public string NoCurrentOpenAccounts
            {
                get
                {
                    return this.noCurrentOpenAccountsField;
                }
                set
                {
                    this.noCurrentOpenAccountsField = value;
                }
            }



            public string CurrentBalance
            {
                get
                {
                    return this.currentBalanceField;
                }
                set
                {
                    this.currentBalanceField = value;
                }
            }



            public string CurrentBalanceInd
            {
                get
                {
                    return this.currentBalanceIndField;
                }
                set
                {
                    this.currentBalanceIndField = value;
                }
            }



            public string CurrentMonthlyInstallment
            {
                get
                {
                    return this.currentMonthlyInstallmentField;
                }
                set
                {
                    this.currentMonthlyInstallmentField = value;
                }
            }



            public string CurrentMonthlyInstallmentBalInd
            {
                get
                {
                    return this.currentMonthlyInstallmentBalIndField;
                }
                set
                {
                    this.currentMonthlyInstallmentBalIndField = value;
                }
            }



            public string CumulativeArrearsAmount
            {
                get
                {
                    return this.cumulativeArrearsAmountField;
                }
                set
                {
                    this.cumulativeArrearsAmountField = value;
                }
            }



            public string CumulativeArrearsAmountBalanceInd
            {
                get
                {
                    return this.cumulativeArrearsAmountBalanceIndField;
                }
                set
                {
                    this.cumulativeArrearsAmountBalanceIndField = value;
                }
            }
        }







        public class ConsEnqTransInfo0102
        {

            private string definiteMatchCountField;

            private string possibleMatchCountField;

            private string matchedConsumerNoField;

            private string possibleConsumerNoField;

            private string possibleAdverseIndicatorField;



            public string DefiniteMatchCount
            {
                get
                {
                    return this.definiteMatchCountField;
                }
                set
                {
                    this.definiteMatchCountField = value;
                }
            }



            public string PossibleMatchCount
            {
                get
                {
                    return this.possibleMatchCountField;
                }
                set
                {
                    this.possibleMatchCountField = value;
                }
            }



            public string MatchedConsumerNo
            {
                get
                {
                    return this.matchedConsumerNoField;
                }
                set
                {
                    this.matchedConsumerNoField = value;
                }
            }



            public string PossibleConsumerNo
            {
                get
                {
                    return this.possibleConsumerNoField;
                }
                set
                {
                    this.possibleConsumerNoField = value;
                }
            }



            public string PossibleAdverseIndicator
            {
                get
                {
                    return this.possibleAdverseIndicatorField;
                }
                set
                {
                    this.possibleAdverseIndicatorField = value;
                }
            }
        }







        public class ConsumerCountersNC04
        {

            private string consumerNoField;

            private string ownEnquiries1YrBackField;

            private string ownEnquiries2YrsBackField;

            private string ownEnquiriesMoreThen2YrsBackField;

            private string otherEnquiries1YrBackField;

            private string otherEnquiries2YrsBackField;

            private string otherEnquiriesMoreThen2YrsBackField;

            private string judgements1YrBackField;

            private string judgements2YrsBackField;

            private string judgementsMoreThen2YrsBackField;

            private string notices1YrBackField;

            private string notices2YrsBackField;

            private string noticesMoreThen2YrsBackField;

            private string defaults1YrBackField;

            private string defaults2YrsBackField;

            private string defaultsMoreThen2YrsBackField;

            private string paymentProfiles1YrBackField;

            private string paymentProfiles2YrsBackField;

            private string paymentProfilesMoreThen2YrsBackField;

            private string traceAlerts1YrBackField;

            private string traceAlerts2YrsBackField;

            private string traceAlertsMoreThen2YrsBackField;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string OwnEnquiries1YrBack
            {
                get
                {
                    return this.ownEnquiries1YrBackField;
                }
                set
                {
                    this.ownEnquiries1YrBackField = value;
                }
            }



            public string OwnEnquiries2YrsBack
            {
                get
                {
                    return this.ownEnquiries2YrsBackField;
                }
                set
                {
                    this.ownEnquiries2YrsBackField = value;
                }
            }



            public string OwnEnquiriesMoreThen2YrsBack
            {
                get
                {
                    return this.ownEnquiriesMoreThen2YrsBackField;
                }
                set
                {
                    this.ownEnquiriesMoreThen2YrsBackField = value;
                }
            }



            public string OtherEnquiries1YrBack
            {
                get
                {
                    return this.otherEnquiries1YrBackField;
                }
                set
                {
                    this.otherEnquiries1YrBackField = value;
                }
            }



            public string OtherEnquiries2YrsBack
            {
                get
                {
                    return this.otherEnquiries2YrsBackField;
                }
                set
                {
                    this.otherEnquiries2YrsBackField = value;
                }
            }



            public string OtherEnquiriesMoreThen2YrsBack
            {
                get
                {
                    return this.otherEnquiriesMoreThen2YrsBackField;
                }
                set
                {
                    this.otherEnquiriesMoreThen2YrsBackField = value;
                }
            }



            public string Judgements1YrBack
            {
                get
                {
                    return this.judgements1YrBackField;
                }
                set
                {
                    this.judgements1YrBackField = value;
                }
            }



            public string Judgements2YrsBack
            {
                get
                {
                    return this.judgements2YrsBackField;
                }
                set
                {
                    this.judgements2YrsBackField = value;
                }
            }



            public string JudgementsMoreThen2YrsBack
            {
                get
                {
                    return this.judgementsMoreThen2YrsBackField;
                }
                set
                {
                    this.judgementsMoreThen2YrsBackField = value;
                }
            }



            public string Notices1YrBack
            {
                get
                {
                    return this.notices1YrBackField;
                }
                set
                {
                    this.notices1YrBackField = value;
                }
            }



            public string Notices2YrsBack
            {
                get
                {
                    return this.notices2YrsBackField;
                }
                set
                {
                    this.notices2YrsBackField = value;
                }
            }



            public string NoticesMoreThen2YrsBack
            {
                get
                {
                    return this.noticesMoreThen2YrsBackField;
                }
                set
                {
                    this.noticesMoreThen2YrsBackField = value;
                }
            }



            public string Defaults1YrBack
            {
                get
                {
                    return this.defaults1YrBackField;
                }
                set
                {
                    this.defaults1YrBackField = value;
                }
            }



            public string Defaults2YrsBack
            {
                get
                {
                    return this.defaults2YrsBackField;
                }
                set
                {
                    this.defaults2YrsBackField = value;
                }
            }



            public string DefaultsMoreThen2YrsBack
            {
                get
                {
                    return this.defaultsMoreThen2YrsBackField;
                }
                set
                {
                    this.defaultsMoreThen2YrsBackField = value;
                }
            }



            public string PaymentProfiles1YrBack
            {
                get
                {
                    return this.paymentProfiles1YrBackField;
                }
                set
                {
                    this.paymentProfiles1YrBackField = value;
                }
            }



            public string PaymentProfiles2YrsBack
            {
                get
                {
                    return this.paymentProfiles2YrsBackField;
                }
                set
                {
                    this.paymentProfiles2YrsBackField = value;
                }
            }



            public string PaymentProfilesMoreThen2YrsBack
            {
                get
                {
                    return this.paymentProfilesMoreThen2YrsBackField;
                }
                set
                {
                    this.paymentProfilesMoreThen2YrsBackField = value;
                }
            }



            public string TraceAlerts1YrBack
            {
                get
                {
                    return this.traceAlerts1YrBackField;
                }
                set
                {
                    this.traceAlerts1YrBackField = value;
                }
            }



            public string TraceAlerts2YrsBack
            {
                get
                {
                    return this.traceAlerts2YrsBackField;
                }
                set
                {
                    this.traceAlerts2YrsBackField = value;
                }
            }



            public string TraceAlertsMoreThen2YrsBack
            {
                get
                {
                    return this.traceAlertsMoreThen2YrsBackField;
                }
                set
                {
                    this.traceAlertsMoreThen2YrsBackField = value;
                }
            }
        }







        public class ConsumerInfoNO04
        {

            private string recordSeqField;

            private string partField;

            private string partSeqField;

            private string consumerNoField;

            private string surnameField;

            private string forename1Field;

            private string forename2Field;

            private string forename3Field;

            private string titleField;

            private string genderField;

            private string nameInfoDateField;

            private string dateOfBirthField;

            private string identityNo1Field;

            private string identityNo2Field;

            private string maritalStatusCodeField;

            private string maritalStatusDescField;

            private string dependantsField;

            private string spouseName1Field;

            private string spouseName2Field;

            private string telephoneNumbersField;

            private string deceasedDateField;



            public string RecordSeq
            {
                get
                {
                    return this.recordSeqField;
                }
                set
                {
                    this.recordSeqField = value;
                }
            }



            public string Part
            {
                get
                {
                    return this.partField;
                }
                set
                {
                    this.partField = value;
                }
            }



            public string PartSeq
            {
                get
                {
                    return this.partSeqField;
                }
                set
                {
                    this.partSeqField = value;
                }
            }



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string Surname
            {
                get
                {
                    return this.surnameField;
                }
                set
                {
                    this.surnameField = value;
                }
            }



            public string Forename1
            {
                get
                {
                    return this.forename1Field;
                }
                set
                {
                    this.forename1Field = value;
                }
            }



            public string Forename2
            {
                get
                {
                    return this.forename2Field;
                }
                set
                {
                    this.forename2Field = value;
                }
            }



            public string Forename3
            {
                get
                {
                    return this.forename3Field;
                }
                set
                {
                    this.forename3Field = value;
                }
            }



            public string Title
            {
                get
                {
                    return this.titleField;
                }
                set
                {
                    this.titleField = value;
                }
            }



            public string Gender
            {
                get
                {
                    return this.genderField;
                }
                set
                {
                    this.genderField = value;
                }
            }



            public string NameInfoDate
            {
                get
                {
                    return this.nameInfoDateField;
                }
                set
                {
                    this.nameInfoDateField = value;
                }
            }



            public string DateOfBirth
            {
                get
                {
                    return this.dateOfBirthField;
                }
                set
                {
                    this.dateOfBirthField = value;
                }
            }



            public string IdentityNo1
            {
                get
                {
                    return this.identityNo1Field;
                }
                set
                {
                    this.identityNo1Field = value;
                }
            }



            public string IdentityNo2
            {
                get
                {
                    return this.identityNo2Field;
                }
                set
                {
                    this.identityNo2Field = value;
                }
            }



            public string MaritalStatusCode
            {
                get
                {
                    return this.maritalStatusCodeField;
                }
                set
                {
                    this.maritalStatusCodeField = value;
                }
            }



            public string MaritalStatusDesc
            {
                get
                {
                    return this.maritalStatusDescField;
                }
                set
                {
                    this.maritalStatusDescField = value;
                }
            }



            public string Dependants
            {
                get
                {
                    return this.dependantsField;
                }
                set
                {
                    this.dependantsField = value;
                }
            }



            public string SpouseName1
            {
                get
                {
                    return this.spouseName1Field;
                }
                set
                {
                    this.spouseName1Field = value;
                }
            }



            public string SpouseName2
            {
                get
                {
                    return this.spouseName2Field;
                }
                set
                {
                    this.spouseName2Field = value;
                }
            }



            public string TelephoneNumbers
            {
                get
                {
                    return this.telephoneNumbersField;
                }
                set
                {
                    this.telephoneNumbersField = value;
                }
            }



            public string DeceasedDate
            {
                get
                {
                    return this.deceasedDateField;
                }
                set
                {
                    this.deceasedDateField = value;
                }
            }
        }







        public class ConsumerInfoNO05
        {

            private string recordSeqField;

            private string partField;

            private string partSeqField;

            private string consumerNoField;

            private string surnameField;

            private string forename1Field;

            private string forename2Field;

            private string forename3Field;

            private string titleField;

            private string genderField;

            private string nameInfoDateField;

            private string dateOfBirthField;

            private string identityNo1Field;

            private string identityNo2Field;

            private string maritalStatusCodeField;

            private string maritalStatusDescField;

            private string dependantsField;

            private string spouseName1Field;

            private string spouseName2Field;

            private string telephoneNumbersField;

            private string deceasedDateField;

            private string cellNumberField;

            private string eMailField;



            public string RecordSeq
            {
                get
                {
                    return this.recordSeqField;
                }
                set
                {
                    this.recordSeqField = value;
                }
            }



            public string Part
            {
                get
                {
                    return this.partField;
                }
                set
                {
                    this.partField = value;
                }
            }



            public string PartSeq
            {
                get
                {
                    return this.partSeqField;
                }
                set
                {
                    this.partSeqField = value;
                }
            }



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string Surname
            {
                get
                {
                    return this.surnameField;
                }
                set
                {
                    this.surnameField = value;
                }
            }



            public string Forename1
            {
                get
                {
                    return this.forename1Field;
                }
                set
                {
                    this.forename1Field = value;
                }
            }



            public string Forename2
            {
                get
                {
                    return this.forename2Field;
                }
                set
                {
                    this.forename2Field = value;
                }
            }



            public string Forename3
            {
                get
                {
                    return this.forename3Field;
                }
                set
                {
                    this.forename3Field = value;
                }
            }



            public string Title
            {
                get
                {
                    return this.titleField;
                }
                set
                {
                    this.titleField = value;
                }
            }



            public string Gender
            {
                get
                {
                    return this.genderField;
                }
                set
                {
                    this.genderField = value;
                }
            }



            public string NameInfoDate
            {
                get
                {
                    return this.nameInfoDateField;
                }
                set
                {
                    this.nameInfoDateField = value;
                }
            }



            public string DateOfBirth
            {
                get
                {
                    return this.dateOfBirthField;
                }
                set
                {
                    this.dateOfBirthField = value;
                }
            }



            public string IdentityNo1
            {
                get
                {
                    return this.identityNo1Field;
                }
                set
                {
                    this.identityNo1Field = value;
                }
            }



            public string IdentityNo2
            {
                get
                {
                    return this.identityNo2Field;
                }
                set
                {
                    this.identityNo2Field = value;
                }
            }



            public string MaritalStatusCode
            {
                get
                {
                    return this.maritalStatusCodeField;
                }
                set
                {
                    this.maritalStatusCodeField = value;
                }
            }



            public string MaritalStatusDesc
            {
                get
                {
                    return this.maritalStatusDescField;
                }
                set
                {
                    this.maritalStatusDescField = value;
                }
            }



            public string Dependants
            {
                get
                {
                    return this.dependantsField;
                }
                set
                {
                    this.dependantsField = value;
                }
            }



            public string SpouseName1
            {
                get
                {
                    return this.spouseName1Field;
                }
                set
                {
                    this.spouseName1Field = value;
                }
            }



            public string SpouseName2
            {
                get
                {
                    return this.spouseName2Field;
                }
                set
                {
                    this.spouseName2Field = value;
                }
            }



            public string TelephoneNumbers
            {
                get
                {
                    return this.telephoneNumbersField;
                }
                set
                {
                    this.telephoneNumbersField = value;
                }
            }



            public string DeceasedDate
            {
                get
                {
                    return this.deceasedDateField;
                }
                set
                {
                    this.deceasedDateField = value;
                }
            }



            public string CellNumber
            {
                get
                {
                    return this.cellNumberField;
                }
                set
                {
                    this.cellNumberField = value;
                }
            }



            public string EMail
            {
                get
                {
                    return this.eMailField;
                }
                set
                {
                    this.eMailField = value;
                }
            }
        }







        public class ConsumerTelephoneHistoryNW01
        {

            private string consumerNoField;

            private string workNumbersField;

            private string homeNumbersField;

            private string cellNumbersField;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string WorkNumbers
            {
                get
                {
                    return this.workNumbersField;
                }
                set
                {
                    this.workNumbersField = value;
                }
            }



            public string HomeNumbers
            {
                get
                {
                    return this.homeNumbersField;
                }
                set
                {
                    this.homeNumbersField = value;
                }
            }



            public string CellNumbers
            {
                get
                {
                    return this.cellNumbersField;
                }
                set
                {
                    this.cellNumbersField = value;
                }
            }
        }







        public class DebtCounsellingDC01
        {

            private string consumerNoField;

            private string debtCounsellingDateField;

            private string debtCounsellingCodeField;

            private string debtCounsellingDescriptionField;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string DebtCounsellingDate
            {
                get
                {
                    return this.debtCounsellingDateField;
                }
                set
                {
                    this.debtCounsellingDateField = value;
                }
            }



            public string DebtCounsellingCode
            {
                get
                {
                    return this.debtCounsellingCodeField;
                }
                set
                {
                    this.debtCounsellingCodeField = value;
                }
            }



            public string DebtCounsellingDescription
            {
                get
                {
                    return this.debtCounsellingDescriptionField;
                }
                set
                {
                    this.debtCounsellingDescriptionField = value;
                }
            }
        }







        public class DefaultD701Part1
        {

            private string informationDateField;

            private string supplierNameField;

            private string accountNoField;

            private string defaultAmountField;

            private string defaultTypeField;



            public string InformationDate
            {
                get
                {
                    return this.informationDateField;
                }
                set
                {
                    this.informationDateField = value;
                }
            }



            public string SupplierName
            {
                get
                {
                    return this.supplierNameField;
                }
                set
                {
                    this.supplierNameField = value;
                }
            }



            public string AccountNo
            {
                get
                {
                    return this.accountNoField;
                }
                set
                {
                    this.accountNoField = value;
                }
            }



            public string DefaultAmount
            {
                get
                {
                    return this.defaultAmountField;
                }
                set
                {
                    this.defaultAmountField = value;
                }
            }



            public string DefaultType
            {
                get
                {
                    return this.defaultTypeField;
                }
                set
                {
                    this.defaultTypeField = value;
                }
            }
        }







        public class DefaultD701Part2
        {
        }







        public class DisputeIndicatorDI01
        {

            private string consumerNoField;

            private string captureDateField;

            private string expiryDateField;

            private string daysToExpiryField;

            private string surnameField;

            private string forename1Field;

            private string forename2Field;

            private string forename3Field;

            private string dateOfBirthField;

            private string identityNoField;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string CaptureDate
            {
                get
                {
                    return this.captureDateField;
                }
                set
                {
                    this.captureDateField = value;
                }
            }



            public string ExpiryDate
            {
                get
                {
                    return this.expiryDateField;
                }
                set
                {
                    this.expiryDateField = value;
                }
            }



            public string DaysToExpiry
            {
                get
                {
                    return this.daysToExpiryField;
                }
                set
                {
                    this.daysToExpiryField = value;
                }
            }



            public string Surname
            {
                get
                {
                    return this.surnameField;
                }
                set
                {
                    this.surnameField = value;
                }
            }



            public string Forename1
            {
                get
                {
                    return this.forename1Field;
                }
                set
                {
                    this.forename1Field = value;
                }
            }



            public string Forename2
            {
                get
                {
                    return this.forename2Field;
                }
                set
                {
                    this.forename2Field = value;
                }
            }



            public string Forename3
            {
                get
                {
                    return this.forename3Field;
                }
                set
                {
                    this.forename3Field = value;
                }
            }



            public string DateOfBirth
            {
                get
                {
                    return this.dateOfBirthField;
                }
                set
                {
                    this.dateOfBirthField = value;
                }
            }



            public string IdentityNo
            {
                get
                {
                    return this.identityNoField;
                }
                set
                {
                    this.identityNoField = value;
                }
            }
        }







        public class EchoData0001
        {

            private string subscriberCodeField;

            private string clientReferenceField;

            private string branchNumberField;

            private string batchNumberField;



            public string SubscriberCode
            {
                get
                {
                    return this.subscriberCodeField;
                }
                set
                {
                    this.subscriberCodeField = value;
                }
            }



            public string ClientReference
            {
                get
                {
                    return this.clientReferenceField;
                }
                set
                {
                    this.clientReferenceField = value;
                }
            }



            public string BranchNumber
            {
                get
                {
                    return this.branchNumberField;
                }
                set
                {
                    this.branchNumberField = value;
                }
            }



            public string BatchNumber
            {
                get
                {
                    return this.batchNumberField;
                }
                set
                {
                    this.batchNumberField = value;
                }
            }
        }







        public class EmpiricaEM04
        {

            private string consumerNoField;

            private string empiricaScoreField;

            private string exclusionCodeField;

            private string exclusionCodeDescriptionField;

            private string[] reasonCodeField;

            private string[] reasonDescriptionField;

            private string expansionScoreField;

            private string expansionScoreDescriptionField;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string EmpiricaScore
            {
                get
                {
                    return this.empiricaScoreField;
                }
                set
                {
                    this.empiricaScoreField = value;
                }
            }



            public string ExclusionCode
            {
                get
                {
                    return this.exclusionCodeField;
                }
                set
                {
                    this.exclusionCodeField = value;
                }
            }



            public string ExclusionCodeDescription
            {
                get
                {
                    return this.exclusionCodeDescriptionField;
                }
                set
                {
                    this.exclusionCodeDescriptionField = value;
                }
            }



            public string[] ReasonCode
            {
                get
                {
                    return this.reasonCodeField;
                }
                set
                {
                    this.reasonCodeField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlArrayItemAttribute("string", typeof(@string))]
            public string[] ReasonDescription
            {
                get
                {
                    return this.reasonDescriptionField;
                }
                set
                {
                    this.reasonDescriptionField = value;
                }
            }


            public string ExpansionScore
            {
                get
                {
                    return this.expansionScoreField;
                }
                set
                {
                    this.expansionScoreField = value;
                }
            }



            public string ExpansionScoreDescription
            {
                get
                {
                    return this.expansionScoreDescriptionField;
                }
                set
                {
                    this.expansionScoreDescriptionField = value;
                }
            }
        }



        public class @string
        {

            private string valueField = string.Empty;

            /// <remarks/>
            //[System.Xml.Serialization.XmlTextAttribute()]
            public string Value
            {
                get
                {
                    return this.valueField;
                }
                set
                {
                    this.valueField = value;
                }
            }
        }



        public class EmpiricaEM05
        {

            private string consumerNoField;

            private string empiricaScoreField;

            private string exclusionCodeField;

            private string reasonCode1Field;

            private string reasonCode2Field;

            private string reasonCode3Field;

            private string reasonCode4Field;

            private string expansionScoreField;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string EmpiricaScore
            {
                get
                {
                    return this.empiricaScoreField;
                }
                set
                {
                    this.empiricaScoreField = value;
                }
            }



            public string ExclusionCode
            {
                get
                {
                    return this.exclusionCodeField;
                }
                set
                {
                    this.exclusionCodeField = value;
                }
            }



            public string ReasonCode1
            {
                get
                {
                    return this.reasonCode1Field;
                }
                set
                {
                    this.reasonCode1Field = value;
                }
            }



            public string ReasonCode2
            {
                get
                {
                    return this.reasonCode2Field;
                }
                set
                {
                    this.reasonCode2Field = value;
                }
            }



            public string ReasonCode3
            {
                get
                {
                    return this.reasonCode3Field;
                }
                set
                {
                    this.reasonCode3Field = value;
                }
            }



            public string ReasonCode4
            {
                get
                {
                    return this.reasonCode4Field;
                }
                set
                {
                    this.reasonCode4Field = value;
                }
            }



            public string ExpansionScore
            {
                get
                {
                    return this.expansionScoreField;
                }
                set
                {
                    this.expansionScoreField = value;
                }
            }
        }







        public class EmpiricaEM07
        {

            private string consumerNoField;

            private string empiricaScoreField;

            private string exclusionCodeField;

            private string exclusionCodeDescriptionField;

            private string reasonCodeField;

            private string reasonDescriptionField;

            private string expansionScoreField;

            private string expansionScoreDescriptionField;

            private string empiricaVersionField;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string EmpiricaScore
            {
                get
                {
                    return this.empiricaScoreField;
                }
                set
                {
                    this.empiricaScoreField = value;
                }
            }



            public string ExclusionCode
            {
                get
                {
                    return this.exclusionCodeField;
                }
                set
                {
                    this.exclusionCodeField = value;
                }
            }



            public string ExclusionCodeDescription
            {
                get
                {
                    return this.exclusionCodeDescriptionField;
                }
                set
                {
                    this.exclusionCodeDescriptionField = value;
                }
            }



            public string ReasonCode
            {
                get
                {
                    return this.reasonCodeField;
                }
                set
                {
                    this.reasonCodeField = value;
                }
            }



            public string ReasonDescription
            {
                get
                {
                    return this.reasonDescriptionField;
                }
                set
                {
                    this.reasonDescriptionField = value;
                }
            }



            public string ExpansionScore
            {
                get
                {
                    return this.expansionScoreField;
                }
                set
                {
                    this.expansionScoreField = value;
                }
            }



            public string ExpansionScoreDescription
            {
                get
                {
                    return this.expansionScoreDescriptionField;
                }
                set
                {
                    this.expansionScoreDescriptionField = value;
                }
            }



            public string EmpiricaVersion
            {
                get
                {
                    return this.empiricaVersionField;
                }
                set
                {
                    this.empiricaVersionField = value;
                }
            }
        }







        public class EnquiryNE50
        {
            private string consumerNumberField;

            private string dateOfEnquiryField;

            private string subscriberField;

            private string contactField;

            private string enquiryTypeCodeField;

            private string enquiryTypeDescriptionField;

            private string ownAccountField;



            public string ConsumerNumber
            {
                get
                {
                    return this.consumerNumberField;
                }
                set
                {
                    this.consumerNumberField = value;
                }
            }



            public string DateOfEnquiry
            {
                get
                {
                    return this.dateOfEnquiryField;
                }
                set
                {
                    this.dateOfEnquiryField = value;
                }
            }



            public string Subscriber
            {
                get
                {
                    return this.subscriberField;
                }
                set
                {
                    this.subscriberField = value;
                }
            }



            public string Contact
            {
                get
                {
                    return this.contactField;
                }
                set
                {
                    this.contactField = value;
                }
            }



            public string EnquiryTypeCode
            {
                get
                {
                    return this.enquiryTypeCodeField;
                }
                set
                {
                    this.enquiryTypeCodeField = value;
                }
            }



            public string EnquiryTypeDescription
            {
                get
                {
                    return this.enquiryTypeDescriptionField;
                }
                set
                {
                    this.enquiryTypeDescriptionField = value;
                }
            }



            public string OwnAccount
            {
                get
                {
                    return this.ownAccountField;
                }
                set
                {
                    this.ownAccountField = value;
                }
            }
        }







        public class FSMS201
        {

            private string consumerNumberField;

            private string fSMScoreBandField;

            private string verifyReason1Field;

            private string verifyReason2Field;

            private string verifyReason3Field;

            private string verifyReason4Field;

            private string verifyReason5Field;



            public string ConsumerNumber
            {
                get
                {
                    return this.consumerNumberField;
                }
                set
                {
                    this.consumerNumberField = value;
                }
            }



            public string FSMScoreBand
            {
                get
                {
                    return this.fSMScoreBandField;
                }
                set
                {
                    this.fSMScoreBandField = value;
                }
            }



            public string VerifyReason1
            {
                get
                {
                    return this.verifyReason1Field;
                }
                set
                {
                    this.verifyReason1Field = value;
                }
            }



            public string VerifyReason2
            {
                get
                {
                    return this.verifyReason2Field;
                }
                set
                {
                    this.verifyReason2Field = value;
                }
            }



            public string VerifyReason3
            {
                get
                {
                    return this.verifyReason3Field;
                }
                set
                {
                    this.verifyReason3Field = value;
                }
            }



            public string VerifyReason4
            {
                get
                {
                    return this.verifyReason4Field;
                }
                set
                {
                    this.verifyReason4Field = value;
                }
            }



            public string VerifyReason5
            {
                get
                {
                    return this.verifyReason5Field;
                }
                set
                {
                    this.verifyReason5Field = value;
                }
            }
        }







        public class FraudScoreFS01
        {

            private string recordSequenceField;

            private string partField;

            private string partSequenceField;

            private string consumerNoField;

            private string ratingField;

            private string ratingDescriptionField;

            private string reasonCodeField;

            private string reasonDescriptionField;



            public string RecordSequence
            {
                get
                {
                    return this.recordSequenceField;
                }
                set
                {
                    this.recordSequenceField = value;
                }
            }



            public string Part
            {
                get
                {
                    return this.partField;
                }
                set
                {
                    this.partField = value;
                }
            }



            public string PartSequence
            {
                get
                {
                    return this.partSequenceField;
                }
                set
                {
                    this.partSequenceField = value;
                }
            }



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string Rating
            {
                get
                {
                    return this.ratingField;
                }
                set
                {
                    this.ratingField = value;
                }
            }



            public string RatingDescription
            {
                get
                {
                    return this.ratingDescriptionField;
                }
                set
                {
                    this.ratingDescriptionField = value;
                }
            }



            public string ReasonCode
            {
                get
                {
                    return this.reasonCodeField;
                }
                set
                {
                    this.reasonCodeField = value;
                }
            }



            public string ReasonDescription
            {
                get
                {
                    return this.reasonDescriptionField;
                }
                set
                {
                    this.reasonDescriptionField = value;
                }
            }
        }







        public class HawkNH05
        {

            private string hawkNoField;

            private string hawkCodeField;

            private string hawkDescField;

            private string hawkFoundField;

            private string deceasedDateField;

            private string subscriberNameField;

            private string subscriberRefField;

            private string contactNameField;

            private string contactTelCodeField;

            private string contactTelNoField;

            private string victimReferenceField;

            private string victimTelCodeField;

            private string victimTelNoField;



            public string HawkNo
            {
                get
                {
                    return this.hawkNoField;
                }
                set
                {
                    this.hawkNoField = value;
                }
            }



            public string HawkCode
            {
                get
                {
                    return this.hawkCodeField;
                }
                set
                {
                    this.hawkCodeField = value;
                }
            }



            public string HawkDesc
            {
                get
                {
                    return this.hawkDescField;
                }
                set
                {
                    this.hawkDescField = value;
                }
            }



            public string HawkFound
            {
                get
                {
                    return this.hawkFoundField;
                }
                set
                {
                    this.hawkFoundField = value;
                }
            }



            public string DeceasedDate
            {
                get
                {
                    return this.deceasedDateField;
                }
                set
                {
                    this.deceasedDateField = value;
                }
            }



            public string SubscriberName
            {
                get
                {
                    return this.subscriberNameField;
                }
                set
                {
                    this.subscriberNameField = value;
                }
            }



            public string SubscriberRef
            {
                get
                {
                    return this.subscriberRefField;
                }
                set
                {
                    this.subscriberRefField = value;
                }
            }



            public string ContactName
            {
                get
                {
                    return this.contactNameField;
                }
                set
                {
                    this.contactNameField = value;
                }
            }



            public string ContactTelCode
            {
                get
                {
                    return this.contactTelCodeField;
                }
                set
                {
                    this.contactTelCodeField = value;
                }
            }



            public string ContactTelNo
            {
                get
                {
                    return this.contactTelNoField;
                }
                set
                {
                    this.contactTelNoField = value;
                }
            }



            public string VictimReference
            {
                get
                {
                    return this.victimReferenceField;
                }
                set
                {
                    this.victimReferenceField = value;
                }
            }



            public string VictimTelCode
            {
                get
                {
                    return this.victimTelCodeField;
                }
                set
                {
                    this.victimTelCodeField = value;
                }
            }



            public string VictimTelNo
            {
                get
                {
                    return this.victimTelNoField;
                }
                set
                {
                    this.victimTelNoField = value;
                }
            }
        }







        public class IdvNI01
        {

            private string iDVerifiedCodeField;

            private string iDVerifiedDescField;

            private string iDWarningField;

            private string iDDescField;

            private string verifiedSurnameField;

            private string verifiedForename1Field;

            private string verifiedForename2Field;

            private string deceasedDateField;



            public string IDVerifiedCode
            {
                get
                {
                    return this.iDVerifiedCodeField;
                }
                set
                {
                    this.iDVerifiedCodeField = value;
                }
            }



            public string IDVerifiedDesc
            {
                get
                {
                    return this.iDVerifiedDescField;
                }
                set
                {
                    this.iDVerifiedDescField = value;
                }
            }



            public string IDWarning
            {
                get
                {
                    return this.iDWarningField;
                }
                set
                {
                    this.iDWarningField = value;
                }
            }



            public string IDDesc
            {
                get
                {
                    return this.iDDescField;
                }
                set
                {
                    this.iDDescField = value;
                }
            }



            public string VerifiedSurname
            {
                get
                {
                    return this.verifiedSurnameField;
                }
                set
                {
                    this.verifiedSurnameField = value;
                }
            }



            public string VerifiedForename1
            {
                get
                {
                    return this.verifiedForename1Field;
                }
                set
                {
                    this.verifiedForename1Field = value;
                }
            }



            public string VerifiedForename2
            {
                get
                {
                    return this.verifiedForename2Field;
                }
                set
                {
                    this.verifiedForename2Field = value;
                }
            }



            public string DeceasedDate
            {
                get
                {
                    return this.deceasedDateField;
                }
                set
                {
                    this.deceasedDateField = value;
                }
            }
        }







        public class IdvNI02
        {

            private string enquiryDateField;

            private string iDNumberField;

            private string iDVerifiedCodeField;

            private string iDVerifiedDescField;

            private string iDWarningField;

            private string iDDescField;

            private string verifiedSurnameField;

            private string verifiedForename1Field;

            private string verifiedForename2Field;

            private string deceasedDateField;



            public string EnquiryDate
            {
                get
                {
                    return this.enquiryDateField;
                }
                set
                {
                    this.enquiryDateField = value;
                }
            }



            public string IDNumber
            {
                get
                {
                    return this.iDNumberField;
                }
                set
                {
                    this.iDNumberField = value;
                }
            }



            public string IDVerifiedCode
            {
                get
                {
                    return this.iDVerifiedCodeField;
                }
                set
                {
                    this.iDVerifiedCodeField = value;
                }
            }



            public string IDVerifiedDesc
            {
                get
                {
                    return this.iDVerifiedDescField;
                }
                set
                {
                    this.iDVerifiedDescField = value;
                }
            }



            public string IDWarning
            {
                get
                {
                    return this.iDWarningField;
                }
                set
                {
                    this.iDWarningField = value;
                }
            }



            public string IDDesc
            {
                get
                {
                    return this.iDDescField;
                }
                set
                {
                    this.iDDescField = value;
                }
            }



            public string VerifiedSurname
            {
                get
                {
                    return this.verifiedSurnameField;
                }
                set
                {
                    this.verifiedSurnameField = value;
                }
            }



            public string VerifiedForename1
            {
                get
                {
                    return this.verifiedForename1Field;
                }
                set
                {
                    this.verifiedForename1Field = value;
                }
            }



            public string VerifiedForename2
            {
                get
                {
                    return this.verifiedForename2Field;
                }
                set
                {
                    this.verifiedForename2Field = value;
                }
            }



            public string DeceasedDate
            {
                get
                {
                    return this.deceasedDateField;
                }
                set
                {
                    this.deceasedDateField = value;
                }
            }
        }







        public class IdvNI03
        {

            private string iDVerifiedCodeField;

            private string iDVerifiedDescField;

            private string iDWarningField;

            private string iDDescField;

            private string verifiedSurnameField;

            private string verifiedForename1Field;

            private string verifiedForename2Field;

            private string deceasedDateField;

            private string iDVerifiedIndicatorField;



            public string IDVerifiedCode
            {
                get
                {
                    return this.iDVerifiedCodeField;
                }
                set
                {
                    this.iDVerifiedCodeField = value;
                }
            }



            public string IDVerifiedDesc
            {
                get
                {
                    return this.iDVerifiedDescField;
                }
                set
                {
                    this.iDVerifiedDescField = value;
                }
            }



            public string IDWarning
            {
                get
                {
                    return this.iDWarningField;
                }
                set
                {
                    this.iDWarningField = value;
                }
            }



            public string IDDesc
            {
                get
                {
                    return this.iDDescField;
                }
                set
                {
                    this.iDDescField = value;
                }
            }



            public string VerifiedSurname
            {
                get
                {
                    return this.verifiedSurnameField;
                }
                set
                {
                    this.verifiedSurnameField = value;
                }
            }



            public string VerifiedForename1
            {
                get
                {
                    return this.verifiedForename1Field;
                }
                set
                {
                    this.verifiedForename1Field = value;
                }
            }



            public string VerifiedForename2
            {
                get
                {
                    return this.verifiedForename2Field;
                }
                set
                {
                    this.verifiedForename2Field = value;
                }
            }



            public string DeceasedDate
            {
                get
                {
                    return this.deceasedDateField;
                }
                set
                {
                    this.deceasedDateField = value;
                }
            }



            public string IDVerifiedIndicator
            {
                get
                {
                    return this.iDVerifiedIndicatorField;
                }
                set
                {
                    this.iDVerifiedIndicatorField = value;
                }
            }
        }







        public class IncomeEstimatorT101
        {

            private string consumerNoField;

            private string predictedIncomeField;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string PredictedIncome
            {
                get
                {
                    return this.predictedIncomeField;
                }
                set
                {
                    this.predictedIncomeField = value;
                }
            }
        }







        public class IncomeEstimatorT102
        {

            private string consumerNoField;

            private string predictedIncomeField;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string PredictedIncome
            {
                get
                {
                    return this.predictedIncomeField;
                }
                set
                {
                    this.predictedIncomeField = value;
                }
            }
        }







        public class NLRAccountInformationM701
        {

            private string lastUpdateDateField = string.Empty;

            private string subscriberNameField = string.Empty;

            private string accountTypeCodeField = string.Empty;

            private string accountNumberField = string.Empty;

            private string dateOpenedField = string.Empty;

            private string openingBalanceField = string.Empty;

            private string instalmentField = string.Empty;

            private string currentBalanceField = string.Empty;

            private string termsField = string.Empty;

            private string endUseCodeField = string.Empty;

            private string subscriberCodeField = string.Empty;

            private PaymentHistory[] paymentHistoriesField;

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string LastUpdateDate
            {
                get
                {
                    return this.lastUpdateDateField;
                }
                set
                {
                    this.lastUpdateDateField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string SubscriberName
            {
                get
                {
                    return this.subscriberNameField;
                }
                set
                {
                    this.subscriberNameField = value;
                }
            }

            /// <remarks/>
            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string AccountType
            {
                get
                {
                    return this.accountTypeCodeField;
                }
                set
                {
                    this.accountTypeCodeField = value;
                }
            }

            /// <remarks/>
            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string AccountNo
            {
                get
                {
                    return this.accountNumberField;
                }
                set
                {
                    this.accountNumberField = value;
                }
            }

            /// <remarks/>
            public string AccountOpenDate
            {
                get
                {
                    return this.dateOpenedField;
                }
                set
                {
                    this.dateOpenedField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string OpeningBalance
            {
                get
                {
                    return this.openingBalanceField;
                }
                set
                {
                    this.openingBalanceField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string Instalment
            {
                get
                {
                    return this.instalmentField;
                }
                set
                {
                    this.instalmentField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string CurrentBalance
            {
                get
                {
                    return this.currentBalanceField;
                }
                set
                {
                    this.currentBalanceField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string Terms
            {
                get
                {
                    return this.termsField;
                }
                set
                {
                    this.termsField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string EndUseCode
            {
                get
                {
                    return this.endUseCodeField;
                }
                set
                {
                    this.endUseCodeField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string SubscriberCode
            {
                get
                {
                    return this.subscriberCodeField;
                }
                set
                {
                    this.subscriberCodeField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlArrayAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            //[System.Xml.Serialization.XmlArrayItemAttribute("PaymentHistory", typeof(SubmitResponseSubmitResultPaymentProfileP701PaymentHistoriesPaymentHistory), Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=false)]
            public PaymentHistory[] PaymentHistories
            {
                get
                {
                    return this.paymentHistoriesField;
                }
                set
                {
                    this.paymentHistoriesField = value;
                }
            }

        }







        public class NLRAccountInformationM703
        {
        }







        public class NLRConfirmationMR01
        {

            private string consumerNoField;

            private string nLREnquiryRefNoField;

            private string nLRLoanRegNoField;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string NLREnquiryRefNo
            {
                get
                {
                    return this.nLREnquiryRefNoField;
                }
                set
                {
                    this.nLREnquiryRefNoField = value;
                }
            }



            public string NLRLoanRegNo
            {
                get
                {
                    return this.nLRLoanRegNoField;
                }
                set
                {
                    this.nLRLoanRegNoField = value;
                }
            }
        }







        public class NLRConfirmationMZ01
        {

            private string consumerNoField;

            private string nLREnquiryRefNoField;

            private string nLRLoanRegNoField;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string NLREnquiryRefNo
            {
                get
                {
                    return this.nLREnquiryRefNoField;
                }
                set
                {
                    this.nLREnquiryRefNoField = value;
                }
            }



            public string NLRLoanRegNo
            {
                get
                {
                    return this.nLRLoanRegNoField;
                }
                set
                {
                    this.nLRLoanRegNoField = value;
                }
            }
        }







        public class NLREnquiryME50
        {
        }







        public class NLRSummaryMY01
        {

            private string consumerNoField;

            private string totalActiveAccountsField;

            private string totalClosedAccounts24MthsField;

            private string totalAdverseAccounts24MthsField;

            private string highestActualMonths24MthsField;

            private string noRevolvingAccountsField;

            private string noCurrentInstallmentAccountsField;

            private string noCurrentOpenAccountsField;

            private string currentBalanceField;

            private string currentBalanceIndField;

            private string currentMonthlyInstallmentField;

            private string currentMonthlyInstallmentBalIndField;

            private string cumulativeArrearsAmountField;

            private string cumulativeArrearsAmountBalanceIndField;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string TotalActiveAccounts
            {
                get
                {
                    return this.totalActiveAccountsField;
                }
                set
                {
                    this.totalActiveAccountsField = value;
                }
            }



            public string TotalClosedAccounts24Mths
            {
                get
                {
                    return this.totalClosedAccounts24MthsField;
                }
                set
                {
                    this.totalClosedAccounts24MthsField = value;
                }
            }



            public string TotalAdverseAccounts24Mths
            {
                get
                {
                    return this.totalAdverseAccounts24MthsField;
                }
                set
                {
                    this.totalAdverseAccounts24MthsField = value;
                }
            }



            public string HighestActualMonths24Mths
            {
                get
                {
                    return this.highestActualMonths24MthsField;
                }
                set
                {
                    this.highestActualMonths24MthsField = value;
                }
            }



            public string NoRevolvingAccounts
            {
                get
                {
                    return this.noRevolvingAccountsField;
                }
                set
                {
                    this.noRevolvingAccountsField = value;
                }
            }



            public string NoCurrentInstallmentAccounts
            {
                get
                {
                    return this.noCurrentInstallmentAccountsField;
                }
                set
                {
                    this.noCurrentInstallmentAccountsField = value;
                }
            }



            public string NoCurrentOpenAccounts
            {
                get
                {
                    return this.noCurrentOpenAccountsField;
                }
                set
                {
                    this.noCurrentOpenAccountsField = value;
                }
            }



            public string CurrentBalance
            {
                get
                {
                    return this.currentBalanceField;
                }
                set
                {
                    this.currentBalanceField = value;
                }
            }



            public string CurrentBalanceInd
            {
                get
                {
                    return this.currentBalanceIndField;
                }
                set
                {
                    this.currentBalanceIndField = value;
                }
            }



            public string CurrentMonthlyInstallment
            {
                get
                {
                    return this.currentMonthlyInstallmentField;
                }
                set
                {
                    this.currentMonthlyInstallmentField = value;
                }
            }



            public string CurrentMonthlyInstallmentBalInd
            {
                get
                {
                    return this.currentMonthlyInstallmentBalIndField;
                }
                set
                {
                    this.currentMonthlyInstallmentBalIndField = value;
                }
            }



            public string CumulativeArrearsAmount
            {
                get
                {
                    return this.cumulativeArrearsAmountField;
                }
                set
                {
                    this.cumulativeArrearsAmountField = value;
                }
            }



            public string CumulativeArrearsAmountBalanceInd
            {
                get
                {
                    return this.cumulativeArrearsAmountBalanceIndField;
                }
                set
                {
                    this.cumulativeArrearsAmountBalanceIndField = value;
                }
            }
        }







        public class PaymentProfileP601
        {
        }







        public class PaymentProfileP701
        {
            private string consumerNoField = string.Empty;

            private string lastUpdateDateField = string.Empty;

            private string supplierNameField = string.Empty;

            private string industryCodeField = string.Empty;

            private string industryField = string.Empty;

            private string accountTypeCodeField = string.Empty;

            private string accountTypeDescField = string.Empty;

            private string accountNumberField = string.Empty;

            private string subAccountField = string.Empty;

            private string dateOpenedField = string.Empty;

            private string openingBalanceField = string.Empty;

            private string instalmentField = string.Empty;

            private string currentBalanceField = string.Empty;

            private string termsField = string.Empty;

            private string overdueAmountField = string.Empty;

            private string ownershipTypeField = string.Empty;

            private string numberOfParticipantsInJointLoanField = string.Empty;

            private string paymentTypeField = string.Empty;

            private string repaymentFrequencyField = string.Empty;

            private string deferredPaymentDateField = string.Empty;

            private string accSoldTo3rdPartyField = string.Empty;

            private string thirdPartyNameField = string.Empty;

            private PaymentHistory[] paymentHistoriesField;

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string LastUpdateDate
            {
                get
                {
                    return this.lastUpdateDateField;
                }
                set
                {
                    this.lastUpdateDateField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string SupplierName
            {
                get
                {
                    return this.supplierNameField;
                }
                set
                {
                    this.supplierNameField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string IndustryCode
            {
                get
                {
                    return this.industryCodeField;
                }
                set
                {
                    this.industryCodeField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string Industry
            {
                get
                {
                    return this.industryField;
                }
                set
                {
                    this.industryField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string AccountTypeCode
            {
                get
                {
                    return this.accountTypeCodeField;
                }
                set
                {
                    this.accountTypeCodeField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string AccountTypeDesc
            {
                get
                {
                    return this.accountTypeDescField;
                }
                set
                {
                    this.accountTypeDescField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string AccountNumber
            {
                get
                {
                    return this.accountNumberField;
                }
                set
                {
                    this.accountNumberField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string SubAccount
            {
                get
                {
                    return this.subAccountField;
                }
                set
                {
                    this.subAccountField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string DateOpened
            {
                get
                {
                    return this.dateOpenedField;
                }
                set
                {
                    this.dateOpenedField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string OpeningBalance
            {
                get
                {
                    return this.openingBalanceField;
                }
                set
                {
                    this.openingBalanceField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string Instalment
            {
                get
                {
                    return this.instalmentField;
                }
                set
                {
                    this.instalmentField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string CurrentBalance
            {
                get
                {
                    return this.currentBalanceField;
                }
                set
                {
                    this.currentBalanceField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string Terms
            {
                get
                {
                    return this.termsField;
                }
                set
                {
                    this.termsField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string OverdueAmount
            {
                get
                {
                    return this.overdueAmountField;
                }
                set
                {
                    this.overdueAmountField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string OwnershipType
            {
                get
                {
                    return this.ownershipTypeField;
                }
                set
                {
                    this.ownershipTypeField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string NumberOfParticipantsInJointLoan
            {
                get
                {
                    return this.numberOfParticipantsInJointLoanField;
                }
                set
                {
                    this.numberOfParticipantsInJointLoanField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string PaymentType
            {
                get
                {
                    return this.paymentTypeField;
                }
                set
                {
                    this.paymentTypeField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string RepaymentFrequency
            {
                get
                {
                    return this.repaymentFrequencyField;
                }
                set
                {
                    this.repaymentFrequencyField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string DeferredPaymentDate
            {
                get
                {
                    return this.deferredPaymentDateField;
                }
                set
                {
                    this.deferredPaymentDateField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string AccSoldTo3rdParty
            {
                get
                {
                    return this.accSoldTo3rdPartyField;
                }
                set
                {
                    this.accSoldTo3rdPartyField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string ThirdPartyName
            {
                get
                {
                    return this.thirdPartyNameField;
                }
                set
                {
                    this.thirdPartyNameField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlArrayAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            //[System.Xml.Serialization.XmlArrayItemAttribute("PaymentHistory", typeof(SubmitResponseSubmitResultPaymentProfileP701PaymentHistoriesPaymentHistory), Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=false)]
            public PaymentHistory[] PaymentHistories
            {
                get
                {
                    return this.paymentHistoriesField;
                }
                set
                {
                    this.paymentHistoriesField = value;
                }
            }
        }


        public class PaymentHistory
        {

            private string dateField = string.Empty;

            private string statusCodeField = string.Empty;

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string Date
            {
                get
                {
                    return this.dateField;
                }
                set
                {
                    this.dateField = value;
                }
            }

            /// <remarks/>
            //[System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified)]
            public string StatusCode
            {
                get
                {
                    return this.statusCodeField;
                }
                set
                {
                    this.statusCodeField = value;
                }
            }
        }




        public class PaymentProfileP702
        {
        }







        public class PaymentProfileP703
        {
        }







        public class PaymentProfileP704
        {
        }







        public class PaymentProfileP705
        {
        }







        public class ScoreCardAC01
        {

            private int consumerNoField;

            private string scoreTypeField;

            private string creditScoreField;

            private string affordabilityReferTriggerField;

            private string installmentDifferenceDebitCreditIndicatorField;

            private string installmentDifferenceField;

            private string utilisationScoreField;



            public int ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string ScoreType
            {
                get
                {
                    return this.scoreTypeField;
                }
                set
                {
                    this.scoreTypeField = value;
                }
            }



            public string CreditScore
            {
                get
                {
                    return this.creditScoreField;
                }
                set
                {
                    this.creditScoreField = value;
                }
            }



            public string AffordabilityReferTrigger
            {
                get
                {
                    return this.affordabilityReferTriggerField;
                }
                set
                {
                    this.affordabilityReferTriggerField = value;
                }
            }



            public string InstallmentDifferenceDebitCreditIndicator
            {
                get
                {
                    return this.installmentDifferenceDebitCreditIndicatorField;
                }
                set
                {
                    this.installmentDifferenceDebitCreditIndicatorField = value;
                }
            }



            public string InstallmentDifference
            {
                get
                {
                    return this.installmentDifferenceField;
                }
                set
                {
                    this.installmentDifferenceField = value;
                }
            }



            public string UtilisationScore
            {
                get
                {
                    return this.utilisationScoreField;
                }
                set
                {
                    this.utilisationScoreField = value;
                }
            }
        }







        public class ScoreCardBX01
        {

            private string consumerNoField;

            private string policyFiltersField;

            private string indicatorsField;

            private string scoreReasonsField;

            private string riskBandField;

            private string referRiskBandField;

            private string recommendedField;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string PolicyFilters
            {
                get
                {
                    return this.policyFiltersField;
                }
                set
                {
                    this.policyFiltersField = value;
                }
            }



            public string Indicators
            {
                get
                {
                    return this.indicatorsField;
                }
                set
                {
                    this.indicatorsField = value;
                }
            }



            public string ScoreReasons
            {
                get
                {
                    return this.scoreReasonsField;
                }
                set
                {
                    this.scoreReasonsField = value;
                }
            }



            public string RiskBand
            {
                get
                {
                    return this.riskBandField;
                }
                set
                {
                    this.riskBandField = value;
                }
            }



            public string ReferRiskBand
            {
                get
                {
                    return this.referRiskBandField;
                }
                set
                {
                    this.referRiskBandField = value;
                }
            }



            public string Recommended
            {
                get
                {
                    return this.recommendedField;
                }
                set
                {
                    this.recommendedField = value;
                }
            }
        }







        public class ScoreCardBX03
        {

            private string consumerNoField;

            private string policyFiltersField;

            private string indicatorsField;

            private string scoreReasonsField;

            private string riskBandField;

            private string referRiskBandField;

            private string recommendedField;

            private string ruleDescriptionField;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string PolicyFilters
            {
                get
                {
                    return this.policyFiltersField;
                }
                set
                {
                    this.policyFiltersField = value;
                }
            }



            public string Indicators
            {
                get
                {
                    return this.indicatorsField;
                }
                set
                {
                    this.indicatorsField = value;
                }
            }



            public string ScoreReasons
            {
                get
                {
                    return this.scoreReasonsField;
                }
                set
                {
                    this.scoreReasonsField = value;
                }
            }



            public string RiskBand
            {
                get
                {
                    return this.riskBandField;
                }
                set
                {
                    this.riskBandField = value;
                }
            }



            public string ReferRiskBand
            {
                get
                {
                    return this.referRiskBandField;
                }
                set
                {
                    this.referRiskBandField = value;
                }
            }



            public string Recommended
            {
                get
                {
                    return this.recommendedField;
                }
                set
                {
                    this.recommendedField = value;
                }
            }



            public string RuleDescription
            {
                get
                {
                    return this.ruleDescriptionField;
                }
                set
                {
                    this.ruleDescriptionField = value;
                }
            }
        }







        public class StandardBatchCharsB101
        {

            private string consumerNoField;

            private string standardBatchCharsField;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string StandardBatchChars
            {
                get
                {
                    return this.standardBatchCharsField;
                }
                set
                {
                    this.standardBatchCharsField = value;
                }
            }
        }







        public class StandardBatchCharsFA01
        {

            private string consumerNoField;

            private string sBCsField;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string SBCs
            {
                get
                {
                    return this.sBCsField;
                }
                set
                {
                    this.sBCsField = value;
                }
            }
        }







        public class StandardBatchCharsFA02
        {

            private string consumerNoField;

            private string sBCsField;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string SBCs
            {
                get
                {
                    return this.sBCsField;
                }
                set
                {
                    this.sBCsField = value;
                }
            }
        }







        public class StandardBatchCharsFA03
        {

            private string consumerNoField;

            private string standardBatchCharsField;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string StandardBatchChars
            {
                get
                {
                    return this.standardBatchCharsField;
                }
                set
                {
                    this.standardBatchCharsField = value;
                }
            }
        }




        public class StandardBatchCharsSB01
        {
            private string consumerNoField;
            private string nameField;
            private string valueField;
            private string weightingField;

            public string ConsumerNo
            {
                get { return this.consumerNoField; }
                set { this.consumerNoField = value; }
            }

            public string Name
            {
                get { return this.nameField; }
                set { this.nameField = value; }
            }

            public string Value
            {
                get { return this.valueField; }
                set { this.valueField = value; }
            }

            public string Weighting
            {
                get { return this.weightingField; }
                set { this.weightingField = value; }
            }
        }


        public class StandardBatchCharsSB04
        {

            private string consumerNoField;

            private string aT002Field;

            private string aT006Field;

            private string aT042Field;

            private string aT044Field;

            private string aT046Field;

            private string aT090Field;

            private string aT091Field;

            private string aT092Field;

            private string aT093Field;

            private string aT094Field;

            private string aT097Field;

            private string aT140Field;

            private string aT150Field;

            private string aT153Field;

            private string aT159Field;

            private string aT161Field;

            private string aT179Field;

            private string g010Field;

            private string g012Field;

            private string g024Field;

            private string g030Field;

            private string g032Field;

            private string iN158Field;

            private string iN159Field;

            private string iN160Field;

            private string rE001Field;

            private string rE042Field;

            private string rE044Field;

            private string rE107Field;

            private string rE160Field;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string AT002
            {
                get
                {
                    return this.aT002Field;
                }
                set
                {
                    this.aT002Field = value;
                }
            }



            public string AT006
            {
                get
                {
                    return this.aT006Field;
                }
                set
                {
                    this.aT006Field = value;
                }
            }



            public string AT042
            {
                get
                {
                    return this.aT042Field;
                }
                set
                {
                    this.aT042Field = value;
                }
            }



            public string AT044
            {
                get
                {
                    return this.aT044Field;
                }
                set
                {
                    this.aT044Field = value;
                }
            }



            public string AT046
            {
                get
                {
                    return this.aT046Field;
                }
                set
                {
                    this.aT046Field = value;
                }
            }



            public string AT090
            {
                get
                {
                    return this.aT090Field;
                }
                set
                {
                    this.aT090Field = value;
                }
            }



            public string AT091
            {
                get
                {
                    return this.aT091Field;
                }
                set
                {
                    this.aT091Field = value;
                }
            }



            public string AT092
            {
                get
                {
                    return this.aT092Field;
                }
                set
                {
                    this.aT092Field = value;
                }
            }



            public string AT093
            {
                get
                {
                    return this.aT093Field;
                }
                set
                {
                    this.aT093Field = value;
                }
            }



            public string AT094
            {
                get
                {
                    return this.aT094Field;
                }
                set
                {
                    this.aT094Field = value;
                }
            }



            public string AT097
            {
                get
                {
                    return this.aT097Field;
                }
                set
                {
                    this.aT097Field = value;
                }
            }



            public string AT140
            {
                get
                {
                    return this.aT140Field;
                }
                set
                {
                    this.aT140Field = value;
                }
            }



            public string AT150
            {
                get
                {
                    return this.aT150Field;
                }
                set
                {
                    this.aT150Field = value;
                }
            }



            public string AT153
            {
                get
                {
                    return this.aT153Field;
                }
                set
                {
                    this.aT153Field = value;
                }
            }



            public string AT159
            {
                get
                {
                    return this.aT159Field;
                }
                set
                {
                    this.aT159Field = value;
                }
            }



            public string AT161
            {
                get
                {
                    return this.aT161Field;
                }
                set
                {
                    this.aT161Field = value;
                }
            }



            public string AT179
            {
                get
                {
                    return this.aT179Field;
                }
                set
                {
                    this.aT179Field = value;
                }
            }



            public string G010
            {
                get
                {
                    return this.g010Field;
                }
                set
                {
                    this.g010Field = value;
                }
            }



            public string G012
            {
                get
                {
                    return this.g012Field;
                }
                set
                {
                    this.g012Field = value;
                }
            }



            public string G024
            {
                get
                {
                    return this.g024Field;
                }
                set
                {
                    this.g024Field = value;
                }
            }



            public string G030
            {
                get
                {
                    return this.g030Field;
                }
                set
                {
                    this.g030Field = value;
                }
            }



            public string G032
            {
                get
                {
                    return this.g032Field;
                }
                set
                {
                    this.g032Field = value;
                }
            }



            public string IN158
            {
                get
                {
                    return this.iN158Field;
                }
                set
                {
                    this.iN158Field = value;
                }
            }



            public string IN159
            {
                get
                {
                    return this.iN159Field;
                }
                set
                {
                    this.iN159Field = value;
                }
            }



            public string IN160
            {
                get
                {
                    return this.iN160Field;
                }
                set
                {
                    this.iN160Field = value;
                }
            }



            public string RE001
            {
                get
                {
                    return this.rE001Field;
                }
                set
                {
                    this.rE001Field = value;
                }
            }



            public string RE042
            {
                get
                {
                    return this.rE042Field;
                }
                set
                {
                    this.rE042Field = value;
                }
            }



            public string RE044
            {
                get
                {
                    return this.rE044Field;
                }
                set
                {
                    this.rE044Field = value;
                }
            }



            public string RE107
            {
                get
                {
                    return this.rE107Field;
                }
                set
                {
                    this.rE107Field = value;
                }
            }



            public string RE160
            {
                get
                {
                    return this.rE160Field;
                }
                set
                {
                    this.rE160Field = value;
                }
            }
        }







        public class StandardBatchCharsSB07
        {

            private string consumerNoField;

            private string aT001Field;

            private string aT002Field;

            private string aT022Field;

            private string aT030Field;

            private string aT033Field;

            private string aT039Field;

            private string aT040Field;

            private string aT042Field;

            private string aT044Field;

            private string aT090Field;

            private string aT092Field;

            private string aT093Field;

            private string aT096Field;

            private string aT097Field;

            private string aT098Field;

            private string aT099Field;

            private string aT100Field;

            private string aT102Field;

            private string aT106Field;

            private string aT108Field;

            private string aT109Field;

            private string aT120Field;

            private string aT140Field;

            private string aT154Field;

            private string aT160Field;

            private string aT165Field;

            private string aT179Field;

            private string aT180Field;

            private string g003Field;

            private string g013Field;

            private string g024Field;

            private string g039Field;

            private string g043Field;

            private string iN001Field;

            private string iN021Field;

            private string iN022Field;

            private string iN025Field;

            private string iN026Field;

            private string iN027Field;

            private string iN039Field;

            private string iN075Field;

            private string iN105Field;

            private string iN122Field;

            private string iN158Field;

            private string iN180Field;

            private string oT001Field;

            private string oT002Field;

            private string oT007Field;

            private string oT008Field;

            private string oT022Field;

            private string oT025Field;

            private string oT152Field;

            private string oT153Field;

            private string oT164Field;

            private string oT167Field;

            private string oT180Field;

            private string rE001Field;

            private string rE002Field;

            private string rE007Field;

            private string rE033Field;

            private string rE039Field;

            private string rE042Field;

            private string rE045Field;

            private string rE102Field;

            private string rE103Field;

            private string rE104Field;

            private string rE105Field;

            private string rE120Field;

            private string rE121Field;

            private string rE122Field;

            private string rE140Field;

            private string rE153Field;

            private string rE156Field;

            private string rE159Field;

            private string rE162Field;

            private string rE164Field;

            private string rE167Field;

            private string rE180Field;

            private string aT038Field;

            private string aT007Field;

            private string aT029Field;

            private string aT091Field;

            private string aT153Field;

            private string g002Field;

            private string g010Field;

            private string g030Field;

            private string iN006Field;

            private string iN106Field;

            private string iN153Field;

            private string iN179Field;

            private string oT179Field;

            private string rE008Field;

            private string rE026Field;

            private string rE179Field;

            private string mL179Field;

            private string aT046Field;

            private string aT036Field;

            private string aT037Field;

            private string aT162Field;

            private string aT163Field;

            private string aT164Field;

            private string g014Field;

            private string g044Field;

            private string g057Field;

            private string g016Field;

            private string g017Field;

            private string aT026Field;

            private string mL026Field;

            private string mL035Field;

            private string mL039Field;

            private string mL002Field;

            private string mL090Field;

            private string mL042Field;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string AT001
            {
                get
                {
                    return this.aT001Field;
                }
                set
                {
                    this.aT001Field = value;
                }
            }



            public string AT002
            {
                get
                {
                    return this.aT002Field;
                }
                set
                {
                    this.aT002Field = value;
                }
            }



            public string AT022
            {
                get
                {
                    return this.aT022Field;
                }
                set
                {
                    this.aT022Field = value;
                }
            }



            public string AT030
            {
                get
                {
                    return this.aT030Field;
                }
                set
                {
                    this.aT030Field = value;
                }
            }



            public string AT033
            {
                get
                {
                    return this.aT033Field;
                }
                set
                {
                    this.aT033Field = value;
                }
            }



            public string AT039
            {
                get
                {
                    return this.aT039Field;
                }
                set
                {
                    this.aT039Field = value;
                }
            }



            public string AT040
            {
                get
                {
                    return this.aT040Field;
                }
                set
                {
                    this.aT040Field = value;
                }
            }



            public string AT042
            {
                get
                {
                    return this.aT042Field;
                }
                set
                {
                    this.aT042Field = value;
                }
            }



            public string AT044
            {
                get
                {
                    return this.aT044Field;
                }
                set
                {
                    this.aT044Field = value;
                }
            }



            public string AT090
            {
                get
                {
                    return this.aT090Field;
                }
                set
                {
                    this.aT090Field = value;
                }
            }



            public string AT092
            {
                get
                {
                    return this.aT092Field;
                }
                set
                {
                    this.aT092Field = value;
                }
            }



            public string AT093
            {
                get
                {
                    return this.aT093Field;
                }
                set
                {
                    this.aT093Field = value;
                }
            }



            public string AT096
            {
                get
                {
                    return this.aT096Field;
                }
                set
                {
                    this.aT096Field = value;
                }
            }



            public string AT097
            {
                get
                {
                    return this.aT097Field;
                }
                set
                {
                    this.aT097Field = value;
                }
            }



            public string AT098
            {
                get
                {
                    return this.aT098Field;
                }
                set
                {
                    this.aT098Field = value;
                }
            }



            public string AT099
            {
                get
                {
                    return this.aT099Field;
                }
                set
                {
                    this.aT099Field = value;
                }
            }



            public string AT100
            {
                get
                {
                    return this.aT100Field;
                }
                set
                {
                    this.aT100Field = value;
                }
            }



            public string AT102
            {
                get
                {
                    return this.aT102Field;
                }
                set
                {
                    this.aT102Field = value;
                }
            }



            public string AT106
            {
                get
                {
                    return this.aT106Field;
                }
                set
                {
                    this.aT106Field = value;
                }
            }



            public string AT108
            {
                get
                {
                    return this.aT108Field;
                }
                set
                {
                    this.aT108Field = value;
                }
            }



            public string AT109
            {
                get
                {
                    return this.aT109Field;
                }
                set
                {
                    this.aT109Field = value;
                }
            }



            public string AT120
            {
                get
                {
                    return this.aT120Field;
                }
                set
                {
                    this.aT120Field = value;
                }
            }



            public string AT140
            {
                get
                {
                    return this.aT140Field;
                }
                set
                {
                    this.aT140Field = value;
                }
            }



            public string AT154
            {
                get
                {
                    return this.aT154Field;
                }
                set
                {
                    this.aT154Field = value;
                }
            }



            public string AT160
            {
                get
                {
                    return this.aT160Field;
                }
                set
                {
                    this.aT160Field = value;
                }
            }



            public string AT165
            {
                get
                {
                    return this.aT165Field;
                }
                set
                {
                    this.aT165Field = value;
                }
            }



            public string AT179
            {
                get
                {
                    return this.aT179Field;
                }
                set
                {
                    this.aT179Field = value;
                }
            }



            public string AT180
            {
                get
                {
                    return this.aT180Field;
                }
                set
                {
                    this.aT180Field = value;
                }
            }



            public string G003
            {
                get
                {
                    return this.g003Field;
                }
                set
                {
                    this.g003Field = value;
                }
            }



            public string G013
            {
                get
                {
                    return this.g013Field;
                }
                set
                {
                    this.g013Field = value;
                }
            }



            public string G024
            {
                get
                {
                    return this.g024Field;
                }
                set
                {
                    this.g024Field = value;
                }
            }



            public string G039
            {
                get
                {
                    return this.g039Field;
                }
                set
                {
                    this.g039Field = value;
                }
            }



            public string G043
            {
                get
                {
                    return this.g043Field;
                }
                set
                {
                    this.g043Field = value;
                }
            }



            public string IN001
            {
                get
                {
                    return this.iN001Field;
                }
                set
                {
                    this.iN001Field = value;
                }
            }



            public string IN021
            {
                get
                {
                    return this.iN021Field;
                }
                set
                {
                    this.iN021Field = value;
                }
            }



            public string IN022
            {
                get
                {
                    return this.iN022Field;
                }
                set
                {
                    this.iN022Field = value;
                }
            }



            public string IN025
            {
                get
                {
                    return this.iN025Field;
                }
                set
                {
                    this.iN025Field = value;
                }
            }



            public string IN026
            {
                get
                {
                    return this.iN026Field;
                }
                set
                {
                    this.iN026Field = value;
                }
            }



            public string IN027
            {
                get
                {
                    return this.iN027Field;
                }
                set
                {
                    this.iN027Field = value;
                }
            }



            public string IN039
            {
                get
                {
                    return this.iN039Field;
                }
                set
                {
                    this.iN039Field = value;
                }
            }



            public string IN075
            {
                get
                {
                    return this.iN075Field;
                }
                set
                {
                    this.iN075Field = value;
                }
            }



            public string IN105
            {
                get
                {
                    return this.iN105Field;
                }
                set
                {
                    this.iN105Field = value;
                }
            }



            public string IN122
            {
                get
                {
                    return this.iN122Field;
                }
                set
                {
                    this.iN122Field = value;
                }
            }



            public string IN158
            {
                get
                {
                    return this.iN158Field;
                }
                set
                {
                    this.iN158Field = value;
                }
            }



            public string IN180
            {
                get
                {
                    return this.iN180Field;
                }
                set
                {
                    this.iN180Field = value;
                }
            }



            public string OT001
            {
                get
                {
                    return this.oT001Field;
                }
                set
                {
                    this.oT001Field = value;
                }
            }



            public string OT002
            {
                get
                {
                    return this.oT002Field;
                }
                set
                {
                    this.oT002Field = value;
                }
            }



            public string OT007
            {
                get
                {
                    return this.oT007Field;
                }
                set
                {
                    this.oT007Field = value;
                }
            }



            public string OT008
            {
                get
                {
                    return this.oT008Field;
                }
                set
                {
                    this.oT008Field = value;
                }
            }



            public string OT022
            {
                get
                {
                    return this.oT022Field;
                }
                set
                {
                    this.oT022Field = value;
                }
            }



            public string OT025
            {
                get
                {
                    return this.oT025Field;
                }
                set
                {
                    this.oT025Field = value;
                }
            }



            public string OT152
            {
                get
                {
                    return this.oT152Field;
                }
                set
                {
                    this.oT152Field = value;
                }
            }



            public string OT153
            {
                get
                {
                    return this.oT153Field;
                }
                set
                {
                    this.oT153Field = value;
                }
            }



            public string OT164
            {
                get
                {
                    return this.oT164Field;
                }
                set
                {
                    this.oT164Field = value;
                }
            }



            public string OT167
            {
                get
                {
                    return this.oT167Field;
                }
                set
                {
                    this.oT167Field = value;
                }
            }



            public string OT180
            {
                get
                {
                    return this.oT180Field;
                }
                set
                {
                    this.oT180Field = value;
                }
            }



            public string RE001
            {
                get
                {
                    return this.rE001Field;
                }
                set
                {
                    this.rE001Field = value;
                }
            }



            public string RE002
            {
                get
                {
                    return this.rE002Field;
                }
                set
                {
                    this.rE002Field = value;
                }
            }



            public string RE007
            {
                get
                {
                    return this.rE007Field;
                }
                set
                {
                    this.rE007Field = value;
                }
            }



            public string RE033
            {
                get
                {
                    return this.rE033Field;
                }
                set
                {
                    this.rE033Field = value;
                }
            }



            public string RE039
            {
                get
                {
                    return this.rE039Field;
                }
                set
                {
                    this.rE039Field = value;
                }
            }



            public string RE042
            {
                get
                {
                    return this.rE042Field;
                }
                set
                {
                    this.rE042Field = value;
                }
            }



            public string RE045
            {
                get
                {
                    return this.rE045Field;
                }
                set
                {
                    this.rE045Field = value;
                }
            }



            public string RE102
            {
                get
                {
                    return this.rE102Field;
                }
                set
                {
                    this.rE102Field = value;
                }
            }



            public string RE103
            {
                get
                {
                    return this.rE103Field;
                }
                set
                {
                    this.rE103Field = value;
                }
            }



            public string RE104
            {
                get
                {
                    return this.rE104Field;
                }
                set
                {
                    this.rE104Field = value;
                }
            }



            public string RE105
            {
                get
                {
                    return this.rE105Field;
                }
                set
                {
                    this.rE105Field = value;
                }
            }



            public string RE120
            {
                get
                {
                    return this.rE120Field;
                }
                set
                {
                    this.rE120Field = value;
                }
            }



            public string RE121
            {
                get
                {
                    return this.rE121Field;
                }
                set
                {
                    this.rE121Field = value;
                }
            }



            public string RE122
            {
                get
                {
                    return this.rE122Field;
                }
                set
                {
                    this.rE122Field = value;
                }
            }



            public string RE140
            {
                get
                {
                    return this.rE140Field;
                }
                set
                {
                    this.rE140Field = value;
                }
            }



            public string RE153
            {
                get
                {
                    return this.rE153Field;
                }
                set
                {
                    this.rE153Field = value;
                }
            }



            public string RE156
            {
                get
                {
                    return this.rE156Field;
                }
                set
                {
                    this.rE156Field = value;
                }
            }



            public string RE159
            {
                get
                {
                    return this.rE159Field;
                }
                set
                {
                    this.rE159Field = value;
                }
            }



            public string RE162
            {
                get
                {
                    return this.rE162Field;
                }
                set
                {
                    this.rE162Field = value;
                }
            }



            public string RE164
            {
                get
                {
                    return this.rE164Field;
                }
                set
                {
                    this.rE164Field = value;
                }
            }



            public string RE167
            {
                get
                {
                    return this.rE167Field;
                }
                set
                {
                    this.rE167Field = value;
                }
            }



            public string RE180
            {
                get
                {
                    return this.rE180Field;
                }
                set
                {
                    this.rE180Field = value;
                }
            }



            public string AT038
            {
                get
                {
                    return this.aT038Field;
                }
                set
                {
                    this.aT038Field = value;
                }
            }



            public string AT007
            {
                get
                {
                    return this.aT007Field;
                }
                set
                {
                    this.aT007Field = value;
                }
            }



            public string AT029
            {
                get
                {
                    return this.aT029Field;
                }
                set
                {
                    this.aT029Field = value;
                }
            }



            public string AT091
            {
                get
                {
                    return this.aT091Field;
                }
                set
                {
                    this.aT091Field = value;
                }
            }



            public string AT153
            {
                get
                {
                    return this.aT153Field;
                }
                set
                {
                    this.aT153Field = value;
                }
            }



            public string G002
            {
                get
                {
                    return this.g002Field;
                }
                set
                {
                    this.g002Field = value;
                }
            }



            public string G010
            {
                get
                {
                    return this.g010Field;
                }
                set
                {
                    this.g010Field = value;
                }
            }



            public string G030
            {
                get
                {
                    return this.g030Field;
                }
                set
                {
                    this.g030Field = value;
                }
            }



            public string IN006
            {
                get
                {
                    return this.iN006Field;
                }
                set
                {
                    this.iN006Field = value;
                }
            }



            public string IN106
            {
                get
                {
                    return this.iN106Field;
                }
                set
                {
                    this.iN106Field = value;
                }
            }



            public string IN153
            {
                get
                {
                    return this.iN153Field;
                }
                set
                {
                    this.iN153Field = value;
                }
            }



            public string IN179
            {
                get
                {
                    return this.iN179Field;
                }
                set
                {
                    this.iN179Field = value;
                }
            }



            public string OT179
            {
                get
                {
                    return this.oT179Field;
                }
                set
                {
                    this.oT179Field = value;
                }
            }



            public string RE008
            {
                get
                {
                    return this.rE008Field;
                }
                set
                {
                    this.rE008Field = value;
                }
            }



            public string RE026
            {
                get
                {
                    return this.rE026Field;
                }
                set
                {
                    this.rE026Field = value;
                }
            }



            public string RE179
            {
                get
                {
                    return this.rE179Field;
                }
                set
                {
                    this.rE179Field = value;
                }
            }



            public string ML179
            {
                get
                {
                    return this.mL179Field;
                }
                set
                {
                    this.mL179Field = value;
                }
            }



            public string AT046
            {
                get
                {
                    return this.aT046Field;
                }
                set
                {
                    this.aT046Field = value;
                }
            }



            public string AT036
            {
                get
                {
                    return this.aT036Field;
                }
                set
                {
                    this.aT036Field = value;
                }
            }



            public string AT037
            {
                get
                {
                    return this.aT037Field;
                }
                set
                {
                    this.aT037Field = value;
                }
            }



            public string AT162
            {
                get
                {
                    return this.aT162Field;
                }
                set
                {
                    this.aT162Field = value;
                }
            }



            public string AT163
            {
                get
                {
                    return this.aT163Field;
                }
                set
                {
                    this.aT163Field = value;
                }
            }



            public string AT164
            {
                get
                {
                    return this.aT164Field;
                }
                set
                {
                    this.aT164Field = value;
                }
            }



            public string G014
            {
                get
                {
                    return this.g014Field;
                }
                set
                {
                    this.g014Field = value;
                }
            }



            public string G044
            {
                get
                {
                    return this.g044Field;
                }
                set
                {
                    this.g044Field = value;
                }
            }



            public string G057
            {
                get
                {
                    return this.g057Field;
                }
                set
                {
                    this.g057Field = value;
                }
            }



            public string G016
            {
                get
                {
                    return this.g016Field;
                }
                set
                {
                    this.g016Field = value;
                }
            }



            public string G017
            {
                get
                {
                    return this.g017Field;
                }
                set
                {
                    this.g017Field = value;
                }
            }



            public string AT026
            {
                get
                {
                    return this.aT026Field;
                }
                set
                {
                    this.aT026Field = value;
                }
            }



            public string ML026
            {
                get
                {
                    return this.mL026Field;
                }
                set
                {
                    this.mL026Field = value;
                }
            }



            public string ML035
            {
                get
                {
                    return this.mL035Field;
                }
                set
                {
                    this.mL035Field = value;
                }
            }



            public string ML039
            {
                get
                {
                    return this.mL039Field;
                }
                set
                {
                    this.mL039Field = value;
                }
            }



            public string ML002
            {
                get
                {
                    return this.mL002Field;
                }
                set
                {
                    this.mL002Field = value;
                }
            }



            public string ML090
            {
                get
                {
                    return this.mL090Field;
                }
                set
                {
                    this.mL090Field = value;
                }
            }



            public string ML042
            {
                get
                {
                    return this.mL042Field;
                }
                set
                {
                    this.mL042Field = value;
                }
            }
        }







        public class StandardBatchCharsSB13
        {

            private string consumerNoField;

            private string aT002Field;

            private string aT006Field;

            private string aT021Field;

            private string aT026Field;

            private string aT066Field;

            private string aT105Field;

            private string aT107Field;

            private string aT110Field;

            private string aT164Field;

            private string g002Field;

            private string g003Field;

            private string g030Field;

            private string g031Field;

            private string g043Field;

            private string iN021Field;

            private string iN025Field;

            private string iN102Field;

            private string mL006Field;

            private string mL022Field;

            private string mL041Field;

            private string mL091Field;

            private string mL097Field;

            private string mL102Field;

            private string mL108Field;

            private string oT006Field;

            private string oT025Field;

            private string oT026Field;

            private string oT106Field;

            private string oT153Field;

            private string rE006Field;

            private string rE033Field;

            private string rE101Field;

            private string rE103Field;

            private string rE153Field;

            private string rE160Field;

            private string aT029Field;

            private string aT160Field;

            private string aT179Field;

            private string g021Field;

            private string mL039Field;

            private string mL040Field;

            private string mL179Field;

            private string aT140Field;

            private string g012Field;

            private string g022Field;

            private string g032Field;

            private string g044Field;

            private string g041Field;

            private string mL002Field;

            private string mL165Field;

            private string rE001Field;

            private string aT025Field;

            private string aT042Field;

            private string aT090Field;

            private string aT093Field;

            private string aT109Field;

            private string aT111Field;

            private string aT120Field;

            private string aT122Field;

            private string aT123Field;

            private string aT152Field;

            private string aT155Field;

            private string iN140Field;

            private string mL090Field;

            private string mL092Field;

            private string oT041Field;

            private string aT108Field;

            private string aT150Field;

            private string iN105Field;

            private string mL007Field;

            private string oT159Field;

            private string rE120Field;

            private string aT092Field;

            private string iN153Field;

            private string mL042Field;

            private string oT158Field;

            private string mL151Field;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string AT002
            {
                get
                {
                    return this.aT002Field;
                }
                set
                {
                    this.aT002Field = value;
                }
            }



            public string AT006
            {
                get
                {
                    return this.aT006Field;
                }
                set
                {
                    this.aT006Field = value;
                }
            }



            public string AT021
            {
                get
                {
                    return this.aT021Field;
                }
                set
                {
                    this.aT021Field = value;
                }
            }



            public string AT026
            {
                get
                {
                    return this.aT026Field;
                }
                set
                {
                    this.aT026Field = value;
                }
            }



            public string AT066
            {
                get
                {
                    return this.aT066Field;
                }
                set
                {
                    this.aT066Field = value;
                }
            }



            public string AT105
            {
                get
                {
                    return this.aT105Field;
                }
                set
                {
                    this.aT105Field = value;
                }
            }



            public string AT107
            {
                get
                {
                    return this.aT107Field;
                }
                set
                {
                    this.aT107Field = value;
                }
            }



            public string AT110
            {
                get
                {
                    return this.aT110Field;
                }
                set
                {
                    this.aT110Field = value;
                }
            }



            public string AT164
            {
                get
                {
                    return this.aT164Field;
                }
                set
                {
                    this.aT164Field = value;
                }
            }



            public string G002
            {
                get
                {
                    return this.g002Field;
                }
                set
                {
                    this.g002Field = value;
                }
            }



            public string G003
            {
                get
                {
                    return this.g003Field;
                }
                set
                {
                    this.g003Field = value;
                }
            }



            public string G030
            {
                get
                {
                    return this.g030Field;
                }
                set
                {
                    this.g030Field = value;
                }
            }



            public string G031
            {
                get
                {
                    return this.g031Field;
                }
                set
                {
                    this.g031Field = value;
                }
            }



            public string G043
            {
                get
                {
                    return this.g043Field;
                }
                set
                {
                    this.g043Field = value;
                }
            }



            public string IN021
            {
                get
                {
                    return this.iN021Field;
                }
                set
                {
                    this.iN021Field = value;
                }
            }



            public string IN025
            {
                get
                {
                    return this.iN025Field;
                }
                set
                {
                    this.iN025Field = value;
                }
            }



            public string IN102
            {
                get
                {
                    return this.iN102Field;
                }
                set
                {
                    this.iN102Field = value;
                }
            }



            public string ML006
            {
                get
                {
                    return this.mL006Field;
                }
                set
                {
                    this.mL006Field = value;
                }
            }



            public string ML022
            {
                get
                {
                    return this.mL022Field;
                }
                set
                {
                    this.mL022Field = value;
                }
            }



            public string ML041
            {
                get
                {
                    return this.mL041Field;
                }
                set
                {
                    this.mL041Field = value;
                }
            }



            public string ML091
            {
                get
                {
                    return this.mL091Field;
                }
                set
                {
                    this.mL091Field = value;
                }
            }



            public string ML097
            {
                get
                {
                    return this.mL097Field;
                }
                set
                {
                    this.mL097Field = value;
                }
            }



            public string ML102
            {
                get
                {
                    return this.mL102Field;
                }
                set
                {
                    this.mL102Field = value;
                }
            }



            public string ML108
            {
                get
                {
                    return this.mL108Field;
                }
                set
                {
                    this.mL108Field = value;
                }
            }



            public string OT006
            {
                get
                {
                    return this.oT006Field;
                }
                set
                {
                    this.oT006Field = value;
                }
            }



            public string OT025
            {
                get
                {
                    return this.oT025Field;
                }
                set
                {
                    this.oT025Field = value;
                }
            }



            public string OT026
            {
                get
                {
                    return this.oT026Field;
                }
                set
                {
                    this.oT026Field = value;
                }
            }



            public string OT106
            {
                get
                {
                    return this.oT106Field;
                }
                set
                {
                    this.oT106Field = value;
                }
            }



            public string OT153
            {
                get
                {
                    return this.oT153Field;
                }
                set
                {
                    this.oT153Field = value;
                }
            }



            public string RE006
            {
                get
                {
                    return this.rE006Field;
                }
                set
                {
                    this.rE006Field = value;
                }
            }



            public string RE033
            {
                get
                {
                    return this.rE033Field;
                }
                set
                {
                    this.rE033Field = value;
                }
            }



            public string RE101
            {
                get
                {
                    return this.rE101Field;
                }
                set
                {
                    this.rE101Field = value;
                }
            }



            public string RE103
            {
                get
                {
                    return this.rE103Field;
                }
                set
                {
                    this.rE103Field = value;
                }
            }



            public string RE153
            {
                get
                {
                    return this.rE153Field;
                }
                set
                {
                    this.rE153Field = value;
                }
            }



            public string RE160
            {
                get
                {
                    return this.rE160Field;
                }
                set
                {
                    this.rE160Field = value;
                }
            }



            public string AT029
            {
                get
                {
                    return this.aT029Field;
                }
                set
                {
                    this.aT029Field = value;
                }
            }



            public string AT160
            {
                get
                {
                    return this.aT160Field;
                }
                set
                {
                    this.aT160Field = value;
                }
            }



            public string AT179
            {
                get
                {
                    return this.aT179Field;
                }
                set
                {
                    this.aT179Field = value;
                }
            }



            public string G021
            {
                get
                {
                    return this.g021Field;
                }
                set
                {
                    this.g021Field = value;
                }
            }



            public string ML039
            {
                get
                {
                    return this.mL039Field;
                }
                set
                {
                    this.mL039Field = value;
                }
            }



            public string ML040
            {
                get
                {
                    return this.mL040Field;
                }
                set
                {
                    this.mL040Field = value;
                }
            }



            public string ML179
            {
                get
                {
                    return this.mL179Field;
                }
                set
                {
                    this.mL179Field = value;
                }
            }



            public string AT140
            {
                get
                {
                    return this.aT140Field;
                }
                set
                {
                    this.aT140Field = value;
                }
            }



            public string G012
            {
                get
                {
                    return this.g012Field;
                }
                set
                {
                    this.g012Field = value;
                }
            }



            public string G022
            {
                get
                {
                    return this.g022Field;
                }
                set
                {
                    this.g022Field = value;
                }
            }



            public string G032
            {
                get
                {
                    return this.g032Field;
                }
                set
                {
                    this.g032Field = value;
                }
            }



            public string G044
            {
                get
                {
                    return this.g044Field;
                }
                set
                {
                    this.g044Field = value;
                }
            }



            public string G041
            {
                get
                {
                    return this.g041Field;
                }
                set
                {
                    this.g041Field = value;
                }
            }



            public string ML002
            {
                get
                {
                    return this.mL002Field;
                }
                set
                {
                    this.mL002Field = value;
                }
            }



            public string ML165
            {
                get
                {
                    return this.mL165Field;
                }
                set
                {
                    this.mL165Field = value;
                }
            }



            public string RE001
            {
                get
                {
                    return this.rE001Field;
                }
                set
                {
                    this.rE001Field = value;
                }
            }



            public string AT025
            {
                get
                {
                    return this.aT025Field;
                }
                set
                {
                    this.aT025Field = value;
                }
            }



            public string AT042
            {
                get
                {
                    return this.aT042Field;
                }
                set
                {
                    this.aT042Field = value;
                }
            }



            public string AT090
            {
                get
                {
                    return this.aT090Field;
                }
                set
                {
                    this.aT090Field = value;
                }
            }



            public string AT093
            {
                get
                {
                    return this.aT093Field;
                }
                set
                {
                    this.aT093Field = value;
                }
            }



            public string AT109
            {
                get
                {
                    return this.aT109Field;
                }
                set
                {
                    this.aT109Field = value;
                }
            }



            public string AT111
            {
                get
                {
                    return this.aT111Field;
                }
                set
                {
                    this.aT111Field = value;
                }
            }



            public string AT120
            {
                get
                {
                    return this.aT120Field;
                }
                set
                {
                    this.aT120Field = value;
                }
            }



            public string AT122
            {
                get
                {
                    return this.aT122Field;
                }
                set
                {
                    this.aT122Field = value;
                }
            }



            public string AT123
            {
                get
                {
                    return this.aT123Field;
                }
                set
                {
                    this.aT123Field = value;
                }
            }



            public string AT152
            {
                get
                {
                    return this.aT152Field;
                }
                set
                {
                    this.aT152Field = value;
                }
            }



            public string AT155
            {
                get
                {
                    return this.aT155Field;
                }
                set
                {
                    this.aT155Field = value;
                }
            }



            public string IN140
            {
                get
                {
                    return this.iN140Field;
                }
                set
                {
                    this.iN140Field = value;
                }
            }



            public string ML090
            {
                get
                {
                    return this.mL090Field;
                }
                set
                {
                    this.mL090Field = value;
                }
            }



            public string ML092
            {
                get
                {
                    return this.mL092Field;
                }
                set
                {
                    this.mL092Field = value;
                }
            }



            public string OT041
            {
                get
                {
                    return this.oT041Field;
                }
                set
                {
                    this.oT041Field = value;
                }
            }



            public string AT108
            {
                get
                {
                    return this.aT108Field;
                }
                set
                {
                    this.aT108Field = value;
                }
            }



            public string AT150
            {
                get
                {
                    return this.aT150Field;
                }
                set
                {
                    this.aT150Field = value;
                }
            }



            public string IN105
            {
                get
                {
                    return this.iN105Field;
                }
                set
                {
                    this.iN105Field = value;
                }
            }



            public string ML007
            {
                get
                {
                    return this.mL007Field;
                }
                set
                {
                    this.mL007Field = value;
                }
            }



            public string OT159
            {
                get
                {
                    return this.oT159Field;
                }
                set
                {
                    this.oT159Field = value;
                }
            }



            public string RE120
            {
                get
                {
                    return this.rE120Field;
                }
                set
                {
                    this.rE120Field = value;
                }
            }



            public string AT092
            {
                get
                {
                    return this.aT092Field;
                }
                set
                {
                    this.aT092Field = value;
                }
            }



            public string IN153
            {
                get
                {
                    return this.iN153Field;
                }
                set
                {
                    this.iN153Field = value;
                }
            }



            public string ML042
            {
                get
                {
                    return this.mL042Field;
                }
                set
                {
                    this.mL042Field = value;
                }
            }



            public string OT158
            {
                get
                {
                    return this.oT158Field;
                }
                set
                {
                    this.oT158Field = value;
                }
            }



            public string ML151
            {
                get
                {
                    return this.mL151Field;
                }
                set
                {
                    this.mL151Field = value;
                }
            }
        }







        public class StandardBatchCharsSB18
        {

            private string consumerNoField;

            private string aT002Field;

            private string aT036Field;

            private string aT037Field;

            private string aT038Field;

            private string aT039Field;

            private string aT040Field;

            private string aT162Field;

            private string aT163Field;

            private string aT164Field;

            private string g010Field;

            private string g014Field;

            private string g030Field;

            private string g043Field;

            private string g044Field;

            private string g057Field;

            private string mL036Field;

            private string mL037Field;

            private string mL038Field;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string AT002
            {
                get
                {
                    return this.aT002Field;
                }
                set
                {
                    this.aT002Field = value;
                }
            }



            public string AT036
            {
                get
                {
                    return this.aT036Field;
                }
                set
                {
                    this.aT036Field = value;
                }
            }



            public string AT037
            {
                get
                {
                    return this.aT037Field;
                }
                set
                {
                    this.aT037Field = value;
                }
            }



            public string AT038
            {
                get
                {
                    return this.aT038Field;
                }
                set
                {
                    this.aT038Field = value;
                }
            }



            public string AT039
            {
                get
                {
                    return this.aT039Field;
                }
                set
                {
                    this.aT039Field = value;
                }
            }



            public string AT040
            {
                get
                {
                    return this.aT040Field;
                }
                set
                {
                    this.aT040Field = value;
                }
            }



            public string AT162
            {
                get
                {
                    return this.aT162Field;
                }
                set
                {
                    this.aT162Field = value;
                }
            }



            public string AT163
            {
                get
                {
                    return this.aT163Field;
                }
                set
                {
                    this.aT163Field = value;
                }
            }



            public string AT164
            {
                get
                {
                    return this.aT164Field;
                }
                set
                {
                    this.aT164Field = value;
                }
            }



            public string G010
            {
                get
                {
                    return this.g010Field;
                }
                set
                {
                    this.g010Field = value;
                }
            }



            public string G014
            {
                get
                {
                    return this.g014Field;
                }
                set
                {
                    this.g014Field = value;
                }
            }



            public string G030
            {
                get
                {
                    return this.g030Field;
                }
                set
                {
                    this.g030Field = value;
                }
            }



            public string G043
            {
                get
                {
                    return this.g043Field;
                }
                set
                {
                    this.g043Field = value;
                }
            }



            public string G044
            {
                get
                {
                    return this.g044Field;
                }
                set
                {
                    this.g044Field = value;
                }
            }



            public string G057
            {
                get
                {
                    return this.g057Field;
                }
                set
                {
                    this.g057Field = value;
                }
            }



            public string ML036
            {
                get
                {
                    return this.mL036Field;
                }
                set
                {
                    this.mL036Field = value;
                }
            }



            public string ML037
            {
                get
                {
                    return this.mL037Field;
                }
                set
                {
                    this.mL037Field = value;
                }
            }



            public string ML038
            {
                get
                {
                    return this.mL038Field;
                }
                set
                {
                    this.mL038Field = value;
                }
            }
        }







        public class StandardBatchCharsSB19
        {

            private string consumerNoField;

            private string aT094Field;

            private string aT107Field;

            private string aT164Field;

            private string aT179Field;

            private string g010Field;

            private string g030Field;

            private string g032Field;

            private string g044Field;

            private string iN106Field;

            private string iN141Field;

            private string mL179Field;

            private string oT041Field;

            private string oT140Field;

            private string rE006Field;

            private string rE105Field;

            private string rE140Field;

            private string rE159Field;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string AT094
            {
                get
                {
                    return this.aT094Field;
                }
                set
                {
                    this.aT094Field = value;
                }
            }



            public string AT107
            {
                get
                {
                    return this.aT107Field;
                }
                set
                {
                    this.aT107Field = value;
                }
            }



            public string AT164
            {
                get
                {
                    return this.aT164Field;
                }
                set
                {
                    this.aT164Field = value;
                }
            }



            public string AT179
            {
                get
                {
                    return this.aT179Field;
                }
                set
                {
                    this.aT179Field = value;
                }
            }



            public string G010
            {
                get
                {
                    return this.g010Field;
                }
                set
                {
                    this.g010Field = value;
                }
            }



            public string G030
            {
                get
                {
                    return this.g030Field;
                }
                set
                {
                    this.g030Field = value;
                }
            }



            public string G032
            {
                get
                {
                    return this.g032Field;
                }
                set
                {
                    this.g032Field = value;
                }
            }



            public string G044
            {
                get
                {
                    return this.g044Field;
                }
                set
                {
                    this.g044Field = value;
                }
            }



            public string IN106
            {
                get
                {
                    return this.iN106Field;
                }
                set
                {
                    this.iN106Field = value;
                }
            }



            public string IN141
            {
                get
                {
                    return this.iN141Field;
                }
                set
                {
                    this.iN141Field = value;
                }
            }



            public string ML179
            {
                get
                {
                    return this.mL179Field;
                }
                set
                {
                    this.mL179Field = value;
                }
            }



            public string OT041
            {
                get
                {
                    return this.oT041Field;
                }
                set
                {
                    this.oT041Field = value;
                }
            }



            public string OT140
            {
                get
                {
                    return this.oT140Field;
                }
                set
                {
                    this.oT140Field = value;
                }
            }



            public string RE006
            {
                get
                {
                    return this.rE006Field;
                }
                set
                {
                    this.rE006Field = value;
                }
            }



            public string RE105
            {
                get
                {
                    return this.rE105Field;
                }
                set
                {
                    this.rE105Field = value;
                }
            }



            public string RE140
            {
                get
                {
                    return this.rE140Field;
                }
                set
                {
                    this.rE140Field = value;
                }
            }



            public string RE159
            {
                get
                {
                    return this.rE159Field;
                }
                set
                {
                    this.rE159Field = value;
                }
            }
        }







        public class StandardBatchCharsSB25
        {

            private string consumerNoField;

            private string aT021Field;

            private string aT022Field;

            private string aT043Field;

            private string aT044Field;

            private string aT045Field;

            private string aT046Field;

            private string aT090Field;

            private string aT091Field;

            private string aT092Field;

            private string aT093Field;

            private string aT096Field;

            private string aT097Field;

            private string aT104Field;

            private string aT106Field;

            private string aT108Field;

            private string aT109Field;

            private string aT111Field;

            private string aT120Field;

            private string aT123Field;

            private string aT140Field;

            private string aT150Field;

            private string aT159Field;

            private string aT160Field;

            private string aT161Field;

            private string aT164Field;

            private string g010Field;

            private string g030Field;

            private string mL001Field;

            private string mL003Field;

            private string mL151Field;

            private string rE104Field;

            private string rE105Field;

            private string rE120Field;

            private string rE140Field;

            private string rE164Field;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string AT021
            {
                get
                {
                    return this.aT021Field;
                }
                set
                {
                    this.aT021Field = value;
                }
            }



            public string AT022
            {
                get
                {
                    return this.aT022Field;
                }
                set
                {
                    this.aT022Field = value;
                }
            }



            public string AT043
            {
                get
                {
                    return this.aT043Field;
                }
                set
                {
                    this.aT043Field = value;
                }
            }



            public string AT044
            {
                get
                {
                    return this.aT044Field;
                }
                set
                {
                    this.aT044Field = value;
                }
            }



            public string AT045
            {
                get
                {
                    return this.aT045Field;
                }
                set
                {
                    this.aT045Field = value;
                }
            }



            public string AT046
            {
                get
                {
                    return this.aT046Field;
                }
                set
                {
                    this.aT046Field = value;
                }
            }



            public string AT090
            {
                get
                {
                    return this.aT090Field;
                }
                set
                {
                    this.aT090Field = value;
                }
            }



            public string AT091
            {
                get
                {
                    return this.aT091Field;
                }
                set
                {
                    this.aT091Field = value;
                }
            }



            public string AT092
            {
                get
                {
                    return this.aT092Field;
                }
                set
                {
                    this.aT092Field = value;
                }
            }



            public string AT093
            {
                get
                {
                    return this.aT093Field;
                }
                set
                {
                    this.aT093Field = value;
                }
            }



            public string AT096
            {
                get
                {
                    return this.aT096Field;
                }
                set
                {
                    this.aT096Field = value;
                }
            }



            public string AT097
            {
                get
                {
                    return this.aT097Field;
                }
                set
                {
                    this.aT097Field = value;
                }
            }



            public string AT104
            {
                get
                {
                    return this.aT104Field;
                }
                set
                {
                    this.aT104Field = value;
                }
            }



            public string AT106
            {
                get
                {
                    return this.aT106Field;
                }
                set
                {
                    this.aT106Field = value;
                }
            }



            public string AT108
            {
                get
                {
                    return this.aT108Field;
                }
                set
                {
                    this.aT108Field = value;
                }
            }



            public string AT109
            {
                get
                {
                    return this.aT109Field;
                }
                set
                {
                    this.aT109Field = value;
                }
            }



            public string AT111
            {
                get
                {
                    return this.aT111Field;
                }
                set
                {
                    this.aT111Field = value;
                }
            }



            public string AT120
            {
                get
                {
                    return this.aT120Field;
                }
                set
                {
                    this.aT120Field = value;
                }
            }



            public string AT123
            {
                get
                {
                    return this.aT123Field;
                }
                set
                {
                    this.aT123Field = value;
                }
            }



            public string AT140
            {
                get
                {
                    return this.aT140Field;
                }
                set
                {
                    this.aT140Field = value;
                }
            }



            public string AT150
            {
                get
                {
                    return this.aT150Field;
                }
                set
                {
                    this.aT150Field = value;
                }
            }



            public string AT159
            {
                get
                {
                    return this.aT159Field;
                }
                set
                {
                    this.aT159Field = value;
                }
            }



            public string AT160
            {
                get
                {
                    return this.aT160Field;
                }
                set
                {
                    this.aT160Field = value;
                }
            }



            public string AT161
            {
                get
                {
                    return this.aT161Field;
                }
                set
                {
                    this.aT161Field = value;
                }
            }



            public string AT164
            {
                get
                {
                    return this.aT164Field;
                }
                set
                {
                    this.aT164Field = value;
                }
            }



            public string G010
            {
                get
                {
                    return this.g010Field;
                }
                set
                {
                    this.g010Field = value;
                }
            }



            public string G030
            {
                get
                {
                    return this.g030Field;
                }
                set
                {
                    this.g030Field = value;
                }
            }



            public string ML001
            {
                get
                {
                    return this.mL001Field;
                }
                set
                {
                    this.mL001Field = value;
                }
            }



            public string ML003
            {
                get
                {
                    return this.mL003Field;
                }
                set
                {
                    this.mL003Field = value;
                }
            }



            public string ML151
            {
                get
                {
                    return this.mL151Field;
                }
                set
                {
                    this.mL151Field = value;
                }
            }



            public string RE104
            {
                get
                {
                    return this.rE104Field;
                }
                set
                {
                    this.rE104Field = value;
                }
            }



            public string RE105
            {
                get
                {
                    return this.rE105Field;
                }
                set
                {
                    this.rE105Field = value;
                }
            }



            public string RE120
            {
                get
                {
                    return this.rE120Field;
                }
                set
                {
                    this.rE120Field = value;
                }
            }



            public string RE140
            {
                get
                {
                    return this.rE140Field;
                }
                set
                {
                    this.rE140Field = value;
                }
            }



            public string RE164
            {
                get
                {
                    return this.rE164Field;
                }
                set
                {
                    this.rE164Field = value;
                }
            }
        }







        public class StandardBatchCharsSB27
        {

            private string consumerNoField;

            private string aT001Field;

            private string aT002Field;

            private string aT022Field;

            private string aT030Field;

            private string aT033Field;

            private string aT039Field;

            private string aT040Field;

            private string aT042Field;

            private string aT044Field;

            private string aT090Field;

            private string aT092Field;

            private string aT093Field;

            private string aT096Field;

            private string aT097Field;

            private string aT098Field;

            private string aT099Field;

            private string aT100Field;

            private string aT102Field;

            private string aT106Field;

            private string aT108Field;

            private string aT109Field;

            private string aT120Field;

            private string aT140Field;

            private string aT154Field;

            private string aT160Field;

            private string aT165Field;

            private string aT179Field;

            private string aT180Field;

            private string g003Field;

            private string g013Field;

            private string g024Field;

            private string g039Field;

            private string g043Field;

            private string iN001Field;

            private string iN021Field;

            private string iN022Field;

            private string iN025Field;

            private string iN026Field;

            private string iN027Field;

            private string iN039Field;

            private string iN075Field;

            private string iN105Field;

            private string iN122Field;

            private string iN158Field;

            private string iN180Field;

            private string oT001Field;

            private string oT002Field;

            private string oT007Field;

            private string oT008Field;

            private string oT022Field;

            private string oT025Field;

            private string oT152Field;

            private string oT153Field;

            private string oT164Field;

            private string oT167Field;

            private string oT180Field;

            private string rE001Field;

            private string rE002Field;

            private string rE007Field;

            private string rE033Field;

            private string rE039Field;

            private string rE042Field;

            private string rE045Field;

            private string rE102Field;

            private string rE103Field;

            private string rE104Field;

            private string rE105Field;

            private string rE120Field;

            private string rE121Field;

            private string rE122Field;

            private string rE140Field;

            private string rE153Field;

            private string rE156Field;

            private string rE159Field;

            private string rE162Field;

            private string rE164Field;

            private string rE167Field;

            private string rE180Field;

            private string aT038Field;

            private string aT007Field;

            private string aT029Field;

            private string aT091Field;

            private string aT153Field;

            private string g002Field;

            private string g010Field;

            private string g030Field;

            private string iN006Field;

            private string iN106Field;

            private string iN153Field;

            private string iN179Field;

            private string oT179Field;

            private string rE008Field;

            private string rE026Field;

            private string rE179Field;

            private string mL179Field;

            private string aT046Field;

            private string aT036Field;

            private string aT037Field;

            private string aT162Field;

            private string aT163Field;

            private string aT164Field;

            private string g014Field;

            private string g044Field;

            private string g057Field;

            private string g016Field;

            private string g017Field;

            private string aT026Field;

            private string mL026Field;

            private string mL035Field;

            private string mL039Field;

            private string mL002Field;

            private string mL090Field;

            private string mL042Field;

            private string aT021Field;

            private string mL021Field;

            private string g041Field;

            private string iN159Field;

            private string mL044Field;

            private string aT025Field;

            private string aT034Field;

            private string aT035Field;

            private string aT155Field;

            private string aT159Field;

            private string g022Field;

            private string g056Field;

            private string iN121Field;

            private string mL025Field;

            private string mL034Field;

            private string mL036Field;

            private string mL037Field;

            private string mL038Field;

            private string mL040Field;

            private string mL091Field;

            private string mL121Field;

            private string mL153Field;

            private string mL162Field;

            private string mL163Field;

            private string mL181Field;

            private string oT122Field;

            private string oT154Field;

            private string aT043Field;

            private string iN107Field;

            private string mL041Field;

            private string mL092Field;

            private string mL097Field;

            private string mL111Field;

            private string g011Field;

            private string g048Field;

            private string aT124Field;

            private string mL001Field;

            private string mL043Field;

            private string mL104Field;

            private string mL140Field;

            private string oT042Field;

            private string aT028Field;

            private string g021Field;

            private string aT152Field;

            private string aT161Field;

            private string iN162Field;

            private string oT026Field;

            private string oT106Field;

            private string oT121Field;

            private string oT162Field;

            private string g052Field;

            private string g054Field;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string AT001
            {
                get
                {
                    return this.aT001Field;
                }
                set
                {
                    this.aT001Field = value;
                }
            }



            public string AT002
            {
                get
                {
                    return this.aT002Field;
                }
                set
                {
                    this.aT002Field = value;
                }
            }



            public string AT022
            {
                get
                {
                    return this.aT022Field;
                }
                set
                {
                    this.aT022Field = value;
                }
            }



            public string AT030
            {
                get
                {
                    return this.aT030Field;
                }
                set
                {
                    this.aT030Field = value;
                }
            }



            public string AT033
            {
                get
                {
                    return this.aT033Field;
                }
                set
                {
                    this.aT033Field = value;
                }
            }



            public string AT039
            {
                get
                {
                    return this.aT039Field;
                }
                set
                {
                    this.aT039Field = value;
                }
            }



            public string AT040
            {
                get
                {
                    return this.aT040Field;
                }
                set
                {
                    this.aT040Field = value;
                }
            }



            public string AT042
            {
                get
                {
                    return this.aT042Field;
                }
                set
                {
                    this.aT042Field = value;
                }
            }



            public string AT044
            {
                get
                {
                    return this.aT044Field;
                }
                set
                {
                    this.aT044Field = value;
                }
            }



            public string AT090
            {
                get
                {
                    return this.aT090Field;
                }
                set
                {
                    this.aT090Field = value;
                }
            }



            public string AT092
            {
                get
                {
                    return this.aT092Field;
                }
                set
                {
                    this.aT092Field = value;
                }
            }



            public string AT093
            {
                get
                {
                    return this.aT093Field;
                }
                set
                {
                    this.aT093Field = value;
                }
            }



            public string AT096
            {
                get
                {
                    return this.aT096Field;
                }
                set
                {
                    this.aT096Field = value;
                }
            }



            public string AT097
            {
                get
                {
                    return this.aT097Field;
                }
                set
                {
                    this.aT097Field = value;
                }
            }



            public string AT098
            {
                get
                {
                    return this.aT098Field;
                }
                set
                {
                    this.aT098Field = value;
                }
            }



            public string AT099
            {
                get
                {
                    return this.aT099Field;
                }
                set
                {
                    this.aT099Field = value;
                }
            }



            public string AT100
            {
                get
                {
                    return this.aT100Field;
                }
                set
                {
                    this.aT100Field = value;
                }
            }



            public string AT102
            {
                get
                {
                    return this.aT102Field;
                }
                set
                {
                    this.aT102Field = value;
                }
            }



            public string AT106
            {
                get
                {
                    return this.aT106Field;
                }
                set
                {
                    this.aT106Field = value;
                }
            }



            public string AT108
            {
                get
                {
                    return this.aT108Field;
                }
                set
                {
                    this.aT108Field = value;
                }
            }



            public string AT109
            {
                get
                {
                    return this.aT109Field;
                }
                set
                {
                    this.aT109Field = value;
                }
            }



            public string AT120
            {
                get
                {
                    return this.aT120Field;
                }
                set
                {
                    this.aT120Field = value;
                }
            }



            public string AT140
            {
                get
                {
                    return this.aT140Field;
                }
                set
                {
                    this.aT140Field = value;
                }
            }



            public string AT154
            {
                get
                {
                    return this.aT154Field;
                }
                set
                {
                    this.aT154Field = value;
                }
            }



            public string AT160
            {
                get
                {
                    return this.aT160Field;
                }
                set
                {
                    this.aT160Field = value;
                }
            }



            public string AT165
            {
                get
                {
                    return this.aT165Field;
                }
                set
                {
                    this.aT165Field = value;
                }
            }



            public string AT179
            {
                get
                {
                    return this.aT179Field;
                }
                set
                {
                    this.aT179Field = value;
                }
            }



            public string AT180
            {
                get
                {
                    return this.aT180Field;
                }
                set
                {
                    this.aT180Field = value;
                }
            }



            public string G003
            {
                get
                {
                    return this.g003Field;
                }
                set
                {
                    this.g003Field = value;
                }
            }



            public string G013
            {
                get
                {
                    return this.g013Field;
                }
                set
                {
                    this.g013Field = value;
                }
            }



            public string G024
            {
                get
                {
                    return this.g024Field;
                }
                set
                {
                    this.g024Field = value;
                }
            }



            public string G039
            {
                get
                {
                    return this.g039Field;
                }
                set
                {
                    this.g039Field = value;
                }
            }



            public string G043
            {
                get
                {
                    return this.g043Field;
                }
                set
                {
                    this.g043Field = value;
                }
            }



            public string IN001
            {
                get
                {
                    return this.iN001Field;
                }
                set
                {
                    this.iN001Field = value;
                }
            }



            public string IN021
            {
                get
                {
                    return this.iN021Field;
                }
                set
                {
                    this.iN021Field = value;
                }
            }



            public string IN022
            {
                get
                {
                    return this.iN022Field;
                }
                set
                {
                    this.iN022Field = value;
                }
            }



            public string IN025
            {
                get
                {
                    return this.iN025Field;
                }
                set
                {
                    this.iN025Field = value;
                }
            }



            public string IN026
            {
                get
                {
                    return this.iN026Field;
                }
                set
                {
                    this.iN026Field = value;
                }
            }



            public string IN027
            {
                get
                {
                    return this.iN027Field;
                }
                set
                {
                    this.iN027Field = value;
                }
            }



            public string IN039
            {
                get
                {
                    return this.iN039Field;
                }
                set
                {
                    this.iN039Field = value;
                }
            }



            public string IN075
            {
                get
                {
                    return this.iN075Field;
                }
                set
                {
                    this.iN075Field = value;
                }
            }



            public string IN105
            {
                get
                {
                    return this.iN105Field;
                }
                set
                {
                    this.iN105Field = value;
                }
            }



            public string IN122
            {
                get
                {
                    return this.iN122Field;
                }
                set
                {
                    this.iN122Field = value;
                }
            }



            public string IN158
            {
                get
                {
                    return this.iN158Field;
                }
                set
                {
                    this.iN158Field = value;
                }
            }



            public string IN180
            {
                get
                {
                    return this.iN180Field;
                }
                set
                {
                    this.iN180Field = value;
                }
            }



            public string OT001
            {
                get
                {
                    return this.oT001Field;
                }
                set
                {
                    this.oT001Field = value;
                }
            }



            public string OT002
            {
                get
                {
                    return this.oT002Field;
                }
                set
                {
                    this.oT002Field = value;
                }
            }



            public string OT007
            {
                get
                {
                    return this.oT007Field;
                }
                set
                {
                    this.oT007Field = value;
                }
            }



            public string OT008
            {
                get
                {
                    return this.oT008Field;
                }
                set
                {
                    this.oT008Field = value;
                }
            }



            public string OT022
            {
                get
                {
                    return this.oT022Field;
                }
                set
                {
                    this.oT022Field = value;
                }
            }



            public string OT025
            {
                get
                {
                    return this.oT025Field;
                }
                set
                {
                    this.oT025Field = value;
                }
            }



            public string OT152
            {
                get
                {
                    return this.oT152Field;
                }
                set
                {
                    this.oT152Field = value;
                }
            }



            public string OT153
            {
                get
                {
                    return this.oT153Field;
                }
                set
                {
                    this.oT153Field = value;
                }
            }



            public string OT164
            {
                get
                {
                    return this.oT164Field;
                }
                set
                {
                    this.oT164Field = value;
                }
            }



            public string OT167
            {
                get
                {
                    return this.oT167Field;
                }
                set
                {
                    this.oT167Field = value;
                }
            }



            public string OT180
            {
                get
                {
                    return this.oT180Field;
                }
                set
                {
                    this.oT180Field = value;
                }
            }



            public string RE001
            {
                get
                {
                    return this.rE001Field;
                }
                set
                {
                    this.rE001Field = value;
                }
            }



            public string RE002
            {
                get
                {
                    return this.rE002Field;
                }
                set
                {
                    this.rE002Field = value;
                }
            }



            public string RE007
            {
                get
                {
                    return this.rE007Field;
                }
                set
                {
                    this.rE007Field = value;
                }
            }



            public string RE033
            {
                get
                {
                    return this.rE033Field;
                }
                set
                {
                    this.rE033Field = value;
                }
            }



            public string RE039
            {
                get
                {
                    return this.rE039Field;
                }
                set
                {
                    this.rE039Field = value;
                }
            }



            public string RE042
            {
                get
                {
                    return this.rE042Field;
                }
                set
                {
                    this.rE042Field = value;
                }
            }



            public string RE045
            {
                get
                {
                    return this.rE045Field;
                }
                set
                {
                    this.rE045Field = value;
                }
            }



            public string RE102
            {
                get
                {
                    return this.rE102Field;
                }
                set
                {
                    this.rE102Field = value;
                }
            }



            public string RE103
            {
                get
                {
                    return this.rE103Field;
                }
                set
                {
                    this.rE103Field = value;
                }
            }



            public string RE104
            {
                get
                {
                    return this.rE104Field;
                }
                set
                {
                    this.rE104Field = value;
                }
            }



            public string RE105
            {
                get
                {
                    return this.rE105Field;
                }
                set
                {
                    this.rE105Field = value;
                }
            }



            public string RE120
            {
                get
                {
                    return this.rE120Field;
                }
                set
                {
                    this.rE120Field = value;
                }
            }



            public string RE121
            {
                get
                {
                    return this.rE121Field;
                }
                set
                {
                    this.rE121Field = value;
                }
            }



            public string RE122
            {
                get
                {
                    return this.rE122Field;
                }
                set
                {
                    this.rE122Field = value;
                }
            }



            public string RE140
            {
                get
                {
                    return this.rE140Field;
                }
                set
                {
                    this.rE140Field = value;
                }
            }



            public string RE153
            {
                get
                {
                    return this.rE153Field;
                }
                set
                {
                    this.rE153Field = value;
                }
            }



            public string RE156
            {
                get
                {
                    return this.rE156Field;
                }
                set
                {
                    this.rE156Field = value;
                }
            }



            public string RE159
            {
                get
                {
                    return this.rE159Field;
                }
                set
                {
                    this.rE159Field = value;
                }
            }



            public string RE162
            {
                get
                {
                    return this.rE162Field;
                }
                set
                {
                    this.rE162Field = value;
                }
            }



            public string RE164
            {
                get
                {
                    return this.rE164Field;
                }
                set
                {
                    this.rE164Field = value;
                }
            }



            public string RE167
            {
                get
                {
                    return this.rE167Field;
                }
                set
                {
                    this.rE167Field = value;
                }
            }



            public string RE180
            {
                get
                {
                    return this.rE180Field;
                }
                set
                {
                    this.rE180Field = value;
                }
            }



            public string AT038
            {
                get
                {
                    return this.aT038Field;
                }
                set
                {
                    this.aT038Field = value;
                }
            }



            public string AT007
            {
                get
                {
                    return this.aT007Field;
                }
                set
                {
                    this.aT007Field = value;
                }
            }



            public string AT029
            {
                get
                {
                    return this.aT029Field;
                }
                set
                {
                    this.aT029Field = value;
                }
            }



            public string AT091
            {
                get
                {
                    return this.aT091Field;
                }
                set
                {
                    this.aT091Field = value;
                }
            }



            public string AT153
            {
                get
                {
                    return this.aT153Field;
                }
                set
                {
                    this.aT153Field = value;
                }
            }



            public string G002
            {
                get
                {
                    return this.g002Field;
                }
                set
                {
                    this.g002Field = value;
                }
            }



            public string G010
            {
                get
                {
                    return this.g010Field;
                }
                set
                {
                    this.g010Field = value;
                }
            }



            public string G030
            {
                get
                {
                    return this.g030Field;
                }
                set
                {
                    this.g030Field = value;
                }
            }



            public string IN006
            {
                get
                {
                    return this.iN006Field;
                }
                set
                {
                    this.iN006Field = value;
                }
            }



            public string IN106
            {
                get
                {
                    return this.iN106Field;
                }
                set
                {
                    this.iN106Field = value;
                }
            }



            public string IN153
            {
                get
                {
                    return this.iN153Field;
                }
                set
                {
                    this.iN153Field = value;
                }
            }



            public string IN179
            {
                get
                {
                    return this.iN179Field;
                }
                set
                {
                    this.iN179Field = value;
                }
            }



            public string OT179
            {
                get
                {
                    return this.oT179Field;
                }
                set
                {
                    this.oT179Field = value;
                }
            }



            public string RE008
            {
                get
                {
                    return this.rE008Field;
                }
                set
                {
                    this.rE008Field = value;
                }
            }



            public string RE026
            {
                get
                {
                    return this.rE026Field;
                }
                set
                {
                    this.rE026Field = value;
                }
            }



            public string RE179
            {
                get
                {
                    return this.rE179Field;
                }
                set
                {
                    this.rE179Field = value;
                }
            }



            public string ML179
            {
                get
                {
                    return this.mL179Field;
                }
                set
                {
                    this.mL179Field = value;
                }
            }



            public string AT046
            {
                get
                {
                    return this.aT046Field;
                }
                set
                {
                    this.aT046Field = value;
                }
            }



            public string AT036
            {
                get
                {
                    return this.aT036Field;
                }
                set
                {
                    this.aT036Field = value;
                }
            }



            public string AT037
            {
                get
                {
                    return this.aT037Field;
                }
                set
                {
                    this.aT037Field = value;
                }
            }



            public string AT162
            {
                get
                {
                    return this.aT162Field;
                }
                set
                {
                    this.aT162Field = value;
                }
            }



            public string AT163
            {
                get
                {
                    return this.aT163Field;
                }
                set
                {
                    this.aT163Field = value;
                }
            }



            public string AT164
            {
                get
                {
                    return this.aT164Field;
                }
                set
                {
                    this.aT164Field = value;
                }
            }



            public string G014
            {
                get
                {
                    return this.g014Field;
                }
                set
                {
                    this.g014Field = value;
                }
            }



            public string G044
            {
                get
                {
                    return this.g044Field;
                }
                set
                {
                    this.g044Field = value;
                }
            }



            public string G057
            {
                get
                {
                    return this.g057Field;
                }
                set
                {
                    this.g057Field = value;
                }
            }



            public string G016
            {
                get
                {
                    return this.g016Field;
                }
                set
                {
                    this.g016Field = value;
                }
            }



            public string G017
            {
                get
                {
                    return this.g017Field;
                }
                set
                {
                    this.g017Field = value;
                }
            }



            public string AT026
            {
                get
                {
                    return this.aT026Field;
                }
                set
                {
                    this.aT026Field = value;
                }
            }



            public string ML026
            {
                get
                {
                    return this.mL026Field;
                }
                set
                {
                    this.mL026Field = value;
                }
            }



            public string ML035
            {
                get
                {
                    return this.mL035Field;
                }
                set
                {
                    this.mL035Field = value;
                }
            }



            public string ML039
            {
                get
                {
                    return this.mL039Field;
                }
                set
                {
                    this.mL039Field = value;
                }
            }



            public string ML002
            {
                get
                {
                    return this.mL002Field;
                }
                set
                {
                    this.mL002Field = value;
                }
            }



            public string ML090
            {
                get
                {
                    return this.mL090Field;
                }
                set
                {
                    this.mL090Field = value;
                }
            }



            public string ML042
            {
                get
                {
                    return this.mL042Field;
                }
                set
                {
                    this.mL042Field = value;
                }
            }



            public string AT021
            {
                get
                {
                    return this.aT021Field;
                }
                set
                {
                    this.aT021Field = value;
                }
            }



            public string ML021
            {
                get
                {
                    return this.mL021Field;
                }
                set
                {
                    this.mL021Field = value;
                }
            }



            public string G041
            {
                get
                {
                    return this.g041Field;
                }
                set
                {
                    this.g041Field = value;
                }
            }



            public string IN159
            {
                get
                {
                    return this.iN159Field;
                }
                set
                {
                    this.iN159Field = value;
                }
            }



            public string ML044
            {
                get
                {
                    return this.mL044Field;
                }
                set
                {
                    this.mL044Field = value;
                }
            }



            public string AT025
            {
                get
                {
                    return this.aT025Field;
                }
                set
                {
                    this.aT025Field = value;
                }
            }



            public string AT034
            {
                get
                {
                    return this.aT034Field;
                }
                set
                {
                    this.aT034Field = value;
                }
            }



            public string AT035
            {
                get
                {
                    return this.aT035Field;
                }
                set
                {
                    this.aT035Field = value;
                }
            }



            public string AT155
            {
                get
                {
                    return this.aT155Field;
                }
                set
                {
                    this.aT155Field = value;
                }
            }



            public string AT159
            {
                get
                {
                    return this.aT159Field;
                }
                set
                {
                    this.aT159Field = value;
                }
            }



            public string G022
            {
                get
                {
                    return this.g022Field;
                }
                set
                {
                    this.g022Field = value;
                }
            }



            public string G056
            {
                get
                {
                    return this.g056Field;
                }
                set
                {
                    this.g056Field = value;
                }
            }



            public string IN121
            {
                get
                {
                    return this.iN121Field;
                }
                set
                {
                    this.iN121Field = value;
                }
            }



            public string ML025
            {
                get
                {
                    return this.mL025Field;
                }
                set
                {
                    this.mL025Field = value;
                }
            }



            public string ML034
            {
                get
                {
                    return this.mL034Field;
                }
                set
                {
                    this.mL034Field = value;
                }
            }



            public string ML036
            {
                get
                {
                    return this.mL036Field;
                }
                set
                {
                    this.mL036Field = value;
                }
            }



            public string ML037
            {
                get
                {
                    return this.mL037Field;
                }
                set
                {
                    this.mL037Field = value;
                }
            }



            public string ML038
            {
                get
                {
                    return this.mL038Field;
                }
                set
                {
                    this.mL038Field = value;
                }
            }



            public string ML040
            {
                get
                {
                    return this.mL040Field;
                }
                set
                {
                    this.mL040Field = value;
                }
            }



            public string ML091
            {
                get
                {
                    return this.mL091Field;
                }
                set
                {
                    this.mL091Field = value;
                }
            }



            public string ML121
            {
                get
                {
                    return this.mL121Field;
                }
                set
                {
                    this.mL121Field = value;
                }
            }



            public string ML153
            {
                get
                {
                    return this.mL153Field;
                }
                set
                {
                    this.mL153Field = value;
                }
            }



            public string ML162
            {
                get
                {
                    return this.mL162Field;
                }
                set
                {
                    this.mL162Field = value;
                }
            }



            public string ML163
            {
                get
                {
                    return this.mL163Field;
                }
                set
                {
                    this.mL163Field = value;
                }
            }



            public string ML181
            {
                get
                {
                    return this.mL181Field;
                }
                set
                {
                    this.mL181Field = value;
                }
            }



            public string OT122
            {
                get
                {
                    return this.oT122Field;
                }
                set
                {
                    this.oT122Field = value;
                }
            }



            public string OT154
            {
                get
                {
                    return this.oT154Field;
                }
                set
                {
                    this.oT154Field = value;
                }
            }



            public string AT043
            {
                get
                {
                    return this.aT043Field;
                }
                set
                {
                    this.aT043Field = value;
                }
            }



            public string IN107
            {
                get
                {
                    return this.iN107Field;
                }
                set
                {
                    this.iN107Field = value;
                }
            }



            public string ML041
            {
                get
                {
                    return this.mL041Field;
                }
                set
                {
                    this.mL041Field = value;
                }
            }



            public string ML092
            {
                get
                {
                    return this.mL092Field;
                }
                set
                {
                    this.mL092Field = value;
                }
            }



            public string ML097
            {
                get
                {
                    return this.mL097Field;
                }
                set
                {
                    this.mL097Field = value;
                }
            }



            public string ML111
            {
                get
                {
                    return this.mL111Field;
                }
                set
                {
                    this.mL111Field = value;
                }
            }



            public string G011
            {
                get
                {
                    return this.g011Field;
                }
                set
                {
                    this.g011Field = value;
                }
            }



            public string G048
            {
                get
                {
                    return this.g048Field;
                }
                set
                {
                    this.g048Field = value;
                }
            }



            public string AT124
            {
                get
                {
                    return this.aT124Field;
                }
                set
                {
                    this.aT124Field = value;
                }
            }



            public string ML001
            {
                get
                {
                    return this.mL001Field;
                }
                set
                {
                    this.mL001Field = value;
                }
            }



            public string ML043
            {
                get
                {
                    return this.mL043Field;
                }
                set
                {
                    this.mL043Field = value;
                }
            }



            public string ML104
            {
                get
                {
                    return this.mL104Field;
                }
                set
                {
                    this.mL104Field = value;
                }
            }



            public string ML140
            {
                get
                {
                    return this.mL140Field;
                }
                set
                {
                    this.mL140Field = value;
                }
            }



            public string OT042
            {
                get
                {
                    return this.oT042Field;
                }
                set
                {
                    this.oT042Field = value;
                }
            }



            public string AT028
            {
                get
                {
                    return this.aT028Field;
                }
                set
                {
                    this.aT028Field = value;
                }
            }



            public string G021
            {
                get
                {
                    return this.g021Field;
                }
                set
                {
                    this.g021Field = value;
                }
            }



            public string AT152
            {
                get
                {
                    return this.aT152Field;
                }
                set
                {
                    this.aT152Field = value;
                }
            }



            public string AT161
            {
                get
                {
                    return this.aT161Field;
                }
                set
                {
                    this.aT161Field = value;
                }
            }



            public string IN162
            {
                get
                {
                    return this.iN162Field;
                }
                set
                {
                    this.iN162Field = value;
                }
            }



            public string OT026
            {
                get
                {
                    return this.oT026Field;
                }
                set
                {
                    this.oT026Field = value;
                }
            }



            public string OT106
            {
                get
                {
                    return this.oT106Field;
                }
                set
                {
                    this.oT106Field = value;
                }
            }



            public string OT121
            {
                get
                {
                    return this.oT121Field;
                }
                set
                {
                    this.oT121Field = value;
                }
            }



            public string OT162
            {
                get
                {
                    return this.oT162Field;
                }
                set
                {
                    this.oT162Field = value;
                }
            }



            public string G052
            {
                get
                {
                    return this.g052Field;
                }
                set
                {
                    this.g052Field = value;
                }
            }



            public string G054
            {
                get
                {
                    return this.g054Field;
                }
                set
                {
                    this.g054Field = value;
                }
            }
        }







        public class StandardBatchCharsSB30
        {

            private string consumerNoField;

            private string aT001Field;

            private string aT006Field;

            private string aT022Field;

            private string aT025Field;

            private string aT041Field;

            private string aT043Field;

            private string aT092Field;

            private string aT093Field;

            private string aT103Field;

            private string aT107Field;

            private string aT121Field;

            private string aT140Field;

            private string aT141Field;

            private string aT152Field;

            private string aT156Field;

            private string aT164Field;

            private string aT167Field;

            private string g002Field;

            private string g011Field;

            private string g031Field;

            private string g057Field;

            private string iN001Field;

            private string iN180Field;

            private string iN181Field;

            private string mL001Field;

            private string mL006Field;

            private string mL042Field;

            private string mL093Field;

            private string mL094Field;

            private string mL104Field;

            private string mL140Field;

            private string mL160Field;

            private string mL181Field;

            private string oT001Field;

            private string rE001Field;

            private string rE003Field;

            private string rE022Field;

            private string rE107Field;

            private string rE121Field;

            private string rE156Field;

            private string rE180Field;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string AT001
            {
                get
                {
                    return this.aT001Field;
                }
                set
                {
                    this.aT001Field = value;
                }
            }



            public string AT006
            {
                get
                {
                    return this.aT006Field;
                }
                set
                {
                    this.aT006Field = value;
                }
            }



            public string AT022
            {
                get
                {
                    return this.aT022Field;
                }
                set
                {
                    this.aT022Field = value;
                }
            }



            public string AT025
            {
                get
                {
                    return this.aT025Field;
                }
                set
                {
                    this.aT025Field = value;
                }
            }



            public string AT041
            {
                get
                {
                    return this.aT041Field;
                }
                set
                {
                    this.aT041Field = value;
                }
            }



            public string AT043
            {
                get
                {
                    return this.aT043Field;
                }
                set
                {
                    this.aT043Field = value;
                }
            }



            public string AT092
            {
                get
                {
                    return this.aT092Field;
                }
                set
                {
                    this.aT092Field = value;
                }
            }



            public string AT093
            {
                get
                {
                    return this.aT093Field;
                }
                set
                {
                    this.aT093Field = value;
                }
            }



            public string AT103
            {
                get
                {
                    return this.aT103Field;
                }
                set
                {
                    this.aT103Field = value;
                }
            }



            public string AT107
            {
                get
                {
                    return this.aT107Field;
                }
                set
                {
                    this.aT107Field = value;
                }
            }



            public string AT121
            {
                get
                {
                    return this.aT121Field;
                }
                set
                {
                    this.aT121Field = value;
                }
            }



            public string AT140
            {
                get
                {
                    return this.aT140Field;
                }
                set
                {
                    this.aT140Field = value;
                }
            }



            public string AT141
            {
                get
                {
                    return this.aT141Field;
                }
                set
                {
                    this.aT141Field = value;
                }
            }



            public string AT152
            {
                get
                {
                    return this.aT152Field;
                }
                set
                {
                    this.aT152Field = value;
                }
            }



            public string AT156
            {
                get
                {
                    return this.aT156Field;
                }
                set
                {
                    this.aT156Field = value;
                }
            }



            public string AT164
            {
                get
                {
                    return this.aT164Field;
                }
                set
                {
                    this.aT164Field = value;
                }
            }



            public string AT167
            {
                get
                {
                    return this.aT167Field;
                }
                set
                {
                    this.aT167Field = value;
                }
            }



            public string G002
            {
                get
                {
                    return this.g002Field;
                }
                set
                {
                    this.g002Field = value;
                }
            }



            public string G011
            {
                get
                {
                    return this.g011Field;
                }
                set
                {
                    this.g011Field = value;
                }
            }



            public string G031
            {
                get
                {
                    return this.g031Field;
                }
                set
                {
                    this.g031Field = value;
                }
            }



            public string G057
            {
                get
                {
                    return this.g057Field;
                }
                set
                {
                    this.g057Field = value;
                }
            }



            public string IN001
            {
                get
                {
                    return this.iN001Field;
                }
                set
                {
                    this.iN001Field = value;
                }
            }



            public string IN180
            {
                get
                {
                    return this.iN180Field;
                }
                set
                {
                    this.iN180Field = value;
                }
            }



            public string IN181
            {
                get
                {
                    return this.iN181Field;
                }
                set
                {
                    this.iN181Field = value;
                }
            }



            public string ML001
            {
                get
                {
                    return this.mL001Field;
                }
                set
                {
                    this.mL001Field = value;
                }
            }



            public string ML006
            {
                get
                {
                    return this.mL006Field;
                }
                set
                {
                    this.mL006Field = value;
                }
            }



            public string ML042
            {
                get
                {
                    return this.mL042Field;
                }
                set
                {
                    this.mL042Field = value;
                }
            }



            public string ML093
            {
                get
                {
                    return this.mL093Field;
                }
                set
                {
                    this.mL093Field = value;
                }
            }



            public string ML094
            {
                get
                {
                    return this.mL094Field;
                }
                set
                {
                    this.mL094Field = value;
                }
            }



            public string ML104
            {
                get
                {
                    return this.mL104Field;
                }
                set
                {
                    this.mL104Field = value;
                }
            }



            public string ML140
            {
                get
                {
                    return this.mL140Field;
                }
                set
                {
                    this.mL140Field = value;
                }
            }



            public string ML160
            {
                get
                {
                    return this.mL160Field;
                }
                set
                {
                    this.mL160Field = value;
                }
            }



            public string ML181
            {
                get
                {
                    return this.mL181Field;
                }
                set
                {
                    this.mL181Field = value;
                }
            }



            public string OT001
            {
                get
                {
                    return this.oT001Field;
                }
                set
                {
                    this.oT001Field = value;
                }
            }



            public string RE001
            {
                get
                {
                    return this.rE001Field;
                }
                set
                {
                    this.rE001Field = value;
                }
            }



            public string RE003
            {
                get
                {
                    return this.rE003Field;
                }
                set
                {
                    this.rE003Field = value;
                }
            }



            public string RE022
            {
                get
                {
                    return this.rE022Field;
                }
                set
                {
                    this.rE022Field = value;
                }
            }



            public string RE107
            {
                get
                {
                    return this.rE107Field;
                }
                set
                {
                    this.rE107Field = value;
                }
            }



            public string RE121
            {
                get
                {
                    return this.rE121Field;
                }
                set
                {
                    this.rE121Field = value;
                }
            }



            public string RE156
            {
                get
                {
                    return this.rE156Field;
                }
                set
                {
                    this.rE156Field = value;
                }
            }



            public string RE180
            {
                get
                {
                    return this.rE180Field;
                }
                set
                {
                    this.rE180Field = value;
                }
            }
        }







        public class StandardBatchCharsSB33
        {

            private string consumerNoField;

            private string aT094Field;

            private string aT107Field;

            private string aT164Field;

            private string aT179Field;

            private string g012Field;

            private string g032Field;

            private string g044Field;

            private string iN106Field;

            private string iN141Field;

            private string mL179Field;

            private string oT007Field;

            private string oT041Field;

            private string rE105Field;

            private string rE140Field;

            private string wEPP_StatusField;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string AT094
            {
                get
                {
                    return this.aT094Field;
                }
                set
                {
                    this.aT094Field = value;
                }
            }



            public string AT107
            {
                get
                {
                    return this.aT107Field;
                }
                set
                {
                    this.aT107Field = value;
                }
            }



            public string AT164
            {
                get
                {
                    return this.aT164Field;
                }
                set
                {
                    this.aT164Field = value;
                }
            }



            public string AT179
            {
                get
                {
                    return this.aT179Field;
                }
                set
                {
                    this.aT179Field = value;
                }
            }



            public string G012
            {
                get
                {
                    return this.g012Field;
                }
                set
                {
                    this.g012Field = value;
                }
            }



            public string G032
            {
                get
                {
                    return this.g032Field;
                }
                set
                {
                    this.g032Field = value;
                }
            }



            public string G044
            {
                get
                {
                    return this.g044Field;
                }
                set
                {
                    this.g044Field = value;
                }
            }



            public string IN106
            {
                get
                {
                    return this.iN106Field;
                }
                set
                {
                    this.iN106Field = value;
                }
            }



            public string IN141
            {
                get
                {
                    return this.iN141Field;
                }
                set
                {
                    this.iN141Field = value;
                }
            }



            public string ML179
            {
                get
                {
                    return this.mL179Field;
                }
                set
                {
                    this.mL179Field = value;
                }
            }



            public string OT007
            {
                get
                {
                    return this.oT007Field;
                }
                set
                {
                    this.oT007Field = value;
                }
            }



            public string OT041
            {
                get
                {
                    return this.oT041Field;
                }
                set
                {
                    this.oT041Field = value;
                }
            }



            public string RE105
            {
                get
                {
                    return this.rE105Field;
                }
                set
                {
                    this.rE105Field = value;
                }
            }



            public string RE140
            {
                get
                {
                    return this.rE140Field;
                }
                set
                {
                    this.rE140Field = value;
                }
            }



            public string WEPP_Status
            {
                get
                {
                    return this.wEPP_StatusField;
                }
                set
                {
                    this.wEPP_StatusField = value;
                }
            }
        }







        public class StandardBatchCharsSB37
        {

            private string consumerNoField;

            private string aT040Field;

            private string aT163Field;

            private string aT179Field;

            private string g021Field;

            private string g030Field;

            private string g044Field;

            private string g057Field;

            private string mL001Field;

            private string mL040Field;

            private string mL163Field;

            private string mL179Field;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string AT040
            {
                get
                {
                    return this.aT040Field;
                }
                set
                {
                    this.aT040Field = value;
                }
            }



            public string AT163
            {
                get
                {
                    return this.aT163Field;
                }
                set
                {
                    this.aT163Field = value;
                }
            }



            public string AT179
            {
                get
                {
                    return this.aT179Field;
                }
                set
                {
                    this.aT179Field = value;
                }
            }



            public string G021
            {
                get
                {
                    return this.g021Field;
                }
                set
                {
                    this.g021Field = value;
                }
            }



            public string G030
            {
                get
                {
                    return this.g030Field;
                }
                set
                {
                    this.g030Field = value;
                }
            }



            public string G044
            {
                get
                {
                    return this.g044Field;
                }
                set
                {
                    this.g044Field = value;
                }
            }



            public string G057
            {
                get
                {
                    return this.g057Field;
                }
                set
                {
                    this.g057Field = value;
                }
            }



            public string ML001
            {
                get
                {
                    return this.mL001Field;
                }
                set
                {
                    this.mL001Field = value;
                }
            }



            public string ML040
            {
                get
                {
                    return this.mL040Field;
                }
                set
                {
                    this.mL040Field = value;
                }
            }



            public string ML163
            {
                get
                {
                    return this.mL163Field;
                }
                set
                {
                    this.mL163Field = value;
                }
            }



            public string ML179
            {
                get
                {
                    return this.mL179Field;
                }
                set
                {
                    this.mL179Field = value;
                }
            }
        }







        public class StandardBatchCharsSB40
        {

            private string consumerNoField;

            private string aT001Field;

            private string aT002Field;

            private string aT006Field;

            private string aT021Field;

            private string aT042Field;

            private string aT043Field;

            private string aT090Field;

            private string aT091Field;

            private string aT099Field;

            private string aT102Field;

            private string aT105Field;

            private string aT108Field;

            private string aT150Field;

            private string aT152Field;

            private string aT160Field;

            private string aT164Field;

            private string g011Field;

            private string g014Field;

            private string g017Field;

            private string g022Field;

            private string g031Field;

            private string g044Field;

            private string g057Field;

            private string mL002Field;

            private string mL006Field;

            private string mL007Field;

            private string mL043Field;

            private string mL093Field;

            private string mL104Field;

            private string mL108Field;

            private string mL120Field;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string AT001
            {
                get
                {
                    return this.aT001Field;
                }
                set
                {
                    this.aT001Field = value;
                }
            }



            public string AT002
            {
                get
                {
                    return this.aT002Field;
                }
                set
                {
                    this.aT002Field = value;
                }
            }



            public string AT006
            {
                get
                {
                    return this.aT006Field;
                }
                set
                {
                    this.aT006Field = value;
                }
            }



            public string AT021
            {
                get
                {
                    return this.aT021Field;
                }
                set
                {
                    this.aT021Field = value;
                }
            }



            public string AT042
            {
                get
                {
                    return this.aT042Field;
                }
                set
                {
                    this.aT042Field = value;
                }
            }



            public string AT043
            {
                get
                {
                    return this.aT043Field;
                }
                set
                {
                    this.aT043Field = value;
                }
            }



            public string AT090
            {
                get
                {
                    return this.aT090Field;
                }
                set
                {
                    this.aT090Field = value;
                }
            }



            public string AT091
            {
                get
                {
                    return this.aT091Field;
                }
                set
                {
                    this.aT091Field = value;
                }
            }



            public string AT099
            {
                get
                {
                    return this.aT099Field;
                }
                set
                {
                    this.aT099Field = value;
                }
            }



            public string AT102
            {
                get
                {
                    return this.aT102Field;
                }
                set
                {
                    this.aT102Field = value;
                }
            }



            public string AT105
            {
                get
                {
                    return this.aT105Field;
                }
                set
                {
                    this.aT105Field = value;
                }
            }



            public string AT108
            {
                get
                {
                    return this.aT108Field;
                }
                set
                {
                    this.aT108Field = value;
                }
            }



            public string AT150
            {
                get
                {
                    return this.aT150Field;
                }
                set
                {
                    this.aT150Field = value;
                }
            }



            public string AT152
            {
                get
                {
                    return this.aT152Field;
                }
                set
                {
                    this.aT152Field = value;
                }
            }



            public string AT160
            {
                get
                {
                    return this.aT160Field;
                }
                set
                {
                    this.aT160Field = value;
                }
            }



            public string AT164
            {
                get
                {
                    return this.aT164Field;
                }
                set
                {
                    this.aT164Field = value;
                }
            }



            public string G011
            {
                get
                {
                    return this.g011Field;
                }
                set
                {
                    this.g011Field = value;
                }
            }



            public string G014
            {
                get
                {
                    return this.g014Field;
                }
                set
                {
                    this.g014Field = value;
                }
            }



            public string G017
            {
                get
                {
                    return this.g017Field;
                }
                set
                {
                    this.g017Field = value;
                }
            }



            public string G022
            {
                get
                {
                    return this.g022Field;
                }
                set
                {
                    this.g022Field = value;
                }
            }



            public string G031
            {
                get
                {
                    return this.g031Field;
                }
                set
                {
                    this.g031Field = value;
                }
            }



            public string G044
            {
                get
                {
                    return this.g044Field;
                }
                set
                {
                    this.g044Field = value;
                }
            }



            public string G057
            {
                get
                {
                    return this.g057Field;
                }
                set
                {
                    this.g057Field = value;
                }
            }



            public string ML002
            {
                get
                {
                    return this.mL002Field;
                }
                set
                {
                    this.mL002Field = value;
                }
            }



            public string ML006
            {
                get
                {
                    return this.mL006Field;
                }
                set
                {
                    this.mL006Field = value;
                }
            }



            public string ML007
            {
                get
                {
                    return this.mL007Field;
                }
                set
                {
                    this.mL007Field = value;
                }
            }



            public string ML043
            {
                get
                {
                    return this.mL043Field;
                }
                set
                {
                    this.mL043Field = value;
                }
            }



            public string ML093
            {
                get
                {
                    return this.mL093Field;
                }
                set
                {
                    this.mL093Field = value;
                }
            }



            public string ML104
            {
                get
                {
                    return this.mL104Field;
                }
                set
                {
                    this.mL104Field = value;
                }
            }



            public string ML108
            {
                get
                {
                    return this.mL108Field;
                }
                set
                {
                    this.mL108Field = value;
                }
            }



            public string ML120
            {
                get
                {
                    return this.mL120Field;
                }
                set
                {
                    this.mL120Field = value;
                }
            }
        }







        public class StandardBatchCharsSB99
        {

            private string consumerNoField;

            private string sBCsField;



            public string ConsumerNo
            {
                get
                {
                    return this.consumerNoField;
                }
                set
                {
                    this.consumerNoField = value;
                }
            }



            public string SBCs
            {
                get
                {
                    return this.sBCsField;
                }
                set
                {
                    this.sBCsField = value;
                }
            }
        }







        public class SuburbCodeGR01
        {
        }







        public class UniqueITCRef
        {

            private string iTCReferenceNumberField;



            public string ITCReferenceNumber
            {
                get
                {
                    return this.iTCReferenceNumberField;
                }
                set
                {
                    this.iTCReferenceNumberField = value;
                }
            }
        }







        public class V1Segment
        {

            private string v1SegsField;



            public string v1Segs
            {
                get
                {
                    return this.v1SegsField;
                }
                set
                {
                    this.v1SegsField = value;
                }
            }
        }







        public class VehicleDataUZ01
        {
        }
    }


    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.1432")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class SubmitP
    {

        private SubmitInputP itemsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("input")]
        public SubmitInputP Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.1432")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class SubmitInputP
    {

        private string reqRepositoryField;

        private string companyField;

        private string branchField;

        private string userField;

        private string passwordField;

        private string ruleSetField;

        private string existingClientField;

        private string titleField;

        private string firstNameField;

        private string surnameField;

        private string iDTypeField;

        private string iDNumberField;

        private string birthDateField;

        private string genderField;

        private string marital_StatusField;

        private string numberOfDependantsField;

        private string addressLine1Field;

        private string addressLine2Field;

        private string suburbField;

        private string cityField;

        private string physicalPostalCodeField;

        private string provinceCodeField;

        private string residentialStatusField;

        private string numberOfYearsAtEmployerField;

        private string grossIncomeField;

        private string incomeField;

        private string brandNameField;

        private string customerAgeField;

        private string employmentStatusField;

        private string referenceField;

        private string bankIndicatorField;

        private string numberOfMonthsAtEmployerField;

        private string maritalStatusField;

        private string clientReferenceField;

        /// <remarks/>
        public string ReqRepository
        {
            get
            {
                return this.reqRepositoryField;
            }
            set
            {
                this.reqRepositoryField = value;
            }
        }

        /// <remarks/>
        public string Company
        {
            get
            {
                return this.companyField;
            }
            set
            {
                this.companyField = value;
            }
        }

        /// <remarks/>
        public string Branch
        {
            get
            {
                return this.branchField;
            }
            set
            {
                this.branchField = value;
            }
        }

        /// <remarks/>
        public string User
        {
            get
            {
                return this.userField;
            }
            set
            {
                this.userField = value;
            }
        }

        /// <remarks/>
        public string Password
        {
            get
            {
                return this.passwordField;
            }
            set
            {
                this.passwordField = value;
            }
        }

        /// <remarks/>
        public string RuleSet
        {
            get
            {
                return this.ruleSetField;
            }
            set
            {
                this.ruleSetField = value;
            }
        }

        /// <remarks/>
        public string ExistingClient
        {
            get
            {
                return this.existingClientField;
            }
            set
            {
                this.existingClientField = value;
            }
        }

        /// <remarks/>
        public string Title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>
        public string FirstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        public string IDType
        {
            get
            {
                return this.iDTypeField;
            }
            set
            {
                this.iDTypeField = value;
            }
        }

        /// <remarks/>
        public string IDNumber
        {
            get
            {
                return this.iDNumberField;
            }
            set
            {
                this.iDNumberField = value;
            }
        }

        /// <remarks/>
        public string BirthDate
        {
            get
            {
                return this.birthDateField;
            }
            set
            {
                this.birthDateField = value;
            }
        }

        /// <remarks/>
        public string Gender
        {
            get
            {
                return this.genderField;
            }
            set
            {
                this.genderField = value;
            }
        }

        /// <remarks/>
        public string Marital_Status
        {
            get
            {
                return this.marital_StatusField;
            }
            set
            {
                this.marital_StatusField = value;
            }
        }

        /// <remarks/>
        public string NumberOfDependants
        {
            get
            {
                return this.numberOfDependantsField;
            }
            set
            {
                this.numberOfDependantsField = value;
            }
        }

        /// <remarks/>
        public string AddressLine1
        {
            get
            {
                return this.addressLine1Field;
            }
            set
            {
                this.addressLine1Field = value;
            }
        }

        /// <remarks/>
        public string AddressLine2
        {
            get
            {
                return this.addressLine2Field;
            }
            set
            {
                this.addressLine2Field = value;
            }
        }

        /// <remarks/>
        public string Suburb
        {
            get
            {
                return this.suburbField;
            }
            set
            {
                this.suburbField = value;
            }
        }

        /// <remarks/>
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        public string PhysicalPostalCode
        {
            get
            {
                return this.physicalPostalCodeField;
            }
            set
            {
                this.physicalPostalCodeField = value;
            }
        }

        /// <remarks/>
        public string ProvinceCode
        {
            get
            {
                return this.provinceCodeField;
            }
            set
            {
                this.provinceCodeField = value;
            }
        }

        /// <remarks/>
        public string ResidentialStatus
        {
            get
            {
                return this.residentialStatusField;
            }
            set
            {
                this.residentialStatusField = value;
            }
        }

        /// <remarks/>
        public string NumberOfYearsAtEmployer
        {
            get
            {
                return this.numberOfYearsAtEmployerField;
            }
            set
            {
                this.numberOfYearsAtEmployerField = value;
            }
        }

        /// <remarks/>
        public string GrossIncome
        {
            get
            {
                return this.grossIncomeField;
            }
            set
            {
                this.grossIncomeField = value;
            }
        }

        /// <remarks/>
        public string Income
        {
            get
            {
                return this.incomeField;
            }
            set
            {
                this.incomeField = value;
            }
        }

        /// <remarks/>
        public string BrandName
        {
            get
            {
                return this.brandNameField;
            }
            set
            {
                this.brandNameField = value;
            }
        }

        /// <remarks/>
        public string CustomerAge
        {
            get
            {
                return this.customerAgeField;
            }
            set
            {
                this.customerAgeField = value;
            }
        }

        /// <remarks/>
        public string EmploymentStatus
        {
            get
            {
                return this.employmentStatusField;
            }
            set
            {
                this.employmentStatusField = value;
            }
        }

        /// <remarks/>
        public string Reference
        {
            get
            {
                return this.referenceField;
            }
            set
            {
                this.referenceField = value;
            }
        }

        /// <remarks/>
        public string BankIndicator
        {
            get
            {
                return this.bankIndicatorField;
            }
            set
            {
                this.bankIndicatorField = value;
            }
        }

        /// <remarks/>
        public string NumberOfMonthsAtEmployer
        {
            get
            {
                return this.numberOfMonthsAtEmployerField;
            }
            set
            {
                this.numberOfMonthsAtEmployerField = value;
            }
        }

        /// <remarks/>
        public string MaritalStatus
        {
            get
            {
                return this.maritalStatusField;
            }
            set
            {
                this.maritalStatusField = value;
            }
        }

        /// <remarks/>
        public string ClientReference
        {
            get
            {
                return this.clientReferenceField;
            }
            set
            {
                this.clientReferenceField = value;
            }
        }

    }


    public class SubmitResponseP
    {

        private SubmitResultP oSubmitResult = new SubmitResultP();


        //[System.Xml.Serialization.XmlElementAttribute("AKANamesNK04", typeof(AKANamesNK04))]
        //[System.Xml.Serialization.XmlElementAttribute("AddressNA07", typeof(AddressNA07))]
        //[System.Xml.Serialization.XmlElementAttribute("AddressNA08", typeof(AddressNA08))]
        //[System.Xml.Serialization.XmlElementAttribute("AlertDetailC3", typeof(AlertDetailC3))]
        //[System.Xml.Serialization.XmlElementAttribute("AlertDetailQC", typeof(AlertDetailQC))]
        //[System.Xml.Serialization.XmlElementAttribute("AlertDetailQD", typeof(AlertDetailQD))]
        //[System.Xml.Serialization.XmlElementAttribute("AlertDetailQE", typeof(AlertDetailQE))]
        //[System.Xml.Serialization.XmlElementAttribute("AlertDetailQJ", typeof(AlertDetailQJ))]
        //[System.Xml.Serialization.XmlElementAttribute("AlertDetailQM", typeof(AlertDetailQM))]
        //[System.Xml.Serialization.XmlElementAttribute("AlertDetailQN", typeof(AlertDetailQN))]
        //[System.Xml.Serialization.XmlElementAttribute("AlertDetailQP", typeof(AlertDetailQP))]
        //[System.Xml.Serialization.XmlElementAttribute("AlertDetailQV", typeof(AlertDetailQV))]
        //[System.Xml.Serialization.XmlElementAttribute("AlertDetailQW", typeof(AlertDetailQW))]
        //[System.Xml.Serialization.XmlElementAttribute("CPAAccountInformationExtendedP8", typeof(CPAAccountInformationExtendedP8))]
        //[System.Xml.Serialization.XmlElementAttribute("CellphoneValidationNZ01", typeof(CellphoneValidationNZ01))]
        //[System.Xml.Serialization.XmlElementAttribute("ConsumerDeedsDD01", typeof(ConsumerDeedsDD01))]
        //[System.Xml.Serialization.XmlElementAttribute("ConsumerMiscellaneousMS", typeof(ConsumerMiscellaneousMS))]
        //[System.Xml.Serialization.XmlElementAttribute("ConsumerNumberFrequencyNY01", typeof(ConsumerNumberFrequencyNY01))]
        //[System.Xml.Serialization.XmlElementAttribute("DebtReviewDR01", typeof(DebtReviewDR01))]
        //[System.Xml.Serialization.XmlElementAttribute("DefaultsND07", typeof(DefaultsND07))]
        //[System.Xml.Serialization.XmlElementAttribute("DriversLicenceUY01", typeof(DriversLicenceUY01))]
        //[System.Xml.Serialization.XmlElementAttribute("EmpiricaEX", typeof(EmpiricaEX))]
        //[System.Xml.Serialization.XmlElementAttribute("EmploymentNM04", typeof(EmploymentNM04))]
        //[System.Xml.Serialization.XmlElementAttribute("EnquiriesNE09", typeof(EnquiriesNE09))]
        //[System.Xml.Serialization.XmlElementAttribute("HawkHA01", typeof(HawkHA01))]
        //[System.Xml.Serialization.XmlElementAttribute("HawkHI01", typeof(HawkHI01))]
        //[System.Xml.Serialization.XmlElementAttribute("IndividualTrace04", typeof(IndividualTrace04))]
        //[System.Xml.Serialization.XmlElementAttribute("IndividualTrace05", typeof(IndividualTrace05))]
        //[System.Xml.Serialization.XmlElementAttribute("InsuranceClaimsUA01", typeof(InsuranceClaimsUA01))]
        //[System.Xml.Serialization.XmlElementAttribute("InsuranceClaimsUB01", typeof(InsuranceClaimsUB01))]
        //[System.Xml.Serialization.XmlElementAttribute("InsuranceClaimsUC01", typeof(InsuranceClaimsUC01))]
        //[System.Xml.Serialization.XmlElementAttribute("InsuranceClaimsUI01", typeof(InsuranceClaimsUI01))]
        //[System.Xml.Serialization.XmlElementAttribute("JudgementsNJ06", typeof(JudgementsNJ06))]
        //[System.Xml.Serialization.XmlElementAttribute("JudgementsNJ07", typeof(JudgementsNJ07))]
        //[System.Xml.Serialization.XmlElementAttribute("NLRAccountInformationExtendedM8", typeof(NLRAccountInformationExtendedM8))]
        //[System.Xml.Serialization.XmlElementAttribute("NLRAccountInformationMP01", typeof(NLRAccountInformationMP01))]
        //[System.Xml.Serialization.XmlElementAttribute("NLRAccountInformationMP03", typeof(NLRAccountInformationMP03))]
        //[System.Xml.Serialization.XmlElementAttribute("NLRCounterSeqmentMC01", typeof(NLRCounterSeqmentMC01))]
        //[System.Xml.Serialization.XmlElementAttribute("NLREnquiriesME01", typeof(NLREnquiriesME01))]
        //[System.Xml.Serialization.XmlElementAttribute("NoticesNN07", typeof(NoticesNN07))]
        //[System.Xml.Serialization.XmlElementAttribute("NoticesNN08", typeof(NoticesNN08))]
        //[System.Xml.Serialization.XmlElementAttribute("NoticesNN10", typeof(NoticesNN10))]
        //[System.Xml.Serialization.XmlElementAttribute("PaymentProfileNP09", typeof(PaymentProfileNP09))]
        //[System.Xml.Serialization.XmlElementAttribute("PaymentProfileNP11", typeof(PaymentProfileNP11))]
        //[System.Xml.Serialization.XmlElementAttribute("PaymentProfileNP15", typeof(PaymentProfileNP15))]
        //[System.Xml.Serialization.XmlElementAttribute("SAFPSNF01", typeof(SAFPSNF01))]
        //[System.Xml.Serialization.XmlElementAttribute("ScorecardS101", typeof(ScorecardS101))]
        //[System.Xml.Serialization.XmlElementAttribute("SubmitResult", typeof(SubmitResult), Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        //[System.Xml.Serialization.XmlElementAttribute("TraceAlertsNT04", typeof(TraceAlertsNT04))]
        //[System.Xml.Serialization.XmlElementAttribute("TraceAlertsNT06", typeof(TraceAlertsNT06))]
        //[System.Xml.Serialization.XmlElementAttribute("TraceAlertsNT07", typeof(TraceAlertsNT07))]
        public SubmitResultP SubmitResult
        {
            get
            {
                return this.oSubmitResult;
            }
            set
            {
                this.oSubmitResult = value;
            }
        }
    }







    public class SubmitResultP
    {

        public enum enumoutcome { Approve, Decline };
        public enum enumstatus { SUCCESS, FAILURE };

        private string applicationIDField;

        private enumstatus statusField;

        private string errorDescriptionField;

        private enumoutcome outcomeField;

        private enumoutcome reasonField;

        private string consumerNumberField;

        private string clientReferenceField;

        private string sBCScoreIndicatorField;

        private string sBCScoreField;

        private decimal a002Field;

        private decimal a003Field;

        private decimal a011Field;

        private decimal a012Field;

        private decimal a015Field;

        private decimal a021Field;

        private decimal a034Field;

        private decimal a035Field;

        private decimal a115Field;

        private decimal aT001Field;

        private decimal aT025Field;

        private decimal aT092Field;

        private decimal aT095Field;

        private decimal aT108Field;

        private decimal aT120Field;

        private decimal aT152Field;

        private decimal aT160Field;

        private decimal aT182Field;

        private decimal iN041Field;

        private decimal mL001Field;

        private decimal mL006Field;

        private decimal mL090Field;

        private decimal mL097Field;

        private decimal mL103Field;

        private decimal rE002Field;

        private decimal rE105Field;

        private decimal rE150Field;

        private decimal rE159Field;

        private decimal rE164Field;

        private BureauResponseP bureauResponseField;



        public string ApplicationID
        {
            get
            {
                return this.applicationIDField;
            }
            set
            {
                this.applicationIDField = value;
            }
        }



        public enumstatus Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }



        public string ErrorDescription
        {
            get
            {
                return this.errorDescriptionField;
            }
            set
            {
                this.errorDescriptionField = value;
            }
        }



        public enumoutcome Outcome
        {
            get
            {
                return this.outcomeField;
            }
            set
            {
                this.outcomeField = value;
            }
        }



        public enumoutcome Reason
        {
            get
            {
                return this.reasonField;
            }
            set
            {
                this.reasonField = value;
            }
        }



        public string ConsumerNumber
        {
            get
            {
                return this.consumerNumberField;
            }
            set
            {
                this.consumerNumberField = value;
            }
        }



        public string ClientReference
        {
            get
            {
                return this.clientReferenceField;
            }
            set
            {
                this.clientReferenceField = value;
            }
        }



        public string SBCScoreIndicator
        {
            get
            {
                return this.sBCScoreIndicatorField;
            }
            set
            {
                this.sBCScoreIndicatorField = value;
            }
        }



        public string SBCScore
        {
            get
            {
                return this.sBCScoreField;
            }
            set
            {
                this.sBCScoreField = value;
            }
        }



        public decimal A002
        {
            get
            {
                return this.a002Field;
            }
            set
            {
                this.a002Field = value;
            }
        }



        public decimal A003
        {
            get
            {
                return this.a003Field;
            }
            set
            {
                this.a003Field = value;
            }
        }



        public decimal A011
        {
            get
            {
                return this.a011Field;
            }
            set
            {
                this.a011Field = value;
            }
        }



        public decimal A012
        {
            get
            {
                return this.a012Field;
            }
            set
            {
                this.a012Field = value;
            }
        }



        public decimal A015
        {
            get
            {
                return this.a015Field;
            }
            set
            {
                this.a015Field = value;
            }
        }



        public decimal A021
        {
            get
            {
                return this.a021Field;
            }
            set
            {
                this.a021Field = value;
            }
        }



        public decimal A034
        {
            get
            {
                return this.a034Field;
            }
            set
            {
                this.a034Field = value;
            }
        }



        public decimal A035
        {
            get
            {
                return this.a035Field;
            }
            set
            {
                this.a035Field = value;
            }
        }



        public decimal A115
        {
            get
            {
                return this.a115Field;
            }
            set
            {
                this.a115Field = value;
            }
        }



        public decimal AT001
        {
            get
            {
                return this.aT001Field;
            }
            set
            {
                this.aT001Field = value;
            }
        }



        public decimal AT025
        {
            get
            {
                return this.aT025Field;
            }
            set
            {
                this.aT025Field = value;
            }
        }



        public decimal AT092
        {
            get
            {
                return this.aT092Field;
            }
            set
            {
                this.aT092Field = value;
            }
        }



        public decimal AT095
        {
            get
            {
                return this.aT095Field;
            }
            set
            {
                this.aT095Field = value;
            }
        }



        public decimal AT108
        {
            get
            {
                return this.aT108Field;
            }
            set
            {
                this.aT108Field = value;
            }
        }



        public decimal AT120
        {
            get
            {
                return this.aT120Field;
            }
            set
            {
                this.aT120Field = value;
            }
        }



        public decimal AT152
        {
            get
            {
                return this.aT152Field;
            }
            set
            {
                this.aT152Field = value;
            }
        }



        public decimal AT160
        {
            get
            {
                return this.aT160Field;
            }
            set
            {
                this.aT160Field = value;
            }
        }



        public decimal AT182
        {
            get
            {
                return this.aT182Field;
            }
            set
            {
                this.aT182Field = value;
            }
        }



        public decimal IN041
        {
            get
            {
                return this.iN041Field;
            }
            set
            {
                this.iN041Field = value;
            }
        }



        public decimal ML001
        {
            get
            {
                return this.mL001Field;
            }
            set
            {
                this.mL001Field = value;
            }
        }



        public decimal ML006
        {
            get
            {
                return this.mL006Field;
            }
            set
            {
                this.mL006Field = value;
            }
        }



        public decimal ML090
        {
            get
            {
                return this.mL090Field;
            }
            set
            {
                this.mL090Field = value;
            }
        }



        public decimal ML097
        {
            get
            {
                return this.mL097Field;
            }
            set
            {
                this.mL097Field = value;
            }
        }



        public decimal ML103
        {
            get
            {
                return this.mL103Field;
            }
            set
            {
                this.mL103Field = value;
            }
        }



        public decimal RE002
        {
            get
            {
                return this.rE002Field;
            }
            set
            {
                this.rE002Field = value;
            }
        }



        public decimal RE105
        {
            get
            {
                return this.rE105Field;
            }
            set
            {
                this.rE105Field = value;
            }
        }



        public decimal RE150
        {
            get
            {
                return this.rE150Field;
            }
            set
            {
                this.rE150Field = value;
            }
        }



        public decimal RE159
        {
            get
            {
                return this.rE159Field;
            }
            set
            {
                this.rE159Field = value;
            }
        }



        public decimal RE164
        {
            get
            {
                return this.rE164Field;
            }
            set
            {
                this.rE164Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("BureauResponse", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public BureauResponseP BureauResponse
        {
            get
            {
                return this.bureauResponseField;
            }
            set
            {
                this.bureauResponseField = value;
            }
        }
    }







    public class BureauResponseP
    {
        public enum enumresponseStatus { SUCCESS, FAILURE };

        private string rawDataField;

        private enumresponseStatus responseStatusField;

        private string errorCodeField;

        private string errorMessageField;

        private DateTime processingStartDateField = DateTime.Now;

        private double processingTimeSecsField;

        private string uniqueRefGuidField;

        [XmlArray("AddressNA07")]
        [XmlArrayItem("AddressNA07")]
        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AddressNA07[] addressNA07Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AddressNA08[] addressNA08Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AddressVerificationNR01[] addressVerificationNR01Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AffordabilityAF01[] affordabilityAF01Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AffordStandardBatchCharsXB01[] affordStandardBatchCharsXB01Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AggregateAG01[] aggregateAG01Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AggregateAG02[] aggregateAG02Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AggregateNX05[] aggregateNX05Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AggregateNX09[] aggregateNX09Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AggregateNX33[] aggregateNX33Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AKANamesNK04[] aKANamesNK04Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AlertDetailC3[] alertDetailC3Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AlertDetailQM[] alertDetailQMField;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AlertDetailQE[] alertDetailQEField;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AlertDetailQC[] alertDetailQCField;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AlertDetailQD[] alertDetailQDField;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AlertDetailQW[] alertDetailQWField;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AlertDetailQP[] alertDetailQPField;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AlertDetailQN[] alertDetailQNField;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AlertDetailQV[] alertDetailQVField;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AlertDetailQJ[] alertDetailQJField;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AlertDetailC4[] alertDetailC4Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.BCCBC01[] bCCBC01Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.BCCBC03[] bCCBC03Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.BCCBC04[] bCCBC04Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.BCCC1[] bCCC1Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.BCCC2[] bCCC2Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.CCASummaryMX01[] cCASummaryMX01Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.CellphoneValidationNZ01[] cellphoneValidationNZ01Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.ConsEnqTransInfo0102[] consEnqTransInfo0102Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.ConsumerCountersNC04[] consumerCountersNC04Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.ConsumerDeedsDD01[] consumerDeedsDD01Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.ConsumerInfoNO04[] consumerInfoNO04Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.ConsumerInfoNO05[] consumerInfoNO05Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.ConsumerMiscellaneousMS[] consumerMiscellaneousMSField;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.ConsumerNumberFrequencyNY01[] consumerNumberFrequencyNY01Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.ConsumerTelephoneHistoryNW01[] consumerTelephoneHistoryNW01Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.CPAAccountInformationExtendedP8[] cPAAccountInformationExtendedP8Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.EmpiricaEM04[] EmpiricaEM04Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.DebtReviewDR01[] debtReviewDR01Field;

        [XmlArray("DefaultD701Part1")]
        [XmlArrayItem("DefaultD701Part1")]
        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.DefaultD701Part1[] defaultsD701Part1Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.DefaultD701Part2[] defaultsD701Part2Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.DefaultsND07[] defaultsND07Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.DisputeIndicatorDI01[] disputeIndicatorDI01Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.DriversLicenceUY01[] driversLicenceUY01Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.EchoData0001[] echoData0001Field;

        //        private EmpiricaEM04 empiricaEM04Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.EmpiricaEM05[] empiricaEM05Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.EmpiricaEM07[] empiricaEM07Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.EmpiricaEX[] empiricaEXField;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.EmploymentNM04[] employmentNM04Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.EnquiriesNE09[] enquiriesNE09Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.EnquiryNE50[] enquiriesNE50Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.FSMS201[] fSMS201Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.FraudScoreFS01[] fraudScoreFS01Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.HawkHA01[] hawkHA01Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.HawkHI01[] hawkHI01Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.HawkNH05[] hawkNH05Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.IdvNI01[] idvNI01Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.IdvNI02[] idvNI02Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.IdvNI03[] idvNI03Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.IncomeEstimatorT101[] incomeEstimatorT101Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.IncomeEstimatorT102[] incomeEstimatorT102Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.InsuranceClaimsUA01[] insuranceClaimsUA01Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.InsuranceClaimsUB01[] insuranceClaimsUB01Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.InsuranceClaimsUC01[] insuranceClaimsUC01Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.InsuranceClaimsUI01[] insuranceClaimsUI01Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.IndividualTrace04[] individualTrace04Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.IndividualTrace05[] individualTrace05Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.JudgementsNJ06[] judgementsNJ06Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.JudgementsNJ07[] judgementsNJ07Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.NLRAccountInformationMP01[] nLRAccountInformationMP01Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.NLRAccountInformationMP03[] nLRAccountInformationMP03Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.NLRAccountInformationM701[] nLRAccountsInformationM701Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.NLRAccountInformationM703[] nLRAccountsInformationM703Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.NLRAccountInformationExtendedM8[] nLRAccountInformationExtendedM8Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.NLRConfirmationMR01[] nLRConfirmationMR01Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.NLRConfirmationMZ01[] nLRConfirmationMZ01Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.NLRCounterSeqmentMC01[] nLRCounterSeqmentMC01Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.NLREnquiriesME01[] nLREnquiriesME01Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.NLREnquiryME50[] nLREnquiriesME50Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.NLRSummaryMY01[] nLRSummaryMY01Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.NoticesNN08[] noticesNN08Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.NoticesNN10[] noticesNN10Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.NoticesNN07[] noticesNN07Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.PaymentProfileNP09[] paymentProfileNP09Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.PaymentProfileNP11[] paymentProfileNP11Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.PaymentProfileNP15[] paymentProfileNP15Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.PaymentProfileP601[] paymentProfilesP601Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.PaymentProfileP701[] paymentProfilesP701Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.PaymentProfileP702[] paymentProfilesP702Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.PaymentProfileP703[] paymentProfilesP703Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.PaymentProfileP704[] paymentProfilesP704Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.PaymentProfileP705[] paymentProfilesP705Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.SAFPSNF01[] sAFPSNF01Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.ScoreCardAC01[] scoreCardAC01Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.ScoreCardBX01[] scoreCardBX01Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.ScoreCardBX03[] scoreCardBX03Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.ScorecardS101[] scorecardS101Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.StandardBatchCharsB101[] standardBatchCharsB101Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.StandardBatchCharsFA01[] standardBatchCharsFA01Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.StandardBatchCharsFA02[] standardBatchCharsFA02Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.StandardBatchCharsFA03[] standardBatchCharsFA03Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.StandardBatchCharsSB01[] standardBatchCharsSB01Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.StandardBatchCharsSB04[] standardBatchCharsSB04Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.StandardBatchCharsSB07[] standardBatchCharsSB07Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.StandardBatchCharsSB13[] standardBatchCharsSB13Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.StandardBatchCharsSB18[] standardBatchCharsSB18Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.StandardBatchCharsSB19[] standardBatchCharsSB19Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.StandardBatchCharsSB25[] standardBatchCharsSB25Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.StandardBatchCharsSB27[] standardBatchCharsSB27Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.StandardBatchCharsSB30[] standardBatchCharsSB30Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.StandardBatchCharsSB33[] standardBatchCharsSB33Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.StandardBatchCharsSB37[] standardBatchCharsSB37Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.StandardBatchCharsSB40[] standardBatchCharsSB40Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.StandardBatchCharsSB99[] standardBatchCharsSB99Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.SuburbCodeGR01[] suburbCodesGR01Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.TraceAlertsNT04[] traceAlertsNT04Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.TraceAlertsNT06[] traceAlertsNT06Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.TraceAlertsNT07[] traceAlertsNT07Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.UniqueITCRef[] uniqueITCRefField;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.V1Segment[] v1SegmentField;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.VehicleDataUZ01[] vehicleDataUZField;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.DebtCounsellingDC01[] debtCounsellingDC01Field;

        private XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.PaymentProfileWMP01[] paymentProfileWMP01Field;

        public string RawData
        {
            get
            {
                return this.rawDataField;
            }
            set
            {
                this.rawDataField = value;
            }
        }



        public enumresponseStatus ResponseStatus
        {
            get
            {
                return this.responseStatusField;
            }
            set
            {
                this.responseStatusField = value;
            }
        }



        public string ErrorCode
        {
            get
            {
                return this.errorCodeField;
            }
            set
            {
                this.errorCodeField = value;
            }
        }



        public string ErrorMessage
        {
            get
            {
                return this.errorMessageField;
            }
            set
            {
                this.errorMessageField = value;
            }
        }



        public DateTime ProcessingStartDate
        {
            get
            {
                return this.processingStartDateField;
            }
            set
            {
                this.processingStartDateField = value;
            }
        }



        public double ProcessingTimeSecs
        {
            get
            {
                return this.processingTimeSecsField;
            }
            set
            {
                this.processingTimeSecsField = value;
            }
        }



        public string UniqueRefGuid
        {
            get
            {
                return this.uniqueRefGuidField;
            }
            set
            {
                this.uniqueRefGuidField = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AddressNA07")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AddressNA07[] AddressNA07
        {
            get
            {
                return this.addressNA07Field;
            }
            set
            {
                this.addressNA07Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AddressNA08")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AddressNA08[] AddressNA08
        {
            get
            {
                return this.addressNA08Field;
            }
            set
            {
                this.addressNA08Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AddressVerificationNR01", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AddressVerificationNR01[] AddressVerificationNR01
        {
            get
            {
                return this.addressVerificationNR01Field;
            }
            set
            {
                this.addressVerificationNR01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AffordabilityAF01", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AffordabilityAF01[] AffordabilityAF01
        {
            get
            {
                return this.affordabilityAF01Field;
            }
            set
            {
                this.affordabilityAF01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AffordStandardBatchCharsXB01", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AffordStandardBatchCharsXB01[] AffordStandardBatchCharsXB01
        {
            get
            {
                return this.affordStandardBatchCharsXB01Field;
            }
            set
            {
                this.affordStandardBatchCharsXB01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AggregateAG01", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AggregateAG01[] AggregateAG01
        {
            get
            {
                return this.aggregateAG01Field;
            }
            set
            {
                this.aggregateAG01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AggregateAG02", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AggregateAG02[] AggregateAG02
        {
            get
            {
                return this.aggregateAG02Field;
            }
            set
            {
                this.aggregateAG02Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AggregateNX05", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AggregateNX05[] AggregateNX05
        {
            get
            {
                return this.aggregateNX05Field;
            }
            set
            {
                this.aggregateNX05Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AggregateNX09", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AggregateNX09[] AggregateNX09
        {
            get
            {
                return this.aggregateNX09Field;
            }
            set
            {
                this.aggregateNX09Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AggregateNX33", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AggregateNX33[] AggregateNX33
        {
            get
            {
                return this.aggregateNX33Field;
            }
            set
            {
                this.aggregateNX33Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AKANamesNK04")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AKANamesNK04[] AKANamesNK04
        {
            get
            {
                return this.aKANamesNK04Field;
            }
            set
            {
                this.aKANamesNK04Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AlertDetailC3")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AlertDetailC3[] AlertDetailC3
        {
            get
            {
                return this.alertDetailC3Field;
            }
            set
            {
                this.alertDetailC3Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AlertDetailQM")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AlertDetailQM[] AlertDetailQM
        {
            get
            {
                return this.alertDetailQMField;
            }
            set
            {
                this.alertDetailQMField = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AlertDetailQE")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AlertDetailQE[] AlertDetailQE
        {
            get
            {
                return this.alertDetailQEField;
            }
            set
            {
                this.alertDetailQEField = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AlertDetailQC")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AlertDetailQC[] AlertDetailQC
        {
            get
            {
                return this.alertDetailQCField;
            }
            set
            {
                this.alertDetailQCField = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AlertDetailQD")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AlertDetailQD[] AlertDetailQD
        {
            get
            {
                return this.alertDetailQDField;
            }
            set
            {
                this.alertDetailQDField = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AlertDetailQW")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AlertDetailQW[] AlertDetailQW
        {
            get
            {
                return this.alertDetailQWField;
            }
            set
            {
                this.alertDetailQWField = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AlertDetailQP")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AlertDetailQP[] AlertDetailQP
        {
            get
            {
                return this.alertDetailQPField;
            }
            set
            {
                this.alertDetailQPField = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AlertDetailQN")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AlertDetailQN[] AlertDetailQN
        {
            get
            {
                return this.alertDetailQNField;
            }
            set
            {
                this.alertDetailQNField = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AlertDetailQV")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AlertDetailQV[] AlertDetailQV
        {
            get
            {
                return this.alertDetailQVField;
            }
            set
            {
                this.alertDetailQVField = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AlertDetailQJ")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AlertDetailQJ[] AlertDetailQJ
        {
            get
            {
                return this.alertDetailQJField;
            }
            set
            {
                this.alertDetailQJField = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AlertDetailC4", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.AlertDetailC4[] AlertDetailC4
        {
            get
            {
                return this.alertDetailC4Field;
            }
            set
            {
                this.alertDetailC4Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("BCCBC01", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.BCCBC01[] BCCBC01
        {
            get
            {
                return this.bCCBC01Field;
            }
            set
            {
                this.bCCBC01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("BCCBC03", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.BCCBC03[] BCCBC03
        {
            get
            {
                return this.bCCBC03Field;
            }
            set
            {
                this.bCCBC03Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("BCCBC04", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.BCCBC04[] BCCBC04
        {
            get
            {
                return this.bCCBC04Field;
            }
            set
            {
                this.bCCBC04Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("BCCC1", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.BCCC1[] BCCC1
        {
            get
            {
                return this.bCCC1Field;
            }
            set
            {
                this.bCCC1Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("BCCC2", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.BCCC2[] BCCC2
        {
            get
            {
                return this.bCCC2Field;
            }
            set
            {
                this.bCCC2Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("CCASummaryMX01", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.CCASummaryMX01[] CCASummaryMX01
        {
            get
            {
                return this.cCASummaryMX01Field;
            }
            set
            {
                this.cCASummaryMX01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("CellphoneValidationNZ01")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.CellphoneValidationNZ01[] CellphoneValidationNZ01
        {
            get
            {
                return this.cellphoneValidationNZ01Field;
            }
            set
            {
                this.cellphoneValidationNZ01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("ConsEnqTransInfo0102", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.ConsEnqTransInfo0102[] ConsEnqTransInfo0102
        {
            get
            {
                return this.consEnqTransInfo0102Field;
            }
            set
            {
                this.consEnqTransInfo0102Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("ConsumerCountersNC04", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.ConsumerCountersNC04[] ConsumerCountersNC04
        {
            get
            {
                return this.consumerCountersNC04Field;
            }
            set
            {
                this.consumerCountersNC04Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("ConsumerDeedsDD01")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.ConsumerDeedsDD01[] ConsumerDeedsDD01
        {
            get
            {
                return this.consumerDeedsDD01Field;
            }
            set
            {
                this.consumerDeedsDD01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("ConsumerInfoNO04", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.ConsumerInfoNO04[] ConsumerInfoNO04
        {
            get
            {
                return this.consumerInfoNO04Field;
            }
            set
            {
                this.consumerInfoNO04Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("ConsumerInfoNO05", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.ConsumerInfoNO05[] ConsumerInfoNO05
        {
            get
            {
                return this.consumerInfoNO05Field;
            }
            set
            {
                this.consumerInfoNO05Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("ConsumerMiscellaneousMS")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.ConsumerMiscellaneousMS[] ConsumerMiscellaneousMS
        {
            get
            {
                return this.consumerMiscellaneousMSField;
            }
            set
            {
                this.consumerMiscellaneousMSField = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("ConsumerNumberFrequencyNY01")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.ConsumerNumberFrequencyNY01[] ConsumerNumberFrequencyNY01
        {
            get
            {
                return this.consumerNumberFrequencyNY01Field;
            }
            set
            {
                this.consumerNumberFrequencyNY01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("ConsumerTelephoneHistoryNW01", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.ConsumerTelephoneHistoryNW01[] ConsumerTelephoneHistoryNW01
        {
            get
            {
                return this.consumerTelephoneHistoryNW01Field;
            }
            set
            {
                this.consumerTelephoneHistoryNW01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("CPAAccountInformationExtendedP8")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.CPAAccountInformationExtendedP8[] CPAAccountInformationExtendedP8
        {
            get
            {
                return this.cPAAccountInformationExtendedP8Field;
            }
            set
            {
                this.cPAAccountInformationExtendedP8Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("EmpiricaEM04", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.EmpiricaEM04[] EmpiricaEM04
        {
            get
            {
                return this.EmpiricaEM04Field;
            }
            set
            {
                this.EmpiricaEM04Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("DebtReviewDR01")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.DebtReviewDR01[] DebtReviewDR01
        {
            get
            {
                return this.debtReviewDR01Field;
            }
            set
            {
                this.debtReviewDR01Field = value;
            }
        }


        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayItemAttribute("DefaultD701Part1", typeof(DefaultD701Part1), Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.DefaultD701Part1[] DefaultsD701Part1
        {
            get
            {
                return this.defaultsD701Part1Field;
            }
            set
            {
                this.defaultsD701Part1Field = value;
            }
        }


        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayItemAttribute("DefaultD701Part2", typeof(DefaultD701Part2), Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.DefaultD701Part2[] DefaultsD701Part2
        {
            get
            {
                return this.defaultsD701Part2Field;
            }
            set
            {
                this.defaultsD701Part2Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("DefaultsND07")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.DefaultsND07[] DefaultsND07
        {
            get
            {
                return this.defaultsND07Field;
            }
            set
            {
                this.defaultsND07Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("DisputeIndicatorDI01", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.DisputeIndicatorDI01[] DisputeIndicatorDI01
        {
            get
            {
                return this.disputeIndicatorDI01Field;
            }
            set
            {
                this.disputeIndicatorDI01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("DriversLicenceUY01")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.DriversLicenceUY01[] DriversLicenceUY01
        {
            get
            {
                return this.driversLicenceUY01Field;
            }
            set
            {
                this.driversLicenceUY01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("EchoData0001", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.EchoData0001[] EchoData0001
        {
            get
            {
                return this.echoData0001Field;
            }
            set
            {
                this.echoData0001Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("EmpiricaEM04", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        //public EmpiricaEM04 EmpiricaScoreEM04
        //{
        //    get
        //    {
        //        return this.empiricaEM04Field;
        //    }
        //    set
        //    {
        //        this.empiricaEM04Field = value;
        //    }
        //}


        //[System.Xml.Serialization.XmlElementAttribute("EmpiricaEM05", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.EmpiricaEM05[] EmpiricaEM05
        {
            get
            {
                return this.empiricaEM05Field;
            }
            set
            {
                this.empiricaEM05Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("EmpiricaEM07", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.EmpiricaEM07[] EmpiricaEM07
        {
            get
            {
                return this.empiricaEM07Field;
            }
            set
            {
                this.empiricaEM07Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("EmpiricaEX")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.EmpiricaEX[] EmpiricaEX
        {
            get
            {
                return this.empiricaEXField;
            }
            set
            {
                this.empiricaEXField = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("EmploymentNM04")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.EmploymentNM04[] EmploymentNM04
        {
            get
            {
                return this.employmentNM04Field;
            }
            set
            {
                this.employmentNM04Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("EnquiriesNE09")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.EnquiriesNE09[] EnquiriesNE09
        {
            get
            {
                return this.enquiriesNE09Field;
            }
            set
            {
                this.enquiriesNE09Field = value;
            }
        }


        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayItemAttribute("EnquiryNE50", typeof(EnquiryNE50), Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.EnquiryNE50[] EnquiriesNE50
        {
            get
            {
                return this.enquiriesNE50Field;
            }
            set
            {
                this.enquiriesNE50Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("FSMS201", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.FSMS201[] FSMS201
        {
            get
            {
                return this.fSMS201Field;
            }
            set
            {
                this.fSMS201Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("FraudScoreFS01", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.FraudScoreFS01[] FraudScoreFS01
        {
            get
            {
                return this.fraudScoreFS01Field;
            }
            set
            {
                this.fraudScoreFS01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("HawkHA01")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.HawkHA01[] HawkHA01
        {
            get
            {
                return this.hawkHA01Field;
            }
            set
            {
                this.hawkHA01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("HawkHI01")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.HawkHI01[] HawkHI01
        {
            get
            {
                return this.hawkHI01Field;
            }
            set
            {
                this.hawkHI01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("HawkNH05", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.HawkNH05[] HawkNH05
        {
            get
            {
                return this.hawkNH05Field;
            }
            set
            {
                this.hawkNH05Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("IdvNI01", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.IdvNI01[] IdvNI01
        {
            get
            {
                return this.idvNI01Field;
            }
            set
            {
                this.idvNI01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("IdvNI02", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.IdvNI02[] IdvNI02
        {
            get
            {
                return this.idvNI02Field;
            }
            set
            {
                this.idvNI02Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("IdvNI03", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.IdvNI03[] IdvNI03
        {
            get
            {
                return this.idvNI03Field;
            }
            set
            {
                this.idvNI03Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("IncomeEstimatorT101", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.IncomeEstimatorT101[] IncomeEstimatorT101
        {
            get
            {
                return this.incomeEstimatorT101Field;
            }
            set
            {
                this.incomeEstimatorT101Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("IncomeEstimatorT102", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.IncomeEstimatorT102[] IncomeEstimatorT102
        {
            get
            {
                return this.incomeEstimatorT102Field;
            }
            set
            {
                this.incomeEstimatorT102Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("InsuranceClaimsUA01")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.InsuranceClaimsUA01[] InsuranceClaimsUA01
        {
            get
            {
                return this.insuranceClaimsUA01Field;
            }
            set
            {
                this.insuranceClaimsUA01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("InsuranceClaimsUB01")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.InsuranceClaimsUB01[] InsuranceClaimsUB01
        {
            get
            {
                return this.insuranceClaimsUB01Field;
            }
            set
            {
                this.insuranceClaimsUB01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("InsuranceClaimsUC01")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.InsuranceClaimsUC01[] InsuranceClaimsUC01
        {
            get
            {
                return this.insuranceClaimsUC01Field;
            }
            set
            {
                this.insuranceClaimsUC01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("InsuranceClaimsUI01")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.InsuranceClaimsUI01[] InsuranceClaimsUI01
        {
            get
            {
                return this.insuranceClaimsUI01Field;
            }
            set
            {
                this.insuranceClaimsUI01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("IndividualTrace04")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.IndividualTrace04[] IndividualTrace04
        {
            get
            {
                return this.individualTrace04Field;
            }
            set
            {
                this.individualTrace04Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("IndividualTrace05")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.IndividualTrace05[] IndividualTrace05
        {
            get
            {
                return this.individualTrace05Field;
            }
            set
            {
                this.individualTrace05Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("JudgementsNJ06")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.JudgementsNJ06[] JudgementsNJ06
        {
            get
            {
                return this.judgementsNJ06Field;
            }
            set
            {
                this.judgementsNJ06Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("JudgementsNJ07")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.JudgementsNJ07[] JudgementsNJ07
        {
            get
            {
                return this.judgementsNJ07Field;
            }
            set
            {
                this.judgementsNJ07Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("NLRAccountInformationMP01")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.NLRAccountInformationMP01[] NLRAccountInformationMP01
        {
            get
            {
                return this.nLRAccountInformationMP01Field;
            }
            set
            {
                this.nLRAccountInformationMP01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("NLRAccountInformationMP03")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.NLRAccountInformationMP03[] NLRAccountInformationMP03
        {
            get
            {
                return this.nLRAccountInformationMP03Field;
            }
            set
            {
                this.nLRAccountInformationMP03Field = value;
            }
        }


        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayItemAttribute("NLRAccountInformationM701", typeof(NLRAccountInformationM701), Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.NLRAccountInformationM701[] NLRAccountsInformationM701
        {
            get
            {
                return this.nLRAccountsInformationM701Field;
            }
            set
            {
                this.nLRAccountsInformationM701Field = value;
            }
        }


        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayItemAttribute("NLRAccountInformationM703", typeof(NLRAccountInformationM703), Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.NLRAccountInformationM703[] NLRAccountsInformationM703
        {
            get
            {
                return this.nLRAccountsInformationM703Field;
            }
            set
            {
                this.nLRAccountsInformationM703Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("NLRAccountInformationExtendedM8")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.NLRAccountInformationExtendedM8[] NLRAccountInformationExtendedM8
        {
            get
            {
                return this.nLRAccountInformationExtendedM8Field;
            }
            set
            {
                this.nLRAccountInformationExtendedM8Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("NLRConfirmationMR01", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.NLRConfirmationMR01[] NLRConfirmationMR01
        {
            get
            {
                return this.nLRConfirmationMR01Field;
            }
            set
            {
                this.nLRConfirmationMR01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("NLRConfirmationMZ01", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.NLRConfirmationMZ01[] NLRConfirmationMZ01
        {
            get
            {
                return this.nLRConfirmationMZ01Field;
            }
            set
            {
                this.nLRConfirmationMZ01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("NLRCounterSeqmentMC01")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.NLRCounterSeqmentMC01[] NLRCounterSeqmentMC01
        {
            get
            {
                return this.nLRCounterSeqmentMC01Field;
            }
            set
            {
                this.nLRCounterSeqmentMC01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("NLREnquiriesME01")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.NLREnquiriesME01[] NLREnquiriesME01
        {
            get
            {
                return this.nLREnquiriesME01Field;
            }
            set
            {
                this.nLREnquiriesME01Field = value;
            }
        }


        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayItemAttribute("NLREnquiryME50", typeof(NLREnquiryME50), Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.NLREnquiryME50[] NLREnquiriesME50
        {
            get
            {
                return this.nLREnquiriesME50Field;
            }
            set
            {
                this.nLREnquiriesME50Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("NLRSummaryMY01", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.NLRSummaryMY01[] NLRSummaryMY01
        {
            get
            {
                return this.nLRSummaryMY01Field;
            }
            set
            {
                this.nLRSummaryMY01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("NoticesNN08")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.NoticesNN08[] NoticesNN08
        {
            get
            {
                return this.noticesNN08Field;
            }
            set
            {
                this.noticesNN08Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("NoticesNN10")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.NoticesNN10[] NoticesNN10
        {
            get
            {
                return this.noticesNN10Field;
            }
            set
            {
                this.noticesNN10Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("NoticesNN07")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.NoticesNN07[] NoticesNN07
        {
            get
            {
                return this.noticesNN07Field;
            }
            set
            {
                this.noticesNN07Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("PaymentProfileNP09")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.PaymentProfileNP09[] PaymentProfileNP09
        {
            get
            {
                return this.paymentProfileNP09Field;
            }
            set
            {
                this.paymentProfileNP09Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("PaymentProfileNP11")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.PaymentProfileNP11[] PaymentProfileNP11
        {
            get
            {
                return this.paymentProfileNP11Field;
            }
            set
            {
                this.paymentProfileNP11Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("PaymentProfileNP15")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.PaymentProfileNP15[] PaymentProfileNP15
        {
            get
            {
                return this.paymentProfileNP15Field;
            }
            set
            {
                this.paymentProfileNP15Field = value;
            }
        }


        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayItemAttribute("PaymentProfileP601", typeof(PaymentProfileP601), Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.PaymentProfileP601[] PaymentProfilesP601
        {
            get
            {
                return this.paymentProfilesP601Field;
            }
            set
            {
                this.paymentProfilesP601Field = value;
            }
        }


        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayItemAttribute("PaymentProfileP701", typeof(PaymentProfileP701), Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.PaymentProfileP701[] PaymentProfilesP701
        {
            get
            {
                return this.paymentProfilesP701Field;
            }
            set
            {
                this.paymentProfilesP701Field = value;
            }
        }


        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayItemAttribute("PaymentProfileP702", typeof(PaymentProfileP702), Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.PaymentProfileP702[] PaymentProfilesP702
        {
            get
            {
                return this.paymentProfilesP702Field;
            }
            set
            {
                this.paymentProfilesP702Field = value;
            }
        }


        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayItemAttribute("PaymentProfileP703", typeof(PaymentProfileP703), Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.PaymentProfileP703[] PaymentProfilesP703
        {
            get
            {
                return this.paymentProfilesP703Field;
            }
            set
            {
                this.paymentProfilesP703Field = value;
            }
        }


        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayItemAttribute("PaymentProfileP704", typeof(PaymentProfileP704), Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.PaymentProfileP704[] PaymentProfilesP704
        {
            get
            {
                return this.paymentProfilesP704Field;
            }
            set
            {
                this.paymentProfilesP704Field = value;
            }
        }


        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayItemAttribute("PaymentProfileP705", typeof(PaymentProfileP705), Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.PaymentProfileP705[] PaymentProfilesP705
        {
            get
            {
                return this.paymentProfilesP705Field;
            }
            set
            {
                this.paymentProfilesP705Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("SAFPSNF01")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.SAFPSNF01[] SAFPSNF01
        {
            get
            {
                return this.sAFPSNF01Field;
            }
            set
            {
                this.sAFPSNF01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("ScoreCardAC01", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.ScoreCardAC01[] ScoreCardAC01
        {
            get
            {
                return this.scoreCardAC01Field;
            }
            set
            {
                this.scoreCardAC01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("ScoreCardBX01", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.ScoreCardBX01[] ScoreCardBX01
        {
            get
            {
                return this.scoreCardBX01Field;
            }
            set
            {
                this.scoreCardBX01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("ScoreCardBX03", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.ScoreCardBX03[] ScoreCardBX03
        {
            get
            {
                return this.scoreCardBX03Field;
            }
            set
            {
                this.scoreCardBX03Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("ScorecardS101")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.ScorecardS101[] ScorecardS101
        {
            get
            {
                return this.scorecardS101Field;
            }
            set
            {
                this.scorecardS101Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("StandardBatchCharsB101", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.StandardBatchCharsB101[] StandardBatchCharsB101
        {
            get
            {
                return this.standardBatchCharsB101Field;
            }
            set
            {
                this.standardBatchCharsB101Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("StandardBatchCharsFA01", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.StandardBatchCharsFA01[] StandardBatchCharsFA01
        {
            get
            {
                return this.standardBatchCharsFA01Field;
            }
            set
            {
                this.standardBatchCharsFA01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("StandardBatchCharsFA02", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.StandardBatchCharsFA02[] StandardBatchCharsFA02
        {
            get
            {
                return this.standardBatchCharsFA02Field;
            }
            set
            {
                this.standardBatchCharsFA02Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("StandardBatchCharsFA03", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.StandardBatchCharsFA03[] StandardBatchCharsFA03
        {
            get
            {
                return this.standardBatchCharsFA03Field;
            }
            set
            {
                this.standardBatchCharsFA03Field = value;
            }
        }

        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.StandardBatchCharsSB01[] StandardBatchCharsSB01
        {
            get
            {
                return this.standardBatchCharsSB01Field;
            }
            set
            {
                this.standardBatchCharsSB01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("StandardBatchCharsSB04", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.StandardBatchCharsSB04[] StandardBatchCharsSB04
        {
            get
            {
                return this.standardBatchCharsSB04Field;
            }
            set
            {
                this.standardBatchCharsSB04Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("StandardBatchCharsSB07", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.StandardBatchCharsSB07[] StandardBatchCharsSB07
        {
            get
            {
                return this.standardBatchCharsSB07Field;
            }
            set
            {
                this.standardBatchCharsSB07Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("StandardBatchCharsSB13", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.StandardBatchCharsSB13[] StandardBatchCharsSB13
        {
            get
            {
                return this.standardBatchCharsSB13Field;
            }
            set
            {
                this.standardBatchCharsSB13Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("StandardBatchCharsSB18", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.StandardBatchCharsSB18[] StandardBatchCharsSB18
        {
            get
            {
                return this.standardBatchCharsSB18Field;
            }
            set
            {
                this.standardBatchCharsSB18Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("StandardBatchCharsSB19", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.StandardBatchCharsSB19[] StandardBatchCharsSB19
        {
            get
            {
                return this.standardBatchCharsSB19Field;
            }
            set
            {
                this.standardBatchCharsSB19Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("StandardBatchCharsSB25", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.StandardBatchCharsSB25[] StandardBatchCharsSB25
        {
            get
            {
                return this.standardBatchCharsSB25Field;
            }
            set
            {
                this.standardBatchCharsSB25Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("StandardBatchCharsSB27", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.StandardBatchCharsSB27[] StandardBatchCharsSB27
        {
            get
            {
                return this.standardBatchCharsSB27Field;
            }
            set
            {
                this.standardBatchCharsSB27Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("StandardBatchCharsSB30", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.StandardBatchCharsSB30[] StandardBatchCharsSB30
        {
            get
            {
                return this.standardBatchCharsSB30Field;
            }
            set
            {
                this.standardBatchCharsSB30Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("StandardBatchCharsSB33", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.StandardBatchCharsSB33[] StandardBatchCharsSB33
        {
            get
            {
                return this.standardBatchCharsSB33Field;
            }
            set
            {
                this.standardBatchCharsSB33Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("StandardBatchCharsSB37", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.StandardBatchCharsSB37[] StandardBatchCharsSB37
        {
            get
            {
                return this.standardBatchCharsSB37Field;
            }
            set
            {
                this.standardBatchCharsSB37Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("StandardBatchCharsSB40", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.StandardBatchCharsSB40[] StandardBatchCharsSB40
        {
            get
            {
                return this.standardBatchCharsSB40Field;
            }
            set
            {
                this.standardBatchCharsSB40Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("StandardBatchCharsSB99", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.StandardBatchCharsSB99[] StandardBatchCharsSB99
        {
            get
            {
                return this.standardBatchCharsSB99Field;
            }
            set
            {
                this.standardBatchCharsSB99Field = value;
            }
        }


        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayItemAttribute("SuburbCodeGR01", typeof(SuburbCodeGR01), Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.SuburbCodeGR01[] SuburbCodesGR01
        {
            get
            {
                return this.suburbCodesGR01Field;
            }
            set
            {
                this.suburbCodesGR01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("TraceAlertsNT04")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.TraceAlertsNT04[] TraceAlertsNT04
        {
            get
            {
                return this.traceAlertsNT04Field;
            }
            set
            {
                this.traceAlertsNT04Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("TraceAlertsNT06")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.TraceAlertsNT06[] TraceAlertsNT06
        {
            get
            {
                return this.traceAlertsNT06Field;
            }
            set
            {
                this.traceAlertsNT06Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("TraceAlertsNT07")]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.TraceAlertsNT07[] TraceAlertsNT07
        {
            get
            {
                return this.traceAlertsNT07Field;
            }
            set
            {
                this.traceAlertsNT07Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("UniqueITCRef", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.UniqueITCRef[] UniqueITCRef
        {
            get
            {
                return this.uniqueITCRefField;
            }
            set
            {
                this.uniqueITCRefField = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("V1Segment", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.V1Segment[] V1Segment
        {
            get
            {
                return this.v1SegmentField;
            }
            set
            {
                this.v1SegmentField = value;
            }
        }


        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayItemAttribute("VehicleDataUZ01", typeof(VehicleDataUZ01), Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.VehicleDataUZ01[] VehicleDataUZ
        {
            get
            {
                return this.vehicleDataUZField;
            }
            set
            {
                this.vehicleDataUZField = value;
            }
        }

        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.DebtCounsellingDC01[] DebtCounsellingDC01
        {
            get
            {
                return this.debtCounsellingDC01Field;
            }
            set
            {
                this.debtCounsellingDC01Field = value;
            }
        }

        public XDSPortalLibrary.Entity_Layer.MTNSoapReqResp.PaymentProfileWMP01[] PaymentProfileWMP01
        {
            get
            {
                return this.paymentProfileWMP01Field;
            }
            set
            {
                this.paymentProfileWMP01Field = value;
            }
        }
    }


}