﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XDSPortalLibrary.Entity_Layer
{
    public class IDVerificationIdeco
    {
      private string IDnovalue = string.Empty, strTmpReference = string.Empty, Surnamevalue = string.Empty, Firstnamevalue = string.Empty, externalreferencevalue = string.Empty;
      private string strUserName = string.Empty, strPwd = string.Empty, strsessionID = string.Empty, strAccountRef = string.Empty, strReferenceNo = string.Empty;

        public IDVerificationIdeco()
        {
        }

  

        public string IDno
        {
            get
            {
                if (String.IsNullOrEmpty(IDnovalue))
                    return DBNull.Value.ToString();
                else
                    return this.IDnovalue;
            }
            set
            {
                this.IDnovalue = value;
            }
        }
        public string Surname
        {
            get
            {
                if (String.IsNullOrEmpty(Surnamevalue))
                    return DBNull.Value.ToString();
                else
                    return this.Surnamevalue;
            }
            set
            {
                this.Surnamevalue = value;
            }
        }
        public string Firstname
        {
            get
            {
                if (String.IsNullOrEmpty(Firstnamevalue))
                    return DBNull.Value.ToString();
                else
                    return this.Firstnamevalue;
            }
            set
            {
                this.Firstnamevalue = value;
            }
        }


        public string ExternalReference
        {
            get
            {
                if (String.IsNullOrEmpty(externalreferencevalue))
                    return DBNull.Value.ToString();
                else
                    return this.externalreferencevalue;
            }
            set
            {
                this.externalreferencevalue = value;
            }
        }
        public string TmpReference
        {
            get
            {
                if (String.IsNullOrEmpty(this.strTmpReference))
                    return DBNull.Value.ToString();
                else
                    return this.strTmpReference;
            }
            set
            {
                this.strTmpReference = value;
            }
        }
        public string UserName
        {
            get { return this.strUserName; }
            set { this.strUserName = value; }
        }
        public string Password
        {
            get { return this.strPwd; }
            set { this.strPwd = value; }
        }
        public string SessionID
        {
            get { return this.strsessionID; }
            set { this.strsessionID = value; }
        }
        public string AccountRef
        {
            get { return this.strAccountRef; }
            set { this.strAccountRef = value; }
        }
        public string ReferenceNo
        {
            get { return this.strReferenceNo; }
            set { this.strReferenceNo = value; }
        }



    }
}
