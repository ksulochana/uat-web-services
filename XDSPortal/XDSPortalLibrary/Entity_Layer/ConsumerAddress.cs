﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace XDSPortalLibrary.Entity_Layer
{
   public class ConsumerAddress
    {
       int subscriberIDvalue, productIDvalue, ConsumerIDValue = 0;
       string CountryValue = string.Empty, strTmpReference = string.Empty, ExternalReferencevalue = string.Empty, ProvinceValue = string.Empty, CityValue = string.Empty, SuburbValue = string.Empty, StreetNoValue = string.Empty, PostalcodeValue = string.Empty, StreetNameValue = string.Empty, BuildingNameValue = string.Empty, UnitHouseNoValue = string.Empty, ComplexNameValue = string.Empty, ReferenceNoValue = string.Empty;
       bool ConfirmationChkBoxvalue, IsConsumerMatchingvalue=false;
        DataSet BonusSegmentsDS;
        private bool boolBonusCheck = false;
        private string strdataSegments = string.Empty;
        private string strAssociationCode = string.Empty;

        public string SubscriberAssociationCode
        {
            get { return this.strAssociationCode; }
            set { this.strAssociationCode = value; }
        }

        public string DataSegments
        {
            get { return this.strdataSegments; }
            set { this.strdataSegments = value; }
        }

        public bool BonusCheck
        {
            get { return this.boolBonusCheck; }
            set { this.boolBonusCheck = value; }
        }

        public ConsumerAddress()
        {
        }
        public int ConsumerID
        {
            get
            {
                return this.ConsumerIDValue;
            }
            set
            {
                this.ConsumerIDValue = value;
            }
        }
        public DataSet BonusSegments
        {
            get
            {
                return this.BonusSegmentsDS;
            }
            set
            {
                this.BonusSegmentsDS = value;
            }
        }
        public string ReferenceNo
        {
            get
            {
                if (String.IsNullOrEmpty(this.ReferenceNoValue))
                    return DBNull.Value.ToString();
                else
                    return this.ReferenceNoValue;
            }
            set
            {
                this.ReferenceNoValue = value;
            }
        }

        public string ExternalReference
        {
            get
            {
                if (String.IsNullOrEmpty(ExternalReferencevalue))
                    return DBNull.Value.ToString();
                else
                    return this.ExternalReferencevalue;
            }
            set
            {
                this.ExternalReferencevalue = value;
            }
        }

        public int subscriberID
        {
            get
            {
                if (String.IsNullOrEmpty(this.subscriberIDvalue.ToString()))
                    return Convert.ToInt16(DBNull.Value);
                else
                    return this.subscriberIDvalue;
            }
            set
            {
                this.subscriberIDvalue = value;
            }
        }
        public int ProductID
        {
            get
            {
                if (String.IsNullOrEmpty(this.subscriberIDvalue.ToString()))
                    return Convert.ToInt16(DBNull.Value);
                else
                    return this.productIDvalue;
            }
            set
            {
                this.productIDvalue = value;
            }
        }
        public string Country
        {
            get
            {
                if (String.IsNullOrEmpty(CountryValue))
                    return DBNull.Value.ToString();
                else
                    return this.CountryValue;
            }
            set
            {
                this.CountryValue=value;
            }
        }
        public string Province
        {
            get
            {
                if (String.IsNullOrEmpty(ProvinceValue))
                    return DBNull.Value.ToString();
                else
                    return this.ProvinceValue;
            }
            set
            {
                this.ProvinceValue = value;
            }
        }
        public string City
        {
            get
            {
                if (String.IsNullOrEmpty(CityValue))
                    return DBNull.Value.ToString();
                else
                    return this.CityValue;
            }
            set
            {
                this.CityValue = value;
            }
        }
        public string Suburb
        {
            get
            {
                if (String.IsNullOrEmpty(SuburbValue))
                    return DBNull.Value.ToString();
                else
                    return this.SuburbValue;
            }
            set
            {
                this.SuburbValue = value;
            }
        }
        public string StreetNo
        {
            get
            {
                if (String.IsNullOrEmpty(StreetNoValue))
                    return DBNull.Value.ToString();
                else
                    return this.StreetNoValue;
            }
            set
            {
                this.StreetNoValue = value;
            }
        }
        public string PostalCode
        {
            get
            {
                if (String.IsNullOrEmpty(PostalcodeValue))
                    return DBNull.Value.ToString();
                else
                    return this.PostalcodeValue;
            }
            set
            {
                this.PostalcodeValue = value;
            }
        }
        public string StreetName
        {
            get
            {
                if (String.IsNullOrEmpty(StreetNameValue))
                    return DBNull.Value.ToString();
                else
                    return this.StreetNameValue;
            }
            set
            {
                this.StreetNameValue = value;
            }
        }
        public string BuildingName
        {
            get
            {
                if (String.IsNullOrEmpty(BuildingNameValue))
                    return DBNull.Value.ToString();
                else
                    return this.BuildingNameValue;
            }
            set
            {
                this.BuildingNameValue = value;
            }
        }

        public string UnitHouseNo
        {
            get 
            {
                if (String.IsNullOrEmpty(UnitHouseNoValue))
                    return DBNull.Value.ToString();
                else
                    return this.UnitHouseNoValue; 
            }
            set 
            {
                this.UnitHouseNoValue = value; 
            }
        }

        public string ComplexName
        {
            get 
            {
                if (String.IsNullOrEmpty(ComplexNameValue))
                    return DBNull.Value.ToString();
                else
                    return this.ComplexNameValue; 
            }
            set 
            { 
                this.ComplexNameValue = value; 
            }
        }

        public bool ConfirmationChkBox
        {
            get
            {
                return this.ConfirmationChkBoxvalue;
            }
            set
            {
                this.ConfirmationChkBoxvalue = value;
            }
        }
        public bool IsConsumerMatching
        {
            get
            {
                return this.IsConsumerMatchingvalue;
            }
            set
            {
                this.IsConsumerMatchingvalue = value;
            }
        }
        public string TmpReference
        {
            get
            {
                if (String.IsNullOrEmpty(this.strTmpReference))
                    return DBNull.Value.ToString();
                else
                    return this.strTmpReference;
            }
            set
            {
                this.strTmpReference = value;
            }
        }
    }
}
