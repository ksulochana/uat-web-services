﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace XDSPortalLibrary.Entity_Layer
{
    public class ConsumerDefaultAlertListing
    {

        private string stridno = string.Empty, strPassportNo = string.Empty, strSecondName = string.Empty, strSurName = string.Empty, strFirstName = string.Empty, strGender = string.Empty, strThirdName = string.Empty;
        private string strAccountNo = string.Empty, strSubAccountNo = string.Empty, strAccountype = string.Empty, strLoadedIndicator = string.Empty, strStatusCode = string.Empty;
        string strEffectiveDate = string.Empty,strBirthdate = string.Empty ;
        private Double dbAmount = 0;
        private string strcomments = string.Empty, strHometelephone = string.Empty, strWorkTelephone = string.Empty, strMobile = string.Empty, strAddress1 = string.Empty, strAddress2 = string.Empty, strAddress3 = string.Empty, strAddress4 = string.Empty, strEmailId = string.Empty, strPostalcode = string.Empty, strCreatedbyuser = string.Empty;
        private DataSet BonusSegmentsDS;
        private bool bSMSNotification = false, bEmailNotification = false;
        private int intSystemUserID = 0, intconsumerID = 0;
        private string strClientContactDetails = string.Empty;
        private string strSubscriberName = string.Empty, strAlertType = string.Empty;
        private string strConsumerNotified = string.Empty;
        private string strClientResponsibleToNotify = string.Empty, strDefaultMoveAfter20D = "";


        public string ClientResponsibleToNotify
        {
            get { return this.strClientResponsibleToNotify; }
            set { this.strClientResponsibleToNotify = value; }
        }

        public string ConsumerNotified
        {
            get { return this.strConsumerNotified; }
            set { this.strConsumerNotified = value; }
        }
        public string AlertType
        {
            get { return this.strAlertType; }
            set { this.strAlertType = value; }
        }
        public string SubscriberName
        {
            get { return this.strSubscriberName; }
            set { this.strSubscriberName = value; }
        }

        public string ClientContactDetails
        {
            get { return this.strClientContactDetails; }
            set { this.strClientContactDetails = value; }
        }

        public string ThirdName
        {
            get { return this.strThirdName; }
            set { this.strThirdName = value; }
        }
        public int ConsumerID
        {
            get { return this.intconsumerID; }
            set { this.intconsumerID = value; }
        }

        public int SystemUserID
        {
            get { return this.intSystemUserID; }
            set {this.intSystemUserID = value;}
        }

        public bool SMSNotification
        {
            get { return this.bSMSNotification; }
            set { this.bSMSNotification = value; }
        }
        public bool EmailNotification
        {
            get { return this.bEmailNotification; }
            set { this.bEmailNotification = value; }
        }
        public DataSet BonusSegments
        {
            get {return this.BonusSegmentsDS;}
            set {this.BonusSegmentsDS = value;}
        }
        public string Surname
        {
            get { return this.strSurName; }
            set { this.strSurName = value; }
        }
        public string SecondName
        {
            get { return this.strSecondName; }
            set { this.strSecondName = value; }
        }

        public string Effectivedate
        {
            get { return this.strEffectiveDate; }
            set { strEffectiveDate = value; }
        }

        public string PassportNo
        {
            get { return this.strPassportNo; }
            set { this.strPassportNo = value; }
        }

        public string idno
        {
            get { return this.stridno; }
            set { this.stridno = value; }
        }
        public string FirstName
        {
            get { return this.strFirstName; }
            set { this.strFirstName = value; }
        }
        public string BirthDate
        {
            get { return strBirthdate; }
            set { strBirthdate = value; }
        }
        public string Gender
        {
            get { return this.strGender; }
            set { this.strGender = value; }
        }
        public string AccountNo
        {
            get { return this.strAccountNo; }
            set { this.strAccountNo = value; }
        }
        public string SubAccountNo
        {
            get { return this.strSubAccountNo; }
            set { this.strSubAccountNo = value; }
        }
        public string Accountype
        {
            get { return this.strAccountype; }
            set { this.strAccountype = value; }
        }
        public string LoadedIndicator
        {
            get { return this.strLoadedIndicator; }
            set { this.strLoadedIndicator = value; }
        }
        public string StatusCode
        {
            get { return this.strStatusCode; }
            set { this.strStatusCode = value; }
        }

        public Double Amount
        {
            get { return this.dbAmount; }
            set { this.dbAmount = value; }
        }
        public string comments
        {
            get { return this.strcomments; }
            set { this.strcomments = value; }
        }
        public string Hometelephone
        {
            get { return this.strHometelephone; }
            set { this.strHometelephone = value; }
        }
        public string WorkTelephone
        {
            get { return this.strWorkTelephone; }
            set { this.strWorkTelephone = value; }
        }
        public string Mobile
        {
            get { return this.strMobile; }
            set { this.strMobile = value; }
        }
        public string Address1
        {
            get { return this.strAddress1; }
            set { this.strAddress1 = value; }
        }

        public string Address2
        {
            get { return this.strAddress2; }
            set { this.strAddress2 = value; }
        }

        public string Address3
        {
            get { return this.strAddress3; }
            set { this.strAddress3 = value; }
        }

        public string Address4
        {
            get { return this.strAddress4; }
            set { this.strAddress4 = value; }
        }
        public string EmailId
        {
            get { return this.strEmailId; }
            set { this.strEmailId = value; }
        }
        public string Postalcode
        {
            get { return this.strPostalcode; }
            set { this.strPostalcode = value; }
        }
        public string Createdbyuser
        {
            get { return this.strCreatedbyuser; }
            set { this.strCreatedbyuser = value; }
        }

        public string DefaultMoveAfter20D
        {
            get { return this.strDefaultMoveAfter20D; }
            set { this.strDefaultMoveAfter20D = value; }
        }

    }
}
