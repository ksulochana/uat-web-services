﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XDSPortalLibrary.Entity_Layer
{
    public class AccountVerificationNed
    {
        public AccountVerificationNed()
        {
        }
        private string strIDNo = string.Empty, strAccNo = string.Empty, strAccType = string.Empty, strBankName = string.Empty, strBranchCode = string.Empty, strSurName = string.Empty, strinitials = string.Empty, strExternalReference = string.Empty, strReferenceNo = string.Empty;
        private string strUserName = string.Empty, strPwd = string.Empty, strAccountRef = string.Empty, strContactNumber = string.Empty, strEmailAddress = string.Empty, strsessionID = string.Empty;
        private string strRequesterFirstName = string.Empty, strRequesterSurName = string.Empty;
        private int intkey = 0;
        private int intreturnCode = 0;
        private bool boolBonusCheck = false;
        private string strIDType = string.Empty;
        private int vendorCode = 0;
        private long subscriberID = 0;
        private string source = string.Empty;

        public string IDType
        {
            get { return this.strIDType; }
            set { this.strIDType = value; }
        }
        public bool BonusCheck
        {
            get { return this.boolBonusCheck; }
            set { this.boolBonusCheck = value; }
        }
        public string IDNo
        {
            get { return this.strIDNo; }
            set { this.strIDNo = value; }
        }
        public string AccNo
        {
            get { return this.strAccNo; }
            set { this.strAccNo = value; }
        }
        public string Acctype
        {
            get { return this.strAccType; }
            set { this.strAccType = value; }
        }
        public string BankName
        {
            get { return this.strBankName; }
            set { this.strBankName = value; }
        }
        public string BranchCode
        {
            get { return this.strBranchCode; }
            set { this.strBranchCode = value; }
        }
        public string SurName
        {
            get { return this.strSurName; }
            set { this.strSurName = value; }
        }
        public string Initials
        {
            get { return this.strinitials; }
            set { this.strinitials = value; }
        }
       
        public string AccountRef
        {
            get { return this.strAccountRef; }
            set { this.strAccountRef = value; }
        }
        public string ReferenceNo
        {
            get { return this.strReferenceNo; }
            set { this.strReferenceNo = value; }
        }
        public string ExternalReference
        {
            get { return this.strExternalReference; }
            set { this.strExternalReference = value; }
        }
        public string ContactNumber
        {
            get { return this.strContactNumber; }
            set { this.strContactNumber = value; }
        }
        public string EmailAddress
        {
            get { return this.strEmailAddress; }
            set { this.strEmailAddress = value; }
        }
        public string SessionID
        {
            get { return this.strsessionID; }
            set { this.strsessionID = value; }
        }
        public int Returncode
        {
            get { return this.intreturnCode; }
            set { this.intreturnCode = value; }
        }
        public int Key
        {
            get { return this.intkey; }
            set { this.intkey = value; }

        }
        public string RequesterFirstName
        {
            get { return this.strRequesterFirstName; }
            set { this.strRequesterFirstName = value; }
        }

        public string RequesterSurName
        {
            get { return this.strRequesterSurName; }
            set { this.strRequesterSurName = value; }
        }

        public int VendorCode
        {
            get { return this.vendorCode; }
            set { this.vendorCode = value; }
        }

        public long SubscriberID
        {
            get { return this.subscriberID; }
            set { this.subscriberID = value; }
        }

        public string Source
        {
            get { return this.source; }
            set { this.source = value; }
        }
    }
}
