﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDSPortalLibrary.Entity_Layer
{
    public partial class VettingVInput
    {

        private string connectTicketField;

        private string externalReferenceField;

        private string enquiryReasonField;

        private byte productIdField;

        private string idNumberField;

        private DateTime iDIssueDateField;

        private string passportNoField;

        private DateTime passportExpiryDateField;

        private string passportIssueCountryCodeField;

        private string firstNameField;

        private string surnameField;

        private string genderField;

        private string maritalStatusField;

        private string birthDateField;

        private decimal incomeField;

        private decimal expensesField;

        private string cellphoneNumberField;

        private string homeTelephoneNumberField;

        private string workTelephoneNumberField;

        private string alternateContactNumberField;

        private string emailAddressField;

        private string residentialAddress1Field;

        private string residentialAddress2Field;

        private string residentialAddress3Field;

        private string residentialAddress4Field;

        private string residentialAddressPostalCodeField;

        private string postalAddress1Field;

        private string postalAddress2Field;

        private string postalAddress3Field;

        private string postalAddress4Field;

        private string postalPostalCodeField;

        private string deliveryAddress1Field;

        private string deliveryAddress2Field;

        private string deliveryAddress3Field;

        private string deliveryAddress4Field;

        private string deliveryAddressPostalCodeField;

        private string employerField;

        private string designationField;

        private string bankNameField;

        private string bankBranchCodeField;

        private string bankAccountNumberField;

        private string deviceIDField;

        private string deviceLocationField;

        private bool deviceMaskedField;

        private string deviceMacAddressField;

        private string gPSLocationCoordinatesField;

        private string publicIPAddressField;

        private bool fingerprintAuthenticationNeededField;

        private bool fingerprintAuthenticationPassedField;

        private bool fingerprintAuthenticationSystemErrorField;

        private bool facialRecognitionNeededField;

        private bool facialRecognitionPassedField;

        private bool facialRecognitionSystemErrorField;

        private bool voiceRecognitionNeededField;

        private bool voiceRecognitionPassedField;

        private bool voiceRecognitionSystemErrorField;

        private bool kBANeededField;

        private bool kBAPassedField;

        private bool kBASystemErrorField;

        private int kBAAttemptsField;

        private bool bankAccountFoundField;

        private bool bankAccountActiveField;

        private bool bankAccountAcceptsDebitsField;

        private bool bankAccountOlderThan3MonthsField;

        private bool bankAccountIDMatchedField;

        private bool aVSNeededField;

        private bool aVSPassedField;

        private bool aVSSystemErrorField;

        private bool aVSNotPerformedField;

        private bool aVSCallNotSuccessfulField;

        private DateTime aVSDateField;

        private bool otherAuthenticationNeededField;

        private bool otherAuthenticationPassedField;

        private bool otherAuthenticationSystemErrorField;

        private string otherauthenticationTypeField;

        private string businessProcessField;

        private string yourReferenceField;

        private string voucherCodeField;


        /// <remarks/>
        public string ConnectTicket
        {
            get
            {
                return this.connectTicketField;
            }
            set
            {
                this.connectTicketField = value;
            }
        }

        /// <remarks/>
        public string ExternalReference
        {
            get
            {
                return this.externalReferenceField;
            }
            set
            {
                this.externalReferenceField = value;
            }
        }

        /// <remarks/>
        public string EnquiryReason
        {
            get
            {
                return this.enquiryReasonField;
            }
            set
            {
                this.enquiryReasonField = value;
            }
        }

        /// <remarks/>
        public byte ProductId
        {
            get
            {
                return this.productIdField;
            }
            set
            {
                this.productIdField = value;
            }
        }

        /// <remarks/>
        public string IdNumber
        {
            get
            {
                return this.idNumberField;
            }
            set
            {
                this.idNumberField = value;
            }
        }

        /// <remarks/>
        public DateTime IDIssueDate
        {
            get
            {
                return this.iDIssueDateField;
            }
            set
            {
                this.iDIssueDateField = value;
            }
        }

        /// <remarks/>
        public string PassportNo
        {
            get
            {
                return this.passportNoField;
            }
            set
            {
                this.passportNoField = value;
            }
        }

        /// <remarks/>
        public DateTime PassportExpiryDate
        {
            get
            {
                return this.passportExpiryDateField;
            }
            set
            {
                this.passportExpiryDateField = value;
            }
        }

        /// <remarks/>
        public string PassportIssueCountryCode
        {
            get
            {
                return this.passportIssueCountryCodeField;
            }
            set
            {
                this.passportIssueCountryCodeField = value;
            }
        }

        /// <remarks/>
        public string FirstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        public string Gender
        {
            get
            {
                return this.genderField;
            }
            set
            {
                this.genderField = value;
            }
        }

        /// <remarks/>
        public string MaritalStatus
        {
            get
            {
                return this.maritalStatusField;
            }
            set
            {
                this.maritalStatusField = value;
            }
        }

        /// <remarks/>
        public string BirthDate
        {
            get
            {
                return this.birthDateField;
            }
            set
            {
                this.birthDateField = value;
            }
        }

        /// <remarks/>
        public decimal Income
        {
            get
            {
                return this.incomeField;
            }
            set
            {
                this.incomeField = value;
            }
        }

        /// <remarks/>
        public decimal Expenses
        {
            get
            {
                return this.expensesField;
            }
            set
            {
                this.expensesField = value;
            }
        }

        /// <remarks/>
        public string CellphoneNumber
        {
            get
            {
                return this.cellphoneNumberField;
            }
            set
            {
                this.cellphoneNumberField = value;
            }
        }

        /// <remarks/>
        public string HomeTelephoneNumber
        {
            get
            {
                return this.homeTelephoneNumberField;
            }
            set
            {
                this.homeTelephoneNumberField = value;
            }
        }

        /// <remarks/>
        public string WorkTelephoneNumber
        {
            get
            {
                return this.workTelephoneNumberField;
            }
            set
            {
                this.workTelephoneNumberField = value;
            }
        }

        /// <remarks/>
        public string AlternateContactNumber
        {
            get
            {
                return this.alternateContactNumberField;
            }
            set
            {
                this.alternateContactNumberField = value;
            }
        }

        /// <remarks/>
        public string EmailAddress
        {
            get
            {
                return this.emailAddressField;
            }
            set
            {
                this.emailAddressField = value;
            }
        }

        /// <remarks/>
        public string ResidentialAddress1
        {
            get
            {
                return this.residentialAddress1Field;
            }
            set
            {
                this.residentialAddress1Field = value;
            }
        }

        /// <remarks/>
        public string ResidentialAddress2
        {
            get
            {
                return this.residentialAddress2Field;
            }
            set
            {
                this.residentialAddress2Field = value;
            }
        }

        /// <remarks/>
        public string ResidentialAddress3
        {
            get
            {
                return this.residentialAddress3Field;
            }
            set
            {
                this.residentialAddress3Field = value;
            }
        }

        /// <remarks/>
        public string ResidentialAddress4
        {
            get
            {
                return this.residentialAddress4Field;
            }
            set
            {
                this.residentialAddress4Field = value;
            }
        }

        /// <remarks/>
        public string ResidentialAddressPostalCode
        {
            get
            {
                return this.residentialAddressPostalCodeField;
            }
            set
            {
                this.residentialAddressPostalCodeField = value;
            }
        }

        /// <remarks/>
        public string PostalAddress1
        {
            get
            {
                return this.postalAddress1Field;
            }
            set
            {
                this.postalAddress1Field = value;
            }
        }

        /// <remarks/>
        public string PostalAddress2
        {
            get
            {
                return this.postalAddress2Field;
            }
            set
            {
                this.postalAddress2Field = value;
            }
        }

        /// <remarks/>
        public string PostalAddress3
        {
            get
            {
                return this.postalAddress3Field;
            }
            set
            {
                this.postalAddress3Field = value;
            }
        }

        /// <remarks/>
        public string PostalAddress4
        {
            get
            {
                return this.postalAddress4Field;
            }
            set
            {
                this.postalAddress4Field = value;
            }
        }

        /// <remarks/>
        public string PostalPostalCode
        {
            get
            {
                return this.postalPostalCodeField;
            }
            set
            {
                this.postalPostalCodeField = value;
            }
        }

        /// <remarks/>
        public string DeliveryAddress1
        {
            get
            {
                return this.deliveryAddress1Field;
            }
            set
            {
                this.deliveryAddress1Field = value;
            }
        }

        /// <remarks/>
        public string DeliveryAddress2
        {
            get
            {
                return this.deliveryAddress2Field;
            }
            set
            {
                this.deliveryAddress2Field = value;
            }
        }

        /// <remarks/>
        public string DeliveryAddress3
        {
            get
            {
                return this.deliveryAddress3Field;
            }
            set
            {
                this.deliveryAddress3Field = value;
            }
        }

        /// <remarks/>
        public string DeliveryAddress4
        {
            get
            {
                return this.deliveryAddress4Field;
            }
            set
            {
                this.deliveryAddress4Field = value;
            }
        }

        /// <remarks/>
        public string DeliveryAddressPostalCode
        {
            get
            {
                return this.deliveryAddressPostalCodeField;
            }
            set
            {
                this.deliveryAddressPostalCodeField = value;
            }
        }

        /// <remarks/>
        public string Employer
        {
            get
            {
                return this.employerField;
            }
            set
            {
                this.employerField = value;
            }
        }

        /// <remarks/>
        public string Designation
        {
            get
            {
                return this.designationField;
            }
            set
            {
                this.designationField = value;
            }
        }

        /// <remarks/>
        public string BankName
        {
            get
            {
                return this.bankNameField;
            }
            set
            {
                this.bankNameField = value;
            }
        }

        /// <remarks/>
        public string BankBranchCode
        {
            get
            {
                return this.bankBranchCodeField;
            }
            set
            {
                this.bankBranchCodeField = value;
            }
        }

        /// <remarks/>
        public string BankAccountNumber
        {
            get
            {
                return this.bankAccountNumberField;
            }
            set
            {
                this.bankAccountNumberField = value;
            }
        }

        /// <remarks/>
        public string DeviceID
        {
            get
            {
                return this.deviceIDField;
            }
            set
            {
                this.deviceIDField = value;
            }
        }

        /// <remarks/>
        public string DeviceLocation
        {
            get
            {
                return this.deviceLocationField;
            }
            set
            {
                this.deviceLocationField = value;
            }
        }

        /// <remarks/>
        public bool DeviceMasked
        {
            get
            {
                return this.deviceMaskedField;
            }
            set
            {
                this.deviceMaskedField = value;
            }
        }

        /// <remarks/>
        public string DeviceMacAddress
        {
            get
            {
                return this.deviceMacAddressField;
            }
            set
            {
                this.deviceMacAddressField = value;
            }
        }

        /// <remarks/>
        public string GPSLocationCoordinates
        {
            get
            {
                return this.gPSLocationCoordinatesField;
            }
            set
            {
                this.gPSLocationCoordinatesField = value;
            }
        }

        /// <remarks/>
        public string PublicIPAddress
        {
            get
            {
                return this.publicIPAddressField;
            }
            set
            {
                this.publicIPAddressField = value;
            }
        }

        /// <remarks/>
        public bool FingerprintAuthenticationNeeded
        {
            get
            {
                return this.fingerprintAuthenticationNeededField;
            }
            set
            {
                this.fingerprintAuthenticationNeededField = value;
            }
        }

        /// <remarks/>
        public bool FingerprintAuthenticationPassed
        {
            get
            {
                return this.fingerprintAuthenticationPassedField;
            }
            set
            {
                this.fingerprintAuthenticationPassedField = value;
            }
        }

        /// <remarks/>
        public bool FingerprintAuthenticationSystemError
        {
            get
            {
                return this.fingerprintAuthenticationSystemErrorField;
            }
            set
            {
                this.fingerprintAuthenticationSystemErrorField = value;
            }
        }

        /// <remarks/>
        public bool FacialRecognitionNeeded
        {
            get
            {
                return this.facialRecognitionNeededField;
            }
            set
            {
                this.facialRecognitionNeededField = value;
            }
        }

        /// <remarks/>
        public bool FacialRecognitionPassed
        {
            get
            {
                return this.facialRecognitionPassedField;
            }
            set
            {
                this.facialRecognitionPassedField = value;
            }
        }

        /// <remarks/>
        public bool FacialRecognitionSystemError
        {
            get
            {
                return this.facialRecognitionSystemErrorField;
            }
            set
            {
                this.facialRecognitionSystemErrorField = value;
            }
        }

        /// <remarks/>
        public bool VoiceRecognitionNeeded
        {
            get
            {
                return this.voiceRecognitionNeededField;
            }
            set
            {
                this.voiceRecognitionNeededField = value;
            }
        }

        /// <remarks/>
        public bool VoiceRecognitionPassed
        {
            get
            {
                return this.voiceRecognitionPassedField;
            }
            set
            {
                this.voiceRecognitionPassedField = value;
            }
        }

        /// <remarks/>
        public bool VoiceRecognitionSystemError
        {
            get
            {
                return this.voiceRecognitionSystemErrorField;
            }
            set
            {
                this.voiceRecognitionSystemErrorField = value;
            }
        }

        /// <remarks/>
        public bool KBANeeded
        {
            get
            {
                return this.kBANeededField;
            }
            set
            {
                this.kBANeededField = value;
            }
        }

        /// <remarks/>
        public bool KBAPassed
        {
            get
            {
                return this.kBAPassedField;
            }
            set
            {
                this.kBAPassedField = value;
            }
        }

        /// <remarks/>
        public bool KBASystemError
        {
            get
            {
                return this.kBASystemErrorField;
            }
            set
            {
                this.kBASystemErrorField = value;
            }
        }

        /// <remarks/>
        public int KBAAttempts
        {
            get
            {
                return this.kBAAttemptsField;
            }
            set
            {
                this.kBAAttemptsField = value;
            }
        }

        /// <remarks/>
        public bool BankAccountFound
        {
            get
            {
                return this.bankAccountFoundField;
            }
            set
            {
                this.bankAccountFoundField = value;
            }
        }

        /// <remarks/>
        public bool BankAccountActive
        {
            get
            {
                return this.bankAccountActiveField;
            }
            set
            {
                this.bankAccountActiveField = value;
            }
        }

        /// <remarks/>
        public bool BankAccountAcceptsDebits
        {
            get
            {
                return this.bankAccountAcceptsDebitsField;
            }
            set
            {
                this.bankAccountAcceptsDebitsField = value;
            }
        }

        /// <remarks/>
        public bool BankAccountOlderThan3Months
        {
            get
            {
                return this.bankAccountOlderThan3MonthsField;
            }
            set
            {
                this.bankAccountOlderThan3MonthsField = value;
            }
        }

        /// <remarks/>
        public bool BankAccountIDMatched
        {
            get
            {
                return this.bankAccountIDMatchedField;
            }
            set
            {
                this.bankAccountIDMatchedField = value;
            }
        }

        /// <remarks/>
        public bool AVSNeeded
        {
            get
            {
                return this.aVSNeededField;
            }
            set
            {
                this.aVSNeededField = value;
            }
        }

        /// <remarks/>
        public bool AVSPassed
        {
            get
            {
                return this.aVSPassedField;
            }
            set
            {
                this.aVSPassedField = value;
            }
        }

        /// <remarks/>
        public bool AVSSystemError
        {
            get
            {
                return this.aVSSystemErrorField;
            }
            set
            {
                this.aVSSystemErrorField = value;
            }
        }

        /// <remarks/>
        public bool AVSNotPerformed
        {
            get
            {
                return this.aVSNotPerformedField;
            }
            set
            {
                this.aVSNotPerformedField = value;
            }
        }

        /// <remarks/>
        public bool AVSCallNotSuccessful
        {
            get
            {
                return this.aVSCallNotSuccessfulField;
            }
            set
            {
                this.aVSCallNotSuccessfulField = value;
            }
        }

        /// <remarks/>
        public DateTime AVSDate
        {
            get
            {
                return this.aVSDateField;
            }
            set
            {
                this.aVSDateField = value;
            }
        }

        /// <remarks/>
        public bool OtherAuthenticationNeeded
        {
            get
            {
                return this.otherAuthenticationNeededField;
            }
            set
            {
                this.otherAuthenticationNeededField = value;
            }
        }

        /// <remarks/>
        public bool OtherAuthenticationPassed
        {
            get
            {
                return this.otherAuthenticationPassedField;
            }
            set
            {
                this.otherAuthenticationPassedField = value;
            }
        }

        /// <remarks/>
        public bool OtherAuthenticationSystemError
        {
            get
            {
                return this.otherAuthenticationSystemErrorField;
            }
            set
            {
                this.otherAuthenticationSystemErrorField = value;
            }
        }

        /// <remarks/>
        public string OtherauthenticationType
        {
            get
            {
                return this.otherauthenticationTypeField;
            }
            set
            {
                this.otherauthenticationTypeField = value;
            }
        }

        /// <remarks/>
        public string BusinessProcess
        {
            get
            {
                return this.businessProcessField;
            }
            set
            {
                this.businessProcessField = value;
            }
        }

        /// <remarks/>
        public string YourReference
        {
            get
            {
                return this.yourReferenceField;
            }
            set
            {
                this.yourReferenceField = value;
            }
        }

        /// <remarks/>
        public string VoucherCode
        {
            get
            {
                return this.voucherCodeField;
            }
            set
            {
                this.voucherCodeField = value;
            }
        }


    }
}
