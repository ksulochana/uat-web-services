﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace XDSPortalLibrary.Entity_Layer
{
    class GenerateReportXML
    {

        public GenerateReportXML()
        {
        }
        int SubscriberIDValue = 0, ProductIDValue = 0;//, ConsumerIDValue = 0, CommercialIDValue = 0, DirectorIDValue = 0, HomeAffairsIDValue = 0, propertyDeedIDValue = 0;
        int KeyIDValue = 0;
        string ReferenceNoValue = string.Empty, ExternalReferenceValue = string.Empty,KeyTypeValue = string.Empty;
        DataSet BonusSegmentsDS = new DataSet();
        string strTmpReference = string.Empty;
        private string strdataSegments = string.Empty;

        public string DataSegments
        {
            get { return this.strdataSegments; }
            set { this.strdataSegments = value; }
        }

        public int SubscriberID
        {
            get
            {
                return this.SubscriberIDValue;
            }
            set
            {
                this.SubscriberIDValue = value;
            }
        }
        public int ProductID
        {
            get
            {
                return this.ProductIDValue;
            }
            set
            {
                this.ProductIDValue = value;
            }
        }
        //public int ConsumerID
        //{
        //    get
        //    {
        //        return this.ConsumerIDValue;
        //    }
        //    set
        //    {
        //        this.ConsumerIDValue = value;
        //    }
        //}
        //public int CommercialID
        //{
        //    get
        //    {
        //        return this.CommercialIDValue;
        //    }
        //    set
        //    {
        //        this.CommercialIDValue = value;
        //    }
        //}
        //public int DirectorID
        //{
        //    get
        //    {
        //        return this.DirectorIDValue;
        //    }
        //    set
        //    {
        //        this.DirectorIDValue = value;
        //    }
        //}
        public string ReferenceNo
        {
            get
            {
                if (String.IsNullOrEmpty(this.ReferenceNoValue))
                    return DBNull.Value.ToString();
                else
                    return this.ReferenceNoValue;
            }
            set
            {
                this.ReferenceNoValue = value;
            }
        }

        public string ExternalReference
        {
            get
            {
                if (String.IsNullOrEmpty(this.ExternalReferenceValue))
                    return DBNull.Value.ToString();
                else
                    return this.ExternalReferenceValue;
            }
            set
            {
                this.ExternalReferenceValue = value;
            }
        }
        public string KeyType
        {
            get
            {
                if (String.IsNullOrEmpty(this.KeyTypeValue))
                    return DBNull.Value.ToString();
                else
                    return this.KeyTypeValue;
            }
            set
            {
                this.KeyTypeValue = value;
            }
        }
        public string TmpReference
        {
            get
            {
                if (String.IsNullOrEmpty(this.strTmpReference))
                    return DBNull.Value.ToString();
                else
                    return this.strTmpReference;
            }
            set
            {
                this.strTmpReference = value;
            }
        }
        public DataSet BonusSegments
        {
            get
            {
                return this.BonusSegmentsDS;
            }
            set
            {
                this.BonusSegmentsDS = value;
            }
        }
        //public int HomeAffairsID
        //{
        //    get
        //    {
        //        return this.HomeAffairsIDValue;
        //    }
        //    set
        //    {
        //        this.HomeAffairsIDValue = value;
        //    }
        //}
        //public int PropertyDeedID
        //{
        //    get
        //    {
        //        return this.propertyDeedIDValue;
        //    }
        //    set
        //    {
        //        this.propertyDeedIDValue = value;
        //    }
        //}
        public int KeyID
        {
            get
            {
                return this.KeyIDValue;
            }
            set
            {
                this.KeyIDValue = value;
            }
        }

    }
}
