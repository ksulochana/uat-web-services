﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XDSPortalLibrary.Entity_Layer
{

    public partial class Response
    {
        public Response()
        {
        }
        public enum ResponseStatusEnum
        {
            Single = 1,
            Multiple = 2,
            None = 3,
            Error = 4,
            Bonus = 5,
            Report = 6
        }

        ResponseStatusEnum ResponseStatusValue = ResponseStatusEnum.None;

        string ResponseDatavalue = string.Empty, ResponseKeyTypeValue = string.Empty, ResponseReferenceNovalue = string.Empty, ResponseExternalReferenceValue = string.Empty, urlvalue = string.Empty;      //,ResponseBonusValue=string.Empty; 
        int ResponseKeyValue;
        int _EnquiryID;
        int _EnquiryResultID;
        string strTmpReference = string.Empty;
        private int _EnquiryLogID =0;
        private double _BillingPrice = 0;
        private bool _Billable = false;
        public string URL
        {
            get
            {
                return this.urlvalue;
            }
            set
            {
                this.urlvalue = value;
            }
        }
        private int _ReportID = 0;

        public int ReportID
        {
            get
            {
                return _ReportID;
            }
            set
            {
                this._ReportID = value;
            }
        }
        public int EnquiryID
        {
            get
            {
                return _EnquiryID;
            }
            set
            {
                this._EnquiryID = value;
            }
        }
        public int EnquiryLogID
        {
            get
            {
                return _EnquiryLogID;
            }
            set
            {
                this._EnquiryLogID = value;
            }
        }

        public int EnquiryResultID
        {
            get
            {
                return _EnquiryResultID;
            }
            set
            {
                this._EnquiryResultID = value;
            }
        }


        public ResponseStatusEnum ResponseStatus
        {
            get
            {
                return ResponseStatusValue;
            }
            set
            {
                this.ResponseStatusValue = value;
            }
        }

        public string ResponseData
        {
            get
            {
                return this.ResponseDatavalue;
            }
            set
            {
                this.ResponseDatavalue = value;
            }
        }

        public string ResponseKeyType
        {
            get
            {
                return this.ResponseKeyTypeValue;
            }
            set
            {
                this.ResponseKeyTypeValue = value;
            }
        }

        public int ResponseKey
        {
            get
            {
                return this.ResponseKeyValue;
            }
            set
            {
                this.ResponseKeyValue = value;
            }
        }

        public string ResponseReferenceNo
        {
            get
            {
                return this.ResponseReferenceNovalue;
            }
            set
            {
                this.ResponseReferenceNovalue = value;
            }
        }

        public string ResponseExternalReferenceNo
        {
            get
            {
                return this.ResponseExternalReferenceValue;
            }
            set
            {
                this.ResponseExternalReferenceValue = value;
            }
        }
        //public string ResponseBonus
        //{
        //    get
        //    {
        //        return this.ResponseBonusValue;
        //    }
        //    set
        //    {
        //        this.ResponseBonusValue = value;
        //    }
        //}

        public string TmpReference
        {
            get
            {
                return this.strTmpReference;
            }
            set
            {
                this.strTmpReference = value;
            }
        }
  
   public bool Billable
        { get { return this._Billable; } set { this._Billable = value; } }

   public double BillingPrice
   { get { return this._BillingPrice; } set { this._BillingPrice = value; } }

    }
    
}
