﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDSPortalLibrary.Entity_Layer
{
    public class VettingW
    {
        public VettingW() { }

        public class WResponse
        {
            private Header _EnquiryHeader ;
            private Information _ApplicantInformation;
          
            public Header EnquiryHeader
            {
                get { return this._EnquiryHeader; }
                set { this._EnquiryHeader = value; }
            }         

            public Information ApplicantInformation
            {
                get { return this._ApplicantInformation; }
                set { this._ApplicantInformation = value; }
            }
          
        }

        public class Information
        {
            private string idnumber;
            private string passportnumber;
            private DateTime birthdate;
            private string title;
            private string firstname;
            private string surname;
            private string cellnumber;
            private float creditscore;
           

            public string IDNumber
            {
                get
                {
                    return this.idnumber;
                }
                set { this.idnumber = value; }
            }

            public string Surname
            {
                get
                {
                    return this.surname;
                }
                set { this.surname = value; }
            }

            public string PassportNumber {
                get{
                    return this.passportnumber;
                }
                set{
                    this.passportnumber = value;
                }
            }

            public DateTime BirthDate {
                get{
                    return this.birthdate;
                }
                set{
                    this.birthdate = value;
                }
            }

            public string Title
            {
                get
                {
                    return this.title;
                }
                set { this.title = value; }
            }

            public string FirstName
            {
                get
                {
                    return this.firstname;
                }
                set { this.firstname = value; }
            }

            public string CellNumber
            {
                get
                {
                    return this.cellnumber;
                }
                set { this.cellnumber = value; }
            }

            public float CreditScore
            {
                get
                {
                    return this.creditscore;
                }
                set { this.creditscore = value; }
            }           

        }



        public class Header
        {
            public enum Estatus { SUCCESS, FAILURE };
            private Estatus enquirystatus;
            private string errors=string.Empty;
            private string clientreference=string.Empty;
            private string xdsreference = string.Empty;

          
            public Estatus EnquiryStatus
            {
                get
                {
                    return this.enquirystatus;
                }
                set { this.enquirystatus = value; }
            }

            public string Error
            {
                get
                {
                    return this.errors;
                }
                set { this.errors = value; }
            }

            public string ClientReference
            {
                get
                {
                    return this.clientreference;
                }
                set { this.clientreference = value; }
            }

            public string XDSReference
            {
                get
                {
                    return this.xdsreference;
                }
                set { this.xdsreference = value; }
            }


        }
    }
}
