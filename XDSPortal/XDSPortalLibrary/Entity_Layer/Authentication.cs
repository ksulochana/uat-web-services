﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XDSPortalLibrary.Entity_Layer
{
    public class Authentication
    {
        public int SubscriberID = 0;
        public int SubscriberAuthenticationID = 0;
        public int ConsumerID = 0;
        public int ActionedBySystemUserID = 0;
        public int ProductID = 0;
        public string BranchCode = string.Empty;
        public int PurposeID = 0;
        public string CreatedbyUser = string.Empty;
        public string Subscribername = string.Empty;
        public string SubscribergroupCode = string.Empty;
        public int productAuthenticationQuestionID = 0;
        public string cellularnumber = string.Empty;
        public bool SAFPSFlag = false;
        public string SAFPSMessage = string.Empty;
        public bool OverrideOTP = false;
        public string OverrideOTPReason = "";
        public string EmailAddress = "";
    }
}
