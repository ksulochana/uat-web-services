﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDSPortalLibrary.Entity_Layer
{
   public class SimRequest
    {
        public SimRequest() { }

        public string ConnectTicket { get; set; }
        public int EnquiryID { get; set; }
        public int EnquiryResultID { get; set; }
        public int ProductID { get; set; }
        public AmountChange[] AmountChanges { get; set; }
    }

    public class AmountChange
    {
        public int ConsumerAccountID { get; set; }
        public double NewAmount { get; set; }
    }
}
