﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDSPortalLibrary.Entity_Layer
{
    public class BioMetricRequest
    {
        public BioMetricRequest()
        { }
       public string ConnectTicket { get; set; }
        public int EnquiryID { get; set; }
        public int EnquiryResultID { get; set; }
        public int ProductID { get; set; }
        public string Errors { get; set; }
        public string Finger1WsqImage { get; set; }
        public string Finger2WsqImage { get; set; }
        public bool HasErrors { get; set; }
        public int Finger1Index { get; set; }
        public int Finger2Index { get; set; }
        public string Finger1Name { get; set; }
        public string Finger2Name { get; set; }
        public string DeviceSerialNumber { get; set; }
        public string DeviceFirmwareVersion { get; set; }
        public string DeviceModel { get; set; }
        public string WorkStationName { get; set; }
        public string WorkstationLoggedInUserName { get; set; }
        public DateTime LastUsedTimeStamp { get; set; }
        public string Finger1NameString { get; set; }
        public string Finger2NameString { get; set; }
        public string BonusXML { get; set; }
    }
}
