﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDSPortalLibrary.Entity
{

    [Serializable]
    public class VettingVRequest
    {
        public VettingVRequest()
        {

        }
        string SearchTypeField;
        string SubjectNameField;
        string RegistrationNoField;
        string ITNumberField;
        string DunsNumberField;
        string VatNumberField;
        string BankAccountNumberField;
        string TradingNumberField;
        PrincipalInfo[] PrincipalInfoArrField = new PrincipalInfo[10];
        string UserField;
        string PasswordField;
        string ClientReferenceField;
        string IsExistingClientField;
        string BillingNumberField;

        public string EnterpriseType
        {
            get { return SearchTypeField; }
            set { SearchTypeField = value; }
        }
        public string EnterpriseName
        {
            get { return SubjectNameField; }
            set { SubjectNameField = value; }
        }
        public string RegistrationNo
        {
            get { return RegistrationNoField; }
            set { RegistrationNoField = value; }
        }
        //public string ITNumber {
        //    get { return ITNumberField; }
        //    set { ITNumberField = value; }
        //}
        //public string DunsNumber {
        //    get { return DunsNumberField; }
        //    set { DunsNumberField = value; }
        //}
        public string VatNumber {
            get { return VatNumberField; }
            set { VatNumberField = value; }
        }
        public string BankAccountNumber {
            get { return BankAccountNumberField; }
            set { BankAccountNumberField = value; }
        }
        //public string TradingNumber
        //{
        //    get { return TradingNumberField; }
        //    set { TradingNumberField = value; }
        //}
        public PrincipalInfo[] PrincipalDetails {
            get { return PrincipalInfoArrField; }
            set { PrincipalInfoArrField = value; }
        }
        public string User {
            get { return UserField; }
            set { UserField = value; }
        }
        public string Password {
            get { return PasswordField; }
            set { PasswordField = value; }
        }
        public string ClientReference {
            get { return ClientReferenceField; }
            set { ClientReferenceField = value; }
        }
        public string IsExistingClient
        {
            get { return this.IsExistingClientField; }
            set { this.IsExistingClientField = value; }
        }

        public string BillingNumber
        {
            get { return this.BillingNumberField; }
            set { this.BillingNumberField = value; }
        }
    }
}
