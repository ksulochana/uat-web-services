﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace XDSPortalLibrary.Entity_Layer
{
   public class DeedsEnquiry
    {
       public DeedsEnquiry()
       {
       }
       private int  subscriberIDvalue, productIDvalue, PropertyDeedIDValue = 0;
       private string strDeedsOffice = string.Empty, strIDno = string.Empty, strTownshipName = string.Empty, strErfNo = string.Empty, strBondAccNo = string.Empty, strTitleDeedNo = string.Empty, ReferenceNoValue = string.Empty, strTmpReference = string.Empty, ExternalReferencevalue = string.Empty, strFirstName = string.Empty, strSurName = string.Empty, strBusinessName = string.Empty, strPortionNo = string.Empty;
       private bool ConfirmationChkBoxvalue, IsConsumerMatchingvalue = false;
       DataSet BonusSegmentsDS;
       private int intSearchTypeINd =0;
       private bool boolBonusCheck = false;
       private string strdataSegments = string.Empty;
       private string strAssociationCode = string.Empty;
       private string strBuyerIDNO = string.Empty;

       public string BuyerIDNO
       {
           get { return strBuyerIDNO; }
           set { strBuyerIDNO = value; }
       }

       public string SubscriberAssociationCode
       {
           get { return this.strAssociationCode; }
           set { this.strAssociationCode = value; }
       }

       public string DataSegments
       {
           get { return this.strdataSegments; }
           set { this.strdataSegments = value; }
       }

       public bool BonusCheck
       {
           get { return this.boolBonusCheck; }
           set { this.boolBonusCheck = value; }
       }

       public string DeedsOffice
       {
           get
           {
               return this.strDeedsOffice;
           }
           set
           {
               this.strDeedsOffice = value;

           }
       }
       public string IDno
       {
           get
           {
               return this.strIDno;
           }
           set
           {
               this.strIDno = value;

           }
       }
       public string TownshipName
       {
           get
           {
               return this.strTownshipName;
           }
           set
           {
               this.strTownshipName = value;

           }
       }
       public string ErfNo
       {
           get
           {
               return this.strErfNo;
           }
           set
           {
               this.strErfNo = value;

           }
       }
       public string BondAccNo
       {
           get
           {
               return this.strBondAccNo;
           }
           set
           {
               this.strBondAccNo = value;

           }
       }
       public string TitleDeedNo
       {
           get
           {
               return this.strTitleDeedNo;
           }
           set
           {
               this.strTitleDeedNo = value;

           }
       }
       public bool ConfirmationChkBox
       {
           get
           {
               return this.ConfirmationChkBoxvalue;
           }
           set
           {
               this.ConfirmationChkBoxvalue = value;
           }
       }
       public bool IsConsumerMatching
       {
           get
           {
               return this.IsConsumerMatchingvalue;
           }
           set
           {
               this.IsConsumerMatchingvalue = value;
           }
       }
       public DataSet BonusSegments
       {
           get
           {
               return this.BonusSegmentsDS;
           }
           set
           {
               this.BonusSegmentsDS = value;
           }
       }
       public int subscriberID
       {
           get
           {
               if (String.IsNullOrEmpty(this.subscriberIDvalue.ToString()))
                   return Convert.ToInt16(DBNull.Value);
               else
                   return this.subscriberIDvalue;
           }
           set
           {
               this.subscriberIDvalue = value;
           }
       }
       public int ProductID
       {
           get
           {
               if (String.IsNullOrEmpty(this.subscriberIDvalue.ToString()))
                   return Convert.ToInt16(DBNull.Value);
               else
                   return this.productIDvalue;
           }
           set
           {
               this.productIDvalue = value;
           }
       }
       public int PropertyDeedID
       {
           get
           {
               return this.PropertyDeedIDValue;
           }
           set
           {
               this.PropertyDeedIDValue = value;
           }
       }
       public string ReferenceNo
       {
           get
           {
               if (String.IsNullOrEmpty(this.ReferenceNoValue))
                   return DBNull.Value.ToString();
               else
                   return this.ReferenceNoValue;
           }
           set
           {
               this.ReferenceNoValue = value;
           }
       }
       public string TmpReference
       {
           get
           {
               if (String.IsNullOrEmpty(this.strTmpReference))
                   return DBNull.Value.ToString();
               else
                   return this.strTmpReference;
           }
           set
           {
               this.strTmpReference = value;
           }
       }
       public string ExternalReference
       {
           get
           {
               if (String.IsNullOrEmpty(this.ExternalReferencevalue))
                   return DBNull.Value.ToString();
               else
                   return this.ExternalReferencevalue;
           }
           set
           {
               this.ExternalReferencevalue = value;
           }
       }
       public string FirstName
       {
           get
           {
               return this.strFirstName;
           }
           set
           {
               this.strFirstName = value;
           }
       }
       public string SurName
       {
           get
           {
               return this.strSurName;
           }
           set
           {
               this.strSurName = value;
           }
       }
       public string BusinessName
       {
           get
           {
               return this.strBusinessName;
           }
           set
           {
               this.strBusinessName = value;
           }
       }
       public string PortionNo
       {
           get
           {
               return this.strPortionNo;
           }
           set
           {
               this.strPortionNo = value;
           }
       }
       public int SearchTypeInd
       {
           get
           {
               return this.intSearchTypeINd;
           }
           set
           {
               this.intSearchTypeINd = value;
           }
       }
    }
}
