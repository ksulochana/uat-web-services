﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XDSPortalLibrary.Entity_Layer
{
    class SubscriberAssociationProductReports
    {
        public SubscriberAssociationProductReports()
        {
        }
        private int subscriberAssociationProductReportIDField;

        private string subscriberAssociationCodeField;

        private int productIDField;

        private bool productIDFieldSpecified;

        private int reportIDField;

        private bool reportIDFieldSpecified;

        private bool activeField;

        private string createdByUserField;

        private System.DateTime createdOnDateField;

        private bool createdOnDateFieldSpecified;

        private string changedByUserField;

        private System.DateTime changedOnDateField;

        private bool changedOnDateFieldSpecified;

        /// <remarks/>
        public int SubscriberAssociationProductReportID
        {
            get
            {
                return this.subscriberAssociationProductReportIDField;
            }
            set
            {
                this.subscriberAssociationProductReportIDField = value;
            }
        }

        /// <remarks/>
        public string SubscriberAssociationCode
        {
            get
            {
                return this.subscriberAssociationCodeField;
            }
            set
            {
                this.subscriberAssociationCodeField = value;
            }
        }

        /// <remarks/>
        public int ProductID
        {
            get
            {
                return this.productIDField;
            }
            set
            {
                this.productIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ProductIDSpecified
        {
            get
            {
                return this.productIDFieldSpecified;
            }
            set
            {
                this.productIDFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int ReportID
        {
            get
            {
                return this.reportIDField;
            }
            set
            {
                this.reportIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ReportIDSpecified
        {
            get
            {
                return this.reportIDFieldSpecified;
            }
            set
            {
                this.reportIDFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool Active
        {
            get
            {
                return this.activeField;
            }
            set
            {
                this.activeField = value;
            }
        }

        /// <remarks/>
        public string CreatedByUser
        {
            get
            {
                return this.createdByUserField;
            }
            set
            {
                this.createdByUserField = value;
            }
        }

        /// <remarks/>
        public System.DateTime CreatedOnDate
        {
            get
            {
                return this.createdOnDateField;
            }
            set
            {
                this.createdOnDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CreatedOnDateSpecified
        {
            get
            {
                return this.createdOnDateFieldSpecified;
            }
            set
            {
                this.createdOnDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string ChangedByUser
        {
            get
            {
                return this.changedByUserField;
            }
            set
            {
                this.changedByUserField = value;
            }
        }

        /// <remarks/>
        public System.DateTime ChangedOnDate
        {
            get
            {
                return this.changedOnDateField;
            }
            set
            {
                this.changedOnDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ChangedOnDateSpecified
        {
            get
            {
                return this.changedOnDateFieldSpecified;
            }
            set
            {
                this.changedOnDateFieldSpecified = value;
            }
        }
    }
}
