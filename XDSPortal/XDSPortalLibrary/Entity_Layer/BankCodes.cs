﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XDSPortalLibrary.Entity_Layer
{
    public class BankCodes
    {
        public BankCodes()
        {
        }
        private string strIDNo = string.Empty, strAccNo = string.Empty, strBankName = string.Empty,strBranchName = string.Empty,strBranchCode = string.Empty,strAccholder = string.Empty,strTerms = string.Empty, strExternalReference = string.Empty, strReferenceNo = string.Empty;
        private string strEmailAddress = string.Empty, strBankCodeRequestType = string.Empty, strRegNo = string.Empty, strTrustno = string.Empty, strAddinfo = string.Empty, strClientCompany = string.Empty, strContactNo = string.Empty;
        private string strRequesterFirstName = string.Empty, strRequesterSurName = string.Empty;
        private bool boolBonusCheck = false;
        private int intKey = 0;
        private double dAmount = 0;
        private string strVoucherCode = string.Empty, strRegion = string.Empty, strCountryCode = string.Empty ;


        public string CountryCode
        {
            get { return this.strCountryCode; }
            set { this.strCountryCode = value; }
        }

        public bool BonusCheck
        {
            get { return this.boolBonusCheck; }
            set { this.boolBonusCheck = value; }
        }
        public string IDNo
        {
            get { return this.strIDNo; }
            set { this.strIDNo = value; }
        }
        public string AccNo
        {
            get { return this.strAccNo; }
            set { this.strAccNo = value; }
        }
        public string BankName
        {
            get { return this.strBankName; }
            set { this.strBankName = value; }
        }
        public string BranchCode
        {
            get { return this.strBranchCode; }
            set { this.strBranchCode = value; }
        }
        public string BranchName
        {
            get { return this.strBranchName; }
            set { this.strBranchName = value; }
        }
        public string AccHolder
        {
            get { return this.strAccholder; }
            set { this.strAccholder = value; }
        }

        public string Terms
        {
            get { return this.strTerms; }
            set { this.strTerms = value; }
        }
        public string ReferenceNo
        {
            get { return this.strReferenceNo; }
            set { this.strReferenceNo = value; }
        }
        public string ExternalReference
        {
            get { return this.strExternalReference; }
            set { this.strExternalReference = value; }
        }
        public string EmailAddress
        {
            get { return this.strEmailAddress; }
            set { this.strEmailAddress = value; }
        }
        public string RequesterFirstName
        {
            get { return this.strRequesterFirstName; }
            set { this.strRequesterFirstName = value; }
        }

        public string RequesterSurName
        {
            get { return this.strRequesterSurName; }
            set { this.strRequesterSurName = value; }
        }
        public string RequesterCompany
        {
            get { return this.strClientCompany; }
            set { this.strClientCompany = value; }
        }
        public string RequesterContactNo
        {
            get { return this.strContactNo; }
            set { this.strContactNo = value; }
        }
        public double Amount
        {
            get { return this.dAmount; }
            set { this.dAmount = value;}
        }
             public string RequestType
        {
            get { return this.strBankCodeRequestType; }
            set { this.strBankCodeRequestType = value; }
        }
        public string RegistrationNo
        {
            get { return this.strRegNo; }
            set { this.strRegNo = value; }
        }
        public string TrustNo
        {
            get { return this.strTrustno; }
            set { this.strTrustno = value; }
        }
        public string AdditionalInfo
        {
            get { return this.strAddinfo; }
            set { this.strAddinfo = value; }
        }
        public int Key
        {
            get { return this.intKey; }
            set { this.intKey = value;}
        }
        public string Region
        {
            get { return this.strRegion; }
            set { this.strRegion = value; }
        }
    }
}
