﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace XDSPortalLibrary.Entity_Layer.MTNADP
{
   

    /// <remarks/>







/// <remarks/>



    [Serializable]
    [XmlRoot(Namespace = "http://www.web.xds.co.za/XDSConnectWS")]
public partial class MTNCommercialFinalOutput {
    
    private string statusField;
    
    private string errorDescriptionField;
    
    private long applicationIDField;
    
    private bool applicationIDFieldSpecified;
    
    private string existingClientField;
    
    private string accountNumberField;
    
    private string investigateFlagField;
    
    private string investigateRecommendationField;
    
    private string finalOutcomeField;
    
    private string finalOutcomeReasonField;
    
    private string matrixBandField;
    
    private string companyScoreField;
    
    private bool companyScoreFieldSpecified;
    
    private string companyBandField;
    
    private int sMEFilter01Field;
    
    private bool sMEFilter01FieldSpecified;
    
    private int sMEFilter02Field;
    
    private bool sMEFilter02FieldSpecified;
    
    private int sMEFilter03Field;
    
    private bool sMEFilter03FieldSpecified;
    
    private int sMEFilter04Field;
    
    private bool sMEFilter04FieldSpecified;
    
    private Principal[] principalsField;
    
    private string finalPrincipalScoreField =string.Empty;
    
    private bool finalPrincipalScoreFieldSpecified;
    
    private string finalPrincipalBandField;
    
    private string scoreFlagField;
    
    private string dateScoredField;
    
    private decimal behaviouralScoreField;
    
    private bool behaviouralScoreFieldSpecified;
    
    private string behaviouralRiskBandField;
    
    private CommercialResponse bureauDataField;
    
    /// <remarks/>
    
    public string Status {
        get {
            return this.statusField;
        }
        set {
            this.statusField = value;
        }
    }
    
    /// <remarks/>
    
    public string ErrorDescription {
        get {
            return this.errorDescriptionField;
        }
        set {
            this.errorDescriptionField = value;
        }
    }
    
    /// <remarks/>
    public long ApplicationID {
        get {
            return this.applicationIDField;
        }
        set {
            this.applicationIDField = value;
        }
    }
    
    /// <remarks/>
    //[System.Xml.Serialization.XmlIgnoreAttribute()]
    //public bool ApplicationIDSpecified {
    //    get {
    //        return this.applicationIDFieldSpecified;
    //    }
    //    set {
    //        this.applicationIDFieldSpecified = value;
    //    }
    //}
    
    /// <remarks/>
    
    public string ExistingClient {
        get {
            return this.existingClientField;
        }
        set {
            this.existingClientField = value;
        }
    }
    
    /// <remarks/>
    
    public string AccountNumber {
        get {
            return this.accountNumberField;
        }
        set {
            this.accountNumberField = value;
        }
    }
    
    /// <remarks/>
    
    public string InvestigateFlag {
        get {
            return this.investigateFlagField;
        }
        set {
            this.investigateFlagField = value;
        }
    }
    
    /// <remarks/>
    
    public string InvestigateRecommendation {
        get {
            return this.investigateRecommendationField;
        }
        set {
            this.investigateRecommendationField = value;
        }
    }
    
    /// <remarks/>
    
    public string FinalOutcome {
        get {
            return this.finalOutcomeField;
        }
        set {
            this.finalOutcomeField = value;
        }
    }
    
    /// <remarks/>
    
    public string FinalOutcomeReason {
        get {
            return this.finalOutcomeReasonField;
        }
        set {
            this.finalOutcomeReasonField = value;
        }
    }
    
    /// <remarks/>
    
    public string MatrixBand {
        get {
            return this.matrixBandField;
        }
        set {
            this.matrixBandField = value;
        }
    }
    
    /// <remarks/>
    public string CompanyScore {
        get {
            return this.companyScoreField;
        }
        set {
            this.companyScoreField = value;
        }
    }
    
    /// <remarks/>
    //[System.Xml.Serialization.XmlIgnoreAttribute()]
    //public bool CompanyScoreSpecified {
    //    get {
    //        return this.companyScoreFieldSpecified;
    //    }
    //    set {
    //        this.companyScoreFieldSpecified = value;
    //    }
    //}
    
    /// <remarks/>
    
    public string CompanyBand {
        get {
            return this.companyBandField;
        }
        set {
            this.companyBandField = value;
        }
    }
    
    /// <remarks/>
    public int SMEFilter01 {
        get {
            return this.sMEFilter01Field;
        }
        set {
            this.sMEFilter01Field = value;
        }
    }
    
    ///// <remarks/>
    //[System.Xml.Serialization.XmlIgnoreAttribute()]
    //public bool SMEFilter01Specified {
    //    get {
    //        return this.sMEFilter01FieldSpecified;
    //    }
    //    set {
    //        this.sMEFilter01FieldSpecified = value;
    //    }
    //}
    
    /// <remarks/>
    public int SMEFilter02 {
        get {
            return this.sMEFilter02Field;
        }
        set {
            this.sMEFilter02Field = value;
        }
    }
    
    /// <remarks/>
    //[System.Xml.Serialization.XmlIgnoreAttribute()]
    //public bool SMEFilter02Specified {
    //    get {
    //        return this.sMEFilter02FieldSpecified;
    //    }
    //    set {
    //        this.sMEFilter02FieldSpecified = value;
    //    }
    //}
    
    /// <remarks/>
    public int SMEFilter03 {
        get {
            return this.sMEFilter03Field;
        }
        set {
            this.sMEFilter03Field = value;
        }
    }
    
    /// <remarks/>
    //[System.Xml.Serialization.XmlIgnoreAttribute()]
    //public bool SMEFilter03Specified {
    //    get {
    //        return this.sMEFilter03FieldSpecified;
    //    }
    //    set {
    //        this.sMEFilter03FieldSpecified = value;
    //    }
    //}
    
    /// <remarks/>
    public int SMEFilter04 {
        get {
            return this.sMEFilter04Field;
        }
        set {
            this.sMEFilter04Field = value;
        }
    }
    
    /// <remarks/>
    //[System.Xml.Serialization.XmlIgnoreAttribute()]
    //public bool SMEFilter04Specified {
    //    get {
    //        return this.sMEFilter04FieldSpecified;
    //    }
    //    set {
    //        this.sMEFilter04FieldSpecified = value;
    //    }
    //}
    
    /// <remarks/>

    public Principal[] Principals
    {
        get
        {
            return this.principalsField;
        }
        set
        {
            this.principalsField = value;
        }
    }
    
    /// <remarks/>
     
    public string FinalPrincipalScore {
        get {
            return this.finalPrincipalScoreField;
        }
        set {
            this.finalPrincipalScoreField = value;
        }
    }
    
    /// <remarks/>
    //[System.Xml.Serialization.XmlIgnoreAttribute()]
    //public bool FinalPrincipalScoreSpecified {
    //    get {
    //        return this.finalPrincipalScoreFieldSpecified;
    //    }
    //    set {
    //        this.finalPrincipalScoreFieldSpecified = value;
    //    }
    //}
    
    /// <remarks/>
    
    public string FinalPrincipalBand {
        get {
            return this.finalPrincipalBandField;
        }
        set {
            this.finalPrincipalBandField = value;
        }
    }
    
    /// <remarks/>
    
    public string ScoreFlag {
        get {
            return this.scoreFlagField;
        }
        set {
            this.scoreFlagField = value;
        }
    }
    
    /// <remarks/>
    
    public string DateScored {
        get {
            return this.dateScoredField;
        }
        set {
            this.dateScoredField = value;
        }
    }
    
    /// <remarks/>
    public decimal BehaviouralScore {
        get {
            return this.behaviouralScoreField;
        }
        set {
            this.behaviouralScoreField = value;
        }
    }
    
    /// <remarks/>
    //[System.Xml.Serialization.XmlIgnoreAttribute()]
    //public bool BehaviouralScoreSpecified {
    //    get {
    //        return this.behaviouralScoreFieldSpecified;
    //    }
    //    set {
    //        this.behaviouralScoreFieldSpecified = value;
    //    }
    //}
    
    /// <remarks/>
    
    public string BehaviouralRiskBand {
        get {
            return this.behaviouralRiskBandField;
        }
        set {
            this.behaviouralRiskBandField = value;
        }
    }
    
    /// <remarks/>
     [XmlElement(Namespace = "http://www.web.xds.co.za/XDSConnectWS")]
    public CommercialResponse BureauData {
        get {
            return this.bureauDataField;
        }
        set {
            this.bureauDataField = value;
        }
    }
}

/// <remarks/>





public partial class Principal {
    
    private int principalDateOfBirthField;
    
    private bool principalDateOfBirthFieldSpecified;
    
    private int principalFilter01Field;
    
    private bool principalFilter01FieldSpecified;
    
    private int principalFilter02Field;
    
    private bool principalFilter02FieldSpecified;
    
    private int principalFilter03Field;
    
    private bool principalFilter03FieldSpecified;
    
    private int principalFilter04Field;
    
    private bool principalFilter04FieldSpecified;
    
    private string principalFirstNameField;
    
    private string principalIdentityNumberField;
    
    private decimal principalScoreField;
    
    private bool principalScoreFieldSpecified;
    
    private string principalSurnameField;
    
    /// <remarks/>
    public int PrincipalDateOfBirth {
        get {
            return this.principalDateOfBirthField;
        }
        set {
            this.principalDateOfBirthField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool PrincipalDateOfBirthSpecified {
        get {
            return this.principalDateOfBirthFieldSpecified;
        }
        set {
            this.principalDateOfBirthFieldSpecified = value;
        }
    }
    
    /// <remarks/>
    public int PrincipalFilter01 {
        get {
            return this.principalFilter01Field;
        }
        set {
            this.principalFilter01Field = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool PrincipalFilter01Specified {
        get {
            return this.principalFilter01FieldSpecified;
        }
        set {
            this.principalFilter01FieldSpecified = value;
        }
    }
    
    /// <remarks/>
    public int PrincipalFilter02 {
        get {
            return this.principalFilter02Field;
        }
        set {
            this.principalFilter02Field = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool PrincipalFilter02Specified {
        get {
            return this.principalFilter02FieldSpecified;
        }
        set {
            this.principalFilter02FieldSpecified = value;
        }
    }
    
    /// <remarks/>
    public int PrincipalFilter03 {
        get {
            return this.principalFilter03Field;
        }
        set {
            this.principalFilter03Field = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool PrincipalFilter03Specified {
        get {
            return this.principalFilter03FieldSpecified;
        }
        set {
            this.principalFilter03FieldSpecified = value;
        }
    }
    
    /// <remarks/>
    public int PrincipalFilter04 {
        get {
            return this.principalFilter04Field;
        }
        set {
            this.principalFilter04Field = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool PrincipalFilter04Specified {
        get {
            return this.principalFilter04FieldSpecified;
        }
        set {
            this.principalFilter04FieldSpecified = value;
        }
    }
    
    /// <remarks/>
    
    public string PrincipalFirstName {
        get {
            return this.principalFirstNameField;
        }
        set {
            this.principalFirstNameField = value;
        }
    }
    
    /// <remarks/>
    
    public string PrincipalIdentityNumber {
        get {
            return this.principalIdentityNumberField;
        }
        set {
            this.principalIdentityNumberField = value;
        }
    }
    
    /// <remarks/>
    public decimal PrincipalScore {
        get {
            return this.principalScoreField;
        }
        set {
            this.principalScoreField = value;
        }
    }
    
    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool PrincipalScoreSpecified {
        get {
            return this.principalScoreFieldSpecified;
        }
        set {
            this.principalScoreFieldSpecified = value;
        }
    }
    
    /// <remarks/>
    
    public string PrincipalSurname {
        get {
            return this.principalSurnameField;
        }
        set {
            this.principalSurnameField = value;
        }
    }
}

/// <remarks/>





public partial class Vehicle {
    
    private string descriptionFieldField;
    
    private string infoDateFieldField;
    
    private string noHPFieldField;
    
    private string noLeasedFieldField;
    
    private string noOwnedFieldField;
    
    private string quantityFieldField;
    
    /// <remarks/>
    
    public string descriptionField {
        get {
            return this.descriptionFieldField;
        }
        set {
            this.descriptionFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string infoDateField {
        get {
            return this.infoDateFieldField;
        }
        set {
            this.infoDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string noHPField {
        get {
            return this.noHPFieldField;
        }
        set {
            this.noHPFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string noLeasedField {
        get {
            return this.noLeasedFieldField;
        }
        set {
            this.noLeasedFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string noOwnedField {
        get {
            return this.noOwnedFieldField;
        }
        set {
            this.noOwnedFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string quantityField {
        get {
            return this.quantityFieldField;
        }
        set {
            this.quantityFieldField = value;
        }
    }
}

/// <remarks/>





public partial class CreditBand {
    
    private string days11to30FieldField;
    
    private string days120toPlusFieldField;
    
    private string days1to10FieldField;
    
    private string days31to60FieldField;
    
    private string days61to90FieldField;
    
    private string days91to120FieldField;
    
    private string invoiceAmountFieldField;
    
    private string noOfInvoicesFieldField;
    
    private string withinTermsFieldField;
    
    /// <remarks/>
    
    public string days11to30Field {
        get {
            return this.days11to30FieldField;
        }
        set {
            this.days11to30FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string days120toPlusField {
        get {
            return this.days120toPlusFieldField;
        }
        set {
            this.days120toPlusFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string days1to10Field {
        get {
            return this.days1to10FieldField;
        }
        set {
            this.days1to10FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string days31to60Field {
        get {
            return this.days31to60FieldField;
        }
        set {
            this.days31to60FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string days61to90Field {
        get {
            return this.days61to90FieldField;
        }
        set {
            this.days61to90FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string days91to120Field {
        get {
            return this.days91to120FieldField;
        }
        set {
            this.days91to120FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string invoiceAmountField {
        get {
            return this.invoiceAmountFieldField;
        }
        set {
            this.invoiceAmountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string noOfInvoicesField {
        get {
            return this.noOfInvoicesFieldField;
        }
        set {
            this.noOfInvoicesFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string withinTermsField {
        get {
            return this.withinTermsFieldField;
        }
        set {
            this.withinTermsFieldField = value;
        }
    }
}

/// <remarks/>





public partial class PaymentHistoryDetail {
    
    private string creditLimitFieldField;
    
    private string days120PercentageFieldField;
    
    private string days30PercentageFieldField;
    
    private string days60PercentageFieldField;
    
    private string days90PercentageFieldField;
    
    private string numberSuppliersFieldField;
    
    private string periodFieldField;
    
    private string totalCreditLimitFieldField;
    
    private string totalCurrentPercentageFieldField;
    
    private string totalOverduePercentageFieldField;
    
    /// <remarks/>
    
    public string creditLimitField {
        get {
            return this.creditLimitFieldField;
        }
        set {
            this.creditLimitFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string days120PercentageField {
        get {
            return this.days120PercentageFieldField;
        }
        set {
            this.days120PercentageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string days30PercentageField {
        get {
            return this.days30PercentageFieldField;
        }
        set {
            this.days30PercentageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string days60PercentageField {
        get {
            return this.days60PercentageFieldField;
        }
        set {
            this.days60PercentageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string days90PercentageField {
        get {
            return this.days90PercentageFieldField;
        }
        set {
            this.days90PercentageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string numberSuppliersField {
        get {
            return this.numberSuppliersFieldField;
        }
        set {
            this.numberSuppliersFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string periodField {
        get {
            return this.periodFieldField;
        }
        set {
            this.periodFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string totalCreditLimitField {
        get {
            return this.totalCreditLimitFieldField;
        }
        set {
            this.totalCreditLimitFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string totalCurrentPercentageField {
        get {
            return this.totalCurrentPercentageFieldField;
        }
        set {
            this.totalCurrentPercentageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string totalOverduePercentageField {
        get {
            return this.totalOverduePercentageFieldField;
        }
        set {
            this.totalOverduePercentageFieldField = value;
        }
    }
}

/// <remarks/>





public partial class AgeAnalysisDetail {
    
    private string creditLimitFieldField;
    
    private string days120PercentageFieldField;
    
    private string days30PercentageFieldField;
    
    private string days60PercentageFieldField;
    
    private string days90PercentageFieldField;
    
    private string supplierFieldField;
    
    private string termFieldField;
    
    private string totalCreditLimitFieldField;
    
    private string totalCurrentPercentageFieldField;
    
    private string totalOverduePercentageFieldField;
    
    /// <remarks/>
    
    public string creditLimitField {
        get {
            return this.creditLimitFieldField;
        }
        set {
            this.creditLimitFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string days120PercentageField {
        get {
            return this.days120PercentageFieldField;
        }
        set {
            this.days120PercentageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string days30PercentageField {
        get {
            return this.days30PercentageFieldField;
        }
        set {
            this.days30PercentageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string days60PercentageField {
        get {
            return this.days60PercentageFieldField;
        }
        set {
            this.days60PercentageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string days90PercentageField {
        get {
            return this.days90PercentageFieldField;
        }
        set {
            this.days90PercentageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string supplierField {
        get {
            return this.supplierFieldField;
        }
        set {
            this.supplierFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string termField {
        get {
            return this.termFieldField;
        }
        set {
            this.termFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string totalCreditLimitField {
        get {
            return this.totalCreditLimitFieldField;
        }
        set {
            this.totalCreditLimitFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string totalCurrentPercentageField {
        get {
            return this.totalCurrentPercentageFieldField;
        }
        set {
            this.totalCurrentPercentageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string totalOverduePercentageField {
        get {
            return this.totalOverduePercentageFieldField;
        }
        set {
            this.totalOverduePercentageFieldField = value;
        }
    }
}

/// <remarks/>





public partial class AccountSynopsisDetail {
    
    private string accountNumberFieldField;
    
    private string creditLimitFieldField;
    
    private string statusFieldField;
    
    private string supplierFieldField;
    
    private string termExtFieldField;
    
    private string termFieldField;
    
    /// <remarks/>
    
    public string accountNumberField {
        get {
            return this.accountNumberFieldField;
        }
        set {
            this.accountNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string creditLimitField {
        get {
            return this.creditLimitFieldField;
        }
        set {
            this.creditLimitFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string statusField {
        get {
            return this.statusFieldField;
        }
        set {
            this.statusFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string supplierField {
        get {
            return this.supplierFieldField;
        }
        set {
            this.supplierFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string termExtField {
        get {
            return this.termExtFieldField;
        }
        set {
            this.termExtFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string termField {
        get {
            return this.termFieldField;
        }
        set {
            this.termFieldField = value;
        }
    }
}

/// <remarks/>





public partial class SegmentDescription {
    
    private string dateFieldField;
    
    private string infoDescriptionFieldField;
    
    private string infoTypeFieldField;
    
    /// <remarks/>
    
    public string dateField {
        get {
            return this.dateFieldField;
        }
        set {
            this.dateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string infoDescriptionField {
        get {
            return this.infoDescriptionFieldField;
        }
        set {
            this.infoDescriptionFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string infoTypeField {
        get {
            return this.infoTypeFieldField;
        }
        set {
            this.infoTypeFieldField = value;
        }
    }
}

/// <remarks/>





public partial class ShareInfo {
    
    private string shareNumberFieldField;
    
    private string shareTypeFieldField;
    
    private string shareValueFieldField;
    
    /// <remarks/>
    
    public string shareNumberField {
        get {
            return this.shareNumberFieldField;
        }
        set {
            this.shareNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string shareTypeField {
        get {
            return this.shareTypeFieldField;
        }
        set {
            this.shareTypeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string shareValueField {
        get {
            return this.shareValueFieldField;
        }
        set {
            this.shareValueFieldField = value;
        }
    }
}

/// <remarks/>





public partial class CapitalInfo {
    
    private string capitalChangedDateFieldField;
    
    private string capitalChangedFromFieldField;
    
    private string capitalChangedToFieldField;
    
    /// <remarks/>
    
    public string capitalChangedDateField {
        get {
            return this.capitalChangedDateFieldField;
        }
        set {
            this.capitalChangedDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string capitalChangedFromField {
        get {
            return this.capitalChangedFromFieldField;
        }
        set {
            this.capitalChangedFromFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string capitalChangedToField {
        get {
            return this.capitalChangedToFieldField;
        }
        set {
            this.capitalChangedToFieldField = value;
        }
    }
}

/// <remarks/>





public partial class SicDetails {
    
    private string sASicCodeFieldField;
    
    private string sicCodeFieldField;
    
    private string sicDescriptionFieldField;
    
    /// <remarks/>
    
    public string sASicCodeField {
        get {
            return this.sASicCodeFieldField;
        }
        set {
            this.sASicCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string sicCodeField {
        get {
            return this.sicCodeFieldField;
        }
        set {
            this.sicCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string sicDescriptionField {
        get {
            return this.sicDescriptionFieldField;
        }
        set {
            this.sicDescriptionFieldField = value;
        }
    }
}

/// <remarks/>





public partial class Module {
    
    private float hoursFieldField;
    
    private string productCodeFieldField;
    
    private string productDescFieldField;
    
    private ProductType productTypeFieldField;
    
    /// <remarks/>
    public float hoursField {
        get {
            return this.hoursFieldField;
        }
        set {
            this.hoursFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string productCodeField {
        get {
            return this.productCodeFieldField;
        }
        set {
            this.productCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string productDescField {
        get {
            return this.productDescFieldField;
        }
        set {
            this.productDescFieldField = value;
        }
    }
    
    /// <remarks/>
    public ProductType productTypeField {
        get {
            return this.productTypeFieldField;
        }
        set {
            this.productTypeFieldField = value;
        }
    }
}

/// <remarks/>



public enum ProductType {
    
    /// <remarks/>
    View,
    
    /// <remarks/>
    Investigate,
}

/// <remarks/>





public partial class DebtorInfo {
    
    private string debtorDescriptionFieldField;
    
    private string debtorNameFieldField;
    
    /// <remarks/>
    
    public string debtorDescriptionField {
        get {
            return this.debtorDescriptionFieldField;
        }
        set {
            this.debtorDescriptionFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string debtorNameField {
        get {
            return this.debtorNameFieldField;
        }
        set {
            this.debtorNameFieldField = value;
        }
    }
}

/// <remarks/>





public partial class FinanceAmounts {
    
    private string amountBeforeFieldField;
    
    private string amountCurrentFieldField;
    
    /// <remarks/>
    
    public string amountBeforeField {
        get {
            return this.amountBeforeFieldField;
        }
        set {
            this.amountBeforeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string amountCurrentField {
        get {
            return this.amountCurrentFieldField;
        }
        set {
            this.amountCurrentFieldField = value;
        }
    }
}

/// <remarks/>





public partial class BusinessBenchmarkDetail {
    
    private string bCA_CC_ValFieldField;
    
    private string bCA_CategoryFieldField;
    
    private string bCA_Ltd_ValFieldField;
    
    private string bCA_Other_valFieldField;
    
    private string bCA_PtyLtd_ValFieldField;
    
    private string bCA_Subj_valFieldField;
    
    private string bCA_Var_IdFieldField;
    
    /// <remarks/>
    
    public string bCA_CC_ValField {
        get {
            return this.bCA_CC_ValFieldField;
        }
        set {
            this.bCA_CC_ValFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bCA_CategoryField {
        get {
            return this.bCA_CategoryFieldField;
        }
        set {
            this.bCA_CategoryFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bCA_Ltd_ValField {
        get {
            return this.bCA_Ltd_ValFieldField;
        }
        set {
            this.bCA_Ltd_ValFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bCA_Other_valField {
        get {
            return this.bCA_Other_valFieldField;
        }
        set {
            this.bCA_Other_valFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bCA_PtyLtd_ValField {
        get {
            return this.bCA_PtyLtd_ValFieldField;
        }
        set {
            this.bCA_PtyLtd_ValFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bCA_Subj_valField {
        get {
            return this.bCA_Subj_valFieldField;
        }
        set {
            this.bCA_Subj_valFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bCA_Var_IdField {
        get {
            return this.bCA_Var_IdFieldField;
        }
        set {
            this.bCA_Var_IdFieldField = value;
        }
    }
}

/// <remarks/>





public partial class Aggregate {
    
    private string nameFieldField;
    
    private string valueFieldField;
    
    /// <remarks/>
    
    public string nameField {
        get {
            return this.nameFieldField;
        }
        set {
            this.nameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string valueField {
        get {
            return this.valueFieldField;
        }
        set {
            this.valueFieldField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlIncludeAttribute(typeof(VeriCheque))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(Vehicles))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(UnmatchedNotarialBond))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(UnmatchedDefault))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(UnmatchedCourtRecord))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(Tradex))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(TradeReference))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(TradeReferenceSummary))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(TradePaymentHistory))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(TradeHistory))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(TradeAgeAnalysis))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(TradeAccountSynopsis))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(TicketStatus))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(SegmentDescriptions))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(SearchResponse))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(SMEAssessment))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(RegistrationDetails))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(RegistrationDetailsExtended))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(RegisteredPrincipal))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(Principal1))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(PrincipalDetailEmpirica))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(PrincipalNotarialBonds))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(LinkedCompany))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(LinkedBusinessDefault))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(PrincipalLink))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(PrincipalIDV))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(PrincipalFirstResponse))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(PrincipalDeedsSummaryP8))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(PrincipalDeedsSummaryCO))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(PrincipalDeedsComprehensiveCV))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(PrincipalDeedsComprehensiveCA))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(PrincipalClearance))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(PrincipalArchiveP5))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(PrincipalArchive))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(OtherOperation))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(AdditionalOperations))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(Operation))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(Observation))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(ObservationCont))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(NotarialBond))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(Names))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(ModuleAvailabilityResponse))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(MailboxRetrieveList))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(LinkedBusinessHeader))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(Header))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(GeneralBankingInfo))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(FirstResponse))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(FinancialRatios))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(FinanceHeader))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(FinanceData))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(FinanceDataFF))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(FinanceDataFE))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(EnquirySummary))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(EnquiryHistory))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(Empirica))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(EmpiricaEM04))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(EmpiricaE1))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(EmpOfCapital))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(DynamicRating))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(Default))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(DeedsMultipleBond))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(DeedHistory))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(CurrentLiabilities))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(CurrentAsset))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(CourtRecord))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(ConsumerTraceAlert))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(ConsumerNotice))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(ConsumerNotarialBonds))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(ConsumerJudgement))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(ConsumerInfoNO04))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(ConsumerHeader))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(ConsumerHeaderC1))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(ConsumerEnquiry))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(ConsumerDefault))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(ConsumerBusinessEnquiry))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(ComplianceIndicatorLI))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(CommercialDisputeResponse))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(CapitalEmployed))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(CancelledTicket))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(BusinessBenchmarkBB))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(BusinessRescueDetail))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(BusinessPlusPrincipalSummary))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(BusinessNotarialBonds))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(BusinessDeedsSummaryDA))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(BusinessDeedsDI))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(BusinessDeedsComprehensivePW))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(BusinessDeedsComprehensivePB))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(BusinessAdverseSummary))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(Branch))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(BankingDetailSummary))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(BankReport))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(BankHistory))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(BankCodes))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(AggregateCW))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(Affiliations))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(AccountVerificationVH))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(AccountVerificationVA))]





public partial class BureauSegment {
}

/// <remarks/>





public partial class VeriCheque : BureauSegment {
    
    private string addressFieldField;
    
    private string cityFieldField;
    
    private string countryFieldField;
    
    private string majorProductFieldField;
    
    private string messageFieldField;
    
    private string numberFoundFieldField;
    
    private string postCodeFieldField;
    
    private string reasonFieldField;
    
    private string suburbFieldField;
    
    private string veriAmountFieldField;
    
    private string veriDateFieldField;
    
    private string veriRegisterNameFieldField;
    
    /// <remarks/>
    
    public string addressField {
        get {
            return this.addressFieldField;
        }
        set {
            this.addressFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string cityField {
        get {
            return this.cityFieldField;
        }
        set {
            this.cityFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string countryField {
        get {
            return this.countryFieldField;
        }
        set {
            this.countryFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string messageField {
        get {
            return this.messageFieldField;
        }
        set {
            this.messageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string numberFoundField {
        get {
            return this.numberFoundFieldField;
        }
        set {
            this.numberFoundFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string postCodeField {
        get {
            return this.postCodeFieldField;
        }
        set {
            this.postCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string reasonField {
        get {
            return this.reasonFieldField;
        }
        set {
            this.reasonFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string suburbField {
        get {
            return this.suburbFieldField;
        }
        set {
            this.suburbFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string veriAmountField {
        get {
            return this.veriAmountFieldField;
        }
        set {
            this.veriAmountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string veriDateField {
        get {
            return this.veriDateFieldField;
        }
        set {
            this.veriDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string veriRegisterNameField {
        get {
            return this.veriRegisterNameFieldField;
        }
        set {
            this.veriRegisterNameFieldField = value;
        }
    }
}

/// <remarks/>





public partial class Vehicles : BureauSegment {
    
    private string commentFieldField;
    
    private string dateInfoFieldField;
    
    private string majorProductFieldField;
    
    private Vehicle[] vehicleListFieldField;
    
    /// <remarks/>
    
    public string commentField {
        get {
            return this.commentFieldField;
        }
        set {
            this.commentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateInfoField {
        get {
            return this.dateInfoFieldField;
        }
        set {
            this.dateInfoFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public Vehicle[] vehicleListField {
        get {
            return this.vehicleListFieldField;
        }
        set {
            this.vehicleListFieldField = value;
        }
    }
}

/// <remarks/>





public partial class UnmatchedNotarialBond : BureauSegment {
    
    private string addressFieldField;
    
    private string bondAmountFieldField;
    
    private string bondDateFieldField;
    
    private string bondDistrictFieldField;
    
    private string bondNumberFieldField;
    
    private string bondPercentFieldField;
    
    private string cityFieldField;
    
    private string countryFieldField;
    
    private string dateCancelledFieldField;
    
    private string majorProductFieldField;
    
    private string messageFieldField;
    
    private string mortgageeFieldField;
    
    private string mortgagorDistrictFieldField;
    
    private string mortgagorFieldField;
    
    private string numberFoundFieldField;
    
    private string postCodeFieldField;
    
    private string serialNumberFieldField;
    
    private string suburbFieldField;
    
    private string tradeStyleFieldField;
    
    /// <remarks/>
    
    public string addressField {
        get {
            return this.addressFieldField;
        }
        set {
            this.addressFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondAmountField {
        get {
            return this.bondAmountFieldField;
        }
        set {
            this.bondAmountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondDateField {
        get {
            return this.bondDateFieldField;
        }
        set {
            this.bondDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondDistrictField {
        get {
            return this.bondDistrictFieldField;
        }
        set {
            this.bondDistrictFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondNumberField {
        get {
            return this.bondNumberFieldField;
        }
        set {
            this.bondNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondPercentField {
        get {
            return this.bondPercentFieldField;
        }
        set {
            this.bondPercentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string cityField {
        get {
            return this.cityFieldField;
        }
        set {
            this.cityFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string countryField {
        get {
            return this.countryFieldField;
        }
        set {
            this.countryFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateCancelledField {
        get {
            return this.dateCancelledFieldField;
        }
        set {
            this.dateCancelledFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string messageField {
        get {
            return this.messageFieldField;
        }
        set {
            this.messageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string mortgageeField {
        get {
            return this.mortgageeFieldField;
        }
        set {
            this.mortgageeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string mortgagorDistrictField {
        get {
            return this.mortgagorDistrictFieldField;
        }
        set {
            this.mortgagorDistrictFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string mortgagorField {
        get {
            return this.mortgagorFieldField;
        }
        set {
            this.mortgagorFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string numberFoundField {
        get {
            return this.numberFoundFieldField;
        }
        set {
            this.numberFoundFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string postCodeField {
        get {
            return this.postCodeFieldField;
        }
        set {
            this.postCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string serialNumberField {
        get {
            return this.serialNumberFieldField;
        }
        set {
            this.serialNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string suburbField {
        get {
            return this.suburbFieldField;
        }
        set {
            this.suburbFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string tradeStyleField {
        get {
            return this.tradeStyleFieldField;
        }
        set {
            this.tradeStyleFieldField = value;
        }
    }
}

/// <remarks/>





public partial class UnmatchedDefault : BureauSegment {
    
    private string amountFieldField;
    
    private string cityFieldField;
    
    private string commentFieldField;
    
    private string countryFieldField;
    
    private string defaultAddressFieldField;
    
    private string defaultDateFieldField;
    
    private string defaultNameFieldField;
    
    private string defaultTradeStyleFieldField;
    
    private string majorProductFieldField;
    
    private string messageFieldField;
    
    private string numberFoundFieldField;
    
    private string onBehalfOfFieldField;
    
    private string postCodeFieldField;
    
    private string serialNoFieldField;
    
    private string statusFieldField;
    
    private string subscriberNameFieldField;
    
    private string suburbFieldField;
    
    private string supplierNameFieldField;
    
    /// <remarks/>
    
    public string amountField {
        get {
            return this.amountFieldField;
        }
        set {
            this.amountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string cityField {
        get {
            return this.cityFieldField;
        }
        set {
            this.cityFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string commentField {
        get {
            return this.commentFieldField;
        }
        set {
            this.commentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string countryField {
        get {
            return this.countryFieldField;
        }
        set {
            this.countryFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string defaultAddressField {
        get {
            return this.defaultAddressFieldField;
        }
        set {
            this.defaultAddressFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string defaultDateField {
        get {
            return this.defaultDateFieldField;
        }
        set {
            this.defaultDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string defaultNameField {
        get {
            return this.defaultNameFieldField;
        }
        set {
            this.defaultNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string defaultTradeStyleField {
        get {
            return this.defaultTradeStyleFieldField;
        }
        set {
            this.defaultTradeStyleFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string messageField {
        get {
            return this.messageFieldField;
        }
        set {
            this.messageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string numberFoundField {
        get {
            return this.numberFoundFieldField;
        }
        set {
            this.numberFoundFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string onBehalfOfField {
        get {
            return this.onBehalfOfFieldField;
        }
        set {
            this.onBehalfOfFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string postCodeField {
        get {
            return this.postCodeFieldField;
        }
        set {
            this.postCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string serialNoField {
        get {
            return this.serialNoFieldField;
        }
        set {
            this.serialNoFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string statusField {
        get {
            return this.statusFieldField;
        }
        set {
            this.statusFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string subscriberNameField {
        get {
            return this.subscriberNameFieldField;
        }
        set {
            this.subscriberNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string suburbField {
        get {
            return this.suburbFieldField;
        }
        set {
            this.suburbFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string supplierNameField {
        get {
            return this.supplierNameFieldField;
        }
        set {
            this.supplierNameFieldField = value;
        }
    }
}

/// <remarks/>





public partial class UnmatchedCourtRecord : BureauSegment {
    
    private string abandonDateFieldField;
    
    private string actionDateFieldField;
    
    private string attorneyFieldField;
    
    private string caseNumberFieldField;
    
    private string cityFieldField;
    
    private string claimAmountFieldField;
    
    private string commentFieldField;
    
    private string countryFieldField;
    
    private string courtDistrictFieldField;
    
    private string courtRecordAddressFieldField;
    
    private string courtTypeFieldField;
    
    private string defendantDistrictFieldField;
    
    private string defendantNameFieldField;
    
    private string defendantTradeStyleFieldField;
    
    private string majorProductFieldField;
    
    private string messageFieldField;
    
    private string natureOfDebtFieldField;
    
    private string numberFoundFieldField;
    
    private string plaintiffNameFieldField;
    
    private string postCodeFieldField;
    
    private string returnDateFieldField;
    
    private string serialNumberFieldField;
    
    private string suburbFieldField;
    
    private string typeCodeFieldField;
    
    private string typeDescFieldField;
    
    /// <remarks/>
    
    public string abandonDateField {
        get {
            return this.abandonDateFieldField;
        }
        set {
            this.abandonDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string actionDateField {
        get {
            return this.actionDateFieldField;
        }
        set {
            this.actionDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string attorneyField {
        get {
            return this.attorneyFieldField;
        }
        set {
            this.attorneyFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string caseNumberField {
        get {
            return this.caseNumberFieldField;
        }
        set {
            this.caseNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string cityField {
        get {
            return this.cityFieldField;
        }
        set {
            this.cityFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string claimAmountField {
        get {
            return this.claimAmountFieldField;
        }
        set {
            this.claimAmountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string commentField {
        get {
            return this.commentFieldField;
        }
        set {
            this.commentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string countryField {
        get {
            return this.countryFieldField;
        }
        set {
            this.countryFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string courtDistrictField {
        get {
            return this.courtDistrictFieldField;
        }
        set {
            this.courtDistrictFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string courtRecordAddressField {
        get {
            return this.courtRecordAddressFieldField;
        }
        set {
            this.courtRecordAddressFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string courtTypeField {
        get {
            return this.courtTypeFieldField;
        }
        set {
            this.courtTypeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string defendantDistrictField {
        get {
            return this.defendantDistrictFieldField;
        }
        set {
            this.defendantDistrictFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string defendantNameField {
        get {
            return this.defendantNameFieldField;
        }
        set {
            this.defendantNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string defendantTradeStyleField {
        get {
            return this.defendantTradeStyleFieldField;
        }
        set {
            this.defendantTradeStyleFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string messageField {
        get {
            return this.messageFieldField;
        }
        set {
            this.messageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string natureOfDebtField {
        get {
            return this.natureOfDebtFieldField;
        }
        set {
            this.natureOfDebtFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string numberFoundField {
        get {
            return this.numberFoundFieldField;
        }
        set {
            this.numberFoundFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string plaintiffNameField {
        get {
            return this.plaintiffNameFieldField;
        }
        set {
            this.plaintiffNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string postCodeField {
        get {
            return this.postCodeFieldField;
        }
        set {
            this.postCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string returnDateField {
        get {
            return this.returnDateFieldField;
        }
        set {
            this.returnDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string serialNumberField {
        get {
            return this.serialNumberFieldField;
        }
        set {
            this.serialNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string suburbField {
        get {
            return this.suburbFieldField;
        }
        set {
            this.suburbFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string typeCodeField {
        get {
            return this.typeCodeFieldField;
        }
        set {
            this.typeCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string typeDescField {
        get {
            return this.typeDescFieldField;
        }
        set {
            this.typeDescFieldField = value;
        }
    }
}

/// <remarks/>





public partial class Tradex : BureauSegment {
    
    private string averageDaysEarlyLateFieldField;
    
    private string calculationPeriodFieldField;
    
    private CreditBand[] creditBandsFieldField;
    
    private string dateCreatedFieldField;
    
    private string debtorNameFieldField;
    
    private string globalScoreFieldField;
    
    private string majorProductFieldField;
    
    private string scoreCriteriaFieldField;
    
    private string totalInvoiceAmountFieldField;
    
    private string totalInvoicesFieldField;
    
    private string totalOverdueAmountFieldField;
    
    private string totalOverdueInvoicesFieldField;
    
    /// <remarks/>
    
    public string averageDaysEarlyLateField {
        get {
            return this.averageDaysEarlyLateFieldField;
        }
        set {
            this.averageDaysEarlyLateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string calculationPeriodField {
        get {
            return this.calculationPeriodFieldField;
        }
        set {
            this.calculationPeriodFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public CreditBand[] creditBandsField {
        get {
            return this.creditBandsFieldField;
        }
        set {
            this.creditBandsFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateCreatedField {
        get {
            return this.dateCreatedFieldField;
        }
        set {
            this.dateCreatedFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string debtorNameField {
        get {
            return this.debtorNameFieldField;
        }
        set {
            this.debtorNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string globalScoreField {
        get {
            return this.globalScoreFieldField;
        }
        set {
            this.globalScoreFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string scoreCriteriaField {
        get {
            return this.scoreCriteriaFieldField;
        }
        set {
            this.scoreCriteriaFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string totalInvoiceAmountField {
        get {
            return this.totalInvoiceAmountFieldField;
        }
        set {
            this.totalInvoiceAmountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string totalInvoicesField {
        get {
            return this.totalInvoicesFieldField;
        }
        set {
            this.totalInvoicesFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string totalOverdueAmountField {
        get {
            return this.totalOverdueAmountFieldField;
        }
        set {
            this.totalOverdueAmountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string totalOverdueInvoicesField {
        get {
            return this.totalOverdueInvoicesFieldField;
        }
        set {
            this.totalOverdueInvoicesFieldField = value;
        }
    }
}

/// <remarks/>





public partial class TradeReference : BureauSegment {
    
    private string commentFieldField;
    
    private string creditLimitFieldField;
    
    private string dateOfRefFieldField;
    
    private string discountFieldField;
    
    private string insuranceDescFieldField;
    
    private string insuranceFieldField;
    
    private string majorProductFieldField;
    
    private string monthsKnownFieldField;
    
    private string obtainedFieldField;
    
    private string purchasesFieldField;
    
    private string referenceNameFieldField;
    
    private string termsGivenFieldField;
    
    private string termsTakenFieldField;
    
    private string tradeAccountNumberFieldField;
    
    private string unlimitedFieldField;
    
    private string yearsKnownFieldField;
    
    /// <remarks/>
    
    public string commentField {
        get {
            return this.commentFieldField;
        }
        set {
            this.commentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string creditLimitField {
        get {
            return this.creditLimitFieldField;
        }
        set {
            this.creditLimitFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateOfRefField {
        get {
            return this.dateOfRefFieldField;
        }
        set {
            this.dateOfRefFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string discountField {
        get {
            return this.discountFieldField;
        }
        set {
            this.discountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string insuranceDescField {
        get {
            return this.insuranceDescFieldField;
        }
        set {
            this.insuranceDescFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string insuranceField {
        get {
            return this.insuranceFieldField;
        }
        set {
            this.insuranceFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string monthsKnownField {
        get {
            return this.monthsKnownFieldField;
        }
        set {
            this.monthsKnownFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string obtainedField {
        get {
            return this.obtainedFieldField;
        }
        set {
            this.obtainedFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string purchasesField {
        get {
            return this.purchasesFieldField;
        }
        set {
            this.purchasesFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string referenceNameField {
        get {
            return this.referenceNameFieldField;
        }
        set {
            this.referenceNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string termsGivenField {
        get {
            return this.termsGivenFieldField;
        }
        set {
            this.termsGivenFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string termsTakenField {
        get {
            return this.termsTakenFieldField;
        }
        set {
            this.termsTakenFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string tradeAccountNumberField {
        get {
            return this.tradeAccountNumberFieldField;
        }
        set {
            this.tradeAccountNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string unlimitedField {
        get {
            return this.unlimitedFieldField;
        }
        set {
            this.unlimitedFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string yearsKnownField {
        get {
            return this.yearsKnownFieldField;
        }
        set {
            this.yearsKnownFieldField = value;
        }
    }
}

/// <remarks/>





public partial class TradeReferenceSummary : BureauSegment {
    
    private string majorProductFieldField;
    
    private string sixMonthAverageFieldField;
    
    private string sixMonthNumberOutsideTermsFieldField;
    
    private string sixMonthNumberWithinTermsFieldField;
    
    private string sixMonthTotalFieldField;
    
    private string sixMonthWeightedAverageFieldField;
    
    private string threeMonthAverageFieldField;
    
    private string threeMonthNumberOutsideTermsFieldField;
    
    private string threeMonthNumberWithinTermsFieldField;
    
    private string threeMonthTotalFieldField;
    
    private string threeMonthWeightedAverageFieldField;
    
    private string twelveMonthAverageFieldField;
    
    private string twelveMonthNumberOutsideTermsFieldField;
    
    private string twelveMonthNumberWithinTermsFieldField;
    
    private string twelveMonthTotalFieldField;
    
    private string twelveMonthWeightedAverageFieldField;
    
    private string twentyFourMonthAverageFieldField;
    
    private string twentyFourMonthNumberOutsideTermsFieldField;
    
    private string twentyFourMonthNumberWithinTermsFieldField;
    
    private string twentyFourMonthTotalFieldField;
    
    private string twentyFourMonthWeightedAverageFieldField;
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string sixMonthAverageField {
        get {
            return this.sixMonthAverageFieldField;
        }
        set {
            this.sixMonthAverageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string sixMonthNumberOutsideTermsField {
        get {
            return this.sixMonthNumberOutsideTermsFieldField;
        }
        set {
            this.sixMonthNumberOutsideTermsFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string sixMonthNumberWithinTermsField {
        get {
            return this.sixMonthNumberWithinTermsFieldField;
        }
        set {
            this.sixMonthNumberWithinTermsFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string sixMonthTotalField {
        get {
            return this.sixMonthTotalFieldField;
        }
        set {
            this.sixMonthTotalFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string sixMonthWeightedAverageField {
        get {
            return this.sixMonthWeightedAverageFieldField;
        }
        set {
            this.sixMonthWeightedAverageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string threeMonthAverageField {
        get {
            return this.threeMonthAverageFieldField;
        }
        set {
            this.threeMonthAverageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string threeMonthNumberOutsideTermsField {
        get {
            return this.threeMonthNumberOutsideTermsFieldField;
        }
        set {
            this.threeMonthNumberOutsideTermsFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string threeMonthNumberWithinTermsField {
        get {
            return this.threeMonthNumberWithinTermsFieldField;
        }
        set {
            this.threeMonthNumberWithinTermsFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string threeMonthTotalField {
        get {
            return this.threeMonthTotalFieldField;
        }
        set {
            this.threeMonthTotalFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string threeMonthWeightedAverageField {
        get {
            return this.threeMonthWeightedAverageFieldField;
        }
        set {
            this.threeMonthWeightedAverageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string twelveMonthAverageField {
        get {
            return this.twelveMonthAverageFieldField;
        }
        set {
            this.twelveMonthAverageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string twelveMonthNumberOutsideTermsField {
        get {
            return this.twelveMonthNumberOutsideTermsFieldField;
        }
        set {
            this.twelveMonthNumberOutsideTermsFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string twelveMonthNumberWithinTermsField {
        get {
            return this.twelveMonthNumberWithinTermsFieldField;
        }
        set {
            this.twelveMonthNumberWithinTermsFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string twelveMonthTotalField {
        get {
            return this.twelveMonthTotalFieldField;
        }
        set {
            this.twelveMonthTotalFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string twelveMonthWeightedAverageField {
        get {
            return this.twelveMonthWeightedAverageFieldField;
        }
        set {
            this.twelveMonthWeightedAverageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string twentyFourMonthAverageField {
        get {
            return this.twentyFourMonthAverageFieldField;
        }
        set {
            this.twentyFourMonthAverageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string twentyFourMonthNumberOutsideTermsField {
        get {
            return this.twentyFourMonthNumberOutsideTermsFieldField;
        }
        set {
            this.twentyFourMonthNumberOutsideTermsFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string twentyFourMonthNumberWithinTermsField {
        get {
            return this.twentyFourMonthNumberWithinTermsFieldField;
        }
        set {
            this.twentyFourMonthNumberWithinTermsFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string twentyFourMonthTotalField {
        get {
            return this.twentyFourMonthTotalFieldField;
        }
        set {
            this.twentyFourMonthTotalFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string twentyFourMonthWeightedAverageField {
        get {
            return this.twentyFourMonthWeightedAverageFieldField;
        }
        set {
            this.twentyFourMonthWeightedAverageFieldField = value;
        }
    }
}

/// <remarks/>





public partial class TradePaymentHistory : BureauSegment {
    
    private string majorProductFieldField;
    
    private string messageFieldField;
    
    private PaymentHistoryDetail[] paymentHistoryDetailsFieldField;
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string messageField {
        get {
            return this.messageFieldField;
        }
        set {
            this.messageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public PaymentHistoryDetail[] paymentHistoryDetailsField {
        get {
            return this.paymentHistoryDetailsFieldField;
        }
        set {
            this.paymentHistoryDetailsFieldField = value;
        }
    }
}

/// <remarks/>





public partial class TradeHistory : BureauSegment {
    
    private string assuredValueFieldField;
    
    private string commentFieldField;
    
    private string creditLimitFieldField;
    
    private string dateOfRefFieldField;
    
    private string discountFieldField;
    
    private string insuranceDescFieldField;
    
    private string insuranceFieldField;
    
    private string majorProductFieldField;
    
    private string monthsKnownFieldField;
    
    private string obtainedFieldField;
    
    private string purchasesFieldField;
    
    private string referenceNameFieldField;
    
    private string termsGivenFieldField;
    
    private string termsTakenFieldField;
    
    private string unlimitedFieldField;
    
    private string yearsKnownFieldField;
    
    /// <remarks/>
    
    public string assuredValueField {
        get {
            return this.assuredValueFieldField;
        }
        set {
            this.assuredValueFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string commentField {
        get {
            return this.commentFieldField;
        }
        set {
            this.commentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string creditLimitField {
        get {
            return this.creditLimitFieldField;
        }
        set {
            this.creditLimitFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateOfRefField {
        get {
            return this.dateOfRefFieldField;
        }
        set {
            this.dateOfRefFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string discountField {
        get {
            return this.discountFieldField;
        }
        set {
            this.discountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string insuranceDescField {
        get {
            return this.insuranceDescFieldField;
        }
        set {
            this.insuranceDescFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string insuranceField {
        get {
            return this.insuranceFieldField;
        }
        set {
            this.insuranceFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string monthsKnownField {
        get {
            return this.monthsKnownFieldField;
        }
        set {
            this.monthsKnownFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string obtainedField {
        get {
            return this.obtainedFieldField;
        }
        set {
            this.obtainedFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string purchasesField {
        get {
            return this.purchasesFieldField;
        }
        set {
            this.purchasesFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string referenceNameField {
        get {
            return this.referenceNameFieldField;
        }
        set {
            this.referenceNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string termsGivenField {
        get {
            return this.termsGivenFieldField;
        }
        set {
            this.termsGivenFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string termsTakenField {
        get {
            return this.termsTakenFieldField;
        }
        set {
            this.termsTakenFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string unlimitedField {
        get {
            return this.unlimitedFieldField;
        }
        set {
            this.unlimitedFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string yearsKnownField {
        get {
            return this.yearsKnownFieldField;
        }
        set {
            this.yearsKnownFieldField = value;
        }
    }
}

/// <remarks/>





public partial class TradeAgeAnalysis : BureauSegment {
    
    private AgeAnalysisDetail[] ageAnalysisDetailsFieldField;
    
    private string dateFieldField;
    
    private string majorProductFieldField;
    
    private string messageFieldField;
    
    /// <remarks/>
    
    public AgeAnalysisDetail[] ageAnalysisDetailsField {
        get {
            return this.ageAnalysisDetailsFieldField;
        }
        set {
            this.ageAnalysisDetailsFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateField {
        get {
            return this.dateFieldField;
        }
        set {
            this.dateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string messageField {
        get {
            return this.messageFieldField;
        }
        set {
            this.messageFieldField = value;
        }
    }
}

/// <remarks/>





public partial class TradeAccountSynopsis : BureauSegment {
    
    private AccountSynopsisDetail[] accountSynopsisDetailsFieldField;
    
    private string dateFieldField;
    
    private string majorProductFieldField;
    
    private string messageFieldField;
    
    /// <remarks/>
    
    public AccountSynopsisDetail[] accountSynopsisDetailsField {
        get {
            return this.accountSynopsisDetailsFieldField;
        }
        set {
            this.accountSynopsisDetailsFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateField {
        get {
            return this.dateFieldField;
        }
        set {
            this.dateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string messageField {
        get {
            return this.messageFieldField;
        }
        set {
            this.messageFieldField = value;
        }
    }
}

/// <remarks/>





public partial class TicketStatus : BureauSegment {
    
    private string[] statusFieldField;
    
    private string[] subjectNameFieldField;
    
    private string[] ticketFieldField;
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] statusField {
        get {
            return this.statusFieldField;
        }
        set {
            this.statusFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] subjectNameField {
        get {
            return this.subjectNameFieldField;
        }
        set {
            this.subjectNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] ticketField {
        get {
            return this.ticketFieldField;
        }
        set {
            this.ticketFieldField = value;
        }
    }
}

/// <remarks/>





public partial class SegmentDescriptions : BureauSegment {
    
    private SegmentDescription[] segmentInfoDescriptionsFieldField;
    
    /// <remarks/>
    
    public SegmentDescription[] segmentInfoDescriptionsField {
        get {
            return this.segmentInfoDescriptionsFieldField;
        }
        set {
            this.segmentInfoDescriptionsFieldField = value;
        }
    }
}

/// <remarks/>





public partial class SearchResponse : BureauSegment {
    
    private string businessNameFieldField;
    
    private string countryFieldField;
    
    private string faxNoFieldField;
    
    private string iTNumberFieldField;
    
    private string nameFieldField;
    
    private string nameTypeFieldField;
    
    private string phoneNoFieldField;
    
    private string physicalAddressFieldField;
    
    private string postCodeFieldField;
    
    private string postalAddressFieldField;
    
    private string postalCountryFieldField;
    
    private string postalPostCodeFieldField;
    
    private string postalSuburbFieldField;
    
    private string postalTownFieldField;
    
    private string regNoFieldField;
    
    private string regStatusCodeFieldField;
    
    private string regStatusFieldField;
    
    private string suburbFieldField;
    
    private string townFieldField;
    
    private string tradingNumberFieldField;
    
    /// <remarks/>
    
    public string businessNameField {
        get {
            return this.businessNameFieldField;
        }
        set {
            this.businessNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string countryField {
        get {
            return this.countryFieldField;
        }
        set {
            this.countryFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string faxNoField {
        get {
            return this.faxNoFieldField;
        }
        set {
            this.faxNoFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string iTNumberField {
        get {
            return this.iTNumberFieldField;
        }
        set {
            this.iTNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string nameField {
        get {
            return this.nameFieldField;
        }
        set {
            this.nameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string nameTypeField {
        get {
            return this.nameTypeFieldField;
        }
        set {
            this.nameTypeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string phoneNoField {
        get {
            return this.phoneNoFieldField;
        }
        set {
            this.phoneNoFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string physicalAddressField {
        get {
            return this.physicalAddressFieldField;
        }
        set {
            this.physicalAddressFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string postCodeField {
        get {
            return this.postCodeFieldField;
        }
        set {
            this.postCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string postalAddressField {
        get {
            return this.postalAddressFieldField;
        }
        set {
            this.postalAddressFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string postalCountryField {
        get {
            return this.postalCountryFieldField;
        }
        set {
            this.postalCountryFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string postalPostCodeField {
        get {
            return this.postalPostCodeFieldField;
        }
        set {
            this.postalPostCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string postalSuburbField {
        get {
            return this.postalSuburbFieldField;
        }
        set {
            this.postalSuburbFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string postalTownField {
        get {
            return this.postalTownFieldField;
        }
        set {
            this.postalTownFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string regNoField {
        get {
            return this.regNoFieldField;
        }
        set {
            this.regNoFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string regStatusCodeField {
        get {
            return this.regStatusCodeFieldField;
        }
        set {
            this.regStatusCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string regStatusField {
        get {
            return this.regStatusFieldField;
        }
        set {
            this.regStatusFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string suburbField {
        get {
            return this.suburbFieldField;
        }
        set {
            this.suburbFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string townField {
        get {
            return this.townFieldField;
        }
        set {
            this.townFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string tradingNumberField {
        get {
            return this.tradingNumberFieldField;
        }
        set {
            this.tradingNumberFieldField = value;
        }
    }
}

/// <remarks/>





public partial class SMEAssessment : BureauSegment {
    
    private string[] averageGraphValueFieldField;
    
    private string bandDescFieldField;
    
    private string bandTrendFieldField;
    
    private string[] commentsFieldField;
    
    private string failureRateFieldField;
    
    private string[] historyAverageFieldField;
    
    private string[] historyDateFieldField;
    
    private string[] historyScoreFieldField;
    
    private string[] keyContrCodesFieldField;
    
    private string majorProductFieldField;
    
    private string oddsOfFailureFieldField;
    
    private string[] recomActionCodeFieldField;
    
    private string sMECommentCodeFieldField;
    
    private string scoreBandFieldField;
    
    private string scoreDateFieldField;
    
    private string scoreFieldField;
    
    private string sizeFieldField;
    
    private string[] subjectGraphValueFieldField;
    
    private string typeOfScoreFieldField;
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] averageGraphValueField {
        get {
            return this.averageGraphValueFieldField;
        }
        set {
            this.averageGraphValueFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bandDescField {
        get {
            return this.bandDescFieldField;
        }
        set {
            this.bandDescFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bandTrendField {
        get {
            return this.bandTrendFieldField;
        }
        set {
            this.bandTrendFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] commentsField {
        get {
            return this.commentsFieldField;
        }
        set {
            this.commentsFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string failureRateField {
        get {
            return this.failureRateFieldField;
        }
        set {
            this.failureRateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] historyAverageField {
        get {
            return this.historyAverageFieldField;
        }
        set {
            this.historyAverageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] historyDateField {
        get {
            return this.historyDateFieldField;
        }
        set {
            this.historyDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] historyScoreField {
        get {
            return this.historyScoreFieldField;
        }
        set {
            this.historyScoreFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] keyContrCodesField {
        get {
            return this.keyContrCodesFieldField;
        }
        set {
            this.keyContrCodesFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string oddsOfFailureField {
        get {
            return this.oddsOfFailureFieldField;
        }
        set {
            this.oddsOfFailureFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] recomActionCodeField {
        get {
            return this.recomActionCodeFieldField;
        }
        set {
            this.recomActionCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string sMECommentCodeField {
        get {
            return this.sMECommentCodeFieldField;
        }
        set {
            this.sMECommentCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string scoreBandField {
        get {
            return this.scoreBandFieldField;
        }
        set {
            this.scoreBandFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string scoreDateField {
        get {
            return this.scoreDateFieldField;
        }
        set {
            this.scoreDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string scoreField {
        get {
            return this.scoreFieldField;
        }
        set {
            this.scoreFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string sizeField {
        get {
            return this.sizeFieldField;
        }
        set {
            this.sizeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] subjectGraphValueField {
        get {
            return this.subjectGraphValueFieldField;
        }
        set {
            this.subjectGraphValueFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string typeOfScoreField {
        get {
            return this.typeOfScoreFieldField;
        }
        set {
            this.typeOfScoreFieldField = value;
        }
    }
}

/// <remarks/>





public partial class RegistrationDetails : BureauSegment {
    
    private string auditorsFieldField;
    
    private string authorisedCapitalFieldField;
    
    private CapitalInfo[] capitalInfoFieldField;
    
    private string cityFieldField;
    
    private string companyTypeCodeFieldField;
    
    private string companyTypeDescFieldField;
    
    private string countryFieldField;
    
    private string financialYearEndFieldField;
    
    private string issuedCapitalFieldField;
    
    private string liquidationDateFieldField;
    
    private string majorProductFieldField;
    
    private string postCodeFieldField;
    
    private string[] postalAddressFieldField;
    
    private string postalPostCodeFieldField;
    
    private string[] previousRegistrationNoFieldField;
    
    private string reg_Info_DateFieldField;
    
    private string reg_Status_ReasonFieldField;
    
    private string registeredAddressFieldField;
    
    private string registrationCountryFieldField;
    
    private string registrationDateFieldField;
    
    private string registrationNoFieldField;
    
    private string registrationStatusCodeFieldField;
    
    private string registrationStatusDescFieldField;
    
    private ShareInfo[] shareInfoFieldField;
    
    private string statedCapitalFieldField;
    
    private string suburbFieldField;
    
    /// <remarks/>
    
    public string auditorsField {
        get {
            return this.auditorsFieldField;
        }
        set {
            this.auditorsFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string authorisedCapitalField {
        get {
            return this.authorisedCapitalFieldField;
        }
        set {
            this.authorisedCapitalFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public CapitalInfo[] capitalInfoField {
        get {
            return this.capitalInfoFieldField;
        }
        set {
            this.capitalInfoFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string cityField {
        get {
            return this.cityFieldField;
        }
        set {
            this.cityFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string companyTypeCodeField {
        get {
            return this.companyTypeCodeFieldField;
        }
        set {
            this.companyTypeCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string companyTypeDescField {
        get {
            return this.companyTypeDescFieldField;
        }
        set {
            this.companyTypeDescFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string countryField {
        get {
            return this.countryFieldField;
        }
        set {
            this.countryFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string financialYearEndField {
        get {
            return this.financialYearEndFieldField;
        }
        set {
            this.financialYearEndFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string issuedCapitalField {
        get {
            return this.issuedCapitalFieldField;
        }
        set {
            this.issuedCapitalFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string liquidationDateField {
        get {
            return this.liquidationDateFieldField;
        }
        set {
            this.liquidationDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string postCodeField {
        get {
            return this.postCodeFieldField;
        }
        set {
            this.postCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] postalAddressField {
        get {
            return this.postalAddressFieldField;
        }
        set {
            this.postalAddressFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string postalPostCodeField {
        get {
            return this.postalPostCodeFieldField;
        }
        set {
            this.postalPostCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] previousRegistrationNoField {
        get {
            return this.previousRegistrationNoFieldField;
        }
        set {
            this.previousRegistrationNoFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string reg_Info_DateField {
        get {
            return this.reg_Info_DateFieldField;
        }
        set {
            this.reg_Info_DateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string reg_Status_ReasonField {
        get {
            return this.reg_Status_ReasonFieldField;
        }
        set {
            this.reg_Status_ReasonFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string registeredAddressField {
        get {
            return this.registeredAddressFieldField;
        }
        set {
            this.registeredAddressFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string registrationCountryField {
        get {
            return this.registrationCountryFieldField;
        }
        set {
            this.registrationCountryFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string registrationDateField {
        get {
            return this.registrationDateFieldField;
        }
        set {
            this.registrationDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string registrationNoField {
        get {
            return this.registrationNoFieldField;
        }
        set {
            this.registrationNoFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string registrationStatusCodeField {
        get {
            return this.registrationStatusCodeFieldField;
        }
        set {
            this.registrationStatusCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string registrationStatusDescField {
        get {
            return this.registrationStatusDescFieldField;
        }
        set {
            this.registrationStatusDescFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public ShareInfo[] shareInfoField {
        get {
            return this.shareInfoFieldField;
        }
        set {
            this.shareInfoFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string statedCapitalField {
        get {
            return this.statedCapitalFieldField;
        }
        set {
            this.statedCapitalFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string suburbField {
        get {
            return this.suburbFieldField;
        }
        set {
            this.suburbFieldField = value;
        }
    }
}

/// <remarks/>





public partial class RegistrationDetailsExtended : BureauSegment {
    
    private string businessRescueCommentFieldField;
    
    private string businessRescueDateFieldField;
    
    private string majorProductFieldField;
    
    /// <remarks/>
    
    public string businessRescueCommentField {
        get {
            return this.businessRescueCommentFieldField;
        }
        set {
            this.businessRescueCommentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string businessRescueDateField {
        get {
            return this.businessRescueDateFieldField;
        }
        set {
            this.businessRescueDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
}

/// <remarks/>





public partial class RegisteredPrincipal : BureauSegment {
    
    private string iDNumberFieldField;
    
    private string initialsFieldField;
    
    private string majorProductFieldField;
    
    private string memberContributionFieldField;
    
    private string memberContributionPercFieldField;
    
    private string rPDateFieldField;
    
    private string surnameFieldField;
    
    /// <remarks/>
    
    public string iDNumberField {
        get {
            return this.iDNumberFieldField;
        }
        set {
            this.iDNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string initialsField {
        get {
            return this.initialsFieldField;
        }
        set {
            this.initialsFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string memberContributionField {
        get {
            return this.memberContributionFieldField;
        }
        set {
            this.memberContributionFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string memberContributionPercField {
        get {
            return this.memberContributionPercFieldField;
        }
        set {
            this.memberContributionPercFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string rPDateField {
        get {
            return this.rPDateFieldField;
        }
        set {
            this.rPDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string surnameField {
        get {
            return this.surnameFieldField;
        }
        set {
            this.surnameFieldField = value;
        }
    }
}

/// <remarks/>




//[System.Xml.Serialization.XmlTypeAttribute(TypeName="Principal", Namespace="http://schemas.datacontract.org/2004/07/MTN_Commercial.CommercialBureauService")]
public partial class Principal1 : BureauSegment {
    
    private string assetNotarialBondFieldField;
    
    private string civilCourtRecordFieldField;
    
    private string commentFieldField;
    
    private string confirmedByRegistrarFieldField;
    
    private string dateDisputedFieldField;
    
    private string dateOfBirthFieldField;
    
    private string dateStartedFieldField;
    
    private string debtCouncilDateFieldField;
    
    private string debtCouncilDescFieldField;
    
    private string deedsFieldField;
    
    private string defaultDataFieldField;
    
    private string empiricaExclusionCodeFieldField;
    
    private string empiricaExclusionDescriptionFieldField;
    
    private string[] empiricaReasonCodeFieldField;
    
    private string[] empiricaReasonDescriptionFieldField;
    
    private string empiricaScoreFieldField;
    
    private string forename1FieldField;
    
    private string forename2FieldField;
    
    private string forename3FieldField;
    
    private string iDNumberFieldField;
    
    private string infoDateFieldField;
    
    private string majorProductFieldField;
    
    private string noticesOrNotarialBondsFieldField;
    
    private string positionFieldField;
    
    private string surnameFieldField;
    
    /// <remarks/>
    
    public string assetNotarialBondField {
        get {
            return this.assetNotarialBondFieldField;
        }
        set {
            this.assetNotarialBondFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string civilCourtRecordField {
        get {
            return this.civilCourtRecordFieldField;
        }
        set {
            this.civilCourtRecordFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string commentField {
        get {
            return this.commentFieldField;
        }
        set {
            this.commentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string confirmedByRegistrarField {
        get {
            return this.confirmedByRegistrarFieldField;
        }
        set {
            this.confirmedByRegistrarFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateDisputedField {
        get {
            return this.dateDisputedFieldField;
        }
        set {
            this.dateDisputedFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateOfBirthField {
        get {
            return this.dateOfBirthFieldField;
        }
        set {
            this.dateOfBirthFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateStartedField {
        get {
            return this.dateStartedFieldField;
        }
        set {
            this.dateStartedFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string debtCouncilDateField {
        get {
            return this.debtCouncilDateFieldField;
        }
        set {
            this.debtCouncilDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string debtCouncilDescField {
        get {
            return this.debtCouncilDescFieldField;
        }
        set {
            this.debtCouncilDescFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string deedsField {
        get {
            return this.deedsFieldField;
        }
        set {
            this.deedsFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string defaultDataField {
        get {
            return this.defaultDataFieldField;
        }
        set {
            this.defaultDataFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string empiricaExclusionCodeField {
        get {
            return this.empiricaExclusionCodeFieldField;
        }
        set {
            this.empiricaExclusionCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string empiricaExclusionDescriptionField {
        get {
            return this.empiricaExclusionDescriptionFieldField;
        }
        set {
            this.empiricaExclusionDescriptionFieldField = value;
        }
    }
    
    /// <remarks/>
    
   // // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] empiricaReasonCodeField {
        get {
            return this.empiricaReasonCodeFieldField;
        }
        set {
            this.empiricaReasonCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
   // // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] empiricaReasonDescriptionField {
        get {
            return this.empiricaReasonDescriptionFieldField;
        }
        set {
            this.empiricaReasonDescriptionFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string empiricaScoreField {
        get {
            return this.empiricaScoreFieldField;
        }
        set {
            this.empiricaScoreFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string forename1Field {
        get {
            return this.forename1FieldField;
        }
        set {
            this.forename1FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string forename2Field {
        get {
            return this.forename2FieldField;
        }
        set {
            this.forename2FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string forename3Field {
        get {
            return this.forename3FieldField;
        }
        set {
            this.forename3FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string iDNumberField {
        get {
            return this.iDNumberFieldField;
        }
        set {
            this.iDNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string infoDateField {
        get {
            return this.infoDateFieldField;
        }
        set {
            this.infoDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string noticesOrNotarialBondsField {
        get {
            return this.noticesOrNotarialBondsFieldField;
        }
        set {
            this.noticesOrNotarialBondsFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string positionField {
        get {
            return this.positionFieldField;
        }
        set {
            this.positionFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string surnameField {
        get {
            return this.surnameFieldField;
        }
        set {
            this.surnameFieldField = value;
        }
    }
}

/// <remarks/>





public partial class PrincipalDetailEmpirica : BureauSegment {
    
    private string assetNotarialBondFieldField;
    
    private string civilCourtRecordFieldField;
    
    private string commentFieldField;
    
    private string confirmedByRegistrarFieldField;
    
    private string dateDisputedFieldField;
    
    private string dateOfBirthFieldField;
    
    private string dateStartedFieldField;
    
    private string debtCouncilDateFieldField;
    
    private string debtCouncilDescFieldField;
    
    private string deedsFieldField;
    
    private string defaultDataFieldField;
    
    private string empiricaExclusionCodeFieldField;
    
    private string empiricaExclusionDescriptionFieldField;
    
    private string[] empiricaReasonCodeFieldField;
    
    private string[] empiricaReasonDescriptionFieldField;
    
    private string empiricaScoreFieldField;
    
    private string forename1FieldField;
    
    private string forename2FieldField;
    
    private string forename3FieldField;
    
    private string iDNumberFieldField;
    
    private string infoDateFieldField;
    
    private string majorProductFieldField;
    
    private string noticesOrNotarialBondsFieldField;
    
    private string positionFieldField;
    
    private string surnameFieldField;
    
    /// <remarks/>
    
    public string assetNotarialBondField {
        get {
            return this.assetNotarialBondFieldField;
        }
        set {
            this.assetNotarialBondFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string civilCourtRecordField {
        get {
            return this.civilCourtRecordFieldField;
        }
        set {
            this.civilCourtRecordFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string commentField {
        get {
            return this.commentFieldField;
        }
        set {
            this.commentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string confirmedByRegistrarField {
        get {
            return this.confirmedByRegistrarFieldField;
        }
        set {
            this.confirmedByRegistrarFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateDisputedField {
        get {
            return this.dateDisputedFieldField;
        }
        set {
            this.dateDisputedFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateOfBirthField {
        get {
            return this.dateOfBirthFieldField;
        }
        set {
            this.dateOfBirthFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateStartedField {
        get {
            return this.dateStartedFieldField;
        }
        set {
            this.dateStartedFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string debtCouncilDateField {
        get {
            return this.debtCouncilDateFieldField;
        }
        set {
            this.debtCouncilDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string debtCouncilDescField {
        get {
            return this.debtCouncilDescFieldField;
        }
        set {
            this.debtCouncilDescFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string deedsField {
        get {
            return this.deedsFieldField;
        }
        set {
            this.deedsFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string defaultDataField {
        get {
            return this.defaultDataFieldField;
        }
        set {
            this.defaultDataFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string empiricaExclusionCodeField {
        get {
            return this.empiricaExclusionCodeFieldField;
        }
        set {
            this.empiricaExclusionCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string empiricaExclusionDescriptionField {
        get {
            return this.empiricaExclusionDescriptionFieldField;
        }
        set {
            this.empiricaExclusionDescriptionFieldField = value;
        }
    }
    
    /// <remarks/>
    
    //// [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] empiricaReasonCodeField {
        get {
            return this.empiricaReasonCodeFieldField;
        }
        set {
            this.empiricaReasonCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
   // // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] empiricaReasonDescriptionField {
        get {
            return this.empiricaReasonDescriptionFieldField;
        }
        set {
            this.empiricaReasonDescriptionFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string empiricaScoreField {
        get {
            return this.empiricaScoreFieldField;
        }
        set {
            this.empiricaScoreFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string forename1Field {
        get {
            return this.forename1FieldField;
        }
        set {
            this.forename1FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string forename2Field {
        get {
            return this.forename2FieldField;
        }
        set {
            this.forename2FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string forename3Field {
        get {
            return this.forename3FieldField;
        }
        set {
            this.forename3FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string iDNumberField {
        get {
            return this.iDNumberFieldField;
        }
        set {
            this.iDNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string infoDateField {
        get {
            return this.infoDateFieldField;
        }
        set {
            this.infoDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string noticesOrNotarialBondsField {
        get {
            return this.noticesOrNotarialBondsFieldField;
        }
        set {
            this.noticesOrNotarialBondsFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string positionField {
        get {
            return this.positionFieldField;
        }
        set {
            this.positionFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string surnameField {
        get {
            return this.surnameFieldField;
        }
        set {
            this.surnameFieldField = value;
        }
    }
}

/// <remarks/>





public partial class PrincipalNotarialBonds : BureauSegment {
    
    private string bondAmountFieldField;
    
    private string bondNumberFieldField;
    
    private string bondOwnerFieldField;
    
    private string bondPercentageFieldField;
    
    private string bondRegDateFieldField;
    
    private string bondTypeFieldField;
    
    private string deedsOfficeFieldField;
    
    private string hOAboveThresholdFieldField;
    
    private string[] heldOverFieldField;
    
    private string majorProductFieldField;
    
    private string messageFieldField;
    
    private string microfilemRefFieldField;
    
    private string multipleOwnersFieldField;
    
    private string ownerIDFieldField;
    
    private string ownerTypeFieldField;
    
    private string parentChildIndFieldField;
    
    /// <remarks/>
    
    public string bondAmountField {
        get {
            return this.bondAmountFieldField;
        }
        set {
            this.bondAmountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondNumberField {
        get {
            return this.bondNumberFieldField;
        }
        set {
            this.bondNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondOwnerField {
        get {
            return this.bondOwnerFieldField;
        }
        set {
            this.bondOwnerFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondPercentageField {
        get {
            return this.bondPercentageFieldField;
        }
        set {
            this.bondPercentageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondRegDateField {
        get {
            return this.bondRegDateFieldField;
        }
        set {
            this.bondRegDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondTypeField {
        get {
            return this.bondTypeFieldField;
        }
        set {
            this.bondTypeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string deedsOfficeField {
        get {
            return this.deedsOfficeFieldField;
        }
        set {
            this.deedsOfficeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string hOAboveThresholdField {
        get {
            return this.hOAboveThresholdFieldField;
        }
        set {
            this.hOAboveThresholdFieldField = value;
        }
    }
    
    /// <remarks/>
    
    //// [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] heldOverField {
        get {
            return this.heldOverFieldField;
        }
        set {
            this.heldOverFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string messageField {
        get {
            return this.messageFieldField;
        }
        set {
            this.messageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string microfilemRefField {
        get {
            return this.microfilemRefFieldField;
        }
        set {
            this.microfilemRefFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string multipleOwnersField {
        get {
            return this.multipleOwnersFieldField;
        }
        set {
            this.multipleOwnersFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string ownerIDField {
        get {
            return this.ownerIDFieldField;
        }
        set {
            this.ownerIDFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string ownerTypeField {
        get {
            return this.ownerTypeFieldField;
        }
        set {
            this.ownerTypeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string parentChildIndField {
        get {
            return this.parentChildIndFieldField;
        }
        set {
            this.parentChildIndFieldField = value;
        }
    }
}

/// <remarks/>





public partial class LinkedCompany : BureauSegment {
    
    private string[] businessNameFieldField;
    
    private string[] businessStatusFieldField;
    
    private string[] registeredDateFieldField;
    
    /// <remarks/>
    
   // // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] businessNameField {
        get {
            return this.businessNameFieldField;
        }
        set {
            this.businessNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
   // // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] businessStatusField {
        get {
            return this.businessStatusFieldField;
        }
        set {
            this.businessStatusFieldField = value;
        }
    }
    
    /// <remarks/>
    
   // // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] registeredDateField {
        get {
            return this.registeredDateFieldField;
        }
        set {
            this.registeredDateFieldField = value;
        }
    }
}

/// <remarks/>





public partial class LinkedBusinessDefault : BureauSegment {
    
    private string[] adverseCountFieldField;
    
    private string[] iTNumberFieldField;
    
    private string[] judgementCountFieldField;
    
    private string moduleProductFieldField;
    
    private string[] registrationNumberFieldField;
    
    /// <remarks/>
    
//    // // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] adverseCountField {
        get {
            return this.adverseCountFieldField;
        }
        set {
            this.adverseCountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] iTNumberField {
        get {
            return this.iTNumberFieldField;
        }
        set {
            this.iTNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] judgementCountField {
        get {
            return this.judgementCountFieldField;
        }
        set {
            this.judgementCountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string moduleProductField {
        get {
            return this.moduleProductFieldField;
        }
        set {
            this.moduleProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] registrationNumberField {
        get {
            return this.registrationNumberFieldField;
        }
        set {
            this.registrationNumberFieldField = value;
        }
    }
}

/// <remarks/>





public partial class PrincipalLink : BureauSegment {
    
    private string consumerNoFieldField;
    
    private string dateOfBirthFieldField;
    
    private Established establishedFieldField;
    
    private string forename1FieldField;
    
    private string forename2FieldField;
    
    private string forename3FieldField;
    
    private string iDNumberFieldField;
    
    private LinkedBusinessDefault[] linkedBusinessDefaultsFieldField;
    
    private LinkedCompany[] linkedCompaniesFieldField;
    
    private string majorProductFieldField;
    
    private string surnameFieldField;
    
    /// <remarks/>
    
    public string consumerNoField {
        get {
            return this.consumerNoFieldField;
        }
        set {
            this.consumerNoFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateOfBirthField {
        get {
            return this.dateOfBirthFieldField;
        }
        set {
            this.dateOfBirthFieldField = value;
        }
    }
    
    /// <remarks/>
    public Established establishedField {
        get {
            return this.establishedFieldField;
        }
        set {
            this.establishedFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string forename1Field {
        get {
            return this.forename1FieldField;
        }
        set {
            this.forename1FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string forename2Field {
        get {
            return this.forename2FieldField;
        }
        set {
            this.forename2FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string forename3Field {
        get {
            return this.forename3FieldField;
        }
        set {
            this.forename3FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string iDNumberField {
        get {
            return this.iDNumberFieldField;
        }
        set {
            this.iDNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public LinkedBusinessDefault[] linkedBusinessDefaultsField {
        get {
            return this.linkedBusinessDefaultsFieldField;
        }
        set {
            this.linkedBusinessDefaultsFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public LinkedCompany[] linkedCompaniesField {
        get {
            return this.linkedCompaniesFieldField;
        }
        set {
            this.linkedCompaniesFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string surnameField {
        get {
            return this.surnameFieldField;
        }
        set {
            this.surnameFieldField = value;
        }
    }
}

/// <remarks/>



public enum Established {
    
    /// <remarks/>
    No,
    
    /// <remarks/>
    Yes,
}

/// <remarks/>





public partial class PrincipalIDV : BureauSegment {
    
    private string forename1FieldField;
    
    private string forename2FieldField;
    
    private string hawkCodeFieldField;
    
    private string hawkDescriptionFieldField;
    
    private string hawkNumberFieldField;
    
    private string iDDescriptionFieldField;
    
    private string iDNumberFieldField;
    
    private string iDVerifiedCodeFieldField;
    
    private string oldIDDescriptionFieldField;
    
    private string oldIDWarningFieldField;
    
    private string partFieldField;
    
    private string partSequenceFieldField;
    
    private string recordSequenceFieldField;
    
    private string subscriberNameFieldField;
    
    private string subscriberNumberFieldField;
    
    private string subscriberReferenceFieldField;
    
    private string surnameFieldField;
    
    private string ticketNumberFieldField;
    
    /// <remarks/>
    
    public string forename1Field {
        get {
            return this.forename1FieldField;
        }
        set {
            this.forename1FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string forename2Field {
        get {
            return this.forename2FieldField;
        }
        set {
            this.forename2FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string hawkCodeField {
        get {
            return this.hawkCodeFieldField;
        }
        set {
            this.hawkCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string hawkDescriptionField {
        get {
            return this.hawkDescriptionFieldField;
        }
        set {
            this.hawkDescriptionFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string hawkNumberField {
        get {
            return this.hawkNumberFieldField;
        }
        set {
            this.hawkNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string iDDescriptionField {
        get {
            return this.iDDescriptionFieldField;
        }
        set {
            this.iDDescriptionFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string iDNumberField {
        get {
            return this.iDNumberFieldField;
        }
        set {
            this.iDNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string iDVerifiedCodeField {
        get {
            return this.iDVerifiedCodeFieldField;
        }
        set {
            this.iDVerifiedCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string oldIDDescriptionField {
        get {
            return this.oldIDDescriptionFieldField;
        }
        set {
            this.oldIDDescriptionFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string oldIDWarningField {
        get {
            return this.oldIDWarningFieldField;
        }
        set {
            this.oldIDWarningFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string partField {
        get {
            return this.partFieldField;
        }
        set {
            this.partFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string partSequenceField {
        get {
            return this.partSequenceFieldField;
        }
        set {
            this.partSequenceFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string recordSequenceField {
        get {
            return this.recordSequenceFieldField;
        }
        set {
            this.recordSequenceFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string subscriberNameField {
        get {
            return this.subscriberNameFieldField;
        }
        set {
            this.subscriberNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string subscriberNumberField {
        get {
            return this.subscriberNumberFieldField;
        }
        set {
            this.subscriberNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string subscriberReferenceField {
        get {
            return this.subscriberReferenceFieldField;
        }
        set {
            this.subscriberReferenceFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string surnameField {
        get {
            return this.surnameFieldField;
        }
        set {
            this.surnameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string ticketNumberField {
        get {
            return this.ticketNumberFieldField;
        }
        set {
            this.ticketNumberFieldField = value;
        }
    }
}

/// <remarks/>





public partial class PrincipalFirstResponse : BureauSegment {
    
    private string consumerNumberFieldField;
    
    private string principalNameFieldField;
    
    private string ticketNumberFieldField;
    
    /// <remarks/>
    
    public string consumerNumberField {
        get {
            return this.consumerNumberFieldField;
        }
        set {
            this.consumerNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string principalNameField {
        get {
            return this.principalNameFieldField;
        }
        set {
            this.principalNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string ticketNumberField {
        get {
            return this.ticketNumberFieldField;
        }
        set {
            this.ticketNumberFieldField = value;
        }
    }
}

/// <remarks/>





public partial class PrincipalDeedsSummaryP8 : BureauSegment {
    
    private string bondAmountFieldField;
    
    private string bondFreeAmountFieldField;
    
    private string businessNameFieldField;
    
    private string dateFieldField;
    
    private string dateOfBirthOrIDNumberFieldField;
    
    private string deedsOfficeFieldField;
    
    private string majorProductFieldField;
    
    private string messageFieldField;
    
    private string numberOfPropertiesFieldField;
    
    private string principalNameFieldField;
    
    private string purchasePriceFieldField;
    
    private string registrationNumberFieldField;
    
    /// <remarks/>
    
    public string bondAmountField {
        get {
            return this.bondAmountFieldField;
        }
        set {
            this.bondAmountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondFreeAmountField {
        get {
            return this.bondFreeAmountFieldField;
        }
        set {
            this.bondFreeAmountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string businessNameField {
        get {
            return this.businessNameFieldField;
        }
        set {
            this.businessNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateField {
        get {
            return this.dateFieldField;
        }
        set {
            this.dateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateOfBirthOrIDNumberField {
        get {
            return this.dateOfBirthOrIDNumberFieldField;
        }
        set {
            this.dateOfBirthOrIDNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string deedsOfficeField {
        get {
            return this.deedsOfficeFieldField;
        }
        set {
            this.deedsOfficeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string messageField {
        get {
            return this.messageFieldField;
        }
        set {
            this.messageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string numberOfPropertiesField {
        get {
            return this.numberOfPropertiesFieldField;
        }
        set {
            this.numberOfPropertiesFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string principalNameField {
        get {
            return this.principalNameFieldField;
        }
        set {
            this.principalNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string purchasePriceField {
        get {
            return this.purchasePriceFieldField;
        }
        set {
            this.purchasePriceFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string registrationNumberField {
        get {
            return this.registrationNumberFieldField;
        }
        set {
            this.registrationNumberFieldField = value;
        }
    }
}

/// <remarks/>





public partial class PrincipalDeedsSummaryCO : BureauSegment {
    
    private string bondAmountFieldField;
    
    private string bondFreeAmountFieldField;
    
    private string dateOfBirthOrIDNumberFieldField;
    
    private string deedsDateFieldField;
    
    private string deedsOfficeFieldField;
    
    private string majorProductFieldField;
    
    private string numberOfPropertiesFieldField;
    
    private string principalNameFieldField;
    
    private string principalSurnameFieldField;
    
    private string purchasePriceFieldField;
    
    /// <remarks/>
    
    public string bondAmountField {
        get {
            return this.bondAmountFieldField;
        }
        set {
            this.bondAmountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondFreeAmountField {
        get {
            return this.bondFreeAmountFieldField;
        }
        set {
            this.bondFreeAmountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateOfBirthOrIDNumberField {
        get {
            return this.dateOfBirthOrIDNumberFieldField;
        }
        set {
            this.dateOfBirthOrIDNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string deedsDateField {
        get {
            return this.deedsDateFieldField;
        }
        set {
            this.deedsDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string deedsOfficeField {
        get {
            return this.deedsOfficeFieldField;
        }
        set {
            this.deedsOfficeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string numberOfPropertiesField {
        get {
            return this.numberOfPropertiesFieldField;
        }
        set {
            this.numberOfPropertiesFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string principalNameField {
        get {
            return this.principalNameFieldField;
        }
        set {
            this.principalNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string principalSurnameField {
        get {
            return this.principalSurnameFieldField;
        }
        set {
            this.principalSurnameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string purchasePriceField {
        get {
            return this.purchasePriceFieldField;
        }
        set {
            this.purchasePriceFieldField = value;
        }
    }
}

/// <remarks/>





public partial class PrincipalDeedsComprehensiveCV : BureauSegment {
    
    private string bondAmountFieldField;
    
    private string bondDateFieldField;
    
    private string bondHolderFieldField;
    
    private string bondNumberFieldField;
    
    private string commentFieldField;
    
    private string dateFieldField;
    
    private string dateOfBirthOrIDNumberFieldField;
    
    private string deedsOfficeFieldField;
    
    private string eRFFieldField;
    
    private string farmFieldField;
    
    private string majorProductFieldField;
    
    private string multipleOwnersFieldField;
    
    private string portionFieldField;
    
    private string propertyNameFieldField;
    
    private string propertySizeFieldField;
    
    private string propertyTypeFieldField;
    
    private string purchaseDateFieldField;
    
    private string purchasePriceFieldField;
    
    private string rowIDFieldField;
    
    private string schemeNameFieldField;
    
    private string schemeNumberFieldField;
    
    private string shareFieldField;
    
    private string titleFieldField;
    
    private string townshipFieldField;
    
    /// <remarks/>
    
    public string bondAmountField {
        get {
            return this.bondAmountFieldField;
        }
        set {
            this.bondAmountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondDateField {
        get {
            return this.bondDateFieldField;
        }
        set {
            this.bondDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondHolderField {
        get {
            return this.bondHolderFieldField;
        }
        set {
            this.bondHolderFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondNumberField {
        get {
            return this.bondNumberFieldField;
        }
        set {
            this.bondNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string commentField {
        get {
            return this.commentFieldField;
        }
        set {
            this.commentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateField {
        get {
            return this.dateFieldField;
        }
        set {
            this.dateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateOfBirthOrIDNumberField {
        get {
            return this.dateOfBirthOrIDNumberFieldField;
        }
        set {
            this.dateOfBirthOrIDNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string deedsOfficeField {
        get {
            return this.deedsOfficeFieldField;
        }
        set {
            this.deedsOfficeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string eRFField {
        get {
            return this.eRFFieldField;
        }
        set {
            this.eRFFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string farmField {
        get {
            return this.farmFieldField;
        }
        set {
            this.farmFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string multipleOwnersField {
        get {
            return this.multipleOwnersFieldField;
        }
        set {
            this.multipleOwnersFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string portionField {
        get {
            return this.portionFieldField;
        }
        set {
            this.portionFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string propertyNameField {
        get {
            return this.propertyNameFieldField;
        }
        set {
            this.propertyNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string propertySizeField {
        get {
            return this.propertySizeFieldField;
        }
        set {
            this.propertySizeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string propertyTypeField {
        get {
            return this.propertyTypeFieldField;
        }
        set {
            this.propertyTypeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string purchaseDateField {
        get {
            return this.purchaseDateFieldField;
        }
        set {
            this.purchaseDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string purchasePriceField {
        get {
            return this.purchasePriceFieldField;
        }
        set {
            this.purchasePriceFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string rowIDField {
        get {
            return this.rowIDFieldField;
        }
        set {
            this.rowIDFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string schemeNameField {
        get {
            return this.schemeNameFieldField;
        }
        set {
            this.schemeNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string schemeNumberField {
        get {
            return this.schemeNumberFieldField;
        }
        set {
            this.schemeNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string shareField {
        get {
            return this.shareFieldField;
        }
        set {
            this.shareFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string titleField {
        get {
            return this.titleFieldField;
        }
        set {
            this.titleFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string townshipField {
        get {
            return this.townshipFieldField;
        }
        set {
            this.townshipFieldField = value;
        }
    }
}

/// <remarks/>





public partial class PrincipalDeedsComprehensiveCA : BureauSegment {
    
    private string bondAmountFieldField;
    
    private string bondDateFieldField;
    
    private string bondHolderFieldField;
    
    private string bondNumberFieldField;
    
    private string commentFieldField;
    
    private string dateFieldField;
    
    private string dateOfBirthOrIDNumberFieldField;
    
    private string deedsOfficeFieldField;
    
    private string eRFFieldField;
    
    private string farmFieldField;
    
    private string majorProductFieldField;
    
    private string multipleOwnersFieldField;
    
    private string portionFieldField;
    
    private string propertyNameFieldField;
    
    private string propertySizeFieldField;
    
    private string propertyTypeFieldField;
    
    private string provinceFieldField;
    
    private string purchaseDateFieldField;
    
    private string purchasePriceFieldField;
    
    private string rowIDFieldField;
    
    private string schemeNameFieldField;
    
    private string schemeNumberFieldField;
    
    private string shareFieldField;
    
    private string streetFieldField;
    
    private string titleFieldField;
    
    private string townshipFieldField;
    
    /// <remarks/>
    
    public string bondAmountField {
        get {
            return this.bondAmountFieldField;
        }
        set {
            this.bondAmountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondDateField {
        get {
            return this.bondDateFieldField;
        }
        set {
            this.bondDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondHolderField {
        get {
            return this.bondHolderFieldField;
        }
        set {
            this.bondHolderFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondNumberField {
        get {
            return this.bondNumberFieldField;
        }
        set {
            this.bondNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string commentField {
        get {
            return this.commentFieldField;
        }
        set {
            this.commentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateField {
        get {
            return this.dateFieldField;
        }
        set {
            this.dateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateOfBirthOrIDNumberField {
        get {
            return this.dateOfBirthOrIDNumberFieldField;
        }
        set {
            this.dateOfBirthOrIDNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string deedsOfficeField {
        get {
            return this.deedsOfficeFieldField;
        }
        set {
            this.deedsOfficeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string eRFField {
        get {
            return this.eRFFieldField;
        }
        set {
            this.eRFFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string farmField {
        get {
            return this.farmFieldField;
        }
        set {
            this.farmFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string multipleOwnersField {
        get {
            return this.multipleOwnersFieldField;
        }
        set {
            this.multipleOwnersFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string portionField {
        get {
            return this.portionFieldField;
        }
        set {
            this.portionFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string propertyNameField {
        get {
            return this.propertyNameFieldField;
        }
        set {
            this.propertyNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string propertySizeField {
        get {
            return this.propertySizeFieldField;
        }
        set {
            this.propertySizeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string propertyTypeField {
        get {
            return this.propertyTypeFieldField;
        }
        set {
            this.propertyTypeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string provinceField {
        get {
            return this.provinceFieldField;
        }
        set {
            this.provinceFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string purchaseDateField {
        get {
            return this.purchaseDateFieldField;
        }
        set {
            this.purchaseDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string purchasePriceField {
        get {
            return this.purchasePriceFieldField;
        }
        set {
            this.purchasePriceFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string rowIDField {
        get {
            return this.rowIDFieldField;
        }
        set {
            this.rowIDFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string schemeNameField {
        get {
            return this.schemeNameFieldField;
        }
        set {
            this.schemeNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string schemeNumberField {
        get {
            return this.schemeNumberFieldField;
        }
        set {
            this.schemeNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string shareField {
        get {
            return this.shareFieldField;
        }
        set {
            this.shareFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string streetField {
        get {
            return this.streetFieldField;
        }
        set {
            this.streetFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string titleField {
        get {
            return this.titleFieldField;
        }
        set {
            this.titleFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string townshipField {
        get {
            return this.townshipFieldField;
        }
        set {
            this.townshipFieldField = value;
        }
    }
}

/// <remarks/>





public partial class PrincipalClearance : BureauSegment {
    
    private string civilCourtCountFieldField;
    
    private string dateOfBirthFieldField;
    
    private string debtCouncilDateFieldField;
    
    private string debtCouncilDescFieldField;
    
    private string defaultCountFieldField;
    
    private string disputeDateFieldField;
    
    private Established establishedFieldField;
    
    private string forename1FieldField;
    
    private string forename2FieldField;
    
    private string forename3FieldField;
    
    private string iDNumberFieldField;
    
    private string majorProductFieldField;
    
    private string noticeNotarialCountFieldField;
    
    private string surnameFieldField;
    
    /// <remarks/>
    
    public string civilCourtCountField {
        get {
            return this.civilCourtCountFieldField;
        }
        set {
            this.civilCourtCountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateOfBirthField {
        get {
            return this.dateOfBirthFieldField;
        }
        set {
            this.dateOfBirthFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string debtCouncilDateField {
        get {
            return this.debtCouncilDateFieldField;
        }
        set {
            this.debtCouncilDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string debtCouncilDescField {
        get {
            return this.debtCouncilDescFieldField;
        }
        set {
            this.debtCouncilDescFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string defaultCountField {
        get {
            return this.defaultCountFieldField;
        }
        set {
            this.defaultCountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string disputeDateField {
        get {
            return this.disputeDateFieldField;
        }
        set {
            this.disputeDateFieldField = value;
        }
    }
    
    /// <remarks/>
    public Established establishedField {
        get {
            return this.establishedFieldField;
        }
        set {
            this.establishedFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string forename1Field {
        get {
            return this.forename1FieldField;
        }
        set {
            this.forename1FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string forename2Field {
        get {
            return this.forename2FieldField;
        }
        set {
            this.forename2FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string forename3Field {
        get {
            return this.forename3FieldField;
        }
        set {
            this.forename3FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string iDNumberField {
        get {
            return this.iDNumberFieldField;
        }
        set {
            this.iDNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string noticeNotarialCountField {
        get {
            return this.noticeNotarialCountFieldField;
        }
        set {
            this.noticeNotarialCountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string surnameField {
        get {
            return this.surnameFieldField;
        }
        set {
            this.surnameFieldField = value;
        }
    }
}

/// <remarks/>





public partial class PrincipalArchiveP5 : BureauSegment {
    
    private string dateOfBirthFieldField;
    
    private string dateResignedFieldField;
    
    private string dateStartedFieldField;
    
    private string iDNumberFieldField;
    
    private string initialsFieldField;
    
    private string majorProductFieldField;
    
    private string positionFieldField;
    
    private string registrationNumberFieldField;
    
    private string statusFieldField;
    
    private string surnameFieldField;
    
    /// <remarks/>
    
    public string dateOfBirthField {
        get {
            return this.dateOfBirthFieldField;
        }
        set {
            this.dateOfBirthFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateResignedField {
        get {
            return this.dateResignedFieldField;
        }
        set {
            this.dateResignedFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateStartedField {
        get {
            return this.dateStartedFieldField;
        }
        set {
            this.dateStartedFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string iDNumberField {
        get {
            return this.iDNumberFieldField;
        }
        set {
            this.iDNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string initialsField {
        get {
            return this.initialsFieldField;
        }
        set {
            this.initialsFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string positionField {
        get {
            return this.positionFieldField;
        }
        set {
            this.positionFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string registrationNumberField {
        get {
            return this.registrationNumberFieldField;
        }
        set {
            this.registrationNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string statusField {
        get {
            return this.statusFieldField;
        }
        set {
            this.statusFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string surnameField {
        get {
            return this.surnameFieldField;
        }
        set {
            this.surnameFieldField = value;
        }
    }
}

/// <remarks/>





public partial class PrincipalArchive : BureauSegment {
    
    private string businessNameFieldField;
    
    private string dateOfBirthFieldField;
    
    private string dateResignedFieldField;
    
    private string dateStartedFieldField;
    
    private string iDNumberFieldField;
    
    private string initialsFieldField;
    
    private string positionFieldField;
    
    private string registrationNumberFieldField;
    
    private string surnameFieldField;
    
    /// <remarks/>
    
    public string businessNameField {
        get {
            return this.businessNameFieldField;
        }
        set {
            this.businessNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateOfBirthField {
        get {
            return this.dateOfBirthFieldField;
        }
        set {
            this.dateOfBirthFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateResignedField {
        get {
            return this.dateResignedFieldField;
        }
        set {
            this.dateResignedFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateStartedField {
        get {
            return this.dateStartedFieldField;
        }
        set {
            this.dateStartedFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string iDNumberField {
        get {
            return this.iDNumberFieldField;
        }
        set {
            this.iDNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string initialsField {
        get {
            return this.initialsFieldField;
        }
        set {
            this.initialsFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string positionField {
        get {
            return this.positionFieldField;
        }
        set {
            this.positionFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string registrationNumberField {
        get {
            return this.registrationNumberFieldField;
        }
        set {
            this.registrationNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string surnameField {
        get {
            return this.surnameFieldField;
        }
        set {
            this.surnameFieldField = value;
        }
    }
}

/// <remarks/>





public partial class OtherOperation : BureauSegment {
    
    private string[] agencyCountryFieldField;
    
    private string[] agencyNameFieldField;
    
    private string[] agencyProductFieldField;
    
    private string[] agentLocationFieldField;
    
    private string[] agentNumberFieldField;
    
    private string[] branchLocationFieldField;
    
    private string[] branchNumberFieldField;
    
    private string majorProductFieldField;
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] agencyCountryField {
        get {
            return this.agencyCountryFieldField;
        }
        set {
            this.agencyCountryFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] agencyNameField {
        get {
            return this.agencyNameFieldField;
        }
        set {
            this.agencyNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] agencyProductField {
        get {
            return this.agencyProductFieldField;
        }
        set {
            this.agencyProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] agentLocationField {
        get {
            return this.agentLocationFieldField;
        }
        set {
            this.agentLocationFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] agentNumberField {
        get {
            return this.agentNumberFieldField;
        }
        set {
            this.agentNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] branchLocationField {
        get {
            return this.branchLocationFieldField;
        }
        set {
            this.branchLocationFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] branchNumberField {
        get {
            return this.branchNumberFieldField;
        }
        set {
            this.branchNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
}

/// <remarks/>





public partial class AdditionalOperations : BureauSegment {
    
    private string majorProductFieldField;
    
    private SicDetails[] sicDetailsFieldField;
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public SicDetails[] sicDetailsField {
        get {
            return this.sicDetailsFieldField;
        }
        set {
            this.sicDetailsFieldField = value;
        }
    }
}

/// <remarks/>





public partial class Operation : BureauSegment {
    
    private string aveCollPeriodFieldField;
    
    private string cashSalesFieldField;
    
    private string cashSalesPercFieldField;
    
    private string creditSalesFieldField;
    
    private string creditSalesPercFieldField;
    
    private string dateFieldField;
    
    private string divisionalNameFieldField;
    
    private string exportCommentFieldField;
    
    private string exportFieldField;
    
    private string importCommentFieldField;
    
    private string importFieldField;
    
    private string leasePeriodFromFieldField;
    
    private string leasePeriodToFieldField;
    
    private string majorProductFieldField;
    
    private string numberSalesStaffFieldField;
    
    private string numberWageStaffFieldField;
    
    private string[] opCommentFieldField;
    
    private string premisesLessorFieldField;
    
    private string premisesOwnerFieldField;
    
    private string premisesRentalFieldField;
    
    private string premisesSizeFieldField;
    
    private string sASicCodeFieldField;
    
    private string sASicDescriptionFieldField;
    
    private string salesAreaFieldField;
    
    private string sicCodeFieldField;
    
    private string sicDescriptionFieldField;
    
    private string termsFromFieldField;
    
    private string termsToFieldField;
    
    private string totalNumberStaffFieldField;
    
    /// <remarks/>
    
    public string aveCollPeriodField {
        get {
            return this.aveCollPeriodFieldField;
        }
        set {
            this.aveCollPeriodFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string cashSalesField {
        get {
            return this.cashSalesFieldField;
        }
        set {
            this.cashSalesFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string cashSalesPercField {
        get {
            return this.cashSalesPercFieldField;
        }
        set {
            this.cashSalesPercFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string creditSalesField {
        get {
            return this.creditSalesFieldField;
        }
        set {
            this.creditSalesFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string creditSalesPercField {
        get {
            return this.creditSalesPercFieldField;
        }
        set {
            this.creditSalesPercFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateField {
        get {
            return this.dateFieldField;
        }
        set {
            this.dateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string divisionalNameField {
        get {
            return this.divisionalNameFieldField;
        }
        set {
            this.divisionalNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string exportCommentField {
        get {
            return this.exportCommentFieldField;
        }
        set {
            this.exportCommentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string exportField {
        get {
            return this.exportFieldField;
        }
        set {
            this.exportFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string importCommentField {
        get {
            return this.importCommentFieldField;
        }
        set {
            this.importCommentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string importField {
        get {
            return this.importFieldField;
        }
        set {
            this.importFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string leasePeriodFromField {
        get {
            return this.leasePeriodFromFieldField;
        }
        set {
            this.leasePeriodFromFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string leasePeriodToField {
        get {
            return this.leasePeriodToFieldField;
        }
        set {
            this.leasePeriodToFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string numberSalesStaffField {
        get {
            return this.numberSalesStaffFieldField;
        }
        set {
            this.numberSalesStaffFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string numberWageStaffField {
        get {
            return this.numberWageStaffFieldField;
        }
        set {
            this.numberWageStaffFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] opCommentField {
        get {
            return this.opCommentFieldField;
        }
        set {
            this.opCommentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string premisesLessorField {
        get {
            return this.premisesLessorFieldField;
        }
        set {
            this.premisesLessorFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string premisesOwnerField {
        get {
            return this.premisesOwnerFieldField;
        }
        set {
            this.premisesOwnerFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string premisesRentalField {
        get {
            return this.premisesRentalFieldField;
        }
        set {
            this.premisesRentalFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string premisesSizeField {
        get {
            return this.premisesSizeFieldField;
        }
        set {
            this.premisesSizeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string sASicCodeField {
        get {
            return this.sASicCodeFieldField;
        }
        set {
            this.sASicCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string sASicDescriptionField {
        get {
            return this.sASicDescriptionFieldField;
        }
        set {
            this.sASicDescriptionFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string salesAreaField {
        get {
            return this.salesAreaFieldField;
        }
        set {
            this.salesAreaFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string sicCodeField {
        get {
            return this.sicCodeFieldField;
        }
        set {
            this.sicCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string sicDescriptionField {
        get {
            return this.sicDescriptionFieldField;
        }
        set {
            this.sicDescriptionFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string termsFromField {
        get {
            return this.termsFromFieldField;
        }
        set {
            this.termsFromFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string termsToField {
        get {
            return this.termsToFieldField;
        }
        set {
            this.termsToFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string totalNumberStaffField {
        get {
            return this.totalNumberStaffFieldField;
        }
        set {
            this.totalNumberStaffFieldField = value;
        }
    }
}

/// <remarks/>





public partial class Observation : BureauSegment {
    
    private string commentDateFieldField;
    
    private string[] commentFieldField;
    
    private string majorProductFieldField;
    
    /// <remarks/>
    
    public string commentDateField {
        get {
            return this.commentDateFieldField;
        }
        set {
            this.commentDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] commentField {
        get {
            return this.commentFieldField;
        }
        set {
            this.commentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
}

/// <remarks/>





public partial class ObservationCont : BureauSegment {
    
    private string[] commentFieldField;
    
    private string majorProductFieldField;
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] commentField {
        get {
            return this.commentFieldField;
        }
        set {
            this.commentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
}

/// <remarks/>





public partial class NotarialBond : BureauSegment {
    
    private string addressFieldField;
    
    private string bondAmountFieldField;
    
    private string bondDateFieldField;
    
    private string bondDistrictFieldField;
    
    private string bondNumberFieldField;
    
    private string bondPercentFieldField;
    
    private string cityFieldField;
    
    private string countryFieldField;
    
    private string dateCancelledFieldField;
    
    private string majorProductFieldField;
    
    private string messageFieldField;
    
    private string mortgageeFieldField;
    
    private string mortgagorDistrictFieldField;
    
    private string mortgagorFieldField;
    
    private string numberFoundFieldField;
    
    private string postCodeFieldField;
    
    private string serialNumberFieldField;
    
    private string suburbFieldField;
    
    private string tradeStyleFieldField;
    
    /// <remarks/>
    
    public string addressField {
        get {
            return this.addressFieldField;
        }
        set {
            this.addressFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondAmountField {
        get {
            return this.bondAmountFieldField;
        }
        set {
            this.bondAmountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondDateField {
        get {
            return this.bondDateFieldField;
        }
        set {
            this.bondDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondDistrictField {
        get {
            return this.bondDistrictFieldField;
        }
        set {
            this.bondDistrictFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondNumberField {
        get {
            return this.bondNumberFieldField;
        }
        set {
            this.bondNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondPercentField {
        get {
            return this.bondPercentFieldField;
        }
        set {
            this.bondPercentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string cityField {
        get {
            return this.cityFieldField;
        }
        set {
            this.cityFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string countryField {
        get {
            return this.countryFieldField;
        }
        set {
            this.countryFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateCancelledField {
        get {
            return this.dateCancelledFieldField;
        }
        set {
            this.dateCancelledFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string messageField {
        get {
            return this.messageFieldField;
        }
        set {
            this.messageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string mortgageeField {
        get {
            return this.mortgageeFieldField;
        }
        set {
            this.mortgageeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string mortgagorDistrictField {
        get {
            return this.mortgagorDistrictFieldField;
        }
        set {
            this.mortgagorDistrictFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string mortgagorField {
        get {
            return this.mortgagorFieldField;
        }
        set {
            this.mortgagorFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string numberFoundField {
        get {
            return this.numberFoundFieldField;
        }
        set {
            this.numberFoundFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string postCodeField {
        get {
            return this.postCodeFieldField;
        }
        set {
            this.postCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string serialNumberField {
        get {
            return this.serialNumberFieldField;
        }
        set {
            this.serialNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string suburbField {
        get {
            return this.suburbFieldField;
        }
        set {
            this.suburbFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string tradeStyleField {
        get {
            return this.tradeStyleFieldField;
        }
        set {
            this.tradeStyleFieldField = value;
        }
    }
}

/// <remarks/>





public partial class Names : BureauSegment {
    
    private string aKANameFieldField;
    
    private string businessNameFieldField;
    
    private string[] divisionalNamesFieldField;
    
    private string majorProductFieldField;
    
    private string[] previousNameDatesFieldField;
    
    private string[] previousNamesFieldField;
    
    private string[] tradeStylesFieldField;
    
    /// <remarks/>
    
    public string aKANameField {
        get {
            return this.aKANameFieldField;
        }
        set {
            this.aKANameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string businessNameField {
        get {
            return this.businessNameFieldField;
        }
        set {
            this.businessNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] divisionalNamesField {
        get {
            return this.divisionalNamesFieldField;
        }
        set {
            this.divisionalNamesFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] previousNameDatesField {
        get {
            return this.previousNameDatesFieldField;
        }
        set {
            this.previousNameDatesFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] previousNamesField {
        get {
            return this.previousNamesFieldField;
        }
        set {
            this.previousNamesFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] tradeStylesField {
        get {
            return this.tradeStylesFieldField;
        }
        set {
            this.tradeStylesFieldField = value;
        }
    }
}

/// <remarks/>





public partial class ModuleAvailabilityResponse : BureauSegment {
    
    private string iTNumberFieldField;
    
    private Module[] modulesFieldField;
    
    /// <remarks/>
    
    public string iTNumberField {
        get {
            return this.iTNumberFieldField;
        }
        set {
            this.iTNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public Module[] modulesField {
        get {
            return this.modulesFieldField;
        }
        set {
            this.modulesFieldField = value;
        }
    }
}

/// <remarks/>





public partial class MailboxRetrieveList : BureauSegment {
    
    private string[] ticketsFieldField;
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] ticketsField {
        get {
            return this.ticketsFieldField;
        }
        set {
            this.ticketsFieldField = value;
        }
    }
}

/// <remarks/>





public partial class LinkedBusinessHeader : BureauSegment {
    
    private string businessNameFieldField;
    
    private string consumerNoFieldField;
    
    private string defaultCounterFieldField;
    
    private string iTNumberFieldField;
    
    private string judgementCounterFieldField;
    
    private string majorProductFieldField;
    
    private string registeredDateFieldField;
    
    private string registeredNumberFieldField;
    
    private string registeredStatusFieldField;
    
    /// <remarks/>
    
    public string businessNameField {
        get {
            return this.businessNameFieldField;
        }
        set {
            this.businessNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string consumerNoField {
        get {
            return this.consumerNoFieldField;
        }
        set {
            this.consumerNoFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string defaultCounterField {
        get {
            return this.defaultCounterFieldField;
        }
        set {
            this.defaultCounterFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string iTNumberField {
        get {
            return this.iTNumberFieldField;
        }
        set {
            this.iTNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string judgementCounterField {
        get {
            return this.judgementCounterFieldField;
        }
        set {
            this.judgementCounterFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string registeredDateField {
        get {
            return this.registeredDateFieldField;
        }
        set {
            this.registeredDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string registeredNumberField {
        get {
            return this.registeredNumberFieldField;
        }
        set {
            this.registeredNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string registeredStatusField {
        get {
            return this.registeredStatusFieldField;
        }
        set {
            this.registeredStatusFieldField = value;
        }
    }
}

/// <remarks/>





public partial class Header : BureauSegment {
    
    private string businessCategoryFieldField;
    
    private string businessFunctionFieldField;
    
    private string businessTypeFieldField;
    
    private string cityFieldField;
    
    private string countryFieldField;
    
    private string dateOfHeaderFieldField;
    
    private string dunsNumberFieldField;
    
    private string emailFieldField;
    
    private string faxFieldField;
    
    private string industryFieldField;
    
    private string majorProductFieldField;
    
    private string phoneFieldField;
    
    private string physicalAddressFieldField;
    
    private string postCodeFieldField;
    
    private string postalAddressFieldField;
    
    private string postalCityFieldField;
    
    private string postalCountryFieldField;
    
    private string postalPostCodeFieldField;
    
    private string postalSuburbFieldField;
    
    private string purchasePriceFieldField;
    
    private string startDateFieldField;
    
    private string startingCapitalFieldField;
    
    private string suburbFieldField;
    
    private string taxNumberFieldField;
    
    private string tradingNumberFieldField;
    
    private string[] vATNumbersFieldField;
    
    private string websiteFieldField;
    
    /// <remarks/>
    
    public string businessCategoryField {
        get {
            return this.businessCategoryFieldField;
        }
        set {
            this.businessCategoryFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string businessFunctionField {
        get {
            return this.businessFunctionFieldField;
        }
        set {
            this.businessFunctionFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string businessTypeField {
        get {
            return this.businessTypeFieldField;
        }
        set {
            this.businessTypeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string cityField {
        get {
            return this.cityFieldField;
        }
        set {
            this.cityFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string countryField {
        get {
            return this.countryFieldField;
        }
        set {
            this.countryFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateOfHeaderField {
        get {
            return this.dateOfHeaderFieldField;
        }
        set {
            this.dateOfHeaderFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dunsNumberField {
        get {
            return this.dunsNumberFieldField;
        }
        set {
            this.dunsNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string emailField {
        get {
            return this.emailFieldField;
        }
        set {
            this.emailFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string faxField {
        get {
            return this.faxFieldField;
        }
        set {
            this.faxFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string industryField {
        get {
            return this.industryFieldField;
        }
        set {
            this.industryFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string phoneField {
        get {
            return this.phoneFieldField;
        }
        set {
            this.phoneFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string physicalAddressField {
        get {
            return this.physicalAddressFieldField;
        }
        set {
            this.physicalAddressFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string postCodeField {
        get {
            return this.postCodeFieldField;
        }
        set {
            this.postCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string postalAddressField {
        get {
            return this.postalAddressFieldField;
        }
        set {
            this.postalAddressFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string postalCityField {
        get {
            return this.postalCityFieldField;
        }
        set {
            this.postalCityFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string postalCountryField {
        get {
            return this.postalCountryFieldField;
        }
        set {
            this.postalCountryFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string postalPostCodeField {
        get {
            return this.postalPostCodeFieldField;
        }
        set {
            this.postalPostCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string postalSuburbField {
        get {
            return this.postalSuburbFieldField;
        }
        set {
            this.postalSuburbFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string purchasePriceField {
        get {
            return this.purchasePriceFieldField;
        }
        set {
            this.purchasePriceFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string startDateField {
        get {
            return this.startDateFieldField;
        }
        set {
            this.startDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string startingCapitalField {
        get {
            return this.startingCapitalFieldField;
        }
        set {
            this.startingCapitalFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string suburbField {
        get {
            return this.suburbFieldField;
        }
        set {
            this.suburbFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string taxNumberField {
        get {
            return this.taxNumberFieldField;
        }
        set {
            this.taxNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string tradingNumberField {
        get {
            return this.tradingNumberFieldField;
        }
        set {
            this.tradingNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] vATNumbersField {
        get {
            return this.vATNumbersFieldField;
        }
        set {
            this.vATNumbersFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string websiteField {
        get {
            return this.websiteFieldField;
        }
        set {
            this.websiteFieldField = value;
        }
    }
}

/// <remarks/>





public partial class GeneralBankingInfo : BureauSegment {
    
    private string accountNoFieldField;
    
    private string bankNameFieldField;
    
    private string branchFieldField;
    
    private string[] commentFieldField;
    
    private string infoDateFieldField;
    
    private string majorProductFieldField;
    
    private string startDateFieldField;
    
    /// <remarks/>
    
    public string accountNoField {
        get {
            return this.accountNoFieldField;
        }
        set {
            this.accountNoFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bankNameField {
        get {
            return this.bankNameFieldField;
        }
        set {
            this.bankNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string branchField {
        get {
            return this.branchFieldField;
        }
        set {
            this.branchFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] commentField {
        get {
            return this.commentFieldField;
        }
        set {
            this.commentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string infoDateField {
        get {
            return this.infoDateFieldField;
        }
        set {
            this.infoDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string startDateField {
        get {
            return this.startDateFieldField;
        }
        set {
            this.startDateFieldField = value;
        }
    }
}

/// <remarks/>





public partial class FirstResponse : BureauSegment {
    
    private string businessNameFieldField;
    
    private string dunsNumberFieldField;
    
    private string iTNumberFieldField;
    
    private string registrationNumberFieldField;
    
    private string ticketNumberFieldField;
    
    /// <remarks/>
    
    public string businessNameField {
        get {
            return this.businessNameFieldField;
        }
        set {
            this.businessNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dunsNumberField {
        get {
            return this.dunsNumberFieldField;
        }
        set {
            this.dunsNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string iTNumberField {
        get {
            return this.iTNumberFieldField;
        }
        set {
            this.iTNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string registrationNumberField {
        get {
            return this.registrationNumberFieldField;
        }
        set {
            this.registrationNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string ticketNumberField {
        get {
            return this.ticketNumberFieldField;
        }
        set {
            this.ticketNumberFieldField = value;
        }
    }
}

/// <remarks/>





public partial class FinancialRatios : BureauSegment {
    
    private string[] acidTestRatioFieldField;
    
    private string[] balanceSheetDateFieldField;
    
    private string[] balanceSheetTypeFieldField;
    
    private string commentFieldField;
    
    private string[] currencyFieldField;
    
    private string[] currentAssetsFieldField;
    
    private string[] currentLiabilitiesFieldField;
    
    private string[] currentRatioFieldField;
    
    private string[] financialYearEndDateFieldField;
    
    private string[] gearingRatioFieldField;
    
    private string infoDateFieldField;
    
    private string[] interestCoverRatioFieldField;
    
    private string[] interestPaidFieldField;
    
    private string[] inventoryFieldField;
    
    private string[] longTermLiabilitiesFieldField;
    
    private string majorProductFieldField;
    
    private string[] ownersEquityFieldField;
    
    private string[] profitBeforeInterestTaxFieldField;
    
    private string[] profitabilityRatioFieldField;
    
    private string[] returnOnEquityRatioFieldField;
    
    private string[] turnoverFieldField;
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] acidTestRatioField {
        get {
            return this.acidTestRatioFieldField;
        }
        set {
            this.acidTestRatioFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] balanceSheetDateField {
        get {
            return this.balanceSheetDateFieldField;
        }
        set {
            this.balanceSheetDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] balanceSheetTypeField {
        get {
            return this.balanceSheetTypeFieldField;
        }
        set {
            this.balanceSheetTypeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string commentField {
        get {
            return this.commentFieldField;
        }
        set {
            this.commentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] currencyField {
        get {
            return this.currencyFieldField;
        }
        set {
            this.currencyFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] currentAssetsField {
        get {
            return this.currentAssetsFieldField;
        }
        set {
            this.currentAssetsFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] currentLiabilitiesField {
        get {
            return this.currentLiabilitiesFieldField;
        }
        set {
            this.currentLiabilitiesFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] currentRatioField {
        get {
            return this.currentRatioFieldField;
        }
        set {
            this.currentRatioFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] financialYearEndDateField {
        get {
            return this.financialYearEndDateFieldField;
        }
        set {
            this.financialYearEndDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] gearingRatioField {
        get {
            return this.gearingRatioFieldField;
        }
        set {
            this.gearingRatioFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string infoDateField {
        get {
            return this.infoDateFieldField;
        }
        set {
            this.infoDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] interestCoverRatioField {
        get {
            return this.interestCoverRatioFieldField;
        }
        set {
            this.interestCoverRatioFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] interestPaidField {
        get {
            return this.interestPaidFieldField;
        }
        set {
            this.interestPaidFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] inventoryField {
        get {
            return this.inventoryFieldField;
        }
        set {
            this.inventoryFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] longTermLiabilitiesField {
        get {
            return this.longTermLiabilitiesFieldField;
        }
        set {
            this.longTermLiabilitiesFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] ownersEquityField {
        get {
            return this.ownersEquityFieldField;
        }
        set {
            this.ownersEquityFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] profitBeforeInterestTaxField {
        get {
            return this.profitBeforeInterestTaxFieldField;
        }
        set {
            this.profitBeforeInterestTaxFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] profitabilityRatioField {
        get {
            return this.profitabilityRatioFieldField;
        }
        set {
            this.profitabilityRatioFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] returnOnEquityRatioField {
        get {
            return this.returnOnEquityRatioFieldField;
        }
        set {
            this.returnOnEquityRatioFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] turnoverField {
        get {
            return this.turnoverFieldField;
        }
        set {
            this.turnoverFieldField = value;
        }
    }
}

/// <remarks/>





public partial class FinanceHeader : BureauSegment {
    
    private string balanceSheetDateFieldField;
    
    private string balanceSheetTypeFieldField;
    
    private string balanceSheetUnitFieldField;
    
    private string commentFieldField;
    
    private string dRCededIndFieldField;
    
    private string dRCededNameFieldField;
    
    private string dRFactoredFieldField;
    
    private string dRFactoredNameFieldField;
    
    private string dateFieldField;
    
    private string finYearMonthFieldField;
    
    private string majorProductFieldField;
    
    private string sourceFieldField;
    
    private string sourceTitleFieldField;
    
    private string turnOverAmount1FieldField;
    
    private string turnOverAmount2FieldField;
    
    private string turnOverDateYear1FieldField;
    
    private string turnOverDateYear2FieldField;
    
    private string turnOverYear1TypeFieldField;
    
    private string turnOverYear2TypeFieldField;
    
    /// <remarks/>
    
    public string balanceSheetDateField {
        get {
            return this.balanceSheetDateFieldField;
        }
        set {
            this.balanceSheetDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string balanceSheetTypeField {
        get {
            return this.balanceSheetTypeFieldField;
        }
        set {
            this.balanceSheetTypeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string balanceSheetUnitField {
        get {
            return this.balanceSheetUnitFieldField;
        }
        set {
            this.balanceSheetUnitFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string commentField {
        get {
            return this.commentFieldField;
        }
        set {
            this.commentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dRCededIndField {
        get {
            return this.dRCededIndFieldField;
        }
        set {
            this.dRCededIndFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dRCededNameField {
        get {
            return this.dRCededNameFieldField;
        }
        set {
            this.dRCededNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dRFactoredField {
        get {
            return this.dRFactoredFieldField;
        }
        set {
            this.dRFactoredFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dRFactoredNameField {
        get {
            return this.dRFactoredNameFieldField;
        }
        set {
            this.dRFactoredNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateField {
        get {
            return this.dateFieldField;
        }
        set {
            this.dateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string finYearMonthField {
        get {
            return this.finYearMonthFieldField;
        }
        set {
            this.finYearMonthFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string sourceField {
        get {
            return this.sourceFieldField;
        }
        set {
            this.sourceFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string sourceTitleField {
        get {
            return this.sourceTitleFieldField;
        }
        set {
            this.sourceTitleFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string turnOverAmount1Field {
        get {
            return this.turnOverAmount1FieldField;
        }
        set {
            this.turnOverAmount1FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string turnOverAmount2Field {
        get {
            return this.turnOverAmount2FieldField;
        }
        set {
            this.turnOverAmount2FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string turnOverDateYear1Field {
        get {
            return this.turnOverDateYear1FieldField;
        }
        set {
            this.turnOverDateYear1FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string turnOverDateYear2Field {
        get {
            return this.turnOverDateYear2FieldField;
        }
        set {
            this.turnOverDateYear2FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string turnOverYear1TypeField {
        get {
            return this.turnOverYear1TypeFieldField;
        }
        set {
            this.turnOverYear1TypeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string turnOverYear2TypeField {
        get {
            return this.turnOverYear2TypeFieldField;
        }
        set {
            this.turnOverYear2TypeFieldField = value;
        }
    }
}

/// <remarks/>





public partial class FinanceData : BureauSegment {
    
    private string balanceSheetDateBeforeFieldField;
    
    private string balanceSheetDateCurrentFieldField;
    
    private string balanceSheetTypeBeforeFieldField;
    
    private string balanceSheetTypeCurrentFieldField;
    
    private string commentFieldField;
    
    private string currencyBeforeFieldField;
    
    private string currencyCurrentFieldField;
    
    private string dateFieldField;
    
    private string finYearMonthBeforeFieldField;
    
    private string finYearMonthCurrentFieldField;
    
    private string financialFactorFieldField;
    
    private string infoSourceFieldField;
    
    private string majorProductFieldField;
    
    private string netProfitApproximationBeforeFieldField;
    
    private string netProfitApproximationCurrentFieldField;
    
    private string turnoverApproximationBeforeFieldField;
    
    private string turnoverApproximationCurrentFieldField;
    
    private string[] turnoverBandBeforeFieldField;
    
    private string[] turnoverBandCurrentFieldField;
    
    private string yearBeforeFieldField;
    
    private string yearCurrentFieldField;
    
    /// <remarks/>
    
    public string balanceSheetDateBeforeField {
        get {
            return this.balanceSheetDateBeforeFieldField;
        }
        set {
            this.balanceSheetDateBeforeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string balanceSheetDateCurrentField {
        get {
            return this.balanceSheetDateCurrentFieldField;
        }
        set {
            this.balanceSheetDateCurrentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string balanceSheetTypeBeforeField {
        get {
            return this.balanceSheetTypeBeforeFieldField;
        }
        set {
            this.balanceSheetTypeBeforeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string balanceSheetTypeCurrentField {
        get {
            return this.balanceSheetTypeCurrentFieldField;
        }
        set {
            this.balanceSheetTypeCurrentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string commentField {
        get {
            return this.commentFieldField;
        }
        set {
            this.commentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string currencyBeforeField {
        get {
            return this.currencyBeforeFieldField;
        }
        set {
            this.currencyBeforeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string currencyCurrentField {
        get {
            return this.currencyCurrentFieldField;
        }
        set {
            this.currencyCurrentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateField {
        get {
            return this.dateFieldField;
        }
        set {
            this.dateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string finYearMonthBeforeField {
        get {
            return this.finYearMonthBeforeFieldField;
        }
        set {
            this.finYearMonthBeforeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string finYearMonthCurrentField {
        get {
            return this.finYearMonthCurrentFieldField;
        }
        set {
            this.finYearMonthCurrentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string financialFactorField {
        get {
            return this.financialFactorFieldField;
        }
        set {
            this.financialFactorFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string infoSourceField {
        get {
            return this.infoSourceFieldField;
        }
        set {
            this.infoSourceFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string netProfitApproximationBeforeField {
        get {
            return this.netProfitApproximationBeforeFieldField;
        }
        set {
            this.netProfitApproximationBeforeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string netProfitApproximationCurrentField {
        get {
            return this.netProfitApproximationCurrentFieldField;
        }
        set {
            this.netProfitApproximationCurrentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string turnoverApproximationBeforeField {
        get {
            return this.turnoverApproximationBeforeFieldField;
        }
        set {
            this.turnoverApproximationBeforeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string turnoverApproximationCurrentField {
        get {
            return this.turnoverApproximationCurrentFieldField;
        }
        set {
            this.turnoverApproximationCurrentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] turnoverBandBeforeField {
        get {
            return this.turnoverBandBeforeFieldField;
        }
        set {
            this.turnoverBandBeforeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] turnoverBandCurrentField {
        get {
            return this.turnoverBandCurrentFieldField;
        }
        set {
            this.turnoverBandCurrentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string yearBeforeField {
        get {
            return this.yearBeforeFieldField;
        }
        set {
            this.yearBeforeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string yearCurrentField {
        get {
            return this.yearCurrentFieldField;
        }
        set {
            this.yearCurrentFieldField = value;
        }
    }
}

/// <remarks/>





public partial class FinanceDataFF : BureauSegment {
    
    private string dateFieldField;
    
    private string debtorDescriptionFieldField;
    
    private string debtorNameFieldField;
    
    private DebtorInfo[] debtorsFieldField;
    
    private string majorProductFieldField;
    
    /// <remarks/>
    
    public string dateField {
        get {
            return this.dateFieldField;
        }
        set {
            this.dateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string debtorDescriptionField {
        get {
            return this.debtorDescriptionFieldField;
        }
        set {
            this.debtorDescriptionFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string debtorNameField {
        get {
            return this.debtorNameFieldField;
        }
        set {
            this.debtorNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public DebtorInfo[] debtorsField {
        get {
            return this.debtorsFieldField;
        }
        set {
            this.debtorsFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
}

/// <remarks/>





public partial class FinanceDataFE : BureauSegment {
    
    private FinanceAmounts[] amountsFieldField;
    
    private string dateFieldField;
    
    private string majorProductFieldField;
    
    /// <remarks/>
    
    public FinanceAmounts[] amountsField {
        get {
            return this.amountsFieldField;
        }
        set {
            this.amountsFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateField {
        get {
            return this.dateFieldField;
        }
        set {
            this.dateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
}

/// <remarks/>





public partial class EnquirySummary : BureauSegment {
    
    private string last12MonthTotalFieldField;
    
    private string last24MonthTotalFieldField;
    
    private string last3MonthTotalFieldField;
    
    private string last6MonthTotalFieldField;
    
    private string majorProductFieldField;
    
    /// <remarks/>
    
    public string last12MonthTotalField {
        get {
            return this.last12MonthTotalFieldField;
        }
        set {
            this.last12MonthTotalFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string last24MonthTotalField {
        get {
            return this.last24MonthTotalFieldField;
        }
        set {
            this.last24MonthTotalFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string last3MonthTotalField {
        get {
            return this.last3MonthTotalFieldField;
        }
        set {
            this.last3MonthTotalFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string last6MonthTotalField {
        get {
            return this.last6MonthTotalFieldField;
        }
        set {
            this.last6MonthTotalFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
}

/// <remarks/>





public partial class EnquiryHistory : BureauSegment {
    
    private string amountFieldField;
    
    private string contactNameFieldField;
    
    private string contactPhoneFieldField;
    
    private string dateOfEnquiryFieldField;
    
    private string majorProductFieldField;
    
    private string reasonFieldField;
    
    private string subscriberFieldField;
    
    private string subscriberNumberFieldField;
    
    /// <remarks/>
    
    public string amountField {
        get {
            return this.amountFieldField;
        }
        set {
            this.amountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string contactNameField {
        get {
            return this.contactNameFieldField;
        }
        set {
            this.contactNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string contactPhoneField {
        get {
            return this.contactPhoneFieldField;
        }
        set {
            this.contactPhoneFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateOfEnquiryField {
        get {
            return this.dateOfEnquiryFieldField;
        }
        set {
            this.dateOfEnquiryFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string reasonField {
        get {
            return this.reasonFieldField;
        }
        set {
            this.reasonFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string subscriberField {
        get {
            return this.subscriberFieldField;
        }
        set {
            this.subscriberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string subscriberNumberField {
        get {
            return this.subscriberNumberFieldField;
        }
        set {
            this.subscriberNumberFieldField = value;
        }
    }
}

/// <remarks/>
[System.Xml.Serialization.XmlIncludeAttribute(typeof(EmpiricaEM04))]
[System.Xml.Serialization.XmlIncludeAttribute(typeof(EmpiricaE1))]





public partial class Empirica : BureauSegment {
}

/// <remarks/>





public partial class EmpiricaEM04 : Empirica {
    
    private string consumerNoFieldField;
    
    private string empiricaScoreFieldField;
    
    private string exclusionCodeDescriptionFieldField;
    
    private string exclusionCodeFieldField;
    
    private string expansionScoreDescriptionFieldField;
    
    private string expansionScoreFieldField;
    
    private string[] reasonCodeFieldField;
    
    private string[] reasonDescriptionFieldField;
    
    /// <remarks/>
    
    public string consumerNoField {
        get {
            return this.consumerNoFieldField;
        }
        set {
            this.consumerNoFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string empiricaScoreField {
        get {
            return this.empiricaScoreFieldField;
        }
        set {
            this.empiricaScoreFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string exclusionCodeDescriptionField {
        get {
            return this.exclusionCodeDescriptionFieldField;
        }
        set {
            this.exclusionCodeDescriptionFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string exclusionCodeField {
        get {
            return this.exclusionCodeFieldField;
        }
        set {
            this.exclusionCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string expansionScoreDescriptionField {
        get {
            return this.expansionScoreDescriptionFieldField;
        }
        set {
            this.expansionScoreDescriptionFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string expansionScoreField {
        get {
            return this.expansionScoreFieldField;
        }
        set {
            this.expansionScoreFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] reasonCodeField {
        get {
            return this.reasonCodeFieldField;
        }
        set {
            this.reasonCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] reasonDescriptionField {
        get {
            return this.reasonDescriptionFieldField;
        }
        set {
            this.reasonDescriptionFieldField = value;
        }
    }
}

/// <remarks/>





public partial class EmpiricaE1 : Empirica {
    
    private string consumerNoFieldField;
    
    private string dateOfBirthFieldField;
    
    private string empiricaScoreFieldField;
    
    private string errorMessageFieldField;
    
    private string exclusionCodeDescriptionFieldField;
    
    private string exclusionCodeFieldField;
    
    private string expansionCodeFieldField;
    
    private string forename1FieldField;
    
    private string forename2FieldField;
    
    private string forename3FieldField;
    
    private string identityNoFieldField;
    
    private string majorProductFieldField;
    
    private string[] reasonCodeFieldField;
    
    private string[] reasonDescriptionFieldField;
    
    private string surnameFieldField;
    
    /// <remarks/>
    
    public string consumerNoField {
        get {
            return this.consumerNoFieldField;
        }
        set {
            this.consumerNoFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateOfBirthField {
        get {
            return this.dateOfBirthFieldField;
        }
        set {
            this.dateOfBirthFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string empiricaScoreField {
        get {
            return this.empiricaScoreFieldField;
        }
        set {
            this.empiricaScoreFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string errorMessageField {
        get {
            return this.errorMessageFieldField;
        }
        set {
            this.errorMessageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string exclusionCodeDescriptionField {
        get {
            return this.exclusionCodeDescriptionFieldField;
        }
        set {
            this.exclusionCodeDescriptionFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string exclusionCodeField {
        get {
            return this.exclusionCodeFieldField;
        }
        set {
            this.exclusionCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string expansionCodeField {
        get {
            return this.expansionCodeFieldField;
        }
        set {
            this.expansionCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string forename1Field {
        get {
            return this.forename1FieldField;
        }
        set {
            this.forename1FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string forename2Field {
        get {
            return this.forename2FieldField;
        }
        set {
            this.forename2FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string forename3Field {
        get {
            return this.forename3FieldField;
        }
        set {
            this.forename3FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string identityNoField {
        get {
            return this.identityNoFieldField;
        }
        set {
            this.identityNoFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] reasonCodeField {
        get {
            return this.reasonCodeFieldField;
        }
        set {
            this.reasonCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] reasonDescriptionField {
        get {
            return this.reasonDescriptionFieldField;
        }
        set {
            this.reasonDescriptionFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string surnameField {
        get {
            return this.surnameFieldField;
        }
        set {
            this.surnameFieldField = value;
        }
    }
}

/// <remarks/>





public partial class EmpOfCapital : BureauSegment {
    
    private string[] empOfCapAmtFieldField;
    
    private string[] empOfCapItemFieldField;
    
    private string empOfCapTotalFieldField;
    
    private string majorProductFieldField;
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] empOfCapAmtField {
        get {
            return this.empOfCapAmtFieldField;
        }
        set {
            this.empOfCapAmtFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] empOfCapItemField {
        get {
            return this.empOfCapItemFieldField;
        }
        set {
            this.empOfCapItemFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string empOfCapTotalField {
        get {
            return this.empOfCapTotalFieldField;
        }
        set {
            this.empOfCapTotalFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
}

/// <remarks/>





public partial class DynamicRating : BureauSegment {
    
    private string[] commentFieldField;
    
    private string conditionFieldField;
    
    private string majorProductFieldField;
    
    private string ratingFieldField;
    
    private string trendFieldField;
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] commentField {
        get {
            return this.commentFieldField;
        }
        set {
            this.commentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string conditionField {
        get {
            return this.conditionFieldField;
        }
        set {
            this.conditionFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string ratingField {
        get {
            return this.ratingFieldField;
        }
        set {
            this.ratingFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string trendField {
        get {
            return this.trendFieldField;
        }
        set {
            this.trendFieldField = value;
        }
    }
}

/// <remarks/>





public partial class Default : BureauSegment {
    
    private string amountFieldField;
    
    private string cityFieldField;
    
    private string commentFieldField;
    
    private string countryFieldField;
    
    private string defaultAddressFieldField;
    
    private string defaultDateFieldField;
    
    private string defaultNameFieldField;
    
    private string defaultTradeStyleFieldField;
    
    private string majorProductFieldField;
    
    private string messageFieldField;
    
    private string numberFoundFieldField;
    
    private string onBehalfOfFieldField;
    
    private string postCodeFieldField;
    
    private string serialNoFieldField;
    
    private string statusFieldField;
    
    private string subscriberNameFieldField;
    
    private string suburbFieldField;
    
    private string supplierNameFieldField;
    
    /// <remarks/>
    
    public string amountField {
        get {
            return this.amountFieldField;
        }
        set {
            this.amountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string cityField {
        get {
            return this.cityFieldField;
        }
        set {
            this.cityFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string commentField {
        get {
            return this.commentFieldField;
        }
        set {
            this.commentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string countryField {
        get {
            return this.countryFieldField;
        }
        set {
            this.countryFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string defaultAddressField {
        get {
            return this.defaultAddressFieldField;
        }
        set {
            this.defaultAddressFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string defaultDateField {
        get {
            return this.defaultDateFieldField;
        }
        set {
            this.defaultDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string defaultNameField {
        get {
            return this.defaultNameFieldField;
        }
        set {
            this.defaultNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string defaultTradeStyleField {
        get {
            return this.defaultTradeStyleFieldField;
        }
        set {
            this.defaultTradeStyleFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string messageField {
        get {
            return this.messageFieldField;
        }
        set {
            this.messageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string numberFoundField {
        get {
            return this.numberFoundFieldField;
        }
        set {
            this.numberFoundFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string onBehalfOfField {
        get {
            return this.onBehalfOfFieldField;
        }
        set {
            this.onBehalfOfFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string postCodeField {
        get {
            return this.postCodeFieldField;
        }
        set {
            this.postCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string serialNoField {
        get {
            return this.serialNoFieldField;
        }
        set {
            this.serialNoFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string statusField {
        get {
            return this.statusFieldField;
        }
        set {
            this.statusFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string subscriberNameField {
        get {
            return this.subscriberNameFieldField;
        }
        set {
            this.subscriberNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string suburbField {
        get {
            return this.suburbFieldField;
        }
        set {
            this.suburbFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string supplierNameField {
        get {
            return this.supplierNameFieldField;
        }
        set {
            this.supplierNameFieldField = value;
        }
    }
}

/// <remarks/>





public partial class DeedsMultipleBond : BureauSegment {
    
    private string actionDateFieldField;
    
    private string bondAmountFieldField;
    
    private string bondBuyerIDFieldField;
    
    private string bondBuyerNameFieldField;
    
    private string bondDateFieldField;
    
    private string bondHolderFieldField;
    
    private string bondNumberFieldField;
    
    private string commentFieldField;
    
    private string majorProductFieldField;
    
    private string rowIDFieldField;
    
    /// <remarks/>
    
    public string actionDateField {
        get {
            return this.actionDateFieldField;
        }
        set {
            this.actionDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondAmountField {
        get {
            return this.bondAmountFieldField;
        }
        set {
            this.bondAmountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondBuyerIDField {
        get {
            return this.bondBuyerIDFieldField;
        }
        set {
            this.bondBuyerIDFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondBuyerNameField {
        get {
            return this.bondBuyerNameFieldField;
        }
        set {
            this.bondBuyerNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondDateField {
        get {
            return this.bondDateFieldField;
        }
        set {
            this.bondDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondHolderField {
        get {
            return this.bondHolderFieldField;
        }
        set {
            this.bondHolderFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondNumberField {
        get {
            return this.bondNumberFieldField;
        }
        set {
            this.bondNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string commentField {
        get {
            return this.commentFieldField;
        }
        set {
            this.commentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string rowIDField {
        get {
            return this.rowIDFieldField;
        }
        set {
            this.rowIDFieldField = value;
        }
    }
}

/// <remarks/>





public partial class DeedHistory : BureauSegment {
    
    private string bondAmountFieldField;
    
    private string bondDateFieldField;
    
    private string bondNumberFieldField;
    
    private string commentFieldField;
    
    private string dateFieldField;
    
    private string dateOfBirthIDFieldField;
    
    private string deedsOfficeFieldField;
    
    private string eRFFieldField;
    
    private string farmFieldField;
    
    private string majorProductFieldField;
    
    private string mortgageeFieldField;
    
    private string portionFieldField;
    
    private string propertyNameFieldField;
    
    private string propertySizeFieldField;
    
    private string purchaseDateFieldField;
    
    private string purchasePriceFieldField;
    
    private string rowIDFieldField;
    
    private string titleFieldField;
    
    private string townshipFieldField;
    
    /// <remarks/>
    
    public string bondAmountField {
        get {
            return this.bondAmountFieldField;
        }
        set {
            this.bondAmountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondDateField {
        get {
            return this.bondDateFieldField;
        }
        set {
            this.bondDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondNumberField {
        get {
            return this.bondNumberFieldField;
        }
        set {
            this.bondNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string commentField {
        get {
            return this.commentFieldField;
        }
        set {
            this.commentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateField {
        get {
            return this.dateFieldField;
        }
        set {
            this.dateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateOfBirthIDField {
        get {
            return this.dateOfBirthIDFieldField;
        }
        set {
            this.dateOfBirthIDFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string deedsOfficeField {
        get {
            return this.deedsOfficeFieldField;
        }
        set {
            this.deedsOfficeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string eRFField {
        get {
            return this.eRFFieldField;
        }
        set {
            this.eRFFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string farmField {
        get {
            return this.farmFieldField;
        }
        set {
            this.farmFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string mortgageeField {
        get {
            return this.mortgageeFieldField;
        }
        set {
            this.mortgageeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string portionField {
        get {
            return this.portionFieldField;
        }
        set {
            this.portionFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string propertyNameField {
        get {
            return this.propertyNameFieldField;
        }
        set {
            this.propertyNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string propertySizeField {
        get {
            return this.propertySizeFieldField;
        }
        set {
            this.propertySizeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string purchaseDateField {
        get {
            return this.purchaseDateFieldField;
        }
        set {
            this.purchaseDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string purchasePriceField {
        get {
            return this.purchasePriceFieldField;
        }
        set {
            this.purchasePriceFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string rowIDField {
        get {
            return this.rowIDFieldField;
        }
        set {
            this.rowIDFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string titleField {
        get {
            return this.titleFieldField;
        }
        set {
            this.titleFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string townshipField {
        get {
            return this.townshipFieldField;
        }
        set {
            this.townshipFieldField = value;
        }
    }
}

/// <remarks/>





public partial class CurrentLiabilities : BureauSegment {
    
    private string[] currentLiabilityItemAmtFieldField;
    
    private string[] currentLiabilityItemFieldField;
    
    private string currentLiabilityTotalFieldField;
    
    private string majorProductFieldField;
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] currentLiabilityItemAmtField {
        get {
            return this.currentLiabilityItemAmtFieldField;
        }
        set {
            this.currentLiabilityItemAmtFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] currentLiabilityItemField {
        get {
            return this.currentLiabilityItemFieldField;
        }
        set {
            this.currentLiabilityItemFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string currentLiabilityTotalField {
        get {
            return this.currentLiabilityTotalFieldField;
        }
        set {
            this.currentLiabilityTotalFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
}

/// <remarks/>





public partial class CurrentAsset : BureauSegment {
    
    private string[] currentAssItemAmtFieldField;
    
    private string[] currentAssItemFieldField;
    
    private string currentAssTotalFieldField;
    
    private string majorProductFieldField;
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] currentAssItemAmtField {
        get {
            return this.currentAssItemAmtFieldField;
        }
        set {
            this.currentAssItemAmtFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] currentAssItemField {
        get {
            return this.currentAssItemFieldField;
        }
        set {
            this.currentAssItemFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string currentAssTotalField {
        get {
            return this.currentAssTotalFieldField;
        }
        set {
            this.currentAssTotalFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
}

/// <remarks/>





public partial class CourtRecord : BureauSegment {
    
    private string abandonDateFieldField;
    
    private string actionDateFieldField;
    
    private string attorneyFieldField;
    
    private string caseNumberFieldField;
    
    private string cityFieldField;
    
    private string claimAmountFieldField;
    
    private string commentFieldField;
    
    private string countryFieldField;
    
    private string courtDistrictFieldField;
    
    private string courtRecordAddressFieldField;
    
    private string courtTypeFieldField;
    
    private string defendantDistrictFieldField;
    
    private string defendantNameFieldField;
    
    private string defendantTradeStyleFieldField;
    
    private string majorProductFieldField;
    
    private string messageFieldField;
    
    private string natureOfDebtFieldField;
    
    private string numberFoundFieldField;
    
    private string plaintiffNameFieldField;
    
    private string postCodeFieldField;
    
    private string returnDateFieldField;
    
    private string serialNumberFieldField;
    
    private string suburbFieldField;
    
    private string typeCodeFieldField;
    
    private string typeDescFieldField;
    
    /// <remarks/>
    
    public string abandonDateField {
        get {
            return this.abandonDateFieldField;
        }
        set {
            this.abandonDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string actionDateField {
        get {
            return this.actionDateFieldField;
        }
        set {
            this.actionDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string attorneyField {
        get {
            return this.attorneyFieldField;
        }
        set {
            this.attorneyFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string caseNumberField {
        get {
            return this.caseNumberFieldField;
        }
        set {
            this.caseNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string cityField {
        get {
            return this.cityFieldField;
        }
        set {
            this.cityFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string claimAmountField {
        get {
            return this.claimAmountFieldField;
        }
        set {
            this.claimAmountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string commentField {
        get {
            return this.commentFieldField;
        }
        set {
            this.commentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string countryField {
        get {
            return this.countryFieldField;
        }
        set {
            this.countryFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string courtDistrictField {
        get {
            return this.courtDistrictFieldField;
        }
        set {
            this.courtDistrictFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string courtRecordAddressField {
        get {
            return this.courtRecordAddressFieldField;
        }
        set {
            this.courtRecordAddressFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string courtTypeField {
        get {
            return this.courtTypeFieldField;
        }
        set {
            this.courtTypeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string defendantDistrictField {
        get {
            return this.defendantDistrictFieldField;
        }
        set {
            this.defendantDistrictFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string defendantNameField {
        get {
            return this.defendantNameFieldField;
        }
        set {
            this.defendantNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string defendantTradeStyleField {
        get {
            return this.defendantTradeStyleFieldField;
        }
        set {
            this.defendantTradeStyleFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string messageField {
        get {
            return this.messageFieldField;
        }
        set {
            this.messageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string natureOfDebtField {
        get {
            return this.natureOfDebtFieldField;
        }
        set {
            this.natureOfDebtFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string numberFoundField {
        get {
            return this.numberFoundFieldField;
        }
        set {
            this.numberFoundFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string plaintiffNameField {
        get {
            return this.plaintiffNameFieldField;
        }
        set {
            this.plaintiffNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string postCodeField {
        get {
            return this.postCodeFieldField;
        }
        set {
            this.postCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string returnDateField {
        get {
            return this.returnDateFieldField;
        }
        set {
            this.returnDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string serialNumberField {
        get {
            return this.serialNumberFieldField;
        }
        set {
            this.serialNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string suburbField {
        get {
            return this.suburbFieldField;
        }
        set {
            this.suburbFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string typeCodeField {
        get {
            return this.typeCodeFieldField;
        }
        set {
            this.typeCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string typeDescField {
        get {
            return this.typeDescFieldField;
        }
        set {
            this.typeDescFieldField = value;
        }
    }
}

/// <remarks/>





public partial class ConsumerTraceAlert : BureauSegment {
    
    private string comment1FieldField;
    
    private string comment2FieldField;
    
    private string consumerNoFieldField;
    
    private string contactNameFieldField;
    
    private string contactPhoneFieldField;
    
    private string informationDateFieldField;
    
    private string majorProductFieldField;
    
    private string messageFieldField;
    
    private string subscriberNameFieldField;
    
    private string subscriberNumberFieldField;
    
    private string traceTypeCodeFieldField;
    
    private string traceTypeFieldField;
    
    private string transactionTypeFieldField;
    
    /// <remarks/>
    
    public string comment1Field {
        get {
            return this.comment1FieldField;
        }
        set {
            this.comment1FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string comment2Field {
        get {
            return this.comment2FieldField;
        }
        set {
            this.comment2FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string consumerNoField {
        get {
            return this.consumerNoFieldField;
        }
        set {
            this.consumerNoFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string contactNameField {
        get {
            return this.contactNameFieldField;
        }
        set {
            this.contactNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string contactPhoneField {
        get {
            return this.contactPhoneFieldField;
        }
        set {
            this.contactPhoneFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string informationDateField {
        get {
            return this.informationDateFieldField;
        }
        set {
            this.informationDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string messageField {
        get {
            return this.messageFieldField;
        }
        set {
            this.messageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string subscriberNameField {
        get {
            return this.subscriberNameFieldField;
        }
        set {
            this.subscriberNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string subscriberNumberField {
        get {
            return this.subscriberNumberFieldField;
        }
        set {
            this.subscriberNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string traceTypeCodeField {
        get {
            return this.traceTypeCodeFieldField;
        }
        set {
            this.traceTypeCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string traceTypeField {
        get {
            return this.traceTypeFieldField;
        }
        set {
            this.traceTypeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string transactionTypeField {
        get {
            return this.transactionTypeFieldField;
        }
        set {
            this.transactionTypeFieldField = value;
        }
    }
}

/// <remarks/>





public partial class ConsumerNotice : BureauSegment {
    
    private string address1FieldField;
    
    private string amountFieldField;
    
    private string attorneyNameFieldField;
    
    private string attorneyPhoneFieldField;
    
    private string attorneyReferenceFieldField;
    
    private string captureDateFieldField;
    
    private string caseNumberFieldField;
    
    private string consumerNumberFieldField;
    
    private string courtCodeFieldField;
    
    private string courtNameFieldField;
    
    private string courtTypeCodeFieldField;
    
    private string courtTypeDescFieldField;
    
    private string dateOfBirthFieldField;
    
    private string defNoFieldField;
    
    private string defendantNameFieldField;
    
    private string iDNoFieldField;
    
    private string majorProductFieldField;
    
    private string masterNoFieldField;
    
    private string messageFieldField;
    
    private string noticeCodeFieldField;
    
    private string noticeDateFieldField;
    
    private string noticeDescriptionFieldField;
    
    private string plaintiffNameFieldField;
    
    private string remarksFieldField;
    
    private string transTypeFieldField;
    
    /// <remarks/>
    
    public string address1Field {
        get {
            return this.address1FieldField;
        }
        set {
            this.address1FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string amountField {
        get {
            return this.amountFieldField;
        }
        set {
            this.amountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string attorneyNameField {
        get {
            return this.attorneyNameFieldField;
        }
        set {
            this.attorneyNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string attorneyPhoneField {
        get {
            return this.attorneyPhoneFieldField;
        }
        set {
            this.attorneyPhoneFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string attorneyReferenceField {
        get {
            return this.attorneyReferenceFieldField;
        }
        set {
            this.attorneyReferenceFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string captureDateField {
        get {
            return this.captureDateFieldField;
        }
        set {
            this.captureDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string caseNumberField {
        get {
            return this.caseNumberFieldField;
        }
        set {
            this.caseNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string consumerNumberField {
        get {
            return this.consumerNumberFieldField;
        }
        set {
            this.consumerNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string courtCodeField {
        get {
            return this.courtCodeFieldField;
        }
        set {
            this.courtCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string courtNameField {
        get {
            return this.courtNameFieldField;
        }
        set {
            this.courtNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string courtTypeCodeField {
        get {
            return this.courtTypeCodeFieldField;
        }
        set {
            this.courtTypeCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string courtTypeDescField {
        get {
            return this.courtTypeDescFieldField;
        }
        set {
            this.courtTypeDescFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateOfBirthField {
        get {
            return this.dateOfBirthFieldField;
        }
        set {
            this.dateOfBirthFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string defNoField {
        get {
            return this.defNoFieldField;
        }
        set {
            this.defNoFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string defendantNameField {
        get {
            return this.defendantNameFieldField;
        }
        set {
            this.defendantNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string iDNoField {
        get {
            return this.iDNoFieldField;
        }
        set {
            this.iDNoFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string masterNoField {
        get {
            return this.masterNoFieldField;
        }
        set {
            this.masterNoFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string messageField {
        get {
            return this.messageFieldField;
        }
        set {
            this.messageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string noticeCodeField {
        get {
            return this.noticeCodeFieldField;
        }
        set {
            this.noticeCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string noticeDateField {
        get {
            return this.noticeDateFieldField;
        }
        set {
            this.noticeDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string noticeDescriptionField {
        get {
            return this.noticeDescriptionFieldField;
        }
        set {
            this.noticeDescriptionFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string plaintiffNameField {
        get {
            return this.plaintiffNameFieldField;
        }
        set {
            this.plaintiffNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string remarksField {
        get {
            return this.remarksFieldField;
        }
        set {
            this.remarksFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string transTypeField {
        get {
            return this.transTypeFieldField;
        }
        set {
            this.transTypeFieldField = value;
        }
    }
}

/// <remarks/>





public partial class ConsumerNotarialBonds : BureauSegment {
    
    private string addressFieldField;
    
    private string amountFieldField;
    
    private string bondPercFieldField;
    
    private string captureDateFieldField;
    
    private string caseNoFieldField;
    
    private string consumerNumberFieldField;
    
    private string courtCodeFieldField;
    
    private string courtDescFieldField;
    
    private string courtTypeDescFieldField;
    
    private string courtTypeFieldField;
    
    private string dateOfBirthFieldField;
    
    private string defendantNameFieldField;
    
    private string defendantNumberFieldField;
    
    private string iDNoFieldField;
    
    private string judgementCodeFieldField;
    
    private string judgementDescFieldField;
    
    private string majorProductFieldField;
    
    private string masterNoFieldField;
    
    private string messageFieldField;
    
    private string notarialDateFieldField;
    
    private string plaintiffFieldField;
    
    private string remarksFieldField;
    
    private string transTypeFieldField;
    
    /// <remarks/>
    
    public string addressField {
        get {
            return this.addressFieldField;
        }
        set {
            this.addressFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string amountField {
        get {
            return this.amountFieldField;
        }
        set {
            this.amountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondPercField {
        get {
            return this.bondPercFieldField;
        }
        set {
            this.bondPercFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string captureDateField {
        get {
            return this.captureDateFieldField;
        }
        set {
            this.captureDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string caseNoField {
        get {
            return this.caseNoFieldField;
        }
        set {
            this.caseNoFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string consumerNumberField {
        get {
            return this.consumerNumberFieldField;
        }
        set {
            this.consumerNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string courtCodeField {
        get {
            return this.courtCodeFieldField;
        }
        set {
            this.courtCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string courtDescField {
        get {
            return this.courtDescFieldField;
        }
        set {
            this.courtDescFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string courtTypeDescField {
        get {
            return this.courtTypeDescFieldField;
        }
        set {
            this.courtTypeDescFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string courtTypeField {
        get {
            return this.courtTypeFieldField;
        }
        set {
            this.courtTypeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateOfBirthField {
        get {
            return this.dateOfBirthFieldField;
        }
        set {
            this.dateOfBirthFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string defendantNameField {
        get {
            return this.defendantNameFieldField;
        }
        set {
            this.defendantNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string defendantNumberField {
        get {
            return this.defendantNumberFieldField;
        }
        set {
            this.defendantNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string iDNoField {
        get {
            return this.iDNoFieldField;
        }
        set {
            this.iDNoFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string judgementCodeField {
        get {
            return this.judgementCodeFieldField;
        }
        set {
            this.judgementCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string judgementDescField {
        get {
            return this.judgementDescFieldField;
        }
        set {
            this.judgementDescFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string masterNoField {
        get {
            return this.masterNoFieldField;
        }
        set {
            this.masterNoFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string messageField {
        get {
            return this.messageFieldField;
        }
        set {
            this.messageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string notarialDateField {
        get {
            return this.notarialDateFieldField;
        }
        set {
            this.notarialDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string plaintiffField {
        get {
            return this.plaintiffFieldField;
        }
        set {
            this.plaintiffFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string remarksField {
        get {
            return this.remarksFieldField;
        }
        set {
            this.remarksFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string transTypeField {
        get {
            return this.transTypeFieldField;
        }
        set {
            this.transTypeFieldField = value;
        }
    }
}

/// <remarks/>





public partial class ConsumerJudgement : BureauSegment {
    
    private string address1FieldField;
    
    private string adminMonthlyAmountFieldField;
    
    private string adminNoMonthsFieldField;
    
    private string adminStartDateFieldField;
    
    private string amountFieldField;
    
    private string attorneyNameFieldField;
    
    private string attorneyPhoneFieldField;
    
    private string attorneyReferenceFieldField;
    
    private string captureDateFieldField;
    
    private string caseNumberFieldField;
    
    private string consumerNumberFieldField;
    
    private string courtNameCodeFieldField;
    
    private string courtNameFieldField;
    
    private string courtTypeCodeFieldField;
    
    private string courtTypeFieldField;
    
    private string dateOfBirthFieldField;
    
    private string debtCodeFieldField;
    
    private string debtDescriptionFieldField;
    
    private string defNoFieldField;
    
    private string defendantNameFieldField;
    
    private string iDNoFieldField;
    
    private string judgementCodeFieldField;
    
    private string judgementDateFieldField;
    
    private string judgementDescriptionFieldField;
    
    private string majorProductFieldField;
    
    private string masterNumberFieldField;
    
    private string messageFieldField;
    
    private string plaintiffNameFieldField;
    
    private string remarksFieldField;
    
    private string tradeStyleFieldField;
    
    private string transTypeFieldField;
    
    /// <remarks/>
    
    public string address1Field {
        get {
            return this.address1FieldField;
        }
        set {
            this.address1FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string adminMonthlyAmountField {
        get {
            return this.adminMonthlyAmountFieldField;
        }
        set {
            this.adminMonthlyAmountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string adminNoMonthsField {
        get {
            return this.adminNoMonthsFieldField;
        }
        set {
            this.adminNoMonthsFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string adminStartDateField {
        get {
            return this.adminStartDateFieldField;
        }
        set {
            this.adminStartDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string amountField {
        get {
            return this.amountFieldField;
        }
        set {
            this.amountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string attorneyNameField {
        get {
            return this.attorneyNameFieldField;
        }
        set {
            this.attorneyNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string attorneyPhoneField {
        get {
            return this.attorneyPhoneFieldField;
        }
        set {
            this.attorneyPhoneFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string attorneyReferenceField {
        get {
            return this.attorneyReferenceFieldField;
        }
        set {
            this.attorneyReferenceFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string captureDateField {
        get {
            return this.captureDateFieldField;
        }
        set {
            this.captureDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string caseNumberField {
        get {
            return this.caseNumberFieldField;
        }
        set {
            this.caseNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string consumerNumberField {
        get {
            return this.consumerNumberFieldField;
        }
        set {
            this.consumerNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string courtNameCodeField {
        get {
            return this.courtNameCodeFieldField;
        }
        set {
            this.courtNameCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string courtNameField {
        get {
            return this.courtNameFieldField;
        }
        set {
            this.courtNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string courtTypeCodeField {
        get {
            return this.courtTypeCodeFieldField;
        }
        set {
            this.courtTypeCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string courtTypeField {
        get {
            return this.courtTypeFieldField;
        }
        set {
            this.courtTypeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateOfBirthField {
        get {
            return this.dateOfBirthFieldField;
        }
        set {
            this.dateOfBirthFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string debtCodeField {
        get {
            return this.debtCodeFieldField;
        }
        set {
            this.debtCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string debtDescriptionField {
        get {
            return this.debtDescriptionFieldField;
        }
        set {
            this.debtDescriptionFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string defNoField {
        get {
            return this.defNoFieldField;
        }
        set {
            this.defNoFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string defendantNameField {
        get {
            return this.defendantNameFieldField;
        }
        set {
            this.defendantNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string iDNoField {
        get {
            return this.iDNoFieldField;
        }
        set {
            this.iDNoFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string judgementCodeField {
        get {
            return this.judgementCodeFieldField;
        }
        set {
            this.judgementCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string judgementDateField {
        get {
            return this.judgementDateFieldField;
        }
        set {
            this.judgementDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string judgementDescriptionField {
        get {
            return this.judgementDescriptionFieldField;
        }
        set {
            this.judgementDescriptionFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string masterNumberField {
        get {
            return this.masterNumberFieldField;
        }
        set {
            this.masterNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string messageField {
        get {
            return this.messageFieldField;
        }
        set {
            this.messageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string plaintiffNameField {
        get {
            return this.plaintiffNameFieldField;
        }
        set {
            this.plaintiffNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string remarksField {
        get {
            return this.remarksFieldField;
        }
        set {
            this.remarksFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string tradeStyleField {
        get {
            return this.tradeStyleFieldField;
        }
        set {
            this.tradeStyleFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string transTypeField {
        get {
            return this.transTypeFieldField;
        }
        set {
            this.transTypeFieldField = value;
        }
    }
}

/// <remarks/>





public partial class ConsumerInfoNO04 : BureauSegment {
    
    private string consumerNoFieldField;
    
    private string dateOfBirthFieldField;
    
    private string deceasedDateFieldField;
    
    private string dependantsFieldField;
    
    private string forename1FieldField;
    
    private string forename2FieldField;
    
    private string forename3FieldField;
    
    private string genderFieldField;
    
    private string identityNo1FieldField;
    
    private string identityNo2FieldField;
    
    private string maritalStatusCodeFieldField;
    
    private string maritalStatusDescFieldField;
    
    private string nameInfoDateFieldField;
    
    private string partFieldField;
    
    private string partSeqFieldField;
    
    private string recordSeqFieldField;
    
    private string spouseName1FieldField;
    
    private string spouseName2FieldField;
    
    private string surnameFieldField;
    
    private string telephoneNumbersFieldField;
    
    private string titleFieldField;
    
    /// <remarks/>
    
    public string consumerNoField {
        get {
            return this.consumerNoFieldField;
        }
        set {
            this.consumerNoFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateOfBirthField {
        get {
            return this.dateOfBirthFieldField;
        }
        set {
            this.dateOfBirthFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string deceasedDateField {
        get {
            return this.deceasedDateFieldField;
        }
        set {
            this.deceasedDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dependantsField {
        get {
            return this.dependantsFieldField;
        }
        set {
            this.dependantsFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string forename1Field {
        get {
            return this.forename1FieldField;
        }
        set {
            this.forename1FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string forename2Field {
        get {
            return this.forename2FieldField;
        }
        set {
            this.forename2FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string forename3Field {
        get {
            return this.forename3FieldField;
        }
        set {
            this.forename3FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string genderField {
        get {
            return this.genderFieldField;
        }
        set {
            this.genderFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string identityNo1Field {
        get {
            return this.identityNo1FieldField;
        }
        set {
            this.identityNo1FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string identityNo2Field {
        get {
            return this.identityNo2FieldField;
        }
        set {
            this.identityNo2FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string maritalStatusCodeField {
        get {
            return this.maritalStatusCodeFieldField;
        }
        set {
            this.maritalStatusCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string maritalStatusDescField {
        get {
            return this.maritalStatusDescFieldField;
        }
        set {
            this.maritalStatusDescFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string nameInfoDateField {
        get {
            return this.nameInfoDateFieldField;
        }
        set {
            this.nameInfoDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string partField {
        get {
            return this.partFieldField;
        }
        set {
            this.partFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string partSeqField {
        get {
            return this.partSeqFieldField;
        }
        set {
            this.partSeqFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string recordSeqField {
        get {
            return this.recordSeqFieldField;
        }
        set {
            this.recordSeqFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string spouseName1Field {
        get {
            return this.spouseName1FieldField;
        }
        set {
            this.spouseName1FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string spouseName2Field {
        get {
            return this.spouseName2FieldField;
        }
        set {
            this.spouseName2FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string surnameField {
        get {
            return this.surnameFieldField;
        }
        set {
            this.surnameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string telephoneNumbersField {
        get {
            return this.telephoneNumbersFieldField;
        }
        set {
            this.telephoneNumbersFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string titleField {
        get {
            return this.titleFieldField;
        }
        set {
            this.titleFieldField = value;
        }
    }
}

/// <remarks/>





public partial class ConsumerHeader : BureauSegment {
    
    private string aKADate1FieldField;
    
    private string aKADate2FieldField;
    
    private string aKADate3FieldField;
    
    private string aKAName1FieldField;
    
    private string aKAName2FieldField;
    
    private string aKAName3FieldField;
    
    private string address1FieldField;
    
    private string address2FieldField;
    
    private string address3FieldField;
    
    private string address4FieldField;
    
    private string addressDate1FieldField;
    
    private string addressDate2FieldField;
    
    private string addressDate3FieldField;
    
    private string addressDate4FieldField;
    
    private string consumerNumberFieldField;
    
    private string dateOfBirthFieldField;
    
    private string debtCouncilDateFieldField;
    
    private string disputeDateFieldField;
    
    private string employer1FieldField;
    
    private string employer2FieldField;
    
    private string employer3FieldField;
    
    private string employmentDate1FieldField;
    
    private string employmentDate2FieldField;
    
    private string employmentDate3FieldField;
    
    private string forename1FieldField;
    
    private string forename2FieldField;
    
    private string forename3FieldField;
    
    private string homePhoneCodeFieldField;
    
    private string homePhoneNumberFieldField;
    
    private string iDNo1FieldField;
    
    private string iDNo2FieldField;
    
    private string majorProductFieldField;
    
    private string matchFieldField;
    
    private string messageFieldField;
    
    private string occupation1FieldField;
    
    private string occupation2FieldField;
    
    private string occupation3FieldField;
    
    private string remarksFieldField;
    
    private string spouseForenameFieldField;
    
    private string surnameFieldField;
    
    private string ticketNumberFieldField;
    
    private string workPhoneCodeFieldField;
    
    private string workPhoneNumberFieldField;
    
    /// <remarks/>
    
    public string aKADate1Field {
        get {
            return this.aKADate1FieldField;
        }
        set {
            this.aKADate1FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string aKADate2Field {
        get {
            return this.aKADate2FieldField;
        }
        set {
            this.aKADate2FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string aKADate3Field {
        get {
            return this.aKADate3FieldField;
        }
        set {
            this.aKADate3FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string aKAName1Field {
        get {
            return this.aKAName1FieldField;
        }
        set {
            this.aKAName1FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string aKAName2Field {
        get {
            return this.aKAName2FieldField;
        }
        set {
            this.aKAName2FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string aKAName3Field {
        get {
            return this.aKAName3FieldField;
        }
        set {
            this.aKAName3FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string address1Field {
        get {
            return this.address1FieldField;
        }
        set {
            this.address1FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string address2Field {
        get {
            return this.address2FieldField;
        }
        set {
            this.address2FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string address3Field {
        get {
            return this.address3FieldField;
        }
        set {
            this.address3FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string address4Field {
        get {
            return this.address4FieldField;
        }
        set {
            this.address4FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string addressDate1Field {
        get {
            return this.addressDate1FieldField;
        }
        set {
            this.addressDate1FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string addressDate2Field {
        get {
            return this.addressDate2FieldField;
        }
        set {
            this.addressDate2FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string addressDate3Field {
        get {
            return this.addressDate3FieldField;
        }
        set {
            this.addressDate3FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string addressDate4Field {
        get {
            return this.addressDate4FieldField;
        }
        set {
            this.addressDate4FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string consumerNumberField {
        get {
            return this.consumerNumberFieldField;
        }
        set {
            this.consumerNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateOfBirthField {
        get {
            return this.dateOfBirthFieldField;
        }
        set {
            this.dateOfBirthFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string debtCouncilDateField {
        get {
            return this.debtCouncilDateFieldField;
        }
        set {
            this.debtCouncilDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string disputeDateField {
        get {
            return this.disputeDateFieldField;
        }
        set {
            this.disputeDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string employer1Field {
        get {
            return this.employer1FieldField;
        }
        set {
            this.employer1FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string employer2Field {
        get {
            return this.employer2FieldField;
        }
        set {
            this.employer2FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string employer3Field {
        get {
            return this.employer3FieldField;
        }
        set {
            this.employer3FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string employmentDate1Field {
        get {
            return this.employmentDate1FieldField;
        }
        set {
            this.employmentDate1FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string employmentDate2Field {
        get {
            return this.employmentDate2FieldField;
        }
        set {
            this.employmentDate2FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string employmentDate3Field {
        get {
            return this.employmentDate3FieldField;
        }
        set {
            this.employmentDate3FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string forename1Field {
        get {
            return this.forename1FieldField;
        }
        set {
            this.forename1FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string forename2Field {
        get {
            return this.forename2FieldField;
        }
        set {
            this.forename2FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string forename3Field {
        get {
            return this.forename3FieldField;
        }
        set {
            this.forename3FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string homePhoneCodeField {
        get {
            return this.homePhoneCodeFieldField;
        }
        set {
            this.homePhoneCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string homePhoneNumberField {
        get {
            return this.homePhoneNumberFieldField;
        }
        set {
            this.homePhoneNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string iDNo1Field {
        get {
            return this.iDNo1FieldField;
        }
        set {
            this.iDNo1FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string iDNo2Field {
        get {
            return this.iDNo2FieldField;
        }
        set {
            this.iDNo2FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string matchField {
        get {
            return this.matchFieldField;
        }
        set {
            this.matchFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string messageField {
        get {
            return this.messageFieldField;
        }
        set {
            this.messageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string occupation1Field {
        get {
            return this.occupation1FieldField;
        }
        set {
            this.occupation1FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string occupation2Field {
        get {
            return this.occupation2FieldField;
        }
        set {
            this.occupation2FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string occupation3Field {
        get {
            return this.occupation3FieldField;
        }
        set {
            this.occupation3FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string remarksField {
        get {
            return this.remarksFieldField;
        }
        set {
            this.remarksFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string spouseForenameField {
        get {
            return this.spouseForenameFieldField;
        }
        set {
            this.spouseForenameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string surnameField {
        get {
            return this.surnameFieldField;
        }
        set {
            this.surnameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string ticketNumberField {
        get {
            return this.ticketNumberFieldField;
        }
        set {
            this.ticketNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string workPhoneCodeField {
        get {
            return this.workPhoneCodeFieldField;
        }
        set {
            this.workPhoneCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string workPhoneNumberField {
        get {
            return this.workPhoneNumberFieldField;
        }
        set {
            this.workPhoneNumberFieldField = value;
        }
    }
}

/// <remarks/>





public partial class ConsumerHeaderC1 : BureauSegment {
    
    private string deceasedDateFieldField;
    
    private string entryDateFieldField;
    
    private string maritalStatusFieldField;
    
    private string segmentCodeFieldField;
    
    private string telHomeCode1FieldField;
    
    private string telHomeCode2FieldField;
    
    private string telHomeNumber1FieldField;
    
    private string telHomeNumber2FieldField;
    
    private string telWorkCode1FieldField;
    
    private string telWorkCode2FieldField;
    
    private string telWorkNumber1FieldField;
    
    private string telWorkNumber2FieldField;
    
    /// <remarks/>
    
    public string deceasedDateField {
        get {
            return this.deceasedDateFieldField;
        }
        set {
            this.deceasedDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string entryDateField {
        get {
            return this.entryDateFieldField;
        }
        set {
            this.entryDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string maritalStatusField {
        get {
            return this.maritalStatusFieldField;
        }
        set {
            this.maritalStatusFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string segmentCodeField {
        get {
            return this.segmentCodeFieldField;
        }
        set {
            this.segmentCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string telHomeCode1Field {
        get {
            return this.telHomeCode1FieldField;
        }
        set {
            this.telHomeCode1FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string telHomeCode2Field {
        get {
            return this.telHomeCode2FieldField;
        }
        set {
            this.telHomeCode2FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string telHomeNumber1Field {
        get {
            return this.telHomeNumber1FieldField;
        }
        set {
            this.telHomeNumber1FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string telHomeNumber2Field {
        get {
            return this.telHomeNumber2FieldField;
        }
        set {
            this.telHomeNumber2FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string telWorkCode1Field {
        get {
            return this.telWorkCode1FieldField;
        }
        set {
            this.telWorkCode1FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string telWorkCode2Field {
        get {
            return this.telWorkCode2FieldField;
        }
        set {
            this.telWorkCode2FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string telWorkNumber1Field {
        get {
            return this.telWorkNumber1FieldField;
        }
        set {
            this.telWorkNumber1FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string telWorkNumber2Field {
        get {
            return this.telWorkNumber2FieldField;
        }
        set {
            this.telWorkNumber2FieldField = value;
        }
    }
}

/// <remarks/>





public partial class ConsumerEnquiry : BureauSegment {
    
    private string consumerNoFieldField;
    
    private string contactFieldField;
    
    private string enquiryDateFieldField;
    
    private string majorProductFieldField;
    
    private string subscriberFieldField;
    
    private string transTypeFieldField;
    
    private string typeFieldField;
    
    /// <remarks/>
    
    public string consumerNoField {
        get {
            return this.consumerNoFieldField;
        }
        set {
            this.consumerNoFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string contactField {
        get {
            return this.contactFieldField;
        }
        set {
            this.contactFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string enquiryDateField {
        get {
            return this.enquiryDateFieldField;
        }
        set {
            this.enquiryDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string subscriberField {
        get {
            return this.subscriberFieldField;
        }
        set {
            this.subscriberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string transTypeField {
        get {
            return this.transTypeFieldField;
        }
        set {
            this.transTypeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string typeField {
        get {
            return this.typeFieldField;
        }
        set {
            this.typeFieldField = value;
        }
    }
}

/// <remarks/>





public partial class ConsumerDefault : BureauSegment {
    
    private string accountF700FieldField;
    
    private string accountFieldField;
    
    private string addressFieldField;
    
    private string amountF700FieldField;
    
    private string amountFieldField;
    
    private string branchCodeFieldField;
    
    private string consumerNoFieldField;
    
    private string contactNameFieldField;
    
    private string contactPhoneCodeFieldField;
    
    private string contactPhoneNoFieldField;
    
    private string dateOfBirthFieldField;
    
    private string debtCodeFieldField;
    
    private string debtDescFieldField;
    
    private string defaultDateFieldField;
    
    private string fnDefaultFieldField;
    
    private string iDNoFieldField;
    
    private string majorProductFieldField;
    
    private string messageFieldField;
    
    private string nameFieldField;
    
    private string remarksFieldField;
    
    private string remarksXFieldField;
    
    private string remarksYFieldField;
    
    private string subAccountFieldField;
    
    private string subscriberNameFieldField;
    
    private string transTypeFieldField;
    
    /// <remarks/>
    
    public string accountF700Field {
        get {
            return this.accountF700FieldField;
        }
        set {
            this.accountF700FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string accountField {
        get {
            return this.accountFieldField;
        }
        set {
            this.accountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string addressField {
        get {
            return this.addressFieldField;
        }
        set {
            this.addressFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string amountF700Field {
        get {
            return this.amountF700FieldField;
        }
        set {
            this.amountF700FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string amountField {
        get {
            return this.amountFieldField;
        }
        set {
            this.amountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string branchCodeField {
        get {
            return this.branchCodeFieldField;
        }
        set {
            this.branchCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string consumerNoField {
        get {
            return this.consumerNoFieldField;
        }
        set {
            this.consumerNoFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string contactNameField {
        get {
            return this.contactNameFieldField;
        }
        set {
            this.contactNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string contactPhoneCodeField {
        get {
            return this.contactPhoneCodeFieldField;
        }
        set {
            this.contactPhoneCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string contactPhoneNoField {
        get {
            return this.contactPhoneNoFieldField;
        }
        set {
            this.contactPhoneNoFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateOfBirthField {
        get {
            return this.dateOfBirthFieldField;
        }
        set {
            this.dateOfBirthFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string debtCodeField {
        get {
            return this.debtCodeFieldField;
        }
        set {
            this.debtCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string debtDescField {
        get {
            return this.debtDescFieldField;
        }
        set {
            this.debtDescFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string defaultDateField {
        get {
            return this.defaultDateFieldField;
        }
        set {
            this.defaultDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string fnDefaultField {
        get {
            return this.fnDefaultFieldField;
        }
        set {
            this.fnDefaultFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string iDNoField {
        get {
            return this.iDNoFieldField;
        }
        set {
            this.iDNoFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string messageField {
        get {
            return this.messageFieldField;
        }
        set {
            this.messageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string nameField {
        get {
            return this.nameFieldField;
        }
        set {
            this.nameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string remarksField {
        get {
            return this.remarksFieldField;
        }
        set {
            this.remarksFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string remarksXField {
        get {
            return this.remarksXFieldField;
        }
        set {
            this.remarksXFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string remarksYField {
        get {
            return this.remarksYFieldField;
        }
        set {
            this.remarksYFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string subAccountField {
        get {
            return this.subAccountFieldField;
        }
        set {
            this.subAccountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string subscriberNameField {
        get {
            return this.subscriberNameFieldField;
        }
        set {
            this.subscriberNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string transTypeField {
        get {
            return this.transTypeFieldField;
        }
        set {
            this.transTypeFieldField = value;
        }
    }
}

/// <remarks/>





public partial class ConsumerBusinessEnquiry : BureauSegment {
    
    private string consumerNoFieldField;
    
    private string contactFieldField;
    
    private string enquiryDateFieldField;
    
    private string majorProductFieldField;
    
    private string subscriberFieldField;
    
    private string transTypeFieldField;
    
    private string typeFieldField;
    
    /// <remarks/>
    
    public string consumerNoField {
        get {
            return this.consumerNoFieldField;
        }
        set {
            this.consumerNoFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string contactField {
        get {
            return this.contactFieldField;
        }
        set {
            this.contactFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string enquiryDateField {
        get {
            return this.enquiryDateFieldField;
        }
        set {
            this.enquiryDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string subscriberField {
        get {
            return this.subscriberFieldField;
        }
        set {
            this.subscriberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string transTypeField {
        get {
            return this.transTypeFieldField;
        }
        set {
            this.transTypeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string typeField {
        get {
            return this.typeFieldField;
        }
        set {
            this.typeFieldField = value;
        }
    }
}

/// <remarks/>





public partial class ComplianceIndicatorLI : BureauSegment {
    
    private string cPAComplianceIndFieldField;
    
    private string dateFieldField;
    
    private string iTNumberFieldField;
    
    private string majorProductFieldField;
    
    private string nCAComplianceIndFieldField;
    
    private string segmentTypeFieldField;
    
    /// <remarks/>
    
    public string cPAComplianceIndField {
        get {
            return this.cPAComplianceIndFieldField;
        }
        set {
            this.cPAComplianceIndFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateField {
        get {
            return this.dateFieldField;
        }
        set {
            this.dateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string iTNumberField {
        get {
            return this.iTNumberFieldField;
        }
        set {
            this.iTNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string nCAComplianceIndField {
        get {
            return this.nCAComplianceIndFieldField;
        }
        set {
            this.nCAComplianceIndFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string segmentTypeField {
        get {
            return this.segmentTypeFieldField;
        }
        set {
            this.segmentTypeFieldField = value;
        }
    }
}

/// <remarks/>





public partial class CommercialDisputeResponse : BureauSegment {
    
    private string responseCodeFieldField;
    
    private string responseDescFieldField;
    
    /// <remarks/>
    
    public string responseCodeField {
        get {
            return this.responseCodeFieldField;
        }
        set {
            this.responseCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string responseDescField {
        get {
            return this.responseDescFieldField;
        }
        set {
            this.responseDescFieldField = value;
        }
    }
}

/// <remarks/>





public partial class CapitalEmployed : BureauSegment {
    
    private string[] capitalEmployedItemAmountFieldField;
    
    private string[] capitalEmployedItemFieldField;
    
    private string capitalEmployedTotalFieldField;
    
    private string majorProductFieldField;
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] capitalEmployedItemAmountField {
        get {
            return this.capitalEmployedItemAmountFieldField;
        }
        set {
            this.capitalEmployedItemAmountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] capitalEmployedItemField {
        get {
            return this.capitalEmployedItemFieldField;
        }
        set {
            this.capitalEmployedItemFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string capitalEmployedTotalField {
        get {
            return this.capitalEmployedTotalFieldField;
        }
        set {
            this.capitalEmployedTotalFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
}

/// <remarks/>





public partial class CancelledTicket : BureauSegment {
    
    private string[] cancelledStatusFieldField;
    
    private string[] statusFieldField;
    
    private string[] subjectNameFieldField;
    
    private string[] ticketFieldField;
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] cancelledStatusField {
        get {
            return this.cancelledStatusFieldField;
        }
        set {
            this.cancelledStatusFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] statusField {
        get {
            return this.statusFieldField;
        }
        set {
            this.statusFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] subjectNameField {
        get {
            return this.subjectNameFieldField;
        }
        set {
            this.subjectNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] ticketField {
        get {
            return this.ticketFieldField;
        }
        set {
            this.ticketFieldField = value;
        }
    }
}

/// <remarks/>





public partial class BusinessBenchmarkBB : BureauSegment {
    
    private string avg_ScoreFieldField;
    
    private BusinessBenchmarkDetail[] businessBenchmarkDetailFieldField;
    
    private string majorProductFieldField;
    
    private string rPT_DateFieldField;
    
    private string ratingFieldField;
    
    private string sASicCodeFieldField;
    
    private string sME_ScoreFieldField;
    
    private string sicDescFieldField;
    
    private string statusFieldField;
    
    private string uSSicCodeFieldField;
    
    /// <remarks/>
    
    public string avg_ScoreField {
        get {
            return this.avg_ScoreFieldField;
        }
        set {
            this.avg_ScoreFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public BusinessBenchmarkDetail[] businessBenchmarkDetailField {
        get {
            return this.businessBenchmarkDetailFieldField;
        }
        set {
            this.businessBenchmarkDetailFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string rPT_DateField {
        get {
            return this.rPT_DateFieldField;
        }
        set {
            this.rPT_DateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string ratingField {
        get {
            return this.ratingFieldField;
        }
        set {
            this.ratingFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string sASicCodeField {
        get {
            return this.sASicCodeFieldField;
        }
        set {
            this.sASicCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string sME_ScoreField {
        get {
            return this.sME_ScoreFieldField;
        }
        set {
            this.sME_ScoreFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string sicDescField {
        get {
            return this.sicDescFieldField;
        }
        set {
            this.sicDescFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string statusField {
        get {
            return this.statusFieldField;
        }
        set {
            this.statusFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string uSSicCodeField {
        get {
            return this.uSSicCodeFieldField;
        }
        set {
            this.uSSicCodeFieldField = value;
        }
    }
}

/// <remarks/>





public partial class BusinessRescueDetail : BureauSegment {
    
    private string businessRescueCommentFieldField;
    
    private string businessRescueDateFieldField;
    
    private string businessRescueTypeFieldField;
    
    private string endDateFieldField;
    
    private string majorProductFieldField;
    
    private string practitionerContactDetailsFieldField;
    
    private string practitionerDateFieldField;
    
    private string practitionerNameFieldField;
    
    private string registrationInfoDateFieldField;
    
    private string registrationStatusCodeFieldField;
    
    private string rescuePlanFieldField;
    
    /// <remarks/>
    
    public string businessRescueCommentField {
        get {
            return this.businessRescueCommentFieldField;
        }
        set {
            this.businessRescueCommentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string businessRescueDateField {
        get {
            return this.businessRescueDateFieldField;
        }
        set {
            this.businessRescueDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string businessRescueTypeField {
        get {
            return this.businessRescueTypeFieldField;
        }
        set {
            this.businessRescueTypeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string endDateField {
        get {
            return this.endDateFieldField;
        }
        set {
            this.endDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string practitionerContactDetailsField {
        get {
            return this.practitionerContactDetailsFieldField;
        }
        set {
            this.practitionerContactDetailsFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string practitionerDateField {
        get {
            return this.practitionerDateFieldField;
        }
        set {
            this.practitionerDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string practitionerNameField {
        get {
            return this.practitionerNameFieldField;
        }
        set {
            this.practitionerNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string registrationInfoDateField {
        get {
            return this.registrationInfoDateFieldField;
        }
        set {
            this.registrationInfoDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string registrationStatusCodeField {
        get {
            return this.registrationStatusCodeFieldField;
        }
        set {
            this.registrationStatusCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string rescuePlanField {
        get {
            return this.rescuePlanFieldField;
        }
        set {
            this.rescuePlanFieldField = value;
        }
    }
}

/// <remarks/>





public partial class BusinessPlusPrincipalSummary : BureauSegment {
    
    private string adverseCountFieldField;
    
    private string debtCounselingFieldField;
    
    private string deedsCountFieldField;
    
    private string forename1FieldField;
    
    private string forename2FieldField;
    
    private string forename3FieldField;
    
    private string iDNumberFieldField;
    
    private string judgementCountFieldField;
    
    private string majorProductFieldField;
    
    private string noticesCountFieldField;
    
    private string numberOfBusinessesFieldField;
    
    private string numberOfNonAdverseNotorialBondsFieldField;
    
    private string surnameFieldField;
    
    /// <remarks/>
    
    public string adverseCountField {
        get {
            return this.adverseCountFieldField;
        }
        set {
            this.adverseCountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string debtCounselingField {
        get {
            return this.debtCounselingFieldField;
        }
        set {
            this.debtCounselingFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string deedsCountField {
        get {
            return this.deedsCountFieldField;
        }
        set {
            this.deedsCountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string forename1Field {
        get {
            return this.forename1FieldField;
        }
        set {
            this.forename1FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string forename2Field {
        get {
            return this.forename2FieldField;
        }
        set {
            this.forename2FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string forename3Field {
        get {
            return this.forename3FieldField;
        }
        set {
            this.forename3FieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string iDNumberField {
        get {
            return this.iDNumberFieldField;
        }
        set {
            this.iDNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string judgementCountField {
        get {
            return this.judgementCountFieldField;
        }
        set {
            this.judgementCountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string noticesCountField {
        get {
            return this.noticesCountFieldField;
        }
        set {
            this.noticesCountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string numberOfBusinessesField {
        get {
            return this.numberOfBusinessesFieldField;
        }
        set {
            this.numberOfBusinessesFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string numberOfNonAdverseNotorialBondsField {
        get {
            return this.numberOfNonAdverseNotorialBondsFieldField;
        }
        set {
            this.numberOfNonAdverseNotorialBondsFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string surnameField {
        get {
            return this.surnameFieldField;
        }
        set {
            this.surnameFieldField = value;
        }
    }
}

/// <remarks/>





public partial class BusinessNotarialBonds : BureauSegment {
    
    private string bondAmountFieldField;
    
    private string bondNumberFieldField;
    
    private string bondOwnerFieldField;
    
    private string bondPercentageFieldField;
    
    private string bondRegDateFieldField;
    
    private string bondTypeFieldField;
    
    private string deedsOfficeFieldField;
    
    private string hOAboveThresholdFieldField;
    
    private string[] heldOverFieldField;
    
    private string majorProductFieldField;
    
    private string messageFieldField;
    
    private string microfilemRefFieldField;
    
    private string multipleOwnersFieldField;
    
    private string ownerIDFieldField;
    
    private string ownerTypeFieldField;
    
    private string parentChildIndFieldField;
    
    /// <remarks/>
    
    public string bondAmountField {
        get {
            return this.bondAmountFieldField;
        }
        set {
            this.bondAmountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondNumberField {
        get {
            return this.bondNumberFieldField;
        }
        set {
            this.bondNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondOwnerField {
        get {
            return this.bondOwnerFieldField;
        }
        set {
            this.bondOwnerFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondPercentageField {
        get {
            return this.bondPercentageFieldField;
        }
        set {
            this.bondPercentageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondRegDateField {
        get {
            return this.bondRegDateFieldField;
        }
        set {
            this.bondRegDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondTypeField {
        get {
            return this.bondTypeFieldField;
        }
        set {
            this.bondTypeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string deedsOfficeField {
        get {
            return this.deedsOfficeFieldField;
        }
        set {
            this.deedsOfficeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string hOAboveThresholdField {
        get {
            return this.hOAboveThresholdFieldField;
        }
        set {
            this.hOAboveThresholdFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] heldOverField {
        get {
            return this.heldOverFieldField;
        }
        set {
            this.heldOverFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string messageField {
        get {
            return this.messageFieldField;
        }
        set {
            this.messageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string microfilemRefField {
        get {
            return this.microfilemRefFieldField;
        }
        set {
            this.microfilemRefFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string multipleOwnersField {
        get {
            return this.multipleOwnersFieldField;
        }
        set {
            this.multipleOwnersFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string ownerIDField {
        get {
            return this.ownerIDFieldField;
        }
        set {
            this.ownerIDFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string ownerTypeField {
        get {
            return this.ownerTypeFieldField;
        }
        set {
            this.ownerTypeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string parentChildIndField {
        get {
            return this.parentChildIndFieldField;
        }
        set {
            this.parentChildIndFieldField = value;
        }
    }
}

/// <remarks/>





public partial class BusinessDeedsSummaryDA : BureauSegment {
    
    private string commentFieldField;
    
    private string deedsOfficeFieldField;
    
    private string majorProductFieldField;
    
    private string numberPropertiesFieldField;
    
    private string oldestPropertyDateFieldField;
    
    private string oldestPropertyValueFieldField;
    
    private string totalBondFieldField;
    
    private string totalOwnedFieldField;
    
    private string totalValueFieldField;
    
    /// <remarks/>
    
    public string commentField {
        get {
            return this.commentFieldField;
        }
        set {
            this.commentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string deedsOfficeField {
        get {
            return this.deedsOfficeFieldField;
        }
        set {
            this.deedsOfficeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string numberPropertiesField {
        get {
            return this.numberPropertiesFieldField;
        }
        set {
            this.numberPropertiesFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string oldestPropertyDateField {
        get {
            return this.oldestPropertyDateFieldField;
        }
        set {
            this.oldestPropertyDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string oldestPropertyValueField {
        get {
            return this.oldestPropertyValueFieldField;
        }
        set {
            this.oldestPropertyValueFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string totalBondField {
        get {
            return this.totalBondFieldField;
        }
        set {
            this.totalBondFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string totalOwnedField {
        get {
            return this.totalOwnedFieldField;
        }
        set {
            this.totalOwnedFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string totalValueField {
        get {
            return this.totalValueFieldField;
        }
        set {
            this.totalValueFieldField = value;
        }
    }
}

/// <remarks/>





public partial class BusinessDeedsDI : BureauSegment {
    
    private string bondAmountFieldField;
    
    private string bondDateFieldField;
    
    private string bondHolderFieldField;
    
    private string bondNumberFieldField;
    
    private string buyerNameFieldField;
    
    private string commentFieldField;
    
    private string dateFieldField;
    
    private string dateOfBirthOrIDNumberFieldField;
    
    private string deedsOfficeFieldField;
    
    private string eRFFieldField;
    
    private string farmFieldField;
    
    private string majorProductFieldField;
    
    private string portionFieldField;
    
    private string propertySizeFieldField;
    
    private string purchaseDateFieldField;
    
    private string purchasePriceFieldField;
    
    private string rowIDFieldField;
    
    private string titleFieldField;
    
    private string townshipFieldField;
    
    /// <remarks/>
    
    public string bondAmountField {
        get {
            return this.bondAmountFieldField;
        }
        set {
            this.bondAmountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondDateField {
        get {
            return this.bondDateFieldField;
        }
        set {
            this.bondDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondHolderField {
        get {
            return this.bondHolderFieldField;
        }
        set {
            this.bondHolderFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondNumberField {
        get {
            return this.bondNumberFieldField;
        }
        set {
            this.bondNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string buyerNameField {
        get {
            return this.buyerNameFieldField;
        }
        set {
            this.buyerNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string commentField {
        get {
            return this.commentFieldField;
        }
        set {
            this.commentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateField {
        get {
            return this.dateFieldField;
        }
        set {
            this.dateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateOfBirthOrIDNumberField {
        get {
            return this.dateOfBirthOrIDNumberFieldField;
        }
        set {
            this.dateOfBirthOrIDNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string deedsOfficeField {
        get {
            return this.deedsOfficeFieldField;
        }
        set {
            this.deedsOfficeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string eRFField {
        get {
            return this.eRFFieldField;
        }
        set {
            this.eRFFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string farmField {
        get {
            return this.farmFieldField;
        }
        set {
            this.farmFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string portionField {
        get {
            return this.portionFieldField;
        }
        set {
            this.portionFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string propertySizeField {
        get {
            return this.propertySizeFieldField;
        }
        set {
            this.propertySizeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string purchaseDateField {
        get {
            return this.purchaseDateFieldField;
        }
        set {
            this.purchaseDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string purchasePriceField {
        get {
            return this.purchasePriceFieldField;
        }
        set {
            this.purchasePriceFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string rowIDField {
        get {
            return this.rowIDFieldField;
        }
        set {
            this.rowIDFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string titleField {
        get {
            return this.titleFieldField;
        }
        set {
            this.titleFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string townshipField {
        get {
            return this.townshipFieldField;
        }
        set {
            this.townshipFieldField = value;
        }
    }
}

/// <remarks/>





public partial class BusinessDeedsComprehensivePW : BureauSegment {
    
    private string bondAmountFieldField;
    
    private string bondDateFieldField;
    
    private string bondNumberFieldField;
    
    private string buyerNameFieldField;
    
    private string commentFieldField;
    
    private string dateFieldField;
    
    private string dateOfBirthIDFieldField;
    
    private string deedsOfficeFieldField;
    
    private string eRFFieldField;
    
    private string farmFieldField;
    
    private string majorProductFieldField;
    
    private string mortgageeFieldField;
    
    private string multipleFieldField;
    
    private string portionFieldField;
    
    private string propertySizeFieldField;
    
    private string propertyTypeFieldField;
    
    private string provinceFieldField;
    
    private string purchaseDateFieldField;
    
    private string purchasePriceFieldField;
    
    private string rowIDFieldField;
    
    private string schemeNameFieldField;
    
    private string schemeNumberFieldField;
    
    private string shareFieldField;
    
    private string streetFieldField;
    
    private string titleFieldField;
    
    private string townshipFieldField;
    
    /// <remarks/>
    
    public string bondAmountField {
        get {
            return this.bondAmountFieldField;
        }
        set {
            this.bondAmountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondDateField {
        get {
            return this.bondDateFieldField;
        }
        set {
            this.bondDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondNumberField {
        get {
            return this.bondNumberFieldField;
        }
        set {
            this.bondNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string buyerNameField {
        get {
            return this.buyerNameFieldField;
        }
        set {
            this.buyerNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string commentField {
        get {
            return this.commentFieldField;
        }
        set {
            this.commentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateField {
        get {
            return this.dateFieldField;
        }
        set {
            this.dateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateOfBirthIDField {
        get {
            return this.dateOfBirthIDFieldField;
        }
        set {
            this.dateOfBirthIDFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string deedsOfficeField {
        get {
            return this.deedsOfficeFieldField;
        }
        set {
            this.deedsOfficeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string eRFField {
        get {
            return this.eRFFieldField;
        }
        set {
            this.eRFFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string farmField {
        get {
            return this.farmFieldField;
        }
        set {
            this.farmFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string mortgageeField {
        get {
            return this.mortgageeFieldField;
        }
        set {
            this.mortgageeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string multipleField {
        get {
            return this.multipleFieldField;
        }
        set {
            this.multipleFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string portionField {
        get {
            return this.portionFieldField;
        }
        set {
            this.portionFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string propertySizeField {
        get {
            return this.propertySizeFieldField;
        }
        set {
            this.propertySizeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string propertyTypeField {
        get {
            return this.propertyTypeFieldField;
        }
        set {
            this.propertyTypeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string provinceField {
        get {
            return this.provinceFieldField;
        }
        set {
            this.provinceFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string purchaseDateField {
        get {
            return this.purchaseDateFieldField;
        }
        set {
            this.purchaseDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string purchasePriceField {
        get {
            return this.purchasePriceFieldField;
        }
        set {
            this.purchasePriceFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string rowIDField {
        get {
            return this.rowIDFieldField;
        }
        set {
            this.rowIDFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string schemeNameField {
        get {
            return this.schemeNameFieldField;
        }
        set {
            this.schemeNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string schemeNumberField {
        get {
            return this.schemeNumberFieldField;
        }
        set {
            this.schemeNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string shareField {
        get {
            return this.shareFieldField;
        }
        set {
            this.shareFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string streetField {
        get {
            return this.streetFieldField;
        }
        set {
            this.streetFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string titleField {
        get {
            return this.titleFieldField;
        }
        set {
            this.titleFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string townshipField {
        get {
            return this.townshipFieldField;
        }
        set {
            this.townshipFieldField = value;
        }
    }
}

/// <remarks/>





public partial class BusinessDeedsComprehensivePB : BureauSegment {
    
    private string bondAmountFieldField;
    
    private string bondDateFieldField;
    
    private string bondHolderFieldField;
    
    private string bondNumberFieldField;
    
    private string commentFieldField;
    
    private string dateFieldField;
    
    private string deedsOfficeFieldField;
    
    private string eRFFieldField;
    
    private string farmFieldField;
    
    private string majorProductFieldField;
    
    private string portionFieldField;
    
    private string propertyNameFieldField;
    
    private string propertySizeFieldField;
    
    private string purchaseDateFieldField;
    
    private string purchasePriceFieldField;
    
    private string registrationNumberFieldField;
    
    private string rowIDFieldField;
    
    private string titleFieldField;
    
    private string townshipFieldField;
    
    /// <remarks/>
    
    public string bondAmountField {
        get {
            return this.bondAmountFieldField;
        }
        set {
            this.bondAmountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondDateField {
        get {
            return this.bondDateFieldField;
        }
        set {
            this.bondDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondHolderField {
        get {
            return this.bondHolderFieldField;
        }
        set {
            this.bondHolderFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bondNumberField {
        get {
            return this.bondNumberFieldField;
        }
        set {
            this.bondNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string commentField {
        get {
            return this.commentFieldField;
        }
        set {
            this.commentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateField {
        get {
            return this.dateFieldField;
        }
        set {
            this.dateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string deedsOfficeField {
        get {
            return this.deedsOfficeFieldField;
        }
        set {
            this.deedsOfficeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string eRFField {
        get {
            return this.eRFFieldField;
        }
        set {
            this.eRFFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string farmField {
        get {
            return this.farmFieldField;
        }
        set {
            this.farmFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string portionField {
        get {
            return this.portionFieldField;
        }
        set {
            this.portionFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string propertyNameField {
        get {
            return this.propertyNameFieldField;
        }
        set {
            this.propertyNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string propertySizeField {
        get {
            return this.propertySizeFieldField;
        }
        set {
            this.propertySizeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string purchaseDateField {
        get {
            return this.purchaseDateFieldField;
        }
        set {
            this.purchaseDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string purchasePriceField {
        get {
            return this.purchasePriceFieldField;
        }
        set {
            this.purchasePriceFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string registrationNumberField {
        get {
            return this.registrationNumberFieldField;
        }
        set {
            this.registrationNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string rowIDField {
        get {
            return this.rowIDFieldField;
        }
        set {
            this.rowIDFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string titleField {
        get {
            return this.titleFieldField;
        }
        set {
            this.titleFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string townshipField {
        get {
            return this.townshipFieldField;
        }
        set {
            this.townshipFieldField = value;
        }
    }
}

/// <remarks/>





public partial class BusinessAdverseSummary : BureauSegment {
    
    private string adverse24MonthsPlusCountFieldField;
    
    private string adverse24MonthsPlusTotalFieldField;
    
    private string adverseLast12MonthsCountFieldField;
    
    private string adverseLast12MonthsTotalFieldField;
    
    private string adverseLast24MonthsCountFieldField;
    
    private string adverseLast24MonthsTotalFieldField;
    
    private string adverseLast6MonthsCountFieldField;
    
    private string adverseLast6MonthsTotalFieldField;
    
    private string adverseTotalAmountFieldField;
    
    private string adverseTotalCountFieldField;
    
    private string judgement24MonthsPlusCountFieldField;
    
    private string judgement24MonthsPlusTotalFieldField;
    
    private string judgementLast12MonthsCountFieldField;
    
    private string judgementLast12MonthsTotalFieldField;
    
    private string judgementLast24MonthsCountFieldField;
    
    private string judgementLast24MonthsTotalFieldField;
    
    private string judgementLast6MonthsCountFieldField;
    
    private string judgementLast6MonthsTotalFieldField;
    
    private string judgementTotalAmountFieldField;
    
    private string judgementTotalCountFieldField;
    
    private string majorProductFieldField;
    
    private string messageFieldField;
    
    private string notarialBond24MonthsPlusCountFieldField;
    
    private string notarialBond24MonthsPlusTotalFieldField;
    
    private string notarialBondLast12MonthsCountFieldField;
    
    private string notarialBondLast12MonthsTotalFieldField;
    
    private string notarialBondLast24MonthsCountFieldField;
    
    private string notarialBondLast24MonthsTotalFieldField;
    
    private string notarialBondLast6MonthsCountFieldField;
    
    private string notarialBondLast6MonthsTotalFieldField;
    
    private string notarialBondTotalAmountFieldField;
    
    private string notarialBondTotalCountFieldField;
    
    /// <remarks/>
    
    public string adverse24MonthsPlusCountField {
        get {
            return this.adverse24MonthsPlusCountFieldField;
        }
        set {
            this.adverse24MonthsPlusCountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string adverse24MonthsPlusTotalField {
        get {
            return this.adverse24MonthsPlusTotalFieldField;
        }
        set {
            this.adverse24MonthsPlusTotalFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string adverseLast12MonthsCountField {
        get {
            return this.adverseLast12MonthsCountFieldField;
        }
        set {
            this.adverseLast12MonthsCountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string adverseLast12MonthsTotalField {
        get {
            return this.adverseLast12MonthsTotalFieldField;
        }
        set {
            this.adverseLast12MonthsTotalFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string adverseLast24MonthsCountField {
        get {
            return this.adverseLast24MonthsCountFieldField;
        }
        set {
            this.adverseLast24MonthsCountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string adverseLast24MonthsTotalField {
        get {
            return this.adverseLast24MonthsTotalFieldField;
        }
        set {
            this.adverseLast24MonthsTotalFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string adverseLast6MonthsCountField {
        get {
            return this.adverseLast6MonthsCountFieldField;
        }
        set {
            this.adverseLast6MonthsCountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string adverseLast6MonthsTotalField {
        get {
            return this.adverseLast6MonthsTotalFieldField;
        }
        set {
            this.adverseLast6MonthsTotalFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string adverseTotalAmountField {
        get {
            return this.adverseTotalAmountFieldField;
        }
        set {
            this.adverseTotalAmountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string adverseTotalCountField {
        get {
            return this.adverseTotalCountFieldField;
        }
        set {
            this.adverseTotalCountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string judgement24MonthsPlusCountField {
        get {
            return this.judgement24MonthsPlusCountFieldField;
        }
        set {
            this.judgement24MonthsPlusCountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string judgement24MonthsPlusTotalField {
        get {
            return this.judgement24MonthsPlusTotalFieldField;
        }
        set {
            this.judgement24MonthsPlusTotalFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string judgementLast12MonthsCountField {
        get {
            return this.judgementLast12MonthsCountFieldField;
        }
        set {
            this.judgementLast12MonthsCountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string judgementLast12MonthsTotalField {
        get {
            return this.judgementLast12MonthsTotalFieldField;
        }
        set {
            this.judgementLast12MonthsTotalFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string judgementLast24MonthsCountField {
        get {
            return this.judgementLast24MonthsCountFieldField;
        }
        set {
            this.judgementLast24MonthsCountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string judgementLast24MonthsTotalField {
        get {
            return this.judgementLast24MonthsTotalFieldField;
        }
        set {
            this.judgementLast24MonthsTotalFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string judgementLast6MonthsCountField {
        get {
            return this.judgementLast6MonthsCountFieldField;
        }
        set {
            this.judgementLast6MonthsCountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string judgementLast6MonthsTotalField {
        get {
            return this.judgementLast6MonthsTotalFieldField;
        }
        set {
            this.judgementLast6MonthsTotalFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string judgementTotalAmountField {
        get {
            return this.judgementTotalAmountFieldField;
        }
        set {
            this.judgementTotalAmountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string judgementTotalCountField {
        get {
            return this.judgementTotalCountFieldField;
        }
        set {
            this.judgementTotalCountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string messageField {
        get {
            return this.messageFieldField;
        }
        set {
            this.messageFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string notarialBond24MonthsPlusCountField {
        get {
            return this.notarialBond24MonthsPlusCountFieldField;
        }
        set {
            this.notarialBond24MonthsPlusCountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string notarialBond24MonthsPlusTotalField {
        get {
            return this.notarialBond24MonthsPlusTotalFieldField;
        }
        set {
            this.notarialBond24MonthsPlusTotalFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string notarialBondLast12MonthsCountField {
        get {
            return this.notarialBondLast12MonthsCountFieldField;
        }
        set {
            this.notarialBondLast12MonthsCountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string notarialBondLast12MonthsTotalField {
        get {
            return this.notarialBondLast12MonthsTotalFieldField;
        }
        set {
            this.notarialBondLast12MonthsTotalFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string notarialBondLast24MonthsCountField {
        get {
            return this.notarialBondLast24MonthsCountFieldField;
        }
        set {
            this.notarialBondLast24MonthsCountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string notarialBondLast24MonthsTotalField {
        get {
            return this.notarialBondLast24MonthsTotalFieldField;
        }
        set {
            this.notarialBondLast24MonthsTotalFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string notarialBondLast6MonthsCountField {
        get {
            return this.notarialBondLast6MonthsCountFieldField;
        }
        set {
            this.notarialBondLast6MonthsCountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string notarialBondLast6MonthsTotalField {
        get {
            return this.notarialBondLast6MonthsTotalFieldField;
        }
        set {
            this.notarialBondLast6MonthsTotalFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string notarialBondTotalAmountField {
        get {
            return this.notarialBondTotalAmountFieldField;
        }
        set {
            this.notarialBondTotalAmountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string notarialBondTotalCountField {
        get {
            return this.notarialBondTotalCountFieldField;
        }
        set {
            this.notarialBondTotalCountFieldField = value;
        }
    }
}

/// <remarks/>





public partial class Branch : BureauSegment {
    
    private string dunsNoFieldField;
    
    private string emailFieldField;
    
    private string faxNumberFieldField;
    
    private string majorProductFieldField;
    
    private string phoneNumberFieldField;
    
    private string physicalAddressFieldField;
    
    private string physicalCountryFieldField;
    
    private string physicalPostCodeFieldField;
    
    private string physicalSuburbFieldField;
    
    private string physicalTownFieldField;
    
    private string postBoxFieldField;
    
    private string sGIndexFieldField;
    
    private string websiteFieldField;
    
    /// <remarks/>
    
    public string dunsNoField {
        get {
            return this.dunsNoFieldField;
        }
        set {
            this.dunsNoFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string emailField {
        get {
            return this.emailFieldField;
        }
        set {
            this.emailFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string faxNumberField {
        get {
            return this.faxNumberFieldField;
        }
        set {
            this.faxNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string phoneNumberField {
        get {
            return this.phoneNumberFieldField;
        }
        set {
            this.phoneNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string physicalAddressField {
        get {
            return this.physicalAddressFieldField;
        }
        set {
            this.physicalAddressFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string physicalCountryField {
        get {
            return this.physicalCountryFieldField;
        }
        set {
            this.physicalCountryFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string physicalPostCodeField {
        get {
            return this.physicalPostCodeFieldField;
        }
        set {
            this.physicalPostCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string physicalSuburbField {
        get {
            return this.physicalSuburbFieldField;
        }
        set {
            this.physicalSuburbFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string physicalTownField {
        get {
            return this.physicalTownFieldField;
        }
        set {
            this.physicalTownFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string postBoxField {
        get {
            return this.postBoxFieldField;
        }
        set {
            this.postBoxFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string sGIndexField {
        get {
            return this.sGIndexFieldField;
        }
        set {
            this.sGIndexFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string websiteField {
        get {
            return this.websiteFieldField;
        }
        set {
            this.websiteFieldField = value;
        }
    }
}

/// <remarks/>





public partial class BankingDetailSummary : BureauSegment {
    
    private string accountNumberFieldField;
    
    private string amountFieldField;
    
    private string bankBranchFieldField;
    
    private string bankCodeDescriptionFieldField;
    
    private string bankCodeFieldField;
    
    private string bankNameFieldField;
    
    private string dateFieldField;
    
    private string majorProductFieldField;
    
    private string startDateFieldField;
    
    private string termsFieldField;
    
    /// <remarks/>
    
    public string accountNumberField {
        get {
            return this.accountNumberFieldField;
        }
        set {
            this.accountNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string amountField {
        get {
            return this.amountFieldField;
        }
        set {
            this.amountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bankBranchField {
        get {
            return this.bankBranchFieldField;
        }
        set {
            this.bankBranchFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bankCodeDescriptionField {
        get {
            return this.bankCodeDescriptionFieldField;
        }
        set {
            this.bankCodeDescriptionFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bankCodeField {
        get {
            return this.bankCodeFieldField;
        }
        set {
            this.bankCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bankNameField {
        get {
            return this.bankNameFieldField;
        }
        set {
            this.bankNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateField {
        get {
            return this.dateFieldField;
        }
        set {
            this.dateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string startDateField {
        get {
            return this.startDateFieldField;
        }
        set {
            this.startDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string termsField {
        get {
            return this.termsFieldField;
        }
        set {
            this.termsFieldField = value;
        }
    }
}

/// <remarks/>





public partial class BankReport : BureauSegment {
    
    private string accHolderFieldField;
    
    private string accountNoFieldField;
    
    private string amountFieldField;
    
    private string bankCodeDescFieldField;
    
    private string bankCodeFieldField;
    
    private string bankNameFieldField;
    
    private string branchFieldField;
    
    private string[] commentFieldField;
    
    private string dateFieldField;
    
    private string majorProductFieldField;
    
    private string startDateFieldField;
    
    private string termsFieldField;
    
    /// <remarks/>
    
    public string accHolderField {
        get {
            return this.accHolderFieldField;
        }
        set {
            this.accHolderFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string accountNoField {
        get {
            return this.accountNoFieldField;
        }
        set {
            this.accountNoFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string amountField {
        get {
            return this.amountFieldField;
        }
        set {
            this.amountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bankCodeDescField {
        get {
            return this.bankCodeDescFieldField;
        }
        set {
            this.bankCodeDescFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bankCodeField {
        get {
            return this.bankCodeFieldField;
        }
        set {
            this.bankCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bankNameField {
        get {
            return this.bankNameFieldField;
        }
        set {
            this.bankNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string branchField {
        get {
            return this.branchFieldField;
        }
        set {
            this.branchFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] commentField {
        get {
            return this.commentFieldField;
        }
        set {
            this.commentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateField {
        get {
            return this.dateFieldField;
        }
        set {
            this.dateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string startDateField {
        get {
            return this.startDateFieldField;
        }
        set {
            this.startDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string termsField {
        get {
            return this.termsFieldField;
        }
        set {
            this.termsFieldField = value;
        }
    }
}

/// <remarks/>





public partial class BankHistory : BureauSegment {
    
    private string accountNoFieldField;
    
    private string amountFieldField;
    
    private string bankCodeDescFieldField;
    
    private string bankCodeFieldField;
    
    private string bankNameFieldField;
    
    private string branchFieldField;
    
    private string[] commentFieldField;
    
    private string dateFieldField;
    
    private string majorProductFieldField;
    
    private string startDateFieldField;
    
    private string termsFieldField;
    
    /// <remarks/>
    
    public string accountNoField {
        get {
            return this.accountNoFieldField;
        }
        set {
            this.accountNoFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string amountField {
        get {
            return this.amountFieldField;
        }
        set {
            this.amountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bankCodeDescField {
        get {
            return this.bankCodeDescFieldField;
        }
        set {
            this.bankCodeDescFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bankCodeField {
        get {
            return this.bankCodeFieldField;
        }
        set {
            this.bankCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bankNameField {
        get {
            return this.bankNameFieldField;
        }
        set {
            this.bankNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string branchField {
        get {
            return this.branchFieldField;
        }
        set {
            this.branchFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] commentField {
        get {
            return this.commentFieldField;
        }
        set {
            this.commentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateField {
        get {
            return this.dateFieldField;
        }
        set {
            this.dateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string startDateField {
        get {
            return this.startDateFieldField;
        }
        set {
            this.startDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string termsField {
        get {
            return this.termsFieldField;
        }
        set {
            this.termsFieldField = value;
        }
    }
}

/// <remarks/>





public partial class BankCodes : BureauSegment {
    
    private string accountNoFieldField;
    
    private string amountFieldField;
    
    private string bankCodeDescFieldField;
    
    private string bankCodeFieldField;
    
    private string bankNameFieldField;
    
    private string branchFieldField;
    
    private string[] commentFieldField;
    
    private string dateFieldField;
    
    private string majorProductFieldField;
    
    private string startDateFieldField;
    
    private string termsFieldField;
    
    /// <remarks/>
    
    public string accountNoField {
        get {
            return this.accountNoFieldField;
        }
        set {
            this.accountNoFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string amountField {
        get {
            return this.amountFieldField;
        }
        set {
            this.amountFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bankCodeDescField {
        get {
            return this.bankCodeDescFieldField;
        }
        set {
            this.bankCodeDescFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bankCodeField {
        get {
            return this.bankCodeFieldField;
        }
        set {
            this.bankCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bankNameField {
        get {
            return this.bankNameFieldField;
        }
        set {
            this.bankNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string branchField {
        get {
            return this.branchFieldField;
        }
        set {
            this.branchFieldField = value;
        }
    }
    
    /// <remarks/>
    
    // [System.Xml.Serialization.XmlArrayItemAttribute(Namespace="http://schemas.microsoft.com/2003/10/Serialization/Arrays")]
    public string[] commentField {
        get {
            return this.commentFieldField;
        }
        set {
            this.commentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dateField {
        get {
            return this.dateFieldField;
        }
        set {
            this.dateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string startDateField {
        get {
            return this.startDateFieldField;
        }
        set {
            this.startDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string termsField {
        get {
            return this.termsFieldField;
        }
        set {
            this.termsFieldField = value;
        }
    }
}

/// <remarks/>





public partial class AggregateCW : BureauSegment {
    
    private Aggregate[] aggregatesFieldField;
    
    private string majorProductFieldField;
    
    /// <remarks/>
    
    public Aggregate[] aggregatesField {
        get {
            return this.aggregatesFieldField;
        }
        set {
            this.aggregatesFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
}

/// <remarks/>





public partial class Affiliations : BureauSegment {
    
    private string commentFieldField;
    
    private string dunsNumberFieldField;
    
    private string endDateFieldField;
    
    private string iTNumberFieldField;
    
    private string infoDateFieldField;
    
    private string majorProductFieldField;
    
    private string objectCountryFieldField;
    
    private string objectNameFieldField;
    
    private string objectTownFieldField;
    
    private string percentShareFieldField;
    
    private string registrationNumberFieldField;
    
    private string relationshipTypeFieldField;
    
    private string startDateFieldField;
    
    /// <remarks/>
    
    public string commentField {
        get {
            return this.commentFieldField;
        }
        set {
            this.commentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string dunsNumberField {
        get {
            return this.dunsNumberFieldField;
        }
        set {
            this.dunsNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string endDateField {
        get {
            return this.endDateFieldField;
        }
        set {
            this.endDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string iTNumberField {
        get {
            return this.iTNumberFieldField;
        }
        set {
            this.iTNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string infoDateField {
        get {
            return this.infoDateFieldField;
        }
        set {
            this.infoDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string objectCountryField {
        get {
            return this.objectCountryFieldField;
        }
        set {
            this.objectCountryFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string objectNameField {
        get {
            return this.objectNameFieldField;
        }
        set {
            this.objectNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string objectTownField {
        get {
            return this.objectTownFieldField;
        }
        set {
            this.objectTownFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string percentShareField {
        get {
            return this.percentShareFieldField;
        }
        set {
            this.percentShareFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string registrationNumberField {
        get {
            return this.registrationNumberFieldField;
        }
        set {
            this.registrationNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string relationshipTypeField {
        get {
            return this.relationshipTypeFieldField;
        }
        set {
            this.relationshipTypeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string startDateField {
        get {
            return this.startDateFieldField;
        }
        set {
            this.startDateFieldField = value;
        }
    }
}

/// <remarks/>





public partial class AccountVerificationVH : BureauSegment {
    
    private string accountAcceptsCreditsFieldField;
    
    private string accountAcceptsDebitsFieldField;
    
    private string accountDormantFieldField;
    
    private string accountHolderFieldField;
    
    private string accountNumberFieldField;
    
    private string accountOpen3MonthsFieldField;
    
    private string accountTypeFieldField;
    
    private string bankNameFieldField;
    
    private string branchCodeFieldField;
    
    private string branchNameFieldField;
    
    private string majorProductFieldField;
    
    private string startDateFieldField;
    
    private string verifiedDatedFieldField;
    
    /// <remarks/>
    
    public string accountAcceptsCreditsField {
        get {
            return this.accountAcceptsCreditsFieldField;
        }
        set {
            this.accountAcceptsCreditsFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string accountAcceptsDebitsField {
        get {
            return this.accountAcceptsDebitsFieldField;
        }
        set {
            this.accountAcceptsDebitsFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string accountDormantField {
        get {
            return this.accountDormantFieldField;
        }
        set {
            this.accountDormantFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string accountHolderField {
        get {
            return this.accountHolderFieldField;
        }
        set {
            this.accountHolderFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string accountNumberField {
        get {
            return this.accountNumberFieldField;
        }
        set {
            this.accountNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string accountOpen3MonthsField {
        get {
            return this.accountOpen3MonthsFieldField;
        }
        set {
            this.accountOpen3MonthsFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string accountTypeField {
        get {
            return this.accountTypeFieldField;
        }
        set {
            this.accountTypeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bankNameField {
        get {
            return this.bankNameFieldField;
        }
        set {
            this.bankNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string branchCodeField {
        get {
            return this.branchCodeFieldField;
        }
        set {
            this.branchCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string branchNameField {
        get {
            return this.branchNameFieldField;
        }
        set {
            this.branchNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string startDateField {
        get {
            return this.startDateFieldField;
        }
        set {
            this.startDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string verifiedDatedField {
        get {
            return this.verifiedDatedFieldField;
        }
        set {
            this.verifiedDatedFieldField = value;
        }
    }
}

/// <remarks/>





public partial class AccountVerificationVA : BureauSegment {
    
    private string accountAcceptsCreditsFieldField;
    
    private string accountAcceptsDebitsFieldField;
    
    private string accountDormantFieldField;
    
    private string accountFoundFieldField;
    
    private string accountHolderFieldField;
    
    private string accountNumberFieldField;
    
    private string accountOpen3MonthsFieldField;
    
    private string accountOpenFieldField;
    
    private string accountTypeFieldField;
    
    private string bankNameFieldField;
    
    private string branchCodeFieldField;
    
    private string branchNameFieldField;
    
    private string commentFieldField;
    
    private string errorReasonFieldField;
    
    private string iDMatchFieldField;
    
    private string majorProductFieldField;
    
    private string startDateFieldField;
    
    private string surnameMatchFieldField;
    
    private string verifiedDatedFieldField;
    
    private string verifiedFieldField;
    
    /// <remarks/>
    
    public string accountAcceptsCreditsField {
        get {
            return this.accountAcceptsCreditsFieldField;
        }
        set {
            this.accountAcceptsCreditsFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string accountAcceptsDebitsField {
        get {
            return this.accountAcceptsDebitsFieldField;
        }
        set {
            this.accountAcceptsDebitsFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string accountDormantField {
        get {
            return this.accountDormantFieldField;
        }
        set {
            this.accountDormantFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string accountFoundField {
        get {
            return this.accountFoundFieldField;
        }
        set {
            this.accountFoundFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string accountHolderField {
        get {
            return this.accountHolderFieldField;
        }
        set {
            this.accountHolderFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string accountNumberField {
        get {
            return this.accountNumberFieldField;
        }
        set {
            this.accountNumberFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string accountOpen3MonthsField {
        get {
            return this.accountOpen3MonthsFieldField;
        }
        set {
            this.accountOpen3MonthsFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string accountOpenField {
        get {
            return this.accountOpenFieldField;
        }
        set {
            this.accountOpenFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string accountTypeField {
        get {
            return this.accountTypeFieldField;
        }
        set {
            this.accountTypeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string bankNameField {
        get {
            return this.bankNameFieldField;
        }
        set {
            this.bankNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string branchCodeField {
        get {
            return this.branchCodeFieldField;
        }
        set {
            this.branchCodeFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string branchNameField {
        get {
            return this.branchNameFieldField;
        }
        set {
            this.branchNameFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string commentField {
        get {
            return this.commentFieldField;
        }
        set {
            this.commentFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string errorReasonField {
        get {
            return this.errorReasonFieldField;
        }
        set {
            this.errorReasonFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string iDMatchField {
        get {
            return this.iDMatchFieldField;
        }
        set {
            this.iDMatchFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string majorProductField {
        get {
            return this.majorProductFieldField;
        }
        set {
            this.majorProductFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string startDateField {
        get {
            return this.startDateFieldField;
        }
        set {
            this.startDateFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string surnameMatchField {
        get {
            return this.surnameMatchFieldField;
        }
        set {
            this.surnameMatchFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string verifiedDatedField {
        get {
            return this.verifiedDatedFieldField;
        }
        set {
            this.verifiedDatedFieldField = value;
        }
    }
    
    /// <remarks/>
    
    public string verifiedField {
        get {
            return this.verifiedFieldField;
        }
        set {
            this.verifiedFieldField = value;
        }
    }
}


// public class BCCB701: BureauSegment {}
//public class DeedsMultipleOwners: BureauSegment {}
//public class DeedsMultipleOwnersDetail: BureauSegment {}
//public class ForensicLinkagesHeaderFL: BureauSegment {}
//public class ForensicLinkagesHomeTelephoneFH: BureauSegment {}
//public class ForensicLinkagesWorkTelephoneFW: BureauSegment {}
//public class ForensicLinkagesCellphoneNumberFX: BureauSegment {}
//public class ForensicLinkagesDefaultsAssociates: BureauSegment {}
//public class ForensicLinkagesDefaultsFF: BureauSegment {}
//public class ForensicLinkagesHomeAddressFZ: BureauSegment {}
//public class ForensicLinkagesJudgementsAssociates: BureauSegment {}
//public class ForensicLinkagesJudgementsFJ: BureauSegment {}
//public class HistoricContactDetailsHD: BureauSegment {}
//public class IndividualTrace06: BureauSegment {}
//public class LinkedBusinessHeaders: BureauSegment {}
//public class LinkagesLK: BureauSegment {}
//public class NLRCounterSegmentMC01: BureauSegment {}
//public class NLRCounterSegmentMC50: BureauSegment {}
//public class NLRSummaryMY50: BureauSegment {}
//public class PrincipalDeedsCA: BureauSegment {}
//public class ScorecardS701: BureauSegment {}
//public class AddressAlertRA: BureauSegment {}
//public class AllRiskTypesAR: BureauSegment {}
//public class ClaimantHeaderCH: BureauSegment {}
//public class ClaimantHeaderHC: BureauSegment {}
//public class ClaimantHeaderSH: BureauSegment {}
//public class ClaimDetailsCR: BureauSegment {}
//public class ClaimsLinkedCL: BureauSegment {}
//public class ClaimantSummaryCS: BureauSegment {}
//public class ClaimsCommentSegmentSX: BureauSegment {}
//public class DriverDetailsDR: BureauSegment {}
//public class FraudAlertS6: BureauSegment {}
//public class HawkAlertHA: BureauSegment {}
//public class HomeOwnersAO: BureauSegment {}
//public class IdentityVerficationAlertHI: BureauSegment {}
//public class InsuredDetailsIN: BureauSegment {}
//public class PersonalLiabiltyPL: BureauSegment {}
//public class ServiceProviderSP: BureauSegment {}
//public class ThirdPartyTP: BureauSegment {}
//public class VehicleAlertVA: BureauSegment {}
//public class VehicleDetailsVH: BureauSegment {}
//public class NatisEnquiryHistoryUS: BureauSegment {}
//public class NatisInputVerificationUT: BureauSegment {}
//public class NatisDetailedVehicleInformationUU: BureauSegment {}
//public class DriversLicenseInformationUX: BureauSegment {}
//public class InternalMessageIM: BureauSegment {}
//public class IndivualOwnOtherActivePoliciesPI: BureauSegment {}
//public class IndivualOwnOtherClaimDetails0to3yearsI1: BureauSegment {}
//public class IndivualOwnOtherClaimDetails4to7yearsSI: BureauSegment {}
//public class IndivualOwnOtherInactivePoliciesP1: BureauSegment {}
//public class InputIdentificationIndividualII: BureauSegment {}
//public class InputIdentificationRiskAddressIA: BureauSegment {}
//public class InputIdentificationVehicleIV: BureauSegment {}
//public class InsuranceEnquirySE: BureauSegment {}
//public class PolicySummaryPS: BureauSegment {}
//public class PolicyCommentSegmentSZ: BureauSegment {}
//public class RiskAddressOwnOtherActivePoliciesPA: BureauSegment {}
//public class RiskAddressOwnOtherClaimDetails0to3yearsI3: BureauSegment {}
//public class RiskAddressOwnOtherClaimDetails4to7yearsSR: BureauSegment {}
//public class RiskAddressOwnOtherInactivePoliciesP3: BureauSegment {}
//public class VehicleOwnOtherActivePoliciesPV: BureauSegment {}
//public class VehicleOwnOtherClaimDetails0to3yearsI2: BureauSegment {}
//public class VehicleOwnOtherClaimDetails4to7yearsSV: BureauSegment {}
//public class VehicleOwnOtherInactivePoliciesP2: BureauSegment {}
//public class VehicleInformationUR: BureauSegment {}
//public class VehicleOwnership_TitleHolderInformationUW: BureauSegment {}
//public class TUVehicleDetailsVL: BureauSegment {}
//public class TUVehicleFinanceDetailsVB: BureauSegment {}
//public class TUVehiclePreviousRegistrationDetailsVC: BureauSegment {}
//public class TUVehicleMileageRecordedVD: BureauSegment {}
//public class TUVehicleEnquiryHistoryVE: BureauSegment {}
//public class TUVehicleEstimateRecordedVF: BureauSegment {}
//public class TUVehicleAlertRecordedVG: BureauSegment {}
//public class TUVehicleStolenRecordedVM: BureauSegment {}
//public class TUVehicleRegistrationAuthorityHistoryRecordedVI: BureauSegment {}
//public class TUVehicleInsuranceClaimsRecordedVJ: BureauSegment {}
//public class TUVehicleInsurancePoliciesRecordedVK: BureauSegment {}
//public class TUInputVerificationUV: BureauSegment {}



/// <remarks/>
/// 
public partial class PrincipalLinks
{

    private PrincipalLink[] principalLinksField;

    [System.Xml.Serialization.XmlElement("PrincipalLink")]
    /// <remarks/>
    public PrincipalLink[] PrincipalLink
    {
        get
        {
            return this.principalLinksField;
        }
        set
        {
            this.principalLinksField = value;
        }
    }
}



public partial class CommercialResponse
{

    private AccountVerificationVA[] accountVerificationVAFieldField;

    private AccountVerificationVH[] accountVerificationVHFieldField;

    private Affiliations[] affiliationsFieldField;

    private AggregateCW aggregateCWFieldField;

    private BankCodes[] bankCodesFieldField;

    private BankHistory[] bankHistoryFieldField;

    private BankReport[] bankReportFieldField;

    private BankingDetailSummary[] bankingDetailsSummaryFieldField;

    private Branch[] branchFieldField;

    private BusinessAdverseSummary businessAdverseSummaryFieldField;

    private BusinessDeedsComprehensivePB[] businessDeedsComprehensivePBFieldField;

    private BusinessDeedsComprehensivePW[] businessDeedsComprehensivePWFieldField;

    private BusinessDeedsDI[] businessDeedsDIFieldField;

    private BusinessDeedsSummaryDA[] businessDeedsSummaryDAFieldField;

    private BusinessNotarialBonds[] businessNotarialBondsFieldField;

    private BusinessPlusPrincipalSummary[] businessPlusPrincipalsSummaryFieldField;

    private BusinessRescueDetail businessRescueDetailFieldField;

    private BusinessBenchmarkBB[] businessbenchmarkFieldField;

    private CancelledTicket[] cancelledTicketsFieldField;

    private CapitalEmployed capitalEmployedFieldField;

    private CommercialDisputeResponse commercialDisputeResponseFieldField;

    private ComplianceIndicatorLI[] complianceIndicatorLIFieldField;

    private ConsumerBusinessEnquiry[] consumerBusinessEnquiriesFieldField;

    private ConsumerDefault[] consumerDefaultsFieldField;

    private ConsumerEnquiry[] consumerEnquiriesFieldField;

    private ConsumerHeaderC1[] consumerHeaderC1FieldField;

    private ConsumerHeader[] consumerHeaderFieldField;

    private ConsumerInfoNO04[] consumerInfoNO04FieldField;

    private ConsumerJudgement[] consumerJudgementsFieldField;

    private ConsumerNotarialBonds[] consumerNotarialBondsFieldField;

    private ConsumerNotice[] consumerNoticesFieldField;

    private ConsumerTraceAlert[] consumerTraceAlertsFieldField;

    private CourtRecord[] courtRecordsFieldField;

    private CurrentAsset currentAssetFieldField;

    private CurrentLiabilities currentLiabilitiesFieldField;

    private DeedHistory[] deedHistoryFieldField;

    private DeedsMultipleBond[] deedsMultipleBondFieldField;

    private Default[] defaultsFieldField;

    private DynamicRating dynamicRatingFieldField;

    private EmpOfCapital empOfCapitalFieldField;

    private EmpiricaE1[] empiricaE1FieldField;

    private EmpiricaEM04 empiricaEM04FieldField;

    private EnquiryHistory[] enquiryHistoryFieldField;

    private EnquirySummary enquirySummaryFieldField;

    private string errorCodeFieldField;

    private string errorMessageFieldField;

    private FinanceDataFE[] financeDataFEFieldField;

    private FinanceDataFF[] financeDataFFFieldField;

    private FinanceData[] financeDataFieldField;

    private FinanceHeader financeHeaderFieldField;

    private FinancialRatios[] financialRatiosFieldField;

    private FirstResponse firstResponseFieldField;

    private GeneralBankingInfo[] generalBankingInfoFieldField;

    private Header headerFieldField;

    private LinkedBusinessHeader[] linkedBusinessHeadersFieldField;

    private MailboxRetrieveList mailboxRetrieveListFieldField;

    private ModuleAvailabilityResponse moduleAvailabilityResponseFieldField;

    private Names namesFieldField;

    private NotarialBond[] notarialBondsFieldField;

    private ObservationCont[] observationContFieldField;

    private Observation[] observationsFieldField;

    private Operation[] operationsFieldField;

    private AdditionalOperations[] operationsHeaderFieldField;

    private OtherOperation[] otherOperationsFieldField;

    private PrincipalArchive[] principalArchivesFieldField;

    private PrincipalArchiveP5[] principalArchivesP5FieldField;

    private PrincipalClearance[] principalClearancesFieldField;

    private PrincipalDeedsComprehensiveCA[] principalDeedsCAFieldField;

    private PrincipalDeedsComprehensiveCV[] principalDeedsComprehensiveCVFieldField;

    private PrincipalDeedsSummaryCO[] principalDeedsSummaryCOFieldField;

    private PrincipalDeedsSummaryP8[] principalDeedsSummaryP8FieldField;

    private PrincipalFirstResponse principalFirstResponseFieldField;

    private PrincipalIDV[] principalIDVsFieldField;

   // private PrincipalLink[] principalLinksFieldField;
    private PrincipalLinks principalLinksFieldField;

    private PrincipalNotarialBonds[] principalNotarialBondsFieldField;

    private PrincipalDetailEmpirica[] principalsDetailEmpiricaFieldField;

    private Principal1[] principalsFieldField;

    private System.DateTime processingStartDateFieldField;

    private double processingTimeSecsFieldField;

    private string rawDataFieldField;

    private RegisteredPrincipal[] registeredPrincipalsFieldField;

    private RegistrationDetailsExtended registrationDetailsExtendedFieldField;

    private RegistrationDetails registrationDetailsFieldField;

    private ResponseStatus responseStatusFieldField;

    private SMEAssessment sMEAssessmentFieldField;

    private SearchResponse[] searchResponseFieldField;

    private SegmentDescriptions[] segmentDescriptionsFieldField;

    private TicketStatus[] ticketsStatusFieldField;

    private TradeAccountSynopsis[] tradeAccountSynopsisFieldField;

    private TradeAgeAnalysis[] tradeAgeAnalysisFieldField;

    private TradeHistory[] tradeHistoryFieldField;

    private TradePaymentHistory[] tradePaymentHistoryFieldField;

    private TradeReferenceSummary tradeReferenceSummaryFieldField;

    private TradeReference[] tradeReferencesFieldField;

    private Tradex[] tradexFieldField;

    private string uniqueRefGuidFieldField;

    private UnmatchedCourtRecord[] unmatchedCourtRecordsFieldField;

    private UnmatchedDefault[] unmatchedDefaultsFieldField;

    private UnmatchedNotarialBond[] unmatchedNotarialBondsFieldField;

    private Vehicles[] vehiclesFieldField;

    private VeriCheque[] veriChequesFieldField;

    /// <remarks/>

    public AccountVerificationVA[] accountVerificationVAField
    {
        get
        {
            return this.accountVerificationVAFieldField;
        }
        set
        {
            this.accountVerificationVAFieldField = value;
        }
    }

    /// <remarks/>

    public AccountVerificationVH[] accountVerificationVHField
    {
        get
        {
            return this.accountVerificationVHFieldField;
        }
        set
        {
            this.accountVerificationVHFieldField = value;
        }
    }

    /// <remarks/>

    public Affiliations[] affiliationsField
    {
        get
        {
            return this.affiliationsFieldField;
        }
        set
        {
            this.affiliationsFieldField = value;
        }
    }

    /// <remarks/>

    public AggregateCW aggregateCWField
    {
        get
        {
            return this.aggregateCWFieldField;
        }
        set
        {
            this.aggregateCWFieldField = value;
        }
    }

    /// <remarks/>

    public BankCodes[] bankCodesField
    {
        get
        {
            return this.bankCodesFieldField;
        }
        set
        {
            this.bankCodesFieldField = value;
        }
    }

    /// <remarks/>

    public BankHistory[] bankHistoryField
    {
        get
        {
            return this.bankHistoryFieldField;
        }
        set
        {
            this.bankHistoryFieldField = value;
        }
    }

    /// <remarks/>

    public BankReport[] bankReportField
    {
        get
        {
            return this.bankReportFieldField;
        }
        set
        {
            this.bankReportFieldField = value;
        }
    }

    /// <remarks/>

    public BankingDetailSummary[] bankingDetailsSummaryField
    {
        get
        {
            return this.bankingDetailsSummaryFieldField;
        }
        set
        {
            this.bankingDetailsSummaryFieldField = value;
        }
    }

    /// <remarks/>

    public Branch[] branchField
    {
        get
        {
            return this.branchFieldField;
        }
        set
        {
            this.branchFieldField = value;
        }
    }

    /// <remarks/>

    public BusinessAdverseSummary businessAdverseSummaryField
    {
        get
        {
            return this.businessAdverseSummaryFieldField;
        }
        set
        {
            this.businessAdverseSummaryFieldField = value;
        }
    }

    /// <remarks/>

    public BusinessDeedsComprehensivePB[] businessDeedsComprehensivePBField
    {
        get
        {
            return this.businessDeedsComprehensivePBFieldField;
        }
        set
        {
            this.businessDeedsComprehensivePBFieldField = value;
        }
    }

    /// <remarks/>

    public BusinessDeedsComprehensivePW[] businessDeedsComprehensivePWField
    {
        get
        {
            return this.businessDeedsComprehensivePWFieldField;
        }
        set
        {
            this.businessDeedsComprehensivePWFieldField = value;
        }
    }

    /// <remarks/>

    public BusinessDeedsDI[] businessDeedsDIField
    {
        get
        {
            return this.businessDeedsDIFieldField;
        }
        set
        {
            this.businessDeedsDIFieldField = value;
        }
    }

    /// <remarks/>

    public BusinessDeedsSummaryDA[] businessDeedsSummaryDAField
    {
        get
        {
            return this.businessDeedsSummaryDAFieldField;
        }
        set
        {
            this.businessDeedsSummaryDAFieldField = value;
        }
    }

    /// <remarks/>

    public BusinessNotarialBonds[] businessNotarialBondsField
    {
        get
        {
            return this.businessNotarialBondsFieldField;
        }
        set
        {
            this.businessNotarialBondsFieldField = value;
        }
    }

    /// <remarks/>

    public BusinessPlusPrincipalSummary[] businessPlusPrincipalsSummaryField
    {
        get
        {
            return this.businessPlusPrincipalsSummaryFieldField;
        }
        set
        {
            this.businessPlusPrincipalsSummaryFieldField = value;
        }
    }

    /// <remarks/>

    public BusinessRescueDetail businessRescueDetailField
    {
        get
        {
            return this.businessRescueDetailFieldField;
        }
        set
        {
            this.businessRescueDetailFieldField = value;
        }
    }

    /// <remarks/>

    public BusinessBenchmarkBB[] businessbenchmarkField
    {
        get
        {
            return this.businessbenchmarkFieldField;
        }
        set
        {
            this.businessbenchmarkFieldField = value;
        }
    }

    /// <remarks/>

    public CancelledTicket[] cancelledTicketsField
    {
        get
        {
            return this.cancelledTicketsFieldField;
        }
        set
        {
            this.cancelledTicketsFieldField = value;
        }
    }

    /// <remarks/>

    public CapitalEmployed capitalEmployedField
    {
        get
        {
            return this.capitalEmployedFieldField;
        }
        set
        {
            this.capitalEmployedFieldField = value;
        }
    }

    /// <remarks/>

    public CommercialDisputeResponse commercialDisputeResponseField
    {
        get
        {
            return this.commercialDisputeResponseFieldField;
        }
        set
        {
            this.commercialDisputeResponseFieldField = value;
        }
    }

    /// <remarks/>

    public ComplianceIndicatorLI[] complianceIndicatorLIField
    {
        get
        {
            return this.complianceIndicatorLIFieldField;
        }
        set
        {
            this.complianceIndicatorLIFieldField = value;
        }
    }

    /// <remarks/>

    public ConsumerBusinessEnquiry[] consumerBusinessEnquiriesField
    {
        get
        {
            return this.consumerBusinessEnquiriesFieldField;
        }
        set
        {
            this.consumerBusinessEnquiriesFieldField = value;
        }
    }

    /// <remarks/>

    public ConsumerDefault[] consumerDefaultsField
    {
        get
        {
            return this.consumerDefaultsFieldField;
        }
        set
        {
            this.consumerDefaultsFieldField = value;
        }
    }

    /// <remarks/>

    public ConsumerEnquiry[] consumerEnquiriesField
    {
        get
        {
            return this.consumerEnquiriesFieldField;
        }
        set
        {
            this.consumerEnquiriesFieldField = value;
        }
    }

    /// <remarks/>

    public ConsumerHeaderC1[] consumerHeaderC1Field
    {
        get
        {
            return this.consumerHeaderC1FieldField;
        }
        set
        {
            this.consumerHeaderC1FieldField = value;
        }
    }

    /// <remarks/>

    public ConsumerHeader[] consumerHeaderField
    {
        get
        {
            return this.consumerHeaderFieldField;
        }
        set
        {
            this.consumerHeaderFieldField = value;
        }
    }

    /// <remarks/>
      [XmlElement(Namespace = "http://www.web.xds.co.za/XDSConnectWS")]
    public ConsumerInfoNO04[] consumerInfoNO04Field
    {
        get
        {
            return this.consumerInfoNO04FieldField;
        }
        set
        {
            this.consumerInfoNO04FieldField = value;
        }
    }

    /// <remarks/>

    public ConsumerJudgement[] consumerJudgementsField
    {
        get
        {
            return this.consumerJudgementsFieldField;
        }
        set
        {
            this.consumerJudgementsFieldField = value;
        }
    }

    /// <remarks/>

    public ConsumerNotarialBonds[] consumerNotarialBondsField
    {
        get
        {
            return this.consumerNotarialBondsFieldField;
        }
        set
        {
            this.consumerNotarialBondsFieldField = value;
        }
    }

    /// <remarks/>

    public ConsumerNotice[] consumerNoticesField
    {
        get
        {
            return this.consumerNoticesFieldField;
        }
        set
        {
            this.consumerNoticesFieldField = value;
        }
    }

    /// <remarks/>

    public ConsumerTraceAlert[] consumerTraceAlertsField
    {
        get
        {
            return this.consumerTraceAlertsFieldField;
        }
        set
        {
            this.consumerTraceAlertsFieldField = value;
        }
    }

    /// <remarks/>

    public CourtRecord[] courtRecordsField
    {
        get
        {
            return this.courtRecordsFieldField;
        }
        set
        {
            this.courtRecordsFieldField = value;
        }
    }

    /// <remarks/>

    public CurrentAsset currentAssetField
    {
        get
        {
            return this.currentAssetFieldField;
        }
        set
        {
            this.currentAssetFieldField = value;
        }
    }

    /// <remarks/>

    public CurrentLiabilities currentLiabilitiesField
    {
        get
        {
            return this.currentLiabilitiesFieldField;
        }
        set
        {
            this.currentLiabilitiesFieldField = value;
        }
    }

    /// <remarks/>

    public DeedHistory[] deedHistoryField
    {
        get
        {
            return this.deedHistoryFieldField;
        }
        set
        {
            this.deedHistoryFieldField = value;
        }
    }

    /// <remarks/>

    public DeedsMultipleBond[] deedsMultipleBondField
    {
        get
        {
            return this.deedsMultipleBondFieldField;
        }
        set
        {
            this.deedsMultipleBondFieldField = value;
        }
    }

    /// <remarks/>

    public Default[] defaultsField
    {
        get
        {
            return this.defaultsFieldField;
        }
        set
        {
            this.defaultsFieldField = value;
        }
    }

    /// <remarks/>

    public DynamicRating dynamicRatingField
    {
        get
        {
            return this.dynamicRatingFieldField;
        }
        set
        {
            this.dynamicRatingFieldField = value;
        }
    }

    /// <remarks/>

    public EmpOfCapital empOfCapitalField
    {
        get
        {
            return this.empOfCapitalFieldField;
        }
        set
        {
            this.empOfCapitalFieldField = value;
        }
    }

    /// <remarks/>

    public EmpiricaE1[] empiricaE1Field
    {
        get
        {
            return this.empiricaE1FieldField;
        }
        set
        {
            this.empiricaE1FieldField = value;
        }
    }

    /// <remarks/>

    public EmpiricaEM04 empiricaEM04Field
    {
        get
        {
            return this.empiricaEM04FieldField;
        }
        set
        {
            this.empiricaEM04FieldField = value;
        }
    }

    /// <remarks/>

    public EnquiryHistory[] enquiryHistoryField
    {
        get
        {
            return this.enquiryHistoryFieldField;
        }
        set
        {
            this.enquiryHistoryFieldField = value;
        }
    }

    /// <remarks/>

    public EnquirySummary enquirySummaryField
    {
        get
        {
            return this.enquirySummaryFieldField;
        }
        set
        {
            this.enquirySummaryFieldField = value;
        }
    }

    /// <remarks/>

    public string errorCodeField
    {
        get
        {
            return this.errorCodeFieldField;
        }
        set
        {
            this.errorCodeFieldField = value;
        }
    }

    /// <remarks/>

    public string errorMessageField
    {
        get
        {
            return this.errorMessageFieldField;
        }
        set
        {
            this.errorMessageFieldField = value;
        }
    }

    /// <remarks/>

    public FinanceDataFE[] financeDataFEField
    {
        get
        {
            return this.financeDataFEFieldField;
        }
        set
        {
            this.financeDataFEFieldField = value;
        }
    }

    /// <remarks/>

    public FinanceDataFF[] financeDataFFField
    {
        get
        {
            return this.financeDataFFFieldField;
        }
        set
        {
            this.financeDataFFFieldField = value;
        }
    }

    /// <remarks/>

    public FinanceData[] financeDataField
    {
        get
        {
            return this.financeDataFieldField;
        }
        set
        {
            this.financeDataFieldField = value;
        }
    }

    /// <remarks/>

    public FinanceHeader financeHeaderField
    {
        get
        {
            return this.financeHeaderFieldField;
        }
        set
        {
            this.financeHeaderFieldField = value;
        }
    }

    /// <remarks/>

    public FinancialRatios[] financialRatiosField
    {
        get
        {
            return this.financialRatiosFieldField;
        }
        set
        {
            this.financialRatiosFieldField = value;
        }
    }

    /// <remarks/>

    public FirstResponse firstResponseField
    {
        get
        {
            return this.firstResponseFieldField;
        }
        set
        {
            this.firstResponseFieldField = value;
        }
    }

    /// <remarks/>

    public GeneralBankingInfo[] generalBankingInfoField
    {
        get
        {
            return this.generalBankingInfoFieldField;
        }
        set
        {
            this.generalBankingInfoFieldField = value;
        }
    }

    /// <remarks/>

    public Header headerField
    {
        get
        {
            return this.headerFieldField;
        }
        set
        {
            this.headerFieldField = value;
        }
    }

    /// <remarks/>

    public LinkedBusinessHeader[] linkedBusinessHeadersField
    {
        get
        {
            return this.linkedBusinessHeadersFieldField;
        }
        set
        {
            this.linkedBusinessHeadersFieldField = value;
        }
    }

    /// <remarks/>

    public MailboxRetrieveList mailboxRetrieveListField
    {
        get
        {
            return this.mailboxRetrieveListFieldField;
        }
        set
        {
            this.mailboxRetrieveListFieldField = value;
        }
    }

    /// <remarks/>

    public ModuleAvailabilityResponse moduleAvailabilityResponseField
    {
        get
        {
            return this.moduleAvailabilityResponseFieldField;
        }
        set
        {
            this.moduleAvailabilityResponseFieldField = value;
        }
    }

    /// <remarks/>

    public Names namesField
    {
        get
        {
            return this.namesFieldField;
        }
        set
        {
            this.namesFieldField = value;
        }
    }

    /// <remarks/>

    public NotarialBond[] notarialBondsField
    {
        get
        {
            return this.notarialBondsFieldField;
        }
        set
        {
            this.notarialBondsFieldField = value;
        }
    }

    /// <remarks/>

    public ObservationCont[] observationContField
    {
        get
        {
            return this.observationContFieldField;
        }
        set
        {
            this.observationContFieldField = value;
        }
    }

    /// <remarks/>

    public Observation[] observationsField
    {
        get
        {
            return this.observationsFieldField;
        }
        set
        {
            this.observationsFieldField = value;
        }
    }

    /// <remarks/>

    public Operation[] operationsField
    {
        get
        {
            return this.operationsFieldField;
        }
        set
        {
            this.operationsFieldField = value;
        }
    }

    /// <remarks/>

    public AdditionalOperations[] operationsHeaderField
    {
        get
        {
            return this.operationsHeaderFieldField;
        }
        set
        {
            this.operationsHeaderFieldField = value;
        }
    }

    /// <remarks/>

    public OtherOperation[] otherOperationsField
    {
        get
        {
            return this.otherOperationsFieldField;
        }
        set
        {
            this.otherOperationsFieldField = value;
        }
    }

    /// <remarks/>

    public PrincipalArchive[] principalArchivesField
    {
        get
        {
            return this.principalArchivesFieldField;
        }
        set
        {
            this.principalArchivesFieldField = value;
        }
    }

    /// <remarks/>

    public PrincipalArchiveP5[] principalArchivesP5Field
    {
        get
        {
            return this.principalArchivesP5FieldField;
        }
        set
        {
            this.principalArchivesP5FieldField = value;
        }
    }

    /// <remarks/>

    public PrincipalClearance[] principalClearancesField
    {
        get
        {
            return this.principalClearancesFieldField;
        }
        set
        {
            this.principalClearancesFieldField = value;
        }
    }

    /// <remarks/>

    public PrincipalDeedsComprehensiveCA[] principalDeedsCAField
    {
        get
        {
            return this.principalDeedsCAFieldField;
        }
        set
        {
            this.principalDeedsCAFieldField = value;
        }
    }

    /// <remarks/>

    public PrincipalDeedsComprehensiveCV[] principalDeedsComprehensiveCVField
    {
        get
        {
            return this.principalDeedsComprehensiveCVFieldField;
        }
        set
        {
            this.principalDeedsComprehensiveCVFieldField = value;
        }
    }

    /// <remarks/>

    public PrincipalDeedsSummaryCO[] principalDeedsSummaryCOField
    {
        get
        {
            return this.principalDeedsSummaryCOFieldField;
        }
        set
        {
            this.principalDeedsSummaryCOFieldField = value;
        }
    }

    /// <remarks/>

    public PrincipalDeedsSummaryP8[] principalDeedsSummaryP8Field
    {
        get
        {
            return this.principalDeedsSummaryP8FieldField;
        }
        set
        {
            this.principalDeedsSummaryP8FieldField = value;
        }
    }

    /// <remarks/>

    public PrincipalFirstResponse principalFirstResponseField
    {
        get
        {
            return this.principalFirstResponseFieldField;
        }
        set
        {
            this.principalFirstResponseFieldField = value;
        }
    }

    /// <remarks/>

    public PrincipalIDV[] principalIDVsField
    {
        get
        {
            return this.principalIDVsFieldField;
        }
        set
        {
            this.principalIDVsFieldField = value;
        }
    }

    /// <remarks/>
      [XmlElement(Namespace = "http://www.web.xds.co.za/XDSConnectWS")]
    public PrincipalLinks principalLinksField
    {
        get
        {
            return this.principalLinksFieldField;
        }
        set
        {
            this.principalLinksFieldField = value;
        }
    }

    /// <remarks/>

    public PrincipalNotarialBonds[] principalNotarialBondsField
    {
        get
        {
            return this.principalNotarialBondsFieldField;
        }
        set
        {
            this.principalNotarialBondsFieldField = value;
        }
    }

    /// <remarks/>

    public PrincipalDetailEmpirica[] principalsDetailEmpiricaField
    {
        get
        {
            return this.principalsDetailEmpiricaFieldField;
        }
        set
        {
            this.principalsDetailEmpiricaFieldField = value;
        }
    }

    /// <remarks/>

    public Principal1[] principalsField
    {
        get
        {
            return this.principalsFieldField;
        }
        set
        {
            this.principalsFieldField = value;
        }
    }

    /// <remarks/>
    public System.DateTime processingStartDateField
    {
        get
        {
            return this.processingStartDateFieldField;
        }
        set
        {
            this.processingStartDateFieldField = value;
        }
    }

    /// <remarks/>
    public double processingTimeSecsField
    {
        get
        {
            return this.processingTimeSecsFieldField;
        }
        set
        {
            this.processingTimeSecsFieldField = value;
        }
    }

    /// <remarks/>

    public string rawDataField
    {
        get
        {
            return this.rawDataFieldField;
        }
        set
        {
            this.rawDataFieldField = value;
        }
    }

    /// <remarks/>

    public RegisteredPrincipal[] registeredPrincipalsField
    {
        get
        {
            return this.registeredPrincipalsFieldField;
        }
        set
        {
            this.registeredPrincipalsFieldField = value;
        }
    }

    /// <remarks/>

    public RegistrationDetailsExtended registrationDetailsExtendedField
    {
        get
        {
            return this.registrationDetailsExtendedFieldField;
        }
        set
        {
            this.registrationDetailsExtendedFieldField = value;
        }
    }

    /// <remarks/>

    public RegistrationDetails registrationDetailsField
    {
        get
        {
            return this.registrationDetailsFieldField;
        }
        set
        {
            this.registrationDetailsFieldField = value;
        }
    }

    /// <remarks/>
    public ResponseStatus responseStatusField
    {
        get
        {
            return this.responseStatusFieldField;
        }
        set
        {
            this.responseStatusFieldField = value;
        }
    }

    /// <remarks/>

    public SMEAssessment sMEAssessmentField
    {
        get
        {
            return this.sMEAssessmentFieldField;
        }
        set
        {
            this.sMEAssessmentFieldField = value;
        }
    }

    /// <remarks/>

    public SearchResponse[] searchResponseField
    {
        get
        {
            return this.searchResponseFieldField;
        }
        set
        {
            this.searchResponseFieldField = value;
        }
    }

    /// <remarks/>

    public SegmentDescriptions[] segmentDescriptionsField
    {
        get
        {
            return this.segmentDescriptionsFieldField;
        }
        set
        {
            this.segmentDescriptionsFieldField = value;
        }
    }

    /// <remarks/>

    public TicketStatus[] ticketsStatusField
    {
        get
        {
            return this.ticketsStatusFieldField;
        }
        set
        {
            this.ticketsStatusFieldField = value;
        }
    }

    /// <remarks/>

    public TradeAccountSynopsis[] tradeAccountSynopsisField
    {
        get
        {
            return this.tradeAccountSynopsisFieldField;
        }
        set
        {
            this.tradeAccountSynopsisFieldField = value;
        }
    }

    /// <remarks/>

    public TradeAgeAnalysis[] tradeAgeAnalysisField
    {
        get
        {
            return this.tradeAgeAnalysisFieldField;
        }
        set
        {
            this.tradeAgeAnalysisFieldField = value;
        }
    }

    /// <remarks/>

    public TradeHistory[] tradeHistoryField
    {
        get
        {
            return this.tradeHistoryFieldField;
        }
        set
        {
            this.tradeHistoryFieldField = value;
        }
    }

    /// <remarks/>

    public TradePaymentHistory[] tradePaymentHistoryField
    {
        get
        {
            return this.tradePaymentHistoryFieldField;
        }
        set
        {
            this.tradePaymentHistoryFieldField = value;
        }
    }

    /// <remarks/>

    public TradeReferenceSummary tradeReferenceSummaryField
    {
        get
        {
            return this.tradeReferenceSummaryFieldField;
        }
        set
        {
            this.tradeReferenceSummaryFieldField = value;
        }
    }

    /// <remarks/>

    public TradeReference[] tradeReferencesField
    {
        get
        {
            return this.tradeReferencesFieldField;
        }
        set
        {
            this.tradeReferencesFieldField = value;
        }
    }

    /// <remarks/>

    public Tradex[] tradexField
    {
        get
        {
            return this.tradexFieldField;
        }
        set
        {
            this.tradexFieldField = value;
        }
    }

    /// <remarks/>
    public string uniqueRefGuidField
    {
        get
        {
            return this.uniqueRefGuidFieldField;
        }
        set
        {
            this.uniqueRefGuidFieldField = value;
        }
    }

    /// <remarks/>

    public UnmatchedCourtRecord[] unmatchedCourtRecordsField
    {
        get
        {
            return this.unmatchedCourtRecordsFieldField;
        }
        set
        {
            this.unmatchedCourtRecordsFieldField = value;
        }
    }

    /// <remarks/>

    public UnmatchedDefault[] unmatchedDefaultsField
    {
        get
        {
            return this.unmatchedDefaultsFieldField;
        }
        set
        {
            this.unmatchedDefaultsFieldField = value;
        }
    }

    /// <remarks/>

    public UnmatchedNotarialBond[] unmatchedNotarialBondsField
    {
        get
        {
            return this.unmatchedNotarialBondsFieldField;
        }
        set
        {
            this.unmatchedNotarialBondsFieldField = value;
        }
    }

    /// <remarks/>

    public Vehicles[] vehiclesField
    {
        get
        {
            return this.vehiclesFieldField;
        }
        set
        {
            this.vehiclesFieldField = value;
        }
    }

    /// <remarks/>

    public VeriCheque[] veriChequesField
    {
        get
        {
            return this.veriChequesFieldField;
        }
        set
        {
            this.veriChequesFieldField = value;
        }
    }


    //private BCCB701[] bccb701field;
    //private DeedsMultipleOwners[] deedsmultipleownersfield;
    //private DeedsMultipleOwnersDetail[] deedsmultipleownersdetailfield;
    //private ForensicLinkagesHeaderFL[] forensiclinkagesheaderflfield;
    //private ForensicLinkagesHomeTelephoneFH[] forensiclinkageshometelephonefhfield;
    //private ForensicLinkagesWorkTelephoneFW[] forensiclinkagesworktelephonefwfield;
    //private ForensicLinkagesCellphoneNumberFX[] forensiclinkagescellphonenumberfxfield;
    //private ForensicLinkagesDefaultsAssociates[] forensiclinkagesdefaultsassociatesfield;
    //private ForensicLinkagesDefaultsFF[] forensiclinkagesdefaultsfffield;
    //private ForensicLinkagesHomeAddressFZ[] forensiclinkageshomeaddressfzfield;
    //private ForensicLinkagesJudgementsAssociates[] forensiclinkagesjudgementsassociatesfield;
    //private ForensicLinkagesJudgementsFJ[] forensiclinkagesjudgementsfjfield;
    //private HistoricContactDetailsHD[] historiccontactdetailshdfield;
    //private IndividualTrace06[] individualtrace06field;
    //private LinkedBusinessHeaders[] linkedbusinessheadersfield;
    //private LinkagesLK[] linkageslkfield;
    //private NLRCounterSegmentMC01[] nlrcountersegmentmc01field;
    //private NLRCounterSegmentMC50[] nlrcountersegmentmc50field;
    //private NLRSummaryMY50[] nlrsummarymy50field;
    //private PrincipalDeedsCA[] principaldeedscafield;
    //private ScorecardS701[] scorecards701field;
    //private AddressAlertRA[] addressalertrafield;
    //private AllRiskTypesAR[] allrisktypesarfield;
    //private ClaimantHeaderCH[] claimantheaderchfield;
    //private ClaimantHeaderHC[] claimantheaderhcfield;
    //private ClaimantHeaderSH[] claimantheadershfield;
    //private ClaimDetailsCR[] claimdetailscrfield;
    //private ClaimsLinkedCL[] claimslinkedclfield;
    //private ClaimantSummaryCS[] claimantsummarycsfield;
    //private ClaimsCommentSegmentSX[] claimscommentsegmentsxfield;
    //private DriverDetailsDR[] driverdetailsdrfield;
    //private FraudAlertS6[] fraudalerts6field;
    //private HawkAlertHA[] hawkalerthafield;
    //private HomeOwnersAO[] homeownersaofield;
    //private IdentityVerficationAlertHI[] identityverficationalerthifield;
    //private InsuredDetailsIN[] insureddetailsinfield;
    //private PersonalLiabiltyPL[] personalliabiltyplfield;
    //private ServiceProviderSP[] serviceproviderspfield;
    //private ThirdPartyTP[] thirdpartytpfield;
    //private VehicleAlertVA[] vehiclealertvafield;
    //private VehicleDetailsVH[] vehicledetailsvhfield;
    //private NatisEnquiryHistoryUS[] natisenquiryhistoryusfield;
    //private NatisInputVerificationUT[] natisinputverificationutfield;
    //private NatisDetailedVehicleInformationUU[] natisdetailedvehicleinformationuufield;
    //private DriversLicenseInformationUX[] driverslicenseinformationuxfield;
    //private InternalMessageIM[] internalmessageimfield;
    //private IndivualOwnOtherActivePoliciesPI[] indivualownotheractivepoliciespifield;
    //private IndivualOwnOtherClaimDetails0to3yearsI1[] indivualownotherclaimdetails0to3yearsi1field;
    //private IndivualOwnOtherClaimDetails4to7yearsSI[] indivualownotherclaimdetails4to7yearssifield;
    //private IndivualOwnOtherInactivePoliciesP1[] indivualownotherinactivepoliciesp1field;
    //private InputIdentificationIndividualII[] inputidentificationindividualiifield;
    //private InputIdentificationRiskAddressIA[] inputidentificationriskaddressiafield;
    //private InputIdentificationVehicleIV[] inputidentificationvehicleivfield;
    //private InsuranceEnquirySE[] insuranceenquirysefield;
    //private PolicySummaryPS[] policysummarypsfield;
    //private PolicyCommentSegmentSZ[] policycommentsegmentszfield;
    //private RiskAddressOwnOtherActivePoliciesPA[] riskaddressownotheractivepoliciespafield;
    //private RiskAddressOwnOtherClaimDetails0to3yearsI3[] riskaddressownotherclaimdetails0to3yearsi3field;
    //private RiskAddressOwnOtherClaimDetails4to7yearsSR[] riskaddressownotherclaimdetails4to7yearssrfield;
    //private RiskAddressOwnOtherInactivePoliciesP3[] riskaddressownotherinactivepoliciesp3field;
    //private VehicleOwnOtherActivePoliciesPV[] vehicleownotheractivepoliciespvfield;
    //private VehicleOwnOtherClaimDetails0to3yearsI2[] vehicleownotherclaimdetails0to3yearsi2field;
    //private VehicleOwnOtherClaimDetails4to7yearsSV[] vehicleownotherclaimdetails4to7yearssvfield;
    //private VehicleOwnOtherInactivePoliciesP2[] vehicleownotherinactivepoliciesp2field;
    //private VehicleInformationUR[] vehicleinformationurfield;
    //private VehicleOwnership_TitleHolderInformationUW[] vehicleownership_titleholderinformationuwfield;
    //private TUVehicleDetailsVL[] tuvehicledetailsvlfield;
    //private TUVehicleFinanceDetailsVB[] tuvehiclefinancedetailsvbfield;
    //private TUVehiclePreviousRegistrationDetailsVC[] tuvehiclepreviousregistrationdetailsvcfield;
    //private TUVehicleMileageRecordedVD[] tuvehiclemileagerecordedvdfield;
    //private TUVehicleEnquiryHistoryVE[] tuvehicleenquiryhistoryvefield;
    //private TUVehicleEstimateRecordedVF[] tuvehicleestimaterecordedvffield;
    //private TUVehicleAlertRecordedVG[] tuvehiclealertrecordedvgfield;
    //private TUVehicleStolenRecordedVM[] tuvehiclestolenrecordedvmfield;
    //private TUVehicleRegistrationAuthorityHistoryRecordedVI[] tuvehicleregistrationauthorityhistoryrecordedvifield;
    //private TUVehicleInsuranceClaimsRecordedVJ[] tuvehicleinsuranceclaimsrecordedvjfield;
    //private TUVehicleInsurancePoliciesRecordedVK[] tuvehicleinsurancepoliciesrecordedvkfield;
    //private TUInputVerificationUV[] tuinputverificationuvfield;

    //public BCCB701[] BCCB701 { get { return this.bccb701field; } set { this.bccb701field = value; } }
    //public DeedsMultipleOwners[] DeedsMultipleOwners { get { return this.deedsmultipleownersfield; } set { this.deedsmultipleownersfield = value; } }
    //public DeedsMultipleOwnersDetail[] DeedsMultipleOwnersDetail { get { return this.deedsmultipleownersdetailfield; } set { this.deedsmultipleownersdetailfield = value; } }
    //public ForensicLinkagesHeaderFL[] ForensicLinkagesHeaderFL { get { return this.forensiclinkagesheaderflfield; } set { this.forensiclinkagesheaderflfield = value; } }
    //public ForensicLinkagesHomeTelephoneFH[] ForensicLinkagesHomeTelephoneFH { get { return this.forensiclinkageshometelephonefhfield; } set { this.forensiclinkageshometelephonefhfield = value; } }
    //public ForensicLinkagesWorkTelephoneFW[] ForensicLinkagesWorkTelephoneFW { get { return this.forensiclinkagesworktelephonefwfield; } set { this.forensiclinkagesworktelephonefwfield = value; } }
    //public ForensicLinkagesCellphoneNumberFX[] ForensicLinkagesCellphoneNumberFX { get { return this.forensiclinkagescellphonenumberfxfield; } set { this.forensiclinkagescellphonenumberfxfield = value; } }
    //public ForensicLinkagesDefaultsAssociates[] ForensicLinkagesDefaultsAssociates { get { return this.forensiclinkagesdefaultsassociatesfield; } set { this.forensiclinkagesdefaultsassociatesfield = value; } }
    //public ForensicLinkagesDefaultsFF[] ForensicLinkagesDefaultsFF { get { return this.forensiclinkagesdefaultsfffield; } set { this.forensiclinkagesdefaultsfffield = value; } }
    //public ForensicLinkagesHomeAddressFZ[] ForensicLinkagesHomeAddressFZ { get { return this.forensiclinkageshomeaddressfzfield; } set { this.forensiclinkageshomeaddressfzfield = value; } }
    //public ForensicLinkagesJudgementsAssociates[] ForensicLinkagesJudgementsAssociates { get { return this.forensiclinkagesjudgementsassociatesfield; } set { this.forensiclinkagesjudgementsassociatesfield = value; } }
    //public ForensicLinkagesJudgementsFJ[] ForensicLinkagesJudgementsFJ { get { return this.forensiclinkagesjudgementsfjfield; } set { this.forensiclinkagesjudgementsfjfield = value; } }
    //public HistoricContactDetailsHD[] HistoricContactDetailsHD { get { return this.historiccontactdetailshdfield; } set { this.historiccontactdetailshdfield = value; } }
    //public IndividualTrace06[] IndividualTrace06 { get { return this.individualtrace06field; } set { this.individualtrace06field = value; } }
    //public LinkedBusinessHeaders[] LinkedBusinessHeaders { get { return this.linkedbusinessheadersfield; } set { this.linkedbusinessheadersfield = value; } }
    //public LinkagesLK[] LinkagesLK { get { return this.linkageslkfield; } set { this.linkageslkfield = value; } }
    //public NLRCounterSegmentMC01[] NLRCounterSegmentMC01 { get { return this.nlrcountersegmentmc01field; } set { this.nlrcountersegmentmc01field = value; } }
    //public NLRCounterSegmentMC50[] NLRCounterSegmentMC50 { get { return this.nlrcountersegmentmc50field; } set { this.nlrcountersegmentmc50field = value; } }
    //public NLRSummaryMY50[] NLRSummaryMY50 { get { return this.nlrsummarymy50field; } set { this.nlrsummarymy50field = value; } }
    //public PrincipalDeedsCA[] PrincipalDeedsCA { get { return this.principaldeedscafield; } set { this.principaldeedscafield = value; } }
    //public ScorecardS701[] ScorecardS701 { get { return this.scorecards701field; } set { this.scorecards701field = value; } }
    //public AddressAlertRA[] AddressAlertRA { get { return this.addressalertrafield; } set { this.addressalertrafield = value; } }
    //public AllRiskTypesAR[] AllRiskTypesAR { get { return this.allrisktypesarfield; } set { this.allrisktypesarfield = value; } }
    //public ClaimantHeaderCH[] ClaimantHeaderCH { get { return this.claimantheaderchfield; } set { this.claimantheaderchfield = value; } }
    //public ClaimantHeaderHC[] ClaimantHeaderHC { get { return this.claimantheaderhcfield; } set { this.claimantheaderhcfield = value; } }
    //public ClaimantHeaderSH[] ClaimantHeaderSH { get { return this.claimantheadershfield; } set { this.claimantheadershfield = value; } }
    //public ClaimDetailsCR[] ClaimDetailsCR { get { return this.claimdetailscrfield; } set { this.claimdetailscrfield = value; } }
    //public ClaimsLinkedCL[] ClaimsLinkedCL { get { return this.claimslinkedclfield; } set { this.claimslinkedclfield = value; } }
    //public ClaimantSummaryCS[] ClaimantSummaryCS { get { return this.claimantsummarycsfield; } set { this.claimantsummarycsfield = value; } }
    //public ClaimsCommentSegmentSX[] ClaimsCommentSegmentSX { get { return this.claimscommentsegmentsxfield; } set { this.claimscommentsegmentsxfield = value; } }
    //public DriverDetailsDR[] DriverDetailsDR { get { return this.driverdetailsdrfield; } set { this.driverdetailsdrfield = value; } }
    //public FraudAlertS6[] FraudAlertS6 { get { return this.fraudalerts6field; } set { this.fraudalerts6field = value; } }
    //public HawkAlertHA[] HawkAlertHA { get { return this.hawkalerthafield; } set { this.hawkalerthafield = value; } }
    //public HomeOwnersAO[] HomeOwnersAO { get { return this.homeownersaofield; } set { this.homeownersaofield = value; } }
    //public IdentityVerficationAlertHI[] IdentityVerficationAlertHI { get { return this.identityverficationalerthifield; } set { this.identityverficationalerthifield = value; } }
    //public InsuredDetailsIN[] InsuredDetailsIN { get { return this.insureddetailsinfield; } set { this.insureddetailsinfield = value; } }
    //public PersonalLiabiltyPL[] PersonalLiabiltyPL { get { return this.personalliabiltyplfield; } set { this.personalliabiltyplfield = value; } }
    //public ServiceProviderSP[] ServiceProviderSP { get { return this.serviceproviderspfield; } set { this.serviceproviderspfield = value; } }
    //public ThirdPartyTP[] ThirdPartyTP { get { return this.thirdpartytpfield; } set { this.thirdpartytpfield = value; } }
    //public VehicleAlertVA[] VehicleAlertVA { get { return this.vehiclealertvafield; } set { this.vehiclealertvafield = value; } }
    //public VehicleDetailsVH[] VehicleDetailsVH { get { return this.vehicledetailsvhfield; } set { this.vehicledetailsvhfield = value; } }
    //public NatisEnquiryHistoryUS[] NatisEnquiryHistoryUS { get { return this.natisenquiryhistoryusfield; } set { this.natisenquiryhistoryusfield = value; } }
    //public NatisInputVerificationUT[] NatisInputVerificationUT { get { return this.natisinputverificationutfield; } set { this.natisinputverificationutfield = value; } }
    //public NatisDetailedVehicleInformationUU[] NatisDetailedVehicleInformationUU { get { return this.natisdetailedvehicleinformationuufield; } set { this.natisdetailedvehicleinformationuufield = value; } }
    //public DriversLicenseInformationUX[] DriversLicenseInformationUX { get { return this.driverslicenseinformationuxfield; } set { this.driverslicenseinformationuxfield = value; } }
    //public InternalMessageIM[] InternalMessageIM { get { return this.internalmessageimfield; } set { this.internalmessageimfield = value; } }
    //public IndivualOwnOtherActivePoliciesPI[] IndivualOwnOtherActivePoliciesPI { get { return this.indivualownotheractivepoliciespifield; } set { this.indivualownotheractivepoliciespifield = value; } }
    //public IndivualOwnOtherClaimDetails0to3yearsI1[] IndivualOwnOtherClaimDetails0to3yearsI1 { get { return this.indivualownotherclaimdetails0to3yearsi1field; } set { this.indivualownotherclaimdetails0to3yearsi1field = value; } }
    //public IndivualOwnOtherClaimDetails4to7yearsSI[] IndivualOwnOtherClaimDetails4to7yearsSI { get { return this.indivualownotherclaimdetails4to7yearssifield; } set { this.indivualownotherclaimdetails4to7yearssifield = value; } }
    //public IndivualOwnOtherInactivePoliciesP1[] IndivualOwnOtherInactivePoliciesP1 { get { return this.indivualownotherinactivepoliciesp1field; } set { this.indivualownotherinactivepoliciesp1field = value; } }
    //public InputIdentificationIndividualII[] InputIdentificationIndividualII { get { return this.inputidentificationindividualiifield; } set { this.inputidentificationindividualiifield = value; } }
    //public InputIdentificationRiskAddressIA[] InputIdentificationRiskAddressIA { get { return this.inputidentificationriskaddressiafield; } set { this.inputidentificationriskaddressiafield = value; } }
    //public InputIdentificationVehicleIV[] InputIdentificationVehicleIV { get { return this.inputidentificationvehicleivfield; } set { this.inputidentificationvehicleivfield = value; } }
    //public InsuranceEnquirySE[] InsuranceEnquirySE { get { return this.insuranceenquirysefield; } set { this.insuranceenquirysefield = value; } }
    //public PolicySummaryPS[] PolicySummaryPS { get { return this.policysummarypsfield; } set { this.policysummarypsfield = value; } }
    //public PolicyCommentSegmentSZ[] PolicyCommentSegmentSZ { get { return this.policycommentsegmentszfield; } set { this.policycommentsegmentszfield = value; } }
    //public RiskAddressOwnOtherActivePoliciesPA[] RiskAddressOwnOtherActivePoliciesPA { get { return this.riskaddressownotheractivepoliciespafield; } set { this.riskaddressownotheractivepoliciespafield = value; } }
    //public RiskAddressOwnOtherClaimDetails0to3yearsI3[] RiskAddressOwnOtherClaimDetails0to3yearsI3 { get { return this.riskaddressownotherclaimdetails0to3yearsi3field; } set { this.riskaddressownotherclaimdetails0to3yearsi3field = value; } }
    //public RiskAddressOwnOtherClaimDetails4to7yearsSR[] RiskAddressOwnOtherClaimDetails4to7yearsSR { get { return this.riskaddressownotherclaimdetails4to7yearssrfield; } set { this.riskaddressownotherclaimdetails4to7yearssrfield = value; } }
    //public RiskAddressOwnOtherInactivePoliciesP3[] RiskAddressOwnOtherInactivePoliciesP3 { get { return this.riskaddressownotherinactivepoliciesp3field; } set { this.riskaddressownotherinactivepoliciesp3field = value; } }
    //public VehicleOwnOtherActivePoliciesPV[] VehicleOwnOtherActivePoliciesPV { get { return this.vehicleownotheractivepoliciespvfield; } set { this.vehicleownotheractivepoliciespvfield = value; } }
    //public VehicleOwnOtherClaimDetails0to3yearsI2[] VehicleOwnOtherClaimDetails0to3yearsI2 { get { return this.vehicleownotherclaimdetails0to3yearsi2field; } set { this.vehicleownotherclaimdetails0to3yearsi2field = value; } }
    //public VehicleOwnOtherClaimDetails4to7yearsSV[] VehicleOwnOtherClaimDetails4to7yearsSV { get { return this.vehicleownotherclaimdetails4to7yearssvfield; } set { this.vehicleownotherclaimdetails4to7yearssvfield = value; } }
    //public VehicleOwnOtherInactivePoliciesP2[] VehicleOwnOtherInactivePoliciesP2 { get { return this.vehicleownotherinactivepoliciesp2field; } set { this.vehicleownotherinactivepoliciesp2field = value; } }
    //public VehicleInformationUR[] VehicleInformationUR { get { return this.vehicleinformationurfield; } set { this.vehicleinformationurfield = value; } }
    //public VehicleOwnership_TitleHolderInformationUW[] VehicleOwnership_TitleHolderInformationUW { get { return this.vehicleownership_titleholderinformationuwfield; } set { this.vehicleownership_titleholderinformationuwfield = value; } }
    //public TUVehicleDetailsVL[] TUVehicleDetailsVL { get { return this.tuvehicledetailsvlfield; } set { this.tuvehicledetailsvlfield = value; } }
    //public TUVehicleFinanceDetailsVB[] TUVehicleFinanceDetailsVB { get { return this.tuvehiclefinancedetailsvbfield; } set { this.tuvehiclefinancedetailsvbfield = value; } }
    //public TUVehiclePreviousRegistrationDetailsVC[] TUVehiclePreviousRegistrationDetailsVC { get { return this.tuvehiclepreviousregistrationdetailsvcfield; } set { this.tuvehiclepreviousregistrationdetailsvcfield = value; } }
    //public TUVehicleMileageRecordedVD[] TUVehicleMileageRecordedVD { get { return this.tuvehiclemileagerecordedvdfield; } set { this.tuvehiclemileagerecordedvdfield = value; } }
    //public TUVehicleEnquiryHistoryVE[] TUVehicleEnquiryHistoryVE { get { return this.tuvehicleenquiryhistoryvefield; } set { this.tuvehicleenquiryhistoryvefield = value; } }
    //public TUVehicleEstimateRecordedVF[] TUVehicleEstimateRecordedVF { get { return this.tuvehicleestimaterecordedvffield; } set { this.tuvehicleestimaterecordedvffield = value; } }
    //public TUVehicleAlertRecordedVG[] TUVehicleAlertRecordedVG { get { return this.tuvehiclealertrecordedvgfield; } set { this.tuvehiclealertrecordedvgfield = value; } }
    //public TUVehicleStolenRecordedVM[] TUVehicleStolenRecordedVM { get { return this.tuvehiclestolenrecordedvmfield; } set { this.tuvehiclestolenrecordedvmfield = value; } }
    //public TUVehicleRegistrationAuthorityHistoryRecordedVI[] TUVehicleRegistrationAuthorityHistoryRecordedVI { get { return this.tuvehicleregistrationauthorityhistoryrecordedvifield; } set { this.tuvehicleregistrationauthorityhistoryrecordedvifield = value; } }
    //public TUVehicleInsuranceClaimsRecordedVJ[] TUVehicleInsuranceClaimsRecordedVJ { get { return this.tuvehicleinsuranceclaimsrecordedvjfield; } set { this.tuvehicleinsuranceclaimsrecordedvjfield = value; } }
    //public TUVehicleInsurancePoliciesRecordedVK[] TUVehicleInsurancePoliciesRecordedVK { get { return this.tuvehicleinsurancepoliciesrecordedvkfield; } set { this.tuvehicleinsurancepoliciesrecordedvkfield = value; } }
    //public TUInputVerificationUV[] TUInputVerificationUV { get { return this.tuinputverificationuvfield; } set { this.tuinputverificationuvfield = value; } }

}

/// <remarks/>



public enum ResponseStatus {
    
    /// <remarks/>
    Success,
    
    /// <remarks/>
    Failure,
}


}
