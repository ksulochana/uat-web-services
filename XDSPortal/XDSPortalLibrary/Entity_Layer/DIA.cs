﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDSPortalLibrary.Entity_Layer
{
    public class DIA
    {
        public DIA() { }

    }

    //VerifyDataWcfReq– Class representing the sent(request) package data.

  	public class VerifyDataWcfReq
    	{
        public string InstName { get; set; }     // represents the company/institution
        public string AccessCode { get; set;}    // company password
        public int FingerNo1 { get; set; }       // 1 for right thumb
        public byte[] WsqData1 { get; set; }     // right thumb data
        public int 	FingerNo2 { get; set; }     // 6 for left thumb
        public byte[] WsqData2 { get; set; }     // left thumb data
        public byte[] portrait { get; set; }     // byte array portrait image
        public string IdNumber { get; set; }     // applicant id number
        public SearchType Search { get; set; }   // database  selection
        public string ReferenceNo { get; set; }  // client reference number
        public int ApplicationCode { get; set; } // reserved for future
        public string UserName{ get; set; }      // user identifier, can be machine no
       }


//3.1.2	 SearchType - Enumerator representing the database to search 

public enum SearchType
    	{
        DIA_ONLY,
        DIA_DHA,
        DHA_ONLY
    	}

//3.1.3	VerifyDataWcfResp - Class representing the response data

public class VerifyDataWcfResp
    	{
        public byte[] portrait { get; set; }    // Received from Hanis if available
        public string IdNumber { get; set; }    // Applicant Id number
        public string Firstnames { get; set; }  // Applicant first names
        public string Surname { get; set; }     // Applicant Surnames
      	 public string Status { get; set; }      // alive or dead
        public int ResponseCode { get; set; }   // response code returned
        public string ResponseMsg { get; set; } // Response Message
}


//3.1.4	GetProfileReq - Class representing the request data

public class GetProfileReq
   	{
        public string InstName { get; set; }     // represents the company/institution
        public string AccessCode { get; set; }   //company password
        public string IdNumber { get; set; }     // Applicant Id number
        public string ReferenceNo { get; set; }  // client reference number
        public int ApplicationCode { get; set; } // reserved for future
        public string UserName { get; set; }     // user identifier
} 



//3.1.5	GetProfileResp – Class Representing the response data

    public class GetProfileResp
    {
        public string InstName { get; set; }     // represents the company/institution
        public string AccessCode { get; set; }   //company password
        public string IdNumber { get; set; }     // Applicant Id number 
        public string ReferenceNo { get; set; }  // client reference number
        public int ApplicationCode { get; set; } // reserved for future
        public string UserName { get; set; }     // user identifier
        public string FirstName { get; set; }    // Applicant Name
        public string Surname { get; set; }      // Applicant surname
        public string Status { get; set; }       // Applicant status
        public int ResponseCode { get; set; }    // Server response code
        public string ResponseMsg { get; set; }  // Server response message
    }

}
