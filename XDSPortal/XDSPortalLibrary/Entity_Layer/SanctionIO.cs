﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XDSPortalLibrary.Entity_Layer
{
    public class SanctionIO
    {
        public string Status { get; set; }
        public string Count { get; set; }
        public Result[] Result { get; set; }
    }

    public class Result
    {
        public float confidence_score { get; set; }
        public string name { get; set; }
        public string[] alt_names { get; set; }
        public string email { get; set; }
        public string entity_type { get; set; }
        public string[] date_of_birth { get; set; }
        public string[] year_of_birth { get; set; }
        public string[] place_of_birth { get; set; }
        public string[] nationality { get; set; }
        public Data_Source data_source { get; set; }
        public string user_source { get; set; }
        public string[] program { get; set; }
        public string si_identifier { get; set; }
        public string entity_number { get; set; }
        public string data_hash { get; set; }
        public string remarks { get; set; }
        public string title { get; set; }
        public string[] addresses { get; set; }
        public string start_date { get; set; }
        public string[] citizenship { get; set; }
        public string[] wallet_address { get; set; }
        public string source_list_url { get; set; }
        public string source_information_url { get; set; }
        public string relation { get; set; }
        public string reason { get; set; }
        public string[] dates_of_birth { get; set; }
        public int score { get; set; }
        public string source { get; set; }
        public string[] citizenships { get; set; }
        public string import_source_id { get; set; }
        public string[] nationalities { get; set; }
    }

    public class Data_Source
    {
        public string name { get; set; }
        public string short_name { get; set; }
    }
}
