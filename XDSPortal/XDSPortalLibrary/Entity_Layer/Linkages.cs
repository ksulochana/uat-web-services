﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDSPortalLibrary.Entity_Layer
{
    public class Linkages
    {
          private string RegistrationNovalue = string.Empty, strTmpReference = string.Empty, Businessnamevalue = string.Empty, ExternalReferencevalue = string.Empty, ReferenceNoValue = string.Empty;
        bool ConfirmationChkBoxvalue,IsMatchingValue = false;
        private int subscriberIDvalue, productIDvalue,CommercialIDValue =0, _ReportID=0, DirectorIDValue = 0, ConsumerIDvalue = 0;
        DataSet BonusSegmentsDS;
        private string VATNumberValue = string.Empty, SolePropIDNoValue = string.Empty, TrustNoValue = string.Empty, _ReportName = string.Empty;
        private bool boolBonusCheck = false, IsConsumerMatchingvalue = false;
        private string strdataSegments = string.Empty;
         string IDnovalue = string.Empty,  Surnamevalue = string.Empty, Firstnamevalue = string.Empty, Secondnamevalue = string.Empty,  FirstInitialvalue = string.Empty, SecondInitialvalue = string.Empty;
         DateTime DOBvalue;


          public int DirectorID
        {
            get
            {
                return this.DirectorIDValue;
            }
            set
            {
                this.DirectorIDValue = value;
            }
        }
        public int ConsumerID
        {
            get
            {
                return this.ConsumerIDvalue;
            }
            set
            {
                this.ConsumerIDvalue = value;
            }
        }

          public string IDno
        {
            get
            {
                if (String.IsNullOrEmpty(IDnovalue))
                    return DBNull.Value.ToString();
                else
                    return this.IDnovalue;
            }
            set
            {
                this.IDnovalue = value;
            }
        }
        public string Surname
        {
            get
            {
                if (String.IsNullOrEmpty(Surnamevalue))
                    return DBNull.Value.ToString();
                else
                    return this.Surnamevalue;
            }
            set
            {
                this.Surnamevalue = value;
            }
        }
        public string Firstname
        {
            get
            {
                if (String.IsNullOrEmpty(Firstnamevalue))
                    return DBNull.Value.ToString();
                else
                    return this.Firstnamevalue;
            }
            set
            {
                this.Firstnamevalue = value;
            }
        }
        public string SecondName
        {
            get
            {
                if (String.IsNullOrEmpty(Secondnamevalue))
                    return DBNull.Value.ToString();
                else
                    return this.Secondnamevalue;
            }
            set
            {
                this.Secondnamevalue = value;
            }
        }
        public DateTime DOB
        {
            get
            {
                if (this.DOBvalue == null)
                    return Convert.ToDateTime(DBNull.Value);
                else
                    return this.DOBvalue;
            }
            set
            {
                this.DOBvalue = value;
            }
        }

          public string FirstInitial
        {
            get
            {
                if (String.IsNullOrEmpty(FirstInitialvalue))
                    return DBNull.Value.ToString();
                else
                    return this.FirstInitialvalue;
            }
            set
            {
                this.FirstInitialvalue = value;
            }
        }
        public string SecondInitial
        {
            get
            {
                if (String.IsNullOrEmpty(SecondInitialvalue))
                    return DBNull.Value.ToString();
                else
                    return this.SecondInitialvalue;
            }
            set
            {
                this.SecondInitialvalue = value;
            }
        }

          public bool IsConsumerMatching
        {
            get
            {
                return this.IsConsumerMatchingvalue;
            }
            set
            {
                this.IsConsumerMatchingvalue = value;
            }
        }

        public int ReportID
        {
            get { return this._ReportID; }
            set { this._ReportID = value; }
        }

        public string ReportName
        {
            get { return this._ReportName; }
            set { this._ReportName = value; }
        }

        public string DataSegments
        {
            get { return this.strdataSegments; }
            set { this.strdataSegments = value; }
        }

        public bool BonusCheck
        {
            get { return this.boolBonusCheck; }
            set { this.boolBonusCheck = value; }
        }

        public Linkages()
        {
        }
        public int CommercialID
        {
            get
            {
                return this.CommercialIDValue;
            }
            set
            {
                this.CommercialIDValue = value;
            }
        }
        public DataSet BonusSegments
        {
            get
            {
                return this.BonusSegmentsDS;
            }
            set
            {
                this.BonusSegmentsDS = value;
            }
        }
        public string ReferenceNo
        {
            get
            {
                if (String.IsNullOrEmpty(this.ReferenceNoValue))
                    return DBNull.Value.ToString();
                else
                    return this.ReferenceNoValue;
            }
            set
            {
                this.ReferenceNoValue = value;
            }
        }

        public int subscriberID
        {
            get
            {
                if (String.IsNullOrEmpty(this.subscriberIDvalue.ToString()))
                    return Convert.ToInt16(DBNull.Value);
                else
                    return this.subscriberIDvalue;
            }
            set
            {
                this.subscriberIDvalue = value;
            }
        }
        public int ProductID
        {
            get
            {
                if (String.IsNullOrEmpty(this.subscriberIDvalue.ToString()))
                    return Convert.ToInt16(DBNull.Value);
                else
                    return this.productIDvalue;
            }
            set
            {
                this.productIDvalue = value;
            }
        }
        public string RegistrationNo
        {
            get
            {
                if (String.IsNullOrEmpty(this.RegistrationNovalue))
                    return DBNull.Value.ToString();
                else
                    return this.RegistrationNovalue;
            }
            set
            {
                this.RegistrationNovalue = value;
            }
        }
        public string BusinessName
        {
            get
            {
                if (String.IsNullOrEmpty(this.Businessnamevalue))
                    return DBNull.Value.ToString();
                else
                    return this.Businessnamevalue;
            }
            set
            {
                this.Businessnamevalue = value;
            }
        }
        public string ExternalReference
        {
            get
            {
                if (String.IsNullOrEmpty(this.ExternalReferencevalue))
                    return DBNull.Value.ToString();
                else
                    return this.ExternalReferencevalue;
            }
            set
            {
                this.ExternalReferencevalue = value;
            }
        }
        public bool ConfirmationChkBox
        {
            get
            {
                return this.ConfirmationChkBoxvalue;
            }
            set
            {
                this.ConfirmationChkBoxvalue = value;
            }
        }
        public bool IsMatching
        {
            get
            {
                return this.IsMatchingValue;
            }
            set
            {
                this.IsMatchingValue = value;
            }
        }
        public string TmpReference
        {
            get
            {
                if (String.IsNullOrEmpty(this.strTmpReference))
                    return DBNull.Value.ToString();
                else
                    return this.strTmpReference;
            }
            set
            {
                this.strTmpReference = value;
            }
        }
        public string VATNumber
        {
            get
            {
                return this.VATNumberValue;
            }
            set
            {
                this.VATNumberValue = value;
            }
        }
        public string SolePropIDNo
        {
            get
            {
                return this.SolePropIDNoValue;
            }
            set
            {
                this.SolePropIDNoValue = value;
            }
        }
        public string TrustNo
        {
            get
            {
                return this.TrustNoValue;
            }
            set
            {
                this.TrustNoValue = value;
            }
        }
    }
}
