﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XDSPortalLibrary.Entity_Layer
{
    public class MDPInput
    {
        public string ProductId { get; set; }
        public string ProductName { get; set; }
        public string EnquiryId { get; set; }
        public string SubscriberId { get; set; }
        public string SubscriberName { get; set; }
        public string SolutionTransactionId { get; set; }
    }
}
