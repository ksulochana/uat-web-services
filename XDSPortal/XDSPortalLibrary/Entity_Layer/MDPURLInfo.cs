﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDSPortalLibrary.Entity_Layer
{
  public static class MDPURLInfo
    {
        static MDPURLInfo() { }

        public static Dictionary<string, URLInfo> oURLinfo = new Dictionary<string, URLInfo>();
    }

    public class URLInfo
    {
        public URLInfo() { }
        public string MDPBaseURL { get; set; }
        public string URL { get; set; }
        public Dictionary<string,string> Parameter { get; set; }
    }
}
