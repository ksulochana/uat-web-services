﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace XDSPortalLibrary.Entity_Layer
{
  //  [Serializable()]
    public class CommercialQuestionnaire
    {
        public CommercialQuestionnaire() { }

        public CIPC CIPC { get; set; }
        public VAT VAT { get; set; }
        public ContactInfo ContatcInfo { get; set; }
        public CompanyOverview[] CompanyOverview { get; set; }
        public CompanyStructure[] CompanyStructure { get; set; }
        public Employees Employees { get; set; }
        public CompanyFleet[] CompanyFleet { get; set; }
        public OperationAssesment OperationAssesment { get; set; }
        public BEE BEE { get; set; }
        public ISO[] ISO { get; set; }
        public WorkmansCompensation WorkmansCompensation { get; set; }
        public TradeReferences[] TradeReferences { get; set; }
        public Financials Financials { get; set; }
        public NewBankCodes NewBankCodes { get; set; }
        public Contracts[] Contracts { get; set; }

    }
  //  [Serializable()]
    public partial class CIPC
    {
        public CompanyInformation CompanyInformation { get; set; }
        public Auditors Auditors { get; set; }
        public Principals[] Principals { get; set; }

    }
  //  [Serializable()]
    public partial class ContactInfo
    {
        public AddressHistory[] AddressHistory { get; set; }
        public ContactHistory[] ContactHistory { get; set; }
    }
  //  [Serializable()]
    public partial class CompanyInformation
    {
        public string RegisteredName { get; set; }
        public string BusinessRegistrationNumber { get; set; }
        public string PreviousBusinessName { get; set; }
        public string NameChangeDate { get; set; }
        public string ConvertedRegNumber { get; set; }
        public string RegistrationDate { get; set; }
        public string BusinessStartDate { get; set; }
        public string TaxNumber { get; set; }
        public string BusinessStatus { get; set; }
        public string FinancialEffectiveDate { get; set; }
        public string FinancialYearEnd { get; set; }
        public string AuthorisedCapitalAmount { get; set; }
        public string IssuedCapitalAmountorMemberContribution { get; set; }
        public string NumberofAuthorisedShares { get; set; }
        public string NumberofIssuedShares { get; set; }
    }
  //  [Serializable()]
    public partial class Auditors
    {
        public string AuditorName { get; set; }
        public string ProfessionNumber { get; set; }
        public string AuditorStartDate { get; set; }
        public string TimewithAuditors { get; set; }
        public string AuditorType { get; set; }
        public string PhysicalAddress { get; set; }
        public string PostalAddress { get; set; }
        public string ContactDetails { get; set; }
    }
  //  [Serializable()]
    public partial class Principals
    {
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string PreviousSurname { get; set; }
        public string PrincipalType { get; set; }
        public string IDNumber { get; set; }
        public string AppointmentDate { get; set; }
        public string YearswithBusiness { get; set; }
        public string PhysicalAddress { get; set; }
        public string PostalAddress { get; set; }
        public string PercentageMemberShareholding { get; set; }
    }
  //  [Serializable()]
    public partial class VAT
    {
        public string CompanyName { get; set; }
        public string TradingName { get; set; }
        public string VatNumber { get; set; }
    }
  //  [Serializable()]
    public partial class AddressHistory
    {
        public string AddressType { get; set; }
        public string HeadOfficeorBranch { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string Suburb { get; set; }
        public string TownorCity { get; set; }
        public string PostalCode { get; set; }
    }
  //  [Serializable()]
    public partial class ContactHistory
    {
        public string ContactType { get; set; }
        public string Details { get; set; }
    }
  //  [Serializable()]
    public partial class CompanyOverview
    {
        public string TypeofStructure { get; set; }
        public string NumberofStructure { get; set; }
        public string Description { get; set; }
    }
  //  [Serializable()]
    public partial class CompanyStructure
    {
        public string Name { get; set; }
        public string PercentageShareholding { get; set; }
        public string Type { get; set; }
        public string Comments { get; set; }
    }
  //  [Serializable()]
    public partial class Employees
    {
        public string NumberofSalariedEmployees { get; set; }
        public string NumberofWagedEmployees { get; set; }
        public string NumberofCasualEmployees { get; set; }
        public string NumberofContractedEmployees { get; set; }
    }
  //  [Serializable()]
    public partial class CompanyFleet
    {
        public string Typeofvehicle { get; set; }
        public string Numberofvehicles { get; set; }
        public string OwnedOrLeased { get; set; }
        public string Value { get; set; }
        public string OutstandingBalance { get; set; }
    }
  //  [Serializable()]
    public partial class OperationAssesment
    {
        public string OperationalDetail { get; set; }
        public string IndustrySector { get; set; }
        public string Customers { get; set; }
        public string ExportCountries { get; set; }
        public string ImportCountries { get; set; }
        public string Comments { get; set; }
    }
  //  [Serializable()]
    public partial class BEE
    {
        public string Level { get; set; }
        public string CertificateAvailable { get; set; }
        public string Expirydate { get; set; }
    }
  //  [Serializable()]
    public partial class ISO
    {
        public string Status { get; set; }
        public string ISOScore { get; set; }
        public string CertificateAvailable { get; set; }
    }
  //  [Serializable()]
    public partial class WorkmansCompensation
    {
        public string Status { get; set; }
        public string CertificateAvailable { get; set; }
    }
  //  [Serializable()]
    public partial class TradeReferences
    {
        public string Supplier { get; set; }
        public string SupplierContact { get; set; }
    }
  //  [Serializable()]
    public partial class Financials
    {
        public string Currentassets { get; set; }
        public string Inventory { get; set; }
        public string AccountsReceivable { get; set; }
        public string Fixedassets { get; set; }
        public string PlantEquipmentVehicles { get; set; }
        public string Property { get; set; }
        public string Currentliabilities { get; set; }
        public string AccountsPayable { get; set; }
        public string ShortTermsLoans { get; set; }
        public string LongTermLoans { get; set; }
        public string Longtermliabilities { get; set; }
        public string MortgageBonds { get; set; }
        public string PreviousYearTurnover { get; set; }
        public string CurrentTurnoverpermonth { get; set; }
        public string CurrentTurnoverperannum { get; set; }
        public string Comments { get; set; }
    }
  //  [Serializable()]
    public partial class NewBankCodes
    {
        public string AccountName { get; set; }
        public string Bank { get; set; }
        public string BranchName { get; set; }
        public string BranchCode { get; set; }
        public string AccountNumber { get; set; }
        public string Terms { get; set; }
    }
  //  [Serializable()]
    public partial class Contracts
    {
        public string ContractValue { get; set; }
        public string Remarks { get; set; }
        public string MainContractor { get; set; }
    }

   

    /// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //[System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class CommercialQuestionnaireResult
    {

        private string resultField;

        private string xDSReferenceNoField;

        private string errorField;

        /// <remarks/>
        public string Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        public string XDSReferenceNo
        {
            get
            {
                return this.xDSReferenceNoField;
            }
            set
            {
                this.xDSReferenceNoField = value;
            }
        }

        /// <remarks/>
        public string Error
        {
            get
            {
                return this.errorField;
            }
            set
            {
                this.errorField = value;
            }
        }
    }

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //[System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    //public partial class CommercialQuestionnaire
    //{

    //    private CommercialQuestionnaireCIPC cIPCField;

    //    private CommercialQuestionnaireVAT vATField;

    //    private CommercialQuestionnaireCompanyOverview companyOverviewField;

    //    private CommercialQuestionnaireCompanyStructure companyStructureField;

    //    private CommercialQuestionnaireEmployees employeesField;

    //    private CommercialQuestionnaireCompanyFleet companyFleetField;

    //    private CommercialQuestionnaireOperationAssesment operationAssesmentField;

    //    private CommercialQuestionnaireBEE bEEField;

    //    private CommercialQuestionnaireISO iSOField;

    //    private CommercialQuestionnaireWorkmansCompensation workmansCompensationField;

    //    private CommercialQuestionnaireTradeReferences tradeReferencesField;

    //    private CommercialQuestionnaireFinancials financialsField;

    //    private CommercialQuestionnaireNewBankCodes newBankCodesField;

    //    private CommercialQuestionnaireContracts contractsField;

    //    /// <remarks/>
    //    public CommercialQuestionnaireCIPC CIPC
    //    {
    //        get
    //        {
    //            return this.cIPCField;
    //        }
    //        set
    //        {
    //            this.cIPCField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public CommercialQuestionnaireVAT VAT
    //    {
    //        get
    //        {
    //            return this.vATField;
    //        }
    //        set
    //        {
    //            this.vATField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public CommercialQuestionnaireCompanyOverview CompanyOverview
    //    {
    //        get
    //        {
    //            return this.companyOverviewField;
    //        }
    //        set
    //        {
    //            this.companyOverviewField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public CommercialQuestionnaireCompanyStructure CompanyStructure
    //    {
    //        get
    //        {
    //            return this.companyStructureField;
    //        }
    //        set
    //        {
    //            this.companyStructureField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public CommercialQuestionnaireEmployees Employees
    //    {
    //        get
    //        {
    //            return this.employeesField;
    //        }
    //        set
    //        {
    //            this.employeesField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public CommercialQuestionnaireCompanyFleet CompanyFleet
    //    {
    //        get
    //        {
    //            return this.companyFleetField;
    //        }
    //        set
    //        {
    //            this.companyFleetField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public CommercialQuestionnaireOperationAssesment OperationAssesment
    //    {
    //        get
    //        {
    //            return this.operationAssesmentField;
    //        }
    //        set
    //        {
    //            this.operationAssesmentField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public CommercialQuestionnaireBEE BEE
    //    {
    //        get
    //        {
    //            return this.bEEField;
    //        }
    //        set
    //        {
    //            this.bEEField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public CommercialQuestionnaireISO ISO
    //    {
    //        get
    //        {
    //            return this.iSOField;
    //        }
    //        set
    //        {
    //            this.iSOField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public CommercialQuestionnaireWorkmansCompensation WorkmansCompensation
    //    {
    //        get
    //        {
    //            return this.workmansCompensationField;
    //        }
    //        set
    //        {
    //            this.workmansCompensationField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public CommercialQuestionnaireTradeReferences TradeReferences
    //    {
    //        get
    //        {
    //            return this.tradeReferencesField;
    //        }
    //        set
    //        {
    //            this.tradeReferencesField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public CommercialQuestionnaireFinancials Financials
    //    {
    //        get
    //        {
    //            return this.financialsField;
    //        }
    //        set
    //        {
    //            this.financialsField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public CommercialQuestionnaireNewBankCodes NewBankCodes
    //    {
    //        get
    //        {
    //            return this.newBankCodesField;
    //        }
    //        set
    //        {
    //            this.newBankCodesField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public CommercialQuestionnaireContracts Contracts
    //    {
    //        get
    //        {
    //            return this.contractsField;
    //        }
    //        set
    //        {
    //            this.contractsField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class CommercialQuestionnaireCIPC
    //{

    //    private CommercialQuestionnaireCIPCCompanyInformation companyInformationField;

    //    private CommercialQuestionnaireCIPCAuditors auditorsField;

    //    private CommercialQuestionnaireCIPCPrincipals principalsField;

    //    /// <remarks/>
    //    public CommercialQuestionnaireCIPCCompanyInformation CompanyInformation
    //    {
    //        get
    //        {
    //            return this.companyInformationField;
    //        }
    //        set
    //        {
    //            this.companyInformationField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public CommercialQuestionnaireCIPCAuditors Auditors
    //    {
    //        get
    //        {
    //            return this.auditorsField;
    //        }
    //        set
    //        {
    //            this.auditorsField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public CommercialQuestionnaireCIPCPrincipals Principals
    //    {
    //        get
    //        {
    //            return this.principalsField;
    //        }
    //        set
    //        {
    //            this.principalsField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class CommercialQuestionnaireCIPCCompanyInformation
    //{

    //    private string registeredNameField;

    //    private string businessRegistrationNumberField;

    //    private string previousBusinessNameField;

    //    private string nameChangeDateField;

    //    private string convertedRegNumberField;

    //    private string registrationDateField;

    //    private string businessStartDateField;

    //    private string taxNumberField;

    //    private string businessStatusField;

    //    private string financialEffectiveDateField;

    //    private string financialYearEndField;

    //    private string authorisedCapitalAmountField;

    //    private string issuedCapitalAmountorMemberContributionField;

    //    private string numberofAuthorisedSharesField;

    //    private string numberofIssuedSharesField;

    //    /// <remarks/>
    //    public string RegisteredName
    //    {
    //        get
    //        {
    //            return this.registeredNameField;
    //        }
    //        set
    //        {
    //            this.registeredNameField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string BusinessRegistrationNumber
    //    {
    //        get
    //        {
    //            return this.businessRegistrationNumberField;
    //        }
    //        set
    //        {
    //            this.businessRegistrationNumberField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string PreviousBusinessName
    //    {
    //        get
    //        {
    //            return this.previousBusinessNameField;
    //        }
    //        set
    //        {
    //            this.previousBusinessNameField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string NameChangeDate
    //    {
    //        get
    //        {
    //            return this.nameChangeDateField;
    //        }
    //        set
    //        {
    //            this.nameChangeDateField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string ConvertedRegNumber
    //    {
    //        get
    //        {
    //            return this.convertedRegNumberField;
    //        }
    //        set
    //        {
    //            this.convertedRegNumberField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string RegistrationDate
    //    {
    //        get
    //        {
    //            return this.registrationDateField;
    //        }
    //        set
    //        {
    //            this.registrationDateField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string BusinessStartDate
    //    {
    //        get
    //        {
    //            return this.businessStartDateField;
    //        }
    //        set
    //        {
    //            this.businessStartDateField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string TaxNumber
    //    {
    //        get
    //        {
    //            return this.taxNumberField;
    //        }
    //        set
    //        {
    //            this.taxNumberField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string BusinessStatus
    //    {
    //        get
    //        {
    //            return this.businessStatusField;
    //        }
    //        set
    //        {
    //            this.businessStatusField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string FinancialEffectiveDate
    //    {
    //        get
    //        {
    //            return this.financialEffectiveDateField;
    //        }
    //        set
    //        {
    //            this.financialEffectiveDateField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string FinancialYearEnd
    //    {
    //        get
    //        {
    //            return this.financialYearEndField;
    //        }
    //        set
    //        {
    //            this.financialYearEndField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string AuthorisedCapitalAmount
    //    {
    //        get
    //        {
    //            return this.authorisedCapitalAmountField;
    //        }
    //        set
    //        {
    //            this.authorisedCapitalAmountField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string IssuedCapitalAmountorMemberContribution
    //    {
    //        get
    //        {
    //            return this.issuedCapitalAmountorMemberContributionField;
    //        }
    //        set
    //        {
    //            this.issuedCapitalAmountorMemberContributionField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string NumberofAuthorisedShares
    //    {
    //        get
    //        {
    //            return this.numberofAuthorisedSharesField;
    //        }
    //        set
    //        {
    //            this.numberofAuthorisedSharesField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string NumberofIssuedShares
    //    {
    //        get
    //        {
    //            return this.numberofIssuedSharesField;
    //        }
    //        set
    //        {
    //            this.numberofIssuedSharesField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class CommercialQuestionnaireCIPCAuditors
    //{

    //    private string auditorNameField;

    //    private string professionNumberField;

    //    private string auditorStartDateField;

    //    private string timewithAuditorsField;

    //    private string auditorTypeField;

    //    private string physicalAddressField;

    //    private string postalAddressField;

    //    /// <remarks/>
    //    public string AuditorName
    //    {
    //        get
    //        {
    //            return this.auditorNameField;
    //        }
    //        set
    //        {
    //            this.auditorNameField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string ProfessionNumber
    //    {
    //        get
    //        {
    //            return this.professionNumberField;
    //        }
    //        set
    //        {
    //            this.professionNumberField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string AuditorStartDate
    //    {
    //        get
    //        {
    //            return this.auditorStartDateField;
    //        }
    //        set
    //        {
    //            this.auditorStartDateField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string TimewithAuditors
    //    {
    //        get
    //        {
    //            return this.timewithAuditorsField;
    //        }
    //        set
    //        {
    //            this.timewithAuditorsField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string AuditorType
    //    {
    //        get
    //        {
    //            return this.auditorTypeField;
    //        }
    //        set
    //        {
    //            this.auditorTypeField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string PhysicalAddress
    //    {
    //        get
    //        {
    //            return this.physicalAddressField;
    //        }
    //        set
    //        {
    //            this.physicalAddressField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string PostalAddress
    //    {
    //        get
    //        {
    //            return this.postalAddressField;
    //        }
    //        set
    //        {
    //            this.postalAddressField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class CommercialQuestionnaireCIPCPrincipals
    //{

    //    private CommercialQuestionnaireCIPCPrincipalsPrincipals principalsField;

    //    /// <remarks/>
    //    public CommercialQuestionnaireCIPCPrincipalsPrincipals Principals
    //    {
    //        get
    //        {
    //            return this.principalsField;
    //        }
    //        set
    //        {
    //            this.principalsField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class CommercialQuestionnaireCIPCPrincipalsPrincipals
    //{

    //    private string firstNameField;

    //    private string surnameField;

    //    private string previousSurnameField;

    //    private string principalTypeField;

    //    private string iDNumberField;

    //    private string appointmentDateField;

    //    private string yearswithBusinessField;

    //    private string physicalAddressField;

    //    private string postalAddressField;

    //    private string percentageMemberShareholdingField;

    //    /// <remarks/>
    //    public string FirstName
    //    {
    //        get
    //        {
    //            return this.firstNameField;
    //        }
    //        set
    //        {
    //            this.firstNameField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string Surname
    //    {
    //        get
    //        {
    //            return this.surnameField;
    //        }
    //        set
    //        {
    //            this.surnameField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string PreviousSurname
    //    {
    //        get
    //        {
    //            return this.previousSurnameField;
    //        }
    //        set
    //        {
    //            this.previousSurnameField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string PrincipalType
    //    {
    //        get
    //        {
    //            return this.principalTypeField;
    //        }
    //        set
    //        {
    //            this.principalTypeField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string IDNumber
    //    {
    //        get
    //        {
    //            return this.iDNumberField;
    //        }
    //        set
    //        {
    //            this.iDNumberField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string AppointmentDate
    //    {
    //        get
    //        {
    //            return this.appointmentDateField;
    //        }
    //        set
    //        {
    //            this.appointmentDateField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string YearswithBusiness
    //    {
    //        get
    //        {
    //            return this.yearswithBusinessField;
    //        }
    //        set
    //        {
    //            this.yearswithBusinessField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string PhysicalAddress
    //    {
    //        get
    //        {
    //            return this.physicalAddressField;
    //        }
    //        set
    //        {
    //            this.physicalAddressField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string PostalAddress
    //    {
    //        get
    //        {
    //            return this.postalAddressField;
    //        }
    //        set
    //        {
    //            this.postalAddressField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string PercentageMemberShareholding
    //    {
    //        get
    //        {
    //            return this.percentageMemberShareholdingField;
    //        }
    //        set
    //        {
    //            this.percentageMemberShareholdingField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class CommercialQuestionnaireVAT
    //{

    //    private string companyNameField;

    //    private string tradingNameField;

    //    private string vatNumberField;

    //    /// <remarks/>
    //    public string CompanyName
    //    {
    //        get
    //        {
    //            return this.companyNameField;
    //        }
    //        set
    //        {
    //            this.companyNameField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string TradingName
    //    {
    //        get
    //        {
    //            return this.tradingNameField;
    //        }
    //        set
    //        {
    //            this.tradingNameField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string VatNumber
    //    {
    //        get
    //        {
    //            return this.vatNumberField;
    //        }
    //        set
    //        {
    //            this.vatNumberField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class CommercialQuestionnaireCompanyOverview
    //{

    //    private CommercialQuestionnaireCompanyOverviewCompanyOverview companyOverviewField;

    //    /// <remarks/>
    //    public CommercialQuestionnaireCompanyOverviewCompanyOverview CompanyOverview
    //    {
    //        get
    //        {
    //            return this.companyOverviewField;
    //        }
    //        set
    //        {
    //            this.companyOverviewField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class CommercialQuestionnaireCompanyOverviewCompanyOverview
    //{

    //    private string typeofStructureField;

    //    private string numberofStructureField;

    //    private string descriptionField;

    //    /// <remarks/>
    //    public string TypeofStructure
    //    {
    //        get
    //        {
    //            return this.typeofStructureField;
    //        }
    //        set
    //        {
    //            this.typeofStructureField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string NumberofStructure
    //    {
    //        get
    //        {
    //            return this.numberofStructureField;
    //        }
    //        set
    //        {
    //            this.numberofStructureField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string Description
    //    {
    //        get
    //        {
    //            return this.descriptionField;
    //        }
    //        set
    //        {
    //            this.descriptionField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class CommercialQuestionnaireCompanyStructure
    //{

    //    private CommercialQuestionnaireCompanyStructureCompanyStructure companyStructureField;

    //    /// <remarks/>
    //    public CommercialQuestionnaireCompanyStructureCompanyStructure CompanyStructure
    //    {
    //        get
    //        {
    //            return this.companyStructureField;
    //        }
    //        set
    //        {
    //            this.companyStructureField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class CommercialQuestionnaireCompanyStructureCompanyStructure
    //{

    //    private string nameField;

    //    private string percentageShareholdingField;

    //    private string typeField;

    //    private string commentsField;

    //    /// <remarks/>
    //    public string Name
    //    {
    //        get
    //        {
    //            return this.nameField;
    //        }
    //        set
    //        {
    //            this.nameField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string PercentageShareholding
    //    {
    //        get
    //        {
    //            return this.percentageShareholdingField;
    //        }
    //        set
    //        {
    //            this.percentageShareholdingField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string Type
    //    {
    //        get
    //        {
    //            return this.typeField;
    //        }
    //        set
    //        {
    //            this.typeField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string Comments
    //    {
    //        get
    //        {
    //            return this.commentsField;
    //        }
    //        set
    //        {
    //            this.commentsField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class CommercialQuestionnaireEmployees
    //{

    //    private string numberofSalariedEmployeesField;

    //    private string numberofWagedEmployeesField;

    //    private string numberofCasualEmployeesField;

    //    private string numberofContractedEmployeesField;

    //    /// <remarks/>
    //    public string NumberofSalariedEmployees
    //    {
    //        get
    //        {
    //            return this.numberofSalariedEmployeesField;
    //        }
    //        set
    //        {
    //            this.numberofSalariedEmployeesField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string NumberofWagedEmployees
    //    {
    //        get
    //        {
    //            return this.numberofWagedEmployeesField;
    //        }
    //        set
    //        {
    //            this.numberofWagedEmployeesField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string NumberofCasualEmployees
    //    {
    //        get
    //        {
    //            return this.numberofCasualEmployeesField;
    //        }
    //        set
    //        {
    //            this.numberofCasualEmployeesField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string NumberofContractedEmployees
    //    {
    //        get
    //        {
    //            return this.numberofContractedEmployeesField;
    //        }
    //        set
    //        {
    //            this.numberofContractedEmployeesField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class CommercialQuestionnaireCompanyFleet
    //{

    //    private CommercialQuestionnaireCompanyFleetCompanyFleet companyFleetField;

    //    /// <remarks/>
    //    public CommercialQuestionnaireCompanyFleetCompanyFleet CompanyFleet
    //    {
    //        get
    //        {
    //            return this.companyFleetField;
    //        }
    //        set
    //        {
    //            this.companyFleetField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class CommercialQuestionnaireCompanyFleetCompanyFleet
    //{

    //    private string typeofvehicleField;

    //    private string numberofvehiclesField;

    //    private string ownedOrLeasedField;

    //    private string valueField;

    //    private string outstandingBalanceField;

    //    /// <remarks/>
    //    public string Typeofvehicle
    //    {
    //        get
    //        {
    //            return this.typeofvehicleField;
    //        }
    //        set
    //        {
    //            this.typeofvehicleField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string Numberofvehicles
    //    {
    //        get
    //        {
    //            return this.numberofvehiclesField;
    //        }
    //        set
    //        {
    //            this.numberofvehiclesField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string OwnedOrLeased
    //    {
    //        get
    //        {
    //            return this.ownedOrLeasedField;
    //        }
    //        set
    //        {
    //            this.ownedOrLeasedField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string Value
    //    {
    //        get
    //        {
    //            return this.valueField;
    //        }
    //        set
    //        {
    //            this.valueField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string OutstandingBalance
    //    {
    //        get
    //        {
    //            return this.outstandingBalanceField;
    //        }
    //        set
    //        {
    //            this.outstandingBalanceField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class CommercialQuestionnaireOperationAssesment
    //{

    //    private string operationalDetailField;

    //    private string industrySectorField;

    //    private string customersField;

    //    private string exportCountriesField;

    //    private string importCountriesField;

    //    /// <remarks/>
    //    public string OperationalDetail
    //    {
    //        get
    //        {
    //            return this.operationalDetailField;
    //        }
    //        set
    //        {
    //            this.operationalDetailField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string IndustrySector
    //    {
    //        get
    //        {
    //            return this.industrySectorField;
    //        }
    //        set
    //        {
    //            this.industrySectorField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string Customers
    //    {
    //        get
    //        {
    //            return this.customersField;
    //        }
    //        set
    //        {
    //            this.customersField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string ExportCountries
    //    {
    //        get
    //        {
    //            return this.exportCountriesField;
    //        }
    //        set
    //        {
    //            this.exportCountriesField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string ImportCountries
    //    {
    //        get
    //        {
    //            return this.importCountriesField;
    //        }
    //        set
    //        {
    //            this.importCountriesField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class CommercialQuestionnaireBEE
    //{

    //    private string levelField;

    //    private string certificateAvailableField;

    //    private string expirydateField;

    //    /// <remarks/>
    //    public string Level
    //    {
    //        get
    //        {
    //            return this.levelField;
    //        }
    //        set
    //        {
    //            this.levelField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string CertificateAvailable
    //    {
    //        get
    //        {
    //            return this.certificateAvailableField;
    //        }
    //        set
    //        {
    //            this.certificateAvailableField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string Expirydate
    //    {
    //        get
    //        {
    //            return this.expirydateField;
    //        }
    //        set
    //        {
    //            this.expirydateField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class CommercialQuestionnaireISO
    //{

    //    private CommercialQuestionnaireISOISO iSOField;

    //    /// <remarks/>
    //    public CommercialQuestionnaireISOISO ISO
    //    {
    //        get
    //        {
    //            return this.iSOField;
    //        }
    //        set
    //        {
    //            this.iSOField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class CommercialQuestionnaireISOISO
    //{

    //    private string statusField;

    //    private string iSOScoreField;

    //    private string certificateAvailableField;

    //    /// <remarks/>
    //    public string Status
    //    {
    //        get
    //        {
    //            return this.statusField;
    //        }
    //        set
    //        {
    //            this.statusField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string ISOScore
    //    {
    //        get
    //        {
    //            return this.iSOScoreField;
    //        }
    //        set
    //        {
    //            this.iSOScoreField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string CertificateAvailable
    //    {
    //        get
    //        {
    //            return this.certificateAvailableField;
    //        }
    //        set
    //        {
    //            this.certificateAvailableField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class CommercialQuestionnaireWorkmansCompensation
    //{

    //    private string statusField;

    //    private string certificateAvailableField;

    //    /// <remarks/>
    //    public string Status
    //    {
    //        get
    //        {
    //            return this.statusField;
    //        }
    //        set
    //        {
    //            this.statusField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string CertificateAvailable
    //    {
    //        get
    //        {
    //            return this.certificateAvailableField;
    //        }
    //        set
    //        {
    //            this.certificateAvailableField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class CommercialQuestionnaireTradeReferences
    //{

    //    private CommercialQuestionnaireTradeReferencesTradeReferences tradeReferencesField;

    //    /// <remarks/>
    //    public CommercialQuestionnaireTradeReferencesTradeReferences TradeReferences
    //    {
    //        get
    //        {
    //            return this.tradeReferencesField;
    //        }
    //        set
    //        {
    //            this.tradeReferencesField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class CommercialQuestionnaireTradeReferencesTradeReferences
    //{

    //    private string supplierField;

    //    private string supplierContactField;

    //    /// <remarks/>
    //    public string Supplier
    //    {
    //        get
    //        {
    //            return this.supplierField;
    //        }
    //        set
    //        {
    //            this.supplierField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string SupplierContact
    //    {
    //        get
    //        {
    //            return this.supplierContactField;
    //        }
    //        set
    //        {
    //            this.supplierContactField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class CommercialQuestionnaireFinancials
    //{

    //    private string currentassetsField;

    //    private string inventoryField;

    //    private string accountsReceivableField;

    //    private string fixedassetsField;

    //    private string plantEquipmentVehiclesField;

    //    private string propertyField;

    //    private string currentliabilitiesField;

    //    private string accountsPayableField;

    //    private string shortTermsLoansField;

    //    private string longTermLoansField;

    //    private string longtermliabilitiesField;

    //    private string mortgageBondsField;

    //    private string previousYearTurnoverField;

    //    private string currentTurnoverpermonthField;

    //    private string currentTurnoverperannumField;

    //    /// <remarks/>
    //    public string Currentassets
    //    {
    //        get
    //        {
    //            return this.currentassetsField;
    //        }
    //        set
    //        {
    //            this.currentassetsField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string Inventory
    //    {
    //        get
    //        {
    //            return this.inventoryField;
    //        }
    //        set
    //        {
    //            this.inventoryField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string AccountsReceivable
    //    {
    //        get
    //        {
    //            return this.accountsReceivableField;
    //        }
    //        set
    //        {
    //            this.accountsReceivableField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string Fixedassets
    //    {
    //        get
    //        {
    //            return this.fixedassetsField;
    //        }
    //        set
    //        {
    //            this.fixedassetsField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string PlantEquipmentVehicles
    //    {
    //        get
    //        {
    //            return this.plantEquipmentVehiclesField;
    //        }
    //        set
    //        {
    //            this.plantEquipmentVehiclesField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string Property
    //    {
    //        get
    //        {
    //            return this.propertyField;
    //        }
    //        set
    //        {
    //            this.propertyField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string Currentliabilities
    //    {
    //        get
    //        {
    //            return this.currentliabilitiesField;
    //        }
    //        set
    //        {
    //            this.currentliabilitiesField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string AccountsPayable
    //    {
    //        get
    //        {
    //            return this.accountsPayableField;
    //        }
    //        set
    //        {
    //            this.accountsPayableField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string ShortTermsLoans
    //    {
    //        get
    //        {
    //            return this.shortTermsLoansField;
    //        }
    //        set
    //        {
    //            this.shortTermsLoansField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string LongTermLoans
    //    {
    //        get
    //        {
    //            return this.longTermLoansField;
    //        }
    //        set
    //        {
    //            this.longTermLoansField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string Longtermliabilities
    //    {
    //        get
    //        {
    //            return this.longtermliabilitiesField;
    //        }
    //        set
    //        {
    //            this.longtermliabilitiesField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string MortgageBonds
    //    {
    //        get
    //        {
    //            return this.mortgageBondsField;
    //        }
    //        set
    //        {
    //            this.mortgageBondsField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string PreviousYearTurnover
    //    {
    //        get
    //        {
    //            return this.previousYearTurnoverField;
    //        }
    //        set
    //        {
    //            this.previousYearTurnoverField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string CurrentTurnoverpermonth
    //    {
    //        get
    //        {
    //            return this.currentTurnoverpermonthField;
    //        }
    //        set
    //        {
    //            this.currentTurnoverpermonthField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string CurrentTurnoverperannum
    //    {
    //        get
    //        {
    //            return this.currentTurnoverperannumField;
    //        }
    //        set
    //        {
    //            this.currentTurnoverperannumField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class CommercialQuestionnaireNewBankCodes
    //{

    //    private string accountNameField;

    //    private string bankField;

    //    private string branchNameField;

    //    private string branchCodeField;

    //    private string accountNumberField;

    //    /// <remarks/>
    //    public string AccountName
    //    {
    //        get
    //        {
    //            return this.accountNameField;
    //        }
    //        set
    //        {
    //            this.accountNameField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string Bank
    //    {
    //        get
    //        {
    //            return this.bankField;
    //        }
    //        set
    //        {
    //            this.bankField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string BranchName
    //    {
    //        get
    //        {
    //            return this.branchNameField;
    //        }
    //        set
    //        {
    //            this.branchNameField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string BranchCode
    //    {
    //        get
    //        {
    //            return this.branchCodeField;
    //        }
    //        set
    //        {
    //            this.branchCodeField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string AccountNumber
    //    {
    //        get
    //        {
    //            return this.accountNumberField;
    //        }
    //        set
    //        {
    //            this.accountNumberField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class CommercialQuestionnaireContracts
    //{

    //    private CommercialQuestionnaireContractsContracts contractsField;

    //    /// <remarks/>
    //    public CommercialQuestionnaireContractsContracts Contracts
    //    {
    //        get
    //        {
    //            return this.contractsField;
    //        }
    //        set
    //        {
    //            this.contractsField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class CommercialQuestionnaireContractsContracts
    //{

    //    private string contractValueField;

    //    private string remarksField;

    //    private string mainContractorField;

    //    /// <remarks/>
    //    public string ContractValue
    //    {
    //        get
    //        {
    //            return this.contractValueField;
    //        }
    //        set
    //        {
    //            this.contractValueField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string Remarks
    //    {
    //        get
    //        {
    //            return this.remarksField;
    //        }
    //        set
    //        {
    //            this.remarksField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string MainContractor
    //    {
    //        get
    //        {
    //            return this.mainContractorField;
    //        }
    //        set
    //        {
    //            this.mainContractorField = value;
    //        }
    //    }
    //}



}
