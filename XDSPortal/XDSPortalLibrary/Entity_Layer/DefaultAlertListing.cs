﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XDSPortalLibrary.Entity_Layer
{
    public class DefaultAlertListing
    {

        private string stridno = string.Empty, strPassportNo = string.Empty, strSecondName = string.Empty, strSurName = string.Empty, strFirstName = string.Empty, strGender = string.Empty;
        private string strAccountNo = string.Empty, strSubAccountNo = string.Empty, strAccountype = string.Empty, strLoadedIndicator = string.Empty, strStatusCode = string.Empty, strEffectiveDate = string.Empty, strAmount = string.Empty;
        private string strcomments = string.Empty, strVatNumber = string.Empty, strHometelephone = string.Empty, strWorkTelephone = string.Empty, strMobile = string.Empty, strAddress1 = string.Empty, strAddress2 = string.Empty, strAddress3 = string.Empty, strAddress4 = string.Empty, strEmailId = string.Empty, strPostalcode = string.Empty, strCreatedbyuser = string.Empty;

        public string VatNumber
        {
            get { return this.strSurName; }
            set { this.strSurName = value; }
        }

        public string Surname
        {
            get { return this.strSurName; }
            set { this.strSurName = value; }
        }
        public string SecondName
        {
            get { return this.strSecondName; }
            set { this.strSecondName = value; }
        }

        public string Effectivedate
        {
            get { return Effectivedate; }
            set { Effectivedate = value; }
        }

        public string PassportNo
        {
            get { return this.strPassportNo; }
            set { this.strPassportNo = value; }
        }

        public string idno
        {
            get { return this.stridno; }
            set { this.stridno = value; }
        }
        public string FirstName
        {
            get { return this.strFirstName; }
            set { this.strFirstName = value; }
        }
        public DateTime BirthDate
        {
            get { return BirthDate; }
            set { BirthDate = value; }
        }
        public string Gender
        {
            get { return this.strGender; }
            set { this.strGender = value; }
        }
        public string AccountNo
        {
            get { return this.strAccountNo; }
            set { this.strAccountNo = value; }
        }
        public string SubAccountNo
        {
            get { return this.strSubAccountNo; }
            set { this.strSubAccountNo = value; }
        }
        public string Accountype
        {
            get { return this.strAccountype; }
            set { this.strAccountype = value; }
        }
        public string LoadedIndicator
        {
            get { return this.strLoadedIndicator; }
            set { this.strLoadedIndicator = value; }
        }
        public string StatusCode
        {
            get { return this.strStatusCode; }
            set { this.strStatusCode = value; }
        }

        public string Amount
        {
            get { return this.strAmount; }
            set { this.strAmount = value; }
        }
        public string comments
        {
            get { return this.strcomments; }
            set { this.strcomments = value; }
        }
        public string Hometelephone
        {
            get { return this.strHometelephone; }
            set { this.strHometelephone = value; }
        }
        public string WorkTelephone
        {
            get { return this.strWorkTelephone; }
            set { this.strWorkTelephone = value; }
        }
        public string Mobile
        {
            get { return this.strMobile; }
            set { this.strMobile = value; }
        }
        public string Address1
        {
            get { return this.strAddress1; }
            set { this.strAddress1 = value; }
        }

        public string Address2
        {
            get { return this.strAddress2; }
            set { this.strAddress2 = value; }
        }

        public string Address3
        {
            get { return this.strAddress3; }
            set { this.strAddress3 = value; }
        }

        public string Address4
        {
            get { return this.strAddress4; }
            set { this.strAddress4 = value; }
        }
        public string EmailId
        {
            get { return this.strEmailId; }
            set { this.strEmailId = value; }
        }
        public string Postalcode
        {
            get { return this.strPostalcode; }
            set { this.strPostalcode = value; }
        }
        public string Createdbyuser
        {
            get { return this.strCreatedbyuser; }
            set { this.strCreatedbyuser = value; }
        }

        private string strCommercialName = string.Empty, strRegistrationNumber = string.Empty;

        public string CommercialName
        {
            get { return this.strCommercialName; }
            set { this.strCommercialName = value; }
        }

        public string RegistrationNumber
        {
            get { return this.strRegistrationNumber; }
            set { this.strRegistrationNumber = value; }
        }

    }
}
