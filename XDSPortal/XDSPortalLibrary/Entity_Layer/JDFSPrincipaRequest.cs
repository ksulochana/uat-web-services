﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XDSPortalLibrary.Entity_Layer
{
    public partial class JDFSPrincipaRequest
    {
        public JDFSPrincipaRequest() { }


        public enum IDType {SAID,PASSPORT,OTHER,NONE};

        //private string _ReqRepository = string.Empty;
        //private string _Company = string.Empty;
        //private string _Branch = string.Empty;
        //private string _User = string.Empty;
        //private string _Password = string.Empty;
        //private string _Ruleset = string.Empty;
        //private string _ExistingClient = string.Empty;
        //private string _Title = string.Empty;
        //private string _FirstName = string.Empty;
        //private string _SurName = string.Empty;
        //private IDType _IDType = IDType.NONE;
        //private string _IDnumber = string.Empty;
        //private string _Birthdate = string.Empty;
        //private string _Gender = string.Empty;
        //private string _Marital_Status = string.Empty;
        //private int _NumberofDependants = 0;
        //private string _AddressLine1 = string.Empty;
        //private string _AddressLine2 = string.Empty;
        //private string _Suburb = string.Empty;
        //private string _City = string.Empty;
        //private int _PhysicalPostalCode = 0;
        //private string _ProvinceCode = string.Empty;
        //private string _ResidentialStatus = string.Empty;
        //private int _numberofYearsAtEmployer = 0;
        //private float _GrossIncome = 0;
        //private float _Income = 0;
        //private string _BrandName = string.Empty;
        //private int _CustomerAge = 0;
        //private string _EmploymentStatus = string.Empty;
        //private string _Reference = string.Empty;
        //private string _BankIndicator = string.Empty;
        //private int _NumberOfMonthsAtEmployer = 0;
        //private string _MaritalStatus = string.Empty;
        //private string _ClientReference = string.Empty;

        //public string ReqRepository { get { return this._ReqRepository; } set{ this._ReqRepository = value; }}
        //public string Company { get { return this._Company; } set{ this._Company = value; }}
        //public string Branch { get { return this._Branch; } set { this._Branch = value; } }
        //public string User { get { return this._User; } set { this._User = value; } }
        //public string Password { get { return this._Password; } set { this._Password = value; } }
        //public string Ruleset { get { return this._Ruleset; } set { this._Ruleset = value; } }
        //public string ExistingClient { get { return this._ExistingClient; } set { this._ExistingClient = value; } }
        //public string Title { get { return this._Title; } set { this._Title = value; } }
        //public string FirstName { get { return this._FirstName; } set { this._FirstName = value; } }
        //public string SurName { get { return this._SurName; } set { this._SurName = value; } }
        //public IDType _IDType = IDType.NONE;
        //public string IDnumber { get { return this._IDnumber; } set { this._IDnumber = value; } }
        //public string Birthdate { get { return this._Birthdate; } set { this._Birthdate = value; } }
        //public string Gender { get { return this._Gender; } set { this._Gender = value; } }
        //public string Marital_Status { get { return this._Marital_Status; } set { this._Marital_Status = value; } }
        //public int _NumberofDependants = 0;
        //public string _AddressLine1 { get { return this._User; } set { this._User = value; } }
        //public string _AddressLine2 { get { return this._User; } set { this._User = value; } }
        //public string _Suburb { get { return this._User; } set { this._User = value; } }
        //public string _City { get { return this._User; } set { this._User = value; } }
        //public int _PhysicalPostalCode = 0;
        //public string _ProvinceCode { get { return this._User; } set { this._User = value; } }
        //public string _ResidentialStatus { get { return this._User; } set { this._User = value; } }
        //public int _numberofYearsAtEmployer = 0;
        //public float _GrossIncome = 0;
        //public float _Income = 0;
        //public string _BrandName { get { return this._User; } set { this._User = value; } }
        //public int _CustomerAge = 0;
        //public string _EmploymentStatus { get { return this._User; } set { this._User = value; } }
        //public string _Reference { get { return this._User; } set { this._User = value; } }
        //public string _BankIndicator { get { return this._User; } set { this._User = value; } }
        //public int _NumberOfMonthsAtEmployer = 0;
        //public string _MaritalStatus { get { return this._User; } set { this._User = value; } }
        //public string _ClientReference { get { return this._User; } set { this._User = value; } }


    }

    //------------------------------------------------------------------------------
    // <auto-generated>
    //     This code was generated by a tool.
    //     Runtime Version:2.0.50727.8000
    //
    //     Changes to this file may cause incorrect behavior and will be lost if
    //     the code is regenerated.
    // </auto-generated>
    //------------------------------------------------------------------------------


    // 
    // This source code was auto-generated by xsd, Version=2.0.50727.1432.
    // 


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.1432")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class Submit
    {

        private SubmitInput itemsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("input")]
        public SubmitInput Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.1432")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class SubmitInput
    {

        private string reqRepositoryField;

        private string companyField;

        private string branchField;

        private string userField;

        private string passwordField;

        private string ruleSetField;

        private string existingClientField;

        private string titleField;

        private string firstNameField;

        private string surnameField;

        private string iDTypeField;

        private string iDNumberField;

        private string birthDateField;

        private string genderField;

        private string marital_StatusField;

        private string numberOfDependantsField;

        private string addressLine1Field;

        private string addressLine2Field;

        private string suburbField;

        private string cityField;

        private string physicalPostalCodeField;

        private string provinceCodeField;

        private string residentialStatusField;

        private string numberOfYearsAtEmployerField;

        private string grossIncomeField;

        private string incomeField;

        private string brandNameField;

        private string customerAgeField;

        private string employmentStatusField;

        private string referenceField;

        private string bankIndicatorField;

        private string numberOfMonthsAtEmployerField;

        private string maritalStatusField;

        private string clientReferenceField;

        /// <remarks/>
        public string ReqRepository
        {
            get
            {
                return this.reqRepositoryField;
            }
            set
            {
                this.reqRepositoryField = value;
            }
        }

        /// <remarks/>
        public string Company
        {
            get
            {
                return this.companyField;
            }
            set
            {
                this.companyField = value;
            }
        }

        /// <remarks/>
        public string Branch
        {
            get
            {
                return this.branchField;
            }
            set
            {
                this.branchField = value;
            }
        }

        /// <remarks/>
        public string User
        {
            get
            {
                return this.userField;
            }
            set
            {
                this.userField = value;
            }
        }

        /// <remarks/>
        public string Password
        {
            get
            {
                return this.passwordField;
            }
            set
            {
                this.passwordField = value;
            }
        }

        /// <remarks/>
        public string RuleSet
        {
            get
            {
                return this.ruleSetField;
            }
            set
            {
                this.ruleSetField = value;
            }
        }

        /// <remarks/>
        public string ExistingClient
        {
            get
            {
                return this.existingClientField;
            }
            set
            {
                this.existingClientField = value;
            }
        }

        /// <remarks/>
        public string Title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>
        public string FirstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        public string IDType
        {
            get
            {
                return this.iDTypeField;
            }
            set
            {
                this.iDTypeField = value;
            }
        }

        /// <remarks/>
        public string IDNumber
        {
            get
            {
                return this.iDNumberField;
            }
            set
            {
                this.iDNumberField = value;
            }
        }

        /// <remarks/>
        public string BirthDate
        {
            get
            {
                return this.birthDateField;
            }
            set
            {
                this.birthDateField = value;
            }
        }

        /// <remarks/>
        public string Gender
        {
            get
            {
                return this.genderField;
            }
            set
            {
                this.genderField = value;
            }
        }

        /// <remarks/>
        public string Marital_Status
        {
            get
            {
                return this.marital_StatusField;
            }
            set
            {
                this.marital_StatusField = value;
            }
        }

        /// <remarks/>
        public string NumberOfDependants
        {
            get
            {
                return this.numberOfDependantsField;
            }
            set
            {
                this.numberOfDependantsField = value;
            }
        }

        /// <remarks/>
        public string AddressLine1
        {
            get
            {
                return this.addressLine1Field;
            }
            set
            {
                this.addressLine1Field = value;
            }
        }

        /// <remarks/>
        public string AddressLine2
        {
            get
            {
                return this.addressLine2Field;
            }
            set
            {
                this.addressLine2Field = value;
            }
        }

        /// <remarks/>
        public string Suburb
        {
            get
            {
                return this.suburbField;
            }
            set
            {
                this.suburbField = value;
            }
        }

        /// <remarks/>
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        public string PhysicalPostalCode
        {
            get
            {
                return this.physicalPostalCodeField;
            }
            set
            {
                this.physicalPostalCodeField = value;
            }
        }

        /// <remarks/>
        public string ProvinceCode
        {
            get
            {
                return this.provinceCodeField;
            }
            set
            {
                this.provinceCodeField = value;
            }
        }

        /// <remarks/>
        public string ResidentialStatus
        {
            get
            {
                return this.residentialStatusField;
            }
            set
            {
                this.residentialStatusField = value;
            }
        }

        /// <remarks/>
        public string NumberOfYearsAtEmployer
        {
            get
            {
                return this.numberOfYearsAtEmployerField;
            }
            set
            {
                this.numberOfYearsAtEmployerField = value;
            }
        }

        /// <remarks/>
        public string GrossIncome
        {
            get
            {
                return this.grossIncomeField;
            }
            set
            {
                this.grossIncomeField = value;
            }
        }

        /// <remarks/>
        public string Income
        {
            get
            {
                return this.incomeField;
            }
            set
            {
                this.incomeField = value;
            }
        }

        /// <remarks/>
        public string BrandName
        {
            get
            {
                return this.brandNameField;
            }
            set
            {
                this.brandNameField = value;
            }
        }

        /// <remarks/>
        public string CustomerAge
        {
            get
            {
                return this.customerAgeField;
            }
            set
            {
                this.customerAgeField = value;
            }
        }

        /// <remarks/>
        public string EmploymentStatus
        {
            get
            {
                return this.employmentStatusField;
            }
            set
            {
                this.employmentStatusField = value;
            }
        }

        /// <remarks/>
        public string Reference
        {
            get
            {
                return this.referenceField;
            }
            set
            {
                this.referenceField = value;
            }
        }

        /// <remarks/>
        public string BankIndicator
        {
            get
            {
                return this.bankIndicatorField;
            }
            set
            {
                this.bankIndicatorField = value;
            }
        }

        /// <remarks/>
        public string NumberOfMonthsAtEmployer
        {
            get
            {
                return this.numberOfMonthsAtEmployerField;
            }
            set
            {
                this.numberOfMonthsAtEmployerField = value;
            }
        }

        /// <remarks/>
        public string MaritalStatus
        {
            get
            {
                return this.maritalStatusField;
            }
            set
            {
                this.maritalStatusField = value;
            }
        }

        /// <remarks/>
        public string ClientReference
        {
            get
            {
                return this.clientReferenceField;
            }
            set
            {
                this.clientReferenceField = value;
            }
        }

    }
}
