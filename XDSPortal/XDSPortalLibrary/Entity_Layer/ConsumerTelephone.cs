﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace XDSPortalLibrary.Entity_Layer
{
   public class ConsumerTelephone
    {
       int subscriberIDvalue, productIDvalue, ConsumerIDValue = 0;
       string TelePhoneCodevalue = string.Empty, strTmpReference = string.Empty, TelephoneNovalue = string.Empty, SurNamevalue = string.Empty, ExternalReferencevalue = string.Empty, SearchTypevalue = string.Empty, ReferenceNoValue = string.Empty;
       bool ConfirmationChkBoxvalue, IsConsumerMatchingvalue = false;
       DataSet BonusSegmentsDS;
       private bool boolBonusCheck = false;
       private string strdataSegments = string.Empty;
       private string strAssociationCode = string.Empty;

       public string SubscriberAssociationCode
       {
           get { return this.strAssociationCode; }
           set { this.strAssociationCode = value; }
       }

       public string DataSegments
       {
           get { return this.strdataSegments; }
           set { this.strdataSegments = value; }
       }

       public bool BonusCheck
       {
           get { return this.boolBonusCheck; }
           set { this.boolBonusCheck = value; }
       }

        public ConsumerTelephone()
        {
        }
        public int ConsumerID
        {
            get
            {
                return this.ConsumerIDValue;
            }
            set
            {
                this.ConsumerIDValue = value;
            }
        }
        public DataSet BonusSegments
        {
            get
            {
                return this.BonusSegmentsDS;
            }
            set
            {
                this.BonusSegmentsDS = value;
            }
        }
        public string ReferenceNo
        {
            get
            {
                if (String.IsNullOrEmpty(this.ReferenceNoValue))
                    return DBNull.Value.ToString();
                else
                    return this.ReferenceNoValue;
            }
            set
            {
                this.ReferenceNoValue = value;
            }
        }
        public int subscriberID
        {
            get
            {
                if (String.IsNullOrEmpty(this.subscriberIDvalue.ToString()))
                    return Convert.ToInt16(DBNull.Value);
                else
                    return this.subscriberIDvalue;
            }
            set
            {
                this.subscriberIDvalue = value;
            }
        }
        public int ProductID
        {
            get
            {
                if (String.IsNullOrEmpty(this.subscriberIDvalue.ToString()))
                    return Convert.ToInt16(DBNull.Value);
                else
                    return this.productIDvalue;
            }
            set
            {
                this.productIDvalue = value;
            }
        }
        public string TelephoneCode
        {
            get
            {
                if (String.IsNullOrEmpty(TelePhoneCodevalue))
                    return DBNull.Value.ToString();
                else
                    return this.TelePhoneCodevalue;
            }
            set
            {
                this.TelePhoneCodevalue = value;
            }
        }
        public string TelePhoneNo
        {
            get
            {
                if (String.IsNullOrEmpty(TelephoneNovalue))
                    return DBNull.Value.ToString();
                else
                    return this.TelephoneNovalue;
            }
            set
            {
                this.TelephoneNovalue = value;
            }
        }
        public string Surname
        {
            get
            {
                if (String.IsNullOrEmpty(this.SurNamevalue))
                    return DBNull.Value.ToString();
                else
                    return this.SurNamevalue;
            }
            set
            {
                this.SurNamevalue = value;
            }
        }
        public string ExternalReference
        {
            get
            {
                if (String.IsNullOrEmpty(ExternalReferencevalue))
                    return DBNull.Value.ToString();
                else
                    return this.ExternalReferencevalue;
            }
            set
            {
                this.ExternalReferencevalue = value;
            }
        }
        public string SearchType
        {
            get
            {
                if (String.IsNullOrEmpty(SearchTypevalue))
                    return DBNull.Value.ToString();
                else
                    return this.SearchTypevalue;
            }
            set
            {
                this.SearchTypevalue = value;
            }
        }
        public bool ConfirmationChkBox
        {
            get
            {
                return this.ConfirmationChkBoxvalue;
            }
            set
            {
                this.ConfirmationChkBoxvalue = value;
            }
        }

        public bool IsConsumerMatching
        {
            get
            {
                return this.IsConsumerMatchingvalue;
            }
            set
            {
                this.IsConsumerMatchingvalue = value;
            }
        }
        public string TmpReference
        {
            get
            {
                if (String.IsNullOrEmpty(this.strTmpReference))
                    return DBNull.Value.ToString();
                else
                    return this.strTmpReference;
            }
            set
            {
                this.strTmpReference = value;
            }
        }
   
    }
}
