﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace XDSPortalLibrary.Entity_Layer
{
    public class AuthenticationQuestionDocument
    {
        public AuthenticationQuestionDocument()
        {
        }

        public int EnquiryID = 0;
        public int EnquiryresultID = 0;
        public int SubscriberAuthenticationID = 0;
        public bool PotentialFraudflag = false;
        public string PotentialFraudReason = string.Empty;
        public string PotentialFraudComments = string.Empty;
        public string ReasonForVoid = string.Empty;
        public string VoidComments = string.Empty;
        public string ErrorMessage = string.Empty;
        public bool IsSuccess = true;
        public string ConsumerAccontAgeMessage = string.Empty;

        public string AuthenticationStatusInd = string.Empty;
        public string AuthenticationTypeInd = "P";
        public string ReferenceNo = string.Empty;
        public int TotalQuestionPointValue = 90;
        public decimal RequiredAuthenticatedPerc = 70;
        public decimal AuthenticatedPerc = 0;

        public int SubscriberID = 0;
        public int ActionedbySystemUserID = 0;



        [XmlArray("AuthenticationQuestions")]
        [XmlArrayItem("Question")]
        public AuthenticationQuestion[] Questions;
    }

    public class AuthenticationQuestion
       {
           public long ProductAuthenticationQuestionID;
           public string Question;
           public string AnswerStatusInd;
           public int QuestionPointValue;
           public int RequiredNoOfAnswers;
           [XmlArray("Answers")]
           [XmlArrayItem("AuthenticationAnswer")]
           public AuthenticationAnswer[] Answers;
       }

    public class AuthenticationAnswer
       {
           public long AnswerID;
           public string Answer;
           private bool IsCorrectAnswerYN;
           public bool IsEnteredAnswerYN;
       }

    
}
