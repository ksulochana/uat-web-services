﻿using System.Collections.Generic;

namespace XDSPortalLibrary.Entity_Layer.MDP
{

    public class QualificationResponse
    {
        public string idNo { get; set; }
        public string consumerId { get; set; }
        public List<Xac> xac { get; set; }
    }

    public class Xac
    {
        public string name { get; set; }
        public string value { get; set; }
        public string description { get; set; }
    }
}
