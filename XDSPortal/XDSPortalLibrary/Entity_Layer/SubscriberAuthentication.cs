﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XDSPortalLibrary.Entity_Layer
{
    public class SubscriberAuthentication
    {
        public SubscriberAuthentication()
        {
        }

        private int _SubscriberID = 0;
        private int _SubscriberAuthenticationID =0;
        private string _AuthenticationTypeInd = string.Empty;
        private DateTime _AuthenticationDate = DateTime.Now;
        private string _ReferenceNo = string.Empty;
        private string _AuthenticationStatusInd = string.Empty;
        private decimal _RequiredAuthenticatedPerc = 0;
        private decimal _AuthenticatedPerc = 0;
        private decimal _TotalQuestionPointValue = 0;
        private bool _IDNoVerificationYN = false;
        private string _RepeatAuthenticationMessage = string.Empty;
        private string _AuthenticationComment = string.Empty;
        private int _ConsumerID = 0;
        private int _ActionedBySystemUserID = 0;
        private string _SubscriberBranchCode = string.Empty;
        private int _AuthenticationPurposeID = 0;
        private int _PreviousSubscriberAuthenticationID = 0;
        private string _CreatedByUser = string.Empty;
        private DateTime _CreatedOnDate = DateTime.Now;
        private string _ChangedByUser =string.Empty;
        private DateTime _ChangedOnDate = DateTime.Now;
        private int _HomeAffairsID = 0;
        private string _ConsumerAccountAgeMessage = string.Empty;
        private int _PreviousSubscriberID = 0;
        private string _SubscriberBillingGroupCode = string.Empty;
        private int _ProductID = 0;
        private bool _SafpsIndicator = false;
        private int _RetryCount = 0;
        private int _EnquiryID = 0;
        private int _EnquiryResultID = 0;
        private string _Reason = string.Empty;
        private string _EncryptedReferenceNo = string.Empty;
        private string _CellularNumber = string.Empty;
        private string _SAFPSMessage = string.Empty;
        private bool _OverrideOTP = false;
        private string _OverrideOTPReason = "";
        private string _EmailAddress = "";
        private string _IDIssuedDate = string.Empty;
        private int _NoofmandatoryQuestions = 0;
        private bool _MandatorycriteriaMet = true;

        //V2.2
        private int _OTPRetryCount = 0;

        public int SubscriberID
        {
            get { return this._SubscriberID; }
            set { this._SubscriberID = value; }
        }

        public int SubscriberAuthenticationID
        {
            get { return this._SubscriberAuthenticationID; }
            set { this._SubscriberAuthenticationID = value; }
        }
        public string AuthenticationTypeInd
        {
            get { return this._AuthenticationTypeInd; }
            set { this._AuthenticationTypeInd = value; }
        }

        public DateTime AuthenticationDate
        {
            get { return this._AuthenticationDate; }
            set { this._AuthenticationDate = value; }
        }

        public string ReferenceNo
        {
            get { return this._ReferenceNo; }
            set { this._ReferenceNo = value; }
        }
        
        public string AuthenticationStatusInd
        {
            get { return this._AuthenticationStatusInd; }
            set { this._AuthenticationStatusInd = value; }
        }

        public decimal RequiredAuthenticatedPerc
        {
            get { return this._RequiredAuthenticatedPerc; }
            set { this._RequiredAuthenticatedPerc = value; }
        }

        public decimal AuthenticatedPerc
        {
            get { return this._AuthenticatedPerc; }
            set { this._AuthenticatedPerc = value; }
        }

        public decimal TotalQuestionPointValue
        {
            get { return this._TotalQuestionPointValue; }
            set { this._TotalQuestionPointValue = value; }
        }

        public bool IDNoVerificationYN
        {
            get { return this._IDNoVerificationYN; }
            set { this._IDNoVerificationYN = value; }
        }

        public string RepeatAuthenticationMessage
        {
            get { return this._RepeatAuthenticationMessage; }
            set { this._RepeatAuthenticationMessage = value; }
        }

        public string AuthenticationComment
        {
            get { return this._AuthenticationComment; }
            set { this._AuthenticationComment = value; }
        }

        public int ConsumerID
        {
            get { return this._ConsumerID; }
            set { this._ConsumerID = value; }
        }

        public int ActionedBySystemUserID
        {
            get { return this._ActionedBySystemUserID; }
            set { this._ActionedBySystemUserID = value; }
        }

        public string SubscriberBranchCode
        {
            get { return this._SubscriberBranchCode; }
            set { this._SubscriberBranchCode = value; }
        }

        public int AuthenticationPurposeID
        {
            get { return this._AuthenticationPurposeID; }
            set { this._AuthenticationPurposeID = value; }
        }

        public int PreviousSubscriberAuthenticationID
        {
            get { return this._PreviousSubscriberAuthenticationID; }
            set { this._PreviousSubscriberAuthenticationID = value; }
        }
        public string CreatedByUser
        {
            get { return this._CreatedByUser; }
            set { this._CreatedByUser = value; }
        }

        public DateTime CreatedOnDate
        {
            get { return this._CreatedOnDate; }
            set { this._CreatedOnDate = value; }
        }

        public string ChangedByUser
        {
            get { return this._ChangedByUser; }
            set { this._ChangedByUser = value; }
        }

        public DateTime ChangedOnDate
        {
            get { return this._ChangedOnDate; }
            set { this._ChangedOnDate = value; }
        }

        public int HomeAffairsID
        {
            get { return this._HomeAffairsID; }
            set { this._HomeAffairsID = value; }
        }

        public string ConsumerAccountAgeMessage
        {
            get { return this._ConsumerAccountAgeMessage; }
            set { this._ConsumerAccountAgeMessage = value; }
        }

        public int PreviousSubscriberID
        {
            get { return this._PreviousSubscriberID; }
            set { this._PreviousSubscriberID = value; }
        }

        public string SubscriberBillingGroupCode
        {
            get { return this._SubscriberBillingGroupCode; }
            set { this._SubscriberBillingGroupCode = value; }
        }

        public int ProductID
        {
            get { return this._ProductID; }
            set { this._ProductID = value; }
        }

        public bool SafpsIndicator
        {
            get { return this._SafpsIndicator; }
            set { this._SafpsIndicator = value; }
        }
        public int RetryCount
        {
            get { return this._RetryCount; }
            set { this._RetryCount = value; }
        }
        public int EnquiryID
        {
            get { return this._EnquiryID; }
            set { this._EnquiryID = value; }
        }
        public int EnquiryResultID
        {
            get { return this._EnquiryResultID; }
            set { this._EnquiryResultID = value; }
        }
        public string Reason
        {
            get { return this._Reason; }
            set { this._Reason = value; }
        }
        public string EncryptedReferenceNo
        {
            get { return this._EncryptedReferenceNo; }
            set { this._EncryptedReferenceNo = value; }
        }
        public string CellularNumber
        {
            get { return this._CellularNumber; }
            set { this._CellularNumber =value; }
        }
        public string SAFPSMessage
        {
            get{return this._SAFPSMessage;}
            set{this._SAFPSMessage=value;}
        }

        public bool OverrideOTP
        {
            get { return this._OverrideOTP; }
            set { this._OverrideOTP = value; }
        }

        public string OverrideOTPReason
        {
            get { return this._OverrideOTPReason; }
            set { this._OverrideOTPReason = value; }
        }

        public string EmailAddress
        {
            get { return this._EmailAddress; }
            set { this._EmailAddress = value; }
        }

        //V2.2
        public int OTPRetryCount
        {
            get { return this._OTPRetryCount; }
            set { this._OTPRetryCount = value; }
        }

        public string IDIssuedDate
        {
            get { return this._IDIssuedDate; }
            set { this._IDIssuedDate = value; }
        }

        public int NoofmandatoryQuestions
        {
            get { return this._NoofmandatoryQuestions; }
            set { this._NoofmandatoryQuestions = value; }
        }
        public bool MandatoryCriteriaMet
        {
            get { return this._MandatorycriteriaMet; }
            set { this._MandatorycriteriaMet = value; }
        }
    }
}
