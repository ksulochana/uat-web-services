﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XDSPortalLibrary.Entity_Layer
{
    public  class NLRDailyRegistrations
    {
        public NLRDailyRegistrations()
        {
        }

        public enum EnquiryStatusInd { P, C ,F};

        private long nLRDailyRegistrationsIDField = 0;

        private string transactionIDField = string.Empty;

        private string transactionTypeField= string.Empty;

        private string enquiryReason1Field = string.Empty;

        private string subscriberCodeField = string.Empty;

        private string subscriberPasswordField = string.Empty;

        private string subscriberGroupIDField = string.Empty;

        private string subscriberOperatorIDField = string.Empty;

        private string subscriberOperatorPasswordField = string.Empty;

        private string transactionStatusField = string.Empty;

        private string errorCodeField = string.Empty;

        private string matchedRecordsFoundField = string.Empty;

        private string consIdentificationNoField = string.Empty;

        private string dateEnquirySentField = string.Empty;

        private string timeEnquirySentField = string.Empty;

        private string echoIdentityNoField = string.Empty;

        private string echoUniqueClientNoField = string.Empty;

        private string maxNoOfInquiriesField = string.Empty;

        private string maxNoOfNLRAccountsField = string.Empty;

        private string maxNoOfAssAddressesField = string.Empty;

        private string maxNoOfOtherEmployersField = string.Empty;

        private string maxNoOfAssHomeTelNosField = string.Empty;

        private string maxNoOfAssBusTelNosField = string.Empty;

        private string maxNoOfAssCellNosField = string.Empty;

        private string maxNoOfAssOtherTelNosField = string.Empty;

        private string filler1Field = string.Empty;

        private string loanRegNoField = string.Empty;

        private string supplierRefNoField = string.Empty;

        private string enquiryRefNoField = string.Empty;

        private string consSurnameField = string.Empty;

        private string forename1Field = string.Empty;

        private string forename2Field = string.Empty;

        private string forename3Field = string.Empty;

        private string spouseCurrSurnameField = string.Empty;

        private string spouseForename1Field = string.Empty;

        private string spouseForename2Field = string.Empty;

        private string consBirthDateField = string.Empty;

        private string consIdentityNoField = string.Empty;

        private string currAddrLn1Field = string.Empty;

        private string currAddrLn2Field = string.Empty;

        private string currAddrLn3Field = string.Empty;

        private string currAddrLn4Field = string.Empty;

        private string currAddressPostalCodeField = string.Empty;

        private string periodAtCurrAddressField = string.Empty;

        private string otherAddrLn1Field = string.Empty;

        private string otherAddrLn2Field = string.Empty;

        private string otherAddrLn3Field = string.Empty;

        private string otherAddrLn4Field = string.Empty;

        private string otherAddressPostalCodeField = string.Empty;

        private string ownerTenantAtCurrAddressField = string.Empty;

        private string consOccupationField = string.Empty;

        private string consEmployerField = string.Empty;

        private string consMaidenNameField = string.Empty;

        private string consAliasNameField = string.Empty;

        private string genderField = string.Empty;

        private string consMaritalStatusField = string.Empty;

        private string consHomeTelDiallingCodeField = string.Empty;

        private string consHomeTelNoField = string.Empty;

        private string consWorkTelDiallingCodeField = string.Empty;

        private string consWorkTelNoField = string.Empty;

        private string consCellularTelNoField = string.Empty;

        private string loanAmt1Field = string.Empty;

        private string instalmentAmtField = string.Empty;

        private string monthlySalaryField = string.Empty;

        private string salaryFrequencyField = string.Empty;

        private string repaymentPeriod1Field = string.Empty;

        private string enquiryReason2Field = string.Empty;

        private string filler2Field = string.Empty;

        private string branchCodeField = string.Empty;

        private string accountNoField = string.Empty;

        private string subAccountNoField = string.Empty;

        private string loanTypeField = string.Empty;

        private string dateLoanDisbursedField = string.Empty;

        private string loanAmt2Field = string.Empty;

        private string loanAmtBalanceIndicatorField = string.Empty;

        private string currBalanceField = string.Empty;

        private string currBalanceIndicatorField = string.Empty;

        private string monthlyInstalmentField = string.Empty;

        private string loadIndicatorField = string.Empty;

        private string repaymentPeriod2Field = string.Empty;

        private string loanPurposeField = string.Empty;

        private string totalAmtRepayableField = string.Empty;

        private string interestTypeField = string.Empty;

        private string annualRateForTotalChargeOfCreditField = string.Empty;

        private string randValueOfInterestChargesField = string.Empty;

        private string randValueOfTotalChargeOfCreditField = string.Empty;

        private string settlementAmtField = string.Empty;

        private string reAdvanceIndicatorField = string.Empty;

        private string filler3Field = string.Empty;

        private string createdByUserField = string.Empty;

        private System.DateTime createdOnDateField;

        private bool createdOnDateFieldSpecified = false;

        private System.DateTime changedOnDateField;

        private bool changedOnDateFieldSpecified = false;

        private string changedByUserField = string.Empty;

        private string recordStatusIndField = string.Empty;

        private int consIDField =0;

        private bool consIDFieldSpecified=false;

        private string nLRDailyFileDateField = string.Empty;

        private string subscriberNameField = string.Empty;

        private int subscriberIDField =0;

        private bool subscriberIDFieldSpecified= false;

        private int loaderIDField=0;

        private bool loaderIDFieldSpecified= false;

        private string nLRDailyRegFileNameField = string.Empty;

        private bool hasBeenSyncedField=false;

        private bool hasBeenSyncedFieldSpecified=false;

        private System.DateTime syncDateField;

        private bool syncDateFieldSpecified= false;

        private string oldBranchCodeField = string.Empty;

        private string oldAccountNoField = string.Empty;

        private string oldSubAccountNoField = string.Empty;

        private int xDSSubscriberIDField=0;

        private bool xDSSubscriberIDFieldSpecified=false;

        private bool isMatchedAccountField=false;

        private bool isMatchedAccountFieldSpecified=false;

        private string PassportNofield = string.Empty;

        private int EnquirySubscriberIDfield=0;

        private string Usernamefield = string.Empty;

        private int SystemUserIDfield=0;

        private string StatusIndfield = string.Empty;

        private int ConsumerIDfield=0;

        private string ErrorDescriptionfield = string.Empty;

        private int NLRDailyRegistrationsLogIDfield=0;

        private string SubscriberReferencefield = string.Empty;

        private DateTime subscriberEnquiryDatefield;

        private string VoucherCodefield = string.Empty;

        private int Payasyougofield=0;

        private bool DetailsViewedYNfield=false;

        private DateTime Detailsvieweddatefield;

        private int productIDfield=0;

        private int BillingtypeIdfield=0;

        private bool Billablefield=false;

        private string ExtraVarInput1field = string.Empty;

        private string ExtraVarInput2field = string.Empty;

        private string ExtraVarInput3field = string.Empty;

        private int ExtraIntInput1field=0;

        private int ExtraIntInput2field=0;

        private string XMLDatafield = string.Empty;

         ///<remarks/>
        public long NLRDailyRegistrationsID
        {
            get
            {
                return this.nLRDailyRegistrationsIDField;
            }
            set
            {
                this.nLRDailyRegistrationsIDField = value;
            }
        }

        /// <remarks/>
        public string TransactionID
        {
            get
            {
                return this.transactionIDField;
            }
            set
            {
                this.transactionIDField = value;
            }
        }

        /// <remarks/>
        public string TransactionType
        {
            get
            {
                return this.transactionTypeField;
            }
            set
            {
                this.transactionTypeField = value;
            }
        }

        /// <remarks/>
        public string EnquiryReason1
        {
            get
            {
                return this.enquiryReason1Field;
            }
            set
            {
                this.enquiryReason1Field = value;
            }
        }

        /// <remarks/>
        public string SubscriberCode
        {
            get
            {
                return this.subscriberCodeField;
            }
            set
            {
                this.subscriberCodeField = value;
            }
        }

        /// <remarks/>
        public string SubscriberPassword
        {
            get
            {
                return this.subscriberPasswordField;
            }
            set
            {
                this.subscriberPasswordField = value;
            }
        }

        /// <remarks/>
        public string SubscriberGroupID
        {
            get
            {
                return this.subscriberGroupIDField;
            }
            set
            {
                this.subscriberGroupIDField = value;
            }
        }

        /// <remarks/>
        public string SubscriberOperatorID
        {
            get
            {
                return this.subscriberOperatorIDField;
            }
            set
            {
                this.subscriberOperatorIDField = value;
            }
        }

        /// <remarks/>
        public string SubscriberOperatorPassword
        {
            get
            {
                return this.subscriberOperatorPasswordField;
            }
            set
            {
                this.subscriberOperatorPasswordField = value;
            }
        }

        /// <remarks/>
        public string TransactionStatus
        {
            get
            {
                return this.transactionStatusField;
            }
            set
            {
                this.transactionStatusField = value;
            }
        }

        /// <remarks/>
        public string ErrorCode
        {
            get
            {
                return this.errorCodeField;
            }
            set
            {
                this.errorCodeField = value;
            }
        }

        /// <remarks/>
        public string MatchedRecordsFound
        {
            get
            {
                return this.matchedRecordsFoundField;
            }
            set
            {
                this.matchedRecordsFoundField = value;
            }
        }

        /// <remarks/>
        public string ConsIdentificationNo
        {
            get
            {
                return this.consIdentificationNoField;
            }
            set
            {
                this.consIdentificationNoField = value;
            }
        }

        /// <remarks/>
        public string DateEnquirySent
        {
            get
            {
                return this.dateEnquirySentField;
            }
            set
            {
                this.dateEnquirySentField = value;
            }
        }

        /// <remarks/>
        public string TimeEnquirySent
        {
            get
            {
                return this.timeEnquirySentField;
            }
            set
            {
                this.timeEnquirySentField = value;
            }
        }

        /// <remarks/>
        public string EchoIdentityNo
        {
            get
            {
                return this.echoIdentityNoField;
            }
            set
            {
                this.echoIdentityNoField = value;
            }
        }

        /// <remarks/>
        public string EchoUniqueClientNo
        {
            get
            {
                return this.echoUniqueClientNoField;
            }
            set
            {
                this.echoUniqueClientNoField = value;
            }
        }

        /// <remarks/>
        public string MaxNoOfInquiries
        {
            get
            {
                return this.maxNoOfInquiriesField;
            }
            set
            {
                this.maxNoOfInquiriesField = value;
            }
        }

        /// <remarks/>
        public string MaxNoOfNLRAccounts
        {
            get
            {
                return this.maxNoOfNLRAccountsField;
            }
            set
            {
                this.maxNoOfNLRAccountsField = value;
            }
        }

        /// <remarks/>
        public string MaxNoOfAssAddresses
        {
            get
            {
                return this.maxNoOfAssAddressesField;
            }
            set
            {
                this.maxNoOfAssAddressesField = value;
            }
        }

        /// <remarks/>
        public string MaxNoOfOtherEmployers
        {
            get
            {
                return this.maxNoOfOtherEmployersField;
            }
            set
            {
                this.maxNoOfOtherEmployersField = value;
            }
        }

        /// <remarks/>
        public string MaxNoOfAssHomeTelNos
        {
            get
            {
                return this.maxNoOfAssHomeTelNosField;
            }
            set
            {
                this.maxNoOfAssHomeTelNosField = value;
            }
        }

        /// <remarks/>
        public string MaxNoOfAssBusTelNos
        {
            get
            {
                return this.maxNoOfAssBusTelNosField;
            }
            set
            {
                this.maxNoOfAssBusTelNosField = value;
            }
        }

        /// <remarks/>
        public string MaxNoOfAssCellNos
        {
            get
            {
                return this.maxNoOfAssCellNosField;
            }
            set
            {
                this.maxNoOfAssCellNosField = value;
            }
        }

        /// <remarks/>
        public string MaxNoOfAssOtherTelNos
        {
            get
            {
                return this.maxNoOfAssOtherTelNosField;
            }
            set
            {
                this.maxNoOfAssOtherTelNosField = value;
            }
        }

        /// <remarks/>
        public string Filler1
        {
            get
            {
                return this.filler1Field;
            }
            set
            {
                this.filler1Field = value;
            }
        }

        /// <remarks/>
        public string LoanRegNo
        {
            get
            {
                return this.loanRegNoField;
            }
            set
            {
                this.loanRegNoField = value;
            }
        }

        /// <remarks/>
        public string SupplierRefNo
        {
            get
            {
                return this.supplierRefNoField;
            }
            set
            {
                this.supplierRefNoField = value;
            }
        }

        /// <remarks/>
        public string EnquiryRefNo
        {
            get
            {
                return this.enquiryRefNoField;
            }
            set
            {
                this.enquiryRefNoField = value;
            }
        }

        /// <remarks/>
        public string ConsSurname
        {
            get
            {
                return this.consSurnameField;
            }
            set
            {
                this.consSurnameField = value;
            }
        }

        /// <remarks/>
        public string Forename1
        {
            get
            {
                return this.forename1Field;
            }
            set
            {
                this.forename1Field = value;
            }
        }

        /// <remarks/>
        public string Forename2
        {
            get
            {
                return this.forename2Field;
            }
            set
            {
                this.forename2Field = value;
            }
        }

        /// <remarks/>
        public string Forename3
        {
            get
            {
                return this.forename3Field;
            }
            set
            {
                this.forename3Field = value;
            }
        }

        /// <remarks/>
        public string SpouseCurrSurname
        {
            get
            {
                return this.spouseCurrSurnameField;
            }
            set
            {
                this.spouseCurrSurnameField = value;
            }
        }

        /// <remarks/>
        public string SpouseForename1
        {
            get
            {
                return this.spouseForename1Field;
            }
            set
            {
                this.spouseForename1Field = value;
            }
        }

        /// <remarks/>
        public string SpouseForename2
        {
            get
            {
                return this.spouseForename2Field;
            }
            set
            {
                this.spouseForename2Field = value;
            }
        }

        /// <remarks/>
        public string ConsBirthDate
        {
            get
            {
                return this.consBirthDateField;
            }
            set
            {
                this.consBirthDateField = value;
            }
        }

        /// <remarks/>
        public string ConsIdentityNo
        {
            get
            {
                return this.consIdentityNoField;
            }
            set
            {
                this.consIdentityNoField = value;
            }
        }

        /// <remarks/>
        public string CurrAddrLn1
        {
            get
            {
                return this.currAddrLn1Field;
            }
            set
            {
                this.currAddrLn1Field = value;
            }
        }

        /// <remarks/>
        public string CurrAddrLn2
        {
            get
            {
                return this.currAddrLn2Field;
            }
            set
            {
                this.currAddrLn2Field = value;
            }
        }

        /// <remarks/>
        public string CurrAddrLn3
        {
            get
            {
                return this.currAddrLn3Field;
            }
            set
            {
                this.currAddrLn3Field = value;
            }
        }

        /// <remarks/>
        public string CurrAddrLn4
        {
            get
            {
                return this.currAddrLn4Field;
            }
            set
            {
                this.currAddrLn4Field = value;
            }
        }

        /// <remarks/>
        public string CurrAddressPostalCode
        {
            get
            {
                return this.currAddressPostalCodeField;
            }
            set
            {
                this.currAddressPostalCodeField = value;
            }
        }

        /// <remarks/>
        public string PeriodAtCurrAddress
        {
            get
            {
                return this.periodAtCurrAddressField;
            }
            set
            {
                this.periodAtCurrAddressField = value;
            }
        }

        /// <remarks/>
        public string OtherAddrLn1
        {
            get
            {
                return this.otherAddrLn1Field;
            }
            set
            {
                this.otherAddrLn1Field = value;
            }
        }

        /// <remarks/>
        public string OtherAddrLn2
        {
            get
            {
                return this.otherAddrLn2Field;
            }
            set
            {
                this.otherAddrLn2Field = value;
            }
        }

        /// <remarks/>
        public string OtherAddrLn3
        {
            get
            {
                return this.otherAddrLn3Field;
            }
            set
            {
                this.otherAddrLn3Field = value;
            }
        }

        /// <remarks/>
        public string OtherAddrLn4
        {
            get
            {
                return this.otherAddrLn4Field;
            }
            set
            {
                this.otherAddrLn4Field = value;
            }
        }

        /// <remarks/>
        public string OtherAddressPostalCode
        {
            get
            {
                return this.otherAddressPostalCodeField;
            }
            set
            {
                this.otherAddressPostalCodeField = value;
            }
        }

        /// <remarks/>
        public string OwnerTenantAtCurrAddress
        {
            get
            {
                return this.ownerTenantAtCurrAddressField;
            }
            set
            {
                this.ownerTenantAtCurrAddressField = value;
            }
        }

        /// <remarks/>
        public string ConsOccupation
        {
            get
            {
                return this.consOccupationField;
            }
            set
            {
                this.consOccupationField = value;
            }
        }

        /// <remarks/>
        public string ConsEmployer
        {
            get
            {
                return this.consEmployerField;
            }
            set
            {
                this.consEmployerField = value;
            }
        }

        /// <remarks/>
        public string ConsMaidenName
        {
            get
            {
                return this.consMaidenNameField;
            }
            set
            {
                this.consMaidenNameField = value;
            }
        }

        /// <remarks/>
        public string ConsAliasName
        {
            get
            {
                return this.consAliasNameField;
            }
            set
            {
                this.consAliasNameField = value;
            }
        }

        /// <remarks/>
        public string Gender
        {
            get
            {
                return this.genderField;
            }
            set
            {
                this.genderField = value;
            }
        }

        /// <remarks/>
        public string ConsMaritalStatus
        {
            get
            {
                return this.consMaritalStatusField;
            }
            set
            {
                this.consMaritalStatusField = value;
            }
        }

        /// <remarks/>
        public string ConsHomeTelDiallingCode
        {
            get
            {
                return this.consHomeTelDiallingCodeField;
            }
            set
            {
                this.consHomeTelDiallingCodeField = value;
            }
        }

        /// <remarks/>
        public string ConsHomeTelNo
        {
            get
            {
                return this.consHomeTelNoField;
            }
            set
            {
                this.consHomeTelNoField = value;
            }
        }

        /// <remarks/>
        public string ConsWorkTelDiallingCode
        {
            get
            {
                return this.consWorkTelDiallingCodeField;
            }
            set
            {
                this.consWorkTelDiallingCodeField = value;
            }
        }

        /// <remarks/>
        public string ConsWorkTelNo
        {
            get
            {
                return this.consWorkTelNoField;
            }
            set
            {
                this.consWorkTelNoField = value;
            }
        }

        /// <remarks/>
        public string ConsCellularTelNo
        {
            get
            {
                return this.consCellularTelNoField;
            }
            set
            {
                this.consCellularTelNoField = value;
            }
        }

        /// <remarks/>
        public string LoanAmt1
        {
            get
            {
                return this.loanAmt1Field;
            }
            set
            {
                this.loanAmt1Field = value;
            }
        }

        /// <remarks/>
        public string InstalmentAmt
        {
            get
            {
                return this.instalmentAmtField;
            }
            set
            {
                this.instalmentAmtField = value;
            }
        }

        /// <remarks/>
        public string MonthlySalary
        {
            get
            {
                return this.monthlySalaryField;
            }
            set
            {
                this.monthlySalaryField = value;
            }
        }

        /// <remarks/>
        public string SalaryFrequency
        {
            get
            {
                return this.salaryFrequencyField;
            }
            set
            {
                this.salaryFrequencyField = value;
            }
        }

        /// <remarks/>
        public string RepaymentPeriod1
        {
            get
            {
                return this.repaymentPeriod1Field;
            }
            set
            {
                this.repaymentPeriod1Field = value;
            }
        }

        /// <remarks/>
        public string EnquiryReason2
        {
            get
            {
                return this.enquiryReason2Field;
            }
            set
            {
                this.enquiryReason2Field = value;
            }
        }

        /// <remarks/>
        public string Filler2
        {
            get
            {
                return this.filler2Field;
            }
            set
            {
                this.filler2Field = value;
            }
        }

        /// <remarks/>
        public string BranchCode
        {
            get
            {
                return this.branchCodeField;
            }
            set
            {
                this.branchCodeField = value;
            }
        }

        /// <remarks/>
        public string AccountNo
        {
            get
            {
                return this.accountNoField;
            }
            set
            {
                this.accountNoField = value;
            }
        }

        /// <remarks/>
        public string SubAccountNo
        {
            get
            {
                return this.subAccountNoField;
            }
            set
            {
                this.subAccountNoField = value;
            }
        }

        /// <remarks/>
        public string LoanType
        {
            get
            {
                return this.loanTypeField;
            }
            set
            {
                this.loanTypeField = value;
            }
        }

        /// <remarks/>
        public string DateLoanDisbursed
        {
            get
            {
                return this.dateLoanDisbursedField;
            }
            set
            {
                this.dateLoanDisbursedField = value;
            }
        }

        /// <remarks/>
        public string LoanAmt2
        {
            get
            {
                return this.loanAmt2Field;
            }
            set
            {
                this.loanAmt2Field = value;
            }
        }

        /// <remarks/>
        public string LoanAmtBalanceIndicator
        {
            get
            {
                return this.loanAmtBalanceIndicatorField;
            }
            set
            {
                this.loanAmtBalanceIndicatorField = value;
            }
        }

        /// <remarks/>
        public string CurrBalance
        {
            get
            {
                return this.currBalanceField;
            }
            set
            {
                this.currBalanceField = value;
            }
        }

        /// <remarks/>
        public string CurrBalanceIndicator
        {
            get
            {
                return this.currBalanceIndicatorField;
            }
            set
            {
                this.currBalanceIndicatorField = value;
            }
        }

        /// <remarks/>
        public string MonthlyInstalment
        {
            get
            {
                return this.monthlyInstalmentField;
            }
            set
            {
                this.monthlyInstalmentField = value;
            }
        }

        /// <remarks/>
        public string LoadIndicator
        {
            get
            {
                return this.loadIndicatorField;
            }
            set
            {
                this.loadIndicatorField = value;
            }
        }

        /// <remarks/>
        public string RepaymentPeriod2
        {
            get
            {
                return this.repaymentPeriod2Field;
            }
            set
            {
                this.repaymentPeriod2Field = value;
            }
        }

        /// <remarks/>
        public string LoanPurpose
        {
            get
            {
                return this.loanPurposeField;
            }
            set
            {
                this.loanPurposeField = value;
            }
        }

        /// <remarks/>
        public string TotalAmtRepayable
        {
            get
            {
                return this.totalAmtRepayableField;
            }
            set
            {
                this.totalAmtRepayableField = value;
            }
        }

        /// <remarks/>
        public string InterestType
        {
            get
            {
                return this.interestTypeField;
            }
            set
            {
                this.interestTypeField = value;
            }
        }

        /// <remarks/>
        public string AnnualRateForTotalChargeOfCredit
        {
            get
            {
                return this.annualRateForTotalChargeOfCreditField;
            }
            set
            {
                this.annualRateForTotalChargeOfCreditField = value;
            }
        }

        /// <remarks/>
        public string RandValueOfInterestCharges
        {
            get
            {
                return this.randValueOfInterestChargesField;
            }
            set
            {
                this.randValueOfInterestChargesField = value;
            }
        }

        /// <remarks/>
        public string RandValueOfTotalChargeOfCredit
        {
            get
            {
                return this.randValueOfTotalChargeOfCreditField;
            }
            set
            {
                this.randValueOfTotalChargeOfCreditField = value;
            }
        }

        /// <remarks/>
        public string SettlementAmt
        {
            get
            {
                return this.settlementAmtField;
            }
            set
            {
                this.settlementAmtField = value;
            }
        }

        /// <remarks/>
        public string ReAdvanceIndicator
        {
            get
            {
                return this.reAdvanceIndicatorField;
            }
            set
            {
                this.reAdvanceIndicatorField = value;
            }
        }

        /// <remarks/>
        public string Filler3
        {
            get
            {
                return this.filler3Field;
            }
            set
            {
                this.filler3Field = value;
            }
        }

        /// <remarks/>
        public string CreatedByUser
        {
            get
            {
                return this.createdByUserField;
            }
            set
            {
                this.createdByUserField = value;
            }
        }

        /// <remarks/>
        public System.DateTime CreatedOnDate
        {
            get
            {
                return this.createdOnDateField;
            }
            set
            {
                this.createdOnDateField = value;
            }
        }

        /// <remarks/>
        
        public bool CreatedOnDateSpecified
        {
            get
            {
                return this.createdOnDateFieldSpecified;
            }
            set
            {
                this.createdOnDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public System.DateTime ChangedOnDate
        {
            get
            {
                return this.changedOnDateField;
            }
            set
            {
                this.changedOnDateField = value;
            }
        }

        /// <remarks/>
        
        public bool ChangedOnDateSpecified
        {
            get
            {
                return this.changedOnDateFieldSpecified;
            }
            set
            {
                this.changedOnDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string ChangedByUser
        {
            get
            {
                return this.changedByUserField;
            }
            set
            {
                this.changedByUserField = value;
            }
        }

        /// <remarks/>
        public string RecordStatusInd
        {
            get
            {
                return this.recordStatusIndField;
            }
            set
            {
                this.recordStatusIndField = value;
            }
        }

        /// <remarks/>
        public int ConsID
        {
            get
            {
                return this.consIDField;
            }
            set
            {
                this.consIDField = value;
            }
        }

        /// <remarks/>
        
        public bool ConsIDSpecified
        {
            get
            {
                return this.consIDFieldSpecified;
            }
            set
            {
                this.consIDFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string NLRDailyFileDate
        {
            get
            {
                return this.nLRDailyFileDateField;
            }
            set
            {
                this.nLRDailyFileDateField = value;
            }
        }

        /// <remarks/>
        public string SubscriberName
        {
            get
            {
                return this.subscriberNameField;
            }
            set
            {
                this.subscriberNameField = value;
            }
        }

        /// <remarks/>
        public int SubscriberID
        {
            get
            {
                return this.subscriberIDField;
            }
            set
            {
                this.subscriberIDField = value;
            }
        }

        /// <remarks/>
        
        public bool SubscriberIDSpecified
        {
            get
            {
                return this.subscriberIDFieldSpecified;
            }
            set
            {
                this.subscriberIDFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int LoaderID
        {
            get
            {
                return this.loaderIDField;
            }
            set
            {
                this.loaderIDField = value;
            }
        }

        /// <remarks/>
        
        public bool LoaderIDSpecified
        {
            get
            {
                return this.loaderIDFieldSpecified;
            }
            set
            {
                this.loaderIDFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string NLRDailyRegFileName
        {
            get
            {
                return this.nLRDailyRegFileNameField;
            }
            set
            {
                this.nLRDailyRegFileNameField = value;
            }
        }

        /// <remarks/>
        public bool HasBeenSynced
        {
            get
            {
                return this.hasBeenSyncedField;
            }
            set
            {
                this.hasBeenSyncedField = value;
            }
        }

        /// <remarks/>
        
        public bool HasBeenSyncedSpecified
        {
            get
            {
                return this.hasBeenSyncedFieldSpecified;
            }
            set
            {
                this.hasBeenSyncedFieldSpecified = value;
            }
        }

        /// <remarks/>
        public System.DateTime SyncDate
        {
            get
            {
                return this.syncDateField;
            }
            set
            {
                this.syncDateField = value;
            }
        }

        /// <remarks/>
        
        public bool SyncDateSpecified
        {
            get
            {
                return this.syncDateFieldSpecified;
            }
            set
            {
                this.syncDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string OldBranchCode
        {
            get
            {
                return this.oldBranchCodeField;
            }
            set
            {
                this.oldBranchCodeField = value;
            }
        }

        /// <remarks/>
        public string OldAccountNo
        {
            get
            {
                return this.oldAccountNoField;
            }
            set
            {
                this.oldAccountNoField = value;
            }
        }

        /// <remarks/>
        public string OldSubAccountNo
        {
            get
            {
                return this.oldSubAccountNoField;
            }
            set
            {
                this.oldSubAccountNoField = value;
            }
        }

        /// <remarks/>
        public int XDSSubscriberID
        {
            get
            {
                return this.xDSSubscriberIDField;
            }
            set
            {
                this.xDSSubscriberIDField = value;
            }
        }

        /// <remarks/>
        
        public bool XDSSubscriberIDSpecified
        {
            get
            {
                return this.xDSSubscriberIDFieldSpecified;
            }
            set
            {
                this.xDSSubscriberIDFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool IsMatchedAccount
        {
            get
            {
                return this.isMatchedAccountField;
            }
            set
            {
                this.isMatchedAccountField = value;
            }
        }

        /// <remarks/>
        
        public bool IsMatchedAccountSpecified
        {
            get
            {
                return this.isMatchedAccountFieldSpecified;
            }
            set
            {
                this.isMatchedAccountFieldSpecified = value;
            }
        }
                    
        /// <remarks/>

        public string PassportNo
        {
            get
            {
                return this.PassportNofield;
            }
            set
            {
                this.PassportNofield = value;
            }
        }

        /// <remarks/>

        public int EnquirySubscriberID
        {
            get
            {
                return this.EnquirySubscriberIDfield;
            }
            set
            {
                this.EnquirySubscriberIDfield = value;
            }
        }

        /// <remarks/>

        public string Username
        {
            get
            {
                return this.Usernamefield;
            }
            set
            {
                this.Usernamefield = value;
            }
        }

      
        /// <remarks/>

        public int SystemUserID
        {
            get
            {
                return this.SystemUserIDfield;
            }
            set
            {
                this.SystemUserIDfield = value;
            }
        }

     
        /// <remarks/>

        public string StatusInd
        {
            get
            {
                return this.StatusIndfield;
            }
            set
            {
                this.StatusIndfield = value;
            }
        }

        /// <remarks/>

        public int ConsumerID
        {
            get
            {
                return this.ConsumerIDfield;
            }
            set
            {
                this.ConsumerIDfield = value;
            }
        }


       
    
        
        /// <remarks/>

        public string ErrorDescription
        {
            get
            {
                return this.ErrorDescriptionfield;
            }
            set
            {
                this.ErrorDescriptionfield = value;
            }
        }   
    
        /// <remarks/>

        public int NLRDailyRegistrationsLogID
        {
            get
            {
                return this.NLRDailyRegistrationsLogIDfield;
            }
            set
            {
                this.NLRDailyRegistrationsLogIDfield = value;
            }
        }


        /// <remarks/>

        public string SubscriberReference
        {
            get
            {
                return this.SubscriberReferencefield;
            }
            set
            {
                this.SubscriberReferencefield = value;
            }
        }

        /// <remarks/>

        public DateTime subscriberEnquiryDate
        {
            get
            {
                return this.subscriberEnquiryDatefield;
            }
            set
            {
                this.subscriberEnquiryDatefield = value;
            }
        }

        /// <remarks/>

        public string VoucherCode
        {
            get
            {
                return this.VoucherCodefield;
            }
            set
            {
                this.VoucherCodefield = value;
            }
        }


        /// <remarks/>

        public int Payasyougo
        {
            get
            {
                return this.Payasyougofield;
            }
            set
            {
                this.Payasyougofield = value;
            }
        }


        /// <remarks/>

        public bool DetailsViewedYN
        {
            get
            {
                return this.DetailsViewedYNfield;
            }
            set
            {
                this.DetailsViewedYNfield = value;
            }
        }

        /// <remarks/>

        public DateTime Detailsvieweddate
        {
            get
            {
                return this.Detailsvieweddatefield;
            }
            set
            {
                this.Detailsvieweddatefield = value;
            }
        }


        /// <remarks/>

        public int productID
        {
            get
            {
                return this.productIDfield;
            }
            set
            {
                this.productIDfield = value;
            }
        }

        /// <remarks/>

        public int BillingtypeId
        {
            get
            {
                return this.BillingtypeIdfield;
            }
            set
            {
                this.BillingtypeIdfield = value;
            }
        }

        /// <remarks/>

        public bool Billable
        {
            get
            {
                return this.Billablefield;
            }
            set
            {
                this.Billablefield = value;
            }
        }


        /// <remarks/>

        public string ExtraVarInput1
        {
            get
            {
                return this.ExtraVarInput1field;
            }
            set
            {
                this.ExtraVarInput1field = value;
            }
        }

        /// <remarks/>

        public string ExtraVarInput2
        {
            get
            {
                return this.ExtraVarInput2field;
            }
            set
            {
                this.ExtraVarInput2field = value;
            }
        }

        /// <remarks/>

        public string ExtraVarInput3
        {
            get
            {
                return this.ExtraVarInput3field;
            }
            set
            {
                this.ExtraVarInput3field = value;
            }
        }

        /// <remarks/>

        public int ExtraIntInput1
        {
            get
            {
                return this.ExtraIntInput1field;
            }
            set
            {
                this.ExtraIntInput1field = value;
            }
        }

        /// <remarks/>

        public int ExtraIntInput2
        {
            get
            {
                return this.ExtraIntInput2field;
            }
            set
            {
                this.ExtraIntInput2field = value;
            }
        }
        /// <remarks/>

        public string XMLData
        {
            get
            {
                return this.XMLDatafield;
            }
            set
            {
                this.XMLDatafield = value;
            }
        }        
    }
}
