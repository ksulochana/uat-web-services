﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace XDSPortalLibrary.Entity_Layer.MTNHResponse
{
    public class MTNHResponse
    {
    }

    /// <remarks/> MTN
    /// 
    public class SubmitApplicationResponse
    {

        private MTNOutput oSubmitResult = new MTNOutput();

        public MTNOutput MTNOutput
        {
            get
            {
                return this.oSubmitResult;
            }
            set
            {
                this.oSubmitResult = value;
            }
        }
    }

    public class MTNOutput
    {
        // public enum enumARoutcome { Approve, Decline };
        //public enum enumARstatus { SUCCESS, FAILURE };

        private string statusField;

        private string consumerNumberField;

        private string applicationIDField;

        private string outcomeField;

        private string reasonField;

        private string errorDescriptionField;

        private string bCCFilter01Field;

        private string bCCFilter02Field;

        private string bCCFilter03Field;

        private string bCCFilter04Field;

        private string bCCFilter05Field;

        private string bCCFilter06Field;

        private string bCCFilter07Field;

        private string bCCFilter08Field;

        private string bCCFilter09Field;

        private string bCCFilter10Field;

        private string scorecardIndicatorField;

        private string bCCScoreField;

        private string riskBandField;

        private decimal availableForInstallmentField;

        private decimal creditLimitField;



        private BureauResponse bureauResponseField;

        /// <remarks/>
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        public string ConsumerNumber
        {
            get
            {
                return this.consumerNumberField;
            }
            set
            {
                this.consumerNumberField = value;
            }
        }

        /// <remarks/>
        public string ApplicationID
        {
            get
            {
                return this.applicationIDField;
            }
            set
            {
                this.applicationIDField = value;
            }
        }

        /// <remarks/>
        public string Outcome
        {
            get
            {
                return this.outcomeField;
            }
            set
            {
                this.outcomeField = value;
            }
        }

        /// <remarks/>
        public string Reason
        {
            get
            {
                return this.reasonField;
            }
            set
            {
                this.reasonField = value;
            }
        }

        /// <remarks/>
        public string ErrorDescription
        {
            get
            {
                return this.errorDescriptionField;
            }
            set
            {
                this.errorDescriptionField = value;
            }
        }

        /// <remarks/>
        public string BCCFilter01
        {
            get
            {
                return this.bCCFilter01Field;
            }
            set
            {
                this.bCCFilter01Field = value;
            }
        }

        /// <remarks/>
        public string BCCFilter02
        {
            get
            {
                return this.bCCFilter02Field;
            }
            set
            {
                this.bCCFilter02Field = value;
            }
        }

        /// <remarks/>
        public string BCCFilter03
        {
            get
            {
                return this.bCCFilter03Field;
            }
            set
            {
                this.bCCFilter03Field = value;
            }
        }

        /// <remarks/>
        public string BCCFilter04
        {
            get
            {
                return this.bCCFilter04Field;
            }
            set
            {
                this.bCCFilter04Field = value;
            }
        }

        /// <remarks/>
        public string BCCFilter05
        {
            get
            {
                return this.bCCFilter05Field;
            }
            set
            {
                this.bCCFilter05Field = value;
            }
        }

        /// <remarks/>
        public string BCCFilter06
        {
            get
            {
                return this.bCCFilter06Field;
            }
            set
            {
                this.bCCFilter06Field = value;
            }
        }

        /// <remarks/>
        public string BCCFilter07
        {
            get
            {
                return this.bCCFilter07Field;
            }
            set
            {
                this.bCCFilter07Field = value;
            }
        }

        /// <remarks/>
        public string BCCFilter08
        {
            get
            {
                return this.bCCFilter08Field;
            }
            set
            {
                this.bCCFilter08Field = value;
            }
        }

        /// <remarks/>
        public string BCCFilter09
        {
            get
            {
                return this.bCCFilter09Field;
            }
            set
            {
                this.bCCFilter09Field = value;
            }
        }

        /// <remarks/>
        public string BCCFilter10
        {
            get
            {
                return this.bCCFilter10Field;
            }
            set
            {
                this.bCCFilter10Field = value;
            }
        }

        /// <remarks/>
        public string ScorecardIndicator
        {
            get
            {
                return this.scorecardIndicatorField;
            }
            set
            {
                this.scorecardIndicatorField = value;
            }
        }

        /// <remarks/>
        public string BCCScore
        {
            get
            {
                return this.bCCScoreField;
            }
            set
            {
                this.bCCScoreField = value;
            }
        }

        /// <remarks/>
        public string RiskBand
        {
            get
            {
                return this.riskBandField;
            }
            set
            {
                this.riskBandField = value;
            }
        }

        /// <remarks/>
        public decimal AvailableForInstallment
        {
            get
            {
                return this.availableForInstallmentField;
            }
            set
            {
                this.availableForInstallmentField = value;
            }
        }

        /// <remarks/>
        public decimal CreditLimit
        {
            get
            {
                return this.creditLimitField;
            }
            set
            {
                this.creditLimitField = value;
            }
        }

      // [System.Xml.Serialization.XmlElementAttribute("BureauResponse", typeof(BureauResponse), Form = System.Xml.Schema.XmlSchemaForm.Qualified, Namespace = "http://www.web.xds.co.za/XDSConnectWS")]
        /// <remarks/>
          [XmlElement(Namespace = "http://www.web.xds.co.za/XDSConnectWS")]
         
        public BureauResponse BureauResponse
        {
            get
            {
                return this.bureauResponseField;
            }
            set
            {
                this.bureauResponseField = value;
            }
        }
    }
    // [XmlRoot(ElementName = "BureauResponse", Namespace = "http://www.web.xds.co.za/XDSConnectWS")]
   // [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.web.xds.co.za/XDSConnectWS")]
    //[XmlType("BureauResponse", Namespace = "http://www.web.xds.co.za/XDSConnectWS")]
    public class BureauResponse
    {
        public enum enumresponseStatus { SUCCESS, FAILURE };

        private string rawDataField;

        private enumresponseStatus responseStatusField;

        private string errorCodeField;

        private string errorMessageField;

        private DateTime processingStartDateField = DateTime.Now;

        private double processingTimeSecsField;

        private string uniqueRefGuidField;

        [XmlArray("AddressNA07")]
        [XmlArrayItem("AddressNA07")]
        private AddressNA07[] addressNA07Field;

        private AddressNA08[] addressNA08Field;

        private AddressVerificationNR01 addressVerificationNR01Field;

        private AffordabilityAF01 affordabilityAF01Field;

        private AffordStandardBatchCharsXB01 affordStandardBatchCharsXB01Field;

        private AggregateAG01 aggregateAG01Field;

        private AggregateAG02 aggregateAG02Field;

        private AggregateNX05 aggregateNX05Field;

        private AggregateNX09 aggregateNX09Field;

        private AggregateNX33 aggregateNX33Field;

        private AKANamesNK04[] aKANamesNK04Field;

        private AlertDetailC3[] alertDetailC3Field;

        private AlertDetailQM[] alertDetailQMField;

        private AlertDetailQE[] alertDetailQEField;

        private AlertDetailQC[] alertDetailQCField;

        private AlertDetailQD[] alertDetailQDField;

        private AlertDetailQW[] alertDetailQWField;

        private AlertDetailQP[] alertDetailQPField;

        private AlertDetailQN[] alertDetailQNField;

        private AlertDetailQV[] alertDetailQVField;

        private AlertDetailQJ[] alertDetailQJField;

        private AlertDetailC4 alertDetailC4Field;

        private BCCBC01 bCCBC01Field;

        private BCCBC03 bCCBC03Field;

        private BCCBC04 bCCBC04Field;

        private BCCC1 bCCC1Field;

        private BCCC2 bCCC2Field;

        private CCASummaryMX01 cCASummaryMX01Field;

        private CCASummaryMX50 cCASummaryMX50Field;

        private CellphoneValidationNZ01 cellphoneValidationNZ01Field;

        private ConsEnqTransInfo0102 consEnqTransInfo0102Field;

        private ConsumerCountersNC04 consumerCountersNC04Field;
        private ConsumerCountersNC50 consumerCountersNC50Field;

        private ConsumerDeedsDD01[] consumerDeedsDD01Field;
        private DeedsMultipleBond[] deedsMultipleBondField;

        private ConsumerInfoNO04 consumerInfoNO04Field;

        private ConsumerInfoNO05 consumerInfoNO05Field;

        private ConsumerMiscellaneousMS[] consumerMiscellaneousMSField;

        private ConsumerNumberFrequencyNY01[] consumerNumberFrequencyNY01Field;

        private ConsumerTelephoneHistoryNW01 consumerTelephoneHistoryNW01Field;

        private CPAAccountInformationExtendedP8[] cPAAccountInformationExtendedP8Field;

        private EmpiricaEM04[] EmpiricaEM04Field;

        private DebtReviewDR01[] debtReviewDR01Field;

        [XmlArray("DefaultD701Part1")]
        [XmlArrayItem("DefaultD701Part1")]
        private DefaultD701Part1[] defaultsD701Part1Field;

        private DefaultD701Part2[] defaultsD701Part2Field;

        [XmlArray("DefaultD801Part1")]
        [XmlArrayItem("DefaultD801Part1")]
        private DefaultD801Part1[] defaultsD801Part1Field;


        private DefaultD801Part2[] defaultsD801Part2Field;

        private DefaultsND07[] defaultsND07Field;

        private DisputeIndicatorDI01 disputeIndicatorDI01Field;

        private DriversLicenceUY01[] driversLicenceUY01Field;

        private EchoData0001[] echoData0001Field;

        //        private EmpiricaEM04 empiricaEM04Field;

        private EmpiricaEM05 empiricaEM05Field;

        private EmpiricaEM07 empiricaEM07Field;

        private EmpiricaEX[] empiricaEXField;

        private EmploymentNM04[] employmentNM04Field;

        private EnquiriesNE09[] enquiriesNE09Field;

        private EnquiryNE50[] enquiriesNE50Field;

        private FSMS201[] fSMS201Field;

        private FraudScoreFS01 fraudScoreFS01Field;

        private HawkHA01[] hawkHA01Field;

        private HawkHI01[] hawkHI01Field;

        private HawkNH05 hawkNH05Field;

        private IdvNI01 idvNI01Field;


        //private IdvNI01MTN idvNI01NField;

        private IdvNI02 idvNI02Field;

        private IdvNI03 idvNI03Field;

        private IncomeEstimatorT101 incomeEstimatorT101Field;

        private IncomeEstimatorT102 incomeEstimatorT102Field;

        private InsuranceClaimsUA01[] insuranceClaimsUA01Field;

        private InsuranceClaimsUB01[] insuranceClaimsUB01Field;

        private InsuranceClaimsUC01[] insuranceClaimsUC01Field;

        private InsuranceClaimsUI01[] insuranceClaimsUI01Field;

        private IndividualTrace04[] individualTrace04Field;

        private IndividualTrace05[] individualTrace05Field;

        private JudgementsNJ06[] judgementsNJ06Field;

        private JudgementsNJ07[] judgementsNJ07Field;

        private NLRAccountInformationMP01[] nLRAccountInformationMP01Field;

        private NLRAccountInformationMP03[] nLRAccountInformationMP03Field;

        private NLRAccountInformationM701[] nLRAccountsInformationM701Field;

        private NLRAccountInformationM703[] nLRAccountsInformationM703Field;

        private NLRAccountInformationExtendedM8[] nLRAccountInformationExtendedM8Field;

        private NLRConfirmationMR01 nLRConfirmationMR01Field;

        private NLRConfirmationMZ01 nLRConfirmationMZ01Field;

        private NLRCounterSeqmentMC01[] nLRCounterSeqmentMC01Field;

        private NLREnquiriesME01[] nLREnquiriesME01Field;

        private NLREnquiryME50[] nLREnquiriesME50Field;

        private NLRSummaryMY01 nLRSummaryMY01Field;

        private NoticesNN08[] noticesNN08Field;

        private NoticesNN10[] noticesNN10Field;

        private NoticesNN07[] noticesNN07Field;

        private PaymentProfileNP09[] paymentProfileNP09Field;

        private PaymentProfileNP11[] paymentProfileNP11Field;

        private PaymentProfileNP15[] paymentProfileNP15Field;

        private PaymentProfileP601[] paymentProfilesP601Field;

        private PaymentProfileP701[] paymentProfilesP701Field;

        private PaymentProfileP702[] paymentProfilesP702Field;

        private PaymentProfileP703[] paymentProfilesP703Field;

        private PaymentProfileP704[] paymentProfilesP704Field;

        private PaymentProfileP705[] paymentProfilesP705Field;

        private SAFPSNF01[] sAFPSNF01Field;

        private ScoreCardAC01 scoreCardAC01Field;

        private ScoreCardBX01 scoreCardBX01Field;

        private ScoreCardBX03 scoreCardBX03Field;

        private ScorecardS101[] scorecardS101Field;

        private StandardBatchCharsB101 standardBatchCharsB101Field;

        private StandardBatchCharsFA01 standardBatchCharsFA01Field;

        private StandardBatchCharsFA02 standardBatchCharsFA02Field;

        private StandardBatchCharsFA03 standardBatchCharsFA03Field;

        private StandardBatchCharsSB01[] standardBatchCharsSB01Field;

        private StandardBatchCharsSB04 standardBatchCharsSB04Field;

        private StandardBatchCharsSB07 standardBatchCharsSB07Field;

        private StandardBatchCharsSB13 standardBatchCharsSB13Field;

        private StandardBatchCharsSB18 standardBatchCharsSB18Field;

        private StandardBatchCharsSB19 standardBatchCharsSB19Field;

        private StandardBatchCharsSB25 standardBatchCharsSB25Field;

        private StandardBatchCharsSB27 standardBatchCharsSB27Field;

        private StandardBatchCharsSB30 standardBatchCharsSB30Field;

        private StandardBatchCharsSB33 standardBatchCharsSB33Field;

        private StandardBatchCharsSB37 standardBatchCharsSB37Field;

        private StandardBatchCharsSB40 standardBatchCharsSB40Field;

        private StandardBatchCharsSB99 standardBatchCharsSB99Field;

        private SuburbCodeGR01[] suburbCodesGR01Field;

        private TraceAlertsNT04[] traceAlertsNT04Field;

        private TraceAlertsNT06[] traceAlertsNT06Field;

        private TraceAlertsNT07[] traceAlertsNT07Field;

        private UniqueITCRef[] uniqueITCRefField;

        private V1Segment[] v1SegmentField;

        private VehicleDataUZ01[] vehicleDataUZ01Field;

        private VehicleDataUZ[] vehicleDataUZField;

        private ClaimsSummaryCS[] claimssummarycsField;

        private DebtCounsellingDC01[] debtCounsellingDC01Field;

        private PaymentProfileWMP01[] paymentProfileWMP01Field;

        private CampaignInformation campaignInformationField;

        private int affordabilityOverrideField;

        private DateTime affordabilityOverrideEndDateField;

        private ScorecardRecommendation scorecardRecommendationField;

        public string RawData
        {
            get
            {
                return this.rawDataField;
            }
            set
            {
                this.rawDataField = value;
            }
        }



        public enumresponseStatus ResponseStatus
        {
            get
            {
                return this.responseStatusField;
            }
            set
            {
                this.responseStatusField = value;
            }
        }



        public string ErrorCode
        {
            get
            {
                return this.errorCodeField;
            }
            set
            {
                this.errorCodeField = value;
            }
        }



        public string ErrorMessage
        {
            get
            {
                return this.errorMessageField;
            }
            set
            {
                this.errorMessageField = value;
            }
        }



        public DateTime ProcessingStartDate
        {
            get
            {
                return this.processingStartDateField;
            }
            set
            {
                this.processingStartDateField = value;
            }
        }



        public double ProcessingTimeSecs
        {
            get
            {
                return this.processingTimeSecsField;
            }
            set
            {
                this.processingTimeSecsField = value;
            }
        }



        public string UniqueRefGuid
        {
            get
            {
                return this.uniqueRefGuidField;
            }
            set
            {
                this.uniqueRefGuidField = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AddressNA07")]
        public AddressNA07[] AddressNA07
        {
            get
            {
                return this.addressNA07Field;
            }
            set
            {
                this.addressNA07Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AddressNA08")]
        public AddressNA08[] AddressNA08
        {
            get
            {
                return this.addressNA08Field;
            }
            set
            {
                this.addressNA08Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AddressVerificationNR01", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public AddressVerificationNR01 AddressVerificationNR01
        {
            get
            {
                return this.addressVerificationNR01Field;
            }
            set
            {
                this.addressVerificationNR01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AffordabilityAF01", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public AffordabilityAF01 AffordabilityAF01
        {
            get
            {
                return this.affordabilityAF01Field;
            }
            set
            {
                this.affordabilityAF01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AffordStandardBatchCharsXB01", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public AffordStandardBatchCharsXB01 AffordStandardBatchCharsXB01
        {
            get
            {
                return this.affordStandardBatchCharsXB01Field;
            }
            set
            {
                this.affordStandardBatchCharsXB01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AggregateAG01", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public AggregateAG01 AggregateAG01
        {
            get
            {
                return this.aggregateAG01Field;
            }
            set
            {
                this.aggregateAG01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AggregateAG02", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public AggregateAG02 AggregateAG02
        {
            get
            {
                return this.aggregateAG02Field;
            }
            set
            {
                this.aggregateAG02Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AggregateNX05", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public AggregateNX05 AggregateNX05
        {
            get
            {
                return this.aggregateNX05Field;
            }
            set
            {
                this.aggregateNX05Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AggregateNX09", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public AggregateNX09 AggregateNX09
        {
            get
            {
                return this.aggregateNX09Field;
            }
            set
            {
                this.aggregateNX09Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AggregateNX33", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public AggregateNX33 AggregateNX33
        {
            get
            {
                return this.aggregateNX33Field;
            }
            set
            {
                this.aggregateNX33Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AKANamesNK04")]
        public AKANamesNK04[] AKANamesNK04
        {
            get
            {
                return this.aKANamesNK04Field;
            }
            set
            {
                this.aKANamesNK04Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AlertDetailC3")]
        public AlertDetailC3[] AlertDetailC3
        {
            get
            {
                return this.alertDetailC3Field;
            }
            set
            {
                this.alertDetailC3Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AlertDetailQM")]
        public AlertDetailQM[] AlertDetailQM
        {
            get
            {
                return this.alertDetailQMField;
            }
            set
            {
                this.alertDetailQMField = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AlertDetailQE")]
        public AlertDetailQE[] AlertDetailQE
        {
            get
            {
                return this.alertDetailQEField;
            }
            set
            {
                this.alertDetailQEField = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AlertDetailQC")]
        public AlertDetailQC[] AlertDetailQC
        {
            get
            {
                return this.alertDetailQCField;
            }
            set
            {
                this.alertDetailQCField = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AlertDetailQD")]
        public AlertDetailQD[] AlertDetailQD
        {
            get
            {
                return this.alertDetailQDField;
            }
            set
            {
                this.alertDetailQDField = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AlertDetailQW")]
        public AlertDetailQW[] AlertDetailQW
        {
            get
            {
                return this.alertDetailQWField;
            }
            set
            {
                this.alertDetailQWField = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AlertDetailQP")]
        public AlertDetailQP[] AlertDetailQP
        {
            get
            {
                return this.alertDetailQPField;
            }
            set
            {
                this.alertDetailQPField = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AlertDetailQN")]
        public AlertDetailQN[] AlertDetailQN
        {
            get
            {
                return this.alertDetailQNField;
            }
            set
            {
                this.alertDetailQNField = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AlertDetailQV")]
        public AlertDetailQV[] AlertDetailQV
        {
            get
            {
                return this.alertDetailQVField;
            }
            set
            {
                this.alertDetailQVField = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AlertDetailQJ")]
        public AlertDetailQJ[] AlertDetailQJ
        {
            get
            {
                return this.alertDetailQJField;
            }
            set
            {
                this.alertDetailQJField = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("AlertDetailC4", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public AlertDetailC4 AlertDetailC4
        {
            get
            {
                return this.alertDetailC4Field;
            }
            set
            {
                this.alertDetailC4Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("BCCBC01", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public BCCBC01 BCCBC01
        {
            get
            {
                return this.bCCBC01Field;
            }
            set
            {
                this.bCCBC01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("BCCBC03", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public BCCBC03 BCCBC03
        {
            get
            {
                return this.bCCBC03Field;
            }
            set
            {
                this.bCCBC03Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("BCCBC04", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public BCCBC04 BCCBC04
        {
            get
            {
                return this.bCCBC04Field;
            }
            set
            {
                this.bCCBC04Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("BCCC1", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public BCCC1 BCCC1
        {
            get
            {
                return this.bCCC1Field;
            }
            set
            {
                this.bCCC1Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("BCCC2", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public BCCC2 BCCC2
        {
            get
            {
                return this.bCCC2Field;
            }
            set
            {
                this.bCCC2Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("CCASummaryMX01", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public CCASummaryMX01 CCASummaryMX01
        {
            get
            {
                return this.cCASummaryMX01Field;
            }
            set
            {
                this.cCASummaryMX01Field = value;
            }
        }

        public CCASummaryMX50 CCASummaryMX50
        {
            get
            {
                return this.cCASummaryMX50Field;
            }
            set
            {
                this.cCASummaryMX50Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("CellphoneValidationNZ01")]
        public CellphoneValidationNZ01 CellphoneValidationNZ01
        {
            get
            {
                return this.cellphoneValidationNZ01Field;
            }
            set
            {
                this.cellphoneValidationNZ01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("ConsEnqTransInfo0102", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public ConsEnqTransInfo0102 ConsEnqTransInfo0102
        {
            get
            {
                return this.consEnqTransInfo0102Field;
            }
            set
            {
                this.consEnqTransInfo0102Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("ConsumerCountersNC04", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public ConsumerCountersNC04 ConsumerCountersNC04
        {
            get
            {
                return this.consumerCountersNC04Field;
            }
            set
            {
                this.consumerCountersNC04Field = value;
            }
        }

        public ConsumerCountersNC50 ConsumerCountersNC50
        {
            get
            {
                return this.consumerCountersNC50Field;
            }
            set
            {
                this.consumerCountersNC50Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("ConsumerDeedsDD01")]
        public ConsumerDeedsDD01[] ConsumerDeedsDD01
        {
            get
            {
                return this.consumerDeedsDD01Field;
            }
            set
            {
                this.consumerDeedsDD01Field = value;
            }
        }

        public DeedsMultipleBond[] DeedsMultipleBond
        {
            get
            {
                return this.deedsMultipleBondField;
            }
            set
            {
                this.deedsMultipleBondField = value;
            }
        }



        //[System.Xml.Serialization.XmlElementAttribute("ConsumerInfoNO04", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public ConsumerInfoNO04 ConsumerInfoNO04
        {
            get
            {
                return this.consumerInfoNO04Field;
            }
            set
            {
                this.consumerInfoNO04Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("ConsumerInfoNO05", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public ConsumerInfoNO05 ConsumerInfoNO05
        {
            get
            {
                return this.consumerInfoNO05Field;
            }
            set
            {
                this.consumerInfoNO05Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("ConsumerMiscellaneousMS")]
        public ConsumerMiscellaneousMS[] ConsumerMiscellaneousMS
        {
            get
            {
                return this.consumerMiscellaneousMSField;
            }
            set
            {
                this.consumerMiscellaneousMSField = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("ConsumerNumberFrequencyNY01")]
        public ConsumerNumberFrequencyNY01[] ConsumerNumberFrequencyNY01
        {
            get
            {
                return this.consumerNumberFrequencyNY01Field;
            }
            set
            {
                this.consumerNumberFrequencyNY01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("ConsumerTelephoneHistoryNW01", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public ConsumerTelephoneHistoryNW01 ConsumerTelephoneHistoryNW01
        {
            get
            {
                return this.consumerTelephoneHistoryNW01Field;
            }
            set
            {
                this.consumerTelephoneHistoryNW01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("CPAAccountInformationExtendedP8")]
        public CPAAccountInformationExtendedP8[] CPAAccountInformationExtendedP8
        {
            get
            {
                return this.cPAAccountInformationExtendedP8Field;
            }
            set
            {
                this.cPAAccountInformationExtendedP8Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("EmpiricaEM04", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public EmpiricaEM04[] EmpiricaEM04
        {
            get
            {
                return this.EmpiricaEM04Field;
            }
            set
            {
                this.EmpiricaEM04Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("DebtReviewDR01")]
        public DebtReviewDR01[] DebtReviewDR01
        {
            get
            {
                return this.debtReviewDR01Field;
            }
            set
            {
                this.debtReviewDR01Field = value;
            }
        }


        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayItemAttribute("DefaultD701Part1", typeof(DefaultD701Part1), Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public DefaultD701Part1[] DefaultsD701Part1
        {
            get
            {
                return this.defaultsD701Part1Field;
            }
            set
            {
                this.defaultsD701Part1Field = value;
            }
        }


        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayItemAttribute("DefaultD701Part2", typeof(DefaultD701Part2), Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public DefaultD701Part2[] DefaultsD701Part2
        {
            get
            {
                return this.defaultsD701Part2Field;
            }
            set
            {
                this.defaultsD701Part2Field = value;
            }
        }

        public DefaultD801Part1[] DefaultsD801Part1
        {
            get
            {
                return this.defaultsD801Part1Field;
            }
            set
            {
                this.defaultsD801Part1Field = value;
            }
        }

        public DefaultD801Part2[] DefaultsD801Part2
        {
            get
            {
                return this.defaultsD801Part2Field;
            }
            set
            {
                this.defaultsD801Part2Field = value;
            }
        }

        //[System.Xml.Serialization.XmlElementAttribute("DefaultsND07")]
        public DefaultsND07[] DefaultsND07
        {
            get
            {
                return this.defaultsND07Field;
            }
            set
            {
                this.defaultsND07Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("DisputeIndicatorDI01", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public DisputeIndicatorDI01 DisputeIndicatorDI01
        {
            get
            {
                return this.disputeIndicatorDI01Field;
            }
            set
            {
                this.disputeIndicatorDI01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("DriversLicenceUY01")]
        public DriversLicenceUY01[] DriversLicenceUY01
        {
            get
            {
                return this.driversLicenceUY01Field;
            }
            set
            {
                this.driversLicenceUY01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("EchoData0001", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public EchoData0001[] EchoData0001
        {
            get
            {
                return this.echoData0001Field;
            }
            set
            {
                this.echoData0001Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("EmpiricaEM04", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        //public EmpiricaEM04 EmpiricaScoreEM04
        //{
        //    get
        //    {
        //        return this.empiricaEM04Field;
        //    }
        //    set
        //    {
        //        this.empiricaEM04Field = value;
        //    }
        //}


        //[System.Xml.Serialization.XmlElementAttribute("EmpiricaEM05", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public EmpiricaEM05 EmpiricaEM05
        {
            get
            {
                return this.empiricaEM05Field;
            }
            set
            {
                this.empiricaEM05Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("EmpiricaEM07", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public EmpiricaEM07 EmpiricaEM07
        {
            get
            {
                return this.empiricaEM07Field;
            }
            set
            {
                this.empiricaEM07Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("EmpiricaEX")]
        public EmpiricaEX[] EmpiricaEX
        {
            get
            {
                return this.empiricaEXField;
            }
            set
            {
                this.empiricaEXField = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("EmploymentNM04")]
        public EmploymentNM04[] EmploymentNM04
        {
            get
            {
                return this.employmentNM04Field;
            }
            set
            {
                this.employmentNM04Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("EnquiriesNE09")]
        public EnquiriesNE09[] EnquiriesNE09
        {
            get
            {
                return this.enquiriesNE09Field;
            }
            set
            {
                this.enquiriesNE09Field = value;
            }
        }


        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayItemAttribute("EnquiryNE50", typeof(EnquiryNE50), Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public EnquiryNE50[] EnquiriesNE50
        {
            get
            {
                return this.enquiriesNE50Field;
            }
            set
            {
                this.enquiriesNE50Field = value;
            }
        }





        //[System.Xml.Serialization.XmlElementAttribute("FSMS201", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public FSMS201[] FSMS201
        {
            get
            {
                return this.fSMS201Field;
            }
            set
            {
                this.fSMS201Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("FraudScoreFS01", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public FraudScoreFS01 FraudScoreFS01
        {
            get
            {
                return this.fraudScoreFS01Field;
            }
            set
            {
                this.fraudScoreFS01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("HawkHA01")]
        public HawkHA01[] HawkHA01
        {
            get
            {
                return this.hawkHA01Field;
            }
            set
            {
                this.hawkHA01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("HawkHI01")]
        public HawkHI01[] HawkHI01
        {
            get
            {
                return this.hawkHI01Field;
            }
            set
            {
                this.hawkHI01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("HawkNH05", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public HawkNH05 HawkNH05
        {
            get
            {
                return this.hawkNH05Field;
            }
            set
            {
                this.hawkNH05Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("IdvNI01", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public IdvNI01 IdvNI01
        {
            get
            {
                return this.idvNI01Field;
            }
            set
            {
                this.idvNI01Field = value;
            }
        }
        //[XmlElement(ElementName = "IdvNI01", Namespace = "Custom")]
        //public IdvNI01MTN IdvNI01N
        //{
        //    get
        //    {
        //        return this.idvNI01NField;
        //    }
        //    set
        //    {
        //        this.idvNI01NField = value;
        //    }
        //}


        //[System.Xml.Serialization.XmlElementAttribute("IdvNI02", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public IdvNI02 IdvNI02
        {
            get
            {
                return this.idvNI02Field;
            }
            set
            {
                this.idvNI02Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("IdvNI03", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public IdvNI03 IdvNI03
        {
            get
            {
                return this.idvNI03Field;
            }
            set
            {
                this.idvNI03Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("IncomeEstimatorT101", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public IncomeEstimatorT101 IncomeEstimatorT101
        {
            get
            {
                return this.incomeEstimatorT101Field;
            }
            set
            {
                this.incomeEstimatorT101Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("IncomeEstimatorT102", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public IncomeEstimatorT102 IncomeEstimatorT102
        {
            get
            {
                return this.incomeEstimatorT102Field;
            }
            set
            {
                this.incomeEstimatorT102Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("InsuranceClaimsUA01")]
        public InsuranceClaimsUA01[] InsuranceClaimsUA01
        {
            get
            {
                return this.insuranceClaimsUA01Field;
            }
            set
            {
                this.insuranceClaimsUA01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("InsuranceClaimsUB01")]
        public InsuranceClaimsUB01[] InsuranceClaimsUB01
        {
            get
            {
                return this.insuranceClaimsUB01Field;
            }
            set
            {
                this.insuranceClaimsUB01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("InsuranceClaimsUC01")]
        public InsuranceClaimsUC01[] InsuranceClaimsUC01
        {
            get
            {
                return this.insuranceClaimsUC01Field;
            }
            set
            {
                this.insuranceClaimsUC01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("InsuranceClaimsUI01")]
        public InsuranceClaimsUI01[] InsuranceClaimsUI01
        {
            get
            {
                return this.insuranceClaimsUI01Field;
            }
            set
            {
                this.insuranceClaimsUI01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("IndividualTrace04")]
        public IndividualTrace04[] IndividualTrace04
        {
            get
            {
                return this.individualTrace04Field;
            }
            set
            {
                this.individualTrace04Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("IndividualTrace05")]
        public IndividualTrace05[] IndividualTrace05
        {
            get
            {
                return this.individualTrace05Field;
            }
            set
            {
                this.individualTrace05Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("JudgementsNJ06")]
        public JudgementsNJ06[] JudgementsNJ06
        {
            get
            {
                return this.judgementsNJ06Field;
            }
            set
            {
                this.judgementsNJ06Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("JudgementsNJ07")]
        public JudgementsNJ07[] JudgementsNJ07
        {
            get
            {
                return this.judgementsNJ07Field;
            }
            set
            {
                this.judgementsNJ07Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("NLRAccountInformationMP01")]
        public NLRAccountInformationMP01[] NLRAccountInformationMP01
        {
            get
            {
                return this.nLRAccountInformationMP01Field;
            }
            set
            {
                this.nLRAccountInformationMP01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("NLRAccountInformationMP03")]
        public NLRAccountInformationMP03[] NLRAccountInformationMP03
        {
            get
            {
                return this.nLRAccountInformationMP03Field;
            }
            set
            {
                this.nLRAccountInformationMP03Field = value;
            }
        }


        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayItemAttribute("NLRAccountInformationM701", typeof(NLRAccountInformationM701), Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public NLRAccountInformationM701[] NLRAccountsInformationM701
        {
            get
            {
                return this.nLRAccountsInformationM701Field;
            }
            set
            {
                this.nLRAccountsInformationM701Field = value;
            }
        }


        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayItemAttribute("NLRAccountInformationM703", typeof(NLRAccountInformationM703), Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public NLRAccountInformationM703[] NLRAccountsInformationM703
        {
            get
            {
                return this.nLRAccountsInformationM703Field;
            }
            set
            {
                this.nLRAccountsInformationM703Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("NLRAccountInformationExtendedM8")]
        public NLRAccountInformationExtendedM8[] NLRAccountInformationExtendedM8
        {
            get
            {
                return this.nLRAccountInformationExtendedM8Field;
            }
            set
            {
                this.nLRAccountInformationExtendedM8Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("NLRConfirmationMR01", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public NLRConfirmationMR01 NLRConfirmationMR01
        {
            get
            {
                return this.nLRConfirmationMR01Field;
            }
            set
            {
                this.nLRConfirmationMR01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("NLRConfirmationMZ01", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public NLRConfirmationMZ01 NLRConfirmationMZ01
        {
            get
            {
                return this.nLRConfirmationMZ01Field;
            }
            set
            {
                this.nLRConfirmationMZ01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("NLRCounterSeqmentMC01")]
        public NLRCounterSeqmentMC01[] NLRCounterSeqmentMC01
        {
            get
            {
                return this.nLRCounterSeqmentMC01Field;
            }
            set
            {
                this.nLRCounterSeqmentMC01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("NLREnquiriesME01")]
        public NLREnquiriesME01[] NLREnquiriesME01
        {
            get
            {
                return this.nLREnquiriesME01Field;
            }
            set
            {
                this.nLREnquiriesME01Field = value;
            }
        }


        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayItemAttribute("NLREnquiryME50", typeof(NLREnquiryME50), Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public NLREnquiryME50[] NLREnquiriesME50
        {
            get
            {
                return this.nLREnquiriesME50Field;
            }
            set
            {
                this.nLREnquiriesME50Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("NLRSummaryMY01", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public NLRSummaryMY01 NLRSummaryMY01
        {
            get
            {
                return this.nLRSummaryMY01Field;
            }
            set
            {
                this.nLRSummaryMY01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("NoticesNN08")]
        public NoticesNN08[] NoticesNN08
        {
            get
            {
                return this.noticesNN08Field;
            }
            set
            {
                this.noticesNN08Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("NoticesNN10")]
        public NoticesNN10[] NoticesNN10
        {
            get
            {
                return this.noticesNN10Field;
            }
            set
            {
                this.noticesNN10Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("NoticesNN07")]
        public NoticesNN07[] NoticesNN07
        {
            get
            {
                return this.noticesNN07Field;
            }
            set
            {
                this.noticesNN07Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("PaymentProfileNP09")]
        public PaymentProfileNP09[] PaymentProfileNP09
        {
            get
            {
                return this.paymentProfileNP09Field;
            }
            set
            {
                this.paymentProfileNP09Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("PaymentProfileNP11")]
        public PaymentProfileNP11[] PaymentProfileNP11
        {
            get
            {
                return this.paymentProfileNP11Field;
            }
            set
            {
                this.paymentProfileNP11Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("PaymentProfileNP15")]
        public PaymentProfileNP15[] PaymentProfileNP15
        {
            get
            {
                return this.paymentProfileNP15Field;
            }
            set
            {
                this.paymentProfileNP15Field = value;
            }
        }


        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayItemAttribute("PaymentProfileP601", typeof(PaymentProfileP601), Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public PaymentProfileP601[] PaymentProfilesP601
        {
            get
            {
                return this.paymentProfilesP601Field;
            }
            set
            {
                this.paymentProfilesP601Field = value;
            }
        }


        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayItemAttribute("PaymentProfileP701", typeof(PaymentProfileP701), Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public PaymentProfileP701[] PaymentProfilesP701
        {
            get
            {
                return this.paymentProfilesP701Field;
            }
            set
            {
                this.paymentProfilesP701Field = value;
            }
        }


        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayItemAttribute("PaymentProfileP702", typeof(PaymentProfileP702), Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public PaymentProfileP702[] PaymentProfilesP702
        {
            get
            {
                return this.paymentProfilesP702Field;
            }
            set
            {
                this.paymentProfilesP702Field = value;
            }
        }


        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayItemAttribute("PaymentProfileP703", typeof(PaymentProfileP703), Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public PaymentProfileP703[] PaymentProfilesP703
        {
            get
            {
                return this.paymentProfilesP703Field;
            }
            set
            {
                this.paymentProfilesP703Field = value;
            }
        }


        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayItemAttribute("PaymentProfileP704", typeof(PaymentProfileP704), Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public PaymentProfileP704[] PaymentProfilesP704
        {
            get
            {
                return this.paymentProfilesP704Field;
            }
            set
            {
                this.paymentProfilesP704Field = value;
            }
        }


        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayItemAttribute("PaymentProfileP705", typeof(PaymentProfileP705), Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public PaymentProfileP705[] PaymentProfilesP705
        {
            get
            {
                return this.paymentProfilesP705Field;
            }
            set
            {
                this.paymentProfilesP705Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("SAFPSNF01")]
        public SAFPSNF01[] SAFPSNF01
        {
            get
            {
                return this.sAFPSNF01Field;
            }
            set
            {
                this.sAFPSNF01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("ScoreCardAC01", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public ScoreCardAC01 ScoreCardAC01
        {
            get
            {
                return this.scoreCardAC01Field;
            }
            set
            {
                this.scoreCardAC01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("ScoreCardBX01", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public ScoreCardBX01 ScoreCardBX01
        {
            get
            {
                return this.scoreCardBX01Field;
            }
            set
            {
                this.scoreCardBX01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("ScoreCardBX03", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public ScoreCardBX03 ScoreCardBX03
        {
            get
            {
                return this.scoreCardBX03Field;
            }
            set
            {
                this.scoreCardBX03Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("ScorecardS101")]
        public ScorecardS101[] ScorecardS101
        {
            get
            {
                return this.scorecardS101Field;
            }
            set
            {
                this.scorecardS101Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("StandardBatchCharsB101", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public StandardBatchCharsB101 StandardBatchCharsB101
        {
            get
            {
                return this.standardBatchCharsB101Field;
            }
            set
            {
                this.standardBatchCharsB101Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("StandardBatchCharsFA01", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public StandardBatchCharsFA01 StandardBatchCharsFA01
        {
            get
            {
                return this.standardBatchCharsFA01Field;
            }
            set
            {
                this.standardBatchCharsFA01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("StandardBatchCharsFA02", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public StandardBatchCharsFA02 StandardBatchCharsFA02
        {
            get
            {
                return this.standardBatchCharsFA02Field;
            }
            set
            {
                this.standardBatchCharsFA02Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("StandardBatchCharsFA03", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public StandardBatchCharsFA03 StandardBatchCharsFA03
        {
            get
            {
                return this.standardBatchCharsFA03Field;
            }
            set
            {
                this.standardBatchCharsFA03Field = value;
            }
        }

        public StandardBatchCharsSB01[] StandardBatchCharsSB01
        {
            get
            {
                return this.standardBatchCharsSB01Field;
            }
            set
            {
                this.standardBatchCharsSB01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("StandardBatchCharsSB04", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public StandardBatchCharsSB04 StandardBatchCharsSB04
        {
            get
            {
                return this.standardBatchCharsSB04Field;
            }
            set
            {
                this.standardBatchCharsSB04Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("StandardBatchCharsSB07", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public StandardBatchCharsSB07 StandardBatchCharsSB07
        {
            get
            {
                return this.standardBatchCharsSB07Field;
            }
            set
            {
                this.standardBatchCharsSB07Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("StandardBatchCharsSB13", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public StandardBatchCharsSB13 StandardBatchCharsSB13
        {
            get
            {
                return this.standardBatchCharsSB13Field;
            }
            set
            {
                this.standardBatchCharsSB13Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("StandardBatchCharsSB18", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public StandardBatchCharsSB18 StandardBatchCharsSB18
        {
            get
            {
                return this.standardBatchCharsSB18Field;
            }
            set
            {
                this.standardBatchCharsSB18Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("StandardBatchCharsSB19", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public StandardBatchCharsSB19 StandardBatchCharsSB19
        {
            get
            {
                return this.standardBatchCharsSB19Field;
            }
            set
            {
                this.standardBatchCharsSB19Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("StandardBatchCharsSB25", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public StandardBatchCharsSB25 StandardBatchCharsSB25
        {
            get
            {
                return this.standardBatchCharsSB25Field;
            }
            set
            {
                this.standardBatchCharsSB25Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("StandardBatchCharsSB27", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public StandardBatchCharsSB27 StandardBatchCharsSB27
        {
            get
            {
                return this.standardBatchCharsSB27Field;
            }
            set
            {
                this.standardBatchCharsSB27Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("StandardBatchCharsSB30", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public StandardBatchCharsSB30 StandardBatchCharsSB30
        {
            get
            {
                return this.standardBatchCharsSB30Field;
            }
            set
            {
                this.standardBatchCharsSB30Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("StandardBatchCharsSB33", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public StandardBatchCharsSB33 StandardBatchCharsSB33
        {
            get
            {
                return this.standardBatchCharsSB33Field;
            }
            set
            {
                this.standardBatchCharsSB33Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("StandardBatchCharsSB37", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public StandardBatchCharsSB37 StandardBatchCharsSB37
        {
            get
            {
                return this.standardBatchCharsSB37Field;
            }
            set
            {
                this.standardBatchCharsSB37Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("StandardBatchCharsSB40", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public StandardBatchCharsSB40 StandardBatchCharsSB40
        {
            get
            {
                return this.standardBatchCharsSB40Field;
            }
            set
            {
                this.standardBatchCharsSB40Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("StandardBatchCharsSB99", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public StandardBatchCharsSB99 StandardBatchCharsSB99
        {
            get
            {
                return this.standardBatchCharsSB99Field;
            }
            set
            {
                this.standardBatchCharsSB99Field = value;
            }
        }


        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayItemAttribute("SuburbCodeGR01", typeof(SuburbCodeGR01), Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public SuburbCodeGR01[] SuburbCodesGR01
        {
            get
            {
                return this.suburbCodesGR01Field;
            }
            set
            {
                this.suburbCodesGR01Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("TraceAlertsNT04")]
        public TraceAlertsNT04[] TraceAlertsNT04
        {
            get
            {
                return this.traceAlertsNT04Field;
            }
            set
            {
                this.traceAlertsNT04Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("TraceAlertsNT06")]
        public TraceAlertsNT06[] TraceAlertsNT06
        {
            get
            {
                return this.traceAlertsNT06Field;
            }
            set
            {
                this.traceAlertsNT06Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("TraceAlertsNT07")]
        public TraceAlertsNT07[] TraceAlertsNT07
        {
            get
            {
                return this.traceAlertsNT07Field;
            }
            set
            {
                this.traceAlertsNT07Field = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("UniqueITCRef", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public UniqueITCRef[] UniqueITCRef
        {
            get
            {
                return this.uniqueITCRefField;
            }
            set
            {
                this.uniqueITCRefField = value;
            }
        }


        //[System.Xml.Serialization.XmlElementAttribute("V1Segment", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public V1Segment[] V1Segment
        {
            get
            {
                return this.v1SegmentField;
            }
            set
            {
                this.v1SegmentField = value;
            }
        }


        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        ////[System.Xml.Serialization.XmlElementAttribute("XmlArrayItemAttribute("VehicleDataUZ01", typeof(VehicleDataUZ01), Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = false)]
        public VehicleDataUZ01[] VehicleDataUZ01
        {
            get
            {
                return this.vehicleDataUZ01Field;
            }
            set
            {
                this.vehicleDataUZ01Field = value;
            }
        }


        public VehicleDataUZ[] VehicleDataUZ
        {
            get
            {
                return this.vehicleDataUZField;
            }
            set
            {
                this.vehicleDataUZField = value;
            }
        }

        public ClaimsSummaryCS[] ClaimsSummaryCS
        {
            get
            {
                return this.claimssummarycsField;
            }
            set
            {
                this.claimssummarycsField = value;
            }
        }

        public DebtCounsellingDC01[] DebtCounsellingDC01
        {
            get
            {
                return this.debtCounsellingDC01Field;
            }
            set
            {
                this.debtCounsellingDC01Field = value;
            }
        }

        public PaymentProfileWMP01[] PaymentProfileWMP01
        {
            get
            {
                return this.paymentProfileWMP01Field;
            }
            set
            {
                this.paymentProfileWMP01Field = value;
            }
        }

        public CampaignInformation CampaignInformation
        {
            get
            {
                return this.campaignInformationField;
            }
            set
            {
                this.campaignInformationField = value;
            }
        }

        public int AffordabilityOverride
        {
            get
            {
                return this.affordabilityOverrideField;
            }
            set
            {
                this.affordabilityOverrideField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime AffordabilityOverrideEndDate
        {
            get
            {
                return this.affordabilityOverrideEndDateField;
            }
            set
            {
                this.affordabilityOverrideEndDateField = value;
            }
        }

        public ScorecardRecommendation ScorecardRecommendation
        {
            get
            {
                return this.scorecardRecommendationField;
            }
            set
            {
                this.scorecardRecommendationField = value;
            }
        }


        private BCCB701[] bccb701field;
        private DeedsMultipleOwners[] deedsmultipleownersfield;
        private DeedsMultipleOwnersDetail[] deedsmultipleownersdetailfield;
        private ForensicLinkagesHeaderFL[] forensiclinkagesheaderflfield;
        private ForensicLinkagesHomeTelephoneFH[] forensiclinkageshometelephonefhfield;
        private ForensicLinkagesWorkTelephoneFW[] forensiclinkagesworktelephonefwfield;
        private ForensicLinkagesCellphoneNumberFX[] forensiclinkagescellphonenumberfxfield;
        private ForensicLinkagesDefaultsAssociates[] forensiclinkagesdefaultsassociatesfield;
        private ForensicLinkagesDefaultsFF[] forensiclinkagesdefaultsfffield;
        private ForensicLinkagesHomeAddressFZ[] forensiclinkageshomeaddressfzfield;
        private ForensicLinkagesJudgementsAssociates[] forensiclinkagesjudgementsassociatesfield;
        private ForensicLinkagesJudgementsFJ[] forensiclinkagesjudgementsfjfield;
        private HistoricContactDetailsHD[] historiccontactdetailshdfield;
        private IndividualTrace06[] individualtrace06field;
        private LinkedBusinessHeaders[] linkedbusinessheadersfield;
        private LinkagesLK[] linkageslkfield;
        private NLRCounterSegmentMC01[] nlrcountersegmentmc01field;
        private NLRCounterSegmentMC50[] nlrcountersegmentmc50field;
        private NLRSummaryMY50[] nlrsummarymy50field;
        private PrincipalDeedsCA[] principaldeedscafield;
        private ScorecardS701[] scorecards701field;
        private AddressAlertRA[] addressalertrafield;
        private AllRiskTypesAR[] allrisktypesarfield;
        private ClaimantHeaderCH[] claimantheaderchfield;
        private ClaimantHeaderHC[] claimantheaderhcfield;
        private ClaimantHeaderSH[] claimantheadershfield;
        private ClaimDetailsCR[] claimdetailscrfield;
        private ClaimsLinkedCL[] claimslinkedclfield;
        private ClaimantSummaryCS[] claimantsummarycsfield;
        private ClaimsCommentSegmentSX[] claimscommentsegmentsxfield;
        private DriverDetailsDR[] driverdetailsdrfield;
        private FraudAlertS6[] fraudalerts6field;
        private HawkAlertHA[] hawkalerthafield;
        private HomeOwnersAO[] homeownersaofield;
        private IdentityVerficationAlertHI[] identityverficationalerthifield;
        private InsuredDetailsIN[] insureddetailsinfield;
        private PersonalLiabiltyPL[] personalliabiltyplfield;
        private ServiceProviderSP[] serviceproviderspfield;
        private ThirdPartyTP[] thirdpartytpfield;
        private VehicleAlertVA[] vehiclealertvafield;
        private VehicleDetailsVH[] vehicledetailsvhfield;
        private NatisEnquiryHistoryUS[] natisenquiryhistoryusfield;
        private NatisInputVerificationUT[] natisinputverificationutfield;
        private NatisDetailedVehicleInformationUU[] natisdetailedvehicleinformationuufield;
        private DriversLicenseInformationUX[] driverslicenseinformationuxfield;
        private InternalMessageIM[] internalmessageimfield;
        private IndivualOwnOtherActivePoliciesPI[] indivualownotheractivepoliciespifield;
        private IndivualOwnOtherClaimDetails0to3yearsI1[] indivualownotherclaimdetails0to3yearsi1field;
        private IndivualOwnOtherClaimDetails4to7yearsSI[] indivualownotherclaimdetails4to7yearssifield;
        private IndivualOwnOtherInactivePoliciesP1[] indivualownotherinactivepoliciesp1field;
        private InputIdentificationIndividualII[] inputidentificationindividualiifield;
        private InputIdentificationRiskAddressIA[] inputidentificationriskaddressiafield;
        private InputIdentificationVehicleIV[] inputidentificationvehicleivfield;
        private InsuranceEnquirySE[] insuranceenquirysefield;
        private PolicySummaryPS[] policysummarypsfield;
        private PolicyCommentSegmentSZ[] policycommentsegmentszfield;
        private RiskAddressOwnOtherActivePoliciesPA[] riskaddressownotheractivepoliciespafield;
        private RiskAddressOwnOtherClaimDetails0to3yearsI3[] riskaddressownotherclaimdetails0to3yearsi3field;
        private RiskAddressOwnOtherClaimDetails4to7yearsSR[] riskaddressownotherclaimdetails4to7yearssrfield;
        private RiskAddressOwnOtherInactivePoliciesP3[] riskaddressownotherinactivepoliciesp3field;
        private VehicleOwnOtherActivePoliciesPV[] vehicleownotheractivepoliciespvfield;
        private VehicleOwnOtherClaimDetails0to3yearsI2[] vehicleownotherclaimdetails0to3yearsi2field;
        private VehicleOwnOtherClaimDetails4to7yearsSV[] vehicleownotherclaimdetails4to7yearssvfield;
        private VehicleOwnOtherInactivePoliciesP2[] vehicleownotherinactivepoliciesp2field;
        private VehicleInformationUR[] vehicleinformationurfield;
        private VehicleOwnership_TitleHolderInformationUW[] vehicleownership_titleholderinformationuwfield;
        private TUVehicleDetailsVL[] tuvehicledetailsvlfield;
        private TUVehicleFinanceDetailsVB[] tuvehiclefinancedetailsvbfield;
        private TUVehiclePreviousRegistrationDetailsVC[] tuvehiclepreviousregistrationdetailsvcfield;
        private TUVehicleMileageRecordedVD[] tuvehiclemileagerecordedvdfield;
        private TUVehicleEnquiryHistoryVE[] tuvehicleenquiryhistoryvefield;
        private TUVehicleEstimateRecordedVF[] tuvehicleestimaterecordedvffield;
        private TUVehicleAlertRecordedVG[] tuvehiclealertrecordedvgfield;
        private TUVehicleStolenRecordedVM[] tuvehiclestolenrecordedvmfield;
        private TUVehicleRegistrationAuthorityHistoryRecordedVI[] tuvehicleregistrationauthorityhistoryrecordedvifield;
        private TUVehicleInsuranceClaimsRecordedVJ[] tuvehicleinsuranceclaimsrecordedvjfield;
        private TUVehicleInsurancePoliciesRecordedVK[] tuvehicleinsurancepoliciesrecordedvkfield;
        private TUInputVerificationUV[] tuinputverificationuvfield;
        private BusinessDeedsSummaryDA[] businessdeedssummarydafield;
        private QualificationValidationNQ[] qualificationvalidationnqfield;

        public BCCB701[] BCCB701 { get { return this.bccb701field; } set { this.bccb701field = value; } }
        public DeedsMultipleOwners[] DeedsMultipleOwners { get { return this.deedsmultipleownersfield; } set { this.deedsmultipleownersfield = value; } }
        public DeedsMultipleOwnersDetail[] DeedsMultipleOwnersDetail { get { return this.deedsmultipleownersdetailfield; } set { this.deedsmultipleownersdetailfield = value; } }
        public ForensicLinkagesHeaderFL[] ForensicLinkagesHeaderFL { get { return this.forensiclinkagesheaderflfield; } set { this.forensiclinkagesheaderflfield = value; } }
        public ForensicLinkagesHomeTelephoneFH[] ForensicLinkagesHomeTelephoneFH { get { return this.forensiclinkageshometelephonefhfield; } set { this.forensiclinkageshometelephonefhfield = value; } }
        public ForensicLinkagesWorkTelephoneFW[] ForensicLinkagesWorkTelephoneFW { get { return this.forensiclinkagesworktelephonefwfield; } set { this.forensiclinkagesworktelephonefwfield = value; } }
        public ForensicLinkagesCellphoneNumberFX[] ForensicLinkagesCellphoneNumberFX { get { return this.forensiclinkagescellphonenumberfxfield; } set { this.forensiclinkagescellphonenumberfxfield = value; } }
        public ForensicLinkagesDefaultsAssociates[] ForensicLinkagesDefaultsAssociates { get { return this.forensiclinkagesdefaultsassociatesfield; } set { this.forensiclinkagesdefaultsassociatesfield = value; } }
        public ForensicLinkagesDefaultsFF[] ForensicLinkagesDefaultsFF { get { return this.forensiclinkagesdefaultsfffield; } set { this.forensiclinkagesdefaultsfffield = value; } }
        public ForensicLinkagesHomeAddressFZ[] ForensicLinkagesHomeAddressFZ { get { return this.forensiclinkageshomeaddressfzfield; } set { this.forensiclinkageshomeaddressfzfield = value; } }
        public ForensicLinkagesJudgementsAssociates[] ForensicLinkagesJudgementsAssociates { get { return this.forensiclinkagesjudgementsassociatesfield; } set { this.forensiclinkagesjudgementsassociatesfield = value; } }
        public ForensicLinkagesJudgementsFJ[] ForensicLinkagesJudgementsFJ { get { return this.forensiclinkagesjudgementsfjfield; } set { this.forensiclinkagesjudgementsfjfield = value; } }
        public HistoricContactDetailsHD[] HistoricContactDetailsHD { get { return this.historiccontactdetailshdfield; } set { this.historiccontactdetailshdfield = value; } }
        public IndividualTrace06[] IndividualTrace06 { get { return this.individualtrace06field; } set { this.individualtrace06field = value; } }
        public LinkedBusinessHeaders[] LinkedBusinessHeaders { get { return this.linkedbusinessheadersfield; } set { this.linkedbusinessheadersfield = value; } }
        public LinkagesLK[] LinkagesLK { get { return this.linkageslkfield; } set { this.linkageslkfield = value; } }
        public NLRCounterSegmentMC01[] NLRCounterSegmentMC01 { get { return this.nlrcountersegmentmc01field; } set { this.nlrcountersegmentmc01field = value; } }
        public NLRCounterSegmentMC50[] NLRCounterSegmentMC50 { get { return this.nlrcountersegmentmc50field; } set { this.nlrcountersegmentmc50field = value; } }
        public NLRSummaryMY50[] NLRSummaryMY50 { get { return this.nlrsummarymy50field; } set { this.nlrsummarymy50field = value; } }
        public PrincipalDeedsCA[] PrincipalDeedsCA { get { return this.principaldeedscafield; } set { this.principaldeedscafield = value; } }
        public ScorecardS701[] ScorecardS701 { get { return this.scorecards701field; } set { this.scorecards701field = value; } }
        public AddressAlertRA[] AddressAlertRA { get { return this.addressalertrafield; } set { this.addressalertrafield = value; } }
        public AllRiskTypesAR[] AllRiskTypesAR { get { return this.allrisktypesarfield; } set { this.allrisktypesarfield = value; } }
        public ClaimantHeaderCH[] ClaimantHeaderCH { get { return this.claimantheaderchfield; } set { this.claimantheaderchfield = value; } }
        public ClaimantHeaderHC[] ClaimantHeaderHC { get { return this.claimantheaderhcfield; } set { this.claimantheaderhcfield = value; } }
        public ClaimantHeaderSH[] ClaimantHeaderSH { get { return this.claimantheadershfield; } set { this.claimantheadershfield = value; } }
        public ClaimDetailsCR[] ClaimDetailsCR { get { return this.claimdetailscrfield; } set { this.claimdetailscrfield = value; } }
        public ClaimsLinkedCL[] ClaimsLinkedCL { get { return this.claimslinkedclfield; } set { this.claimslinkedclfield = value; } }
        public ClaimantSummaryCS[] ClaimantSummaryCS { get { return this.claimantsummarycsfield; } set { this.claimantsummarycsfield = value; } }
        public ClaimsCommentSegmentSX[] ClaimsCommentSegmentSX { get { return this.claimscommentsegmentsxfield; } set { this.claimscommentsegmentsxfield = value; } }
        public DriverDetailsDR[] DriverDetailsDR { get { return this.driverdetailsdrfield; } set { this.driverdetailsdrfield = value; } }
        public FraudAlertS6[] FraudAlertS6 { get { return this.fraudalerts6field; } set { this.fraudalerts6field = value; } }
        public HawkAlertHA[] HawkAlertHA { get { return this.hawkalerthafield; } set { this.hawkalerthafield = value; } }
        public HomeOwnersAO[] HomeOwnersAO { get { return this.homeownersaofield; } set { this.homeownersaofield = value; } }
        public IdentityVerficationAlertHI[] IdentityVerficationAlertHI { get { return this.identityverficationalerthifield; } set { this.identityverficationalerthifield = value; } }
        public InsuredDetailsIN[] InsuredDetailsIN { get { return this.insureddetailsinfield; } set { this.insureddetailsinfield = value; } }
        public PersonalLiabiltyPL[] PersonalLiabiltyPL { get { return this.personalliabiltyplfield; } set { this.personalliabiltyplfield = value; } }
        public ServiceProviderSP[] ServiceProviderSP { get { return this.serviceproviderspfield; } set { this.serviceproviderspfield = value; } }
        public ThirdPartyTP[] ThirdPartyTP { get { return this.thirdpartytpfield; } set { this.thirdpartytpfield = value; } }
        public VehicleAlertVA[] VehicleAlertVA { get { return this.vehiclealertvafield; } set { this.vehiclealertvafield = value; } }
        public VehicleDetailsVH[] VehicleDetailsVH { get { return this.vehicledetailsvhfield; } set { this.vehicledetailsvhfield = value; } }
        public NatisEnquiryHistoryUS[] NatisEnquiryHistoryUS { get { return this.natisenquiryhistoryusfield; } set { this.natisenquiryhistoryusfield = value; } }
        public NatisInputVerificationUT[] NatisInputVerificationUT { get { return this.natisinputverificationutfield; } set { this.natisinputverificationutfield = value; } }
        public NatisDetailedVehicleInformationUU[] NatisDetailedVehicleInformationUU { get { return this.natisdetailedvehicleinformationuufield; } set { this.natisdetailedvehicleinformationuufield = value; } }
        public DriversLicenseInformationUX[] DriversLicenseInformationUX { get { return this.driverslicenseinformationuxfield; } set { this.driverslicenseinformationuxfield = value; } }
        public InternalMessageIM[] InternalMessageIM { get { return this.internalmessageimfield; } set { this.internalmessageimfield = value; } }
        public IndivualOwnOtherActivePoliciesPI[] IndivualOwnOtherActivePoliciesPI { get { return this.indivualownotheractivepoliciespifield; } set { this.indivualownotheractivepoliciespifield = value; } }
        public IndivualOwnOtherClaimDetails0to3yearsI1[] IndivualOwnOtherClaimDetails0to3yearsI1 { get { return this.indivualownotherclaimdetails0to3yearsi1field; } set { this.indivualownotherclaimdetails0to3yearsi1field = value; } }
        public IndivualOwnOtherClaimDetails4to7yearsSI[] IndivualOwnOtherClaimDetails4to7yearsSI { get { return this.indivualownotherclaimdetails4to7yearssifield; } set { this.indivualownotherclaimdetails4to7yearssifield = value; } }
        public IndivualOwnOtherInactivePoliciesP1[] IndivualOwnOtherInactivePoliciesP1 { get { return this.indivualownotherinactivepoliciesp1field; } set { this.indivualownotherinactivepoliciesp1field = value; } }
        public InputIdentificationIndividualII[] InputIdentificationIndividualII { get { return this.inputidentificationindividualiifield; } set { this.inputidentificationindividualiifield = value; } }
        public InputIdentificationRiskAddressIA[] InputIdentificationRiskAddressIA { get { return this.inputidentificationriskaddressiafield; } set { this.inputidentificationriskaddressiafield = value; } }
        public InputIdentificationVehicleIV[] InputIdentificationVehicleIV { get { return this.inputidentificationvehicleivfield; } set { this.inputidentificationvehicleivfield = value; } }
        public InsuranceEnquirySE[] InsuranceEnquirySE { get { return this.insuranceenquirysefield; } set { this.insuranceenquirysefield = value; } }
        public PolicySummaryPS[] PolicySummaryPS { get { return this.policysummarypsfield; } set { this.policysummarypsfield = value; } }
        public PolicyCommentSegmentSZ[] PolicyCommentSegmentSZ { get { return this.policycommentsegmentszfield; } set { this.policycommentsegmentszfield = value; } }
        public RiskAddressOwnOtherActivePoliciesPA[] RiskAddressOwnOtherActivePoliciesPA { get { return this.riskaddressownotheractivepoliciespafield; } set { this.riskaddressownotheractivepoliciespafield = value; } }
        public RiskAddressOwnOtherClaimDetails0to3yearsI3[] RiskAddressOwnOtherClaimDetails0to3yearsI3 { get { return this.riskaddressownotherclaimdetails0to3yearsi3field; } set { this.riskaddressownotherclaimdetails0to3yearsi3field = value; } }
        public RiskAddressOwnOtherClaimDetails4to7yearsSR[] RiskAddressOwnOtherClaimDetails4to7yearsSR { get { return this.riskaddressownotherclaimdetails4to7yearssrfield; } set { this.riskaddressownotherclaimdetails4to7yearssrfield = value; } }
        public RiskAddressOwnOtherInactivePoliciesP3[] RiskAddressOwnOtherInactivePoliciesP3 { get { return this.riskaddressownotherinactivepoliciesp3field; } set { this.riskaddressownotherinactivepoliciesp3field = value; } }
        public VehicleOwnOtherActivePoliciesPV[] VehicleOwnOtherActivePoliciesPV { get { return this.vehicleownotheractivepoliciespvfield; } set { this.vehicleownotheractivepoliciespvfield = value; } }
        public VehicleOwnOtherClaimDetails0to3yearsI2[] VehicleOwnOtherClaimDetails0to3yearsI2 { get { return this.vehicleownotherclaimdetails0to3yearsi2field; } set { this.vehicleownotherclaimdetails0to3yearsi2field = value; } }
        public VehicleOwnOtherClaimDetails4to7yearsSV[] VehicleOwnOtherClaimDetails4to7yearsSV { get { return this.vehicleownotherclaimdetails4to7yearssvfield; } set { this.vehicleownotherclaimdetails4to7yearssvfield = value; } }
        public VehicleOwnOtherInactivePoliciesP2[] VehicleOwnOtherInactivePoliciesP2 { get { return this.vehicleownotherinactivepoliciesp2field; } set { this.vehicleownotherinactivepoliciesp2field = value; } }
        public VehicleInformationUR[] VehicleInformationUR { get { return this.vehicleinformationurfield; } set { this.vehicleinformationurfield = value; } }
        public VehicleOwnership_TitleHolderInformationUW[] VehicleOwnership_TitleHolderInformationUW { get { return this.vehicleownership_titleholderinformationuwfield; } set { this.vehicleownership_titleholderinformationuwfield = value; } }
        public TUVehicleDetailsVL[] TUVehicleDetailsVL { get { return this.tuvehicledetailsvlfield; } set { this.tuvehicledetailsvlfield = value; } }
        public TUVehicleFinanceDetailsVB[] TUVehicleFinanceDetailsVB { get { return this.tuvehiclefinancedetailsvbfield; } set { this.tuvehiclefinancedetailsvbfield = value; } }
        public TUVehiclePreviousRegistrationDetailsVC[] TUVehiclePreviousRegistrationDetailsVC { get { return this.tuvehiclepreviousregistrationdetailsvcfield; } set { this.tuvehiclepreviousregistrationdetailsvcfield = value; } }
        public TUVehicleMileageRecordedVD[] TUVehicleMileageRecordedVD { get { return this.tuvehiclemileagerecordedvdfield; } set { this.tuvehiclemileagerecordedvdfield = value; } }
        public TUVehicleEnquiryHistoryVE[] TUVehicleEnquiryHistoryVE { get { return this.tuvehicleenquiryhistoryvefield; } set { this.tuvehicleenquiryhistoryvefield = value; } }
        public TUVehicleEstimateRecordedVF[] TUVehicleEstimateRecordedVF { get { return this.tuvehicleestimaterecordedvffield; } set { this.tuvehicleestimaterecordedvffield = value; } }
        public TUVehicleAlertRecordedVG[] TUVehicleAlertRecordedVG { get { return this.tuvehiclealertrecordedvgfield; } set { this.tuvehiclealertrecordedvgfield = value; } }
        public TUVehicleStolenRecordedVM[] TUVehicleStolenRecordedVM { get { return this.tuvehiclestolenrecordedvmfield; } set { this.tuvehiclestolenrecordedvmfield = value; } }
        public TUVehicleRegistrationAuthorityHistoryRecordedVI[] TUVehicleRegistrationAuthorityHistoryRecordedVI { get { return this.tuvehicleregistrationauthorityhistoryrecordedvifield; } set { this.tuvehicleregistrationauthorityhistoryrecordedvifield = value; } }
        public TUVehicleInsuranceClaimsRecordedVJ[] TUVehicleInsuranceClaimsRecordedVJ { get { return this.tuvehicleinsuranceclaimsrecordedvjfield; } set { this.tuvehicleinsuranceclaimsrecordedvjfield = value; } }
        public TUVehicleInsurancePoliciesRecordedVK[] TUVehicleInsurancePoliciesRecordedVK { get { return this.tuvehicleinsurancepoliciesrecordedvkfield; } set { this.tuvehicleinsurancepoliciesrecordedvkfield = value; } }
        public TUInputVerificationUV[] TUInputVerificationUV { get { return this.tuinputverificationuvfield; } set { this.tuinputverificationuvfield = value; } }
        public BusinessDeedsSummaryDA[] BusinessDeedsSummaryDA { get { return this.businessdeedssummarydafield; } set { this.businessdeedssummarydafield = value; } }
        public QualificationValidationNQ[] QualificationValidationNQ { get { return this.qualificationvalidationnqfield; } set { this.qualificationvalidationnqfield = value; } }



    }

    public partial class CampaignInformation
    {

        private bool preApprovedField;

        private string campaignNameField;

        private string campaignFileNameField;

        private System.DateTime campaignValidityField;

        /// <remarks/>
        public bool PreApproved
        {
            get
            {
                return this.preApprovedField;
            }
            set
            {
                this.preApprovedField = value;
            }
        }

        /// <remarks/>
        public string CampaignName
        {
            get
            {
                return this.campaignNameField;
            }
            set
            {
                this.campaignNameField = value;
            }
        }

        /// <remarks/>
        public string CampaignFileName
        {
            get
            {
                return this.campaignFileNameField;
            }
            set
            {
                this.campaignFileNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime CampaignValidity
        {
            get
            {
                return this.campaignValidityField;
            }
            set
            {
                this.campaignValidityField = value;
            }
        }
    }

    public partial class ScorecardRecommendation
    {

        private string recommendationField;

        private ScorecardRecommendationReasonsReason[] reasonsField;

        /// <remarks/>
        public string Recommendation
        {
            get
            {
                return this.recommendationField;
            }
            set
            {
                this.recommendationField = value;
            }
        }

        /// <remarks/>
        /// [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [XmlArray("Reasons")]
        [XmlArrayItem("Reason")]
        public ScorecardRecommendationReasonsReason[] Reasons
        {
            get
            {
                return this.reasonsField;
            }
            set
            {
                this.reasonsField = value;
            }
        }
    }

    /// <remarks/>


    /// <remarks/>

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ScorecardRecommendationReasonsReason
    {

        private string codeField;

        private string internalField;

        private string externalField;

        /// <remarks/>
        public string Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }

        /// <remarks/>
        public string Internal
        {
            get
            {
                return this.internalField;
            }
            set
            {
                this.internalField = value;
            }
        }

        /// <remarks/>
        public string External
        {
            get
            {
                return this.externalField;
            }
            set
            {
                this.externalField = value;
            }
        }
    }
    // [XmlType( "SubmitApplication",Namespace = "http://www.web.xds.co.za/XDSConnectWS")]
    /// <remarks/>
    public partial class SubmitApplication
    {

        private MTNInput inputField;

        /// <remarks/>
        public MTNInput input
        {
            get
            {
                return this.inputField;
            }
            set
            {
                this.inputField = value;
            }
        }
    }

    /// <remarks/>
    public partial class MTNInput
    {

        private string companyField;

        private string ruleSetField;

        private string branchField;

        private string userField;

        private string passwordField;

        private string isExistingClientField;

        private string grossMonthlyIncomeField;

        private string surnameField;

        private string firstNameField;

        private string iDTypeField;

        private string birthDateField;

        private string identityNumberField;

        private string genderField;

        private string maritalStatusField;

        private string addressLine1Field;

        private string addressLine2Field;

        private string suburbField;

        private string cityField;

        private string postalCodeField;

        private string homeTelephoneCodeField;

        private string homeTelephoneNumberField;

        private string workTelephoneCodeField;

        private string workTelephoneNumberField;

        private string cellNumberField;

        private string occupationField;

        private string employerField;

        private string numberOfYearsAtEmployerField;

        private string bankNameField;

        private string bankAccountTypeField;

        private string bankBranchCodeField;

        private string bankAccountNumberField;

        private string highestQualificationField;

        private string dealerCodeField;

        private string customerNameField;

        private string channelField;

        private string alternatecontactnumberField;
        private string emailaddressField;
        private string uniquereferencenumberField;

        /// <remarks/>
        public string Company
        {
            get
            {
                return this.companyField;
            }
            set
            {
                this.companyField = value;
            }
        }

        /// <remarks/>
        public string RuleSet
        {
            get
            {
                return this.ruleSetField;
            }
            set
            {
                this.ruleSetField = value;
            }
        }

        /// <remarks/>
        public string Branch
        {
            get
            {
                return this.branchField;
            }
            set
            {
                this.branchField = value;
            }
        }

        /// <remarks/>
        public string User
        {
            get
            {
                return this.userField;
            }
            set
            {
                this.userField = value;
            }
        }

        /// <remarks/>
        public string Password
        {
            get
            {
                return this.passwordField;
            }
            set
            {
                this.passwordField = value;
            }
        }

        /// <remarks/>
        public string IsExistingClient
        {
            get
            {
                return this.isExistingClientField;
            }
            set
            {
                this.isExistingClientField = value;
            }
        }

        /// <remarks/>
        public string GrossMonthlyIncome
        {
            get
            {
                return this.grossMonthlyIncomeField;
            }
            set
            {
                this.grossMonthlyIncomeField = value;
            }
        }

        /// <remarks/>
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        public string FirstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string IDType
        {
            get
            {
                return this.iDTypeField;
            }
            set
            {
                this.iDTypeField = value;
            }
        }

        /// <remarks/>
        public string BirthDate
        {
            get
            {
                return this.birthDateField;
            }
            set
            {
                this.birthDateField = value;
            }
        }

        /// <remarks/>
        public string IdentityNumber
        {
            get
            {
                return this.identityNumberField;
            }
            set
            {
                this.identityNumberField = value;
            }
        }

        /// <remarks/>
        public string Gender
        {
            get
            {
                return this.genderField;
            }
            set
            {
                this.genderField = value;
            }
        }

        /// <remarks/>
        public string MaritalStatus
        {
            get
            {
                return this.maritalStatusField;
            }
            set
            {
                this.maritalStatusField = value;
            }
        }

        /// <remarks/>
        public string AddressLine1
        {
            get
            {
                return this.addressLine1Field;
            }
            set
            {
                this.addressLine1Field = value;
            }
        }

        /// <remarks/>
        public string AddressLine2
        {
            get
            {
                return this.addressLine2Field;
            }
            set
            {
                this.addressLine2Field = value;
            }
        }

        /// <remarks/>
        public string Suburb
        {
            get
            {
                return this.suburbField;
            }
            set
            {
                this.suburbField = value;
            }
        }

        /// <remarks/>
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        public string PostalCode
        {
            get
            {
                return this.postalCodeField;
            }
            set
            {
                this.postalCodeField = value;
            }
        }

        /// <remarks/>
        public string HomeTelephoneCode
        {
            get
            {
                return this.homeTelephoneCodeField;
            }
            set
            {
                this.homeTelephoneCodeField = value;
            }
        }

        /// <remarks/>
        public string HomeTelephoneNumber
        {
            get
            {
                return this.homeTelephoneNumberField;
            }
            set
            {
                this.homeTelephoneNumberField = value;
            }
        }

        /// <remarks/>
        public string WorkTelephoneCode
        {
            get
            {
                return this.workTelephoneCodeField;
            }
            set
            {
                this.workTelephoneCodeField = value;
            }
        }

        /// <remarks/>
        public string WorkTelephoneNumber
        {
            get
            {
                return this.workTelephoneNumberField;
            }
            set
            {
                this.workTelephoneNumberField = value;
            }
        }

        /// <remarks/>
        public string CellNumber
        {
            get
            {
                return this.cellNumberField;
            }
            set
            {
                this.cellNumberField = value;
            }
        }

        /// <remarks/>
        public string Occupation
        {
            get
            {
                return this.occupationField;
            }
            set
            {
                this.occupationField = value;
            }
        }

        /// <remarks/>
        public string Employer
        {
            get
            {
                return this.employerField;
            }
            set
            {
                this.employerField = value;
            }
        }

        /// <remarks/>
        public string NumberOfYearsAtEmployer
        {
            get
            {
                return this.numberOfYearsAtEmployerField;
            }
            set
            {
                this.numberOfYearsAtEmployerField = value;
            }
        }

        /// <remarks/>
        public string BankName
        {
            get
            {
                return this.bankNameField;
            }
            set
            {
                this.bankNameField = value;
            }
        }

        /// <remarks/>
        public string BankAccountType
        {
            get
            {
                return this.bankAccountTypeField;
            }
            set
            {
                this.bankAccountTypeField = value;
            }
        }

        /// <remarks/>
        public string BankBranchCode
        {
            get
            {
                return this.bankBranchCodeField;
            }
            set
            {
                this.bankBranchCodeField = value;
            }
        }

        /// <remarks/>
        public string BankAccountNumber
        {
            get
            {
                return this.bankAccountNumberField;
            }
            set
            {
                this.bankAccountNumberField = value;
            }
        }

        /// <remarks/>
        public string HighestQualification
        {
            get
            {
                return this.highestQualificationField;
            }
            set
            {
                this.highestQualificationField = value;
            }
        }

        /// <remarks/>
        public string DealerCode
        {
            get
            {
                return this.dealerCodeField;
            }
            set
            {
                this.dealerCodeField = value;
            }
        }

        /// <remarks/>
        public string CustomerName
        {
            get
            {
                return this.customerNameField;
            }
            set
            {
                this.customerNameField = value;
            }
        }

        /// <remarks/>
        public string Channel
        {
            get
            {
                return this.channelField;
            }
            set
            {
                this.channelField = value;
            }
        }

        /// <remarks/>
        public string AlternateContactNumber
        {
            get
            {
                return this.alternatecontactnumberField;
            }
            set
            {
                this.alternatecontactnumberField = value;
            }
        }

        /// <remarks/>
        public string EmailAddress
        {
            get
            {
                return this.emailaddressField;
            }
            set
            {
                this.emailaddressField = value;
            }
        }

        public string UniqueReferenceNumber
        {
            get
            {
                return this.uniquereferencenumberField;
            }
            set
            {
                this.uniquereferencenumberField = value;
            }
        }
    }

    public class FraudScoreFS01
    {

        private string recordSequenceField;

        private string partField;

        private string partSequenceField;

        private string consumerNoField;

        private string ratingField;

        private string ratingDescriptionField;

        private string[] reasonCodeField;

        private string[] reasonDescriptionField;



        public string RecordSequence
        {
            get
            {
                return this.recordSequenceField;
            }
            set
            {
                this.recordSequenceField = value;
            }
        }



        public string Part
        {
            get
            {
                return this.partField;
            }
            set
            {
                this.partField = value;
            }
        }



        public string PartSequence
        {
            get
            {
                return this.partSequenceField;
            }
            set
            {
                this.partSequenceField = value;
            }
        }



        public string ConsumerNo
        {
            get
            {
                return this.consumerNoField;
            }
            set
            {
                this.consumerNoField = value;
            }
        }



        public string Rating
        {
            get
            {
                return this.ratingField;
            }
            set
            {
                this.ratingField = value;
            }
        }



        public string RatingDescription
        {
            get
            {
                return this.ratingDescriptionField;
            }
            set
            {
                this.ratingDescriptionField = value;
            }
        }



        public string[] ReasonCode
        {
            get
            {
                return this.reasonCodeField;
            }
            set
            {
                this.reasonCodeField = value;
            }
        }



        public string[] ReasonDescription
        {
            get
            {
                return this.reasonDescriptionField;
            }
            set
            {
                this.reasonDescriptionField = value;
            }
        }
    }

    public class ConsumerTelephoneHistoryNW01
    {

        private string consumerNoField;

        private string workNumbersField;

        private string homeNumbersField;

        private string cellNumbersField;



        public string ConsumerNo
        {
            get
            {
                return this.consumerNoField;
            }
            set
            {
                this.consumerNoField = value;
            }
        }



        public string WorkNumbers
        {
            get
            {
                return this.workNumbersField;
            }
            set
            {
                this.workNumbersField = value;
            }
        }



        public string HomeNumbers
        {
            get
            {
                return this.homeNumbersField;
            }
            set
            {
                this.homeNumbersField = value;
            }
        }



        public string CellNumbers
        {
            get
            {
                return this.cellNumbersField;
            }
            set
            {
                this.cellNumbersField = value;
            }
        }
    }
}
