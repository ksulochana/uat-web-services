﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace XDSPortalLibrary.Entity_Layer
{

   public class ConsumerTrace
    {
        public ConsumerTrace()
        {
        }

        int subscriberIDvalue,productIDvalue,ConsumerIDValue=0,HomeAffairsIDValue = 0;
        string strIDType = string.Empty, IDnovalue = string.Empty,strTmpReference = string.Empty, passportNovalue = string.Empty, SurNamevalue = string.Empty, MaidenNamevalue = string.Empty, FirstNamevalue = string.Empty, SecondNamevalue = string.Empty, Gendervalue = string.Empty, ExternalReferencevalue = string.Empty, FirstInitialvalue = string.Empty, SecondInitialvalue = string.Empty, ReferenceNoValue = string.Empty;
        DateTime DOBvalue;
        bool ConfirmationChkBoxvalue, IsConsumerMatchingvalue = false, _IsCustomVetting = false;
        DataSet BonusSegmentsDS;
        private bool boolBonusCheck = false;
        double _GrossMonthlyIncome = 0;
        string _Income = string.Empty;
        private string strdataSegments = string.Empty;
        private string strAssociationCode = string.Empty;
        private int intEnquiryID = 0;
        private int intEnquiryResultID = 0;
        private string strAccountType = string.Empty;
        private string strBankname = string.Empty;
        //private string strPostalCode = string.Empty;
       private string strCreatedbyUser= string.Empty;
       private string strAssociationTypeCode = string.Empty;

       private string strCompanyName = string.Empty;
       private string strBranch = string.Empty;
       private string strUserName = string.Empty;
       private string strPassword = string.Empty;
       private string strRuleSet = string.Empty;
       private string strTitle = string.Empty;
       private string strAddressLine1 = string.Empty;
       private string strAddressLine2 = string.Empty;
       private string strSuburb = string.Empty;
       private string strCity = string.Empty;
       private string strPhysicalPostalCode = string.Empty;
       private string strProvinceCode = string.Empty;
       private string strOwnerTenant = string.Empty;
       private string strPostalAddressLine1 = string.Empty;
       private string strPostalAddressLine2 = string.Empty;
       private string strPostalSuburb = string.Empty;
       private string strPostalCity = string.Empty;
       private string strPostalCode = string.Empty;
       private string strPostalProvinceCode = string.Empty;
       private string strHomeTelephoneCode = string.Empty;
       private string strHomePhoneNumber = string.Empty;
       private string strWorkTelephoneCode = string.Empty;
       private string strWorkPhoneNumber = string.Empty;
       private int strEmploymentDuration = 0;
       private string _YearsatCurrentEmployer = string.Empty;
       private string strAccountHolderName = string.Empty;
       private string strBranchCode = string.Empty;
       private string strBankAccountNumber = string.Empty;
       private string strCreditCardNumber = string.Empty;
       private string strSFID = string.Empty;

       private int intRequestNo = 0;
       private int intRequestMSISDN = 0;
       private string strContactNo = string.Empty;

       private bool bSAFPSInd = false;
       private bool bDisplayUnusableInformation = false;
       private string Processingdate = string.Empty;
       private string strEmploymentType = string.Empty;
       private string strEmploymentSector = string.Empty;
       private string strNoOfDependants = string.Empty;
        private DataTable dtIDnos = new DataTable();
        private string strcellPhoneNo = string.Empty;
        private string strPressageScoreVersion = string.Empty;
       
       public string Income
       {
           get { return this._Income; }
           set { this._Income = value; }
       }

       public string YearsatCurrentEmployer
       {
           get { return this._YearsatCurrentEmployer; }
           set { this._YearsatCurrentEmployer = value; }
       }

       public int RequestNo
       {
           get { return this.intRequestNo; }
           set { this.intRequestNo = value; }
       }

       public int RequestMSISDN
       {
           get { return this.intRequestMSISDN; }
           set { this.intRequestMSISDN = value; }
       }

       public string ContactNo
       {
           get { return this.strContactNo; }
           set { this.strContactNo = value; }
       }

       public string IDType
       {
           get { return this.strIDType; }
           set { this.strIDType = value; }
       }

       public string CompanyName
       {
           get { return this.strCompanyName; }
           set { this.strCompanyName = value; }
       }

       public string Branch
       {
           get { return this.strBranch; }
           set { this.strBranch = value; }
       }

       public string UserName
       {
           get { return this.strUserName; }
           set { this.strUserName = value; }
       }

       public string Password
       {
           get { return this.strPassword; }
           set { this.strPassword = value; }
       }

       public string RuleSet
       {
           get { return this.strRuleSet; }
           set { this.strRuleSet = value; }
       }

       public string Title
       {
           get { return this.strTitle; }
           set { this.strTitle = value; }
       }

       public string AddressLine1
       {
           get { return this.strAddressLine1; }
           set { this.strAddressLine1 = value; }
       }

       public string AddressLine2
       {
           get { return this.strAddressLine2; }
           set { this.strAddressLine2 = value; }
       }

       public string Suburb
       {
           get { return this.strSuburb; }
           set { this.strSuburb = value; }
       }

       public string City
       {
           get { return this.strCity; }
           set { this.strCity = value; }
       }

       public string PhysicalPostalCode
       {
           get { return this.strPhysicalPostalCode; }
           set { this.strPhysicalPostalCode = value; }
       }

       public string ProvinceCode
       {
           get { return this.strProvinceCode; }
           set { this.strProvinceCode = value; }
       }

       public string OwnerTenant
       {
           get { return this.strOwnerTenant; }
           set { this.strOwnerTenant = value; }
       }

       public string PostalAddressLine1
       {
           get { return this.strPostalAddressLine1; }
           set { this.strPostalAddressLine1 = value; }
       }

       public string PostalAddressLine2
       {
           get { return this.strPostalAddressLine2; }
           set { this.strPostalAddressLine2 = value; }
       }

       public string PostalSuburb
       {
           get { return this.strPostalSuburb; }
           set { this.strPostalSuburb = value; }
       }

       public string PostalCity
       {
           get { return this.strPostalCity; }
           set { this.strPostalCity = value; }
       }

       public string PostalCode
       {
           get { return this.strPostalCode; }
           set { this.strPostalCode = value; }
       }

       public string PostalProvinceCode
       {
           get { return this.strPostalProvinceCode; }
           set { this.strPostalProvinceCode = value; }
       }

       public string HomeTelephoneCode
       {
           get { return this.strHomeTelephoneCode; }
           set { this.strHomeTelephoneCode = value; }
       }

       public string HomePhoneNumber
       {
           get { return this.strHomePhoneNumber; }
           set { this.strHomePhoneNumber = value; }
       }

       public string WorkTelephoneCode
       {
           get { return this.strWorkTelephoneCode; }
           set { this.strWorkTelephoneCode = value; }
       }

       public string WorkPhoneNumber
       {
           get { return this.strWorkPhoneNumber; }
           set { this.strWorkPhoneNumber = value; }
       }

       public int EmploymentDuration
       {
           get { return this.strEmploymentDuration; }
           set { this.strEmploymentDuration = value; }
       }

       public string AccountHolderName
       {
           get { return this.strAccountHolderName; }
           set { this.strAccountHolderName = value; }
       }

       public string BranchCode
       {
           get { return this.strBranchCode; }
           set { this.strBranchCode = value; }
       }

       public string BankAccountNumber
       {
           get { return this.strBankAccountNumber; }
           set { this.strBankAccountNumber = value; }
       }

       public string CreditCardNumber
       {
           get { return this.strCreditCardNumber; }
           set { this.strCreditCardNumber = value; }
       }

       public string SFID
       {
           get { return this.strSFID; }
           set { this.strSFID = value; }
       }


       public string AccountType
       {
           get { return this.strAccountType; }
           set { this.strAccountType = value; }
       }
       public string BankName
       {
           get { return this.strBankname; }
           set { this.strBankname = value; }
       }
       //public string PostalCode
       //{
       //    get { return this.strPostalCode; }
       //    set { this.strPostalCode = value; }
       //}
       public string CreatedbyUser
       {
           get { return this.strCreatedbyUser; }
           set { this.strCreatedbyUser = value; }
       }

        public int EnquiryResultID
        {
            get { return this.intEnquiryResultID; }
            set { this.intEnquiryResultID = value; }
        }
        public int EnquiryID
        {
            get { return this.intEnquiryID; }
            set { this.intEnquiryID = value; }
        }
        public string SubscriberAssociationCode
        {
            get { return this.strAssociationCode; }
            set { this.strAssociationCode = value; }
        }

        public string DataSegments
        {
            get { return this.strdataSegments; }
            set { this.strdataSegments = value; }
        }


        public bool BonusCheck
        {
            get { return this.boolBonusCheck; }
            set { this.boolBonusCheck = value; }
        }

        public int ConsumerID
        {
            get
            {
                return this.ConsumerIDValue;
            }
            set
            {
                this.ConsumerIDValue = value;
            }
        }
        public double GrossMonthlyIncome
        {
            get
            {
                if (String.IsNullOrEmpty(this._GrossMonthlyIncome.ToString()))
                    return Convert.ToInt16(DBNull.Value);
                else
                    return this._GrossMonthlyIncome;
            }
            set
            {
                this._GrossMonthlyIncome = value;
            }
        }
        public int subscriberID
        {
            get
            {
                if (String.IsNullOrEmpty(this.subscriberIDvalue.ToString()))
                return Convert.ToInt16(DBNull.Value);
                else
                    return this.subscriberIDvalue;
            }
            set
            {
                this.subscriberIDvalue=value;
            }
        }
        public int ProductID
        {
            get
            {
                if (String.IsNullOrEmpty(this.subscriberIDvalue.ToString()))
                    return Convert.ToInt16(DBNull.Value);
                else
                return this.productIDvalue;
            }
            set
            {
               this.productIDvalue = value;
            }
        }
        public string IDno
        {
            get
            {
                if (String.IsNullOrEmpty(this.IDnovalue))
                    return DBNull.Value.ToString();
                else
                return this.IDnovalue;
            }
            set
            {
                this.IDnovalue = value;
            }
        }
        public string Passportno
        {
            get
            {
                if (String.IsNullOrEmpty(this.passportNovalue))
                    return DBNull.Value.ToString();
                else
                return this.passportNovalue;
            }
            set
            {
                this.passportNovalue = value;
            }
        }
        public string TmpReference
        {
            get
            {
                if (String.IsNullOrEmpty(this.strTmpReference))
                    return DBNull.Value.ToString();
                else
                    return this.strTmpReference;
            }
            set
            {
                this.strTmpReference = value;
            }
        }
        public string Surname
        {
            get
            {
                if (String.IsNullOrEmpty(this.SurNamevalue))
                    return DBNull.Value.ToString();
                else
                return this.SurNamevalue;
            }
            set
            {
                this.SurNamevalue = value;
            }
        }
        public string MaidenName
        {
            get
            {
                if (String.IsNullOrEmpty(this.MaidenNamevalue))
                    return DBNull.Value.ToString();
                else
                return this.MaidenNamevalue;
            }
            set
            {
                this.MaidenNamevalue = value;
            }
        }
        public string FirstName
        {
            get
            {
                if (String.IsNullOrEmpty(this.FirstNamevalue))
                    return DBNull.Value.ToString();
                else
                return this.FirstNamevalue;
            }
            set
            {
                this.FirstNamevalue = value;
            }
        }
        public string SecondName
        {
            get
            {
                if (String.IsNullOrEmpty(this.SecondNamevalue))
                    return DBNull.Value.ToString();
                else
                return this.SecondNamevalue;
            }
            set
            {
                this.SecondNamevalue = value;
            }
        }
        public string Gender
        {
            get
            {
                if (String.IsNullOrEmpty(this.Gendervalue))
                    return DBNull.Value.ToString();
                else
                return this.Gendervalue;
            }
            set
            {
                this.Gendervalue = value;
            }
        }
        public string ExternalReference
        {
            get
            {
                if (String.IsNullOrEmpty(this.ExternalReferencevalue))
                    return DBNull.Value.ToString();
                else
                return this.ExternalReferencevalue;
            }
            set
            {
                this.ExternalReferencevalue = value;
            }
        }

        
        public DateTime DOB
        {
            get
            {
                if (this.DOBvalue == null)
                    return Convert.ToDateTime(DBNull.Value);
                else
                return this.DOBvalue;
            }
            set
            {
                this.DOBvalue = value;
            }
        }
        public string FirstInitial
        {
            get
            {
                if (String.IsNullOrEmpty(this.FirstInitialvalue))
                    return DBNull.Value.ToString();
                else
                    return this.FirstInitialvalue;
            }
            set
            {
                this.FirstInitialvalue = value;
            }
        }
        public string SecondInitial
        {
            get
            {
                if (String.IsNullOrEmpty(this.SecondInitialvalue))
                    return DBNull.Value.ToString();
                else
                    return this.SecondInitialvalue;
            }
            set
            {
                this.SecondInitialvalue = value;
            }
        }
        public bool ConfirmationChkBox
        {
            get
            {
                return this.ConfirmationChkBoxvalue;
            }
            set
            {
                this.ConfirmationChkBoxvalue = value;
            }
        }
        public string ReferenceNo
        {
            get
            {
                if (String.IsNullOrEmpty(this.ReferenceNoValue))
                    return DBNull.Value.ToString();
                else
                    return this.ReferenceNoValue;
            }
            set
            {
                this.ReferenceNoValue = value;
            }
        }
        public DataSet BonusSegments
        {
            get
            {
                return this.BonusSegmentsDS;
            }
            set
            {
                this.BonusSegmentsDS = value;
            }
        }
        public bool IsConsumerMatching
        {
            get
            {
                return this.IsConsumerMatchingvalue;
            }
            set
            {
                this.IsConsumerMatchingvalue = value;
            }
        }

        public string AssociationTypeCode
        {
            get
            {               
                    return this.strAssociationTypeCode;
            }
            set
            {
                this.strAssociationTypeCode = value;
            }
        }

        public bool IsCustomVetting
        {
            get
            {
                return this._IsCustomVetting;
            }
            set
            {
                this._IsCustomVetting = value;
            }
        }

        public bool SAFPSIndicator
        {
            get { return this.bSAFPSInd; }
            set { this.bSAFPSInd = value; }
        }

        public bool DisplayUnusableInformation
        {
            get { return this.bDisplayUnusableInformation; }
            set { this.bDisplayUnusableInformation = value; }
        }

        public int HomeAffairsID
        {
            get
            {
                return this.HomeAffairsIDValue;
            }
            set
            {
                this.HomeAffairsIDValue = value;
            }
        }

        public string ProcessedDate
        {
            get { return this.Processingdate; }
            set { this.Processingdate = value; }
        }

        public string EmploymentType
        {
            get { return this.strEmploymentType; }
            set { this.strEmploymentType = value; }
        }

        public string EmploymentSector
        {
            get { return this.strEmploymentSector; }
            set { this.strEmploymentSector = value; }
        }

        public string NoOfDependants
        {
            get { return this.strNoOfDependants; }
            set { this.strNoOfDependants = value; }
        }

        public DataTable IDnosList
        {
            get { return this.dtIDnos; }
            set { this.dtIDnos = value; }
        }

        public string Cellphonenumber
        {
            get { return this.strcellPhoneNo; }
            set { this.strcellPhoneNo = value; }
        }

        public bool AVSNeeded { get; set; }
        public bool AVSPassed { get; set; }
        public bool AVSSystemError { get; set; }
        public bool OtherAuthenticationNeeded { get; set; }
        public bool OtherAuthenticationPassed { get; set; }
        public bool OtherAuthenticationSystemError { get; set; }
        public string OtherauthenticationType { get; set; }
        public string CreditVettingDecision { get; set; }
        public string CreditVettingAuthenticationUser { get; set; }
        public bool DeviceMasked { get; set; }
        public string DeviceMacAddress { get; set; }
        public string GPSLocationCoordinates { get; set; }
        public string PublicIPAddress { get; set; }
        public bool BankAccountFound { get; set; }
        public bool BankAccountActive { get; set; }
        public bool BankAccountAcceptsDebits { get; set; }
        public bool BankAccountOlderThan3Months { get; set; }
        public bool BankAccountIDMatched { get; set; }
        public bool AVSNotPerformed { get; set; }
        public bool AVSCallNotSuccessful { get; set; }
        public bool AVSDate { get; set; }
        public string PressageScoreVersion { get { return this.strPressageScoreVersion; } set { this.strPressageScoreVersion = value; } }
    }
}
