﻿namespace XDSPortalLibrary.Entity_Layer
{
    public class Zrequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string IdNumber { get; set; }
        public string PassportNo { get; set; }
        public string FirstName { get; set; }
        public string SurName { get; set; }
        public string DateofBirth { get; set; }
        public string YourReference { get; set; }
        public string VoucherCode { get; set; }
    }

    public class ZRESULT
    {
        public string Status { get; set; }
        public string ErrorDescription { get; set; }
        public string XDSReference { get; set; }
        public string ClientReference { get; set; }
        public ConsumerDetails ConsumerInfo { get; set; }
        public ScoreInfo MtnPrevetScore { get; set; }
        public ScoreInfo VodacomPrevetScore { get; set; }
        public ScoreInfo TelkomPrevetScore { get; set; }

        public XDSPortalLibrary.Entity_Layer.OutputData FMPResponse { get; set; }

    }

    public class ConsumerDetails
    {
        public string IDNumber { get; set; }
        public string PassportNo { get; set; }
        public string BirthDate { get; set; }
        public string Title { get; set; }
        public string Surname { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Cell1 { get; set; }
        public string Cell2 { get; set; }
        public string Cell3 { get; set; }
        public string HomeTelephone1 { get; set; }
        public string HomeTelephone2 { get; set; }
        public string HomeTelephone3 { get; set; }
        public string WorkTelephone1 { get; set; }
        public string WorkTelephone2 { get; set; }
        public string WorkTelephone3 { get; set; }
    }

    public class ScoreInfo
    {
        public string CreditLimit { get; set; }
        public string Note { get; set; }
        public string Result { get; set; }
        public string Score { get; set; }
        public string Exclusionreason { get; set; }
    }
}
