﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDSPortalLibrary.Entity_Layer
{
    public class Yrequest
    {
        public string pUsername { get; set; }
        public string pPassword { get; set; }
        public string pIdNumber { get; set; }
        public string pPassportNo { get; set; }
        public string pFirstName { get; set; }
        public string pLastName { get; set; }
        public string pEnquiryPurpose { get; set; }
        public string pPDFIndicator { get; set; }
        public string pYourReference { get; set; }
        public string pVoucherCode { get; set; }
    }

    public class YRESULT
    {
        public string ID_NUMBER { get; set; }
        public string FORENAME1 { get; set; }
        public string FORENAME2 { get; set; }
        public string FORENAME3 { get; set; }
        public string SURNAME { get; set; }
        public string DEBTRESTRUCTURING { get; set; }
        public string JUDGEMENTS { get; set; }
        public string ADMINORDERS { get; set; }
        public string FRAUDINDICATOR { get; set; }
        public int NUMBEROFLOANS { get; set; }
        public float TOTALINSTALLMENT { get; set; }
        public float LOANBALANCE { get; set; }
        public string Status { get; set; }
        public string ErrorDescription { get; set; }
        public string XDSReference { get; set; }
        public string ClientReference { get; set; }
        public byte[] PDF { get; set; }
    }
}
