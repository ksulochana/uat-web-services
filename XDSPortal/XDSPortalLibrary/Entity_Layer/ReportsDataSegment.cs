﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XDSPortalLibrary.Entity_Layer
{
    class ReportsDataSegment
    {
        public ReportsDataSegment()
        {
        }

        private int reportDataSegmentIDField;

        private int reportIDField;

        private bool reportIDFieldSpecified;

        private int dataSegmentIDField;

        private bool dataSegmentIDFieldSpecified;

        private bool activeField;

        private string createdByUserField;

        private System.DateTime createdOnDateField;

        private bool createdOnDateFieldSpecified;

        private string changedByUserField;

        private System.DateTime changedOnDateField;

        private bool changedOnDateFieldSpecified;

        /// <remarks/>
        public int ReportDataSegmentID
        {
            get
            {
                return this.reportDataSegmentIDField;
            }
            set
            {
                this.reportDataSegmentIDField = value;
            }
        }

        /// <remarks/>
        public int ReportID
        {
            get
            {
                return this.reportIDField;
            }
            set
            {
                this.reportIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ReportIDSpecified
        {
            get
            {
                return this.reportIDFieldSpecified;
            }
            set
            {
                this.reportIDFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int DataSegmentID
        {
            get
            {
                return this.dataSegmentIDField;
            }
            set
            {
                this.dataSegmentIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DataSegmentIDSpecified
        {
            get
            {
                return this.dataSegmentIDFieldSpecified;
            }
            set
            {
                this.dataSegmentIDFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool Active
        {
            get
            {
                return this.activeField;
            }
            set
            {
                this.activeField = value;
            }
        }

        /// <remarks/>
        public string CreatedByUser
        {
            get
            {
                return this.createdByUserField;
            }
            set
            {
                this.createdByUserField = value;
            }
        }

        /// <remarks/>
        public System.DateTime CreatedOnDate
        {
            get
            {
                return this.createdOnDateField;
            }
            set
            {
                this.createdOnDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CreatedOnDateSpecified
        {
            get
            {
                return this.createdOnDateFieldSpecified;
            }
            set
            {
                this.createdOnDateFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string ChangedByUser
        {
            get
            {
                return this.changedByUserField;
            }
            set
            {
                this.changedByUserField = value;
            }
        }

        /// <remarks/>
        public System.DateTime ChangedOnDate
        {
            get
            {
                return this.changedOnDateField;
            }
            set
            {
                this.changedOnDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ChangedOnDateSpecified
        {
            get
            {
                return this.changedOnDateFieldSpecified;
            }
            set
            {
                this.changedOnDateFieldSpecified = value;
            }
        }
    }
}
