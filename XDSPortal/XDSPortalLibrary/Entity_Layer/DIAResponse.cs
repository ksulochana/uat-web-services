﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XDSPortalLibrary.Entity_Layer
{
    public class DIAResponse
    {
        public int Score { get; set; }                            //Matching Score
        public string Quality1 { get; set; }                      //Quality of Liveness Photo       
        public string Sharpness1 { get; set; }                    //Sharpness of Liveness Photo     
        public string Age1 { get; set; }                          //Estimate Age of Liveness Photo                           
        public string Quality2 { get; set; }                      //Quality of Home Affairs Portrait
        public string Sharpness2 { get; set; }                    //Sharpness of Home Affairs Portrait
        public string Age2 { get; set; }                          //Estimate Age of Home Affairs Portrait
        public string Gender1 { get; set; }                       //Gender of Liveness Photo
        public string Gender2 { get; set; }                       //Gender of Home Affairs Photo
        public long DiaReference { get; set; }                    //DIA Reference (Transaction Number)
        public string HanisReference { get; set; }                //Home Affairs Reference
        public string Name { get; set; }                          //Applicant Name
        public string Surname { get; set; }                       //Applicant Surname
        public string Status { get; set; }                        //Applicant Status
        public string DeceasedDate { get; set; }                  //Applicant Deceased date (If Applicable)
        public byte[] MatchedImgArray { get; set; }               //Liveness Image Submitted For match to Home Affairs Portrait.
        public byte[] CapturedImgArray { get; set; }               //Liveness Image Submitted For match to Home Affairs Portrait.
        public string ResponseMsg { get; set; }                   //DIA Response Message
        public int ResponseCode { get; set; }                     //DIA Response Code
        public string HAIDBookIssuedDate { get; set; }           //ID Book Issued Date (If Enabled)
        public string IdCardInd { get; set; }                     //ID Card Indicator (If Enabled)
        public string IdCardDate { get; set; }                    //ID Card Date (If enabled)
        public string UnigueOTLref { get; set; }
        public bool glasses { get; set; }                         //True – Wearing Glasses, False – Not Wearing Glasses
        public int faceMapType { get; set; }                      //Supplier Propriety for Queries 
        public int livenessStatus { get; set; }                   //Liveness Passed – 1, Liveness failed - 0
        public int auditTrailVerificationCheck { get; set; }     //Supplier Propriety for Queries
        public int retryReason { get; set; }                      //Liveness Retry Reason (Handled by Liveness Front End)
        public bool isLowQuality { get; set; }                    //Liveness Image Low Quality
        public int auditTrailMatchCheck { get; set; }             //Supplier Propriety for Queries
        public bool isReplayFaceMap { get; set; }                 //Resubmitted Face Map
        public int ageEstimateGroup { get; set; }                 //Age Estimation of Liveness
        public int matchLevel { get; set; }                       //Identification Doc Match level of liveness portrait and portrait on Doc
        public string StreetNumber { get; set; }                      //Quality of Liveness Photo       
        public string Route { get; set; }                    //Sharpness of Liveness Photo     
        public string Sublocality { get; set; }                          //Estimate Age of Liveness Photo                           
        public string Locality { get; set; }                      //Quality of Home Affairs Portrait
        public string Country { get; set; }                    //Sharpness of Home Affairs Portrait
        public string PostalCode { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }

    public class XDSDOVResponse
    {
        public int ResponseCode { get; set; }
        public string ResponseMsg { get; set; }
        public string Errors { get; set; }
    }
}
