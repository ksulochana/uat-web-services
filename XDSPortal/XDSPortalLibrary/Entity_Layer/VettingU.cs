﻿using System;

namespace XDSPortalLibrary.Entity_Layer
{
    public class VettingU
    {
        public VettingU() { }

        public class UResponse
        {
            private FMP _fmp;
            // private MPrevet _mprevet;
            private RPrevet _rprevet;
            private string _enquirystatus;
            private string _errorcode = "000";
            private string _errordescription;

            public FMP FmpResponse
            {
                get { return this._fmp; }
                set { this._fmp = value; }
            }

            //public MPrevet MPrevetResponse
            //{
            //    get { return this._mprevet; }
            //    set { this._mprevet = value; }
            //}

            public RPrevet RPrevetResponse
            {
                get { return this._rprevet; }
                set { this._rprevet = value; }
            }

            public string EnquiryStatus
            {
                get
                {
                    return this._enquirystatus;
                }
                set { this._enquirystatus = value; }
            }

            public string ErrorCode
            {
                get
                {
                    return this._errorcode;
                }
                set { this._errorcode = value; }
            }

            public string ErrorDescription
            {
                get
                {
                    return this._errordescription;
                }
                set { this._errordescription = value; }
            }
        }

        public class QResponse
        {
            private RPrevet _rprevet;
            private string _enquirystatus;
            private string _errorcode = "000";
            private string _errordescription;


            public RPrevet RPrevetResponse
            {
                get { return this._rprevet; }
                set { this._rprevet = value; }
            }

            public string EnquiryStatus
            {
                get
                {
                    return this._enquirystatus;
                }
                set { this._enquirystatus = value; }
            }

            public string ErrorCode
            {
                get
                {
                    return this._errorcode;
                }
                set { this._errorcode = value; }
            }

            public string ErrorDescription
            {
                get
                {
                    return this._errordescription;
                }
                set { this._errordescription = value; }
            }
        }

        public class FMP
        {
            private string idnumber;
            private string surname;
            private string emailaddress;
            private string cellnumber;
            private bool frd00001;
            private bool frd00002;
            private bool frd00003;
            private bool frd00004;
            private bool frd00005;
            private bool frd00013;
            private bool frd00015;
            private bool frd00017;
            private bool frd00019;
            private bool frd00021;
            private bool frd00022;
            private bool frd00023;
            private bool frd00024;
            private bool frd00026;
            private bool frd00028;
            private bool frd00030;
            private bool frd00032;
            private bool frd00033;
            private bool frd00035;
            private bool frd00041;
            private bool frd00042;
            private bool frd00043;
            private bool frd00044;
            private bool frd00045;
            private bool frd00046;
            private bool frd00050;
            private bool frd00061;
            private bool frd00064;
            private bool frd00065;
            private bool frd00069;
            private bool frd00072;
            private bool frd00074;
            private bool frd00075;
            private bool frd00076;
            private bool frd00077;
            private bool frd00078;
            private bool frd00079;
            private bool frd00080;
            private bool frd00081;
            private bool frd00082;
            private bool frd00083;
            private bool frd00084;
            private bool frd00085;
            private bool frd00086;
            private bool frd00087;
            private bool frd00088;
            private bool frd00089;
            private bool frd00090;
            private bool frd00091;

            public string IDNumber
            {
                get
                {
                    return this.idnumber;
                }
                set { this.idnumber = value; }
            }

            public string Surname
            {
                get
                {
                    return this.surname;
                }
                set { this.surname = value; }
            }

            public string EmailAddress
            {
                get
                {
                    return this.emailaddress;
                }
                set
                {
                    this.emailaddress = value;
                }
            }

            public string CellNumber
            {
                get
                {
                    return this.cellnumber;
                }
                set
                {
                    this.cellnumber = value;
                }
            }

            public bool FRD00001
            {
                get
                {
                    return this.frd00001;
                }
                set { this.frd00001 = value; }
            }

            public bool FRD00002
            {
                get
                {
                    return this.frd00002;
                }
                set { this.frd00002 = value; }
            }

            public bool FRD00003
            {
                get
                {
                    return this.frd00003;
                }
                set { this.frd00003 = value; }
            }

            public bool FRD00004
            {
                get
                {
                    return this.frd00004;
                }
                set { this.frd00004 = value; }
            }

            public bool FRD00005
            {
                get
                {
                    return this.frd00005;
                }
                set { this.frd00005 = value; }
            }

            public bool FRD00013
            {
                get
                {
                    return this.frd00013;
                }
                set { this.frd00013 = value; }
            }

            public bool FRD00015
            {
                get
                {
                    return this.frd00015;
                }
                set { this.frd00015 = value; }
            }

            public bool FRD00017
            {
                get
                {
                    return this.frd00017;
                }
                set { this.frd00017 = value; }
            }

            public bool FRD00019
            {
                get
                {
                    return this.frd00019;
                }
                set { this.frd00019 = value; }
            }

            public bool FRD00021
            {
                get
                {
                    return this.frd00021;
                }
                set { this.frd00021 = value; }
            }

            public bool FRD00022
            {
                get
                {
                    return this.frd00022;
                }
                set { this.frd00022 = value; }
            }

            public bool FRD00023
            {
                get
                {
                    return this.frd00023;
                }
                set { this.frd00023 = value; }
            }

            public bool FRD00024
            {
                get
                {
                    return this.frd00024;
                }
                set { this.frd00024 = value; }
            }

            public bool FRD00026
            {
                get
                {
                    return this.frd00026;
                }
                set { this.frd00026 = value; }
            }

            public bool FRD00028
            {
                get
                {
                    return this.frd00028;
                }
                set { this.frd00028 = value; }
            }

            public bool FRD00030
            {
                get
                {
                    return this.frd00030;
                }
                set { this.frd00030 = value; }
            }

            public bool FRD00032
            {
                get
                {
                    return this.frd00032;
                }
                set { this.frd00032 = value; }
            }

            public bool FRD00033
            {
                get
                {
                    return this.frd00033;
                }
                set { this.frd00033 = value; }
            }

            public bool FRD00035
            {
                get
                {
                    return this.frd00035;
                }
                set { this.frd00035 = value; }
            }

            public bool FRD00041
            {
                get
                {
                    return this.frd00041;
                }
                set { this.frd00041 = value; }
            }

            public bool FRD00042
            {
                get
                {
                    return this.frd00042;
                }
                set { this.frd00042 = value; }
            }

            public bool FRD00043
            {
                get
                {
                    return this.frd00043;
                }
                set { this.frd00043 = value; }
            }

            public bool FRD00044
            {
                get
                {
                    return this.frd00044;
                }
                set { this.frd00044 = value; }
            }

            public bool FRD00045
            {
                get
                {
                    return this.frd00045;
                }
                set { this.frd00045 = value; }
            }

            public bool FRD00046
            {
                get
                {
                    return this.frd00046;
                }
                set { this.frd00046 = value; }
            }

            public bool FRD00050
            {
                get
                {
                    return this.frd00050;
                }
                set { this.frd00050 = value; }
            }

            public bool FRD00061
            {
                get
                {
                    return this.frd00061;
                }
                set { this.frd00061 = value; }
            }

            public bool FRD00064
            {
                get
                {
                    return this.frd00064;
                }
                set { this.frd00064 = value; }
            }

            public bool FRD00065
            {
                get
                {
                    return this.frd00065;
                }
                set { this.frd00065 = value; }
            }

            public bool FRD00069
            {
                get
                {
                    return this.frd00069;
                }
                set { this.frd00069 = value; }
            }

            public bool FRD00072
            {
                get
                {
                    return this.frd00072;
                }
                set { this.frd00072 = value; }
            }

            public bool FRD00074
            {
                get
                {
                    return this.frd00074;
                }
                set { this.frd00074 = value; }
            }

            public bool FRD00075
            {
                get
                {
                    return this.frd00075;
                }
                set { this.frd00075 = value; }
            }

            public bool FRD00076
            {
                get
                {
                    return this.frd00076;
                }
                set { this.frd00076 = value; }
            }

            public bool FRD00077
            {
                get
                {
                    return this.frd00077;
                }
                set { this.frd00077 = value; }
            }

            public bool FRD00078
            {
                get
                {
                    return this.frd00078;
                }
                set { this.frd00078 = value; }
            }

            public bool FRD00079
            {
                get
                {
                    return this.frd00079;
                }
                set { this.frd00079 = value; }
            }

            public bool FRD00080
            {
                get
                {
                    return this.frd00080;
                }
                set { this.frd00080 = value; }
            }

            public bool FRD00081
            {
                get
                {
                    return this.frd00081;
                }
                set { this.frd00081 = value; }
            }

            public bool FRD00082
            {
                get
                {
                    return this.frd00082;
                }
                set { this.frd00082 = value; }
            }

            public bool FRD00083
            {
                get
                {
                    return this.frd00083;
                }
                set { this.frd00083 = value; }
            }

            public bool FRD00084
            {
                get
                {
                    return this.frd00084;
                }
                set { this.frd00084 = value; }
            }

            public bool FRD00085
            {
                get
                {
                    return this.frd00085;
                }
                set { this.frd00085 = value; }
            }

            public bool FRD00086
            {
                get
                {
                    return this.frd00086;
                }
                set { this.frd00086 = value; }
            }

            public bool FRD00087
            {
                get
                {
                    return this.frd00087;
                }
                set { this.frd00087 = value; }
            }

            public bool FRD00088
            {
                get
                {
                    return this.frd00088;
                }
                set { this.frd00088 = value; }
            }

            public bool FRD00089
            {
                get
                {
                    return this.frd00089;
                }
                set { this.frd00089 = value; }
            }

            public bool FRD00090
            {
                get
                {
                    return this.frd00090;
                }
                set { this.frd00090 = value; }
            }

            public bool FRD00091
            {
                get
                {
                    return this.frd00091;
                }
                set { this.frd00091 = value; }
            }



        }

        public class MPrevet
        {
            private string idnumber;
            private DateTime date;
            private string creditexposure;
            private double disposableincome;
            private string mtnexistingflag;
            private string exclusionreason;
            private bool filter1;
            private bool filter2;
            private bool filter3;
            private bool filter4;
            private bool filter5;
            private bool filter6;
            private bool filter7;
            private bool filter8;
            private bool filter9;
            private bool filter10;
            private string result;
            private string riskband;
            private double creditlimit;
            private string gender;
            private string title;
            private string surname;
            private string firstname;
            private string secondname;
            private string thirdname;
            private string maritalstats;
            private string emailaddress;
            private string cell1;
            private string cell2;
            private string cell3;
            private double predictedincome;
            private string employment;
            private DateTime employmentdate;
            private string residentialaddressline1;
            private string residentialaddressline2;
            private string residentialaddressline3;
            private string residentialaddressline4;
            private string residentialaddresspostalcode;

            public string IDNumber
            {
                get
                {
                    return this.idnumber;
                }
                set { this.idnumber = value; }
            }

            public DateTime Date
            {
                get
                {
                    return this.date;
                }
                set { this.date = value; }
            }

            public string CreditExposure
            {
                get
                {
                    return this.creditexposure;
                }
                set { this.creditexposure = value; }
            }

            public double DisposableIncome
            {
                get
                {
                    return this.disposableincome;
                }
                set { this.disposableincome = value; }
            }

            public string MTNExistingFlag
            {
                get
                {
                    return this.mtnexistingflag;
                }
                set { this.mtnexistingflag = value; }
            }

            public string ExclusionReason
            {
                get
                {
                    return this.exclusionreason;
                }
                set { this.exclusionreason = value; }
            }

            public bool Filter1
            {
                get
                {
                    return this.filter1;
                }
                set { this.filter1 = value; }
            }

            public bool Filter2
            {
                get
                {
                    return this.filter2;
                }
                set { this.filter2 = value; }
            }

            public bool Filter3
            {
                get
                {
                    return this.filter3;
                }
                set { this.filter3 = value; }
            }

            public bool Filter4
            {
                get
                {
                    return this.filter4;
                }
                set { this.filter4 = value; }
            }

            public bool Filter5
            {
                get
                {
                    return this.filter5;
                }
                set { this.filter5 = value; }
            }

            public bool Filter6
            {
                get
                {
                    return this.filter6;
                }
                set { this.filter6 = value; }
            }

            public bool Filter7
            {
                get
                {
                    return this.filter7;
                }
                set { this.filter7 = value; }
            }

            public bool Filter8
            {
                get
                {
                    return this.filter8;
                }
                set { this.filter8 = value; }
            }

            public bool Filter9
            {
                get
                {
                    return this.filter9;
                }
                set { this.filter9 = value; }
            }

            public bool Filter10
            {
                get
                {
                    return this.filter10;
                }
                set { this.filter10 = value; }
            }

            public string Result
            {
                get
                {
                    return this.result;
                }
                set { this.result = value; }
            }

            public string RiskBand
            {
                get
                {
                    return this.riskband;
                }
                set { this.riskband = value; }
            }

            public double CreditLimit
            {
                get
                {
                    return this.creditlimit;
                }
                set { this.creditlimit = value; }
            }

            public string Gender
            {
                get
                {
                    return this.gender;
                }
                set { this.gender = value; }
            }

            public string Title
            {
                get
                {
                    return this.title;
                }
                set { this.title = value; }
            }

            public string Surname
            {
                get
                {
                    return this.surname;
                }
                set { this.surname = value; }
            }

            public string FirstName
            {
                get
                {
                    return this.firstname;
                }
                set { this.firstname = value; }
            }

            public string SecondName
            {
                get
                {
                    return this.secondname;
                }
                set { this.secondname = value; }
            }

            public string ThirdName
            {
                get
                {
                    return this.thirdname;
                }
                set { this.thirdname = value; }
            }

            public string MaritalStats
            {
                get
                {
                    return this.maritalstats;
                }
                set { this.maritalstats = value; }
            }

            public string EmailAddress
            {
                get
                {
                    return this.emailaddress;
                }
                set { this.emailaddress = value; }
            }

            public string Cell1
            {
                get
                {
                    return this.cell1;
                }
                set { this.cell1 = value; }
            }

            public string Cell2
            {
                get
                {
                    return this.cell2;
                }
                set { this.cell2 = value; }
            }

            public string Cell3
            {
                get
                {
                    return this.cell3;
                }
                set { this.cell3 = value; }
            }

            public double PredictedIncome
            {
                get
                {
                    return this.predictedincome;
                }
                set { this.predictedincome = value; }
            }

            public string Employment
            {
                get
                {
                    return this.employment;
                }
                set { this.employment = value; }
            }

            public DateTime EmploymentDate
            {
                get
                {
                    return this.employmentdate;
                }
                set { this.employmentdate = value; }
            }

            public string ResidentialAddressLine1
            {
                get
                {
                    return this.residentialaddressline1;
                }
                set { this.residentialaddressline1 = value; }
            }

            public string ResidentialAddressLine2
            {
                get
                {
                    return this.residentialaddressline2;
                }
                set { this.residentialaddressline2 = value; }
            }

            public string ResidentialAddressLine3
            {
                get
                {
                    return this.residentialaddressline3;
                }
                set { this.residentialaddressline3 = value; }
            }

            public string ResidentialAddressLine4
            {
                get
                {
                    return this.residentialaddressline4;
                }
                set { this.residentialaddressline4 = value; }
            }

            public string ResidentialAddressPostalCode
            {
                get
                {
                    return this.residentialaddresspostalcode;
                }
                set { this.residentialaddresspostalcode = value; }
            }



        }

        public class RPrevet
        {
            private string idnumber;
            private string passportno;
            private DateTime? birthdate;
            private double creditlimit;
            private string result;
            private string title;
            private string surname;
            private string firstname;
            private string secondname;
            private string cell1;
            private string cell2;
            private string cell3;
            private string hometelephone1;
            private string hometelephone2;
            private string hometelephone3;
            private string worktelephone1;
            private string worktelephone2;
            private string worktelephone3;
            private int dealtype;
            private bool existingconsumer;
            private double pscore;
            private string preason;


            public string IDNumber
            {
                get
                {
                    return this.idnumber;
                }
                set { this.idnumber = value; }
            }

            public string Passportno
            {
                get
                {
                    return this.passportno;
                }
                set { this.passportno = value; }
            }

            public DateTime? BirthDate
            {
                get
                {
                    return this.birthdate;
                }
                set { this.birthdate = value; }
            }

            public double CreditLimit
            {
                get
                {
                    return this.creditlimit;
                }
                set { this.creditlimit = value; }
            }

            public string Result
            {
                get
                {
                    return this.result;
                }
                set { this.result = value; }
            }

            public string Title
            {
                get
                {
                    return this.title;
                }
                set { this.title = value; }
            }

            public string Surname
            {
                get
                {
                    return this.surname;
                }
                set { this.surname = value; }
            }

            public string FirstName
            {
                get
                {
                    return this.firstname;
                }
                set { this.firstname = value; }
            }

            public string SecondName
            {
                get
                {
                    return this.secondname;
                }
                set { this.secondname = value; }
            }

            public string Cell1
            {
                get
                {
                    return this.cell1;
                }
                set { this.cell1 = value; }
            }

            public string Cell2
            {
                get
                {
                    return this.cell2;
                }
                set { this.cell2 = value; }
            }

            public string Cell3
            {
                get
                {
                    return this.cell3;
                }
                set { this.cell3 = value; }
            }

            public string HomeTelephone1
            {
                get
                {
                    return this.hometelephone1;
                }
                set { this.hometelephone1 = value; }
            }

            public string HomeTelephone2
            {
                get
                {
                    return this.hometelephone2;
                }
                set { this.hometelephone2 = value; }
            }

            public string HomeTelephone3
            {
                get
                {
                    return this.hometelephone3;
                }
                set { this.hometelephone3 = value; }
            }

            public string WorkTelephone1
            {
                get
                {
                    return this.worktelephone1;
                }
                set { this.worktelephone1 = value; }
            }

            public string WorkTelephone2
            {
                get
                {
                    return this.worktelephone2;
                }
                set { this.worktelephone2 = value; }
            }

            public string WorkTelephone3
            {
                get
                {
                    return this.worktelephone3;
                }
                set { this.worktelephone3 = value; }
            }

            public int DealType
            {
                get
                {
                    return this.dealtype;
                }
                set { this.dealtype = value; }
            }
            public bool ExistingConsumer
            {
                get
                {
                    return this.existingconsumer;
                }
                set { this.existingconsumer = value; }
            }

            public double PScore
            {
                get
                {
                    return this.pscore;
                }
                set { this.pscore = value; }
            }

            public string PExclusionreason
            {
                get
                {
                    return this.preason;
                }
                set { this.preason = value; }
            }

        }
    }
}
