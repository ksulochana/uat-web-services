﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Security;
using XDSPortalAuthentication;

namespace XDSConnectJSON
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class XDSConnectWS : System.Web.Services.WebService
    {
        private SqlConnection EnquiryCon = new SqlConnection(ConfigurationManager.ConnectionStrings["XDSConnect.Properties.Settings.EnquiryConnectionString"].ToString());
        private SqlConnection AdminCon = new SqlConnection(ConfigurationManager.ConnectionStrings["XDSConnect.Properties.Settings.AdminConnectionString"].ToString());
        private SqlConnection AuthCon = new SqlConnection(ConfigurationManager.ConnectionStrings["XDSConnect.Properties.Settings.AuthConnectionstring"].ToString());
        private SqlConnection AuthEnquiryCon = new SqlConnection(ConfigurationManager.ConnectionStrings["XDSConnect.Properties.Settings.AuthEnquiryConnection"].ToString());
        private SqlConnection AVSCon = new SqlConnection(ConfigurationManager.ConnectionStrings["XDSConnect.Properties.Settings.AVSConnection"].ToString());
        private SqlConnection BCCOn = new SqlConnection(ConfigurationManager.ConnectionStrings["XDSConnect.Properties.Settings.BCConnection"].ToString());
        private SqlConnection AVSEnquiryCon = new SqlConnection(ConfigurationManager.ConnectionStrings["XDSConnect.Properties.Settings.AVSConnectionEnquiryDB"].ToString());
        private SqlConnection BCEnquiryCOn = new SqlConnection(ConfigurationManager.ConnectionStrings["XDSConnect.Properties.Settings.BCConnectionEnquiryDB"].ToString());
        private SqlConnection BIVEnquiryCOn = new SqlConnection(ConfigurationManager.ConnectionStrings["XDSConnect.Properties.Settings.BIVConnectionEnquiryDB"].ToString());
        private string strSmtpServerName = ConfigurationManager.AppSettings["XDSSmtp"].ToString();
        private string strSmtpUserLoginID = ConfigurationManager.AppSettings["XDSSmtpUserLoginID"].ToString();
        private string strSmtpUserPassword = ConfigurationManager.AppSettings["XDSSmtpUserPassword"].ToString();
        private int intport = Convert.ToInt16(ConfigurationManager.AppSettings["XDSSmtpPort"].ToString());
        private SqlConnection SMSCon = new SqlConnection(ConfigurationManager.ConnectionStrings["XDSConnect.Properties.Settings.SMSDBConnectionString"].ToString());
        private SqlConnection IdenticateCon = new SqlConnection(ConfigurationManager.ConnectionStrings["XDSConnect.Properties.Settings.IdenticateConnection"].ToString());
        private string CVetUserName = ConfigurationManager.AppSettings["CustomVettingLogUserName"].ToString();
        private string CVetPassword = ConfigurationManager.AppSettings["CustomVettingLogPassword"].ToString();
        private string CVetUrL = ConfigurationManager.AppSettings["CustomVettingLogURL"].ToString();
        private bool UseCustomVettingLog = bool.Parse(ConfigurationManager.AppSettings["UseCustomvettingLog"].ToString());
        private string sCredit4LifeDirectory = ConfigurationManager.AppSettings["Credit4LifeDirectory"].ToString();
        private string sCredit4LifeReportURL = ConfigurationManager.AppSettings["Credit4LifeReportURL"].ToString();

        public enum AuthenticationProcessAction
        {
            Authenticate = 1,
            Void = 2,
            ReferToFraud = 3
        }

        [WebMethod(Description = "Log in XDS Connect Web Service and returns a Ticket")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Login(string strUser, string strPwd)
        {
            LoginUser oLoginUser = AuthenticateUser(strUser, strPwd);

            if (oLoginUser.LoginStatus == LoginUser.LoginUserStatus.Authenticated)
            {

                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                   1,                            // version
                   oLoginUser.LoginSystemUser.SystemUserID.ToString(),                      // user name
                   DateTime.Now,                 // create time
                   DateTime.Now.AddHours(5),  // expire time
                   false,                        // persistent
                   oLoginUser.LoginSystemUser.SubscriberID.ToString());              // user data

                oLoginUser.LoginTicket = FormsAuthentication.Encrypt(ticket);
                //HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, oLoginUser.LoginTicket);
                //Context.Response.Cookies.Add(cookie);
                return oLoginUser.LoginTicket;
            }
            else
                return oLoginUser.LoginStatus.ToString();
        }

        [WebMethod(Description = "Checks Validity of your XDS Connect Ticket")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public bool IsTicketValid(string XDSConnectTicket)
        {
            try
            {
                FormsAuthenticationTicket a = FormsAuthentication.Decrypt(XDSConnectTicket);

                bool isValid = true;
                if (a.Expired) isValid = false;
                return isValid;
            }
            catch
            {
                return false;
            }
        }

        private LoginUser AuthenticateUser(string strUser, string strPwd)
        {
            LoginUser oLoginUser = new LoginUser();

            AdminCon.Open();

            if (Membership.ValidateUser(strUser, strPwd))
            {
                XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUser(AdminCon, strUser);

                if (oSystemUser != null)
                {
                    XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
                    XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(AdminCon, oSystemUser.SubscriberID);

                    oLoginUser.LoginStatus = LoginUser.LoginUserStatus.Authenticated;
                    oLoginUser.LoginSystemUser = oSystemUser;

                    if (!oSystemUser.ActiveYN) oLoginUser.LoginStatus = LoginUser.LoginUserStatus.UserDeactivated;
                    if (oSubscriber.StatusInd != "A") oLoginUser.LoginStatus = LoginUser.LoginUserStatus.SubscriberDeactivated;

                }
                else
                {
                    oLoginUser.LoginStatus = LoginUser.LoginUserStatus.UserNotFound;
                }

            }
            else
            {
                oLoginUser.LoginStatus = LoginUser.LoginUserStatus.NotAuthenticated;
            }
            AdminCon.Close();
            return oLoginUser;
            //throw new NotImplementedException();
        }

        public void LogOut()
        {
            // Deprive client of the authentication key
            FormsAuthentication.SignOut();
        }


        [WebMethod(Description = "Submit a Request to get the Subscriber Profile ")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string ConnectFraudGetProfile(string ConnectTicket)
        {
            string rXml = "";
            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();
            DataSet dsresult = new DataSet("Profile");

            if (IsTicketValid(ConnectTicket))
            {
                FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);

                try
                {
                    XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                    XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(AdminCon.ConnectionString, int.Parse(oTicket.Name));

                    XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
                    XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(AdminCon.ConnectionString, oSystemUser.SubscriberID);
                    XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

                    DataTable dBranches = new DataTable();
                    dBranches = odSubscriber.GetBranches(AuthCon.ConnectionString, oSystemUser.SubscriberID).Tables[0].Copy();
                    dBranches.TableName = "Branches";
                    DataTable dSubscriberProfile = new DataTable();
                    dSubscriberProfile = odSubscriber.GetSubscriberProfile(AuthCon.ConnectionString, oSystemUser.SubscriberID).Tables[0].Copy();
                    dSubscriberProfile.TableName = "SubscriberProfile";
                    DataTable dPurposes = new DataTable();
                    dPurposes = odSubscriber.GetPurposes(AuthCon.ConnectionString, oSystemUser.SubscriberID).Tables[0].Copy();
                    dPurposes.TableName = "Purposes";

                    dsresult.Tables.Add(dBranches);
                    dsresult.Tables.Add(dSubscriberProfile);
                    dsresult.Tables.Add(dPurposes);

                    rXml = dsresult.GetXml();
                    rsXml.LoadXml(rXml);
                }

                catch (Exception oException)
                {
                    rXml = "<NoResult><Error>" + oException.Message + "</Error></NoResult>";
                    rsXml.LoadXml(rXml);
                }
            }
            else
            {
                rXml = "<NoResult><Error>Invalid Ticket</Error></NoResult>";
                rsXml.LoadXml(rXml);
            }

            return Newtonsoft.Json.JsonConvert.SerializeXmlNode(rsXml);
        }

        [WebMethod(Description = "Process a Consumer match based on ID Number/Passport or Telephone No or Account No and returns matched Consumers")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string ConnectFraudConsumerMatch(string ConnectTicket, string SearchType, string BranchCode,
            int PurposeID, string IdNumber, string PassportNo, string CellularCode, string CellularNo, string AccountNo,
            string SubaccountNo, string YourReference, string VoucherCode)
        {
            string rXml = "";
            System.Xml.XmlDocument rsXml = new System.Xml.XmlDocument();

            AuthenticationSearchType oSearchType = (AuthenticationSearchType)Enum.Parse(typeof(AuthenticationSearchType), SearchType);

            if (IsTicketValid(ConnectTicket))
            {
                FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);

                try
                {
                    XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                    XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(AdminCon.ConnectionString, int.Parse(oTicket.Name));

                    XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
                    XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(AdminCon.ConnectionString, oSystemUser.SubscriberID);
                    XDSPortalLibrary.Entity_Layer.Response rp = new XDSPortalLibrary.Entity_Layer.Response();

                    XDSPortalAuthentication.AuthenticationConsumer dtConsumertraceMS = new XDSPortalAuthentication.AuthenticationConsumer();
                    //rp = dtConsumertraceMS.SubmitConsumerTrace(AuthEnquiryCon, AuthCon, oSubscriber.SubscriberID, oSystemUser.SystemUserID, 1, oSubscriber.SubscriberName, SearchType, BranchCode, PurposeID, IdNumber, PassportNo, CellularCode, CellularNo, AccountNo, SubaccountNo, OverrideOTP, OverrideOTPReason, YourReference, true, VoucherCode);
                    rp = dtConsumertraceMS.SubmitConsumerTrace(AuthEnquiryCon, AuthCon, oSubscriber.SubscriberID, oSystemUser.SystemUserID, 1, oSubscriber.SubscriberName, oSearchType, BranchCode, PurposeID, IdNumber, PassportNo, CellularCode, CellularNo, AccountNo, SubaccountNo, false, "", "", YourReference, true, VoucherCode);

                    switch (rp.ResponseStatus)
                    {
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Single:
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Multiple:
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Report:
                            rsXml.LoadXml(rp.ResponseData);
                            break;

                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.Error:
                            rXml = "<NoResult><Error>" + rp.ResponseData + "</Error></NoResult>";
                            rsXml.LoadXml(rXml);
                            break;
                        case XDSPortalLibrary.Entity_Layer.Response.ResponseStatusEnum.None:
                            rXml = "<NoResult><NotFound>" + "No Record Found" + "</NotFound></NoResult>";
                            rsXml.LoadXml(rXml);
                            break;
                    }

                }

                catch (Exception oException)
                {
                    rXml = "<NoResult><Error>" + oException.Message + "</Error></NoResult>";
                    rsXml.LoadXml(rXml);
                }
            }
            else
            {
                rXml = "<NoResult><Error>Invalid Ticket</Error></NoResult>";
                rsXml.LoadXml(rXml);
            }

            return Newtonsoft.Json.JsonConvert.SerializeXmlNode(rsXml);
        }

        [WebMethod(Description = "Submit a Request to get the authentication questions")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string ConnectFraudGetQuestions(string ConnectTicket, int EnquiryID, int EnquiryResultID)
        {
            AuthenticationProcess moMyAuthenticationManager = new AuthenticationProcess();

            try
            {

                if (IsTicketValid(ConnectTicket))
                {
                    FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);

                    try
                    {

                        XDSPortalEnquiry.Data.SystemUser odSystemUser = new XDSPortalEnquiry.Data.SystemUser();
                        XDSPortalEnquiry.Entity.SystemUser oSystemUser = odSystemUser.GetSystemUserRecord(AdminCon.ConnectionString, int.Parse(oTicket.Name));

                        XDSPortalEnquiry.Data.Subscriber odSubscriber = new XDSPortalEnquiry.Data.Subscriber();
                        XDSPortalEnquiry.Entity.Subscriber oSubscriber = odSubscriber.GetSubscriberRecord(AdminCon.ConnectionString, oSystemUser.SubscriberID);

                        XDSPortalEnquiry.Business.FraudEnquiry oFraudEnquiry = new XDSPortalEnquiry.Business.FraudEnquiry(AdminCon);

                        XDSPortalEnquiry.Data.SubscriberEnquiryResult dsec = new XDSPortalEnquiry.Data.SubscriberEnquiryResult();
                        XDSPortalEnquiry.Entity.SubscriberEnquiryResult sec = dsec.GetSubscriberEnquiryResultObject(AuthEnquiryCon, EnquiryResultID);


                        moMyAuthenticationManager = oFraudEnquiry.GetQuestions(EnquiryCon, AdminCon, SMSCon, sec.SubscriberEnquiryID, EnquiryResultID, strSmtpServerName, strSmtpUserLoginID, strSmtpUserPassword, intport);


                    }

                    catch (Exception oException)
                    {
                        moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.ErrorMessage = oException.Message;
                    }
                }
                else
                {
                    throw new Exception("Invalid ticket");
                }
            }
            catch (Exception ex)
            {
                moMyAuthenticationManager.CurrentObjectState.AuthenticationDocument.ErrorMessage = ex.Message;
            }

            return JsonConvert.SerializeObject(moMyAuthenticationManager);
        }

        [WebMethod(Description = "Submit a Request to Complete or Cancel an authentication")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string ConnectFraudProcess(string ConnectTicket, string ProcessAction, string MyAuthenticationProcess, string Comment)
        {

            XDSPortalEnquiry.Business.FraudEnquiry oFraudEnquiry = new XDSPortalEnquiry.Business.FraudEnquiry(AdminCon);

            AuthenticationProcessAction oProcesAction = (AuthenticationProcessAction)Enum.Parse(typeof(AuthenticationProcessAction), ProcessAction);

            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            AuthenticationProcess oMyAuthenticationProcess = (AuthenticationProcess)json_serializer.DeserializeObject(MyAuthenticationProcess);

            // AuthenticationProcess oMyAuthenticationProcess = MyAuthenticationProcess;


            try
            {

                if (IsTicketValid(ConnectTicket))
                {
                    FormsAuthenticationTicket oTicket = FormsAuthentication.Decrypt(ConnectTicket);

                    if (oProcesAction == AuthenticationProcessAction.Authenticate)
                    {
                        oMyAuthenticationProcess = oFraudEnquiry.AuthenticationProcess(EnquiryCon, AdminCon, SMSCon, oMyAuthenticationProcess, strSmtpServerName, strSmtpUserLoginID, strSmtpUserPassword, intport);
                    }
                    else if (oProcesAction == AuthenticationProcessAction.Void)
                    {
                        oMyAuthenticationProcess = oFraudEnquiry.AuthenticationProcessVoid(EnquiryCon, AdminCon, oMyAuthenticationProcess, Comment, strSmtpServerName, strSmtpUserLoginID, strSmtpUserPassword, intport);
                    }
                    else if (oProcesAction == AuthenticationProcessAction.ReferToFraud)
                    {
                        oMyAuthenticationProcess = oFraudEnquiry.AuthenticationProcessReferToFraud(EnquiryCon, AdminCon, oMyAuthenticationProcess, Comment, strSmtpServerName, strSmtpUserLoginID, strSmtpUserPassword, intport);
                    }

                }
                else
                {
                    throw new Exception("Invalid ticket");
                }
            }
            catch (Exception ex)
            {
                oMyAuthenticationProcess.CurrentObjectState.AuthenticationDocument.ErrorMessage = ex.Message;
            }


            return JsonConvert.SerializeObject(oMyAuthenticationProcess, Formatting.Indented);

        }

    }
}
