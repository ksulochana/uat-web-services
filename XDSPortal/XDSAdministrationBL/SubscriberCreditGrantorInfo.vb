Imports sembleWare.Runtime
Imports System
Public Class SubscriberCreditGrantorInfo 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly IDNo As New StringElement("IDNo", Me, False, True, True, True, False, 13, "ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PassportNo As New StringElement("PassportNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FirstName As New StringElement("FirstName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SecondName As New StringElement("SecondName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Surname As New StringElement("Surname", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly BirthDate As New DateTimeElement("BirthDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly GenderInd As New IndicatorElement("GenderInd", Me, False, False, True, True, True, False, "Gender", Nothing, SubscriberCreditGrantorInfo.GenderIndOptions, Nothing)
  Public ReadOnly ResidentialAddress1 As New StringElement("ResidentialAddress1", Me, False, True, True, True, False, 100, "Line 1", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResidentialAddress2 As New StringElement("ResidentialAddress2", Me, False, True, True, True, False, 100, "Line 2", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResidentialAddress3 As New StringElement("ResidentialAddress3", Me, False, True, True, True, False, 100, "Line 3", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResidentialAddress4 As New StringElement("ResidentialAddress4", Me, False, True, True, True, False, 100, "Line 4", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResidentialPostalCode As New StringElement("ResidentialPostalCode", Me, False, True, True, True, False, 10, "Postal Code", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly OccupantTypeInd As New IndicatorElement("OccupantTypeInd", Me, False, False, True, True, True, False, "Occupant Type", Nothing, SubscriberCreditGrantorInfo.OccupantTypeIndOptions, Nothing)
  Public ReadOnly PostalAddress1 As New StringElement("PostalAddress1", Me, False, True, True, True, False, 100, "Line 1", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PostalAddress2 As New StringElement("PostalAddress2", Me, False, True, True, True, False, 100, "Line 2", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PostalAddress3 As New StringElement("PostalAddress3", Me, False, True, True, True, False, 100, "Line 3", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PostalAddress4 As New StringElement("PostalAddress4", Me, False, True, True, True, False, 100, "Line 4", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PostalPostalCode As New StringElement("PostalPostalCode", Me, False, True, True, True, False, 10, "Postal Code", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly HomeTelephoneCode As New StringElement("HomeTelephoneCode", Me, False, True, True, True, False, 5, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly HomeTelephoneNo As New StringElement("HomeTelephoneNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly WorkTelephoneCode As New StringElement("WorkTelephoneCode", Me, False, True, True, True, False, 5, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly WorkTelephoneNo As New StringElement("WorkTelephoneNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CellularCode As New StringElement("CellularCode", Me, False, True, True, True, False, 5, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CellularNo As New StringElement("CellularNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly EmailAddress As New StringElement("EmailAddress", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly EmploymentTypeInd As New IndicatorElement("EmploymentTypeInd", Me, False, False, True, True, True, False, "Employment Type", Nothing, SubscriberCreditGrantorInfo.EmploymentTypeIndOptions, Nothing)
  Public ReadOnly EmployerDetail As New StringElement("EmployerDetail", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly OccupationTitle As New StringElement("OccupationTitle", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly EmployerAddress1 As New StringElement("EmployerAddress1", Me, False, True, True, True, False, 100, "Line 1", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly EmployerAddress2 As New StringElement("EmployerAddress2", Me, False, True, True, True, False, 100, "Line 2", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly EmployerAddress3 As New StringElement("EmployerAddress3", Me, False, True, True, True, False, 100, "Line 3", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly EmployerAddress4 As New StringElement("EmployerAddress4", Me, False, True, True, True, False, 100, "Line 4", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly EmployerPostalCode As New StringElement("EmployerPostalCode", Me, False, True, True, True, False, 10, "Postal Code", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly GrossMonthlySalaryAmt As New DecimalElement("GrossMonthlySalaryAmt", Me, False, True, True, True, False, "Gross Monthly Salary Amount", Nothing, "c", 0, Nothing, Nothing)
  Public ReadOnly CurrentEmployerTermYears As New IntegerElement("CurrentEmployerTermYears", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CurrentEmployerTermMonths As New IntegerElement("CurrentEmployerTermMonths", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SpouseFirstName As New StringElement("SpouseFirstName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SpouseSecondName As New StringElement("SpouseSecondName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SpouseSurname As New StringElement("SpouseSurname", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SpouseMaidenName As New StringElement("SpouseMaidenName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SpouseBirthDate As New DateTimeElement("SpouseBirthDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly SpouseEmployerDetail As New StringElement("SpouseEmployerDetail", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SpouseOccupationTitle As New StringElement("SpouseOccupationTitle", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SpouseNettMonthlyIncomeAmt As New DecimalElement("SpouseNettMonthlyIncomeAmt", Me, False, True, True, True, False, "Nett Monthly Income Amount", Nothing, "c", 0, Nothing, Nothing)
  Public ReadOnly SpouseCurrentEmployerTermYears As New IntegerElement("SpouseCurrentEmployerTermYears", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SpouseCurrentEmployerTermMonths As New IntegerElement("SpouseCurrentEmployerTermMonths", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SpouseWorkTelephoneCode As New StringElement("SpouseWorkTelephoneCode", Me, False, True, True, True, False, 5, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SpouseWorkTelephoneNo As New StringElement("SpouseWorkTelephoneNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SpouseCellularCode As New StringElement("SpouseCellularCode", Me, False, True, True, True, False, 5, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SpouseCellularNo As New StringElement("SpouseCellularNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly IncludeAffordabilityCalculationInd As New IndicatorElement("IncludeAffordabilityCalculationInd", Me, False, False, True, True, True, True, "Include Affordability Calculation?", Nothing, SubscriberCreditGrantorInfo.IncludeAffordabilityCalculationIndOptions, "N")
  Public ReadOnly NettMonthlyIncomeAmt As New DecimalElement("NettMonthlyIncomeAmt", Me, False, True, True, True, False, "Total Nett Monthly Income", Nothing, "c", 0, Nothing, Nothing)
  Public ReadOnly MonthlyExpenseAmt As New DecimalElement("MonthlyExpenseAmt", Me, False, True, True, True, False, "Total Existing Monthly Debt Expense (Before New Loan)", Nothing, "c", 0, Nothing, Nothing)
  Public ReadOnly NettMonthlyResultAmt As New DecimalElement("NettMonthlyResultAmt", Me, False, False, True, True, False, "Net Monthly Profit (Before New Loan)", Nothing, "c", 0, Nothing, Nothing)
  Public ReadOnly InterestRateStressPerc As New DecimalElement("InterestRateStressPerc", Me, False, True, True, True, False, "Interest Rate Stress %", Nothing, "##0.00", Nothing, Nothing, Nothing)
  Public ReadOnly CalculateNewLoanMonthlyRepaymentTypeInd As New IndicatorElement("CalculateNewLoanMonthlyRepaymentTypeInd", Me, False, False, True, True, True, False, "Calculate Monthly Repayment?", Nothing, SubscriberCreditGrantorInfo.CalculateNewLoanMonthlyRepaymentTypeIndOptions, Nothing)
  Public ReadOnly NewLoanMonthlyRepaymentAmt As New DecimalElement("NewLoanMonthlyRepaymentAmt", Me, False, True, True, True, False, "Amount To Be Repaid Per Month (Including Interest)", Nothing, "c", Nothing, Nothing, Nothing)
  Public ReadOnly NewLoanAmt As New DecimalElement("NewLoanAmt", Me, False, True, True, True, False, "New Loan Amount", Nothing, "c", Nothing, Nothing, Nothing)
  Public ReadOnly NewLoanMonthlyInterestPerc As New DecimalElement("NewLoanMonthlyInterestPerc", Me, False, True, True, True, False, "Monthly Interest %", Nothing, "##0.00", Nothing, Nothing, Nothing)
  Public ReadOnly NewLoanTerm As New IntegerElement("NewLoanTerm", Me, False, True, True, True, False, "Term (In Months)", Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AffordabilityLevelPerc As New DecimalElement("AffordabilityLevelPerc", Me, False, False, True, True, False, "Affordability Level %", Nothing, "##0.00", Nothing, Nothing, Nothing)
  Public ReadOnly OtherMonthlyIncomeAmt As New DecimalElement("OtherMonthlyIncomeAmt", Me, False, True, True, True, False, "Total Nett Monthly Income", Nothing, "c", 0, Nothing, Nothing)
  Public ReadOnly OtherMonthlyExpenseAmt As New DecimalElement("OtherMonthlyExpenseAmt", Me, False, True, True, True, False, "Total Nett Monthly Income", Nothing, "c", 0, Nothing, Nothing)
  Public ReadOnly Title As Relationship = New TitleRelationship("Title", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly MaritalStatus As Relationship = New MaritalStatusRelationship("MaritalStatus", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly PreferredLanguage As Relationship = New LanguageRelationship("PreferredLanguage", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly SubscriberCreditGrantor As Relationship = New SubscriberCreditGrantorRelationship("SubscriberCreditGrantor", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly SubscriberCreditGrantorInfoBankReference_OwnMany As Relationship = New SubscriberCreditGrantorInfoBankReferenceRelationship("SubscriberCreditGrantorInfoBankReference", Nothing, "SubscriberCreditGrantorAffordability", Me)
  Public ReadOnly SubscriberCreditGrantorInfoAccountReference_OwnMany As Relationship = New SubscriberCreditGrantorInfoAccountReferenceRelationship("SubscriberCreditGrantorInfoAccountReference", Nothing, "SubscriberCreditGrantorAffordability", Me)
  Public ReadOnly SubscriberCreditGrantorInfoAffordabilityExpense_OwnMany As Relationship = New SubscriberCreditGrantorInfoAffordabilityExpenseRelationship("SubscriberCreditGrantorInfoAffordabilityExpense", Nothing, "SubscriberCreditGrantorInfo", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _TitleCode As New StringElement("TitleCode", Me, False)
  Public ReadOnly _MaritalStatusCode As New StringElement("MaritalStatusCode", Me, False)
  Public ReadOnly _PreferredLanguageCode As New StringElement("PreferredLanguageCode", Me, False)
  Public ReadOnly _SubscriberCreditGrantorID As New IntegerElement("SubscriberCreditGrantorID", Me, True)
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _Title As TitleRelationship = Title
  Public ReadOnly _MaritalStatus As MaritalStatusRelationship = MaritalStatus
  Public ReadOnly _PreferredLanguage As LanguageRelationship = PreferredLanguage
  Public ReadOnly _SubscriberCreditGrantor As SubscriberCreditGrantorRelationship = SubscriberCreditGrantor
  Public ReadOnly _SubscriberCreditGrantorInfoBankReference_OwnMany As SubscriberCreditGrantorInfoBankReferenceRelationship = SubscriberCreditGrantorInfoBankReference_OwnMany
  Public ReadOnly _SubscriberCreditGrantorInfoAccountReference_OwnMany As SubscriberCreditGrantorInfoAccountReferenceRelationship = SubscriberCreditGrantorInfoAccountReference_OwnMany
  Public ReadOnly _SubscriberCreditGrantorInfoAffordabilityExpense_OwnMany As SubscriberCreditGrantorInfoAffordabilityExpenseRelationship = SubscriberCreditGrantorInfoAffordabilityExpense_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moGenderIndOptions As IndicatorOptions
  Public Shared ReadOnly Property GenderIndOptions() As IndicatorOptions
    Get
      If moGenderIndOptions Is Nothing Then
        moGenderIndOptions = New IndicatorOptions
        moGenderIndOptions.Add("M", "Male")
        moGenderIndOptions.Add("F", "Female")
      End If
      Return moGenderIndOptions
    End Get
  End Property
  Private Shared moOccupantTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property OccupantTypeIndOptions() As IndicatorOptions
    Get
      If moOccupantTypeIndOptions Is Nothing Then
        moOccupantTypeIndOptions = New IndicatorOptions
        moOccupantTypeIndOptions.Add("O", "Owner")
        moOccupantTypeIndOptions.Add("T", "Tenant")
      End If
      Return moOccupantTypeIndOptions
    End Get
  End Property
  Private Shared moEmploymentTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property EmploymentTypeIndOptions() As IndicatorOptions
    Get
      If moEmploymentTypeIndOptions Is Nothing Then
        moEmploymentTypeIndOptions = New IndicatorOptions
        moEmploymentTypeIndOptions.Add("P", "Permanent")
        moEmploymentTypeIndOptions.Add("C", "Contract")
        moEmploymentTypeIndOptions.Add("T", "Temporary")
        moEmploymentTypeIndOptions.Add("S", "Self-employed")
      End If
      Return moEmploymentTypeIndOptions
    End Get
  End Property
  Private Shared moIncludeAffordabilityCalculationIndOptions As IndicatorOptions
  Public Shared ReadOnly Property IncludeAffordabilityCalculationIndOptions() As IndicatorOptions
    Get
      If moIncludeAffordabilityCalculationIndOptions Is Nothing Then
        moIncludeAffordabilityCalculationIndOptions = New IndicatorOptions
        moIncludeAffordabilityCalculationIndOptions.Add("Y", "Yes")
        moIncludeAffordabilityCalculationIndOptions.Add("N", "No")
      End If
      Return moIncludeAffordabilityCalculationIndOptions
    End Get
  End Property
  Private Shared moCalculateNewLoanMonthlyRepaymentTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property CalculateNewLoanMonthlyRepaymentTypeIndOptions() As IndicatorOptions
    Get
      If moCalculateNewLoanMonthlyRepaymentTypeIndOptions Is Nothing Then
        moCalculateNewLoanMonthlyRepaymentTypeIndOptions = New IndicatorOptions
        moCalculateNewLoanMonthlyRepaymentTypeIndOptions.Add("Y", "Yes")
        moCalculateNewLoanMonthlyRepaymentTypeIndOptions.Add("N", "No")
      End If
      Return moCalculateNewLoanMonthlyRepaymentTypeIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberCreditGrantorInfo", ApplicationSettings)
    _Title._TitleCode.LocalElement = _TitleCode
    _MaritalStatus._MaritalStatusCode.LocalElement = _MaritalStatusCode
    _PreferredLanguage._LanguageCode.LocalElement = _PreferredLanguageCode
    _SubscriberCreditGrantor._SubscriberCreditGrantorID.LocalElement = _SubscriberCreditGrantorID
    _SubscriberCreditGrantor._SubscriberID.LocalElement = _SubscriberID
    AddHandler IncludeAffordabilityCalculationInd.Changed, AddressOf IncludeAffordabilityCalculationInd_Changed
    AddHandler CalculateNewLoanMonthlyRepaymentTypeInd.Changed, AddressOf CalculateNewLoanMonthlyRepaymentTypeInd_Changed
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub IncludeAffordabilityCalculationInd_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BRIncludeAffordabilityCalculationInd(Element, True, True)
  End Sub

  Private Sub CalculateNewLoanMonthlyRepaymentTypeInd_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BRCalculateNewLoanMonthlyRepaymentTypeInd(Element, True, True)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"

  Public Sub CalculateNettMonthlyResultAmt()
    BRCalculateNettMonthlyResultAmt()
  End Sub

  Public Sub CalculateNewLoanMonthlyRepayment()
    BRCalculateNewLoanMonthlyRepaymentAmt()
  End Sub

#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreate()
  End Sub

  Public Overrides Sub Save()
    With ApplicationSettings
      Try
        .BeginTransaction()
        Dim bIsNewYN As Boolean = Me.IsNew

        BRBeforeSave(bIsNewYN)
        MyBase.Save()
        .CommitTransaction()
      Catch Exception As Exception
        .RollbackTransaction()
        Throw Exception
      End Try
    End With
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreate()
    BRIncludeAffordabilityCalculationInd(Me.IncludeAffordabilityCalculationInd, True, False)
    BRCalculateNewLoanMonthlyRepaymentTypeInd(Me.CalculateNewLoanMonthlyRepaymentTypeInd, True, False)
  End Sub

  Private Sub BRBeforeSave(ByVal IsNewYN As Boolean)
    BRCalculateNettMonthlyResultAmt()
    BRCalculateNewLoanMonthlyRepaymentAmt()
    BRCalculateAffordabilityLevelPerc()
  End Sub

  Private Sub BRIncludeAffordabilityCalculationInd(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If SetEnableDisableYN Then
      Me.NettMonthlyIncomeAmt.Enabled = False
      Me.NettMonthlyIncomeAmt.Mandatory = False
      Me.MonthlyExpenseAmt.Enabled = False
      Me.MonthlyExpenseAmt.Mandatory = False
      Me.InterestRateStressPerc.Enabled = False
      Me.InterestRateStressPerc.Mandatory = False
      Me.CalculateNewLoanMonthlyRepaymentTypeInd.Enabled = False
      Me.CalculateNewLoanMonthlyRepaymentTypeInd.Mandatory = False
      Select Case Me.IncludeAffordabilityCalculationInd.Value
        Case Constants.YesNo.Yes
          Me.NettMonthlyIncomeAmt.Enabled = True
          Me.NettMonthlyIncomeAmt.Mandatory = True
          Me.MonthlyExpenseAmt.Enabled = True
          Me.MonthlyExpenseAmt.Mandatory = True
          Me.InterestRateStressPerc.Enabled = True
          Me.InterestRateStressPerc.Mandatory = True
          Me.CalculateNewLoanMonthlyRepaymentTypeInd.Enabled = True
          Me.CalculateNewLoanMonthlyRepaymentTypeInd.Mandatory = True
      End Select
    End If

    If PerformRulesYN Then
      Me.NettMonthlyIncomeAmt.SetToNullValue()
      Me.MonthlyExpenseAmt.SetToNullValue()
      Me.NettMonthlyResultAmt.SetToNullValue()
      Me.InterestRateStressPerc.SetToNullValue()
      Me.CalculateNewLoanMonthlyRepaymentTypeInd.SetToNullValue()
    End If
  End Sub

  Private Sub BRCalculateNewLoanMonthlyRepaymentTypeInd(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If SetEnableDisableYN Then
      Me.NewLoanAmt.Enabled = False
      Me.NewLoanAmt.Mandatory = False
      Me.NewLoanMonthlyInterestPerc.Enabled = False
      Me.NewLoanMonthlyInterestPerc.Mandatory = False
      Me.NewLoanMonthlyRepaymentAmt.Enabled = False
      Me.NewLoanMonthlyRepaymentAmt.Mandatory = False
      Me.NewLoanTerm.Enabled = False
      Me.NewLoanTerm.Mandatory = False
      Select Case Me.CalculateNewLoanMonthlyRepaymentTypeInd.Value
        Case Constants.YesNo.Yes
          Me.NewLoanAmt.Enabled = True
          Me.NewLoanAmt.Mandatory = True
          Me.NewLoanMonthlyInterestPerc.Enabled = True
          Me.NewLoanMonthlyInterestPerc.Mandatory = True
          Me.NewLoanTerm.Enabled = True
          Me.NewLoanTerm.Mandatory = True
        Case Constants.YesNo.No
          Me.NewLoanMonthlyRepaymentAmt.Enabled = True
          Me.NewLoanMonthlyRepaymentAmt.Mandatory = True
      End Select
    End If

    If PerformRulesYN Then
      Me.NewLoanAmt.SetToNullValue()
      Me.NewLoanMonthlyInterestPerc.SetToNullValue()
      Me.NewLoanMonthlyRepaymentAmt.SetToNullValue()
      Me.NewLoanTerm.SetToNullValue()
      Me.NewLoanMonthlyRepaymentAmt.SetToNullValue()
    End If
  End Sub

  Private Sub BRCalculateNettMonthlyResultAmt()
    Me.NettMonthlyResultAmt.Value = Me.NettMonthlyIncomeAmt.Value - Me.MonthlyExpenseAmt.Value
  End Sub

  Private Sub BRCalculateNewLoanMonthlyRepaymentAmt()
    Select Case Me.CalculateNewLoanMonthlyRepaymentTypeInd.Value
      Case Constants.YesNo.Yes
        Dim dInterestPerMonthPerc As Decimal = Me.NewLoanMonthlyInterestPerc.Value / 1200
        Dim dNewLoanAmt As Decimal = Me.NewLoanAmt.Value

        ' If Balloon calculation is required in the future!
        'If Me.BalloonAmt.Value > 0 Then
        '  dNewLoanAmt = Me.NewLoanAmt.Value - (Me.BalloonAmt.Value * (1 + dInterestPerMonthPerc) ^ (-Me.NewLoanTerm.Value))
        'End If

        If Me.NewLoanMonthlyInterestPerc.Value = 0 Then
          Me.NewLoanMonthlyRepaymentAmt.Value = dNewLoanAmt / Me.NewLoanTerm.Value
        Else
          Me.NewLoanMonthlyRepaymentAmt.Value = dNewLoanAmt * dInterestPerMonthPerc / (1 - (Math.Pow(1 / (1 + dInterestPerMonthPerc), Me.NewLoanTerm.Value)))
        End If
    End Select
  End Sub

  Private Sub BRCalculateAffordabilityLevelPerc()
    Dim dRatio1 As Decimal = 0
    Dim dRatio2 As Decimal = 0
    Dim dRatio3 As Decimal = 0
    Dim dTotalScore As Decimal = 0
    Dim dTotalDebtAmt As Decimal = Me.MonthlyExpenseAmt.Value + Me.NewLoanMonthlyRepaymentAmt.Value

    If Me.NettMonthlyIncomeAmt.Value > 0 Then
      dRatio1 = Me.NewLoanMonthlyRepaymentAmt.Value / Me.NettMonthlyIncomeAmt.Value
    End If
    If Me.NettMonthlyResultAmt.Value > 0 Then
      dRatio2 = dTotalDebtAmt / Me.NettMonthlyResultAmt.Value
      dRatio3 = (dTotalDebtAmt * (1 + Me.InterestRateStressPerc.Value / 100)) / Me.NettMonthlyResultAmt.Value
    End If
    dTotalScore = (0.25 * dRatio1) + (0.5 * dRatio2) + (0.25 * dRatio3)
    Me.AffordabilityLevelPerc.Value = dTotalScore * 10
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class SubscriberCreditGrantorInfoRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberCreditGrantorID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberCreditGrantorInfo(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberCreditGrantorID.ForeignElement = CType(Instance, SubscriberCreditGrantorInfo)._SubscriberCreditGrantorID
    _SubscriberID.ForeignElement = CType(Instance, SubscriberCreditGrantorInfo)._SubscriberID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
