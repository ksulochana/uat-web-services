Imports sembleWare.Runtime
Imports System
Public Class SubscriberAssociationConsumerCreditGrantorReport 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly DisplayPersonalSummaryYN As New BooleanElement("DisplayPersonalSummaryYN", Me, False, True, True, True, True, "Display Personal Summary?", Nothing, False, "Yes;No")
  Public ReadOnly DisplayFraudIndicatorsSummaryYN As New BooleanElement("DisplayFraudIndicatorsSummaryYN", Me, False, True, True, True, True, "Display Fraud Indicators Summary?", Nothing, False, "Yes;No")
  Public ReadOnly DisplayCreditAccountSummaryYN As New BooleanElement("DisplayCreditAccountSummaryYN", Me, False, True, True, True, True, "Display Credit Account Summary?", Nothing, False, "Yes;No")
  Public ReadOnly DisplayPropertyInformationSummaryYN As New BooleanElement("DisplayPropertyInformationSummaryYN", Me, False, True, True, True, True, "Display Property Information Summary?", Nothing, False, "Yes;No")
  Public ReadOnly DisplayDirectorshipSummaryYN As New BooleanElement("DisplayDirectorshipSummaryYN", Me, False, True, True, True, True, "Display Directorship Summary?", Nothing, False, "Yes;No")
  Public ReadOnly DisplayAccountStatusYN As New BooleanElement("DisplayAccountStatusYN", Me, False, True, True, True, True, "Display Account Status?", Nothing, False, "Yes;No")
  Public ReadOnly DisplayMonthlyPaymentDetailsYN As New BooleanElement("DisplayMonthlyPaymentDetailsYN", Me, False, True, True, True, True, "Display Monthly Payment Details?", Nothing, False, "Yes;No")
  Public ReadOnly DisplayAdverseInfoYN As New BooleanElement("DisplayAdverseInfoYN", Me, False, True, True, True, True, "Display Public Domain - Adverse?", Nothing, False, "Yes;No")
  Public ReadOnly DisplayJudgementsYN As New BooleanElement("DisplayJudgementsYN", Me, False, True, True, True, True, "Display Public Domain - Judgements?", Nothing, False, "Yes;No")
  Public ReadOnly DisplayAdministrationOrdersYN As New BooleanElement("DisplayAdministrationOrdersYN", Me, False, True, True, True, True, "Display Public Domain - Administration Orders?", Nothing, False, "Yes;No")
  Public ReadOnly DisplaySequestrationsYN As New BooleanElement("DisplaySequestrationsYN", Me, False, True, True, True, True, "Display Public Domain - Sequestrations?", Nothing, False, "Yes;No")
  Public ReadOnly DisplayCreditEnquiryHistoryYN As New BooleanElement("DisplayCreditEnquiryHistoryYN", Me, False, True, True, True, True, "Display Credit Enquiry History?", Nothing, False, "Yes;No")
  Public ReadOnly DisplayDebtReviewYN As New BooleanElement("DisplayDebtReviewYN", Me, False, True, True, True, True, "Display Debt Review?", Nothing, False, "Yes;No")
  Public ReadOnly DisplayFraudIndicatorsDetailYN As New BooleanElement("DisplayFraudIndicatorsDetailYN", Me, False, True, True, True, True, "Display Fraud Indicators Detail?", Nothing, False, "Yes;No")
  Public ReadOnly DisplayDirectorshipDetailYN As New BooleanElement("DisplayDirectorshipDetailYN", Me, False, True, True, True, True, "Display Directorship Detail?", Nothing, False, "Yes;No")
  Public ReadOnly DisplayPropertyInformationDetailYN As New BooleanElement("DisplayPropertyInformationDetailYN", Me, False, True, True, True, True, "Display Property Information Detail?", Nothing, False, "Yes;No")
  Public ReadOnly DisplayNameHistoryYN As New BooleanElement("DisplayNameHistoryYN", Me, False, True, True, True, True, "Display Name History?", Nothing, False, "Yes;No")
  Public ReadOnly DisplayAddressHistoryYN As New BooleanElement("DisplayAddressHistoryYN", Me, False, True, True, True, True, "Display Address History?", Nothing, False, "Yes;No")
  Public ReadOnly DisplayTelephoneHistoryYN As New BooleanElement("DisplayTelephoneHistoryYN", Me, False, True, True, True, True, "Display Telephone History?", Nothing, False, "Yes;No")
    Public ReadOnly DisplayEmploymentHistoryYN As New BooleanElement("DisplayEmploymentHistoryYN", Me, False, True, True, True, True, "Display Employment History?", Nothing, False, "Yes;No")
    Public ReadOnly DisplayScoringResultYN As New BooleanElement("DisplayScoringResultYN", Me, False, True, True, True, True, "Display Scoring Information?", Nothing, False, "Yes;No")

    Public ReadOnly DisplaySemiDefaultsYN As New BooleanElement("DisplaySemiDefaultsYN", Me, False, True, True, True, True, "Display Semi Defaults Information?", Nothing, False, "Yes;No")

  Public ReadOnly SubscriberAssociation As Relationship = New SubscriberAssociationRelationship("SubscriberAssociation", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberAssociationCode As New StringElement("SubscriberAssociationCode", Me, True)
  Public ReadOnly _SubscriberAssociation As SubscriberAssociationRelationship = SubscriberAssociation
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberAssociationConsumerCreditGrantorReport", ApplicationSettings)
    _SubscriberAssociation._SubscriberAssociationCode.LocalElement = _SubscriberAssociationCode
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Shared Procedures"
  Public Shared Function GetSubscriberAssociationConsumerCreditGrantorReport(ByVal SubscriberAssociationCode As String, ByVal ApplicationSettings As ApplicationSettings) As SubscriberAssociationConsumerCreditGrantorReport
    Dim oSubscriberAssociationConsumerCreditGrantorReport As SubscriberAssociationConsumerCreditGrantorReport = New SubscriberAssociationConsumerCreditGrantorReportRelationship(ApplicationSettings).NewInstance
    Try
      oSubscriberAssociationConsumerCreditGrantorReport._SubscriberAssociationCode.Load(SubscriberAssociationCode)
      oSubscriberAssociationConsumerCreditGrantorReport.Load()
    Catch oException As sembleWare.Runtime.RecordNotFoundException
      oSubscriberAssociationConsumerCreditGrantorReport = New SubscriberAssociationConsumerCreditGrantorReportRelationship(ApplicationSettings).NewInstance
      oSubscriberAssociationConsumerCreditGrantorReport._SubscriberAssociationCode.Load(SubscriberAssociationCode)
    Catch oException As Exception
      Throw oException
    End Try
    Return oSubscriberAssociationConsumerCreditGrantorReport
  End Function
#End Region
End Class 'sembleWare: Part

Public Class SubscriberAssociationConsumerCreditGrantorReportRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberAssociationCode As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberAssociationConsumerCreditGrantorReport(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberAssociationCode.ForeignElement = CType(Instance, SubscriberAssociationConsumerCreditGrantorReport)._SubscriberAssociationCode
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
