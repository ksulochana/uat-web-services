Imports sembleWare.Runtime
Imports System
Public Class RoleControlAccess 'sembleWare: Part
  Inherits CustomDBPart

#Region "Indicator Options"
  Public Const Access_None As Integer = 0
  Public Const Access_CanView As Integer = 1
  Public Const Access_CanChange As Integer = 2
#End Region

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly AccessInd As New IndicatorElement("AccessInd", Me, False, True, True, True, True, True, "Access", Nothing, RoleControlAccess.AccessIndOptions, Nothing)
  Public ReadOnly SecurityControl As Relationship = New SecurityControlRelationship("SecurityControl", Nothing, RelationshipType.OwnedBy, True, False, True, Me)
  Public ReadOnly Role As Relationship = New RoleRelationship("Role", Nothing, RelationshipType.OwnedBy, True, False, True, Me)
  Public ReadOnly RoleFormAccess As Relationship = New RoleFormAccessRelationship("RoleFormAccess", Nothing, RelationshipType.OwnedBy, True, False, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _ControlName As New StringElement("ControlName", Me, True)
  Public ReadOnly _FormName As New StringElement("FormName", Me, True)
  Public ReadOnly _AssemblyName As New StringElement("AssemblyName", Me, True)
  Public ReadOnly _RoleCode As New StringElement("RoleCode", Me, True)
  Public ReadOnly _SecurityControl As SecurityControlRelationship = SecurityControl
  Public ReadOnly _Role As RoleRelationship = Role
  Public ReadOnly _RoleFormAccess As RoleFormAccessRelationship = RoleFormAccess
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moAccessIndOptions As IndicatorOptions
  Public Shared ReadOnly Property AccessIndOptions() As IndicatorOptions
    Get
      If moAccessIndOptions Is Nothing Then
        moAccessIndOptions = New IndicatorOptions
        moAccessIndOptions.Add(0, "None")
        moAccessIndOptions.Add(1, "Can View")
        moAccessIndOptions.Add(2, "Can Change")
      End If
      Return moAccessIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("RoleControlAccess", ApplicationSettings)
    _SecurityControl._ControlName.LocalElement = _ControlName
    _SecurityControl._FormName.LocalElement = _FormName
    _SecurityControl._AssemblyName.LocalElement = _AssemblyName
    _Role._RoleCode.LocalElement = _RoleCode
    _RoleFormAccess._FormName.LocalElement = _FormName
    _RoleFormAccess._AssemblyName.LocalElement = _AssemblyName
    _RoleFormAccess._RoleCode.LocalElement = _RoleCode
    'sembleWare: Constructor End - Do Not Modify
    Me.ForceUpperCaseKeys = False
  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class RoleControlAccessRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _ControlName As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _FormName As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _AssemblyName As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _RoleCode As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New RoleControlAccess(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _ControlName.ForeignElement = CType(Instance, RoleControlAccess)._ControlName
    _FormName.ForeignElement = CType(Instance, RoleControlAccess)._FormName
    _AssemblyName.ForeignElement = CType(Instance, RoleControlAccess)._AssemblyName
    _RoleCode.ForeignElement = CType(Instance, RoleControlAccess)._RoleCode
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function CurrentUserAccessList() As sembleWare.Runtime.List
    Dim nSystemUserID = SystemUser.GetLoggedInSystemUser(ApplicationSettings).SystemUserID.Value
    'Custom List
    Dim oList As DBList = New DBList(Me, "A0RoleControlAccess")
    oList.Columns.Add(New ListColumn("ControlName", "[A0RoleControlAccess].[ControlName]", "ControlName", DataType.String, Nothing, True, 0, True, 100, "Control Name"))
    oList.Columns.Add(New ListColumn("AccessInd", "Max([A0RoleControlAccess].[AccessInd])", "AccessInd", DataType.Integer, Nothing, False, 0, True, 100, "Access"))
    oList.SelectStatement = "select"
    oList.FromClause = "[RoleControlAccess]  [A0RoleControlAccess]" + _
        "   join [SystemUserRole] [A1SystemUserRole] on [A0RoleControlAccess].[RoleCode] = [A1SystemUserRole].[RoleCode] and [A1SystemUserRole].[SystemUserID] = " + ApplicationSettings.QueryBuilder.ToSQL(nSystemUserID)
    oList.WhereClause = ""
    oList.GroupByClause = "ControlName"
    Return oList
  End Function








  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0RoleControlAccess")
    oList.Columns.Add(New ListColumn("ControlName", "[A0RoleControlAccess].[ControlName]", "ControlName", DataType.String, Nothing, True, 0, False, 100, "Control Name"))
    oList.Columns.Add(New ListColumn("FormName", "[A0RoleControlAccess].[FormName]", "FormName", DataType.String, Nothing, True, 0, False, 100, "Form Name"))
    oList.Columns.Add(New ListColumn("ControlName1", "[A1SecurityControl].[ControlName]", "ControlName", DataType.String, Nothing, False, 0, True, 200, "Control Name"))
    oList.Columns.Add(New ListColumn("ControlCaption", "[A1SecurityControl].[ControlCaption]", "ControlCaption", DataType.String, Nothing, False, 0, True, 200, "Control Caption"))
    oList.Columns.Add(New ListColumn("AccessInd", "[A0RoleControlAccess].[AccessInd]", "AccessInd", DataType.Integer, Nothing, False, 0, True, 100, "Access"))
    oList.Columns.Add(New ListColumn("AssemblyName", "[A0RoleControlAccess].[AssemblyName]", "AssemblyName", DataType.String, Nothing, True, 0, False, 100, "Assembly Name"))
    oList.Columns.Add(New ListColumn("RoleCode", "[A0RoleControlAccess].[RoleCode]", "RoleCode", DataType.String, Nothing, True, 0, True, 100, "Role Code"))
    oList.SelectStatement = "select"
    oList.FromClause = "([RoleControlAccess]  [A0RoleControlAccess]" + _
           "    join [SecurityControl]  [A1SecurityControl] on [A0RoleControlAccess].[ControlName] = [A1SecurityControl].[ControlName] and [A0RoleControlAccess].[FormName] = [A1SecurityControl].[FormName] and [A0RoleControlAccess].[AssemblyName] = [A1SecurityControl].[AssemblyName])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
