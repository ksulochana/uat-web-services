Imports sembleWare.Runtime
Imports System
Public Class DirectorName 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly DirectorNameID As New IdentityElement("DirectorNameID", Me, True, Nothing, Nothing)
  Public ReadOnly FirstInitial As New StringElement("FirstInitial", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SecondInitial As New StringElement("SecondInitial", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FirstName As New StringElement("FirstName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SecondName As New StringElement("SecondName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Surname As New StringElement("Surname", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SurnameParticular As New StringElement("SurnameParticular", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SurnamePrevious As New StringElement("SurnamePrevious", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly IDNo As New StringElement("IDNo", Me, False, True, True, True, False, 13, "ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly BirthDate As New DateTimeElement("BirthDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly RecordStatusInd As New IndicatorElement("RecordStatusInd", Me, False, False, False, True, True, True, "Record Status", Nothing, DirectorName.RecordStatusIndOptions, "A")
  Public ReadOnly DeletedReason As New StringElement("DeletedReason", Me, False, True, True, True, False, 500, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LastUpdatedDate As New DateTimeElement("LastUpdatedDate", Me, False, False, True, True, False, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly Loader As Relationship = New LoaderRelationship("Loader", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Director As Relationship = New DirectorRelationship("Director", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _LoaderID As New IntegerElement("LoaderID", Me, False)
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, False)
  Public ReadOnly _DirectorID As New IntegerElement("DirectorID", Me, True)
  Public ReadOnly _Loader As LoaderRelationship = Loader
  Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
  Public ReadOnly _Director As DirectorRelationship = Director
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moRecordStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property RecordStatusIndOptions() As IndicatorOptions
    Get
      If moRecordStatusIndOptions Is Nothing Then
        moRecordStatusIndOptions = New IndicatorOptions
        moRecordStatusIndOptions.Add("A", "Active")
        moRecordStatusIndOptions.Add("D", "Deleted")
      End If
      Return moRecordStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("DirectorName", ApplicationSettings)
    _Loader._LoaderID.LocalElement = _LoaderID
    _Subscriber._SubscriberID.LocalElement = _SubscriberID
    _Director._DirectorID.LocalElement = _DirectorID
    'sembleWare: Constructor End - Do Not Modify
    Me.DeleteType = CustomDBPart.enumDeleteType.ChangeRecordStatus
  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class DirectorNameRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _DirectorNameID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _DirectorID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New DirectorName(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _DirectorNameID.ForeignElement = CType(Instance, DirectorName).DirectorNameID
    _DirectorID.ForeignElement = CType(Instance, DirectorName)._DirectorID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0DirectorName")
    oList.Columns.Add(New ListColumn("DirectorID", "[A0DirectorName].[DirectorID]", "DirectorID", DataType.Integer, Nothing, True, 0, False, 100, "Director ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0DirectorName].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, False, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("LoaderID", "[A0DirectorName].[LoaderID]", "LoaderID", DataType.Integer, Nothing, False, 0, False, 100, "Loader ID"))
    oList.Columns.Add(New ListColumn("DirectorNameID", "[A0DirectorName].[DirectorNameID]", "DirectorNameID", DataType.Long, Nothing, True, 0, False, 100, "Director Name ID"))
    oList.Columns.Add(New ListColumn("LastUpdatedDate", "[A0DirectorName].[LastUpdatedDate]", "LastUpdatedDate", DataType.DateTime, "g", False, -1, True, 100, "Last Updated Date"))
    oList.Columns.Add(New ListColumn("IDNo", "[A0DirectorName].[IDNo]", "IDNo", DataType.String, Nothing, False, 2, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("FirstInitial", "[A0DirectorName].[FirstInitial]", "FirstInitial", DataType.String, Nothing, False, 0, True, 100, "First Initial"))
    oList.Columns.Add(New ListColumn("FirstName", "[A0DirectorName].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("Surname", "[A0DirectorName].[Surname]", "Surname", DataType.String, Nothing, False, 0, True, 100, "Surname"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A0DirectorName].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Birth Date"))
    oList.Columns.Add(New ListColumn("SubscriberID1", "[A1Subscriber].[SubscriberID]", "SubscriberID", DataType.Long, Nothing, False, 0, False, 75, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberName", "[A1Subscriber].[SubscriberName]", "SubscriberName", DataType.String, Nothing, False, 0, True, 150, "Subscriber Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "([DirectorName]  [A0DirectorName]" + _
           "    left join [Subscriber]  [A1Subscriber] on [A0DirectorName].[SubscriberID] = [A1Subscriber].[SubscriberID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
