Imports sembleWare.Runtime
Imports System

Public Class SystemUserRole 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly Role As Relationship = New RoleRelationship("Role", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly SystemUser As Relationship = New SystemUserRelationship("SystemUser", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _RoleCode As New StringElement("RoleCode", Me, True)
  Public ReadOnly _SystemUserID As New IntegerElement("SystemUserID", Me, True)
  Public ReadOnly _Role As RoleRelationship = Role
  Public ReadOnly _SystemUser As SystemUserRelationship = SystemUser
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SystemUserRole", ApplicationSettings)
    _Role._RoleCode.LocalElement = _RoleCode
    _SystemUser._SystemUserID.LocalElement = _SystemUserID
    AddHandler Role.Changed, AddressOf Role_Changed
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub Role_Changed(ByVal Relationship As Relationship)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Shared Procedures"
  Public Shared Function IsLoggedInSystemUserInRole(ByVal Role As Role, ByVal Applicationsettings As ApplicationSettings) As Boolean
    Dim oSystemUser As SystemUser = XDSAdministrationBL.SystemUser.GetLoggedInSystemUser(Applicationsettings)
    Return XDSAdministrationBL.SystemUserRole.IsSystemUserInRole(oSystemUser, Role, Applicationsettings)
  End Function

  Public Shared Function IsSystemUserInRole(ByVal SystemUser As SystemUser, ByVal Role As Role, ByVal Applicationsettings As ApplicationSettings) As Boolean
    Dim bIsInRoleYN As Boolean = False
    Dim oXDSAdministrationRoot As XDSAdministrationRoot = New XDSAdministrationRootRelationship(Applicationsettings).NewInstance()
    Dim oList As List = oXDSAdministrationRoot._SystemUserRole_OwnMany.List

    oList.LimitByRelatedPart(SystemUser, "SystemUser", True)
    oList.LimitByRelatedPart(Role, "Role", True)
    If oList.DataTable.Rows.Count > 0 Then
      bIsInRoleYN = True
    End If
    Return bIsInRoleYN
  End Function
#End Region

End Class 'sembleWare: Part

Public Class SystemUserRoleRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _RoleCode As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SystemUserID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SystemUserRole(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _RoleCode.ForeignElement = CType(Instance, SystemUserRole)._RoleCode
    _SystemUserID.ForeignElement = CType(Instance, SystemUserRole)._SystemUserID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
  Public Function DataAccessList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SystemUserRole")
    oList.Columns.Add(New ListColumn("SystemUserRoleDetailID", "[A0SystemUserRole].[SystemUserRoleDetailID]", "SystemUserRoleDetailID", DataType.Long, Nothing, True, 0, True, 100, "SystemUser Role Detail ID"))
    oList.Columns.Add(New ListColumn("RoleCode", "[A0SystemUserRole].[RoleCode]", "RoleCode", DataType.String, Nothing, True, 0, True, 100, "Role Code"))
    oList.Columns.Add(New ListColumn("AllowChangeYN", "[A0SystemUserRole].[AllowChangeYN]", "AllowChangeYN", DataType.Boolean, "Allow Change;View Only", False, 0, True, 100, "Allow Change YN"))
    oList.Columns.Add(New ListColumn("FieldValue1", "[A0SystemUserRole].[FieldValue1]", "FieldValue1", DataType.String, Nothing, False, 0, True, 100, "Field Value 1"))
    oList.Columns.Add(New ListColumn("FieldValue2", "[A0SystemUserRole].[FieldValue2]", "FieldValue2", DataType.String, Nothing, False, 0, True, 100, "Field Value 2"))
    oList.Columns.Add(New ListColumn("FieldValue3", "[A0SystemUserRole].[FieldValue3]", "FieldValue3", DataType.String, Nothing, False, 0, True, 100, "Field Value 3"))
    oList.Columns.Add(New ListColumn("FieldValue4", "[A0SystemUserRole].[FieldValue4]", "FieldValue4", DataType.String, Nothing, False, 0, True, 100, "Field Value 4"))
    oList.Columns.Add(New ListColumn("FieldValue5", "[A0SystemUserRole].[FieldValue5]", "FieldValue5", DataType.String, Nothing, False, 0, True, 100, "Field Value 5"))
    oList.Columns.Add(New ListColumn("FieldValue6", "[A0SystemUserRole].[FieldValue6]", "FieldValue6", DataType.String, Nothing, False, 0, True, 100, "Field Value 6"))
    oList.Columns.Add(New ListColumn("RoleCode1", "[A1ParentRole].[RoleCode]", "RoleCode", DataType.String, Nothing, False, 0, True, 100, "Code"))
    oList.Columns.Add(New ListColumn("RoleLevelCode", "[A2RoleLevel].[RoleLevelCode]", "RoleLevelCode", DataType.String, Nothing, False, 0, True, 100, "Role Level Code"))
    oList.Columns.Add(New ListColumn("ApplyLevelInd", "[A2RoleLevel].[ApplyLevelInd]", "ApplyLevelInd", DataType.String, Nothing, False, 0, True, 100, "Apply Level Ind"))
    oList.Columns.Add(New ListColumn("FieldCount", "[A2RoleLevel].[FieldCount]", "FieldCount", DataType.Integer, Nothing, False, 0, True, 100, "Field Count"))
    oList.Columns.Add(New ListColumn("FieldName1", "[A2RoleLevel].[FieldName1]", "FieldName1", DataType.String, Nothing, False, 0, True, 100, "Field Name 1"))
    oList.Columns.Add(New ListColumn("FieldName2", "[A2RoleLevel].[FieldName2]", "FieldName2", DataType.String, Nothing, False, 0, True, 100, "Field Name 2"))
    oList.Columns.Add(New ListColumn("FieldName3", "[A2RoleLevel].[FieldName3]", "FieldName3", DataType.String, Nothing, False, 0, True, 100, "Field Name 3"))
    oList.Columns.Add(New ListColumn("FieldName4", "[A2RoleLevel].[FieldName4]", "FieldName4", DataType.String, Nothing, False, 0, True, 100, "Field Name 4"))
    oList.Columns.Add(New ListColumn("FieldName5", "[A2RoleLevel].[FieldName5]", "FieldName5", DataType.String, Nothing, False, 0, True, 100, "Field Name 5"))
    oList.Columns.Add(New ListColumn("FieldName6", "[A2RoleLevel].[FieldName6]", "FieldName6", DataType.String, Nothing, False, 0, True, 100, "Field Name 6"))
    oList.Columns.Add(New ListColumn("SystemUserID", "[A0SystemUserRole].[SystemUserID]", "SystemUserID", DataType.Integer, Nothing, False, 0, False, 100, "SystemUser ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "([SystemUserRole]  [A0SystemUserRole]" + _
           "    join ([Role]  [A1ParentRole]" + _
           "    join [RoleLevel]  [A2RoleLevel] on [A1ParentRole].[RoleLevelCode] = [A2RoleLevel].[RoleLevelCode]) on [A0SystemUserRole].[RoleCode] = [A1ParentRole].[RoleCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function SystemUserRoleGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SystemUserRole")
    oList.Columns.Add(New ListColumn("SystemUserRoleDetailID", "[A0SystemUserRole].[SystemUserRoleDetailID]", "SystemUserRoleDetailID", DataType.Long, Nothing, True, 0, False, 120, "SystemUser Role Detail ID"))
    oList.Columns.Add(New ListColumn("RoleCode", "[A0SystemUserRole].[RoleCode]", "RoleCode", DataType.String, Nothing, True, 0, False, 100, "Role Code"))
    oList.Columns.Add(New ListColumn("RoleDescription", "[A1ParentRole].[Description]", "Description", DataType.String, Nothing, False, 0, True, 189, "Role"))
    oList.Columns.Add(New ListColumn("RoleCode1", "[A1ParentRole].[RoleCode]", "RoleCode", DataType.String, Nothing, False, 0, False, 100, "Role Code"))
    oList.Columns.Add(New ListColumn("FirstName", "[A2SystemUser].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 125, "First Name"))
    oList.Columns.Add(New ListColumn("Surname", "[A2SystemUser].[Surname]", "Surname", DataType.String, Nothing, False, 0, True, 127, "Surname"))
    oList.Columns.Add(New ListColumn("AllowChangeYN", "[A0SystemUserRole].[AllowChangeYN]", "AllowChangeYN", DataType.Boolean, "Allow Change;View Only", False, 0, True, 93, "Access"))
    oList.Columns.Add(New ListColumn("SystemUserID", "[A0SystemUserRole].[SystemUserID]", "SystemUserID", DataType.Integer, Nothing, False, 0, False, 100, "SystemUser ID"))
    oList.Columns.Add(New ListColumn("FieldValue1", "[A0SystemUserRole].[FieldValue1]", "FieldValue1", DataType.String, Nothing, False, 0, True, 77, "Field Value 1"))
    oList.Columns.Add(New ListColumn("FieldValue2", "[A0SystemUserRole].[FieldValue2]", "FieldValue2", DataType.String, Nothing, False, 0, True, 76, "Field Value 2"))
    oList.Columns.Add(New ListColumn("FieldValue3", "[A0SystemUserRole].[FieldValue3]", "FieldValue3", DataType.String, Nothing, False, 0, True, 75, "Field Value 3"))
    oList.Columns.Add(New ListColumn("FieldValue4", "[A0SystemUserRole].[FieldValue4]", "FieldValue4", DataType.String, Nothing, False, 0, True, 75, "Field Value 4"))
    oList.Columns.Add(New ListColumn("FieldValue5", "[A0SystemUserRole].[FieldValue5]", "FieldValue5", DataType.String, Nothing, False, 0, True, 76, "Field Value 5"))
    oList.Columns.Add(New ListColumn("FieldValue6", "[A0SystemUserRole].[FieldValue6]", "FieldValue6", DataType.String, Nothing, False, 0, True, 77, "Field Value 6"))
    oList.Columns.Add(New ListColumn("SystemUserID1", "[A2SystemUser].[SystemUserID]", "SystemUserID", DataType.Long, Nothing, False, 0, False, 100, "SystemUser ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "(([SystemUserRole]  [A0SystemUserRole]" + _
           "    join [Role]  [A1ParentRole] on [A0SystemUserRole].[RoleCode] = [A1ParentRole].[RoleCode])" + _
           "    join [SystemUser]  [A2SystemUser] on [A0SystemUserRole].[SystemUserID] = [A2SystemUser].[SystemUserID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

  Public Function RolesForSystemUserList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SystemUserRole")
    oList.Columns.Add(New ListColumn("SystemUserRoleDetailID", "[A0SystemUserRole].[SystemUserRoleDetailID]", "SystemUserRoleDetailID", DataType.Long, Nothing, True, 0, False, 100, "SystemUser Role Detail ID"))
    oList.Columns.Add(New ListColumn("RoleCode", "[A0SystemUserRole].[RoleCode]", "RoleCode", DataType.String, Nothing, True, 0, False, 100, "Role Code"))
    oList.Columns.Add(New ListColumn("RoleCode1", "[A1ParentRole].[RoleCode]", "RoleCode", DataType.String, Nothing, False, 0, True, 59, "Code"))
    oList.Columns.Add(New ListColumn("Description", "[A1ParentRole].[Description]", "Description", DataType.String, Nothing, False, 0, True, 157, "Role"))
    oList.Columns.Add(New ListColumn("RoleLevelDesc", "[A2RoleLevel].[Description]", "Description", DataType.String, Nothing, False, 0, True, 174, "Level"))
    oList.Columns.Add(New ListColumn("FieldName1", "[A2RoleLevel].[FieldName1]", "FieldName1", DataType.String, Nothing, False, 0, True, 87, "Field 1"))
    oList.Columns.Add(New ListColumn("FieldValue1", "[A0SystemUserRole].[FieldValue1]", "FieldValue1", DataType.String, Nothing, False, 0, True, 50, "Value"))
    oList.Columns.Add(New ListColumn("FieldName2", "[A2RoleLevel].[FieldName2]", "FieldName2", DataType.String, Nothing, False, 0, True, 83, "Field 2"))
    oList.Columns.Add(New ListColumn("FieldValue2", "[A0SystemUserRole].[FieldValue2]", "FieldValue2", DataType.String, Nothing, False, 0, True, 45, "Value"))
    oList.Columns.Add(New ListColumn("FieldName3", "[A2RoleLevel].[FieldName3]", "FieldName3", DataType.String, Nothing, False, 0, True, 100, "Field 3"))
    oList.Columns.Add(New ListColumn("FieldValue3", "[A0SystemUserRole].[FieldValue3]", "FieldValue3", DataType.String, Nothing, False, 0, True, 45, "Value"))
    oList.Columns.Add(New ListColumn("FieldName4", "[A2RoleLevel].[FieldName4]", "FieldName4", DataType.String, Nothing, False, 0, True, 100, "Field 4"))
    oList.Columns.Add(New ListColumn("FieldValue4", "[A0SystemUserRole].[FieldValue4]", "FieldValue4", DataType.String, Nothing, False, 0, True, 44, "Value"))
    oList.Columns.Add(New ListColumn("FieldName5", "[A2RoleLevel].[FieldName5]", "FieldName5", DataType.String, Nothing, False, 0, True, 100, "Field 5"))
    oList.Columns.Add(New ListColumn("FieldValue5", "[A0SystemUserRole].[FieldValue5]", "FieldValue5", DataType.String, Nothing, False, 0, True, 45, "Value"))
    oList.Columns.Add(New ListColumn("FieldName6", "[A2RoleLevel].[FieldName6]", "FieldName6", DataType.String, Nothing, False, 0, True, 100, "Field 6"))
    oList.Columns.Add(New ListColumn("FieldValue6", "[A0SystemUserRole].[FieldValue6]", "FieldValue6", DataType.String, Nothing, False, 0, True, 48, "Value"))
    oList.SelectStatement = "select"
    oList.FromClause = "([SystemUserRole]  [A0SystemUserRole]" + _
           "    join ([Role]  [A1ParentRole]" + _
           "    join [RoleLevel]  [A2RoleLevel] on [A1ParentRole].[RoleLevelCode] = [A2RoleLevel].[RoleLevelCode]) on [A0SystemUserRole].[RoleCode] = [A1ParentRole].[RoleCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

  Public Function SystemUserGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SystemUserRole")
    oList.Columns.Add(New ListColumn("RoleCode1", "[A1Role].[RoleCode]", "RoleCode", DataType.String, Nothing, False, 1, True, 100, "Role Code"))
    oList.Columns.Add(New ListColumn("RoleDescription", "[A1Role].[RoleDesc]", "RoleDesc", DataType.String, Nothing, False, 0, True, 200, "Role Description"))
    oList.Columns.Add(New ListColumn("SystemUserID", "[A0SystemUserRole].[SystemUserID]", "SystemUserID", DataType.Integer, Nothing, True, 0, False, 100, "System User ID"))
    oList.Columns.Add(New ListColumn("RoleCode", "[A0SystemUserRole].[RoleCode]", "RoleCode", DataType.String, Nothing, True, 0, False, 100, "Role Code"))
    oList.SelectStatement = "select"
    oList.FromClause = "([SystemUserRole]  [A0SystemUserRole]" + _
           "    join [Role]  [A1Role] on [A0SystemUserRole].[RoleCode] = [A1Role].[RoleCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

  Public Function RoleGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SystemUserRole")
    oList.Columns.Add(New ListColumn("SystemUserID", "[A0SystemUserRole].[SystemUserID]", "SystemUserID", DataType.Integer, Nothing, True, 0, False, 100, "System User ID"))
    oList.Columns.Add(New ListColumn("RoleCode", "[A0SystemUserRole].[RoleCode]", "RoleCode", DataType.String, Nothing, True, 0, False, 100, "Role Code"))
    oList.Columns.Add(New ListColumn("FullName", "isnull([A1SystemUser].[FirstName], '') + ' ' + isnull([A1SystemUser].[Surname], '')", "FullName", DataType.String, Nothing, False, 0, True, 200, "Full Name"))
    oList.Columns.Add(New ListColumn("EmailAddress", "[A1SystemUser].[EmailAddress]", "EmailAddress", DataType.String, Nothing, False, 0, False, 100, "Email Address"))
    oList.SelectStatement = "select"
    oList.FromClause = "([SystemUserRole]  [A0SystemUserRole]" + _
           "    join [SystemUser]  [A1SystemUser] on [A0SystemUserRole].[SystemUserID] = [A1SystemUser].[SystemUserID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship 