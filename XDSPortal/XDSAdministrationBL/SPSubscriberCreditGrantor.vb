Imports sembleWare.Runtime
Imports System
Public Class SPSubscriberCreditGrantor 'sembleWare: Part
  Inherits CustomMemoryPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly EnquiryDateFrom_SP As New DateTimeElement("EnquiryDateFrom_SP", Me, False, True, True, True, False, "Trace Date From", Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly EnquiryDateTo_SP As New DateTimeElement("EnquiryDateTo_SP", Me, False, True, True, True, False, "Trace Date To", Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly ReferenceNo_SP As New StringElement("ReferenceNo_SP", Me, False, True, True, True, False, 50, "Reference No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Product_SP As Relationship = New ProductRelationship("Product_SP", "Product", RelationshipType.Include, False, True, True, Me)
  Public ReadOnly ProductType_SP As Relationship = New ProductTypeRelationship("ProductType_SP", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly ActionedBySystemUser_SP As Relationship = New SystemUserRelationship("ActionedBySystemUser_SP", "Actioned By", RelationshipType.Include, False, True, True, Me)
  Public ReadOnly Subscriber_SP As Relationship = New SubscriberRelationship("Subscriber_SP", "Subscriber", RelationshipType.Include, False, True, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _ProductID As New IntegerElement("ProductID", Me, False)
  Public ReadOnly _ProductTypeID As New IntegerElement("ProductTypeID", Me, False)
  Public ReadOnly _ActionedBySystemUserID As New IntegerElement("ActionedBySystemUserID", Me, False)
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, False)
  Public ReadOnly _Product_SP As ProductRelationship = Product_SP
  Public ReadOnly _ProductType_SP As ProductTypeRelationship = ProductType_SP
  Public ReadOnly _ActionedBySystemUser_SP As SystemUserRelationship = ActionedBySystemUser_SP
  Public ReadOnly _Subscriber_SP As SubscriberRelationship = Subscriber_SP
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SPSubscriberCreditGrantor", ApplicationSettings)
    _Product_SP._ProductID.LocalElement = _ProductID
    _ProductType_SP._ProductTypeID.LocalElement = _ProductTypeID
    _ActionedBySystemUser_SP._SystemUserID.LocalElement = _ActionedBySystemUserID
    _Subscriber_SP._SubscriberID.LocalElement = _SubscriberID
    'sembleWare: Constructor End - Do Not Modify
    AddHandler ActionedBySystemUser_SP.LimitRelatedList, AddressOf ActionedBySystemUser_SP_LimitRelatedList
    AddHandler Product_SP.LimitRelatedList, AddressOf Product_SP_LimitRelatedList
  End Sub

#End Region 'sembleWare: Constructor

#Region " LimitRelatedList Events"
  Private Sub ActionedBySystemUser_SP_LimitRelatedList(ByVal List As List)
    List.LimitByRelatedPart(Me.Subscriber_SP.Instance, "Subscriber", True)
  End Sub

  Private Sub Product_SP_LimitRelatedList(ByVal List As List)
    List.LimitByRelatedPart(Me.ProductType_SP.Instance, "ProductType", True)
  End Sub
#End Region

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class SPSubscriberCreditGrantorRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SPSubscriberCreditGrantor(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
