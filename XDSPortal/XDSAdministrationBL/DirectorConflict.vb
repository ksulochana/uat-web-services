Imports sembleWare.Runtime
Imports System
Public Class DirectorConflict 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly Director As Relationship = New DirectorRelationship("Director", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly ConflictDirector As Relationship = New DirectorRelationship("ConflictDirector", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _DirectorID As New IntegerElement("DirectorID", Me, True)
  Public ReadOnly _ConflictDirectorID As New IntegerElement("ConflictDirectorID", Me, True)
  Public ReadOnly _Director As DirectorRelationship = Director
  Public ReadOnly _ConflictDirector As DirectorRelationship = ConflictDirector
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("DirectorConflict", ApplicationSettings)
    _Director._DirectorID.LocalElement = _DirectorID
    _ConflictDirector._DirectorID.LocalElement = _ConflictDirectorID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class DirectorConflictRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _DirectorID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _ConflictDirectorID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New DirectorConflict(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _DirectorID.ForeignElement = CType(Instance, DirectorConflict)._DirectorID
    _ConflictDirectorID.ForeignElement = CType(Instance, DirectorConflict)._ConflictDirectorID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0DirectorConflict")
    oList.Columns.Add(New ListColumn("DirectorID", "[A0DirectorConflict].[DirectorID]", "DirectorID", DataType.Integer, Nothing, True, 0, False, 100, "Director ID"))
    oList.Columns.Add(New ListColumn("ConflictDirectorID", "[A0DirectorConflict].[ConflictDirectorID]", "ConflictDirectorID", DataType.Integer, Nothing, True, 0, False, 100, "Conflict Director ID"))
    oList.Columns.Add(New ListColumn("DirectorID1", "[A1ConflictDirector].[DirectorID]", "DirectorID", DataType.Long, Nothing, False, 1, True, 100, "Director ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A1ConflictDirector].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("Surname", "[A1ConflictDirector].[Surname]", "Surname", DataType.String, Nothing, False, 0, True, 100, "Surname"))
    oList.Columns.Add(New ListColumn("FirstName", "[A1ConflictDirector].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("FirstInitial", "[A1ConflictDirector].[FirstInitial]", "FirstInitial", DataType.String, Nothing, False, 0, True, 100, "First Initial"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A1ConflictDirector].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Birth Date"))
    oList.SelectStatement = "select"
    oList.FromClause = "([DirectorConflict]  [A0DirectorConflict]" + _
           "    join [Director]  [A1ConflictDirector] on [A0DirectorConflict].[ConflictDirectorID] = [A1ConflictDirector].[DirectorID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
