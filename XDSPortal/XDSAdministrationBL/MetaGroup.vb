Imports sembleWare.Runtime
Imports System
Public Class MetaGroup 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly MetaGroupCode As New StringElement("MetaGroupCode", Me, True, True, True, True, True, 10, "Group Code", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly MetaGroupDesc As New StringElement("MetaGroupDesc", Me, False, True, True, True, True, 50, "Group Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LoaderDataUpdateModeInd As New IndicatorElement("LoaderDataUpdateModeInd", Me, False, False, True, True, True, True, "Update Mode To Use During Loader Data Update Process", Nothing, MetaGroup.LoaderDataUpdateModeIndOptions, Nothing)
  Public ReadOnly SequenceNum As New IntegerElement("SequenceNum", Me, False, False, True, True, True, "Sequence Number", Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly MetaGroupLabelDescription As New StringElement("MetaGroupLabelDescription", Me, False, True, True, False, False, 500, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly MetaTable As Relationship = New MetaTableRelationship("MetaTable", Nothing, RelationshipType.Include, True, True, True, Me)
  Public ReadOnly MetaDatabase As Relationship = New MetaDatabaseRelationship("MetaDatabase", "Database", RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly MetaGroupField_OwnMany As Relationship = New MetaGroupFieldRelationship("MetaGroupField", Nothing, "MetaGroup", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _MetaDatabaseCode As New StringElement("MetaDatabaseCode", Me, True)
  Public ReadOnly _MetaTableName As New StringElement("MetaTableName", Me, False)
  Public ReadOnly _MetaTable As MetaTableRelationship = MetaTable
  Public ReadOnly _MetaDatabase As MetaDatabaseRelationship = MetaDatabase
  Public ReadOnly _MetaGroupField_OwnMany As MetaGroupFieldRelationship = MetaGroupField_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moLoaderDataUpdateModeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property LoaderDataUpdateModeIndOptions() As IndicatorOptions
    Get
      If moLoaderDataUpdateModeIndOptions Is Nothing Then
        moLoaderDataUpdateModeIndOptions = New IndicatorOptions
        moLoaderDataUpdateModeIndOptions.Add("L", "Last Updated Date")
        moLoaderDataUpdateModeIndOptions.Add("A", "All Fields")
        moLoaderDataUpdateModeIndOptions.Add("N", "None")
      End If
      Return moLoaderDataUpdateModeIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("MetaGroup", ApplicationSettings)
    _MetaTable._MetaTableName.LocalElement = _MetaTableName
    _MetaTable._MetaDatabaseCode.LocalElement = _MetaDatabaseCode
    _MetaDatabase._MetaDatabaseCode.LocalElement = _MetaDatabaseCode
    'sembleWare: Constructor End - Do Not Modify
    AddHandler MetaTable.LimitRelatedList, AddressOf MetaTable_LimitRelatedList
  End Sub

#End Region 'sembleWare: Constructor

#Region " LimitRelatedList Events"
  Private Sub MetaTable_LimitRelatedList(ByVal List As List)
    List.LimitByRelatedPart(Me.MetaDatabase.Instance, "MetaDatabase", True)
  End Sub
#End Region

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"

  Public Sub CheckMetaDatabaseType()
    Dim oMetaDatabase As MetaDatabase = Me.MetaDatabase.Instance

    Select Case oMetaDatabase.MetaDatabaseTypeInd.Value
      Case Constants.MetaDatabase.Types.Repository, Constants.MetaDatabase.Types.Enquiry
        Throw New BusinessRuleException("You may not add Meta Groups as this Meta Database Type is not ""Credit Bureau""!")
    End Select
  End Sub

  Public Sub MoveUp()
    If Me.SequenceNum.Value = 0 Then
      Return
    End If
    Me.SequenceNum.Value = Me.SequenceNum.Value - 1
    Save()
  End Sub

  Public Sub MoveDown()
    Dim nSequenceNum = Utility.GetNextSequenceNumber(Me.TableName, Me.MetaDatabase.Instance, ApplicationSettings) - 1
    If Me.SequenceNum.Value = nSequenceNum Then
      Return
    End If
    Me.SequenceNum.Value = Me.SequenceNum.Value + 1
    Save()
  End Sub

#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreate()
  End Sub

  Public Overrides Sub Load()
    MyBase.Load()
    BRLoad()
  End Sub

  Public Overrides Sub Save()
    With ApplicationSettings
      .BeginTransaction()
      Try
        Dim bIsNewYN As Boolean = Me.IsNew
        BRBeforeSave(bIsNewYN)
        MyBase.Save()
        BRAfterSave()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
      .CommitTransaction()
    End With
  End Sub

  Public Overrides Sub Delete()
    With ApplicationSettings
      .BeginTransaction()
      Try
        BRBeforeDelete()
        MyBase.Delete()
        BRAfterDelete()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
      .CommitTransaction()
    End With
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreate()
    BRMetaGroupLabelDescription()
  End Sub

  Private Sub BRLoad()
    BRMetaGroupLabelDescription()
  End Sub

  Private Sub BRBeforeSave(ByVal IsNewYN As Boolean)
    If IsNewYN Then
      If Me.SequenceNum.IsEmpty Then
        Me.SequenceNum.Value = Utility.GetNextSequenceNumber(Me.TableName, Me.MetaDatabase.Instance, ApplicationSettings)
      End If
    End If
    Utility.UpdateSequenceNumber(Me, Me.MetaDatabase.Instance, False, ApplicationSettings)
  End Sub

  Private Sub BRAfterSave()
    BRMetaGroupLabelDescription()
  End Sub

  Private Sub BRBeforeDelete()
    Dim oMetaDatabase As MetaDatabase = Me.MetaDatabase.Instance

    Select Case oMetaDatabase.MetaDatabaseTypeInd.Value
      Case Constants.MetaDatabase.Types.Repository, Constants.MetaDatabase.Types.Enquiry
        Throw New BusinessRuleException("You cannot delete this Meta Group as this Meta Database Type is not of ""Credit Bureau""!")
    End Select
  End Sub

  Private Sub BRAfterDelete()
    Utility.UpdateSequenceNumber(Me, Me.MetaDatabase.Instance, True, ApplicationSettings)
  End Sub

  Private Sub BRMetaGroupLabelDescription()
    Dim oMetaDatabase As MetaDatabase = Me.MetaDatabase.Instance

    If Me.IsNew Then
      Me.MetaGroupLabelDescription.Value = oMetaDatabase.MetaDatabaseLabelDescription.Value & " - Meta Group: (New)"
    Else
      Dim sDesc As String = Trim(Me.MetaGroupDesc.Value)
      Me.MetaGroupLabelDescription.Value = oMetaDatabase.MetaDatabaseLabelDescription.Value & " - Meta Group: " & Me.MetaGroupCode.Value & " (" & sDesc & ")"
    End If
  End Sub
#End Region

End Class 'sembleWare: Part

Public Class MetaGroupRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _MetaDatabaseCode As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _MetaGroupCode As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New MetaGroup(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _MetaDatabaseCode.ForeignElement = CType(Instance, MetaGroup)._MetaDatabaseCode
    _MetaGroupCode.ForeignElement = CType(Instance, MetaGroup).MetaGroupCode
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0MetaGroup")
    oList.Columns.Add(New ListColumn("MetaDatabaseCode", "[A0MetaGroup].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, True, 0, False, 100, "Meta Database Code"))
    oList.Columns.Add(New ListColumn("MetaGroupCode", "[A0MetaGroup].[MetaGroupCode]", "MetaGroupCode", DataType.String, Nothing, True, 0, True, 100, "Group Code"))
    oList.Columns.Add(New ListColumn("MetaGroupDesc", "[A0MetaGroup].[MetaGroupDesc]", "MetaGroupDesc", DataType.String, Nothing, False, 0, True, 200, "Group Description"))
    oList.Columns.Add(New ListColumn("SequenceNum", "[A0MetaGroup].[SequenceNum]", "SequenceNum", DataType.Integer, Nothing, False, 1, False, 100, "Sequence Number"))
    oList.SelectStatement = "select"
    oList.FromClause = "[MetaGroup]  [A0MetaGroup]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0MetaGroup")
    oList.Columns.Add(New ListColumn("MetaDatabaseCode", "[A0MetaGroup].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, True, 0, False, 100, "Meta Database Code"))
    oList.Columns.Add(New ListColumn("MetaGroupCode", "[A0MetaGroup].[MetaGroupCode]", "MetaGroupCode", DataType.String, Nothing, True, 0, True, 100, "Group Code"))
    oList.Columns.Add(New ListColumn("MetaGroupDesc", "[A0MetaGroup].[MetaGroupDesc]", "MetaGroupDesc", DataType.String, Nothing, False, 0, True, 200, "Group Description"))
    oList.Columns.Add(New ListColumn("SequenceNum", "[A0MetaGroup].[SequenceNum]", "SequenceNum", DataType.Integer, Nothing, False, 1, True, 100, "Sequence Number"))
    oList.SelectStatement = "select"
    oList.FromClause = "[MetaGroup]  [A0MetaGroup]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function DropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0MetaGroup")
    oList.Columns.Add(New ListColumn("MetaDatabaseCode", "[A0MetaGroup].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, True, 0, False, 100, "Meta Database Code"))
    oList.Columns.Add(New ListColumn("MetaGroupCode", "[A0MetaGroup].[MetaGroupCode]", "MetaGroupCode", DataType.String, Nothing, True, 0, True, 100, "Group Code"))
    oList.Columns.Add(New ListColumn("MetaGroupDesc", "[A0MetaGroup].[MetaGroupDesc]", "MetaGroupDesc", DataType.String, Nothing, False, 0, True, 200, "Group Description"))
    oList.Columns.Add(New ListColumn("SequenceNum", "[A0MetaGroup].[SequenceNum]", "SequenceNum", DataType.Integer, Nothing, False, 1, False, 100, "Sequence Number"))
    oList.SelectStatement = "select"
    oList.FromClause = "[MetaGroup]  [A0MetaGroup]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
