Imports sembleWare.Runtime
Imports System
Public Class SPProductAuthenticationQuestion 'sembleWare: Part
  Inherits CustomMemoryPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly StatusInd_SP As New IndicatorElement("StatusInd_SP", Me, False, False, True, True, True, False, "Status", Nothing, SPProductAuthenticationQuestion.StatusInd_SPOptions, "All")
  Public ReadOnly QuestionTypeInd_SP As New IndicatorElement("QuestionTypeInd_SP", Me, False, False, True, True, True, False, "Question Type", Nothing, SPProductAuthenticationQuestion.QuestionTypeInd_SPOptions, "All")
  Public ReadOnly AnswerTypeInd_SP As New IndicatorElement("AnswerTypeInd_SP", Me, False, False, True, True, True, False, "Answer Type", Nothing, SPProductAuthenticationQuestion.AnswerTypeInd_SPOptions, "All")
  Public ReadOnly ProductAuthenticationQuestionGroup_SP As Relationship = New ProductAuthenticationQuestionGroupRelationship("ProductAuthenticationQuestionGroup_SP", "Authentication Question Group", RelationshipType.Include, False, True, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _ProductAuthenticationQuestionGroupCode As New StringElement("ProductAuthenticationQuestionGroupCode", Me, False)
  Public ReadOnly _ProductAuthenticationQuestionGroup_SP As ProductAuthenticationQuestionGroupRelationship = ProductAuthenticationQuestionGroup_SP
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moStatusInd_SPOptions As IndicatorOptions
  Public Shared ReadOnly Property StatusInd_SPOptions() As IndicatorOptions
    Get
      If moStatusInd_SPOptions Is Nothing Then
        moStatusInd_SPOptions = New IndicatorOptions
        moStatusInd_SPOptions.Add("A", "Active")
        moStatusInd_SPOptions.Add("I", "Inactive")
        moStatusInd_SPOptions.Add("All", "All")
      End If
      Return moStatusInd_SPOptions
    End Get
  End Property
  Private Shared moQuestionTypeInd_SPOptions As IndicatorOptions
  Public Shared ReadOnly Property QuestionTypeInd_SPOptions() As IndicatorOptions
    Get
      If moQuestionTypeInd_SPOptions Is Nothing Then
        moQuestionTypeInd_SPOptions = New IndicatorOptions
        moQuestionTypeInd_SPOptions.Add("P", "Positive")
        moQuestionTypeInd_SPOptions.Add("N", "Negative")
        moQuestionTypeInd_SPOptions.Add("All", "All")
      End If
      Return moQuestionTypeInd_SPOptions
    End Get
  End Property
  Private Shared moAnswerTypeInd_SPOptions As IndicatorOptions
  Public Shared ReadOnly Property AnswerTypeInd_SPOptions() As IndicatorOptions
    Get
      If moAnswerTypeInd_SPOptions Is Nothing Then
        moAnswerTypeInd_SPOptions = New IndicatorOptions
        moAnswerTypeInd_SPOptions.Add("R", "Range")
        moAnswerTypeInd_SPOptions.Add("Y", "Yes/No")
        moAnswerTypeInd_SPOptions.Add("All", "All")
      End If
      Return moAnswerTypeInd_SPOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SPProductAuthenticationQuestion", ApplicationSettings)
    _ProductAuthenticationQuestionGroup_SP._ProductAuthenticationQuestionGroupCode.LocalElement = _ProductAuthenticationQuestionGroupCode
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class SPProductAuthenticationQuestionRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SPProductAuthenticationQuestion(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
