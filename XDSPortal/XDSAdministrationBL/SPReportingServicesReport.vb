Imports sembleWare.Runtime
Imports System
Public Class SPReportingServicesReport 'sembleWare: Part
  Inherits CustomMemoryPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly ReportName_SP As New StringElement("ReportName_SP", Me, False, True, True, True, False, 100, "Report Name", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ReportingServicesServer_SP As Relationship = New ReportingServicesServerRelationship("ReportingServicesServer_SP", "Server", RelationshipType.Include, False, True, True, Me)
  Public ReadOnly ReportingServicesFolder_SP As Relationship = New ReportingServicesFolderRelationship("ReportingServicesFolder_SP", "Folder Path", RelationshipType.Include, False, True, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _ReportingServicesServerID As New IntegerElement("ReportingServicesServerID", Me, False)
  Public ReadOnly _ReportingServicesFolderPath As New StringElement("ReportingServicesFolderPath", Me, False)
  Public ReadOnly _ReportingServicesServer_SP As ReportingServicesServerRelationship = ReportingServicesServer_SP
  Public ReadOnly _ReportingServicesFolder_SP As ReportingServicesFolderRelationship = ReportingServicesFolder_SP
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SPReportingServicesReport", ApplicationSettings)
    _ReportingServicesServer_SP._ReportingServicesServerID.LocalElement = _ReportingServicesServerID
    _ReportingServicesFolder_SP._ReportingServicesFolderPath.LocalElement = _ReportingServicesFolderPath
    _ReportingServicesFolder_SP._ReportingServicesServerID.LocalElement = _ReportingServicesServerID
    'sembleWare: Constructor End - Do Not Modify
    AddHandler ReportingServicesFolder_SP.LimitRelatedList, AddressOf ReportingServicesFolder_SP_LimitRelatedList
  End Sub

#End Region 'sembleWare: Constructor

#Region " LimitRelatedList Events"
  Private Sub ReportingServicesFolder_SP_LimitRelatedList(ByVal List As List)
    List.LimitByRelatedPart(Me.ReportingServicesServer_SP.Instance, "ReportingServicesServer", True)
  End Sub
#End Region

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes
End Class 'sembleWare: Part

Public Class SPReportingServicesReportRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SPReportingServicesReport(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
