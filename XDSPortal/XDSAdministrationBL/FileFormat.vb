Imports sembleWare.Runtime
Imports System
Public Class FileFormat 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly FileFormatCode As New StringElement("FileFormatCode", Me, True, True, True, True, True, 10, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FileFormatDesc As New StringElement("FileFormatDesc", Me, False, True, True, True, True, 50, "File Format Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FileTypeInd As New IndicatorElement("FileTypeInd", Me, False, False, True, True, True, True, "File Type", Nothing, FileFormat.FileTypeIndOptions, Nothing)
  Public ReadOnly IsFirstRowColumnNameYN As New BooleanElement("IsFirstRowColumnNameYN", Me, False, True, True, True, False, "Is First Row Column Name?", Nothing, False, "Yes;No")
  Public ReadOnly NoOfRowsToSkip As New IntegerElement("NoOfRowsToSkip", Me, False, True, True, True, False, Nothing, Nothing, Nothing, 0, Nothing, Nothing)
  Public ReadOnly RowDelimiterInd As New IndicatorElement("RowDelimiterInd", Me, False, False, True, True, True, False, "Row Delimiter", Nothing, FileFormat.RowDelimiterIndOptions, "CRLF")
  Public ReadOnly FileFormatFieldTypeInd As New IndicatorElement("FileFormatFieldTypeInd", Me, False, False, True, True, True, False, "File Format Field Type", Nothing, FileFormat.FileFormatFieldTypeIndOptions, "F")
  Public ReadOnly DelimiterTypeInd As New IndicatorElement("DelimiterTypeInd", Me, False, False, True, True, True, False, "Delimiter Type", Nothing, FileFormat.DelimiterTypeIndOptions, Nothing)
  Public ReadOnly DelimitedCharacter As New StringElement("DelimitedCharacter", Me, False, True, True, True, False, 10, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FileFormatLabelDescription As New StringElement("FileFormatLabelDescription", Me, False, True, True, False, False, 250, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly TextQualiferInd As New IndicatorElement("TextQualiferInd", Me, False, False, True, True, True, False, "Text Qualifer", Nothing, FileFormat.TextQualiferIndOptions, Nothing)
  Public ReadOnly XMLNamespace As New StringElement("XMLNamespace", Me, False, True, True, True, False, 100, "XML Namespace", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly XMLElement As New StringElement("XMLElement", Me, False, True, True, True, False, 100, "XML Element", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FileFormatType As Relationship = New FileFormatTypeRelationship("FileFormatType", Nothing, RelationshipType.Include, True, False, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly FileFormatField_OwnMany As Relationship = New FileFormatFieldRelationship("FileFormatField", Nothing, "FileFormat", Me)
  Public ReadOnly FileFormatMetaDatabase_OwnMany As Relationship = New FileFormatMetaDatabaseRelationship("FileFormatMetaDatabase", Nothing, "FileFormat", Me)
  Public ReadOnly FileFormatMetaDataProcedure_OwnMany As Relationship = New FileFormatMetaDataProcedureRelationship("FileFormatMetaDataProcedure", Nothing, "FileFormat", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _FileFormatTypeCode As New StringElement("FileFormatTypeCode", Me, False)
  Public ReadOnly _FileFormatType As FileFormatTypeRelationship = FileFormatType
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  Public ReadOnly _FileFormatField_OwnMany As FileFormatFieldRelationship = FileFormatField_OwnMany
  Public ReadOnly _FileFormatMetaDatabase_OwnMany As FileFormatMetaDatabaseRelationship = FileFormatMetaDatabase_OwnMany
  Public ReadOnly _FileFormatMetaDataProcedure_OwnMany As FileFormatMetaDataProcedureRelationship = FileFormatMetaDataProcedure_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moFileTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property FileTypeIndOptions() As IndicatorOptions
    Get
      If moFileTypeIndOptions Is Nothing Then
        moFileTypeIndOptions = New IndicatorOptions
        moFileTypeIndOptions.Add("T", "Text File")
        moFileTypeIndOptions.Add("X", "XML File")
      End If
      Return moFileTypeIndOptions
    End Get
  End Property
  Private Shared moRowDelimiterIndOptions As IndicatorOptions
  Public Shared ReadOnly Property RowDelimiterIndOptions() As IndicatorOptions
    Get
      If moRowDelimiterIndOptions Is Nothing Then
        moRowDelimiterIndOptions = New IndicatorOptions
        moRowDelimiterIndOptions.Add("CRLF", "{CR}{LF}")
        moRowDelimiterIndOptions.Add("CR", "{CR}")
        moRowDelimiterIndOptions.Add("LF", "{LF}")
        moRowDelimiterIndOptions.Add("S", "Semicolon")
        moRowDelimiterIndOptions.Add("C", "Comma")
        moRowDelimiterIndOptions.Add("T", "Tab")
        moRowDelimiterIndOptions.Add("N", "None")
      End If
      Return moRowDelimiterIndOptions
    End Get
  End Property
  Private Shared moFileFormatFieldTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property FileFormatFieldTypeIndOptions() As IndicatorOptions
    Get
      If moFileFormatFieldTypeIndOptions Is Nothing Then
        moFileFormatFieldTypeIndOptions = New IndicatorOptions
        moFileFormatFieldTypeIndOptions.Add("F", "Fixed Field")
        moFileFormatFieldTypeIndOptions.Add("D", "Delimted")
      End If
      Return moFileFormatFieldTypeIndOptions
    End Get
  End Property
  Private Shared moDelimiterTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property DelimiterTypeIndOptions() As IndicatorOptions
    Get
      If moDelimiterTypeIndOptions Is Nothing Then
        moDelimiterTypeIndOptions = New IndicatorOptions
        moDelimiterTypeIndOptions.Add("C", "Comma")
        moDelimiterTypeIndOptions.Add("S", "Semicolon")
        moDelimiterTypeIndOptions.Add("T", "Tab")
        moDelimiterTypeIndOptions.Add("O", "Other")
      End If
      Return moDelimiterTypeIndOptions
    End Get
  End Property
  Private Shared moTextQualiferIndOptions As IndicatorOptions
  Public Shared ReadOnly Property TextQualiferIndOptions() As IndicatorOptions
    Get
      If moTextQualiferIndOptions Is Nothing Then
        moTextQualiferIndOptions = New IndicatorOptions
        moTextQualiferIndOptions.Add("D", "Double Quote")
        moTextQualiferIndOptions.Add("S", "Single Quote")
        moTextQualiferIndOptions.Add("N", "None")
      End If
      Return moTextQualiferIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("FileFormat", ApplicationSettings)
    _FileFormatType._FileFormatTypeCode.LocalElement = _FileFormatTypeCode
    AddHandler FileTypeInd.Changed, AddressOf FileTypeInd_Changed
    AddHandler FileFormatFieldTypeInd.Changed, AddressOf FileFormatFieldTypeInd_Changed
    AddHandler DelimiterTypeInd.Changed, AddressOf DelimiterTypeInd_Changed
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub FileTypeInd_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BRFileTypeInd(Element, True, True)
  End Sub

  Private Sub FileFormatFieldTypeInd_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BRFileFormatFieldTypeInd(Element, True, True)
  End Sub

  Private Sub DelimiterTypeInd_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BRDelimiterTypeInd(Element, True, True)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"

#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreate()
  End Sub

  Public Overrides Sub Load()
    MyBase.Load()
    BRLoad()
  End Sub

  Public Overrides Sub Save()
    With ApplicationSettings
      .BeginTransaction()
      Try
        Dim bIsNewYN As Boolean = Me.IsNew
        MyBase.Save()
        BRAfterSave(bIsNewYN)
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
      .CommitTransaction()
    End With
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreate()
    Me.FileFormatType.Enabled = True
    BRFileTypeInd(Me.FileTypeInd, True, False)
    BRFileFormatFieldTypeInd(Me.FileFormatFieldTypeInd, True, False)
    BRDelimiterTypeInd(Me.DelimiterTypeInd, True, False)
    BRFileFormatLabelDescription()
  End Sub

  Private Sub BRLoad()
    BRFileTypeInd(Me.FileTypeInd, True, False)
    BRFileFormatFieldTypeInd(Me.FileFormatFieldTypeInd, True, False)
    BRDelimiterTypeInd(Me.DelimiterTypeInd, True, False)
    BRFileFormatLabelDescription()
  End Sub

  Private Sub BRAfterSave(ByVal IsNew As Boolean)
    If IsNew Then
      Dim oFileFormatField As FileFormatField = Me.FileFormatField_OwnMany.NewInstance()
      oFileFormatField.FileFormatFieldTypeInd.Value = Constants.FileFormatField.Types.System
      oFileFormatField.FileFormatFieldDesc.Value = "Subscriber ID"
      oFileFormatField.Save()
    End If
    BRFileFormatLabelDescription()
  End Sub

  Private Sub BRFileTypeInd(ByVal Element As Element, ByVal SetEnableMandatory As Boolean, ByVal PerformRules As Boolean)
    If SetEnableMandatory Then
      Me.IsFirstRowColumnNameYN.Enabled = False
      Me.IsFirstRowColumnNameYN.Mandatory = False
      Me.NoOfRowsToSkip.Enabled = False
      Me.NoOfRowsToSkip.Mandatory = False
      Me.RowDelimiterInd.Enabled = False
      Me.RowDelimiterInd.Mandatory = False
      Me.FileFormatFieldTypeInd.Enabled = False
      Me.FileFormatFieldTypeInd.Mandatory = False
      Me.DelimiterTypeInd.Enabled = False
      Me.DelimiterTypeInd.Mandatory = False
      Me.TextQualiferInd.Enabled = False
      Me.TextQualiferInd.Mandatory = False
      Me.DelimitedCharacter.Enabled = False
      Me.DelimitedCharacter.Mandatory = False
      Me.XMLNamespace.Enabled = False
      Me.XMLNamespace.Mandatory = False
      Me.XMLElement.Enabled = False
      Me.XMLElement.Mandatory = False
      Select Case Element.ValueAsObject
        Case Constants.FileFormat.FileTypes.TextFile
          Me.IsFirstRowColumnNameYN.Enabled = True
          Me.IsFirstRowColumnNameYN.Mandatory = True
          Me.NoOfRowsToSkip.Enabled = True
          Me.NoOfRowsToSkip.Mandatory = True
          Me.RowDelimiterInd.Enabled = True
          Me.RowDelimiterInd.Mandatory = True
          Me.FileFormatFieldTypeInd.Enabled = True
          Me.FileFormatFieldTypeInd.Mandatory = True
        Case Constants.FileFormat.FileTypes.XMLFile
          Me.XMLNamespace.Enabled = True
          Me.XMLNamespace.Mandatory = True
          Me.XMLElement.Enabled = True
          Me.XMLElement.Mandatory = True
      End Select
    End If

    If PerformRules Then
      Me.IsFirstRowColumnNameYN.SetToNullValue()
      Me.NoOfRowsToSkip.SetToNullValue()
      Me.RowDelimiterInd.SetToNullValue()
      Me.FileFormatFieldTypeInd.SetToNullValue()
      Me.DelimiterTypeInd.SetToNullValue()
      Me.TextQualiferInd.SetToNullValue()
      Me.DelimitedCharacter.SetToNullValue()
      Me.XMLNamespace.SetToNullValue()
      Me.XMLElement.SetToNullValue()
    End If
  End Sub

  Private Sub BRFileFormatFieldTypeInd(ByVal Element As Element, ByVal SetEnableMandatory As Boolean, ByVal PerformRules As Boolean)
    If SetEnableMandatory Then
      Me.DelimiterTypeInd.Enabled = False
      Me.DelimiterTypeInd.Mandatory = False
      Me.TextQualiferInd.Enabled = False
      Me.TextQualiferInd.Mandatory = False
      Select Case Element.ValueAsObject
        Case Constants.FileFormat.FieldTypes.Delimted
          Me.DelimiterTypeInd.Enabled = True
          Me.DelimiterTypeInd.Mandatory = True
          Me.TextQualiferInd.Enabled = True
          Me.TextQualiferInd.Mandatory = True
      End Select
    End If

    If PerformRules Then
      Me.DelimiterTypeInd.SetToNullValue()
      Me.TextQualiferInd.SetToNullValue()
    End If
  End Sub

  Private Sub BRDelimiterTypeInd(ByVal Element As Element, ByVal SetEnableMandatory As Boolean, ByVal PerformRules As Boolean)
    If SetEnableMandatory Then
      Me.DelimitedCharacter.Enabled = False
      Me.DelimitedCharacter.Mandatory = False

      Select Case Element.ValueAsObject
        Case Constants.FileFormat.DelimiterTypes.Other
          Me.DelimitedCharacter.Enabled = True
          Me.DelimitedCharacter.Mandatory = True
      End Select
    End If

    If PerformRules Then
      Me.DelimitedCharacter.SetToNullValue()
    End If
  End Sub

  Private Sub BRFileFormatLabelDescription()
    If Me.IsNew Then
      Me.FileFormatLabelDescription.Value = "File Format: (New)"
    Else
      Dim sDesc As String = Trim(Me.FileFormatDesc.Value)
      Me.FileFormatLabelDescription.Value = "File Format: " & Me.FileFormatCode.Value & " (" & sDesc & ")"
    End If
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class FileFormatRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _FileFormatCode As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New FileFormat(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _FileFormatCode.ForeignElement = CType(Instance, FileFormat).FileFormatCode
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0FileFormat")
    oList.Columns.Add(New ListColumn("FileFormatCode", "[A0FileFormat].[FileFormatCode]", "FileFormatCode", DataType.String, Nothing, True, 1, True, 100, "File Format Code"))
    oList.Columns.Add(New ListColumn("FileFormatDesc", "[A0FileFormat].[FileFormatDesc]", "FileFormatDesc", DataType.String, Nothing, False, 0, True, 200, "File Format Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "[FileFormat]  [A0FileFormat]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0FileFormat")
    oList.Columns.Add(New ListColumn("FileFormatCode", "[A0FileFormat].[FileFormatCode]", "FileFormatCode", DataType.String, Nothing, True, 1, True, 100, "File Format Code"))
    oList.Columns.Add(New ListColumn("FileFormatDesc", "[A0FileFormat].[FileFormatDesc]", "FileFormatDesc", DataType.String, Nothing, False, 0, True, 200, "Import File Format Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "[FileFormat]  [A0FileFormat]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function DropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0FileFormat")
    oList.Columns.Add(New ListColumn("FileFormatCode", "[A0FileFormat].[FileFormatCode]", "FileFormatCode", DataType.String, Nothing, True, 1, True, 100, "File Format Code"))
    oList.Columns.Add(New ListColumn("FileFormatDesc", "[A0FileFormat].[FileFormatDesc]", "FileFormatDesc", DataType.String, Nothing, False, 0, True, 200, "Import File Format Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "[FileFormat]  [A0FileFormat]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
