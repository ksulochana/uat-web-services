Imports sembleWare.Runtime
Imports System
Public Class vwDeploymentFileFormatMetaGroup 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly FileFormatMetaGroupID As New IntegerElement("FileFormatMetaGroupID", Me, True, True, True, True, True, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly DatabaseName As New StringElement("DatabaseName", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LoaderDataUpdateModeInd As New StringElement("LoaderDataUpdateModeInd", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly MetaGroupSequenceNum As New IntegerElement("MetaGroupSequenceNum", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly MetaTable As Relationship = New MetaTableRelationship("MetaTable", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly FileFormat As Relationship = New FileFormatRelationship("FileFormat", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly MetaDatabase As Relationship = New MetaDatabaseRelationship("MetaDatabase", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly MetaGroup As Relationship = New MetaGroupRelationship("MetaGroup", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _MetaTableName As New StringElement("MetaTableName", Me, False)
  Public ReadOnly _MetaDatabaseCode As New StringElement("MetaDatabaseCode", Me, True)
  Public ReadOnly _FileFormatCode As New StringElement("FileFormatCode", Me, True)
  Public ReadOnly _MetaGroupCode As New StringElement("MetaGroupCode", Me, True)
  Public ReadOnly _MetaTable As MetaTableRelationship = MetaTable
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  Public ReadOnly _FileFormat As FileFormatRelationship = FileFormat
  Public ReadOnly _MetaDatabase As MetaDatabaseRelationship = MetaDatabase
  Public ReadOnly _MetaGroup As MetaGroupRelationship = MetaGroup
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("vwDeploymentFileFormatMetaGroup", ApplicationSettings)
    _MetaTable._MetaTableName.LocalElement = _MetaTableName
    _MetaTable._MetaDatabaseCode.LocalElement = _MetaDatabaseCode
    _FileFormat._FileFormatCode.LocalElement = _FileFormatCode
    _MetaDatabase._MetaDatabaseCode.LocalElement = _MetaDatabaseCode
    _MetaGroup._MetaGroupCode.LocalElement = _MetaGroupCode
    _MetaGroup._MetaDatabaseCode.LocalElement = _MetaDatabaseCode
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class vwDeploymentFileFormatMetaGroupRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _FileFormatMetaGroupID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _MetaDatabaseCode As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _FileFormatCode As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _MetaGroupCode As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New vwDeploymentFileFormatMetaGroup(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _FileFormatMetaGroupID.ForeignElement = CType(Instance, vwDeploymentFileFormatMetaGroup).FileFormatMetaGroupID
    _MetaDatabaseCode.ForeignElement = CType(Instance, vwDeploymentFileFormatMetaGroup)._MetaDatabaseCode
    _FileFormatCode.ForeignElement = CType(Instance, vwDeploymentFileFormatMetaGroup)._FileFormatCode
    _MetaGroupCode.ForeignElement = CType(Instance, vwDeploymentFileFormatMetaGroup)._MetaGroupCode
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0vwDeploymentFileFormatMetaGroup")
    oList.Columns.Add(New ListColumn("MetaDatabaseCode", "[A0vwDeploymentFileFormatMetaGroup].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, True, 0, True, 100, "Meta Database Code"))
    oList.Columns.Add(New ListColumn("DatabaseName", "[A0vwDeploymentFileFormatMetaGroup].[DatabaseName]", "DatabaseName", DataType.String, Nothing, False, 0, True, 100, "Database Name"))
    oList.Columns.Add(New ListColumn("MetaGroupCode", "[A0vwDeploymentFileFormatMetaGroup].[MetaGroupCode]", "MetaGroupCode", DataType.String, Nothing, True, 0, True, 100, "Meta Group Code"))
    oList.Columns.Add(New ListColumn("FileFormatCode", "[A0vwDeploymentFileFormatMetaGroup].[FileFormatCode]", "FileFormatCode", DataType.String, Nothing, True, 0, True, 100, "File Format Code"))
    oList.Columns.Add(New ListColumn("FileFormatMetaGroupID", "[A0vwDeploymentFileFormatMetaGroup].[FileFormatMetaGroupID]", "FileFormatMetaGroupID", DataType.Integer, Nothing, True, 0, True, 100, "File Format Meta Group ID"))
    oList.Columns.Add(New ListColumn("MetaGroupSequenceNum", "[A0vwDeploymentFileFormatMetaGroup].[MetaGroupSequenceNum]", "MetaGroupSequenceNum", DataType.Integer, Nothing, False, 1, True, 100, "Meta Group Sequence Num"))
    oList.Columns.Add(New ListColumn("MetaTableName", "[A0vwDeploymentFileFormatMetaGroup].[MetaTableName]", "MetaTableName", DataType.String, Nothing, False, 0, True, 100, "Meta Table Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "[vwDeploymentFileFormatMetaGroup]  [A0vwDeploymentFileFormatMetaGroup]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
