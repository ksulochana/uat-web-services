Imports sembleWare.Runtime
Imports System
Public Class NotificationTemplate 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly NotificationTemplateID As New IntegerElement("NotificationTemplateID", Me, True, True, True, True, True, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly NotificationTemplateDesc As New StringElement("NotificationTemplateDesc", Me, False, False, True, True, True, 50, "Template Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly TemplateSubject As New StringElement("TemplateSubject", Me, False, True, True, True, True, 250, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly TemplateDetails As New TextElement("TemplateDetails", Me, True, True, True, True, "Template Details", Nothing, Nothing)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("NotificationTemplate", ApplicationSettings)
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Shared Procedures"
  Public Shared Sub CreateForgotPasswordNotification(ByVal SystemUser As SystemUser, ByVal ApplicationSettings As ApplicationSettings)
    Dim oEncryption As sembleWare.Security.Encryption = New sembleWare.Security.Encryption(Constants.PasswordHashString)
    Dim oNotificationQueue As NotificationQueue = New NotificationQueueRelationship(ApplicationSettings).NewInstance

    oNotificationQueue._NotificationTemplateID.Value = Constants.NotificationTemplate.Records.ForgottenPassword
    oNotificationQueue.Save()

    Dim oNotificationQueueRecipient As NotificationQueueRecipient = oNotificationQueue.NotificationQueueRecipient_OwnMany.NewInstance
    oNotificationQueueRecipient.RecipientTypeInd.Value = Constants.NotificationQueueRecipient.Types.SystemUser
    oNotificationQueueRecipient._SystemUserID.Value = SystemUser.SystemUserID.Value
    oNotificationQueueRecipient.Save()

    Dim oNotificationQueueParameter As NotificationQueueParameter = oNotificationQueue.NotificationQueueParameter_OwnMany.NewInstance
    oNotificationQueueParameter.ParameterName.Value = "Username"
    oNotificationQueueParameter.ParameterValue.Value = SystemUser.Username.Value
    oNotificationQueueParameter.Save()

    oNotificationQueueParameter = oNotificationQueue.NotificationQueueParameter_OwnMany.NewInstance
    oNotificationQueueParameter.ParameterName.Value = "Password"
    oNotificationQueueParameter.ParameterValue.Value = oEncryption.Decrypt(SystemUser.Password.Value)
    oNotificationQueueParameter.Save()
  End Sub

  Public Shared Sub CreateSubscriberTraceConsumerAlertNotification(ByVal SubscriberTraceConsumerAlert As SubscriberTraceConsumerAlert, ByVal ApplicationSettings As ApplicationSettings)
    Dim oNotificationQueue As NotificationQueue = New NotificationQueueRelationship(ApplicationSettings).NewInstance
    oNotificationQueue._NotificationTemplateID.Value = Constants.NotificationTemplate.Records.SubscriberTraceAlert
    oNotificationQueue.Save()

    Dim oNotificationQueueRecipient As NotificationQueueRecipient = oNotificationQueue.NotificationQueueRecipient_OwnMany.NewInstance
    oNotificationQueueRecipient.RecipientTypeInd.Value = Constants.NotificationQueueRecipient.Types.SystemUser
    oNotificationQueueRecipient._SystemUserID.Value = SubscriberTraceConsumerAlert._ActionedBySystemUserID.Value
    oNotificationQueueRecipient.Save()

    Dim oNotificationQueueParameter As NotificationQueueParameter = oNotificationQueue.NotificationQueueParameter_OwnMany.NewInstance
    oNotificationQueueParameter.ParameterName.Value = "ReferenceNo"
    oNotificationQueueParameter.ParameterValue.Value = SubscriberTraceConsumerAlert.ReferenceNo.Value
    oNotificationQueueParameter.Save()

    Dim oSubscriberTraceConsumer As SubscriberTraceConsumer = SubscriberTraceConsumerAlert.SubscriberTraceConsumer.Instance
    oNotificationQueueParameter = oNotificationQueue.NotificationQueueParameter_OwnMany.NewInstance
    oNotificationQueueParameter.ParameterName.Value = "ConsumerFullName"
    If Not oSubscriberTraceConsumer.Consumer.IsEmpty Then
      Dim oConsumer As Consumer = oSubscriberTraceConsumer.Consumer.Instance

      oNotificationQueueParameter.ParameterValue.Value = oConsumer.FirstName.Value & " " & oConsumer.Surname.Value
    Else
      Dim oSubscriberConsumerEnquiry As SubscriberConsumerEnquiry = oSubscriberTraceConsumer.SubscriberConsumerEnquiry.Instance

      oNotificationQueueParameter.ParameterValue.Value = oSubscriberConsumerEnquiry.FirstInitial.Value & " " & oSubscriberConsumerEnquiry.Surname.Value
    End If
    oNotificationQueueParameter.Save()
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class NotificationTemplateRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _NotificationTemplateID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New NotificationTemplate(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _NotificationTemplateID.ForeignElement = CType(Instance, NotificationTemplate).NotificationTemplateID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0NotificationTemplate")
    oList.Columns.Add(New ListColumn("NotificationTemplateID", "[A0NotificationTemplate].[NotificationTemplateID]", "NotificationTemplateID", DataType.Integer, Nothing, True, 0, True, 100, "Notification Template ID"))
    oList.Columns.Add(New ListColumn("NotificationTemplateDesc", "[A0NotificationTemplate].[NotificationTemplateDesc]", "NotificationTemplateDesc", DataType.String, Nothing, False, 1, True, 200, "Template Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "[NotificationTemplate]  [A0NotificationTemplate]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
