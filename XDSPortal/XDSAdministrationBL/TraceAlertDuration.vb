Imports sembleWare.Runtime
Imports System
Public Class TraceAlertDuration 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly TraceAlertDurationID As New IdentityElement("TraceAlertDurationID", Me, True, Nothing, Nothing)
  Public ReadOnly TraceAlertDurationDesc As New StringElement("TraceAlertDurationDesc", Me, False, True, True, True, True, 50, "Duration Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly DurationTypeInd As New IndicatorElement("DurationTypeInd", Me, False, False, True, True, True, True, "Duration Type", Nothing, TraceAlertDuration.DurationTypeIndOptions, Nothing)
  Public ReadOnly DurationCount As New IntegerElement("DurationCount", Me, False, True, True, True, True, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SequenceNum As New IntegerElement("SequenceNum", Me, False, True, True, True, True, "Sequence Number", Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moDurationTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property DurationTypeIndOptions() As IndicatorOptions
    Get
      If moDurationTypeIndOptions Is Nothing Then
        moDurationTypeIndOptions = New IndicatorOptions
        moDurationTypeIndOptions.Add("D", "Days")
        moDurationTypeIndOptions.Add("W", "Weeks")
        moDurationTypeIndOptions.Add("M", "Months")
      End If
      Return moDurationTypeIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("TraceAlertDuration", ApplicationSettings)
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"

  Public Sub MoveUp()
    If Me.SequenceNum.Value = 0 Then
      Return
    End If
    Me.SequenceNum.Value = Me.SequenceNum.Value - 1
    Save()
  End Sub

  Public Sub MoveDown()
    Dim nSequenceNum = Utility.GetNextSequenceNumber(Me.TableName, "", ApplicationSettings) - 1
    If Me.SequenceNum.Value = nSequenceNum Then
      Return
    End If
    Me.SequenceNum.Value = Me.SequenceNum.Value + 1
    Save()
  End Sub

#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Save()
    With ApplicationSettings
      .BeginTransaction()
      Try
        Dim bIsNewYN As Boolean = Me.IsNew
        BRBeforeSave(bIsNewYN)
        MyBase.Save()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
      .CommitTransaction()
    End With
  End Sub

  Public Overrides Sub Delete()
    With ApplicationSettings
      .BeginTransaction()
      Try
        MyBase.Delete()
        BRDelete()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
      .CommitTransaction()
    End With
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRBeforeSave(ByVal IsNewYN As Boolean)
    If IsNewYN Then
      If Me.SequenceNum.IsEmpty Then
        Me.SequenceNum.Value = Utility.GetNextSequenceNumber(Me.TableName, "", ApplicationSettings)
      End If
    End If
    Utility.UpdateSequenceNumber(Me, "", False, ApplicationSettings)
  End Sub

  Private Sub BRDelete()
    Utility.UpdateSequenceNumber(Me, Me, True, ApplicationSettings)
  End Sub
#End Region

#Region " Public Methods"
  Public Function AddDuration(ByVal StartDate As DateTime) As DateTime
    Select Case Me.DurationTypeInd.Value
      Case Constants.TraceAlertDuration.DurationTypes.Day
        Return DateAdd(DateInterval.Day, Me.DurationCount.Value, StartDate)
      Case Constants.TraceAlertDuration.DurationTypes.Week
        Return DateAdd(DateInterval.Day, Me.DurationCount.Value * 7, StartDate)
      Case Constants.TraceAlertDuration.DurationTypes.Month
        Dim oDate As DateTime = DateAdd(DateInterval.Month, Me.DurationCount.Value, StartDate)
        oDate = DateAdd(DateInterval.Day, -1, oDate)
        Return oDate
    End Select
    Return Nothing
  End Function
#End Region
End Class 'sembleWare: Part

Public Class TraceAlertDurationRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _TraceAlertDurationID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New TraceAlertDuration(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _TraceAlertDurationID.ForeignElement = CType(Instance, TraceAlertDuration).TraceAlertDurationID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0TraceAlertDuration")
    oList.Columns.Add(New ListColumn("TraceAlertDurationID", "[A0TraceAlertDuration].[TraceAlertDurationID]", "TraceAlertDurationID", DataType.Long, Nothing, True, 0, False, 100, "Trace Alert Duration ID"))
    oList.Columns.Add(New ListColumn("TraceAlertDurationDesc", "[A0TraceAlertDuration].[TraceAlertDurationDesc]", "TraceAlertDurationDesc", DataType.String, Nothing, False, 0, True, 200, "Duration Description"))
    oList.Columns.Add(New ListColumn("DurationTypeInd", "[A0TraceAlertDuration].[DurationTypeInd]", "DurationTypeInd", DataType.String, Nothing, False, 0, True, 100, "Duration Type"))
    oList.Columns.Add(New ListColumn("DurationCount", "[A0TraceAlertDuration].[DurationCount]", "DurationCount", DataType.Integer, Nothing, False, 0, True, 100, "Duration Count"))
    oList.Columns.Add(New ListColumn("SequenceNum", "[A0TraceAlertDuration].[SequenceNum]", "SequenceNum", DataType.Integer, Nothing, False, 1, False, 100, "Sequence Number"))
    oList.SelectStatement = "select"
    oList.FromClause = "[TraceAlertDuration]  [A0TraceAlertDuration]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function DropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0TraceAlertDuration")
    oList.Columns.Add(New ListColumn("TraceAlertDurationID", "[A0TraceAlertDuration].[TraceAlertDurationID]", "TraceAlertDurationID", DataType.Long, Nothing, True, 0, False, 100, "Trace Alert Duration ID"))
    oList.Columns.Add(New ListColumn("TraceAlertDurationDesc", "[A0TraceAlertDuration].[TraceAlertDurationDesc]", "TraceAlertDurationDesc", DataType.String, Nothing, False, 0, True, 200, "Trace Alert Duration Description"))
    oList.Columns.Add(New ListColumn("DurationTypeInd", "[A0TraceAlertDuration].[DurationTypeInd]", "DurationTypeInd", DataType.String, Nothing, False, 0, True, 100, "Duration Type"))
    oList.Columns.Add(New ListColumn("DurationCount", "[A0TraceAlertDuration].[DurationCount]", "DurationCount", DataType.Integer, Nothing, False, 0, True, 100, "Duration Count"))
    oList.Columns.Add(New ListColumn("SequenceNum", "[A0TraceAlertDuration].[SequenceNum]", "SequenceNum", DataType.Integer, Nothing, False, 1, False, 100, "Sequence Number"))
    oList.SelectStatement = "select"
    oList.FromClause = "[TraceAlertDuration]  [A0TraceAlertDuration]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
