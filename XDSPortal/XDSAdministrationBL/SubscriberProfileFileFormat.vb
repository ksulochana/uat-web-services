Imports sembleWare.Runtime
Imports System
Public Class SubscriberProfileFileFormat 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly LoaderValidationReportingServicesReport As Relationship = New ReportingServicesReportRelationship("LoaderValidationReportingServicesReport", "Loader Validation Report", RelationshipType.Include, False, True, True, Me)
  Public ReadOnly SubscriberProfile As Relationship = New SubscriberProfileRelationship("SubscriberProfile", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly FileFormat As Relationship = New FileFormatRelationship("FileFormat", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _FileFormatCode As New StringElement("FileFormatCode", Me, True)
  Public ReadOnly _LoaderValidationReportingServicesReportID As New IntegerElement("LoaderValidationReportingServicesReportID", Me, False)
  Public ReadOnly _LoaderValidationReportingServicesReport As ReportingServicesReportRelationship = LoaderValidationReportingServicesReport
  Public ReadOnly _SubscriberProfile As SubscriberProfileRelationship = SubscriberProfile
  Public ReadOnly _FileFormat As FileFormatRelationship = FileFormat
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberProfileFileFormat", ApplicationSettings)
    _LoaderValidationReportingServicesReport._ReportingServicesReportID.LocalElement = _LoaderValidationReportingServicesReportID
    _SubscriberProfile._SubscriberID.LocalElement = _SubscriberID
    _FileFormat._FileFormatCode.LocalElement = _FileFormatCode
    'sembleWare: Constructor End - Do Not Modify
    AddHandler FileFormat.LimitRelatedList, AddressOf FileFormat_LimitRelatedList
  End Sub

#End Region 'sembleWare: Constructor

#Region " LimitRelatedList Events"
  Private Sub FileFormat_LimitRelatedList(ByVal List As List)
    List.AndFilters.Add(New ListFilter("FileFormatTypeCode", "FileFormatTypeCode", Constants.FileFormat.Types.Loader, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
  End Sub
#End Region

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Shared Procedures"
  Public Shared Function GetDefaultFileFormat(ByVal SubscriberID As Long, ByVal ApplicationSettings As ApplicationSettings) As FileFormat
    Dim oSubscriberProfile As SubscriberProfile = XDSAdministrationBL.SubscriberProfile.GetSubscriberProfile(SubscriberID, ApplicationSettings)
    Dim oList As List = oSubscriberProfile._SubscriberProfileFileFormat_OwnMany.GridList
    Dim oDataTable As DataTable = oList.DataTable
    Dim oFileFormat As FileFormat = New FileFormatRelationship(ApplicationSettings).NewInstance()

    If oDataTable.Rows.Count = 1 Then
      oFileFormat.FileFormatCode.Load(oDataTable.Rows(0)("FileFormatCode"))
      oFileFormat.Load()

      Return oFileFormat
    End If
    Return Nothing
  End Function

  Public Shared Function GetSubscriberProfileFileFormat(ByVal SubscriberID As Long, ByVal FileFormatCode As String, ByVal ApplicationSettings As ApplicationSettings) As SubscriberProfileFileFormat
    Dim oSubscriberProfileFileFormat As SubscriberProfileFileFormat = New SubscriberProfileFileFormatRelationship(ApplicationSettings).NewInstance

    oSubscriberProfileFileFormat._SubscriberID.Load(SubscriberID)
    oSubscriberProfileFileFormat._FileFormatCode.Load(FileFormatCode)
    oSubscriberProfileFileFormat.Load()

    Return oSubscriberProfileFileFormat
  End Function
#End Region
End Class 'sembleWare: Part

Public Class SubscriberProfileFileFormatRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _FileFormatCode As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberProfileFileFormat(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberID.ForeignElement = CType(Instance, SubscriberProfileFileFormat)._SubscriberID
    _FileFormatCode.ForeignElement = CType(Instance, SubscriberProfileFileFormat)._FileFormatCode
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberProfileFileFormat")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberProfileFileFormat].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("FileFormatCode", "[A0SubscriberProfileFileFormat].[FileFormatCode]", "FileFormatCode", DataType.String, Nothing, True, 0, False, 100, "File Format Code"))
    oList.Columns.Add(New ListColumn("FileFormatCode1", "[A1FileFormat].[FileFormatCode]", "FileFormatCode", DataType.String, Nothing, False, 1, True, 100, "File Format Code"))
    oList.Columns.Add(New ListColumn("FileFormatDesc", "[A1FileFormat].[FileFormatDesc]", "FileFormatDesc", DataType.String, Nothing, False, 0, True, 200, "Import File Format Description"))
    oList.Columns.Add(New ListColumn("ReportName", "[A2LoaderValidationReportingServicesReport].[ReportName]", "ReportName", DataType.String, Nothing, False, 0, True, 200, "Loader Validation Report Name"))
    oList.Columns.Add(New ListColumn("ReportDesc", "[A2LoaderValidationReportingServicesReport].[ReportDesc]", "ReportDesc", DataType.String, Nothing, False, 0, True, 200, "Loader Validation Report Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "(([SubscriberProfileFileFormat]  [A0SubscriberProfileFileFormat]" + _
           "    join [FileFormat]  [A1FileFormat] on [A0SubscriberProfileFileFormat].[FileFormatCode] = [A1FileFormat].[FileFormatCode])" + _
           "    left join [ReportingServicesReport]  [A2LoaderValidationReportingServicesReport] on [A0SubscriberProfileFileFormat].[LoaderValidationReportingServicesReportID] = [A2LoaderValidationReportingServicesReport].[ReportingServicesReportID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
