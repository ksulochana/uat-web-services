Imports sembleWare.Runtime
Imports System
Public Class FileFormatMatchingEngineField 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly SequenceNum As New IntegerElement("SequenceNum", Me, False, False, True, True, True, "Sequence Number", Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FileFormatMetaDatabase As Relationship = New FileFormatMetaDatabaseRelationship("FileFormatMetaDatabase", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly FileFormatField As Relationship = New FileFormatFieldRelationship("FileFormatField", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _FileFormatCode As New StringElement("FileFormatCode", Me, True)
  Public ReadOnly _MetaDatabaseCode As New StringElement("MetaDatabaseCode", Me, True)
  Public ReadOnly _FileFormatFieldID As New IntegerElement("FileFormatFieldID", Me, True)
  Public ReadOnly _FileFormatMetaDatabase As FileFormatMetaDatabaseRelationship = FileFormatMetaDatabase
  Public ReadOnly _FileFormatField As FileFormatFieldRelationship = FileFormatField
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("FileFormatMatchingEngineField", ApplicationSettings)
    _FileFormatMetaDatabase._FileFormatCode.LocalElement = _FileFormatCode
    _FileFormatMetaDatabase._MetaDatabaseCode.LocalElement = _MetaDatabaseCode
    _FileFormatField._FileFormatFieldID.LocalElement = _FileFormatFieldID
    _FileFormatField._FileFormatCode.LocalElement = _FileFormatCode
    'sembleWare: Constructor End - Do Not Modify
    AddHandler FileFormatField.LimitRelatedList, AddressOf FileFormatField_LimitRelatedList
  End Sub

#End Region 'sembleWare: Constructor

#Region " LimitRelatedList Events"
  Private Sub FileFormatField_LimitRelatedList(ByVal List As List)
    Dim oFileFormatMetaDatabase As FileFormatMetaDatabase = Me.FileFormatMetaDatabase.Instance
    List.LimitByRelatedPart(oFileFormatMetaDatabase.FileFormat.Instance, "FileFormat", True)
  End Sub
#End Region

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"

  Public Sub MoveUp()
    If Me.SequenceNum.Value = 0 Then
      Return
    End If
    Me.SequenceNum.Value = Me.SequenceNum.Value - 1
    Save()
  End Sub

  Public Sub MoveDown()
    Dim nSequenceNum = Utility.GetNextSequenceNumber(Me.TableName, Me.FileFormatMetaDatabase.Instance, ApplicationSettings) - 1
    If Me.SequenceNum.Value = nSequenceNum Then
      Return
    End If
    Me.SequenceNum.Value = Me.SequenceNum.Value + 1
    Save()
  End Sub

#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Save()
    With ApplicationSettings
      .BeginTransaction()
      Try
        Dim bIsNewYN As Boolean = Me.IsNew
        BRBeforeSave(bIsNewYN)
        MyBase.Save()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
      .CommitTransaction()
    End With
  End Sub

  Public Overrides Sub Delete()
    With ApplicationSettings
      .BeginTransaction()
      Try
        MyBase.Delete()
        BRAfterDelete()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
      .CommitTransaction()
    End With
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRBeforeSave(ByVal IsNewYN As Boolean)
    If IsNewYN Then
      If Me.SequenceNum.IsEmpty Then
        Me.SequenceNum.Value = Utility.GetNextSequenceNumber(Me.TableName, BRGetSequenceWhereClause(), ApplicationSettings)
      End If
    End If
    Utility.UpdateSequenceNumber(Me, BRGetSequenceWhereClause(), False, ApplicationSettings)
  End Sub

  Private Sub BRAfterDelete()
    Utility.UpdateSequenceNumber(Me, Me.FileFormatMetaDatabase.Instance, True, ApplicationSettings)
  End Sub

  Private Function BRGetSequenceWhereClause() As String
    Return CustomDBPart.InstanceWhereClause(Me.FileFormatMetaDatabase.Instance)
  End Function
#End Region
End Class 'sembleWare: Part

Public Class FileFormatMatchingEngineFieldRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _FileFormatCode As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _MetaDatabaseCode As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _FileFormatFieldID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New FileFormatMatchingEngineField(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _FileFormatCode.ForeignElement = CType(Instance, FileFormatMatchingEngineField)._FileFormatCode
    _MetaDatabaseCode.ForeignElement = CType(Instance, FileFormatMatchingEngineField)._MetaDatabaseCode
    _FileFormatFieldID.ForeignElement = CType(Instance, FileFormatMatchingEngineField)._FileFormatFieldID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0FileFormatMatchingEngineField")
    oList.Columns.Add(New ListColumn("FileFormatCode", "[A0FileFormatMatchingEngineField].[FileFormatCode]", "FileFormatCode", DataType.String, Nothing, True, 0, False, 100, "File Format Code"))
    oList.Columns.Add(New ListColumn("MetaDatabaseCode", "[A0FileFormatMatchingEngineField].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, True, 0, False, 100, "Meta Database Code"))
    oList.Columns.Add(New ListColumn("FileFormatFieldID", "[A0FileFormatMatchingEngineField].[FileFormatFieldID]", "FileFormatFieldID", DataType.Integer, Nothing, True, 0, False, 100, "File Format Field ID"))
    oList.Columns.Add(New ListColumn("FileFormatFieldDesc", "[A1FileFormatField].[FileFormatFieldDesc]", "FileFormatFieldDesc", DataType.String, Nothing, False, 0, True, 200, "Field Description "))
    oList.Columns.Add(New ListColumn("SequenceNum", "[A0FileFormatMatchingEngineField].[SequenceNum]", "SequenceNum", DataType.Integer, Nothing, False, 1, False, 100, "Sequence Number"))
    oList.SelectStatement = "select"
    oList.FromClause = "([FileFormatMatchingEngineField]  [A0FileFormatMatchingEngineField]" + _
           "    join [FileFormatField]  [A1FileFormatField] on [A0FileFormatMatchingEngineField].[FileFormatFieldID] = [A1FileFormatField].[FileFormatFieldID] and [A0FileFormatMatchingEngineField].[FileFormatCode] = [A1FileFormatField].[FileFormatCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
