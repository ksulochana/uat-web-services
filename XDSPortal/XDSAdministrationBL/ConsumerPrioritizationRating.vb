Imports sembleWare.Runtime
Imports System
Public Class ConsumerPrioritizationRating 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly RatingCode As New StringElement("RatingCode", Me, False, True, True, True, True, 10, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RatingDesc As New StringElement("RatingDesc", Me, False, True, True, True, True, 50, "Rating Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LastUpdatedDate As New DateTimeElement("LastUpdatedDate", Me, False, False, True, True, True, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly Consumer As Relationship = New ConsumerRelationship("Consumer", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly PrioritizationRating As Relationship = New PrioritizationRatingRelationship("PrioritizationRating", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _ConsumerID As New IntegerElement("ConsumerID", Me, True)
  Public ReadOnly _PrioritizationRatingID As New IntegerElement("PrioritizationRatingID", Me, True)
  Public ReadOnly _Consumer As ConsumerRelationship = Consumer
  Public ReadOnly _PrioritizationRating As PrioritizationRatingRelationship = PrioritizationRating
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("ConsumerPrioritizationRating", ApplicationSettings)
    _Consumer._ConsumerID.LocalElement = _ConsumerID
    _PrioritizationRating._PrioritizationRatingID.LocalElement = _PrioritizationRatingID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class ConsumerPrioritizationRatingRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _ConsumerID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _PrioritizationRatingID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New ConsumerPrioritizationRating(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _ConsumerID.ForeignElement = CType(Instance, ConsumerPrioritizationRating)._ConsumerID
    _PrioritizationRatingID.ForeignElement = CType(Instance, ConsumerPrioritizationRating)._PrioritizationRatingID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0ConsumerPrioritizationRating")
    oList.Columns.Add(New ListColumn("ConsumerID", "[A0ConsumerPrioritizationRating].[ConsumerID]", "ConsumerID", DataType.Integer, Nothing, True, 0, False, 100, "Consumer ID"))
    oList.Columns.Add(New ListColumn("PrioritizationRatingID", "[A0ConsumerPrioritizationRating].[PrioritizationRatingID]", "PrioritizationRatingID", DataType.Integer, Nothing, True, 0, False, 100, "Prioritization Rating ID"))
    oList.Columns.Add(New ListColumn("PrioritizationRatingDesc", "[A1PrioritizationRating].[PrioritizationRatingDesc]", "PrioritizationRatingDesc", DataType.String, Nothing, False, 1, True, 200, "Prioritization Rating Description"))
    oList.Columns.Add(New ListColumn("RatingCode", "[A0ConsumerPrioritizationRating].[RatingCode]", "RatingCode", DataType.String, Nothing, False, 0, True, 100, "Rating Code"))
    oList.Columns.Add(New ListColumn("RatingDesc", "[A0ConsumerPrioritizationRating].[RatingDesc]", "RatingDesc", DataType.String, Nothing, False, 0, True, 200, "Rating Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "([ConsumerPrioritizationRating]  [A0ConsumerPrioritizationRating]" + _
           "    join [PrioritizationRating]  [A1PrioritizationRating] on [A0ConsumerPrioritizationRating].[PrioritizationRatingID] = [A1PrioritizationRating].[PrioritizationRatingID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
