Imports sembleWare.Runtime
Imports System
Public Class LoaderProcessMetaDatabase 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly DuplicateRecordCount As New LongElement("DuplicateRecordCount", Me, False, False, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ValidRecordCount As New LongElement("ValidRecordCount", Me, False, False, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly InvalidRecordCount As New LongElement("InvalidRecordCount", Me, False, False, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly TotalRecordCount As New LongElement("TotalRecordCount", Me, False, False, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly NoMatchCount As New LongElement("NoMatchCount", Me, False, False, True, True, False, "No Match Count (New Record)", Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SingleMatchCount As New LongElement("SingleMatchCount", Me, False, False, True, True, False, "Single Match Count (Existing Record)", Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly MultipleMatchCount As New LongElement("MultipleMatchCount", Me, False, False, True, True, False, "Multiple Match Count (Existing Records)", Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PossibleNameConflictCount As New LongElement("PossibleNameConflictCount", Me, False, False, True, True, False, "Possible Name Conflict Count (New Record)", Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PossibleDuplicateRecordCount As New LongElement("PossibleDuplicateRecordCount", Me, False, False, True, True, False, "Possible Duplicate Record Count (New Record)", Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly InsertedRecordCount As New LongElement("InsertedRecordCount", Me, False, False, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly UpdatedRecordCount As New LongElement("UpdatedRecordCount", Me, False, False, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CompletionPerc As New DecimalElement("CompletionPerc", Me, False, True, True, True, True, "Completion %", Nothing, "##0.00", 0, Nothing, Nothing)
  Public ReadOnly LoaderProcess As Relationship = New LoaderProcessRelationship("LoaderProcess", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly MetaDatabase As Relationship = New MetaDatabaseRelationship("MetaDatabase", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly LoaderProcessMetaDataProcedure_OwnMany As Relationship = New LoaderProcessMetaDataProcedureRelationship("LoaderProcessMetaDataProcedure", Nothing, "LoaderProcessMetaDatabase", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _LoaderID As New IntegerElement("LoaderID", Me, True)
  Public ReadOnly _LoaderProcessTypeCode As New StringElement("LoaderProcessTypeCode", Me, True)
  Public ReadOnly _MetaDatabaseCode As New StringElement("MetaDatabaseCode", Me, True)
  Public ReadOnly _LoaderProcess As LoaderProcessRelationship = LoaderProcess
  Public ReadOnly _MetaDatabase As MetaDatabaseRelationship = MetaDatabase
  Public ReadOnly _LoaderProcessMetaDataProcedure_OwnMany As LoaderProcessMetaDataProcedureRelationship = LoaderProcessMetaDataProcedure_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("LoaderProcessMetaDatabase", ApplicationSettings)
    _LoaderProcess._LoaderID.LocalElement = _LoaderID
    _LoaderProcess._LoaderProcessTypeCode.LocalElement = _LoaderProcessTypeCode
    _MetaDatabase._MetaDatabaseCode.LocalElement = _MetaDatabaseCode
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Methods"
  Public Function GetInvalidLoaderFileName()
    Dim oLoaderProcess As LoaderProcess = Me.LoaderProcess.Instance
    Dim oLoader As Loader = oLoaderProcess.Loader.Instance
    Dim sFolderPath As String = SystemDefault.GetLoaderInvalidFolderPath(ApplicationSettings)
    Dim sFileName As String = System.IO.Path.GetFileNameWithoutExtension(oLoader.LoaderFileName.Value) & " - " & Me._MetaDatabaseCode.Value & " - Invalid" & System.IO.Path.GetExtension(oLoader.LoaderFileName.Value)

    Return System.IO.Path.Combine(sFolderPath, sFileName)
  End Function
#End Region

#Region " Shared Procedures"
  Public Shared Sub AddLoaderProcessMetaDatabase(ByVal LoaderID As Long, ByVal LoaderProcessTypeCode As String, ByVal MetaDatabase As MetaDatabase, ByVal ApplicationSettings As ApplicationSettings)
    AddLoaderProcessMetaDatabase(LoaderID, LoaderProcessTypeCode, MetaDatabase, 0, 0, ApplicationSettings)
  End Sub

  Public Shared Sub AddLoaderProcessMetaDatabase(ByVal LoaderID As Long, ByVal LoaderProcessTypeCode As String, ByVal MetaDatabase As MetaDatabase, ByVal CompletionPerc As Decimal, ByVal TotalRecordCount As Long, ByVal ApplicationSettings As ApplicationSettings)
    Dim oLoaderProcessMetaDatabase As LoaderProcessMetaDatabase = New LoaderProcessMetaDatabaseRelationship(ApplicationSettings).NewInstance()
    oLoaderProcessMetaDatabase._LoaderID.Load(LoaderID)
    oLoaderProcessMetaDatabase._LoaderProcessTypeCode.Load(LoaderProcessTypeCode)
    oLoaderProcessMetaDatabase.MetaDatabase.Instance = MetaDatabase
    oLoaderProcessMetaDatabase.CompletionPerc.Value = CompletionPerc
    oLoaderProcessMetaDatabase.TotalRecordCount.Value = TotalRecordCount
    oLoaderProcessMetaDatabase.Save()
  End Sub

  Public Shared Sub UpdateLoaderProcessMetaDatabaseCompletion(ByVal LoaderID As Long, ByVal LoaderProcessTypeCode As String, ByVal MetaDatabase As MetaDatabase, ByVal CompletionPerc As Decimal, ByVal ApplicationSettings As ApplicationSettings)
    Dim oLoaderProcessMetaDatabase As LoaderProcessMetaDatabase = New LoaderProcessMetaDatabaseRelationship(ApplicationSettings).NewInstance()
    oLoaderProcessMetaDatabase._LoaderID.Load(LoaderID)
    oLoaderProcessMetaDatabase._LoaderProcessTypeCode.Load(LoaderProcessTypeCode)
    oLoaderProcessMetaDatabase.MetaDatabase.Instance = MetaDatabase
    oLoaderProcessMetaDatabase.IgnoreCacheOnce = True
    oLoaderProcessMetaDatabase.Load()

    oLoaderProcessMetaDatabase.CompletionPerc.Value = CompletionPerc
    oLoaderProcessMetaDatabase.Save()
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class LoaderProcessMetaDatabaseRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _LoaderID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _LoaderProcessTypeCode As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _MetaDatabaseCode As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New LoaderProcessMetaDatabase(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _LoaderID.ForeignElement = CType(Instance, LoaderProcessMetaDatabase)._LoaderID
    _LoaderProcessTypeCode.ForeignElement = CType(Instance, LoaderProcessMetaDatabase)._LoaderProcessTypeCode
    _MetaDatabaseCode.ForeignElement = CType(Instance, LoaderProcessMetaDatabase)._MetaDatabaseCode
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function DataLoadGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0LoaderProcessMetaDatabase")
    oList.Columns.Add(New ListColumn("LoaderID", "[A0LoaderProcessMetaDatabase].[LoaderID]", "LoaderID", DataType.Integer, Nothing, True, 0, False, 100, "Loader ID"))
    oList.Columns.Add(New ListColumn("LoaderProcessTypeCode", "[A0LoaderProcessMetaDatabase].[LoaderProcessTypeCode]", "LoaderProcessTypeCode", DataType.String, Nothing, True, 0, False, 100, "Loader Process Type Code"))
    oList.Columns.Add(New ListColumn("MetaDatabaseCode", "[A0LoaderProcessMetaDatabase].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, True, 0, False, 100, "Meta Database Code"))
    oList.Columns.Add(New ListColumn("MetaDatabaseCode1", "[A1MetaDatabase].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, False, 1, True, 100, "Database Code"))
    oList.Columns.Add(New ListColumn("MetaDatabaseDesc", "[A1MetaDatabase].[MetaDatabaseDesc]", "MetaDatabaseDesc", DataType.String, Nothing, False, 0, True, 200, "Database Description"))
    oList.Columns.Add(New ListColumn("MetaDatabaseTypeInd", "[A1MetaDatabase].[MetaDatabaseTypeInd]", "MetaDatabaseTypeInd", DataType.String, Nothing, False, 0, False, 100, "Database Type"))
    oList.Columns.Add(New ListColumn("TotalRecordCount", "[A0LoaderProcessMetaDatabase].[TotalRecordCount]", "TotalRecordCount", DataType.Long, Nothing, False, 0, True, 150, "Total Record Count"))
    oList.Columns.Add(New ListColumn("CompletionPerc", "[A0LoaderProcessMetaDatabase].[CompletionPerc]", "CompletionPerc", DataType.Decimal, "##0.00", False, 0, True, 100, "Completion %"))
    oList.SelectStatement = "select"
    oList.FromClause = "([LoaderProcessMetaDatabase]  [A0LoaderProcessMetaDatabase]" + _
           "    join [MetaDatabase]  [A1MetaDatabase] on [A0LoaderProcessMetaDatabase].[MetaDatabaseCode] = [A1MetaDatabase].[MetaDatabaseCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function DataValidationGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0LoaderProcessMetaDatabase")
    oList.Columns.Add(New ListColumn("LoaderID", "[A0LoaderProcessMetaDatabase].[LoaderID]", "LoaderID", DataType.Integer, Nothing, True, 0, False, 100, "Loader ID"))
    oList.Columns.Add(New ListColumn("LoaderProcessTypeCode", "[A0LoaderProcessMetaDatabase].[LoaderProcessTypeCode]", "LoaderProcessTypeCode", DataType.String, Nothing, True, 0, False, 100, "Loader Process Type Code"))
    oList.Columns.Add(New ListColumn("MetaDatabaseCode", "[A0LoaderProcessMetaDatabase].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, True, 0, False, 100, "Meta Database Code"))
    oList.Columns.Add(New ListColumn("MetaDatabaseCode1", "[A1MetaDatabase].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, False, 1, True, 100, "Database Code"))
    oList.Columns.Add(New ListColumn("MetaDatabaseDesc", "[A1MetaDatabase].[MetaDatabaseDesc]", "MetaDatabaseDesc", DataType.String, Nothing, False, 0, True, 200, "Database Description"))
    oList.Columns.Add(New ListColumn("MetaDatabaseTypeInd", "[A1MetaDatabase].[MetaDatabaseTypeInd]", "MetaDatabaseTypeInd", DataType.String, Nothing, False, 0, False, 100, "Database Type"))
    oList.Columns.Add(New ListColumn("DuplicateRecordCount", "[A0LoaderProcessMetaDatabase].[DuplicateRecordCount]", "DuplicateRecordCount", DataType.Long, Nothing, False, 0, True, 150, "Duplicate Record Count"))
    oList.Columns.Add(New ListColumn("InvalidRecordCount", "[A0LoaderProcessMetaDatabase].[InvalidRecordCount]", "InvalidRecordCount", DataType.Long, Nothing, False, 0, True, 150, "Invalid Record Count"))
    oList.Columns.Add(New ListColumn("ValidRecordCount", "[A0LoaderProcessMetaDatabase].[ValidRecordCount]", "ValidRecordCount", DataType.Long, Nothing, False, 0, True, 150, "Valid Record Count"))
    oList.Columns.Add(New ListColumn("TotalRecordCount", "[A0LoaderProcessMetaDatabase].[TotalRecordCount]", "TotalRecordCount", DataType.Long, Nothing, False, 0, True, 150, "Total Record Count"))
    oList.Columns.Add(New ListColumn("CompletionPerc", "[A0LoaderProcessMetaDatabase].[CompletionPerc]", "CompletionPerc", DataType.Decimal, "##0.00", False, 0, True, 100, "Completion %"))
    oList.SelectStatement = "select"
    oList.FromClause = "([LoaderProcessMetaDatabase]  [A0LoaderProcessMetaDatabase]" + _
           "    join [MetaDatabase]  [A1MetaDatabase] on [A0LoaderProcessMetaDatabase].[MetaDatabaseCode] = [A1MetaDatabase].[MetaDatabaseCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function DataMatchingGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0LoaderProcessMetaDatabase")
    oList.Columns.Add(New ListColumn("LoaderID", "[A0LoaderProcessMetaDatabase].[LoaderID]", "LoaderID", DataType.Integer, Nothing, True, 0, False, 100, "Loader ID"))
    oList.Columns.Add(New ListColumn("LoaderProcessTypeCode", "[A0LoaderProcessMetaDatabase].[LoaderProcessTypeCode]", "LoaderProcessTypeCode", DataType.String, Nothing, True, 0, False, 100, "Loader Process Type Code"))
    oList.Columns.Add(New ListColumn("MetaDatabaseCode", "[A0LoaderProcessMetaDatabase].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, True, 0, False, 100, "Meta Database Code"))
    oList.Columns.Add(New ListColumn("MetaDatabaseCode1", "[A1MetaDatabase].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, False, 1, True, 100, "Database Code"))
    oList.Columns.Add(New ListColumn("MetaDatabaseDesc", "[A1MetaDatabase].[MetaDatabaseDesc]", "MetaDatabaseDesc", DataType.String, Nothing, False, 0, True, 200, "Database Description"))
    oList.Columns.Add(New ListColumn("MetaDatabaseTypeInd", "[A1MetaDatabase].[MetaDatabaseTypeInd]", "MetaDatabaseTypeInd", DataType.String, Nothing, False, 0, False, 100, "Database Type"))
    oList.Columns.Add(New ListColumn("NoMatchCount", "[A0LoaderProcessMetaDatabase].[NoMatchCount]", "NoMatchCount", DataType.Long, Nothing, False, 0, True, 150, "No Match Count (New Record)"))
    oList.Columns.Add(New ListColumn("SingleMatchCount", "[A0LoaderProcessMetaDatabase].[SingleMatchCount]", "SingleMatchCount", DataType.Long, Nothing, False, 0, True, 150, "Single Match Count (Existing Record)"))
    oList.Columns.Add(New ListColumn("PossibleNameConflictCount", "[A0LoaderProcessMetaDatabase].[PossibleNameConflictCount]", "PossibleNameConflictCount", DataType.Long, Nothing, False, 0, True, 150, "Possible Name Conflict Count (New Record)"))
    oList.Columns.Add(New ListColumn("PossibleDuplicateRecordCount", "[A0LoaderProcessMetaDatabase].[PossibleDuplicateRecordCount]", "PossibleDuplicateRecordCount", DataType.Long, Nothing, False, 0, True, 100, "Possible Duplicate Record Count (New Record)"))
    oList.Columns.Add(New ListColumn("TotalRecordCount", "[A0LoaderProcessMetaDatabase].[TotalRecordCount]", "TotalRecordCount", DataType.Long, Nothing, False, 0, True, 150, "Total Record Count"))
    oList.Columns.Add(New ListColumn("CompletionPerc", "[A0LoaderProcessMetaDatabase].[CompletionPerc]", "CompletionPerc", DataType.Decimal, "##0.00", False, 0, True, 100, "Completion %"))
    oList.SelectStatement = "select"
    oList.FromClause = "([LoaderProcessMetaDatabase]  [A0LoaderProcessMetaDatabase]" + _
           "    join [MetaDatabase]  [A1MetaDatabase] on [A0LoaderProcessMetaDatabase].[MetaDatabaseCode] = [A1MetaDatabase].[MetaDatabaseCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function DataUpdateGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0LoaderProcessMetaDatabase")
    oList.Columns.Add(New ListColumn("LoaderID", "[A0LoaderProcessMetaDatabase].[LoaderID]", "LoaderID", DataType.Integer, Nothing, True, 0, False, 100, "Loader ID"))
    oList.Columns.Add(New ListColumn("LoaderProcessTypeCode", "[A0LoaderProcessMetaDatabase].[LoaderProcessTypeCode]", "LoaderProcessTypeCode", DataType.String, Nothing, True, 0, False, 100, "Loader Process Type Code"))
    oList.Columns.Add(New ListColumn("MetaDatabaseCode", "[A0LoaderProcessMetaDatabase].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, True, 0, False, 100, "Meta Database Code"))
    oList.Columns.Add(New ListColumn("MetaDatabaseCode1", "[A1MetaDatabase].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, False, 1, True, 100, "Database Code"))
    oList.Columns.Add(New ListColumn("MetaDatabaseDesc", "[A1MetaDatabase].[MetaDatabaseDesc]", "MetaDatabaseDesc", DataType.String, Nothing, False, 0, True, 200, "Database Description"))
    oList.Columns.Add(New ListColumn("MetaDatabaseTypeInd", "[A1MetaDatabase].[MetaDatabaseTypeInd]", "MetaDatabaseTypeInd", DataType.String, Nothing, False, 0, False, 100, "Database Type"))
    oList.Columns.Add(New ListColumn("InsertedRecordCount", "[A0LoaderProcessMetaDatabase].[InsertedRecordCount]", "InsertedRecordCount", DataType.Long, Nothing, False, 0, True, 150, "Inserted Record Count"))
    oList.Columns.Add(New ListColumn("UpdatedRecordCount", "[A0LoaderProcessMetaDatabase].[UpdatedRecordCount]", "UpdatedRecordCount", DataType.Long, Nothing, False, 0, True, 150, "Updated Record Count"))
    oList.Columns.Add(New ListColumn("TotalRecordCount", "[A0LoaderProcessMetaDatabase].[TotalRecordCount]", "TotalRecordCount", DataType.Long, Nothing, False, 0, True, 150, "Total Record Count"))
    oList.Columns.Add(New ListColumn("CompletionPerc", "[A0LoaderProcessMetaDatabase].[CompletionPerc]", "CompletionPerc", DataType.Decimal, "##0.00", False, 0, True, 100, "Completion %"))
    oList.SelectStatement = "select"
    oList.FromClause = "([LoaderProcessMetaDatabase]  [A0LoaderProcessMetaDatabase]" + _
           "    join [MetaDatabase]  [A1MetaDatabase] on [A0LoaderProcessMetaDatabase].[MetaDatabaseCode] = [A1MetaDatabase].[MetaDatabaseCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function DataUndoGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0LoaderProcessMetaDatabase")
    oList.Columns.Add(New ListColumn("LoaderID", "[A0LoaderProcessMetaDatabase].[LoaderID]", "LoaderID", DataType.Integer, Nothing, True, 0, False, 100, "Loader ID"))
    oList.Columns.Add(New ListColumn("LoaderProcessTypeCode", "[A0LoaderProcessMetaDatabase].[LoaderProcessTypeCode]", "LoaderProcessTypeCode", DataType.String, Nothing, True, 0, False, 100, "Loader Process Type Code"))
    oList.Columns.Add(New ListColumn("MetaDatabaseCode", "[A0LoaderProcessMetaDatabase].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, True, 0, False, 100, "Meta Database Code"))
    oList.Columns.Add(New ListColumn("MetaDatabaseCode1", "[A1MetaDatabase].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, False, 1, True, 100, "Database Code"))
    oList.Columns.Add(New ListColumn("MetaDatabaseDesc", "[A1MetaDatabase].[MetaDatabaseDesc]", "MetaDatabaseDesc", DataType.String, Nothing, False, 0, True, 200, "Database Description"))
    oList.Columns.Add(New ListColumn("MetaDatabaseTypeInd", "[A1MetaDatabase].[MetaDatabaseTypeInd]", "MetaDatabaseTypeInd", DataType.String, Nothing, False, 0, False, 100, "Database Type"))
    oList.Columns.Add(New ListColumn("TotalRecordCount", "[A0LoaderProcessMetaDatabase].[TotalRecordCount]", "TotalRecordCount", DataType.Long, Nothing, False, 0, True, 150, "Total Record Count"))
    oList.Columns.Add(New ListColumn("CompletionPerc", "[A0LoaderProcessMetaDatabase].[CompletionPerc]", "CompletionPerc", DataType.Decimal, "##0.00", False, 0, True, 100, "Completion %"))
    oList.SelectStatement = "select"
    oList.FromClause = "([LoaderProcessMetaDatabase]  [A0LoaderProcessMetaDatabase]" + _
           "    join [MetaDatabase]  [A1MetaDatabase] on [A0LoaderProcessMetaDatabase].[MetaDatabaseCode] = [A1MetaDatabase].[MetaDatabaseCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
