Imports sembleWare.Runtime
Imports System
Public Class LoaderMatchingEngine 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly LoaderMatchingEngineID As New IdentityElement("LoaderMatchingEngineID", Me, True, Nothing, Nothing)
  Public ReadOnly MatchingEngineDate As New DateTimeElement("MatchingEngineDate", Me, False, True, True, True, True, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly LoaderID As New IntegerElement("LoaderID", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly LoaderMatchingEngineConsumer_OwnMany As Relationship = New LoaderMatchingEngineConsumerRelationship("LoaderMatchingEngineConsumer", Nothing, "LoaderMatchingEngine", Me)
  Public ReadOnly LoaderMatchingEngineCommercial_OwnMany As Relationship = New LoaderMatchingEngineCommercialRelationship("LoaderMatchingEngineCommercial", Nothing, "LoaderMatchingEngine", Me)
  Public ReadOnly LoaderMatchingEngineHomeAffairs_OwnMany As Relationship = New LoaderMatchingEngineHomeAffairsRelationship("LoaderMatchingEngineHomeAffairs", Nothing, "LoaderMatchingEngine", Me)
  Public ReadOnly LoaderMatchingEngineDirector_OwnMany As Relationship = New LoaderMatchingEngineDirectorRelationship("LoaderMatchingEngineDirector", Nothing, "LoaderMatchingEngine", Me)
  Public ReadOnly LoaderMatchingEngineAuditor_OwnMany As Relationship = New LoaderMatchingEngineAuditorRelationship("LoaderMatchingEngineAuditor", Nothing, "LoaderMatchingEngine", Me)
  Public ReadOnly LoaderMatchingEnginePropertyDeed_OwnMany As Relationship = New LoaderMatchingEnginePropertyDeedRelationship("LoaderMatchingEnginePropertyDeed", Nothing, "LoaderMatchingEngine", Me)
  Public ReadOnly LoaderMatchingEngineSAFPSSubject_OwnMany As Relationship = New LoaderMatchingEngineSAFPSSubjectRelationship("LoaderMatchingEngineSAFPSSubject", Nothing, "LoaderMatchingEngine", Me)
  Public ReadOnly LoaderMatchingEngineSAFPSProtectiveRegistration_OwnMany As Relationship = New LoaderMatchingEngineSAFPSProtectiveRegistrationRelationship("LoaderMatchingEngineSAFPSProtectiveRegistration", Nothing, "LoaderMatchingEngine", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  Public ReadOnly _LoaderMatchingEngineConsumer_OwnMany As LoaderMatchingEngineConsumerRelationship = LoaderMatchingEngineConsumer_OwnMany
  Public ReadOnly _LoaderMatchingEngineCommercial_OwnMany As LoaderMatchingEngineCommercialRelationship = LoaderMatchingEngineCommercial_OwnMany
  Public ReadOnly _LoaderMatchingEngineHomeAffairs_OwnMany As LoaderMatchingEngineHomeAffairsRelationship = LoaderMatchingEngineHomeAffairs_OwnMany
  Public ReadOnly _LoaderMatchingEngineDirector_OwnMany As LoaderMatchingEngineDirectorRelationship = LoaderMatchingEngineDirector_OwnMany
  Public ReadOnly _LoaderMatchingEngineAuditor_OwnMany As LoaderMatchingEngineAuditorRelationship = LoaderMatchingEngineAuditor_OwnMany
  Public ReadOnly _LoaderMatchingEnginePropertyDeed_OwnMany As LoaderMatchingEnginePropertyDeedRelationship = LoaderMatchingEnginePropertyDeed_OwnMany
  Public ReadOnly _LoaderMatchingEngineSAFPSSubject_OwnMany As LoaderMatchingEngineSAFPSSubjectRelationship = LoaderMatchingEngineSAFPSSubject_OwnMany
  Public ReadOnly _LoaderMatchingEngineSAFPSProtectiveRegistration_OwnMany As LoaderMatchingEngineSAFPSProtectiveRegistrationRelationship = LoaderMatchingEngineSAFPSProtectiveRegistration_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("LoaderMatchingEngine", ApplicationSettings)
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreate()
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreate()
    MatchingEngineDate.Value = System.DateTime.Now
  End Sub
#End Region

#Region " Shared Procedures"
  Public Shared Function ConsumerMatch(ByVal IDNo As Element, ByVal PassportNo As Element, ByVal Surname As Element, ByVal MaidenName As Element, ByVal FirstInitial As Element, ByVal SecondInitial As Element, ByVal BirthDate As Element, ByVal AccountNo As Element, ByVal SubAccountNo As Element, ByVal ApplicationSettings As ApplicationSettings) As Integer
    Dim oLoaderMatchingEngine As LoaderMatchingEngine = New LoaderMatchingEngineRelationship(ApplicationSettings).NewInstance
    oLoaderMatchingEngine.Save()

    Dim oLoaderMatchingEngineConsumer As LoaderMatchingEngineConsumer = oLoaderMatchingEngine.LoaderMatchingEngineConsumer_OwnMany.NewInstance
    oLoaderMatchingEngineConsumer.RecordID.Value = 1
    If Not IDNo.IsEmpty Then
      oLoaderMatchingEngineConsumer.IDNo.Value = IDNo.ValueAsObject
    End If
    If Not PassportNo.IsEmpty Then
      oLoaderMatchingEngineConsumer.PassportNo.Value = PassportNo.ValueAsObject
    End If
    If Not Surname.IsEmpty Then
      oLoaderMatchingEngineConsumer.Surname.Value = Surname.ValueAsObject
    End If
    If Not MaidenName.IsEmpty Then
      oLoaderMatchingEngineConsumer.MaidenName.Value = MaidenName.ValueAsObject
    End If
    If Not FirstInitial.IsEmpty Then
      oLoaderMatchingEngineConsumer.FirstInitial.Value = FirstInitial.ValueAsObject
    End If
    If Not SecondInitial.IsEmpty Then
      oLoaderMatchingEngineConsumer.SecondInitial.Value = SecondInitial.ValueAsObject
    End If
    If Not BirthDate.IsEmpty Then
      oLoaderMatchingEngineConsumer.BirthDate.Value = BirthDate.ValueAsObject
    End If
    If Not AccountNo.IsEmpty Then
      oLoaderMatchingEngineConsumer.AccountNo.Value = AccountNo.ValueAsObject
    End If
    If Not SubAccountNo.IsEmpty Then
      oLoaderMatchingEngineConsumer.SubAccountNo.Value = SubAccountNo.ValueAsObject
    End If
    oLoaderMatchingEngineConsumer.Save()

    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand
    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spLoaderConsumer_S_Match"

    oCommand.Parameters.Add("@LoaderMatchingEngineID", SqlDbType.Int)

    oCommand.Parameters("@LoaderMatchingEngineID").Value = oLoaderMatchingEngine.LoaderMatchingEngineID.Value

    ApplicationSettings.ActionQuery(oCommand)

    Return oLoaderMatchingEngine.LoaderMatchingEngineID.Value
  End Function

  Public Shared Function HomeAffairsMatch(ByVal IDNo As Element, ByVal Surname As Element, ByVal FirstName As Element, ByVal SecondName As Element, ByVal BirthDate As Element, ByVal ApplicationSettings As ApplicationSettings) As Integer
    Dim oLoaderMatchingEngine As LoaderMatchingEngine = New LoaderMatchingEngineRelationship(ApplicationSettings).NewInstance
    oLoaderMatchingEngine.Save()

    Dim oLoaderMatchingEngineHomeAffairs As LoaderMatchingEngineHomeAffairs = oLoaderMatchingEngine.LoaderMatchingEngineHomeAffairs_OwnMany.NewInstance
    oLoaderMatchingEngineHomeAffairs.RecordID.Value = 1
    If Not IDNo.IsEmpty Then
      oLoaderMatchingEngineHomeAffairs.IDNo.Value = IDNo.ValueAsObject
    End If
    If Not Surname.IsEmpty Then
      oLoaderMatchingEngineHomeAffairs.Surname.Value = Surname.ValueAsObject
    End If
    If Not FirstName.IsEmpty Then
      oLoaderMatchingEngineHomeAffairs.FirstName.Value = FirstName.ValueAsObject
    End If
    If Not SecondName.IsEmpty Then
      oLoaderMatchingEngineHomeAffairs.SecondName.Value = SecondName.ValueAsObject
    End If
    If Not BirthDate.IsEmpty Then
      oLoaderMatchingEngineHomeAffairs.BirthDate.Value = BirthDate.ValueAsObject
    End If
    oLoaderMatchingEngineHomeAffairs.Save()

    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand
    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spLoaderHomeAffairs_S_Match"

    oCommand.Parameters.Add("@LoaderMatchingEngineID", SqlDbType.Int)

    oCommand.Parameters("@LoaderMatchingEngineID").Value = oLoaderMatchingEngine.LoaderMatchingEngineID.Value

    ApplicationSettings.ActionQuery(oCommand)

    Return oLoaderMatchingEngine.LoaderMatchingEngineID.Value
  End Function

  Public Shared Function CommercialMatch(ByVal RegistrationNo As Element, ByVal CommercialName As Element, ByVal SICCode As Element, ByVal ApplicationSettings As ApplicationSettings) As Integer
    Dim oLoaderMatchingEngine As LoaderMatchingEngine = New LoaderMatchingEngineRelationship(ApplicationSettings).NewInstance
    oLoaderMatchingEngine.Save()

    Dim oLoaderMatchingEngineCommercial As LoaderMatchingEngineCommercial = oLoaderMatchingEngine.LoaderMatchingEngineCommercial_OwnMany.NewInstance
    oLoaderMatchingEngineCommercial.RecordID.Value = 1
    If Not RegistrationNo.IsEmpty Then
      oLoaderMatchingEngineCommercial.RegistrationNo.Value = RegistrationNo.ValueAsObject
    End If
    If Not CommercialName.IsEmpty Then
      oLoaderMatchingEngineCommercial.CommercialName.Value = CommercialName.ValueAsObject
    End If
    If Not SICCode.IsEmpty Then
      oLoaderMatchingEngineCommercial.SICCode.Value = SICCode.ValueAsObject
    End If
    oLoaderMatchingEngineCommercial.Save()

    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand
    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spLoaderCommercial_S_Match"

    oCommand.Parameters.Add("@LoaderMatchingEngineID", SqlDbType.Int)

    oCommand.Parameters("@LoaderMatchingEngineID").Value = oLoaderMatchingEngine.LoaderMatchingEngineID.Value

    ApplicationSettings.ActionQuery(oCommand)

    Return oLoaderMatchingEngine.LoaderMatchingEngineID.Value
  End Function

  Public Shared Function AuditorMatch(ByVal ProfessionNo As Element, ByVal AuditorName As Element, ByVal ApplicationSettings As ApplicationSettings) As Integer
    Dim oLoaderMatchingEngine As LoaderMatchingEngine = New LoaderMatchingEngineRelationship(ApplicationSettings).NewInstance
    oLoaderMatchingEngine.Save()

    Dim oLoaderMatchingEngineAuditor As LoaderMatchingEngineAuditor = oLoaderMatchingEngine.LoaderMatchingEngineAuditor_OwnMany.NewInstance
    oLoaderMatchingEngineAuditor.RecordID.Value = 1
    If Not ProfessionNo.IsEmpty Then
      oLoaderMatchingEngineAuditor.ProfessionNo.Value = ProfessionNo.ValueAsObject
    End If
    If Not AuditorName.IsEmpty Then
      oLoaderMatchingEngineAuditor.AuditorName.Value = AuditorName.ValueAsObject
    End If
    oLoaderMatchingEngineAuditor.Save()

    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand
    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spLoaderAuditor_S_Match"

    oCommand.Parameters.Add("@LoaderMatchingEngineID", SqlDbType.Int)

    oCommand.Parameters("@LoaderMatchingEngineID").Value = oLoaderMatchingEngine.LoaderMatchingEngineID.Value

    ApplicationSettings.ActionQuery(oCommand)

    Return oLoaderMatchingEngine.LoaderMatchingEngineID.Value
  End Function

  Public Shared Function DirectorMatch(ByVal IDNo As Element, ByVal Surname As Element, ByVal FirstInitial As Element, ByVal SecondInitial As Element, ByVal BirthDate As Element, ByVal ApplicationSettings As ApplicationSettings) As Integer
    Dim oLoaderMatchingEngine As LoaderMatchingEngine = New LoaderMatchingEngineRelationship(ApplicationSettings).NewInstance
    oLoaderMatchingEngine.Save()

    Dim oLoaderMatchingEngineDirector As LoaderMatchingEngineDirector = oLoaderMatchingEngine.LoaderMatchingEngineDirector_OwnMany.NewInstance
    oLoaderMatchingEngineDirector.RecordID.Value = 1
    If Not IDNo.IsEmpty Then
      oLoaderMatchingEngineDirector.IDNo.Value = IDNo.ValueAsObject
    End If
    If Not Surname.IsEmpty Then
      oLoaderMatchingEngineDirector.Surname.Value = Surname.ValueAsObject
    End If
    If Not FirstInitial.IsEmpty Then
      oLoaderMatchingEngineDirector.FirstInitial.Value = FirstInitial.ValueAsObject
    End If
    If Not SecondInitial.IsEmpty Then
      oLoaderMatchingEngineDirector.SecondInitial.Value = SecondInitial.ValueAsObject
    End If
    If Not BirthDate.IsEmpty Then
      oLoaderMatchingEngineDirector.BirthDate.Value = BirthDate.ValueAsObject
    End If

    oLoaderMatchingEngineDirector.Save()

    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand
    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spLoaderDirector_S_Match"

    oCommand.Parameters.Add("@LoaderMatchingEngineID", SqlDbType.Int)

    oCommand.Parameters("@LoaderMatchingEngineID").Value = oLoaderMatchingEngine.LoaderMatchingEngineID.Value

    ApplicationSettings.ActionQuery(oCommand)

    Return oLoaderMatchingEngine.LoaderMatchingEngineID.Value
  End Function

  Public Shared Function PropertyDeedMatch(ByVal ExternalSourceID As Element, ByVal ApplicationSettings As ApplicationSettings) As Integer
    Dim oLoaderMatchingEngine As LoaderMatchingEngine = New LoaderMatchingEngineRelationship(ApplicationSettings).NewInstance
    oLoaderMatchingEngine.Save()

    Dim oLoaderMatchingEnginePropertyDeed As LoaderMatchingEnginePropertyDeed = oLoaderMatchingEngine.LoaderMatchingEnginePropertyDeed_OwnMany.NewInstance
    oLoaderMatchingEnginePropertyDeed.RecordID.Value = 1
    If Not ExternalSourceID.IsEmpty Then
      oLoaderMatchingEnginePropertyDeed.ExternalSourceID.Value = ExternalSourceID.ValueAsObject
    End If
    oLoaderMatchingEnginePropertyDeed.Save()

    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand
    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spLoaderPropertyDeed_S_Match"

    oCommand.Parameters.Add("@LoaderMatchingEngineID", SqlDbType.Int)

    oCommand.Parameters("@LoaderMatchingEngineID").Value = oLoaderMatchingEngine.LoaderMatchingEngineID.Value

    ApplicationSettings.ActionQuery(oCommand)

    Return oLoaderMatchingEngine.LoaderMatchingEngineID.Value
  End Function

    Public Shared Function SAFPSSubjectMatch(ByVal ExternalSourceID As Element, ByVal ApplicationSettings As ApplicationSettings) As Integer
        Dim oLoaderMatchingEngine As LoaderMatchingEngine = New LoaderMatchingEngineRelationship(ApplicationSettings).NewInstance
        oLoaderMatchingEngine.Save()

        Dim oLoaderMatchingEngineSAFPSSubject As LoaderMatchingEngineSAFPSSubject = oLoaderMatchingEngine.LoaderMatchingEngineSAFPSSubject_OwnMany.NewInstance
        oLoaderMatchingEngineSAFPSSubject.RecordID.Value = 1
        If Not ExternalSourceID.IsEmpty Then
            oLoaderMatchingEngineSAFPSSubject.ExternalSourceID.Value = ExternalSourceID.ValueAsObject
        End If
        oLoaderMatchingEngineSAFPSSubject.Save()

        Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand
        oCommand.CommandType = CommandType.StoredProcedure
        oCommand.CommandText = "spLoaderSAFPSSubject_S_Match"

        oCommand.Parameters.Add("@LoaderMatchingEngineID", SqlDbType.Int)

        oCommand.Parameters("@LoaderMatchingEngineID").Value = oLoaderMatchingEngine.LoaderMatchingEngineID.Value

        ApplicationSettings.ActionQuery(oCommand)

        Return oLoaderMatchingEngine.LoaderMatchingEngineID.Value
    End Function

    Public Shared Function SAFPSProtectiveRegistrationMatch(ByVal PRReference As Element, ByVal ApplicationSettings As ApplicationSettings) As Integer
        Dim oLoaderMatchingEngine As LoaderMatchingEngine = New LoaderMatchingEngineRelationship(ApplicationSettings).NewInstance
        oLoaderMatchingEngine.Save()

        Dim oLoaderMatchingEngineSAFPSProtectiveRegistration As LoaderMatchingEngineSAFPSProtectiveRegistration = oLoaderMatchingEngine.LoaderMatchingEngineSAFPSProtectiveRegistration_OwnMany.NewInstance
        oLoaderMatchingEngineSAFPSProtectiveRegistration.RecordID.Value = 1
        If Not PRReference.IsEmpty Then
            oLoaderMatchingEngineSAFPSProtectiveRegistration.PRReference.Value = PRReference.ValueAsObject
        End If
        oLoaderMatchingEngineSAFPSProtectiveRegistration.Save()

        Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand
        oCommand.CommandType = CommandType.StoredProcedure
        oCommand.CommandText = "spLoaderSAFPSProtectiveRegistration_S_Match"

        oCommand.Parameters.Add("@LoaderMatchingEngineID", SqlDbType.Int)

        oCommand.Parameters("@LoaderMatchingEngineID").Value = oLoaderMatchingEngine.LoaderMatchingEngineID.Value

        ApplicationSettings.ActionQuery(oCommand)

        Return oLoaderMatchingEngine.LoaderMatchingEngineID.Value
    End Function


#End Region
End Class 'sembleWare: Part

Public Class LoaderMatchingEngineRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _LoaderMatchingEngineID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New LoaderMatchingEngine(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _LoaderMatchingEngineID.ForeignElement = CType(Instance, LoaderMatchingEngine).LoaderMatchingEngineID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
