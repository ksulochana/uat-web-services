Imports sembleWare.Runtime
Imports System
Public Class ReportTraceConsumerSystemUserProductDetail 'sembleWare: Part
  Inherits CustomMemoryPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly TraceDateFrom As New DateTimeElement("TraceDateFrom", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly TraceDateTo As New DateTimeElement("TraceDateTo", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.Include, True, True, True, Me)
  Public ReadOnly SystemUser As Relationship = New SystemUserRelationship("SystemUser", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly ProductType As Relationship = New ProductTypeRelationship("ProductType", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, False)
  Public ReadOnly _SystemUserID As New IntegerElement("SystemUserID", Me, False)
  Public ReadOnly _ProductTypeID As New IntegerElement("ProductTypeID", Me, False)
  Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
  Public ReadOnly _SystemUser As SystemUserRelationship = SystemUser
  Public ReadOnly _ProductType As ProductTypeRelationship = ProductType
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("ReportTraceConsumerSystemUserProductDetail", ApplicationSettings)
    _Subscriber._SubscriberID.LocalElement = _SubscriberID
    _SystemUser._SystemUserID.LocalElement = _SystemUserID
    _ProductType._ProductTypeID.LocalElement = _ProductTypeID
    'sembleWare: Constructor End - Do Not Modify
    AddHandler SystemUser.LimitRelatedList, AddressOf SystemUser_LimitRelatedList
  End Sub

#End Region 'sembleWare: Constructor

#Region " LimitRelatedList Events"
  Private Sub SystemUser_LimitRelatedList(ByVal List As List)
    List.LimitByRelatedPart(Me.Subscriber.Instance, "Subscriber")
  End Sub
#End Region

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class ReportTraceConsumerSystemUserProductDetailRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New ReportTraceConsumerSystemUserProductDetail(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
