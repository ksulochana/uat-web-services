Imports sembleWare.Runtime
Imports System
Public Class SubscriberAssociationConsumerTraceReport 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly DisplayIdentityInformationYN As New BooleanElement("DisplayIdentityInformationYN", Me, False, True, True, True, True, "Display Identity Information?", Nothing, False, "Yes;No")
  Public ReadOnly DisplayDeceasedIndicatorYN As New BooleanElement("DisplayDeceasedIndicatorYN", Me, False, True, True, True, True, "Display Deceased Indicator?", Nothing, False, "Yes;No")
  Public ReadOnly DisplayReportSummaryYN As New BooleanElement("DisplayReportSummaryYN", Me, False, True, True, True, True, "Display Report Summary?", Nothing, False, "Yes;No")
  Public ReadOnly DisplayPrioritizationRatingYN As New BooleanElement("DisplayPrioritizationRatingYN", Me, False, True, True, True, True, "Display Prioritization Rating?", Nothing, False, "Yes;No")
  Public ReadOnly DisplayMonthlyPaymentDetailsYN As New BooleanElement("DisplayMonthlyPaymentDetailsYN", Me, False, True, True, True, True, "Display Monthly Payment Details?", Nothing, False, "Yes;No")
  Public ReadOnly DisplayAdverseInfoYN As New BooleanElement("DisplayAdverseInfoYN", Me, False, True, True, True, True, "Display Public Domain - Adverse Information?", Nothing, False, "Yes;No")
  Public ReadOnly DisplayJudgementsYN As New BooleanElement("DisplayJudgementsYN", Me, False, True, True, True, True, "Display Public Domain - Judgements?", Nothing, False, "Yes;No")
  Public ReadOnly DisplayAdministrationOrdersYN As New BooleanElement("DisplayAdministrationOrdersYN", Me, False, True, True, True, True, "Display Public Domain - Administration Orders?", Nothing, False, "Yes;No")
  Public ReadOnly DisplaySequestrationsYN As New BooleanElement("DisplaySequestrationsYN", Me, False, True, True, True, True, "Display Public Domain - Sequestrations?", Nothing, False, "Yes;No")
  Public ReadOnly DisplayNameHistoryYN As New BooleanElement("DisplayNameHistoryYN", Me, False, True, True, True, True, "Display Name History?", Nothing, False, "Yes;No")
  Public ReadOnly DisplayAddressHistoryYN As New BooleanElement("DisplayAddressHistoryYN", Me, False, True, True, True, True, "Display Address History?", Nothing, False, "Yes;No")
  Public ReadOnly DisplayTelephoneHistoryYN As New BooleanElement("DisplayTelephoneHistoryYN", Me, False, True, True, True, True, "Display Telephone History?", Nothing, False, "Yes;No")
    Public ReadOnly DisplayEmploymentHistoryYN As New BooleanElement("DisplayEmploymentHistoryYN", Me, False, True, True, True, True, "Display Employment History?", Nothing, False, "Yes;No")
    Public ReadOnly DisplayScoringInformationYN As New BooleanElement("DisplayScoringInformationYN", Me, False, True, True, True, True, "Display Scoring Information?", Nothing, False, "Yes;No")
    Public ReadOnly DisplayAdditionalTelephoneSearchYN As New BooleanElement("DisplayAdditionalTelephoneSearchYN", Me, False, True, True, True, True, "Display Additional Telephone Search details?", Nothing, False, "Yes;No")

    Public ReadOnly DisplayFraudIndicatorsSummaryYN As New BooleanElement("DisplayFraudIndicatorsSummaryYN", Me, False, True, True, True, True, "Display Fraud Indicators Summary?", Nothing, False, "Yes;No")
    Public ReadOnly DisplayCreditAccountSummaryYN As New BooleanElement("DisplayCreditAccountSummaryYN", Me, False, True, True, True, True, "Display Credit Account Summary?", Nothing, False, "Yes;No")
    Public ReadOnly DisplaySemiDefaultInfoYN As New BooleanElement("DisplaySemiDefaultInfoYN", Me, False, True, True, True, True, "Display Semi Default Information?", Nothing, False, "Yes;No")

    Public ReadOnly DisplayPropertyInterestsYN As New BooleanElement("DisplayPropertyInterestsYN", Me, False, True, True, True, True, "Display Property Interests ?", Nothing, False, "Yes;No")
    Public ReadOnly DisplayTraceEnquiryHistoryYN As New BooleanElement("DisplayTraceEnquiryHistoryYN", Me, False, True, True, True, True, "Display Trace Enquiry History?", Nothing, False, "Yes;No")
    Public ReadOnly DisplayDebtReviewStatusYN As New BooleanElement("DisplayDebtReviewStatusYN", Me, False, True, True, True, True, "Display Debt Review Status ?", Nothing, False, "Yes;No")
    Public ReadOnly DisplayCreditAccountStatusYN As New BooleanElement("DisplayCreditAccountStatusYN", Me, False, True, True, True, True, "Display Credit Account Status ?", Nothing, False, "Yes;No")
    Public ReadOnly DisplayDebtSummaryNonCPAYN As New BooleanElement("DisplayDebtSummaryNonCPAYN", Me, False, True, True, True, True, "Display Debt Summary for Non CPA Members?", Nothing, False, "Yes;No")
    Public ReadOnly DisplayConfirmationPersonalDetailsYN As New BooleanElement("DisplayConfirmationPersonalDetailsYN", Me, False, True, True, True, True, "Display Consumer Personal Confirmation Details?", Nothing, False, "Yes;No")

    Public ReadOnly DisplayDirectorshipLinksYN As New BooleanElement("DisplayDirectorshipLinksYN", Me, False, True, True, True, True, "Display Directorship Links?", Nothing, False, "Yes;No")

    Public ReadOnly DisplayDebtSummaryYN As New BooleanElement("DisplayDebtSummaryYN", Me, False, True, True, True, True, "Display Debt Summary?", Nothing, False, "Yes;No")

  Public ReadOnly SubscriberAssociation As Relationship = New SubscriberAssociationRelationship("SubscriberAssociation", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberAssociationCode As New StringElement("SubscriberAssociationCode", Me, True)
  Public ReadOnly _SubscriberAssociation As SubscriberAssociationRelationship = SubscriberAssociation
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberAssociationConsumerTraceReport", ApplicationSettings)
    _SubscriberAssociation._SubscriberAssociationCode.LocalElement = _SubscriberAssociationCode
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Shared Procedures"
  Public Shared Function GetSubscriberAssociationConsumerTraceReport(ByVal SubscriberAssociationCode As String, ByVal ApplicationSettings As ApplicationSettings) As SubscriberAssociationConsumerTraceReport
    Dim oSubscriberAssociationConsumerTraceReport As SubscriberAssociationConsumerTraceReport = New SubscriberAssociationConsumerTraceReportRelationship(ApplicationSettings).NewInstance
    Try
      oSubscriberAssociationConsumerTraceReport._SubscriberAssociationCode.Load(SubscriberAssociationCode)
      oSubscriberAssociationConsumerTraceReport.Load()
    Catch oException As sembleWare.Runtime.RecordNotFoundException
      oSubscriberAssociationConsumerTraceReport = New SubscriberAssociationConsumerTraceReportRelationship(ApplicationSettings).NewInstance
      oSubscriberAssociationConsumerTraceReport._SubscriberAssociationCode.Load(SubscriberAssociationCode)
    Catch oException As Exception
      Throw oException
    End Try
    Return oSubscriberAssociationConsumerTraceReport
  End Function
#End Region
End Class 'sembleWare: Part

Public Class SubscriberAssociationConsumerTraceReportRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberAssociationCode As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberAssociationConsumerTraceReport(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberAssociationCode.ForeignElement = CType(Instance, SubscriberAssociationConsumerTraceReport)._SubscriberAssociationCode
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
