Imports sembleWare.Runtime
Imports System
Public Class SAFPSSubjectOtherNumber 'sembleWare: Part
    Inherits CustomDBPart

#Region " Elements & Relationships"

    'sembleWare: Elements Start - Do Not Modify
    Public ReadOnly SAFPSSubjectOtherNumberID As New IdentityElement("SAFPSSubjectOtherNumberID", Me, True, Nothing, Nothing)
    Public ReadOnly TelephoneType As New StringElement("TelephoneType", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly TelephoneNo As New StringElement("TelephoneNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly City As New StringElement("City", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly Country As New StringElement("Country", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly ExternalSubjectSourceID As New IntegerElement("ExternalSubjectSourceID", Me, False, False, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly ExternalOtherSourceID As New IntegerElement("ExternalOtherSourceID", Me, False, False, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly RecordStatusInd As New IndicatorElement("RecordStatusInd", Me, False, False, False, True, True, True, "Record Status", Nothing, SAFPSSubjectOtherNumber.RecordStatusIndOptions, "A")
    Public ReadOnly DeletedReason As New StringElement("DeletedReason", Me, False, True, True, True, False, 500, Nothing, Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly LastUpdatedDate As New DateTimeElement("LastUpdatedDate", Me, False, False, True, True, True, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
    Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.Include, False, False, True, Me)
    Public ReadOnly Loader As Relationship = New LoaderRelationship("Loader", Nothing, RelationshipType.Include, False, False, True, Me)
    Public ReadOnly SAFPSSubject As Relationship = New SAFPSSubjectRelationship("SAFPSSubject", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
    'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

    'sembleWare: Foreign Items Start - Do Not Modify
    Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, False)
    Public ReadOnly _LoaderID As New IntegerElement("LoaderID", Me, False)
    Public ReadOnly _SAFPSSubjectID As New IntegerElement("SAFPSSubjectID", Me, True)
    Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
    Public ReadOnly _Loader As LoaderRelationship = Loader
    Public ReadOnly _SAFPSSubject As SAFPSSubjectRelationship = SAFPSSubject
    'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

    'sembleWare: Indicator Options Start - Do Not Modify
    Private Shared moRecordStatusIndOptions As IndicatorOptions
    Public Shared ReadOnly Property RecordStatusIndOptions() As IndicatorOptions
        Get
            If moRecordStatusIndOptions Is Nothing Then
                moRecordStatusIndOptions = New IndicatorOptions
                moRecordStatusIndOptions.Add("A", "Active")
                moRecordStatusIndOptions.Add("D", "Deleted")
            End If
            Return moRecordStatusIndOptions
        End Get
    End Property
    'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

    Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

        'sembleWare: Constructor Start - Do Not Modify
        MyBase.New("SAFPSSubjectOtherNumber", ApplicationSettings)
        _Subscriber._SubscriberID.LocalElement = _SubscriberID
        _Loader._LoaderID.LocalElement = _LoaderID
        _SAFPSSubject._SAFPSSubjectID.LocalElement = _SAFPSSubjectID
        'sembleWare: Constructor End - Do Not Modify
        Me.DeleteType = CustomDBPart.enumDeleteType.ChangeRecordStatus
    End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region "base Overrides"
    Public Overrides Sub Create()
        MyBase.Create()
        BRCreate()
    End Sub
#End Region


#Region "User Code"
    Private Sub BRCreate()
        Me.LastUpdatedDate.Value = System.DateTime.Now
    End Sub
#End Region


End Class 'sembleWare: Part

Public Class SAFPSSubjectOtherNumberRelationship 'sembleWare: Relationship
    Inherits Relationship

#Region " Relationship Code"

    'sembleWare: Relationship Start - Do Not Modify
    Public ReadOnly _SAFPSSubjectOtherNumberID As ForeignKey = New ForeignKey(Me)
    Public ReadOnly _SAFPSSubjectID As ForeignKey = New ForeignKey(Me)

    Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
        MyBase.New(ApplicationSettings)
    End Sub

    Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
        MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
    End Sub

    Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
        MyBase.New(Name, Caption, OwnedByName, Owner)
    End Sub

    Public Overrides Function CreateObject() As sembleWare.Runtime.Part
        Return New SAFPSSubjectOtherNumber(ApplicationSettings)
    End Function

    Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
        _SAFPSSubjectOtherNumberID.ForeignElement = CType(Instance, SAFPSSubjectOtherNumber).SAFPSSubjectOtherNumberID
        _SAFPSSubjectID.ForeignElement = CType(Instance, SAFPSSubjectOtherNumber)._SAFPSSubjectID
    End Sub

    'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

    Public Function GridList() As sembleWare.Runtime.List
        'sembleWare: List Declaration Start - Do Not Modify
        Dim oList As DBList = New DBList(Me, "A0SAFPSSubjectOtherNumber")
        oList.Columns.Add(New ListColumn("SAFPSSubjectOtherNumberID", "[A0SAFPSSubjectOtherNumber].[SAFPSSubjectOtherNumberID]", "SAFPSSubjectOtherNumberID", DataType.Long, Nothing, True, 0, False, 100, "SAFPSSubject Other Number ID"))
        oList.Columns.Add(New ListColumn("SAFPSSubjectID", "[A0SAFPSSubjectOtherNumber].[SAFPSSubjectID]", "SAFPSSubjectID", DataType.Integer, Nothing, True, 0, False, 100, "SAFPSSubject ID"))
        oList.Columns.Add(New ListColumn("TelephoneNo", "[A0SAFPSSubjectOtherNumber].[TelephoneNo]", "TelephoneNo", DataType.String, Nothing, False, 0, True, 100, "Telephone No"))
        oList.Columns.Add(New ListColumn("TelephoneType", "[A0SAFPSSubjectOtherNumber].[TelephoneType]", "TelephoneType", DataType.String, Nothing, False, 0, True, 100, "Telephone Type"))
        oList.Columns.Add(New ListColumn("City", "[A0SAFPSSubjectOtherNumber].[City]", "City", DataType.String, Nothing, False, 0, True, 100, "City"))
        oList.Columns.Add(New ListColumn("Country", "[A0SAFPSSubjectOtherNumber].[Country]", "Country", DataType.String, Nothing, False, 0, True, 100, "Country"))
        oList.SelectStatement = "select"
        oList.FromClause = "[SAFPSSubjectOtherNumber]  [A0SAFPSSubjectOtherNumber]"
        oList.WhereClause = ""
        oList.ApplyRelationshipLimits()
        'sembleWare: List Declaration End - Do Not Modify
        oList.AndFilters.Add(New ListFilter("RecordStatusInd", "[A0SAFPSSubjectOtherNumber].[RecordStatusInd]", Constants.RecordStatus.Active, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
        Return oList
    End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
