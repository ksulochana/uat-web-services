Imports sembleWare.Runtime
Imports System
Public Class SPSystemUser 'sembleWare: Part
  Inherits CustomMemoryPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly SystemUserID_SP As New IntegerElement("SystemUserID_SP", Me, False, True, True, True, False, "System User ID", Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FirstName_SP As New StringElement("FirstName_SP", Me, False, True, True, True, False, 50, "First Name", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Surname_SP As New StringElement("Surname_SP", Me, False, True, True, True, False, 50, "Surname", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ActiveInd_SP As New IndicatorElement("ActiveInd_SP", Me, False, False, True, True, True, False, "Active?", Nothing, SPSystemUser.ActiveInd_SPOptions, "All")
  Public ReadOnly Username_SP As New StringElement("Username_SP", Me, False, True, True, True, False, 50, "Username", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moActiveInd_SPOptions As IndicatorOptions
  Public Shared ReadOnly Property ActiveInd_SPOptions() As IndicatorOptions
    Get
      If moActiveInd_SPOptions Is Nothing Then
        moActiveInd_SPOptions = New IndicatorOptions
        moActiveInd_SPOptions.Add("Y", "Yes")
        moActiveInd_SPOptions.Add("N", "No")
        moActiveInd_SPOptions.Add("All", "All")
      End If
      Return moActiveInd_SPOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SPSystemUser", ApplicationSettings)
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class SPSystemUserRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SPSystemUser(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
