Imports sembleWare.Runtime
Imports System
Public Class Role 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly RoleCode As New StringElement("RoleCode", Me, True, True, True, True, True, 10, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RoleDesc As New StringElement("RoleDesc", Me, False, True, True, True, True, 50, "Role Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RoleLabelDescription As New StringElement("RoleLabelDescription", Me, False, True, True, False, False, 250, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly SystemUserRole_OwnMany As Relationship = New SystemUserRoleRelationship("SystemUserRole", Nothing, "Role", Me)
  Public ReadOnly RoleControlAccess_OwnMany As Relationship = New RoleControlAccessRelationship("RoleControlAccess", Nothing, "Role", Me)
  Public ReadOnly RoleFormAccess_OwnMany As Relationship = New RoleFormAccessRelationship("RoleFormAccess", Nothing, "Role", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  Public ReadOnly _SystemUserRole_OwnMany As SystemUserRoleRelationship = SystemUserRole_OwnMany
  Public ReadOnly _RoleControlAccess_OwnMany As RoleControlAccessRelationship = RoleControlAccess_OwnMany
  Public ReadOnly _RoleFormAccess_OwnMany As RoleFormAccessRelationship = RoleFormAccess_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("Role", ApplicationSettings)
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreate()
  End Sub

  Public Overrides Sub Load()
    MyBase.Load()
    BRLoad()
  End Sub

  Public Overrides Sub Save()
    MyBase.Save()
    BRSave()
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreate()
    BRRoleLabelDescription()
  End Sub

  Private Sub BRLoad()
    BRRoleLabelDescription()
  End Sub

  Private Sub BRSave()
    BRRoleLabelDescription()
  End Sub

  Public Sub BRRoleLabelDescription()
    If Me.IsNew Then
      Me.RoleLabelDescription.Value = "Role: (New)"
    Else
      Dim sDesc As String = Trim(Me.RoleDesc.Value)
      Me.RoleLabelDescription.Value = "Role: " & Me.RoleCode.Value & " (" & sDesc & ")"
    End If
  End Sub
#End Region

#Region " Shared Procedures"
  Public Shared Function GetRole(ByVal RoleCode As String, ByVal ApplicationSettings As ApplicationSettings) As Role
    Dim oRole As Role = New RoleRelationship(ApplicationSettings).NewInstance()

    oRole.RoleCode.Load(RoleCode)
    oRole.Load()

    Return oRole
  End Function

  Public Shared Function GetSubscriberAdministrators(ByVal ApplicationSettings As ApplicationSettings) As Role
    Return GetRole(Constants.Role.Records.SubscriberAdministrators, ApplicationSettings)
  End Function

  Public Shared Function GetSubscriberUsers(ByVal ApplicationSettings As ApplicationSettings) As Role
    Return GetRole(Constants.Role.Records.SubscriberUsers, ApplicationSettings)
  End Function

  Public Shared Function GetSystemAdministrators(ByVal ApplicationSettings As ApplicationSettings) As Role
    Return GetRole(Constants.Role.Records.SystemAdministrators, ApplicationSettings)
  End Function

  Public Shared Sub RepopulateAllRolesAccessRights(ByVal ApplicationSettings As ApplicationSettings, ByVal AssemblyName As String)
    'COMMENT: This could be implemented as a stored procedure
    'Copies all NEW SecurityForms and insert into the RoleFormAccess for all roles with CanView rights
    'Copies all NEW SecurityControls and insert into the RoleControlAccess for all roles with CanView rights
    Dim oQB As IQueryBuilder = ApplicationSettings.QueryBuilder

    Try
      ApplicationSettings.BeginTransaction()
      Dim oList As List = New RoleRelationship(ApplicationSettings).List()
      Dim oRow As ListRow
      For Each oRow In oList
        RepopulateRoleAccessRights(ApplicationSettings, AssemblyName, oRow("RoleCode"))
      Next

      RoleFormAccess.UpdateRoleFormControlAccess(AssemblyName, ApplicationSettings)

      ApplicationSettings.CommitTransaction()
    Catch
      ApplicationSettings.RollbackTransaction()
      Throw
    End Try
  End Sub

  Public Sub ResetRoleAccessRights()
    'COMMENT: This could be implemented as a stored procedure
    'Copies all the SecurityForms and insert into the RoleFormAccess for this role with CanView rights
    'Copies all the SecurityControls and insert into the RoleControlAccess for this role with CanView rights
    Dim oQB As IQueryBuilder = ApplicationSettings.QueryBuilder
    Dim q As String

    ApplicationSettings.BeginTransaction()
    Try

      q = "delete from RoleControlAccess where RoleCode = " + oQB.ToSQL(RoleCode.Value)
      ApplicationSettings.ActionQuery(q)

      q = "delete from RoleFormAccess where RoleCode = " + oQB.ToSQL(RoleCode.Value)
      ApplicationSettings.ActionQuery(q)

      q = "insert into RoleFormAccess(FormName, RoleCode, Access) " + _
        " select SecurityForm.FormName, " + oQB.ToSQL(RoleCode.Value) + ", " + oQB.ToSQL(RoleFormAccess.Access_CanView) + _
        "  from SecurityForm "
      ApplicationSettings.ActionQuery(q)

      q = "insert into RoleControlAccess(FormName, ControlName, RoleCode, Access) " + _
        " select SecurityControl.FormName, SecurityControl.ControlName, " + oQB.ToSQL(RoleCode.Value) + ", " + oQB.ToSQL(RoleControlAccess.Access_CanView) + _
        "  from SecurityControl "
      ApplicationSettings.ActionQuery(q)
      ApplicationSettings.CommitTransaction()
    Catch
      ApplicationSettings.RollbackTransaction()
      Throw
    End Try
  End Sub

  Public Sub RepopulateRoleAccessRights(ByVal AssemblyName As String)
    RepopulateRoleAccessRights(ApplicationSettings, AssemblyName, RoleCode.Value)
  End Sub

  Public Shared Sub RepopulateRoleAccessRights(ByVal ApplicationSettings As ApplicationSettings, ByVal AssemblyName As String, ByVal RoleCode As String)
    'COMMENT: This could be implemented as a stored procedure
    'Copies all NEW SecurityForms and insert into the RoleFormAccess for this role with No rights
    'Copies all NEW SecurityControls and insert into the RoleControlAccess for this role with CanView rights
    Dim oQB As IQueryBuilder = ApplicationSettings.QueryBuilder
    Dim q As String

    ApplicationSettings.BeginTransaction()
    Try
      q = "insert into RoleFormAccess(AssemblyName, FormName, RoleCode, AccessInd) " + _
        " select SecurityForm.AssemblyName, SecurityForm.FormName, " + oQB.ToSQL(RoleCode) + ", " + oQB.ToSQL(RoleFormAccess.Access_None) + _
        "  from SecurityForm " + _
        " where SecurityForm.FormName not in " + _
        "         (select CurrentFormAccess.FormName " + _
        "            from RoleFormAccess CurrentFormAccess " + _
        "           where CurrentFormAccess.AssemblyName = " + oQB.ToSQL(AssemblyName) + _
        "             and CurrentFormAccess.RoleCode = " + oQB.ToSQL(RoleCode) + _
        "          )" + _
        "   and SecurityForm.AssemblyName = " + oQB.ToSQL(AssemblyName)
      ApplicationSettings.ActionQuery(q)

      q = "insert into RoleControlAccess(AssemblyName, FormName, ControlName, RoleCode, AccessInd) " + _
        " select SecurityControl.AssemblyName, SecurityControl.FormName, SecurityControl.ControlName, " + oQB.ToSQL(RoleCode) + ", " + oQB.ToSQL(RoleControlAccess.Access_None) + _
        "  from SecurityControl " + _
        " where SecurityControl.FormName + '_' + SecurityControl.ControlName not in " + _
        "         (select CurrentControlAccess.FormName + '_' + CurrentControlAccess.ControlName " + _
        "            from RoleControlAccess CurrentControlAccess " + _
        "           where CurrentControlAccess.AssemblyName = " + oQB.ToSQL(AssemblyName) + _
        "             and CurrentControlAccess.RoleCode = " + oQB.ToSQL(RoleCode) + _
        "          )" + _
        "   and SecurityControl.AssemblyName = " + oQB.ToSQL(AssemblyName)
      ApplicationSettings.ActionQuery(q)
      ApplicationSettings.CommitTransaction()
    Catch
      ApplicationSettings.RollbackTransaction()
      Throw
    End Try
  End Sub

  Public Sub CopyRoleAccessRights(ByVal FromRole As Role)
    'COMMENT: This could be implemented as a stored procedure
    'Copies all the RoleFormAccess from one role to another 
    'Copies all the RoleControlAccess from one role to another 
    Dim oQB As IQueryBuilder = ApplicationSettings.QueryBuilder
    Dim q As String

    ApplicationSettings.BeginTransaction()
    Try
      'Delete the current access rights for this role
      q = "delete from RoleControlAccess where RoleCode = " + oQB.ToSQL(RoleCode.Value)
      ApplicationSettings.ActionQuery(q)
      q = "delete from RoleFormAccess where RoleCode = " + oQB.ToSQL(RoleCode.Value)
      ApplicationSettings.ActionQuery(q)

      'Copy the access rights from the FromRole
      q = "insert into RoleFormAccess(AssemblyName, FormName, RoleCode, AccessInd) " + _
        " select FromAccess.AssemblyName, FromAccess.FormName, " + oQB.ToSQL(RoleCode.Value) + ", FromAccess.Access " + _
        "  from RoleFormAccess FromAccess " + _
        " where FromAccess.RoleCode = " + oQB.ToSQL(FromRole.RoleCode.Value)
      ApplicationSettings.ActionQuery(q)

      q = "insert into RoleControlAccess(AssemblyName, FormName, ControlName, RoleCode, AccessInd) " + _
        " select FromAccess.AssemblyName, FromAccess.FormName, FromAccess.ControlName, " + oQB.ToSQL(RoleCode.Value) + ", FromAccess.Access " + _
        "  from RoleControlAccess FromAccess " + _
        " where FromAccess.RoleCode = " + oQB.ToSQL(FromRole.RoleCode.Value)
      ApplicationSettings.ActionQuery(q)
      ApplicationSettings.CommitTransaction()
    Catch
      ApplicationSettings.RollbackTransaction()
      Throw
    End Try
  End Sub
#End Region

End Class 'sembleWare: Part

Public Class RoleRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _RoleCode As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New Role(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _RoleCode.ForeignElement = CType(Instance, Role).RoleCode
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"



  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0Role")
    oList.Columns.Add(New ListColumn("RoleCode", "[A0Role].[RoleCode]", "RoleCode", DataType.String, Nothing, True, 1, True, 100, "Role Code"))
    oList.Columns.Add(New ListColumn("RoleDesc", "[A0Role].[RoleDesc]", "RoleDesc", DataType.String, Nothing, False, 0, True, 200, "Role Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "[Role]  [A0Role]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function











  Public Function RolesForSystemUserList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0Role")
    oList.Columns.Add(New ListColumn("RoleCode", "[A0Role].[RoleCode]", "RoleCode", DataType.String, Nothing, True, 0, True, 81, "Code"))
    oList.Columns.Add(New ListColumn("Description", "[A0Role].[Description]", "Description", DataType.String, Nothing, False, 0, True, 157, "Role"))
    oList.Columns.Add(New ListColumn("RoleLevelCode", "[A1RoleLevel].[RoleLevelCode]", "RoleLevelCode", DataType.String, Nothing, False, 0, False, 87, "Role Level Code"))
    oList.Columns.Add(New ListColumn("RoleLevelDesc", "[A1RoleLevel].[Description]", "Description", DataType.String, Nothing, False, 0, True, 174, "Level"))
    oList.Columns.Add(New ListColumn("FieldName1", "[A1RoleLevel].[FieldName1]", "FieldName1", DataType.String, Nothing, False, 0, True, 87, "Field Name 1"))
    oList.Columns.Add(New ListColumn("FieldName2", "[A1RoleLevel].[FieldName2]", "FieldName2", DataType.String, Nothing, False, 0, True, 83, "Field Name 2"))
    oList.Columns.Add(New ListColumn("FieldName3", "[A1RoleLevel].[FieldName3]", "FieldName3", DataType.String, Nothing, False, 0, True, 100, "Field Name 3"))
    oList.Columns.Add(New ListColumn("FieldName4", "[A1RoleLevel].[FieldName4]", "FieldName4", DataType.String, Nothing, False, 0, True, 100, "Field Name 4"))
    oList.Columns.Add(New ListColumn("FieldName5", "[A1RoleLevel].[FieldName5]", "FieldName5", DataType.String, Nothing, False, 0, True, 100, "Field Name 5"))
    oList.Columns.Add(New ListColumn("FieldName6", "[A1RoleLevel].[FieldName6]", "FieldName6", DataType.String, Nothing, False, 0, True, 100, "Field Name 6"))
    oList.SelectStatement = "select"
    oList.FromClause = "([Role]  [A0Role]" + _
           "    join [RoleLevel]  [A1RoleLevel] on [A0Role].[RoleLevelCode] = [A1RoleLevel].[RoleLevelCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0Role")
    oList.Columns.Add(New ListColumn("RoleCode", "[A0Role].[RoleCode]", "RoleCode", DataType.String, Nothing, True, 1, True, 100, "Role Code"))
    oList.Columns.Add(New ListColumn("RoleDesc", "[A0Role].[RoleDesc]", "RoleDesc", DataType.String, Nothing, False, 0, True, 200, "Role Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "[Role]  [A0Role]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function DropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0Role")
    oList.Columns.Add(New ListColumn("RoleCode", "[A0Role].[RoleCode]", "RoleCode", DataType.String, Nothing, True, 1, True, 100, "Role Code"))
    oList.Columns.Add(New ListColumn("RoleDesc", "[A0Role].[RoleDesc]", "RoleDesc", DataType.String, Nothing, False, 0, True, 200, "Role Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "[Role]  [A0Role]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
