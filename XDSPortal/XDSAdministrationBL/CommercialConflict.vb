Imports sembleWare.Runtime
Imports System
Public Class CommercialConflict 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly Commercial As Relationship = New CommercialRelationship("Commercial", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly ConflictCommercial As Relationship = New CommercialRelationship("ConflictCommercial", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _CommercialID As New IntegerElement("CommercialID", Me, True)
  Public ReadOnly _ConflictCommercialID As New IntegerElement("ConflictCommercialID", Me, True)
  Public ReadOnly _Commercial As CommercialRelationship = Commercial
  Public ReadOnly _ConflictCommercial As CommercialRelationship = ConflictCommercial
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("CommercialConflict", ApplicationSettings)
    _Commercial._CommercialID.LocalElement = _CommercialID
    _ConflictCommercial._CommercialID.LocalElement = _ConflictCommercialID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class CommercialConflictRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _CommercialID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _ConflictCommercialID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New CommercialConflict(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _CommercialID.ForeignElement = CType(Instance, CommercialConflict)._CommercialID
    _ConflictCommercialID.ForeignElement = CType(Instance, CommercialConflict)._ConflictCommercialID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0CommercialConflict")
    oList.Columns.Add(New ListColumn("CommercialID", "[A0CommercialConflict].[CommercialID]", "CommercialID", DataType.Integer, Nothing, True, 0, False, 100, "Commercial ID"))
    oList.Columns.Add(New ListColumn("ConflictCommercialID", "[A0CommercialConflict].[ConflictCommercialID]", "ConflictCommercialID", DataType.Integer, Nothing, True, 0, False, 100, "Conflict Commercial ID"))
    oList.Columns.Add(New ListColumn("CommercialID1", "[A1ConflictCommercial].[CommercialID]", "CommercialID", DataType.Long, Nothing, False, 1, True, 100, "Commercial ID"))
    oList.Columns.Add(New ListColumn("RegistrationNo", "[A1ConflictCommercial].[RegistrationNo]", "RegistrationNo", DataType.String, Nothing, False, 0, True, 100, "Registration No"))
    oList.Columns.Add(New ListColumn("CommercialName", "[A1ConflictCommercial].[CommercialName]", "CommercialName", DataType.String, Nothing, False, 0, True, 200, "Commercial Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "([CommercialConflict]  [A0CommercialConflict]" + _
           "    join [Commercial]  [A1ConflictCommercial] on [A0CommercialConflict].[ConflictCommercialID] = [A1ConflictCommercial].[CommercialID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
