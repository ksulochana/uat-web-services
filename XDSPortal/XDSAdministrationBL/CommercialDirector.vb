Imports sembleWare.Runtime
Imports System
Public Class CommercialDirector 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly CommercialDirectorID As New IdentityElement("CommercialDirectorID", Me, True, Nothing, Nothing)
  Public ReadOnly AppointmentDate As New DateTimeElement("AppointmentDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly DirectorStatusDate As New DateTimeElement("DirectorStatusDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly IsRSAResidentYN As New BooleanElement("IsRSAResidentYN", Me, False, True, True, True, True, "Is RSA Resident?", Nothing, False, "Yes;No")
  Public ReadOnly IsWithdrawnFromPublicYN As New BooleanElement("IsWithdrawnFromPublicYN", Me, False, True, True, True, True, "Is Withdrawn From Public?", Nothing, False, "Yes;No")
  Public ReadOnly Executor As New StringElement("Executor", Me, False, True, True, True, False, 300, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ExecutorAppointmentDate As New DateTimeElement("ExecutorAppointmentDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly RegisterNo As New StringElement("RegisterNo", Me, False, True, True, True, False, 14, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly TrusteeOf As New StringElement("TrusteeOf", Me, False, True, True, True, False, 200, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CM29Date As New DateTimeElement("CM29Date", Me, False, True, True, True, False, "CM 29 Date", Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly ReceiveDate As New DateTimeElement("ReceiveDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly CK12Date As New DateTimeElement("CK12Date", Me, False, True, True, True, False, "CK 1 2 Date", Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly MemberSize As New IntegerElement("MemberSize", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly MemberControlPerc As New DecimalElement("MemberControlPerc", Me, False, True, True, True, False, "Member Control %", Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly MemberControlType As New StringElement("MemberControlType", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FineExpiryDate As New DateTimeElement("FineExpiryDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly NatureOfChange As New StringElement("NatureOfChange", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly DirectorSetDate As New DateTimeElement("DirectorSetDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly Profession As New StringElement("Profession", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Estate As New StringElement("Estate", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly OccupationCode As New StringElement("OccupationCode", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly NatCode As New StringElement("NatCode", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RecordStatusInd As New IndicatorElement("RecordStatusInd", Me, False, False, False, True, True, True, "Record Status", Nothing, CommercialDirector.RecordStatusIndOptions, "A")
  Public ReadOnly DeletedReason As New StringElement("DeletedReason", Me, False, True, True, True, False, 500, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LastUpdatedDate As New DateTimeElement("LastUpdatedDate", Me, False, False, True, True, False, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly Loader As Relationship = New LoaderRelationship("Loader", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly DirectorDesignation As Relationship = New DirectorDesignationRelationship("DirectorDesignation", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly DirectorStatus As Relationship = New DirectorStatusRelationship("DirectorStatus", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly DirectorType As Relationship = New DirectorTypeRelationship("DirectorType", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly Director As Relationship = New DirectorRelationship("Director", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly Commercial As Relationship = New CommercialRelationship("Commercial", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _LoaderID As New IntegerElement("LoaderID", Me, False)
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, False)
  Public ReadOnly _DirectorDesignationCode As New StringElement("DirectorDesignationCode", Me, False)
  Public ReadOnly _DirectorStatusCode As New StringElement("DirectorStatusCode", Me, False)
  Public ReadOnly _DirectorTypeCode As New StringElement("DirectorTypeCode", Me, False)
  Public ReadOnly _DirectorID As New IntegerElement("DirectorID", Me, False)
  Public ReadOnly _CommercialID As New IntegerElement("CommercialID", Me, True)
  Public ReadOnly _Loader As LoaderRelationship = Loader
  Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
  Public ReadOnly _DirectorDesignation As DirectorDesignationRelationship = DirectorDesignation
  Public ReadOnly _DirectorStatus As DirectorStatusRelationship = DirectorStatus
  Public ReadOnly _DirectorType As DirectorTypeRelationship = DirectorType
  Public ReadOnly _Director As DirectorRelationship = Director
  Public ReadOnly _Commercial As CommercialRelationship = Commercial
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moRecordStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property RecordStatusIndOptions() As IndicatorOptions
    Get
      If moRecordStatusIndOptions Is Nothing Then
        moRecordStatusIndOptions = New IndicatorOptions
        moRecordStatusIndOptions.Add("A", "Active")
        moRecordStatusIndOptions.Add("D", "Deleted")
      End If
      Return moRecordStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("CommercialDirector", ApplicationSettings)
    _Loader._LoaderID.LocalElement = _LoaderID
    _Subscriber._SubscriberID.LocalElement = _SubscriberID
    _DirectorDesignation._DirectorDesignationCode.LocalElement = _DirectorDesignationCode
    _DirectorStatus._DirectorStatusCode.LocalElement = _DirectorStatusCode
    _DirectorType._DirectorTypeCode.LocalElement = _DirectorTypeCode
    _Director._DirectorID.LocalElement = _DirectorID
    _Commercial._CommercialID.LocalElement = _CommercialID
    'sembleWare: Constructor End - Do Not Modify
    Me.DeleteType = CustomDBPart.enumDeleteType.ChangeRecordStatus
  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class CommercialDirectorRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _CommercialDirectorID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _CommercialID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New CommercialDirector(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _CommercialDirectorID.ForeignElement = CType(Instance, CommercialDirector).CommercialDirectorID
    _CommercialID.ForeignElement = CType(Instance, CommercialDirector)._CommercialID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0CommercialDirector")
    oList.Columns.Add(New ListColumn("CommercialID", "[A0CommercialDirector].[CommercialID]", "CommercialID", DataType.Integer, Nothing, True, 0, False, 100, "Commercial ID"))
    oList.Columns.Add(New ListColumn("CommercialDirectorID", "[A0CommercialDirector].[CommercialDirectorID]", "CommercialDirectorID", DataType.Long, Nothing, True, 0, False, 100, "Commercial Director ID"))
    oList.Columns.Add(New ListColumn("LastUpdatedDate", "[A0CommercialDirector].[LastUpdatedDate]", "LastUpdatedDate", DataType.DateTime, "g", False, -1, True, 100, "Last Updated Date"))
    oList.Columns.Add(New ListColumn("IDNo", "[A2Director].[IDNo]", "IDNo", DataType.String, Nothing, False, 2, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("Surname", "[A2Director].[Surname]", "Surname", DataType.String, Nothing, False, 0, True, 100, "Surname"))
    oList.Columns.Add(New ListColumn("FirstName", "[A2Director].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("FirstInitial", "[A2Director].[FirstInitial]", "FirstInitial", DataType.String, Nothing, False, 0, True, 100, "First Initial"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A2Director].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Birth Date"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A1Subscriber].[SubscriberID]", "SubscriberID", DataType.Long, Nothing, False, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberName", "[A1Subscriber].[SubscriberName]", "SubscriberName", DataType.String, Nothing, False, 0, True, 200, "Subscriber Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "(([CommercialDirector]  [A0CommercialDirector]" + _
           "    left join [Subscriber]  [A1Subscriber] on [A0CommercialDirector].[SubscriberID] = [A1Subscriber].[SubscriberID])" + _
           "    left join [Director]  [A2Director] on [A0CommercialDirector].[DirectorID] = [A2Director].[DirectorID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function CommercialGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0CommercialDirector")
    oList.Columns.Add(New ListColumn("CommercialDirectorID", "[A0CommercialDirector].[CommercialDirectorID]", "CommercialDirectorID", DataType.Long, Nothing, True, 0, False, 100, "Commercial Director ID"))
    oList.Columns.Add(New ListColumn("CommercialID", "[A0CommercialDirector].[CommercialID]", "CommercialID", DataType.Integer, Nothing, True, 0, False, 100, "Commercial ID"))
    oList.Columns.Add(New ListColumn("CommercialID1", "[A1Commercial].[CommercialID]", "CommercialID", DataType.Long, Nothing, False, 0, False, 100, "Commercial ID"))
    oList.Columns.Add(New ListColumn("RegistrationNo", "[A1Commercial].[RegistrationNo]", "RegistrationNo", DataType.String, Nothing, False, 0, True, 100, "Registration No"))
    oList.Columns.Add(New ListColumn("CommercialName", "[A1Commercial].[CommercialName]", "CommercialName", DataType.String, Nothing, False, 0, True, 200, "Business Name"))
    oList.Columns.Add(New ListColumn("CommercialStatusDesc", "[A2CommercialStatus].[CommercialStatusDesc]", "CommercialStatusDesc", DataType.String, Nothing, False, 0, True, 200, "Business Status Description"))
    oList.Columns.Add(New ListColumn("MemberControlPerc", "[A0CommercialDirector].[MemberControlPerc]", "MemberControlPerc", DataType.Decimal, Nothing, False, 0, True, 100, "Member Control %"))
    oList.SelectStatement = "select"
    oList.FromClause = "([CommercialDirector]  [A0CommercialDirector]" + _
           "    join ([Commercial]  [A1Commercial]" + _
           "    left join [CommercialStatus]  [A2CommercialStatus] on [A1Commercial].[CommercialStatusCode] = [A2CommercialStatus].[CommercialStatusCode]) on [A0CommercialDirector].[CommercialID] = [A1Commercial].[CommercialID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

Public Function CommercialGridList1() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim strSubQuery = "case when right([A1Commercial].[RegistrationNo],2) =  '06' " + _
	                    "then  [A1Commercial].[CommercialName] + ' LTD'  " + _
	                    "when right([A1Commercial].[RegistrationNo],2) =  '07'  " + _ 
	                    "then  [A1Commercial].[CommercialName] + ' (PTY)LTD'  " + _
	                    "when right([A1Commercial].[RegistrationNo],2) =  '23'  " + _
	                    "then  [A1Commercial].[CommercialName] + ' CC'  " + _
	                    "when right([A1Commercial].[RegistrationNo],2) =  '21'  " + _
	                    "then  [A1Commercial].[CommercialName] + ' INC'  " + _
	                    "else [A1Commercial].[CommercialName] end"
    Dim oList As DBList = New DBList(Me, "A0CommercialDirector")
    oList.Columns.Add(New ListColumn("CommercialDirectorID", "[A0CommercialDirector].[CommercialDirectorID]", "CommercialDirectorID", DataType.Long, Nothing, True, 0, False, 100, "Commercial Director ID"))
    oList.Columns.Add(New ListColumn("CommercialID", "[A0CommercialDirector].[CommercialID]", "CommercialID", DataType.Integer, Nothing, True, 0, False, 100, "Commercial ID"))
    oList.Columns.Add(New ListColumn("CommercialID1", "[A1Commercial].[CommercialID]", "CommercialID", DataType.Long, Nothing, False, 0, False, 100, "Commercial ID"))
    oList.Columns.Add(New ListColumn("RegistrationNo", "[A1Commercial].[RegistrationNo]", "RegistrationNo", DataType.String, Nothing, False, 0, True, 100, "Registration No"))
    oList.Columns.Add(New ListColumn("CommercialName", strSubQuery, "CommercialName", DataType.String, Nothing, False, 0, True, 200, "Business Name"))
    oList.Columns.Add(New ListColumn("CommercialStatusDesc", "[A2CommercialStatus].[CommercialStatusDesc]", "CommercialStatusDesc", DataType.String, Nothing, False, 0, True, 200, "Business Status Description"))
    oList.Columns.Add(New ListColumn("MemberControlPerc", "[A0CommercialDirector].[MemberControlPerc]", "MemberControlPerc", DataType.Decimal, Nothing, False, 0, True, 100, "Member Control %"))
    oList.SelectStatement = "select"
    oList.FromClause = "([CommercialDirector]  [A0CommercialDirector]" + _
           "    join ([Commercial]  [A1Commercial]" + _
           "    left join [CommercialStatus]  [A2CommercialStatus] on [A1Commercial].[CommercialStatusCode] = [A2CommercialStatus].[CommercialStatusCode]) on [A0CommercialDirector].[CommercialID] = [A1Commercial].[CommercialID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

  Public Function DirectorGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0CommercialDirector")
    oList.Columns.Add(New ListColumn("CommercialDirectorID", "[A0CommercialDirector].[CommercialDirectorID]", "CommercialDirectorID", DataType.Long, Nothing, True, 0, False, 100, "Commercial Director ID"))
    oList.Columns.Add(New ListColumn("CommercialID", "[A0CommercialDirector].[CommercialID]", "CommercialID", DataType.Integer, Nothing, True, 0, False, 100, "Commercial ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A1Director].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("Surname", "[A1Director].[Surname]", "Surname", DataType.String, Nothing, False, 0, True, 100, "Surname"))
    oList.Columns.Add(New ListColumn("FirstName", "[A1Director].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("FirstInitial", "[A1Director].[FirstInitial]", "FirstInitial", DataType.String, Nothing, False, 0, True, 100, "First Initial"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A1Director].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Birth Date"))
    oList.SelectStatement = "select"
    oList.FromClause = "([CommercialDirector]  [A0CommercialDirector]" + _
           "    left join [Director]  [A1Director] on [A0CommercialDirector].[DirectorID] = [A1Director].[DirectorID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
    End Function


    Public Function DirectorGridList1() As sembleWare.Runtime.List
        'sembleWare: List Declaration Start - Do Not Modify
        Dim strDisbQuery = "(select count(IDNo) from ConsumerNameSoundEx where " + _
                      "(IDNo = substring([A1Director].[IDNo],0,11) and Surname = [A1Director].[Surname]) " + _
                      "or (IDNo = substring([A1Director].[IDNo],0,11) and FirstInitial = [A1Director].[FirstInitial]) " + _
                      "or (Surname = [A1Director].[Surname] and FirstInitial = [A1Director].[FirstInitial] and BirthDate = [A1Director].[BirthDate]) ) "

        Dim oList As DBList = New DBList(Me, "A0CommercialDirector")
        oList.Columns.Add(New ListColumn("CommercialDirectorID", "[A0CommercialDirector].[CommercialDirectorID]", "CommercialDirectorID", DataType.Long, Nothing, True, 0, False, 100, "Commercial Director ID"))
        oList.Columns.Add(New ListColumn("CommercialID", "[A0CommercialDirector].[CommercialID]", "CommercialID", DataType.Integer, Nothing, True, 0, False, 100, "Commercial ID"))
        oList.Columns.Add(New ListColumn("IDNo", "[A1Director].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
        oList.Columns.Add(New ListColumn("Surname", "[A1Director].[Surname]", "Surname", DataType.String, Nothing, False, 0, True, 100, "Surname"))
        oList.Columns.Add(New ListColumn("FirstName", "[A1Director].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
        oList.Columns.Add(New ListColumn("FirstInitial", "[A1Director].[FirstInitial]", "FirstInitial", DataType.String, Nothing, False, 0, True, 100, "First Initial"))
        oList.Columns.Add(New ListColumn("BirthDate", "[A1Director].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Birth Date"))
        oList.Columns.Add(New ListColumn("DisbFlag", strDisbQuery, "DisbFlag", DataType.Integer, Nothing, False, 0, True, 100, "DisbFlag"))
        oList.SelectStatement = "select distinct"
        oList.FromClause = "([CommercialDirector]  [A0CommercialDirector]" + _
               "    left join [Director]  [A1Director] on [A0CommercialDirector].[DirectorID] = [A1Director].[DirectorID])"
        oList.WhereClause = ""
        oList.ApplyRelationshipLimits()
        'sembleWare: List Declaration End - Do Not Modify
        Return oList
    End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
