Imports sembleWare.Runtime
Imports System
Public Class ConsumerAddress 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly ConsumerAddressID As New IdentityElement("ConsumerAddressID", Me, True, Nothing, Nothing)
  Public ReadOnly AddressTypeInd As New IndicatorElement("AddressTypeInd", Me, False, False, True, True, True, True, "Address Type", Nothing, ConsumerAddress.AddressTypeIndOptions, Nothing)
  Public ReadOnly OriginalAddress1 As New StringElement("OriginalAddress1", Me, False, True, True, True, False, 100, "Line 1", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly OriginalAddress2 As New StringElement("OriginalAddress2", Me, False, True, True, True, False, 100, "Line 2", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly OriginalAddress3 As New StringElement("OriginalAddress3", Me, False, True, True, True, False, 100, "Line 3", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly OriginalAddress4 As New StringElement("OriginalAddress4", Me, False, True, True, True, False, 100, "Line 4", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly OriginalPostalCode As New StringElement("OriginalPostalCode", Me, False, True, True, True, False, 10, "Postal Code", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CleanAddressValidTypeInd As New IndicatorElement("CleanAddressValidTypeInd", Me, False, False, True, True, True, False, "Clean Address Valid Type", Nothing, ConsumerAddress.CleanAddressValidTypeIndOptions, Nothing)
  Public ReadOnly CleanAddress1 As New StringElement("CleanAddress1", Me, False, False, True, True, False, 100, "Line 1", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CleanAddress2 As New StringElement("CleanAddress2", Me, False, False, True, True, False, 100, "Line 2", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CleanAddress3 As New StringElement("CleanAddress3", Me, False, False, True, True, False, 100, "Line 3", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CleanAddress4 As New StringElement("CleanAddress4", Me, False, False, True, True, False, 100, "Line 4", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CleanPostalCode As New StringElement("CleanPostalCode", Me, False, False, True, True, False, 10, "Postal Code", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CleanAddressDupCode As New StringElement("CleanAddressDupCode", Me, False, True, True, True, False, 200, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly OccupantTypeInd As New IndicatorElement("OccupantTypeInd", Me, False, False, True, True, True, False, "Occupant Type", Nothing, ConsumerAddress.OccupantTypeIndOptions, Nothing)
  Public ReadOnly RecordStatusInd As New IndicatorElement("RecordStatusInd", Me, False, False, False, True, True, True, "Record Status", Nothing, ConsumerAddress.RecordStatusIndOptions, "A")
  Public ReadOnly DeletedReason As New StringElement("DeletedReason", Me, False, True, True, True, False, 500, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LastUpdatedDate As New DateTimeElement("LastUpdatedDate", Me, False, False, True, True, True, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly IsVerifiedYN As New BooleanElement("IsVerifiedYN", Me, False, True, True, True, True, "Is Verified?", Nothing, False, "Yes;No")
  Public ReadOnly VerifiedDate As New DateTimeElement("VerifiedDate", Me, False, False, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Loader As Relationship = New LoaderRelationship("Loader", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Consumer As Relationship = New ConsumerRelationship("Consumer", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, False)
  Public ReadOnly _LoaderID As New IntegerElement("LoaderID", Me, False)
  Public ReadOnly _ConsumerID As New IntegerElement("ConsumerID", Me, True)
  Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
  Public ReadOnly _Loader As LoaderRelationship = Loader
  Public ReadOnly _Consumer As ConsumerRelationship = Consumer
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moAddressTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property AddressTypeIndOptions() As IndicatorOptions
    Get
      If moAddressTypeIndOptions Is Nothing Then
        moAddressTypeIndOptions = New IndicatorOptions
        moAddressTypeIndOptions.Add("R", "Residential")
        moAddressTypeIndOptions.Add("P", "Postal")
        moAddressTypeIndOptions.Add("W", "Work")
      End If
      Return moAddressTypeIndOptions
    End Get
  End Property
  Private Shared moCleanAddressValidTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property CleanAddressValidTypeIndOptions() As IndicatorOptions
    Get
      If moCleanAddressValidTypeIndOptions Is Nothing Then
        moCleanAddressValidTypeIndOptions = New IndicatorOptions
        moCleanAddressValidTypeIndOptions.Add("S", "Suspect")
        moCleanAddressValidTypeIndOptions.Add("Y", "Valid")
        moCleanAddressValidTypeIndOptions.Add("N", "Invalid")
      End If
      Return moCleanAddressValidTypeIndOptions
    End Get
  End Property
  Private Shared moOccupantTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property OccupantTypeIndOptions() As IndicatorOptions
    Get
      If moOccupantTypeIndOptions Is Nothing Then
        moOccupantTypeIndOptions = New IndicatorOptions
        moOccupantTypeIndOptions.Add("O", "Owner")
        moOccupantTypeIndOptions.Add("T", "Tenant")
      End If
      Return moOccupantTypeIndOptions
    End Get
  End Property
  Private Shared moRecordStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property RecordStatusIndOptions() As IndicatorOptions
    Get
      If moRecordStatusIndOptions Is Nothing Then
        moRecordStatusIndOptions = New IndicatorOptions
        moRecordStatusIndOptions.Add("A", "Active")
        moRecordStatusIndOptions.Add("D", "Deleted")
      End If
      Return moRecordStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("ConsumerAddress", ApplicationSettings)
    _Subscriber._SubscriberID.LocalElement = _SubscriberID
    _Loader._LoaderID.LocalElement = _LoaderID
    _Consumer._ConsumerID.LocalElement = _ConsumerID
    AddHandler IsVerifiedYN.Changed, AddressOf IsVerifiedYN_Changed
    'sembleWare: Constructor End - Do Not Modify
    Me.DeleteType = CustomDBPart.enumDeleteType.ChangeRecordStatus
  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub IsVerifiedYN_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BRIsVerifiedYN(Element, True, True)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Save()
    With ApplicationSettings
      .BeginTransaction()
      Try
        Dim bIsNewYN As Boolean = Me.IsNew
        BRBeforeSave(bIsNewYN)
        MyBase.Save()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
      .CommitTransaction()
    End With
  End Sub
#End Region

#Region " Business Rules"
  Public Sub BRBeforeSave(ByVal IsNewYN As Boolean)
    Me.LastUpdatedDate.Value = System.DateTime.Now

    Me.CleanAddress1.Value = Me.OriginalAddress1.Value
    Me.CleanAddress2.Value = Me.OriginalAddress2.Value
    Me.CleanAddress3.Value = Me.OriginalAddress3.Value
    Me.CleanAddress4.Value = Me.OriginalAddress4.Value
    Me.CleanAddressDupCode.Value = ConsumerAddress.CalculateDupCode(Me.OriginalAddress1.Value, Me.OriginalAddress2.Value, Me.OriginalPostalCode.Value)
    Return

    Dim sCleanAddress1 As String = ""
    Dim sCleanAddress2 As String = ""
    Dim sCleanAddress3 As String = ""
    Dim sCleanAddress4 As String = ""
    Dim sCleanPostalCode As String = ""

    ConsumerAddress.CleanAddressDetails(Me.OriginalAddress1.Value, Me.OriginalAddress2.Value, Me.OriginalAddress3.Value, Me.OriginalAddress4.Value, Me.OriginalPostalCode.Value, _
                                        sCleanAddress1, sCleanAddress2, sCleanAddress3, sCleanAddress4, sCleanPostalCode, ApplicationSettings)
    Me.CleanAddress1.Value = sCleanAddress1
    Me.CleanAddress2.Value = sCleanAddress2
    Me.CleanAddress3.Value = sCleanAddress3
    Me.CleanAddress4.Value = sCleanAddress4
    Me.CleanPostalCode.Value = sCleanPostalCode
  End Sub

  ' NEW ETL Replacement Method
  'Public Sub BRBeforeSave2(ByVal IsNewYN As Boolean)
  '  Dim sCleanAddressValidTypeInd As String = ""
  '  Dim sCleanAddress1 As String = ""
  '  Dim sCleanAddress2 As String = ""
  '  Dim sCleanAddress3 As String = ""
  '  Dim sCleanAddress4 As String = ""
  '  Dim sCleanPostalCode As String = ""
  '  Dim sCleanAddressDupCode As String = ""

  '  ConsumerAddress.CleanAddressDetails(Me.OriginalAddress1.Value, Me.OriginalAddress2.Value, Me.OriginalAddress3.Value, Me.OriginalAddress4.Value, Me.OriginalPostalCode.Value, _
  '                                      sCleanAddressValidTypeInd, sCleanAddress1, sCleanAddress2, sCleanAddress3, sCleanAddress4, sCleanPostalCode, sCleanAddressDupCode, ApplicationSettings)
  '  Me.CleanAddressValidTypeInd.Value = sCleanAddressValidTypeInd
  '  Me.CleanAddress1.Value = sCleanAddress1
  '  Me.CleanAddress2.Value = sCleanAddress2
  '  Me.CleanAddress3.Value = sCleanAddress3
  '  Me.CleanAddress4.Value = sCleanAddress4
  '  Me.CleanPostalCode.Value = sCleanPostalCode
  '  Me.CleanAddressDupCode.Value = sCleanAddressDupCode

  '  Me.LastUpdatedDate.Value = System.DateTime.Now
  'End Sub

  Private Sub BRIsVerifiedYN(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If PerformRulesYN Then
      If Element.ValueAsObject Then
        Me.VerifiedDate.Value = System.DateTime.Now
      Else
        Me.VerifiedDate.SetToNullValue()
      End If
    End If
  End Sub
#End Region

#Region " Shared Procedures"
  Public Shared Sub CleanAddressDetails(ByVal OriginalAddress1 As String, ByVal OriginalAddress2 As String, ByVal OriginalAddress3 As String, ByVal OriginalAddress4 As String, ByVal OriginalPostalCode As String, _
                                        ByRef CleanAddress1 As String, ByRef CleanAddress2 As String, ByRef CleanAddress3 As String, ByRef CleanAddress4 As String, ByRef CleanPostalCode As String, _
                                        ByVal ApplicationSettings As ApplicationSettings)
    Dim sQuery As String = "exec master..xp_CleanAdr1 "

    sQuery &= OriginalAddress1 & "," & OriginalAddress2 & "," & OriginalAddress3 & "," & OriginalAddress4 & "," & OriginalPostalCode & "'"

    Dim oDataTable As DataTable = ApplicationSettings.ResultQuery(sQuery)
    Dim oDataRow As DataRow

    For Each oDataRow In oDataTable.Rows
      CleanAddress1 = oDataRow("Address1").ToString()
      CleanAddress2 = oDataRow("Address2").ToString()
      CleanAddress3 = oDataRow("Address3").ToString()
      CleanAddress4 = oDataRow("Address4").ToString()
      CleanPostalCode = oDataRow("PostalCode").ToString()
      Exit For
    Next
  End Sub

  Public Shared Function CalculateDupCode(ByVal Address1 As String, ByVal Address2 As String, ByVal PostalCode As String) As String
        'Dim oclsDedupeClass As Dedupe.clsDedupeClass = New Dedupe.clsDedupeClass

        'Return oclsDedupeClass.Dedupe(Address1, Address2, PostalCode)
  End Function

  ' NEW ETL REPLACEMENT METHODS
  'Public Shared Sub CleanAddressDetails(ByVal OriginalAddress1 As String, ByVal OriginalAddress2 As String, ByVal OriginalAddress3 As String, ByVal OriginalAddress4 As String, ByVal OriginalPostalCode As String, _
  '                                      ByRef CleanAddressValidTypeInd As String, ByRef CleanAddress1 As String, ByRef CleanAddress2 As String, ByRef CleanAddress3 As String, ByRef CleanAddress4 As String, _
  '                                      ByRef CleanPostalCode As String, ByRef CleanAddressDupCode As String, ByVal ApplicationSettings As ApplicationSettings)
  '  Dim sQuery As String = "exec master..xp_CleanAdr1 "

  '  sQuery &= "'1," & OriginalAddress1 & "," & OriginalAddress2 & "," & OriginalAddress3 & "," & OriginalAddress4 & "," & OriginalPostalCode & "'"

  '  Dim oDataTable As DataTable = ApplicationSettings.ResultQuery(sQuery)
  '  Dim oDataRow As DataRow

  '  For Each oDataRow In oDataTable.Rows
  '    CleanAddressValidTypeInd = oDataRow("IsValidAddress").ToString()
  '    CleanAddress1 = oDataRow("Address1").ToString()
  '    CleanAddress2 = oDataRow("Address2").ToString()
  '    CleanAddress3 = oDataRow("Address3").ToString()
  '    CleanAddress4 = oDataRow("Address4").ToString()
  '    CleanPostalCode = oDataRow("PostalCode").ToString()
  '    CleanAddressDupCode = oDataRow("DupeCode").ToString()
  '    Exit For
  '  Next
  'End Sub

  'Public Shared Function CalculateDupCode(ByVal Address1 As String, ByVal Address2 As String, ByVal PostalCode As String, ByVal ApplicationSettings As ApplicationSettings) As String
  '  Dim sQuery As String = "exec master..xp_CleanAdr1 "

  '  sQuery &= "'1," & Address1 & "," & Address2 & "," & PostalCode & "'"

  '  Dim oDataTable As DataTable = ApplicationSettings.ResultQuery(sQuery)
  '  Dim oDataRow As DataRow

  '  For Each oDataRow In oDataTable.Rows
  '    Return oDataRow("DupeCode").ToString()
  '  Next

  '  Return ""
  'End Function
#End Region
End Class 'sembleWare: Part

Public Class ConsumerAddressRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _ConsumerAddressID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _ConsumerID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New ConsumerAddress(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _ConsumerAddressID.ForeignElement = CType(Instance, ConsumerAddress).ConsumerAddressID
    _ConsumerID.ForeignElement = CType(Instance, ConsumerAddress)._ConsumerID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0ConsumerAddress")
    oList.Columns.Add(New ListColumn("ConsumerID", "[A0ConsumerAddress].[ConsumerID]", "ConsumerID", DataType.Integer, Nothing, True, 0, False, 100, "Consumer ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0ConsumerAddress].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, False, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("LastUpdatedDate", "[A0ConsumerAddress].[LastUpdatedDate]", "LastUpdatedDate", DataType.DateTime, "g", False, -1, True, 100, "Last Updated Date"))
    oList.Columns.Add(New ListColumn("AddressTypeInd", "[A0ConsumerAddress].[AddressTypeInd]", "AddressTypeInd", DataType.String, Nothing, False, 2, True, 100, "Address Type"))
    oList.Columns.Add(New ListColumn("OriginalAddress1", "[A0ConsumerAddress].[OriginalAddress1]", "OriginalAddress1", DataType.String, Nothing, False, 0, True, 100, "Line 1"))
    oList.Columns.Add(New ListColumn("OriginalAddress2", "[A0ConsumerAddress].[OriginalAddress2]", "OriginalAddress2", DataType.String, Nothing, False, 0, True, 100, "Line 2"))
    oList.Columns.Add(New ListColumn("OriginalAddress3", "[A0ConsumerAddress].[OriginalAddress3]", "OriginalAddress3", DataType.String, Nothing, False, 0, True, 100, "Line 3"))
    oList.Columns.Add(New ListColumn("OriginalAddress4", "[A0ConsumerAddress].[OriginalAddress4]", "OriginalAddress4", DataType.String, Nothing, False, 0, True, 100, "Line 4"))
    oList.Columns.Add(New ListColumn("OriginalPostalCode", "[A0ConsumerAddress].[OriginalPostalCode]", "OriginalPostalCode", DataType.String, Nothing, False, 0, True, 100, "Postal Code"))
    oList.Columns.Add(New ListColumn("OccupantTypeInd", "[A0ConsumerAddress].[OccupantTypeInd]", "OccupantTypeInd", DataType.String, Nothing, False, 0, True, 100, "Occupant Type"))
    oList.Columns.Add(New ListColumn("SubscriberID1", "[A1Subscriber].[SubscriberID]", "SubscriberID", DataType.Long, Nothing, False, 0, False, 75, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberName", "[A1Subscriber].[SubscriberName]", "SubscriberName", DataType.String, Nothing, False, 0, True, 150, "Subscriber Name"))
    oList.Columns.Add(New ListColumn("ConsumerAddressID", "[A0ConsumerAddress].[ConsumerAddressID]", "ConsumerAddressID", DataType.Long, Nothing, True, 0, False, 100, "Consumer Address ID"))
    oList.Columns.Add(New ListColumn("LoaderID", "[A0ConsumerAddress].[LoaderID]", "LoaderID", DataType.Integer, Nothing, False, 0, False, 100, "Loader ID"))
    oList.Columns.Add(New ListColumn("IsVerifiedYN", "[A0ConsumerAddress].[IsVerifiedYN]", "IsVerifiedYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Is Verified?"))
    oList.Columns.Add(New ListColumn("VerifiedDate", "[A0ConsumerAddress].[VerifiedDate]", "VerifiedDate", DataType.DateTime, "d", False, 0, True, 100, "Verified Date"))
    oList.SelectStatement = "select"
    oList.FromClause = "(([ConsumerAddress]  [A0ConsumerAddress]" + _
           "    left join [Subscriber]  [A1Subscriber] on [A0ConsumerAddress].[SubscriberID] = [A1Subscriber].[SubscriberID])" + _
           "    join [Consumer]  [A2Consumer] on [A0ConsumerAddress].[ConsumerID] = [A2Consumer].[ConsumerID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    oList.AndFilters.Add(New ListFilter("RecordStatusInd", "[A2Consumer].[RecordStatusInd]", Constants.RecordStatus.Active, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
    oList.AndFilters.Add(New ListFilter("RecordStatusInd1", "[A0ConsumerAddress].[RecordStatusInd]", Constants.RecordStatus.Active, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
