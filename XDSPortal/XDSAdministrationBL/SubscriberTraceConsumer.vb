Imports sembleWare.Runtime
Imports System
Public Class SubscriberTraceConsumer 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly SubscriberTraceConsumerID As New IdentityElement("SubscriberTraceConsumerID", Me, True, Nothing, Nothing)
  Public ReadOnly TraceDate As New DateTimeElement("TraceDate", Me, False, False, True, True, False, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly ReferenceNo As New StringElement("ReferenceNo", Me, False, False, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ExternalReference As New StringElement("ExternalReference", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly IncludePrioritizationRatingYN As New BooleanElement("IncludePrioritizationRatingYN", Me, False, False, True, True, True, "Include Prioritization Rating?", Nothing, False, "Yes;No")
  Public ReadOnly IncludePublicDomainYN As New BooleanElement("IncludePublicDomainYN", Me, False, False, True, True, True, "Include Public Domain?", Nothing, False, "Yes;No")
  Public ReadOnly ReportFile As New BinaryElement("ReportFile", Me, False, True, True, False, Nothing, Nothing)
  Public ReadOnly ReportFileMimeType As New StringElement("ReportFileMimeType", Me, False, False, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SubscriberConsumerEnquiry As Relationship = New SubscriberConsumerEnquiryRelationship("SubscriberConsumerEnquiry", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Product As Relationship = New ProductRelationship("Product", Nothing, RelationshipType.Include, True, False, True, Me)
  Public ReadOnly ActionedBySystemUser As Relationship = New SystemUserRelationship("ActionedBySystemUser", "Actioned By", RelationshipType.Include, True, False, True, Me)
  Public ReadOnly SubscriberHomeAffairsEnquiry As Relationship = New SubscriberHomeAffairsEnquiryRelationship("SubscriberHomeAffairsEnquiry", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly SubscriberBillingGroup As Relationship = New SubscriberBillingGroupRelationship("SubscriberBillingGroup", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly Consumer As Relationship = New ConsumerRelationship("Consumer", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly HomeAffairs As Relationship = New HomeAffairsRelationship("HomeAffairs", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly SubscriberTraceConsumerInfo_OwnMany As Relationship = New SubscriberTraceConsumerInfoRelationship("SubscriberTraceConsumerInfo", Nothing, "SubscriberTraceConsumer", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberConsumerEnquiryID As New IntegerElement("SubscriberConsumerEnquiryID", Me, False)
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _ProductID As New IntegerElement("ProductID", Me, False)
  Public ReadOnly _ActionedBySystemUserID As New IntegerElement("ActionedBySystemUserID", Me, False)
  Public ReadOnly _SubscriberHomeAffairsEnquiryID As New IntegerElement("SubscriberHomeAffairsEnquiryID", Me, False)
  Public ReadOnly _SubscriberBillingGroupCode As New StringElement("SubscriberBillingGroupCode", Me, False)
  Public ReadOnly _ConsumerID As New IntegerElement("ConsumerID", Me, False)
  Public ReadOnly _HomeAffairsID As New IntegerElement("HomeAffairsID", Me, False)
  Public ReadOnly _SubscriberConsumerEnquiry As SubscriberConsumerEnquiryRelationship = SubscriberConsumerEnquiry
  Public ReadOnly _Product As ProductRelationship = Product
  Public ReadOnly _ActionedBySystemUser As SystemUserRelationship = ActionedBySystemUser
  Public ReadOnly _SubscriberHomeAffairsEnquiry As SubscriberHomeAffairsEnquiryRelationship = SubscriberHomeAffairsEnquiry
  Public ReadOnly _SubscriberBillingGroup As SubscriberBillingGroupRelationship = SubscriberBillingGroup
  Public ReadOnly _Consumer As ConsumerRelationship = Consumer
  Public ReadOnly _HomeAffairs As HomeAffairsRelationship = HomeAffairs
  Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
  Public ReadOnly _SubscriberTraceConsumerInfo_OwnMany As SubscriberTraceConsumerInfoRelationship = SubscriberTraceConsumerInfo_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberTraceConsumer", ApplicationSettings)
    _SubscriberConsumerEnquiry._SubscriberConsumerEnquiryID.LocalElement = _SubscriberConsumerEnquiryID
    _SubscriberConsumerEnquiry._SubscriberID.LocalElement = _SubscriberID
    _Product._ProductID.LocalElement = _ProductID
    _ActionedBySystemUser._SystemUserID.LocalElement = _ActionedBySystemUserID
    _SubscriberHomeAffairsEnquiry._SubscriberHomeAffairsEnquiryID.LocalElement = _SubscriberHomeAffairsEnquiryID
    _SubscriberHomeAffairsEnquiry._SubscriberID.LocalElement = _SubscriberID
    _SubscriberBillingGroup._SubscriberBillingGroupCode.LocalElement = _SubscriberBillingGroupCode
    _Consumer._ConsumerID.LocalElement = _ConsumerID
    _HomeAffairs._HomeAffairsID.LocalElement = _HomeAffairsID
    _Subscriber._SubscriberID.LocalElement = _SubscriberID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Save()
    With ApplicationSettings
      Try
        .BeginTransaction()
        Dim bIsNewYN As Boolean = Me.IsNew

        BRBeforeSave(bIsNewYN)
        MyBase.Save()
        BRAfterSave(bIsNewYN)
        .CommitTransaction()
      Catch Exception As Exception
        .RollbackTransaction()
        Throw Exception
      End Try
    End With
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRBeforeSave(ByVal IsNewYN As Boolean)
    If IsNewYN Then
      Me.TraceDate.Value = System.DateTime.Now
      ' If ActionedBySystemUser is not empty, then the XDSAdministration Services has populated this value.
      If ActionedBySystemUser.IsEmpty Then
        Me.ActionedBySystemUser.Instance = SystemUser.GetLoggedInSystemUser(ApplicationSettings)
      End If
    Else
      BRGetReferenceNo()
    End If
  End Sub

  Private Sub BRAfterSave(ByVal IsNewYN As Boolean)
    If IsNewYN Then
      BRGetReferenceNo()
      Me.Save()
    End If
  End Sub

  Private Sub BRGetReferenceNo()
    Select Case Me._ProductID.Value
      Case Constants.Product.Records.TraceConsumerIdentityVerificationWebService, Constants.Product.Records.TraceConsumerIdentityVerificationWebService
        Me.ReferenceNo.Value = "T" & Me.SubscriberTraceConsumerID.Value.ToString() & "-" & Me._HomeAffairsID.Value.ToString()
      Case Else
        Me.ReferenceNo.Value = "T" & Me.SubscriberTraceConsumerID.Value.ToString() & "-" & Me._ConsumerID.Value.ToString()
    End Select
  End Sub

  Private Sub BRPerformHomeAffairsMatch()
    If Not Me.Consumer.IsEmpty And Me.HomeAffairs.IsEmpty Then
      Dim oConsumer As Consumer = Me.Consumer.Instance

      If Not oConsumer.IDNo.ValueAsObject = "" Then
        Dim oSubscriber As Subscriber = Me.Subscriber.Instance
        Dim oSubscriberHomeAffairsEnquiry As SubscriberHomeAffairsEnquiry = oSubscriber.SubscriberHomeAffairsEnquiry_OwnMany.NewInstance

        oSubscriberHomeAffairsEnquiry.SystemUser.Instance = Me.ActionedBySystemUser.Instance
        oSubscriberHomeAffairsEnquiry.Product.Instance = XDSAdministrationBL.Product.GetProduct(Constants.Product.Records.TraceConsumerIdentityVerification, ApplicationSettings)
        oSubscriberHomeAffairsEnquiry.IDNo.ValueAsObject = oConsumer.IDNo.ValueAsObject
        oSubscriberHomeAffairsEnquiry.Surname.ValueAsObject = oConsumer.Surname.ValueAsObject
        oSubscriberHomeAffairsEnquiry.FirstName.ValueAsObject = oConsumer.FirstName.ValueAsObject
        oSubscriberHomeAffairsEnquiry.SecondName.ValueAsObject = oConsumer.SecondName.ValueAsObject
        oSubscriberHomeAffairsEnquiry.BirthDate.ValueAsObject = oConsumer.BirthDate.ValueAsObject
        oSubscriberHomeAffairsEnquiry.Match()

        Me.SubscriberHomeAffairsEnquiry.Instance = oSubscriberHomeAffairsEnquiry
        Select Case oSubscriberHomeAffairsEnquiry.EnquiryResultInd.Value
          Case Constants.SubscriberHomeAffairsEnquiry.Results.RecordFound
            Me.HomeAffairs.Instance = oSubscriberHomeAffairsEnquiry.GetSubscriberHomeAffairsEnquiryResult().HomeAffairs.Instance
        End Select
      End If
    End If
  End Sub

#End Region

#Region " Public Methods"
  Public Sub UpdateSubscriberConsumerEnquiry(ByVal SubscriberConsumerEnquiry As SubscriberConsumerEnquiry, ByVal IncludePrioritizationRatingYN As Boolean, ByVal IncludePublicDomainYN As Boolean)
    Select Case SubscriberConsumerEnquiry.EnquiryResultInd.Value
      Case Constants.SubscriberConsumerEnquiry.Results.RecordFound
        Dim oSystemUserProfile As SystemUserProfile = XDSAdministrationBL.SystemUserProfile.GetLoggedInSystemUserProfile(ApplicationSettings)

        If Not oSystemUserProfile.ConfirmPurchaseYN.Value Then
          Dim oSubscriberConsumerEnquiryResult As SubscriberConsumerEnquiryResult = SubscriberConsumerEnquiry.GetSubscriberConsumerEnquiryResult()
          oSubscriberConsumerEnquiryResult.SelectRecord()
          Me.Consumer.Instance = oSubscriberConsumerEnquiryResult.Consumer.Instance
          BRPerformHomeAffairsMatch()
        End If
    End Select

    Me.Product.Instance = SubscriberConsumerEnquiry.Product.Instance
    Me.SubscriberConsumerEnquiry.Instance = SubscriberConsumerEnquiry
    Me.IncludePrioritizationRatingYN.Value = IncludePrioritizationRatingYN
    Me.IncludePublicDomainYN.Value = IncludePublicDomainYN
    Me.Save()
  End Sub

  Public Sub UpdateSubscriberConsumerEnquiry(ByVal SubscriberConsumerEnquiryResult As SubscriberConsumerEnquiryResult, ByVal IncludePrioritizationRatingYN As Boolean, ByVal IncludePublicDomainYN As Boolean)
    Dim oSubscriberConsumerEnquiry As SubscriberConsumerEnquiry = SubscriberConsumerEnquiryResult.SubscriberConsumerEnquiry.Instance
    UpdateSubscriberConsumerEnquiry(oSubscriberConsumerEnquiry, IncludePrioritizationRatingYN, IncludePublicDomainYN)
    Me.Consumer.Instance = SubscriberConsumerEnquiryResult.Consumer.Instance
    BRPerformHomeAffairsMatch()
    Me.Save()
  End Sub

  Public Sub UpdateSubscriberHomeAffairsEnquiry(ByVal SubscriberHomeAffairsEnquiry As SubscriberHomeAffairsEnquiry, ByVal IncludePrioritizationRatingYN As Boolean, ByVal IncludePublicDomainYN As Boolean)
    Select Case SubscriberHomeAffairsEnquiry.EnquiryResultInd.Value
      Case Constants.SubscriberHomeAffairsEnquiry.Results.RecordFound
        Dim oSystemUserProfile As SystemUserProfile = XDSAdministrationBL.SystemUserProfile.GetLoggedInSystemUserProfile(ApplicationSettings)

        If Not oSystemUserProfile.ConfirmPurchaseYN.Value Then
          Dim oSubscriberHomeAffairsEnquiryResult As SubscriberHomeAffairsEnquiryResult = SubscriberHomeAffairsEnquiry.GetSubscriberHomeAffairsEnquiryResult()
          oSubscriberHomeAffairsEnquiryResult.SelectRecord()
          Me.HomeAffairs.Instance = oSubscriberHomeAffairsEnquiryResult.HomeAffairs.Instance
        End If
    End Select

    Me.Product.Instance = SubscriberHomeAffairsEnquiry.Product.Instance
    Me.SubscriberHomeAffairsEnquiry.Instance = SubscriberHomeAffairsEnquiry
    Me.IncludePrioritizationRatingYN.Value = IncludePrioritizationRatingYN
    Me.IncludePublicDomainYN.Value = IncludePublicDomainYN
    Me.Save()
  End Sub

  Public Sub UpdateSubscriberHomeAffairsEnquiry(ByVal SubscriberHomeAffairsEnquiryResult As SubscriberHomeAffairsEnquiryResult, ByVal IncludePrioritizationRatingYN As Boolean, ByVal IncludePublicDomainYN As Boolean)
    Dim oSubscriberHomeAffairsEnquiry As SubscriberHomeAffairsEnquiry = SubscriberHomeAffairsEnquiryResult.SubscriberHomeAffairsEnquiry.Instance
    UpdateSubscriberHomeAffairsEnquiry(oSubscriberHomeAffairsEnquiry, IncludePrioritizationRatingYN, IncludePublicDomainYN)
    Me.HomeAffairs.Instance = SubscriberHomeAffairsEnquiryResult.HomeAffairs.Instance
    Me.Save()
  End Sub

  Public Sub GenerateReport(ByVal ReportImageUrl As String, ByVal ReportImagePath As String)
    Me.Save()

    Dim sMimeType As String
    Dim oProduct As Product = XDSAdministrationBL.Product.GetProduct(Me._ProductID.Value, ApplicationSettings)

    If Not oProduct Is Nothing Then
      Utility.BRCheckProductReport(oProduct)

      Dim oReportingServicesReport As ReportingServicesReport = Utility.BRGetProductReportingServicesReport(SubscriberProfile.GetSubscriberProfile(Me._SubscriberID.Value, ApplicationSettings), oProduct)
      Dim oReportTraceDetail As ReportTraceDetail = New ReportTraceDetailRelationship(ApplicationSettings).NewInstance()

      oReportTraceDetail.ReportingServicesReportFormat.Value = SystemUserProfile.GetSystemUserProfile(Me._ActionedBySystemUserID.Value, ApplicationSettings).DefaultTraceEnquiryReportTypeInd.Value
      oReportTraceDetail.ReportingServicesReport.Instance = oReportingServicesReport
      oReportTraceDetail.ReportImageUrl.Value = ReportImageUrl
      oReportTraceDetail.ReportImagePath.Value = ReportImagePath
      oReportTraceDetail.SubscriberID.Value = Me._SubscriberID.Value
      oReportTraceDetail.SubscriberTraceConsumerID.Value = Me.SubscriberTraceConsumerID.Value
      With oReportTraceDetail.ReportParameterCollection
        .Add("SubscriberID", oReportTraceDetail.SubscriberID.Value, oReportTraceDetail.SubscriberID.ElementType)
        .Add("SubscriberTraceConsumerID", oReportTraceDetail.SubscriberTraceConsumerID.Value, oReportTraceDetail.SubscriberTraceConsumerID.ElementType)
      End With
      Me.ReportFile.Value = oReportTraceDetail.RunReport(sMimeType)
      Me.ReportFileMimeType.Value = sMimeType
    End If
    Me.Save()
  End Sub
#End Region

#Region " Shared Procedures"
  Public Shared Function GenerateConcatenatedConsumerHTMLReport(ByVal Subscriber As Subscriber, ByVal TraceIDArray As ArrayList, ByVal ApplicationSettings As ApplicationSettings) As Byte()
    Return GenerateConcatenatedHTMLReport(Subscriber._SubscriberTraceConsumer_OwnMany.MultipleConsumerGridList, Subscriber, TraceIDArray, ApplicationSettings)
  End Function

  Public Shared Function GenerateConcatenatedConsumerPDFReport(ByVal Subscriber As Subscriber, ByVal TraceIDArray As ArrayList, ByVal ApplicationSettings As ApplicationSettings) As Byte()
    Return GenerateConcatenatedPDFReport(Subscriber._SubscriberTraceConsumer_OwnMany.MultipleConsumerGridList, Subscriber, TraceIDArray, ApplicationSettings)
  End Function

  Public Shared Function GenerateConcatenatedHTMLReport(ByVal SubscriberTraceConsumerList As List, ByVal Subscriber As Subscriber, ByVal TraceIDArray As ArrayList, ByVal ApplicationSettings As ApplicationSettings) As Byte()
    If TraceIDArray.Count > 0 Then
      Dim sTraceIDString As String = ""
      Dim lTraceID As Long
      Dim oStreamArray As ArrayList = New ArrayList

      For Each lTraceID In TraceIDArray
        sTraceIDString &= lTraceID.ToString() & ", "
      Next
      If sTraceIDString.Length > 0 Then
        sTraceIDString = sTraceIDString.Substring(0, sTraceIDString.Length - 2)
      End If

      Dim oMemoryStream As IO.MemoryStream = New IO.MemoryStream
      Dim oDataRow As DataRow
      SubscriberTraceConsumerList.AndFilters.Add(New ListFilter("SubscriberTraceConsumerID", "SubscriberTraceConsumerID", sTraceIDString, DataType.Integer, FilterType.In, FilterBlankBehavior.DisplayEmptyList, True))
      For Each oDataRow In SubscriberTraceConsumerList.DataTable.Rows
        If Not IsDBNull(oDataRow("ReportFile")) Then
          Dim bByte() As Byte = oDataRow("ReportFile")

          oStreamArray.Add(New IO.MemoryStream(bByte))
          oMemoryStream.Write(bByte, 0, bByte.Length)
        End If
      Next

      Return oMemoryStream.GetBuffer()
    End If
    Return Nothing
  End Function

  Public Shared Function GenerateConcatenatedPDFReport(ByVal SubscriberTraceConsumerList As List, ByVal Subscriber As Subscriber, ByVal TraceIDArray As ArrayList, ByVal ApplicationSettings As ApplicationSettings) As Byte()
    If TraceIDArray.Count > 0 Then
      Dim sTraceIDString As String = ""
      Dim lTraceID As Long
      Dim oStreamArray As ArrayList = New ArrayList

      For Each lTraceID In TraceIDArray
        sTraceIDString &= lTraceID.ToString() & ", "
      Next
      If sTraceIDString.Length > 0 Then
        sTraceIDString = sTraceIDString.Substring(0, sTraceIDString.Length - 2)
      End If

      Dim oDataRow As DataRow
      SubscriberTraceConsumerList.AndFilters.Add(New ListFilter("SubscriberTraceConsumerID", "SubscriberTraceConsumerID", sTraceIDString, DataType.Integer, FilterType.In, FilterBlankBehavior.DisplayEmptyList, True))
      For Each oDataRow In SubscriberTraceConsumerList.DataTable.Rows
        If Not IsDBNull(oDataRow("ReportFile")) Then
          Dim bByte() As Byte = oDataRow("ReportFile")
          oStreamArray.Add(New IO.MemoryStream(bByte))
        End If
      Next

      If oStreamArray.Count > 0 Then
        Dim oMemoryStreams(oStreamArray.Count - 1) As IO.MemoryStream
        Dim oMemoryStream As IO.MemoryStream
        Dim nCounter As Integer = 0

        For Each oMemoryStream In oStreamArray
          oMemoryStreams(nCounter) = oMemoryStream
          nCounter += 1
        Next

        Dim oPDFManager As PDFManager.Manager = New PDFManager.Manager

        oMemoryStream = oPDFManager.Concatenate(oMemoryStreams)

        Return oMemoryStream.GetBuffer()
      End If
    End If
    Return Nothing
  End Function
#End Region
End Class 'sembleWare: Part

Public Class SubscriberTraceConsumerRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberTraceConsumerID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberTraceConsumer(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberTraceConsumerID.ForeignElement = CType(Instance, SubscriberTraceConsumer).SubscriberTraceConsumerID
    _SubscriberID.ForeignElement = CType(Instance, SubscriberTraceConsumer)._SubscriberID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function SubscriberGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberTraceConsumer")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberTraceConsumer].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberTraceConsumerID", "[A0SubscriberTraceConsumer].[SubscriberTraceConsumerID]", "SubscriberTraceConsumerID", DataType.Long, Nothing, True, 0, False, 100, "Subscriber Trace Consumer ID"))
    oList.Columns.Add(New ListColumn("ReferenceNo", "[A0SubscriberTraceConsumer].[ReferenceNo]", "ReferenceNo", DataType.String, Nothing, False, 0, True, 100, "Reference No"))
    oList.Columns.Add(New ListColumn("TraceDate", "[A0SubscriberTraceConsumer].[TraceDate]", "TraceDate", DataType.DateTime, "g", False, -1, True, 100, "Trace Date"))
    oList.Columns.Add(New ListColumn("ProductTypeDesc", "[A2ProductType].[ProductTypeDesc]", "ProductTypeDesc", DataType.String, Nothing, False, 0, False, 200, "Product Type Description"))
    oList.Columns.Add(New ListColumn("ProductDesc", "[A1Product].[ProductDesc]", "ProductDesc", DataType.String, Nothing, False, 0, True, 200, "Product Description"))
    oList.Columns.Add(New ListColumn("DefaultPointValue", "[A1Product].[DefaultPointValue]", "DefaultPointValue", DataType.Decimal, Nothing, False, 0, False, 100, "Default Point Value"))
    oList.Columns.Add(New ListColumn("ProductDetails", "[A1Product].[ProductDetails]", "ProductDetails", DataType.Text, Nothing, False, 0, False, 200, "Product Details"))
    oList.Columns.Add(New ListColumn("IncludePrioritizationRatingYN", "[A0SubscriberTraceConsumer].[IncludePrioritizationRatingYN]", "IncludePrioritizationRatingYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Include Prioritization Rating?"))
    oList.Columns.Add(New ListColumn("FullName", "isnull([A3ActionedBySystemUser].[FirstName], '') + ' ' + isnull([A3ActionedBySystemUser].[Surname], '')", "FullName", DataType.String, Nothing, False, 0, True, 200, "Actioned By"))
    oList.SelectStatement = "select"
    oList.FromClause = "(([SubscriberTraceConsumer]  [A0SubscriberTraceConsumer]" + _
           "    join ([Product]  [A1Product]" + _
           "    join [ProductType]  [A2ProductType] on [A1Product].[ProductTypeID] = [A2ProductType].[ProductTypeID]) on [A0SubscriberTraceConsumer].[ProductID] = [A1Product].[ProductID])" + _
           "    join [SystemUser]  [A3ActionedBySystemUser] on [A0SubscriberTraceConsumer].[ActionedBySystemUserID] = [A3ActionedBySystemUser].[SystemUserID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function MultipleConsumerGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberTraceConsumer")
    oList.Columns.Add(New ListColumn("SubscriberTraceConsumerID", "[A0SubscriberTraceConsumer].[SubscriberTraceConsumerID]", "SubscriberTraceConsumerID", DataType.Long, Nothing, True, 0, True, 100, "Subscriber Trace Consumer ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberTraceConsumer].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, True, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberConsumerEnquiryID", "[A1SubscriberConsumerEnquiry].[SubscriberConsumerEnquiryID]", "SubscriberConsumerEnquiryID", DataType.Long, Nothing, False, 0, True, 100, "Subscriber Consumer Enquiry ID"))
    oList.Columns.Add(New ListColumn("EnquiryDate", "[A1SubscriberConsumerEnquiry].[EnquiryDate]", "EnquiryDate", DataType.DateTime, "g", False, 0, True, 100, "Enquiry Date"))
    oList.Columns.Add(New ListColumn("EnquiryStatusInd", "[A1SubscriberConsumerEnquiry].[EnquiryStatusInd]", "EnquiryStatusInd", DataType.String, Nothing, False, 0, True, 100, "Enquiry Status"))
    oList.Columns.Add(New ListColumn("EnquiryResultInd", "[A1SubscriberConsumerEnquiry].[EnquiryResultInd]", "EnquiryResultInd", DataType.String, Nothing, False, 0, True, 100, "Enquiry Result"))
    oList.Columns.Add(New ListColumn("ProductPointValue", "[A1SubscriberConsumerEnquiry].[ProductPointValue]", "ProductPointValue", DataType.Decimal, Nothing, False, 0, True, 100, "Product Point Value"))
    oList.Columns.Add(New ListColumn("ExternalReference", "[A1SubscriberConsumerEnquiry].[ExternalReference]", "ExternalReference", DataType.String, Nothing, False, 0, True, 100, "External Reference"))
    oList.Columns.Add(New ListColumn("IDNo", "[A1SubscriberConsumerEnquiry].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("PassportNo", "[A1SubscriberConsumerEnquiry].[PassportNo]", "PassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No / Other ID No"))
    oList.Columns.Add(New ListColumn("Surname", "[A1SubscriberConsumerEnquiry].[Surname]", "Surname", DataType.String, Nothing, False, 0, True, 100, "Surname"))
    oList.Columns.Add(New ListColumn("MaidenName", "[A1SubscriberConsumerEnquiry].[MaidenName]", "MaidenName", DataType.String, Nothing, False, 0, True, 100, "Maiden Name"))
    oList.Columns.Add(New ListColumn("FirstInitial", "[A1SubscriberConsumerEnquiry].[FirstInitial]", "FirstInitial", DataType.String, Nothing, False, 0, True, 100, "First Initial"))
    oList.Columns.Add(New ListColumn("SecondInitial", "[A1SubscriberConsumerEnquiry].[SecondInitial]", "SecondInitial", DataType.String, Nothing, False, 0, True, 100, "Second Initial"))
    oList.Columns.Add(New ListColumn("FirstName", "[A1SubscriberConsumerEnquiry].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("SecondName", "[A1SubscriberConsumerEnquiry].[SecondName]", "SecondName", DataType.String, Nothing, False, 0, True, 100, "Second Name"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A1SubscriberConsumerEnquiry].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Date of Birth"))
    oList.Columns.Add(New ListColumn("GenderInd", "[A1SubscriberConsumerEnquiry].[GenderInd]", "GenderInd", DataType.String, Nothing, False, 0, True, 100, "Gender"))
    oList.Columns.Add(New ListColumn("Address1", "[A1SubscriberConsumerEnquiry].[Address1]", "Address1", DataType.String, Nothing, False, 0, True, 100, "Address 1"))
    oList.Columns.Add(New ListColumn("Address2", "[A1SubscriberConsumerEnquiry].[Address2]", "Address2", DataType.String, Nothing, False, 0, True, 100, "Address 2"))
    oList.Columns.Add(New ListColumn("Address3", "[A1SubscriberConsumerEnquiry].[Address3]", "Address3", DataType.String, Nothing, False, 0, True, 100, "Address 3"))
    oList.Columns.Add(New ListColumn("Address4", "[A1SubscriberConsumerEnquiry].[Address4]", "Address4", DataType.String, Nothing, False, 0, True, 100, "Address 4"))
    oList.Columns.Add(New ListColumn("PostalCode", "[A1SubscriberConsumerEnquiry].[PostalCode]", "PostalCode", DataType.String, Nothing, False, 0, True, 100, "Postal Code"))
    oList.Columns.Add(New ListColumn("TelephoneCode", "[A1SubscriberConsumerEnquiry].[TelephoneCode]", "TelephoneCode", DataType.String, Nothing, False, 0, True, 100, "Telephone Code"))
    oList.Columns.Add(New ListColumn("TelephoneNo", "[A1SubscriberConsumerEnquiry].[TelephoneNo]", "TelephoneNo", DataType.String, Nothing, False, 0, True, 100, "Telephone No"))
    oList.Columns.Add(New ListColumn("AgeSearchTypeInd", "[A1SubscriberConsumerEnquiry].[AgeSearchTypeInd]", "AgeSearchTypeInd", DataType.String, Nothing, False, 0, True, 100, "Type of Deviation"))
    oList.Columns.Add(New ListColumn("Age", "[A1SubscriberConsumerEnquiry].[Age]", "Age", DataType.Integer, Nothing, False, 0, True, 100, "Age"))
    oList.Columns.Add(New ListColumn("AgeBirthYear", "[A1SubscriberConsumerEnquiry].[AgeBirthYear]", "AgeBirthYear", DataType.Integer, Nothing, False, 0, True, 100, "Year"))
    oList.Columns.Add(New ListColumn("AgeDeviation", "[A1SubscriberConsumerEnquiry].[AgeDeviation]", "AgeDeviation", DataType.Integer, Nothing, False, 0, True, 100, "Deviation in Years (+-)"))
    oList.Columns.Add(New ListColumn("AccountNo", "[A1SubscriberConsumerEnquiry].[AccountNo]", "AccountNo", DataType.String, Nothing, False, 0, True, 100, "Account No"))
    oList.Columns.Add(New ListColumn("SubAccountNo", "[A1SubscriberConsumerEnquiry].[SubAccountNo]", "SubAccountNo", DataType.String, Nothing, False, 0, True, 100, "Sub Account No"))
    oList.Columns.Add(New ListColumn("ResultIDNo", "[A1SubscriberConsumerEnquiry].[ResultIDNo]", "ResultIDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("ResultPassportNo", "[A1SubscriberConsumerEnquiry].[ResultPassportNo]", "ResultPassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No / Other ID No"))
    oList.Columns.Add(New ListColumn("ResultFirstInitial", "[A1SubscriberConsumerEnquiry].[ResultFirstInitial]", "ResultFirstInitial", DataType.String, Nothing, False, 0, True, 100, "First Initial"))
    oList.Columns.Add(New ListColumn("ResultSecondInitial", "[A1SubscriberConsumerEnquiry].[ResultSecondInitial]", "ResultSecondInitial", DataType.String, Nothing, False, 0, True, 100, "Second Initial"))
    oList.Columns.Add(New ListColumn("ResultFirstName", "[A1SubscriberConsumerEnquiry].[ResultFirstName]", "ResultFirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("ResultSecondName", "[A1SubscriberConsumerEnquiry].[ResultSecondName]", "ResultSecondName", DataType.String, Nothing, False, 0, True, 100, "Second Name"))
    oList.Columns.Add(New ListColumn("ResultSurname", "[A1SubscriberConsumerEnquiry].[ResultSurname]", "ResultSurname", DataType.String, Nothing, False, 0, True, 100, "Surname"))
    oList.Columns.Add(New ListColumn("ResultBirthDate", "[A1SubscriberConsumerEnquiry].[ResultBirthDate]", "ResultBirthDate", DataType.DateTime, "d", False, 0, True, 100, "Birth Date"))
    oList.Columns.Add(New ListColumn("ResultGenderInd", "[A1SubscriberConsumerEnquiry].[ResultGenderInd]", "ResultGenderInd", DataType.String, Nothing, False, 0, True, 100, "Gender"))
    oList.Columns.Add(New ListColumn("TelephoneTypeInd", "null", "TelephoneTypeInd", DataType.String, Nothing, False, 0, True, 100, "Search On"))
    oList.Columns.Add(New ListColumn("TelephoneTelephoneCode", "null", "TelephoneTelephoneCode", DataType.String, Nothing, False, 0, True, 100, "Landline Code"))
    oList.Columns.Add(New ListColumn("TelephoneTelephoneNo", "null", "TelephoneTelephoneNo", DataType.String, Nothing, False, 0, True, 100, "Landline No"))
    oList.Columns.Add(New ListColumn("TelephoneCellularCode", "null", "TelephoneCellularCode", DataType.String, Nothing, False, 0, True, 100, "Cellular Code"))
    oList.Columns.Add(New ListColumn("TelephoneCellularNo", "null", "TelephoneCellularNo", DataType.String, Nothing, False, 0, True, 100, "Cellular No"))
    oList.Columns.Add(New ListColumn("AddressTypeInd", "null", "AddressTypeInd", DataType.String, Nothing, False, 0, True, 100, "Search On"))
    oList.Columns.Add(New ListColumn("AddressStreetNo", "null", "AddressStreetNo", DataType.String, Nothing, False, 0, True, 100, "Street Number"))
    oList.Columns.Add(New ListColumn("AddressStreetName", "null", "AddressStreetName", DataType.String, Nothing, False, 0, True, 100, "Street Name"))
    oList.Columns.Add(New ListColumn("AddressStreetSuburb", "null", "AddressStreetSuburb", DataType.String, Nothing, False, 0, True, 100, "Suburb"))
    oList.Columns.Add(New ListColumn("AddressStreetPostalCode", "null", "AddressStreetPostalCode", DataType.String, Nothing, False, 0, True, 100, "Postal Code"))
    oList.Columns.Add(New ListColumn("AddressPostalNo", "null", "AddressPostalNo", DataType.String, Nothing, False, 0, True, 100, "Postal No"))
    oList.Columns.Add(New ListColumn("AddressPostalSuburb", "null", "AddressPostalSuburb", DataType.String, Nothing, False, 0, True, 100, "Suburb"))
    oList.Columns.Add(New ListColumn("AddressPostalPostalCode", "null", "AddressPostalPostalCode", DataType.String, Nothing, False, 0, True, 100, "Postal Code"))
    oList.Columns.Add(New ListColumn("ProductID", "[A1SubscriberConsumerEnquiry].[ProductID]", "ProductID", DataType.Integer, Nothing, False, 0, True, 100, "Product ID"))
    oList.Columns.Add(New ListColumn("SubscriberID1", "[A1SubscriberConsumerEnquiry].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, False, 0, True, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("ReportFile", "[A0SubscriberTraceConsumer].[ReportFile]", "ReportFile", DataType.Binary, Nothing, False, 0, True, 100, "Report File"))
    oList.Columns.Add(New ListColumn("ReportFileMimeType", "[A0SubscriberTraceConsumer].[ReportFileMimeType]", "ReportFileMimeType", DataType.String, Nothing, False, 0, True, 100, "Report File Mime Type"))
    oList.Columns.Add(New ListColumn("SystemUserID", "[A1SubscriberConsumerEnquiry].[SystemUserID]", "SystemUserID", DataType.Integer, Nothing, False, 0, True, 100, "System User ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "([SubscriberTraceConsumer]  [A0SubscriberTraceConsumer]" + _
           "    left join [SubscriberConsumerEnquiry]  [A1SubscriberConsumerEnquiry] on [A0SubscriberTraceConsumer].[SubscriberConsumerEnquiryID] = [A1SubscriberConsumerEnquiry].[SubscriberConsumerEnquiryID] and [A0SubscriberTraceConsumer].[SubscriberID] = [A1SubscriberConsumerEnquiry].[SubscriberID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberTraceConsumer")
    oList.Columns.Add(New ListColumn("SubscriberTraceConsumerID", "[A0SubscriberTraceConsumer].[SubscriberTraceConsumerID]", "SubscriberTraceConsumerID", DataType.Long, Nothing, True, 0, False, 100, "Subscriber Trace Consumer ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberTraceConsumer].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberConsumerEnquiryID", "[A1SubscriberConsumerEnquiry].[SubscriberConsumerEnquiryID]", "SubscriberConsumerEnquiryID", DataType.Long, Nothing, False, 0, True, 100, "Subscriber Consumer Enquiry ID"))
    oList.Columns.Add(New ListColumn("EnquiryDate", "[A1SubscriberConsumerEnquiry].[EnquiryDate]", "EnquiryDate", DataType.DateTime, "g", False, 0, True, 100, "Enquiry Date"))
    oList.Columns.Add(New ListColumn("EnquiryStatusInd", "[A1SubscriberConsumerEnquiry].[EnquiryStatusInd]", "EnquiryStatusInd", DataType.String, Nothing, False, 0, True, 100, "Enquiry Status"))
    oList.Columns.Add(New ListColumn("EnquiryResultInd", "[A1SubscriberConsumerEnquiry].[EnquiryResultInd]", "EnquiryResultInd", DataType.String, Nothing, False, 0, True, 100, "Enquiry Result"))
    oList.Columns.Add(New ListColumn("ProductPointValue", "[A1SubscriberConsumerEnquiry].[ProductPointValue]", "ProductPointValue", DataType.Decimal, Nothing, False, 0, True, 100, "Product Point Value"))
    oList.Columns.Add(New ListColumn("ExternalReference", "[A1SubscriberConsumerEnquiry].[ExternalReference]", "ExternalReference", DataType.String, Nothing, False, 0, True, 100, "External Reference"))
    oList.Columns.Add(New ListColumn("IDNo", "[A1SubscriberConsumerEnquiry].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("PassportNo", "[A1SubscriberConsumerEnquiry].[PassportNo]", "PassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No / Other ID No"))
    oList.Columns.Add(New ListColumn("Surname", "[A1SubscriberConsumerEnquiry].[Surname]", "Surname", DataType.String, Nothing, False, 0, True, 100, "Surname"))
    oList.Columns.Add(New ListColumn("MaidenName", "[A1SubscriberConsumerEnquiry].[MaidenName]", "MaidenName", DataType.String, Nothing, False, 0, True, 100, "Maiden Name"))
    oList.Columns.Add(New ListColumn("FirstInitial", "[A1SubscriberConsumerEnquiry].[FirstInitial]", "FirstInitial", DataType.String, Nothing, False, 0, True, 100, "First Initial"))
    oList.Columns.Add(New ListColumn("SecondInitial", "[A1SubscriberConsumerEnquiry].[SecondInitial]", "SecondInitial", DataType.String, Nothing, False, 0, True, 100, "Second Initial"))
    oList.Columns.Add(New ListColumn("FirstName", "[A1SubscriberConsumerEnquiry].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("SecondName", "[A1SubscriberConsumerEnquiry].[SecondName]", "SecondName", DataType.String, Nothing, False, 0, True, 100, "Second Name"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A1SubscriberConsumerEnquiry].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Date of Birth"))
    oList.Columns.Add(New ListColumn("GenderInd", "[A1SubscriberConsumerEnquiry].[GenderInd]", "GenderInd", DataType.String, Nothing, False, 0, True, 100, "Gender"))
    oList.Columns.Add(New ListColumn("Address1", "[A1SubscriberConsumerEnquiry].[Address1]", "Address1", DataType.String, Nothing, False, 0, True, 100, "Address 1"))
    oList.Columns.Add(New ListColumn("Address2", "[A1SubscriberConsumerEnquiry].[Address2]", "Address2", DataType.String, Nothing, False, 0, True, 100, "Address 2"))
    oList.Columns.Add(New ListColumn("Address3", "[A1SubscriberConsumerEnquiry].[Address3]", "Address3", DataType.String, Nothing, False, 0, True, 100, "Address 3"))
    oList.Columns.Add(New ListColumn("Address4", "[A1SubscriberConsumerEnquiry].[Address4]", "Address4", DataType.String, Nothing, False, 0, True, 100, "Address 4"))
    oList.Columns.Add(New ListColumn("PostalCode", "[A1SubscriberConsumerEnquiry].[PostalCode]", "PostalCode", DataType.String, Nothing, False, 0, True, 100, "Postal Code"))
    oList.Columns.Add(New ListColumn("TelephoneCode", "[A1SubscriberConsumerEnquiry].[TelephoneCode]", "TelephoneCode", DataType.String, Nothing, False, 0, True, 100, "Telephone Code"))
    oList.Columns.Add(New ListColumn("TelephoneNo", "[A1SubscriberConsumerEnquiry].[TelephoneNo]", "TelephoneNo", DataType.String, Nothing, False, 0, True, 100, "Telephone No"))
    oList.Columns.Add(New ListColumn("AgeSearchTypeInd", "[A1SubscriberConsumerEnquiry].[AgeSearchTypeInd]", "AgeSearchTypeInd", DataType.String, Nothing, False, 0, True, 100, "Type of Deviation"))
    oList.Columns.Add(New ListColumn("Age", "[A1SubscriberConsumerEnquiry].[Age]", "Age", DataType.Integer, Nothing, False, 0, True, 100, "Age"))
    oList.Columns.Add(New ListColumn("AgeBirthYear", "[A1SubscriberConsumerEnquiry].[AgeBirthYear]", "AgeBirthYear", DataType.Integer, Nothing, False, 0, True, 100, "Year"))
    oList.Columns.Add(New ListColumn("AgeDeviation", "[A1SubscriberConsumerEnquiry].[AgeDeviation]", "AgeDeviation", DataType.Integer, Nothing, False, 0, True, 100, "Deviation in Years (+-)"))
    oList.Columns.Add(New ListColumn("AccountNo", "[A1SubscriberConsumerEnquiry].[AccountNo]", "AccountNo", DataType.String, Nothing, False, 0, True, 100, "Account No"))
    oList.Columns.Add(New ListColumn("SubAccountNo", "[A1SubscriberConsumerEnquiry].[SubAccountNo]", "SubAccountNo", DataType.String, Nothing, False, 0, True, 100, "Sub Account No"))
    oList.Columns.Add(New ListColumn("ResultIDNo", "[A1SubscriberConsumerEnquiry].[ResultIDNo]", "ResultIDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("ResultPassportNo", "[A1SubscriberConsumerEnquiry].[ResultPassportNo]", "ResultPassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No / Other ID No"))
    oList.Columns.Add(New ListColumn("ResultFirstInitial", "[A1SubscriberConsumerEnquiry].[ResultFirstInitial]", "ResultFirstInitial", DataType.String, Nothing, False, 0, True, 100, "First Initial"))
    oList.Columns.Add(New ListColumn("ResultSecondInitial", "[A1SubscriberConsumerEnquiry].[ResultSecondInitial]", "ResultSecondInitial", DataType.String, Nothing, False, 0, True, 100, "Second Initial"))
    oList.Columns.Add(New ListColumn("ResultFirstName", "[A1SubscriberConsumerEnquiry].[ResultFirstName]", "ResultFirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("ResultSecondName", "[A1SubscriberConsumerEnquiry].[ResultSecondName]", "ResultSecondName", DataType.String, Nothing, False, 0, True, 100, "Second Name"))
    oList.Columns.Add(New ListColumn("ResultSurname", "[A1SubscriberConsumerEnquiry].[ResultSurname]", "ResultSurname", DataType.String, Nothing, False, 0, True, 100, "Surname"))
    oList.Columns.Add(New ListColumn("ResultBirthDate", "[A1SubscriberConsumerEnquiry].[ResultBirthDate]", "ResultBirthDate", DataType.DateTime, "d", False, 0, True, 100, "Birth Date"))
    oList.Columns.Add(New ListColumn("ResultGenderInd", "[A1SubscriberConsumerEnquiry].[ResultGenderInd]", "ResultGenderInd", DataType.String, Nothing, False, 0, True, 100, "Gender"))
    oList.Columns.Add(New ListColumn("TelephoneTypeInd", "null", "TelephoneTypeInd", DataType.String, Nothing, False, 0, True, 100, "Search On"))
    oList.Columns.Add(New ListColumn("TelephoneTelephoneCode", "null", "TelephoneTelephoneCode", DataType.String, Nothing, False, 0, True, 100, "Landline Code"))
    oList.Columns.Add(New ListColumn("TelephoneTelephoneNo", "null", "TelephoneTelephoneNo", DataType.String, Nothing, False, 0, True, 100, "Landline No"))
    oList.Columns.Add(New ListColumn("TelephoneCellularCode", "null", "TelephoneCellularCode", DataType.String, Nothing, False, 0, True, 100, "Cellular Code"))
    oList.Columns.Add(New ListColumn("TelephoneCellularNo", "null", "TelephoneCellularNo", DataType.String, Nothing, False, 0, True, 100, "Cellular No"))
    oList.Columns.Add(New ListColumn("AddressTypeInd", "null", "AddressTypeInd", DataType.String, Nothing, False, 0, True, 100, "Search On"))
    oList.Columns.Add(New ListColumn("AddressStreetNo", "null", "AddressStreetNo", DataType.String, Nothing, False, 0, True, 100, "Street Number"))
    oList.Columns.Add(New ListColumn("AddressStreetName", "null", "AddressStreetName", DataType.String, Nothing, False, 0, True, 100, "Street Name"))
    oList.Columns.Add(New ListColumn("AddressStreetSuburb", "null", "AddressStreetSuburb", DataType.String, Nothing, False, 0, True, 100, "Suburb"))
    oList.Columns.Add(New ListColumn("AddressStreetPostalCode", "null", "AddressStreetPostalCode", DataType.String, Nothing, False, 0, True, 100, "Postal Code"))
    oList.Columns.Add(New ListColumn("AddressPostalNo", "null", "AddressPostalNo", DataType.String, Nothing, False, 0, True, 100, "Postal No"))
    oList.Columns.Add(New ListColumn("AddressPostalSuburb", "null", "AddressPostalSuburb", DataType.String, Nothing, False, 0, True, 100, "Suburb"))
    oList.Columns.Add(New ListColumn("AddressPostalPostalCode", "null", "AddressPostalPostalCode", DataType.String, Nothing, False, 0, True, 100, "Postal Code"))
    oList.Columns.Add(New ListColumn("ProductID", "[A1SubscriberConsumerEnquiry].[ProductID]", "ProductID", DataType.Integer, Nothing, False, 0, True, 100, "Product ID"))
    oList.Columns.Add(New ListColumn("SubscriberID1", "[A1SubscriberConsumerEnquiry].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, False, 0, True, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SystemUserID", "[A1SubscriberConsumerEnquiry].[SystemUserID]", "SystemUserID", DataType.Integer, Nothing, False, 0, True, 100, "System User ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "([SubscriberTraceConsumer]  [A0SubscriberTraceConsumer]" + _
           "    left join [SubscriberConsumerEnquiry]  [A1SubscriberConsumerEnquiry] on [A0SubscriberTraceConsumer].[SubscriberConsumerEnquiryID] = [A1SubscriberConsumerEnquiry].[SubscriberConsumerEnquiryID] and [A0SubscriberTraceConsumer].[SubscriberID] = [A1SubscriberConsumerEnquiry].[SubscriberID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberTraceConsumer")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberTraceConsumer].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberTraceConsumerID", "[A0SubscriberTraceConsumer].[SubscriberTraceConsumerID]", "SubscriberTraceConsumerID", DataType.Long, Nothing, True, 0, False, 100, "Subscriber Trace Consumer ID"))
    oList.Columns.Add(New ListColumn("ReferenceNo", "[A0SubscriberTraceConsumer].[ReferenceNo]", "ReferenceNo", DataType.String, Nothing, False, 0, True, 100, "Reference No"))
    oList.Columns.Add(New ListColumn("TraceDate", "[A0SubscriberTraceConsumer].[TraceDate]", "TraceDate", DataType.DateTime, "g", False, -1, True, 100, "Trace Date"))
    oList.Columns.Add(New ListColumn("ProductTypeDesc", "[A2ProductType].[ProductTypeDesc]", "ProductTypeDesc", DataType.String, Nothing, False, 0, False, 200, "Product Type Description"))
    oList.Columns.Add(New ListColumn("ProductDesc", "[A1Product].[ProductDesc]", "ProductDesc", DataType.String, Nothing, False, 0, True, 200, "Product Description"))
    oList.Columns.Add(New ListColumn("DefaultPointValue", "[A1Product].[DefaultPointValue]", "DefaultPointValue", DataType.Decimal, Nothing, False, 0, False, 100, "Default Point Value"))
    oList.Columns.Add(New ListColumn("ProductDetails", "[A1Product].[ProductDetails]", "ProductDetails", DataType.Text, Nothing, False, 0, False, 200, "Product Details"))
    oList.Columns.Add(New ListColumn("IncludePrioritizationRatingYN", "[A0SubscriberTraceConsumer].[IncludePrioritizationRatingYN]", "IncludePrioritizationRatingYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Include Prioritization Rating?"))
    oList.Columns.Add(New ListColumn("IncludePublicDomainYN", "[A0SubscriberTraceConsumer].[IncludePublicDomainYN]", "IncludePublicDomainYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Include Public Domain?"))
    oList.Columns.Add(New ListColumn("FullName", "isnull([A3ActionedBySystemUser].[FirstName], '') + ' ' + isnull([A3ActionedBySystemUser].[Surname], '')", "FullName", DataType.String, Nothing, False, 0, True, 200, "Full Name"))
    oList.Columns.Add(New ListColumn("EmailAddress", "[A3ActionedBySystemUser].[EmailAddress]", "EmailAddress", DataType.String, Nothing, False, 0, False, 100, "Email Address"))
    oList.SelectStatement = "select"
    oList.FromClause = "(([SubscriberTraceConsumer]  [A0SubscriberTraceConsumer]" + _
           "    join ([Product]  [A1Product]" + _
           "    join [ProductType]  [A2ProductType] on [A1Product].[ProductTypeID] = [A2ProductType].[ProductTypeID]) on [A0SubscriberTraceConsumer].[ProductID] = [A1Product].[ProductID])" + _
           "    join [SystemUser]  [A3ActionedBySystemUser] on [A0SubscriberTraceConsumer].[ActionedBySystemUserID] = [A3ActionedBySystemUser].[SystemUserID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
