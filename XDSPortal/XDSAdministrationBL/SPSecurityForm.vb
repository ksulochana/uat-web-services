Imports sembleWare.Runtime
Imports System
Public Class SPSecurityForm 'sembleWare: Part
  Inherits CustomMemoryPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly FormName_SP As New StringElement("FormName_SP", Me, False, True, True, True, False, 100, "Form Name", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FormCaption_SP As New StringElement("FormCaption_SP", Me, False, True, True, True, False, 250, "Form Caption", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AccessInd_SP As New IndicatorElement("AccessInd_SP", Me, False, True, True, True, True, False, "Access", Nothing, SPSecurityForm.AccessInd_SPOptions, "3")
  Public ReadOnly SecurityModule_SP As Relationship = New SecurityModuleRelationship("SecurityModule_SP", "Security Module", RelationshipType.Include, False, True, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _ModuleCode As New StringElement("ModuleCode", Me, False)
  Public ReadOnly _SecurityModule_SP As SecurityModuleRelationship = SecurityModule_SP
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moAccessInd_SPOptions As IndicatorOptions
  Public Shared ReadOnly Property AccessInd_SPOptions() As IndicatorOptions
    Get
      If moAccessInd_SPOptions Is Nothing Then
        moAccessInd_SPOptions = New IndicatorOptions
        moAccessInd_SPOptions.Add(0, "None")
        moAccessInd_SPOptions.Add(1, "Can View")
        moAccessInd_SPOptions.Add(2, "Can Change")
        moAccessInd_SPOptions.Add(3, "All")
      End If
      Return moAccessInd_SPOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SPSecurityForm", ApplicationSettings)
    _SecurityModule_SP._ModuleCode.LocalElement = _ModuleCode
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class SPSecurityFormRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SPSecurityForm(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
