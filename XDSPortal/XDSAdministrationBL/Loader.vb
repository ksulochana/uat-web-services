Imports sembleWare.Runtime
Imports System
Imports System.Text
Imports System.Web
Public Class Loader 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly LoaderID As New IdentityElement("LoaderID", Me, True, Nothing, Nothing)
  Public ReadOnly LoaderDate As New DateTimeElement("LoaderDate", Me, False, False, True, True, True, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly LoaderFileName As New StringElement("LoaderFileName", Me, False, False, True, True, True, 250, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LoaderFileSize As New IntegerElement("LoaderFileSize", Me, False, True, True, True, True, Nothing, Nothing, "###,###,##0 KB", Nothing, Nothing, Nothing)
  Public ReadOnly OriginalFileName As New StringElement("OriginalFileName", Me, False, False, True, True, True, 250, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LoaderLabelDescription As New StringElement("LoaderLabelDescription", Me, False, True, True, False, False, 250, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly EnableQuickLoadYN As New BooleanElement("EnableQuickLoadYN", Me, False, True, True, True, True, "Enable Quick Load?", Nothing, False, "Yes;No")
  Public ReadOnly FileFormat As Relationship = New FileFormatRelationship("FileFormat", Nothing, RelationshipType.Include, True, True, True, Me)
  Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.Include, True, True, True, Me)
  Public ReadOnly FileFormatDeployment As Relationship = New FileFormatDeploymentRelationship("FileFormatDeployment", Nothing, RelationshipType.Include, True, True, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly LoaderProcess_OwnMany As Relationship = New LoaderProcessRelationship("LoaderProcess", Nothing, "Loader", Me)
  Public ReadOnly LoaderProcessHistory_OwnMany As Relationship = New LoaderProcessHistoryRelationship("LoaderProcessHistory", Nothing, "Loader", Me)
  Public ReadOnly LoaderParameter_OwnMany As Relationship = New LoaderParameterRelationship("LoaderParameter", Nothing, "Loader", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _FileFormatCode As New StringElement("FileFormatCode", Me, False)
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, False)
  Public ReadOnly _FileFormatDeploymentID As New IntegerElement("FileFormatDeploymentID", Me, False)
  Public ReadOnly _FileFormat As FileFormatRelationship = FileFormat
  Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
  Public ReadOnly _FileFormatDeployment As FileFormatDeploymentRelationship = FileFormatDeployment
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  Public ReadOnly _LoaderProcess_OwnMany As LoaderProcessRelationship = LoaderProcess_OwnMany
  Public ReadOnly _LoaderProcessHistory_OwnMany As LoaderProcessHistoryRelationship = LoaderProcessHistory_OwnMany
  Public ReadOnly _LoaderParameter_OwnMany As LoaderParameterRelationship = LoaderParameter_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("Loader", ApplicationSettings)
    _FileFormat._FileFormatCode.LocalElement = _FileFormatCode
    _Subscriber._SubscriberID.LocalElement = _SubscriberID
    _FileFormatDeployment._FileFormatDeploymentID.LocalElement = _FileFormatDeploymentID
    AddHandler Subscriber.Changed, AddressOf Subscriber_Changed
    'sembleWare: Constructor End - Do Not Modify
    AddHandler Subscriber.LimitRelatedList, AddressOf Subscriber_LimitRelatedList
    AddHandler FileFormat.LimitRelatedList, AddressOf FileFormat_LimitRelatedList
  End Sub

#End Region 'sembleWare: Constructor

#Region " LimitRelatedList Events"
  Private Sub Subscriber_LimitRelatedList(ByVal List As List)
    Dim sValue As String = ApplicationSettings.QueryBuilder.ToSQL(Constants.Subscriber.Types.Both) & ", " & ApplicationSettings.QueryBuilder.ToSQL(Constants.Subscriber.Types.DataSupplier)

    List.AndFilters.Add(New ListFilter("SubscriberTypeInd", "SubscriberTypeInd", sValue, DataType.String, FilterType.In, FilterBlankBehavior.DisplayEmptyList, True))
  End Sub

  Private Sub FileFormat_LimitRelatedList(ByVal List As List)
    Dim oDBList As DBList = List

    oDBList.AppendToWhereClause("FileFormatCode in (select FileFormatCode from SubscriberProfileFileFormat where SubscriberID = " & ApplicationSettings.QueryBuilder.ToSQL(Me._SubscriberID.Value) & ")")
  End Sub
#End Region

#Region " Change Events"
  Private Sub Subscriber_Changed(ByVal Relationship As Relationship)
    BRSubscriber(Relationship, True, True)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"

  Public Sub CheckMandatoryLoaderParameters()
    Dim oDataRow As DataRow
    Dim oStringBuilder As StringBuilder = New StringBuilder

    oStringBuilder.Append("select count(LoaderID) ")
    oStringBuilder.Append("  from LoaderParameter ")
    oStringBuilder.Append(Me.InstanceWhereClause(Me))
    oStringBuilder.Append("   and ParameterValue is null")

    For Each oDataRow In ApplicationSettings.ResultQuery(oStringBuilder.ToString()).Rows
      If System.Convert.ToInt32(oDataRow(0)) = 0 Then
        Return
      End If
    Next
    Throw New BusinessRuleException("Mandatory Loader Parameter Values are missing!")
  End Sub

#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreate()
  End Sub

  Public Overrides Sub Load()
    MyBase.Load()
    BRLoad()
  End Sub

  Public Overrides Sub Save()
    With ApplicationSettings
      Try
        Dim bIsNewYN As Boolean = Me.IsNew

        .BeginTransaction()
        BRBeforeSave(bIsNewYN)
        MyBase.Save()
        BRAfterSave(bIsNewYN)
        .CommitTransaction()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
    End With
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreate()
    Me.LoaderDate.Value = System.DateTime.Now
    Me.EnableQuickLoadYN.Value = SystemDefault.Current(ApplicationSettings).EnableLoaderProcessQuickLoadYN.Value
    BRLoaderLabelDescription()
  End Sub

  Private Sub BRLoad()
    Me.LoaderDate.Enabled = False
    Me.Subscriber.Enabled = False
    Me.FileFormat.Enabled = False
    Me.EnableQuickLoadYN.Enabled = False
    BRLoaderLabelDescription()
  End Sub

  Private Sub BRBeforeSave(ByVal IsNewYN As Boolean)
    If IsNewYN Then
      Me.LoaderFileName.Value = "LoaderID"
      Me.FileFormatDeployment.Instance = XDSAdministrationBL.FileFormatDeployment.GetMaxCompletedFileFormatDeployment(ApplicationSettings)
      If Me.FileFormatDeployment.IsEmpty Then
        Throw New BusinessRuleException("A Successful Deployment of the File Formats does not exist! Please deploy the File Formats before attempting to Load a File!")
      End If
    End If
  End Sub

  Private Sub BRAfterSave(ByVal IsNewYN As Boolean)
    If IsNewYN Then
      Me.LoaderFileName.Value = Me.LoaderFileName.Value & Me.LoaderID.Value & ".txt"
      MyBase.Save()
      Loader.InsertLoaderParameters(Me, ApplicationSettings)
      Dim oLoaderProcess As LoaderProcess = LoaderProcess.GetDataLoad(Me, ApplicationSettings)
      oLoaderProcess.LoaderProcessStatus.Instance = LoaderProcessStatus.GetRequireSubmit(ApplicationSettings)
      oLoaderProcess.Save()
      oLoaderProcess.CheckForQuickLoad()

      Dim sUploadFolderPath As String = SystemDefault.GetLoaderUploadFolderPath(ApplicationSettings)
      Dim sStorageFolderPath As String = SystemDefault.GetLoaderStorageFolderPath(ApplicationSettings)
      Dim sUploadFileName As String = System.IO.Path.Combine(sUploadFolderPath, Me.OriginalFileName.Value)
      Dim sStorageFileName As String = System.IO.Path.Combine(sStorageFolderPath, Me.LoaderFileName.Value)
      System.IO.File.Move(sUploadFileName, sStorageFileName)
    End If

    Me.LoaderDate.Enabled = False
    Me.Subscriber.Enabled = False
    Me.FileFormat.Enabled = False
    Me.EnableQuickLoadYN.Enabled = False
    BRLoaderLabelDescription()
  End Sub

  Private Sub BRSubscriber(ByVal Relationship As Relationship, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If PerformRulesYN Then
      If Not Relationship.IsEmpty Then
        Me.FileFormat.Instance = SubscriberProfileFileFormat.GetDefaultFileFormat(Me._SubscriberID.Value, ApplicationSettings)
      End If
    End If
  End Sub

  Public Sub BRLoaderLabelDescription()
    If Me.IsNew Then
      Me.LoaderLabelDescription.Value = "Loader: (New)"
    Else
      Dim sDesc As String = Trim(Me.OriginalFileName.Value)
      Me.LoaderLabelDescription.Value = "Loader: " & Me.LoaderID.Value & " (" & sDesc & ")"
    End If
  End Sub
#End Region

#Region " Methods"
  Public Sub DeployLoaderFileFormat(ByVal ConnectionString As String)
    Dim sServerName As String = ""
    Dim sDatabaseName As String = ""
    Dim sUsername As String = ""
    Dim sPassword As String = ""
    Dim sItems() As String = ConnectionString.Split(";")
    Dim sItem As String

    For Each sItem In sItems
      Dim sSubItems() As String = sItem.Split("=")
      Select Case sSubItems(0).ToLower()
        Case "data source"
          sServerName = sSubItems(1)
        Case "initial catalog"
          sDatabaseName = sSubItems(1)
        Case "user id"
          sUsername = sSubItems(1)
        Case "pwd"
          sPassword = sSubItems(1)
      End Select
    Next

    Dim oLoaderEngineManager As LoaderEngineManager.Manager = New LoaderEngineManager.Manager(Me.FileFormatDeployment.Instance, Me.FileFormat.Instance, sServerName, sDatabaseName, sUsername, sPassword, ApplicationSettings)
    oLoaderEngineManager.DeployLoaderFileFormats(Me.LoaderID.Value)
  End Sub

  Public Function DefineLoaderStagingGridList(ByVal MetaDatabase As MetaDatabase) As sembleWare.Runtime.List
    If Not MetaDatabase.IsNullInstance Then
      Dim oXDSAdministrationRoot As XDSAdministrationRoot = Me.XDSAdministrationRoot.Instance
      Dim oDBList As DBList = oXDSAdministrationRoot._vwLoaderStagingTable_OwnMany.GridList
      Dim oSubscriber As Subscriber = Me.Subscriber.Instance
      Dim oFileFormat As FileFormat = Me.FileFormat.Instance
      Dim oLoaderEngineManager As LoaderEngineManager.Manager = New LoaderEngineManager.Manager(Me.FileFormatDeployment.Instance, oFileFormat, ApplicationSettings)
      Dim oList As List = oFileFormat._FileFormatField_OwnMany.GridList
      Dim oDataRow As DataRow

      oList.AndFilters.Add(New ListFilter("LoaderPostDataUpdateDisplayInListYN", "LoaderPostDataUpdateDisplayInListYN", True, DataType.Boolean, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      For Each oDataRow In oList.DataTable.Rows
        Dim sCaption As String = oDataRow("FileFormatFieldDesc")
        Dim sFieldName As String = oLoaderEngineManager.ParseFileFormatFieldDesc(oDataRow("FileFormatFieldDesc"))
        oDBList.Columns.Add(New ListColumn(sFieldName, "[A0vwLoaderStagingTable]." & sFieldName, sFieldName, DataType.String, Nothing, False, 0, True, 100, sCaption))
      Next

      oDBList.FromClause = oDBList.FromClause.Replace("[vwLoaderStagingTable]", "[" & oLoaderEngineManager.StagingTableName(MetaDatabase) & "]")
      oDBList.AppendToWhereClause("LoaderID = " & ApplicationSettings.QueryBuilder.ToSQL(Me.LoaderID.Value))
      oDBList.AppendToWhereClause("ConsumerID = -1")

      Return oDBList
    End If
    Return Nothing
  End Function

  Public Sub DefineLoaderStagingGridColumns(ByRef DataGrid As System.Web.UI.WebControls.DataGrid)
    Dim oXDSAdministrationRoot As XDSAdministrationRoot = Me.XDSAdministrationRoot.Instance
    Dim oDBList As DBList = oXDSAdministrationRoot._vwLoaderStagingTable_OwnMany.GridList
    Dim oSubscriber As Subscriber = Me.Subscriber.Instance
    Dim oFileFormat As FileFormat = Me.FileFormat.Instance
    Dim oLoaderEngineManager As LoaderEngineManager.Manager = New LoaderEngineManager.Manager(Me.FileFormatDeployment.Instance, oFileFormat, ApplicationSettings)
    Dim oList As List = oFileFormat._FileFormatField_OwnMany.GridList
    Dim oDataRow As DataRow

    oList.AndFilters.Add(New ListFilter("LoaderPostDataUpdateDisplayInListYN", "LoaderPostDataUpdateDisplayInListYN", True, DataType.Boolean, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
    For Each oDataRow In oList.DataTable.Rows
      Dim sCaption As String = oDataRow("FileFormatFieldDesc")
      Dim sFieldName As String = oLoaderEngineManager.ParseFileFormatFieldDesc(oDataRow("FileFormatFieldDesc"))
      Dim oBoundColumn As System.Web.UI.WebControls.BoundColumn = New System.Web.UI.WebControls.BoundColumn
      oBoundColumn.DataField = sFieldName
      oBoundColumn.HeaderText = sCaption
      oBoundColumn.SortExpression = sFieldName
      DataGrid.Columns.Add(oBoundColumn)
    Next
  End Sub

  Private Sub DefineStagingTableFields(ByRef HtmlTable As System.Web.UI.HtmlControls.HtmlTable)
    Dim oXDSAdministrationRoot As XDSAdministrationRoot = Me.XDSAdministrationRoot.Instance
    Dim oDBList As DBList = oXDSAdministrationRoot._vwLoaderStagingTable_OwnMany.GridList
    Dim oSubscriber As Subscriber = Me.Subscriber.Instance
    Dim oFileFormat As FileFormat = Me.FileFormat.Instance
    Dim oLoaderEngineManager As LoaderEngineManager.Manager = New LoaderEngineManager.Manager(Me.FileFormatDeployment.Instance, oFileFormat, ApplicationSettings)

    Dim oList As List = oFileFormat._FileFormatField_OwnMany.GridList
    Dim oDataRow As DataRow
    oList.AndFilters.Add(New ListFilter("LoaderPostDataUpdateDisplayInListYN", "LoaderPostDataUpdateDisplayInListYN", True, DataType.Boolean, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
    For Each oDataRow In oList.DataTable.Rows
      Dim sCaption As String = oDataRow("FileFormatFieldDesc")
      Dim sFieldName As String = oLoaderEngineManager.ParseFileFormatFieldDesc(oDataRow("FileFormatFieldDesc"))

      Dim oHtmlTableRow As System.Web.UI.HtmlControls.HtmlTableRow = New System.Web.UI.HtmlControls.HtmlTableRow
      Dim oHtmlTableCell As System.Web.UI.HtmlControls.HtmlTableCell = New System.Web.UI.HtmlControls.HtmlTableCell

      Dim oLabel As System.Web.UI.WebControls.Label = New System.Web.UI.WebControls.Label
      oLabel.ID = "lbl" & sFieldName
      oLabel.Text = sCaption
      oLabel.CssClass = "LabelStyle"
      oHtmlTableCell.Controls.Add(oLabel)
      oHtmlTableRow.Cells.Add(oHtmlTableCell)

      oHtmlTableCell = New System.Web.UI.HtmlControls.HtmlTableCell

      Dim oTextBox As System.Web.UI.WebControls.TextBox = New System.Web.UI.WebControls.TextBox
      oTextBox.ID = "txt" & sFieldName
      oTextBox.CssClass = "TextBoxStyle"
      oHtmlTableCell.Controls.Add(oTextBox)
      oHtmlTableRow.Cells.Add(oHtmlTableCell)

      HtmlTable.Rows.Add(oHtmlTableRow)
    Next
  End Sub
#End Region

#Region " Shared Procedures"
  Public Shared Function GetLoader(ByVal LoaderID As Long, ByVal ApplicationSettings As ApplicationSettings) As Loader
    Dim oLoader As Loader = New LoaderRelationship(ApplicationSettings).NewInstance

    Try
      oLoader.LoaderID.Load(LoaderID)
      oLoader.Load()
    Catch oException As sembleWare.Runtime.RecordNotFoundException
      Return Nothing
    Catch oException As Exception
      Throw oException
    End Try
    Return oLoader
  End Function

  Public Shared Sub InsertLoaderParameters(ByVal Loader As Loader, ByVal ApplicationSettings As ApplicationSettings)
    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand

    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spLoaderParameter_I_FileFormatFields"

    oCommand.Parameters.Add("@LoaderID", SqlDbType.VarChar, 10)
    oCommand.Parameters.Add("@FileFormatCode", SqlDbType.VarChar, 10)

    oCommand.Parameters("@LoaderID").Value = Loader.LoaderID.Value
    oCommand.Parameters("@FileFormatCode").Value = Loader._FileFormatCode.Value

    ApplicationSettings.ActionQuery(oCommand)
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class LoaderRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _LoaderID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New Loader(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _LoaderID.ForeignElement = CType(Instance, Loader).LoaderID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0Loader")
    oList.Columns.Add(New ListColumn("LoaderID", "[A0Loader].[LoaderID]", "LoaderID", DataType.Long, Nothing, True, -1, True, 100, "Loader ID"))
    oList.Columns.Add(New ListColumn("LoaderDate", "[A0Loader].[LoaderDate]", "LoaderDate", DataType.DateTime, "d", False, 0, True, 100, "Loader Date"))
    oList.Columns.Add(New ListColumn("LoaderFileName", "[A0Loader].[LoaderFileName]", "LoaderFileName", DataType.String, Nothing, False, 0, True, 200, "Loader File Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "[Loader]  [A0Loader]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0Loader")
    oList.Columns.Add(New ListColumn("LoaderID", "[A0Loader].[LoaderID]", "LoaderID", DataType.Long, Nothing, True, -1, False, 100, "Loader ID"))
    oList.Columns.Add(New ListColumn("LoaderDate", "[A0Loader].[LoaderDate]", "LoaderDate", DataType.DateTime, "d", False, 0, True, 100, "Loader Date"))
    oList.Columns.Add(New ListColumn("LoaderFileName", "[A0Loader].[LoaderFileName]", "LoaderFileName", DataType.String, Nothing, False, 0, True, 200, "Loader File Name"))
    oList.Columns.Add(New ListColumn("OriginalFileName", "[A0Loader].[OriginalFileName]", "OriginalFileName", DataType.String, Nothing, False, 0, True, 200, "Original File Name"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A1Subscriber].[SubscriberID]", "SubscriberID", DataType.Long, Nothing, False, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberName", "[A1Subscriber].[SubscriberName]", "SubscriberName", DataType.String, Nothing, False, 0, True, 200, "Subscriber Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "([Loader]  [A0Loader]" + _
           "    join [Subscriber]  [A1Subscriber] on [A0Loader].[SubscriberID] = [A1Subscriber].[SubscriberID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function DropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0Loader")
    oList.Columns.Add(New ListColumn("LoaderID", "[A0Loader].[LoaderID]", "LoaderID", DataType.Long, Nothing, True, -1, True, 100, "Loader ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "[Loader]  [A0Loader]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
