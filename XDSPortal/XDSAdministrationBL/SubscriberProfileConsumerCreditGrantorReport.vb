Imports sembleWare.Runtime
Imports System
Public Class SubscriberProfileConsumerCreditGrantorReport 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly OverrideSubscriberAssociationDefaultsYN As New BooleanElement("OverrideSubscriberAssociationDefaultsYN", Me, False, True, True, True, True, "Override Subscriber Association Defaults?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplayPersonalSummaryYN As New BooleanElement("OverrideDisplayPersonalSummaryYN", Me, False, True, True, True, True, "Display Personal Summary?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplayFraudIndicatorsSummaryYN As New BooleanElement("OverrideDisplayFraudIndicatorsSummaryYN", Me, False, True, True, True, True, "Display Fraud Indicators Summary?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplayCreditAccountSummaryYN As New BooleanElement("OverrideDisplayCreditAccountSummaryYN", Me, False, True, True, True, True, "Display Credit Account Summary?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplayPropertyInformationSummaryYN As New BooleanElement("OverrideDisplayPropertyInformationSummaryYN", Me, False, True, True, True, True, "Display Property Information Summary?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplayDirectorshipSummaryYN As New BooleanElement("OverrideDisplayDirectorshipSummaryYN", Me, False, True, True, True, True, "Display Directorship Summary?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplayAccountStatusYN As New BooleanElement("OverrideDisplayAccountStatusYN", Me, False, True, True, True, True, "Display Account Status?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplayMonthlyPaymentDetailsYN As New BooleanElement("OverrideDisplayMonthlyPaymentDetailsYN", Me, False, True, True, True, True, "Display Monthly Payment Details?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplayAdverseInfoYN As New BooleanElement("OverrideDisplayAdverseInfoYN", Me, False, True, True, True, True, "Display Public Domain - Adverse?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplayJudgementsYN As New BooleanElement("OverrideDisplayJudgementsYN", Me, False, True, True, True, True, "Display Public Domain - Judgements?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplayAdministrationOrdersYN As New BooleanElement("OverrideDisplayAdministrationOrdersYN", Me, False, True, True, True, True, "Display Public Domain - Administration Orders?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplaySequestrationsYN As New BooleanElement("OverrideDisplaySequestrationsYN", Me, False, True, True, True, True, "Display Public Domain - Sequestrations?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplayCreditEnquiryHistoryYN As New BooleanElement("OverrideDisplayCreditEnquiryHistoryYN", Me, False, True, True, True, True, "Display Credit Enquiry History?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplayDebtReviewYN As New BooleanElement("OverrideDisplayDebtReviewYN", Me, False, True, True, True, True, "Display Debt Review?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplayFraudIndicatorsDetailYN As New BooleanElement("OverrideDisplayFraudIndicatorsDetailYN", Me, False, True, True, True, True, "Display Fraud Indicators Detail?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplayDirectorshipDetailYN As New BooleanElement("OverrideDisplayDirectorshipDetailYN", Me, False, True, True, True, True, "Display Directorship Detail?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplayPropertyInformationDetailYN As New BooleanElement("OverrideDisplayPropertyInformationDetailYN", Me, False, True, True, True, True, "Display Property Information Detail?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplayNameHistoryYN As New BooleanElement("OverrideDisplayNameHistoryYN", Me, False, True, True, True, True, "Display Name History?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplayAddressHistoryYN As New BooleanElement("OverrideDisplayAddressHistoryYN", Me, False, True, True, True, True, "Display Address History?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplayTelephoneHistoryYN As New BooleanElement("OverrideDisplayTelephoneHistoryYN", Me, False, True, True, True, True, "Display Telephone History?", Nothing, False, "Yes;No")
    Public ReadOnly OverrideDisplayEmploymentHistoryYN As New BooleanElement("OverrideDisplayEmploymentHistoryYN", Me, False, True, True, True, True, "Display Employment History?", Nothing, False, "Yes;No")
    Public ReadOnly OverrideDisplayScoringResultYN As New BooleanElement("OverrideDisplayScoringResultYN", Me, False, True, True, True, True, "Display Scoring information?", Nothing, False, "Yes;No")
    Public ReadOnly OverrideDisplaySemiDefaultsYN As New BooleanElement("OverrideDisplaySemiDefaultsYN", Me, False, True, True, True, True, "Display Semi Default information?", Nothing, False, "Yes;No")
  Public ReadOnly SubscriberProfile As Relationship = New SubscriberProfileRelationship("SubscriberProfile", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _SubscriberProfile As SubscriberProfileRelationship = SubscriberProfile
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberProfileConsumerCreditGrantorReport", ApplicationSettings)
    _SubscriberProfile._SubscriberID.LocalElement = _SubscriberID
    AddHandler OverrideSubscriberAssociationDefaultsYN.Changed, AddressOf OverrideSubscriberAssociationDefaultsYN_Changed
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub OverrideSubscriberAssociationDefaultsYN_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BROverrideSubscriberAssociationDefaultsYN(Element, True, True)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreateLoad()
  End Sub

  Public Overrides Sub Load()
    MyBase.Load()
    BRCreateLoad()
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreateLoad()
    BROverrideSubscriberAssociationDefaultsYN(Me.OverrideSubscriberAssociationDefaultsYN, True, False)
  End Sub

  Private Sub BROverrideSubscriberAssociationDefaultsYN(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If SetEnableDisableYN Then
      Me.OverrideDisplayAccountStatusYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplayAccountStatusYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplayAddressHistoryYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplayAddressHistoryYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplayAdministrationOrdersYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplayAdministrationOrdersYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplayAdverseInfoYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplayAdverseInfoYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplayCreditAccountSummaryYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplayCreditAccountSummaryYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplayCreditEnquiryHistoryYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplayCreditEnquiryHistoryYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplayDebtReviewYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplayDebtReviewYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplayDirectorshipDetailYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplayDirectorshipDetailYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplayDirectorshipSummaryYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplayDirectorshipSummaryYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplayEmploymentHistoryYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplayEmploymentHistoryYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplayFraudIndicatorsDetailYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplayFraudIndicatorsDetailYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplayFraudIndicatorsSummaryYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplayFraudIndicatorsSummaryYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplayJudgementsYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplayJudgementsYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplayMonthlyPaymentDetailsYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplayMonthlyPaymentDetailsYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplayNameHistoryYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplayNameHistoryYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplayPersonalSummaryYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplayPersonalSummaryYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplayPropertyInformationDetailYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplayPropertyInformationDetailYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplayPropertyInformationSummaryYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplayPropertyInformationSummaryYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplaySequestrationsYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplaySequestrationsYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplayTelephoneHistoryYN.Enabled = Element.ValueAsObject
            Me.OverrideDisplayTelephoneHistoryYN.Mandatory = Element.ValueAsObject
            Me.OverrideDisplayScoringResultYN.Enabled = Element.ValueAsObject
            Me.OverrideDisplayScoringResultYN.Mandatory = Element.ValueAsObject
            Me.OverrideDisplaySemiDefaultsYN.Enabled = Element.ValueAsObject
            Me.OverrideDisplaySemiDefaultsYN.Mandatory = Element.ValueAsObject
    End If

    If PerformRulesYN Then
      Me.OverrideDisplayAccountStatusYN.Value = False
      Me.OverrideDisplayAddressHistoryYN.Value = False
      Me.OverrideDisplayAdministrationOrdersYN.Value = False
      Me.OverrideDisplayAdverseInfoYN.Value = False
      Me.OverrideDisplayCreditAccountSummaryYN.Value = False
      Me.OverrideDisplayCreditEnquiryHistoryYN.Value = False
      Me.OverrideDisplayDebtReviewYN.Value = False
      Me.OverrideDisplayDirectorshipDetailYN.Value = False
      Me.OverrideDisplayDirectorshipSummaryYN.Value = False
      Me.OverrideDisplayEmploymentHistoryYN.Value = False
      Me.OverrideDisplayFraudIndicatorsDetailYN.Value = False
      Me.OverrideDisplayFraudIndicatorsSummaryYN.Value = False
      Me.OverrideDisplayJudgementsYN.Value = False
      Me.OverrideDisplayMonthlyPaymentDetailsYN.Value = False
      Me.OverrideDisplayNameHistoryYN.Value = False
      Me.OverrideDisplayPersonalSummaryYN.Value = False
      Me.OverrideDisplayPropertyInformationDetailYN.Value = False
      Me.OverrideDisplayPropertyInformationSummaryYN.Value = False
      Me.OverrideDisplaySequestrationsYN.Value = False
            Me.OverrideDisplayTelephoneHistoryYN.Value = False
            Me.OverrideDisplayScoringResultYN.Value = False
            Me.OverrideDisplaySemiDefaultsYN.Value = False
    End If
  End Sub
#End Region

#Region " Shared Procedures"
  Public Shared Function GetSubscriberProfileConsumerCreditGrantorReport(ByVal SubscriberID As Long, ByVal ApplicationSettings As ApplicationSettings) As SubscriberProfileConsumerCreditGrantorReport
    Dim oSubscriberProfileConsumerCreditGrantorReport As SubscriberProfileConsumerCreditGrantorReport = New SubscriberProfileConsumerCreditGrantorReportRelationship(ApplicationSettings).NewInstance
    Try
      oSubscriberProfileConsumerCreditGrantorReport._SubscriberID.Load(SubscriberID)
      oSubscriberProfileConsumerCreditGrantorReport.Load()
    Catch oException As sembleWare.Runtime.RecordNotFoundException
      oSubscriberProfileConsumerCreditGrantorReport = New SubscriberProfileConsumerCreditGrantorReportRelationship(ApplicationSettings).NewInstance
      oSubscriberProfileConsumerCreditGrantorReport._SubscriberID.Load(SubscriberID)
    Catch oException As Exception
      Throw oException
    End Try
    Return oSubscriberProfileConsumerCreditGrantorReport
  End Function
#End Region
End Class 'sembleWare: Part

Public Class SubscriberProfileConsumerCreditGrantorReportRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberProfileConsumerCreditGrantorReport(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberID.ForeignElement = CType(Instance, SubscriberProfileConsumerCreditGrantorReport)._SubscriberID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
