Imports sembleWare.Runtime
Imports System
Public Class FileFormatMetaGroupField 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly MetaGroupFieldTypeInd As New IndicatorElement("MetaGroupFieldTypeInd", Me, False, False, False, True, True, True, "Meta Group Field Type", Nothing, FileFormatMetaGroupField.MetaGroupFieldTypeIndOptions, Nothing)
  Public ReadOnly AssignedValue As New StringElement("AssignedValue", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly IncludeInChecksumYN As New BooleanElement("IncludeInChecksumYN", Me, False, True, True, True, True, "Include In Checksum?", Nothing, Nothing, "Yes;No")
  Public ReadOnly ChecksumSequenceNum As New IntegerElement("ChecksumSequenceNum", Me, False, True, True, True, False, "Checksum Sequence", Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FileFormatField As Relationship = New FileFormatFieldRelationship("FileFormatField", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly FileFormatMetaGroup As Relationship = New FileFormatMetaGroupRelationship("FileFormatMetaGroup", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly MetaField As Relationship = New MetaFieldRelationship("MetaField", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly MetaGroupField As Relationship = New MetaGroupFieldRelationship("MetaGroupField", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _FileFormatFieldID As New IntegerElement("FileFormatFieldID", Me, False)
  Public ReadOnly _FileFormatCode As New StringElement("FileFormatCode", Me, True)
  Public ReadOnly _MetaDatabaseCode As New StringElement("MetaDatabaseCode", Me, True)
  Public ReadOnly _FileFormatMetaGroupID As New IntegerElement("FileFormatMetaGroupID", Me, True)
  Public ReadOnly _MetaGroupCode As New StringElement("MetaGroupCode", Me, True)
  Public ReadOnly _MetaFieldName As New StringElement("MetaFieldName", Me, True)
  Public ReadOnly _MetaTableName As New StringElement("MetaTableName", Me, True)
  Public ReadOnly _FileFormatField As FileFormatFieldRelationship = FileFormatField
  Public ReadOnly _FileFormatMetaGroup As FileFormatMetaGroupRelationship = FileFormatMetaGroup
  Public ReadOnly _MetaField As MetaFieldRelationship = MetaField
  Public ReadOnly _MetaGroupField As MetaGroupFieldRelationship = MetaGroupField
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moMetaGroupFieldTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property MetaGroupFieldTypeIndOptions() As IndicatorOptions
    Get
      If moMetaGroupFieldTypeIndOptions Is Nothing Then
        moMetaGroupFieldTypeIndOptions = New IndicatorOptions
        moMetaGroupFieldTypeIndOptions.Add("F", "File Format Field")
        moMetaGroupFieldTypeIndOptions.Add("V", "Assigned Value")
      End If
      Return moMetaGroupFieldTypeIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("FileFormatMetaGroupField", ApplicationSettings)
    _FileFormatField._FileFormatFieldID.LocalElement = _FileFormatFieldID
    _FileFormatField._FileFormatCode.LocalElement = _FileFormatCode
    _FileFormatMetaGroup._FileFormatCode.LocalElement = _FileFormatCode
    _FileFormatMetaGroup._MetaDatabaseCode.LocalElement = _MetaDatabaseCode
    _FileFormatMetaGroup._FileFormatMetaGroupID.LocalElement = _FileFormatMetaGroupID
    _FileFormatMetaGroup._MetaGroupCode.LocalElement = _MetaGroupCode
    _MetaField._MetaFieldName.LocalElement = _MetaFieldName
    _MetaField._MetaTableName.LocalElement = _MetaTableName
    _MetaField._MetaDatabaseCode.LocalElement = _MetaDatabaseCode
    _MetaGroupField._MetaFieldName.LocalElement = _MetaFieldName
    _MetaGroupField._MetaTableName.LocalElement = _MetaTableName
    _MetaGroupField._MetaDatabaseCode.LocalElement = _MetaDatabaseCode
    _MetaGroupField._MetaGroupCode.LocalElement = _MetaGroupCode
    AddHandler IncludeInChecksumYN.Changed, AddressOf IncludeInChecksumYN_Changed
    'sembleWare: Constructor End - Do Not Modify
    Me.ForceUpperCaseKeys = False
    AddHandler FileFormatField.LimitRelatedList, AddressOf FileFormatField_LimitRelatedList
  End Sub

#End Region 'sembleWare: Constructor

#Region " LimitRelatedList Events"
  Private Sub FileFormatField_LimitRelatedList(ByVal List As List)
    If Not Me.FileFormatMetaGroup.Instance.IsNullInstance Then
      Dim oFileFormatMetaGroup As FileFormatMetaGroup = Me.FileFormatMetaGroup.Instance
      Dim oFileFormat As FileFormat = oFileFormatMetaGroup.FileFormat.Instance

      List.LimitByRelatedPart(oFileFormat, "FileFormat", True)
    End If
  End Sub
#End Region

#Region " Change Events"
  Private Sub IncludeInChecksumYN_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BRIncludeInChecksumYN(Element, True, True)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Load()
    MyBase.Load()
    BRLoad()
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRLoad()
    BRMetaGroupFieldTypeInd(Me.MetaGroupFieldTypeInd, True, False)
    BRIncludeInChecksumYN(Me.IncludeInChecksumYN, True, False)
  End Sub

  Private Sub BRMetaGroupFieldTypeInd(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If SetEnableDisableYN Then
      Me.FileFormatField.Enabled = False
      Me.FileFormatField.Mandatory = False
      Me.AssignedValue.Enabled = False
      Me.AssignedValue.Mandatory = False
      Select Case Element.ValueAsObject
        Case "F"
          Me.FileFormatField.Enabled = True
          Me.FileFormatField.Mandatory = Me.IncludeInChecksumYN.Value
        Case "V"
          Me.AssignedValue.Enabled = True
          Me.AssignedValue.Mandatory = Me.IncludeInChecksumYN.Value
      End Select
    End If
  End Sub

  Private Sub BRIncludeInChecksumYN(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If SetEnableDisableYN Then
      Me.ChecksumSequenceNum.Enabled = Element.ValueAsObject
      Me.ChecksumSequenceNum.Mandatory = Element.ValueAsObject
      BRMetaGroupFieldTypeInd(Me.MetaGroupFieldTypeInd, True, False)
    End If

    If PerformRulesYN Then
      Me.ChecksumSequenceNum.SetToNullValue()
    End If
  End Sub
#End Region

End Class 'sembleWare: Part

Public Class FileFormatMetaGroupFieldRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _FileFormatCode As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _MetaDatabaseCode As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _FileFormatMetaGroupID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _MetaGroupCode As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _MetaFieldName As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _MetaTableName As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New FileFormatMetaGroupField(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _FileFormatCode.ForeignElement = CType(Instance, FileFormatMetaGroupField)._FileFormatCode
    _MetaDatabaseCode.ForeignElement = CType(Instance, FileFormatMetaGroupField)._MetaDatabaseCode
    _FileFormatMetaGroupID.ForeignElement = CType(Instance, FileFormatMetaGroupField)._FileFormatMetaGroupID
    _MetaGroupCode.ForeignElement = CType(Instance, FileFormatMetaGroupField)._MetaGroupCode
    _MetaFieldName.ForeignElement = CType(Instance, FileFormatMetaGroupField)._MetaFieldName
    _MetaTableName.ForeignElement = CType(Instance, FileFormatMetaGroupField)._MetaTableName
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0FileFormatMetaGroupField")
    oList.Columns.Add(New ListColumn("FileFormatCode", "[A0FileFormatMetaGroupField].[FileFormatCode]", "FileFormatCode", DataType.String, Nothing, True, 0, False, 100, "File Format Code"))
    oList.Columns.Add(New ListColumn("MetaDatabaseCode", "[A0FileFormatMetaGroupField].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, True, 0, False, 100, "Meta Database Code"))
    oList.Columns.Add(New ListColumn("MetaGroupCode", "[A0FileFormatMetaGroupField].[MetaGroupCode]", "MetaGroupCode", DataType.String, Nothing, True, 0, False, 100, "Meta Group Code"))
    oList.Columns.Add(New ListColumn("FileFormatMetaGroupID", "[A0FileFormatMetaGroupField].[FileFormatMetaGroupID]", "FileFormatMetaGroupID", DataType.Integer, Nothing, True, 0, False, 100, "File Format Meta Group ID"))
    oList.Columns.Add(New ListColumn("MetaTableName", "[A0FileFormatMetaGroupField].[MetaTableName]", "MetaTableName", DataType.String, Nothing, True, 0, False, 100, "Meta Table Name"))
    oList.Columns.Add(New ListColumn("MetaFieldName", "[A0FileFormatMetaGroupField].[MetaFieldName]", "MetaFieldName", DataType.String, Nothing, True, 0, False, 100, "Meta Field Name"))
    oList.Columns.Add(New ListColumn("MetaFieldName1", "[A1MetaField].[MetaFieldName]", "MetaFieldName", DataType.String, Nothing, False, 1, True, 100, "Field Name"))
    oList.Columns.Add(New ListColumn("MetaFieldDesc", "[A1MetaField].[MetaFieldDesc]", "MetaFieldDesc", DataType.String, Nothing, False, 0, True, 200, "Field Description"))
    oList.Columns.Add(New ListColumn("FileFormatFieldDesc", "[A2FileFormatField].[FileFormatFieldDesc]", "FileFormatFieldDesc", DataType.String, Nothing, False, 0, True, 200, "Field Description "))
    oList.Columns.Add(New ListColumn("MetaGroupFieldTypeInd", "[A0FileFormatMetaGroupField].[MetaGroupFieldTypeInd]", "MetaGroupFieldTypeInd", DataType.String, Nothing, False, 0, True, 100, "Meta Group Field Type"))
    oList.Columns.Add(New ListColumn("AssignedValue", "[A0FileFormatMetaGroupField].[AssignedValue]", "AssignedValue", DataType.String, Nothing, False, 0, True, 100, "Assigned Value"))
    oList.Columns.Add(New ListColumn("IncludeInChecksumYN", "[A0FileFormatMetaGroupField].[IncludeInChecksumYN]", "IncludeInChecksumYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Include In Checksum?"))
    oList.Columns.Add(New ListColumn("ChecksumSequenceNum", "[A0FileFormatMetaGroupField].[ChecksumSequenceNum]", "ChecksumSequenceNum", DataType.Integer, Nothing, False, 0, True, 100, "Checksum Sequence"))
    oList.SelectStatement = "select"
    oList.FromClause = "((([FileFormatMetaGroupField]  [A0FileFormatMetaGroupField]" + _
           "    join [MetaField]  [A1MetaField] on [A0FileFormatMetaGroupField].[MetaFieldName] = [A1MetaField].[MetaFieldName] and [A0FileFormatMetaGroupField].[MetaTableName] = [A1MetaField].[MetaTableName] and [A0FileFormatMetaGroupField].[MetaDatabaseCode] = [A1MetaField].[MetaDatabaseCode])" + _
           "    left join [FileFormatField]  [A2FileFormatField] on [A0FileFormatMetaGroupField].[FileFormatFieldID] = [A2FileFormatField].[FileFormatFieldID] and [A0FileFormatMetaGroupField].[FileFormatCode] = [A2FileFormatField].[FileFormatCode])" + _
           "    join [MetaGroupField]  [A3MetaGroupField] on [A0FileFormatMetaGroupField].[MetaFieldName] = [A3MetaGroupField].[MetaFieldName] and [A0FileFormatMetaGroupField].[MetaTableName] = [A3MetaGroupField].[MetaTableName] and [A0FileFormatMetaGroupField].[MetaDatabaseCode] = [A3MetaGroupField].[MetaDatabaseCode] and [A0FileFormatMetaGroupField].[MetaGroupCode] = [A3MetaGroupField].[MetaGroupCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
