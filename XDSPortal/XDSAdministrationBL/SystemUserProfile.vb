Imports sembleWare.Runtime
Imports System
Public Class SystemUserProfile 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly ConfirmPurchaseYN As New BooleanElement("ConfirmPurchaseYN", Me, False, True, True, True, True, "I want to confirm the selected record before displaying the results.", Nothing, True, "Yes;No")
  Public ReadOnly DefaultPrioritizationRatingSelectionInd As New IndicatorElement("DefaultPrioritizationRatingSelectionInd", Me, False, False, True, True, True, True, "Default Prioritization Rating Selection", Nothing, SystemUserProfile.DefaultPrioritizationRatingSelectionIndOptions, "P")
  Public ReadOnly DefaultPublicDomainSelectionInd As New IndicatorElement("DefaultPublicDomainSelectionInd", Me, False, False, True, True, True, True, "Default Public Info Selection", Nothing, SystemUserProfile.DefaultPublicDomainSelectionIndOptions, "P")
  Public ReadOnly DefaultTraceEnquiryReportTypeInd As New IndicatorElement("DefaultTraceEnquiryReportTypeInd", Me, False, False, True, True, True, True, "Default Trace Enquiry Report Type", Nothing, SystemUserProfile.DefaultTraceEnquiryReportTypeIndOptions, "PDF")
  Public ReadOnly AuthenticationDefaultRepeatAuthenticationSelectionInd As New IndicatorElement("AuthenticationDefaultRepeatAuthenticationSelectionInd", Me, False, False, True, True, True, True, "Default Repeat Authentication Selection", Nothing, SystemUserProfile.AuthenticationDefaultRepeatAuthenticationSelectionIndOptions, "C")
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly SystemUser As Relationship = New SystemUserRelationship("SystemUser", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly SystemUserProfileProduct_OwnMany As Relationship = New SystemUserProfileProductRelationship("SystemUserProfileProduct", Nothing, "SystemUserProfile", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SystemUserID As New IntegerElement("SystemUserID", Me, True)
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  Public ReadOnly _SystemUser As SystemUserRelationship = SystemUser
  Public ReadOnly _SystemUserProfileProduct_OwnMany As SystemUserProfileProductRelationship = SystemUserProfileProduct_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moDefaultPrioritizationRatingSelectionIndOptions As IndicatorOptions
  Public Shared ReadOnly Property DefaultPrioritizationRatingSelectionIndOptions() As IndicatorOptions
    Get
      If moDefaultPrioritizationRatingSelectionIndOptions Is Nothing Then
        moDefaultPrioritizationRatingSelectionIndOptions = New IndicatorOptions
        moDefaultPrioritizationRatingSelectionIndOptions.Add("P", "Prompt")
        moDefaultPrioritizationRatingSelectionIndOptions.Add("I", "Include")
        moDefaultPrioritizationRatingSelectionIndOptions.Add("G", "Ignore")
      End If
      Return moDefaultPrioritizationRatingSelectionIndOptions
    End Get
  End Property
  Private Shared moDefaultPublicDomainSelectionIndOptions As IndicatorOptions
  Public Shared ReadOnly Property DefaultPublicDomainSelectionIndOptions() As IndicatorOptions
    Get
      If moDefaultPublicDomainSelectionIndOptions Is Nothing Then
        moDefaultPublicDomainSelectionIndOptions = New IndicatorOptions
        moDefaultPublicDomainSelectionIndOptions.Add("P", "Prompt")
        moDefaultPublicDomainSelectionIndOptions.Add("I", "Include")
        moDefaultPublicDomainSelectionIndOptions.Add("G", "Ignore")
      End If
      Return moDefaultPublicDomainSelectionIndOptions
    End Get
  End Property
  Private Shared moDefaultTraceEnquiryReportTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property DefaultTraceEnquiryReportTypeIndOptions() As IndicatorOptions
    Get
      If moDefaultTraceEnquiryReportTypeIndOptions Is Nothing Then
        moDefaultTraceEnquiryReportTypeIndOptions = New IndicatorOptions
        moDefaultTraceEnquiryReportTypeIndOptions.Add("PDF", "PDF")
        moDefaultTraceEnquiryReportTypeIndOptions.Add("HTML4.0", "HTML")
      End If
      Return moDefaultTraceEnquiryReportTypeIndOptions
    End Get
  End Property
  Private Shared moAuthenticationDefaultRepeatAuthenticationSelectionIndOptions As IndicatorOptions
  Public Shared ReadOnly Property AuthenticationDefaultRepeatAuthenticationSelectionIndOptions() As IndicatorOptions
    Get
      If moAuthenticationDefaultRepeatAuthenticationSelectionIndOptions Is Nothing Then
        moAuthenticationDefaultRepeatAuthenticationSelectionIndOptions = New IndicatorOptions
        moAuthenticationDefaultRepeatAuthenticationSelectionIndOptions.Add("O", "Open (View Repeat Authentication within all Subscriber Groups)")
        moAuthenticationDefaultRepeatAuthenticationSelectionIndOptions.Add("C", "Closed (View Repeat Authentication within Subscriber's Group)")
      End If
      Return moAuthenticationDefaultRepeatAuthenticationSelectionIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SystemUserProfile", ApplicationSettings)
    _SystemUser._SystemUserID.LocalElement = _SystemUserID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Shared Procedures"
  Public Shared Function GetSystemUserProfile(ByVal SystemUserID As Long, ByVal ApplicationSettings As ApplicationSettings) As SystemUserProfile
    Dim oSystemUserProfile As SystemUserProfile = New SystemUserProfile(ApplicationSettings)
    oSystemUserProfile._SystemUserID.Load(SystemUserID)
    oSystemUserProfile.Load()
    Return oSystemUserProfile
  End Function

  Public Shared Function GetLoggedInSystemUserProfile(ByVal ApplicationSettings As ApplicationSettings) As SystemUserProfile
    Return SystemUserProfile.GetSystemUserProfile(GlobalSettings.SystemUser.LoggedInSystemUserID, ApplicationSettings)
  End Function
#End Region
End Class 'sembleWare: Part

Public Class SystemUserProfileRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SystemUserID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SystemUserProfile(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SystemUserID.ForeignElement = CType(Instance, SystemUserProfile)._SystemUserID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
