Imports sembleWare.Runtime
Imports System
Public Class ReportingServicesSubReport 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly ReportingServicesSubReportID As New IdentityElement("ReportingServicesSubReportID", Me, True, Nothing, Nothing)
  Public ReadOnly ReportName As New StringElement("ReportName", Me, False, True, True, True, True, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ReportDesc As New StringElement("ReportDesc", Me, False, True, True, True, False, 100, "Report Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FileName As New StringElement("FileName", Me, False, False, True, True, True, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly BinaryFile As New BinaryElement("BinaryFile", Me, True, True, True, True, Nothing, Nothing)
  Public ReadOnly UploadFileYN As New BooleanElement("UploadFileYN", Me, False, True, True, False, False, "Upload File?", Nothing, False, "Yes;No")
  Public ReadOnly ReportingServicesSubReportLabelDescription As New StringElement("ReportingServicesSubReportLabelDescription", Me, False, True, True, False, False, 500, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ReportingServicesReport As Relationship = New ReportingServicesReportRelationship("ReportingServicesReport", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _ReportingServicesReportID As New IntegerElement("ReportingServicesReportID", Me, True)
  Public ReadOnly _ReportingServicesReport As ReportingServicesReportRelationship = ReportingServicesReport
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("ReportingServicesSubReport", ApplicationSettings)
    _ReportingServicesReport._ReportingServicesReportID.LocalElement = _ReportingServicesReportID
    AddHandler UploadFileYN.Changed, AddressOf UploadFileYN_Changed
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub UploadFileYN_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BRUploadFileYN(Element, True, True)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"

  Public Sub DeleteAll()
    Dim oReportingServicesReport As ReportingServicesReport = Me.ReportingServicesReport.Instance
    Dim oList As List = oReportingServicesReport._ReportingServicesSubReport_OwnMany.GridList
    Dim oDataRow As DataRow

    For Each oDataRow In oList.DataTable.Rows
      Dim oReportingServicesSubReport As ReportingServicesSubReport = oReportingServicesReport.ReportingServicesSubReport_OwnMany.NewInstance()
      oReportingServicesSubReport.ReportingServicesSubReportID.Load(oDataRow("ReportingServicesSubReportID"))
      oReportingServicesSubReport.Load()
      oReportingServicesSubReport.Delete()
    Next
  End Sub

  Public Sub PublishToReportingServices()
    Me.MessageToDisplay.SetToNullValue()

    Dim oReportingServicesReport As ReportingServicesReport = Me.ReportingServicesReport.Instance
    Dim oReportingServicesFolder As ReportingServicesFolder = oReportingServicesReport.ReportingServicesFolder.Instance
    Dim oReportingServicesServer As ReportingServicesServer = oReportingServicesReport.ReportingServicesServer.Instance

    SHBS.ReportingServices.ReportManager.CreateReport(oReportingServicesServer.ServerUrl.Value, oReportingServicesServer.RootFolderPath.Value, oReportingServicesFolder.ReportingServicesFolderPath.Value, Me.ReportName.Value, Me.ReportDesc.Value, Me.BinaryFile.Value, oReportingServicesFolder.DataSourceName.Value)
    Me.MessageToDisplay.Value = "Reporting Services Sub Report Successfully Published to: " & oReportingServicesServer.ReportingServicesServerDesc.Value
  End Sub

  Public Sub RemoveFromReportingServices()
    Me.MessageToDisplay.SetToNullValue()

    Dim oReportingServicesReport As ReportingServicesReport = Me.ReportingServicesReport.Instance
    Dim oReportingServicesFolder As ReportingServicesFolder = oReportingServicesReport.ReportingServicesFolder.Instance
    Dim oReportingServicesServer As ReportingServicesServer = oReportingServicesReport.ReportingServicesServer.Instance

    SHBS.ReportingServices.ReportManager.DeleteItem(oReportingServicesServer.ServerUrl.Value, oReportingServicesServer.RootFolderPath.Value, oReportingServicesFolder.ReportingServicesFolderPath.Value, Me.ReportName.Value)
    Me.MessageToDisplay.Value = "Reporting Services Sub Report Successfully Removed from: " & oReportingServicesServer.ReportingServicesServerDesc.Value
  End Sub

#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreate()
  End Sub

  Public Overrides Sub Load()
    MyBase.Load()
    BRLoad()
  End Sub

  Public Overrides Sub Save()
    With ApplicationSettings
      Try
        .BeginTransaction()
        Dim bIsNewYN As Boolean = Me.IsNew
        BRBeforeSave(bIsNewYN)
        MyBase.Save()
        BRAfterSave(bIsNewYN)
        .CommitTransaction()
      Catch Exception As Exception
        .RollbackTransaction()
        Throw Exception
      End Try
    End With
  End Sub

  Public Overrides Sub Delete()
    With ApplicationSettings
      Try
        .BeginTransaction()
        MyBase.Delete()
        BRDelete()
        .CommitTransaction()
      Catch Exception As Exception
        .RollbackTransaction()
        Throw Exception
      End Try
    End With
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreate()
    Me.UploadFileYN.Enabled = False
    Me.UploadFileYN.Value = True
    BRUploadFileYN(Me.UploadFileYN, True, False)
  End Sub

  Private Sub BRLoad()
    BRUploadFileYN(Me.UploadFileYN, True, False)
  End Sub

  Private Sub BRBeforeSave(ByVal IsNewYN As Boolean)
    Me.MessageToDisplay.SetToNullValue()
    If IsNewYN Then
      Me.ReportName.Value = System.IO.Path.GetFileNameWithoutExtension(Me.FileName.Value)
    End If
  End Sub

  Private Sub BRAfterSave(ByVal IsNewYN As Boolean)
    If IsNewYN Or Me.UploadFileYN.Value Then
      PublishToReportingServices()
    End If
  End Sub

  Private Sub BRDelete()
    RemoveFromReportingServices()
  End Sub

  Private Sub BRUploadFileYN(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If SetEnableDisableYN Then
      Me.BinaryFile.Mandatory = Element.ValueAsObject
    End If
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class ReportingServicesSubReportRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _ReportingServicesSubReportID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _ReportingServicesReportID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New ReportingServicesSubReport(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _ReportingServicesSubReportID.ForeignElement = CType(Instance, ReportingServicesSubReport).ReportingServicesSubReportID
    _ReportingServicesReportID.ForeignElement = CType(Instance, ReportingServicesSubReport)._ReportingServicesReportID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0ReportingServicesSubReport")
    oList.Columns.Add(New ListColumn("ReportingServicesReportID", "[A0ReportingServicesSubReport].[ReportingServicesReportID]", "ReportingServicesReportID", DataType.Integer, Nothing, True, 0, False, 100, "Reporting Services Report ID"))
    oList.Columns.Add(New ListColumn("ReportingServicesSubReportID", "[A0ReportingServicesSubReport].[ReportingServicesSubReportID]", "ReportingServicesSubReportID", DataType.Long, Nothing, True, 0, False, 100, "Reporting Services Sub Report ID"))
    oList.Columns.Add(New ListColumn("ReportName", "[A0ReportingServicesSubReport].[ReportName]", "ReportName", DataType.String, Nothing, False, 1, True, 200, "Report Name"))
    oList.Columns.Add(New ListColumn("ReportDesc", "[A0ReportingServicesSubReport].[ReportDesc]", "ReportDesc", DataType.String, Nothing, False, 0, True, 200, "Report Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "[ReportingServicesSubReport]  [A0ReportingServicesSubReport]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
