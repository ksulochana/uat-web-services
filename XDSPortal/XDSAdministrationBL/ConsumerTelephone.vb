Imports sembleWare.Runtime
Imports System
Public Class ConsumerTelephone 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly ConsumerTelephoneID As New IdentityElement("ConsumerTelephoneID", Me, True, Nothing, Nothing)
  Public ReadOnly TelephoneTypeInd As New IndicatorElement("TelephoneTypeInd", Me, False, False, True, True, True, True, "Telephone Type", Nothing, ConsumerTelephone.TelephoneTypeIndOptions, Nothing)
  Public ReadOnly TelephoneCode As New StringElement("TelephoneCode", Me, False, True, True, True, False, 5, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly TelephoneNo As New StringElement("TelephoneNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly EmailAddress As New StringElement("EmailAddress", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RecordStatusInd As New IndicatorElement("RecordStatusInd", Me, False, False, False, True, True, True, "Record Status", Nothing, ConsumerTelephone.RecordStatusIndOptions, "A")
  Public ReadOnly DeletedReason As New StringElement("DeletedReason", Me, False, True, True, True, False, 500, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LastUpdatedDate As New DateTimeElement("LastUpdatedDate", Me, False, False, True, True, True, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly IsVerifiedYN As New BooleanElement("IsVerifiedYN", Me, False, True, True, True, True, "Is Verified?", Nothing, False, "Yes;No")
  Public ReadOnly VerifiedDate As New DateTimeElement("VerifiedDate", Me, False, False, True, True, False, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Loader As Relationship = New LoaderRelationship("Loader", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Consumer As Relationship = New ConsumerRelationship("Consumer", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, False)
  Public ReadOnly _LoaderID As New IntegerElement("LoaderID", Me, False)
  Public ReadOnly _ConsumerID As New IntegerElement("ConsumerID", Me, True)
  Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
  Public ReadOnly _Loader As LoaderRelationship = Loader
  Public ReadOnly _Consumer As ConsumerRelationship = Consumer
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moTelephoneTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property TelephoneTypeIndOptions() As IndicatorOptions
    Get
      If moTelephoneTypeIndOptions Is Nothing Then
        moTelephoneTypeIndOptions = New IndicatorOptions
        moTelephoneTypeIndOptions.Add("H", "Home")
        moTelephoneTypeIndOptions.Add("W", "Work")
        moTelephoneTypeIndOptions.Add("F", "Fax")
        moTelephoneTypeIndOptions.Add("C", "Cellular")
        moTelephoneTypeIndOptions.Add("E", "Email Address")
      End If
      Return moTelephoneTypeIndOptions
    End Get
  End Property
  Private Shared moRecordStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property RecordStatusIndOptions() As IndicatorOptions
    Get
      If moRecordStatusIndOptions Is Nothing Then
        moRecordStatusIndOptions = New IndicatorOptions
        moRecordStatusIndOptions.Add("A", "Active")
        moRecordStatusIndOptions.Add("D", "Deleted")
      End If
      Return moRecordStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("ConsumerTelephone", ApplicationSettings)
    _Subscriber._SubscriberID.LocalElement = _SubscriberID
    _Loader._LoaderID.LocalElement = _LoaderID
    _Consumer._ConsumerID.LocalElement = _ConsumerID
    AddHandler IsVerifiedYN.Changed, AddressOf IsVerifiedYN_Changed
    'sembleWare: Constructor End - Do Not Modify
    Me.DeleteType = CustomDBPart.enumDeleteType.ChangeRecordStatus
  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub IsVerifiedYN_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BRIsVerifiedYN(Element, True, True)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Save()
    With ApplicationSettings
      .BeginTransaction()
      Try
        Dim bIsNewYN As Boolean = Me.IsNew
        BRBeforeSave(bIsNewYN)
        MyBase.Save()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
      .CommitTransaction()
    End With
  End Sub
#End Region

#Region " Business Rules"
  Public Sub BRBeforeSave(ByVal IsNewYN As Boolean)
    Me.LastUpdatedDate.Value = System.DateTime.Now
  End Sub

  Private Sub BRIsVerifiedYN(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If PerformRulesYN Then
      If Element.ValueAsObject Then
        Me.VerifiedDate.Value = System.DateTime.Now
      Else
        Me.VerifiedDate.SetToNullValue()
      End If
    End If
  End Sub
#End Region

End Class 'sembleWare: Part

Public Class ConsumerTelephoneRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _ConsumerTelephoneID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _ConsumerID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New ConsumerTelephone(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _ConsumerTelephoneID.ForeignElement = CType(Instance, ConsumerTelephone).ConsumerTelephoneID
    _ConsumerID.ForeignElement = CType(Instance, ConsumerTelephone)._ConsumerID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0ConsumerTelephone")
    oList.Columns.Add(New ListColumn("ConsumerID", "[A0ConsumerTelephone].[ConsumerID]", "ConsumerID", DataType.Integer, Nothing, True, 0, False, 100, "Consumer ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0ConsumerTelephone].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, False, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("LastUpdatedDate", "[A0ConsumerTelephone].[LastUpdatedDate]", "LastUpdatedDate", DataType.DateTime, "g", False, -1, True, 100, "Last Updated Date"))
    oList.Columns.Add(New ListColumn("TelephoneTypeInd", "[A0ConsumerTelephone].[TelephoneTypeInd]", "TelephoneTypeInd", DataType.String, Nothing, False, 2, True, 100, "Telephone Type"))
    oList.Columns.Add(New ListColumn("TelephoneCode", "[A0ConsumerTelephone].[TelephoneCode]", "TelephoneCode", DataType.String, Nothing, False, 0, True, 100, "Telephone Code"))
    oList.Columns.Add(New ListColumn("TelephoneNo", "[A0ConsumerTelephone].[TelephoneNo]", "TelephoneNo", DataType.String, Nothing, False, 0, True, 100, "Telephone No"))
    oList.Columns.Add(New ListColumn("EmailAddress", "[A0ConsumerTelephone].[EmailAddress]", "EmailAddress", DataType.String, Nothing, False, 0, True, 100, "Email Address"))
    oList.Columns.Add(New ListColumn("SubscriberID1", "[A1Subscriber].[SubscriberID]", "SubscriberID", DataType.Long, Nothing, False, 0, False, 75, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberName", "[A1Subscriber].[SubscriberName]", "SubscriberName", DataType.String, Nothing, False, 0, True, 150, "Subscriber Name"))
    oList.Columns.Add(New ListColumn("ConsumerTelephoneID", "[A0ConsumerTelephone].[ConsumerTelephoneID]", "ConsumerTelephoneID", DataType.Long, Nothing, True, 0, False, 100, "Consumer Telephone ID"))
    oList.Columns.Add(New ListColumn("LoaderID", "[A0ConsumerTelephone].[LoaderID]", "LoaderID", DataType.Integer, Nothing, False, 0, False, 100, "Loader ID"))
    oList.Columns.Add(New ListColumn("IsVerifiedYN", "[A0ConsumerTelephone].[IsVerifiedYN]", "IsVerifiedYN", DataType.Boolean, "Yes;No", False, 0, True, 75, "Is Verified?"))
    oList.Columns.Add(New ListColumn("VerifiedDate", "[A0ConsumerTelephone].[VerifiedDate]", "VerifiedDate", DataType.DateTime, "g", False, 0, True, 100, "Verified Date"))
    oList.SelectStatement = "select"
    oList.FromClause = "(([ConsumerTelephone]  [A0ConsumerTelephone]" + _
           "    left join [Subscriber]  [A1Subscriber] on [A0ConsumerTelephone].[SubscriberID] = [A1Subscriber].[SubscriberID])" + _
           "    join [Consumer]  [A2Consumer] on [A0ConsumerTelephone].[ConsumerID] = [A2Consumer].[ConsumerID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    oList.AndFilters.Add(New ListFilter("RecordStatusInd", "[A2Consumer].[RecordStatusInd]", Constants.RecordStatus.Active, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
    oList.AndFilters.Add(New ListFilter("RecordStatusInd1", "[A0ConsumerTelephone].[RecordStatusInd]", Constants.RecordStatus.Active, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
