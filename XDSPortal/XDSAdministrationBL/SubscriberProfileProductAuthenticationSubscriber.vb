Imports sembleWare.Runtime
Imports System
Public Class SubscriberProfileProductAuthenticationSubscriber 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly StatusInd As New IndicatorElement("StatusInd", Me, False, False, True, True, True, True, "Status", Nothing, SubscriberProfileProductAuthenticationSubscriber.StatusIndOptions, Nothing)
  Public ReadOnly SubscriberProfile As Relationship = New SubscriberProfileRelationship("SubscriberProfile", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly AuthenticationSubscriber As Relationship = New SubscriberRelationship("AuthenticationSubscriber", "Subscriber", RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _AuthenticationSubscriberID As New IntegerElement("AuthenticationSubscriberID", Me, True)
  Public ReadOnly _SubscriberProfile As SubscriberProfileRelationship = SubscriberProfile
  Public ReadOnly _AuthenticationSubscriber As SubscriberRelationship = AuthenticationSubscriber
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property StatusIndOptions() As IndicatorOptions
    Get
      If moStatusIndOptions Is Nothing Then
        moStatusIndOptions = New IndicatorOptions
        moStatusIndOptions.Add("A", "Active")
        moStatusIndOptions.Add("I", "Inactive")
      End If
      Return moStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberProfileProductAuthenticationSubscriber", ApplicationSettings)
    _SubscriberProfile._SubscriberID.LocalElement = _SubscriberID
    _AuthenticationSubscriber._SubscriberID.LocalElement = _AuthenticationSubscriberID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class SubscriberProfileProductAuthenticationSubscriberRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _AuthenticationSubscriberID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberProfileProductAuthenticationSubscriber(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberID.ForeignElement = CType(Instance, SubscriberProfileProductAuthenticationSubscriber)._SubscriberID
    _AuthenticationSubscriberID.ForeignElement = CType(Instance, SubscriberProfileProductAuthenticationSubscriber)._AuthenticationSubscriberID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberProfileProductAuthenticationSubscriber")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberProfileProductAuthenticationSubscriber].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("AuthenticationSubscriberID", "[A0SubscriberProfileProductAuthenticationSubscriber].[AuthenticationSubscriberID]", "AuthenticationSubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Authentication Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberID1", "[A1AuthenticationSubscriber].[SubscriberID]", "SubscriberID", DataType.Long, Nothing, False, 0, True, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberName", "[A1AuthenticationSubscriber].[SubscriberName]", "SubscriberName", DataType.String, Nothing, False, 1, True, 200, "Subscriber Name"))
    oList.Columns.Add(New ListColumn("StatusInd", "[A0SubscriberProfileProductAuthenticationSubscriber].[StatusInd]", "StatusInd", DataType.String, Nothing, False, 0, True, 100, "Status"))
    oList.SelectStatement = "select"
    oList.FromClause = "([SubscriberProfileProductAuthenticationSubscriber]  [A0SubscriberProfileProductAuthenticationSubscriber]" + _
           "    join [Subscriber]  [A1AuthenticationSubscriber] on [A0SubscriberProfileProductAuthenticationSubscriber].[AuthenticationSubscriberID] = [A1AuthenticationSubscriber].[SubscriberID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
