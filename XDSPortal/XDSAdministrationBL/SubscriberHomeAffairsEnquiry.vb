Imports sembleWare.Runtime
Imports System
Public Class SubscriberHomeAffairsEnquiry 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly SubscriberHomeAffairsEnquiryID As New IdentityElement("SubscriberHomeAffairsEnquiryID", Me, True, Nothing, Nothing)
  Public ReadOnly EnquiryDate As New DateTimeElement("EnquiryDate", Me, False, False, True, True, True, "Enquiry Date", Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly EnquiryStatusInd As New IndicatorElement("EnquiryStatusInd", Me, False, False, False, True, True, True, "Enquiry Status", Nothing, SubscriberHomeAffairsEnquiry.EnquiryStatusIndOptions, "Q")
  Public ReadOnly EnquiryResultInd As New IndicatorElement("EnquiryResultInd", Me, False, False, False, True, True, True, "Enquiry Result", Nothing, SubscriberHomeAffairsEnquiry.EnquiryResultIndOptions, "P")
  Public ReadOnly ProductPointValue As New DecimalElement("ProductPointValue", Me, False, False, True, True, True, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ExternalReference As New StringElement("ExternalReference", Me, False, True, True, True, False, 50, "External Reference", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly IDNo As New StringElement("IDNo", Me, False, True, True, True, False, 13, "ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Surname As New StringElement("Surname", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly MaidenName As New StringElement("MaidenName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FirstInitial As New StringElement("FirstInitial", Me, False, True, True, True, False, 1, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SecondInitial As New StringElement("SecondInitial", Me, False, True, True, True, False, 1, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FirstName As New StringElement("FirstName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SecondName As New StringElement("SecondName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly BirthDate As New DateTimeElement("BirthDate", Me, False, True, True, True, False, "Date of Birth", Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly ResultIDNo As New StringElement("ResultIDNo", Me, False, False, True, True, False, 13, "ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResultFirstInitial As New StringElement("ResultFirstInitial", Me, False, False, True, True, False, 1, "First Initial", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResultSecondInitial As New StringElement("ResultSecondInitial", Me, False, False, True, True, False, 1, "Second Initial", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResultFirstName As New StringElement("ResultFirstName", Me, False, False, True, True, False, 150, "First Name", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResultSecondName As New StringElement("ResultSecondName", Me, False, False, True, True, False, 150, "Second Name", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResultSurname As New StringElement("ResultSurname", Me, False, False, True, True, False, 150, "Surname", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResultBirthDate As New DateTimeElement("ResultBirthDate", Me, False, False, True, True, False, "Birth Date", Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly ResultHomeAffairsDeceasedStatus As New StringElement("ResultHomeAffairsDeceasedStatus", Me, False, True, True, True, False, 100, "Deceased Status", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResultHomeAffairsDeceasedDate As New DateTimeElement("ResultHomeAffairsDeceasedDate", Me, False, True, True, True, False, "Deceased Date", Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly ResultHomeAffairsCauseOfDeath As New StringElement("ResultHomeAffairsCauseOfDeath", Me, False, True, True, True, False, 100, "Cause Of Death", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Product As Relationship = New ProductRelationship("Product", Nothing, RelationshipType.Include, True, True, True, Me)
  Public ReadOnly SystemUser As Relationship = New SystemUserRelationship("SystemUser", Nothing, RelationshipType.Include, True, False, True, Me)
  Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly SubscriberHomeAffairsEnquiryResult_OwnMany As Relationship = New SubscriberHomeAffairsEnquiryResultRelationship("SubscriberHomeAffairsEnquiryResult", Nothing, "SubscriberHomeAffairsEnquiry", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _ProductID As New IntegerElement("ProductID", Me, False)
  Public ReadOnly _SystemUserID As New IntegerElement("SystemUserID", Me, False)
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _Product As ProductRelationship = Product
  Public ReadOnly _SystemUser As SystemUserRelationship = SystemUser
  Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
  Public ReadOnly _SubscriberHomeAffairsEnquiryResult_OwnMany As SubscriberHomeAffairsEnquiryResultRelationship = SubscriberHomeAffairsEnquiryResult_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moEnquiryStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property EnquiryStatusIndOptions() As IndicatorOptions
    Get
      If moEnquiryStatusIndOptions Is Nothing Then
        moEnquiryStatusIndOptions = New IndicatorOptions
        moEnquiryStatusIndOptions.Add("Q", "Queued")
        moEnquiryStatusIndOptions.Add("P", "Processing")
        moEnquiryStatusIndOptions.Add("C", "Completed")
      End If
      Return moEnquiryStatusIndOptions
    End Get
  End Property
  Private Shared moEnquiryResultIndOptions As IndicatorOptions
  Public Shared ReadOnly Property EnquiryResultIndOptions() As IndicatorOptions
    Get
      If moEnquiryResultIndOptions Is Nothing Then
        moEnquiryResultIndOptions = New IndicatorOptions
        moEnquiryResultIndOptions.Add("P", "Pending")
        moEnquiryResultIndOptions.Add("N", "No Record Found")
        moEnquiryResultIndOptions.Add("F", "Record Found")
        moEnquiryResultIndOptions.Add("M", "Multiple Records Found")
        moEnquiryResultIndOptions.Add("S", "Record Selected")
      End If
      Return moEnquiryResultIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberHomeAffairsEnquiry", ApplicationSettings)
    _Product._ProductID.LocalElement = _ProductID
    _SystemUser._SystemUserID.LocalElement = _SystemUserID
    _Subscriber._SubscriberID.LocalElement = _SubscriberID
    AddHandler Product.Changed, AddressOf Product_Changed
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub Product_Changed(ByVal Relationship As Relationship)
    BRProduct(Relationship, True, True)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"

  Public Sub Validate()
    Dim bAllowYN As Boolean = False
    Dim oException As Exception

    Select Case Me._ProductID.Value
      Case Constants.Product.Records.TraceConsumerIdentityVerification
        oException = New MissingValuesException("ID No", MyBase.Caption, "")
        If Not Me.IDNo.IsEmpty Then
          bAllowYN = True
        End If
      Case Else
        bAllowYN = True
    End Select

    If Not bAllowYN Then
      Throw New BusinessRuleException(oException.Message)
    End If
  End Sub

  Public Sub QueueMatch()
    Validate()
    Me.EnquiryStatusInd.Value = Constants.SubscriberHomeAffairsEnquiry.Statuses.Queued
    Me.Save()
  End Sub

    Public Sub Match()
        If Not Me.IDNo.IsEmpty Then
            Validate()
            Me.EnquiryStatusInd.Value = Constants.SubscriberHomeAffairsEnquiry.Statuses.Processing
            Me.Save()

            Dim oDataRow As DataRow
            Dim nEnquiryMatchingEngineID As Integer = EnquiryMatchingEngine.HomeAffairsMatch(Me.IDNo, Me.Surname, Me.FirstName, Me.SecondName, _
                                                                                            Me.BirthDate, Me._ProductID.Value, ApplicationSettings)
            Dim oString As Text.StringBuilder = New Text.StringBuilder

            oString.Append("insert into SubscriberHomeAffairsEnquiryResult(SubscriberID, SubscriberHomeAffairsEnquiryID, HomeAffairsID, DetailsViewedYN, HomeAffairsSelectedYN, IDNo, FirstName, SecondName, Surname, BirthDate, DeceasedStatus, DeceasedDate, CauseOfDeath)" & vbNewLine)
            oString.Append("select " & Me._SubscriberID.Value & ", " & Me.SubscriberHomeAffairsEnquiryID.Value & ", har.HomeAffairsID, 0, 0, ha.IDNo, ha.FirstName, ha.SecondName, ha.Surname, ha.BirthDate, isnull((select case isnull(ha.DeceasedDate, '') when '' then 'Active' else 'Deceased' end), 'Unknown'), ha.DeceasedDate, ha.CauseOfDeath" & vbNewLine)
            oString.Append("  from EnquiryMatchingEngineHomeAffairsResult har inner join EnquiryMatchingEngineHomeAffairs eha")
            oString.Append("                                                  on har.EnquiryMatchingEngineID = eha.EnquiryMatchingEngineID" & vbNewLine)
            oString.Append("                                                 and har.EnquiryMatchingEngineHomeAffairsID = eha.EnquiryMatchingEngineHomeAffairsID" & vbNewLine)
            oString.Append("                                               inner join HomeAffairs ha" & vbNewLine)
            oString.Append("                                                  on har.HomeAffairsID = ha.HomeAffairsID" & vbNewLine)
            oString.Append(" where eha.EnquiryMatchingEngineID = " & ApplicationSettings.QueryBuilder.ToSQL(nEnquiryMatchingEngineID) & vbNewLine)
            oString.Append("   and har.IgnoreRecordYN = 0")
            ApplicationSettings.ActionQuery(oString.ToString())

            oString = New Text.StringBuilder
            oString.Append("select count(har.HomeAffairsID)" & vbNewLine)
            oString.Append("  from EnquiryMatchingEngineHomeAffairsResult har inner join EnquiryMatchingEngineHomeAffairs eha" & vbNewLine)
            oString.Append("                                                  on har.EnquiryMatchingEngineID = eha.EnquiryMatchingEngineID" & vbNewLine)
            oString.Append("                                                 and har.EnquiryMatchingEngineHomeAffairsID = eha.EnquiryMatchingEngineHomeAffairsID" & vbNewLine)
            oString.Append(" where eha.EnquiryMatchingEngineID = " & ApplicationSettings.QueryBuilder.ToSQL(nEnquiryMatchingEngineID) & vbNewLine)
            oString.Append("   and har.IgnoreRecordYN = 0")
            For Each oDataRow In ApplicationSettings.ResultQuery(oString.ToString()).Rows
                Select Case System.Convert.ToInt32(oDataRow(0))
                    Case 0
                        Me.EnquiryResultInd.Value = Constants.SubscriberHomeAffairsEnquiry.Results.NoRecordFound
                    Case 1
                        Me.EnquiryResultInd.Value = Constants.SubscriberHomeAffairsEnquiry.Results.RecordFound
                    Case Else
                        Me.EnquiryResultInd.Value = Constants.SubscriberHomeAffairsEnquiry.Results.MultipleRecordsFound
                End Select
            Next

            oString = New Text.StringBuilder
            oString.Append("delete" & vbNewLine)
            oString.Append("  from EnquiryMatchingEngineHomeAffairsResult" & vbNewLine)
            oString.Append(" where EnquiryMatchingEngineID = " & ApplicationSettings.QueryBuilder.ToSQL(nEnquiryMatchingEngineID))
            ApplicationSettings.ActionQuery(oString.ToString())

            oString = New Text.StringBuilder
            oString.Append("delete" & vbNewLine)
            oString.Append("  from EnquiryMatchingEngineHomeAffairs" & vbNewLine)
            oString.Append(" where EnquiryMatchingEngineID = " & ApplicationSettings.QueryBuilder.ToSQL(nEnquiryMatchingEngineID))
            ApplicationSettings.ActionQuery(oString.ToString())

            Me.EnquiryStatusInd.Value = Constants.SubscriberHomeAffairsEnquiry.Statuses.Completed
            Me.Save()
        End If
    End Sub

#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Load()
    MyBase.Load()
    BRLoad()
  End Sub

  Public Overrides Sub Save()
    With ApplicationSettings
      Try
        Dim bIsNewYN As Boolean = Me.IsNew
        .BeginTransaction()
        BRBeforeSave(bIsNewYN)
        MyBase.Save()
        .CommitTransaction()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
    End With
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRLoad()
    Me.DisableElementsAndRelationship()
  End Sub

  Private Sub BRBeforeSave(ByVal IsNewYN As Boolean)
    If IsNewYN Then
      Me.EnquiryDate.Value = System.DateTime.Now
      ' If SystemUser is not empty, then the XDSAdministration Services has populated this value.
      If SystemUser.IsEmpty Then
        Me.SystemUser.Instance = XDSAdministrationBL.SystemUser.GetLoggedInSystemUser(ApplicationSettings)
      End If

      Me.ResultIDNo.Value = Me.IDNo.Value
      Me.ResultFirstName.ValueAsObject = Me.FirstInitial.ValueAsObject
      Me.ResultSecondName.ValueAsObject = Me.SecondInitial.ValueAsObject
      Me.ResultSurname.ValueAsObject = Me.Surname.ValueAsObject
      Me.ResultBirthDate.ValueAsObject = Me.BirthDate.ValueAsObject
    End If
    XDSAdministrationBL.Consumer.ValidateIDNo(Me.IDNo.Value, ApplicationSettings)
  End Sub

  Private Sub BRProduct(ByVal Relationship As Relationship, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    Dim oProduct As Product = Nothing

    If Not Relationship.IsEmpty Then
      oProduct = Relationship.Instance
    End If
    Utility.BRProductTraceHomeAffairs(oProduct, Me.IDNo, Me.Surname, _
                                      Me.FirstName, Me.SecondName, Me.BirthDate, _
                                      SetEnableDisableYN, PerformRulesYN)

    If PerformRulesYN Then
      If Not oProduct Is Nothing Then
        Dim oSubscriberProfile As SubscriberProfile = SubscriberProfile.GetSubscriberProfile(Me._SubscriberID.Value, ApplicationSettings)
        Dim oSubscriberProfileProduct As SubscriberProfileProduct = SubscriberProfileProduct.GetSubscriberProfileProduct(Me._SubscriberID.Value, oProduct.ProductID.Value, ApplicationSettings)

        If oSubscriberProfileProduct.OverrideDefaultPointValueYN.Value Then
          Me.ProductPointValue.Value = oSubscriberProfileProduct.OverridePointValue.Value
        Else
          Me.ProductPointValue.Value = oProduct.DefaultPointValue.Value
        End If
      End If
    End If
  End Sub
#End Region

#Region " Public Methods"
  Public Function GetSubscriberHomeAffairsEnquiryResult() As SubscriberHomeAffairsEnquiryResult
    If Me.EnquiryResultInd.Value = Constants.SubscriberHomeAffairsEnquiry.Results.RecordFound Or Me.EnquiryResultInd.Value = Constants.SubscriberHomeAffairsEnquiry.Results.RecordSelected Then
      Dim oList As List = Me._SubscriberHomeAffairsEnquiryResult_OwnMany.GridList
      Dim oDataTable As DataTable = oList.DataTable
      Dim oDataRow As DataRow = oDataTable.Rows(0)
      Dim oSubscriberHomeAffairsEnquiryResult As SubscriberHomeAffairsEnquiryResult = Me.SubscriberHomeAffairsEnquiryResult_OwnMany.NewInstance()

      oSubscriberHomeAffairsEnquiryResult._HomeAffairsID.Load(oDataRow("HomeAffairsID"))
      oSubscriberHomeAffairsEnquiryResult.Load()
      Return oSubscriberHomeAffairsEnquiryResult
    End If
    Return Nothing
  End Function
#End Region
End Class 'sembleWare: Part

Public Class SubscriberHomeAffairsEnquiryRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberHomeAffairsEnquiryID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberHomeAffairsEnquiry(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberHomeAffairsEnquiryID.ForeignElement = CType(Instance, SubscriberHomeAffairsEnquiry).SubscriberHomeAffairsEnquiryID
    _SubscriberID.ForeignElement = CType(Instance, SubscriberHomeAffairsEnquiry)._SubscriberID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"




  Public Function MultipleConsumerGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberHomeAffairsEnquiry")
    oList.Columns.Add(New ListColumn("SubscriberHomeAffairsEnquiryID", "[A0SubscriberHomeAffairsEnquiry].[SubscriberHomeAffairsEnquiryID]", "SubscriberHomeAffairsEnquiryID", DataType.Long, Nothing, True, 0, False, 100, "Subscriber Home Affairs Enquiry ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberHomeAffairsEnquiry].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("EnquiryDate", "[A0SubscriberHomeAffairsEnquiry].[EnquiryDate]", "EnquiryDate", DataType.DateTime, "g", False, -1, True, 100, "Enquiry Date"))
    oList.Columns.Add(New ListColumn("ResultIDNo", "[A0SubscriberHomeAffairsEnquiry].[ResultIDNo]", "ResultIDNo", DataType.String, Nothing, False, 2, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("ResultSurname", "[A0SubscriberHomeAffairsEnquiry].[ResultSurname]", "ResultSurname", DataType.String, Nothing, False, 0, True, 100, "Surname"))
    oList.Columns.Add(New ListColumn("ResultFirstName", "[A0SubscriberHomeAffairsEnquiry].[ResultFirstName]", "ResultFirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("ResultSecondName", "[A0SubscriberHomeAffairsEnquiry].[ResultSecondName]", "ResultSecondName", DataType.String, Nothing, False, 0, True, 100, "Second Name"))
    oList.Columns.Add(New ListColumn("ResultBirthDate", "[A0SubscriberHomeAffairsEnquiry].[ResultBirthDate]", "ResultBirthDate", DataType.DateTime, "d", False, 0, True, 100, "Birth Date"))
    oList.Columns.Add(New ListColumn("EnquiryStatusInd", "[A0SubscriberHomeAffairsEnquiry].[EnquiryStatusInd]", "EnquiryStatusInd", DataType.String, Nothing, False, 0, True, 100, "Enquiry Status"))
    oList.Columns.Add(New ListColumn("EnquiryResultInd", "[A0SubscriberHomeAffairsEnquiry].[EnquiryResultInd]", "EnquiryResultInd", DataType.String, Nothing, False, 0, True, 100, "Enquiry Result"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberHomeAffairsEnquiry]  [A0SubscriberHomeAffairsEnquiry]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function AuthenticationGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberHomeAffairsEnquiry")
    oList.Columns.Add(New ListColumn("SubscriberHomeAffairsEnquiryID", "[A0SubscriberHomeAffairsEnquiry].[SubscriberHomeAffairsEnquiryID]", "SubscriberHomeAffairsEnquiryID", DataType.Long, Nothing, True, 0, False, 100, "Subscriber Home Affairs Enquiry ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberHomeAffairsEnquiry].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("ResultIDNo", "[A0SubscriberHomeAffairsEnquiry].[ResultIDNo]", "ResultIDNo", DataType.String, Nothing, False, 1, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("ResultSurname", "[A0SubscriberHomeAffairsEnquiry].[ResultSurname]", "ResultSurname", DataType.String, Nothing, False, 0, True, 100, "Surname"))
    oList.Columns.Add(New ListColumn("ResultFirstName", "[A0SubscriberHomeAffairsEnquiry].[ResultFirstName]", "ResultFirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("ResultSecondName", "[A0SubscriberHomeAffairsEnquiry].[ResultSecondName]", "ResultSecondName", DataType.String, Nothing, False, 0, True, 100, "Second Name"))
    oList.Columns.Add(New ListColumn("ResultBirthDate", "[A0SubscriberHomeAffairsEnquiry].[ResultBirthDate]", "ResultBirthDate", DataType.DateTime, "d", False, 0, True, 100, "Birth Date"))
    oList.Columns.Add(New ListColumn("ResultHomeAffairsDeceasedStatus", "[A0SubscriberHomeAffairsEnquiry].[ResultHomeAffairsDeceasedStatus]", "ResultHomeAffairsDeceasedStatus", DataType.String, Nothing, False, 0, True, 100, "Deceased Status"))
    oList.Columns.Add(New ListColumn("ResultHomeAffairsDeceasedDate", "[A0SubscriberHomeAffairsEnquiry].[ResultHomeAffairsDeceasedDate]", "ResultHomeAffairsDeceasedDate", DataType.DateTime, "d", False, 0, True, 100, "Deceased Date"))
    oList.Columns.Add(New ListColumn("ResultHomeAffairsCauseOfDeath", "[A0SubscriberHomeAffairsEnquiry].[ResultHomeAffairsCauseOfDeath]", "ResultHomeAffairsCauseOfDeath", DataType.String, Nothing, False, 0, True, 100, "Cause Of Death"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberHomeAffairsEnquiry]  [A0SubscriberHomeAffairsEnquiry]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
