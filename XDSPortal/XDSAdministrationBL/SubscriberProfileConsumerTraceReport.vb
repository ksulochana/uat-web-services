Imports sembleWare.Runtime
Imports System
Public Class SubscriberProfileConsumerTraceReport 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly OverrideSubscriberAssociationDefaultsYN As New BooleanElement("OverrideSubscriberAssociationDefaultsYN", Me, False, True, True, True, True, "Override Subscriber Association Defaults?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplayIdentityInformationYN As New BooleanElement("OverrideDisplayIdentityInformationYN", Me, False, True, True, True, True, "Display Identity Information?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplayDeceasedIndicatorYN As New BooleanElement("OverrideDisplayDeceasedIndicatorYN", Me, False, True, True, True, True, "Display Deceased Indicator?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplayReportSummaryYN As New BooleanElement("OverrideDisplayReportSummaryYN", Me, False, True, True, True, True, "Display Report Summary?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplayPrioritizationRatingYN As New BooleanElement("OverrideDisplayPrioritizationRatingYN", Me, False, True, True, True, True, "Display Prioritization Rating?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplayMonthlyPaymentDetailsYN As New BooleanElement("OverrideDisplayMonthlyPaymentDetailsYN", Me, False, True, True, True, True, "Display Monthly Payment Details?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplayAdverseInfoYN As New BooleanElement("OverrideDisplayAdverseInfoYN", Me, False, True, True, True, True, "Display Public Domain - Adverse Information?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplayJudgementsYN As New BooleanElement("OverrideDisplayJudgementsYN", Me, False, True, True, True, True, "Display Public Domain - Judgements?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplayAdministrationOrdersYN As New BooleanElement("OverrideDisplayAdministrationOrdersYN", Me, False, True, True, True, True, "Display Public Domain - Administration Orders?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplaySequestrationsYN As New BooleanElement("OverrideDisplaySequestrationsYN", Me, False, True, True, True, True, "Display Public Domain - Sequestrations?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplayNameHistoryYN As New BooleanElement("OverrideDisplayNameHistoryYN", Me, False, True, True, True, True, "Display Name History?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplayAddressHistoryYN As New BooleanElement("OverrideDisplayAddressHistoryYN", Me, False, True, True, True, True, "Display Address History?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplayTelephoneHistoryYN As New BooleanElement("OverrideDisplayTelephoneHistoryYN", Me, False, True, True, True, True, "Display Telephone History?", Nothing, False, "Yes;No")
    Public ReadOnly OverrideDisplayEmploymentHistoryYN As New BooleanElement("OverrideDisplayEmploymentHistoryYN", Me, False, True, True, True, True, "Display Employment History?", Nothing, False, "Yes;No")
    Public ReadOnly OverrideDisplayAdditionalTelephoneSearchYN As New BooleanElement("OverrideDisplayAdditionalTelephoneSearchYN", Me, False, True, True, True, True, "Display Aditional Telephone Search Details?", Nothing, False, "Yes;No")
    Public ReadOnly OverrideDisplayScoringInformationYN As New BooleanElement("OverrideDisplayScoringInformationYN", Me, False, True, True, True, True, "Display Scoring Information?", Nothing, False, "Yes;No")
    Public ReadOnly OverrideDisplayCreditAccountSummaryYN As New BooleanElement("OverrideDisplayCreditAccountSummaryYN", Me, False, True, True, True, True, "Display Credit Account Summary Details?", Nothing, False, "Yes;No")
    Public ReadOnly OverrideDisplayFraudIndicatorsSummaryYN As New BooleanElement("OverrideDisplayFraudIndicatorsSummaryYN", Me, False, True, True, True, True, "Display Fraud Indicators Summary Details?", Nothing, False, "Yes;No")
    Public ReadOnly OverrideDisplaySemiDefaultInfoYN As New BooleanElement("OverrideDisplaySemiDefaultInfoYN", Me, False, True, True, True, True, "Display Semi Default Information?", Nothing, False, "Yes;No")

    Public ReadOnly OverrideDisplayPropertyInterestsYN As New BooleanElement("OverrideDisplayPropertyInterestsYN", Me, False, True, True, True, True, "Display Property Interests ?", Nothing, False, "Yes;No")
    Public ReadOnly OverrideDisplayTraceEnquiryHistoryYN As New BooleanElement("OverrideDisplayTraceEnquiryHistoryYN", Me, False, True, True, True, True, "Display Trace Enquiry History?", Nothing, False, "Yes;No")
    Public ReadOnly OverrideDisplayDebtReviewStatusYN As New BooleanElement("OverrideDisplayDebtReviewStatusYN", Me, False, True, True, True, True, "Display Debt Review Status ?", Nothing, False, "Yes;No")
    Public ReadOnly OverrideDisplayCreditAccountStatusYN As New BooleanElement("OverrideDisplayCreditAccountStatusYN", Me, False, True, True, True, True, "Display Credit Account Status ?", Nothing, False, "Yes;No")
    Public ReadOnly OverrideDisplayDebtSummaryNonCPAYN As New BooleanElement("OverrideDisplayDebtSummaryNonCPAYN", Me, False, True, True, True, True, "Display Debt Summary for Non CPA Members?", Nothing, False, "Yes;No")
    Public ReadOnly OverrideDisplayConfirmationPersonalDetailsYN As New BooleanElement("OverrideDisplayConfirmationPersonalDetailsYN", Me, False, True, True, True, True, "Display Consumer Personal Confirmation Details?", Nothing, False, "Yes;No")

    Public ReadOnly OverrideDisplayDirectorshipLinksYN As New BooleanElement("OverrideDisplayDirectorshipLinksYN", Me, False, True, True, True, True, "Display Directorship Links?", Nothing, False, "Yes;No")

    Public ReadOnly OverrideDisplayDebtSummaryYN As New BooleanElement("OverrideDisplayDebtSummaryYN", Me, False, True, True, True, True, "Display Debt Summary?", Nothing, False, "Yes;No")

  Public ReadOnly SubscriberProfile As Relationship = New SubscriberProfileRelationship("SubscriberProfile", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _SubscriberProfile As SubscriberProfileRelationship = SubscriberProfile
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberProfileConsumerTraceReport", ApplicationSettings)
    _SubscriberProfile._SubscriberID.LocalElement = _SubscriberID
    AddHandler OverrideSubscriberAssociationDefaultsYN.Changed, AddressOf OverrideSubscriberAssociationDefaultsYN_Changed
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub OverrideSubscriberAssociationDefaultsYN_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BROverrideSubscriberAssociationDefaultsYN(Element, True, True)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreateLoad()
  End Sub

  Public Overrides Sub Load()
    MyBase.Load()
    BRCreateLoad()
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreateLoad()
    BROverrideSubscriberAssociationDefaultsYN(Me.OverrideSubscriberAssociationDefaultsYN, True, False)
  End Sub

  Private Sub BROverrideSubscriberAssociationDefaultsYN(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If SetEnableDisableYN Then
      Me.OverrideDisplayAddressHistoryYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplayAddressHistoryYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplayAdministrationOrdersYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplayAdministrationOrdersYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplayAdverseInfoYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplayAdverseInfoYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplayDeceasedIndicatorYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplayDeceasedIndicatorYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplayEmploymentHistoryYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplayEmploymentHistoryYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplayIdentityInformationYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplayIdentityInformationYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplayJudgementsYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplayJudgementsYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplayMonthlyPaymentDetailsYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplayMonthlyPaymentDetailsYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplayNameHistoryYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplayNameHistoryYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplayPrioritizationRatingYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplayPrioritizationRatingYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplayReportSummaryYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplayReportSummaryYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplaySequestrationsYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplaySequestrationsYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplayTelephoneHistoryYN.Enabled = Element.ValueAsObject
            Me.OverrideDisplayTelephoneHistoryYN.Mandatory = Element.ValueAsObject
            Me.OverrideDisplayAdditionalTelephoneSearchYN.Enabled = Element.ValueAsObject
            Me.OverrideDisplayAdditionalTelephoneSearchYN.Mandatory = Element.ValueAsObject
            Me.OverrideDisplayScoringInformationYN.Enabled = Element.ValueAsObject
            Me.OverrideDisplayScoringInformationYN.Mandatory = Element.ValueAsObject

            Me.OverrideDisplayFraudIndicatorsSummaryYN.Enabled = Element.ValueAsObject
            Me.OverrideDisplayFraudIndicatorsSummaryYN.Mandatory = Element.ValueAsObject
            Me.OverrideDisplayCreditAccountSummaryYN.Enabled = Element.ValueAsObject
            Me.OverrideDisplayCreditAccountSummaryYN.Mandatory = Element.ValueAsObject
            Me.OverrideDisplaySemiDefaultInfoYN.Enabled = Element.ValueAsObject
            Me.OverrideDisplaySemiDefaultInfoYN.Mandatory = Element.ValueAsObject

            Me.OverrideDisplayPropertyInterestsYN.Enabled = Element.ValueAsObject
            Me.OverrideDisplayPropertyInterestsYN.Mandatory = Element.ValueAsObject

            Me.OverrideDisplayTraceEnquiryHistoryYN.Enabled = Element.ValueAsObject
            Me.OverrideDisplayTraceEnquiryHistoryYN.Mandatory = Element.ValueAsObject

            Me.OverrideDisplayDebtReviewStatusYN.Enabled = Element.ValueAsObject
            Me.OverrideDisplayDebtReviewStatusYN.Mandatory = Element.ValueAsObject

            Me.OverrideDisplayCreditAccountStatusYN.Enabled = Element.ValueAsObject
            Me.OverrideDisplayCreditAccountStatusYN.Mandatory = Element.ValueAsObject

            Me.OverrideDisplayDebtSummaryNonCPAYN.Enabled = Element.ValueAsObject
            Me.OverrideDisplayDebtSummaryNonCPAYN.Mandatory = Element.ValueAsObject

            Me.OverrideDisplayConfirmationPersonalDetailsYN.Enabled = Element.ValueAsObject
            Me.OverrideDisplayConfirmationPersonalDetailsYN.Mandatory = Element.ValueAsObject

            Me.OverrideDisplayDirectorshipLinksYN.Enabled = Element.ValueAsObject
            Me.OverrideDisplayDirectorshipLinksYN.Mandatory = Element.ValueAsObject

            Me.OverrideDisplayDebtSummaryYN.Enabled = Element.ValueAsObject
            Me.OverrideDisplayDebtSummaryYN.Mandatory = Element.ValueAsObject
    End If

    If PerformRulesYN Then
      Me.OverrideDisplayAddressHistoryYN.Value = False
      Me.OverrideDisplayAdministrationOrdersYN.Value = False
      Me.OverrideDisplayAdverseInfoYN.Value = False
      Me.OverrideDisplayDeceasedIndicatorYN.Value = False
      Me.OverrideDisplayEmploymentHistoryYN.Value = False
      Me.OverrideDisplayIdentityInformationYN.Value = False
      Me.OverrideDisplayJudgementsYN.Value = False
      Me.OverrideDisplayMonthlyPaymentDetailsYN.Value = False
      Me.OverrideDisplayNameHistoryYN.Value = False
      Me.OverrideDisplayPrioritizationRatingYN.Value = False
      Me.OverrideDisplayReportSummaryYN.Value = False
            Me.OverrideDisplayTelephoneHistoryYN.Value = False
            Me.OverrideDisplayAdditionalTelephoneSearchYN.Value = False
            Me.OverrideDisplayScoringInformationYN.Value = False
            Me.OverrideDisplaySequestrationsYN.Value = False
            Me.OverrideDisplayFraudIndicatorsSummaryYN.Value = False
            Me.OverrideDisplayCreditAccountSummaryYN.Value = False
            Me.OverrideDisplaySemiDefaultInfoYN.Value = False

            Me.OverrideDisplayPropertyInterestsYN.Value = False
            Me.OverrideDisplayTraceEnquiryHistoryYN.Value = False
            Me.OverrideDisplayDebtReviewStatusYN.Value = False
            Me.OverrideDisplayCreditAccountStatusYN.Value = False
            Me.OverrideDisplayDebtSummaryNonCPAYN.Value = False
            Me.OverrideDisplayConfirmationPersonalDetailsYN.Value = False
            Me.OverrideDisplayDirectorshipLinksYN.Value = False

            Me.OverrideDisplayDebtSummaryYN.Value = False
    End If
  End Sub
#End Region

#Region " Shared Procedures"
  Public Shared Function GetSubscriberProfileConsumerTraceReport(ByVal SubscriberID As Long, ByVal ApplicationSettings As ApplicationSettings) As SubscriberProfileConsumerTraceReport
    Dim oSubscriberProfileConsumerTraceReport As SubscriberProfileConsumerTraceReport = New SubscriberProfileConsumerTraceReportRelationship(ApplicationSettings).NewInstance
    Try
      oSubscriberProfileConsumerTraceReport._SubscriberID.Load(SubscriberID)
      oSubscriberProfileConsumerTraceReport.Load()
    Catch oException As sembleWare.Runtime.RecordNotFoundException
      oSubscriberProfileConsumerTraceReport = New SubscriberProfileConsumerTraceReportRelationship(ApplicationSettings).NewInstance
      oSubscriberProfileConsumerTraceReport._SubscriberID.Load(SubscriberID)
    Catch oException As Exception
      Throw oException
    End Try
    Return oSubscriberProfileConsumerTraceReport
  End Function
#End Region
End Class 'sembleWare: Part

Public Class SubscriberProfileConsumerTraceReportRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberProfileConsumerTraceReport(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberID.ForeignElement = CType(Instance, SubscriberProfileConsumerTraceReport)._SubscriberID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
