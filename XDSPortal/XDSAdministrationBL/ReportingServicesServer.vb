Imports sembleWare.Runtime
Imports System
Public Class ReportingServicesServer 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly ReportingServicesServerID As New IdentityElement("ReportingServicesServerID", Me, True, "Server ID", Nothing)
  Public ReadOnly ReportingServicesServerDesc As New StringElement("ReportingServicesServerDesc", Me, False, True, True, True, True, 100, "Server Description", "ReportingServicesServerDesc", Nothing, Nothing, Nothing)
  Public ReadOnly ServerUrl As New StringElement("ServerUrl", Me, False, True, True, True, True, 250, "Server Url", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RootFolderPath As New StringElement("RootFolderPath", Me, False, True, True, True, True, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RootFolderDesc As New StringElement("RootFolderDesc", Me, False, True, True, True, False, 100, "Root Folder Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ReportingServicesServerLabelDescription As New StringElement("ReportingServicesServerLabelDescription", Me, False, True, True, False, False, 250, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly ReportingServicesFolder_OwnMany As Relationship = New ReportingServicesFolderRelationship("ReportingServicesFolder", Nothing, "ReportingServicesServer", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  Public ReadOnly _ReportingServicesFolder_OwnMany As ReportingServicesFolderRelationship = ReportingServicesFolder_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("ReportingServicesServer", ApplicationSettings)
    AddHandler RootFolderPath.Changed, AddressOf RootFolderPath_Changed
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub ReportingServicesServerRoot_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    'If ReportingServicesServerRoot.Value.StartsWith("/") Or ReportingServicesServerRoot.Value.EndsWith("/") Then
    '  Throw New BusinessRuleException("Please remove any / characters from the server root")
    'End If
  End Sub
  Private Sub RootFolder_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
  End Sub
  Private Sub RootFolderPath_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"

  Public Sub VerifyReportServer()
    Me.MessageToDisplay.SetToNullValue()

    ' Verify Server and Root Path
    PublishToReportingServices()

    ' Verify Folders
    Dim oList As List = Me._ReportingServicesFolder_OwnMany.GridList
    Dim oDataRow As DataRow

    For Each oDataRow In oList.DataTable.Rows
      Dim oReportingServicesFolder As ReportingServicesFolder = Me.ReportingServicesFolder_OwnMany.NewInstance()
      oReportingServicesFolder.ReportingServicesFolderPath.Load(oDataRow("ReportingServicesFolderPath"))
      oReportingServicesFolder.Load()
      oReportingServicesFolder.PublishToReportingServices()
    Next

    ' Verify Reports
    Dim oXDSAdministrationRoot As XDSAdministrationRoot = Me.XDSAdministrationRoot.Instance
    oList = oXDSAdministrationRoot._ReportingServicesReport_OwnMany.GridList
    oList.LimitByRelatedPart(Me, "ReportingServicesServer")

    For Each oDataRow In oList.DataTable.Rows
      Dim oReportingServicesReport As ReportingServicesReport = oXDSAdministrationRoot.ReportingServicesReport_OwnMany.NewInstance()
      oReportingServicesReport.ReportingServicesReportID.Load(oDataRow("ReportingServicesReportID"))
      oReportingServicesReport.Load()
      oReportingServicesReport.PublishToReportingServices()

      Dim oList2 As List = oReportingServicesReport._ReportingServicesSubReport_OwnMany.GridList
      Dim oDataRow2 As DataRow

      For Each oDataRow2 In oList2.DataTable.Rows
        Dim oReportingServicesSubReport As ReportingServicesSubReport = oReportingServicesReport.ReportingServicesSubReport_OwnMany.NewInstance()
        oReportingServicesSubReport.ReportingServicesSubReportID.Load(oDataRow2("ReportingServicesSubReportID"))
        oReportingServicesSubReport.Load()
        oReportingServicesSubReport.PublishToReportingServices()
      Next
    Next

    Me.MessageToDisplay.Value = "Reporting Services Report Successfully Verified on: " & Me.ReportingServicesServerDesc.Value
  End Sub

  Public Sub PublishToReportingServices()
    Me.MessageToDisplay.SetToNullValue()

    SHBS.ReportingServices.ReportManager.CreateFolder(Me.ServerUrl.Value, "", Me.RootFolderPath.Value, Me.RootFolderDesc.Value & " Root Folder")

    Me.MessageToDisplay.Value = "Reporting Services Root Folder Successfully Published to: " & Me.ReportingServicesServerDesc.Value
  End Sub

  Public Sub RemoveFromReportingServices()
    Me.MessageToDisplay.SetToNullValue()

    SHBS.ReportingServices.ReportManager.DeleteItem(Me.ServerUrl.Value, "", Me.RootFolderPath.Value, "")

    Me.MessageToDisplay.Value = "Reporting Services Root Folder Successfully Removed from: " & Me.ReportingServicesServerDesc.Value
  End Sub

#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreateLoad()
  End Sub

  Public Overrides Sub Load()
    MyBase.Load()
    BRCreateLoad()
  End Sub

  Public Overrides Sub Save()
    With ApplicationSettings
      Try
        .BeginTransaction()
        Dim bIsNewYN As Boolean = Me.IsNew
        BRBeforeSave(bIsNewYN)
        MyBase.Save()
        BRAfterSave(bIsNewYN)
        .CommitTransaction()
      Catch Exception As Exception
        .RollbackTransaction()
        Throw Exception
      End Try
    End With
  End Sub

  Public Overrides Sub Delete()
    With ApplicationSettings
      Try
        .BeginTransaction()
        MyBase.Delete()
        BRDelete()
        .CommitTransaction()
      Catch Exception As Exception
        .RollbackTransaction()
        Throw Exception
      End Try
    End With
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreateLoad()
    BRReportingServicesServerLabelDescription()
  End Sub

  Private Sub BRBeforeSave(ByVal IsNewYN As Boolean)
    Me.MessageToDisplay.SetToNullValue()
  End Sub

  Private Sub BRAfterSave(ByVal IsNewYN As Boolean)
    If IsNewYN Then
      PublishToReportingServices()
    End If
    BRReportingServicesServerLabelDescription()
  End Sub

  Private Sub BRDelete()
    RemoveFromReportingServices()
  End Sub

  Private Sub BRReportingServicesServerLabelDescription()
    Me.ReportingServicesServerLabelDescription.Value = CreateLabelDescription("Server", Me.ReportingServicesServerDesc)
  End Sub
#End Region

#Region " Shared Procedures"
  Public Shared Sub VerifyAllReportServers(ByVal ApplicationSettings As ApplicationSettings)
    Dim oXDSAdministrationRoot As XDSAdministrationRoot = New XDSAdministrationRoot(ApplicationSettings)
    Dim oList As List = oXDSAdministrationRoot._ReportingServicesServer_OwnMany.GridList
    Dim oDataRow As DataRow

    For Each oDataRow In oList.DataTable.Rows
      Dim oReportingServicesServer As ReportingServicesServer = oList.NewInstance()

      oReportingServicesServer.ReportingServicesServerID.Load(oDataRow("ReportingServicesServerID"))
      oReportingServicesServer.Load()

      oReportingServicesServer.VerifyReportServer()
    Next
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class ReportingServicesServerRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _ReportingServicesServerID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New ReportingServicesServer(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _ReportingServicesServerID.ForeignElement = CType(Instance, ReportingServicesServer).ReportingServicesServerID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0ReportingServicesServer")
    oList.Columns.Add(New ListColumn("ReportingServicesServerID", "[A0ReportingServicesServer].[ReportingServicesServerID]", "ReportingServicesServerID", DataType.Long, Nothing, True, 1, True, 100, "Server ID"))
    oList.Columns.Add(New ListColumn("ReportingServicesServerDesc", "[A0ReportingServicesServer].[ReportingServicesServerDesc]", "ReportingServicesServerDesc", DataType.String, Nothing, False, 0, True, 200, "Server Description"))
    oList.Columns.Add(New ListColumn("ServerUrl", "[A0ReportingServicesServer].[ServerUrl]", "ServerUrl", DataType.String, Nothing, False, 0, True, 200, "Server Url"))
    oList.Columns.Add(New ListColumn("RootFolderPath", "[A0ReportingServicesServer].[RootFolderPath]", "RootFolderPath", DataType.String, Nothing, False, 0, True, 100, "Root Folder Path"))
    oList.SelectStatement = "select"
    oList.FromClause = "[ReportingServicesServer]  [A0ReportingServicesServer]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0ReportingServicesServer")
    oList.Columns.Add(New ListColumn("ReportingServicesServerID", "[A0ReportingServicesServer].[ReportingServicesServerID]", "ReportingServicesServerID", DataType.Long, Nothing, True, 0, False, 100, "Server ID"))
    oList.Columns.Add(New ListColumn("ReportingServicesServerDesc", "[A0ReportingServicesServer].[ReportingServicesServerDesc]", "ReportingServicesServerDesc", DataType.String, Nothing, False, 1, True, 200, "Server Description"))
    oList.Columns.Add(New ListColumn("RootFolderPath", "[A0ReportingServicesServer].[RootFolderPath]", "RootFolderPath", DataType.String, Nothing, False, 0, True, 100, "Root Folder Path"))
    oList.SelectStatement = "select"
    oList.FromClause = "[ReportingServicesServer]  [A0ReportingServicesServer]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function DropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0ReportingServicesServer")
    oList.Columns.Add(New ListColumn("ReportingServicesServerID", "[A0ReportingServicesServer].[ReportingServicesServerID]", "ReportingServicesServerID", DataType.Long, Nothing, True, 0, False, 100, "Server ID"))
    oList.Columns.Add(New ListColumn("ReportingServicesServerDesc", "[A0ReportingServicesServer].[ReportingServicesServerDesc]", "ReportingServicesServerDesc", DataType.String, Nothing, False, 1, True, 200, "Server Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "[ReportingServicesServer]  [A0ReportingServicesServer]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
