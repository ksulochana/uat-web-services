Imports sembleWare.Runtime
Imports System
Public Class CommercialType 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly CommercialTypeCode As New StringElement("CommercialTypeCode", Me, True, True, True, True, True, 10, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CommercialTypeDesc As New StringElement("CommercialTypeDesc", Me, False, True, True, True, True, 50, "Commercial Type Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("CommercialType", ApplicationSettings)
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class CommercialTypeRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _CommercialTypeCode As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New CommercialType(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _CommercialTypeCode.ForeignElement = CType(Instance, CommercialType).CommercialTypeCode
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0CommercialType")
    oList.Columns.Add(New ListColumn("CommercialTypeCode", "[A0CommercialType].[CommercialTypeCode]", "CommercialTypeCode", DataType.String, Nothing, True, 1, True, 100, "Commercial Type Code"))
    oList.Columns.Add(New ListColumn("CommercialTypeDesc", "[A0CommercialType].[CommercialTypeDesc]", "CommercialTypeDesc", DataType.String, Nothing, False, 0, True, 200, "Commercial Type Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "[CommercialType]  [A0CommercialType]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function DropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0CommercialType")
    oList.Columns.Add(New ListColumn("CommercialTypeCode", "[A0CommercialType].[CommercialTypeCode]", "CommercialTypeCode", DataType.String, Nothing, True, 1, True, 100, "Commercial Type Code"))
    oList.Columns.Add(New ListColumn("CommercialTypeDesc", "[A0CommercialType].[CommercialTypeDesc]", "CommercialTypeDesc", DataType.String, Nothing, False, 0, True, 200, "Commercial Type Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "[CommercialType]  [A0CommercialType]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
