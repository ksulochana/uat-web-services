Imports sembleWare.Runtime
Imports System
Public Class SubscriberCommercial 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly SubscriberCommercialID As New IdentityElement("SubscriberCommercialID", Me, True, Nothing, Nothing)
  Public ReadOnly EnquiryDate As New DateTimeElement("EnquiryDate", Me, False, False, True, True, False, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly ReferenceNo As New StringElement("ReferenceNo", Me, False, False, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ExternalReference As New StringElement("ExternalReference", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ReportFile As New BinaryElement("ReportFile", Me, False, True, True, False, Nothing, Nothing)
  Public ReadOnly ReportFileMimeType As New StringElement("ReportFileMimeType", Me, False, False, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SubscriberCommercialEnquiry As Relationship = New SubscriberCommercialEnquiryRelationship("SubscriberCommercialEnquiry", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Product As Relationship = New ProductRelationship("Product", Nothing, RelationshipType.Include, True, False, True, Me)
  Public ReadOnly ActionedBySystemUser As Relationship = New SystemUserRelationship("ActionedBySystemUser", "Actioned By", RelationshipType.Include, True, False, True, Me)
  Public ReadOnly SubscriberBillingGroup As Relationship = New SubscriberBillingGroupRelationship("SubscriberBillingGroup", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly Commercial As Relationship = New CommercialRelationship("Commercial", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly SubscriberDirectorEnquiry As Relationship = New SubscriberDirectorEnquiryRelationship("SubscriberDirectorEnquiry", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Director As Relationship = New DirectorRelationship("Director", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberCommercialEnquiryID As New IntegerElement("SubscriberCommercialEnquiryID", Me, False)
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _ProductID As New IntegerElement("ProductID", Me, False)
  Public ReadOnly _ActionedBySystemUserID As New IntegerElement("ActionedBySystemUserID", Me, False)
  Public ReadOnly _SubscriberBillingGroupCode As New StringElement("SubscriberBillingGroupCode", Me, False)
  Public ReadOnly _CommercialID As New IntegerElement("CommercialID", Me, False)
  Public ReadOnly _SubscriberDirectorEnquiryID As New IntegerElement("SubscriberDirectorEnquiryID", Me, False)
  Public ReadOnly _DirectorID As New IntegerElement("DirectorID", Me, False)
  Public ReadOnly _SubscriberCommercialEnquiry As SubscriberCommercialEnquiryRelationship = SubscriberCommercialEnquiry
  Public ReadOnly _Product As ProductRelationship = Product
  Public ReadOnly _ActionedBySystemUser As SystemUserRelationship = ActionedBySystemUser
  Public ReadOnly _SubscriberBillingGroup As SubscriberBillingGroupRelationship = SubscriberBillingGroup
  Public ReadOnly _Commercial As CommercialRelationship = Commercial
  Public ReadOnly _SubscriberDirectorEnquiry As SubscriberDirectorEnquiryRelationship = SubscriberDirectorEnquiry
  Public ReadOnly _Director As DirectorRelationship = Director
  Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberCommercial", ApplicationSettings)
    _SubscriberCommercialEnquiry._SubscriberCommercialEnquiryID.LocalElement = _SubscriberCommercialEnquiryID
    _SubscriberCommercialEnquiry._SubscriberID.LocalElement = _SubscriberID
    _Product._ProductID.LocalElement = _ProductID
    _ActionedBySystemUser._SystemUserID.LocalElement = _ActionedBySystemUserID
    _SubscriberBillingGroup._SubscriberBillingGroupCode.LocalElement = _SubscriberBillingGroupCode
    _Commercial._CommercialID.LocalElement = _CommercialID
    _SubscriberDirectorEnquiry._SubscriberDirectorEnquiryID.LocalElement = _SubscriberDirectorEnquiryID
    _SubscriberDirectorEnquiry._SubscriberID.LocalElement = _SubscriberID
    _Director._DirectorID.LocalElement = _DirectorID
    _Subscriber._SubscriberID.LocalElement = _SubscriberID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Save()
    With ApplicationSettings
      Try
        .BeginTransaction()
        Dim bIsNewYN As Boolean = Me.IsNew

        BRBeforeSave(bIsNewYN)
        MyBase.Save()
        BRAfterSave(bIsNewYN)
        .CommitTransaction()
      Catch Exception As Exception
        .RollbackTransaction()
        Throw Exception
      End Try
    End With
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRBeforeSave(ByVal IsNewYN As Boolean)
    If IsNewYN Then
      Me.EnquiryDate.Value = System.DateTime.Now
      ' If ActionedBySystemUser is not empty, then the XDSAdministration Services has populated this value.
      If ActionedBySystemUser.IsEmpty Then
        Me.ActionedBySystemUser.Instance = SystemUser.GetLoggedInSystemUser(ApplicationSettings)
      End If
    Else
      BRGetReferenceNo()
    End If
  End Sub

  Private Sub BRAfterSave(ByVal IsNewYN As Boolean)
    If IsNewYN Then
      BRGetReferenceNo()
      Me.Save()
    End If
  End Sub

  Private Sub BRGetReferenceNo()
    Select Case Me._ProductID.Value
      Case Constants.Product.Records.EnquiryCommercialDirector
        Me.ReferenceNo.Value = "CD" & Me.SubscriberCommercialID.Value.ToString() & "-" & Me._DirectorID.Value.ToString()
      Case Else
        Me.ReferenceNo.Value = "CC" & Me.SubscriberCommercialID.Value.ToString() & "-" & Me._CommercialID.Value.ToString()
    End Select
  End Sub
#End Region

#Region " Public Methods"
  Public Sub UpdateSubscriberCommercialEnquiry(ByVal SubscriberCommercialEnquiry As SubscriberCommercialEnquiry)
    Select Case SubscriberCommercialEnquiry.EnquiryResultInd.Value
      Case Constants.SubscriberCommercialEnquiry.Results.RecordFound
        Dim oSystemUserProfile As SystemUserProfile = XDSAdministrationBL.SystemUserProfile.GetLoggedInSystemUserProfile(ApplicationSettings)

        If Not oSystemUserProfile.ConfirmPurchaseYN.Value Then
          Dim oSubscriberCommercialEnquiryResult As SubscriberCommercialEnquiryResult = SubscriberCommercialEnquiry.GetSubscriberCommercialEnquiryResult()
          oSubscriberCommercialEnquiryResult.SelectRecord()
          Me.Commercial.Instance = oSubscriberCommercialEnquiryResult.Commercial.Instance
        End If
    End Select

    Me.Product.Instance = SubscriberCommercialEnquiry.Product.Instance
    Me.SubscriberCommercialEnquiry.Instance = SubscriberCommercialEnquiry
    Me.Save()
  End Sub

  Public Sub UpdateSubscriberCommercialEnquiry(ByVal SubscriberCommercialEnquiryResult As SubscriberCommercialEnquiryResult)
    Dim oSubscriberCommercialEnquiry As SubscriberCommercialEnquiry = SubscriberCommercialEnquiryResult.SubscriberCommercialEnquiry.Instance
    UpdateSubscriberCommercialEnquiry(oSubscriberCommercialEnquiry)
    Me.Commercial.Instance = SubscriberCommercialEnquiryResult.Commercial.Instance
    Me.Save()
  End Sub

  Public Sub UpdateSubscriberDirectorEnquiry(ByVal SubscriberDirectorEnquiry As SubscriberDirectorEnquiry)
    Select Case SubscriberDirectorEnquiry.EnquiryResultInd.Value
      Case Constants.SubscriberDirectorEnquiry.Results.RecordFound
        Dim oSystemUserProfile As SystemUserProfile = XDSAdministrationBL.SystemUserProfile.GetLoggedInSystemUserProfile(ApplicationSettings)

        If Not oSystemUserProfile.ConfirmPurchaseYN.Value Then
          Dim oSubscriberDirectorEnquiryResult As SubscriberDirectorEnquiryResult = SubscriberDirectorEnquiry.GetSubscriberDirectorEnquiryResult()
          oSubscriberDirectorEnquiryResult.SelectRecord()
          Me.Director.Instance = oSubscriberDirectorEnquiryResult.Director.Instance
        End If
    End Select

    Me.Product.Instance = SubscriberDirectorEnquiry.Product.Instance
    Me.SubscriberDirectorEnquiry.Instance = SubscriberDirectorEnquiry
    Me.Save()
  End Sub

  Public Sub UpdateSubscriberDirectorEnquiry(ByVal SubscriberDirectorEnquiryResult As SubscriberDirectorEnquiryResult)
    Dim oSubscriberDirectorEnquiry As SubscriberDirectorEnquiry = SubscriberDirectorEnquiryResult.SubscriberDirectorEnquiry.Instance
    UpdateSubscriberDirectorEnquiry(oSubscriberDirectorEnquiry)
    Me.Director.Instance = SubscriberDirectorEnquiryResult.Director.Instance
    Me.Save()
  End Sub

  Public Sub GenerateReport(ByVal ReportImageUrl As String, ByVal ReportImagePath As String)
    Me.Save()

    Dim sMimeType As String
    Dim oProduct As Product = XDSAdministrationBL.Product.GetProduct(Me._ProductID.Value, ApplicationSettings)

    If Not oProduct Is Nothing Then
      Utility.BRCheckProductReport(oProduct)

      Dim oReportingServicesReport As ReportingServicesReport = Utility.BRGetProductReportingServicesReport(SubscriberProfile.GetSubscriberProfile(Me._SubscriberID.Value, ApplicationSettings), oProduct)
      Dim oReportCommercialDetail As ReportCommercialDetail = New ReportCommercialDetailRelationship(ApplicationSettings).NewInstance()

      oReportCommercialDetail.ReportingServicesReportFormat.Value = SystemUserProfile.GetSystemUserProfile(Me._ActionedBySystemUserID.Value, ApplicationSettings).DefaultTraceEnquiryReportTypeInd.Value
      oReportCommercialDetail.ReportingServicesReport.Instance = oReportingServicesReport
      oReportCommercialDetail.ReportImageUrl.Value = ReportImageUrl
      oReportCommercialDetail.ReportImagePath.Value = ReportImagePath
      oReportCommercialDetail.SubscriberID.Value = Me._SubscriberID.Value
      oReportCommercialDetail.SubscriberCommercialID.Value = Me.SubscriberCommercialID.Value
      With oReportCommercialDetail.ReportParameterCollection
        .Add("SubscriberID", oReportCommercialDetail.SubscriberID.Value, oReportCommercialDetail.SubscriberID.ElementType)
        .Add("SubscriberCommercialID", oReportCommercialDetail.SubscriberCommercialID.Value, oReportCommercialDetail.SubscriberCommercialID.ElementType)
      End With
      Me.ReportFile.Value = oReportCommercialDetail.RunReport(sMimeType)
      Me.ReportFileMimeType.Value = sMimeType
    End If
    Me.Save()
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class SubscriberCommercialRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberCommercialID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberCommercial(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberCommercialID.ForeignElement = CType(Instance, SubscriberCommercial).SubscriberCommercialID
    _SubscriberID.ForeignElement = CType(Instance, SubscriberCommercial)._SubscriberID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function SubscriberGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberCommercial")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberCommercial].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberCommercialID", "[A0SubscriberCommercial].[SubscriberCommercialID]", "SubscriberCommercialID", DataType.Long, Nothing, True, 0, False, 100, "Subscriber Commercial ID"))
    oList.Columns.Add(New ListColumn("ReferenceNo", "[A0SubscriberCommercial].[ReferenceNo]", "ReferenceNo", DataType.String, Nothing, False, 0, True, 100, "Reference No"))
    oList.Columns.Add(New ListColumn("EnquiryDate", "[A0SubscriberCommercial].[EnquiryDate]", "EnquiryDate", DataType.DateTime, "g", False, -1, True, 100, "Enquiry Date"))
    oList.Columns.Add(New ListColumn("ProductTypeDesc", "[A2ProductType].[ProductTypeDesc]", "ProductTypeDesc", DataType.String, Nothing, False, 0, False, 200, "Product Type Description"))
    oList.Columns.Add(New ListColumn("ProductDesc", "[A1Product].[ProductDesc]", "ProductDesc", DataType.String, Nothing, False, 0, True, 200, "Product Description"))
    oList.Columns.Add(New ListColumn("DefaultPointValue", "[A1Product].[DefaultPointValue]", "DefaultPointValue", DataType.Decimal, Nothing, False, 0, False, 100, "Default Point Value"))
    oList.Columns.Add(New ListColumn("ProductDetails", "[A1Product].[ProductDetails]", "ProductDetails", DataType.Text, Nothing, False, 0, False, 200, "Product Details"))
    oList.Columns.Add(New ListColumn("FullName", "isnull([A3ActionedBySystemUser].[FirstName], '') + ' ' + isnull([A3ActionedBySystemUser].[Surname], '')", "FullName", DataType.String, Nothing, False, 0, True, 200, "Actioned By"))
    oList.SelectStatement = "select"
    oList.FromClause = "(([SubscriberCommercial]  [A0SubscriberCommercial]" + _
           "    join ([Product]  [A1Product]" + _
           "    join [ProductType]  [A2ProductType] on [A1Product].[ProductTypeID] = [A2ProductType].[ProductTypeID]) on [A0SubscriberCommercial].[ProductID] = [A1Product].[ProductID])" + _
           "    join [SystemUser]  [A3ActionedBySystemUser] on [A0SubscriberCommercial].[ActionedBySystemUserID] = [A3ActionedBySystemUser].[SystemUserID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function





  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberCommercial")
    oList.Columns.Add(New ListColumn("SubscriberCommercialID", "[A0SubscriberCommercial].[SubscriberCommercialID]", "SubscriberCommercialID", DataType.Long, Nothing, True, 0, False, 100, "Subscriber Commercial ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberCommercial].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberCommercialEnquiryID", "[A1SubscriberCommercialEnquiry].[SubscriberCommercialEnquiryID]", "SubscriberCommercialEnquiryID", DataType.Long, Nothing, False, 0, True, 100, "Subscriber Commercial Enquiry ID"))
    oList.Columns.Add(New ListColumn("EnquiryDate", "[A1SubscriberCommercialEnquiry].[EnquiryDate]", "EnquiryDate", DataType.DateTime, "g", False, 0, True, 100, "Enquiry Date"))
    oList.Columns.Add(New ListColumn("EnquiryStatusInd", "[A1SubscriberCommercialEnquiry].[EnquiryStatusInd]", "EnquiryStatusInd", DataType.String, Nothing, False, 0, True, 100, "Enquiry Status"))
    oList.Columns.Add(New ListColumn("EnquiryResultInd", "[A1SubscriberCommercialEnquiry].[EnquiryResultInd]", "EnquiryResultInd", DataType.String, Nothing, False, 0, True, 100, "Enquiry Result"))
    oList.Columns.Add(New ListColumn("ProductPointValue", "[A1SubscriberCommercialEnquiry].[ProductPointValue]", "ProductPointValue", DataType.Decimal, Nothing, False, 0, True, 100, "Product Point Value"))
    oList.Columns.Add(New ListColumn("RegistrationNoPart1", "null", "RegistrationNoPart1", DataType.String, Nothing, False, 0, True, 100, "Registration No Part 1"))
    oList.Columns.Add(New ListColumn("RegistrationNoPart2", "null", "RegistrationNoPart2", DataType.String, Nothing, False, 0, True, 100, "Registration No Part 2"))
    oList.Columns.Add(New ListColumn("RegistrationNoPart3", "null", "RegistrationNoPart3", DataType.String, Nothing, False, 0, True, 100, "Registration No Part 3"))
    oList.Columns.Add(New ListColumn("RegistrationNo", "[A1SubscriberCommercialEnquiry].[RegistrationNo]", "RegistrationNo", DataType.String, Nothing, False, 0, True, 100, "Registration No"))
    oList.Columns.Add(New ListColumn("CommercialName", "[A1SubscriberCommercialEnquiry].[CommercialName]", "CommercialName", DataType.String, Nothing, False, 0, True, 100, "Business Name"))
    oList.Columns.Add(New ListColumn("ResultRegistrationNo", "[A1SubscriberCommercialEnquiry].[ResultRegistrationNo]", "ResultRegistrationNo", DataType.String, Nothing, False, 0, True, 100, "Registration No"))
    oList.Columns.Add(New ListColumn("ResultCommercialName", "[A1SubscriberCommercialEnquiry].[ResultCommercialName]", "ResultCommercialName", DataType.String, Nothing, False, 0, True, 100, "Business Name"))
    oList.Columns.Add(New ListColumn("ProductID", "[A1SubscriberCommercialEnquiry].[ProductID]", "ProductID", DataType.Integer, Nothing, False, 0, True, 100, "Product ID"))
    oList.Columns.Add(New ListColumn("SubscriberID1", "[A1SubscriberCommercialEnquiry].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, False, 0, True, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SICCode", "[A1SubscriberCommercialEnquiry].[SICCode]", "SICCode", DataType.String, Nothing, False, 0, True, 100, "SICCode"))
    oList.Columns.Add(New ListColumn("ResultSICCode", "[A1SubscriberCommercialEnquiry].[ResultSICCode]", "ResultSICCode", DataType.String, Nothing, False, 0, True, 100, "Result SICCode"))
    oList.Columns.Add(New ListColumn("SystemUserID", "[A1SubscriberCommercialEnquiry].[SystemUserID]", "SystemUserID", DataType.Integer, Nothing, False, 0, True, 100, "System User ID"))
    oList.Columns.Add(New ListColumn("NewElement1", "[A1SubscriberCommercialEnquiry].[TaxNo]", "TaxNo", DataType.String, Nothing, False, 0, True, 100, "Tax No"))
    oList.Columns.Add(New ListColumn("NewElement2", "[A1SubscriberCommercialEnquiry].[ResultTaxNo]", "ResultTaxNo", DataType.String, Nothing, False, 0, True, 100, "Result Tax No"))
    oList.SelectStatement = "select"
    oList.FromClause = "([SubscriberCommercial]  [A0SubscriberCommercial]" + _
           "    left join [SubscriberCommercialEnquiry]  [A1SubscriberCommercialEnquiry] on [A0SubscriberCommercial].[SubscriberCommercialEnquiryID] = [A1SubscriberCommercialEnquiry].[SubscriberCommercialEnquiryID] and [A0SubscriberCommercial].[SubscriberID] = [A1SubscriberCommercialEnquiry].[SubscriberID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberCommercial")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberCommercial].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberCommercialID", "[A0SubscriberCommercial].[SubscriberCommercialID]", "SubscriberCommercialID", DataType.Long, Nothing, True, 0, False, 100, "Subscriber Commercial ID"))
    oList.Columns.Add(New ListColumn("ReferenceNo", "[A0SubscriberCommercial].[ReferenceNo]", "ReferenceNo", DataType.String, Nothing, False, 0, True, 100, "Reference No"))
    oList.Columns.Add(New ListColumn("EnquiryDate", "[A0SubscriberCommercial].[EnquiryDate]", "EnquiryDate", DataType.DateTime, "g", False, -1, True, 100, "Enquiry Date"))
    oList.Columns.Add(New ListColumn("ProductTypeDesc", "[A2ProductType].[ProductTypeDesc]", "ProductTypeDesc", DataType.String, Nothing, False, 0, False, 200, "Product Type Description"))
    oList.Columns.Add(New ListColumn("ProductDesc", "[A1Product].[ProductDesc]", "ProductDesc", DataType.String, Nothing, False, 0, True, 200, "Product Description"))
    oList.Columns.Add(New ListColumn("DefaultPointValue", "[A1Product].[DefaultPointValue]", "DefaultPointValue", DataType.Decimal, Nothing, False, 0, False, 100, "Default Point Value"))
    oList.Columns.Add(New ListColumn("ProductDetails", "[A1Product].[ProductDetails]", "ProductDetails", DataType.Text, Nothing, False, 0, False, 200, "Product Details"))
    oList.Columns.Add(New ListColumn("FullName", "isnull([A3ActionedBySystemUser].[FirstName], '') + ' ' + isnull([A3ActionedBySystemUser].[Surname], '')", "FullName", DataType.String, Nothing, False, 0, True, 200, "Full Name"))
    oList.Columns.Add(New ListColumn("EmailAddress", "[A3ActionedBySystemUser].[EmailAddress]", "EmailAddress", DataType.String, Nothing, False, 0, False, 100, "Email Address"))
    oList.SelectStatement = "select"
    oList.FromClause = "(([SubscriberCommercial]  [A0SubscriberCommercial]" + _
           "    join ([Product]  [A1Product]" + _
           "    join [ProductType]  [A2ProductType] on [A1Product].[ProductTypeID] = [A2ProductType].[ProductTypeID]) on [A0SubscriberCommercial].[ProductID] = [A1Product].[ProductID])" + _
           "    join [SystemUser]  [A3ActionedBySystemUser] on [A0SubscriberCommercial].[ActionedBySystemUserID] = [A3ActionedBySystemUser].[SystemUserID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
