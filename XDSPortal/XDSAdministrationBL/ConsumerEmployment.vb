Imports sembleWare.Runtime
Imports System
Public Class ConsumerEmployment 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly ConsumerEmploymentID As New IdentityElement("ConsumerEmploymentID", Me, True, Nothing, Nothing)
  Public ReadOnly EmployerDetail As New StringElement("EmployerDetail", Me, False, True, True, True, True, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Occupation As New StringElement("Occupation", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Department As New StringElement("Department", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Designation As New StringElement("Designation", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RecordStatusInd As New IndicatorElement("RecordStatusInd", Me, False, False, False, True, True, True, "Record Status", Nothing, ConsumerEmployment.RecordStatusIndOptions, "A")
  Public ReadOnly DeletedReason As New StringElement("DeletedReason", Me, False, True, True, True, False, 500, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LastUpdatedDate As New DateTimeElement("LastUpdatedDate", Me, False, False, True, True, True, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly IsVerifiedYN As New BooleanElement("IsVerifiedYN", Me, False, True, True, True, True, "Is Verified?", Nothing, False, "Yes;No")
  Public ReadOnly VerifiedDate As New DateTimeElement("VerifiedDate", Me, False, False, True, True, False, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Loader As Relationship = New LoaderRelationship("Loader", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Consumer As Relationship = New ConsumerRelationship("Consumer", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, False)
  Public ReadOnly _LoaderID As New IntegerElement("LoaderID", Me, False)
  Public ReadOnly _ConsumerID As New IntegerElement("ConsumerID", Me, True)
  Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
  Public ReadOnly _Loader As LoaderRelationship = Loader
  Public ReadOnly _Consumer As ConsumerRelationship = Consumer
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moRecordStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property RecordStatusIndOptions() As IndicatorOptions
    Get
      If moRecordStatusIndOptions Is Nothing Then
        moRecordStatusIndOptions = New IndicatorOptions
        moRecordStatusIndOptions.Add("A", "Active")
        moRecordStatusIndOptions.Add("D", "Deleted")
      End If
      Return moRecordStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("ConsumerEmployment", ApplicationSettings)
    _Subscriber._SubscriberID.LocalElement = _SubscriberID
    _Loader._LoaderID.LocalElement = _LoaderID
    _Consumer._ConsumerID.LocalElement = _ConsumerID
    AddHandler IsVerifiedYN.Changed, AddressOf IsVerifiedYN_Changed
    'sembleWare: Constructor End - Do Not Modify
    Me.DeleteType = CustomDBPart.enumDeleteType.ChangeRecordStatus
  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub IsVerifiedYN_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BRIsVerifiedYN(Element, True, True)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Save()
    With ApplicationSettings
      .BeginTransaction()
      Try
        Dim bIsNewYN As Boolean = Me.IsNew
        BRBeforeSave(bIsNewYN)
        MyBase.Save()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
      .CommitTransaction()
    End With
  End Sub
#End Region

#Region " Business Rules"
  Public Sub BRBeforeSave(ByVal IsNewYN As Boolean)
    Me.LastUpdatedDate.Value = System.DateTime.Now
  End Sub

  Private Sub BRIsVerifiedYN(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If PerformRulesYN Then
      If Element.ValueAsObject Then
        Me.VerifiedDate.Value = System.DateTime.Now
      Else
        Me.VerifiedDate.SetToNullValue()
      End If
    End If
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class ConsumerEmploymentRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _ConsumerEmploymentID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _ConsumerID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New ConsumerEmployment(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _ConsumerEmploymentID.ForeignElement = CType(Instance, ConsumerEmployment).ConsumerEmploymentID
    _ConsumerID.ForeignElement = CType(Instance, ConsumerEmployment)._ConsumerID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0ConsumerEmployment")
    oList.Columns.Add(New ListColumn("ConsumerEmploymentID", "[A0ConsumerEmployment].[ConsumerEmploymentID]", "ConsumerEmploymentID", DataType.Long, Nothing, True, 0, False, 100, "Consumer Employment ID"))
    oList.Columns.Add(New ListColumn("ConsumerID", "[A0ConsumerEmployment].[ConsumerID]", "ConsumerID", DataType.Integer, Nothing, True, 0, False, 100, "Consumer ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0ConsumerEmployment].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, False, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("LastUpdatedDate", "[A0ConsumerEmployment].[LastUpdatedDate]", "LastUpdatedDate", DataType.DateTime, "g", False, -1, True, 100, "Last Updated Date"))
    oList.Columns.Add(New ListColumn("EmployerDetail", "[A0ConsumerEmployment].[EmployerDetail]", "EmployerDetail", DataType.String, Nothing, False, 0, True, 200, "Employer Detail"))
    oList.Columns.Add(New ListColumn("Department", "[A0ConsumerEmployment].[Department]", "Department", DataType.String, Nothing, False, 0, True, 100, "Department"))
    oList.Columns.Add(New ListColumn("Designation", "[A0ConsumerEmployment].[Designation]", "Designation", DataType.String, Nothing, False, 0, True, 100, "Designation"))
    oList.Columns.Add(New ListColumn("Occupation", "[A0ConsumerEmployment].[Occupation]", "Occupation", DataType.String, Nothing, False, 0, True, 100, "Occupation"))
    oList.Columns.Add(New ListColumn("SubscriberID1", "[A1Subscriber].[SubscriberID]", "SubscriberID", DataType.Long, Nothing, False, 0, False, 75, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberName", "[A1Subscriber].[SubscriberName]", "SubscriberName", DataType.String, Nothing, False, 0, True, 150, "Subscriber Name"))
    oList.Columns.Add(New ListColumn("RecordStatusInd", "[A0ConsumerEmployment].[RecordStatusInd]", "RecordStatusInd", DataType.String, Nothing, False, 0, False, 100, "Record Status"))
    oList.Columns.Add(New ListColumn("LoaderID", "[A0ConsumerEmployment].[LoaderID]", "LoaderID", DataType.Integer, Nothing, False, 0, False, 100, "Loader ID"))
    oList.Columns.Add(New ListColumn("IsVerifiedYN", "[A0ConsumerEmployment].[IsVerifiedYN]", "IsVerifiedYN", DataType.Boolean, "Yes;No", False, 0, True, 75, "Is Verified?"))
    oList.Columns.Add(New ListColumn("VerifiedDate", "[A0ConsumerEmployment].[VerifiedDate]", "VerifiedDate", DataType.DateTime, "g", False, 0, True, 100, "Verified Date"))
    oList.SelectStatement = "select"
    oList.FromClause = "(([ConsumerEmployment]  [A0ConsumerEmployment]" + _
           "    left join [Subscriber]  [A1Subscriber] on [A0ConsumerEmployment].[SubscriberID] = [A1Subscriber].[SubscriberID])" + _
           "    join [Consumer]  [A2Consumer] on [A0ConsumerEmployment].[ConsumerID] = [A2Consumer].[ConsumerID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    oList.AndFilters.Add(New ListFilter("RecordStatusInd", "[A2Consumer].[RecordStatusInd]", Constants.RecordStatus.Active, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
    oList.AndFilters.Add(New ListFilter("RecordStatusInd1", "[A0ConsumerEmployment].[RecordStatusInd]", Constants.RecordStatus.Active, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
