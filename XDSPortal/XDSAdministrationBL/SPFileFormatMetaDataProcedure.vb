Imports sembleWare.Runtime
Imports System
Public Class SPFileFormatMetaDataProcedure 'sembleWare: Part
  Inherits CustomMemoryPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly DataProcedureTypeInd_SP As New IndicatorElement("DataProcedureTypeInd_SP", Me, False, False, True, True, True, False, "Data Procedure Type", Nothing, SPFileFormatMetaDataProcedure.DataProcedureTypeInd_SPOptions, "CO")
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moDataProcedureTypeInd_SPOptions As IndicatorOptions
  Public Shared ReadOnly Property DataProcedureTypeInd_SPOptions() As IndicatorOptions
    Get
      If moDataProcedureTypeInd_SPOptions Is Nothing Then
        moDataProcedureTypeInd_SPOptions = New IndicatorOptions
        moDataProcedureTypeInd_SPOptions.Add("DE", "Deletion")
        moDataProcedureTypeInd_SPOptions.Add("CO", "Conversion")
        moDataProcedureTypeInd_SPOptions.Add("V", "Required Validation")
        moDataProcedureTypeInd_SPOptions.Add("NV", "Non-Required Validation")
        moDataProcedureTypeInd_SPOptions.Add("DS", "Data Statistics")
        moDataProcedureTypeInd_SPOptions.Add("CA", "Calculation")
        moDataProcedureTypeInd_SPOptions.Add("ME", "Matching Evaluation")
      End If
      Return moDataProcedureTypeInd_SPOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SPFileFormatMetaDataProcedure", ApplicationSettings)
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class SPFileFormatMetaDataProcedureRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SPFileFormatMetaDataProcedure(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
