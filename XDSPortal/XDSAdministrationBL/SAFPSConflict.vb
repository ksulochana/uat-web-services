Imports sembleWare.Runtime
Imports System
Public Class SAFPSConflict 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly SAFPS As Relationship = New SAFPSRelationship("SAFPS", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly ConflictSAFPS As Relationship = New SAFPSRelationship("ConflictSAFPS", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SAFPSID As New IntegerElement("SAFPSID", Me, True)
  Public ReadOnly _ConflictSAFPSID As New IntegerElement("ConflictSAFPSID", Me, True)
  Public ReadOnly _SAFPS As SAFPSRelationship = SAFPS
  Public ReadOnly _ConflictSAFPS As SAFPSRelationship = ConflictSAFPS
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SAFPSConflict", ApplicationSettings)
    _SAFPS._SAFPSID.LocalElement = _SAFPSID
    _ConflictSAFPS._SAFPSID.LocalElement = _ConflictSAFPSID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class SAFPSConflictRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SAFPSID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _ConflictSAFPSID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SAFPSConflict(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SAFPSID.ForeignElement = CType(Instance, SAFPSConflict)._SAFPSID
    _ConflictSAFPSID.ForeignElement = CType(Instance, SAFPSConflict)._ConflictSAFPSID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SAFPSConflict")
    oList.Columns.Add(New ListColumn("SAFPSID", "[A0SAFPSConflict].[SAFPSID]", "SAFPSID", DataType.Integer, Nothing, True, 0, False, 100, "SAFPSID"))
    oList.Columns.Add(New ListColumn("ConflictSAFPSID", "[A0SAFPSConflict].[ConflictSAFPSID]", "ConflictSAFPSID", DataType.Integer, Nothing, True, 0, False, 100, "Conflict SAFPSID"))
    oList.Columns.Add(New ListColumn("SAFPSID1", "[A1ConflictSAFPS].[SAFPSID]", "SAFPSID", DataType.Long, Nothing, False, 1, True, 100, "SAFPS ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "([SAFPSConflict]  [A0SAFPSConflict]" + _
           "    join [SAFPS]  [A1ConflictSAFPS] on [A0SAFPSConflict].[ConflictSAFPSID] = [A1ConflictSAFPS].[SAFPSID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
