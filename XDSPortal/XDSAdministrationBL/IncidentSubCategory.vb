Imports sembleWare.Runtime
Imports System
Public Class IncidentSubCategory 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly IncidentSubCategoryCode As New StringElement("IncidentSubCategoryCode", Me, True, True, True, True, True, 10, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly IncidentSubCategoryDesc As New StringElement("IncidentSubCategoryDesc", Me, False, True, True, True, True, 50, "Incident Sub Category Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly IncidentCategory As Relationship = New IncidentCategoryRelationship("IncidentCategory", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _IncidentCategoryCode As New StringElement("IncidentCategoryCode", Me, True)
  Public ReadOnly _IncidentCategory As IncidentCategoryRelationship = IncidentCategory
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("IncidentSubCategory", ApplicationSettings)
    _IncidentCategory._IncidentCategoryCode.LocalElement = _IncidentCategoryCode
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class IncidentSubCategoryRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _IncidentCategoryCode As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _IncidentSubCategoryCode As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New IncidentSubCategory(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _IncidentCategoryCode.ForeignElement = CType(Instance, IncidentSubCategory)._IncidentCategoryCode
    _IncidentSubCategoryCode.ForeignElement = CType(Instance, IncidentSubCategory).IncidentSubCategoryCode
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0IncidentSubCategory")
    oList.Columns.Add(New ListColumn("IncidentCategoryCode", "[A0IncidentSubCategory].[IncidentCategoryCode]", "IncidentCategoryCode", DataType.String, Nothing, True, 0, False, 100, "Incident Category Code"))
    oList.Columns.Add(New ListColumn("IncidentSubCategoryCode", "[A0IncidentSubCategory].[IncidentSubCategoryCode]", "IncidentSubCategoryCode", DataType.String, Nothing, True, 1, True, 100, "Incident Sub Category Code"))
    oList.Columns.Add(New ListColumn("IncidentSubCategoryDesc", "[A0IncidentSubCategory].[IncidentSubCategoryDesc]", "IncidentSubCategoryDesc", DataType.String, Nothing, False, 0, True, 200, "Incident Sub Category Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "[IncidentSubCategory]  [A0IncidentSubCategory]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function DropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0IncidentSubCategory")
    oList.Columns.Add(New ListColumn("IncidentCategoryCode", "[A0IncidentSubCategory].[IncidentCategoryCode]", "IncidentCategoryCode", DataType.String, Nothing, True, 0, False, 100, "Incident Category Code"))
    oList.Columns.Add(New ListColumn("IncidentSubCategoryCode", "[A0IncidentSubCategory].[IncidentSubCategoryCode]", "IncidentSubCategoryCode", DataType.String, Nothing, True, 1, True, 100, "Incident Sub Category Code"))
    oList.Columns.Add(New ListColumn("IncidentSubCategoryDesc", "[A0IncidentSubCategory].[IncidentSubCategoryDesc]", "IncidentSubCategoryDesc", DataType.String, Nothing, False, 0, True, 200, "Incident Sub Category Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "[IncidentSubCategory]  [A0IncidentSubCategory]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
