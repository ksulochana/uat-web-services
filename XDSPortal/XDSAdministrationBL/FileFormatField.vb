Imports sembleWare.Runtime
Imports System
Public Class FileFormatField 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly FileFormatFieldID As New IdentityElement("FileFormatFieldID", Me, True, Nothing, Nothing)
  Public ReadOnly FileFormatFieldDesc As New StringElement("FileFormatFieldDesc", Me, False, True, True, True, True, 100, "Field Description ", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FileFormatFieldTypeInd As New IndicatorElement("FileFormatFieldTypeInd", Me, False, False, True, True, True, True, "File Format Field Type", Nothing, FileFormatField.FileFormatFieldTypeIndOptions, "F")
  Public ReadOnly FieldLength As New IntegerElement("FieldLength", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LoaderDataValidationUseForDuplicateYN As New BooleanElement("LoaderDataValidationUseForDuplicateYN", Me, False, True, True, True, True, "Use To Check For Duplicate Record When Validating?", Nothing, False, "Yes;No")
  Public ReadOnly LoaderPostDataUpdateDisplayInListYN As New BooleanElement("LoaderPostDataUpdateDisplayInListYN", Me, False, True, True, True, True, "Use To Display Record In Post Data Update List?", Nothing, False, "Yes;No")
  Public ReadOnly SequenceNum As New IntegerElement("SequenceNum", Me, False, False, True, True, True, "Sequence Number", Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FileFormatFieldLabelDescription As New StringElement("FileFormatFieldLabelDescription", Me, False, True, True, False, False, 500, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FileFormat As Relationship = New FileFormatRelationship("FileFormat", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _FileFormatCode As New StringElement("FileFormatCode", Me, True)
  Public ReadOnly _FileFormat As FileFormatRelationship = FileFormat
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moFileFormatFieldTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property FileFormatFieldTypeIndOptions() As IndicatorOptions
    Get
      If moFileFormatFieldTypeIndOptions Is Nothing Then
        moFileFormatFieldTypeIndOptions = New IndicatorOptions
        moFileFormatFieldTypeIndOptions.Add("F", "File Field")
        moFileFormatFieldTypeIndOptions.Add("C", "Calculation Field")
        moFileFormatFieldTypeIndOptions.Add("L", "Loader Field")
        moFileFormatFieldTypeIndOptions.Add("S", "System Field")
      End If
      Return moFileFormatFieldTypeIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("FileFormatField", ApplicationSettings)
    _FileFormat._FileFormatCode.LocalElement = _FileFormatCode
    AddHandler FileFormatFieldTypeInd.Changed, AddressOf FileFormatFieldTypeInd_Changed
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub FileFormatFieldTypeInd_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BRFileFormatFieldTypeInd(Element, True, True)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"

  Public Sub MoveUp()
    If Me.SequenceNum.Value = 0 Then
      Return
    End If
    Me.SequenceNum.Value = Me.SequenceNum.Value - 1
    Save()
  End Sub

  Public Sub MoveDown()
    Dim nSequenceNum = Utility.GetNextSequenceNumber(Me.TableName, BRGetSequenceWhereClause(), ApplicationSettings) - 1
    If Me.SequenceNum.Value = nSequenceNum Then
      Return
    End If
    Me.SequenceNum.Value = Me.SequenceNum.Value + 1
    Save()
  End Sub

#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreate()
  End Sub

  Public Overrides Sub Load()
    MyBase.Load()
    BRLoad()
  End Sub

  Public Overrides Sub Save()
    With ApplicationSettings
      .BeginTransaction()
      Try
        Dim bIsNewYN As Boolean = Me.IsNew
        BRBeforeSave(bIsNewYN)
        MyBase.Save()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
      .CommitTransaction()
    End With
  End Sub

  Public Overrides Sub Delete()
    With ApplicationSettings
      .BeginTransaction()
      Try
        MyBase.Delete()
        BRDelete()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
      .CommitTransaction()
    End With
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreate()
    BRFileFormatFieldTypeInd(Me.FileFormatFieldTypeInd, True, False)
    BRFileFormatFieldLabelDescription()
  End Sub

  Private Sub BRLoad()
    BRFileFormatFieldTypeInd(Me.FileFormatFieldTypeInd, True, False)
    BRFileFormatFieldLabelDescription()
  End Sub

  Private Sub BRBeforeSave(ByVal IsNewYN As Boolean)
    If IsNewYN Then
      If Me.SequenceNum.IsEmpty Then
        Me.SequenceNum.Value = Utility.GetNextSequenceNumber(Me.TableName, BRGetSequenceWhereClause(), ApplicationSettings)
      End If
    Else
      If Me.FileFormatFieldTypeInd.MustPersist Then
        BRDeleteMetaDataProcedures()
      End If
    End If
    Utility.UpdateSequenceNumber(Me, BRGetSequenceWhereClause(), False, ApplicationSettings)
    BRFileFormatFieldLabelDescription()
  End Sub

  Private Sub BRDelete()
    Utility.UpdateSequenceNumber(Me, Me.FileFormat.Instance, True, ApplicationSettings)
  End Sub

  Private Sub BRFileFormatFieldTypeInd(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If SetEnableDisableYN Then
      Me.FieldLength.Enabled = False
      Me.FieldLength.Mandatory = False
      Select Case Element.ValueAsObject
        Case Constants.FileFormatField.Types.File
          Dim oFileFormat As FileFormat = Me.FileFormat.Instance
          Select Case oFileFormat.FileTypeInd.Value
            Case Constants.FileFormat.FileTypes.TextFile
              Select Case oFileFormat.FileFormatFieldTypeInd.Value
                Case Constants.FileFormat.FieldTypes.FixedField
                  Me.FieldLength.Enabled = True
                  Me.FieldLength.Mandatory = True
              End Select
            Case Constants.FileFormat.FileTypes.XMLFile
              Me.FieldLength.Enabled = True
              Me.FieldLength.Mandatory = True
          End Select
        Case Constants.FileFormatField.Types.Calculation, Constants.FileFormatField.Types.Loader
          Me.FieldLength.Enabled = True
          Me.FieldLength.Mandatory = True
      End Select
    End If
  End Sub

  Private Sub BRDeleteMetaDataProcedures()
    Dim oFileFormat As FileFormat = Me.FileFormat.Instance
    Dim oList As List = oFileFormat._FileFormatMetaDatabase_OwnMany.GridList
    Dim oDataRow As DataRow

    For Each oDataRow In oList.DataTable.Rows
      Dim sMetaDatabaseCode As String = oDataRow("MetaDatabaseCode")

      Dim sQuery As String = "delete "
      sQuery &= " from FileFormatMetaDataProcedureParameter "
      sQuery &= Me.InstanceWhereClause(Me)
      sQuery &= "  and MetaDatabaseCode = " & ApplicationSettings.QueryBuilder.ToSQL(sMetaDatabaseCode)
      sQuery &= "  and DataProcedureID in (select DataProcedureID "
      sQuery &= "                            from DataProcedure "
      sQuery &= "                           where DataProcedureTypeInd in ('V', 'CA'))"

      ApplicationSettings.ActionQuery(sQuery)

      sQuery = "delete "
      sQuery &= " from FileFormatMetaDataProcedure "
      sQuery &= Me.InstanceWhereClause(Me)
      sQuery &= "  and MetaDatabaseCode = " & ApplicationSettings.QueryBuilder.ToSQL(sMetaDatabaseCode)
      sQuery &= "  and DataProcedureID in (select DataProcedureID "
      sQuery &= "                            from DataProcedure "
      sQuery &= "                           where DataProcedureTypeInd in ('V', 'CA'))"

      ApplicationSettings.ActionQuery(sQuery)
    Next
  End Sub

  Private Function BRGetSequenceWhereClause() As String
    Dim sSequenceWhere As String = CustomDBPart.InstanceWhereClause(Me.FileFormat.Instance)
    sSequenceWhere &= " and FileFormatFieldTypeInd = " & ApplicationSettings.QueryBuilder.ToSQL(Me.FileFormatFieldTypeInd.Value)

    Return sSequenceWhere
  End Function

  Private Sub BRFileFormatFieldLabelDescription()
    Dim oFileFormat As FileFormat = Me.FileFormat.Instance
    If Me.IsNew Then
      Me.FileFormatFieldLabelDescription.Value = oFileFormat.FileFormatLabelDescription.Value & " - Field: (New)"
    Else
      Dim sDesc As String = Trim(Me.FileFormatFieldDesc.Value)
      Me.FileFormatFieldLabelDescription.Value = oFileFormat.FileFormatLabelDescription.Value & " - Field: " & Me.FileFormatFieldID.Value & " (" & sDesc & ")"
    End If
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class FileFormatFieldRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _FileFormatFieldID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _FileFormatCode As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New FileFormatField(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _FileFormatFieldID.ForeignElement = CType(Instance, FileFormatField).FileFormatFieldID
    _FileFormatCode.ForeignElement = CType(Instance, FileFormatField)._FileFormatCode
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function FileCalculationGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0FileFormatField")
    oList.Columns.Add(New ListColumn("FileFormatFieldID", "[A0FileFormatField].[FileFormatFieldID]", "FileFormatFieldID", DataType.Long, Nothing, True, 0, False, 100, "File Format Field ID"))
    oList.Columns.Add(New ListColumn("FileFormatFieldDesc", "[A0FileFormatField].[FileFormatFieldDesc]", "FileFormatFieldDesc", DataType.String, Nothing, False, 0, True, 200, "Field Description "))
    oList.Columns.Add(New ListColumn("FileFormatFieldTypeInd", "[A0FileFormatField].[FileFormatFieldTypeInd]", "FileFormatFieldTypeInd", DataType.String, Nothing, False, 0, False, 100, "File Format Field Type"))
    oList.Columns.Add(New ListColumn("FieldLength", "[A0FileFormatField].[FieldLength]", "FieldLength", DataType.Integer, Nothing, False, 0, True, 100, "Field Length"))
    oList.Columns.Add(New ListColumn("SequenceNum", "[A0FileFormatField].[SequenceNum]", "SequenceNum", DataType.Integer, Nothing, False, 1, False, 100, "Sequence Number"))
    oList.Columns.Add(New ListColumn("FileFormatCode", "[A0FileFormatField].[FileFormatCode]", "FileFormatCode", DataType.String, Nothing, True, 0, False, 100, "File Format Code"))
    oList.SelectStatement = "select"
    oList.FromClause = "[FileFormatField]  [A0FileFormatField]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function DropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0FileFormatField")
    oList.Columns.Add(New ListColumn("FileFormatFieldID", "[A0FileFormatField].[FileFormatFieldID]", "FileFormatFieldID", DataType.Long, Nothing, True, 0, False, 100, "File Format Field ID"))
    oList.Columns.Add(New ListColumn("FileFormatFieldDesc", "[A0FileFormatField].[FileFormatFieldDesc]", "FileFormatFieldDesc", DataType.String, Nothing, False, 0, True, 200, "Field Description "))
    oList.Columns.Add(New ListColumn("FileFormatFieldTypeInd", "[A0FileFormatField].[FileFormatFieldTypeInd]", "FileFormatFieldTypeInd", DataType.String, Nothing, False, 0, True, 100, "File Format Field Type"))
    oList.Columns.Add(New ListColumn("FieldLength", "[A0FileFormatField].[FieldLength]", "FieldLength", DataType.Integer, Nothing, False, 0, True, 100, "Fixed Field Start"))
    oList.Columns.Add(New ListColumn("SequenceNum", "[A0FileFormatField].[SequenceNum]", "SequenceNum", DataType.Integer, Nothing, False, 1, False, 100, "Order Num"))
    oList.Columns.Add(New ListColumn("FileFormatCode", "[A0FileFormatField].[FileFormatCode]", "FileFormatCode", DataType.String, Nothing, True, 0, False, 100, "File Format Code"))
    oList.SelectStatement = "select"
    oList.FromClause = "[FileFormatField]  [A0FileFormatField]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0FileFormatField")
    oList.Columns.Add(New ListColumn("FileFormatFieldID", "[A0FileFormatField].[FileFormatFieldID]", "FileFormatFieldID", DataType.Long, Nothing, True, 0, False, 100, "File Format Field ID"))
    oList.Columns.Add(New ListColumn("FileFormatFieldDesc", "[A0FileFormatField].[FileFormatFieldDesc]", "FileFormatFieldDesc", DataType.String, Nothing, False, 0, True, 200, "Field Description "))
    oList.Columns.Add(New ListColumn("FieldLength", "[A0FileFormatField].[FieldLength]", "FieldLength", DataType.Integer, Nothing, False, 0, True, 100, "Fixed Field Start"))
    oList.Columns.Add(New ListColumn("SequenceNum", "[A0FileFormatField].[SequenceNum]", "SequenceNum", DataType.Integer, Nothing, False, 0, True, 100, "Sequence Number"))
    oList.Columns.Add(New ListColumn("FileFormatFieldTypeInd", "[A0FileFormatField].[FileFormatFieldTypeInd]", "FileFormatFieldTypeInd", DataType.String, Nothing, False, 0, True, 100, "File Format Field Type"))
    oList.Columns.Add(New ListColumn("FileFormatCode", "[A0FileFormatField].[FileFormatCode]", "FileFormatCode", DataType.String, Nothing, True, 0, False, 100, "File Format Code"))
    oList.SelectStatement = "select"
    oList.FromClause = "[FileFormatField]  [A0FileFormatField]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function





  Public Function LoaderSystemGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0FileFormatField")
    oList.Columns.Add(New ListColumn("FileFormatFieldID", "[A0FileFormatField].[FileFormatFieldID]", "FileFormatFieldID", DataType.Long, Nothing, True, 0, False, 100, "File Format Field ID"))
    oList.Columns.Add(New ListColumn("FileFormatFieldDesc", "[A0FileFormatField].[FileFormatFieldDesc]", "FileFormatFieldDesc", DataType.String, Nothing, False, 0, True, 200, "Field Description "))
    oList.Columns.Add(New ListColumn("FileFormatFieldTypeInd", "[A0FileFormatField].[FileFormatFieldTypeInd]", "FileFormatFieldTypeInd", DataType.String, Nothing, False, 0, False, 100, "File Format Field Type"))
    oList.Columns.Add(New ListColumn("SequenceNum", "[A0FileFormatField].[SequenceNum]", "SequenceNum", DataType.Integer, Nothing, False, 1, False, 100, "Sequence Number"))
    oList.Columns.Add(New ListColumn("FileFormatCode", "[A0FileFormatField].[FileFormatCode]", "FileFormatCode", DataType.String, Nothing, True, 0, False, 100, "File Format Code"))
    oList.SelectStatement = "select"
    oList.FromClause = "[FileFormatField]  [A0FileFormatField]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0FileFormatField")
    oList.Columns.Add(New ListColumn("FileFormatFieldID", "[A0FileFormatField].[FileFormatFieldID]", "FileFormatFieldID", DataType.Long, Nothing, True, 0, False, 100, "File Format Field ID"))
    oList.Columns.Add(New ListColumn("FileFormatFieldDesc", "[A0FileFormatField].[FileFormatFieldDesc]", "FileFormatFieldDesc", DataType.String, Nothing, False, 0, True, 200, "Field Description "))
    oList.Columns.Add(New ListColumn("FileFormatFieldTypeInd", "[A0FileFormatField].[FileFormatFieldTypeInd]", "FileFormatFieldTypeInd", DataType.String, Nothing, False, 0, False, 100, "File Format Field Type"))
    oList.Columns.Add(New ListColumn("FieldLength", "[A0FileFormatField].[FieldLength]", "FieldLength", DataType.Integer, Nothing, False, 0, True, 100, "Field Length"))
    oList.Columns.Add(New ListColumn("SequenceNum", "[A0FileFormatField].[SequenceNum]", "SequenceNum", DataType.Integer, Nothing, False, 1, True, 100, "Sequence Number"))
    oList.Columns.Add(New ListColumn("FileFormatCode", "[A0FileFormatField].[FileFormatCode]", "FileFormatCode", DataType.String, Nothing, True, 0, False, 100, "File Format Code"))
    oList.SelectStatement = "select"
    oList.FromClause = "[FileFormatField]  [A0FileFormatField]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
