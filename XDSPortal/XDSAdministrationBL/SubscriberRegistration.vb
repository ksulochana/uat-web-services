Imports sembleWare.Runtime
Imports System
Public Class SubscriberRegistration 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly SubscriberRegistrationID As New IdentityElement("SubscriberRegistrationID", Me, True, Nothing, Nothing)
  Public ReadOnly StatusInd As New IndicatorElement("StatusInd", Me, False, False, True, True, True, True, "Status", Nothing, SubscriberRegistration.StatusIndOptions, "IP")
  Public ReadOnly SessionID As New StringElement("SessionID", Me, False, True, True, True, True, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RegistrationDate As New DateTimeElement("RegistrationDate", Me, False, True, True, True, True, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly IAgreeInd As New IndicatorElement("IAgreeInd", Me, False, False, True, True, True, False, "I Agree to ISPortalAdmin's subscriber terms and conditions, which I have read and understood, and I am duly authorised to bind the company to these terms and conditions.", Nothing, SubscriberRegistration.IAgreeIndOptions, Nothing)
  Public ReadOnly CompanyName As New StringElement("CompanyName", Me, False, True, True, True, True, 250, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CompanyRegistrationNo As New StringElement("CompanyRegistrationNo", Me, False, True, True, True, False, 50, "Registration No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CompanyVATNo As New StringElement("CompanyVATNo", Me, False, True, True, True, False, 50, "VAT No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CompanyDetails As New TextElement("CompanyDetails", Me, True, True, True, False, Nothing, Nothing, Nothing)
  Public ReadOnly CompanyTradingName1 As New StringElement("CompanyTradingName1", Me, False, True, True, True, False, 250, "Trading Name 1", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CompanyTradingName2 As New StringElement("CompanyTradingName2", Me, False, True, True, True, False, 250, "Trading Name 2", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CompanyPhysicalAddress1 As New StringElement("CompanyPhysicalAddress1", Me, False, True, True, True, False, 100, "Physical Address 1", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CompanyPhysicalAddress2 As New StringElement("CompanyPhysicalAddress2", Me, False, True, True, True, False, 100, "Physical Address 2", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CompanyPhysicalAddress3 As New StringElement("CompanyPhysicalAddress3", Me, False, True, True, True, False, 100, "Physical Address 3", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CompanyPhysicalPostalCode As New StringElement("CompanyPhysicalPostalCode", Me, False, True, True, True, False, 10, "Physical Postal Code", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CompanyPostalAddress1 As New StringElement("CompanyPostalAddress1", Me, False, True, True, True, False, 100, "Postal Address 1", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CompanyPostalAddress2 As New StringElement("CompanyPostalAddress2", Me, False, True, True, True, False, 100, "Postal Address 2", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CompanyPostalAddress3 As New StringElement("CompanyPostalAddress3", Me, False, True, True, True, False, 100, "Postal Address 3", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CompanyPostalPostalCode As New StringElement("CompanyPostalPostalCode", Me, False, True, True, True, False, 10, "Postal Postal Code", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CompanyTelephoneCode As New StringElement("CompanyTelephoneCode", Me, False, True, True, True, False, 5, "Telephone Code", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CompanyTelephoneNo As New StringElement("CompanyTelephoneNo", Me, False, True, True, True, False, 50, "Telephone No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CompanyFaxCode As New StringElement("CompanyFaxCode", Me, False, True, True, True, False, 5, "Fax Code", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CompanyFaxNo As New StringElement("CompanyFaxNo", Me, False, True, True, True, False, 50, "Fax No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CompanyWebsiteUrl As New StringElement("CompanyWebsiteUrl", Me, False, True, True, True, False, 250, "Website Url", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ContactPersonFirstName As New StringElement("ContactPersonFirstName", Me, False, True, True, True, False, 150, "First Name", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ContactPersonSurname As New StringElement("ContactPersonSurname", Me, False, True, True, True, False, 150, "Surname", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ContactPersonIDNo As New StringElement("ContactPersonIDNo", Me, False, True, True, True, False, 13, "ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ContactPersonJobPosition As New StringElement("ContactPersonJobPosition", Me, False, True, True, True, False, 50, "Job Position", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ContactPersonEmailAddress As New StringElement("ContactPersonEmailAddress", Me, False, True, True, True, False, 100, "Email Address", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ContactPersonCellularCode As New StringElement("ContactPersonCellularCode", Me, False, True, True, True, False, 5, "Cellular Code", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ContactPersonCellularNo As New StringElement("ContactPersonCellularNo", Me, False, True, True, True, False, 50, "Cellular No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ContactPersonUsername As New StringElement("ContactPersonUsername", Me, False, True, True, True, False, 150, "Username", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ContactPersonPassword As New StringElement("ContactPersonPassword", Me, False, True, True, True, False, 100, "Password", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PaymentAccountHolder As New StringElement("PaymentAccountHolder", Me, False, True, True, True, False, 50, "Account Holder", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PaymentBankName As New StringElement("PaymentBankName", Me, False, True, True, True, False, 150, "Bank Name", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PaymentBranchName As New StringElement("PaymentBranchName", Me, False, True, True, True, False, 150, "Branch Name", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PaymentBranchCode As New StringElement("PaymentBranchCode", Me, False, True, True, True, False, 50, "Branch Code", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PaymentAccountNo As New StringElement("PaymentAccountNo", Me, False, True, True, True, False, 50, "Account No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ApprovedBySystemUser As New StringElement("ApprovedBySystemUser", Me, False, False, True, True, False, 50, "Approved By", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ApprovedOnDate As New DateTimeElement("ApprovedOnDate", Me, False, False, True, True, False, "Approved On", Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly ApprovalReason As New TextElement("ApprovalReason", Me, True, True, True, False, Nothing, Nothing, Nothing)
  Public ReadOnly ContactPersonNewPassword As New StringElement("ContactPersonNewPassword", Me, False, True, True, False, False, 30, "New Password", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ContactPersonConfirmPassword As New StringElement("ContactPersonConfirmPassword", Me, False, True, True, False, False, 30, "Confirm Password", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SubscriberBillingGroup As Relationship = New SubscriberBillingGroupRelationship("SubscriberBillingGroup", "Billing Group", RelationshipType.Include, False, True, True, Me)
  Public ReadOnly SubscriberAssociation As Relationship = New SubscriberAssociationRelationship("SubscriberAssociation", "Association", RelationshipType.Include, False, True, True, Me)
  Public ReadOnly SubscriberBusinessType As Relationship = New SubscriberBusinessTypeRelationship("SubscriberBusinessType", "Business Type", RelationshipType.Include, False, True, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberBillingGroupCode As New StringElement("SubscriberBillingGroupCode", Me, False)
  Public ReadOnly _SubscriberAssociationCode As New StringElement("SubscriberAssociationCode", Me, False)
  Public ReadOnly _SubscriberBusinessTypeCode As New StringElement("SubscriberBusinessTypeCode", Me, False)
  Public ReadOnly _SubscriberBillingGroup As SubscriberBillingGroupRelationship = SubscriberBillingGroup
  Public ReadOnly _SubscriberAssociation As SubscriberAssociationRelationship = SubscriberAssociation
  Public ReadOnly _SubscriberBusinessType As SubscriberBusinessTypeRelationship = SubscriberBusinessType
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property StatusIndOptions() As IndicatorOptions
    Get
      If moStatusIndOptions Is Nothing Then
        moStatusIndOptions = New IndicatorOptions
        moStatusIndOptions.Add("IP", "In Process")
        moStatusIndOptions.Add("AA", "Awaiting Approval")
        moStatusIndOptions.Add("A", "Approved")
      End If
      Return moStatusIndOptions
    End Get
  End Property
  Private Shared moIAgreeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property IAgreeIndOptions() As IndicatorOptions
    Get
      If moIAgreeIndOptions Is Nothing Then
        moIAgreeIndOptions = New IndicatorOptions
        moIAgreeIndOptions.Add("Y", "Yes")
        moIAgreeIndOptions.Add("N", "No")
      End If
      Return moIAgreeIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberRegistration", ApplicationSettings)
    _SubscriberBillingGroup._SubscriberBillingGroupCode.LocalElement = _SubscriberBillingGroupCode
    _SubscriberAssociation._SubscriberAssociationCode.LocalElement = _SubscriberAssociationCode
    _SubscriberBusinessType._SubscriberBusinessTypeCode.LocalElement = _SubscriberBusinessTypeCode
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
  Public Sub ClearValidate()
    Me.CompanyName.Mandatory = False
    Me.CompanyRegistrationNo.Mandatory = False
    Me.ContactPersonFirstName.Mandatory = False
    Me.ContactPersonSurname.Mandatory = False
    Me.ContactPersonIDNo.Mandatory = False
    Me.ContactPersonEmailAddress.Mandatory = False
    Me.ContactPersonUsername.Mandatory = False
    Me.ContactPersonNewPassword.Mandatory = False
    Me.ContactPersonConfirmPassword.Mandatory = False
  End Sub

  Public Sub CompanyValidate()
    Me.CompanyName.Mandatory = True
    Me.CompanyRegistrationNo.Mandatory = True
  End Sub

  Public Sub ContactPersonValidate()
    Me.ContactPersonFirstName.Mandatory = True
    Me.ContactPersonSurname.Mandatory = True
    Me.ContactPersonIDNo.Mandatory = True
    Me.ContactPersonEmailAddress.Mandatory = True
    Me.ContactPersonUsername.Mandatory = True
    Me.ContactPersonNewPassword.Mandatory = True
    Me.ContactPersonConfirmPassword.Mandatory = True
  End Sub

  Public Sub ContactPersonPasswordValidate()
    Me.ContactPersonPassword.Value = Utility.ValidatePassword(Me.ContactPersonNewPassword.Value, Me.ContactPersonConfirmPassword.Value)
  End Sub

  Public Sub Submit()
    BRSubmit()
  End Sub

  Public Sub ApproveValidate()
    Me.SubscriberAssociation.Enabled = True
    Me.SubscriberAssociation.Mandatory = True
    Me.SubscriberBillingGroup.Enabled = True
    Me.SubscriberBillingGroup.Mandatory = True
    Me.SubscriberBusinessType.Enabled = True
    Me.SubscriberBusinessType.Mandatory = True
    Me.ApprovalReason.Enabled = True
    Me.ApprovalReason.Mandatory = True
  End Sub

  Public Sub Approve()
    ApproveValidate()
    BRApprove()
  End Sub
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreate()
  End Sub

  Public Overrides Sub Load()
    MyBase.Load()
    BRLoad()
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreate()
    Me.RegistrationDate.Value = System.DateTime.Now
    Me.SessionID.Value = System.Web.HttpContext.Current.Session.SessionID
  End Sub

  Private Sub BRLoad()
    Me.DisableElementsAndRelationship()
  End Sub

  Private Sub BRSubmit()
    If Not Me.IAgreeInd.Value = Constants.YesNo.Yes Then
      Throw New BusinessRuleException("Please select 'Yes' to agreeing to the terms and conditions!")
    End If
    Me.StatusInd.Value = Constants.SubscriberRegistration.Statuses.AwaitingApproval
    Me.Save()
  End Sub

  Private Sub BRApprove()
    Select Case Me.StatusInd.Value
      Case Constants.SubscriberRegistration.Statuses.Approved
        Throw New BusinessRuleException("This registration cannot be approved as it has have been previously approved!")
      Case Constants.SubscriberRegistration.Statuses.InProcess
        Throw New BusinessRuleException("This registration cannot be approved as it has not been submitted!")
    End Select

    With ApplicationSettings
      Try
        .BeginTransaction()
        Me.StatusInd.Value = Constants.SubscriberRegistration.Statuses.Approved
        Me.ApprovedBySystemUser.Value = GlobalSettings.SystemUser.LoggedInSystemUsername()
        Me.ApprovedOnDate.Value = System.DateTime.Now
        Me.Save()

        ' Create Subscriber
        Dim oSubscriber As Subscriber = New SubscriberRelationship(ApplicationSettings).NewInstance
        oSubscriber.SubscriberName.Value = Me.CompanyName.Value
        oSubscriber.SubscriberTypeInd.Value = Constants.Subscriber.Types.Enquiries
        oSubscriber.SubscriberRegistration.Instance = Me
        oSubscriber.CompanyVATNo.Value = Me.CompanyVATNo.Value
        oSubscriber.CompanyRegistrationNo.Value = Me.CompanyRegistrationNo.Value
        oSubscriber.CompanyTradingName1.Value = Me.CompanyTradingName1.Value
        oSubscriber.CompanyTradingName2.Value = Me.CompanyTradingName2.Value
        oSubscriber.CompanyDetails.Value = Me.CompanyDetails.Value
        oSubscriber.CompanyPhysicalAddress1.Value = Me.CompanyPhysicalAddress1.Value
        oSubscriber.CompanyPhysicalAddress2.Value = Me.CompanyPhysicalAddress2.Value
        oSubscriber.CompanyPhysicalAddress3.Value = Me.CompanyPhysicalAddress3.Value
        oSubscriber.CompanyPhysicalPostalCode.Value = Me.CompanyPhysicalPostalCode.Value
        oSubscriber.CompanyPostalAddress1.Value = Me.CompanyPostalAddress1.Value
        oSubscriber.CompanyPostalAddress2.Value = Me.CompanyPostalAddress2.Value
        oSubscriber.CompanyPostalAddress3.Value = Me.CompanyPostalAddress3.Value
        oSubscriber.CompanyPostalPostalCode.Value = Me.CompanyPostalPostalCode.Value
        oSubscriber.CompanyTelephoneCode.Value = Me.CompanyTelephoneCode.Value
        oSubscriber.CompanyTelephoneNo.Value = Me.CompanyTelephoneNo.Value
        oSubscriber.CompanyFaxCode.Value = Me.CompanyFaxCode.Value
        oSubscriber.CompanyFaxNo.Value = Me.CompanyFaxNo.Value
        oSubscriber.CompanyWebsiteUrl.Value = Me.CompanyWebsiteUrl.Value
        oSubscriber.SubscriberAssociation.Instance = Me.SubscriberAssociation.Instance
        oSubscriber.SubscriberBillingGroup.Instance = Me.SubscriberBillingGroup.Instance
        oSubscriber.SubscriberBusinessType.Instance = Me.SubscriberBusinessType.Instance
        oSubscriber.Save()

        ' Create Subscriber User
        Dim oSystemUser As SystemUser = New SystemUserRelationship(ApplicationSettings).NewInstance
        oSystemUser.SystemUserTypeInd.Value = Constants.SystemUser.Types.SubscriberUser
        oSystemUser.Subscriber.Instance = oSubscriber
        oSystemUser.FirstName.Value = Me.ContactPersonFirstName.Value
        oSystemUser.Surname.Value = Me.ContactPersonSurname.Value
        oSystemUser.IDNo.Value = Me.ContactPersonIDNo.Value
        oSystemUser.JobPosition.Value = Me.ContactPersonJobPosition.Value
        oSystemUser.CellularCode.Value = Me.ContactPersonCellularCode.Value
        oSystemUser.CellularNo.Value = Me.ContactPersonCellularNo.Value
        oSystemUser.EmailAddress.Value = Me.ContactPersonEmailAddress.Value
        oSystemUser.Username.Value = Me.ContactPersonUsername.Value
        oSystemUser.Password.Value = Me.ContactPersonPassword.Value
        oSystemUser.ResetPasswordYN.Value = False
        oSystemUser.ActiveYN.Value = True
        oSystemUser.Save()

        ' Create Subscriber User Roles
        Dim oSystemUserRole As SystemUserRole = oSystemUser.SystemUserRole_OwnMany.NewInstance()
        oSystemUserRole._RoleCode.Value = Constants.Role.Records.SubscriberAdministrators
        oSystemUserRole.Save()

        .CommitTransaction()
      Catch oException As Exception
        Me.StatusInd.Value = Me.StatusInd.LoadValueAsObject
        Me.ApprovedBySystemUser.SetToNullValue()
        Me.ApprovedOnDate.SetToNullValue()
        .RollbackTransaction()
        Throw oException
      End Try
    End With
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class SubscriberRegistrationRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberRegistrationID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberRegistration(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberRegistrationID.ForeignElement = CType(Instance, SubscriberRegistration).SubscriberRegistrationID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberRegistration")
    oList.Columns.Add(New ListColumn("SubscriberRegistrationID", "[A0SubscriberRegistration].[SubscriberRegistrationID]", "SubscriberRegistrationID", DataType.Long, Nothing, True, -1, True, 75, "Subscriber Registration ID"))
    oList.Columns.Add(New ListColumn("RegistrationDate", "[A0SubscriberRegistration].[RegistrationDate]", "RegistrationDate", DataType.DateTime, "g", False, 0, True, 100, "Registration Date"))
    oList.Columns.Add(New ListColumn("StatusInd", "[A0SubscriberRegistration].[StatusInd]", "StatusInd", DataType.String, Nothing, False, 0, True, 100, "Status"))
    oList.Columns.Add(New ListColumn("CompanyName", "[A0SubscriberRegistration].[CompanyName]", "CompanyName", DataType.String, Nothing, False, 0, True, 150, "Company Name"))
    oList.Columns.Add(New ListColumn("ContactPersonFirstName", "[A0SubscriberRegistration].[ContactPersonFirstName]", "ContactPersonFirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("ContactPersonSurname", "[A0SubscriberRegistration].[ContactPersonSurname]", "ContactPersonSurname", DataType.String, Nothing, False, 0, True, 100, "Surname"))
    oList.Columns.Add(New ListColumn("IAgreeInd", "[A0SubscriberRegistration].[IAgreeInd]", "IAgreeInd", DataType.String, Nothing, False, 0, True, 100, "I Agree?"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberRegistration]  [A0SubscriberRegistration]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
