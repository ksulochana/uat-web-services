Imports sembleWare.Runtime
Imports System
Public Class LoaderMatchingEngineSAFPSResult 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly IgnoreRecordYN As New BooleanElement("IgnoreRecordYN", Me, False, True, True, True, True, "Ignore Record?", Nothing, False)
  Public ReadOnly LoaderMatchingEngineSAFPS As Relationship = New LoaderMatchingEngineSAFPSRelationship("LoaderMatchingEngineSAFPS", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly SAFPS As Relationship = New SAFPSRelationship("SAFPS", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _RecordID As New IntegerElement("RecordID", Me, True)
  Public ReadOnly _LoaderMatchingEngineID As New IntegerElement("LoaderMatchingEngineID", Me, True)
  Public ReadOnly _SAFPSID As New IntegerElement("SAFPSID", Me, True)
  Public ReadOnly _LoaderMatchingEngineSAFPS As LoaderMatchingEngineSAFPSRelationship = LoaderMatchingEngineSAFPS
  Public ReadOnly _SAFPS As SAFPSRelationship = SAFPS
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("LoaderMatchingEngineSAFPSResult", ApplicationSettings)
    _LoaderMatchingEngineSAFPS._RecordID.LocalElement = _RecordID
    _LoaderMatchingEngineSAFPS._LoaderMatchingEngineID.LocalElement = _LoaderMatchingEngineID
    _SAFPS._SAFPSID.LocalElement = _SAFPSID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class LoaderMatchingEngineSAFPSResultRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _RecordID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _LoaderMatchingEngineID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SAFPSID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New LoaderMatchingEngineSAFPSResult(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _RecordID.ForeignElement = CType(Instance, LoaderMatchingEngineSAFPSResult)._RecordID
    _LoaderMatchingEngineID.ForeignElement = CType(Instance, LoaderMatchingEngineSAFPSResult)._LoaderMatchingEngineID
    _SAFPSID.ForeignElement = CType(Instance, LoaderMatchingEngineSAFPSResult)._SAFPSID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
