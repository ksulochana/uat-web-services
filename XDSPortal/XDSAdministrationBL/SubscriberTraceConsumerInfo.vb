Imports sembleWare.Runtime
Imports System
Public Class SubscriberTraceConsumerInfo 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly NettMonthlyIncomeAmt As New DecimalElement("NettMonthlyIncomeAmt", Me, False, True, True, True, True, "Nett Monthly Income Amount", Nothing, "c", 0, Nothing, Nothing)
  Public ReadOnly OtherMonthlyIncomeAmt As New DecimalElement("OtherMonthlyIncomeAmt", Me, False, True, True, True, False, "Other Monthly Income Amount", Nothing, "c", 0, Nothing, Nothing)
  Public ReadOnly MonthlyExpenseAmt As New DecimalElement("MonthlyExpenseAmt", Me, False, True, True, True, True, "Monthly Expense Amount", Nothing, "c", 0, Nothing, Nothing)
  Public ReadOnly NettMonthlyResultAmt As New DecimalElement("NettMonthlyResultAmt", Me, False, False, True, True, True, "Nett Monthly Result Amount", Nothing, "c", 0, Nothing, Nothing)
  Public ReadOnly SubscriberTraceConsumer As Relationship = New SubscriberTraceConsumerRelationship("SubscriberTraceConsumer", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _SubscriberTraceConsumerID As New IntegerElement("SubscriberTraceConsumerID", Me, True)
  Public ReadOnly _SubscriberTraceConsumer As SubscriberTraceConsumerRelationship = SubscriberTraceConsumer
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberTraceConsumerInfo", ApplicationSettings)
    _SubscriberTraceConsumer._SubscriberID.LocalElement = _SubscriberID
    _SubscriberTraceConsumer._SubscriberTraceConsumerID.LocalElement = _SubscriberTraceConsumerID
    AddHandler NettMonthlyIncomeAmt.Changed, AddressOf NettMonthlyIncomeAmt_Changed
    AddHandler OtherMonthlyIncomeAmt.Changed, AddressOf OtherMonthlyIncomeAmt_Changed
    AddHandler MonthlyExpenseAmt.Changed, AddressOf MonthlyExpenseAmt_Changed
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub NettMonthlyIncomeAmt_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
  End Sub
  Private Sub OtherMonthlyIncomeAmt_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
  End Sub
  Private Sub MonthlyExpenseAmt_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"

  Public Sub SelectRecord()
  End Sub

#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class SubscriberTraceConsumerInfoRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SubscriberTraceConsumerID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberTraceConsumerInfo(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberID.ForeignElement = CType(Instance, SubscriberTraceConsumerInfo)._SubscriberID
    _SubscriberTraceConsumerID.ForeignElement = CType(Instance, SubscriberTraceConsumerInfo)._SubscriberTraceConsumerID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
