Imports System
Imports System.Data
Imports System.Text
Imports sembleWare.Runtime
Imports XDSAdministrationBL

Namespace XMLManager
  Public Class Manager
#Region " Variables"
    Private moLoaderEngineManager As LoaderEngineManager.Manager
#End Region

#Region " Constructor"
    Public Sub New(ByVal LoaderEngineManager As LoaderEngineManager.Manager)
      moLoaderEngineManager = LoaderEngineManager
    End Sub
#End Region

    Private Function GetMetaDatqbaseApplicationSettings(ByVal MetaDatabase As MetaDatabase)
      Dim oEncryption As sembleWare.Security.Encryption = New sembleWare.Security.Encryption(XDSAdministrationBL.Constants.PasswordHashString)
      Dim sConnectionString As String = "data source=" & MetaDatabase.ServerName.Value
      sConnectionString &= ";initial catalog=" & MetaDatabase.DatabaseName.Value
      sConnectionString &= ";persist security info=False"
      sConnectionString &= ";user id = " & MetaDatabase.Username.Value
      sConnectionString &= ";pwd=" & oEncryption.Decrypt(MetaDatabase.Password.Value)
      sConnectionString &= ";packet size=4096"

      Return New ApplicationSettings(New System.Data.SqlClient.SqlConnection(sConnectionString), New sembleWare.Runtime.SQLData.SQLQueryBuilder, 1800)
    End Function

    Private Function GetImportXMLDataSet() As DataSet
      Dim oList As List = moLoaderEngineManager.FileFormat._FileFormatField_OwnMany.GridList
      Dim oDataRow As DataRow
      Dim oXMLDataSet As DataSet = New DataSet
      Dim oDataTable As DataTable = New DataTable

      oList.AndFilters.Add(New ListFilter("FileFormatFieldTypeInd", "FileFormatFieldTypeInd", XDSAdministrationBL.Constants.FileFormatField.Types.File, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      For Each oDataRow In oList.DataTable.Rows
        oDataTable.Columns.Add(New DataColumn(moLoaderEngineManager.ParseFileFormatFieldDesc(oDataRow("FileFormatFieldDesc"))))
      Next
      oDataTable.Namespace = moLoaderEngineManager.FileFormat.XMLNamespace.Value
      oDataTable.TableName = moLoaderEngineManager.FileFormat.XMLElement.Value
      oXMLDataSet.Tables.Add(oDataTable)

      Return oXMLDataSet
    End Function

    Private Function GetExportXMLDataSet() As DataSet
      Dim oXMLDataSet As DataSet = GetImportXMLDataSet()
      Dim oDataTable As DataTable = oXMLDataSet.Tables(0)

      oDataTable.Columns.Add(New DataColumn(DTSManager.Constants.Field_RequiredValidationDataProcedureDetails))
      oDataTable.Columns.Add(New DataColumn(DTSManager.Constants.Field_NonRequiredValidationDataProcedureDetails))

      Return oXMLDataSet
    End Function

    Private Function CreateInsertQuery(ByVal Loader As Loader, ByVal MetaDatabase As MetaDatabase) As String
      Dim oInsertQuery As StringBuilder = New StringBuilder
      Dim oList As List
      Dim oDataRow As DataRow
      Dim oDataRow2 As DataRow
      Dim sStagingTableName As String = ""

      Select Case MetaDatabase.MetaDatabaseTypeInd.Value
        Case Constants.MetaDatabase.Types.System
          sStagingTableName = SQLManager.StagingTable.TableName(MetaDatabase, moLoaderEngineManager.FileFormat, moLoaderEngineManager.FileFormatDeploymentID, Loader.LoaderID.Value)
        Case Constants.MetaDatabase.Types.Repository
          sStagingTableName = SQLManager.RepositoryTable.TableName(moLoaderEngineManager.FileFormat, moLoaderEngineManager.FileFormatDeploymentID)
      End Select
      oInsertQuery.Append("insert into " & sStagingTableName & "(")
      oInsertQuery.Append(DTSManager.Constants.Field_LoaderID)
      Select Case MetaDatabase.MetaDatabaseTypeInd.Value
        Case XDSAdministrationBL.Constants.MetaDatabase.Types.System
          oInsertQuery.Append(", " & DTSManager.Constants.Field_SubscriberID & ", " & DTSManager.Constants.Field_IsValidRecordYN & ", " & DTSManager.Constants.Field_IsPossibleNameConflictYN)
          oInsertQuery.Append(", " & DTSManager.Constants.Field_IsPossibleDuplicateRecordYN & ", " & DTSManager.Constants.Field_RequiredValidationDataProcedureDetails & ", " & DTSManager.Constants.Field_NonRequiredValidationDataProcedureDetails)

          ' Create the Loader Type Fields
          oList = moLoaderEngineManager.FileFormat._FileFormatField_OwnMany.GridList
          oList.AndFilters.Add(New ListFilter("FileFormatFieldTypeInd", "FileFormatFieldTypeInd", XDSAdministrationBL.Constants.FileFormatField.Types.Loader, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
          For Each oDataRow2 In oList.DataTable.Rows
            oInsertQuery.Append(", " & moLoaderEngineManager.ParseFileFormatFieldDesc(oDataRow2("FileFormatFieldDesc")))
          Next
      End Select

      ' Create the File Type Fields
      oList = moLoaderEngineManager.FileFormat._FileFormatField_OwnMany.GridList
      oList.AndFilters.Add(New ListFilter("FileFormatFieldTypeInd", "FileFormatFieldTypeInd", XDSAdministrationBL.Constants.FileFormatField.Types.File, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      For Each oDataRow2 In oList.DataTable.Rows
        oInsertQuery.Append(", " & moLoaderEngineManager.ParseFileFormatFieldDesc(oDataRow2("FileFormatFieldDesc")))
      Next
      oInsertQuery.Append(")")

      Return oInsertQuery.ToString()
    End Function

    Public Sub ExecuteImport(ByVal Loader As Loader)
      Dim sFolderPath As String = SystemDefault.GetLoaderStorageFolderPath(moLoaderEngineManager.ApplicationSettings)
      Dim sFileName As String = System.IO.Path.Combine(sFolderPath, Loader.LoaderFileName.Value)
      Dim oDataRow As DataRow
      Dim oApplicationSettings As ApplicationSettings

      For Each oDataRow In moLoaderEngineManager.MetaDatabaseList(Nothing).DataTable.Rows
        Dim oMetaDatabase As MetaDatabase = New MetaDatabaseRelationship(moLoaderEngineManager.ApplicationSettings).NewInstance()
        oMetaDatabase.MetaDatabaseCode.Load(oDataRow("MetaDatabaseCode"))
        oMetaDatabase.Load()
        Dim oValueQuery As StringBuilder = New StringBuilder
        Dim oXMLDataSet As DataSet = GetImportXMLDataSet()
        Dim oDataRow2 As DataRow
        Dim nCounter As Integer
        Dim nCounterRow As Integer
        Dim sBaseQuery As String = CreateInsertQuery(Loader, oMetaDatabase)

        Select Case oMetaDatabase.MetaDatabaseTypeInd.Value
          Case Constants.MetaDatabase.Types.System
            oApplicationSettings = moLoaderEngineManager.ApplicationSettings
          Case Constants.MetaDatabase.Types.Repository
            Dim sConnectionString As String = SQLManager.RepositoryTable.ConnectionString(oMetaDatabase)
            oApplicationSettings = New ApplicationSettings(New System.Data.SqlClient.SqlConnection(sConnectionString), New sembleWare.Runtime.SQLData.SQLQueryBuilder, 60)
        End Select
        With oApplicationSettings.QueryBuilder
          oValueQuery.Append("values(" & .ToSQL(Loader.LoaderID.Value))

          Select Case oMetaDatabase.MetaDatabaseTypeInd.Value
            Case XDSAdministrationBL.Constants.MetaDatabase.Types.System
              Dim oDataRow3 As DataRow

              oValueQuery.Append(", " & .ToSQL(Loader._SubscriberID.Value))
              oValueQuery.Append(", " & .ToSQL(False))
              oValueQuery.Append(", " & .ToSQL(False))
              oValueQuery.Append(", " & .ToSQL(False))
              oValueQuery.Append(", " & .ToSQL(""))
              oValueQuery.Append(", " & .ToSQL(""))
              ' Create Loader Parameter Fields
              For Each oDataRow3 In Loader._LoaderParameter_OwnMany.GridList.DataTable.Rows
                oValueQuery.Append(", " & .ToSQL(SHBS.General.EmptyStringIfDBNull(oDataRow3("ParameterValue"))))
              Next
          End Select

          sBaseQuery &= oValueQuery.ToString()
          oXMLDataSet.ReadXml(sFileName, XmlReadMode.Auto)
          For nCounterRow = 0 To oXMLDataSet.Tables(0).Rows.Count - 1
            Dim oQuery As StringBuilder = New StringBuilder
            Dim sFieldName As String

            oDataRow2 = oXMLDataSet.Tables(0).Rows(nCounterRow)
            If nCounterRow = oXMLDataSet.Tables(0).Rows.Count - 1 Then
              Dim bAddLastRow As Boolean

              bAddLastRow = False
              For nCounter = 0 To oXMLDataSet.Tables(0).Columns.Count - 1
                If SHBS.General.EmptyStringIfDBNull(oDataRow2(nCounter)) <> "" Then
                  bAddLastRow = True
                End If
              Next
              If bAddLastRow = False Then
                Exit For
              End If
            End If
            oQuery.Append(sBaseQuery)
            ' Create the File Type Fields
            For nCounter = 0 To oXMLDataSet.Tables(0).Columns.Count - 1
              oQuery.Append(", " & IIf(SHBS.General.EmptyStringIfDBNull(oDataRow2(nCounter)) = "", "null", .ToSQL(oDataRow2(nCounter))))
            Next
            oQuery.Append(")")
            oApplicationSettings.ActionQuery(oQuery.ToString())
          Next
        End With
      Next
    End Sub

    Public Sub ExecuteExport(ByVal Loader As Loader, ByVal MetaDatabase As MetaDatabase)
      Dim oLoaderProcess As LoaderProcess = LoaderProcess.GetDataValidation(Loader, moLoaderEngineManager.ApplicationSettings)
      Dim oLoaderProcessMetaDatabase As LoaderProcessMetaDatabase = oLoaderProcess.LoaderProcessMetaDatabase_OwnMany.NewInstance()
      oLoaderProcessMetaDatabase.MetaDatabase.Instance = MetaDatabase
      oLoaderProcessMetaDatabase.Load()
      Dim sDestinationFilePath As String = oLoaderProcessMetaDatabase.GetInvalidLoaderFileName()

      Select Case MetaDatabase.MetaDatabaseTypeInd.Value
        Case XDSAdministrationBL.Constants.MetaDatabase.Types.System
          Dim oXMLDataSet As DataSet = GetExportXMLDataSet()
          Dim oQuery As StringBuilder = New StringBuilder
          Dim oDataRow As DataRow
          Dim oDataColumn As DataColumn

          oQuery.Append("select * ")
          oQuery.Append("  from " & SQLManager.StagingTable.TableName(MetaDatabase, moLoaderEngineManager.FileFormat, moLoaderEngineManager.FileFormatDeploymentID, Loader.LoaderID.Value))
          oQuery.Append(" where IsValidRecordYN = 0")
          oQuery.Append(" order by RecordID")
          For Each oDataRow In moLoaderEngineManager.ApplicationSettings.ResultQuery(oQuery.ToString()).Rows
            Dim oNewDataRow As DataRow = oXMLDataSet.Tables(0).NewRow

            For Each oDataColumn In oXMLDataSet.Tables(0).Columns
              oNewDataRow(oDataColumn.ColumnName) = oDataRow(oDataColumn.ColumnName)
            Next

            oXMLDataSet.Tables(0).Rows.Add(oNewDataRow)
          Next

          oXMLDataSet.WriteXml(sDestinationFilePath)
      End Select
    End Sub
  End Class
End Namespace
