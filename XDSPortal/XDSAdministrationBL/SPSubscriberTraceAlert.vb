Imports sembleWare.Runtime
Imports System
Public Class SPSubscriberTraceAlert 'sembleWare: Part
  Inherits CustomMemoryPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly TraceAlertStartDateFrom_SP As New DateTimeElement("TraceAlertStartDateFrom_SP", Me, False, True, True, True, False, "Start Date", Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly TraceAlertStartDateTo_SP As New DateTimeElement("TraceAlertStartDateTo_SP", Me, False, True, True, True, False, "Trace Alert Start Date To", Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly TraceAlertEndDateFrom_SP As New DateTimeElement("TraceAlertEndDateFrom_SP", Me, False, True, True, True, False, "End Date", Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly TraceAlertEndDateTo_SP As New DateTimeElement("TraceAlertEndDateTo_SP", Me, False, True, True, True, False, "Trace Alert End Date To", Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly ReferenceNo_SP As New StringElement("ReferenceNo_SP", Me, False, True, True, True, False, 50, "Reference No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly StatusInd_SP As New IndicatorElement("StatusInd_SP", Me, False, False, True, True, True, False, "Status", Nothing, SPSubscriberTraceAlert.StatusInd_SPOptions, "All")
  Public ReadOnly ActionedBySystemUser_SP As Relationship = New SystemUserRelationship("ActionedBySystemUser_SP", "Actioned By", RelationshipType.Include, False, True, True, Me)
  Public ReadOnly Subscriber_SP As Relationship = New SubscriberRelationship("Subscriber_SP", "Subscriber", RelationshipType.Include, False, True, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _ActionedBySystemUserID As New IntegerElement("ActionedBySystemUserID", Me, False)
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, False)
  Public ReadOnly _ActionedBySystemUser_SP As SystemUserRelationship = ActionedBySystemUser_SP
  Public ReadOnly _Subscriber_SP As SubscriberRelationship = Subscriber_SP
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moStatusInd_SPOptions As IndicatorOptions
  Public Shared ReadOnly Property StatusInd_SPOptions() As IndicatorOptions
    Get
      If moStatusInd_SPOptions Is Nothing Then
        moStatusInd_SPOptions = New IndicatorOptions
        moStatusInd_SPOptions.Add("A", "Active")
        moStatusInd_SPOptions.Add("P", "Processing")
        moStatusInd_SPOptions.Add("C", "Completed")
        moStatusInd_SPOptions.Add("E", "Expired")
        moStatusInd_SPOptions.Add("F", "Failed")
        moStatusInd_SPOptions.Add("X", "Cancelled")
        moStatusInd_SPOptions.Add("All", "All")
      End If
      Return moStatusInd_SPOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SPSubscriberTraceAlert", ApplicationSettings)
    _ActionedBySystemUser_SP._SystemUserID.LocalElement = _ActionedBySystemUserID
    _Subscriber_SP._SubscriberID.LocalElement = _SubscriberID
    'sembleWare: Constructor End - Do Not Modify
    AddHandler ActionedBySystemUser_SP.LimitRelatedList, AddressOf ActionedBySystemUser_SP_LimitRelatedList
  End Sub

#End Region 'sembleWare: Constructor

#Region " LimitRelatedList Events"
  Private Sub ActionedBySystemUser_SP_LimitRelatedList(ByVal List As List)
    List.LimitByRelatedPart(Me.Subscriber_SP.Instance, "Subscriber", True)
  End Sub
#End Region

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class SPSubscriberTraceAlertRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SPSubscriberTraceAlert(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
