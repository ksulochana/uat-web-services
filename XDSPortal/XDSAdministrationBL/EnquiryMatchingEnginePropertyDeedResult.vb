Imports sembleWare.Runtime
Imports System
Public Class EnquiryMatchingEnginePropertyDeedResult 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly IgnoreRecordYN As New BooleanElement("IgnoreRecordYN", Me, False, True, True, True, True, "Ignore Record?", Nothing, False, "Yes;No")
  Public ReadOnly EnquiryMatchingEnginePropertyDeed As Relationship = New EnquiryMatchingEnginePropertyDeedRelationship("EnquiryMatchingEnginePropertyDeed", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly PropertyDeed As Relationship = New PropertyDeedRelationship("PropertyDeed", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _EnquiryMatchingEngineID As New IntegerElement("EnquiryMatchingEngineID", Me, True)
  Public ReadOnly _EnquiryMatchingEnginePropertyDeedID As New IntegerElement("EnquiryMatchingEnginePropertyDeedID", Me, True)
  Public ReadOnly _PropertyDeedID As New IntegerElement("PropertyDeedID", Me, True)
  Public ReadOnly _EnquiryMatchingEnginePropertyDeed As EnquiryMatchingEnginePropertyDeedRelationship = EnquiryMatchingEnginePropertyDeed
  Public ReadOnly _PropertyDeed As PropertyDeedRelationship = PropertyDeed
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("EnquiryMatchingEnginePropertyDeedResult", ApplicationSettings)
    _EnquiryMatchingEnginePropertyDeed._EnquiryMatchingEngineID.LocalElement = _EnquiryMatchingEngineID
    _EnquiryMatchingEnginePropertyDeed._EnquiryMatchingEnginePropertyDeedID.LocalElement = _EnquiryMatchingEnginePropertyDeedID
    _PropertyDeed._PropertyDeedID.LocalElement = _PropertyDeedID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class EnquiryMatchingEnginePropertyDeedResultRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _EnquiryMatchingEngineID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _EnquiryMatchingEnginePropertyDeedID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _PropertyDeedID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New EnquiryMatchingEnginePropertyDeedResult(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _EnquiryMatchingEngineID.ForeignElement = CType(Instance, EnquiryMatchingEnginePropertyDeedResult)._EnquiryMatchingEngineID
    _EnquiryMatchingEnginePropertyDeedID.ForeignElement = CType(Instance, EnquiryMatchingEnginePropertyDeedResult)._EnquiryMatchingEnginePropertyDeedID
    _PropertyDeedID.ForeignElement = CType(Instance, EnquiryMatchingEnginePropertyDeedResult)._PropertyDeedID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
