Imports sembleWare.Runtime
Imports System
Public Class NotificationQueueRecipient 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly NotificationQueueRecipientID As New IdentityElement("NotificationQueueRecipientID", Me, True, Nothing, Nothing)
  Public ReadOnly RecipientTypeInd As New IndicatorElement("RecipientTypeInd", Me, False, False, True, True, True, True, "Recipient Type", Nothing, NotificationQueueRecipient.RecipientTypeIndOptions, Nothing)
  Public ReadOnly EmailAddress As New StringElement("EmailAddress", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SystemUser As Relationship = New SystemUserRelationship("SystemUser", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly NotificationQueue As Relationship = New NotificationQueueRelationship("NotificationQueue", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SystemUserID As New IntegerElement("SystemUserID", Me, False)
  Public ReadOnly _NotificationQueueID As New IntegerElement("NotificationQueueID", Me, True)
  Public ReadOnly _SystemUser As SystemUserRelationship = SystemUser
  Public ReadOnly _NotificationQueue As NotificationQueueRelationship = NotificationQueue
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moRecipientTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property RecipientTypeIndOptions() As IndicatorOptions
    Get
      If moRecipientTypeIndOptions Is Nothing Then
        moRecipientTypeIndOptions = New IndicatorOptions
        moRecipientTypeIndOptions.Add("S", "System User")
        moRecipientTypeIndOptions.Add("E", "Email Address")
      End If
      Return moRecipientTypeIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("NotificationQueueRecipient", ApplicationSettings)
    _SystemUser._SystemUserID.LocalElement = _SystemUserID
    _NotificationQueue._NotificationQueueID.LocalElement = _NotificationQueueID
    AddHandler RecipientTypeInd.Changed, AddressOf RecipientTypeInd_Changed
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub RecipientTypeInd_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BRRecipientTypeInd(Element, True, True)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Business Rules"
  Private Sub BRRecipientTypeInd(ByVal Element As Element, ByVal SetEnableMandatory As Boolean, ByVal PerformRules As Boolean)
    If SetEnableMandatory Then
      Me.SystemUser.Enabled = False
      Me.SystemUser.Mandatory = False
      Me.EmailAddress.Enabled = False
      Me.EmailAddress.Mandatory = False

      Select Case Element.ValueAsObject
        Case Constants.NotificationQueueRecipient.Types.SystemUser
          Me.SystemUser.Enabled = True
          Me.SystemUser.Mandatory = True
        Case Constants.NotificationQueueRecipient.Types.EmailAddress
          Me.EmailAddress.Enabled = True
          Me.EmailAddress.Mandatory = True
      End Select
    End If

    If PerformRules Then
      Me.EmailAddress.SetToNullValue()
      Me.SystemUser.Instance = Nothing
    End If
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class NotificationQueueRecipientRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _NotificationQueueRecipientID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _NotificationQueueID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New NotificationQueueRecipient(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _NotificationQueueRecipientID.ForeignElement = CType(Instance, NotificationQueueRecipient).NotificationQueueRecipientID
    _NotificationQueueID.ForeignElement = CType(Instance, NotificationQueueRecipient)._NotificationQueueID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0NotificationQueueRecipient")
    oList.Columns.Add(New ListColumn("NotificationQueueRecipientID", "[A0NotificationQueueRecipient].[NotificationQueueRecipientID]", "NotificationQueueRecipientID", DataType.Long, Nothing, True, 0, True, 100, "Notification Queue Recipient ID"))
    oList.Columns.Add(New ListColumn("NotificationQueueID", "[A0NotificationQueueRecipient].[NotificationQueueID]", "NotificationQueueID", DataType.Integer, Nothing, True, 0, True, 100, "Notification Queue ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "[NotificationQueueRecipient]  [A0NotificationQueueRecipient]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
