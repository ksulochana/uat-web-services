Imports sembleWare.Runtime
Imports System
Public Class LoaderUploadFile 'sembleWare: Part
  Inherits CustomMemoryPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly FileName As New StringElement("FileName", Me, True, True, True, True, True, 250, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FileSize As New IntegerElement("FileSize", Me, False, True, True, True, False, Nothing, Nothing, "###,###,##0 KB", Nothing, Nothing, Nothing)
  Public ReadOnly FileCreatedDate As New DateTimeElement("FileCreatedDate", Me, False, True, True, True, False, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("LoaderUploadFile", ApplicationSettings)
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Load()
    Dim oSystemDefault As SystemDefault = SystemDefault.Current(ApplicationSettings)
    Dim sFileName As String

    Dim oDirectoryInfo As System.IO.DirectoryInfo = New System.IO.DirectoryInfo(oSystemDefault.LoaderUploadFolderPath.Value)
    Dim oFileInfo As System.IO.FileInfo

    For Each oFileInfo In oDirectoryInfo.GetFiles(Me.FileName.Value)
      Me.FileSize.Value = System.Convert.ToInt32(oFileInfo.Length / 1024)
      Me.FileCreatedDate.Value = oFileInfo.CreationTime()
      Me.PartState = PartStates.ExistingPart
      Exit For
    Next
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class LoaderUploadFileRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _FileName As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New LoaderUploadFile(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _FileName.ForeignElement = CType(Instance, LoaderUploadFile).FileName
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0LoaderUploadFile")
    oList.Columns.Add(New ListColumn("FileName", "[A0LoaderUploadFile].[FileName]", "FileName", DataType.String, Nothing, True, 1, True, 300, "File Name"))
    oList.Columns.Add(New ListColumn("FileSize", "[A0LoaderUploadFile].[FileSize]", "FileSize", DataType.Integer, "###,###,##0 KB", False, 0, True, 100, "File Size"))
    oList.Columns.Add(New ListColumn("FileCreatedDate", "[A0LoaderUploadFile].[FileCreatedDate]", "FileCreatedDate", DataType.DateTime, "g", False, 2, True, 100, "File Created Date"))
    oList.SelectStatement = "select"
    oList.FromClause = ""
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

  Public Function CustomGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As LoaderUploadFileList = New LoaderUploadFileList(Me, "A0LoaderUploadFile")
    oList.Columns.Add(New ListColumn("FileName", "[A0LoaderUploadFile].[FileName]", "FileName", DataType.String, Nothing, True, 1, True, 300, "File Name"))
    oList.Columns.Add(New ListColumn("FileSize", "[A0LoaderUploadFile].[FileSize]", "FileSize", DataType.Integer, "###,###,##0 KB", False, 0, True, 100, "File Size"))
    oList.Columns.Add(New ListColumn("FileCreatedDate", "[A0LoaderUploadFile].[FileCreatedDate]", "FileCreatedDate", DataType.DateTime, "g", False, 2, True, 100, "File Created Date"))
    oList.SelectStatement = "select"
    oList.FromClause = ""
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    oList.FromClause = SystemDefault.GetLoaderUploadFolderPath(ApplicationSettings)
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship 

#Region " Class LoaderUploadFileList"
Public Class LoaderUploadFileList
  Inherits sembleWare.Runtime.DBList

  Public Sub New(ByVal Relationship As sembleWare.Runtime.Relationship, ByVal RootTableAlias As String)
    MyBase.New(Relationship, RootTableAlias)
  End Sub

  Public Overrides Function GetOrderByForCurrentRow(ByVal CurrentRow As sembleWare.Runtime.ListRow) As sembleWare.Runtime.ListRow

  End Function

  Protected Overrides Sub PopulateDataTable(ByVal DataTable As System.Data.DataTable, ByVal CurrentRow As sembleWare.Runtime.ListRow, ByVal Direction As sembleWare.Runtime.RepositionDirection, ByVal Mode As sembleWare.Runtime.RepositionMode, ByVal NumRows As Integer, ByVal ExcludeCurrentRow As Boolean)
    Dim oDirectoryInfo As System.IO.DirectoryInfo = New System.IO.DirectoryInfo(Me.FromClause)
    Dim oFileInfo As System.IO.FileInfo

    For Each oFileInfo In oDirectoryInfo.GetFiles()
      Dim oDataRow As DataRow = DataTable.NewRow()

      oDataRow.Item(0) = oFileInfo.Name
      oDataRow.Item(1) = System.Convert.ToInt32(oFileInfo.Length / 1024)
      oDataRow.Item(2) = oFileInfo.CreationTime()
      DataTable.Rows.Add(oDataRow)
    Next
  End Sub
End Class
#End Region