Imports sembleWare.Runtime
Imports System
Public Class EnquiryMatchingEngineHomeAffairsResult 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly IgnoreRecordYN As New BooleanElement("IgnoreRecordYN", Me, False, True, True, True, True, "Ignore Record?", Nothing, False, "Yes;No")
  Public ReadOnly EnquiryMatchingEngineHomeAffairs As Relationship = New EnquiryMatchingEngineHomeAffairsRelationship("EnquiryMatchingEngineHomeAffairs", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly HomeAffairs As Relationship = New HomeAffairsRelationship("HomeAffairs", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _EnquiryMatchingEngineID As New IntegerElement("EnquiryMatchingEngineID", Me, True)
  Public ReadOnly _EnquiryMatchingEngineHomeAffairsID As New IntegerElement("EnquiryMatchingEngineHomeAffairsID", Me, True)
  Public ReadOnly _HomeAffairsID As New IntegerElement("HomeAffairsID", Me, True)
  Public ReadOnly _EnquiryMatchingEngineHomeAffairs As EnquiryMatchingEngineHomeAffairsRelationship = EnquiryMatchingEngineHomeAffairs
  Public ReadOnly _HomeAffairs As HomeAffairsRelationship = HomeAffairs
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("EnquiryMatchingEngineHomeAffairsResult", ApplicationSettings)
    _EnquiryMatchingEngineHomeAffairs._EnquiryMatchingEngineID.LocalElement = _EnquiryMatchingEngineID
    _EnquiryMatchingEngineHomeAffairs._EnquiryMatchingEngineHomeAffairsID.LocalElement = _EnquiryMatchingEngineHomeAffairsID
    _HomeAffairs._HomeAffairsID.LocalElement = _HomeAffairsID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class EnquiryMatchingEngineHomeAffairsResultRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _EnquiryMatchingEngineID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _EnquiryMatchingEngineHomeAffairsID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _HomeAffairsID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New EnquiryMatchingEngineHomeAffairsResult(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _EnquiryMatchingEngineID.ForeignElement = CType(Instance, EnquiryMatchingEngineHomeAffairsResult)._EnquiryMatchingEngineID
    _EnquiryMatchingEngineHomeAffairsID.ForeignElement = CType(Instance, EnquiryMatchingEngineHomeAffairsResult)._EnquiryMatchingEngineHomeAffairsID
    _HomeAffairsID.ForeignElement = CType(Instance, EnquiryMatchingEngineHomeAffairsResult)._HomeAffairsID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
