Imports sembleWare.Runtime
Imports System
Public Class SubscriberSAFPSEnquiryResult 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly TitleDeedNo As New StringElement("TitleDeedNo", Me, False, False, True, True, False, 13, "ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly DetailsViewedYN As New BooleanElement("DetailsViewedYN", Me, False, False, True, True, True, "Details Viewed?", Nothing, False, "Yes;No")
  Public ReadOnly DetailsViewedDate As New DateTimeElement("DetailsViewedDate", Me, False, False, True, True, False, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly SAFPSSelectedYN As New BooleanElement("SAFPSSelectedYN", Me, False, True, True, True, True, "SAFPS Selected?", Nothing, False, "Yes;No")
  Public ReadOnly SubscriberSAFPSEnquiry As Relationship = New SubscriberSAFPSEnquiryRelationship("SubscriberSAFPSEnquiry", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly SAFPS As Relationship = New SAFPSRelationship("SAFPS", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _SubscriberSAFPSEnquiryID As New IntegerElement("SubscriberSAFPSEnquiryID", Me, True)
  Public ReadOnly _SAFPSID As New IntegerElement("SAFPSID", Me, True)
  Public ReadOnly _SubscriberSAFPSEnquiry As SubscriberSAFPSEnquiryRelationship = SubscriberSAFPSEnquiry
  Public ReadOnly _SAFPS As SAFPSRelationship = SAFPS
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberSAFPSEnquiryResult", ApplicationSettings)
    _SubscriberSAFPSEnquiry._SubscriberID.LocalElement = _SubscriberID
    _SubscriberSAFPSEnquiry._SubscriberSAFPSEnquiryID.LocalElement = _SubscriberSAFPSEnquiryID
    _SAFPS._SAFPSID.LocalElement = _SAFPSID
    AddHandler DetailsViewedYN.Changed, AddressOf DetailsViewedYN_Changed
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub DetailsViewedYN_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"

  Public Sub SelectRecord()
    With ApplicationSettings
      Try
        .BeginTransaction()
        Me.SAFPSSelectedYN.Value = True
        Me.DetailsViewedYN.Value = True
        Me.DetailsViewedDate.Value = System.DateTime.Now
        Me.Save()

        Dim oSAFPS As SAFPS = Me.SAFPS.Instance
        Dim OSubscriberSAFPSEnquiry As SubscriberSAFPSEnquiry = Me.SubscriberSAFPSEnquiry.Instance
        OSubscriberSAFPSEnquiry.EnquiryResultInd.Value = Constants.SubscriberSAFPSEnquiry.Results.RecordSelected

        OSubscriberSAFPSEnquiry.ResultTitleDeedNo.Value = oSAFPS.TitleDeedNo.Value

        OSubscriberSAFPSEnquiry.Save()
        .CommitTransaction()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
    End With
  End Sub

#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class SubscriberSAFPSEnquiryResultRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SubscriberSAFPSEnquiryID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SAFPSID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberSAFPSEnquiryResult(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberID.ForeignElement = CType(Instance, SubscriberSAFPSEnquiryResult)._SubscriberID
    _SubscriberSAFPSEnquiryID.ForeignElement = CType(Instance, SubscriberSAFPSEnquiryResult)._SubscriberSAFPSEnquiryID
    _SAFPSID.ForeignElement = CType(Instance, SubscriberSAFPSEnquiryResult)._SAFPSID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberSAFPSEnquiryResult")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberSAFPSEnquiryResult].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberSAFPSEnquiryID", "[A0SubscriberSAFPSEnquiryResult].[SubscriberSAFPSEnquiryID]", "SubscriberSAFPSEnquiryID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber Property Deed Enquiry ID"))
    oList.Columns.Add(New ListColumn("SAFPSID", "[A0SubscriberSAFPSEnquiryResult].[SAFPSID]", "SAFPSID", DataType.Integer, Nothing, True, 0, False, 100, "Property Deed ID"))
    oList.Columns.Add(New ListColumn("TitleDeedNo", "[A0SubscriberSAFPSEnquiryResult].[TitleDeedNo]", "TitleDeedNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberSAFPSEnquiryResult]  [A0SubscriberSAFPSEnquiryResult]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function MultipleMatchGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberSAFPSEnquiryResult")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberSAFPSEnquiryResult].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberSAFPSEnquiryID", "[A0SubscriberSAFPSEnquiryResult].[SubscriberSAFPSEnquiryID]", "SubscriberSAFPSEnquiryID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber Property Deed Enquiry ID"))
    oList.Columns.Add(New ListColumn("SAFPSID", "[A0SubscriberSAFPSEnquiryResult].[SAFPSID]", "SAFPSID", DataType.Integer, Nothing, True, 0, False, 100, "Property Deed ID"))
    oList.Columns.Add(New ListColumn("TitleDeedNo", "[A0SubscriberSAFPSEnquiryResult].[TitleDeedNo]", "TitleDeedNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("DetailsViewedYN", "[A0SubscriberSAFPSEnquiryResult].[DetailsViewedYN]", "DetailsViewedYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Details Viewed?"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberSAFPSEnquiryResult]  [A0SubscriberSAFPSEnquiryResult]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberSAFPSEnquiryResult")
    oList.Columns.Add(New ListColumn("SAFPSID", "[A0SubscriberSAFPSEnquiryResult].[SAFPSID]", "SAFPSID", DataType.Integer, Nothing, True, 0, False, 100, "Property Deed ID"))
    oList.Columns.Add(New ListColumn("SubscriberSAFPSEnquiryID", "[A0SubscriberSAFPSEnquiryResult].[SubscriberSAFPSEnquiryID]", "SubscriberSAFPSEnquiryID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber Property Deed Enquiry ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberSAFPSEnquiryResult].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("TitleDeedNo", "[A0SubscriberSAFPSEnquiryResult].[TitleDeedNo]", "TitleDeedNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberSAFPSEnquiryResult]  [A0SubscriberSAFPSEnquiryResult]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
