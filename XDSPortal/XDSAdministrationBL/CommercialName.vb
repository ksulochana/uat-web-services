Imports sembleWare.Runtime
Imports System
Public Class CommercialName 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly CommercialNameID As New IdentityElement("CommercialNameID", Me, True, Nothing, Nothing)
  Public ReadOnly RegistrationNo As New StringElement("RegistrationNo", Me, False, True, True, True, True, 14, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RegistrationNoOld As New StringElement("RegistrationNoOld", Me, False, True, True, True, False, 14, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RegistrationNoConverted As New StringElement("RegistrationNoConverted", Me, False, True, True, True, False, 14, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CommercialTypeDate As New DateTimeElement("CommercialTypeDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly CommercialName As New StringElement("CommercialName", Me, False, False, True, True, True, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CommercialShortName As New StringElement("CommercialShortName", Me, False, False, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CommercialTranslatedName As New StringElement("CommercialTranslatedName", Me, False, False, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RegistrationDate As New DateTimeElement("RegistrationDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly BusinessStartDate As New DateTimeElement("BusinessStartDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly IsWithdrawnFromPublicYN As New BooleanElement("IsWithdrawnFromPublicYN", Me, False, True, True, True, True, "Is Withdrawn From Public?", Nothing, False, "Yes;No")
  Public ReadOnly CommercialStatusDate As New DateTimeElement("CommercialStatusDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly FinancialYearEnd As New IndicatorElement("FinancialYearEnd", Me, False, True, True, True, True, False, Nothing, Nothing, XDSAdministrationBL.CommercialName.FinancialYearEndOptions, Nothing)
  Public ReadOnly FinancialEffectiveDate As New DateTimeElement("FinancialEffectiveDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly AuthorisedCapitalAmt As New DecimalElement("AuthorisedCapitalAmt", Me, False, True, True, True, False, "Authorised Capital Amount", Nothing, "c", Nothing, Nothing, Nothing)
  Public ReadOnly AuthorisedNoOfShares As New DecimalElement("AuthorisedNoOfShares", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly IssuedCapitalAmt As New DecimalElement("IssuedCapitalAmt", Me, False, True, True, True, False, Nothing, Nothing, "c", Nothing, Nothing, Nothing)
  Public ReadOnly IssuedNoOfShares As New DecimalElement("IssuedNoOfShares", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CKReceivedDate As New DateTimeElement("CKReceivedDate", Me, False, True, True, True, False, "CK Received Date", Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly CKOnFormDate As New DateTimeElement("CKOnFormDate", Me, False, True, True, True, False, "CK On Form Date", Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly TaxNo As New StringElement("TaxNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CountryOfOrigin As New StringElement("CountryOfOrigin", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RecordStatusInd As New IndicatorElement("RecordStatusInd", Me, False, False, False, True, True, True, "Record Status", Nothing, XDSAdministrationBL.CommercialName.RecordStatusIndOptions, "A")
  Public ReadOnly DeletedReason As New StringElement("DeletedReason", Me, False, True, True, True, False, 500, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LastUpdatedDate As New DateTimeElement("LastUpdatedDate", Me, False, False, True, True, True, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Loader As Relationship = New LoaderRelationship("Loader", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly SIC As Relationship = New SICRelationship("SIC", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly CommercialStatus As Relationship = New CommercialStatusRelationship("CommercialStatus", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly CommercialType As Relationship = New CommercialTypeRelationship("CommercialType", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly Commercial As Relationship = New CommercialRelationship("Commercial", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, False)
  Public ReadOnly _LoaderID As New IntegerElement("LoaderID", Me, False)
  Public ReadOnly _SICCode As New StringElement("SICCode", Me, False)
  Public ReadOnly _CommercialStatusCode As New StringElement("CommercialStatusCode", Me, False)
  Public ReadOnly _CommercialTypeCode As New StringElement("CommercialTypeCode", Me, False)
  Public ReadOnly _CommercialID As New IntegerElement("CommercialID", Me, True)
  Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
  Public ReadOnly _Loader As LoaderRelationship = Loader
  Public ReadOnly _SIC As SICRelationship = SIC
  Public ReadOnly _CommercialStatus As CommercialStatusRelationship = CommercialStatus
  Public ReadOnly _CommercialType As CommercialTypeRelationship = CommercialType
  Public ReadOnly _Commercial As CommercialRelationship = Commercial
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moFinancialYearEndOptions As IndicatorOptions
  Public Shared ReadOnly Property FinancialYearEndOptions() As IndicatorOptions
    Get
      If moFinancialYearEndOptions Is Nothing Then
        moFinancialYearEndOptions = New IndicatorOptions
        moFinancialYearEndOptions.Add(1, "January")
        moFinancialYearEndOptions.Add(2, "February")
        moFinancialYearEndOptions.Add(3, "March")
        moFinancialYearEndOptions.Add(4, "April")
        moFinancialYearEndOptions.Add(5, "May")
        moFinancialYearEndOptions.Add(6, "June")
        moFinancialYearEndOptions.Add(7, "July")
        moFinancialYearEndOptions.Add(8, "August")
        moFinancialYearEndOptions.Add(9, "September")
        moFinancialYearEndOptions.Add(10, "October")
        moFinancialYearEndOptions.Add(11, "November")
        moFinancialYearEndOptions.Add(12, "December")
      End If
      Return moFinancialYearEndOptions
    End Get
  End Property
  Private Shared moRecordStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property RecordStatusIndOptions() As IndicatorOptions
    Get
      If moRecordStatusIndOptions Is Nothing Then
        moRecordStatusIndOptions = New IndicatorOptions
        moRecordStatusIndOptions.Add("A", "Active")
        moRecordStatusIndOptions.Add("D", "Deleted")
      End If
      Return moRecordStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("CommercialName", ApplicationSettings)
    _Subscriber._SubscriberID.LocalElement = _SubscriberID
    _Loader._LoaderID.LocalElement = _LoaderID
    _SIC._SICCode.LocalElement = _SICCode
    _CommercialStatus._CommercialStatusCode.LocalElement = _CommercialStatusCode
    _CommercialType._CommercialTypeCode.LocalElement = _CommercialTypeCode
    _Commercial._CommercialID.LocalElement = _CommercialID
    'sembleWare: Constructor End - Do Not Modify
    Me.DeleteType = CustomDBPart.enumDeleteType.ChangeRecordStatus
  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class CommercialNameRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _CommercialNameID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _CommercialID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New CommercialName(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _CommercialNameID.ForeignElement = CType(Instance, CommercialName).CommercialNameID
    _CommercialID.ForeignElement = CType(Instance, CommercialName)._CommercialID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0CommercialName")
    oList.Columns.Add(New ListColumn("CommercialID", "[A0CommercialName].[CommercialID]", "CommercialID", DataType.Integer, Nothing, True, 0, False, 100, "Commercial ID"))
    oList.Columns.Add(New ListColumn("CommercialNameID", "[A0CommercialName].[CommercialNameID]", "CommercialNameID", DataType.Long, Nothing, True, 0, False, 100, "Commercial Name ID"))
    oList.Columns.Add(New ListColumn("LastUpdatedDate", "[A0CommercialName].[LastUpdatedDate]", "LastUpdatedDate", DataType.DateTime, "g", False, -1, True, 100, "Last Updated Date"))
    oList.Columns.Add(New ListColumn("RegistrationNo", "[A0CommercialName].[RegistrationNo]", "RegistrationNo", DataType.String, Nothing, False, 0, True, 100, "Registration No"))
    oList.Columns.Add(New ListColumn("RegistrationDate", "[A0CommercialName].[RegistrationDate]", "RegistrationDate", DataType.DateTime, "d", False, 0, True, 100, "Registration Date"))
    oList.Columns.Add(New ListColumn("CommercialName", "[A0CommercialName].[CommercialName]", "CommercialName", DataType.String, Nothing, False, 0, True, 100, "Commercial Name"))
    oList.Columns.Add(New ListColumn("CommercialShortName", "[A0CommercialName].[CommercialShortName]", "CommercialShortName", DataType.String, Nothing, False, 0, True, 100, "Commercial Short Name"))
    oList.Columns.Add(New ListColumn("CommercialTranslatedName", "[A0CommercialName].[CommercialTranslatedName]", "CommercialTranslatedName", DataType.String, Nothing, False, 0, True, 100, "Commercial Translated Name"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A1Subscriber].[SubscriberID]", "SubscriberID", DataType.Long, Nothing, False, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberName", "[A1Subscriber].[SubscriberName]", "SubscriberName", DataType.String, Nothing, False, 0, True, 200, "Subscriber Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "([CommercialName]  [A0CommercialName]" + _
           "    left join [Subscriber]  [A1Subscriber] on [A0CommercialName].[SubscriberID] = [A1Subscriber].[SubscriberID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
