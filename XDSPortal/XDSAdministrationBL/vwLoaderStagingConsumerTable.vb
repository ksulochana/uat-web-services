Imports sembleWare.Runtime
Imports System
Imports System.Text
Public Class vwLoaderStagingConsumerTable 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly vwLoaderStagingTable As Relationship = New vwLoaderStagingTableRelationship("vwLoaderStagingTable", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly Consumer As Relationship = New ConsumerRelationship("Consumer", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _RecordID As New IntegerElement("RecordID", Me, True)
  Public ReadOnly _ConsumerID As New IntegerElement("ConsumerID", Me, True)
  Public ReadOnly _vwLoaderStagingTable As vwLoaderStagingTableRelationship = vwLoaderStagingTable
  Public ReadOnly _Consumer As ConsumerRelationship = Consumer
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("vwLoaderStagingConsumerTable", ApplicationSettings)
    _vwLoaderStagingTable._RecordID.LocalElement = _RecordID
    _Consumer._ConsumerID.LocalElement = _ConsumerID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"

  Public Sub UpdateConsumer()

  End Sub

#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Load()
    Me.CreatedByUser.Persist = False
    Me.CreatedOnDate.Persist = False
    Me.ChangedByUser.Persist = False
    Me.ChangedOnDate.Persist = False

    Dim oMetaDatabase As MetaDatabase = New MetaDatabaseRelationship(ApplicationSettings).NewInstance()
    If ApplicationSettings.Settings.Contains(Constants.vwLoaderStagingTable.LoaderStagingTable_MetaDatabaseCode) Then
      oMetaDatabase.MetaDatabaseCode.Load(ApplicationSettings.Settings.Item(Constants.vwLoaderStagingTable.LoaderStagingTable_MetaDatabaseCode))
      oMetaDatabase.Load()
    End If

    Me.TableName = GetLoaderEngineManager().StagingMultipleMatchTableName(oMetaDatabase)

    MyBase.Load()
  End Sub
#End Region

#Region " Business Rules"
  Private Function GetLoaderEngineManager() As LoaderEngineManager.Manager
    Dim oSubscriber As Subscriber = New SubscriberRelationship(ApplicationSettings).NewInstance()
    oSubscriber.SubscriberID.Load(ApplicationSettings.Settings.Item("LoaderStagingTable_SubscriberID"))
    oSubscriber.Load()
    Dim ovwLoaderStagingTable As vwLoaderStagingTable = Me.vwLoaderStagingTable.Instance
    Dim oLoader As Loader = ovwLoaderStagingTable.Loader.Instance
    Dim oFileFormat As FileFormat = oLoader.FileFormat.Instance
    Dim oLoaderEngineManager As LoaderEngineManager.Manager = New LoaderEngineManager.Manager(oLoader.FileFormatDeployment.Instance, oFileFormat, ApplicationSettings)

    Return oLoaderEngineManager
  End Function
#End Region
End Class 'sembleWare: Part

Public Class vwLoaderStagingConsumerTableRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _RecordID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _ConsumerID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New vwLoaderStagingConsumerTable(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _RecordID.ForeignElement = CType(Instance, vwLoaderStagingConsumerTable)._RecordID
    _ConsumerID.ForeignElement = CType(Instance, vwLoaderStagingConsumerTable)._ConsumerID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0vwLoaderStagingConsumerTable")
    oList.Columns.Add(New ListColumn("RecordID", "[A0vwLoaderStagingConsumerTable].[RecordID]", "RecordID", DataType.Integer, Nothing, True, 0, False, 100, "Record ID"))
    oList.Columns.Add(New ListColumn("ConsumerID", "[A0vwLoaderStagingConsumerTable].[ConsumerID]", "ConsumerID", DataType.Integer, Nothing, True, 0, False, 100, "Consumer ID"))
    oList.Columns.Add(New ListColumn("ConsumerID1", "[A1Consumer].[ConsumerID]", "ConsumerID", DataType.Long, Nothing, False, 0, True, 100, "Consumer ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A1Consumer].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("PassportNo", "[A1Consumer].[PassportNo]", "PassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No / Other ID No"))
    oList.Columns.Add(New ListColumn("FirstInitial", "[A1Consumer].[FirstInitial]", "FirstInitial", DataType.String, Nothing, False, 0, True, 100, "First Initial"))
    oList.Columns.Add(New ListColumn("FirstName", "[A1Consumer].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 200, "First Name"))
    oList.Columns.Add(New ListColumn("Surname", "[A1Consumer].[Surname]", "Surname", DataType.String, Nothing, False, 0, True, 200, "Surname"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A1Consumer].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Date of Birth"))
    oList.Columns.Add(New ListColumn("GenderInd", "[A1Consumer].[GenderInd]", "GenderInd", DataType.String, Nothing, False, 0, True, 100, "Gender"))
    oList.SelectStatement = "select"
    oList.FromClause = "([vwLoaderStagingConsumerTable]  [A0vwLoaderStagingConsumerTable]" + _
           "    join [Consumer]  [A1Consumer] on [A0vwLoaderStagingConsumerTable].[ConsumerID] = [A1Consumer].[ConsumerID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
