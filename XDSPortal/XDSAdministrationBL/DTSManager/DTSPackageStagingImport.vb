Imports System
Imports System.IO
Imports System.Text
Imports sembleWare.Runtime
Imports SHBS.SQLDTS
Imports XDSAdministrationBL

Namespace DTSManager
  Friend Class DTSPackageStagingImport
#Region " Variables"
    Private moLoaderEngineManager As LoaderEngineManager.Manager
#End Region

#Region " Properties"
    Public ReadOnly Property PackageName() As String
      Get
        Return DTSPackageStagingImport.PackageName(moLoaderEngineManager.FileFormat, moLoaderEngineManager.FileFormatDeploymentID)
      End Get
    End Property
#End Region

#Region " Shared Properties"
    Public Shared ReadOnly Property PackageName(ByVal FileFormat As FileFormat, ByVal FileFormatDeploymentID As Integer) As String
      Get
        Return Constants.Package & FileFormat.FileFormatCode.Value & "_" & FileFormatDeploymentID.ToString() & Constants.Import
      End Get
    End Property
#End Region

#Region " Construtors"
    Public Sub New(ByVal LoaderEngineManager As LoaderEngineManager.Manager)
      moLoaderEngineManager = LoaderEngineManager
    End Sub
#End Region

#Region " Methods"
    Public Sub RemoveDTSPackage()
      Dim oPackage As Package = New Package(Me.PackageName)
      oPackage.RemovePackageFromSQLServer(moLoaderEngineManager.ServerName, moLoaderEngineManager.Username, moLoaderEngineManager.Password)
    End Sub

    Public Sub CreateDTSPackage()
      Dim nConnectionID As Integer = 1
      Dim oPackage As Package = New Package(Me.PackageName)
      Dim oFileConnection As Connections.FileConnection
      Dim sSourceFilePath As String = "Text.txt"
      Dim oDataRow As DataRow

      oPackage.GlobalVariables.Add(oPackage.NewGlobalVariable(Constants.Field_LoaderID, 1))
      oPackage.GlobalVariables.Add(oPackage.NewGlobalVariable(Constants.Field_SubscriberID, 1))
      oPackage.GlobalVariables.Add(oPackage.NewGlobalVariable(Constants.Field_IsValidRecordYN, False))
      oPackage.GlobalVariables.Add(oPackage.NewGlobalVariable(Constants.Field_IsPossibleNameConflictYN, False))
      oPackage.GlobalVariables.Add(oPackage.NewGlobalVariable(Constants.Field_IsPossibleDuplicateRecordYN, False))
      oPackage.GlobalVariables.Add(oPackage.NewGlobalVariable(Constants.Field_RequiredValidationDataProcedureDetails, ""))
      oPackage.GlobalVariables.Add(oPackage.NewGlobalVariable(Constants.Field_NonRequiredValidationDataProcedureDetails, ""))

      Dim sRowDelimiter As String = DTSPackageLibrary.GetFileRowDelimiter(moLoaderEngineManager)
      Dim nNoOfRowsToSkip As Integer = moLoaderEngineManager.FileFormat.NoOfRowsToSkip.Value
      Dim sColumnLengths As String = DTSPackageLibrary.GetFileFormatColumnLengths(moLoaderEngineManager)
      Dim nColumnCount As Integer = DTSPackageLibrary.GetFileFormatColumnCount(moLoaderEngineManager)
      Dim sColumnDelimiter As String = DTSPackageLibrary.GetFileFormatColumnDelimiter(moLoaderEngineManager)
      Dim sTextQualifer As String = DTSPackageLibrary.GetFileTextQualifier(moLoaderEngineManager)
      Dim bIsFirstRowColumnNameYN As Boolean = moLoaderEngineManager.FileFormat.IsFirstRowColumnNameYN.Value

      Select Case moLoaderEngineManager.FileFormat.FileFormatFieldTypeInd.Value
        Case XDSAdministrationBL.Constants.FileFormat.FieldTypes.FixedField
          oFileConnection = oPackage.NewSourceFileConnection(nConnectionID, Constants.File, sSourceFilePath, sRowDelimiter, nNoOfRowsToSkip, sColumnLengths, nColumnCount, bIsFirstRowColumnNameYN, 255, 60)
        Case XDSAdministrationBL.Constants.FileFormat.FieldTypes.Delimted
          oFileConnection = oPackage.NewSourceFileConnection(nConnectionID, Constants.File, sSourceFilePath, sRowDelimiter, sColumnDelimiter, sTextQualifer, nNoOfRowsToSkip, bIsFirstRowColumnNameYN, 255, 60)
      End Select

      For Each oDataRow In moLoaderEngineManager.MetaDatabaseList(Nothing).DataTable.Rows
        Dim oMetaDatabase As MetaDatabase = New MetaDatabaseRelationship(moLoaderEngineManager.ApplicationSettings).NewInstance()
        oMetaDatabase.MetaDatabaseCode.Load(oDataRow("MetaDatabaseCode"))
        oMetaDatabase.Load()

        nConnectionID += 1
        Select Case oMetaDatabase.MetaDatabaseTypeInd.Value
          Case XDSAdministrationBL.Constants.MetaDatabase.Types.System
            CreateStagingPackageItems(oPackage, oMetaDatabase, oFileConnection.ConnectionID, sSourceFilePath, nConnectionID)
          Case XDSAdministrationBL.Constants.MetaDatabase.Types.Repository
            CreateRepositoryPackageItems(oPackage, oMetaDatabase, oFileConnection.ConnectionID, sSourceFilePath, nConnectionID)
        End Select
      Next

      oPackage.SavePackageToSQLServer(moLoaderEngineManager.ServerName, moLoaderEngineManager.Username, moLoaderEngineManager.Password, True)
    End Sub
#End Region

#Region " Helper Functions"
    Private Sub CreateStagingPackageItems(ByVal Package As Package, ByVal MetaDatabase As MetaDatabase, ByVal FileConnectionID As Integer, ByVal SourceFilePath As String, ByVal ConnectionID As Integer)
      Dim oString As StringBuilder
      Dim oList As List = moLoaderEngineManager.FileFormat._FileFormatField_OwnMany.GridList
      Dim oDataRow As DataRow
      Dim nColumnCounter As Integer = 1

      Dim oStep As Steps.Step = Package.NewStep(Constants.Step_Staging, Constants.Transform, Constants.Task_Staging & MetaDatabase.MetaDatabaseCode.Value)
      Dim oEncryption As sembleWare.Security.Encryption = New sembleWare.Security.Encryption(XDSAdministrationBL.Constants.PasswordHashString)
      Dim oSQLConnection As Connections.SQLConnection = Package.NewSQLConnection(ConnectionID, Constants.SQL_Staging & MetaDatabase.MetaDatabaseCode.Value, moLoaderEngineManager.ServerName, moLoaderEngineManager.DatabaseName, False, moLoaderEngineManager.Username, moLoaderEngineManager.Password, 60)
      Dim oDataPumpTask As Tasks.DataPumpTask = Package.NewDataPumpTask(Constants.Task_Staging & MetaDatabase.MetaDatabaseCode.Value, Constants.Transform, FileConnectionID, False, SourceFilePath, oSQLConnection.ConnectionID, SQLManager.StagingTable.TableName(MetaDatabase, moLoaderEngineManager.FileFormat, moLoaderEngineManager.FileFormatDeploymentID), "|", vbCrLf)

      ' Create the File Type Fields
      oList.AndFilters.Add(New ListFilter("FileFormatFieldTypeInd", "FileFormatFieldTypeInd", XDSAdministrationBL.Constants.FileFormatField.Types.File, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      For Each oDataRow In oList.DataTable.Rows
        Dim sFieldName As String = moLoaderEngineManager.ParseFileFormatFieldDesc(oDataRow("FileFormatFieldDesc"))
        Dim nFieldLength As Integer = System.Convert.ToInt32(SHBS.General.ZeroIfDBNull(oDataRow("FieldLength")))

        oString = New StringBuilder
        oString.Append("Function Main()" & vbNewLine)
        oString.Append("	if isnull(DTSSource(""" & Constants.Col & nColumnCounter.ToString().PadLeft(3, "0") & """)) then" & vbNewLine)
        oString.Append("	  DTSDestination(""" & sFieldName & """) = null" & vbNewLine)
        oString.Append("	else" & vbNewLine)
        oString.Append("	  dim sString1" & vbNewLine)
        oString.Append("  	dim sString2" & vbNewLine)
        oString.Append("	  sString1 = ltrim(rtrim(DTSSource(""" & Constants.Col & nColumnCounter.ToString().PadLeft(3, "0") & """)))" & vbNewLine)
        oString.Append("	  sString2 = replace(sString1, ""0"", """")" & vbNewLine)
        oString.Append("	  if (sString1 = """") or (sString2 = """") then" & vbNewLine)
        oString.Append("	    DTSDestination(""" & sFieldName & """) = null" & vbNewLine)
        oString.Append("	  else" & vbNewLine)
        oString.Append("	    DTSDestination(""" & sFieldName & """) = sString1" & vbNewLine)
        oString.Append("	  end if" & vbNewLine)
        oString.Append("  end if" & vbNewLine)
        oString.Append("	Main = DTSTransformStat_OK" & vbNewLine)
        oString.Append("End Function")
        oDataPumpTask.NewDataPumpTransformationScript(Constants.Transformation & nColumnCounter.ToString(), sFieldName, 129, 120, nFieldLength, 0, 0, True, oString.ToString(), "", "")

        nColumnCounter += 1
      Next

      oString = New StringBuilder
      oString.Append("Function Main()" & vbNewLine)
      oString.Append("	DTSDestination(""" & Constants.Field_LoaderID & """) = DTSGlobalVariables(""" & Constants.Field_LoaderID & """).Value" & vbNewLine)
      oString.Append("	Main = DTSTransformStat_OK" & vbNewLine)
      oString.Append("End Function")
      oDataPumpTask.NewDataPumpTransformationScript(Constants.Transformation & nColumnCounter.ToString(), Constants.Field_LoaderID, 3, 120, 0, 0, 0, False, oString.ToString(), "", "")
      nColumnCounter += 1

      oString = New StringBuilder
      oString.Append("Function Main()" & vbNewLine)
      oString.Append("	DTSDestination(""" & Constants.Field_SubscriberID & """) = DTSGlobalVariables(""" & Constants.Field_SubscriberID & """).Value" & vbNewLine)
      oString.Append("	Main = DTSTransformStat_OK" & vbNewLine)
      oString.Append("End Function")
      oDataPumpTask.NewDataPumpTransformationScript(Constants.Transformation & nColumnCounter.ToString(), Constants.Field_SubscriberID, 3, 120, 0, 0, 0, False, oString.ToString(), "", "")
      nColumnCounter += 1

      oString = New StringBuilder
      oString.Append("Function Main()" & vbNewLine)
      oString.Append("	DTSDestination(""" & Constants.Field_IsValidRecordYN & """) = DTSGlobalVariables(""" & Constants.Field_IsValidRecordYN & """).Value" & vbNewLine)
      oString.Append("	Main = DTSTransformStat_OK" & vbNewLine)
      oString.Append("End Function")
      oDataPumpTask.NewDataPumpTransformationScript(Constants.Transformation & nColumnCounter.ToString(), Constants.Field_IsValidRecordYN, 3, 120, 0, 0, 0, False, oString.ToString(), "", "")
      nColumnCounter += 1

      oString = New StringBuilder
      oString.Append("Function Main()" & vbNewLine)
      oString.Append("	DTSDestination(""" & Constants.Field_IsPossibleNameConflictYN & """) = DTSGlobalVariables(""" & Constants.Field_IsPossibleNameConflictYN & """).Value" & vbNewLine)
      oString.Append("	Main = DTSTransformStat_OK" & vbNewLine)
      oString.Append("End Function")
      oDataPumpTask.NewDataPumpTransformationScript(Constants.Transformation & nColumnCounter.ToString(), Constants.Field_IsPossibleNameConflictYN, 3, 120, 0, 0, 0, False, oString.ToString(), "", "")
      nColumnCounter += 1

      oString = New StringBuilder
      oString.Append("Function Main()" & vbNewLine)
      oString.Append("	DTSDestination(""" & Constants.Field_IsPossibleDuplicateRecordYN & """) = DTSGlobalVariables(""" & Constants.Field_IsPossibleDuplicateRecordYN & """).Value" & vbNewLine)
      oString.Append("	Main = DTSTransformStat_OK" & vbNewLine)
      oString.Append("End Function")
      oDataPumpTask.NewDataPumpTransformationScript(Constants.Transformation & nColumnCounter.ToString(), Constants.Field_IsPossibleDuplicateRecordYN, 3, 120, 0, 0, 0, False, oString.ToString(), "", "")
      nColumnCounter += 1

      oString = New StringBuilder
      oString.Append("Function Main()" & vbNewLine)
      oString.Append("	DTSDestination(""" & Constants.Field_RequiredValidationDataProcedureDetails & """) = DTSGlobalVariables(""" & Constants.Field_RequiredValidationDataProcedureDetails & """).Value" & vbNewLine)
      oString.Append("	Main = DTSTransformStat_OK" & vbNewLine)
      oString.Append("End Function")
      oDataPumpTask.NewDataPumpTransformationScript(Constants.Transformation & nColumnCounter.ToString(), Constants.Field_RequiredValidationDataProcedureDetails, 129, 120, 0, 0, 0, False, oString.ToString(), "", "")
      nColumnCounter += 1

      oString = New StringBuilder
      oString.Append("Function Main()" & vbNewLine)
      oString.Append("	DTSDestination(""" & Constants.Field_NonRequiredValidationDataProcedureDetails & """) = DTSGlobalVariables(""" & Constants.Field_NonRequiredValidationDataProcedureDetails & """).Value" & vbNewLine)
      oString.Append("	Main = DTSTransformStat_OK" & vbNewLine)
      oString.Append("End Function")
      oDataPumpTask.NewDataPumpTransformationScript(Constants.Transformation & nColumnCounter.ToString(), Constants.Field_NonRequiredValidationDataProcedureDetails, 129, 120, 0, 0, 0, False, oString.ToString(), "", "")

      nColumnCounter += 1
      ' Create the Loader Type Fields
      oList = moLoaderEngineManager.FileFormat._FileFormatField_OwnMany.GridList
      oList.AndFilters.Add(New ListFilter("FileFormatFieldTypeInd", "FileFormatFieldTypeInd", XDSAdministrationBL.Constants.FileFormatField.Types.Loader, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      For Each oDataRow In oList.DataTable.Rows
        Dim sFieldName As String = moLoaderEngineManager.ParseFileFormatFieldDesc(oDataRow("FileFormatFieldDesc"))

        Package.GlobalVariables.Add(Package.NewGlobalVariable(sFieldName, ""))

        oString = New StringBuilder
        oString.Append("Function Main()" & vbNewLine)
        oString.Append("	DTSDestination(""" & sFieldName & """) = DTSGlobalVariables(""" & sFieldName & """).Value" & vbNewLine)
        oString.Append("	Main = DTSTransformStat_OK" & vbNewLine)
        oString.Append("End Function")
        oDataPumpTask.NewDataPumpTransformationScript(Constants.Transformation & nColumnCounter.ToString(), sFieldName, 129, 120, 0, 0, 0, False, oString.ToString(), "", "")
        nColumnCounter += 1
      Next
    End Sub

    Private Sub CreateRepositoryPackageItems(ByVal Package As Package, ByVal MetaDatabase As MetaDatabase, ByVal FileConnectionID As Integer, ByVal SourceFilePath As String, ByVal ConnectionID As Integer)
      ConnectionID += 1
      Dim oStep As Steps.Step = Package.NewStep(Constants.Step_Repository & MetaDatabase.MetaDatabaseCode.Value, Constants.Transform, Constants.Task_Repository & MetaDatabase.MetaDatabaseCode.Value)
      Dim oEncryption As sembleWare.Security.Encryption = New sembleWare.Security.Encryption(XDSAdministrationBL.Constants.PasswordHashString)
      Dim oSQLConnection As Connections.SQLConnection = Package.NewSQLConnection(ConnectionID, Constants.SQL_Repository & MetaDatabase.MetaDatabaseCode.Value, MetaDatabase.ServerName.Value, MetaDatabase.DatabaseName.Value, False, MetaDatabase.Username.Value, oEncryption.Decrypt(MetaDatabase.Password.Value), 60)
      Dim oDataPumpTask As Tasks.DataPumpTask = Package.NewDataPumpTask(Constants.Task_Repository & MetaDatabase.MetaDatabaseCode.Value, Constants.Transform, FileConnectionID, False, SourceFilePath, oSQLConnection.ConnectionID, SQLManager.StagingTable.TableName(MetaDatabase, moLoaderEngineManager.FileFormat, moLoaderEngineManager.FileFormatDeploymentID), "|", vbCrLf)
      Dim oList2 As List = moLoaderEngineManager.FileFormat._FileFormatField_OwnMany.GridList
      Dim oDataRow2 As DataRow
      Dim nColumnCounter As Integer = 1

      oList2.AndFilters.Add(New ListFilter("FileFormatFieldTypeInd", "FileFormatFieldTypeInd", XDSAdministrationBL.Constants.FileFormatField.Types.File, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      For Each oDataRow2 In oList2.DataTable.Rows
        Dim sFieldName As String = moLoaderEngineManager.ParseFileFormatFieldDesc(oDataRow2("FileFormatFieldDesc"))
        Dim nFieldLength As Integer = System.Convert.ToInt32(SHBS.General.ZeroIfDBNull(oDataRow2("FieldLength")))

        oDataPumpTask.NewDataPumpTransformationCopy(Constants.Transformation & nColumnCounter.ToString(), Constants.Col & nColumnCounter.ToString().PadLeft(3, "0"), 129, 48, nFieldLength, 0, 0, False, sFieldName, 129, 120, nFieldLength, 0, 0, False)
        nColumnCounter += 1
      Next

      Dim oString As StringBuilder = New StringBuilder
      oString.Append("Function Main()" & vbNewLine)
      oString.Append("	DTSDestination(""" & Constants.Field_LoaderID & """) = DTSGlobalVariables(""" & Constants.Field_LoaderID & """).Value" & vbNewLine)
      oString.Append("	Main = DTSTransformStat_OK" & vbNewLine)
      oString.Append("End Function")
      oDataPumpTask.NewDataPumpTransformationScript(Constants.Transformation & nColumnCounter.ToString(), Constants.Field_LoaderID, 3, 120, 0, 0, 0, False, oString.ToString(), "", "")
    End Sub
#End Region
  End Class
End Namespace