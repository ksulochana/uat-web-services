Imports System
Imports System.IO
Imports System.Text
Imports sembleWare.Runtime
Imports SHBS.SQLDTS
Imports XDSAdministrationBL

Namespace DTSManager
  Friend Class DTSPackageLibrary
#Region " Shared Procedures"
    Public Shared Function GetFileTextQualifier(ByVal LoaderEngineManager As LoaderEngineManager.Manager) As String
      Select Case LoaderEngineManager.FileFormat.TextQualiferInd.Value
        Case XDSAdministrationBL.Constants.FileFormat.TextQualiferTypes.DoubleQuote
          Return """"
        Case XDSAdministrationBL.Constants.FileFormat.TextQualiferTypes.SingleQuote
          Return "'"
        Case XDSAdministrationBL.Constants.FileFormat.TextQualiferTypes.None
          Return ""
      End Select
      Return ""
    End Function

    Public Shared Function GetFileRowDelimiter(ByVal LoaderEngineManager As LoaderEngineManager.Manager) As String
      Select Case LoaderEngineManager.FileFormat.RowDelimiterInd.Value
        Case XDSAdministrationBL.Constants.FileFormat.RowDelimiterTypes.CRLF
          Return vbCrLf
        Case XDSAdministrationBL.Constants.FileFormat.RowDelimiterTypes.CR
          Return vbCr
        Case XDSAdministrationBL.Constants.FileFormat.RowDelimiterTypes.LF
          Return vbLf
        Case XDSAdministrationBL.Constants.FileFormat.RowDelimiterTypes.Semicolon
          Return ";"
        Case XDSAdministrationBL.Constants.FileFormat.RowDelimiterTypes.Comma
          Return ","
        Case XDSAdministrationBL.Constants.FileFormat.RowDelimiterTypes.Tab
          Return vbTab
        Case Else
          Return ""
      End Select
      Return ""
    End Function

    Public Shared Function GetFileFormatColumnLengths(ByVal LoaderEngineManager As LoaderEngineManager.Manager) As String
      Dim oString As StringBuilder = New StringBuilder
      Dim oColumnLengthsString As StringBuilder = New StringBuilder

      oString.Append(" select FieldLength ")
      oString.Append(" from FileFormatField ")
      oString.Append(CustomDBPart.InstanceWhereClause(LoaderEngineManager.FileFormat))
      oString.Append(" and FileFormatFieldTypeInd = " & LoaderEngineManager.ApplicationSettings.QueryBuilder.ToSQL(XDSAdministrationBL.Constants.FileFormat.FieldTypes.FixedField))
      oString.Append(" order by SequenceNum")

      Dim oDataRow As DataRow
      Dim oDataTable As DataTable = LoaderEngineManager.ApplicationSettings.ResultQuery(oString.ToString())

      For Each oDataRow In oDataTable.Rows
        oColumnLengthsString.Append(SHBS.General.ZeroIfDBNull(oDataRow(0)))
        oColumnLengthsString.Append(",")
      Next
      If oColumnLengthsString.Length > 0 Then
        oColumnLengthsString.Remove(oColumnLengthsString.Length - 1, 1)
      End If
      Return oColumnLengthsString.ToString()
    End Function

    Public Shared Function GetFileFormatColumnCount(ByVal LoaderEngineManager As LoaderEngineManager.Manager) As Integer
      Dim oString As StringBuilder = New StringBuilder

      oString.Append(" select count(FileFormatFieldID) ")
      oString.Append(" from FileFormatField ")
      oString.Append(CustomDBPart.InstanceWhereClause(LoaderEngineManager.FileFormat))
      oString.Append(" and FileFormatFieldTypeInd = " & LoaderEngineManager.ApplicationSettings.QueryBuilder.ToSQL(XDSAdministrationBL.Constants.FileFormat.FieldTypes.FixedField))

      Dim oDataRow As DataRow
      Dim oDataTable As DataTable = LoaderEngineManager.ApplicationSettings.ResultQuery(oString.ToString())

      For Each oDataRow In oDataTable.Rows
        Return System.Convert.ToInt32(SHBS.General.ZeroIfDBNull(oDataRow(0)))
      Next
      Return 0
    End Function

    Public Shared Function GetFileFormatColumnDelimiter(ByVal LoaderEngineManager As LoaderEngineManager.Manager) As String
      Select Case LoaderEngineManager.FileFormat.FileFormatFieldTypeInd.Value
        Case XDSAdministrationBL.Constants.FileFormat.FieldTypes.Delimted
          Select Case LoaderEngineManager.FileFormat.DelimiterTypeInd.Value
            Case XDSAdministrationBL.Constants.FileFormat.DelimiterTypes.Comma
              Return ","
            Case XDSAdministrationBL.Constants.FileFormat.DelimiterTypes.Semicolon
              Return ";"
            Case XDSAdministrationBL.Constants.FileFormat.DelimiterTypes.Tab
              Return vbTab
            Case Else
              Return LoaderEngineManager.FileFormat.DelimitedCharacter.Value
          End Select
      End Select
      Return ""
    End Function
#End Region
  End Class
End Namespace