Imports System
Imports System.IO
Imports System.Text
Imports sembleWare.Runtime
Imports SHBS.SQLDTS
Imports XDSAdministrationBL

Namespace DTSManager
  Friend Class DTSPackageStagingExport
#Region " Variables"
    Private moMetaDatabase As MetaDatabase
    Private moLoaderEngineManager As LoaderEngineManager.Manager
    Private mRepositoryApplicationSettings As ApplicationSettings
#End Region

#Region " Properties"
    Public ReadOnly Property PackageName() As String
      Get
        Return DTSPackageStagingExport.PackageName(moMetaDatabase, moLoaderEngineManager.FileFormat, moLoaderEngineManager.FileFormatDeploymentID)
      End Get
    End Property
#End Region

#Region " Shared Properties"
    Public Shared ReadOnly Property PackageName(ByVal MetaDatabase As MetaDatabase, ByVal FileFormat As FileFormat, ByVal FileFormatDeploymentID As Integer) As String
      Get
        Return Constants.Package & MetaDatabase.MetaDatabaseCode.Value & FileFormat.FileFormatCode.Value & "_" & FileFormatDeploymentID.ToString() & Constants.Export
      End Get
    End Property
#End Region

#Region " Construtors"
    Public Sub New(ByVal MetaDatabase As MetaDatabase, ByVal LoaderEngineManager As LoaderEngineManager.Manager)
      moMetaDatabase = MetaDatabase
      moLoaderEngineManager = LoaderEngineManager
    End Sub
#End Region

#Region " Methods"
    Public Sub RemoveDTSPackage()
      Dim oPackage As Package = New Package(Me.PackageName)
      oPackage.RemovePackageFromSQLServer(moLoaderEngineManager.ServerName, moLoaderEngineManager.Username, moLoaderEngineManager.Password)
    End Sub

    Public Sub CreateDTSPackage()
      Dim nConnectionID As Integer = 1
      Dim oPackage As Package = New Package(Me.PackageName)
      Dim oFileConnection As Connections.FileConnection
      Dim sSourceFilePath As String = "Text.txt"

      Select Case moLoaderEngineManager.FileFormat.FileFormatFieldTypeInd.Value
        Case "F"
          oFileConnection = oPackage.NewDestinationFileConnection(nConnectionID, Constants.File, sSourceFilePath, DTSPackageLibrary.GetFileRowDelimiter(moLoaderEngineManager), moLoaderEngineManager.FileFormat.NoOfRowsToSkip.Value, DTSPackageLibrary.GetFileFormatColumnLengths(moLoaderEngineManager) + ",100,100", DTSPackageLibrary.GetFileFormatColumnCount(moLoaderEngineManager) + 2, moLoaderEngineManager.FileFormat.IsFirstRowColumnNameYN.Value, 255, 60)
        Case "D"
          Dim sDelimitedCharacter As String = ""

          Select Case moLoaderEngineManager.FileFormat.DelimiterTypeInd.Value
            Case XDSAdministrationBL.Constants.FileFormat.DelimiterTypes.Comma
              sDelimitedCharacter = ","
            Case XDSAdministrationBL.Constants.FileFormat.DelimiterTypes.Semicolon
              sDelimitedCharacter = ";"
            Case XDSAdministrationBL.Constants.FileFormat.DelimiterTypes.Tab
              sDelimitedCharacter = vbTab
            Case Else
              sDelimitedCharacter = moLoaderEngineManager.FileFormat.DelimitedCharacter.Value
          End Select
          oFileConnection = oPackage.NewDestinationFileConnection(nConnectionID, Constants.File, sSourceFilePath, DTSPackageLibrary.GetFileRowDelimiter(moLoaderEngineManager), sDelimitedCharacter, "", moLoaderEngineManager.FileFormat.NoOfRowsToSkip.Value, moLoaderEngineManager.FileFormat.IsFirstRowColumnNameYN.Value, 255, 60)
      End Select

      CreatePackageItems(oPackage, oFileConnection.ConnectionID, sSourceFilePath, nConnectionID)

      oPackage.SavePackageToSQLServer(moLoaderEngineManager.ServerName, moLoaderEngineManager.Username, moLoaderEngineManager.Password, True)
    End Sub
#End Region

#Region " Helper Functions"
    Private Sub CreatePackageItems(ByVal Package As Package, ByVal FileConnectionID As Integer, ByVal SourceFilePath As String, ByRef ConnectionID As Integer)
      Dim oList As List = moLoaderEngineManager.FileFormat._FileFormatField_OwnMany.GridList
      Dim oDataRow As DataRow
      Dim nColumnCounter As Integer = 1
      Dim oQuery As StringBuilder = New StringBuilder

      oQuery.Append("select * ")
      oQuery.Append("  from " & SQLManager.StagingTable.TableName(moMetaDatabase, moLoaderEngineManager.FileFormat, moLoaderEngineManager.FileFormatDeploymentID))
      oQuery.Append(" where IsValidRecordYN = 0")
      oQuery.Append(" order by RecordID")

      ConnectionID += 1
      Dim oStep As Steps.Step = Package.NewStep(Constants.Step_Staging, Constants.Transform, Constants.Task_Staging & moMetaDatabase.MetaDatabaseCode.Value)
      Dim oSQLConnection As Connections.SQLConnection = Package.NewSQLConnection(ConnectionID, Constants.SQL_Staging & moMetaDatabase.MetaDatabaseCode.Value, moLoaderEngineManager.ServerName, moLoaderEngineManager.DatabaseName, False, moLoaderEngineManager.Username, moLoaderEngineManager.Password, 60)
      Dim oDataPumpTask As Tasks.DataPumpTask = Package.NewDataPumpTask(Constants.Task_Staging & moMetaDatabase.MetaDatabaseCode.Value, Constants.Transform, oSQLConnection.ConnectionID, True, oQuery.ToString(), FileConnectionID, SourceFilePath, "|", vbCrLf)

      oList.AndFilters.Add(New ListFilter("FileFormatFieldTypeInd", "FileFormatFieldTypeInd", "F", DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      For Each oDataRow In oList.DataTable.Rows
        Dim sFieldName As String = moLoaderEngineManager.ParseFileFormatFieldDesc(oDataRow("FileFormatFieldDesc"))
        Dim nFieldLength As Integer = System.Convert.ToInt32(SHBS.General.ZeroIfDBNull(oDataRow("FieldLength")))

        oDataPumpTask.NewDataPumpTransformationCopy(Constants.Transformation & nColumnCounter.ToString(), sFieldName, 129, 120, nFieldLength, 0, 0, False, Constants.Col & nColumnCounter.ToString().PadLeft(3, "0"), 129, 48, nFieldLength, 0, 0, False)
        nColumnCounter += 1
      Next

      oDataPumpTask.NewDataPumpTransformationCopy(Constants.Transformation & nColumnCounter.ToString(), Constants.Field_RequiredValidationDataProcedureDetails, 129, 120, 100, 0, 0, False, Constants.Col & nColumnCounter.ToString().PadLeft(3, "0"), 129, 48, 100, 0, 0, False)

      nColumnCounter += 1
      oDataPumpTask.NewDataPumpTransformationCopy(Constants.Transformation & nColumnCounter.ToString(), Constants.Field_NonRequiredValidationDataProcedureDetails, 129, 120, 100, 0, 0, False, Constants.Col & nColumnCounter.ToString().PadLeft(3, "0"), 129, 48, 100, 0, 0, False)
    End Sub
#End Region
  End Class
End Namespace