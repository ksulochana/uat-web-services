Imports System
Imports System.IO
Imports System.Text
Imports sembleWare.Runtime
Imports SHBS.SQLDTS
Imports XDSAdministrationBL

Namespace DTSManager
Friend Class Manager
#Region " Variables"
    Private moLoaderEngineManager As LoaderEngineManager.Manager
#End Region

#Region " Constructor"
    Public Sub New(ByVal LoaderEngineManager As LoaderEngineManager.Manager)
      moLoaderEngineManager = LoaderEngineManager
    End Sub
#End Region

#Region " Methods"
#Region " Deploy"
    Public Sub DeployImportPackages()
      Dim oDTSPackageStagingImport As DTSPackageStagingImport = New DTSPackageStagingImport(moLoaderEngineManager)
      oDTSPackageStagingImport.CreateDTSPackage()
    End Sub

    Public Sub DeployExportPackage(ByVal MetaDatabase As MetaDatabase)
      Select Case MetaDatabase.MetaDatabaseTypeInd.Value
        Case XDSAdministrationBL.Constants.MetaDatabase.Types.System
          Dim oDTSPackageStagingExport As DTSPackageStagingExport = New DTSPackageStagingExport(MetaDatabase, moLoaderEngineManager)
          oDTSPackageStagingExport.CreateDTSPackage()
      End Select
    End Sub
#End Region

#Region " Remove"
    Public Sub RemoveImportPackages()
      Dim oDTSPackageStagingImport As DTSPackageStagingImport = New DTSPackageStagingImport(moLoaderEngineManager)
      oDTSPackageStagingImport.RemoveDTSPackage()
    End Sub

    Public Sub RemoveExportPackage(ByVal MetaDatabase As MetaDatabase)
      Select Case MetaDatabase.MetaDatabaseTypeInd.Value
        Case XDSAdministrationBL.Constants.MetaDatabase.Types.System
          Dim oDTSPackageStagingExport As DTSPackageStagingExport = New DTSPackageStagingExport(MetaDatabase, moLoaderEngineManager)
          oDTSPackageStagingExport.RemoveDTSPackage()
      End Select
    End Sub
#End Region

    Public Sub ExecuteDTSImportPackage(ByVal Loader As Loader)
      Dim sFolderPath As String = SystemDefault.GetLoaderStorageFolderPath(moLoaderEngineManager.ApplicationSettings)
      Dim sFileName As String = System.IO.Path.Combine(sFolderPath, Loader.LoaderFileName.Value)
      Dim oPackage As Package = New Package
      Dim oDataRow As DataRow

      oPackage.LoadPackageFromSQLServer(moLoaderEngineManager.ServerName, moLoaderEngineManager.Username, moLoaderEngineManager.Password, DTSPackageStagingImport.PackageName(moLoaderEngineManager.FileFormat, moLoaderEngineManager.FileFormatDeploymentID))
      'TODO: Remove when system runs on Windows Server 2003
      'oPackage.GlobalVariables.Item(msFIELD_LOADERID).Value = oLoader.LoaderID.Value
      oPackage.GlobalVariables.Item(Constants.Field_LoaderID).Value = System.Convert.ToInt32(Loader.LoaderID.Value)
      oPackage.GlobalVariables.Item(Constants.Field_SubscriberID).Value = System.Convert.ToInt32(Loader._SubscriberID.Value)
      oPackage.GlobalVariables.Item(Constants.Field_IsValidRecordYN).Value = False
      oPackage.GlobalVariables.Item(Constants.Field_IsPossibleNameConflictYN).Value = False
      oPackage.GlobalVariables.Item(Constants.Field_IsPossibleDuplicateRecordYN).Value = False

      oPackage.Connections.Item(Constants.File).SetDataSource(sFileName)

      For Each oDataRow In moLoaderEngineManager.MetaDatabaseList(Nothing).DataTable.Rows
        Dim oMetaDatabase As MetaDatabase = New MetaDatabaseRelationship(moLoaderEngineManager.ApplicationSettings).NewInstance()
        oMetaDatabase.MetaDatabaseCode.Load(oDataRow("MetaDatabaseCode"))
        oMetaDatabase.Load()

        Select Case oMetaDatabase.MetaDatabaseTypeInd.Value
          Case XDSAdministrationBL.Constants.MetaDatabase.Types.System
            ' Setup the Loader Type Fields
            Dim oDataRow2 As DataRow
            Dim oList As List = Loader._LoaderParameter_OwnMany.GridList
            For Each oDataRow2 In oList.DataTable.Rows
              Dim sFieldName As String = moLoaderEngineManager.ParseFileFormatFieldDesc(oDataRow2("FileFormatFieldDesc"))
              Dim nFieldLength As Integer = SHBS.General.ZeroIfDBNull(oDataRow2("FieldLength"))
              Dim sParameterValue As String = SHBS.General.EmptyStringIfDBNull(oDataRow2("ParameterValue"))

              oPackage.GlobalVariables.Item(sFieldName).Value = Left(sParameterValue, nFieldLength)
            Next
        End Select

        Dim oDictionaryEntry As System.Collections.DictionaryEntry
        For Each oDictionaryEntry In oPackage.Tasks.Items
          Dim oDataPumpTask As SHBS.SQLDTS.Tasks.DataPumpTask = CType(oDictionaryEntry.Value, SHBS.SQLDTS.Tasks.DataPumpTask)

          Select Case oMetaDatabase.MetaDatabaseTypeInd.Value
            Case XDSAdministrationBL.Constants.MetaDatabase.Types.System
              If oDataPumpTask.TaskName = Constants.Task_Staging & oMetaDatabase.MetaDatabaseCode.Value Then
                oDataPumpTask.SetSourceObjectName(sFileName)
                oDataPumpTask.SetDestinationObjectName(SQLManager.StagingTable.TableName(oMetaDatabase, moLoaderEngineManager.FileFormat, moLoaderEngineManager.FileFormatDeploymentID, Loader.LoaderID.Value))
              End If
            Case XDSAdministrationBL.Constants.MetaDatabase.Types.Repository
              If oDataPumpTask.TaskName = Constants.Task_Repository & oMetaDatabase.MetaDatabaseCode.Value Then
                oDataPumpTask.SetSourceObjectName(sFileName)
                oDataPumpTask.SetDestinationObjectName(SQLManager.RepositoryTable.TableName(moLoaderEngineManager.FileFormat, moLoaderEngineManager.FileFormatDeploymentID))
              End If
          End Select
        Next
      Next

      oPackage.ExecutePackage()
    End Sub

    Public Sub ExecuteDTSExportPackage(ByVal Loader As Loader, ByVal MetaDatabase As MetaDatabase)
      Dim oLoaderProcess As LoaderProcess = LoaderProcess.GetDataValidation(Loader, moLoaderEngineManager.ApplicationSettings)
      Dim oLoaderProcessMetaDatabase As LoaderProcessMetaDatabase = oLoaderProcess.LoaderProcessMetaDatabase_OwnMany.NewInstance()
      oLoaderProcessMetaDatabase.MetaDatabase.Instance = MetaDatabase
      oLoaderProcessMetaDatabase.Load()
      Dim sDestinationFilePath As String = oLoaderProcessMetaDatabase.GetInvalidLoaderFileName()
      Dim oPackage As Package = New Package

      Select Case MetaDatabase.MetaDatabaseTypeInd.Value
        Case XDSAdministrationBL.constants.MetaDatabase.Types.System
          Dim oQuery As StringBuilder = New StringBuilder

          oQuery.Append("select * ")
          oQuery.Append("  from " & SQLManager.StagingTable.TableName(MetaDatabase, moLoaderEngineManager.FileFormat, moLoaderEngineManager.FileFormatDeploymentID, Loader.LoaderID.Value))
          oQuery.Append(" where IsValidRecordYN = 0")
          oQuery.Append(" order by RecordID")

          oPackage.LoadPackageFromSQLServer(moLoaderEngineManager.ServerName, moLoaderEngineManager.Username, moLoaderEngineManager.Password, DTSPackageStagingExport.PackageName(MetaDatabase, moLoaderEngineManager.FileFormat, moLoaderEngineManager.FileFormatDeploymentID))

          oPackage.Connections.Item(Constants.File).SetDataSource(sDestinationFilePath)
          Dim oDataPumpTask As SHBS.SQLDTS.Tasks.DataPumpTask = CType(oPackage.Tasks.Item(Constants.Task_Staging & MetaDatabase.MetaDatabaseCode.Value), SHBS.SQLDTS.Tasks.DataPumpTask)
          oDataPumpTask.SetSourceSQLStatement(oQuery.ToString())
          oDataPumpTask.SetDestinationObjectName(sDestinationFilePath)
      End Select

      oPackage.ExecutePackage()
    End Sub
#End Region
  End Class
End Namespace