Imports System
Imports System.IO
Imports System.Text
Imports sembleWare.Runtime
Imports SHBS.SQLDTS
Imports XDSAdministrationBL

Namespace DTSManager
  Public Class Constants
    Public Const Field_RecordID As String = "RecordID"
    Public Const Field_LoaderID As String = "LoaderID"
    Public Const Field_SubscriberID As String = "SubscriberID"
    Public Const Field_IsValidRecordYN As String = "IsValidRecordYN"
    Public Const Field_RequiredValidationDataProcedureDetails As String = "RequiredValidationDataProcedureDetails"
    Public Const Field_NonRequiredValidationDataProcedureDetails As String = "NonRequiredValidationDataProcedureDetails"
    Public Const Field_IsPossibleNameConflictYN As String = "IsPossibleNameConflictYN"
    Public Const Field_IsPossibleDuplicateRecordYN As String = "IsPossibleDuplicateRecordYN"
    Public Const Field_RecordChecksumMetaGroup As String = "RecordChecksumMetaGroup"
    Public Const Field_TotalLengthMetaGroup As String = "TotalLengthMetaGroup"
    Public Const Package As String = "Package"
    Public Const Step_Repository As String = "Step_File_To_SQL_Repository_"
    Public Const Step_Staging As String = "Step_File_To_SQL_Staging_"
    Public Const Task_Repository As String = "Task_File_To_SQL_Repository_"
    Public Const Task_Staging As String = "Task_File_To_SQL_Staging_"
    Public Const SQL_Repository As String = "SQL_Repository_"
    Public Const SQL_Staging As String = "SQL_Staging_"
    Public Const Transformation As String = "Transformation_"
    Public Const File As String = "File"
    Public Const Transform As String = "Transform Data Task"
    Public Const Import As String = "_Import"
    Public Const Export As String = "_Export"
    Public Const Col As String = "Col"
    Public Const IndexFillFactor As Integer = 70
  End Class
End Namespace