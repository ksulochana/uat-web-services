Imports sembleWare.Runtime
Imports System
Public Class ConsumerAccountHistory 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly ConsumerAccountHistoryID As New IdentityElement("ConsumerAccountHistoryID", Me, True, Nothing, Nothing)
  Public ReadOnly AccountNo As New StringElement("AccountNo", Me, False, True, True, True, False, 25, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SubAccountNo As New StringElement("SubAccountNo", Me, False, True, True, True, False, 4, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AccountTypeInd As New IndicatorElement("AccountTypeInd", Me, False, False, True, True, True, False, "Account Type", Nothing, ConsumerAccountHistory.AccountTypeIndOptions, Nothing)
  Public ReadOnly AccountOpenedDate As New DateTimeElement("AccountOpenedDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly DeferredPaymentDate As New DateTimeElement("DeferredPaymentDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly LastPaymentDate As New DateTimeElement("LastPaymentDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly OpeningBalanceAmt As New DecimalElement("OpeningBalanceAmt", Me, False, True, True, True, False, "Opening Balance Amount", Nothing, "c", Nothing, Nothing, Nothing)
  Public ReadOnly OpeningBalanceTypeInd As New IndicatorElement("OpeningBalanceTypeInd", Me, False, False, True, True, True, False, "Opening Balance Type", Nothing, ConsumerAccountHistory.OpeningBalanceTypeIndOptions, Nothing)
  Public ReadOnly CurrentBalanceAmt As New DecimalElement("CurrentBalanceAmt", Me, False, True, True, True, False, "Current Balance Amount", Nothing, "c", Nothing, Nothing, Nothing)
  Public ReadOnly CurrentBalanceTypeInd As New IndicatorElement("CurrentBalanceTypeInd", Me, False, False, True, True, True, False, "Current Balance Type", Nothing, ConsumerAccountHistory.CurrentBalanceTypeIndOptions, Nothing)
  Public ReadOnly BalanceOverdueAmt As New DecimalElement("BalanceOverdueAmt", Me, False, True, True, True, False, "Balance Overdue Amount", Nothing, "c", Nothing, Nothing, Nothing)
  Public ReadOnly BalanceOverdueTypeInd As New IndicatorElement("BalanceOverdueTypeInd", Me, False, False, True, True, True, False, "Balance Overdue Type", Nothing, ConsumerAccountHistory.BalanceOverdueTypeIndOptions, Nothing)
  Public ReadOnly MonthlyInstalmentAmt As New DecimalElement("MonthlyInstalmentAmt", Me, False, True, True, True, False, "Monthly Instalment Amount", Nothing, "c", Nothing, Nothing, Nothing)
  Public ReadOnly LoadTypeInd As New IndicatorElement("LoadTypeInd", Me, False, False, True, True, True, False, "Load Type", Nothing, ConsumerAccountHistory.LoadTypeIndOptions, Nothing)
  Public ReadOnly MonthsInArrears As New IntegerElement("MonthsInArrears", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly InstallmentTerms As New StringElement("InstallmentTerms", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly WriteOffDate As New DateTimeElement("WriteOffDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly OldSupplierBranchCode As New StringElement("OldSupplierBranchCode", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly OldAccountNo As New StringElement("OldAccountNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly OldSubAccountNo As New StringElement("OldSubAccountNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly OldSupplierReferenceNo As New StringElement("OldSupplierReferenceNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly IncomeAmt As New DecimalElement("IncomeAmt", Me, False, True, True, True, False, "Income Amount", Nothing, "c", Nothing, Nothing, Nothing)
  Public ReadOnly AccountSoldThirdPartyInd As New IndicatorElement("AccountSoldThirdPartyInd", Me, False, False, True, True, True, False, "Account Sold To Third Party", Nothing, ConsumerAccountHistory.AccountSoldThirdPartyIndOptions, Nothing)
  Public ReadOnly NoOfParticipantsInJointLoan As New IntegerElement("NoOfParticipantsInJointLoan", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ThirdPartyName As New StringElement("ThirdPartyName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RecordStatusInd As New IndicatorElement("RecordStatusInd", Me, False, False, False, True, True, True, "Record Status", Nothing, ConsumerAccountHistory.RecordStatusIndOptions, "A")
  Public ReadOnly DeletedReason As New StringElement("DeletedReason", Me, False, True, True, True, False, 500, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LastUpdatedDate As New DateTimeElement("LastUpdatedDate", Me, False, False, True, True, True, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly IsVerifiedYN As New BooleanElement("IsVerifiedYN", Me, False, True, True, True, True, "Is Verified?", Nothing, False, "Yes;No")
  Public ReadOnly VerifiedDate As New DateTimeElement("VerifiedDate", Me, False, True, True, True, False, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly Comment As New TextElement("Comment", Me, True, True, False, False, Nothing, Nothing, Nothing)
  Public ReadOnly DataStatus As Relationship = New DataStatusRelationship("DataStatus", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Loader As Relationship = New LoaderRelationship("Loader", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Period As Relationship = New PeriodRelationship("Period", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly AccountType As Relationship = New AccountTypeRelationship("AccountType", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly LoanEndUse As Relationship = New LoanEndUseRelationship("LoanEndUse", "Loan End-Use", RelationshipType.Include, False, True, True, Me)
  Public ReadOnly OwnershipType As Relationship = New OwnershipTypeRelationship("OwnershipType", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly PaymentType As Relationship = New PaymentTypeRelationship("PaymentType", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly RepaymentFrequency As Relationship = New RepaymentFrequencyRelationship("RepaymentFrequency", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly IncomeFrequency As Relationship = New IncomeFrequencyRelationship("IncomeFrequency", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly Consumer As Relationship = New ConsumerRelationship("Consumer", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _DataStatusCode As New StringElement("DataStatusCode", Me, False)
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, False)
  Public ReadOnly _LoaderID As New IntegerElement("LoaderID", Me, False)
  Public ReadOnly _PeriodNum As New IntegerElement("PeriodNum", Me, False)
  Public ReadOnly _AccountTypeCode As New StringElement("AccountTypeCode", Me, False)
  Public ReadOnly _LoanEndUseCode As New StringElement("LoanEndUseCode", Me, False)
  Public ReadOnly _OwnershipTypeCode As New StringElement("OwnershipTypeCode", Me, False)
  Public ReadOnly _PaymentTypeCode As New StringElement("PaymentTypeCode", Me, False)
  Public ReadOnly _RepaymentFrequencyCode As New StringElement("RepaymentFrequencyCode", Me, False)
  Public ReadOnly _IncomeFrequencyCode As New StringElement("IncomeFrequencyCode", Me, False)
  Public ReadOnly _ConsumerID As New IntegerElement("ConsumerID", Me, True)
  Public ReadOnly _DataStatus As DataStatusRelationship = DataStatus
  Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
  Public ReadOnly _Loader As LoaderRelationship = Loader
  Public ReadOnly _Period As PeriodRelationship = Period
  Public ReadOnly _AccountType As AccountTypeRelationship = AccountType
  Public ReadOnly _LoanEndUse As LoanEndUseRelationship = LoanEndUse
  Public ReadOnly _OwnershipType As OwnershipTypeRelationship = OwnershipType
  Public ReadOnly _PaymentType As PaymentTypeRelationship = PaymentType
  Public ReadOnly _RepaymentFrequency As RepaymentFrequencyRelationship = RepaymentFrequency
  Public ReadOnly _IncomeFrequency As IncomeFrequencyRelationship = IncomeFrequency
  Public ReadOnly _Consumer As ConsumerRelationship = Consumer
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moAccountTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property AccountTypeIndOptions() As IndicatorOptions
    Get
      If moAccountTypeIndOptions Is Nothing Then
        moAccountTypeIndOptions = New IndicatorOptions
        moAccountTypeIndOptions.Add("I", "Installment")
        moAccountTypeIndOptions.Add("R", "Revolving")
        moAccountTypeIndOptions.Add("O", "Open")
        moAccountTypeIndOptions.Add("C", "Credit Card")
        moAccountTypeIndOptions.Add("P", "Personal")
        moAccountTypeIndOptions.Add("H", "Home Loans")
        moAccountTypeIndOptions.Add("S", "Short Term Insurance")
        moAccountTypeIndOptions.Add("L", "Long Term Insurance")
      End If
      Return moAccountTypeIndOptions
    End Get
  End Property
  Private Shared moOpeningBalanceTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property OpeningBalanceTypeIndOptions() As IndicatorOptions
    Get
      If moOpeningBalanceTypeIndOptions Is Nothing Then
        moOpeningBalanceTypeIndOptions = New IndicatorOptions
        moOpeningBalanceTypeIndOptions.Add("H", "Highest Balance")
        moOpeningBalanceTypeIndOptions.Add("C", "Credit Limit")
        moOpeningBalanceTypeIndOptions.Add("O", "Opening Balance")
      End If
      Return moOpeningBalanceTypeIndOptions
    End Get
  End Property
  Private Shared moCurrentBalanceTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property CurrentBalanceTypeIndOptions() As IndicatorOptions
    Get
      If moCurrentBalanceTypeIndOptions Is Nothing Then
        moCurrentBalanceTypeIndOptions = New IndicatorOptions
        moCurrentBalanceTypeIndOptions.Add("D", "Debit")
        moCurrentBalanceTypeIndOptions.Add("C", "Credit")
      End If
      Return moCurrentBalanceTypeIndOptions
    End Get
  End Property
  Private Shared moBalanceOverdueTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property BalanceOverdueTypeIndOptions() As IndicatorOptions
    Get
      If moBalanceOverdueTypeIndOptions Is Nothing Then
        moBalanceOverdueTypeIndOptions = New IndicatorOptions
        moBalanceOverdueTypeIndOptions.Add("D", "Debit")
        moBalanceOverdueTypeIndOptions.Add("C", "Credit")
      End If
      Return moBalanceOverdueTypeIndOptions
    End Get
  End Property
  Private Shared moLoadTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property LoadTypeIndOptions() As IndicatorOptions
    Get
      If moLoadTypeIndOptions Is Nothing Then
        moLoadTypeIndOptions = New IndicatorOptions
        moLoadTypeIndOptions.Add("P", "PP")
        moLoadTypeIndOptions.Add("A", "Adverse")
        moLoadTypeIndOptions.Add("B", "Both")
      End If
      Return moLoadTypeIndOptions
    End Get
  End Property
  Private Shared moAccountSoldThirdPartyIndOptions As IndicatorOptions
  Public Shared ReadOnly Property AccountSoldThirdPartyIndOptions() As IndicatorOptions
    Get
      If moAccountSoldThirdPartyIndOptions Is Nothing Then
        moAccountSoldThirdPartyIndOptions = New IndicatorOptions
        moAccountSoldThirdPartyIndOptions.Add("01", "Yes")
        moAccountSoldThirdPartyIndOptions.Add("02", "No")
      End If
      Return moAccountSoldThirdPartyIndOptions
    End Get
  End Property
  Private Shared moRecordStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property RecordStatusIndOptions() As IndicatorOptions
    Get
      If moRecordStatusIndOptions Is Nothing Then
        moRecordStatusIndOptions = New IndicatorOptions
        moRecordStatusIndOptions.Add("A", "Active")
        moRecordStatusIndOptions.Add("D", "Deleted")
      End If
      Return moRecordStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("ConsumerAccountHistory", ApplicationSettings)
    _DataStatus._DataStatusCode.LocalElement = _DataStatusCode
    _Subscriber._SubscriberID.LocalElement = _SubscriberID
    _Loader._LoaderID.LocalElement = _LoaderID
    _Period._PeriodNum.LocalElement = _PeriodNum
    _AccountType._AccountTypeCode.LocalElement = _AccountTypeCode
    _LoanEndUse._LoanEndUseCode.LocalElement = _LoanEndUseCode
    _OwnershipType._OwnershipTypeCode.LocalElement = _OwnershipTypeCode
    _PaymentType._PaymentTypeCode.LocalElement = _PaymentTypeCode
    _RepaymentFrequency._RepaymentFrequencyCode.LocalElement = _RepaymentFrequencyCode
    _IncomeFrequency._IncomeFrequencyCode.LocalElement = _IncomeFrequencyCode
    _Consumer._ConsumerID.LocalElement = _ConsumerID
    'sembleWare: Constructor End - Do Not Modify
    Me.DeleteType = CustomDBPart.enumDeleteType.ChangeRecordStatus
  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Save()
    With ApplicationSettings
      .BeginTransaction()
      Try
        Dim bIsNewYN As Boolean = Me.IsNew
        BRBeforeSave(bIsNewYN)
        MyBase.Save()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
      .CommitTransaction()
    End With
  End Sub
#End Region

#Region " Business Rules"
  Public Sub BRBeforeSave(ByVal IsNewYN As Boolean)
    Me.LastUpdatedDate.Value = System.DateTime.Now
  End Sub
#End Region

#Region "User Code"
  Public Function getCommentID() As String
    Dim oString As String
    Dim iCommentID As Integer
    Dim oDataTable As DataTable

    oString = "select Max(CommentID) from Comments where ConsumerID=" + Me._ConsumerID.Value.ToString() + _
          " and ConsumerAccountHistoryID=" + Me.ConsumerAccountHistoryID.Value.ToString()

    oDataTable = ApplicationSettings.ResultQuery(oString)

    If oDataTable.Rows.Count > 0 Then
      Dim oDataRow As DataRow = oDataTable.Rows(0)

      If Not oDataRow(0) Is System.DBNull.Value Then
        Return oDataRow(0)
      End If

    End If

    Return -1
  End Function


#End Region
End Class 'sembleWare: Part

Public Class ConsumerAccountHistoryRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _ConsumerAccountHistoryID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _ConsumerID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New ConsumerAccountHistory(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _ConsumerAccountHistoryID.ForeignElement = CType(Instance, ConsumerAccountHistory).ConsumerAccountHistoryID
    _ConsumerID.ForeignElement = CType(Instance, ConsumerAccountHistory)._ConsumerID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0ConsumerAccountHistory")
    oList.Columns.Add(New ListColumn("ConsumerAccountHistoryID", "[A0ConsumerAccountHistory].[ConsumerAccountHistoryID]", "ConsumerAccountHistoryID", DataType.Long, Nothing, True, 0, False, 100, "Consumer Account History ID"))
    oList.Columns.Add(New ListColumn("ConsumerID", "[A0ConsumerAccountHistory].[ConsumerID]", "ConsumerID", DataType.Integer, Nothing, True, 0, False, 100, "Consumer ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0ConsumerAccountHistory].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, False, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("LoaderID", "[A0ConsumerAccountHistory].[LoaderID]", "LoaderID", DataType.Integer, Nothing, False, 0, False, 100, "Loader ID"))
    oList.Columns.Add(New ListColumn("PeriodNum", "[A0ConsumerAccountHistory].[PeriodNum]", "PeriodNum", DataType.Integer, Nothing, False, -1, True, 100, "Period"))
    oList.Columns.Add(New ListColumn("AccountNo", "[A0ConsumerAccountHistory].[AccountNo]", "AccountNo", DataType.String, Nothing, False, 2, True, 100, "Account No"))
    oList.Columns.Add(New ListColumn("SubAccountNo", "[A0ConsumerAccountHistory].[SubAccountNo]", "SubAccountNo", DataType.String, Nothing, False, 3, True, 100, "Sub Account No"))
    oList.Columns.Add(New ListColumn("DataStatusCode", "[A0ConsumerAccountHistory].[DataStatusCode]", "DataStatusCode", DataType.String, Nothing, False, 0, True, 100, "Data Status Code"))
    oList.Columns.Add(New ListColumn("OpeningBalanceAmt", "[A0ConsumerAccountHistory].[OpeningBalanceAmt]", "OpeningBalanceAmt", DataType.Decimal, "c", False, 0, True, 100, "Opening Balance Amount"))
    oList.Columns.Add(New ListColumn("CurrentBalanceAmt", "[A0ConsumerAccountHistory].[CurrentBalanceAmt]", "CurrentBalanceAmt", DataType.Decimal, "c", False, 0, True, 100, "Current Balance Amount"))
    oList.Columns.Add(New ListColumn("BalanceOverdueAmt", "[A0ConsumerAccountHistory].[BalanceOverdueAmt]", "BalanceOverdueAmt", DataType.Decimal, "c", False, 0, True, 100, "Balance Overdue Amount"))
    oList.Columns.Add(New ListColumn("SubscriberID1", "[A2Subscriber].[SubscriberID]", "SubscriberID", DataType.Long, Nothing, False, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberName", "[A2Subscriber].[SubscriberName]", "SubscriberName", DataType.String, Nothing, False, 0, True, 200, "Subscriber Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "(([ConsumerAccountHistory]  [A0ConsumerAccountHistory]" + _
           "    join [Consumer]  [A1Consumer] on [A0ConsumerAccountHistory].[ConsumerID] = [A1Consumer].[ConsumerID])" + _
           "    left join [Subscriber]  [A2Subscriber] on [A0ConsumerAccountHistory].[SubscriberID] = [A2Subscriber].[SubscriberID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    oList.AndFilters.Add(New ListFilter("RecordStatusInd", "[A1Consumer].[RecordStatusInd]", Constants.RecordStatus.Active, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
    oList.AndFilters.Add(New ListFilter("RecordStatusInd1", "[A0ConsumerAccountHistory].[RecordStatusInd]", Constants.RecordStatus.Active, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
