Imports sembleWare.Runtime
Imports System
Public Class FileFormatDeployment 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly FileFormatDeploymentID As New IdentityElement("FileFormatDeploymentID", Me, True, Nothing, Nothing)
  Public ReadOnly DeploymentDate As New DateTimeElement("DeploymentDate", Me, False, False, True, True, True, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly DeploymentStatusInd As New IndicatorElement("DeploymentStatusInd", Me, False, False, False, True, True, True, "Deployment Status", Nothing, FileFormatDeployment.DeploymentStatusIndOptions, "P")
  Public ReadOnly RemovedDate As New DateTimeElement("RemovedDate", Me, False, False, True, True, False, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly ErrorMessage As New TextElement("ErrorMessage", Me, False, True, True, False, Nothing, Nothing, Nothing)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly FileFormatDeploymentMetaLoaderProcedure_OwnMany As Relationship = New FileFormatDeploymentMetaLoaderProcedureRelationship("FileFormatDeploymentMetaLoaderProcedure", Nothing, "FileFormatDeployment", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  Public ReadOnly _FileFormatDeploymentMetaLoaderProcedure_OwnMany As FileFormatDeploymentMetaLoaderProcedureRelationship = FileFormatDeploymentMetaLoaderProcedure_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moDeploymentStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property DeploymentStatusIndOptions() As IndicatorOptions
    Get
      If moDeploymentStatusIndOptions Is Nothing Then
        moDeploymentStatusIndOptions = New IndicatorOptions
        moDeploymentStatusIndOptions.Add("P", "Processing")
        moDeploymentStatusIndOptions.Add("C", "Completed")
        moDeploymentStatusIndOptions.Add("F", "Failed")
        moDeploymentStatusIndOptions.Add("R", "Removed")
      End If
      Return moDeploymentStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("FileFormatDeployment", ApplicationSettings)
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"

#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreate()
  End Sub

  Public Overrides Sub Save()
    With ApplicationSettings
      .BeginTransaction()
      Try
        Dim bIsNewYN As Boolean = Me.IsNew
        MyBase.Save()
        BRAfterSave(bIsNewYN)
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
      .CommitTransaction()
    End With
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreate()
    Me.DeploymentDate.Value = System.DateTime.Now
    Me.DeploymentStatusInd.Value = Constants.FileFormatDeployment.Statuses.Processing
  End Sub

  Private Sub BRAfterSave(ByVal IsNewYN As Boolean)
    If IsNewYN Then
      FileFormatDeploymentMetaLoaderProcedure.InsertFileFormatDeploymentMetaLoaderProcedures(Me, ApplicationSettings)
    End If
  End Sub
#End Region

#Region " Methods"
  Public Sub DeployAll(ByVal ConnectString As String)
    Dim sServerName As String = ""
    Dim sDatabaseName As String = ""
    Dim sUsername As String = ""
    Dim sPassword As String = ""
    Dim sItems() As String = ConnectString.Split(";")
    Dim sItem As String

    For Each sItem In sItems
      Dim sSubItems() As String = sItem.Split("=")
      Select Case sSubItems(0).ToLower()
        Case "data source"
          sServerName = sSubItems(1)
        Case "initial catalog"
          sDatabaseName = sSubItems(1)
        Case "user id"
          sUsername = sSubItems(1)
        Case "pwd"
          sPassword = sSubItems(1)
      End Select
    Next

    Me.Save()
    Dim oLoaderEngineManager As LoaderEngineManager.Manager = New LoaderEngineManager.Manager(Me, sServerName, sDatabaseName, sUsername, sPassword, ApplicationSettings)
    Try
      oLoaderEngineManager.DeployFileFormats()
      Me.DeploymentStatusInd.Value = Constants.FileFormatDeployment.Statuses.Completed
      Me.Save()
    Catch oException As Exception
      Me.DeploymentStatusInd.Value = Constants.FileFormatDeployment.Statuses.Failed
      Me.ErrorMessage.Value = oException.ToString()
      Me.Save()
      Throw oException
    End Try
    oLoaderEngineManager.RemovePreviousDeployments()
  End Sub
#End Region

#Region " Shared Procedures"
  Public Shared Function GetMaxCompletedFileFormatDeployment(ByVal ApplicationSettings As ApplicationSettings) As FileFormatDeployment
    Dim oXDSAdministrationRoot As XDSAdministrationRoot = New XDSAdministrationRoot(ApplicationSettings)
    Dim oList As List = oXDSAdministrationRoot._FileFormatDeployment_OwnMany.GridList
    Dim oDataRow As DataRow

    oList.AndFilters.Add(New ListFilter("DeploymentStatusInd", "DeploymentStatusInd", Constants.FileFormatDeployment.Statuses.Completed, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
    For Each oDataRow In oList.DataTable.Rows
      Dim oFileFormatDeployment As FileFormatDeployment = New FileFormatDeploymentRelationship(ApplicationSettings).NewInstance()

      oFileFormatDeployment.FileFormatDeploymentID.Load(oDataRow("FileFormatDeploymentID"))
      oFileFormatDeployment.Load()

      Return oFileFormatDeployment
    Next

    Return Nothing
  End Function
#End Region
End Class 'sembleWare: Part

Public Class FileFormatDeploymentRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _FileFormatDeploymentID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New FileFormatDeployment(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _FileFormatDeploymentID.ForeignElement = CType(Instance, FileFormatDeployment).FileFormatDeploymentID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0FileFormatDeployment")
    oList.Columns.Add(New ListColumn("FileFormatDeploymentID", "[A0FileFormatDeployment].[FileFormatDeploymentID]", "FileFormatDeploymentID", DataType.Long, Nothing, True, -1, True, 200, "File Format Deployment ID"))
    oList.Columns.Add(New ListColumn("DeploymentDate", "[A0FileFormatDeployment].[DeploymentDate]", "DeploymentDate", DataType.DateTime, "g", False, 0, True, 200, "Deployment Date"))
    oList.Columns.Add(New ListColumn("DeploymentStatusInd", "[A0FileFormatDeployment].[DeploymentStatusInd]", "DeploymentStatusInd", DataType.String, Nothing, False, 0, True, 200, "Deployment Status"))
    oList.Columns.Add(New ListColumn("RemovedDate", "[A0FileFormatDeployment].[RemovedDate]", "RemovedDate", DataType.DateTime, "g", False, 0, True, 200, "Removed Date"))
    oList.SelectStatement = "select"
    oList.FromClause = "[FileFormatDeployment]  [A0FileFormatDeployment]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
