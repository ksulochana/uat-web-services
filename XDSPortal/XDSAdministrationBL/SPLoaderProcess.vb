Imports sembleWare.Runtime
Imports System
Public Class SPLoaderProcess 'sembleWare: Part
  Inherits CustomMemoryPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly LoaderID_SP As New LongElement("LoaderID_SP", Me, False, True, True, True, False, "Loader ID", Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LoaderIDFrom_SP As New LongElement("LoaderIDFrom_SP", Me, False, True, True, True, False, "Loader ID From", Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LoaderIDTo_SP As New LongElement("LoaderIDTo_SP", Me, False, True, True, True, False, "Loader ID To", Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LoaderQueuedDateFrom_SP As New DateTimeElement("LoaderQueuedDateFrom_SP", Me, False, True, True, True, False, "Loader Queued Date From", Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly LoaderQueuedDateTo_SP As New DateTimeElement("LoaderQueuedDateTo_SP", Me, False, True, True, True, False, "Loader Queued Date To", Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly LoaderProcessStatus_RequireSubmitYN As New BooleanElement("LoaderProcessStatus_RequireSubmitYN", Me, False, True, True, True, False, "Require Submit?", Nothing, True, "Yes;No")
  Public ReadOnly LoaderProcessStatus_QueuedYN As New BooleanElement("LoaderProcessStatus_QueuedYN", Me, False, True, True, True, False, "Queued?", Nothing, True, "Yes;No")
  Public ReadOnly LoaderProcessStatus_ProcessingYN As New BooleanElement("LoaderProcessStatus_ProcessingYN", Me, False, True, True, True, False, "Processing?", Nothing, True, "Yes;No")
  Public ReadOnly LoaderProcessStatus_CompletedYN As New BooleanElement("LoaderProcessStatus_CompletedYN", Me, False, True, True, True, False, "Completed?", Nothing, True, "Yes;No")
  Public ReadOnly LoaderProcessStatus_FailedYN As New BooleanElement("LoaderProcessStatus_FailedYN", Me, False, True, True, True, False, "Failed?", Nothing, True, "Yes;No")
  Public ReadOnly EnhancedLoaderProcessStatusYN_SP As New BooleanElement("EnhancedLoaderProcessStatusYN_SP", Me, False, True, True, True, False, "Enhanced?", Nothing, False, "Yes;No")
  Public ReadOnly LoaderProcessType_DataLoadYN As New BooleanElement("LoaderProcessType_DataLoadYN", Me, False, True, True, True, False, "Data Load?", Nothing, True, "Yes;No")
  Public ReadOnly LoaderProcessType_DataValidYN As New BooleanElement("LoaderProcessType_DataValidYN", Me, False, True, True, True, False, "Data Validiation?", Nothing, True, "Yes;No")
  Public ReadOnly LoaderProcessType_DataMatchYN As New BooleanElement("LoaderProcessType_DataMatchYN", Me, False, True, True, True, False, "Data Matching?", Nothing, True, "Yes;No")
  Public ReadOnly LoaderProcessType_DataUpdateYN As New BooleanElement("LoaderProcessType_DataUpdateYN", Me, False, True, True, True, False, "Data Update?", Nothing, True, "Yes;No")
  Public ReadOnly LoaderProcessType_DataUndoYN As New BooleanElement("LoaderProcessType_DataUndoYN", Me, False, True, True, True, False, "Data Undo?", Nothing, True, "Yes;No")
  Public ReadOnly EnhancedLoaderProcessTypeYN_SP As New BooleanElement("EnhancedLoaderProcessTypeYN_SP", Me, False, True, True, True, False, "Enhanced?", Nothing, False, "Yes;No")
  Public ReadOnly LoaderThread_SP As Relationship = New LoaderThreadRelationship("LoaderThread_SP", "Loader Thread", RelationshipType.Include, False, True, True, Me)
  Public ReadOnly LoaderProcessType_SP As Relationship = New LoaderProcessTypeRelationship("LoaderProcessType_SP", "Loader Process Type", RelationshipType.Include, False, True, True, Me)
  Public ReadOnly LoaderProcessStatus_SP As Relationship = New LoaderProcessStatusRelationship("LoaderProcessStatus_SP", "Loader Process Status", RelationshipType.Include, False, True, True, Me)
  Public ReadOnly Subscriber_SP As Relationship = New SubscriberRelationship("Subscriber_SP", "Subscriber", RelationshipType.Include, False, True, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _LoaderThreadCode As New StringElement("LoaderThreadCode", Me, False)
  Public ReadOnly _LoaderProcessTypeCode As New StringElement("LoaderProcessTypeCode", Me, False)
  Public ReadOnly _LoaderProcessStatusCode As New StringElement("LoaderProcessStatusCode", Me, False)
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, False)
  Public ReadOnly _LoaderThread_SP As LoaderThreadRelationship = LoaderThread_SP
  Public ReadOnly _LoaderProcessType_SP As LoaderProcessTypeRelationship = LoaderProcessType_SP
  Public ReadOnly _LoaderProcessStatus_SP As LoaderProcessStatusRelationship = LoaderProcessStatus_SP
  Public ReadOnly _Subscriber_SP As SubscriberRelationship = Subscriber_SP
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SPLoaderProcess", ApplicationSettings)
    _LoaderThread_SP._LoaderThreadCode.LocalElement = _LoaderThreadCode
    _LoaderProcessType_SP._LoaderProcessTypeCode.LocalElement = _LoaderProcessTypeCode
    _LoaderProcessStatus_SP._LoaderProcessStatusCode.LocalElement = _LoaderProcessStatusCode
    _Subscriber_SP._SubscriberID.LocalElement = _SubscriberID
    AddHandler EnhancedLoaderProcessStatusYN_SP.Changed, AddressOf EnhancedLoaderProcessStatusYN_SP_Changed
    AddHandler EnhancedLoaderProcessTypeYN_SP.Changed, AddressOf EnhancedLoaderProcessTypeYN_SP_Changed
    'sembleWare: Constructor End - Do Not Modify
    AddHandler LoaderProcessType_SP.LimitRelatedList, AddressOf LoaderProcessType_SP_LimitRelatedList
    AddHandler LoaderThread_SP.LimitRelatedList, AddressOf LoaderThread_SP_LimitRelatedList
  End Sub

#End Region 'sembleWare: Constructor

#Region " LimitRelatedList Events"
  Private Sub LoaderProcessType_SP_LimitRelatedList(ByVal List As List)
    List.AndFilters.Add(New ListFilter("LoaderProcessTypeCode", "[A0LoaderProcessType].[LoaderProcessTypeCode]", Constants.LoaderProcess.Types.DataUndo, DataType.String, FilterType.NotEquals, FilterBlankBehavior.IgnoreFilter, True))
  End Sub

  Private Sub LoaderThread_SP_LimitRelatedList(ByVal List As List)
    List.AndFilters.Add(New ListFilter("LoaderThreadStatusInd", "LoaderThreadStatusInd", Constants.LoaderThread.Statuses.Active, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
    List.AndFilters.Add(New ListFilter("LoaderProcessTypeCode", "[A0LoaderThread].[LoaderProcessTypeCode]", Constants.LoaderProcess.Types.DataUndo, DataType.String, FilterType.NotEquals, FilterBlankBehavior.IgnoreFilter, True))
  End Sub
#End Region

#Region " Change Events"
  Private Sub EnhancedLoaderProcessStatusYN_SP_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BREnhancedLoaderProcessStatusYN_SP(Element, True, True)
  End Sub

  Private Sub EnhancedLoaderProcessTypeYN_SP_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BREnhancedLoaderProcessTypeYN_SP(Element, True, True)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreate()
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreate()
    Me.EnhancedLoaderProcessStatusYN_SP.Value = True
    Me.LoaderProcessStatus_CompletedYN.Value = False
    BREnhancedLoaderProcessTypeYN_SP(Me.EnhancedLoaderProcessTypeYN_SP, True, False)
    BREnhancedLoaderProcessStatusYN_SP(Me.EnhancedLoaderProcessStatusYN_SP, True, False)
  End Sub

  Private Sub BREnhancedLoaderProcessTypeYN_SP(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If SetEnableDisableYN Then
      Me.LoaderProcessType_SP.Enabled = Not Element.ValueAsObject
      Me.LoaderProcessType_DataLoadYN.Enabled = Element.ValueAsObject
      Me.LoaderProcessType_DataValidYN.Enabled = Element.ValueAsObject
      Me.LoaderProcessType_DataMatchYN.Enabled = Element.ValueAsObject
      Me.LoaderProcessType_DataUpdateYN.Enabled = Element.ValueAsObject
      Me.LoaderProcessType_DataUndoYN.Enabled = Element.ValueAsObject
    End If

    If PerformRulesYN Then
      Me.LoaderProcessType_SP.Instance = Nothing
    End If
  End Sub

  Private Sub BREnhancedLoaderProcessStatusYN_SP(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If SetEnableDisableYN Then
      Me.LoaderProcessStatus_SP.Enabled = Not Element.ValueAsObject
      Me.LoaderProcessStatus_RequireSubmitYN.Enabled = Element.ValueAsObject
      Me.LoaderProcessStatus_QueuedYN.Enabled = Element.ValueAsObject
      Me.LoaderProcessStatus_ProcessingYN.Enabled = Element.ValueAsObject
      Me.LoaderProcessStatus_CompletedYN.Enabled = Element.ValueAsObject
      Me.LoaderProcessStatus_FailedYN.Enabled = Element.ValueAsObject
    End If

    If PerformRulesYN Then
      Me.LoaderProcessStatus_SP.Instance = Nothing
    End If
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class SPLoaderProcessRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SPLoaderProcess(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
