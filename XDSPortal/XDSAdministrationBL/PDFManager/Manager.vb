Imports System
Imports System.IO
Imports Aspose.Pdf.Kit

Namespace PDFManager
  Public Class Manager
    Public Function Concatenate(ByVal Streams() As System.IO.Stream) As System.IO.Stream
      Dim oPdfFileEditor As PdfFileEditor = New PdfFileEditor
      Dim oMemoryStream As MemoryStream = New MemoryStream
      Dim oLicense As Aspose.Pdf.Kit.License = New Aspose.Pdf.Kit.License
      oLicense.SetLicense("Aspose.Pdf.Kit.lic")

      If oPdfFileEditor.Concatenate(Streams, oMemoryStream) Then
        Return oMemoryStream
      End If

      Return Nothing
    End Function
  End Class
End Namespace