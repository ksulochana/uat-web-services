Imports sembleWare.Runtime
Imports System
Public Class MetaDatabase 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly MetaDatabaseCode As New StringElement("MetaDatabaseCode", Me, True, True, True, True, True, 10, "Database Code", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly MetaDatabaseDesc As New StringElement("MetaDatabaseDesc", Me, False, True, True, True, True, 100, "Database Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly MetaDatabaseTypeInd As New IndicatorElement("MetaDatabaseTypeInd", Me, False, False, False, True, True, True, "Database Type", Nothing, MetaDatabase.MetaDatabaseTypeIndOptions, Nothing)
  Public ReadOnly ServerName As New StringElement("ServerName", Me, False, True, True, True, True, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly DatabaseName As New StringElement("DatabaseName", Me, False, True, True, True, True, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Username As New StringElement("Username", Me, False, True, True, True, True, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Password As New StringElement("Password", Me, False, False, True, True, True, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly MetaDatabaseLabelDescription As New StringElement("MetaDatabaseLabelDescription", Me, False, True, True, False, False, 250, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResetPasswordYN As New BooleanElement("ResetPasswordYN", Me, False, True, True, False, False, "Reset Password?", Nothing, False)
  Public ReadOnly NewPassword As New StringElement("NewPassword", Me, False, True, True, False, False, 30, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ConfirmPassword As New StringElement("ConfirmPassword", Me, False, True, True, False, False, 30, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly MetaTable_OwnMany As Relationship = New MetaTableRelationship("MetaTable", Nothing, "MetaDatabase", Me)
  Public ReadOnly MetaField_OwnMany As Relationship = New MetaFieldRelationship("MetaField", Nothing, "MetaDatabase", Me)
  Public ReadOnly MetaGroup_OwnMany As Relationship = New MetaGroupRelationship("MetaGroup", Nothing, "MetaDatabase", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  Public ReadOnly _MetaTable_OwnMany As MetaTableRelationship = MetaTable_OwnMany
  Public ReadOnly _MetaField_OwnMany As MetaFieldRelationship = MetaField_OwnMany
  Public ReadOnly _MetaGroup_OwnMany As MetaGroupRelationship = MetaGroup_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moMetaDatabaseTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property MetaDatabaseTypeIndOptions() As IndicatorOptions
    Get
      If moMetaDatabaseTypeIndOptions Is Nothing Then
        moMetaDatabaseTypeIndOptions = New IndicatorOptions
        moMetaDatabaseTypeIndOptions.Add("S", "System")
        moMetaDatabaseTypeIndOptions.Add("R", "Repository")
      End If
      Return moMetaDatabaseTypeIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("MetaDatabase", ApplicationSettings)
    AddHandler ResetPasswordYN.Changed, AddressOf ResetPasswordYN_Changed
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub ResetPasswordYN_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BRResetPasswordYN(Element, True, True)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"

#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreate()
  End Sub

  Public Overrides Sub Load()
    MyBase.Load()
    BRLoad()
  End Sub

  Public Overrides Sub Save()
    With ApplicationSettings
      Try
        .BeginTransaction()
        Dim bIsNewYN As Boolean = Me.IsNew

        BRBeforeSave(bIsNewYN)
        MyBase.Save()
        BRAfterSave(bIsNewYN)
        .CommitTransaction()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
    End With
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreate()
    Me.ResetPasswordYN.Value = True
    BRResetPasswordYN(Me.ResetPasswordYN, True, False)
    Me.ResetPasswordYN.Enabled = False
    Me.MetaDatabaseTypeInd.Enabled = True
    BRMetaDatabaseLabelDescription()
  End Sub

  Private Sub BRLoad()
    BRMetaDatabaseLabelDescription()
  End Sub

  Private Sub BRBeforeSave(ByVal IsNewYN As Boolean)
    If Me.ResetPasswordYN.Value Then
      Dim oEncryption As sembleWare.Security.Encryption = New sembleWare.Security.Encryption(Constants.PasswordHashString)
      Dim sNewPassword As String = oEncryption.Encrypt(Me.NewPassword.Value)
      Dim sConfirmPassword As String = oEncryption.Encrypt(Me.ConfirmPassword.Value)

      If Not sNewPassword = sConfirmPassword Then
        Throw New BusinessRuleException("The Password does match the Confirm Password. Please re-enter the password!")
      End If
      Me.Password.Value = sNewPassword
    End If
  End Sub

  Private Sub BRAfterSave(ByVal IsNewYN As Boolean)
    If IsNewYN Then
      Select Case Me.MetaDatabaseTypeInd.Value
        Case XDSAdministrationBL.Constants.MetaDatabase.Types.Repository
          Dim oMetaTable As MetaTable = Me.MetaTable_OwnMany.NewInstance()
          oMetaTable.MetaTableName.Value = "SystemAssigned"
          oMetaTable.MetaTableDesc.Value = "System Assigned"
          oMetaTable.Save()

          Dim oMetaGroup As MetaGroup = Me.MetaGroup_OwnMany.NewInstance()
          oMetaGroup.MetaGroupCode.Value = "SFD"
          oMetaGroup.MetaGroupDesc.Value = "Subscriber File Repository"
          oMetaGroup.MetaTable.Instance = oMetaTable
          oMetaGroup.Save()
      End Select
      InsertDefaultLoaderProcedures(Me, ApplicationSettings)
    End If
    Me.ResetPasswordYN.Enabled = True
    Me.ResetPasswordYN.Value = False
    BRResetPasswordYN(Me.ResetPasswordYN, True, True)
    BRMetaDatabaseLabelDescription()
  End Sub

  Private Sub BRResetPasswordYN(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If SetEnableDisableYN Then
      Me.NewPassword.Enabled = Element.ValueAsObject
      Me.NewPassword.Mandatory = Element.ValueAsObject
      Me.ConfirmPassword.Enabled = Element.ValueAsObject
      Me.ConfirmPassword.Mandatory = Element.ValueAsObject
    End If

    If PerformRulesYN Then
      If Not Element.ValueAsObject Then
        Me.NewPassword.SetToNullValue()
        Me.ConfirmPassword.SetToNullValue()
      End If
    End If
  End Sub

  Private Sub BRMetaDatabaseLabelDescription()
    If Me.IsNew Then
      Me.MetaDatabaseLabelDescription.Value = "Database: " & "(New)"
    Else
      Dim sDesc As String = Trim(Me.MetaDatabaseDesc.Value)
      Me.MetaDatabaseLabelDescription.Value = "Database: " & Me.MetaDatabaseCode.Value & " (" & sDesc & ")"
    End If
  End Sub
#End Region

#Region " Shared Procedures"
  Public Shared Sub InsertDefaultLoaderProcedures(ByVal MetaDatabase As MetaDatabase, ByVal ApplicationSettings As ApplicationSettings)
    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand

    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spMetaLoaderProcedure_I_LoaderProcessType"

    oCommand.Parameters.Add("@MetaDatabaseCode", SqlDbType.VarChar, 10)

    oCommand.Parameters("@MetaDatabaseCode").Value = MetaDatabase.MetaDatabaseCode.Value

    ApplicationSettings.ActionQuery(oCommand)
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class MetaDatabaseRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _MetaDatabaseCode As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New MetaDatabase(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _MetaDatabaseCode.ForeignElement = CType(Instance, MetaDatabase).MetaDatabaseCode
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0MetaDatabase")
    oList.Columns.Add(New ListColumn("MetaDatabaseCode", "[A0MetaDatabase].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, True, 1, True, 100, "Database Code"))
    oList.Columns.Add(New ListColumn("MetaDatabaseDesc", "[A0MetaDatabase].[MetaDatabaseDesc]", "MetaDatabaseDesc", DataType.String, Nothing, False, 0, True, 200, "Database Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "[MetaDatabase]  [A0MetaDatabase]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function





  Public Function DropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0MetaDatabase")
    oList.Columns.Add(New ListColumn("MetaDatabaseCode", "[A0MetaDatabase].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, True, 1, True, 100, "Database Code"))
    oList.Columns.Add(New ListColumn("MetaDatabaseDesc", "[A0MetaDatabase].[MetaDatabaseDesc]", "MetaDatabaseDesc", DataType.String, Nothing, False, 0, True, 200, "Database Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "[MetaDatabase]  [A0MetaDatabase]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0MetaDatabase")
    oList.Columns.Add(New ListColumn("MetaDatabaseCode", "[A0MetaDatabase].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, True, 1, True, 100, "Database Code"))
    oList.Columns.Add(New ListColumn("MetaDatabaseDesc", "[A0MetaDatabase].[MetaDatabaseDesc]", "MetaDatabaseDesc", DataType.String, Nothing, False, 0, True, 200, "Database Description"))
    oList.Columns.Add(New ListColumn("MetaDatabaseTypeInd", "[A0MetaDatabase].[MetaDatabaseTypeInd]", "MetaDatabaseTypeInd", DataType.String, Nothing, False, 0, True, 100, "Database Type"))
    oList.SelectStatement = "select"
    oList.FromClause = "[MetaDatabase]  [A0MetaDatabase]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function




#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
