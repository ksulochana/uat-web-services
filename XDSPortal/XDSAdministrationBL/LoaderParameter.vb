Imports sembleWare.Runtime
Imports System
Public Class LoaderParameter 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly ParameterValue As New StringElement("ParameterValue", Me, False, True, True, True, False, 250, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Loader As Relationship = New LoaderRelationship("Loader", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly FileFormatField As Relationship = New FileFormatFieldRelationship("FileFormatField", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _LoaderID As New IntegerElement("LoaderID", Me, True)
  Public ReadOnly _FileFormatFieldID As New IntegerElement("FileFormatFieldID", Me, True)
  Public ReadOnly _FileFormatCode As New StringElement("FileFormatCode", Me, True)
  Public ReadOnly _Loader As LoaderRelationship = Loader
  Public ReadOnly _FileFormatField As FileFormatFieldRelationship = FileFormatField
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("LoaderParameter", ApplicationSettings)
    _Loader._LoaderID.LocalElement = _LoaderID
    _FileFormatField._FileFormatFieldID.LocalElement = _FileFormatFieldID
    _FileFormatField._FileFormatCode.LocalElement = _FileFormatCode
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Load()
    MyBase.Load()
    BRLoad()
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRLoad()
    Me.ParameterValue.Mandatory = True
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class LoaderParameterRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _LoaderID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _FileFormatFieldID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _FileFormatCode As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New LoaderParameter(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _LoaderID.ForeignElement = CType(Instance, LoaderParameter)._LoaderID
    _FileFormatFieldID.ForeignElement = CType(Instance, LoaderParameter)._FileFormatFieldID
    _FileFormatCode.ForeignElement = CType(Instance, LoaderParameter)._FileFormatCode
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0LoaderParameter")
    oList.Columns.Add(New ListColumn("FileFormatFieldDesc", "[A1FileFormatField].[FileFormatFieldDesc]", "FileFormatFieldDesc", DataType.String, Nothing, False, 0, True, 200, "Field Description "))
    oList.Columns.Add(New ListColumn("FieldLength", "[A1FileFormatField].[FieldLength]", "FieldLength", DataType.Integer, Nothing, False, 0, False, 100, "Fixed Field Start"))
    oList.Columns.Add(New ListColumn("ParameterValue", "[A0LoaderParameter].[ParameterValue]", "ParameterValue", DataType.String, Nothing, False, 0, True, 200, "Parameter Value"))
    oList.Columns.Add(New ListColumn("LoaderID", "[A0LoaderParameter].[LoaderID]", "LoaderID", DataType.Integer, Nothing, True, 0, False, 100, "Loader ID"))
    oList.Columns.Add(New ListColumn("FileFormatFieldID", "[A0LoaderParameter].[FileFormatFieldID]", "FileFormatFieldID", DataType.Integer, Nothing, True, 0, False, 100, "File Format Field ID"))
    oList.Columns.Add(New ListColumn("FileFormatCode", "[A0LoaderParameter].[FileFormatCode]", "FileFormatCode", DataType.String, Nothing, True, 0, False, 100, "File Format Code"))
    oList.SelectStatement = "select"
    oList.FromClause = "([LoaderParameter]  [A0LoaderParameter]" + _
           "    join [FileFormatField]  [A1FileFormatField] on [A0LoaderParameter].[FileFormatFieldID] = [A1FileFormatField].[FileFormatFieldID] and [A0LoaderParameter].[FileFormatCode] = [A1FileFormatField].[FileFormatCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
