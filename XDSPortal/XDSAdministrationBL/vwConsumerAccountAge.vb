Imports sembleWare.Runtime
Imports System
Public Class vwConsumerAccountAge 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly NoOfYears As New IntegerElement("NoOfYears", Me, False, False, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly NoOfMonths As New IntegerElement("NoOfMonths", Me, False, False, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RecordStatusInd As New IndicatorElement("RecordStatusInd", Me, False, False, False, True, True, False, Nothing, Nothing, vwConsumerAccountAge.RecordStatusIndOptions, Nothing)
  Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly SubscriberGroup As Relationship = New SubscriberGroupRelationship("SubscriberGroup", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly Consumer As Relationship = New ConsumerRelationship("Consumer", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly ConsumerAccount As Relationship = New ConsumerAccountRelationship("ConsumerAccount", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, False)
  Public ReadOnly _SubscriberGroupCode As New StringElement("SubscriberGroupCode", Me, False)
  Public ReadOnly _ConsumerID As New IntegerElement("ConsumerID", Me, True)
  Public ReadOnly _ConsumerAccountID As New IntegerElement("ConsumerAccountID", Me, True)
  Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
  Public ReadOnly _SubscriberGroup As SubscriberGroupRelationship = SubscriberGroup
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  Public ReadOnly _Consumer As ConsumerRelationship = Consumer
  Public ReadOnly _ConsumerAccount As ConsumerAccountRelationship = ConsumerAccount
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moRecordStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property RecordStatusIndOptions() As IndicatorOptions
    Get
      If moRecordStatusIndOptions Is Nothing Then
        moRecordStatusIndOptions = New IndicatorOptions
        moRecordStatusIndOptions.Add("A", "Active")
        moRecordStatusIndOptions.Add("D", "Deleted")
      End If
      Return moRecordStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("vwConsumerAccountAge", ApplicationSettings)
    _Subscriber._SubscriberID.LocalElement = _SubscriberID
    _SubscriberGroup._SubscriberGroupCode.LocalElement = _SubscriberGroupCode
    _Consumer._ConsumerID.LocalElement = _ConsumerID
    _ConsumerAccount._ConsumerAccountID.LocalElement = _ConsumerAccountID
    _ConsumerAccount._ConsumerID.LocalElement = _ConsumerID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class vwConsumerAccountAgeRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _ConsumerID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _ConsumerAccountID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New vwConsumerAccountAge(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _ConsumerID.ForeignElement = CType(Instance, vwConsumerAccountAge)._ConsumerID
    _ConsumerAccountID.ForeignElement = CType(Instance, vwConsumerAccountAge)._ConsumerAccountID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0vwConsumerAccountAge")
    oList.Columns.Add(New ListColumn("ConsumerID", "[A0vwConsumerAccountAge].[ConsumerID]", "ConsumerID", DataType.Integer, Nothing, True, 0, False, 100, "Consumer ID"))
    oList.Columns.Add(New ListColumn("ConsumerAccountID", "[A0vwConsumerAccountAge].[ConsumerAccountID]", "ConsumerAccountID", DataType.Integer, Nothing, True, 0, False, 100, "Consumer Account ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A1Subscriber].[SubscriberID]", "SubscriberID", DataType.Long, Nothing, False, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberName", "[A1Subscriber].[SubscriberName]", "SubscriberName", DataType.String, Nothing, False, 1, True, 200, "Subscriber Name"))
    oList.Columns.Add(New ListColumn("NoOfYears", "[A0vwConsumerAccountAge].[NoOfYears]", "NoOfYears", DataType.Integer, Nothing, False, 0, True, 100, "No Of Years"))
    oList.Columns.Add(New ListColumn("NoOfMonths", "[A0vwConsumerAccountAge].[NoOfMonths]", "NoOfMonths", DataType.Integer, Nothing, False, 0, True, 100, "No Of Months"))
    oList.SelectStatement = "select"
    oList.FromClause = "([vwConsumerAccountAge]  [A0vwConsumerAccountAge]" + _
           "    left join [Subscriber]  [A1Subscriber] on [A0vwConsumerAccountAge].[SubscriberID] = [A1Subscriber].[SubscriberID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
