Imports sembleWare.Runtime
Imports System
Public Class NotificationQueueParameter 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly ParameterName As New StringElement("ParameterName", Me, True, True, True, True, True, 250, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ParameterValue As New StringElement("ParameterValue", Me, False, True, True, True, True, 500, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly NotificationQueue As Relationship = New NotificationQueueRelationship("NotificationQueue", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _NotificationQueueID As New IntegerElement("NotificationQueueID", Me, True)
  Public ReadOnly _NotificationQueue As NotificationQueueRelationship = NotificationQueue
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("NotificationQueueParameter", ApplicationSettings)
    _NotificationQueue._NotificationQueueID.LocalElement = _NotificationQueueID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class NotificationQueueParameterRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _ParameterName As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _NotificationQueueID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New NotificationQueueParameter(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _ParameterName.ForeignElement = CType(Instance, NotificationQueueParameter).ParameterName
    _NotificationQueueID.ForeignElement = CType(Instance, NotificationQueueParameter)._NotificationQueueID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0NotificationQueueParameter")
    oList.Columns.Add(New ListColumn("ParameterName", "[A0NotificationQueueParameter].[ParameterName]", "ParameterName", DataType.String, Nothing, True, 0, True, 100, "Parameter Name"))
    oList.Columns.Add(New ListColumn("NotificationQueueID", "[A0NotificationQueueParameter].[NotificationQueueID]", "NotificationQueueID", DataType.Integer, Nothing, True, 0, True, 100, "Notification Queue ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "[NotificationQueueParameter]  [A0NotificationQueueParameter]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
