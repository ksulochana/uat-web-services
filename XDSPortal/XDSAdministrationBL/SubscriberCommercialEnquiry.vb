Imports sembleWare.Runtime
Imports System
Public Class SubscriberCommercialEnquiry 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly SubscriberCommercialEnquiryID As New IdentityElement("SubscriberCommercialEnquiryID", Me, True, Nothing, Nothing)
  Public ReadOnly EnquiryDate As New DateTimeElement("EnquiryDate", Me, False, False, True, True, True, "Enquiry Date", Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly EnquiryStatusInd As New IndicatorElement("EnquiryStatusInd", Me, False, False, False, True, True, True, "Enquiry Status", Nothing, SubscriberCommercialEnquiry.EnquiryStatusIndOptions, "Q")
  Public ReadOnly EnquiryResultInd As New IndicatorElement("EnquiryResultInd", Me, False, False, False, True, True, True, "Enquiry Result", Nothing, SubscriberCommercialEnquiry.EnquiryResultIndOptions, "P")
  Public ReadOnly ProductPointValue As New DecimalElement("ProductPointValue", Me, False, False, True, True, True, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RegistrationNoPart1 As New StringElement("RegistrationNoPart1", Me, False, True, True, False, False, 4, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RegistrationNoPart2 As New StringElement("RegistrationNoPart2", Me, False, True, True, False, False, 6, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RegistrationNoPart3 As New StringElement("RegistrationNoPart3", Me, False, True, True, False, False, 2, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RegistrationNo As New StringElement("RegistrationNo", Me, False, True, True, True, False, 14, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CommercialName As New StringElement("CommercialName", Me, False, True, True, True, False, 150, "Business Name", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly TaxNo As New StringElement("TaxNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResultRegistrationNo As New StringElement("ResultRegistrationNo", Me, False, True, True, True, False, 14, "Registration No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResultCommercialName As New StringElement("ResultCommercialName", Me, False, True, True, True, False, 150, "Business Name", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResultTaxNo As New StringElement("ResultTaxNo", Me, False, False, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Product As Relationship = New ProductRelationship("Product", Nothing, RelationshipType.Include, True, True, True, Me)
  Public ReadOnly SIC As Relationship = New SICRelationship("SIC", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly ResultSIC As Relationship = New SICRelationship("ResultSIC", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly SystemUser As Relationship = New SystemUserRelationship("SystemUser", Nothing, RelationshipType.Include, True, False, True, Me)
  Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly SubscriberCommercialEnquiryResult_OwnMany As Relationship = New SubscriberCommercialEnquiryResultRelationship("SubscriberCommercialEnquiryResult", Nothing, "SubscriberCommercialEnquiry", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _ProductID As New IntegerElement("ProductID", Me, False)
  Public ReadOnly _SICCode As New StringElement("SICCode", Me, False)
  Public ReadOnly _ResultSICCode As New StringElement("ResultSICCode", Me, False)
  Public ReadOnly _SystemUserID As New IntegerElement("SystemUserID", Me, False)
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _Product As ProductRelationship = Product
  Public ReadOnly _SIC As SICRelationship = SIC
  Public ReadOnly _ResultSIC As SICRelationship = ResultSIC
  Public ReadOnly _SystemUser As SystemUserRelationship = SystemUser
  Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
  Public ReadOnly _SubscriberCommercialEnquiryResult_OwnMany As SubscriberCommercialEnquiryResultRelationship = SubscriberCommercialEnquiryResult_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moEnquiryStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property EnquiryStatusIndOptions() As IndicatorOptions
    Get
      If moEnquiryStatusIndOptions Is Nothing Then
        moEnquiryStatusIndOptions = New IndicatorOptions
        moEnquiryStatusIndOptions.Add("Q", "Queued")
        moEnquiryStatusIndOptions.Add("P", "Processing")
        moEnquiryStatusIndOptions.Add("C", "Completed")
      End If
      Return moEnquiryStatusIndOptions
    End Get
  End Property
  Private Shared moEnquiryResultIndOptions As IndicatorOptions
  Public Shared ReadOnly Property EnquiryResultIndOptions() As IndicatorOptions
    Get
      If moEnquiryResultIndOptions Is Nothing Then
        moEnquiryResultIndOptions = New IndicatorOptions
        moEnquiryResultIndOptions.Add("P", "Pending")
        moEnquiryResultIndOptions.Add("N", "No Record Found")
        moEnquiryResultIndOptions.Add("F", "Record Found")
        moEnquiryResultIndOptions.Add("M", "Multiple Records Found")
        moEnquiryResultIndOptions.Add("S", "Record Selected")
      End If
      Return moEnquiryResultIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberCommercialEnquiry", ApplicationSettings)
    _Product._ProductID.LocalElement = _ProductID
    _SIC._SICCode.LocalElement = _SICCode
    _ResultSIC._SICCode.LocalElement = _ResultSICCode
    _SystemUser._SystemUserID.LocalElement = _SystemUserID
    _Subscriber._SubscriberID.LocalElement = _SubscriberID
    AddHandler Product.Changed, AddressOf Product_Changed
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub Product_Changed(ByVal Relationship As Relationship)
    BRProduct(Relationship, True, True)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"

  Public Sub Validate()
    Dim bAllowYN As Boolean = False
    Dim oException As Exception

    Select Case Me._ProductID.Value
      Case Constants.Product.Records.EnquiryCommercialBusiness
        oException = New MissingValuesException("Registration No", MyBase.Caption, "")
        If Not Me.RegistrationNoPart1.IsEmpty And Not Me.RegistrationNoPart2.IsEmpty And Not Me.RegistrationNoPart3.IsEmpty Then
          bAllowYN = True
        End If

        If Not Me.CommercialName.IsEmpty Then
          bAllowYN = True
        End If

        If Not Me.TaxNo.IsEmpty Then
          bAllowYN = True
        End If
      Case Constants.Product.Records.EnquiryCommercial
        oException = New MissingValuesException("Registration No", MyBase.Caption, "")
        If Not Me.RegistrationNoPart1.IsEmpty And Not Me.RegistrationNoPart2.IsEmpty And Not Me.RegistrationNoPart3.IsEmpty Then
          bAllowYN = True
        End If
      Case Constants.Product.Records.EnquiryCommercialBusinessName
        oException = New MissingValuesException("Commercial Name", MyBase.Caption, "")
        If Not Me.CommercialName.IsEmpty Then
          bAllowYN = True
        End If
      Case Constants.Product.Records.EnquiryCommercialEasy
        oException = New MissingValuesException("Commercial Name", MyBase.Caption, "")
        If Not Me.CommercialName.IsEmpty Then
          If Me.CommercialName.Value.Length() < 3 Then
            oException = New BusinessRuleException("Commercial Name must be at least 3 chars in length.")
          Else
            bAllowYN = True
          End If
        End If
      Case Else
        bAllowYN = True
    End Select

    If Not bAllowYN Then
      Throw New BusinessRuleException(oException.Message)
    End If
  End Sub

  Public Sub QueueMatch()
    Validate()
    Me.EnquiryStatusInd.Value = Constants.SubscriberConsumerEnquiry.Statuses.Queued
    Me.Save()
  End Sub

  Public Sub Match()
    Validate()
    Me.EnquiryStatusInd.Value = Constants.SubscriberConsumerEnquiry.Statuses.Processing
    Me.Save()

    Dim oDataRow As DataRow
    If Not Me.RegistrationNoPart1.IsEmpty AndAlso Not Me.RegistrationNoPart2.IsEmpty AndAlso Not Me.RegistrationNoPart3.IsEmpty Then
      Me.RegistrationNo.Value = Me.RegistrationNoPart1.Value & "/" & Me.RegistrationNoPart2.Value & "/" & Me.RegistrationNoPart3.Value
    End If
    Dim nEnquiryMatchingEngineID As Integer = EnquiryMatchingEngine.CommercialMatch(Me.RegistrationNo, Me.CommercialName, Me.TaxNo, Me._SICCode, _
                                                                                    Me._ProductID.Value, ApplicationSettings)
    Dim oString As Text.StringBuilder = New Text.StringBuilder

    oString.Append("insert into SubscriberCommercialEnquiryResult(SubscriberID, SubscriberCommercialEnquiryID, CommercialID, DetailsViewedYN, CommercialSelectedYN, RegistrationNo, CommercialName, TaxNo, SICCode)" & vbNewLine)
    oString.Append("select " & Me._SubscriberID.Value & ", " & Me.SubscriberCommercialEnquiryID.Value & ", ecr.CommercialID, 0, 0, c.RegistrationNo, c.CommercialName, c.TaxNo, c.SICCode" & vbNewLine)
    oString.Append("  from EnquiryMatchingEngineCommercialResult ecr inner join EnquiryMatchingEngineCommercial mec")
    oString.Append("                                                    on ecr.EnquiryMatchingEngineID = mec.EnquiryMatchingEngineID" & vbNewLine)
    oString.Append("                                                   and ecr.EnquiryMatchingEngineCommercialID = mec.EnquiryMatchingEngineCommercialID" & vbNewLine)
    oString.Append("                                                 inner join Commercial c" & vbNewLine)
    oString.Append("                                                    on ecr.CommercialID = c.CommercialID" & vbNewLine)
    oString.Append(" where mec.EnquiryMatchingEngineID = " & ApplicationSettings.QueryBuilder.ToSQL(nEnquiryMatchingEngineID) & vbNewLine)
    oString.Append("   and ecr.IgnoreRecordYN = 0")
    ApplicationSettings.ActionQuery(oString.ToString())

    oString = New Text.StringBuilder
    oString.Append("select count(ecr.CommercialID)" & vbNewLine)
    oString.Append("  from EnquiryMatchingEngineCommercialResult ecr inner join EnquiryMatchingEngineCommercial mec" & vbNewLine)
    oString.Append("                                                    on ecr.EnquiryMatchingEngineID = mec.EnquiryMatchingEngineID" & vbNewLine)
    oString.Append("                                                   and ecr.EnquiryMatchingEngineCommercialID = mec.EnquiryMatchingEngineCommercialID" & vbNewLine)
    oString.Append(" where mec.EnquiryMatchingEngineID = " & ApplicationSettings.QueryBuilder.ToSQL(nEnquiryMatchingEngineID) & vbNewLine)
    oString.Append("   and ecr.IgnoreRecordYN = 0")
    For Each oDataRow In ApplicationSettings.ResultQuery(oString.ToString()).Rows
      Select Case System.Convert.ToInt32(oDataRow(0))
        Case 0
          Me.EnquiryResultInd.Value = Constants.SubscriberConsumerEnquiry.Results.NoRecordFound
        Case 1
          Me.EnquiryResultInd.Value = Constants.SubscriberConsumerEnquiry.Results.RecordFound
        Case Else
          Me.EnquiryResultInd.Value = Constants.SubscriberConsumerEnquiry.Results.MultipleRecordsFound
      End Select
    Next

    Me.EnquiryStatusInd.Value = Constants.SubscriberConsumerEnquiry.Statuses.Completed
    Me.Save()
  End Sub
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Load()
    MyBase.Load()
    BRLoad()
  End Sub

  Public Overrides Sub Save()
    With ApplicationSettings
      Try
        Dim bIsNewYN As Boolean = Me.IsNew
        .BeginTransaction()
        BRBeforeSave(bIsNewYN)
        MyBase.Save()
        .CommitTransaction()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
    End With
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRLoad()
    Me.DisableElementsAndRelationship()
  End Sub

  Private Sub BRBeforeSave(ByVal IsNewYN As Boolean)
    If IsNewYN Then
      Me.EnquiryDate.Value = System.DateTime.Now
      ' If SystemUser is not empty, then the XDSAdministration Services has populated this value.
      If SystemUser.IsEmpty Then
        Me.SystemUser.Instance = XDSAdministrationBL.SystemUser.GetLoggedInSystemUser(ApplicationSettings)
      End If

      Me.ResultRegistrationNo.ValueAsObject = Me.RegistrationNo.ValueAsObject
      Me.ResultCommercialName.ValueAsObject = Me.CommercialName.ValueAsObject
      Me.ResultTaxNo.ValueAsObject = Me.TaxNo.ValueAsObject
      Me.ResultSIC.Instance = Me.SIC.Instance
    End If
  End Sub

  Private Sub BRProduct(ByVal Relationship As Relationship, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    Dim oProduct As Product = Nothing

    If Not Relationship.IsEmpty Then
      oProduct = Relationship.Instance
    End If
    Utility.BRProductEnquiryCommercial(oProduct, Me.RegistrationNoPart1, Me.RegistrationNoPart2, Me.RegistrationNoPart3, Me.CommercialName, Me.TaxNo, Me.SIC, _
                                       SetEnableDisableYN, PerformRulesYN)

    If PerformRulesYN Then
      If Not oProduct Is Nothing Then
        Dim oSubscriberProfile As SubscriberProfile = SubscriberProfile.GetSubscriberProfile(Me._SubscriberID.Value, ApplicationSettings)
        Dim oSubscriberProfileProduct As SubscriberProfileProduct = SubscriberProfileProduct.GetSubscriberProfileProduct(Me._SubscriberID.Value, oProduct.ProductID.Value, ApplicationSettings)

        If oSubscriberProfileProduct.OverrideDefaultPointValueYN.Value Then
          Me.ProductPointValue.Value = oSubscriberProfileProduct.OverridePointValue.Value
        Else
          Me.ProductPointValue.Value = oProduct.DefaultPointValue.Value
        End If
      End If
    End If
  End Sub
#End Region

#Region " Public Methods"
  Public Function GetSubscriberCommercialEnquiryResult() As SubscriberCommercialEnquiryResult
    If Me.EnquiryResultInd.Value = Constants.SubscriberConsumerEnquiry.Results.RecordFound Or Me.EnquiryResultInd.Value = Constants.SubscriberConsumerEnquiry.Results.RecordSelected Then
      Dim oList As List = Me._SubscriberCommercialEnquiryResult_OwnMany.GridList
      Dim oDataTable As DataTable = oList.DataTable
      Dim oDataRow As DataRow = oDataTable.Rows(0)
      Dim oSubscriberCommercialEnquiryResult As SubscriberCommercialEnquiryResult = Me.SubscriberCommercialEnquiryResult_OwnMany.NewInstance()

      oSubscriberCommercialEnquiryResult._CommercialID.Load(oDataRow("CommercialID"))
      oSubscriberCommercialEnquiryResult.Load()
      Return oSubscriberCommercialEnquiryResult
    End If
    Return Nothing
  End Function
#End Region
End Class 'sembleWare: Part

Public Class SubscriberCommercialEnquiryRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberCommercialEnquiryID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberCommercialEnquiry(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberCommercialEnquiryID.ForeignElement = CType(Instance, SubscriberCommercialEnquiry).SubscriberCommercialEnquiryID
    _SubscriberID.ForeignElement = CType(Instance, SubscriberCommercialEnquiry)._SubscriberID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function MultipleCommercialGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberCommercialEnquiry")
    oList.Columns.Add(New ListColumn("SubscriberCommercialEnquiryID", "[A0SubscriberCommercialEnquiry].[SubscriberCommercialEnquiryID]", "SubscriberCommercialEnquiryID", DataType.Long, Nothing, True, 0, False, 100, "Subscriber Commercial Enquiry ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberCommercialEnquiry].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("EnquiryDate", "[A0SubscriberCommercialEnquiry].[EnquiryDate]", "EnquiryDate", DataType.DateTime, "g", False, -1, True, 100, "Enquiry Date"))
    oList.Columns.Add(New ListColumn("ResultRegistrationNo", "[A0SubscriberCommercialEnquiry].[ResultRegistrationNo]", "ResultRegistrationNo", DataType.String, Nothing, False, 0, True, 100, "Registration No"))
    oList.Columns.Add(New ListColumn("ResultCommercialName", "[A0SubscriberCommercialEnquiry].[ResultCommercialName]", "ResultCommercialName", DataType.String, Nothing, False, 0, True, 100, "Business Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberCommercialEnquiry]  [A0SubscriberCommercialEnquiry]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
