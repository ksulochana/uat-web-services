Imports sembleWare.Runtime
Imports System
Public Class SubscriberProfileCommercialEnquiryReport 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly OverrideSubscriberAssociationDefaultsYN As New BooleanElement("OverrideSubscriberAssociationDefaultsYN", Me, False, True, True, True, True, "Override Subscriber Association Defaults?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplayCommercialInformationYN As New BooleanElement("OverrideDisplayCommercialInformationYN", Me, False, True, True, True, True, "Display Commercial Information?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplayAuditorInformationYN As New BooleanElement("OverrideDisplayAuditorInformationYN", Me, False, True, True, True, True, "Display Auditor Information?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplayDirectorInformationYN As New BooleanElement("OverrideDisplayDirectorInformationYN", Me, False, True, True, True, True, "Display Director Information?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplayEnquiryInformationYN As New BooleanElement("OverrideDisplayEnquiryInformationYN", Me, False, True, True, True, True, "Display Enquiry Information?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplayJudgementInformationYN As New BooleanElement("OverrideDisplayJudgementInformationYN", Me, False, True, True, True, True, "Display Judgement Information?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplayNameHistoryYN As New BooleanElement("OverrideDisplayNameHistoryYN", Me, False, True, True, True, True, "Display Name History?", Nothing, False, "Yes;No")
  Public ReadOnly OverrideDisplayAddressHistoryYN As New BooleanElement("OverrideDisplayAddressHistoryYN", Me, False, True, True, True, True, "Display Address History?", Nothing, False, "Yes;No")
    Public ReadOnly OverrideDisplayTelephoneHistoryYN As New BooleanElement("OverrideDisplayTelephoneHistoryYN", Me, False, True, True, True, True, "Display Telephone History?", Nothing, False, "Yes;No")
    Public ReadOnly OverrideDisplayPersonalJudgementInformationYN As New BooleanElement("OverrideDisplayPersonalJudgementInformationYN", Me, False, True, True, True, True, "Display Personal Judgement Information?", Nothing, False, "Yes;No")
  Public ReadOnly SubscriberProfile As Relationship = New SubscriberProfileRelationship("SubscriberProfile", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _SubscriberProfile As SubscriberProfileRelationship = SubscriberProfile
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberProfileCommercialEnquiryReport", ApplicationSettings)
    _SubscriberProfile._SubscriberID.LocalElement = _SubscriberID
    AddHandler OverrideSubscriberAssociationDefaultsYN.Changed, AddressOf OverrideSubscriberAssociationDefaultsYN_Changed
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub OverrideSubscriberAssociationDefaultsYN_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BROverrideSubscriberAssociationDefaultsYN(Element, True, True)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreateLoad()
  End Sub

  Public Overrides Sub Load()
    MyBase.Load()
    BRCreateLoad()
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreateLoad()
    BROverrideSubscriberAssociationDefaultsYN(Me.OverrideSubscriberAssociationDefaultsYN, True, False)
  End Sub

  Private Sub BROverrideSubscriberAssociationDefaultsYN(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If SetEnableDisableYN Then
      Me.OverrideDisplayAddressHistoryYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplayAddressHistoryYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplayAuditorInformationYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplayAuditorInformationYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplayCommercialInformationYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplayCommercialInformationYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplayDirectorInformationYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplayDirectorInformationYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplayEnquiryInformationYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplayEnquiryInformationYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplayJudgementInformationYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplayJudgementInformationYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplayNameHistoryYN.Enabled = Element.ValueAsObject
      Me.OverrideDisplayNameHistoryYN.Mandatory = Element.ValueAsObject
      Me.OverrideDisplayTelephoneHistoryYN.Enabled = Element.ValueAsObject
            Me.OverrideDisplayTelephoneHistoryYN.Mandatory = Element.ValueAsObject
            Me.OverrideDisplayPersonalJudgementInformationYN.Enabled = Element.ValueAsObject
            Me.OverrideDisplayPersonalJudgementInformationYN.Mandatory = Element.ValueAsObject
    End If

    If PerformRulesYN Then
      Me.OverrideDisplayAddressHistoryYN.Value = False
      Me.OverrideDisplayAuditorInformationYN.Value = False
      Me.OverrideDisplayCommercialInformationYN.Value = False
      Me.OverrideDisplayDirectorInformationYN.Value = False
      Me.OverrideDisplayEnquiryInformationYN.Value = False
      Me.OverrideDisplayJudgementInformationYN.Value = False
      Me.OverrideDisplayNameHistoryYN.Value = False
            Me.OverrideDisplayTelephoneHistoryYN.Value = False
            Me.OverrideDisplayPersonalJudgementInformationYN.Value = False
    End If
  End Sub
#End Region

#Region " Shared Procedures"
  Public Shared Function GetSubscriberProfileCommercialEnquiryReport(ByVal SubscriberID As Long, ByVal ApplicationSettings As ApplicationSettings) As SubscriberProfileCommercialEnquiryReport
    Dim oSubscriberProfileCommercialEnquiryReport As SubscriberProfileCommercialEnquiryReport = New SubscriberProfileCommercialEnquiryReportRelationship(ApplicationSettings).NewInstance
    Try
      oSubscriberProfileCommercialEnquiryReport._SubscriberID.Load(SubscriberID)
      oSubscriberProfileCommercialEnquiryReport.Load()
    Catch oException As sembleWare.Runtime.RecordNotFoundException
      oSubscriberProfileCommercialEnquiryReport = New SubscriberProfileCommercialEnquiryReportRelationship(ApplicationSettings).NewInstance
      oSubscriberProfileCommercialEnquiryReport._SubscriberID.Load(SubscriberID)
    Catch oException As Exception
      Throw oException
    End Try
    Return oSubscriberProfileCommercialEnquiryReport
  End Function
#End Region
End Class 'sembleWare: Part

Public Class SubscriberProfileCommercialEnquiryReportRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberProfileCommercialEnquiryReport(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberID.ForeignElement = CType(Instance, SubscriberProfileCommercialEnquiryReport)._SubscriberID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
