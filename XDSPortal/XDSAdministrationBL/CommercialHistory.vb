Imports sembleWare.Runtime
Imports System
Public Class CommercialHistory 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly CommercialHistoryID As New IdentityElement("CommercialHistoryID", Me, True, Nothing, Nothing)
  Public ReadOnly ChangeDate As New DateTimeElement("ChangeDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly AuthorisedCapitalAmt As New DecimalElement("AuthorisedCapitalAmt", Me, False, True, True, True, False, Nothing, Nothing, "c", Nothing, Nothing, Nothing)
  Public ReadOnly IssuedCapitalAmt As New DecimalElement("IssuedCapitalAmt", Me, False, True, True, True, False, Nothing, Nothing, "c", Nothing, Nothing, Nothing)
  Public ReadOnly CaseNo As New StringElement("CaseNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CourtName As New StringElement("CourtName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly GovermentPrintersSentYN As New BooleanElement("GovermentPrintersSentYN", Me, False, True, True, True, True, "Sent To Goverment Printers?", Nothing, False, "Yes;No")
  Public ReadOnly GovermentPrintersSentDate As New DateTimeElement("GovermentPrintersSentDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly PreviousRegistrationNo As New StringElement("PreviousRegistrationNo", Me, False, True, True, True, False, 14, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly GovernmentGazettePublishedNo As New StringElement("GovernmentGazettePublishedNo", Me, False, True, True, True, False, 10, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly GovernmentGazetteNoticeNo As New StringElement("GovernmentGazetteNoticeNo", Me, False, True, True, True, False, 10, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly GovernmentGazettePublishedDate As New DateTimeElement("GovernmentGazettePublishedDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly Memo As New TextElement("Memo", Me, True, True, True, False, Nothing, Nothing, Nothing)
  Public ReadOnly RecordStatusInd As New IndicatorElement("RecordStatusInd", Me, False, False, False, True, True, True, "Record Status", Nothing, CommercialHistory.RecordStatusIndOptions, "A")
  Public ReadOnly DeletedReason As New StringElement("DeletedReason", Me, False, True, True, True, False, 500, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LastUpdatedDate As New DateTimeElement("LastUpdatedDate", Me, False, False, True, True, False, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly Loader As Relationship = New LoaderRelationship("Loader", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly CommercialStatus As Relationship = New CommercialStatusRelationship("CommercialStatus", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly PreviousCommercialStatus As Relationship = New CommercialStatusRelationship("PreviousCommercialStatus", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly CommercialChangeType As Relationship = New CommercialChangeTypeRelationship("CommercialChangeType", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly PreviousCommercialType As Relationship = New CommercialTypeRelationship("PreviousCommercialType", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly CommercialType As Relationship = New CommercialTypeRelationship("CommercialType", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly Commercial As Relationship = New CommercialRelationship("Commercial", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _LoaderID As New IntegerElement("LoaderID", Me, False)
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, False)
  Public ReadOnly _CommercialStatusCode As New StringElement("CommercialStatusCode", Me, False)
  Public ReadOnly _PreviousCommercialStatusCode As New StringElement("PreviousCommercialStatusCode", Me, False)
  Public ReadOnly _CommercialChangeTypeCode As New StringElement("CommercialChangeTypeCode", Me, False)
  Public ReadOnly _PreviousCommercialTypeCode As New StringElement("PreviousCommercialTypeCode", Me, False)
  Public ReadOnly _CommercialTypeCode As New StringElement("CommercialTypeCode", Me, False)
  Public ReadOnly _CommercialID As New IntegerElement("CommercialID", Me, True)
  Public ReadOnly _Loader As LoaderRelationship = Loader
  Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
  Public ReadOnly _CommercialStatus As CommercialStatusRelationship = CommercialStatus
  Public ReadOnly _PreviousCommercialStatus As CommercialStatusRelationship = PreviousCommercialStatus
  Public ReadOnly _CommercialChangeType As CommercialChangeTypeRelationship = CommercialChangeType
  Public ReadOnly _PreviousCommercialType As CommercialTypeRelationship = PreviousCommercialType
  Public ReadOnly _CommercialType As CommercialTypeRelationship = CommercialType
  Public ReadOnly _Commercial As CommercialRelationship = Commercial
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moRecordStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property RecordStatusIndOptions() As IndicatorOptions
    Get
      If moRecordStatusIndOptions Is Nothing Then
        moRecordStatusIndOptions = New IndicatorOptions
        moRecordStatusIndOptions.Add("A", "Active")
        moRecordStatusIndOptions.Add("D", "Deleted")
      End If
      Return moRecordStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("CommercialHistory", ApplicationSettings)
    _Loader._LoaderID.LocalElement = _LoaderID
    _Subscriber._SubscriberID.LocalElement = _SubscriberID
    _CommercialStatus._CommercialStatusCode.LocalElement = _CommercialStatusCode
    _PreviousCommercialStatus._CommercialStatusCode.LocalElement = _PreviousCommercialStatusCode
    _CommercialChangeType._CommercialChangeTypeCode.LocalElement = _CommercialChangeTypeCode
    _PreviousCommercialType._CommercialTypeCode.LocalElement = _PreviousCommercialTypeCode
    _CommercialType._CommercialTypeCode.LocalElement = _CommercialTypeCode
    _Commercial._CommercialID.LocalElement = _CommercialID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class CommercialHistoryRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _CommercialHistoryID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _CommercialID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New CommercialHistory(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _CommercialHistoryID.ForeignElement = CType(Instance, CommercialHistory).CommercialHistoryID
    _CommercialID.ForeignElement = CType(Instance, CommercialHistory)._CommercialID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0CommercialHistory")
    oList.Columns.Add(New ListColumn("CommercialID", "[A0CommercialHistory].[CommercialID]", "CommercialID", DataType.Integer, Nothing, True, 0, False, 100, "Commercial ID"))
    oList.Columns.Add(New ListColumn("CommercialHistoryID", "[A0CommercialHistory].[CommercialHistoryID]", "CommercialHistoryID", DataType.Long, Nothing, True, 0, False, 100, "Commercial History ID"))
    oList.Columns.Add(New ListColumn("ChangeDate", "[A0CommercialHistory].[ChangeDate]", "ChangeDate", DataType.DateTime, "d", False, -1, True, 100, "Change Date"))
    oList.Columns.Add(New ListColumn("CaseNo", "[A0CommercialHistory].[CaseNo]", "CaseNo", DataType.String, Nothing, False, 0, True, 100, "Case No"))
    oList.Columns.Add(New ListColumn("CourtName", "[A0CommercialHistory].[CourtName]", "CourtName", DataType.String, Nothing, False, 0, True, 200, "Court Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "[CommercialHistory]  [A0CommercialHistory]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
