Imports sembleWare.Runtime
Imports System
Public Class SubscriberProfileProductAuthenticationPurpose 'sembleWare: Part
    Inherits CustomDBPart

#Region " Elements & Relationships"

    'sembleWare: Elements Start - Do Not Modify
    Public ReadOnly AuthenticationPurposeID As New IdentityElement("AuthenticationPurposeID", Me, True, Nothing, Nothing)
    Public ReadOnly AuthenticationPurposeDesc As New StringElement("AuthenticationPurposeDesc", Me, False, True, True, True, True, 50, "Purpose Description", Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly SubscriberProfile As Relationship = New SubscriberProfileRelationship("SubscriberProfile", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
    'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

    'sembleWare: Foreign Items Start - Do Not Modify
    Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
    Public ReadOnly _SubscriberProfile As SubscriberProfileRelationship = SubscriberProfile
    'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

    'sembleWare: Indicator Options Start - Do Not Modify
    'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

    Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

        'sembleWare: Constructor Start - Do Not Modify
        MyBase.New("SubscriberProfileProductAuthenticationPurpose", ApplicationSettings)
        _SubscriberProfile._SubscriberID.LocalElement = _SubscriberID
        'sembleWare: Constructor End - Do Not Modify

    End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class SubscriberProfileProductAuthenticationPurposeRelationship 'sembleWare: Relationship
    Inherits Relationship

#Region " Relationship Code"

    'sembleWare: Relationship Start - Do Not Modify
    Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)
    Public ReadOnly _AuthenticationPurposeID As ForeignKey = New ForeignKey(Me)

    Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
        MyBase.New(ApplicationSettings)
    End Sub

    Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
        MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
    End Sub

    Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
        MyBase.New(Name, Caption, OwnedByName, Owner)
    End Sub

    Public Overrides Function CreateObject() As sembleWare.Runtime.Part
        Return New SubscriberProfileProductAuthenticationPurpose(ApplicationSettings)
    End Function

    Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
        _SubscriberID.ForeignElement = CType(Instance, SubscriberProfileProductAuthenticationPurpose)._SubscriberID
        _AuthenticationPurposeID.ForeignElement = CType(Instance, SubscriberProfileProductAuthenticationPurpose).AuthenticationPurposeID
    End Sub

    'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

    Public Function GridList() As sembleWare.Runtime.List
        'sembleWare: List Declaration Start - Do Not Modify
        Dim oList As DBList = New DBList(Me, "A0SubscriberProfileProductAuthenticationType")
        oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberProfileProductAuthenticationType].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
        oList.Columns.Add(New ListColumn("AuthenticationPurposeID", "[A0SubscriberProfileProductAuthenticationType].[AuthenticationPurposeID]", "AuthenticationPurposeID", DataType.Long, Nothing, True, 0, False, 100, "Authentication Purpose ID"))
        oList.Columns.Add(New ListColumn("AuthenticationPurposeDesc", "[A0SubscriberProfileProductAuthenticationType].[AuthenticationPurposeDesc]", "AuthenticationPurposeDesc", DataType.String, Nothing, False, 1, True, 200, "Purpose Description"))
        oList.SelectStatement = "select"
        oList.FromClause = "[SubscriberProfileProductAuthenticationPurpose]  [A0SubscriberProfileProductAuthenticationType]"
        oList.WhereClause = ""
        oList.ApplyRelationshipLimits()
        'sembleWare: List Declaration End - Do Not Modify
        Return oList
    End Function


    Public Function DropDownList() As sembleWare.Runtime.List
        'sembleWare: List Declaration Start - Do Not Modify
        Dim oList As DBList = New DBList(Me, "A0SubscriberProfileProductAuthenticationType")
        oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberProfileProductAuthenticationType].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
        oList.Columns.Add(New ListColumn("AuthenticationPurposeID", "[A0SubscriberProfileProductAuthenticationType].[AuthenticationPurposeID]", "AuthenticationPurposeID", DataType.Long, Nothing, True, 0, False, 100, "Authentication Purpose ID"))
        oList.Columns.Add(New ListColumn("AuthenticationPurposeDesc", "[A0SubscriberProfileProductAuthenticationType].[AuthenticationPurposeDesc]", "AuthenticationPurposeDesc", DataType.String, Nothing, False, 1, True, 200, "Purpose Description"))
        oList.SelectStatement = "select"
        oList.FromClause = "[SubscriberProfileProductAuthenticationPurpose]  [A0SubscriberProfileProductAuthenticationType]"
        oList.WhereClause = ""
        oList.ApplyRelationshipLimits()
        'sembleWare: List Declaration End - Do Not Modify
        Return oList
    End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
