Imports sembleWare.Runtime
Imports System
Public Class ConsumerPropertyDeedHistory 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly ConsumerPropertyDeedHistoryID As New IdentityElement("ConsumerPropertyDeedHistoryID", Me, True, Nothing, Nothing)
  Public ReadOnly TitleDeedNo As New StringElement("TitleDeedNo", Me, False, True, True, True, False, 20, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly OldTitleDeedNo As New StringElement("OldTitleDeedNo", Me, False, True, True, True, False, 20, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly TitleDeedFeeAmt As New DecimalElement("TitleDeedFeeAmt", Me, False, True, True, True, False, Nothing, Nothing, "c", Nothing, Nothing, Nothing)
  Public ReadOnly BondNo As New StringElement("BondNo", Me, False, True, True, True, False, 20, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly BondHolder As New StringElement("BondHolder", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly BondAmt As New DecimalElement("BondAmt", Me, False, True, True, True, False, Nothing, Nothing, "c", Nothing, Nothing, Nothing)
  Public ReadOnly BuyerIDNo As New StringElement("BuyerIDNo", Me, False, True, True, True, False, 13, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly BuyerName As New StringElement("BuyerName", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SellerIDNo As New StringElement("SellerIDNo", Me, False, True, True, True, False, 13, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SellerName As New StringElement("SellerName", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Authority As New StringElement("Authority", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Registrar As New StringElement("Registrar", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RegistrarDivision As New StringElement("RegistrarDivision", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AttorneyFileNo As New StringElement("AttorneyFileNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AttorneyFirmNo As New StringElement("AttorneyFirmNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly TransferDate As New DateTimeElement("TransferDate", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SoldDate As New DateTimeElement("SoldDate", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SoldPriceAmt As New DecimalElement("SoldPriceAmt", Me, False, True, True, True, False, Nothing, Nothing, "c", Nothing, Nothing, Nothing)
  Public ReadOnly AvailabilityDate As New DateTimeElement("AvailabilityDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly Size As New StringElement("Size", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly P24ID As New StringElement("P24ID", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ErfNo As New IntegerElement("ErfNo", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ErfPortion As New DecimalElement("ErfPortion", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly StreetNo As New StringElement("StreetNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly StreetName As New StringElement("StreetName", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FarmName As New StringElement("FarmName", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CityName As New StringElement("CityName", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SuburbName As New StringElement("SuburbName", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly TownshipNo As New IntegerElement("TownshipNo", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly TownshipName As New StringElement("TownshipName", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SectionalSchemeErfNo As New StringElement("SectionalSchemeErfNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SectionalSchemeName As New StringElement("SectionalSchemeName", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SectionalSchemeUnitNo As New DecimalElement("SectionalSchemeUnitNo", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Latitude As New DecimalElement("Latitude", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Longitude As New DecimalElement("Longitude", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AdditionalDesc As New StringElement("AdditionalDesc", Me, False, True, True, True, False, 100, "Additional Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ExternalSourceID As New IntegerElement("ExternalSourceID", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ExternalSourceUpdatedDate As New DateTimeElement("ExternalSourceUpdatedDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly RecordStatusInd As New IndicatorElement("RecordStatusInd", Me, False, False, False, True, True, True, "Record Status", Nothing, ConsumerPropertyDeedHistory.RecordStatusIndOptions, "A")
  Public ReadOnly DeletedReason As New StringElement("DeletedReason", Me, False, True, True, True, False, 500, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Loader As Relationship = New LoaderRelationship("Loader", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Period As Relationship = New PeriodRelationship("Period", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly BuyerMaritalStatus As Relationship = New MaritalStatusRelationship("BuyerMaritalStatus", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly SellerMaritalStatus As Relationship = New MaritalStatusRelationship("SellerMaritalStatus", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly CountryRegion As Relationship = New CountryRegionRelationship("CountryRegion", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly PropertyType As Relationship = New PropertyTypeRelationship("PropertyType", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly Consumer As Relationship = New ConsumerRelationship("Consumer", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, False)
  Public ReadOnly _LoaderID As New IntegerElement("LoaderID", Me, False)
  Public ReadOnly _PeriodNum As New IntegerElement("PeriodNum", Me, False)
  Public ReadOnly _BuyerMaritalStatusCode As New StringElement("BuyerMaritalStatusCode", Me, False)
  Public ReadOnly _SellerMaritalStatusCode As New StringElement("SellerMaritalStatusCode", Me, False)
  Public ReadOnly _CountryRegionCode As New StringElement("CountryRegionCode", Me, False)
  Public ReadOnly _CountryCode As New StringElement("CountryCode", Me, False)
  Public ReadOnly _PropertyTypeCode As New StringElement("PropertyTypeCode", Me, False)
  Public ReadOnly _ConsumerID As New IntegerElement("ConsumerID", Me, True)
  Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
  Public ReadOnly _Loader As LoaderRelationship = Loader
  Public ReadOnly _Period As PeriodRelationship = Period
  Public ReadOnly _BuyerMaritalStatus As MaritalStatusRelationship = BuyerMaritalStatus
  Public ReadOnly _SellerMaritalStatus As MaritalStatusRelationship = SellerMaritalStatus
  Public ReadOnly _CountryRegion As CountryRegionRelationship = CountryRegion
  Public ReadOnly _PropertyType As PropertyTypeRelationship = PropertyType
  Public ReadOnly _Consumer As ConsumerRelationship = Consumer
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moRecordStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property RecordStatusIndOptions() As IndicatorOptions
    Get
      If moRecordStatusIndOptions Is Nothing Then
        moRecordStatusIndOptions = New IndicatorOptions
        moRecordStatusIndOptions.Add("A", "Active")
        moRecordStatusIndOptions.Add("D", "Deleted")
      End If
      Return moRecordStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("ConsumerPropertyDeedHistory", ApplicationSettings)
    _Subscriber._SubscriberID.LocalElement = _SubscriberID
    _Loader._LoaderID.LocalElement = _LoaderID
    _Period._PeriodNum.LocalElement = _PeriodNum
    _BuyerMaritalStatus._MaritalStatusCode.LocalElement = _BuyerMaritalStatusCode
    _SellerMaritalStatus._MaritalStatusCode.LocalElement = _SellerMaritalStatusCode
    _CountryRegion._CountryRegionCode.LocalElement = _CountryRegionCode
    _CountryRegion._CountryCode.LocalElement = _CountryCode
    _PropertyType._PropertyTypeCode.LocalElement = _PropertyTypeCode
    _Consumer._ConsumerID.LocalElement = _ConsumerID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class ConsumerPropertyDeedHistoryRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _ConsumerPropertyDeedHistoryID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _ConsumerID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New ConsumerPropertyDeedHistory(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _ConsumerPropertyDeedHistoryID.ForeignElement = CType(Instance, ConsumerPropertyDeedHistory).ConsumerPropertyDeedHistoryID
    _ConsumerID.ForeignElement = CType(Instance, ConsumerPropertyDeedHistory)._ConsumerID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0ConsumerPropertyDeedHistory")
    oList.Columns.Add(New ListColumn("ConsumerPropertyDeedHistoryID", "[A0ConsumerPropertyDeedHistory].[ConsumerPropertyDeedHistoryID]", "ConsumerPropertyDeedHistoryID", DataType.Long, Nothing, True, 0, False, 100, "Consumer Property Deed History ID"))
    oList.Columns.Add(New ListColumn("ConsumerID", "[A0ConsumerPropertyDeedHistory].[ConsumerID]", "ConsumerID", DataType.Integer, Nothing, True, 0, False, 100, "Consumer ID"))
    oList.Columns.Add(New ListColumn("PeriodNum", "[A0ConsumerPropertyDeedHistory].[PeriodNum]", "PeriodNum", DataType.Integer, Nothing, False, -1, True, 100, "Period Num"))
    oList.Columns.Add(New ListColumn("TitleDeedNo", "[A0ConsumerPropertyDeedHistory].[TitleDeedNo]", "TitleDeedNo", DataType.String, Nothing, False, 2, True, 100, "Title Deed No"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A1Subscriber].[SubscriberID]", "SubscriberID", DataType.Long, Nothing, False, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberName", "[A1Subscriber].[SubscriberName]", "SubscriberName", DataType.String, Nothing, False, 0, True, 200, "Subscriber Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "([ConsumerPropertyDeedHistory]  [A0ConsumerPropertyDeedHistory]" + _
           "    left join [Subscriber]  [A1Subscriber] on [A0ConsumerPropertyDeedHistory].[SubscriberID] = [A1Subscriber].[SubscriberID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
