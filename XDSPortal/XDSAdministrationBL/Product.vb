Imports sembleWare.Runtime
Imports System
Public Class Product 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly ProductID As New IntegerElement("ProductID", Me, True, False, True, True, True, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ProductDesc As New StringElement("ProductDesc", Me, False, True, True, True, True, 50, "Product Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly DefaultPointValue As New DecimalElement("DefaultPointValue", Me, False, True, True, True, True, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly EnableReportingServicesReportYN As New BooleanElement("EnableReportingServicesReportYN", Me, False, True, True, True, True, "Enable Reporting Services Report?", Nothing, False, "Yes;No")
  Public ReadOnly ReportingServicesReportDisplayInd As New IndicatorElement("ReportingServicesReportDisplayInd", Me, False, False, True, True, True, False, "Reporting Services Report Display", Nothing, Product.ReportingServicesReportDisplayIndOptions, "N")
  Public ReadOnly ProductDetails As New TextElement("ProductDetails", Me, True, True, True, False, Nothing, Nothing, Nothing)
  Public ReadOnly ProductType As Relationship = New ProductTypeRelationship("ProductType", Nothing, RelationshipType.Include, True, False, True, Me)
  Public ReadOnly ReportingServicesReport As Relationship = New ReportingServicesReportRelationship("ReportingServicesReport", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _ProductTypeID As New IntegerElement("ProductTypeID", Me, False)
  Public ReadOnly _ReportingServicesReportID As New IntegerElement("ReportingServicesReportID", Me, False)
  Public ReadOnly _ProductType As ProductTypeRelationship = ProductType
  Public ReadOnly _ReportingServicesReport As ReportingServicesReportRelationship = ReportingServicesReport
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moReportingServicesReportDisplayIndOptions As IndicatorOptions
  Public Shared ReadOnly Property ReportingServicesReportDisplayIndOptions() As IndicatorOptions
    Get
      If moReportingServicesReportDisplayIndOptions Is Nothing Then
        moReportingServicesReportDisplayIndOptions = New IndicatorOptions
        moReportingServicesReportDisplayIndOptions.Add("E", "Existing Window")
        moReportingServicesReportDisplayIndOptions.Add("N", "New Window")
      End If
      Return moReportingServicesReportDisplayIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("Product", ApplicationSettings)
    _ProductType._ProductTypeID.LocalElement = _ProductTypeID
    _ReportingServicesReport._ReportingServicesReportID.LocalElement = _ReportingServicesReportID
    AddHandler EnableReportingServicesReportYN.Changed, AddressOf EnableReportingServicesReportYN_Changed
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub EnableReportingServicesReportYN_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BREnableReportingServicesReportYN(Element, True, True)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreateLoad()
  End Sub

  Public Overrides Sub Load()
    MyBase.Load()
    BRCreateLoad()
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreateLoad()
    BREnableReportingServicesReportYN(Me.EnableReportingServicesReportYN, True, False)
  End Sub

  Private Sub BREnableReportingServicesReportYN(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If SetEnableDisableYN Then
      Me.ReportingServicesReport.Enabled = Element.ValueAsObject
      Me.ReportingServicesReport.Mandatory = Element.ValueAsObject
      Me.ReportingServicesReportDisplayInd.Enabled = Element.ValueAsObject
      Me.ReportingServicesReportDisplayInd.Mandatory = Element.ValueAsObject
    End If

    If PerformRulesYN Then
      If Not Element.ValueAsObject Then
        Me.ReportingServicesReport.Instance = Nothing
        Me.ReportingServicesReportDisplayInd.SetToNullValue()
      End If
    End If
  End Sub
#End Region

#Region " Shared Procedures"
  Public Shared Function GetProduct(ByVal ProductID As Integer, ByVal ApplicationSettings As ApplicationSettings) As Product
    Dim oProduct As Product = New ProductRelationship(ApplicationSettings).NewInstance()

    oProduct.ProductID.Load(ProductID)
    oProduct.Load()

    Return oProduct
  End Function

  Public Shared Sub CheckProductAccess(ByVal ProductID As Integer, ByVal ApplicationSettings As ApplicationSettings)
    If Not SystemUserRole.IsLoggedInSystemUserInRole(Role.GetSystemAdministrators(ApplicationSettings), ApplicationSettings) Then
      Dim oSystemUserProfile As SystemUserProfile = SystemUserProfile.GetLoggedInSystemUserProfile(ApplicationSettings)
      Dim oList As List = oSystemUserProfile._SystemUserProfileProduct_OwnMany.GridList()
      Dim oDataRow As DataRow

      oList.AndFilters.Add(New ListFilter("ProductID", "[A0SystemUserProfileProduct].[ProductID]", ProductID, DataType.Integer, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      oList.AndFilters.Add(New ListFilter("StatusInd", "[A0SystemUserProfileProduct].[StatusInd]", XDSAdministrationBL.Constants.SystemUserProfileProduct.Statuses.Allowed, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      If oList.DataTable.Rows.Count = 0 Then
        Throw New BusinessRuleException("You do not have access to this Product. Please contact XDS Support.")
      End If
    End If
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class ProductRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _ProductID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New Product(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _ProductID.ForeignElement = CType(Instance, Product).ProductID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function DropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0Product")
    oList.Columns.Add(New ListColumn("ProductID", "[A0Product].[ProductID]", "ProductID", DataType.Integer, Nothing, True, 0, False, 100, "Product ID"))
    oList.Columns.Add(New ListColumn("ProductTypeDesc", "[A1ProductType].[ProductTypeDesc]", "ProductTypeDesc", DataType.String, Nothing, False, 1, True, 200, "Product Type Description"))
    oList.Columns.Add(New ListColumn("ProductDesc", "[A0Product].[ProductDesc]", "ProductDesc", DataType.String, Nothing, False, 2, True, 200, "Product Description"))
    oList.Columns.Add(New ListColumn("DefaultPointValue", "[A0Product].[DefaultPointValue]", "DefaultPointValue", DataType.Decimal, Nothing, False, 0, True, 100, "Default Point Value"))
    oList.SelectStatement = "select"
    oList.FromClause = "([Product]  [A0Product]" + _
           "    join [ProductType]  [A1ProductType] on [A0Product].[ProductTypeID] = [A1ProductType].[ProductTypeID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0Product")
    oList.Columns.Add(New ListColumn("ProductID", "[A0Product].[ProductID]", "ProductID", DataType.Integer, Nothing, True, 0, False, 100, "Product ID"))
    oList.Columns.Add(New ListColumn("ProductTypeDesc", "[A1ProductType].[ProductTypeDesc]", "ProductTypeDesc", DataType.String, Nothing, False, 1, True, 200, "Product Type Description"))
    oList.Columns.Add(New ListColumn("ProductDesc", "[A0Product].[ProductDesc]", "ProductDesc", DataType.String, Nothing, False, 2, True, 200, "Product Description"))
    oList.Columns.Add(New ListColumn("DefaultPointValue", "[A0Product].[DefaultPointValue]", "DefaultPointValue", DataType.Decimal, Nothing, False, 0, True, 150, "Default Point Value"))
    oList.SelectStatement = "select"
    oList.FromClause = "([Product]  [A0Product]" + _
           "    join [ProductType]  [A1ProductType] on [A0Product].[ProductTypeID] = [A1ProductType].[ProductTypeID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0Product")
    oList.Columns.Add(New ListColumn("ProductID", "[A0Product].[ProductID]", "ProductID", DataType.Integer, Nothing, True, 0, False, 100, "Product ID"))
    oList.Columns.Add(New ListColumn("ProductTypeDesc", "[A1ProductType].[ProductTypeDesc]", "ProductTypeDesc", DataType.String, Nothing, False, 0, True, 200, "Product Type Description"))
    oList.Columns.Add(New ListColumn("ProductDesc", "[A0Product].[ProductDesc]", "ProductDesc", DataType.String, Nothing, False, 1, True, 200, "Product Description"))
    oList.Columns.Add(New ListColumn("DefaultPointValue", "[A0Product].[DefaultPointValue]", "DefaultPointValue", DataType.Decimal, Nothing, False, 0, True, 100, "Default Point Value"))
    oList.Columns.Add(New ListColumn("ProductDetails", "[A0Product].[ProductDetails]", "ProductDetails", DataType.Text, Nothing, False, 0, True, 200, "Product Details"))
    oList.SelectStatement = "select"
    oList.FromClause = "([Product]  [A0Product]" + _
           "    join [ProductType]  [A1ProductType] on [A0Product].[ProductTypeID] = [A1ProductType].[ProductTypeID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
