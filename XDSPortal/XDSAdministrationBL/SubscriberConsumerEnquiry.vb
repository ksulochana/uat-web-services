Imports sembleWare.Runtime
Imports System
Public Class SubscriberConsumerEnquiry 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly SubscriberConsumerEnquiryID As New IdentityElement("SubscriberConsumerEnquiryID", Me, True, Nothing, Nothing)
  Public ReadOnly EnquiryDate As New DateTimeElement("EnquiryDate", Me, False, False, True, True, True, "Enquiry Date", Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly EnquiryStatusInd As New IndicatorElement("EnquiryStatusInd", Me, False, False, False, True, True, True, "Enquiry Status", Nothing, SubscriberConsumerEnquiry.EnquiryStatusIndOptions, "Q")
  Public ReadOnly EnquiryResultInd As New IndicatorElement("EnquiryResultInd", Me, False, False, False, True, True, True, "Enquiry Result", Nothing, SubscriberConsumerEnquiry.EnquiryResultIndOptions, "P")
  Public ReadOnly ProductPointValue As New DecimalElement("ProductPointValue", Me, False, False, True, True, True, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ExternalReference As New StringElement("ExternalReference", Me, False, True, True, True, False, 50, "External Reference", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly IDNo As New StringElement("IDNo", Me, False, True, True, True, False, 13, "ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PassportNo As New StringElement("PassportNo", Me, False, True, True, True, False, 16, "Passport No / Other ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Surname As New StringElement("Surname", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly MaidenName As New StringElement("MaidenName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FirstInitial As New StringElement("FirstInitial", Me, False, True, True, True, False, 1, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SecondInitial As New StringElement("SecondInitial", Me, False, True, True, True, False, 1, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FirstName As New StringElement("FirstName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SecondName As New StringElement("SecondName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly BirthDate As New DateTimeElement("BirthDate", Me, False, True, True, True, False, "Date of Birth", Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly GenderInd As New IndicatorElement("GenderInd", Me, False, False, True, True, True, False, "Gender", Nothing, SubscriberConsumerEnquiry.GenderIndOptions, Nothing)
  Public ReadOnly Address1 As New StringElement("Address1", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Address2 As New StringElement("Address2", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Address3 As New StringElement("Address3", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Address4 As New StringElement("Address4", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PostalCode As New StringElement("PostalCode", Me, False, True, True, True, False, 10, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly TelephoneCode As New StringElement("TelephoneCode", Me, False, True, True, True, False, 5, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly TelephoneNo As New StringElement("TelephoneNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AgeSearchTypeInd As New IndicatorElement("AgeSearchTypeInd", Me, False, False, True, True, True, False, "Type of Deviation", Nothing, SubscriberConsumerEnquiry.AgeSearchTypeIndOptions, "E")
  Public ReadOnly Age As New IntegerElement("Age", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AgeBirthYear As New IntegerElement("AgeBirthYear", Me, False, True, True, True, False, "Year", Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AgeDeviation As New IntegerElement("AgeDeviation", Me, False, True, True, True, False, "Deviation in Years (+-)", Nothing, Nothing, 3, Nothing, Nothing)
  Public ReadOnly AccountNo As New StringElement("AccountNo", Me, False, True, True, True, False, 25, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SubAccountNo As New StringElement("SubAccountNo", Me, False, True, True, True, False, 4, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResultIDNo As New StringElement("ResultIDNo", Me, False, False, True, True, False, 13, "ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResultPassportNo As New StringElement("ResultPassportNo", Me, False, False, True, True, False, 16, "Passport No / Other ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResultFirstInitial As New StringElement("ResultFirstInitial", Me, False, False, True, True, False, 1, "First Initial", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResultSecondInitial As New StringElement("ResultSecondInitial", Me, False, False, True, True, False, 1, "Second Initial", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResultFirstName As New StringElement("ResultFirstName", Me, False, False, True, True, False, 150, "First Name", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResultSecondName As New StringElement("ResultSecondName", Me, False, False, True, True, False, 150, "Second Name", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResultSurname As New StringElement("ResultSurname", Me, False, False, True, True, False, 150, "Surname", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResultBirthDate As New DateTimeElement("ResultBirthDate", Me, False, False, True, True, False, "Birth Date", Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly ResultGenderInd As New IndicatorElement("ResultGenderInd", Me, False, False, False, True, True, False, "Gender", Nothing, SubscriberConsumerEnquiry.ResultGenderIndOptions, Nothing)
  Public ReadOnly TelephoneTypeInd As New IndicatorElement("TelephoneTypeInd", Me, False, False, True, True, False, True, "Search On", Nothing, SubscriberConsumerEnquiry.TelephoneTypeIndOptions, "T")
  Public ReadOnly TelephoneTelephoneCode As New StringElement("TelephoneTelephoneCode", Me, False, True, True, False, False, 5, "Landline Code", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly TelephoneTelephoneNo As New StringElement("TelephoneTelephoneNo", Me, False, True, True, False, False, 50, "Landline No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly TelephoneCellularCode As New StringElement("TelephoneCellularCode", Me, False, True, True, False, False, 5, "Cellular Code", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly TelephoneCellularNo As New StringElement("TelephoneCellularNo", Me, False, True, True, False, False, 50, "Cellular No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AddressTypeInd As New IndicatorElement("AddressTypeInd", Me, False, False, True, True, False, True, "Search On", Nothing, SubscriberConsumerEnquiry.AddressTypeIndOptions, "S")
  Public ReadOnly AddressStreetNo As New StringElement("AddressStreetNo", Me, False, True, True, False, False, 100, "Street Number", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AddressStreetName As New StringElement("AddressStreetName", Me, False, True, True, False, False, 100, "Street Name", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AddressStreetSuburb As New StringElement("AddressStreetSuburb", Me, False, True, True, False, False, 100, "Suburb", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AddressStreetPostalCode As New StringElement("AddressStreetPostalCode", Me, False, True, True, False, False, 10, "Postal Code", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AddressPostalNo As New StringElement("AddressPostalNo", Me, False, True, True, False, False, 100, "Postal No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AddressPostalSuburb As New StringElement("AddressPostalSuburb", Me, False, True, True, False, False, 100, "Suburb", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AddressPostalPostalCode As New StringElement("AddressPostalPostalCode", Me, False, True, True, False, False, 10, "Postal Code", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Product As Relationship = New ProductRelationship("Product", Nothing, RelationshipType.Include, True, True, True, Me)
  Public ReadOnly SystemUser As Relationship = New SystemUserRelationship("SystemUser", Nothing, RelationshipType.Include, True, False, True, Me)
  Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly SubscriberConsumerEnquiryResult_OwnMany As Relationship = New SubscriberConsumerEnquiryResultRelationship("SubscriberConsumerEnquiryResult", Nothing, "SubscriberConsumerEnquiry", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _ProductID As New IntegerElement("ProductID", Me, False)
  Public ReadOnly _SystemUserID As New IntegerElement("SystemUserID", Me, False)
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _Product As ProductRelationship = Product
  Public ReadOnly _SystemUser As SystemUserRelationship = SystemUser
  Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
  Public ReadOnly _SubscriberConsumerEnquiryResult_OwnMany As SubscriberConsumerEnquiryResultRelationship = SubscriberConsumerEnquiryResult_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moEnquiryStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property EnquiryStatusIndOptions() As IndicatorOptions
    Get
      If moEnquiryStatusIndOptions Is Nothing Then
        moEnquiryStatusIndOptions = New IndicatorOptions
        moEnquiryStatusIndOptions.Add("Q", "Queued")
        moEnquiryStatusIndOptions.Add("P", "Processing")
        moEnquiryStatusIndOptions.Add("C", "Completed")
      End If
      Return moEnquiryStatusIndOptions
    End Get
  End Property
  Private Shared moEnquiryResultIndOptions As IndicatorOptions
  Public Shared ReadOnly Property EnquiryResultIndOptions() As IndicatorOptions
    Get
      If moEnquiryResultIndOptions Is Nothing Then
        moEnquiryResultIndOptions = New IndicatorOptions
        moEnquiryResultIndOptions.Add("P", "Pending")
        moEnquiryResultIndOptions.Add("N", "No Record Found")
        moEnquiryResultIndOptions.Add("F", "Record Found")
        moEnquiryResultIndOptions.Add("M", "Multiple Records Found")
        moEnquiryResultIndOptions.Add("S", "Record Selected")
      End If
      Return moEnquiryResultIndOptions
    End Get
  End Property
  Private Shared moGenderIndOptions As IndicatorOptions
  Public Shared ReadOnly Property GenderIndOptions() As IndicatorOptions
    Get
      If moGenderIndOptions Is Nothing Then
        moGenderIndOptions = New IndicatorOptions
        moGenderIndOptions.Add("M", "Male")
        moGenderIndOptions.Add("F", "Female")
      End If
      Return moGenderIndOptions
    End Get
  End Property
  Private Shared moAgeSearchTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property AgeSearchTypeIndOptions() As IndicatorOptions
    Get
      If moAgeSearchTypeIndOptions Is Nothing Then
        moAgeSearchTypeIndOptions = New IndicatorOptions
        moAgeSearchTypeIndOptions.Add("E", "Exact")
        moAgeSearchTypeIndOptions.Add("D", "Deviation")
      End If
      Return moAgeSearchTypeIndOptions
    End Get
  End Property
  Private Shared moResultGenderIndOptions As IndicatorOptions
  Public Shared ReadOnly Property ResultGenderIndOptions() As IndicatorOptions
    Get
      If moResultGenderIndOptions Is Nothing Then
        moResultGenderIndOptions = New IndicatorOptions
        moResultGenderIndOptions.Add("M", "Male")
        moResultGenderIndOptions.Add("F", "Female")
      End If
      Return moResultGenderIndOptions
    End Get
  End Property
  Private Shared moTelephoneTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property TelephoneTypeIndOptions() As IndicatorOptions
    Get
      If moTelephoneTypeIndOptions Is Nothing Then
        moTelephoneTypeIndOptions = New IndicatorOptions
        moTelephoneTypeIndOptions.Add("T", "Landline Number")
        moTelephoneTypeIndOptions.Add("C", "Cellular Number")
      End If
      Return moTelephoneTypeIndOptions
    End Get
  End Property
  Private Shared moAddressTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property AddressTypeIndOptions() As IndicatorOptions
    Get
      If moAddressTypeIndOptions Is Nothing Then
        moAddressTypeIndOptions = New IndicatorOptions
        moAddressTypeIndOptions.Add("S", "Street Address")
        moAddressTypeIndOptions.Add("P", "Postal Address")
      End If
      Return moAddressTypeIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberConsumerEnquiry", ApplicationSettings)
    _Product._ProductID.LocalElement = _ProductID
    _SystemUser._SystemUserID.LocalElement = _SystemUserID
    _Subscriber._SubscriberID.LocalElement = _SubscriberID
    AddHandler FirstName.Changed, AddressOf FirstName_Changed
    AddHandler SecondName.Changed, AddressOf SecondName_Changed
    AddHandler AgeSearchTypeInd.Changed, AddressOf AgeSearchTypeInd_Changed
    AddHandler Age.Changed, AddressOf Age_Changed
    AddHandler AgeBirthYear.Changed, AddressOf AgeBirthYear_Changed
    AddHandler TelephoneTypeInd.Changed, AddressOf TelephoneTypeInd_Changed
    AddHandler AddressTypeInd.Changed, AddressOf AddressTypeInd_Changed
    AddHandler Product.Changed, AddressOf Product_Changed
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub Product_Changed(ByVal Relationship As Relationship)
    BRProduct(Relationship, True, True)
  End Sub

  Private Sub AgeSearchTypeInd_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BRAgeSearchTypeInd(Element, True, True)
  End Sub

  Private Sub Age_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BRAge(Element, True, True)
  End Sub

  Private Sub AgeBirthYear_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BRAgeBirthYear(Element, True, True)
  End Sub

  Private Sub TelephoneTypeInd_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BRTelephoneTypeInd(Element, True, True)
  End Sub

  Private Sub AddressTypeInd_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BRAddressTypeInd(Element, True, True)
  End Sub

  Private Sub FirstName_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BRFirstName(Element, True, True)
  End Sub

  Private Sub SecondName_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BRSecondName(Element, True, True)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"

  Public Sub Validate()
    Dim bAllowYN As Boolean = False
    Dim oException As Exception

    Select Case Me._ProductID.Value
      Case Constants.Product.Records.TraceConsumer, Constants.Product.Records.TraceConsumerWebService, _
           Constants.Product.Records.TraceConsumerPreScreen, _
           Constants.Product.Records.CreditGrantorConsumerCreditEnquiry, Constants.Product.Records.CreditGrantorConsumerCreditEnquiryWebService, _
           Constants.Product.Records.CreditGrantorConsumerApplicationCreditEnquiry, Constants.Product.Records.CreditGrantorConsumerApplicationCreditEnquiryWebService, _
           Constants.Product.Records.CreditGrantorCreditApplication
        oException = New MissingValuesException("(ID No) or (Passport No / Other ID No) or (Surname and First Initial and Date of Birth)", MyBase.Caption, "")
        If Not Me.IDNo.IsEmpty Then
          bAllowYN = True
        End If

        If Not Me.PassportNo.IsEmpty Then
          bAllowYN = True
        End If

        If Not Me.Surname.IsEmpty And Not Me.FirstInitial.IsEmpty And Not Me.BirthDate.IsEmpty Then
          bAllowYN = True
        End If
      Case Constants.Product.Records.TraceConsumerMultiple
        oException = New MissingValuesException("(ID No) or (Passport No / Other ID No) or (Surname and First Initial and Date of Birth) or (Account No)", MyBase.Caption, "")
        If Not Me.IDNo.IsEmpty Then
          bAllowYN = True
        End If

        If Not Me.PassportNo.IsEmpty Then
          bAllowYN = True
        End If

        If Not Me.Surname.IsEmpty And Not Me.FirstInitial.IsEmpty And Not Me.BirthDate.IsEmpty Then
          bAllowYN = True
        End If

        If Not Me.AccountNo.IsEmpty Then
          bAllowYN = True
        End If
      Case Constants.Product.Records.TraceConsumerIdentityVerification, Constants.Product.Records.TraceConsumerIdentityVerificationWebService
        oException = New MissingValuesException("ID No", MyBase.Caption, "")
        If Not Me.IDNo.IsEmpty Then
          bAllowYN = True
        End If
      Case Constants.Product.Records.AuthenticationConsumer
        oException = New MissingValuesException("(ID No) or (Passport No / Other ID No) or (Cellular No) or (Account No)", MyBase.Caption, "")
        If Not Me.IDNo.IsEmpty Then
          bAllowYN = True
        End If

        If Not Me.PassportNo.IsEmpty Then
          bAllowYN = True
        End If

        If Not Me.TelephoneCellularCode.IsEmpty And Not Me.TelephoneCellularNo.IsEmpty Then
          bAllowYN = True
        End If

        If Not Me.AccountNo.IsEmpty Then
          bAllowYN = True
        End If

        If Not Me.IDNo.IsEmpty AndAlso Not Me.PassportNo.IsEmpty Then
          Throw New BusinessRuleException("Invalid search criteria. Please enter either an ID No OR a Passport No.")
        End If
      Case Else
        bAllowYN = True
    End Select

    If Not bAllowYN Then
      Throw New BusinessRuleException(oException.Message)
    End If
  End Sub

  Public Sub QueueMatch()
    Validate()
    Me.EnquiryStatusInd.Value = Constants.SubscriberConsumerEnquiry.Statuses.Queued
    Me.Save()
  End Sub

  Public Sub Match()
    Validate()
    Me.EnquiryStatusInd.Value = Constants.SubscriberConsumerEnquiry.Statuses.Processing
    Me.Save()

    Dim oDataRow As DataRow
    Select Case Me.TelephoneTypeInd.Value
      Case "T"
        Me.TelephoneCode.Value = Me.TelephoneTelephoneCode.Value
        Me.TelephoneNo.Value = Me.TelephoneTelephoneNo.Value
      Case "C"
        Me.TelephoneCode.Value = Me.TelephoneCellularCode.Value
        Me.TelephoneNo.Value = Me.TelephoneCellularNo.Value
    End Select
    Select Case Me.AddressTypeInd.Value
      Case "S"
        Me.Address1.Value = Me.AddressStreetNo.Value & " " & Me.AddressStreetName.Value
        Me.Address2.Value = Me.AddressStreetSuburb.Value
        Me.PostalCode.Value = Me.AddressStreetPostalCode.Value
      Case "P"
        Me.Address1.Value = Me.AddressPostalNo.Value
        Me.Address2.Value = Me.AddressPostalSuburb.Value
        Me.PostalCode.Value = Me.AddressPostalPostalCode.Value
    End Select
    Dim nEnquiryMatchingEngineID As Integer = EnquiryMatchingEngine.ConsumerMatch(Me.IDNo, Me.PassportNo, Me.Surname, Me.MaidenName, Me.FirstInitial, Me.FirstName, _
                                                                                  Me.SecondInitial, Me.SecondName, Me.GenderInd, Me.BirthDate, Me.AccountNo, _
                                                                                  Me.SubAccountNo, Me.Address1, Me.Address2, Me.Address3, Me.Address4, Me.PostalCode, _
                                                                                  Me.TelephoneCode, Me.TelephoneNo, Me.AgeSearchTypeInd, Me.Age, Me.AgeBirthYear, _
                                                                                  Me.AgeDeviation, Me._ProductID.Value, ApplicationSettings)
    Dim oString As Text.StringBuilder = New Text.StringBuilder

    oString.Append("insert into SubscriberConsumerEnquiryResult(SubscriberID, SubscriberConsumerEnquiryID, ConsumerID, DetailsViewedYN, ConsumerSelectedYN, IDNo, PassportNo, FirstInitial, SecondInitial, FirstName, Surname, BirthDate, GenderInd)" & vbNewLine)
    oString.Append("select " & Me._SubscriberID.Value & ", " & Me.SubscriberConsumerEnquiryID.Value & ", ecr.ConsumerID, 0, 0, c.IDNo, c.PassportNo, c.FirstInitial, c.SecondInitial, c.FirstName, c.Surname, c.BirthDate, c.GenderInd" & vbNewLine)
    oString.Append("  from EnquiryMatchingEngineConsumerResult ecr inner join EnquiryMatchingEngineConsumer mec")
    oString.Append("                                                  on ecr.EnquiryMatchingEngineID = mec.EnquiryMatchingEngineID" & vbNewLine)
    oString.Append("                                                 and ecr.EnquiryMatchingEngineConsumerID = mec.EnquiryMatchingEngineConsumerID" & vbNewLine)
    oString.Append("                                               inner join Consumer c" & vbNewLine)
    oString.Append("                                                  on ecr.ConsumerID = c.ConsumerID" & vbNewLine)
    oString.Append(" where mec.EnquiryMatchingEngineID = " & ApplicationSettings.QueryBuilder.ToSQL(nEnquiryMatchingEngineID) & vbNewLine)
    oString.Append("   and ecr.IgnoreRecordYN = 0")
    ApplicationSettings.ActionQuery(oString.ToString())

    oString = New Text.StringBuilder
    oString.Append("select count(ecr.ConsumerID)" & vbNewLine)
    oString.Append("  from EnquiryMatchingEngineConsumerResult ecr inner join EnquiryMatchingEngineConsumer mec" & vbNewLine)
    oString.Append("                                                  on ecr.EnquiryMatchingEngineID = mec.EnquiryMatchingEngineID" & vbNewLine)
    oString.Append("                                                 and ecr.EnquiryMatchingEngineConsumerID = mec.EnquiryMatchingEngineConsumerID" & vbNewLine)
    oString.Append(" where mec.EnquiryMatchingEngineID = " & ApplicationSettings.QueryBuilder.ToSQL(nEnquiryMatchingEngineID) & vbNewLine)
    oString.Append("   and ecr.IgnoreRecordYN = 0")
    For Each oDataRow In ApplicationSettings.ResultQuery(oString.ToString()).Rows
      Select Case System.Convert.ToInt32(oDataRow(0))
        Case 0
          Me.EnquiryResultInd.Value = Constants.SubscriberConsumerEnquiry.Results.NoRecordFound
        Case 1
          Me.EnquiryResultInd.Value = Constants.SubscriberConsumerEnquiry.Results.RecordFound
        Case Else
          Me.EnquiryResultInd.Value = Constants.SubscriberConsumerEnquiry.Results.MultipleRecordsFound
      End Select
    Next

    Me.EnquiryStatusInd.Value = Constants.SubscriberConsumerEnquiry.Statuses.Completed
    Me.Save()
  End Sub
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreate()
  End Sub

  Public Overrides Sub Load()
    MyBase.Load()
    BRLoad()
  End Sub

  Public Overrides Sub Save()
    With ApplicationSettings
      Try
        Dim bIsNewYN As Boolean = Me.IsNew
        .BeginTransaction()
        BRBeforeSave(bIsNewYN)
        MyBase.Save()
        .CommitTransaction()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
    End With
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreate()
    BRAgeSearchTypeInd(Me.AgeSearchTypeInd, True, False)
  End Sub

  Private Sub BRLoad()
    Me.DisableElementsAndRelationship()
  End Sub

  Private Sub BRBeforeSave(ByVal IsNewYN As Boolean)
    If IsNewYN Then
      Me.EnquiryDate.Value = System.DateTime.Now
      ' If SystemUser is not empty, then the XDSAdministration Services has populated this value.
      If SystemUser.IsEmpty Then
        Me.SystemUser.Instance = XDSAdministrationBL.SystemUser.GetLoggedInSystemUser(ApplicationSettings)
      End If

      Me.ResultIDNo.Value = Me.IDNo.Value
      Me.ResultPassportNo.ValueAsObject = Me.PassportNo.ValueAsObject
      Me.ResultFirstInitial.ValueAsObject = Me.FirstInitial.ValueAsObject
      Me.ResultSecondInitial.ValueAsObject = Me.SecondInitial.ValueAsObject
      Me.ResultSurname.ValueAsObject = Me.Surname.ValueAsObject
      Me.ResultBirthDate.ValueAsObject = Me.BirthDate.ValueAsObject
      Me.ResultGenderInd.Value = Me.GenderInd.Value
    End If
    XDSAdministrationBL.Consumer.ValidateIDNo(Me.IDNo.Value, ApplicationSettings)
  End Sub

  Private Sub BRAgeSearchTypeInd(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If SetEnableDisableYN Then
      Me.AgeDeviation.Enabled = False
      Me.AgeDeviation.Mandatory = False
      Select Case Element.ValueAsObject
        Case Constants.SubscriberConsumerEnquiry.AgeSearchTypes.Deviation
          Me.AgeDeviation.Enabled = True
          Me.AgeDeviation.Mandatory = True
      End Select
    End If

    If PerformRulesYN Then
      Select Case Element.ValueAsObject
        Case Constants.SubscriberConsumerEnquiry.AgeSearchTypes.Deviation
          Me.AgeDeviation.Value = 3
        Case Constants.SubscriberConsumerEnquiry.AgeSearchTypes.Exact
          Me.AgeDeviation.SetToNullValue()
      End Select
    End If
  End Sub

  Private Sub BRAgeBirthYear(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If PerformRulesYN Then
      Me.Age.Value = DatePart(DateInterval.Year, System.DateTime.Now) - Element.ValueAsObject
    End If
  End Sub

  Private Sub BRAge(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If PerformRulesYN Then
      Me.AgeBirthYear.Value = DatePart(DateInterval.Year, DateAdd(DateInterval.Year, Element.ValueAsObject * -1, System.DateTime.Now))
    End If
  End Sub

  Private Sub BRTelephoneTypeInd(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    Utility.BRTelephoneTypeInd(Element, Me.TelephoneTelephoneCode, Me.TelephoneTelephoneNo, Me.TelephoneCellularCode, _
                               Me.TelephoneCellularNo, SetEnableDisableYN, PerformRulesYN)
  End Sub

  Private Sub BRAddressTypeInd(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    Utility.BRAddressTypeInd(Element, Me.AddressPostalNo, Me.AddressPostalSuburb, Me.AddressPostalPostalCode, Me.AddressStreetNo, _
                             Me.AddressStreetName, Me.AddressStreetSuburb, Me.AddressStreetPostalCode, SetEnableDisableYN, PerformRulesYN)
  End Sub

  Private Sub BRProduct(ByVal Relationship As Relationship, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    Dim oProduct As Product = Nothing

    If Not Relationship.IsEmpty Then
      oProduct = Relationship.Instance
    End If
    Utility.BRProductTraceConsumer(oProduct, Me.IDNo, Me.PassportNo, Me.Surname, Me.MaidenName, _
                                   Me.FirstInitial, Me.SecondInitial, Me.FirstName, Me.SecondName, _
                                   Me.BirthDate, Me.GenderInd, Me.AddressTypeInd, Me.AddressStreetNo, _
                                   Me.AddressStreetName, Me.AddressStreetSuburb, Me.AddressStreetPostalCode, _
                                   Me.AddressPostalNo, Me.AddressPostalSuburb, Me.AddressPostalPostalCode, _
                                   Me.TelephoneTypeInd, Me.TelephoneTelephoneCode, Me.TelephoneTelephoneNo, _
                                   Me.TelephoneCellularCode, Me.TelephoneCellularNo, Me.AccountNo, _
                                   Me.SubAccountNo, Me.Age, Me.AgeBirthYear, Me.AgeDeviation, Me.AgeSearchTypeInd, _
                                   SetEnableDisableYN, PerformRulesYN)

    If PerformRulesYN Then
      If Not oProduct Is Nothing Then
        Dim oSubscriberProfile As SubscriberProfile = SubscriberProfile.GetSubscriberProfile(Me._SubscriberID.Value, ApplicationSettings)
        Dim oSubscriberProfileProduct As SubscriberProfileProduct = SubscriberProfileProduct.GetSubscriberProfileProduct(Me._SubscriberID.Value, oProduct.ProductID.Value, ApplicationSettings)

        If oSubscriberProfileProduct.OverrideDefaultPointValueYN.Value Then
          Me.ProductPointValue.Value = oSubscriberProfileProduct.OverridePointValue.Value
        Else
          Me.ProductPointValue.Value = oProduct.DefaultPointValue.Value
        End If
      End If
    End If
  End Sub

  Private Sub BRFirstName(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If PerformRulesYN Then
      If Not Element.IsEmpty Then
        Dim sFirstName As String = Element.ValueAsObject

        Me.FirstInitial.Value = sFirstName.Substring(0, 1)
      End If
    End If
  End Sub

  Private Sub BRSecondName(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If PerformRulesYN Then
      If Not Element.IsEmpty Then
        Dim sSecondName As String = Element.ValueAsObject

        Me.SecondInitial.Value = sSecondName.Substring(0, 1)
      End If
    End If
  End Sub
#End Region

#Region " Public Methods"
  Public Function GetSubscriberConsumerEnquiryResult() As SubscriberConsumerEnquiryResult
        If Me.EnquiryResultInd.Value = Constants.SubscriberConsumerEnquiry.Results.RecordFound Or Me.EnquiryResultInd.Value = Constants.SubscriberConsumerEnquiry.Results.RecordSelected Or Me.EnquiryResultInd.Value = "M" Then
            Dim oList As List = Me._SubscriberConsumerEnquiryResult_OwnMany.GridList
            Dim oDataTable As DataTable = oList.DataTable
            Dim oDataRow As DataRow = oDataTable.Rows(0)
            Dim oSubscriberConsumerEnquiryResult As SubscriberConsumerEnquiryResult = Me.SubscriberConsumerEnquiryResult_OwnMany.NewInstance()
            oSubscriberConsumerEnquiryResult._ConsumerID.Load(oDataRow("ConsumerID"))
            oSubscriberConsumerEnquiryResult.Load()
            Return oSubscriberConsumerEnquiryResult
        End If
        Return Nothing
  End Function

  Public Function GetSubscriberTraceConsumer() As SubscriberTraceConsumer
    If Me.EnquiryResultInd.Value = Constants.SubscriberConsumerEnquiry.Results.RecordFound Or Me.EnquiryResultInd.Value = Constants.SubscriberConsumerEnquiry.Results.RecordSelected Or Me.EnquiryResultInd.Value = "M" Then
      Dim oSubscriber As Subscriber = Me.Subscriber.Instance
      Dim oSubscriberConsumerEnquiryResult As SubscriberConsumerEnquiryResult = Me.GetSubscriberConsumerEnquiryResult()
      Dim oList As List = oSubscriber._SubscriberTraceConsumer_OwnMany.SubscriberGridList
      Dim oDataRow As DataRow

      oList.LimitByRelatedPart(Me, "SubscriberConsumerEnquiry")

      If Not Me._ProductID.Value = Constants.Product.Records.TraceConsumerMultiple Then
        oList.LimitByRelatedPart(oSubscriberConsumerEnquiryResult.Consumer.Instance, "Consumer")
      End If

      For Each oDataRow In oList.DataTable.Rows
        Dim oSubscriberTraceConsumer As SubscriberTraceConsumer = oSubscriber.SubscriberTraceConsumer_OwnMany.NewInstance

        oSubscriberTraceConsumer.SubscriberTraceConsumerID.Load(oDataRow("SubscriberTraceConsumerID"))
        oSubscriberTraceConsumer.Load()
        Return oSubscriberTraceConsumer
      Next
      Return Nothing
    End If
    Return Nothing
  End Function
#End Region
End Class 'sembleWare: Part

Public Class SubscriberConsumerEnquiryRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberConsumerEnquiryID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberConsumerEnquiry(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberConsumerEnquiryID.ForeignElement = CType(Instance, SubscriberConsumerEnquiry).SubscriberConsumerEnquiryID
    _SubscriberID.ForeignElement = CType(Instance, SubscriberConsumerEnquiry)._SubscriberID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function MultipleConsumerGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberConsumerEnquiry")
    oList.Columns.Add(New ListColumn("SubscriberConsumerEnquiryID", "[A0SubscriberConsumerEnquiry].[SubscriberConsumerEnquiryID]", "SubscriberConsumerEnquiryID", DataType.Long, Nothing, True, 0, False, 100, "Subscriber Consumer Enquiry ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberConsumerEnquiry].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("EnquiryDate", "[A0SubscriberConsumerEnquiry].[EnquiryDate]", "EnquiryDate", DataType.DateTime, "g", False, -1, True, 100, "Enquiry Date"))
    oList.Columns.Add(New ListColumn("ResultIDNo", "[A0SubscriberConsumerEnquiry].[ResultIDNo]", "ResultIDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("ResultPassportNo", "[A0SubscriberConsumerEnquiry].[ResultPassportNo]", "ResultPassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No / Other ID No"))
    oList.Columns.Add(New ListColumn("ResultSurname", "[A0SubscriberConsumerEnquiry].[ResultSurname]", "ResultSurname", DataType.String, Nothing, False, 0, True, 100, "Surname"))
    oList.Columns.Add(New ListColumn("ResultFirstInitial", "[A0SubscriberConsumerEnquiry].[ResultFirstInitial]", "ResultFirstInitial", DataType.String, Nothing, False, 0, True, 100, "First Initial"))
    oList.Columns.Add(New ListColumn("ResultBirthDate", "[A0SubscriberConsumerEnquiry].[ResultBirthDate]", "ResultBirthDate", DataType.DateTime, "d", False, 0, True, 100, "Birth Date"))
    oList.Columns.Add(New ListColumn("AccountNo", "[A0SubscriberConsumerEnquiry].[AccountNo]", "AccountNo", DataType.String, Nothing, False, 0, True, 100, "Account No"))
    oList.Columns.Add(New ListColumn("SubAccountNo", "[A0SubscriberConsumerEnquiry].[SubAccountNo]", "SubAccountNo", DataType.String, Nothing, False, 0, True, 100, "Sub Account No"))
    oList.Columns.Add(New ListColumn("EnquiryStatusInd", "[A0SubscriberConsumerEnquiry].[EnquiryStatusInd]", "EnquiryStatusInd", DataType.String, Nothing, False, 0, True, 100, "Enquiry Status"))
    oList.Columns.Add(New ListColumn("EnquiryResultInd", "[A0SubscriberConsumerEnquiry].[EnquiryResultInd]", "EnquiryResultInd", DataType.String, Nothing, False, 0, True, 100, "Enquiry Result"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberConsumerEnquiry]  [A0SubscriberConsumerEnquiry]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function AuthenticationGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberConsumerEnquiry")
    oList.Columns.Add(New ListColumn("SubscriberConsumerEnquiryID", "[A0SubscriberConsumerEnquiry].[SubscriberConsumerEnquiryID]", "SubscriberConsumerEnquiryID", DataType.Long, Nothing, True, 0, False, 100, "Subscriber Consumer Enquiry ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberConsumerEnquiry].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("ResultIDNo", "[A0SubscriberConsumerEnquiry].[ResultIDNo]", "ResultIDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("ResultPassportNo", "[A0SubscriberConsumerEnquiry].[ResultPassportNo]", "ResultPassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No / Other ID No"))
    oList.Columns.Add(New ListColumn("ResultSurname", "[A0SubscriberConsumerEnquiry].[ResultSurname]", "ResultSurname", DataType.String, Nothing, False, 0, True, 100, "Surname"))
    oList.Columns.Add(New ListColumn("ResultFirstName", "[A0SubscriberConsumerEnquiry].[ResultFirstName]", "ResultFirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("ResultSecondName", "[A0SubscriberConsumerEnquiry].[ResultSecondName]", "ResultSecondName", DataType.String, Nothing, False, 0, True, 100, "Second Name"))
    oList.Columns.Add(New ListColumn("ResultBirthDate", "[A0SubscriberConsumerEnquiry].[ResultBirthDate]", "ResultBirthDate", DataType.DateTime, "d", False, 0, True, 100, "Birth Date"))
    oList.Columns.Add(New ListColumn("ResultGenderInd", "[A0SubscriberConsumerEnquiry].[ResultGenderInd]", "ResultGenderInd", DataType.String, Nothing, False, 0, True, 100, "Gender"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberConsumerEnquiry]  [A0SubscriberConsumerEnquiry]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
