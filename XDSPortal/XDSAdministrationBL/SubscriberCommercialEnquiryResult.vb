Imports sembleWare.Runtime
Imports System
Public Class SubscriberCommercialEnquiryResult 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly RegistrationNo As New StringElement("RegistrationNo", Me, False, False, True, True, False, 14, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CommercialName As New StringElement("CommercialName", Me, False, True, True, True, False, 150, "Business Name", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly TaxNo As New StringElement("TaxNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly DetailsViewedYN As New BooleanElement("DetailsViewedYN", Me, False, False, True, True, True, "Details Viewed?", Nothing, False, "Yes;No")
  Public ReadOnly DetailsViewedDate As New DateTimeElement("DetailsViewedDate", Me, False, False, True, True, False, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly CommercialSelectedYN As New BooleanElement("CommercialSelectedYN", Me, False, True, True, True, True, "Commercial Selected?", Nothing, False, "Yes;No")
  Public ReadOnly SIC As Relationship = New SICRelationship("SIC", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly SubscriberCommercialEnquiry As Relationship = New SubscriberCommercialEnquiryRelationship("SubscriberCommercialEnquiry", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly Commercial As Relationship = New CommercialRelationship("Commercial", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SICCode As New StringElement("SICCode", Me, False)
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _SubscriberCommercialEnquiryID As New IntegerElement("SubscriberCommercialEnquiryID", Me, True)
  Public ReadOnly _CommercialID As New IntegerElement("CommercialID", Me, True)
  Public ReadOnly _SIC As SICRelationship = SIC
  Public ReadOnly _SubscriberCommercialEnquiry As SubscriberCommercialEnquiryRelationship = SubscriberCommercialEnquiry
  Public ReadOnly _Commercial As CommercialRelationship = Commercial
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberCommercialEnquiryResult", ApplicationSettings)
    _SIC._SICCode.LocalElement = _SICCode
    _SubscriberCommercialEnquiry._SubscriberID.LocalElement = _SubscriberID
    _SubscriberCommercialEnquiry._SubscriberCommercialEnquiryID.LocalElement = _SubscriberCommercialEnquiryID
    _Commercial._CommercialID.LocalElement = _CommercialID
    AddHandler DetailsViewedYN.Changed, AddressOf DetailsViewedYN_Changed
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub DetailsViewedYN_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"

  Public Sub SelectRecord()
    With ApplicationSettings
      Try
        .BeginTransaction()
        Me.CommercialSelectedYN.Value = True
        Me.DetailsViewedYN.Value = True
        Me.DetailsViewedDate.Value = System.DateTime.Now
        Me.Save()

        Dim oCommercial As Commercial = Me.Commercial.Instance
        Dim oSubscriberCommercialEnquiry As SubscriberCommercialEnquiry = Me.SubscriberCommercialEnquiry.Instance
        oSubscriberCommercialEnquiry.EnquiryResultInd.Value = Constants.SubscriberConsumerEnquiry.Results.RecordSelected

        oSubscriberCommercialEnquiry.ResultRegistrationNo.Value = oCommercial.RegistrationNo.Value
        oSubscriberCommercialEnquiry.ResultCommercialName.Value = oCommercial.CommercialName.Value
        oSubscriberCommercialEnquiry.ResultTaxNo.Value = oCommercial.TaxNo.Value
        oSubscriberCommercialEnquiry.ResultSIC.Instance = oCommercial.SIC.Instance

        oSubscriberCommercialEnquiry.Save()
        .CommitTransaction()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
    End With
  End Sub

#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class SubscriberCommercialEnquiryResultRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SubscriberCommercialEnquiryID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _CommercialID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberCommercialEnquiryResult(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberID.ForeignElement = CType(Instance, SubscriberCommercialEnquiryResult)._SubscriberID
    _SubscriberCommercialEnquiryID.ForeignElement = CType(Instance, SubscriberCommercialEnquiryResult)._SubscriberCommercialEnquiryID
    _CommercialID.ForeignElement = CType(Instance, SubscriberCommercialEnquiryResult)._CommercialID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberCommercialEnquiryResult")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberCommercialEnquiryResult].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberCommercialEnquiryID", "[A0SubscriberCommercialEnquiryResult].[SubscriberCommercialEnquiryID]", "SubscriberCommercialEnquiryID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber Commercial Enquiry ID"))
    oList.Columns.Add(New ListColumn("CommercialID", "[A0SubscriberCommercialEnquiryResult].[CommercialID]", "CommercialID", DataType.Integer, Nothing, True, 0, False, 100, "Commercial ID"))
    oList.Columns.Add(New ListColumn("RegistrationNo", "[A0SubscriberCommercialEnquiryResult].[RegistrationNo]", "RegistrationNo", DataType.String, Nothing, False, 0, True, 100, "Registration No"))
    oList.Columns.Add(New ListColumn("CommercialName", "[A0SubscriberCommercialEnquiryResult].[CommercialName]", "CommercialName", DataType.String, Nothing, False, 0, True, 100, "Business Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberCommercialEnquiryResult]  [A0SubscriberCommercialEnquiryResult]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function MultipleMatchGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberCommercialEnquiryResult")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberCommercialEnquiryResult].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberCommercialEnquiryID", "[A0SubscriberCommercialEnquiryResult].[SubscriberCommercialEnquiryID]", "SubscriberCommercialEnquiryID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber Commercial Enquiry ID"))
    oList.Columns.Add(New ListColumn("CommercialID", "[A0SubscriberCommercialEnquiryResult].[CommercialID]", "CommercialID", DataType.Integer, Nothing, True, 0, False, 100, "Commercial ID"))
    oList.Columns.Add(New ListColumn("RegistrationNo", "[A0SubscriberCommercialEnquiryResult].[RegistrationNo]", "RegistrationNo", DataType.String, Nothing, False, 0, True, 100, "Registration No"))
    oList.Columns.Add(New ListColumn("CommercialName", "[A0SubscriberCommercialEnquiryResult].[CommercialName]", "CommercialName", DataType.String, Nothing, False, 0, True, 100, "Business Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberCommercialEnquiryResult]  [A0SubscriberCommercialEnquiryResult]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

  Public Function MultipleMatchGridList1() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim strSubQuery = "case when right([A0SubscriberCommercialEnquiryResult].[RegistrationNo],2) =  '06' " + _
	                    "then  [A0SubscriberCommercialEnquiryResult].[CommercialName] + ' LTD' " + _
	                    "when right([A0SubscriberCommercialEnquiryResult].[RegistrationNo],2) =  '07' " + _
	                    "then  [A0SubscriberCommercialEnquiryResult].[CommercialName] + ' (PTY)LTD' " + _
	                    "when right([A0SubscriberCommercialEnquiryResult].[RegistrationNo],2) =  '23' " + _
	                    "then  [A0SubscriberCommercialEnquiryResult].[CommercialName] + ' CC' " + _
	                    "when right([A0SubscriberCommercialEnquiryResult].[RegistrationNo],2) =  '21' " + _
	                    "then  [A0SubscriberCommercialEnquiryResult].[CommercialName] + ' INC' " + _
	                    "else [A0SubscriberCommercialEnquiryResult].[CommercialName] end"
    Dim oList As DBList = New DBList(Me, "A0SubscriberCommercialEnquiryResult")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberCommercialEnquiryResult].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberCommercialEnquiryID", "[A0SubscriberCommercialEnquiryResult].[SubscriberCommercialEnquiryID]", "SubscriberCommercialEnquiryID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber Commercial Enquiry ID"))
    oList.Columns.Add(New ListColumn("CommercialID", "[A0SubscriberCommercialEnquiryResult].[CommercialID]", "CommercialID", DataType.Integer, Nothing, True, 0, False, 100, "Commercial ID"))
    oList.Columns.Add(New ListColumn("RegistrationNo", "[A0SubscriberCommercialEnquiryResult].[RegistrationNo]", "RegistrationNo", DataType.String, Nothing, False, 0, True, 100, "Registration No"))
    oList.Columns.Add(New ListColumn("CommercialName", strSubQuery, "CommercialName", DataType.String, Nothing, False, 0, True, 100, "Business Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberCommercialEnquiryResult]  [A0SubscriberCommercialEnquiryResult]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberCommercialEnquiryResult")
    oList.Columns.Add(New ListColumn("CommercialID", "[A0SubscriberCommercialEnquiryResult].[CommercialID]", "CommercialID", DataType.Integer, Nothing, True, 0, False, 100, "Commercial ID"))
    oList.Columns.Add(New ListColumn("SubscriberCommercialEnquiryID", "[A0SubscriberCommercialEnquiryResult].[SubscriberCommercialEnquiryID]", "SubscriberCommercialEnquiryID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber Commercial Enquiry ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberCommercialEnquiryResult].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("RegistrationNo", "[A0SubscriberCommercialEnquiryResult].[RegistrationNo]", "RegistrationNo", DataType.String, Nothing, False, 0, True, 100, "Registration No"))
    oList.Columns.Add(New ListColumn("CommercialName", "[A0SubscriberCommercialEnquiryResult].[CommercialName]", "CommercialName", DataType.String, Nothing, False, 0, True, 100, "Business Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberCommercialEnquiryResult]  [A0SubscriberCommercialEnquiryResult]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
