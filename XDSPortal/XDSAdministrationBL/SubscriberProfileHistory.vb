Imports sembleWare.Runtime
Imports System
Public Class SubscriberProfileHistory 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly SubscriberProfileHistoryID As New IdentityElement("SubscriberProfileHistoryID", Me, True, Nothing, Nothing)
  Public ReadOnly ActionDesc As New StringElement("ActionDesc", Me, False, False, True, True, True, 250, "Action Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ActionedByUser As New StringElement("ActionedByUser", Me, False, False, True, True, True, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ActionedOnDate As New DateTimeElement("ActionedOnDate", Me, False, False, True, True, True, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly PreviousStatusInd As New IndicatorElement("PreviousStatusInd", Me, False, False, False, True, True, True, "Previous Status", Nothing, SubscriberProfileHistory.PreviousStatusIndOptions, Nothing)
  Public ReadOnly ChangedStatusInd As New IndicatorElement("ChangedStatusInd", Me, False, False, False, True, True, True, "Changed Status", Nothing, SubscriberProfileHistory.ChangedStatusIndOptions, Nothing)
  Public ReadOnly PreviousOverrideDefaultPointValueYN As New BooleanElement("PreviousOverrideDefaultPointValueYN", Me, False, False, True, True, False, Nothing, Nothing, Nothing, "Yes;No")
  Public ReadOnly PreviousOverridePointValue As New DecimalElement("PreviousOverridePointValue", Me, False, False, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ChangedOverrideDefaultPointValueYN As New BooleanElement("ChangedOverrideDefaultPointValueYN", Me, False, False, True, True, False, Nothing, Nothing, Nothing, "Yes;No")
  Public ReadOnly ChangedOverridePointValue As New DecimalElement("ChangedOverridePointValue", Me, False, False, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SubscriberProfile As Relationship = New SubscriberProfileRelationship("SubscriberProfile", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _SubscriberProfile As SubscriberProfileRelationship = SubscriberProfile
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moPreviousStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property PreviousStatusIndOptions() As IndicatorOptions
    Get
      If moPreviousStatusIndOptions Is Nothing Then
        moPreviousStatusIndOptions = New IndicatorOptions
        moPreviousStatusIndOptions.Add("A", "Allowed")
        moPreviousStatusIndOptions.Add("D", "Denied")
      End If
      Return moPreviousStatusIndOptions
    End Get
  End Property
  Private Shared moChangedStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property ChangedStatusIndOptions() As IndicatorOptions
    Get
      If moChangedStatusIndOptions Is Nothing Then
        moChangedStatusIndOptions = New IndicatorOptions
        moChangedStatusIndOptions.Add("A", "Allowed")
        moChangedStatusIndOptions.Add("D", "Denied")
      End If
      Return moChangedStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberProfileHistory", ApplicationSettings)
    _SubscriberProfile._SubscriberID.LocalElement = _SubscriberID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class SubscriberProfileHistoryRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberProfileHistoryID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberProfileHistory(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberProfileHistoryID.ForeignElement = CType(Instance, SubscriberProfileHistory).SubscriberProfileHistoryID
    _SubscriberID.ForeignElement = CType(Instance, SubscriberProfileHistory)._SubscriberID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
