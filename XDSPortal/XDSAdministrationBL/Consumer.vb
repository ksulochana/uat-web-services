Imports sembleWare.Runtime
Imports System
Public Class Consumer 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly ConsumerID As New IdentityElement("ConsumerID", Me, True, Nothing, Nothing)
  Public ReadOnly FirstInitial As New StringElement("FirstInitial", Me, False, False, True, True, False, 1, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SecondInitial As New StringElement("SecondInitial", Me, False, False, True, True, False, 1, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FirstName As New StringElement("FirstName", Me, False, False, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SecondName As New StringElement("SecondName", Me, False, False, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ThirdName As New StringElement("ThirdName", Me, False, False, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Surname As New StringElement("Surname", Me, False, False, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly IDNo As New StringElement("IDNo", Me, False, False, True, True, False, 13, "ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PassportNo As New StringElement("PassportNo", Me, False, False, True, True, False, 16, "Passport No / Other ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly BirthDate As New DateTimeElement("BirthDate", Me, False, False, True, True, False, "Date of Birth", Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly MaidenName As New StringElement("MaidenName", Me, False, False, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly GenderInd As New IndicatorElement("GenderInd", Me, False, False, False, True, True, False, "Gender", Nothing, Consumer.GenderIndOptions, Nothing)
  Public ReadOnly IsPossibleNameConflictYN As New BooleanElement("IsPossibleNameConflictYN", Me, False, False, True, True, True, "Is Possible Name Conflict?", Nothing, False, "Yes;No")
  Public ReadOnly IsPossibleDuplicateRecordYN As New BooleanElement("IsPossibleDuplicateRecordYN", Me, False, False, True, True, True, "Is Possible Duplicate Record?", Nothing, False, "Yes;No")
  Public ReadOnly RecordStatusInd As New IndicatorElement("RecordStatusInd", Me, False, False, False, True, True, True, "Record Status", Nothing, Consumer.RecordStatusIndOptions, "A")
  Public ReadOnly ConsumerLabelDescription As New StringElement("ConsumerLabelDescription", Me, False, True, True, False, False, 250, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly DeletedReason As New StringElement("DeletedReason", Me, False, True, True, True, False, 500, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LastUpdatedDate As New DateTimeElement("LastUpdatedDate", Me, False, False, True, True, True, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly SubscriberName As New StringElement("SubscriberName", Me, False, True, True, False, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Title As Relationship = New TitleRelationship("Title", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly MaritalStatus As Relationship = New MaritalStatusRelationship("MaritalStatus", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly ConsumerAddress_OwnMany As Relationship = New ConsumerAddressRelationship("ConsumerAddress", Nothing, "Consumer", Me)
  Public ReadOnly ConsumerTelephone_OwnMany As Relationship = New ConsumerTelephoneRelationship("ConsumerTelephone", Nothing, "Consumer", Me)
  Public ReadOnly ConsumerEmployment_OwnMany As Relationship = New ConsumerEmploymentRelationship("ConsumerEmployment", Nothing, "Consumer", Me)
  Public ReadOnly ConsumerAccount_OwnMany As Relationship = New ConsumerAccountRelationship("ConsumerAccount", Nothing, "Consumer", Me)
  Public ReadOnly ConsumerAccountHistory_OwnMany As Relationship = New ConsumerAccountHistoryRelationship("ConsumerAccountHistory", Nothing, "Consumer", Me)
  Public ReadOnly ConsumerName_OwnMany As Relationship = New ConsumerNameRelationship("ConsumerName", Nothing, "Consumer", Me)
  Public ReadOnly ConsumerPrioritizationRating_OwnMany As Relationship = New ConsumerPrioritizationRatingRelationship("ConsumerPrioritizationRating", Nothing, "Consumer", Me)
  Public ReadOnly ConsumerHistory_OwnMany As Relationship = New ConsumerHistoryRelationship("ConsumerHistory", Nothing, "Consumer", Me)
  Public ReadOnly ConsumerJudgement_OwnMany As Relationship = New ConsumerJudgementRelationship("ConsumerJudgement", Nothing, "Consumer", Me)
  Public ReadOnly ConsumerNameSoundEx_OwnMany As Relationship = New ConsumerNameSoundExRelationship("ConsumerNameSoundEx", Nothing, "Consumer", Me)
  Public ReadOnly ConsumerConflict_OwnMany As Relationship = New ConsumerConflictRelationship("ConsumerConflict", Nothing, "Consumer", Me)
  Public ReadOnly ConsumerDebtReview_OwnMany As Relationship = New ConsumerDebtReviewRelationship("ConsumerDebtReview", Nothing, "Consumer", Me)
  Public ReadOnly Comments_OwnMany As Relationship = New CommentsRelationship("Comments", Nothing, "Owner", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _TitleCode As New StringElement("TitleCode", Me, False)
  Public ReadOnly _MaritalStatusCode As New StringElement("MaritalStatusCode", Me, False)
  Public ReadOnly _Title As TitleRelationship = Title
  Public ReadOnly _MaritalStatus As MaritalStatusRelationship = MaritalStatus
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  Public ReadOnly _ConsumerAddress_OwnMany As ConsumerAddressRelationship = ConsumerAddress_OwnMany
  Public ReadOnly _ConsumerTelephone_OwnMany As ConsumerTelephoneRelationship = ConsumerTelephone_OwnMany
  Public ReadOnly _ConsumerEmployment_OwnMany As ConsumerEmploymentRelationship = ConsumerEmployment_OwnMany
  Public ReadOnly _ConsumerAccount_OwnMany As ConsumerAccountRelationship = ConsumerAccount_OwnMany
  Public ReadOnly _ConsumerAccountHistory_OwnMany As ConsumerAccountHistoryRelationship = ConsumerAccountHistory_OwnMany
  Public ReadOnly _ConsumerName_OwnMany As ConsumerNameRelationship = ConsumerName_OwnMany
  Public ReadOnly _ConsumerPrioritizationRating_OwnMany As ConsumerPrioritizationRatingRelationship = ConsumerPrioritizationRating_OwnMany
  Public ReadOnly _ConsumerHistory_OwnMany As ConsumerHistoryRelationship = ConsumerHistory_OwnMany
  Public ReadOnly _ConsumerJudgement_OwnMany As ConsumerJudgementRelationship = ConsumerJudgement_OwnMany
  Public ReadOnly _ConsumerNameSoundEx_OwnMany As ConsumerNameSoundExRelationship = ConsumerNameSoundEx_OwnMany
  Public ReadOnly _ConsumerConflict_OwnMany As ConsumerConflictRelationship = ConsumerConflict_OwnMany
  Public ReadOnly _ConsumerDebtReview_OwnMany As ConsumerDebtReviewRelationship = ConsumerDebtReview_OwnMany
  Public ReadOnly _Comments_OwnMany As CommentsRelationship = Comments_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moGenderIndOptions As IndicatorOptions
  Public Shared ReadOnly Property GenderIndOptions() As IndicatorOptions
    Get
      If moGenderIndOptions Is Nothing Then
        moGenderIndOptions = New IndicatorOptions
        moGenderIndOptions.Add("M", "Male")
        moGenderIndOptions.Add("F", "Female")
      End If
      Return moGenderIndOptions
    End Get
  End Property
  Private Shared moRecordStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property RecordStatusIndOptions() As IndicatorOptions
    Get
      If moRecordStatusIndOptions Is Nothing Then
        moRecordStatusIndOptions = New IndicatorOptions
        moRecordStatusIndOptions.Add("A", "Active")
        moRecordStatusIndOptions.Add("D", "Deleted")
      End If
      Return moRecordStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("Consumer", ApplicationSettings)
    _Title._TitleCode.LocalElement = _TitleCode
    _MaritalStatus._MaritalStatusCode.LocalElement = _MaritalStatusCode
    'sembleWare: Constructor End - Do Not Modify
    Me.DeleteType = CustomDBPart.enumDeleteType.ChangeRecordStatus
  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"

#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreate()
  End Sub

  Public Overrides Sub Load()
    MyBase.Load()
    BRLoad()
  End Sub

  Public Overrides Sub Save()
    With ApplicationSettings
      Try
        .BeginTransaction()
        Dim bIsNewYN As Boolean = Me.IsNew
        BRBeforeSave(bIsNewYN)
        MyBase.Save()
        BRAfterSave(bIsNewYN)
        .CommitTransaction()
      Catch Exception As Exception
        .RollbackTransaction()
        Throw Exception
      End Try
    End With
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreate()
    Me.LastUpdatedDate.Value = System.DateTime.Now
    BRConsumerLabelDescription()
  End Sub

  Private Sub BRLoad()
    BRConsumerLabelDescription()
  End Sub

  Private Sub BRBeforeSave(ByVal IsNewYN As Boolean)
    Consumer.ValidateIDNo(Me.IDNo.Value, ApplicationSettings)
  End Sub

  Private Sub BRAfterSave(ByVal IsNewYN As Boolean)
    BRConsumerLabelDescription()
  End Sub

  Private Sub BRConsumerLabelDescription()
    If Me.IsNew Then
      Me.ConsumerLabelDescription.Value = "Consumer: (New)"
    Else
      Dim sDesc As String = Trim(Me._TitleCode.Value & " " & Me.FirstName.Value & " " & Me.Surname.Value)
      Me.ConsumerLabelDescription.Value = "Consumer: " & Me.ConsumerID.Value & " (" & sDesc & ")"
    End If
  End Sub
#End Region

#Region " Shared Procedures"
  Public Shared Sub ValidateIDNo(ByVal IDNo As String, ByVal ApplicationSettings As ApplicationSettings)
    If Not IDNo = "" Then
      ' The ID Number checksum does not cater for old ID Numbers, thus it we will only validate length and the numeric digits
      If IDNo.Length = 13 Then
        Return
        'Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand
        'Dim oDataRow

        'oCommand.CommandType = CommandType.Text
        'oCommand.CommandText = "select dbo.fnValidateIDNo(" & ApplicationSettings.QueryBuilder.ToSQL(IDNo) & ")"

        'For Each oDataRow In ApplicationSettings.ResultQuery(oCommand).Rows
        '  If oDataRow(0) = 1 Then
        '    Return
        '  End If
        'Next
      End If
      Throw New BusinessRuleException("The ID Number is invalid!")
    End If
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class ConsumerRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _ConsumerID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New Consumer(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _ConsumerID.ForeignElement = CType(Instance, Consumer).ConsumerID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0Consumer")
    oList.Columns.Add(New ListColumn("ConsumerID", "[A0Consumer].[ConsumerID]", "ConsumerID", DataType.Long, Nothing, True, 1, True, 100, "Consumer ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A0Consumer].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("PassportNo", "[A0Consumer].[PassportNo]", "PassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No / Other ID No"))
    oList.Columns.Add(New ListColumn("Surname", "[A0Consumer].[Surname]", "Surname", DataType.String, Nothing, False, 0, True, 200, "Surname"))
    oList.Columns.Add(New ListColumn("FirstName", "[A0Consumer].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 200, "First Name"))
    oList.Columns.Add(New ListColumn("FirstInitial", "[A0Consumer].[FirstInitial]", "FirstInitial", DataType.String, Nothing, False, 0, True, 100, "First Initial"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A0Consumer].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Date of Birth"))
    oList.Columns.Add(New ListColumn("GenderInd", "[A0Consumer].[GenderInd]", "GenderInd", DataType.String, Nothing, False, 0, True, 100, "Gender"))
    oList.SelectStatement = "select"
    oList.FromClause = "[Consumer]  [A0Consumer]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    oList.AndFilters.Add(New ListFilter("RecordStatusInd", "[A0Consumer].[RecordStatusInd]", Constants.RecordStatus.Active, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
    Return oList
  End Function


  Public Function DropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0Consumer")
    oList.Columns.Add(New ListColumn("ConsumerID", "[A0Consumer].[ConsumerID]", "ConsumerID", DataType.Long, Nothing, True, 1, True, 100, "Consumer ID"))
    oList.Columns.Add(New ListColumn("FirstName", "[A0Consumer].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 200, "First Name"))
    oList.Columns.Add(New ListColumn("Surname", "[A0Consumer].[Surname]", "Surname", DataType.String, Nothing, False, 0, True, 200, "Surname"))
    oList.SelectStatement = "select"
    oList.FromClause = "[Consumer]  [A0Consumer]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    oList.AndFilters.Add(New ListFilter("RecordStatusInd", "[A0Consumer].[RecordStatusInd]", Constants.RecordStatus.Active, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0Consumer")
    oList.Columns.Add(New ListColumn("ConsumerID", "[A0Consumer].[ConsumerID]", "ConsumerID", DataType.Long, Nothing, True, 1, True, 100, "Consumer ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A0Consumer].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("PassportNo", "[A0Consumer].[PassportNo]", "PassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No / Other ID No"))
    oList.Columns.Add(New ListColumn("FirstInitial", "[A0Consumer].[FirstInitial]", "FirstInitial", DataType.String, Nothing, False, 0, True, 100, "First Initial"))
    oList.Columns.Add(New ListColumn("FirstName", "[A0Consumer].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 200, "First Name"))
    oList.Columns.Add(New ListColumn("Surname", "[A0Consumer].[Surname]", "Surname", DataType.String, Nothing, False, 0, True, 200, "Surname"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A0Consumer].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Date of Birth"))
    oList.Columns.Add(New ListColumn("GenderInd", "[A0Consumer].[GenderInd]", "GenderInd", DataType.String, Nothing, False, 0, True, 100, "Gender"))
    oList.SelectStatement = "select"
    oList.FromClause = "[Consumer]  [A0Consumer]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    oList.AndFilters.Add(New ListFilter("RecordStatusInd", "[A0Consumer].[RecordStatusInd]", Constants.RecordStatus.Active, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
    Return oList
  End Function





  Public Function ConflictGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0Consumer")
    oList.Columns.Add(New ListColumn("ConsumerID", "[A0Consumer].[ConsumerID]", "ConsumerID", DataType.Long, Nothing, True, 1, True, 100, "Consumer ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A0Consumer].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("PassportNo", "[A0Consumer].[PassportNo]", "PassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No / Other ID No"))
    oList.Columns.Add(New ListColumn("FirstName", "[A0Consumer].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 200, "First Name"))
    oList.Columns.Add(New ListColumn("Surname", "[A0Consumer].[Surname]", "Surname", DataType.String, Nothing, False, 0, True, 200, "Surname"))
    oList.Columns.Add(New ListColumn("FirstInitial", "[A0Consumer].[FirstInitial]", "FirstInitial", DataType.String, Nothing, False, 0, True, 100, "First Initial"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A0Consumer].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Date of Birth"))
    oList.Columns.Add(New ListColumn("GenderInd", "[A0Consumer].[GenderInd]", "GenderInd", DataType.String, Nothing, False, 0, True, 100, "Gender"))
    oList.Columns.Add(New ListColumn("IsPossibleNameConflictYN", "[A0Consumer].[IsPossibleNameConflictYN]", "IsPossibleNameConflictYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Is Possible Name Conflict?"))
    oList.Columns.Add(New ListColumn("IsPossibleDuplicateRecordYN", "[A0Consumer].[IsPossibleDuplicateRecordYN]", "IsPossibleDuplicateRecordYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Is Possible Duplicate Record?"))
    oList.SelectStatement = "select"
    oList.FromClause = "[Consumer]  [A0Consumer]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ConflictForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0Consumer")
    oList.Columns.Add(New ListColumn("ConsumerID", "[A0Consumer].[ConsumerID]", "ConsumerID", DataType.Long, Nothing, True, 1, True, 100, "Consumer ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A0Consumer].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("PassportNo", "[A0Consumer].[PassportNo]", "PassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No / Other ID No"))
    oList.Columns.Add(New ListColumn("FirstName", "[A0Consumer].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 200, "First Name"))
    oList.Columns.Add(New ListColumn("FirstInitial", "[A0Consumer].[FirstInitial]", "FirstInitial", DataType.String, Nothing, False, 0, True, 100, "First Initial"))
    oList.Columns.Add(New ListColumn("Surname", "[A0Consumer].[Surname]", "Surname", DataType.String, Nothing, False, 0, True, 200, "Surname"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A0Consumer].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Date of Birth"))
    oList.Columns.Add(New ListColumn("GenderInd", "[A0Consumer].[GenderInd]", "GenderInd", DataType.String, Nothing, False, 0, True, 100, "Gender"))
    oList.Columns.Add(New ListColumn("SubscriberName", "(select top 1 s.SubscriberName from ConsumerName cn inner join Subscriber s on cn.SubscriberID = s.SubscriberID where cn.ConsumerID = A0Consumer.ConsumerID order by cn.CreatedOnDate desc)", "SubscriberName", DataType.String, Nothing, False, 0, True, 200, "Subscriber Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "[Consumer]  [A0Consumer]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
