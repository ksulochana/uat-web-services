Imports sembleWare.Runtime
Imports System
Public Class vwDeploymentFileFormatMetaGroupField 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly FileFormatMetaGroupID As New IntegerElement("FileFormatMetaGroupID", Me, True, True, True, True, True, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FileFormatFieldDesc As New StringElement("FileFormatFieldDesc", Me, True, True, True, True, True, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly DatabaseName As New StringElement("DatabaseName", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly MetaGroupSequenceNum As New IntegerElement("MetaGroupSequenceNum", Me, False, True, True, True, False, "Sequence Number", Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FileFormatFieldSequenceNum As New IntegerElement("FileFormatFieldSequenceNum", Me, False, True, True, True, False, "File Format Field Sequence Number", Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly MetaGroupFieldTypeInd As New StringElement("MetaGroupFieldTypeInd", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AssignedValue As New StringElement("AssignedValue", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly MetaFieldDataTypeInd As New StringElement("MetaFieldDataTypeInd", Me, False, True, True, True, False, 1, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LoaderDataUpdateUseForInsertYN As New BooleanElement("LoaderDataUpdateUseForInsertYN", Me, False, True, True, True, False, "Loader Data Update Use For Insert?", Nothing, Nothing)
  Public ReadOnly LoaderDataUpdateUseForUpdateYN As New BooleanElement("LoaderDataUpdateUseForUpdateYN", Me, False, True, True, True, False, "Loader Data Update Use For Update?", Nothing, Nothing)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly MetaDatabase As Relationship = New MetaDatabaseRelationship("MetaDatabase", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly MetaGroupField As Relationship = New MetaGroupFieldRelationship("MetaGroupField", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly FileFormatField As Relationship = New FileFormatFieldRelationship("FileFormatField", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _MetaDatabaseCode As New StringElement("MetaDatabaseCode", Me, True)
  Public ReadOnly _MetaFieldName As New StringElement("MetaFieldName", Me, True)
  Public ReadOnly _MetaTableName As New StringElement("MetaTableName", Me, True)
  Public ReadOnly _MetaGroupCode As New StringElement("MetaGroupCode", Me, True)
  Public ReadOnly _FileFormatFieldID As New IntegerElement("FileFormatFieldID", Me, True)
  Public ReadOnly _FileFormatCode As New StringElement("FileFormatCode", Me, True)
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  Public ReadOnly _MetaDatabase As MetaDatabaseRelationship = MetaDatabase
  Public ReadOnly _MetaGroupField As MetaGroupFieldRelationship = MetaGroupField
  Public ReadOnly _FileFormatField As FileFormatFieldRelationship = FileFormatField
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("vwDeploymentFileFormatMetaGroupField", ApplicationSettings)
    _MetaDatabase._MetaDatabaseCode.LocalElement = _MetaDatabaseCode
    _MetaGroupField._MetaFieldName.LocalElement = _MetaFieldName
    _MetaGroupField._MetaTableName.LocalElement = _MetaTableName
    _MetaGroupField._MetaDatabaseCode.LocalElement = _MetaDatabaseCode
    _MetaGroupField._MetaGroupCode.LocalElement = _MetaGroupCode
    _FileFormatField._FileFormatFieldID.LocalElement = _FileFormatFieldID
    _FileFormatField._FileFormatCode.LocalElement = _FileFormatCode
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class vwDeploymentFileFormatMetaGroupFieldRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _FileFormatMetaGroupID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _FileFormatFieldDesc As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _MetaDatabaseCode As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _MetaFieldName As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _MetaTableName As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _MetaGroupCode As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _FileFormatFieldID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _FileFormatCode As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New vwDeploymentFileFormatMetaGroupField(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _FileFormatMetaGroupID.ForeignElement = CType(Instance, vwDeploymentFileFormatMetaGroupField).FileFormatMetaGroupID
    _FileFormatFieldDesc.ForeignElement = CType(Instance, vwDeploymentFileFormatMetaGroupField).FileFormatFieldDesc
    _MetaDatabaseCode.ForeignElement = CType(Instance, vwDeploymentFileFormatMetaGroupField)._MetaDatabaseCode
    _MetaFieldName.ForeignElement = CType(Instance, vwDeploymentFileFormatMetaGroupField)._MetaFieldName
    _MetaTableName.ForeignElement = CType(Instance, vwDeploymentFileFormatMetaGroupField)._MetaTableName
    _MetaGroupCode.ForeignElement = CType(Instance, vwDeploymentFileFormatMetaGroupField)._MetaGroupCode
    _FileFormatFieldID.ForeignElement = CType(Instance, vwDeploymentFileFormatMetaGroupField)._FileFormatFieldID
    _FileFormatCode.ForeignElement = CType(Instance, vwDeploymentFileFormatMetaGroupField)._FileFormatCode
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0vwDeploymentFileFormatMetaGroupField")
    oList.Columns.Add(New ListColumn("MetaDatabaseCode", "[A0vwDeploymentFileFormatMetaGroupField].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, True, 0, True, 100, "Meta Database Code"))
    oList.Columns.Add(New ListColumn("DatabaseName", "[A0vwDeploymentFileFormatMetaGroupField].[DatabaseName]", "DatabaseName", DataType.String, Nothing, False, 3, True, 100, "Database Name"))
    oList.Columns.Add(New ListColumn("MetaFieldName", "[A0vwDeploymentFileFormatMetaGroupField].[MetaFieldName]", "MetaFieldName", DataType.String, Nothing, True, 0, True, 100, "Meta Field Name"))
    oList.Columns.Add(New ListColumn("MetaGroupCode", "[A0vwDeploymentFileFormatMetaGroupField].[MetaGroupCode]", "MetaGroupCode", DataType.String, Nothing, True, 0, True, 100, "Meta Group Code"))
    oList.Columns.Add(New ListColumn("MetaTableName", "[A0vwDeploymentFileFormatMetaGroupField].[MetaTableName]", "MetaTableName", DataType.String, Nothing, True, 0, True, 100, "Meta Table Name"))
    oList.Columns.Add(New ListColumn("FileFormatFieldID", "[A0vwDeploymentFileFormatMetaGroupField].[FileFormatFieldID]", "FileFormatFieldID", DataType.Integer, Nothing, True, 0, True, 100, "File Format Field ID"))
    oList.Columns.Add(New ListColumn("FileFormatCode", "[A0vwDeploymentFileFormatMetaGroupField].[FileFormatCode]", "FileFormatCode", DataType.String, Nothing, True, 0, True, 100, "File Format Code"))
    oList.Columns.Add(New ListColumn("FileFormatMetaGroupID", "[A0vwDeploymentFileFormatMetaGroupField].[FileFormatMetaGroupID]", "FileFormatMetaGroupID", DataType.Integer, Nothing, True, 0, True, 100, "File Format Meta Group ID"))
    oList.Columns.Add(New ListColumn("FileFormatFieldDesc", "[A0vwDeploymentFileFormatMetaGroupField].[FileFormatFieldDesc]", "FileFormatFieldDesc", DataType.String, Nothing, True, 0, True, 100, "File Format Field Desc"))
    oList.Columns.Add(New ListColumn("MetaGroupSequenceNum", "[A0vwDeploymentFileFormatMetaGroupField].[MetaGroupSequenceNum]", "MetaGroupSequenceNum", DataType.Integer, Nothing, False, 1, True, 100, "Sequence Number"))
    oList.Columns.Add(New ListColumn("FileFormatFieldSequenceNum", "[A0vwDeploymentFileFormatMetaGroupField].[FileFormatFieldSequenceNum]", "FileFormatFieldSequenceNum", DataType.Integer, Nothing, False, 2, True, 100, "File Format Field Sequence Number"))
    oList.Columns.Add(New ListColumn("AssignedValue", "[A0vwDeploymentFileFormatMetaGroupField].[AssignedValue]", "AssignedValue", DataType.String, Nothing, False, 0, True, 100, "Assigned Value"))
    oList.Columns.Add(New ListColumn("MetaGroupFieldTypeInd", "[A0vwDeploymentFileFormatMetaGroupField].[MetaGroupFieldTypeInd]", "MetaGroupFieldTypeInd", DataType.String, Nothing, False, 0, True, 100, "Meta Group Field Type Ind"))
    oList.Columns.Add(New ListColumn("MetaFieldDataTypeInd", "[A0vwDeploymentFileFormatMetaGroupField].[MetaFieldDataTypeInd]", "MetaFieldDataTypeInd", DataType.String, Nothing, False, 0, True, 100, "Meta Field Data Type Ind"))
    oList.SelectStatement = "select"
    oList.FromClause = "[vwDeploymentFileFormatMetaGroupField]  [A0vwDeploymentFileFormatMetaGroupField]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
