Imports sembleWare.Runtime
Imports System
Public Class ProductAuthenticationQuestionGroup 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly ProductAuthenticationQuestionGroupCode As New StringElement("ProductAuthenticationQuestionGroupCode", Me, True, True, True, True, True, 10, "Group Code", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ProductAuthenticationQuestionGroupDesc As New StringElement("ProductAuthenticationQuestionGroupDesc", Me, False, True, True, True, True, 50, "Group Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("ProductAuthenticationQuestionGroup", ApplicationSettings)
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class ProductAuthenticationQuestionGroupRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _ProductAuthenticationQuestionGroupCode As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New ProductAuthenticationQuestionGroup(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _ProductAuthenticationQuestionGroupCode.ForeignElement = CType(Instance, ProductAuthenticationQuestionGroup).ProductAuthenticationQuestionGroupCode
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0ProductAuthenticationQuestionGroup")
    oList.Columns.Add(New ListColumn("ProductAuthenticationQuestionGroupCode", "[A0ProductAuthenticationQuestionGroup].[ProductAuthenticationQuestionGroupCode]", "ProductAuthenticationQuestionGroupCode", DataType.String, Nothing, True, 0, False, 100, "Group Code"))
    oList.Columns.Add(New ListColumn("ProductAuthenticationQuestionGroupDesc", "[A0ProductAuthenticationQuestionGroup].[ProductAuthenticationQuestionGroupDesc]", "ProductAuthenticationQuestionGroupDesc", DataType.String, Nothing, False, 1, True, 200, "Group Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "[ProductAuthenticationQuestionGroup]  [A0ProductAuthenticationQuestionGroup]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function DropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0ProductAuthenticationQuestionGroup")
    oList.Columns.Add(New ListColumn("ProductAuthenticationQuestionGroupCode", "[A0ProductAuthenticationQuestionGroup].[ProductAuthenticationQuestionGroupCode]", "ProductAuthenticationQuestionGroupCode", DataType.String, Nothing, True, 1, True, 100, "Group Code"))
    oList.Columns.Add(New ListColumn("ProductAuthenticationQuestionGroupDesc", "[A0ProductAuthenticationQuestionGroup].[ProductAuthenticationQuestionGroupDesc]", "ProductAuthenticationQuestionGroupDesc", DataType.String, Nothing, False, 0, True, 200, "Group Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "[ProductAuthenticationQuestionGroup]  [A0ProductAuthenticationQuestionGroup]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
