Imports sembleWare.Runtime
Imports System
Public Class SPLoaderMatchingEngine 'sembleWare: Part
    Inherits CustomMemoryPart

#Region " Elements & Relationships"

    'sembleWare: Elements Start - Do Not Modify
    Public ReadOnly IDNo_SP As New StringElement("IDNo_SP", Me, False, True, True, True, False, 13, "ID No", Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly PassportNo_SP As New StringElement("PassportNo_SP", Me, False, True, True, True, False, 16, "Passport No / Other ID No", Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly Surname_SP As New StringElement("Surname_SP", Me, False, True, True, True, False, 100, "Surname", Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly MaidenName_SP As New StringElement("MaidenName_SP", Me, False, True, True, True, False, 100, "Maiden Name", Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly FirstName_SP As New StringElement("FirstName_SP", Me, False, True, True, True, False, 100, "First Name", Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly SecondName_SP As New StringElement("SecondName_SP", Me, False, True, True, True, False, 100, "Second Name", Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly FirstInitial_SP As New StringElement("FirstInitial_SP", Me, False, True, True, True, False, 1, "First Initial", Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly SecondInitial_SP As New StringElement("SecondInitial_SP", Me, False, True, True, True, False, 1, "Second Initial", Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly BirthDate_SP As New DateTimeElement("BirthDate_SP", Me, False, True, True, True, False, "Date of Birth", Nothing, "yyyy-MM-dd", Nothing, Nothing, Nothing)
    Public ReadOnly AccountNo_SP As New StringElement("AccountNo_SP", Me, False, True, True, True, False, 16, "Account No", Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly SubAccountNo_SP As New StringElement("SubAccountNo_SP", Me, False, True, True, True, False, 4, "Sub Account No", Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly RegistrationNoPart1_SP As New StringElement("RegistrationNoPart1_SP", Me, False, True, True, True, False, 4, "Registration No Part 1", Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly RegistrationNoPart2_SP As New StringElement("RegistrationNoPart2_SP", Me, False, True, True, True, False, 6, "Registration No Part 2", Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly RegistrationNoPart3_SP As New StringElement("RegistrationNoPart3_SP", Me, False, True, True, True, False, 2, "Registration No Part 3", Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly RegistrationNo_SP As New StringElement("RegistrationNo_SP", Me, False, True, True, True, False, 14, "Registration No", Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly CommercialName_SP As New StringElement("CommercialName_SP", Me, False, True, True, True, False, 150, "Commercial Name", Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly AuditorName_SP As New StringElement("AuditorName_SP", Me, False, True, True, True, False, 150, "Auditor Name", Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly ProfessionNo_SP As New StringElement("ProfessionNo_SP", Me, False, True, True, True, False, 10, "Profession No", Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly TitleDeedNo_SP As New StringElement("TitleDeedNo_SP", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly ConsumerIDText As New TextElement("ConsumerIDText", Me, True, True, True, False, Nothing, Nothing, "0")
    Public ReadOnly HomeAffairsIDText As New TextElement("HomeAffairsIDText", Me, True, True, True, False, Nothing, Nothing, Nothing)
    Public ReadOnly CommercialIDText As New TextElement("CommercialIDText", Me, True, True, True, False, Nothing, Nothing, Nothing)
    Public ReadOnly AuditorIDText As New TextElement("AuditorIDText", Me, True, True, True, False, "Auditor ID Text", Nothing, Nothing)
    Public ReadOnly DirectorIDText As New TextElement("DirectorIDText", Me, True, True, True, False, "Director ID Text", Nothing, Nothing)
    Public ReadOnly PropertyDeedIDText As New TextElement("PropertyDeedIDText", Me, True, True, True, False, Nothing, Nothing, Nothing)
    Public ReadOnly SAFPSSubjectIDText As New TextElement("SAFPSSubjectIDText", Me, True, True, True, False, Nothing, Nothing, Nothing)
    Public ReadOnly SAFPSExternalSourceID_SP As New IntegerElement("SAFPSExternalSourceID_SP", Me, False, True, True, True, False, Nothing, "ExternalSourceID_SP", Nothing, Nothing, Nothing, Nothing, 50)
    Public ReadOnly SAFPSPRReferenceNo_SP As New StringElement("SAFPSPRReferenceNo_SP", Me, False, True, True, True, False, 10, Nothing, Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly SAFPSPRReferenceText As New StringElement("SAFPSPRReferenceText", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly SIC_SP As Relationship = New SICRelationship("SIC_SP", "SIC", RelationshipType.Include, False, True, True, Me)
    Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
    'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

    'sembleWare: Foreign Items Start - Do Not Modify
    Public ReadOnly _SICCode As New StringElement("SICCode", Me, False)
    Public ReadOnly _SIC_SP As SICRelationship = SIC_SP
    Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
    'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

    'sembleWare: Indicator Options Start - Do Not Modify
    'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

    Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

        'sembleWare: Constructor Start - Do Not Modify
        MyBase.New("SPLoaderMatchingEngine", ApplicationSettings)
        _SIC_SP._SICCode.LocalElement = _SICCode
        'sembleWare: Constructor End - Do Not Modify

    End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
    Public Sub ConsumerMatch()
        Consumer.ValidateIDNo(Me.IDNo_SP.Value, ApplicationSettings)

        Dim oDataTable As DataTable
        Dim oDataRow As DataRow
        Dim oString As Text.StringBuilder = New Text.StringBuilder
        Dim nLoaderMatchingEngineID As Integer = LoaderMatchingEngine.ConsumerMatch(Me.IDNo_SP, Me.PassportNo_SP, Me.Surname_SP, Me.MaidenName_SP, Me.FirstInitial_SP, Me.SecondInitial_SP, Me.BirthDate_SP, Me.AccountNo_SP, Me.SubAccountNo_SP, ApplicationSettings)

        oString.Append("select ConsumerID" & vbNewLine)
        oString.Append("  from LoaderMatchingEngineConsumer" & vbNewLine)
        oString.Append(" where LoaderMatchingEngineID = " & ApplicationSettings.QueryBuilder.ToSQL(nLoaderMatchingEngineID))
        oDataRow = ApplicationSettings.ResultQuery(oString.ToString()).Rows(0)
        If SHBS.General.ZeroIfDBNull(oDataRow(0)) = 0 Then
            oString = New Text.StringBuilder

            oString.Append("select ecr.ConsumerID")
            oString.Append("  from LoaderMatchingEngineConsumerResult ecr inner join LoaderMatchingEngineConsumer mec")
            oString.Append("                                                 on ecr.LoaderMatchingEngineID = mec.LoaderMatchingEngineID")
            oString.Append("                                                and ecr.RecordID = mec.RecordID")
            oString.Append(" where ecr.LoaderMatchingEngineID = " & ApplicationSettings.QueryBuilder.ToSQL(nLoaderMatchingEngineID))
            oString.Append("   and mec.ConsumerID = 0")
            oString.Append("   and (mec.IsPossibleNameConflictYN = 1 or mec.IsPossibleDuplicateRecordYN = 1)")

            Me.ConsumerIDText.Value = "0"
            For Each oDataRow In ApplicationSettings.ResultQuery(oString.ToString()).Rows
                Me.ConsumerIDText.Value = Me.ConsumerIDText.Value & ", " & oDataRow(0)
            Next
        Else
            Me.ConsumerIDText.Value = oDataRow(0)
        End If
    End Sub

    Public Sub HomeAffairsMatch()
        Consumer.ValidateIDNo(Me.IDNo_SP.Value, ApplicationSettings)

        Dim oDataTable As DataTable
        Dim oDataRow As DataRow
        Dim oString As Text.StringBuilder = New Text.StringBuilder
        Dim nLoaderMatchingEngineID As Integer = LoaderMatchingEngine.HomeAffairsMatch(Me.IDNo_SP, Me.Surname_SP, Me.FirstName_SP, Me.SecondName_SP, Me.BirthDate_SP, ApplicationSettings)

        oString.Append("select HomeAffairsID" & vbNewLine)
        oString.Append("  from LoaderMatchingEngineHomeAffairs" & vbNewLine)
        oString.Append(" where LoaderMatchingEngineID = " & ApplicationSettings.QueryBuilder.ToSQL(nLoaderMatchingEngineID))
        oDataRow = ApplicationSettings.ResultQuery(oString.ToString()).Rows(0)
        If SHBS.General.ZeroIfDBNull(oDataRow(0)) = 0 Then
            oString = New Text.StringBuilder
            oString.Append("select ecr.HomeAffairsID")
            oString.Append("  from LoaderMatchingEngineHomeAffairsResult ecr inner join LoaderMatchingEngineHomeAffairs mec")
            oString.Append("                                                 on ecr.LoaderMatchingEngineID = mec.LoaderMatchingEngineID")
            oString.Append("                                                and ecr.RecordID = mec.RecordID")
            oString.Append(" where ecr.LoaderMatchingEngineID = " & ApplicationSettings.QueryBuilder.ToSQL(nLoaderMatchingEngineID))
            oString.Append("   and mec.HomeAffairsID = 0")
            oString.Append("   and (mec.IsPossibleNameConflictYN = 1 or mec.IsPossibleDuplicateRecordYN = 1)")

            Me.HomeAffairsIDText.Value = "0"
            For Each oDataRow In ApplicationSettings.ResultQuery(oString.ToString()).Rows
                Me.HomeAffairsIDText.Value = Me.HomeAffairsIDText.Value & ", " & oDataRow(0)
            Next
        Else
            Me.HomeAffairsIDText.Value = oDataRow(0)
        End If
    End Sub

    Public Sub CommercialMatch()
        Dim oDataTable As DataTable
        Dim oDataRow As DataRow
        Dim oString As Text.StringBuilder = New Text.StringBuilder
        Dim nLoaderMatchingEngineID As Integer = LoaderMatchingEngine.CommercialMatch(Me.RegistrationNo_SP, Me.CommercialName_SP, Me._SICCode, ApplicationSettings)

        oString.Append("select CommercialID" & vbNewLine)
        oString.Append("  from LoaderMatchingEngineCommercial" & vbNewLine)
        oString.Append(" where LoaderMatchingEngineID = " & ApplicationSettings.QueryBuilder.ToSQL(nLoaderMatchingEngineID))
        oDataRow = ApplicationSettings.ResultQuery(oString.ToString()).Rows(0)
        If SHBS.General.ZeroIfDBNull(oDataRow(0)) = 0 Then
            oString = New Text.StringBuilder
            oString.Append("select ecr.CommercialID")
            oString.Append("  from LoaderMatchingEngineCommercialResult ecr inner join LoaderMatchingEngineCommercial mec")
            oString.Append("                                                 on ecr.LoaderMatchingEngineID = mec.LoaderMatchingEngineID")
            oString.Append("                                                and ecr.RecordID = mec.RecordID")
            oString.Append(" where ecr.LoaderMatchingEngineID = " & ApplicationSettings.QueryBuilder.ToSQL(nLoaderMatchingEngineID))
            oString.Append("   and mec.CommercialID = 0")
            oString.Append("   and (mec.IsPossibleNameConflictYN = 1 or mec.IsPossibleDuplicateRecordYN = 1)")

            Me.CommercialIDText.Value = "0"
            For Each oDataRow In ApplicationSettings.ResultQuery(oString.ToString()).Rows
                Me.CommercialIDText.Value = Me.CommercialIDText.Value & ", " & oDataRow(0)
            Next
        Else
            Me.CommercialIDText.Value = oDataRow(0)
        End If
    End Sub

    Public Sub AuditorMatch()
        Dim oDataTable As DataTable
        Dim oDataRow As DataRow
        Dim oString As Text.StringBuilder = New Text.StringBuilder
        Dim nLoaderMatchingEngineID As Integer = LoaderMatchingEngine.AuditorMatch(Me.ProfessionNo_SP, Me.AuditorName_SP, ApplicationSettings)

        oString.Append("select AuditorID" & vbNewLine)
        oString.Append("  from LoaderMatchingEngineAuditor" & vbNewLine)
        oString.Append(" where LoaderMatchingEngineID = " & ApplicationSettings.QueryBuilder.ToSQL(nLoaderMatchingEngineID))
        oDataRow = ApplicationSettings.ResultQuery(oString.ToString()).Rows(0)
        If SHBS.General.ZeroIfDBNull(oDataRow(0)) = 0 Then
            oString = New Text.StringBuilder
            oString.Append("select ecr.AuditorID")
            oString.Append("  from LoaderMatchingEngineAuditorResult ecr inner join LoaderMatchingEngineAuditor mec")
            oString.Append("                                                 on ecr.LoaderMatchingEngineID = mec.LoaderMatchingEngineID")
            oString.Append("                                                and ecr.RecordID = mec.RecordID")
            oString.Append(" where ecr.LoaderMatchingEngineID = " & ApplicationSettings.QueryBuilder.ToSQL(nLoaderMatchingEngineID))
            oString.Append("   and mec.AuditorID = 0")
            oString.Append("   and (mec.IsPossibleNameConflictYN = 1 or mec.IsPossibleDuplicateRecordYN = 1)")

            Me.AuditorIDText.Value = "0"
            For Each oDataRow In ApplicationSettings.ResultQuery(oString.ToString()).Rows
                Me.AuditorIDText.Value = Me.AuditorIDText.Value & ", " & oDataRow(0)
            Next
        Else
            Me.AuditorIDText.Value = oDataRow(0)
        End If
    End Sub

    Public Sub DirectorMatch()
        Dim oDataTable As DataTable
        Dim oDataRow As DataRow
        Dim oString As Text.StringBuilder = New Text.StringBuilder
        Dim nLoaderMatchingEngineID As Integer = LoaderMatchingEngine.DirectorMatch(Me.IDNo_SP, Me.Surname_SP, Me.FirstInitial_SP, Me.SecondInitial_SP, Me.BirthDate_SP, ApplicationSettings)

        oString.Append("select DirectorID" & vbNewLine)
        oString.Append("  from LoaderMatchingEngineDirector" & vbNewLine)
        oString.Append(" where LoaderMatchingEngineID = " & ApplicationSettings.QueryBuilder.ToSQL(nLoaderMatchingEngineID))
        oDataRow = ApplicationSettings.ResultQuery(oString.ToString()).Rows(0)
        If SHBS.General.ZeroIfDBNull(oDataRow(0)) = 0 Then
            oString = New Text.StringBuilder
            oString.Append("select ecr.DirectorID")
            oString.Append("  from LoaderMatchingEngineDirectorResult ecr inner join LoaderMatchingEngineDirector mec")
            oString.Append("                                                 on ecr.LoaderMatchingEngineID = mec.LoaderMatchingEngineID")
            oString.Append("                                                and ecr.RecordID = mec.RecordID")
            oString.Append(" where ecr.LoaderMatchingEngineID = " & ApplicationSettings.QueryBuilder.ToSQL(nLoaderMatchingEngineID))
            oString.Append("   and mec.DirectorID = 0")
            oString.Append("   and (mec.IsPossibleNameConflictYN = 1 or mec.IsPossibleDuplicateRecordYN = 1)")

            Me.DirectorIDText.Value = "0"
            For Each oDataRow In ApplicationSettings.ResultQuery(oString.ToString()).Rows
                Me.DirectorIDText.Value = Me.DirectorIDText.Value & ", " & oDataRow(0)
            Next
        Else
            Me.DirectorIDText.Value = oDataRow(0)
        End If
    End Sub



    Public Sub SAFPSSubjectMatch()
        Dim oDataTable As DataTable
        Dim oDataRow As DataRow
        Dim oString As Text.StringBuilder = New Text.StringBuilder
        Dim nLoaderMatchingEngineID As Integer = LoaderMatchingEngine.SAFPSSubjectMatch(Me.SAFPSExternalSourceID_SP, ApplicationSettings)

        oString.Append("select SAFPSSubjectID" & vbNewLine)
        oString.Append("  from LoaderMatchingEngineSAFPSSubject" & vbNewLine)
        oString.Append(" where LoaderMatchingEngineID = " & ApplicationSettings.QueryBuilder.ToSQL(nLoaderMatchingEngineID))
        oDataRow = ApplicationSettings.ResultQuery(oString.ToString()).Rows(0)
        If SHBS.General.ZeroIfDBNull(oDataRow(0)) = 0 Then
            oString = New Text.StringBuilder

            oString.Append("select ecr.SAFPSSubjectID")
            oString.Append("  from LoaderMatchingEngineSAFPSSubjectResult ecr inner join LoaderMatchingEngineSAFPSSubject mec")
            oString.Append("                                                 on ecr.LoaderMatchingEngineID = mec.LoaderMatchingEngineID")
            oString.Append("                                                and ecr.RecordID = mec.RecordID")
            oString.Append(" where ecr.LoaderMatchingEngineID = " & ApplicationSettings.QueryBuilder.ToSQL(nLoaderMatchingEngineID))
            oString.Append("   and mec.SAFPSSubjectID = 0")
            oString.Append("   and (mec.IsPossibleNameConflictYN = 1 or mec.IsPossibleDuplicateRecordYN = 1)")

            Me.SAFPSSubjectIDText.Value = "0"
            For Each oDataRow In ApplicationSettings.ResultQuery(oString.ToString()).Rows
                Me.SAFPSSubjectIDText.Value = Me.SAFPSSubjectIDText.Value & ", " & oDataRow(0)
            Next
        Else
            Me.SAFPSSubjectIDText.Value = oDataRow(0)
        End If
    End Sub


    Public Sub SAFPSProtectiveRegistrationMatch()
        Dim oDataTable As DataTable
        Dim oDataRow As DataRow
        Dim oString As Text.StringBuilder = New Text.StringBuilder
        Dim nLoaderMatchingEngineID As Integer = LoaderMatchingEngine.SAFPSProtectiveRegistrationMatch(Me.SAFPSPRReferenceNo_SP, ApplicationSettings)

        oString.Append("select SAFPSProtectiveRegistrationID" & vbNewLine)
        oString.Append("  from LoaderMatchingEngineSAFPSProtectiveRegistration" & vbNewLine)
        oString.Append(" where LoaderMatchingEngineID = " & ApplicationSettings.QueryBuilder.ToSQL(nLoaderMatchingEngineID))
        oDataRow = ApplicationSettings.ResultQuery(oString.ToString()).Rows(0)
        If SHBS.General.ZeroIfDBNull(oDataRow(0)) = 0 Then
            oString = New Text.StringBuilder

            oString.Append("select ecr.SAFPSProtectiveRegistrationID")
            oString.Append("  from LoaderMatchingEngineSAFPSProtectiveRegistrationResult ecr inner join LoaderMatchingEngineSAFPSProtectiveRegistration mec")
            oString.Append("                                                 on ecr.LoaderMatchingEngineID = mec.LoaderMatchingEngineID")
            oString.Append("                                                and ecr.RecordID = mec.RecordID")
            oString.Append(" where ecr.LoaderMatchingEngineID = " & ApplicationSettings.QueryBuilder.ToSQL(nLoaderMatchingEngineID))
            oString.Append("   and mec.SAFPSProtectiveRegistrationID = 0")
            oString.Append("   and (mec.IsPossibleNameConflictYN = 1 or mec.IsPossibleDuplicateRecordYN = 1)")

            Me.SAFPSSubjectIDText.Value = "0"
            For Each oDataRow In ApplicationSettings.ResultQuery(oString.ToString()).Rows
                Me.SAFPSPRReferenceText.Value = Me.SAFPSPRReferenceText.Value & ", " & oDataRow(0)
            Next
        Else
            Me.SAFPSPRReferenceText.Value = oDataRow(0)
        End If
    End Sub

#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class SPLoaderMatchingEngineRelationship 'sembleWare: Relationship
    Inherits Relationship

#Region " Relationship Code"

    'sembleWare: Relationship Start - Do Not Modify

    Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
        MyBase.New(ApplicationSettings)
    End Sub

    Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
        MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
    End Sub

    Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
        MyBase.New(Name, Caption, OwnedByName, Owner)
    End Sub

    Public Overrides Function CreateObject() As sembleWare.Runtime.Part
        Return New SPLoaderMatchingEngine(ApplicationSettings)
    End Function

    Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    End Sub

    'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
