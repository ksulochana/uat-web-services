Imports sembleWare.Runtime
Imports System.Web


Public Class GlobalSettings




#Region " Class SystemUser"
    Public Class SystemUser
#Region " Shared Procedures"
        Public Shared LoggedInUserID As Long = 0
        Public Shared LoggedInUserName As String = ""
        Public Shared IsUserLoggedIn As Boolean = False

        Public Shared Function IsLoggedIn() As Boolean
            If IsUserLoggedIn = True Then
                Return True
            End If
            'If Not HttpContext.Current.Session(Constants.LoggedInSystemUserIDKey) Is Nothing Then
            '    Return True
            'End If
            Return False
        End Function

        Public Shared Sub SetLoggedInSystemUserID(ByVal SystemUserID As Long)
            LoggedInUserID = SystemUserID
            'If Not HttpContext.Current.Session Is Nothing Then
            '    HttpContext.Current.Session.Add(Constants.LoggedInSystemUserIDKey, SystemUserID)
            'Else
            '    HttpContext.Current.ApplicationInstance.Application.Add(Constants.LoggedInSystemUserIDKey, SystemUserID)
            'End If
        End Sub

        Public Shared Sub SetLoggedInSystemUsername(ByVal Username As String)
            LoggedInUserName = Username
            'If Not HttpContext.Current.Session Is Nothing Then
            '    HttpContext.Current.Session.Add(Constants.LoggedInSystemUsernameKey, Username)
            'Else
            '    HttpContext.Current.ApplicationInstance.Application.Add(Constants.LoggedInSystemUsernameKey, Username)
            'End If
        End Sub

        Public Shared ReadOnly Property LoggedInSystemUserID() As Integer
            Get
                'If HttpContext.Current Is Nothing Then
                '    Return 0
                'Else
                '    If Not HttpContext.Current.Session Is Nothing Then
                '        If Not HttpContext.Current.Session(Constants.LoggedInSystemUserIDKey) Is Nothing Then
                '            Return System.Convert.ToInt32(HttpContext.Current.Session(Constants.LoggedInSystemUserIDKey))
                '        End If
                '    Else
                '        Return System.Convert.ToInt32(HttpContext.Current.ApplicationInstance.Application(Constants.LoggedInSystemUserIDKey))
                '    End If
                'End If
                Return LoggedInUserID
            End Get
        End Property

        Public Shared ReadOnly Property LoggedInSystemUsername() As String
            Get
                'If HttpContext.Current Is Nothing Then
                '    Return Constants.XDSAdministrationServicesUser
                'Else
                '    If Not HttpContext.Current.Session Is Nothing Then
                '        If Not HttpContext.Current.Session(Constants.LoggedInSystemUsernameKey) Is Nothing Then
                '            Return HttpContext.Current.Session(Constants.LoggedInSystemUsernameKey.ToString())
                '        End If
                '    Else
                '        Return HttpContext.Current.ApplicationInstance.Application(Constants.LoggedInSystemUsernameKey)
                '    End If
                'End If
                Return LoggedInUserName
            End Get
        End Property

        Public Shared Property AdministrationMenuDisplay() As Boolean
            Get
                If Not HttpContext.Current.Session(Constants.AdministrationMenuDisplayKey) Is Nothing Then
                    Return System.Convert.ToBoolean(HttpContext.Current.Session(Constants.AdministrationMenuDisplayKey))
                End If
                Return True
            End Get
            Set(ByVal Value As Boolean)
                HttpContext.Current.Session.Add(Constants.AdministrationMenuDisplayKey, Value)
            End Set
        End Property
#End Region
    End Class
#End Region
End Class
