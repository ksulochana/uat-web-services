Public Class Constants
  Public Const XDSAdministrationServicesUser As String = "XDSAdministration Services"
  Public Const PasswordHashString As String = "XDS.CRB"
  Public Const AuthenticationHashString As String = "XDS.Auth.Reference"
  Public Const LoaderProcessMetaDatabaseKey As String = "LoaderProcessMetaDatabaseKey"
  Public Const ReportingServicesReportKey As String = "ReportingServicesReportKey"
  Public Const LoggedInSubscriberIDKey As String = "LoggedInSubscriberIDKey"
  Public Const LoggedInSubscriberGroupIDKey As String = "LoggedInSubscriberGroupIDKey"
  Public Const LoggedInSystemUserIDKey As String = "LoggedInSystemUserIDKey"
  Public Const LoggedInSystemUsernameKey As String = "LoggedInSystemUsernameKey"
  Public Const AdministrationMenuDisplayKey As String = "AdministrationMenuDisplayKey"
  Public Const Subscriber_IsNewSubscriberRegistrationKey As String = "Subscriber_IsNewSubscriberRegistrationKey"
  Public Const SubscriberTrace_IsNewTraceKey As String = "SubscriberTrace_IsNewTraceKey"
  Public Const SubscriberTrace_TraceMultipleKey As String = "SubscriberTrace_TraceMultipleKey"
  Public Const SubscriberTrace_IsNewTraceAlertKey As String = "SubscriberTrace_IsNewTraceAlertKey"
  Public Const SubscriberCommercial_IsNewEnquiryKey As String = "SubscriberCommercial_IsNewEnquiryKey"
  Public Const SubscriberCreditGrantor_IsNewEnquiryKey As String = "SubscriberCreditGrantor_IsNewEnquiryKey"
  Public Const SubscriberCreditGrantor_PrintReportKey As String = "SubscriberCreditGrantor_PrintReportKey"
  Public Const SubscriberAuthentication_IsNewAuthenticationKey As String = "SubscriberAuthentication_IsNewAuthenticationKey"
  Public Const SubscriberAuthentication_BlankAnswer As String = "SubscriberAuthentication_BlankAnswer"
  Public Const WebConfig_WebsiteUrlKey As String = "WebsiteUrl"
  Public Const WebConfig_ReportImageUrlKey As String = "ReportImageUrl"
  Public Const WebConfig_ReportImagePathKey As String = "ReportImagePath"
  Public Const WebConfig_ConsumerTraceUrlKey As String = "ConsumerTraceUrl"
  Public Const WebConfig_ConsumerCreditGrantorUrlKey As String = "ConsumerCreditGrantorUrl"
  Public Const WebConfig_ConsumerAuthenticationUrlKey As String = "ConsumerAuthenticationUrl"
  Public Const WebConfig_CommercialEnquiryUrlKey As String = "CommercialEnquiryUrl"
  Public Const ArraySplitKey As String = "_sw_"

#Region " Class ConsumerAddress"
  Public Class ConsumerAddress
    Public Class Types
      Public Const Residential As String = "R"
      Public Const Postal As String = "P"
      Public Const Work As String = "W"
    End Class
  End Class
#End Region

#Region "Class MaritalStatus"
  Public Class MaritalStatus
    Public Const Divorced = "DIV"
    Public Const ForeignMarriage = "MF"
    Public Const MarriedIn = "MI"
    Public Const MarriedOut = "MO"
    Public Const MuslimHindu = "MUSHIN"
    Public Const Single_Status = "SIN"
    Public Const Widow = "WID"
    Public Const Widower = "WIDR"
  End Class
#End Region
#Region " Class ConsumerTelephone"
  Public Class ConsumerTelephone
    Public Class Types
      Public Const Home As String = "H"
      Public Const Work As String = "W"
      Public Const Fax As String = "F"
      Public Const Cellular As String = "C"
      Public Const Email As String = "E"
    End Class
  End Class
#End Region

#Region " Class DataProcedure"
  Public Class DataProcedure
    Public Class Types
      Public Const Conversion As String = "CO"
      Public Const Deletion As String = "DE"
      Public Const RequiredValidation As String = "V"
      Public Const NonRequiredValidation As String = "NV"
      Public Const DataStatistic As String = "DS"
      Public Const Calculation As String = "CA"
      Public Const MatchingEvaluation As String = "ME"
    End Class

    Public Class ParameterTypes
      Public Const None As String = "N"
      Public Const DefaultToSelectedField As String = "S"
      Public Const MultipleSpecifiedFields As String = "M"
    End Class

    Public Class AppliesToTypes
      Public Const All As String = "A"
      Public Const MetaDatabase As String = "M"
    End Class
  End Class
#End Region

#Region " Class FileFormat"
  Public Class FileFormat
    Public Class Types
      Public Const Loader As String = "LOADER"
      Public Const Direct As String = "DIRECT"
      Public Const Batch As String = "BATCH"
    End Class

    Public Class FieldTypes
      Public Const FixedField As String = "F"
      Public Const Delimted As String = "D"
    End Class

    Public Class FileTypes
      Public Const TextFile As String = "T"
      Public Const XMLFile As String = "X"
    End Class

    Public Class DelimiterTypes
      Public Const Comma As String = "C"
      Public Const Semicolon As String = "S"
      Public Const Tab As String = "T"
      Public Const Other As String = "O"
    End Class

    Public Class RowDelimiterTypes
      Public Const CRLF As String = "CRLF"
      Public Const CR As String = "CR"
      Public Const LF As String = "LF"
      Public Const Semicolon As String = ";"
      Public Const Comma As String = ","
      Public Const Tab As String = vbTab
    End Class

    Public Class TextQualiferTypes
      Public Const DoubleQuote As String = "D"
      Public Const SingleQuote As String = "S"
      Public Const None As String = "N"
    End Class
  End Class
#End Region

#Region " Class FileFormatField"
  Public Class FileFormatField
    Public Class Types
      Public Const File As String = "F"
      Public Const Calculation As String = "C"
      Public Const Loader As String = "L"
      Public Const System As String = "S"
    End Class
  End Class
#End Region

#Region " Class FileFormatDeployment"
  Public Class FileFormatDeployment
    Public Class Statuses
      Public Const Processing As String = "P"
      Public Const Completed As String = "C"
      Public Const Failed As String = "F"
      Public Const Removed As String = "R"
    End Class
  End Class
#End Region

#Region " Class Loader"
  Public Class Loader
    Public Class Statuses
      Public Const InProcess As String = "I"
      Public Const Completed As String = "C"
    End Class
  End Class
#End Region

#Region " Class LoaderProcess"
  Public Class LoaderProcess
    Public Class Types
      Public Const DataLoad As String = "DATALOAD"
      Public Const DataValid As String = "DATAVALID"
      Public Const DataMatch As String = "DATAMATCH"
      Public Const DataUpdate As String = "DATAUPDATE"
      Public Const DataUndo As String = "DATAUNDO"
    End Class

    Public Class Statuses
      Public Const RequireSubmit As String = "R"
      Public Const Queued As String = "Q"
      Public Const Processing As String = "P"
      Public Const Completed As String = "C"
      Public Const Failed As String = "F"
    End Class
  End Class
#End Region

#Region " Class LoaderThread"
  Public Class LoaderThread
    Public Class Statuses
      Public Const Active As String = "A"
      Public Const Inactive As String = "I"
    End Class
  End Class
#End Region

#Region " Class MetaDatabase"
  Public Class MetaDatabase
    Public Class Types
      Public Const System As String = "S"
      Public Const Repository As String = "R"
      Public Const Enquiry As String = "E"
    End Class
  End Class
#End Region

#Region " Class MetaGroupField"
  Public Class MetaGroupField
    Public Class Types
      Public Const FileFormatField As String = "F"
      Public Const AssignedValue As String = "V"
    End Class
  End Class
#End Region

#Region " Class NotificationTemplate"
  Public Class NotificationTemplate
    Public Enum Records
      ForgottenPassword = 1
      SubscriberTraceAlert = 2
    End Enum
  End Class
#End Region

#Region " Class NotificationHistory"
  Public Class NotificationHistory
    Public Class Statuses
      Public Const Queued As String = "Q"
      Public Const Processing As String = "P"
      Public Const Sent As String = "S"
      Public Const Failed As String = "F"
    End Class
  End Class
#End Region

#Region " Class NotificationQueue"
  Public Class NotificationQueue
    Public Class SenderTypes
      Public Const SystemUser As String = "S"
      Public Const EmailAddress As String = "E"
    End Class

    Public Class Statuses
      Public Const Queued As String = "Q"
      Public Const Processing As String = "P"
      Public Const Completed As String = "C"
      Public Const Failed As String = "F"
    End Class
  End Class
#End Region

#Region " Class NotificationQueueRecipient"
  Public Class NotificationQueueRecipient
    Public Class Types
      Public Const SystemUser As String = "S"
      Public Const EmailAddress As String = "E"
    End Class

  End Class
#End Region

#Region " Class PrioritizationRating"
  Public Class PrioritizationRating
    Public Class DurationTypes
      Public Const Month As String = "M"
    End Class
  End Class
#End Region

#Region " Class PrioritizationRatingCriteria"
  Public Class PrioritizationRatingCriteria
    Public Class OperatorTypes
      Public Const LessThan As String = "L"
      Public Const EqualsTo As String = "E"
      Public Const GreaterThan As String = "G"
      Public Const Between As String = "B"
    End Class
  End Class
#End Region

#Region " Class Product"
  Public Class Product
    Public Enum Records
      AuthenticationConsumer = 1
      TraceConsumer = 2
      TraceConsumerAddress = 3
      TraceConsumerTelephone = 4
      TraceConsumerAccountNumber = 5
      TraceConsumerMultiple = 6
      TraceConsumerEasy = 7
      TraceConsumerPreScreen = 8
      TraceConsumerIdentityVerification = 9
      EnquiryCommercial = 10
      EnquiryCommercialBusinessName = 11
      EnquiryCommercialBusiness = 12
      EnquiryCommercialEasy = 13
      EnquiryCommercialDirector = 14
      CreditGrantorConsumerCreditEnquiry = 15
      CreditGrantorConsumerApplicationCreditEnquiry = 16
      CreditGrantorReEnquiry = 17
      CreditGrantorDebtCollectorsEnquiry = 18
      TraceConsumerWebService = 19
      TraceConsumerAddressWebService = 20
      TraceConsumerTelephoneWebService = 21
      TraceConsumerAccountNumberWebService = 22
      TraceConsumerEasyWebService = 23
      TraceConsumerIdentityVerificationWebService = 24
      EnquiryCommercialBusinessWebService = 25
      EnquiryCommercialDirectorWebService = 26
      CreditGrantorConsumerCreditEnquiryWebService = 27
      CreditGrantorConsumerApplicationCreditEnquiryWebService = 28
      CreditGrantorReEnquiryWebService = 29
      CreditGrantorDebtCollectorsEnquiryWebService = 30
      CreditGrantorCreditApplication = 31
    End Enum

    Public Class ReportDisplayTypes
      Public Const ExistingWindow As String = "E"
      Public Const NewWindow As String = "N"
    End Class
  End Class
#End Region

#Region " Class ProductAuthenticationQuestion"
  Public Class ProductAuthenticationQuestion
    Public Class AnswerTypes
      Public Const YesNo As String = "Y"
      Public Const RangeOfAnswers As String = "RA"
      Public Const RangeOfYears As String = "RDY"
      Public Const RangeOfAmounts1 As String = "RDA1"
      Public Const RangeOfAmounts2 As String = "RDA2"
      Public Const RangeOfAmounts3 As String = "RDA3"
    End Class

    Public Class RangerAnswerTypes
      Public Const NoneOfThese As String = "N"
      Public Const IDoNotHave As String = "I"
    End Class
  End Class
#End Region

#Region " Class ProductType"
  Public Class ProductType
    Public Enum Records
      ConsumerAuthentication = 1
      ConsumerTrace = 2
      CommercialEnquiry = 3
      ConsumerCreditGrantor = 4
    End Enum
  End Class
#End Region

#Region " Class ReportingServicesServer"
  Public Class ReportingServicesServer
    Public Class Records
      Public Const Administration As Integer = 1
      Public Const ConsumerTrace As Integer = 2
      Public Const ConsumerAuthentication As Integer = 3
      Public Const CommercialEnquiry As Integer = 4
      Public Const ConsumerCreditGrantor As Integer = 5
    End Class
  End Class
#End Region

#Region " Class ReportingServicesReport"
  Public Class ReportingServicesReport
    Public Class Statuses
      Public Const Active As String = "A"
      Public Const Inactive As String = "I"
    End Class

    Public Class ScreenTypes
      Public Const None As String = "N"
      Public Const Standard As String = "S"
      Public Const Custom As String = "C"
    End Class
  End Class
#End Region

#Region " Class Role"
  Public Class Role
    Public Class Records
      Public Const SystemAdministrators As String = "SYSADMIN"
      Public Const SubscriberAdministrators As String = "SUBSADMIN"
      Public Const SubscriberUsers As String = "SUBSUSERS"
    End Class
  End Class
#End Region

#Region " Class Subscriber"
  Public Class Subscriber
    Public Class Types
      Public Const DataSupplier As String = "D"
      Public Const Enquiries As String = "E"
      Public Const Both As String = "B"
    End Class
  End Class
#End Region

#Region " Class SubscriberAuthentication"
  Public Class SubscriberAuthentication
    Public Class AuthenticationTypes
      Public Const Primary As String = "P"
      Public Const Secondary As String = "S"
    End Class

    Public Class AuthenticationStatuses
      Public Const Pending As String = "P"
      Public Const Authenticated As String = "A"
      Public Const NotAuthenticated As String = "N"
      Public Const Voided As String = "V"
      Public Const NoConsumerFound As String = "NC"
    End Class
  End Class
#End Region

#Region " Class SubscriberCreditGrantorInfo"
  Public Class SubscriberCreditGrantorInfo
    Public Class ExpenseTypes
      Public Const PropertyExpense As String = "P"
      Public Const Vehicle As String = "V"
      Public Const CreditCard As String = "CC"
      Public Const CreditAccount As String = "CA"
    End Class
  End Class
#End Region

#Region " Class SubscriberCommercialEnquiry"
  Public Class SubscriberCommercialEnquiry
    Public Class Statuses
      Public Const Queued As String = "Q"
      Public Const Processing As String = "P"
      Public Const Completed As String = "C"
    End Class

    Public Class Results
      Public Const Pending As String = "Q"
      Public Const NoRecordFound As String = "N"
      Public Const RecordFound As String = "F"
      Public Const MultipleRecordsFound As String = "M"
      Public Const RecordSelected As String = "S"
    End Class
  End Class
#End Region

#Region " Class SubscriberConsumerEnquiry"
  Public Class SubscriberConsumerEnquiry
    Public Class TelephoneTypes
      Public Const Landline As String = "T"
      Public Const Cellular As String = "C"
    End Class

    Public Class AddressTypes
      Public Const Street As String = "S"
      Public Const Postal As String = "P"
    End Class

    Public Class Statuses
      Public Const Queued As String = "Q"
      Public Const Processing As String = "P"
      Public Const Completed As String = "C"
    End Class

    Public Class Results
      Public Const Pending As String = "Q"
      Public Const NoRecordFound As String = "N"
      Public Const RecordFound As String = "F"
      Public Const MultipleRecordsFound As String = "M"
      Public Const RecordSelected As String = "S"
    End Class

    Public Class AgeSearchTypes
      Public Const Exact As String = "E"
      Public Const Deviation As String = "D"
    End Class
  End Class
#End Region

#Region " Class SubscriberCreditGrantorEnquiry"
  Public Class SubscriberCreditGrantorEnquiry
    Public Class Statuses
      Public Const Queued As String = "Q"
      Public Const Processing As String = "P"
      Public Const Completed As String = "C"
    End Class

    Public Class Results
      Public Const Pending As String = "Q"
      Public Const NoRecordFound As String = "N"
      Public Const RecordFound As String = "F"
      Public Const MultipleRecordsFound As String = "M"
      Public Const RecordSelected As String = "S"
    End Class
  End Class
#End Region

#Region " Class SubscriberDirectorEnquiry"
  Public Class SubscriberDirectorEnquiry
    Public Class Statuses
      Public Const Queued As String = "Q"
      Public Const Processing As String = "P"
      Public Const Completed As String = "C"
    End Class

    Public Class Results
      Public Const Pending As String = "Q"
      Public Const NoRecordFound As String = "N"
      Public Const RecordFound As String = "F"
      Public Const MultipleRecordsFound As String = "M"
      Public Const RecordSelected As String = "S"
    End Class
  End Class
#End Region

#Region " Class SubscriberHomeAffairsEnquiry"
  Public Class SubscriberHomeAffairsEnquiry
    Public Class Statuses
      Public Const Queued As String = "Q"
      Public Const Processing As String = "P"
      Public Const Completed As String = "C"
    End Class

    Public Class Results
      Public Const Pending As String = "Q"
      Public Const NoRecordFound As String = "N"
      Public Const RecordFound As String = "F"
      Public Const MultipleRecordsFound As String = "M"
      Public Const RecordSelected As String = "S"
    End Class
  End Class
#End Region

#Region " Class SubscriberPropertyDeedEnquiry"
  Public Class SubscriberPropertyDeedEnquiry
    Public Class Statuses
      Public Const Queued As String = "Q"
      Public Const Processing As String = "P"
      Public Const Completed As String = "C"
    End Class

    Public Class Results
      Public Const Pending As String = "Q"
      Public Const NoRecordFound As String = "N"
      Public Const RecordFound As String = "F"
      Public Const MultipleRecordsFound As String = "M"
      Public Const RecordSelected As String = "S"
    End Class
  End Class
#End Region

#Region " Class SubscriberSAFPSSubjectEnquiry"
  Public Class SubscriberSAFPSSubjectEnquiry
    Public Class Statuses
      Public Const Queued As String = "Q"
      Public Const Processing As String = "P"
      Public Const Completed As String = "C"
    End Class

    Public Class Results
      Public Const Pending As String = "Q"
      Public Const NoRecordFound As String = "N"
      Public Const RecordFound As String = "F"
      Public Const MultipleRecordsFound As String = "M"
      Public Const RecordSelected As String = "S"
    End Class
  End Class
#End Region

#Region " Class SubscriberProfile"
  Public Class SubscriberProfile
    Public Class DefaultIDVerificationSelectionTypes
      Public Const Include As String = "I"
      Public Const Ignore As String = "G"
    End Class
  End Class
#End Region

#Region " Class SubscriberProfileProduct"
  Public Class SubscriberProfileProduct
    Public Class Statuses
      Public Const Allowed As String = "A"
      Public Const Denied As String = "D"
    End Class
  End Class
#End Region

#Region " Class SubscriberProfileProductAuthenticationQuestion"
  Public Class SubscriberProfileProductAuthenticationQuestion
    Public Class PriorityTypes
      Public Const High As String = "H"
      Public Const Medium As String = "M"
      Public Const Low As String = "L"
    End Class

    Public Class Statuses
      Public Const Active As String = "A"
      Public Const Inactive As String = "I"
    End Class
  End Class
#End Region

#Region " Class SubscriberRegistration"
  Public Class SubscriberRegistration
    Public Class Statuses
      Public Const InProcess As String = "IP"
      Public Const AwaitingApproval As String = "AA"
      Public Const Approved As String = "A"
    End Class
  End Class
#End Region

#Region " Class SubscriberTraceConsumerAlert"
  Public Class SubscriberTraceConsumerAlert
    Public Class Statuses
      Public Const Active As String = "A"
      Public Const Processing As String = "P"
      Public Const Completed As String = "C"
      Public Const Expired As String = "E"
      Public Const Failed As String = "F"
      Public Const Cancelled As String = "X"
    End Class

    Public Class NotificationStatuses
      Public Const Queued As String = "Q"
      Public Const Processing As String = "P"
      Public Const Completed As String = "C"
      Public Const Failed As String = "F"
    End Class
  End Class
#End Region

#Region " Class SystemUser"
  Public Class SystemUser
    Public Class Types
      Public Const InternalUser As String = "I"
      Public Const SubscriberUser As String = "S"
    End Class
  End Class
#End Region

#Region " Class SystemUserProfile"
  Public Class SystemUserProfile
    Public Class DefaultPrioritizationRatingSelectionTypes
      Public Const Prompt As String = "P"
      Public Const Include As String = "I"
      Public Const Ignore As String = "G"
    End Class

    Public Class DefaultPublicDomainSelectionTypes
      Public Const Prompt As String = "P"
      Public Const Include As String = "I"
      Public Const Ignore As String = "G"
    End Class

    Public Class DefaultTraceEnquiryReportTypes
      Public Const PDF As String = "PDF"
      Public Const HTML As String = "HTML4.0"
    End Class

    Public Class AuthenticationDefaultRepeatAuthenticationSelectionTypes
      Public Const Open As String = "O"
      Public Const Closed As String = "C"
    End Class
  End Class
#End Region

#Region " Class SystemUserProfileProduct"
  Public Class SystemUserProfileProduct
    Public Class Statuses
      Public Const Allowed As String = "A"
      Public Const Denied As String = "D"
    End Class
  End Class
#End Region

#Region " Class SubscriberTraceAlert"
  Public Class SubscriberTraceAlert
    Public Class Statuses
      Public Const Active As String = "A"
      Public Const Processing As String = "P"
      Public Const Completed As String = "C"
      Public Const Expired As String = "E"
      Public Const Failed As String = "F"
      Public Const Cancelled As String = "X"
    End Class

    Public Class NotificationStatuses
      Public Const Queued As String = "Q"
      Public Const Processing As String = "P"
      Public Const Completed As String = "C"
      Public Const Failed As String = "F"
    End Class
  End Class
#End Region

#Region " Class TraceAlertDuration"
  Public Class TraceAlertDuration
    Public Class DurationTypes
      Public Const Day As String = "D"
      Public Const Week As String = "W"
      Public Const Month As String = "M"
    End Class
  End Class
#End Region

  Public Class RecordStatus
    Public Const Active As String = "A"
    Public Const Deleted As String = "D"
  End Class

  Public Class YesNo
    Public Const Yes As String = "Y"
    Public Const No As String = "N"
  End Class

  Public Class LoaderDataUpdateMode
    Public Const LastUpdatedDate As String = "L"
    Public Const AllFields As String = "A"
    Public Const None As String = "N"
  End Class

  Public Class vwLoaderStagingTable
    Public Const LoaderStagingTable_SubscriberID As String = "LoaderStagingTable_SubscriberID"
    Public Const LoaderStagingTable_MetaDatabaseCode As String = "LoaderStagingTable_MetaDatabaseCode"
    Public Const LoaderStagingTable_FileFormatDeploymentID As String = "LoaderStagingTable_FileFormatDeploymentID"
  End Class
End Class