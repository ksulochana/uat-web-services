Imports System.Text
Imports sembleWare.Runtime

Public Class Utility
  Public Shared Function GetNextSequenceNumber(ByVal TableName As String, ByVal WhereDBPart As DBPart, ByVal ApplicationSettings As ApplicationSettings) As Integer
    Return Utility.GetNextSequenceNumber(TableName, CustomDBPart.InstanceWhereClause(WhereDBPart), ApplicationSettings)
  End Function

  Public Shared Function GetNextSequenceNumber(ByVal TableName As String, ByVal WhereClause As String, ByVal ApplicationSettings As ApplicationSettings) As Integer
    Dim sQuery As String = "select isnull(max(SequenceNum), 0) from  " & TableName & " " & WhereClause
    Dim oDataTable As DataTable = ApplicationSettings.ResultQuery(sQuery)
    Dim oDataRow As DataRow

    For Each oDataRow In oDataTable.Rows
      Return System.Convert.ToInt32(oDataRow(0)) + 1
    Next
    Return 0
  End Function

  Public Shared Sub UpdateSequenceNumber(ByVal DBPart As CustomDBPart, ByVal WhereDBPart As DBPart, ByVal RecordDeletedYN As Boolean, ByVal ApplicationSettings As ApplicationSettings)
    UpdateSequenceNumber(DBPart, CustomDBPart.InstanceWhereClause(WhereDBPart), RecordDeletedYN, ApplicationSettings)
  End Sub

  Public Shared Sub UpdateSequenceNumber(ByVal DBPart As CustomDBPart, ByVal WhereClause As String, ByVal RecordDeletedYN As Boolean, ByVal ApplicationSettings As ApplicationSettings)
    Dim oSequenceNum As Element = DBPart.Elements("SequenceNum")
    Dim sQuery As String = ""

    If RecordDeletedYN Then
      sQuery = "update " & DBPart.TableName
      sQuery &= " set SequenceNum = SequenceNum - 1 "
      If Not WhereClause = "" Then
        sQuery &= WhereClause
        sQuery &= " and "
      Else
        sQuery &= " where "
      End If
      sQuery &= " SequenceNum > " & ApplicationSettings.QueryBuilder.ToSQL(oSequenceNum.ValueAsObject)
      ApplicationSettings.ActionQuery(sQuery)
    Else
      If oSequenceNum.MustPersist Then
        Dim nLoadValue As Integer = IIf(oSequenceNum.LoadValueAsObject Is Nothing, oSequenceNum.ValueAsObject, oSequenceNum.LoadValueAsObject)

        If oSequenceNum.ValueAsObject = nLoadValue Then
          sQuery = "update " & DBPart.TableName
          sQuery &= " set SequenceNum = SequenceNum + 1 "
          If Not WhereClause = "" Then
            sQuery &= WhereClause
            sQuery &= " and "
          Else
            sQuery &= " where "
          End If
          sQuery &= " SequenceNum >= " & ApplicationSettings.QueryBuilder.ToSQL(oSequenceNum.ValueAsObject)
          ApplicationSettings.ActionQuery(sQuery)
        Else
          If oSequenceNum.ValueAsObject < nLoadValue Then
            sQuery = "update " & DBPart.TableName
            sQuery &= " set SequenceNum = SequenceNum + 1 "
            If Not WhereClause = "" Then
              sQuery &= WhereClause
              sQuery &= " and "
            Else
              sQuery &= " where "
            End If
            sQuery &= " SequenceNum >= " & ApplicationSettings.QueryBuilder.ToSQL(oSequenceNum.ValueAsObject)
            sQuery &= " and SequenceNum < " & ApplicationSettings.QueryBuilder.ToSQL(nLoadValue)
            ApplicationSettings.ActionQuery(sQuery)
          Else
            sQuery = "update " & DBPart.TableName
            sQuery &= " set SequenceNum = SequenceNum - 1 "
            If Not WhereClause = "" Then
              sQuery &= WhereClause
              sQuery &= " and "
            Else
              sQuery &= " where "
            End If
            sQuery &= " SequenceNum > " & ApplicationSettings.QueryBuilder.ToSQL(nLoadValue)
            sQuery &= " and SequenceNum <= " & ApplicationSettings.QueryBuilder.ToSQL(oSequenceNum.ValueAsObject)

            ApplicationSettings.ActionQuery(sQuery)
          End If
        End If
      End If
    End If
  End Sub

  Public Shared Function FalseIfNull(ByVal oValue As Object) As Object
    If IsDBNull(oValue) OrElse IsNothing(oValue) Then
      Return False
    Else
      Return oValue
    End If
  End Function

  Public Shared Function NullIfNull(ByVal oValue As Object) As Object
    If IsDBNull(oValue) OrElse IsNothing(oValue) Then
      Return Nothing
    Else
      Return oValue
    End If
  End Function

  Public Shared Function BlankIfNull(ByVal oValue As Object) As String
    If IsDBNull(oValue) OrElse IsNothing(oValue) Then
      Return ""
    Else
      Return oValue
    End If
  End Function

  Public Shared Function ZeroIfNull(ByVal oValue As Object) As Object
    If IsDBNull(oValue) OrElse IsNothing(oValue) Then
      Return 0
    Else
      Return oValue
    End If
  End Function

  Public Shared Function ConvertByteArrayToMemoryStream(ByVal ByteArray As Byte()) As System.IO.MemoryStream
    Dim oStream As System.IO.MemoryStream = New System.IO.MemoryStream
    Dim oBinaryWriter As System.IO.BinaryWriter = New System.IO.BinaryWriter(oStream)
    Dim nCounter As Integer

    For nCounter = 0 To ByteArray.Length - 1
      oBinaryWriter.Write(ByteArray(nCounter))
    Next nCounter

    Return oStream
  End Function

  Public Shared Function ValidatePassword(ByVal NewPassword As String, ByVal ConfirmPassword As String) As String
    Dim oEncryption As sembleWare.Security.Encryption = New sembleWare.Security.Encryption(Constants.PasswordHashString)
    NewPassword = oEncryption.Encrypt(NewPassword)
    ConfirmPassword = oEncryption.Encrypt(ConfirmPassword)

    If Not NewPassword = ConfirmPassword Then
      Throw New BusinessRuleException("The Password does match the Confirm Password. Please re-enter the password!")
    End If
    Return NewPassword
  End Function

  Public Shared Sub BRTelephoneTypeInd(ByVal TelephoneTypeInd As Element, ByVal TelephoneTelephoneCode As Element, ByVal TelephoneTelephoneNo As Element, _
                                       ByVal TelephoneCellularCode As Element, ByVal TelephoneCellularNo As Element, _
                                       ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If SetEnableDisableYN Then
      TelephoneTelephoneCode.Enabled = False
      TelephoneTelephoneNo.Enabled = False
      TelephoneTelephoneNo.Mandatory = False
      TelephoneCellularCode.Enabled = False
      TelephoneCellularNo.Enabled = False
      TelephoneCellularNo.Mandatory = False
      Select Case TelephoneTypeInd.ValueAsObject
        Case Constants.SubscriberConsumerEnquiry.TelephoneTypes.Landline
          TelephoneTelephoneCode.Enabled = True
          TelephoneTelephoneNo.Enabled = True
          TelephoneTelephoneNo.Mandatory = True
        Case Constants.SubscriberConsumerEnquiry.TelephoneTypes.Cellular
          TelephoneCellularCode.Enabled = True
          TelephoneCellularNo.Enabled = True
          TelephoneCellularNo.Mandatory = True
      End Select
    End If

    If PerformRulesYN Then
      TelephoneTelephoneCode.SetToNullValue()
      TelephoneTelephoneNo.SetToNullValue()
      TelephoneCellularCode.SetToNullValue()
      TelephoneCellularNo.SetToNullValue()
    End If
  End Sub

  Public Shared Sub BRAddressTypeInd(ByVal AddressTypeInd As Element, ByVal AddressPostalNo As Element, ByVal AddressPostalSuburb As Element, ByVal AddressPostalPostalCode As Element, _
                                     ByVal AddressStreetNo As Element, ByVal AddressStreetName As Element, ByVal AddressStreetSuburb As Element, ByVal AddressStreetPostalCode As Element, _
                                     ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If SetEnableDisableYN Then
      AddressPostalNo.Enabled = False
      AddressPostalNo.Mandatory = False
      AddressPostalSuburb.Enabled = False
      AddressPostalSuburb.Mandatory = False
      AddressPostalPostalCode.Enabled = False
      AddressPostalPostalCode.Mandatory = False
      AddressStreetNo.Enabled = False
      AddressStreetNo.Mandatory = False
      AddressStreetName.Enabled = False
      AddressStreetName.Mandatory = False
      AddressStreetSuburb.Enabled = False
      AddressStreetSuburb.Mandatory = False
      AddressStreetPostalCode.Enabled = False
      AddressStreetPostalCode.Mandatory = False
      Select Case AddressTypeInd.ValueAsObject
        Case Constants.SubscriberConsumerEnquiry.AddressTypes.Postal
          AddressPostalNo.Enabled = True
          AddressPostalNo.Mandatory = True
          AddressPostalSuburb.Enabled = True
          AddressPostalSuburb.Mandatory = True
          AddressPostalPostalCode.Enabled = True
          AddressPostalPostalCode.Mandatory = True
        Case Constants.SubscriberConsumerEnquiry.AddressTypes.Street
          AddressStreetNo.Enabled = True
          AddressStreetNo.Mandatory = True
          AddressStreetName.Enabled = True
          AddressStreetName.Mandatory = True
          AddressStreetSuburb.Enabled = True
          AddressStreetSuburb.Mandatory = True
          AddressStreetPostalCode.Enabled = True
          AddressStreetPostalCode.Mandatory = True
      End Select
    End If

    If PerformRulesYN Then
      AddressPostalNo.SetToNullValue()
      AddressPostalSuburb.SetToNullValue()
      AddressPostalPostalCode.SetToNullValue()
      AddressStreetNo.SetToNullValue()
      AddressStreetName.SetToNullValue()
      AddressStreetSuburb.SetToNullValue()
      AddressStreetPostalCode.SetToNullValue()
    End If
  End Sub

  Public Shared Sub BRProductTraceConsumer(ByVal Product As Product, ByVal IDNo As Element, ByVal PassportNo As Element, ByVal Surname As Element, ByVal MaidenName As Element, _
                                           ByVal FirstInitial As Element, ByVal SecondInitial As Element, ByVal FirstName As Element, ByVal SecondName As Element, _
                                           ByVal BirthDate As Element, ByVal GenderInd As Element, ByVal AddressTypeInd As Element, ByVal AddressStreetNo As Element, _
                                           ByVal AddressStreetName As Element, ByVal AddressStreetSuburb As Element, ByVal AddressStreetPostalCode As Element, _
                                           ByVal AddressPostalNo As Element, ByVal AddressPostalSuburb As Element, ByVal AddressPostalPostalCode As Element, _
                                           ByVal TelephoneTypeInd As Element, ByVal TelephoneTelephoneCode As Element, ByVal TelephoneTelephoneNo As Element, _
                                           ByVal TelephoneCellularCode As Element, ByVal TelephoneCellularNo As Element, ByVal AccountNo As Element, _
                                           ByVal SubAccountNo As Element, ByVal Age As Element, ByVal AgeBirthYear As Element, ByVal AgeDeviation As Element, _
                                           ByVal AgeSearchTypeInd As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If SetEnableDisableYN Then
      IDNo.Enabled = False
      PassportNo.Enabled = False
      Surname.Enabled = False
      Surname.Mandatory = False
      MaidenName.Enabled = False
      FirstInitial.Enabled = False
      SecondInitial.Enabled = False
      BirthDate.Enabled = False
      GenderInd.Enabled = False
      AddressStreetNo.Enabled = False
      AddressStreetNo.Mandatory = False
      AddressStreetName.Enabled = False
      AddressStreetName.Mandatory = False
      AddressStreetSuburb.Enabled = False
      AddressStreetSuburb.Mandatory = False
      AddressStreetPostalCode.Enabled = False
      AddressStreetPostalCode.Mandatory = False
      AddressPostalNo.Enabled = False
      AddressPostalNo.Mandatory = False
      AddressPostalSuburb.Enabled = False
      AddressPostalSuburb.Mandatory = False
      AddressPostalPostalCode.Enabled = False
      AddressPostalPostalCode.Mandatory = False
      TelephoneTelephoneCode.Enabled = False
      TelephoneTelephoneCode.Mandatory = False
      TelephoneTelephoneNo.Enabled = False
      TelephoneTelephoneNo.Mandatory = False
      TelephoneCellularCode.Enabled = False
      TelephoneCellularCode.Mandatory = False
      TelephoneCellularNo.Enabled = False
      TelephoneCellularNo.Mandatory = False
      AccountNo.Enabled = False
      AccountNo.Mandatory = False
      SubAccountNo.Enabled = False
      Age.Enabled = False
      Age.Mandatory = False
      AgeBirthYear.Enabled = False
      AgeBirthYear.Mandatory = False
      AgeDeviation.Enabled = False
      AgeSearchTypeInd.Enabled = False
      AgeSearchTypeInd.Mandatory = False

      If Not Product Is Nothing Then
        Select Case Product.ProductID.Value
          Case Constants.Product.Records.AuthenticationConsumer
            IDNo.Enabled = True
            PassportNo.Enabled = True
            TelephoneCellularCode.Enabled = True
            TelephoneCellularNo.Enabled = True
            AccountNo.Enabled = True
            SubAccountNo.Enabled = True
          Case Constants.Product.Records.TraceConsumer, Constants.Product.Records.TraceConsumerPreScreen, Constants.Product.Records.TraceConsumerWebService
            IDNo.Enabled = True
            PassportNo.Enabled = True
            Surname.Enabled = True
            MaidenName.Enabled = True
            FirstInitial.Enabled = True
            SecondInitial.Enabled = True
            BirthDate.Enabled = True
            GenderInd.Enabled = True
          Case Constants.Product.Records.TraceConsumerMultiple
            IDNo.Enabled = True
            PassportNo.Enabled = True
            Surname.Enabled = True
            FirstInitial.Enabled = True
            SecondInitial.Enabled = True
            BirthDate.Enabled = True
            GenderInd.Enabled = True
            AccountNo.Enabled = True
            SubAccountNo.Enabled = True
          Case Constants.Product.Records.TraceConsumerAddress, Constants.Product.Records.TraceConsumerAddressWebService
            Utility.BRAddressTypeInd(AddressTypeInd, AddressPostalNo, AddressPostalSuburb, AddressPostalPostalCode, AddressStreetNo, AddressStreetName, AddressStreetSuburb, AddressStreetPostalCode, True, True)
          Case Constants.Product.Records.TraceConsumerTelephone, Constants.Product.Records.TraceConsumerTelephoneWebService
            Utility.BRTelephoneTypeInd(TelephoneTypeInd, TelephoneTelephoneCode, TelephoneTelephoneNo, TelephoneCellularCode, TelephoneCellularNo, True, True)
          Case Constants.Product.Records.TraceConsumerAccountNumber, Constants.Product.Records.TraceConsumerAccountNumberWebService
            AccountNo.Enabled = True
            AccountNo.Mandatory = True
            SubAccountNo.Enabled = True
          Case Constants.Product.Records.TraceConsumerEasy, Constants.Product.Records.TraceConsumerEasyWebService
            Surname.Enabled = True
            Surname.Mandatory = True
            FirstName.Enabled = True
            SecondName.Enabled = True
            FirstInitial.Enabled = True
            SecondInitial.Enabled = True
            GenderInd.Enabled = True
            Age.Enabled = True
            Age.Mandatory = True
            AgeBirthYear.Enabled = True
            AgeBirthYear.Mandatory = True
            AgeDeviation.Enabled = True
            AgeSearchTypeInd.Enabled = True
            AgeSearchTypeInd.Mandatory = True
          Case Constants.Product.Records.TraceConsumerIdentityVerification, Constants.Product.Records.TraceConsumerIdentityVerificationWebService
            IDNo.Enabled = True
            IDNo.Mandatory = True
            Surname.Enabled = True
            FirstName.Enabled = True
            SecondName.Enabled = True
            BirthDate.Enabled = True
          Case Constants.Product.Records.CreditGrantorConsumerApplicationCreditEnquiry, Constants.Product.Records.CreditGrantorConsumerCreditEnquiry, _
               Constants.Product.Records.CreditGrantorConsumerApplicationCreditEnquiryWebService, Constants.Product.Records.CreditGrantorConsumerCreditEnquiryWebService, _
               Constants.Product.Records.CreditGrantorCreditApplication
            IDNo.Enabled = True
            PassportNo.Enabled = True
            Surname.Enabled = True
            MaidenName.Enabled = True
            FirstInitial.Enabled = True
            SecondInitial.Enabled = True
            BirthDate.Enabled = True
            GenderInd.Enabled = True
        End Select
      End If
    End If

    If PerformRulesYN Then
      IDNo.SetToNullValue()
      PassportNo.SetToNullValue()
      Surname.SetToNullValue()
      FirstInitial.SetToNullValue()
      SecondInitial.SetToNullValue()
      BirthDate.SetToNullValue()
      GenderInd.SetToNullValue()
      AddressPostalNo.SetToNullValue()
      AddressPostalSuburb.SetToNullValue()
      AddressPostalPostalCode.SetToNullValue()
      AddressStreetNo.SetToNullValue()
      AddressStreetName.SetToNullValue()
      AddressStreetSuburb.SetToNullValue()
      AddressStreetPostalCode.SetToNullValue()
      TelephoneTelephoneCode.SetToNullValue()
      TelephoneTelephoneNo.SetToNullValue()
      TelephoneCellularCode.SetToNullValue()
      TelephoneCellularNo.SetToNullValue()
      AccountNo.SetToNullValue()
      SubAccountNo.SetToNullValue()
      Age.SetToNullValue()
      AgeBirthYear.SetToNullValue()
      AgeDeviation.SetToNullValue()
      AgeSearchTypeInd.SetToNullValue()
    End If
  End Sub

  Public Shared Sub BRProductTraceHomeAffairs(ByVal Product As Product, ByVal IDNo As Element, ByVal Surname As Element, _
                                              ByVal FirstName As Element, ByVal SecondName As Element, ByVal BirthDate As Element, _
                                              ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If SetEnableDisableYN Then
      IDNo.Enabled = False
      Surname.Enabled = False
      Surname.Mandatory = False
      BirthDate.Enabled = False

      If Not Product Is Nothing Then
        Select Case Product.ProductID.Value
          Case Constants.Product.Records.TraceConsumerIdentityVerification, Constants.Product.Records.TraceConsumerIdentityVerificationWebService
            IDNo.Enabled = True
            IDNo.Mandatory = True
            Surname.Enabled = True
            FirstName.Enabled = True
            SecondName.Enabled = True
            BirthDate.Enabled = True
        End Select
      End If
    End If

    If PerformRulesYN Then
      IDNo.SetToNullValue()
      Surname.SetToNullValue()
      BirthDate.SetToNullValue()
    End If
  End Sub

  Public Shared Sub BRProductEnquiryCommercial(ByVal Product As Product, ByVal RegistrationNoPart1 As Element, ByVal RegistrationNoPart2 As Element, _
                                               ByVal RegistrationNoPart3 As Element, ByVal CommercialName As Element, _
                                               ByVal TaxNo As Element, ByVal SIC As Relationship, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If SetEnableDisableYN Then
      RegistrationNoPart1.Enabled = False
      RegistrationNoPart1.Mandatory = False
      RegistrationNoPart2.Enabled = False
      RegistrationNoPart2.Mandatory = False
      RegistrationNoPart3.Enabled = False
      RegistrationNoPart3.Mandatory = False
      CommercialName.Enabled = False
      CommercialName.Mandatory = False
      TaxNo.Enabled = False
      TaxNo.Mandatory = False
      SIC.Enabled = False
      SIC.Mandatory = False

      If Not Product Is Nothing Then
        Select Case Product.ProductID.Value
          Case Constants.Product.Records.EnquiryCommercialBusiness, Constants.Product.Records.EnquiryCommercialBusinessWebService
            RegistrationNoPart1.Enabled = True
            RegistrationNoPart2.Enabled = True
            RegistrationNoPart3.Enabled = True
            CommercialName.Enabled = True
            TaxNo.Enabled = True
          Case Constants.Product.Records.EnquiryCommercial
            RegistrationNoPart1.Enabled = True
            RegistrationNoPart1.Mandatory = True
            RegistrationNoPart2.Enabled = True
            RegistrationNoPart2.Mandatory = True
            RegistrationNoPart3.Enabled = True
            RegistrationNoPart3.Mandatory = True
          Case Constants.Product.Records.EnquiryCommercialBusinessName
            CommercialName.Enabled = True
            CommercialName.Mandatory = True
            SIC.Enabled = True
          Case Constants.Product.Records.EnquiryCommercialEasy
            CommercialName.Enabled = True
            CommercialName.Mandatory = True
        End Select
      End If
    End If

    If PerformRulesYN Then
      RegistrationNoPart1.SetToNullValue()
      RegistrationNoPart2.SetToNullValue()
      RegistrationNoPart3.SetToNullValue()
      CommercialName.SetToNullValue()
      TaxNo.SetToNullValue()
      SIC.Instance = Nothing
    End If
  End Sub

  Public Shared Sub BRProductTraceDirector(ByVal Product As Product, ByVal IDNo As Element, ByVal Surname As Element, _
                                           ByVal FirstInitial As Element, ByVal SecondInitial As Element, ByVal FirstName As Element, ByVal SecondName As Element, _
                                           ByVal BirthDate As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If SetEnableDisableYN Then
      IDNo.Enabled = False
      Surname.Enabled = False
      Surname.Mandatory = False
      FirstInitial.Enabled = False
      SecondInitial.Enabled = False
      BirthDate.Enabled = False

      If Not Product Is Nothing Then
        Select Case Product.ProductID.Value
          Case Constants.Product.Records.EnquiryCommercialDirector, Constants.Product.Records.EnquiryCommercialDirectorWebService
            IDNo.Enabled = True
            Surname.Enabled = True
            FirstInitial.Enabled = True
            SecondInitial.Enabled = True
            BirthDate.Enabled = True
        End Select
      End If
    End If

    If PerformRulesYN Then
      IDNo.SetToNullValue()
      Surname.SetToNullValue()
      FirstInitial.SetToNullValue()
      SecondInitial.SetToNullValue()
      BirthDate.SetToNullValue()
    End If
  End Sub

  Public Shared Sub BRProductCreditGrantor(ByVal Product As Product, ByVal ReferenceNo As Element, ByVal EnquiryDate As Element, _
                                           ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If SetEnableDisableYN Then
      ReferenceNo.Enabled = False
      EnquiryDate.Enabled = False

      If Not Product Is Nothing Then
        Select Case Product.ProductID.Value
          Case Constants.Product.Records.CreditGrantorDebtCollectorsEnquiry, Constants.Product.Records.CreditGrantorReEnquiry, _
               Constants.Product.Records.CreditGrantorDebtCollectorsEnquiryWebService, Constants.Product.Records.CreditGrantorReEnquiryWebService
            ReferenceNo.Enabled = True
            EnquiryDate.Enabled = True
        End Select
      End If
    End If

    If PerformRulesYN Then
      ReferenceNo.SetToNullValue()
      EnquiryDate.SetToNullValue()
    End If
  End Sub

  'Public Shared Sub BRProductEnquirySAFPSSubject(ByVal Product As Product, ByVal IDNo As Element, ByVal PassportNo As Element, ByVal Surname As Element, _
  '                                               ByVal FirstName As Element, ByVal SecondName As Element, ByVal BirthDate As Element, ByVal GenderInd As Element, _
  '                                               ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
  '  If SetEnableDisableYN Then
  '    IDNo.Enabled = False
  '    PassportNo.Enabled = False
  '    Surname.Enabled = False
  '    Surname.Mandatory = False
  '    BirthDate.Enabled = False
  '    GenderInd.Enabled = False

  '    If Not Product Is Nothing Then
  '      Select Case Product.ProductID.Value
  '        Case Constants.Product.Records.EnquirySAFPSSubject
  '          IDNo.Enabled = True
  '          PassportNo.Enabled = True
  '      End Select
  '    End If
  '  End If

  '  If PerformRulesYN Then
  '    IDNo.SetToNullValue()
  '    PassportNo.SetToNullValue()
  '    Surname.SetToNullValue()
  '    BirthDate.SetToNullValue()
  '    GenderInd.SetToNullValue()
  '  End If
  'End Sub

  Public Shared Sub BRCheckProductReport(ByVal Product As Product)
    If Not Product.EnableReportingServicesReportYN.Value Then
      Throw New BusinessRuleException("A Report has not been defined for this Product! Please contact the administrator!")
    End If
  End Sub

  Public Shared Function BRGetProductReportingServicesReport(ByVal SubscriberProfile As SubscriberProfile, ByVal Product As Product) As ReportingServicesReport
    Dim oSubscriberProfileProduct As SubscriberProfileProduct = SubscriberProfile.SubscriberProfileProduct_OwnMany.NewInstance
    oSubscriberProfileProduct.Product.Instance = Product
    oSubscriberProfileProduct.Load()
    If oSubscriberProfileProduct.OverrideReportingServicesReportYN.Value Then
      Return oSubscriberProfileProduct.OverrideReportingServicesReport.Instance
    End If
    Return Product.ReportingServicesReport.Instance
  End Function

  Public Shared Function BRGetProductReportingServicesReportDisplayInd(ByVal SubscriberProfile As SubscriberProfile, ByVal Product As Product) As String
    Dim oSubscriberProfileProduct As SubscriberProfileProduct = SubscriberProfile.SubscriberProfileProduct_OwnMany.NewInstance
    oSubscriberProfileProduct.Product.Instance = Product
    oSubscriberProfileProduct.Load()
    If oSubscriberProfileProduct.OverrideReportingServicesReportYN.Value Then
      Return oSubscriberProfileProduct.OverrideReportingServicesReportDisplayInd.Value
    End If
    Return Product.ReportingServicesReportDisplayInd.Value
  End Function
End Class