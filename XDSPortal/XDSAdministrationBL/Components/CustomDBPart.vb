Imports System
Imports System.Text
Imports sembleWare.Runtime

Public Class CustomDBPart
  Inherits sembleWare.Runtime.DBPart

  Public Enum enumDeleteType
    DeleteRecord
    ChangeRecordStatus
  End Enum

  Public ReadOnly CreatedByUser As New StringElement("CreatedByUser", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CreatedOnDate As New DateTimeElement("CreatedOnDate", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ChangedByUser As New StringElement("ChangedByUser", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ChangedOnDate As New DateTimeElement("ChangedOnDate", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly MessageToDisplay As New TextElement("MessageToDisplay", Me, True, True, False, False, Nothing, Nothing, Nothing)

  Private mbForceUpperCaseKeysYN As Boolean = True
  Private mbEnableAuditFieldsYN As Boolean = True
  Private meDeleteType As enumDeleteType = enumDeleteType.DeleteRecord

#Region " Properties"
  Public Property EnableAuditFields() As Boolean
    Get
      Return mbEnableAuditFieldsYN
    End Get
    Set(ByVal Value As Boolean)
      mbEnableAuditFieldsYN = Value
    End Set
  End Property

  Public Property ForceUpperCaseKeys() As Boolean
    Get
      Return mbForceUpperCaseKeysYN
    End Get
    Set(ByVal Value As Boolean)
      mbForceUpperCaseKeysYN = Value
    End Set
  End Property

  Public Property DeleteType() As enumDeleteType
    Get
      Return meDeleteType
    End Get
    Set(ByVal Value As enumDeleteType)
      meDeleteType = Value
    End Set
  End Property
#End Region

#Region " Constructor"
  Public Sub New(ByVal Name As String, ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(Name, ApplicationSettings)
  End Sub
#End Region

#Region " Base Overrides"
  Public Overrides Sub Load()
    Try
      If mbForceUpperCaseKeysYN Then
        Dim oElement As Element
        For Each oElement In Keys
          If oElement.ElementType = DataType.String AndAlso Not oElement.IsNull Then
            oElement.Load(oElement.ValueAsObject.ToString().ToUpper())
          End If
        Next
      End If
      MyBase.Load()
    Catch oException As Exception
      Throw oException
    End Try
  End Sub

  Public Overrides Sub Save()
    CheckMandatoryFields()
    If MustPersist() Then
      If IsNew Then
        Select Case DeleteType
          Case enumDeleteType.ChangeRecordStatus
            Me.Elements("RecordStatusInd").ValueAsObject = "A"
        End Select
      End If
      If IsNew And mbForceUpperCaseKeysYN Then
        Dim oElement As Element

        For Each oElement In Keys
          If oElement.ElementType = DataType.String AndAlso Not oElement.IsNull Then
            oElement.ValueAsObject = oElement.ValueAsObject.ToString().ToUpper()
          End If
        Next
      End If
      '//Update changed by and created on fields
      If mbEnableAuditFieldsYN Then
        Call UpdateAuditFields()
      End If
      Try
        MyBase.Save()
      Catch oConcurrencyException As sembleWare.Runtime.ConcurrencyException
        Throw New ConcurrencyException(Me, oConcurrencyException)
      Catch oException As Exception
        Throw oException
      End Try
    End If
  End Sub

  Public Overrides Sub Delete()
    Select Case DeleteType
      Case enumDeleteType.ChangeRecordStatus
        Me.Elements("RecordStatusInd").ValueAsObject = "D"
        MyBase.Save()
      Case enumDeleteType.DeleteRecord
        MyBase.Delete()
    End Select
  End Sub
#End Region

#Region " Methods"
  Public Sub UpdateAuditFields()
    Dim sUsername = Constants.XDSAdministrationServicesUser

    If Not GlobalSettings.SystemUser.LoggedInSystemUserID = 0 Then
      sUsername = XDSAdministrationBL.GlobalSettings.SystemUser.LoggedInSystemUsername()
    End If
    If Me.IsNew Then
      CreatedByUser.Value = sUsername
      CreatedOnDate.Value = System.DateTime.Now
    Else
      ChangedByUser.Value = sUsername
      ChangedOnDate.Value = System.DateTime.Now
    End If
  End Sub

  Public Sub CheckMandatoryFields()
    Dim sMissingElements As String = MyBase.MissingMandatoryFields()
    If Not sMissingElements = "" Then
      Dim oMissingValuesException As MissingValuesException = New MissingValuesException(sMissingElements, MyBase.Caption, "")
      Throw New BusinessRuleException(oMissingValuesException.Message)
    End If
  End Sub

  Public Sub DisableElementsAndRelationship()
    Dim oElement As Element
    Dim oRelationship As Relationship

    For Each oElement In Me.Elements
      oElement.Enabled = False
    Next

    For Each oRelationship In Me.OwnedBys
      oRelationship.Enabled = False
    Next

    For Each oRelationship In Me.Includes
      oRelationship.Enabled = False
    Next
  End Sub

  Public Function CreateLabelDescription(ByVal Caption As String, ByVal Description As Element) As String
    Return CreateLabelDescription(Caption, Nothing, Description)
  End Function

  Public Function CreateLabelDescription(ByVal Caption As String, ByVal Key As Element, ByVal Description As Element) As String
    Dim sKeyValue As String = ""
    Dim sDescriptionValue As String = ""

    If Key Is Nothing And Description.IsEmpty Then
      sKeyValue = "New"
    End If
    If Not Key Is Nothing Then
      sKeyValue = Key.ValueAsObject
    End If
    If Not Description Is Nothing Then
      sDescriptionValue = Description.ValueAsObject
    End If
    If sKeyValue = "" Or sDescriptionValue = "" Then
      Return Caption & ": " & sKeyValue & sDescriptionValue
    End If
    Return Caption & ": " & sKeyValue & " (" & sDescriptionValue & ")"
  End Function
#End Region

#Region " Shared Procedures"
  '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  'Created By : Reynold Yip
  'Created On : 10 May 2004
  'Notes      : Creates a where clause based on the primary keys of a part
  '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  Public Shared Function InstanceWhereClause(ByVal Part As DBPart) As String
    Return InstanceWhereClause(Part, "")
  End Function

  '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  'Created By : Reynold Yip
  'Created On : 7 June 2004
  'Notes      : Creates a where clause based on the primary keys of a part
  '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  Public Shared Function InstanceWhereClause(ByVal Part As DBPart, ByVal TableAlias As String) As String
    Dim sWhere As New StringBuilder
    Dim sSeparator As String = " where "
    Dim oElement As Element
    If Not TableAlias = "" Then
      TableAlias = Part.ApplicationSettings.QueryBuilder.Wrap(TableAlias)
      TableAlias &= "."
    End If
    For Each oElement In Part.Keys
      sWhere.Append(sSeparator)
      sWhere.Append(TableAlias & Part.ApplicationSettings.QueryBuilder.Wrap(oElement.FieldName))
      sWhere.Append(" = ")
      sWhere.Append(Part.ApplicationSettings.QueryBuilder.ToSQL(oElement.ValueAsObject))
      If sSeparator = " where " Then
        sSeparator = " and "
      End If
    Next
    Return sWhere.ToString()
  End Function
#End Region

  Public Enum ElementState
    RequiredEntry
    OptionalEntry
    RequiredReadOnly
    OptionalReadOnly
    NotApplicable
  End Enum

  Public Sub SetElementByState(ByVal oElement As Element, ByVal eState As ElementState)
    With oElement
      Select Case eState
        Case ElementState.RequiredEntry
          .Mandatory = True
          .Enabled = True
        Case ElementState.OptionalEntry
          .Mandatory = False
          .Enabled = True
        Case ElementState.RequiredReadOnly
          .Mandatory = True
          .Enabled = False
        Case ElementState.OptionalReadOnly
          .Mandatory = False
          .Enabled = False
        Case ElementState.NotApplicable
          .SetToNullValue()
          .Mandatory = False
          .Enabled = False
      End Select
    End With
  End Sub
End Class