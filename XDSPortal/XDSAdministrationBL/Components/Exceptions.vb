Imports sembleWare.Runtime

' Use this exception to NOT send an error to the Error Management System.
Public Class BusinessRuleException
  Inherits Exception

  Public Sub New(ByVal Message As String)
    MyBase.New(Message)
  End Sub
End Class

' Use this exception to send an error to the Error Management System.
Public Class BusinessRuleNotificationException
  Inherits Exception

  Public Sub New(ByVal Message As String)
    MyBase.New(Message)
  End Sub
End Class

Public Class ConcurrencyException
  Inherits sembleWare.Runtime.ConcurrencyException

  Public ReadOnly _DBPart As DBPart
  Public ReadOnly _ConcurrencyException As sembleWare.Runtime.ConcurrencyException
  Public ReadOnly Fields As String

  Public Sub New(ByVal oPart As DBPart, ByVal oConcurrencyException As sembleWare.Runtime.ConcurrencyException)
    MyBase.New(oPart.Name)
    _DBPart = oPart : _ConcurrencyException = oConcurrencyException
    'Fields = BLGeneral.FindConcurrencyIssueFields(oPart)
  End Sub

  Public Overrides Function ToString() As String
    Return "[Fields = " & Fields & "] " & vbCrLf & MyBase.ToString
  End Function
End Class