Imports sembleWare.Runtime
Imports System

Public Class CustomMemoryPart
  Inherits MemoryPart

  Public ReportParameterCollection As ReportingServicesReport.ReportParameterCollection = New ReportingServicesReport.ReportParameterCollection

#Region " Elements & Relationships"
  Public ReadOnly ReportImageUrl As New StringElement("ReportImageUrl", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ReportImagePath As New StringElement("ReportImagePath", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ReportingServicesReportFormat As New StringElement("ReportingServicesReportFormat", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ReportingServicesReport As Relationship = New ReportingServicesReportRelationship("ReportingServicesReport", Nothing, RelationshipType.Include, False, True, True, Me)

#Region "  Foreign Items"
  Public ReadOnly _ReportingServicesReportID As New IntegerElement("ReportingServicesReportID", Me, False)
  Public ReadOnly _ReportingServicesReport As ReportingServicesReportRelationship = ReportingServicesReport
#End Region
#End Region

#Region " Constructor"
  Public Sub New(ByVal Name As String, ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(Name, ApplicationSettings)
    _ReportingServicesReport._ReportingServicesReportID.LocalElement = _ReportingServicesReportID
    ReportingServicesReportFormat.Value = "PDF"
  End Sub
#End Region

#Region " Methods"
  Public Sub SetReportingServicesReport(ByVal CustomScreenName As String)
    Dim oReportingServicesReport As ReportingServicesReport = New ReportingServicesReportRelationship(ApplicationSettings).NewInstance
    oReportingServicesReport.LoadOnCustomScreenName(CustomScreenName)
    Me.ReportingServicesReport.Instance = oReportingServicesReport
  End Sub

  Public Sub CheckMandatoryFields()
    Dim sMissingElements As String = MyBase.MissingMandatoryFields()
    If Not sMissingElements = "" Then
      Dim oMissingValuesException As MissingValuesException = New MissingValuesException(sMissingElements, MyBase.Caption, "")
      Throw New BusinessRuleException(oMissingValuesException.Message)
    End If
  End Sub
#End Region

#Region " Overridable Methods"
  Public Overridable Function RunReport(ByRef MimeType As String) As Byte()
    Dim oReportingServicesReport As ReportingServicesReport = Me.ReportingServicesReport.Instance

    Return oReportingServicesReport.GetReportBytes(Me.ReportParameterCollection, Me.ReportingServicesReportFormat.Value, Me.ReportImageUrl.Value, Me.ReportImagePath.Value, MimeType)
  End Function
#End Region
End Class