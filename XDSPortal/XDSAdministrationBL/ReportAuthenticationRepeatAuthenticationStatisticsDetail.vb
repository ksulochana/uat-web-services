Imports sembleWare.Runtime
Imports System
Public Class ReportAuthenticationRepeatAuthenticationStatisticsDetail 'sembleWare: Part
  Inherits CustomMemoryPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly AuthenticationDateFrom As New DateTimeElement("AuthenticationDateFrom", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly AuthenticationDateTo As New DateTimeElement("AuthenticationDateTo", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly ReferenceNo As New StringElement("ReferenceNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AuthenticationTypeInd As New IndicatorElement("AuthenticationTypeInd", Me, False, False, True, True, True, False, "Authentication Type", Nothing, ReportAuthenticationRepeatAuthenticationStatisticsDetail.AuthenticationTypeIndOptions, "All")
  Public ReadOnly AuthenticationStatusInd As New IndicatorElement("AuthenticationStatusInd", Me, False, False, True, True, True, False, "Authentication Status", Nothing, ReportAuthenticationRepeatAuthenticationStatisticsDetail.AuthenticationStatusIndOptions, "All")
  Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.Include, True, True, True, Me)
  Public ReadOnly ActionedBySystemUser As Relationship = New SystemUserRelationship("ActionedBySystemUser", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly SubscriberProfileProductAuthenticationBranch As Relationship = New SubscriberProfileProductAuthenticationBranchRelationship("SubscriberProfileProductAuthenticationBranch", "Branch", RelationshipType.Include, False, True, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, False)
  Public ReadOnly _ActionedBySystemUserID As New IntegerElement("ActionedBySystemUserID", Me, False)
  Public ReadOnly _SubscriberBranchCode As New StringElement("SubscriberBranchCode", Me, False)
  Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
  Public ReadOnly _ActionedBySystemUser As SystemUserRelationship = ActionedBySystemUser
  Public ReadOnly _SubscriberProfileProductAuthenticationBranch As SubscriberProfileProductAuthenticationBranchRelationship = SubscriberProfileProductAuthenticationBranch
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moAuthenticationTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property AuthenticationTypeIndOptions() As IndicatorOptions
    Get
      If moAuthenticationTypeIndOptions Is Nothing Then
        moAuthenticationTypeIndOptions = New IndicatorOptions
        moAuthenticationTypeIndOptions.Add("P", "Primary")
        moAuthenticationTypeIndOptions.Add("S", "Secondary")
        moAuthenticationTypeIndOptions.Add("All", "All")
      End If
      Return moAuthenticationTypeIndOptions
    End Get
  End Property
  Private Shared moAuthenticationStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property AuthenticationStatusIndOptions() As IndicatorOptions
    Get
      If moAuthenticationStatusIndOptions Is Nothing Then
        moAuthenticationStatusIndOptions = New IndicatorOptions
        moAuthenticationStatusIndOptions.Add("P", "Pending")
        moAuthenticationStatusIndOptions.Add("A", "Authenticated")
        moAuthenticationStatusIndOptions.Add("N", "Not Authenticated")
        moAuthenticationStatusIndOptions.Add("V", "Voided")
        moAuthenticationStatusIndOptions.Add("All", "All")
      End If
      Return moAuthenticationStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("ReportAuthenticationRepeatAuthenticationStatisticsDetail", ApplicationSettings)
    _Subscriber._SubscriberID.LocalElement = _SubscriberID
    _ActionedBySystemUser._SystemUserID.LocalElement = _ActionedBySystemUserID
    _SubscriberProfileProductAuthenticationBranch._SubscriberBranchCode.LocalElement = _SubscriberBranchCode
    _SubscriberProfileProductAuthenticationBranch._SubscriberID.LocalElement = _SubscriberID
    'sembleWare: Constructor End - Do Not Modify
    AddHandler ActionedBySystemUser.LimitRelatedList, AddressOf ActionedBySystemUser_LimitRelatedList
  End Sub

#End Region 'sembleWare: Constructor

#Region " LimitRelatedList Events"
  Private Sub ActionedBySystemUser_LimitRelatedList(ByVal List As List)
    List.LimitByRelatedPart(Me.Subscriber.Instance, "Subscriber")
  End Sub
#End Region

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class ReportAuthenticationRepeatAuthenticationStatisticsDetailRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New ReportAuthenticationRepeatAuthenticationStatisticsDetail(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
