Imports sembleWare.Runtime
Imports System
Public Class SubscriberProfileProductAuthenticationBranch 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly SubscriberBranchCode As New StringElement("SubscriberBranchCode", Me, True, True, True, True, True, 20, "Branch Code", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SubscriberBranchDesc As New StringElement("SubscriberBranchDesc", Me, False, True, True, True, True, 50, "Branch Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly IsHighFraudInd As New IndicatorElement("IsHighFraudInd", Me, False, False, True, True, True, True, "Is High Fraud?", Nothing, SubscriberProfileProductAuthenticationBranch.IsHighFraudIndOptions, Nothing)
  Public ReadOnly IsHighDisputeInd As New IndicatorElement("IsHighDisputeInd", Me, False, False, True, True, True, True, "Is High Dispute?", Nothing, SubscriberProfileProductAuthenticationBranch.IsHighDisputeIndOptions, Nothing)
  Public ReadOnly SubscriberBranchDescription As New StringElement("SubscriberBranchDescription", Me, False, True, True, False, False, 250, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SubscriberProfile As Relationship = New SubscriberProfileRelationship("SubscriberProfile", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _SubscriberProfile As SubscriberProfileRelationship = SubscriberProfile
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moIsHighFraudIndOptions As IndicatorOptions
  Public Shared ReadOnly Property IsHighFraudIndOptions() As IndicatorOptions
    Get
      If moIsHighFraudIndOptions Is Nothing Then
        moIsHighFraudIndOptions = New IndicatorOptions
        moIsHighFraudIndOptions.Add("Y", "Yes")
        moIsHighFraudIndOptions.Add("N", "No")
      End If
      Return moIsHighFraudIndOptions
    End Get
  End Property
  Private Shared moIsHighDisputeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property IsHighDisputeIndOptions() As IndicatorOptions
    Get
      If moIsHighDisputeIndOptions Is Nothing Then
        moIsHighDisputeIndOptions = New IndicatorOptions
        moIsHighDisputeIndOptions.Add("Y", "Yes")
        moIsHighDisputeIndOptions.Add("N", "No")
      End If
      Return moIsHighDisputeIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberProfileProductAuthenticationBranch", ApplicationSettings)
    _SubscriberProfile._SubscriberID.LocalElement = _SubscriberID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRSubscriberBranchDescription()
  End Sub

  Public Overrides Sub Load()
    MyBase.Load()
    BRSubscriberBranchDescription()
  End Sub
#End Region

#Region " Busioness Rules"
  Private Sub BRSubscriberBranchDescription()
    If Me.IsNew Then
      Me.SubscriberBranchDescription.Value = "New Branch"
    Else
      Me.SubscriberBranchDescription.Value = Me.SubscriberBranchCode.ValueAsObject & " - " & Me.SubscriberBranchDesc.Value
    End If
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class SubscriberProfileProductAuthenticationBranchRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberBranchCode As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberProfileProductAuthenticationBranch(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberBranchCode.ForeignElement = CType(Instance, SubscriberProfileProductAuthenticationBranch).SubscriberBranchCode
    _SubscriberID.ForeignElement = CType(Instance, SubscriberProfileProductAuthenticationBranch)._SubscriberID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberProfileProductAuthenticationBranch")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberProfileProductAuthenticationBranch].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberBranchCode", "[A0SubscriberProfileProductAuthenticationBranch].[SubscriberBranchCode]", "SubscriberBranchCode", DataType.String, Nothing, True, 1, True, 100, "Branch Code"))
    oList.Columns.Add(New ListColumn("SubscriberBranchDesc", "[A0SubscriberProfileProductAuthenticationBranch].[SubscriberBranchDesc]", "SubscriberBranchDesc", DataType.String, Nothing, False, 0, True, 200, "Branch Description"))
    oList.Columns.Add(New ListColumn("IsHighFraudInd", "[A0SubscriberProfileProductAuthenticationBranch].[IsHighFraudInd]", "IsHighFraudInd", DataType.String, Nothing, False, 0, True, 100, "Is High Fraud?"))
    oList.Columns.Add(New ListColumn("IsHighDisputeInd", "[A0SubscriberProfileProductAuthenticationBranch].[IsHighDisputeInd]", "IsHighDisputeInd", DataType.String, Nothing, False, 0, True, 100, "Is High Dispute?"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberProfileProductAuthenticationBranch]  [A0SubscriberProfileProductAuthenticationBranch]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function DropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberProfileProductAuthenticationBranch")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberProfileProductAuthenticationBranch].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberBranchCode", "[A0SubscriberProfileProductAuthenticationBranch].[SubscriberBranchCode]", "SubscriberBranchCode", DataType.String, Nothing, True, 1, True, 100, "Branch Code"))
    oList.Columns.Add(New ListColumn("SubscriberBranchDesc", "[A0SubscriberProfileProductAuthenticationBranch].[SubscriberBranchDesc]", "SubscriberBranchDesc", DataType.String, Nothing, False, 0, True, 200, "Branch Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberProfileProductAuthenticationBranch]  [A0SubscriberProfileProductAuthenticationBranch]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function SimpleDropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberProfileProductAuthenticationBranch")
    oList.Columns.Add(New ListColumn("SubscriberBranchCode", "[A0SubscriberProfileProductAuthenticationBranch].[SubscriberBranchCode]", "SubscriberBranchCode", DataType.String, Nothing, True, 1, False, 100, "Branch Code"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberProfileProductAuthenticationBranch].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberBranchDescription", "isnull(SubscriberBranchCode, '') + ' - ' + isnull(SubscriberBranchDesc, '')", "SubscriberBranchDescription", DataType.String, Nothing, False, 0, True, 200, "Subscriber Branch Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberProfileProductAuthenticationBranch]  [A0SubscriberProfileProductAuthenticationBranch]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
