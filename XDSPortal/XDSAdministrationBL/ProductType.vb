Imports sembleWare.Runtime
Imports System
Public Class ProductType 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly ProductTypeID As New IntegerElement("ProductTypeID", Me, True, True, True, True, True, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ProductTypeDesc As New StringElement("ProductTypeDesc", Me, False, True, True, True, True, 50, "Product Type Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("ProductType", ApplicationSettings)
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Shared Procedures"
  Public Shared Function GetProductType(ByVal ProductTypeID As Integer, ByVal ApplicationSettings As ApplicationSettings) As ProductType
    Dim oProductType As ProductType = New ProductTypeRelationship(ApplicationSettings).NewInstance()

    oProductType.ProductTypeID.Load(ProductTypeID)
    oProductType.Load()

    Return oProductType
  End Function

  Public Shared Function GetConsumerAuthentication(ByVal ApplicationSettings As ApplicationSettings) As ProductType
    Return GetProductType(Constants.ProductType.Records.ConsumerAuthentication, ApplicationSettings)
  End Function

  Public Shared Function GetConsumerTrace(ByVal ApplicationSettings As ApplicationSettings) As ProductType
    Return GetProductType(Constants.ProductType.Records.ConsumerTrace, ApplicationSettings)
  End Function

  Public Shared Function GetCommercialEnquiry(ByVal ApplicationSettings As ApplicationSettings) As ProductType
    Return GetProductType(Constants.ProductType.Records.CommercialEnquiry, ApplicationSettings)
  End Function

  Public Shared Function GetConsumerCreditGrantor(ByVal ApplicationSettings As ApplicationSettings) As ProductType
    Return GetProductType(Constants.ProductType.Records.ConsumerCreditGrantor, ApplicationSettings)
  End Function
#End Region
End Class 'sembleWare: Part

Public Class ProductTypeRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _ProductTypeID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New ProductType(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _ProductTypeID.ForeignElement = CType(Instance, ProductType).ProductTypeID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function DropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0ProductType")
    oList.Columns.Add(New ListColumn("ProductTypeID", "[A0ProductType].[ProductTypeID]", "ProductTypeID", DataType.Integer, Nothing, True, 0, False, 100, "Product Type ID"))
    oList.Columns.Add(New ListColumn("ProductTypeDesc", "[A0ProductType].[ProductTypeDesc]", "ProductTypeDesc", DataType.String, Nothing, False, 1, True, 200, "Product Type Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "[ProductType]  [A0ProductType]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0ProductType")
    oList.Columns.Add(New ListColumn("ProductTypeID", "[A0ProductType].[ProductTypeID]", "ProductTypeID", DataType.Integer, Nothing, True, 0, False, 100, "Product Type ID"))
    oList.Columns.Add(New ListColumn("ProductTypeDesc", "[A0ProductType].[ProductTypeDesc]", "ProductTypeDesc", DataType.String, Nothing, False, 0, True, 200, "Product Type Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "[ProductType]  [A0ProductType]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0ProductType")
    oList.Columns.Add(New ListColumn("ProductTypeID", "[A0ProductType].[ProductTypeID]", "ProductTypeID", DataType.Integer, Nothing, True, 0, False, 100, "Enquiry Product Type ID"))
    oList.Columns.Add(New ListColumn("ProductTypeDesc", "[A0ProductType].[ProductTypeDesc]", "ProductTypeDesc", DataType.String, Nothing, False, 1, True, 200, "Product Type Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "[ProductType]  [A0ProductType]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
