Imports sembleWare.Runtime
Imports System
Public Class SPConsumer 'sembleWare: Part
  Inherits CustomMemoryPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly IDNo_SP As New StringElement("IDNo_SP", Me, False, True, True, True, False, 13, "ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PassportNo_SP As New StringElement("PassportNo_SP", Me, False, True, True, True, False, 16, "Passport No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Surname_SP As New StringElement("Surname_SP", Me, False, True, True, True, False, 100, "Surname", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FirstName_SP As New StringElement("FirstName_SP", Me, False, True, True, True, False, 100, "First Name", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FirstInitial_SP As New StringElement("FirstInitial_SP", Me, False, True, True, True, False, 1, "First Initial", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly BirthDate_SP As New DateTimeElement("BirthDate_SP", Me, False, True, True, True, False, "Date of Birth", Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly GenderInd_SP As New IndicatorElement("GenderInd_SP", Me, False, False, True, True, True, False, "Gender", Nothing, SPConsumer.GenderInd_SPOptions, "All")
  Public ReadOnly SearchCriteriaInd_SP As New IndicatorElement("SearchCriteriaInd_SP", Me, False, False, True, True, True, False, "Search Criteria", Nothing, SPConsumer.SearchCriteriaInd_SPOptions, "U")
  Public ReadOnly Consumer_SP As Relationship = New ConsumerRelationship("Consumer_SP", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _ConsumerID As New IntegerElement("ConsumerID", Me, False)
  Public ReadOnly _Consumer_SP As ConsumerRelationship = Consumer_SP
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moGenderInd_SPOptions As IndicatorOptions
  Public Shared ReadOnly Property GenderInd_SPOptions() As IndicatorOptions
    Get
      If moGenderInd_SPOptions Is Nothing Then
        moGenderInd_SPOptions = New IndicatorOptions
        moGenderInd_SPOptions.Add("M", "Male")
        moGenderInd_SPOptions.Add("F", "Female")
        moGenderInd_SPOptions.Add("All", "All")
      End If
      Return moGenderInd_SPOptions
    End Get
  End Property
  Private Shared moSearchCriteriaInd_SPOptions As IndicatorOptions
  Public Shared ReadOnly Property SearchCriteriaInd_SPOptions() As IndicatorOptions
    Get
      If moSearchCriteriaInd_SPOptions Is Nothing Then
        moSearchCriteriaInd_SPOptions = New IndicatorOptions
        moSearchCriteriaInd_SPOptions.Add("S", "System Defaults")
        moSearchCriteriaInd_SPOptions.Add("U", "User Specified")
      End If
      Return moSearchCriteriaInd_SPOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SPConsumer", ApplicationSettings)
    _Consumer_SP._ConsumerID.LocalElement = _ConsumerID
    AddHandler SearchCriteriaInd_SP.Changed, AddressOf SearchCriteriaInd_SP_Changed
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub SearchCriteriaInd_SP_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BRSearchCriteriaInd_SP(Element, True, True)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreate()
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreate()
    BRSearchCriteriaInd_SP(Me.SearchCriteriaInd_SP, True, False)
  End Sub

  Private Sub BRSearchCriteriaInd_SP(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If SetEnableDisableYN Then
      Me.IDNo_SP.Enabled = False
      Me.PassportNo_SP.Enabled = False
      Me.FirstInitial_SP.Enabled = False
      Me.FirstName_SP.Enabled = False
      Me.Surname_SP.Enabled = False
      Me.IDNo_SP.Enabled = False
      Me.GenderInd_SP.Enabled = False
      Select Case Element.ValueAsObject
        Case "S"
        Case "U"
          Me.IDNo_SP.Enabled = True
          Me.PassportNo_SP.Enabled = True
          Me.FirstInitial_SP.Enabled = True
          Me.FirstName_SP.Enabled = True
          Me.Surname_SP.Enabled = True
          Me.IDNo_SP.Enabled = True
          Me.GenderInd_SP.Enabled = True
      End Select
    End If

    If PerformRulesYN Then
      Select Case Element.ValueAsObject
        Case "S"
          Dim oConsumer As Consumer = Me.Consumer_SP.Instance
          Me.IDNo_SP.Value = oConsumer.IDNo.Value
          Me.PassportNo_SP.Value = oConsumer.PassportNo.Value
          Me.FirstInitial_SP.Value = oConsumer.FirstInitial.Value
          Me.FirstName_SP.Value = oConsumer.FirstName.Value
          Me.Surname_SP.Value = oConsumer.Surname.Value
          Me.IDNo_SP.Value = oConsumer.IDNo.Value
          Me.GenderInd_SP.Value = oConsumer.GenderInd.Value
        Case "U"
          Me.IDNo_SP.SetToNullValue()
          Me.PassportNo_SP.SetToNullValue()
          Me.FirstInitial_SP.SetToNullValue()
          Me.FirstName_SP.SetToNullValue()
          Me.Surname_SP.SetToNullValue()
          Me.IDNo_SP.SetToNullValue()
          Me.GenderInd_SP.Value = "All"
      End Select
    End If
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class SPConsumerRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SPConsumer(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
