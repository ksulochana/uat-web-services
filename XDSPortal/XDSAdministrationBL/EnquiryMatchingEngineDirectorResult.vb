Imports sembleWare.Runtime
Imports System
Public Class EnquiryMatchingEngineDirectorResult 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly IgnoreRecordYN As New BooleanElement("IgnoreRecordYN", Me, False, True, True, True, True, "Ignore Record?", Nothing, False, "Yes;No")
  Public ReadOnly EnquiryMatchingEngineDirector As Relationship = New EnquiryMatchingEngineDirectorRelationship("EnquiryMatchingEngineDirector", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly Director As Relationship = New DirectorRelationship("Director", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _EnquiryMatchingEngineID As New IntegerElement("EnquiryMatchingEngineID", Me, True)
  Public ReadOnly _DirectorID As New IntegerElement("DirectorID", Me, True)
  Public ReadOnly _EnquiryMatchingEngineDirectorID As New IntegerElement("EnquiryMatchingEngineDirectorID", Me, True)
  Public ReadOnly _EnquiryMatchingEngineDirector As EnquiryMatchingEngineDirectorRelationship = EnquiryMatchingEngineDirector
  Public ReadOnly _Director As DirectorRelationship = Director
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("EnquiryMatchingEngineDirectorResult", ApplicationSettings)
    _EnquiryMatchingEngineDirector._EnquiryMatchingEngineID.LocalElement = _EnquiryMatchingEngineID
    _EnquiryMatchingEngineDirector._EnquiryMatchingEngineDirectorID.LocalElement = _EnquiryMatchingEngineDirectorID
    _Director._DirectorID.LocalElement = _DirectorID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class EnquiryMatchingEngineDirectorResultRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _EnquiryMatchingEngineID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _DirectorID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _EnquiryMatchingEngineDirectorID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New EnquiryMatchingEngineDirectorResult(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _EnquiryMatchingEngineID.ForeignElement = CType(Instance, EnquiryMatchingEngineDirectorResult)._EnquiryMatchingEngineID
    _DirectorID.ForeignElement = CType(Instance, EnquiryMatchingEngineDirectorResult)._DirectorID
    _EnquiryMatchingEngineDirectorID.ForeignElement = CType(Instance, EnquiryMatchingEngineDirectorResult)._EnquiryMatchingEngineDirectorID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
