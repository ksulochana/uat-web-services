Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("XDS Administration Credit Bureau System")> 
<Assembly: AssemblyDescription("XDS Administration System - Business Layer")> 
<Assembly: AssemblyCompany("Shandon Business Solutions")> 
<Assembly: AssemblyProduct("XDS Administration Credit Bureau System")> 
<Assembly: AssemblyCopyright("Copyright @ SHBS 2008")> 
<Assembly: AssemblyTrademark("XDS Administration Credit Bureau System")> 
<Assembly: CLSCompliant(True)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("B2A24646-7DBD-44CE-BC3B-2DF29FC115DE")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("1.3.2.2")> 
