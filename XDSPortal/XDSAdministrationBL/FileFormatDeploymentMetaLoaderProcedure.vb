Imports sembleWare.Runtime
Imports System
Public Class FileFormatDeploymentMetaLoaderProcedure 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly TemplateDetails As New TextElement("TemplateDetails", Me, True, True, True, False, Nothing, Nothing, Nothing)
  Public ReadOnly FileFormatDeployment As Relationship = New FileFormatDeploymentRelationship("FileFormatDeployment", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly MetaDatabase As Relationship = New MetaDatabaseRelationship("MetaDatabase", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly LoaderProcessType As Relationship = New LoaderProcessTypeRelationship("LoaderProcessType", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly FileFormat As Relationship = New FileFormatRelationship("FileFormat", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _FileFormatDeploymentID As New IntegerElement("FileFormatDeploymentID", Me, True)
  Public ReadOnly _MetaDatabaseCode As New StringElement("MetaDatabaseCode", Me, True)
  Public ReadOnly _LoaderProcessTypeCode As New StringElement("LoaderProcessTypeCode", Me, True)
  Public ReadOnly _FileFormatCode As New StringElement("FileFormatCode", Me, True)
  Public ReadOnly _FileFormatDeployment As FileFormatDeploymentRelationship = FileFormatDeployment
  Public ReadOnly _MetaDatabase As MetaDatabaseRelationship = MetaDatabase
  Public ReadOnly _LoaderProcessType As LoaderProcessTypeRelationship = LoaderProcessType
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  Public ReadOnly _FileFormat As FileFormatRelationship = FileFormat
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("FileFormatDeploymentMetaLoaderProcedure", ApplicationSettings)
    _FileFormatDeployment._FileFormatDeploymentID.LocalElement = _FileFormatDeploymentID
    _MetaDatabase._MetaDatabaseCode.LocalElement = _MetaDatabaseCode
    _LoaderProcessType._LoaderProcessTypeCode.LocalElement = _LoaderProcessTypeCode
    _FileFormat._FileFormatCode.LocalElement = _FileFormatCode
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Shared Procedures"
  Public Shared Sub InsertFileFormatDeploymentMetaLoaderProcedures(ByVal FileFormatDeployment As FileFormatDeployment, ByVal ApplicationSettings As ApplicationSettings)
    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand

    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spFileFormatDeploymentMetaLoaderProcedure_I_MetaLoaderProcedure"

    oCommand.Parameters.Add("@FileFormatDeploymentID", SqlDbType.Int)

    oCommand.Parameters("@FileFormatDeploymentID").Value = FileFormatDeployment.FileFormatDeploymentID.Value

    ApplicationSettings.ActionQuery(oCommand)
  End Sub

  Private Shared Function GetMetaLoaderProcedure(ByVal FileFormatDeployment As FileFormatDeployment, ByVal FileFormat As FileFormat, ByVal MetaDatabase As MetaDatabase, ByVal LoaderProcessType As LoaderProcessType, ByVal ApplicationSettings As ApplicationSettings) As FileFormatDeploymentMetaLoaderProcedure
    Dim oFileFormatDeploymentMetaLoaderProcedure As FileFormatDeploymentMetaLoaderProcedure = FileFormatDeployment.FileFormatDeploymentMetaLoaderProcedure_OwnMany.NewInstance()

    oFileFormatDeploymentMetaLoaderProcedure.IgnoreCacheOnce = True
    oFileFormatDeploymentMetaLoaderProcedure.MetaDatabase.Instance = MetaDatabase
    oFileFormatDeploymentMetaLoaderProcedure.LoaderProcessType.Instance = LoaderProcessType
    oFileFormatDeploymentMetaLoaderProcedure.FileFormat.Instance = FileFormat
    oFileFormatDeploymentMetaLoaderProcedure.Load()

    Return oFileFormatDeploymentMetaLoaderProcedure
  End Function

  Public Shared Function GetDataLoad(ByVal FileFormatDeployment As FileFormatDeployment, ByVal FileFormat As FileFormat, ByVal MetaDatabase As MetaDatabase, ByVal ApplicationSettings As ApplicationSettings) As FileFormatDeploymentMetaLoaderProcedure
    Return GetMetaLoaderProcedure(FileFormatDeployment, FileFormat, MetaDatabase, XDSAdministrationBL.LoaderProcessType.GetDataLoad(ApplicationSettings), ApplicationSettings)
  End Function

  Public Shared Function GetDataValidation(ByVal FileFormatDeployment As FileFormatDeployment, ByVal FileFormat As FileFormat, ByVal MetaDatabase As MetaDatabase, ByVal ApplicationSettings As ApplicationSettings) As FileFormatDeploymentMetaLoaderProcedure
    Return GetMetaLoaderProcedure(FileFormatDeployment, FileFormat, MetaDatabase, XDSAdministrationBL.LoaderProcessType.GetDataValidation(ApplicationSettings), ApplicationSettings)
  End Function

  Public Shared Function GetDataMatching(ByVal FileFormatDeployment As FileFormatDeployment, ByVal FileFormat As FileFormat, ByVal MetaDatabase As MetaDatabase, ByVal ApplicationSettings As ApplicationSettings) As FileFormatDeploymentMetaLoaderProcedure
    Return GetMetaLoaderProcedure(FileFormatDeployment, FileFormat, MetaDatabase, XDSAdministrationBL.LoaderProcessType.GetDataMatching(ApplicationSettings), ApplicationSettings)
  End Function

  Public Shared Function GetDataUpdate(ByVal FileFormatDeployment As FileFormatDeployment, ByVal FileFormat As FileFormat, ByVal MetaDatabase As MetaDatabase, ByVal ApplicationSettings As ApplicationSettings) As FileFormatDeploymentMetaLoaderProcedure
    Return GetMetaLoaderProcedure(FileFormatDeployment, FileFormat, MetaDatabase, XDSAdministrationBL.LoaderProcessType.GetDataUpdate(ApplicationSettings), ApplicationSettings)
  End Function

  Public Shared Function GetDataUndo(ByVal FileFormatDeployment As FileFormatDeployment, ByVal FileFormat As FileFormat, ByVal MetaDatabase As MetaDatabase, ByVal ApplicationSettings As ApplicationSettings) As FileFormatDeploymentMetaLoaderProcedure
    Return GetMetaLoaderProcedure(FileFormatDeployment, FileFormat, MetaDatabase, XDSAdministrationBL.LoaderProcessType.GetDataUndo(ApplicationSettings), ApplicationSettings)
  End Function
#End Region
End Class 'sembleWare: Part

Public Class FileFormatDeploymentMetaLoaderProcedureRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _FileFormatDeploymentID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _MetaDatabaseCode As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _LoaderProcessTypeCode As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _FileFormatCode As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New FileFormatDeploymentMetaLoaderProcedure(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _FileFormatDeploymentID.ForeignElement = CType(Instance, FileFormatDeploymentMetaLoaderProcedure)._FileFormatDeploymentID
    _MetaDatabaseCode.ForeignElement = CType(Instance, FileFormatDeploymentMetaLoaderProcedure)._MetaDatabaseCode
    _LoaderProcessTypeCode.ForeignElement = CType(Instance, FileFormatDeploymentMetaLoaderProcedure)._LoaderProcessTypeCode
    _FileFormatCode.ForeignElement = CType(Instance, FileFormatDeploymentMetaLoaderProcedure)._FileFormatCode
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0FileFormatDeploymentMetaLoaderProcedure")
    oList.Columns.Add(New ListColumn("FileFormatDeploymentID", "[A0FileFormatDeploymentMetaLoaderProcedure].[FileFormatDeploymentID]", "FileFormatDeploymentID", DataType.Integer, Nothing, True, 1, True, 100, "File Format Deployment ID"))
    oList.Columns.Add(New ListColumn("MetaDatabaseCode", "[A0FileFormatDeploymentMetaLoaderProcedure].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, True, 0, True, 100, "Meta Database Code"))
    oList.Columns.Add(New ListColumn("LoaderProcessTypeCode", "[A0FileFormatDeploymentMetaLoaderProcedure].[LoaderProcessTypeCode]", "LoaderProcessTypeCode", DataType.String, Nothing, True, 0, True, 100, "Loader Process Type Code"))
    oList.Columns.Add(New ListColumn("FileFormatCode", "[A0FileFormatDeploymentMetaLoaderProcedure].[FileFormatCode]", "FileFormatCode", DataType.String, Nothing, True, 0, True, 100, "File Format Code"))
    oList.SelectStatement = "select"
    oList.FromClause = "[FileFormatDeploymentMetaLoaderProcedure]  [A0FileFormatDeploymentMetaLoaderProcedure]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
