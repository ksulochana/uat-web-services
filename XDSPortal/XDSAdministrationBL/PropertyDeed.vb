Imports sembleWare.Runtime
Imports System
Public Class PropertyDeed 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly PropertyDeedID As New IdentityElement("PropertyDeedID", Me, True, Nothing, Nothing)
  Public ReadOnly BuyerIDNo As New StringElement("BuyerIDNo", Me, False, True, True, True, False, 13, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly BuyerName As New StringElement("BuyerName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly BuyerTypeCode As New StringElement("BuyerTypeCode", Me, False, True, True, True, False, 10, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly BuyerSharePerc As New DecimalElement("BuyerSharePerc", Me, False, True, True, True, False, Nothing, Nothing, "##0.00", Nothing, Nothing, Nothing)
  Public ReadOnly TitleDeedNo As New StringElement("TitleDeedNo", Me, False, True, True, True, False, 20, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SellerIDNo As New StringElement("SellerIDNo", Me, False, True, True, True, False, 13, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SellerName As New StringElement("SellerName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SellerTypeCode As New StringElement("SellerTypeCode", Me, False, True, True, True, False, 10, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly OldTitleDeedNo As New StringElement("OldTitleDeedNo", Me, False, True, True, True, False, 20, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly StreetNo As New StringElement("StreetNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly StreetName As New StringElement("StreetName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SuburbName As New StringElement("SuburbName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CityName As New StringElement("CityName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AuthorityName As New StringElement("AuthorityName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly TownshipName As New StringElement("TownshipName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FarmName As New StringElement("FarmName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RegistrarName As New StringElement("RegistrarName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RegistrarDivision As New StringElement("RegistrarDivision", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly StandNo As New StringElement("StandNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PortionNo As New StringElement("PortionNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SchemeName As New StringElement("SchemeName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ErfNo As New StringElement("ErfNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ErfSize As New StringElement("ErfSize", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AttorneyFileNo As New StringElement("AttorneyFileNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AttorneyFirmNo As New StringElement("AttorneyFirmNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly TransferDate As New DateTimeElement("TransferDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly PurchaseDate As New DateTimeElement("PurchaseDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly PurchasePriceAmt As New DecimalElement("PurchasePriceAmt", Me, False, True, True, True, False, Nothing, Nothing, "c", Nothing, Nothing, Nothing)
  Public ReadOnly TitleDeedFeeAmt As New DecimalElement("TitleDeedFeeAmt", Me, False, True, True, True, False, Nothing, Nothing, "c", Nothing, Nothing, Nothing)
  Public ReadOnly BondAccountNo As New StringElement("BondAccountNo", Me, False, True, True, True, False, 20, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly BondHolderName As New StringElement("BondHolderName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly BondAmt As New DecimalElement("BondAmt", Me, False, True, True, True, False, Nothing, Nothing, "c", Nothing, Nothing, Nothing)
  Public ReadOnly Restant As New StringElement("Restant", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AdditionalDesc As New StringElement("AdditionalDesc", Me, False, True, True, True, False, 100, "Additional Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ExemptInd As New BooleanElement("ExemptInd", Me, False, True, True, True, False, "Additional Description", Nothing, Nothing, "Yes;No")
  Public ReadOnly ExternalSourceID As New IntegerElement("ExternalSourceID", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ExternalSourceUpdatedDate As New DateTimeElement("ExternalSourceUpdatedDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly TransferID As New IntegerElement("TransferID", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RecordStatusInd As New IndicatorElement("RecordStatusInd", Me, False, False, False, True, True, True, "Record Status", Nothing, PropertyDeed.RecordStatusIndOptions, "A")
  Public ReadOnly DeletedReason As New StringElement("DeletedReason", Me, False, True, True, True, False, 500, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LastUpdatedDate As New DateTimeElement("LastUpdatedDate", Me, False, False, True, True, True, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly BuyerMaritalStatus As Relationship = New MaritalStatusRelationship("BuyerMaritalStatus", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly SellerMaritalStatus As Relationship = New MaritalStatusRelationship("SellerMaritalStatus", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly CountryRegion As Relationship = New CountryRegionRelationship("CountryRegion", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Loader As Relationship = New LoaderRelationship("Loader", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly PropertyType As Relationship = New PropertyTypeRelationship("PropertyType", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _BuyerMaritalStatusCode As New StringElement("BuyerMaritalStatusCode", Me, False)
  Public ReadOnly _SellerMaritalStatusCode As New StringElement("SellerMaritalStatusCode", Me, False)
  Public ReadOnly _CountryRegionCode As New StringElement("CountryRegionCode", Me, False)
  Public ReadOnly _CountryCode As New StringElement("CountryCode", Me, False)
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, False)
  Public ReadOnly _LoaderID As New IntegerElement("LoaderID", Me, False)
  Public ReadOnly _PropertyTypeCode As New StringElement("PropertyTypeCode", Me, False)
  Public ReadOnly _BuyerMaritalStatus As MaritalStatusRelationship = BuyerMaritalStatus
  Public ReadOnly _SellerMaritalStatus As MaritalStatusRelationship = SellerMaritalStatus
  Public ReadOnly _CountryRegion As CountryRegionRelationship = CountryRegion
  Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
  Public ReadOnly _Loader As LoaderRelationship = Loader
  Public ReadOnly _PropertyType As PropertyTypeRelationship = PropertyType
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moRecordStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property RecordStatusIndOptions() As IndicatorOptions
    Get
      If moRecordStatusIndOptions Is Nothing Then
        moRecordStatusIndOptions = New IndicatorOptions
        moRecordStatusIndOptions.Add("A", "Active")
        moRecordStatusIndOptions.Add("D", "Deleted")
      End If
      Return moRecordStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("PropertyDeed", ApplicationSettings)
    _BuyerMaritalStatus._MaritalStatusCode.LocalElement = _BuyerMaritalStatusCode
    _SellerMaritalStatus._MaritalStatusCode.LocalElement = _SellerMaritalStatusCode
    _CountryRegion._CountryRegionCode.LocalElement = _CountryRegionCode
    _CountryRegion._CountryCode.LocalElement = _CountryCode
    _Subscriber._SubscriberID.LocalElement = _SubscriberID
    _Loader._LoaderID.LocalElement = _LoaderID
    _PropertyType._PropertyTypeCode.LocalElement = _PropertyTypeCode
    'sembleWare: Constructor End - Do Not Modify
    Me.DeleteType = CustomDBPart.enumDeleteType.ChangeRecordStatus
  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class PropertyDeedRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _PropertyDeedID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New PropertyDeed(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _PropertyDeedID.ForeignElement = CType(Instance, PropertyDeed).PropertyDeedID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
