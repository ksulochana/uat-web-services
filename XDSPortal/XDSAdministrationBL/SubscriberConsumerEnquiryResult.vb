Imports sembleWare.Runtime
Imports System
Public Class SubscriberConsumerEnquiryResult 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly IDNo As New StringElement("IDNo", Me, False, False, True, True, False, 13, "ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PassportNo As New StringElement("PassportNo", Me, False, False, True, True, False, 16, "Passport No / Other ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FirstInitial As New StringElement("FirstInitial", Me, False, False, True, True, False, 1, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SecondInitial As New StringElement("SecondInitial", Me, False, False, True, True, False, 1, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FirstName As New StringElement("FirstName", Me, False, False, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SecondName As New StringElement("SecondName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Surname As New StringElement("Surname", Me, False, False, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly BirthDate As New DateTimeElement("BirthDate", Me, False, False, True, True, False, "Date of Birth", Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly GenderInd As New IndicatorElement("GenderInd", Me, False, False, False, True, True, False, "Gender", Nothing, SubscriberConsumerEnquiryResult.GenderIndOptions, Nothing)
  Public ReadOnly DetailsViewedYN As New BooleanElement("DetailsViewedYN", Me, False, False, True, True, True, "Details Viewed?", Nothing, False, "Yes;No")
  Public ReadOnly DetailsViewedDate As New DateTimeElement("DetailsViewedDate", Me, False, False, True, True, False, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly ConsumerSelectedYN As New BooleanElement("ConsumerSelectedYN", Me, False, True, True, True, True, "Consumer Selected?", Nothing, False, "Yes;No")
  Public ReadOnly SubscriberConsumerEnquiry As Relationship = New SubscriberConsumerEnquiryRelationship("SubscriberConsumerEnquiry", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly Consumer As Relationship = New ConsumerRelationship("Consumer", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _SubscriberConsumerEnquiryID As New IntegerElement("SubscriberConsumerEnquiryID", Me, True)
  Public ReadOnly _ConsumerID As New IntegerElement("ConsumerID", Me, True)
  Public ReadOnly _SubscriberConsumerEnquiry As SubscriberConsumerEnquiryRelationship = SubscriberConsumerEnquiry
  Public ReadOnly _Consumer As ConsumerRelationship = Consumer
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moGenderIndOptions As IndicatorOptions
  Public Shared ReadOnly Property GenderIndOptions() As IndicatorOptions
    Get
      If moGenderIndOptions Is Nothing Then
        moGenderIndOptions = New IndicatorOptions
        moGenderIndOptions.Add("M", "Male")
        moGenderIndOptions.Add("F", "Female")
      End If
      Return moGenderIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberConsumerEnquiryResult", ApplicationSettings)
    _SubscriberConsumerEnquiry._SubscriberID.LocalElement = _SubscriberID
    _SubscriberConsumerEnquiry._SubscriberConsumerEnquiryID.LocalElement = _SubscriberConsumerEnquiryID
    _Consumer._ConsumerID.LocalElement = _ConsumerID
    AddHandler DetailsViewedYN.Changed, AddressOf DetailsViewedYN_Changed
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub DetailsViewedYN_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"
  Public Sub SelectRecord()
    With ApplicationSettings
      Try
        .BeginTransaction()
        Me.ConsumerSelectedYN.Value = True
        Me.DetailsViewedYN.Value = True
        Me.DetailsViewedDate.Value = System.DateTime.Now
        Me.Save()

        Dim oConsumer As Consumer = Me.Consumer.Instance
        Dim oSubscriberConsumerEnquiry As SubscriberConsumerEnquiry = Me.SubscriberConsumerEnquiry.Instance

        oSubscriberConsumerEnquiry.EnquiryResultInd.Value = Constants.SubscriberConsumerEnquiry.Results.RecordSelected

        oSubscriberConsumerEnquiry.ResultIDNo.Value = oConsumer.IDNo.Value
        oSubscriberConsumerEnquiry.ResultPassportNo.Value = oConsumer.PassportNo.Value
        oSubscriberConsumerEnquiry.ResultFirstInitial.Value = oConsumer.FirstInitial.Value
        oSubscriberConsumerEnquiry.ResultSecondInitial.Value = oConsumer.SecondInitial.Value
        oSubscriberConsumerEnquiry.ResultFirstName.Value = oConsumer.FirstName.Value
        oSubscriberConsumerEnquiry.ResultSecondName.Value = oConsumer.SecondName.Value
        oSubscriberConsumerEnquiry.ResultSurname.Value = oConsumer.Surname.Value
        oSubscriberConsumerEnquiry.ResultBirthDate.ValueAsObject = oConsumer.BirthDate.ValueAsObject
        oSubscriberConsumerEnquiry.ResultGenderInd.Value = oConsumer.GenderInd.Value

        oSubscriberConsumerEnquiry.Save()
        .CommitTransaction()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
    End With
  End Sub
#End Region 'sembleWare: Actions

#Region "Public Methods"
  Public Function GetMultipleSubscriberTraceConsumer() As SubscriberTraceConsumer


    Dim oSubscriberConsumerEnquiry As SubscriberConsumerEnquiry = Me.SubscriberConsumerEnquiry.Instance
    Dim oSubscriber As Subscriber = oSubscriberConsumerEnquiry.Subscriber.Instance

    Dim oList As List = oSubscriber._SubscriberTraceConsumer_OwnMany.SubscriberGridList
    Dim oDataRow As DataRow

    oList.LimitByRelatedPart(oSubscriberConsumerEnquiry, "SubscriberConsumerEnquiry")
    oList.LimitByRelatedPart(Me.Consumer.Instance, "Consumer")
    For Each oDataRow In oList.DataTable.Rows
      Dim oSubscriberTraceConsumer As SubscriberTraceConsumer = oSubscriber.SubscriberTraceConsumer_OwnMany.NewInstance

      oSubscriberTraceConsumer.SubscriberTraceConsumerID.Load(oDataRow("SubscriberTraceConsumerID"))
      oSubscriberTraceConsumer.Load()
      Return oSubscriberTraceConsumer
    Next
    Return Nothing

    Return Nothing
  End Function
#End Region


#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class SubscriberConsumerEnquiryResultRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SubscriberConsumerEnquiryID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _ConsumerID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberConsumerEnquiryResult(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberID.ForeignElement = CType(Instance, SubscriberConsumerEnquiryResult)._SubscriberID
    _SubscriberConsumerEnquiryID.ForeignElement = CType(Instance, SubscriberConsumerEnquiryResult)._SubscriberConsumerEnquiryID
    _ConsumerID.ForeignElement = CType(Instance, SubscriberConsumerEnquiryResult)._ConsumerID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberConsumerEnquiryResult")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberConsumerEnquiryResult].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberConsumerEnquiryID", "[A0SubscriberConsumerEnquiryResult].[SubscriberConsumerEnquiryID]", "SubscriberConsumerEnquiryID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber Consumer Enquiry ID"))
    oList.Columns.Add(New ListColumn("ConsumerID", "[A0SubscriberConsumerEnquiryResult].[ConsumerID]", "ConsumerID", DataType.Integer, Nothing, True, 0, False, 100, "Consumer ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A0SubscriberConsumerEnquiryResult].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("PassportNo", "[A0SubscriberConsumerEnquiryResult].[PassportNo]", "PassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No / Other ID No"))
    oList.Columns.Add(New ListColumn("FirstInitial", "[A0SubscriberConsumerEnquiryResult].[FirstInitial]", "FirstInitial", DataType.String, Nothing, False, 0, True, 100, "First Initial"))
    oList.Columns.Add(New ListColumn("FirstName", "[A0SubscriberConsumerEnquiryResult].[FirstName]", "FirstName", DataType.String, Nothing, False, 2, True, 150, "First Name"))
    oList.Columns.Add(New ListColumn("Surname", "[A0SubscriberConsumerEnquiryResult].[Surname]", "Surname", DataType.String, Nothing, False, 1, True, 150, "Surname"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A0SubscriberConsumerEnquiryResult].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Date of Birth"))
    oList.Columns.Add(New ListColumn("GenderInd", "[A0SubscriberConsumerEnquiryResult].[GenderInd]", "GenderInd", DataType.String, Nothing, False, 0, True, 100, "Gender"))
    oList.Columns.Add(New ListColumn("DetailsViewedYN", "[A0SubscriberConsumerEnquiryResult].[DetailsViewedYN]", "DetailsViewedYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Details Viewed?"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberConsumerEnquiryResult]  [A0SubscriberConsumerEnquiryResult]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function MultipleMatchGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberConsumerEnquiryResult")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberConsumerEnquiryResult].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberConsumerEnquiryID", "[A0SubscriberConsumerEnquiryResult].[SubscriberConsumerEnquiryID]", "SubscriberConsumerEnquiryID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber Consumer Enquiry ID"))
    oList.Columns.Add(New ListColumn("ConsumerID", "[A0SubscriberConsumerEnquiryResult].[ConsumerID]", "ConsumerID", DataType.Integer, Nothing, True, 0, False, 100, "Consumer ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A0SubscriberConsumerEnquiryResult].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("PassportNo", "[A0SubscriberConsumerEnquiryResult].[PassportNo]", "PassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No / Other ID No"))
    oList.Columns.Add(New ListColumn("FirstInitial", "[A0SubscriberConsumerEnquiryResult].[FirstInitial]", "FirstInitial", DataType.String, Nothing, False, 0, True, 100, "First Initial"))
    oList.Columns.Add(New ListColumn("FirstName", "[A0SubscriberConsumerEnquiryResult].[FirstName]", "FirstName", DataType.String, Nothing, False, 2, True, 150, "First Name"))
    oList.Columns.Add(New ListColumn("Surname", "[A0SubscriberConsumerEnquiryResult].[Surname]", "Surname", DataType.String, Nothing, False, 1, True, 150, "Surname"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A0SubscriberConsumerEnquiryResult].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Date of Birth"))
    oList.Columns.Add(New ListColumn("GenderInd", "[A0SubscriberConsumerEnquiryResult].[GenderInd]", "GenderInd", DataType.String, Nothing, False, 0, True, 100, "Gender"))
    oList.Columns.Add(New ListColumn("DetailsViewedYN", "[A0SubscriberConsumerEnquiryResult].[DetailsViewedYN]", "DetailsViewedYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Details Viewed?"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberConsumerEnquiryResult]  [A0SubscriberConsumerEnquiryResult]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberConsumerEnquiryResult")
    oList.Columns.Add(New ListColumn("ConsumerID", "[A0SubscriberConsumerEnquiryResult].[ConsumerID]", "ConsumerID", DataType.Integer, Nothing, True, 0, False, 100, "Consumer ID"))
    oList.Columns.Add(New ListColumn("SubscriberConsumerEnquiryID", "[A0SubscriberConsumerEnquiryResult].[SubscriberConsumerEnquiryID]", "SubscriberConsumerEnquiryID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber Consumer Enquiry ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberConsumerEnquiryResult].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A0SubscriberConsumerEnquiryResult].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("PassportNo", "[A0SubscriberConsumerEnquiryResult].[PassportNo]", "PassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No / Other ID No"))
    oList.Columns.Add(New ListColumn("Surname", "[A0SubscriberConsumerEnquiryResult].[Surname]", "Surname", DataType.String, Nothing, False, 0, True, 100, "Surname"))
    oList.Columns.Add(New ListColumn("FirstName", "[A0SubscriberConsumerEnquiryResult].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A0SubscriberConsumerEnquiryResult].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Date of Birth"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberConsumerEnquiryResult]  [A0SubscriberConsumerEnquiryResult]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
