Imports sembleWare.Runtime
Imports System
Public Class ConsumerNameSoundEx 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly FirstInitial As New StringElement("FirstInitial", Me, False, False, True, True, False, 1, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SecondInitial As New StringElement("SecondInitial", Me, False, False, True, True, False, 1, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FirstName As New StringElement("FirstName", Me, False, False, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SecondName As New StringElement("SecondName", Me, False, False, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ThirdName As New StringElement("ThirdName", Me, False, False, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Surname As New StringElement("Surname", Me, False, False, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly IDNo As New StringElement("IDNo", Me, False, False, True, True, False, 13, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PassportNo As New StringElement("PassportNo", Me, False, False, True, True, False, 16, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly BirthDate As New DateTimeElement("BirthDate", Me, False, False, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly GenderInd As New IndicatorElement("GenderInd", Me, False, False, False, True, True, False, "Gender", Nothing, ConsumerNameSoundEx.GenderIndOptions, Nothing)
  Public ReadOnly MaidenName As New StringElement("MaidenName", Me, False, False, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RecordStatusInd As New IndicatorElement("RecordStatusInd", Me, False, False, False, True, True, True, "Record Status", Nothing, ConsumerNameSoundEx.RecordStatusIndOptions, "A")
  Public ReadOnly Consumer As Relationship = New ConsumerRelationship("Consumer", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly ConsumerName As Relationship = New ConsumerNameRelationship("ConsumerName", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _ConsumerID As New IntegerElement("ConsumerID", Me, True)
  Public ReadOnly _ConsumerNameID As New IntegerElement("ConsumerNameID", Me, True)
  Public ReadOnly _Consumer As ConsumerRelationship = Consumer
  Public ReadOnly _ConsumerName As ConsumerNameRelationship = ConsumerName
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moGenderIndOptions As IndicatorOptions
  Public Shared ReadOnly Property GenderIndOptions() As IndicatorOptions
    Get
      If moGenderIndOptions Is Nothing Then
        moGenderIndOptions = New IndicatorOptions
        moGenderIndOptions.Add("M", "Male")
        moGenderIndOptions.Add("F", "Female")
      End If
      Return moGenderIndOptions
    End Get
  End Property
  Private Shared moRecordStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property RecordStatusIndOptions() As IndicatorOptions
    Get
      If moRecordStatusIndOptions Is Nothing Then
        moRecordStatusIndOptions = New IndicatorOptions
        moRecordStatusIndOptions.Add("A", "Active")
        moRecordStatusIndOptions.Add("D", "Deleted")
      End If
      Return moRecordStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("ConsumerNameSoundEx", ApplicationSettings)
    _Consumer._ConsumerID.LocalElement = _ConsumerID
    _ConsumerName._ConsumerNameID.LocalElement = _ConsumerNameID
    _ConsumerName._ConsumerID.LocalElement = _ConsumerID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class ConsumerNameSoundExRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _ConsumerID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _ConsumerNameID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New ConsumerNameSoundEx(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _ConsumerID.ForeignElement = CType(Instance, ConsumerNameSoundEx)._ConsumerID
    _ConsumerNameID.ForeignElement = CType(Instance, ConsumerNameSoundEx)._ConsumerNameID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
