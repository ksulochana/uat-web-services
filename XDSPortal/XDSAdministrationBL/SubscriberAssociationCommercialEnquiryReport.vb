Imports sembleWare.Runtime
Imports System
Public Class SubscriberAssociationCommercialEnquiryReport 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly DisplayCommercialInformationYN As New BooleanElement("DisplayCommercialInformationYN", Me, False, True, True, True, True, "Display Commercial Information?", Nothing, False, "Yes;No")
  Public ReadOnly DisplayAuditorInformationYN As New BooleanElement("DisplayAuditorInformationYN", Me, False, True, True, True, True, "Display Auditor Information?", Nothing, False, "Yes;No")
  Public ReadOnly DisplayDirectorInformationYN As New BooleanElement("DisplayDirectorInformationYN", Me, False, True, True, True, True, "Display Director Information?", Nothing, False, "Yes;No")
  Public ReadOnly DisplayEnquiryInformationYN As New BooleanElement("DisplayEnquiryInformationYN", Me, False, True, True, True, True, "Display Enquiry Information?", Nothing, False, "Yes;No")
    Public ReadOnly DisplayJudgementInformationYN As New BooleanElement("DisplayJudgementInformationYN", Me, False, True, True, True, True, "Display Judgement Information?", Nothing, False, "Yes;No")
    Public ReadOnly DisplayNameHistoryYN As New BooleanElement("DisplayNameHistoryYN", Me, False, True, True, True, True, "Display Name History?", Nothing, False, "Yes;No")
  Public ReadOnly DisplayAddressHistoryYN As New BooleanElement("DisplayAddressHistoryYN", Me, False, True, True, True, True, "Display Address History?", Nothing, False, "Yes;No")
    Public ReadOnly DisplayTelephoneHistoryYN As New BooleanElement("DisplayTelephoneHistoryYN", Me, False, True, True, True, True, "Display Telephone History?", Nothing, False, "Yes;No")
    Public ReadOnly DisplayPersonalJudgementInformationYN As New BooleanElement("DisplayPersonalJudgementInformationYN", Me, False, True, True, True, True, "Display Personal Judgement Information?", Nothing, False, "Yes;No")
  Public ReadOnly SubscriberAssociation As Relationship = New SubscriberAssociationRelationship("SubscriberAssociation", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberAssociationCode As New StringElement("SubscriberAssociationCode", Me, True)
  Public ReadOnly _SubscriberAssociation As SubscriberAssociationRelationship = SubscriberAssociation
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberAssociationCommercialEnquiryReport", ApplicationSettings)
    _SubscriberAssociation._SubscriberAssociationCode.LocalElement = _SubscriberAssociationCode
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Shared Procedures"
  Public Shared Function GetSubscriberAssociationCommercialEnquiryReport(ByVal SubscriberAssociationCode As String, ByVal ApplicationSettings As ApplicationSettings) As SubscriberAssociationCommercialEnquiryReport
    Dim oSubscriberAssociationCommercialEnquiryReport As SubscriberAssociationCommercialEnquiryReport = New SubscriberAssociationCommercialEnquiryReportRelationship(ApplicationSettings).NewInstance
    Try
      oSubscriberAssociationCommercialEnquiryReport._SubscriberAssociationCode.Load(SubscriberAssociationCode)
      oSubscriberAssociationCommercialEnquiryReport.Load()
    Catch oException As sembleWare.Runtime.RecordNotFoundException
      oSubscriberAssociationCommercialEnquiryReport = New SubscriberAssociationCommercialEnquiryReportRelationship(ApplicationSettings).NewInstance
      oSubscriberAssociationCommercialEnquiryReport._SubscriberAssociationCode.Load(SubscriberAssociationCode)
    Catch oException As Exception
      Throw oException
    End Try
    Return oSubscriberAssociationCommercialEnquiryReport
  End Function
#End Region
End Class 'sembleWare: Part

Public Class SubscriberAssociationCommercialEnquiryReportRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberAssociationCode As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberAssociationCommercialEnquiryReport(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberAssociationCode.ForeignElement = CType(Instance, SubscriberAssociationCommercialEnquiryReport)._SubscriberAssociationCode
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
