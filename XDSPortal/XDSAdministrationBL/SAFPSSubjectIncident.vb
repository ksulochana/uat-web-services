Imports sembleWare.Runtime
Imports System
Public Class SAFPSSubjectIncident 'sembleWare: Part
    Inherits CustomDBPart

#Region " Elements & Relationships"

    'sembleWare: Elements Start - Do Not Modify
    Public ReadOnly SAFPSSubjectIncidentID As New IdentityElement("SAFPSSubjectIncidentID", Me, True, Nothing, Nothing)
    Public ReadOnly MemRefNo As New StringElement("MemRefNo", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly LogDate As New DateTimeElement("LogDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
    Public ReadOnly IncidentDate As New DateTimeElement("IncidentDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
    Public ReadOnly SubjectsRole As New StringElement("SubjectsRole", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly City As New StringElement("City", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly SavingsAmt As New DecimalElement("SavingsAmt", Me, False, True, True, True, False, "Savings Amount", Nothing, "c", Nothing, Nothing, Nothing)
    Public ReadOnly LossAmt As New DecimalElement("LossAmt", Me, False, True, True, True, False, "Loss Amount", Nothing, "c", Nothing, Nothing, Nothing)
    Public ReadOnly Details As New TextElement("Details", Me, True, True, True, False, Nothing, Nothing, Nothing)
    Public ReadOnly IsVictimYN As New BooleanElement("IsVictimYN", Me, False, True, True, True, True, "Is Victim?", Nothing, False, "Yes;No")
    Public ReadOnly ITCNo As New StringElement("ITCNo", Me, False, True, True, True, False, 50, "ITC No", Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly IsForensicInfoYN As New BooleanElement("IsForensicInfoYN", Me, False, True, True, True, True, "Is Forensic Info?", Nothing, False, "Yes;No")
    Public ReadOnly ExternalSubjectSourceID As New IntegerElement("ExternalSubjectSourceID", Me, False, False, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly ExternalIncidentSourceID As New IntegerElement("ExternalIncidentSourceID", Me, False, False, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly RecordStatusInd As New IndicatorElement("RecordStatusInd", Me, False, False, False, True, True, True, "Record Status", Nothing, SAFPSSubjectIncident.RecordStatusIndOptions, "A")
    Public ReadOnly DeletedReason As New StringElement("DeletedReason", Me, False, True, True, True, False, 500, Nothing, Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly LastUpdatedDate As New DateTimeElement("LastUpdatedDate", Me, False, False, True, True, True, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
    Public ReadOnly IncidentCategory As Relationship = New IncidentCategoryRelationship("IncidentCategory", Nothing, RelationshipType.Include, False, True, True, Me)
    Public ReadOnly IncidentSubCategory As Relationship = New IncidentSubCategoryRelationship("IncidentSubCategory", Nothing, RelationshipType.Include, False, True, True, Me)
    Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.Include, False, False, True, Me)
    Public ReadOnly Loader As Relationship = New LoaderRelationship("Loader", Nothing, RelationshipType.Include, False, False, True, Me)
    Public ReadOnly SAFPSSubject As Relationship = New SAFPSSubjectRelationship("SAFPSSubject", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
    'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

    'sembleWare: Foreign Items Start - Do Not Modify
    Public ReadOnly _IncidentCategoryCode As New StringElement("IncidentCategoryCode", Me, False)
    Public ReadOnly _IncidentSubCategoryCode As New StringElement("IncidentSubCategoryCode", Me, False)
    Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, False)
    Public ReadOnly _LoaderID As New IntegerElement("LoaderID", Me, False)
    Public ReadOnly _SAFPSSubjectID As New IntegerElement("SAFPSSubjectID", Me, True)
    Public ReadOnly _IncidentCategory As IncidentCategoryRelationship = IncidentCategory
    Public ReadOnly _IncidentSubCategory As IncidentSubCategoryRelationship = IncidentSubCategory
    Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
    Public ReadOnly _Loader As LoaderRelationship = Loader
    Public ReadOnly _SAFPSSubject As SAFPSSubjectRelationship = SAFPSSubject
    'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

    'sembleWare: Indicator Options Start - Do Not Modify
    Private Shared moRecordStatusIndOptions As IndicatorOptions
    Public Shared ReadOnly Property RecordStatusIndOptions() As IndicatorOptions
        Get
            If moRecordStatusIndOptions Is Nothing Then
                moRecordStatusIndOptions = New IndicatorOptions
                moRecordStatusIndOptions.Add("A", "Active")
                moRecordStatusIndOptions.Add("D", "Deleted")
            End If
            Return moRecordStatusIndOptions
        End Get
    End Property
    'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

    Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

        'sembleWare: Constructor Start - Do Not Modify
        MyBase.New("SAFPSSubjectIncident", ApplicationSettings)
        _IncidentCategory._IncidentCategoryCode.LocalElement = _IncidentCategoryCode
        _IncidentSubCategory._IncidentCategoryCode.LocalElement = _IncidentCategoryCode
        _IncidentSubCategory._IncidentSubCategoryCode.LocalElement = _IncidentSubCategoryCode
        _Subscriber._SubscriberID.LocalElement = _SubscriberID
        _Loader._LoaderID.LocalElement = _LoaderID
        _SAFPSSubject._SAFPSSubjectID.LocalElement = _SAFPSSubjectID
        'sembleWare: Constructor End - Do Not Modify
        Me.DeleteType = CustomDBPart.enumDeleteType.ChangeRecordStatus
    End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes


#Region "base Overrides"
    Public Overrides Sub Save()
        With ApplicationSettings
            Try
                .BeginTransaction()
                Dim bIsNewYN As Boolean = Me.IsNew
                BRBeforeSave(bIsNewYN)
                MyBase.Save()
                .CommitTransaction()
            Catch Exception As Exception
                .RollbackTransaction()
                Throw Exception
            End Try
        End With
    End Sub

#End Region

#Region "Business Rules"
    Private Sub BRBeforeSave(ByVal IsNewYN As Boolean)
        Me.LastUpdatedDate.Value = System.DateTime.Now
    End Sub
#End Region
End Class 'sembleWare: Part

Public Class SAFPSSubjectIncidentRelationship 'sembleWare: Relationship
    Inherits Relationship

#Region " Relationship Code"

    'sembleWare: Relationship Start - Do Not Modify
    Public ReadOnly _SAFPSSubjectIncidentID As ForeignKey = New ForeignKey(Me)
    Public ReadOnly _SAFPSSubjectID As ForeignKey = New ForeignKey(Me)

    Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
        MyBase.New(ApplicationSettings)
    End Sub

    Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
        MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
    End Sub

    Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
        MyBase.New(Name, Caption, OwnedByName, Owner)
    End Sub

    Public Overrides Function CreateObject() As sembleWare.Runtime.Part
        Return New SAFPSSubjectIncident(ApplicationSettings)
    End Function

    Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
        _SAFPSSubjectIncidentID.ForeignElement = CType(Instance, SAFPSSubjectIncident).SAFPSSubjectIncidentID
        _SAFPSSubjectID.ForeignElement = CType(Instance, SAFPSSubjectIncident)._SAFPSSubjectID
    End Sub

    'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

    Public Function GridList() As sembleWare.Runtime.List
        'sembleWare: List Declaration Start - Do Not Modify
        Dim oList As DBList = New DBList(Me, "A0SAFPSSubjectIncident")
        oList.Columns.Add(New ListColumn("SAFPSSubjectIncidentID", "[A0SAFPSSubjectIncident].[SAFPSSubjectIncidentID]", "SAFPSSubjectIncidentID", DataType.Long, Nothing, True, 0, False, 100, "SAFPSSubject Incident ID"))
        oList.Columns.Add(New ListColumn("SAFPSSubjectID", "[A0SAFPSSubjectIncident].[SAFPSSubjectID]", "SAFPSSubjectID", DataType.Integer, Nothing, True, 0, False, 100, "SAFPSSubject ID"))
        oList.Columns.Add(New ListColumn("MemRefNo", "[A0SAFPSSubjectIncident].[MemRefNo]", "MemRefNo", DataType.String, Nothing, False, 0, True, 100, "Mem Ref No"))
        oList.Columns.Add(New ListColumn("LogDate", "[A0SAFPSSubjectIncident].[LogDate]", "LogDate", DataType.DateTime, "d", False, 0, True, 100, "Log Date"))
        oList.Columns.Add(New ListColumn("IncidentDate", "[A0SAFPSSubjectIncident].[IncidentDate]", "IncidentDate", DataType.DateTime, "d", False, 0, True, 100, "Incident Date"))
        oList.Columns.Add(New ListColumn("SubjectsRole", "[A0SAFPSSubjectIncident].[SubjectsRole]", "SubjectsRole", DataType.String, Nothing, False, 0, True, 100, "Subjects Role"))
        oList.Columns.Add(New ListColumn("City", "[A0SAFPSSubjectIncident].[City]", "City", DataType.String, Nothing, False, 0, True, 100, "City"))
        oList.Columns.Add(New ListColumn("SavingsAmt", "[A0SAFPSSubjectIncident].[SavingsAmt]", "SavingsAmt", DataType.Decimal, "c", False, 0, True, 100, "Savings Amount"))
        oList.Columns.Add(New ListColumn("LossAmt", "[A0SAFPSSubjectIncident].[LossAmt]", "LossAmt", DataType.Decimal, "c", False, 0, True, 100, "Loss Amount"))
        oList.SelectStatement = "select"
        oList.FromClause = "[SAFPSSubjectIncident]  [A0SAFPSSubjectIncident]"
        oList.WhereClause = ""
        oList.ApplyRelationshipLimits()
        'sembleWare: List Declaration End - Do Not Modify
        oList.AndFilters.Add(New ListFilter("RecordStatusInd", "[A0SAFPSSubjectIncident].[RecordStatusInd]", Constants.RecordStatus.Active, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
        Return oList
    End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
