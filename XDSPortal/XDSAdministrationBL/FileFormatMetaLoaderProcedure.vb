Imports sembleWare.Runtime
Imports System
Public Class FileFormatMetaLoaderProcedure 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly TemplateDetails As New TextElement("TemplateDetails", Me, True, True, True, True, Nothing, Nothing, Nothing)
  Public ReadOnly FileFormatMetaDatabase As Relationship = New FileFormatMetaDatabaseRelationship("FileFormatMetaDatabase", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly LoaderProcessType As Relationship = New LoaderProcessTypeRelationship("LoaderProcessType", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _FileFormatCode As New StringElement("FileFormatCode", Me, True)
  Public ReadOnly _MetaDatabaseCode As New StringElement("MetaDatabaseCode", Me, True)
  Public ReadOnly _LoaderProcessTypeCode As New StringElement("LoaderProcessTypeCode", Me, True)
  Public ReadOnly _FileFormatMetaDatabase As FileFormatMetaDatabaseRelationship = FileFormatMetaDatabase
  Public ReadOnly _LoaderProcessType As LoaderProcessTypeRelationship = LoaderProcessType
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("FileFormatMetaLoaderProcedure", ApplicationSettings)
    _FileFormatMetaDatabase._FileFormatCode.LocalElement = _FileFormatCode
    _FileFormatMetaDatabase._MetaDatabaseCode.LocalElement = _MetaDatabaseCode
    _LoaderProcessType._LoaderProcessTypeCode.LocalElement = _LoaderProcessTypeCode
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class FileFormatMetaLoaderProcedureRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _FileFormatCode As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _MetaDatabaseCode As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _LoaderProcessTypeCode As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New FileFormatMetaLoaderProcedure(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _FileFormatCode.ForeignElement = CType(Instance, FileFormatMetaLoaderProcedure)._FileFormatCode
    _MetaDatabaseCode.ForeignElement = CType(Instance, FileFormatMetaLoaderProcedure)._MetaDatabaseCode
    _LoaderProcessTypeCode.ForeignElement = CType(Instance, FileFormatMetaLoaderProcedure)._LoaderProcessTypeCode
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0FileFormatMetaLoaderProcedure")
    oList.Columns.Add(New ListColumn("FileFormatCode", "[A0FileFormatMetaLoaderProcedure].[FileFormatCode]", "FileFormatCode", DataType.String, Nothing, True, 0, False, 100, "File Format Code"))
    oList.Columns.Add(New ListColumn("MetaDatabaseCode", "[A0FileFormatMetaLoaderProcedure].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, True, 0, False, 100, "Meta Database Code"))
    oList.Columns.Add(New ListColumn("LoaderProcessTypeCode", "[A0FileFormatMetaLoaderProcedure].[LoaderProcessTypeCode]", "LoaderProcessTypeCode", DataType.String, Nothing, True, 0, False, 100, "Loader Process Type Code"))
    oList.Columns.Add(New ListColumn("LoaderProcessTypeCode1", "[A1LoaderProcessType].[LoaderProcessTypeCode]", "LoaderProcessTypeCode", DataType.String, Nothing, False, 1, True, 100, "Loader Process Type Code"))
    oList.Columns.Add(New ListColumn("LoaderProcessTypeDesc", "[A1LoaderProcessType].[LoaderProcessTypeDesc]", "LoaderProcessTypeDesc", DataType.String, Nothing, False, 0, True, 200, "Loader Process Type Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "([FileFormatMetaLoaderProcedure]  [A0FileFormatMetaLoaderProcedure]" + _
           "    join [LoaderProcessType]  [A1LoaderProcessType] on [A0FileFormatMetaLoaderProcedure].[LoaderProcessTypeCode] = [A1LoaderProcessType].[LoaderProcessTypeCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
