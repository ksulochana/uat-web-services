Imports sembleWare.Runtime
Imports System
Public Class DataStatus 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly DataStatusCode As New StringElement("DataStatusCode", Me, True, True, True, True, True, 10, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly DataStatusDesc As New StringElement("DataStatusDesc", Me, False, True, True, True, True, 50, "Data Status Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly DataStatusTypeInd As New IndicatorElement("DataStatusTypeInd", Me, False, False, True, True, True, True, "Data Status Type", Nothing, DataStatus.DataStatusTypeIndOptions, Nothing)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moDataStatusTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property DataStatusTypeIndOptions() As IndicatorOptions
    Get
      If moDataStatusTypeIndOptions Is Nothing Then
        moDataStatusTypeIndOptions = New IndicatorOptions
        moDataStatusTypeIndOptions.Add("A", "Account Information")
        moDataStatusTypeIndOptions.Add("D", "Default Information")
      End If
      Return moDataStatusTypeIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("DataStatus", ApplicationSettings)
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class DataStatusRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _DataStatusCode As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New DataStatus(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _DataStatusCode.ForeignElement = CType(Instance, DataStatus).DataStatusCode
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0DataStatus")
    oList.Columns.Add(New ListColumn("DataStatusCode", "[A0DataStatus].[DataStatusCode]", "DataStatusCode", DataType.String, Nothing, True, 1, True, 100, "Data Status Code"))
    oList.Columns.Add(New ListColumn("DataStatusDesc", "[A0DataStatus].[DataStatusDesc]", "DataStatusDesc", DataType.String, Nothing, False, 0, True, 200, "Data Status Description"))
    oList.Columns.Add(New ListColumn("DataStatusTypeInd", "[A0DataStatus].[DataStatusTypeInd]", "DataStatusTypeInd", DataType.String, Nothing, False, 0, True, 150, "Data Status Type"))
    oList.SelectStatement = "select"
    oList.FromClause = "[DataStatus]  [A0DataStatus]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0DataStatus")
    oList.Columns.Add(New ListColumn("DataStatusCode", "[A0DataStatus].[DataStatusCode]", "DataStatusCode", DataType.String, Nothing, True, 1, True, 100, "Data Status Code"))
    oList.Columns.Add(New ListColumn("DataStatusDesc", "[A0DataStatus].[DataStatusDesc]", "DataStatusDesc", DataType.String, Nothing, False, 0, True, 200, "Data Status Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "[DataStatus]  [A0DataStatus]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function DropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0DataStatus")
    oList.Columns.Add(New ListColumn("DataStatusCode", "[A0DataStatus].[DataStatusCode]", "DataStatusCode", DataType.String, Nothing, True, 1, True, 100, "Data Status Code"))
    oList.Columns.Add(New ListColumn("DataStatusDesc", "[A0DataStatus].[DataStatusDesc]", "DataStatusDesc", DataType.String, Nothing, False, 0, True, 200, "Data Status Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "[DataStatus]  [A0DataStatus]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
