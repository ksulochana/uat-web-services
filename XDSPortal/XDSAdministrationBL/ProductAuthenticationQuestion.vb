Imports sembleWare.Runtime
Imports System
Public Class ProductAuthenticationQuestion 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly ProductAuthenticationQuestionID As New IntegerElement("ProductAuthenticationQuestionID", Me, True, True, True, True, True, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Question As New StringElement("Question", Me, False, False, True, True, True, 250, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly StatusInd As New IndicatorElement("StatusInd", Me, False, False, True, True, True, True, "Status", Nothing, ProductAuthenticationQuestion.StatusIndOptions, Nothing)
  Public ReadOnly QuestionTypeInd As New IndicatorElement("QuestionTypeInd", Me, False, False, False, True, True, True, "Question Type", Nothing, ProductAuthenticationQuestion.QuestionTypeIndOptions, Nothing)
  Public ReadOnly AnswerTypeInd As New IndicatorElement("AnswerTypeInd", Me, False, False, False, True, True, True, "Answer Type", Nothing, ProductAuthenticationQuestion.AnswerTypeIndOptions, Nothing)
  Public ReadOnly RangeAnswerTypeInd As New IndicatorElement("RangeAnswerTypeInd", Me, False, False, True, True, True, False, "Range Answer Type", Nothing, ProductAuthenticationQuestion.RangeAnswerTypeIndOptions, Nothing)
  Public ReadOnly RequiredNoOfAnswers As New IntegerElement("RequiredNoOfAnswers", Me, False, False, True, True, True, Nothing, Nothing, Nothing, 1, Nothing, Nothing)
  Public ReadOnly AnswerDataSourceInd As New IndicatorElement("AnswerDataSourceInd", Me, False, False, False, True, True, True, "Answer Data Source", Nothing, ProductAuthenticationQuestion.AnswerDataSourceIndOptions, Nothing)
  Public ReadOnly AnswerCalculationTypeInd As New IndicatorElement("AnswerCalculationTypeInd", Me, False, False, False, True, True, False, "Answer Calculation Type", Nothing, ProductAuthenticationQuestion.AnswerCalculationTypeIndOptions, Nothing)
  Public ReadOnly AnswerAddressTypeInd As New IndicatorElement("AnswerAddressTypeInd", Me, False, False, False, True, True, False, "Answer Address Type", Nothing, ProductAuthenticationQuestion.AnswerAddressTypeIndOptions, Nothing)
  Public ReadOnly AnswerTelephoneTypeInd As New IndicatorElement("AnswerTelephoneTypeInd", Me, False, False, False, True, True, False, "Answer Telephone Type", Nothing, ProductAuthenticationQuestion.AnswerTelephoneTypeIndOptions, Nothing)
  Public ReadOnly AnswerAccountTypeInd As New IndicatorElement("AnswerAccountTypeInd", Me, False, False, False, True, True, False, "Answer Account Type", Nothing, ProductAuthenticationQuestion.AnswerAccountTypeIndOptions, Nothing)
  Public ReadOnly AnswerAccountCalculationTypeInd As New IndicatorElement("AnswerAccountCalculationTypeInd", Me, False, False, False, True, True, False, "Answer Account Calculation Type", Nothing, ProductAuthenticationQuestion.AnswerAccountCalculationTypeIndOptions, Nothing)
  Public ReadOnly ProductAuthenticationQuestionGroup As Relationship = New ProductAuthenticationQuestionGroupRelationship("ProductAuthenticationQuestionGroup", "Question Group", RelationshipType.Include, True, True, True, Me)
  Public ReadOnly SubscriberBusinessType As Relationship = New SubscriberBusinessTypeRelationship("SubscriberBusinessType", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _ProductAuthenticationQuestionGroupCode As New StringElement("ProductAuthenticationQuestionGroupCode", Me, False)
  Public ReadOnly _SubscriberBusinessTypeCode As New StringElement("SubscriberBusinessTypeCode", Me, False)
  Public ReadOnly _ProductAuthenticationQuestionGroup As ProductAuthenticationQuestionGroupRelationship = ProductAuthenticationQuestionGroup
  Public ReadOnly _SubscriberBusinessType As SubscriberBusinessTypeRelationship = SubscriberBusinessType
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property StatusIndOptions() As IndicatorOptions
    Get
      If moStatusIndOptions Is Nothing Then
        moStatusIndOptions = New IndicatorOptions
        moStatusIndOptions.Add("A", "Active")
        moStatusIndOptions.Add("I", "Inactive")
      End If
      Return moStatusIndOptions
    End Get
  End Property
  Private Shared moQuestionTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property QuestionTypeIndOptions() As IndicatorOptions
    Get
      If moQuestionTypeIndOptions Is Nothing Then
        moQuestionTypeIndOptions = New IndicatorOptions
        moQuestionTypeIndOptions.Add("P", "Positive")
        moQuestionTypeIndOptions.Add("N", "Negative")
      End If
      Return moQuestionTypeIndOptions
    End Get
  End Property
  Private Shared moAnswerTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property AnswerTypeIndOptions() As IndicatorOptions
    Get
      If moAnswerTypeIndOptions Is Nothing Then
        moAnswerTypeIndOptions = New IndicatorOptions
        moAnswerTypeIndOptions.Add("Y", "Yes/No")
        moAnswerTypeIndOptions.Add("RA", "Range of Answers")
        moAnswerTypeIndOptions.Add("RDY", "Range of Defined Years")
        moAnswerTypeIndOptions.Add("RDA1", "Range of Defined Amounts 1")
        moAnswerTypeIndOptions.Add("RDA2", "Range of Defined Amounts 2")
        moAnswerTypeIndOptions.Add("RDA3", "Range of Defined Amounts 3")
      End If
      Return moAnswerTypeIndOptions
    End Get
  End Property
  Private Shared moRangeAnswerTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property RangeAnswerTypeIndOptions() As IndicatorOptions
    Get
      If moRangeAnswerTypeIndOptions Is Nothing Then
        moRangeAnswerTypeIndOptions = New IndicatorOptions
        moRangeAnswerTypeIndOptions.Add("N", "None of these")
        moRangeAnswerTypeIndOptions.Add("I", "I do not have")
      End If
      Return moRangeAnswerTypeIndOptions
    End Get
  End Property
  Private Shared moAnswerDataSourceIndOptions As IndicatorOptions
  Public Shared ReadOnly Property AnswerDataSourceIndOptions() As IndicatorOptions
    Get
      If moAnswerDataSourceIndOptions Is Nothing Then
        moAnswerDataSourceIndOptions = New IndicatorOptions
        moAnswerDataSourceIndOptions.Add("C", "Consumer")
        moAnswerDataSourceIndOptions.Add("CA", "Consumer Account")
        moAnswerDataSourceIndOptions.Add("CE", "Consumer Employment")
        moAnswerDataSourceIndOptions.Add("CT", "Consumer Telephone")
        moAnswerDataSourceIndOptions.Add("CACC", "Consumer Account")
        moAnswerDataSourceIndOptions.Add("CACCOPNBAL", "Consumer Account Open Balance")
        moAnswerDataSourceIndOptions.Add("CACCCURBAL", "Consumer Account Current Balance")
        moAnswerDataSourceIndOptions.Add("CACCMULT", "Consumer Account Multiple")
        moAnswerDataSourceIndOptions.Add("CACCLPAYDT", "Consumer Account Last Payment Date")
        moAnswerDataSourceIndOptions.Add("CACCMINAMT", "Consumer Account Minimum Amount")
      End If
      Return moAnswerDataSourceIndOptions
    End Get
  End Property
  Private Shared moAnswerCalculationTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property AnswerCalculationTypeIndOptions() As IndicatorOptions
    Get
      If moAnswerCalculationTypeIndOptions Is Nothing Then
        moAnswerCalculationTypeIndOptions = New IndicatorOptions
        moAnswerCalculationTypeIndOptions.Add("CURAGE", "Current Age")
        moAnswerCalculationTypeIndOptions.Add("NEXTAGE", "Next Age")
        moAnswerCalculationTypeIndOptions.Add("PREVAGE", "Previous Age")
        moAnswerCalculationTypeIndOptions.Add("CURMONTH", "Current Month")
        moAnswerCalculationTypeIndOptions.Add("NEXTMONTH", "Next Month")
        moAnswerCalculationTypeIndOptions.Add("PREVMONTH", "Previous Month")
        moAnswerCalculationTypeIndOptions.Add("CURYEAR", "Current Year")
        moAnswerCalculationTypeIndOptions.Add("NEXTYRAGE", "Next Year Age")
        moAnswerCalculationTypeIndOptions.Add("PREVYRAGE", "Previous Year Age")
        moAnswerCalculationTypeIndOptions.Add("LASTID3", "Last ID 3")
        moAnswerCalculationTypeIndOptions.Add("FIRSTID2", "First ID 2")
      End If
      Return moAnswerCalculationTypeIndOptions
    End Get
  End Property
  Private Shared moAnswerAddressTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property AnswerAddressTypeIndOptions() As IndicatorOptions
    Get
      If moAnswerAddressTypeIndOptions Is Nothing Then
        moAnswerAddressTypeIndOptions = New IndicatorOptions
        moAnswerAddressTypeIndOptions.Add("R", "Residential")
        moAnswerAddressTypeIndOptions.Add("P", "Postal")
      End If
      Return moAnswerAddressTypeIndOptions
    End Get
  End Property
  Private Shared moAnswerTelephoneTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property AnswerTelephoneTypeIndOptions() As IndicatorOptions
    Get
      If moAnswerTelephoneTypeIndOptions Is Nothing Then
        moAnswerTelephoneTypeIndOptions = New IndicatorOptions
        moAnswerTelephoneTypeIndOptions.Add("H", "Home")
        moAnswerTelephoneTypeIndOptions.Add("W", "Work")
        moAnswerTelephoneTypeIndOptions.Add("C", "Cellular")
      End If
      Return moAnswerTelephoneTypeIndOptions
    End Get
  End Property
  Private Shared moAnswerAccountTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property AnswerAccountTypeIndOptions() As IndicatorOptions
    Get
      If moAnswerAccountTypeIndOptions Is Nothing Then
        moAnswerAccountTypeIndOptions = New IndicatorOptions
        moAnswerAccountTypeIndOptions.Add("C", "Credit Card")
        moAnswerAccountTypeIndOptions.Add("H", "Home Loan")
        moAnswerAccountTypeIndOptions.Add("R", "Revolving")
      End If
      Return moAnswerAccountTypeIndOptions
    End Get
  End Property
  Private Shared moAnswerAccountCalculationTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property AnswerAccountCalculationTypeIndOptions() As IndicatorOptions
    Get
      If moAnswerAccountCalculationTypeIndOptions Is Nothing Then
        moAnswerAccountCalculationTypeIndOptions = New IndicatorOptions
        moAnswerAccountCalculationTypeIndOptions.Add("YEARMONTH", "Year Month")
      End If
      Return moAnswerAccountCalculationTypeIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("ProductAuthenticationQuestion", ApplicationSettings)
    _ProductAuthenticationQuestionGroup._ProductAuthenticationQuestionGroupCode.LocalElement = _ProductAuthenticationQuestionGroupCode
    _SubscriberBusinessType._SubscriberBusinessTypeCode.LocalElement = _SubscriberBusinessTypeCode
    AddHandler AnswerTypeInd.Changed, AddressOf AnswerTypeInd_Changed
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub AnswerTypeInd_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BRAnswerTypeInd(Element, True, True)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreateLoad()
  End Sub

  Public Overrides Sub Load()
    MyBase.Load()
    BRCreateLoad()
  End Sub

  Public Overrides Sub Save()
    With ApplicationSettings
      Try
        .BeginTransaction()
        Dim bIsNewYN As Boolean = Me.IsNew

        MyBase.Save()
        BRAfterSave(bIsNewYN)
        .CommitTransaction()
      Catch Exception As Exception
        .RollbackTransaction()
        Throw Exception
      End Try
    End With
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreateLoad()
    BRAnswerTypeInd(Me.AnswerTypeInd, True, False)
  End Sub

  Private Sub BRAfterSave(ByVal IsNewYN As Boolean)
    If IsNewYN Then
      InsertSubscriberProfileProductAuthenticationQuestion(Me.ProductAuthenticationQuestionID.Value, ApplicationSettings)
    Else
      UpdateSubscriberProfileProductAuthenticationQuestion(Me.ProductAuthenticationQuestionID.Value, ApplicationSettings)
    End If
  End Sub

  Private Sub BRAnswerTypeInd(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If SetEnableDisableYN Then
      Me.RangeAnswerTypeInd.Enabled = False
      Me.RangeAnswerTypeInd.Mandatory = False

      Select Case Element.ValueAsObject
        Case Constants.ProductAuthenticationQuestion.AnswerTypes.RangeOfAnswers, _
             Constants.ProductAuthenticationQuestion.AnswerTypes.RangeOfYears, _
             Constants.ProductAuthenticationQuestion.AnswerTypes.RangeOfAmounts1, _
             Constants.ProductAuthenticationQuestion.AnswerTypes.RangeOfAmounts2, _
             Constants.ProductAuthenticationQuestion.AnswerTypes.RangeOfAmounts3
          Me.RangeAnswerTypeInd.Enabled = True
          Me.RangeAnswerTypeInd.Mandatory = True
      End Select
    End If
  End Sub
#End Region

#Region " Shared Procedures"
  Public Shared Sub InsertSubscriberProfileProductAuthenticationQuestion(ByVal ProductAuthenticationQuestionID As Integer, ByVal ApplicationSettings As ApplicationSettings)
    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand

    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spProductAuthenticationQuestion_I_SubscriberProfileProductAuthenticationQuestion"

    oCommand.Parameters.Add("@ProductAuthenticationQuestionID", SqlDbType.Int)
    oCommand.Parameters.Add("@Username", SqlDbType.VarChar, 50)

    oCommand.Parameters("@ProductAuthenticationQuestionID").Value = ProductAuthenticationQuestionID
    oCommand.Parameters("@Username").Value = SystemUser.GetLoggedInSystemUser(ApplicationSettings).Username.Value

    ApplicationSettings.ActionQuery(oCommand)
  End Sub

  Public Shared Sub UpdateSubscriberProfileProductAuthenticationQuestion(ByVal ProductAuthenticationQuestionID As Integer, ByVal ApplicationSettings As ApplicationSettings)
    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand

    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spProductAuthenticationQuestion_U_SubscriberProfileProductAuthenticationQuestion"

    oCommand.Parameters.Add("@ProductAuthenticationQuestionID", SqlDbType.Int)
    oCommand.Parameters.Add("@Username", SqlDbType.VarChar, 50)

    oCommand.Parameters("@ProductAuthenticationQuestionID").Value = ProductAuthenticationQuestionID
    oCommand.Parameters("@Username").Value = SystemUser.GetLoggedInSystemUser(ApplicationSettings).Username.Value

    ApplicationSettings.ActionQuery(oCommand)
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class ProductAuthenticationQuestionRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _ProductAuthenticationQuestionID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New ProductAuthenticationQuestion(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _ProductAuthenticationQuestionID.ForeignElement = CType(Instance, ProductAuthenticationQuestion).ProductAuthenticationQuestionID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function DropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0ProductAuthenticationQuestion")
    oList.Columns.Add(New ListColumn("ProductAuthenticationQuestionID", "[A0ProductAuthenticationQuestion].[ProductAuthenticationQuestionID]", "ProductAuthenticationQuestionID", DataType.Integer, Nothing, True, 0, False, 100, "Product Authentication Question ID"))
    oList.Columns.Add(New ListColumn("ProductAuthenticationQuestionGroupDesc", "[A1ProductAuthenticationQuestionGroup].[ProductAuthenticationQuestionGroupDesc]", "ProductAuthenticationQuestionGroupDesc", DataType.String, Nothing, False, 1, True, 150, "Group Description"))
    oList.Columns.Add(New ListColumn("Question", "[A0ProductAuthenticationQuestion].[Question]", "Question", DataType.String, Nothing, False, 2, True, 200, "Question"))
    oList.Columns.Add(New ListColumn("QuestionTypeInd", "[A0ProductAuthenticationQuestion].[QuestionTypeInd]", "QuestionTypeInd", DataType.String, Nothing, False, 0, True, 100, "Question Type"))
    oList.SelectStatement = "select"
    oList.FromClause = "([ProductAuthenticationQuestion]  [A0ProductAuthenticationQuestion]" + _
           "    join [ProductAuthenticationQuestionGroup]  [A1ProductAuthenticationQuestionGroup] on [A0ProductAuthenticationQuestion].[ProductAuthenticationQuestionGroupCode] = [A1ProductAuthenticationQuestionGroup].[ProductAuthenticationQuestionGroupCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0ProductAuthenticationQuestion")
    oList.Columns.Add(New ListColumn("ProductAuthenticationQuestionID", "[A0ProductAuthenticationQuestion].[ProductAuthenticationQuestionID]", "ProductAuthenticationQuestionID", DataType.Integer, Nothing, True, 0, False, 100, "Product Authentication Question ID"))
    oList.Columns.Add(New ListColumn("ProductAuthenticationQuestionGroupDesc", "[A1ProductAuthenticationQuestionGroup].[ProductAuthenticationQuestionGroupDesc]", "ProductAuthenticationQuestionGroupDesc", DataType.String, Nothing, False, 2, True, 200, "Group Description"))
    oList.Columns.Add(New ListColumn("Question", "[A0ProductAuthenticationQuestion].[Question]", "Question", DataType.String, Nothing, False, 1, True, 200, "Question"))
    oList.Columns.Add(New ListColumn("QuestionTypeInd", "[A0ProductAuthenticationQuestion].[QuestionTypeInd]", "QuestionTypeInd", DataType.String, Nothing, False, 0, True, 100, "Question Type"))
    oList.SelectStatement = "select"
    oList.FromClause = "([ProductAuthenticationQuestion]  [A0ProductAuthenticationQuestion]" + _
           "    join [ProductAuthenticationQuestionGroup]  [A1ProductAuthenticationQuestionGroup] on [A0ProductAuthenticationQuestion].[ProductAuthenticationQuestionGroupCode] = [A1ProductAuthenticationQuestionGroup].[ProductAuthenticationQuestionGroupCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0ProductAuthenticationQuestion")
    oList.Columns.Add(New ListColumn("ProductAuthenticationQuestionID", "[A0ProductAuthenticationQuestion].[ProductAuthenticationQuestionID]", "ProductAuthenticationQuestionID", DataType.Integer, Nothing, True, 1, True, 75, "Product Authentication Question ID"))
    oList.Columns.Add(New ListColumn("Question", "[A0ProductAuthenticationQuestion].[Question]", "Question", DataType.String, Nothing, False, 0, True, 200, "Question"))
    oList.Columns.Add(New ListColumn("StatusInd", "[A0ProductAuthenticationQuestion].[StatusInd]", "StatusInd", DataType.String, Nothing, False, 0, True, 75, "Status"))
    oList.Columns.Add(New ListColumn("QuestionTypeInd", "[A0ProductAuthenticationQuestion].[QuestionTypeInd]", "QuestionTypeInd", DataType.String, Nothing, False, 0, True, 75, "Question Type"))
    oList.Columns.Add(New ListColumn("AnswerTypeInd", "[A0ProductAuthenticationQuestion].[AnswerTypeInd]", "AnswerTypeInd", DataType.String, Nothing, False, 0, True, 75, "Answer Type"))
    oList.Columns.Add(New ListColumn("ProductAuthenticationQuestionGroupDesc", "[A1ProductAuthenticationQuestionGroup].[ProductAuthenticationQuestionGroupDesc]", "ProductAuthenticationQuestionGroupDesc", DataType.String, Nothing, False, 0, True, 200, "Group Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "([ProductAuthenticationQuestion]  [A0ProductAuthenticationQuestion]" + _
           "    join [ProductAuthenticationQuestionGroup]  [A1ProductAuthenticationQuestionGroup] on [A0ProductAuthenticationQuestion].[ProductAuthenticationQuestionGroupCode] = [A1ProductAuthenticationQuestionGroup].[ProductAuthenticationQuestionGroupCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
