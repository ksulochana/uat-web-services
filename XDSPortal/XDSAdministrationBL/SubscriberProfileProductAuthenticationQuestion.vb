Imports sembleWare.Runtime
Imports System
Public Class SubscriberProfileProductAuthenticationQuestion 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly Question As New StringElement("Question", Me, False, True, True, True, True, 250, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly StatusInd As New IndicatorElement("StatusInd", Me, False, False, True, True, True, True, "Status", Nothing, SubscriberProfileProductAuthenticationQuestion.StatusIndOptions, Nothing)
  Public ReadOnly QuestionPointValue As New IntegerElement("QuestionPointValue", Me, False, True, True, True, True, "Point Value", Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PriorityTypeInd As New IndicatorElement("PriorityTypeInd", Me, False, False, True, True, True, True, "Priority Type", Nothing, SubscriberProfileProductAuthenticationQuestion.PriorityTypeIndOptions, Nothing)
  Public ReadOnly MaxNoOfCorrectAnswers As New IntegerElement("MaxNoOfCorrectAnswers", Me, False, True, True, True, True, Nothing, Nothing, Nothing, 1, Nothing, Nothing)
  Public ReadOnly SubscriberProfile As Relationship = New SubscriberProfileRelationship("SubscriberProfile", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly ProductAuthenticationQuestion As Relationship = New ProductAuthenticationQuestionRelationship("ProductAuthenticationQuestion", "Question", RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _ProductAuthenticationQuestionID As New IntegerElement("ProductAuthenticationQuestionID", Me, True)
  Public ReadOnly _SubscriberProfile As SubscriberProfileRelationship = SubscriberProfile
  Public ReadOnly _ProductAuthenticationQuestion As ProductAuthenticationQuestionRelationship = ProductAuthenticationQuestion
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property StatusIndOptions() As IndicatorOptions
    Get
      If moStatusIndOptions Is Nothing Then
        moStatusIndOptions = New IndicatorOptions
        moStatusIndOptions.Add("A", "Active")
        moStatusIndOptions.Add("I", "Inactive")
      End If
      Return moStatusIndOptions
    End Get
  End Property
  Private Shared moPriorityTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property PriorityTypeIndOptions() As IndicatorOptions
    Get
      If moPriorityTypeIndOptions Is Nothing Then
        moPriorityTypeIndOptions = New IndicatorOptions
        moPriorityTypeIndOptions.Add("H", "High")
        moPriorityTypeIndOptions.Add("M", "Medium")
        moPriorityTypeIndOptions.Add("L", "Low")
      End If
      Return moPriorityTypeIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberProfileProductAuthenticationQuestion", ApplicationSettings)
    _SubscriberProfile._SubscriberID.LocalElement = _SubscriberID
    _ProductAuthenticationQuestion._ProductAuthenticationQuestionID.LocalElement = _ProductAuthenticationQuestionID
    'sembleWare: Constructor End - Do Not Modify
  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes
End Class 'sembleWare: Part

Public Class SubscriberProfileProductAuthenticationQuestionRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _ProductAuthenticationQuestionID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberProfileProductAuthenticationQuestion(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberID.ForeignElement = CType(Instance, SubscriberProfileProductAuthenticationQuestion)._SubscriberID
    _ProductAuthenticationQuestionID.ForeignElement = CType(Instance, SubscriberProfileProductAuthenticationQuestion)._ProductAuthenticationQuestionID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberProfileProductAuthenticationQuestion")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberProfileProductAuthenticationQuestion].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("ProductAuthenticationQuestionGroupDesc", "[A2ProductAuthenticationQuestionGroup].[ProductAuthenticationQuestionGroupDesc]", "ProductAuthenticationQuestionGroupDesc", DataType.String, Nothing, False, 1, True, 100, "Group Description"))
    oList.Columns.Add(New ListColumn("Question", "[A0SubscriberProfileProductAuthenticationQuestion].[Question]", "Question", DataType.String, Nothing, False, 0, True, 200, "Question"))
    oList.Columns.Add(New ListColumn("QuestionTypeInd", "[A1ProductAuthenticationQuestion].[QuestionTypeInd]", "QuestionTypeInd", DataType.String, Nothing, False, 0, True, 100, "Question Type"))
    oList.Columns.Add(New ListColumn("PriorityTypeInd", "[A0SubscriberProfileProductAuthenticationQuestion].[PriorityTypeInd]", "PriorityTypeInd", DataType.String, Nothing, False, 0, True, 100, "Priority Type"))
    oList.Columns.Add(New ListColumn("QuestionPointValue", "[A0SubscriberProfileProductAuthenticationQuestion].[QuestionPointValue]", "QuestionPointValue", DataType.Integer, Nothing, False, 0, True, 100, "Point Value"))
    oList.Columns.Add(New ListColumn("StatusInd", "[A0SubscriberProfileProductAuthenticationQuestion].[StatusInd]", "StatusInd", DataType.String, Nothing, False, 0, True, 100, "Status"))
    oList.Columns.Add(New ListColumn("ProductAuthenticationQuestionID", "[A0SubscriberProfileProductAuthenticationQuestion].[ProductAuthenticationQuestionID]", "ProductAuthenticationQuestionID", DataType.Integer, Nothing, True, 0, False, 100, "Product Authentication Question ID"))
    oList.Columns.Add(New ListColumn("MaxNoOfCorrectAnswers", "[A0SubscriberProfileProductAuthenticationQuestion].[MaxNoOfCorrectAnswers]", "MaxNoOfCorrectAnswers", DataType.Integer, Nothing, False, 0, True, 100, "Max No Of Correct Answers"))
    oList.SelectStatement = "select"
    oList.FromClause = "([SubscriberProfileProductAuthenticationQuestion]  [A0SubscriberProfileProductAuthenticationQuestion]" + _
           "    join ([ProductAuthenticationQuestion]  [A1ProductAuthenticationQuestion]" + _
           "    join [ProductAuthenticationQuestionGroup]  [A2ProductAuthenticationQuestionGroup] on [A1ProductAuthenticationQuestion].[ProductAuthenticationQuestionGroupCode] = [A2ProductAuthenticationQuestionGroup].[ProductAuthenticationQuestionGroupCode]) on [A0SubscriberProfileProductAuthenticationQuestion].[ProductAuthenticationQuestionID] = [A1ProductAuthenticationQuestion].[ProductAuthenticationQuestionID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberProfileProductAuthenticationQuestion")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberProfileProductAuthenticationQuestion].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("Question", "[A1ProductAuthenticationQuestion].[Question]", "Question", DataType.String, Nothing, False, 1, True, 200, "Question"))
    oList.Columns.Add(New ListColumn("QuestionTypeInd", "[A1ProductAuthenticationQuestion].[QuestionTypeInd]", "QuestionTypeInd", DataType.String, Nothing, False, 0, True, 100, "Question Type"))
    oList.Columns.Add(New ListColumn("ProductAuthenticationQuestionID", "[A0SubscriberProfileProductAuthenticationQuestion].[ProductAuthenticationQuestionID]", "ProductAuthenticationQuestionID", DataType.Integer, Nothing, True, 0, False, 100, "Product Authentication Question ID"))
    oList.Columns.Add(New ListColumn("ProductAuthenticationQuestionGroupDesc", "[A2ProductAuthenticationQuestionGroup].[ProductAuthenticationQuestionGroupDesc]", "ProductAuthenticationQuestionGroupDesc", DataType.String, Nothing, False, 0, True, 200, "Group Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "([SubscriberProfileProductAuthenticationQuestion]  [A0SubscriberProfileProductAuthenticationQuestion]" + _
           "    join ([ProductAuthenticationQuestion]  [A1ProductAuthenticationQuestion]" + _
           "    join [ProductAuthenticationQuestionGroup]  [A2ProductAuthenticationQuestionGroup] on [A1ProductAuthenticationQuestion].[ProductAuthenticationQuestionGroupCode] = [A2ProductAuthenticationQuestionGroup].[ProductAuthenticationQuestionGroupCode]) on [A0SubscriberProfileProductAuthenticationQuestion].[ProductAuthenticationQuestionID] = [A1ProductAuthenticationQuestion].[ProductAuthenticationQuestionID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function DropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberProfileProductAuthenticationQuestion")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberProfileProductAuthenticationQuestion].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("ProductAuthenticationQuestionID", "[A0SubscriberProfileProductAuthenticationQuestion].[ProductAuthenticationQuestionID]", "ProductAuthenticationQuestionID", DataType.Integer, Nothing, True, 0, False, 100, "Product Authentication Question ID"))
    oList.Columns.Add(New ListColumn("Question", "[A1ProductAuthenticationQuestion].[Question]", "Question", DataType.String, Nothing, False, 1, True, 200, "Question"))
    oList.Columns.Add(New ListColumn("QuestionTypeInd", "[A1ProductAuthenticationQuestion].[QuestionTypeInd]", "QuestionTypeInd", DataType.String, Nothing, False, 0, True, 100, "Question Type"))
    oList.Columns.Add(New ListColumn("ProductAuthenticationQuestionGroupDesc", "[A2ProductAuthenticationQuestionGroup].[ProductAuthenticationQuestionGroupDesc]", "ProductAuthenticationQuestionGroupDesc", DataType.String, Nothing, False, 0, True, 200, "Group Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "([SubscriberProfileProductAuthenticationQuestion]  [A0SubscriberProfileProductAuthenticationQuestion]" + _
           "    join ([ProductAuthenticationQuestion]  [A1ProductAuthenticationQuestion]" + _
           "    join [ProductAuthenticationQuestionGroup]  [A2ProductAuthenticationQuestionGroup] on [A1ProductAuthenticationQuestion].[ProductAuthenticationQuestionGroupCode] = [A2ProductAuthenticationQuestionGroup].[ProductAuthenticationQuestionGroupCode]) on [A0SubscriberProfileProductAuthenticationQuestion].[ProductAuthenticationQuestionID] = [A1ProductAuthenticationQuestion].[ProductAuthenticationQuestionID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
