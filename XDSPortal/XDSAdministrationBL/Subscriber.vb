Imports sembleWare.Runtime
Imports System
Public Class Subscriber 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly SubscriberID As New IdentityElement("SubscriberID", Me, True, Nothing, Nothing)
  Public ReadOnly SubscriberName As New StringElement("SubscriberName", Me, False, True, True, True, True, 250, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AuthenticationSubscriberName As New StringElement("AuthenticationSubscriberName", Me, False, True, True, True, False, 250, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SubscriberTypeInd As New IndicatorElement("SubscriberTypeInd", Me, False, False, True, True, True, True, "Subscriber Type", Nothing, Subscriber.SubscriberTypeIndOptions, Nothing)
  Public ReadOnly StatusInd As New IndicatorElement("StatusInd", Me, False, False, True, True, True, True, "Status", Nothing, Subscriber.StatusIndOptions, "A")
  Public ReadOnly CompanyRegistrationNo As New StringElement("CompanyRegistrationNo", Me, False, True, True, True, False, 50, "Registration No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CompanyVATNo As New StringElement("CompanyVATNo", Me, False, True, True, True, False, 50, "VAT No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CompanyDetails As New TextElement("CompanyDetails", Me, True, True, True, False, Nothing, Nothing, Nothing)
  Public ReadOnly CompanyTradingName1 As New StringElement("CompanyTradingName1", Me, False, True, True, True, False, 250, "Trading Name 1", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CompanyTradingName2 As New StringElement("CompanyTradingName2", Me, False, True, True, True, False, 250, "Trading Name 2", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CompanyTelephoneCode As New StringElement("CompanyTelephoneCode", Me, False, True, True, True, False, 5, "Telephone Code", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CompanyTelephoneNo As New StringElement("CompanyTelephoneNo", Me, False, True, True, True, False, 50, "Telephone No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CompanyFaxCode As New StringElement("CompanyFaxCode", Me, False, True, True, True, False, 5, "Fax Code", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CompanyFaxNo As New StringElement("CompanyFaxNo", Me, False, True, True, True, False, 50, "Fax No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CompanyWebsiteUrl As New StringElement("CompanyWebsiteUrl", Me, False, True, True, True, False, 250, "Website Url", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CompanyPhysicalAddress1 As New StringElement("CompanyPhysicalAddress1", Me, False, True, True, True, False, 50, "Physical Address 1", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CompanyPhysicalAddress2 As New StringElement("CompanyPhysicalAddress2", Me, False, True, True, True, False, 50, "Physical Address 2", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CompanyPhysicalAddress3 As New StringElement("CompanyPhysicalAddress3", Me, False, True, True, True, False, 50, "Physical Address 3", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CompanyPhysicalPostalCode As New StringElement("CompanyPhysicalPostalCode", Me, False, True, True, True, False, 10, "Physical Postal Code", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CompanyPostalAddress1 As New StringElement("CompanyPostalAddress1", Me, False, True, True, True, False, 100, "Postal Address 1", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CompanyPostalAddress2 As New StringElement("CompanyPostalAddress2", Me, False, True, True, True, False, 100, "Postal Address 2", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CompanyPostalAddress3 As New StringElement("CompanyPostalAddress3", Me, False, True, True, True, False, 100, "Postal Address 3", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CompanyPostalPostalCode As New StringElement("CompanyPostalPostalCode", Me, False, True, True, True, False, 10, "Postal Postal Code", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LevelNo As New IntegerElement("LevelNo", Me, False, False, True, True, True, Nothing, Nothing, Nothing, 0, Nothing, Nothing)
  Public ReadOnly Level1Desc As New StringElement("Level1Desc", Me, False, True, True, True, False, 50, "Level 1 Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Level2Desc As New StringElement("Level2Desc", Me, False, True, True, True, False, 50, "Level 2 Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Level3Desc As New StringElement("Level3Desc", Me, False, True, True, True, False, 50, "Level 3 Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Level4Desc As New StringElement("Level4Desc", Me, False, True, True, True, False, 50, "Level 4 Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Level5Desc As New StringElement("Level5Desc", Me, False, True, True, True, False, 50, "Level 5 Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SubscriberLabelDescription As New StringElement("SubscriberLabelDescription", Me, False, True, True, False, False, 250, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SubscriberRegistration As Relationship = New SubscriberRegistrationRelationship("SubscriberRegistration", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly Level1Subscriber As Relationship = New SubscriberRelationship("Level1Subscriber", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Level3Subscriber As Relationship = New SubscriberRelationship("Level3Subscriber", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Level2Subscriber As Relationship = New SubscriberRelationship("Level2Subscriber", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Level4Subscriber As Relationship = New SubscriberRelationship("Level4Subscriber", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Level5Subscriber As Relationship = New SubscriberRelationship("Level5Subscriber", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly SubscriberBillingGroup As Relationship = New SubscriberBillingGroupRelationship("SubscriberBillingGroup", "Billing Group", RelationshipType.Include, False, False, True, Me)
  Public ReadOnly SubscriberBusinessType As Relationship = New SubscriberBusinessTypeRelationship("SubscriberBusinessType", "Business Type", RelationshipType.Include, False, True, True, Me)
  Public ReadOnly SubscriberAssociation As Relationship = New SubscriberAssociationRelationship("SubscriberAssociation", "Association", RelationshipType.Include, True, True, True, Me)
  Public ReadOnly SubscriberGroup As Relationship = New SubscriberGroupRelationship("SubscriberGroup", "Group", RelationshipType.Include, False, True, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
    Public ReadOnly SubscriberAuthentication_OwnMany As Relationship = New SubscriberAuthenticationRelationship("SubscriberAuthentication", Nothing, "Subscriber", Me)
  Public ReadOnly SubscriberCreditGrantorEnquiry_OwnMany As Relationship = New SubscriberCreditGrantorEnquiryRelationship("SubscriberCreditGrantorEnquiry", Nothing, "Subscriber", Me)
  Public ReadOnly SubscriberTraceConsumer_OwnMany As Relationship = New SubscriberTraceConsumerRelationship("SubscriberTraceConsumer", Nothing, "Subscriber", Me)
  Public ReadOnly SubscriberCommercial_OwnMany As Relationship = New SubscriberCommercialRelationship("SubscriberCommercial", Nothing, "Subscriber", Me)
  Public ReadOnly SubscriberConsumerEnquiry_OwnMany As Relationship = New SubscriberConsumerEnquiryRelationship("SubscriberConsumerEnquiry", Nothing, "Subscriber", Me)
  Public ReadOnly SubscriberCreditGrantor_OwnMany As Relationship = New SubscriberCreditGrantorRelationship("SubscriberCreditGrantor", Nothing, "Subscriber", Me)
  Public ReadOnly SubscriberHomeAffairsEnquiry_OwnMany As Relationship = New SubscriberHomeAffairsEnquiryRelationship("SubscriberHomeAffairsEnquiry", Nothing, "Subscriber", Me)
  Public ReadOnly SubscriberTraceConsumerAlert_OwnMany As Relationship = New SubscriberTraceConsumerAlertRelationship("SubscriberTraceConsumerAlert", Nothing, "Subscriber", Me)
  Public ReadOnly SubscriberCommercialEnquiry_OwnMany As Relationship = New SubscriberCommercialEnquiryRelationship("SubscriberCommercialEnquiry", Nothing, "Subscriber", Me)
  Public ReadOnly SubscriberDirectorEnquiry_OwnMany As Relationship = New SubscriberDirectorEnquiryRelationship("SubscriberDirectorEnquiry", Nothing, "Subscriber", Me)
  Public ReadOnly SubscriberSAFPSSubjectEnquiry_OwnMany As Relationship = New SubscriberSAFPSSubjectEnquiryRelationship("SubscriberSAFPSSubjectEnquiry", Nothing, "Subscriber", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberRegistrationID As New IntegerElement("SubscriberRegistrationID", Me, False)
  Public ReadOnly _Level1SubscriberID As New IntegerElement("Level1SubscriberID", Me, False)
  Public ReadOnly _Level3SubscriberID As New IntegerElement("Level3SubscriberID", Me, False)
  Public ReadOnly _Level2SubscriberID As New IntegerElement("Level2SubscriberID", Me, False)
  Public ReadOnly _Level4SubscriberID As New IntegerElement("Level4SubscriberID", Me, False)
  Public ReadOnly _Level5SubscriberID As New IntegerElement("Level5SubscriberID", Me, False)
  Public ReadOnly _SubscriberBillingGroupCode As New StringElement("SubscriberBillingGroupCode", Me, False)
  Public ReadOnly _SubscriberBusinessTypeCode As New StringElement("SubscriberBusinessTypeCode", Me, False)
  Public ReadOnly _SubscriberAssociationCode As New StringElement("SubscriberAssociationCode", Me, False)
  Public ReadOnly _SubscriberGroupCode As New StringElement("SubscriberGroupCode", Me, False)
  Public ReadOnly _SubscriberRegistration As SubscriberRegistrationRelationship = SubscriberRegistration
  Public ReadOnly _Level1Subscriber As SubscriberRelationship = Level1Subscriber
  Public ReadOnly _Level3Subscriber As SubscriberRelationship = Level3Subscriber
  Public ReadOnly _Level2Subscriber As SubscriberRelationship = Level2Subscriber
  Public ReadOnly _Level4Subscriber As SubscriberRelationship = Level4Subscriber
  Public ReadOnly _Level5Subscriber As SubscriberRelationship = Level5Subscriber
  Public ReadOnly _SubscriberBillingGroup As SubscriberBillingGroupRelationship = SubscriberBillingGroup
  Public ReadOnly _SubscriberBusinessType As SubscriberBusinessTypeRelationship = SubscriberBusinessType
  Public ReadOnly _SubscriberAssociation As SubscriberAssociationRelationship = SubscriberAssociation
  Public ReadOnly _SubscriberGroup As SubscriberGroupRelationship = SubscriberGroup
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  Public ReadOnly _SubscriberAuthentication_OwnMany As SubscriberAuthenticationRelationship = SubscriberAuthentication_OwnMany
  Public ReadOnly _SubscriberCreditGrantorEnquiry_OwnMany As SubscriberCreditGrantorEnquiryRelationship = SubscriberCreditGrantorEnquiry_OwnMany
  Public ReadOnly _SubscriberTraceConsumer_OwnMany As SubscriberTraceConsumerRelationship = SubscriberTraceConsumer_OwnMany
  Public ReadOnly _SubscriberCommercial_OwnMany As SubscriberCommercialRelationship = SubscriberCommercial_OwnMany
  Public ReadOnly _SubscriberConsumerEnquiry_OwnMany As SubscriberConsumerEnquiryRelationship = SubscriberConsumerEnquiry_OwnMany
  Public ReadOnly _SubscriberCreditGrantor_OwnMany As SubscriberCreditGrantorRelationship = SubscriberCreditGrantor_OwnMany
  Public ReadOnly _SubscriberHomeAffairsEnquiry_OwnMany As SubscriberHomeAffairsEnquiryRelationship = SubscriberHomeAffairsEnquiry_OwnMany
  Public ReadOnly _SubscriberTraceConsumerAlert_OwnMany As SubscriberTraceConsumerAlertRelationship = SubscriberTraceConsumerAlert_OwnMany
  Public ReadOnly _SubscriberCommercialEnquiry_OwnMany As SubscriberCommercialEnquiryRelationship = SubscriberCommercialEnquiry_OwnMany
  Public ReadOnly _SubscriberDirectorEnquiry_OwnMany As SubscriberDirectorEnquiryRelationship = SubscriberDirectorEnquiry_OwnMany
  Public ReadOnly _SubscriberSAFPSSubjectEnquiry_OwnMany As SubscriberSAFPSSubjectEnquiryRelationship = SubscriberSAFPSSubjectEnquiry_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moSubscriberTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property SubscriberTypeIndOptions() As IndicatorOptions
    Get
      If moSubscriberTypeIndOptions Is Nothing Then
        moSubscriberTypeIndOptions = New IndicatorOptions
        moSubscriberTypeIndOptions.Add("D", "Supplier of Data Only")
        moSubscriberTypeIndOptions.Add("E", "Enquiries Only")
        moSubscriberTypeIndOptions.Add("B", "Both")
      End If
      Return moSubscriberTypeIndOptions
    End Get
  End Property
  Private Shared moStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property StatusIndOptions() As IndicatorOptions
    Get
      If moStatusIndOptions Is Nothing Then
        moStatusIndOptions = New IndicatorOptions
        moStatusIndOptions.Add("A", "Active")
        moStatusIndOptions.Add("I", "Inactive")
      End If
      Return moStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("Subscriber", ApplicationSettings)
    _SubscriberRegistration._SubscriberRegistrationID.LocalElement = _SubscriberRegistrationID
    _Level1Subscriber._SubscriberID.LocalElement = _Level1SubscriberID
    _Level3Subscriber._SubscriberID.LocalElement = _Level3SubscriberID
    _Level2Subscriber._SubscriberID.LocalElement = _Level2SubscriberID
    _Level4Subscriber._SubscriberID.LocalElement = _Level4SubscriberID
    _Level5Subscriber._SubscriberID.LocalElement = _Level5SubscriberID
    _SubscriberBillingGroup._SubscriberBillingGroupCode.LocalElement = _SubscriberBillingGroupCode
    _SubscriberBusinessType._SubscriberBusinessTypeCode.LocalElement = _SubscriberBusinessTypeCode
    _SubscriberAssociation._SubscriberAssociationCode.LocalElement = _SubscriberAssociationCode
    _SubscriberGroup._SubscriberGroupCode.LocalElement = _SubscriberGroupCode
    AddHandler SubscriberTypeInd.Changed, AddressOf SubscriberTypeInd_Changed
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"

  Private Sub SubscriberTypeInd_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BRSubscriberTypeInd(Element, True, True)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreateLoad()
  End Sub

  Public Overrides Sub Load()
    MyBase.Load()
    BRCreateLoad()
  End Sub

  Public Overrides Sub Save()
    With ApplicationSettings
      Try
        Dim bIsNewYN As Boolean = Me.IsNew
        .BeginTransaction()
        MyBase.Save()
        BRAfterSave(bIsNewYN)
        .CommitTransaction()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
    End With
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreateLoad()
    BRSubscriberTypeInd(Me.SubscriberTypeInd, True, False)
    BRSubscriberLabelDescription()
  End Sub

  Private Sub BRAfterSave(ByVal IsNewYN As Boolean)
    If IsNewYN Then
      Dim oSubscriberProfile As SubscriberProfile = New SubscriberProfileRelationship(ApplicationSettings).NewInstance
      oSubscriberProfile.Subscriber.Instance = Me
      oSubscriberProfile.Save()
      Dim oSubscriberProfileConsumerTraceReport As SubscriberProfileConsumerTraceReport = oSubscriberProfile.SubscriberProfileConsumerTraceReport_OwnMany.NewInstance
      oSubscriberProfileConsumerTraceReport.Save()
      Dim oSubscriberProfileConsumerCreditGrantorReport As SubscriberProfileConsumerCreditGrantorReport = oSubscriberProfile.SubscriberProfileConsumerCreditGrantorReport_OwnMany.NewInstance
      oSubscriberProfileConsumerCreditGrantorReport.Save()
      Dim oSubscriberProfileCommercialEnquiryReport As SubscriberProfileCommercialEnquiryReport = oSubscriberProfile.SubscriberProfileCommercialEnquiryReport_OwnMany.NewInstance
      oSubscriberProfileCommercialEnquiryReport.Save()

      InsertSubscriberProfileProducts(Me.SubscriberID.Value, ApplicationSettings)
      InsertSubscriberProfileProductAuthenticationQuestion(Me.SubscriberID.Value, ApplicationSettings)
      InsertSubscriberProfileProductAuthenticationSubscriber(Me.SubscriberID.Value, ApplicationSettings)
    End If
    BRSubscriberLabelDescription()
  End Sub

  Private Sub BRSubscriberTypeInd(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If SetEnableDisableYN Then
      Me.SubscriberBillingGroup.Enabled = False
      Me.SubscriberBillingGroup.Mandatory = False
      Select Case Element.ValueAsObject
        Case Constants.Subscriber.Types.Both, Constants.Subscriber.Types.Enquiries
          Me.SubscriberBillingGroup.Enabled = True
          Me.SubscriberBillingGroup.Mandatory = True
      End Select
    End If

    If PerformRulesYN Then
      Select Case Element.ValueAsObject
        Case Constants.Subscriber.Types.DataSupplier
          Me.SubscriberBillingGroup.Instance = Nothing
      End Select
    End If
  End Sub

  Public Sub BRSubscriberLabelDescription()
    If Me.IsNew Then
      Me.SubscriberLabelDescription.Value = "Subscriber: (New)"
    Else
      Dim sDesc As String = Trim(Me.SubscriberName.Value)
      Me.SubscriberLabelDescription.Value = "Subscriber: " & Me.SubscriberID.Value & " (" & sDesc & ")"
    End If
  End Sub
#End Region

#Region " Methods"
  Public Sub AddUnder(ByVal ParentSubscriber As Subscriber)
    If ParentSubscriber.LevelNo.Value = 5 Then
      Throw New Exception("Create proper error: 5 levels max!")
    End If

    Me.LevelNo.Value = ParentSubscriber.LevelNo.Value + 1
    Select Case Me.LevelNo.Value
      Case 1
        Me.Level1Subscriber.Instance = ParentSubscriber
      Case 2
        Me.Level1Subscriber.Instance = ParentSubscriber.Level1Subscriber.Instance
        Me.Level2Subscriber.Instance = ParentSubscriber
      Case 3
        Me.Level1Subscriber.Instance = ParentSubscriber.Level1Subscriber.Instance
        Me.Level2Subscriber.Instance = ParentSubscriber.Level2Subscriber.Instance
        Me.Level3Subscriber.Instance = ParentSubscriber
      Case 4
        Me.Level1Subscriber.Instance = ParentSubscriber.Level1Subscriber.Instance
        Me.Level2Subscriber.Instance = ParentSubscriber.Level2Subscriber.Instance
        Me.Level3Subscriber.Instance = ParentSubscriber.Level3Subscriber.Instance
        Me.Level4Subscriber.Instance = ParentSubscriber
      Case 5
        Me.Level1Subscriber.Instance = ParentSubscriber.Level1Subscriber.Instance
        Me.Level2Subscriber.Instance = ParentSubscriber.Level2Subscriber.Instance
        Me.Level3Subscriber.Instance = ParentSubscriber.Level3Subscriber.Instance
        Me.Level4Subscriber.Instance = ParentSubscriber.Level4Subscriber.Instance
        Me.Level5Subscriber.Instance = ParentSubscriber
    End Select
  End Sub
#End Region

#Region " Shared Procedures"
  Public Shared Function InsertSubscriberProfileProducts(ByVal SubscriberID As Long, ByVal ApplicationSettings As ApplicationSettings) As Boolean
    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand

    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spSubscriber_I_SubscriberProfileProduct"

    oCommand.Parameters.Add("@SubscriberID", SqlDbType.Int)
    oCommand.Parameters.Add("@Username", SqlDbType.VarChar, 50)

    oCommand.Parameters("@SubscriberID").Value = SubscriberID
    oCommand.Parameters("@Username").Value = SystemUser.GetLoggedInSystemUser(ApplicationSettings).Username.Value

    ApplicationSettings.ActionQuery(oCommand)
  End Function

  Public Shared Sub InsertSubscriberProfileProductAuthenticationQuestion(ByVal SubscriberID As Long, ByVal ApplicationSettings As ApplicationSettings)
    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand

    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spSubscriber_I_SubscriberProfileProductAuthenticationQuestion"

    oCommand.Parameters.Add("@SubscriberID", SqlDbType.Int)
    oCommand.Parameters.Add("@Username", SqlDbType.VarChar, 50)

    oCommand.Parameters("@SubscriberID").Value = SubscriberID
    oCommand.Parameters("@Username").Value = SystemUser.GetLoggedInSystemUser(ApplicationSettings).Username.Value

    ApplicationSettings.ActionQuery(oCommand)
  End Sub

  Public Shared Sub InsertSubscriberProfileProductAuthenticationSubscriber(ByVal SubscriberID As Long, ByVal ApplicationSettings As ApplicationSettings)
    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand

    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spSubscriber_I_SubscriberProfileProductAuthenticationSubscriber"

    oCommand.Parameters.Add("@SubscriberID", SqlDbType.Int)
    oCommand.Parameters.Add("@Username", SqlDbType.VarChar, 50)

    oCommand.Parameters("@SubscriberID").Value = SubscriberID
    oCommand.Parameters("@Username").Value = SystemUser.GetLoggedInSystemUser(ApplicationSettings).Username.Value

    ApplicationSettings.ActionQuery(oCommand)
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class SubscriberRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New Subscriber(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberID.ForeignElement = CType(Instance, Subscriber).SubscriberID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0Subscriber")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0Subscriber].[SubscriberID]", "SubscriberID", DataType.Long, Nothing, True, 1, True, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberName", "[A0Subscriber].[SubscriberName]", "SubscriberName", DataType.String, Nothing, False, 0, True, 200, "Subscriber Name"))
    oList.Columns.Add(New ListColumn("StatusInd", "[A0Subscriber].[StatusInd]", "StatusInd", DataType.String, Nothing, False, 0, True, 100, "Status"))
    oList.Columns.Add(New ListColumn("SubscriberTypeInd", "[A0Subscriber].[SubscriberTypeInd]", "SubscriberTypeInd", DataType.String, Nothing, False, 0, True, 150, "Subscriber Type"))
    oList.Columns.Add(New ListColumn("LevelNo", "[A0Subscriber].[LevelNo]", "LevelNo", DataType.Integer, Nothing, False, 0, False, 100, "Level No"))
    oList.Columns.Add(New ListColumn("SubscriberBillingGroupCode", "[A1SubscriberBillingGroup].[SubscriberBillingGroupCode]", "SubscriberBillingGroupCode", DataType.String, Nothing, False, 0, False, 100, "Billing Group Code"))
    oList.Columns.Add(New ListColumn("SubscriberBillingGroupDesc", "[A1SubscriberBillingGroup].[SubscriberBillingGroupDesc]", "SubscriberBillingGroupDesc", DataType.String, Nothing, False, 0, True, 200, "Billing Group Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "([Subscriber]  [A0Subscriber]" + _
           "    left join [SubscriberBillingGroup]  [A1SubscriberBillingGroup] on [A0Subscriber].[SubscriberBillingGroupCode] = [A1SubscriberBillingGroup].[SubscriberBillingGroupCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function DropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0Subscriber")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0Subscriber].[SubscriberID]", "SubscriberID", DataType.Long, Nothing, True, 1, True, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberName", "[A0Subscriber].[SubscriberName]", "SubscriberName", DataType.String, Nothing, False, 0, True, 200, "Subscriber Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "[Subscriber]  [A0Subscriber]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0Subscriber")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0Subscriber].[SubscriberID]", "SubscriberID", DataType.Long, Nothing, True, 1, True, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberName", "[A0Subscriber].[SubscriberName]", "SubscriberName", DataType.String, Nothing, False, 0, True, 200, "Subscriber Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "[Subscriber]  [A0Subscriber]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function




#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
