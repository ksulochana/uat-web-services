Imports sembleWare.Runtime
Imports System
Public Class SecurityForm 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly AssemblyName As New StringElement("AssemblyName", Me, True, True, True, True, True, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FormName As New StringElement("FormName", Me, True, False, True, True, True, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FormCaption As New StringElement("FormCaption", Me, False, False, True, True, False, 250, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly IsActiveYN As New BooleanElement("IsActiveYN", Me, False, True, True, True, True, "Is Active?", Nothing, True, "Yes;No")
  Public ReadOnly SecurityModule As Relationship = New SecurityModuleRelationship("SecurityModule", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly SecurityControl_OwnMany As Relationship = New SecurityControlRelationship("SecurityControl", Nothing, "SecurityForm", Me)
  Public ReadOnly SecurityFormAccess_OwnMany As Relationship = New RoleFormAccessRelationship("SecurityFormAccess", Nothing, "SecurityForm", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _ModuleCode As New StringElement("ModuleCode", Me, False)
  Public ReadOnly _SecurityModule As SecurityModuleRelationship = SecurityModule
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  Public ReadOnly _SecurityControl_OwnMany As SecurityControlRelationship = SecurityControl_OwnMany
  Public ReadOnly _SecurityFormAccess_OwnMany As RoleFormAccessRelationship = SecurityFormAccess_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SecurityForm", ApplicationSettings)
    _SecurityModule._ModuleCode.LocalElement = _ModuleCode
    'sembleWare: Constructor End - Do Not Modify
    Me.ForceUpperCaseKeys = False
  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class SecurityFormRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _AssemblyName As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _FormName As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SecurityForm(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _AssemblyName.ForeignElement = CType(Instance, SecurityForm).AssemblyName
    _FormName.ForeignElement = CType(Instance, SecurityForm).FormName
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"




  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SecurityForm")
    oList.Columns.Add(New ListColumn("FormName", "[A0SecurityForm].[FormName]", "FormName", DataType.String, Nothing, True, 1, True, 200, "Form Name"))
    oList.Columns.Add(New ListColumn("FormCaption", "[A0SecurityForm].[FormCaption]", "FormCaption", DataType.String, Nothing, False, 0, True, 200, "Form Caption"))
    oList.Columns.Add(New ListColumn("ModuleCode", "[A1SecurityModule].[ModuleCode]", "ModuleCode", DataType.String, Nothing, False, 0, True, 100, "Module Code"))
    oList.Columns.Add(New ListColumn("ModuleDesc", "[A1SecurityModule].[ModuleDesc]", "ModuleDesc", DataType.String, Nothing, False, 0, True, 200, "Module Description"))
    oList.Columns.Add(New ListColumn("AssemblyName", "[A0SecurityForm].[AssemblyName]", "AssemblyName", DataType.String, Nothing, True, 0, False, 100, "Assembly Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "([SecurityForm]  [A0SecurityForm]" + _
           "    left join [SecurityModule]  [A1SecurityModule] on [A0SecurityForm].[ModuleCode] = [A1SecurityModule].[ModuleCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SecurityForm")
    oList.Columns.Add(New ListColumn("ModuleCode", "[A1SecurityModule].[ModuleCode]", "ModuleCode", DataType.String, Nothing, False, 1, True, 200, "Module Code"))
    oList.Columns.Add(New ListColumn("ModuleDesc", "[A1SecurityModule].[ModuleDesc]", "ModuleDesc", DataType.String, Nothing, False, 0, True, 200, "Module Description"))
    oList.Columns.Add(New ListColumn("FormName", "[A0SecurityForm].[FormName]", "FormName", DataType.String, Nothing, True, 2, True, 200, "Form Name"))
    oList.Columns.Add(New ListColumn("FormCaption", "[A0SecurityForm].[FormCaption]", "FormCaption", DataType.String, Nothing, False, 0, True, 200, "Form Caption"))
    oList.Columns.Add(New ListColumn("AssemblyName", "[A0SecurityForm].[AssemblyName]", "AssemblyName", DataType.String, Nothing, True, 0, False, 100, "Assembly Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "([SecurityForm]  [A0SecurityForm]" + _
           "    left join [SecurityModule]  [A1SecurityModule] on [A0SecurityForm].[ModuleCode] = [A1SecurityModule].[ModuleCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function DropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SecurityForm")
    oList.Columns.Add(New ListColumn("FormName", "[A0SecurityForm].[FormName]", "FormName", DataType.String, Nothing, True, 1, True, 200, "Form Name"))
    oList.Columns.Add(New ListColumn("FormCaption", "[A0SecurityForm].[FormCaption]", "FormCaption", DataType.String, Nothing, False, 0, True, 200, "Form Caption"))
    oList.Columns.Add(New ListColumn("ModuleCode", "[A1SecurityModule].[ModuleCode]", "ModuleCode", DataType.String, Nothing, False, 0, True, 200, "Module Code"))
    oList.Columns.Add(New ListColumn("ModuleDesc", "[A1SecurityModule].[ModuleDesc]", "ModuleDesc", DataType.String, Nothing, False, 0, True, 200, "Module Description"))
    oList.Columns.Add(New ListColumn("AssemblyName", "[A0SecurityForm].[AssemblyName]", "AssemblyName", DataType.String, Nothing, True, 0, False, 100, "Assembly Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "([SecurityForm]  [A0SecurityForm]" + _
           "    left join [SecurityModule]  [A1SecurityModule] on [A0SecurityForm].[ModuleCode] = [A1SecurityModule].[ModuleCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
