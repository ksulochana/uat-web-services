Imports sembleWare.Runtime
Imports System
Public Class SubscriberSAFPSSubjectEnquiryResult 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly IDNo As New StringElement("IDNo", Me, False, False, True, True, False, 13, "ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PassportNo As New StringElement("PassportNo", Me, False, False, True, True, False, 16, "Passport No / Other ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FirstName As New StringElement("FirstName", Me, False, False, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SecondName As New StringElement("SecondName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Surname As New StringElement("Surname", Me, False, False, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly BirthDate As New DateTimeElement("BirthDate", Me, False, False, True, True, False, "Date of Birth", Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly GenderInd As New IndicatorElement("GenderInd", Me, False, False, False, True, True, False, "Gender", Nothing, SubscriberSAFPSSubjectEnquiryResult.GenderIndOptions, Nothing)
  Public ReadOnly DetailsViewedYN As New BooleanElement("DetailsViewedYN", Me, False, False, True, True, True, "Details Viewed?", Nothing, False, "Yes;No")
  Public ReadOnly DetailsViewedDate As New DateTimeElement("DetailsViewedDate", Me, False, False, True, True, False, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly SAFPSSubjectSelectedYN As New BooleanElement("SAFPSSubjectSelectedYN", Me, False, True, True, True, True, "SAFPSSubject Selected?", Nothing, False, "Yes;No")
  Public ReadOnly SubscriberSAFPSSubjectEnquiry As Relationship = New SubscriberSAFPSSubjectEnquiryRelationship("SubscriberSAFPSSubjectEnquiry", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly SAFPSSubject As Relationship = New SAFPSSubjectRelationship("SAFPSSubject", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _SubscriberSAFPSSubjectEnquiryID As New IntegerElement("SubscriberSAFPSSubjectEnquiryID", Me, True)
  Public ReadOnly _SAFPSSubjectID As New IntegerElement("SAFPSSubjectID", Me, True)
  Public ReadOnly _SubscriberSAFPSSubjectEnquiry As SubscriberSAFPSSubjectEnquiryRelationship = SubscriberSAFPSSubjectEnquiry
  Public ReadOnly _SAFPSSubject As SAFPSSubjectRelationship = SAFPSSubject
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moGenderIndOptions As IndicatorOptions
  Public Shared ReadOnly Property GenderIndOptions() As IndicatorOptions
    Get
      If moGenderIndOptions Is Nothing Then
        moGenderIndOptions = New IndicatorOptions
        moGenderIndOptions.Add("M", "Male")
        moGenderIndOptions.Add("F", "Female")
      End If
      Return moGenderIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberSAFPSSubjectEnquiryResult", ApplicationSettings)
    _SubscriberSAFPSSubjectEnquiry._SubscriberID.LocalElement = _SubscriberID
    _SubscriberSAFPSSubjectEnquiry._SubscriberSAFPSSubjectEnquiryID.LocalElement = _SubscriberSAFPSSubjectEnquiryID
    _SAFPSSubject._SAFPSSubjectID.LocalElement = _SAFPSSubjectID
    AddHandler DetailsViewedYN.Changed, AddressOf DetailsViewedYN_Changed
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub DetailsViewedYN_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"
  Public Sub SelectRecord()
    With ApplicationSettings
      Try
        .BeginTransaction()
        Me.SAFPSSubjectSelectedYN.Value = True
        Me.DetailsViewedYN.Value = True
        Me.DetailsViewedDate.Value = System.DateTime.Now
        Me.Save()

        Dim oSAFPSSubject As SAFPSSubject = Me.SAFPSSubject.Instance
        Dim oSubscriberSAFPSSubjectEnquiry As SubscriberSAFPSSubjectEnquiry = Me.SubscriberSAFPSSubjectEnquiry.Instance
        oSubscriberSAFPSSubjectEnquiry.EnquiryResultInd.Value = Constants.SubscriberSAFPSSubjectEnquiry.Results.RecordSelected

        oSubscriberSAFPSSubjectEnquiry.ResultIDNo.Value = oSAFPSSubject.IDNo.Value
        oSubscriberSAFPSSubjectEnquiry.ResultPassportNo.Value = oSAFPSSubject.PassportNo.Value
        oSubscriberSAFPSSubjectEnquiry.ResultFirstName.Value = oSAFPSSubject.FirstName.Value
        oSubscriberSAFPSSubjectEnquiry.ResultSecondName.Value = oSAFPSSubject.SecondName.Value
        oSubscriberSAFPSSubjectEnquiry.ResultSurname.Value = oSAFPSSubject.Surname.Value
        oSubscriberSAFPSSubjectEnquiry.ResultBirthDate.ValueAsObject = oSAFPSSubject.BirthDate.ValueAsObject
        oSubscriberSAFPSSubjectEnquiry.ResultGenderInd.Value = oSAFPSSubject.GenderInd.Value

        oSubscriberSAFPSSubjectEnquiry.Save()
        .CommitTransaction()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
    End With
  End Sub
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class SubscriberSAFPSSubjectEnquiryResultRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SubscriberSAFPSSubjectEnquiryID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SAFPSSubjectID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberSAFPSSubjectEnquiryResult(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberID.ForeignElement = CType(Instance, SubscriberSAFPSSubjectEnquiryResult)._SubscriberID
    _SubscriberSAFPSSubjectEnquiryID.ForeignElement = CType(Instance, SubscriberSAFPSSubjectEnquiryResult)._SubscriberSAFPSSubjectEnquiryID
    _SAFPSSubjectID.ForeignElement = CType(Instance, SubscriberSAFPSSubjectEnquiryResult)._SAFPSSubjectID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberSAFPSSubjectEnquiryResult")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberSAFPSSubjectEnquiryResult].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberSAFPSSubjectEnquiryID", "[A0SubscriberSAFPSSubjectEnquiryResult].[SubscriberSAFPSSubjectEnquiryID]", "SubscriberSAFPSSubjectEnquiryID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber SAFPSSubject Enquiry ID"))
    oList.Columns.Add(New ListColumn("SAFPSSubjectID", "[A0SubscriberSAFPSSubjectEnquiryResult].[SAFPSSubjectID]", "SAFPSSubjectID", DataType.Integer, Nothing, True, 0, False, 100, "SAFPSSubject ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A0SubscriberSAFPSSubjectEnquiryResult].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("PassportNo", "[A0SubscriberSAFPSSubjectEnquiryResult].[PassportNo]", "PassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No / Other ID No"))
    oList.Columns.Add(New ListColumn("FirstName", "[A0SubscriberSAFPSSubjectEnquiryResult].[FirstName]", "FirstName", DataType.String, Nothing, False, 2, True, 150, "First Name"))
    oList.Columns.Add(New ListColumn("Surname", "[A0SubscriberSAFPSSubjectEnquiryResult].[Surname]", "Surname", DataType.String, Nothing, False, 1, True, 150, "Surname"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A0SubscriberSAFPSSubjectEnquiryResult].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Date of Birth"))
    oList.Columns.Add(New ListColumn("GenderInd", "[A0SubscriberSAFPSSubjectEnquiryResult].[GenderInd]", "GenderInd", DataType.String, Nothing, False, 0, True, 100, "Gender"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberSAFPSSubjectEnquiryResult]  [A0SubscriberSAFPSSubjectEnquiryResult]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function MultipleMatchGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberSAFPSSubjectEnquiryResult")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberSAFPSSubjectEnquiryResult].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberSAFPSSubjectEnquiryID", "[A0SubscriberSAFPSSubjectEnquiryResult].[SubscriberSAFPSSubjectEnquiryID]", "SubscriberSAFPSSubjectEnquiryID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber SAFPSSubject Enquiry ID"))
    oList.Columns.Add(New ListColumn("SAFPSSubjectID", "[A0SubscriberSAFPSSubjectEnquiryResult].[SAFPSSubjectID]", "SAFPSSubjectID", DataType.Integer, Nothing, True, 0, False, 100, "SAFPSSubject ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A0SubscriberSAFPSSubjectEnquiryResult].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("PassportNo", "[A0SubscriberSAFPSSubjectEnquiryResult].[PassportNo]", "PassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No / Other ID No"))
    oList.Columns.Add(New ListColumn("FirstName", "[A0SubscriberSAFPSSubjectEnquiryResult].[FirstName]", "FirstName", DataType.String, Nothing, False, 2, True, 150, "First Name"))
    oList.Columns.Add(New ListColumn("Surname", "[A0SubscriberSAFPSSubjectEnquiryResult].[Surname]", "Surname", DataType.String, Nothing, False, 1, True, 150, "Surname"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A0SubscriberSAFPSSubjectEnquiryResult].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Date of Birth"))
    oList.Columns.Add(New ListColumn("GenderInd", "[A0SubscriberSAFPSSubjectEnquiryResult].[GenderInd]", "GenderInd", DataType.String, Nothing, False, 0, True, 100, "Gender"))
    oList.Columns.Add(New ListColumn("DetailsViewedYN", "[A0SubscriberSAFPSSubjectEnquiryResult].[DetailsViewedYN]", "DetailsViewedYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Details Viewed?"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberSAFPSSubjectEnquiryResult]  [A0SubscriberSAFPSSubjectEnquiryResult]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberSAFPSSubjectEnquiryResult")
    oList.Columns.Add(New ListColumn("SAFPSSubjectID", "[A0SubscriberSAFPSSubjectEnquiryResult].[SAFPSSubjectID]", "SAFPSSubjectID", DataType.Integer, Nothing, True, 0, False, 100, "SAFPSSubject ID"))
    oList.Columns.Add(New ListColumn("SubscriberSAFPSSubjectEnquiryID", "[A0SubscriberSAFPSSubjectEnquiryResult].[SubscriberSAFPSSubjectEnquiryID]", "SubscriberSAFPSSubjectEnquiryID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber SAFPSSubject Enquiry ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberSAFPSSubjectEnquiryResult].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A0SubscriberSAFPSSubjectEnquiryResult].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("PassportNo", "[A0SubscriberSAFPSSubjectEnquiryResult].[PassportNo]", "PassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No / Other ID No"))
    oList.Columns.Add(New ListColumn("Surname", "[A0SubscriberSAFPSSubjectEnquiryResult].[Surname]", "Surname", DataType.String, Nothing, False, 0, True, 100, "Surname"))
    oList.Columns.Add(New ListColumn("FirstName", "[A0SubscriberSAFPSSubjectEnquiryResult].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A0SubscriberSAFPSSubjectEnquiryResult].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Date of Birth"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberSAFPSSubjectEnquiryResult]  [A0SubscriberSAFPSSubjectEnquiryResult]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
