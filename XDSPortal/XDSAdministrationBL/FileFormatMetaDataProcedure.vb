Imports sembleWare.Runtime
Imports System
Public Class FileFormatMetaDataProcedure 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly SequenceNum As New IntegerElement("SequenceNum", Me, False, False, True, True, True, "Sequence Number", Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FileFormatMetaDataProcedureLabelDescription As New StringElement("FileFormatMetaDataProcedureLabelDescription", Me, False, True, True, False, False, 1000, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FileFormatField As Relationship = New FileFormatFieldRelationship("FileFormatField", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly DataProcedure As Relationship = New DataProcedureRelationship("DataProcedure", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly FileFormatMetaDatabase As Relationship = New FileFormatMetaDatabaseRelationship("FileFormatMetaDatabase", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly MetaDatabase As Relationship = New MetaDatabaseRelationship("MetaDatabase", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly FileFormat As Relationship = New FileFormatRelationship("FileFormat", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly FileFormatMetaDataProcedureParameter_OwnMany As Relationship = New FileFormatMetaDataProcedureParameterRelationship("FileFormatMetaDataProcedureParameter", Nothing, "FileFormatMetaDataProcedure", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _FileFormatFieldID As New IntegerElement("FileFormatFieldID", Me, True)
  Public ReadOnly _FileFormatCode As New StringElement("FileFormatCode", Me, True)
  Public ReadOnly _DataProcedureID As New IntegerElement("DataProcedureID", Me, True)
  Public ReadOnly _MetaDatabaseCode As New StringElement("MetaDatabaseCode", Me, True)
  Public ReadOnly _FileFormatField As FileFormatFieldRelationship = FileFormatField
  Public ReadOnly _DataProcedure As DataProcedureRelationship = DataProcedure
  Public ReadOnly _FileFormatMetaDatabase As FileFormatMetaDatabaseRelationship = FileFormatMetaDatabase
  Public ReadOnly _MetaDatabase As MetaDatabaseRelationship = MetaDatabase
  Public ReadOnly _FileFormat As FileFormatRelationship = FileFormat
  Public ReadOnly _FileFormatMetaDataProcedureParameter_OwnMany As FileFormatMetaDataProcedureParameterRelationship = FileFormatMetaDataProcedureParameter_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("FileFormatMetaDataProcedure", ApplicationSettings)
    _FileFormatField._FileFormatFieldID.LocalElement = _FileFormatFieldID
    _FileFormatField._FileFormatCode.LocalElement = _FileFormatCode
    _DataProcedure._DataProcedureID.LocalElement = _DataProcedureID
    _FileFormatMetaDatabase._FileFormatCode.LocalElement = _FileFormatCode
    _FileFormatMetaDatabase._MetaDatabaseCode.LocalElement = _MetaDatabaseCode
    _MetaDatabase._MetaDatabaseCode.LocalElement = _MetaDatabaseCode
    _FileFormat._FileFormatCode.LocalElement = _FileFormatCode
    'sembleWare: Constructor End - Do Not Modify
    AddHandler DataProcedure.LimitRelatedList, AddressOf MetaDataProcedure_LimitRelatedList
    AddHandler FileFormatField.LimitRelatedList, AddressOf FileFormatField_LimitRelatedList
  End Sub

#End Region 'sembleWare: Constructor

#Region " LimitRelatedList Events"
  Private Sub MetaDataProcedure_LimitRelatedList(ByVal List As List)
    Dim oDBList As DBList = List
    oDBList.AppendToWhereClause("AppliesToInd = 'A' or (AppliesToInd = 'M' and MetaDatabaseCode = " & ApplicationSettings.QueryBuilder.ToSQL(Me._MetaDatabaseCode.Value) & ")")
  End Sub

  Private Sub FileFormatField_LimitRelatedList(ByVal List As List)
    List.LimitByRelatedPart(Me.FileFormat.Instance, "FileFormat", True)
  End Sub
#End Region

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"

  Public Sub MoveUp()
    If Me.SequenceNum.Value = 0 Then
      Return
    End If
    Me.SequenceNum.Value = Me.SequenceNum.Value - 1
    Save()
  End Sub

  Public Sub MoveDown()
    Dim nSequenceNum = Utility.GetNextSequenceNumber(Me.TableName, Me.FileFormatMetaDatabase.Instance, ApplicationSettings) - 1
    If Me.SequenceNum.Value = nSequenceNum Then
      Return
    End If
    Me.SequenceNum.Value = Me.SequenceNum.Value + 1
    Save()
  End Sub

#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreate()
  End Sub

  Public Overrides Sub Load()
    MyBase.Load()
    BRLoad()
  End Sub

  Public Overrides Sub Save()
    With ApplicationSettings
      .BeginTransaction()
      Try
        Dim bIsNewYN As Boolean = Me.IsNew
        BRBeforeSave(bIsNewYN)
        MyBase.Save()
        BRAfterSave(bIsNewYN)
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
      .CommitTransaction()
    End With
  End Sub

  Public Overrides Sub Delete()
    With ApplicationSettings
      .BeginTransaction()
      Try
        BRBeforeDelete()
        MyBase.Delete()
        BRAfterDelete()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
      .CommitTransaction()
    End With
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreate()
    BRFileFormatMetaDataProcedureLabelDescription()
  End Sub

  Private Sub BRLoad()
    BRFileFormatMetaDataProcedureLabelDescription()
  End Sub

  Private Sub BRBeforeSave(ByVal IsNewYN As Boolean)
    If IsNewYN Then
      If Me.SequenceNum.IsEmpty Then
        Me.SequenceNum.Value = Utility.GetNextSequenceNumber(Me.TableName, BRGetSequenceWhereClause(), ApplicationSettings)
      End If
    End If
    Utility.UpdateSequenceNumber(Me, BRGetSequenceWhereClause(), False, ApplicationSettings)
  End Sub

  Public Sub BRAfterSave(ByVal IsNewYN As Boolean)
    If IsNewYN Then
      InsertDataProcedureParameters(Me, ApplicationSettings)
    End If
    BRFileFormatMetaDataProcedureLabelDescription()
  End Sub

  Private Sub BRBeforeDelete()
    DeleteDataProcedureParameters(Me, ApplicationSettings)
  End Sub

  Private Sub BRAfterDelete()
    Utility.UpdateSequenceNumber(Me, Me.FileFormatMetaDatabase.Instance, True, ApplicationSettings)
  End Sub

  Private Function BRGetSequenceWhereClause() As String
    Dim oDataProcedure As DataProcedure = Me.DataProcedure.Instance
    Dim sSequenceWhere As String = CustomDBPart.InstanceWhereClause(Me.FileFormatMetaDatabase.Instance)
    sSequenceWhere &= " and DataProcedureID in (select DataProcedureID from DataProcedure where DataProcedureTypeInd = " & ApplicationSettings.QueryBuilder.ToSQL(oDataProcedure.DataProcedureTypeInd.Value) & ")"

    Return sSequenceWhere
  End Function

  Private Sub BRFileFormatMetaDataProcedureLabelDescription()
    Dim oFileFormatMetaDatabase As FileFormatMetaDatabase = Me.FileFormatMetaDatabase.Instance
    Dim oFileFormatField As FileFormatField = Me.FileFormatField.Instance
    Dim oDataProcedure As DataProcedure = Me.DataProcedure.Instance
    Dim sFileFormatFieldDesc As String = Trim(oFileFormatField.FileFormatFieldDesc.Value)

    If Me.IsNew Then
      Me.FileFormatMetaDataProcedureLabelDescription.Value = oFileFormatMetaDatabase.FileFormatMetaDatabaseLabelDescription.Value & " - Field: " & oFileFormatField.FileFormatFieldID.Value & " (" & sFileFormatFieldDesc & ")" & " - Data Procedure: (New)"
    Else
      Dim sDataProcedureDesc As String = Trim(oDataProcedure.DataProcedureDesc.Value)
      Me.FileFormatMetaDataProcedureLabelDescription.Value = oFileFormatMetaDatabase.FileFormatMetaDatabaseLabelDescription.Value & " - Field: " & oFileFormatField.FileFormatFieldID.Value & " (" & sFileFormatFieldDesc & ")" & " - Data Procedure: " & oDataProcedure.DataProcedureID.Value & " (" & sDataProcedureDesc & ")"
    End If
  End Sub
#End Region

#Region " Shared Procedures"
  Public Shared Sub InsertDataProcedureParameters(ByVal FileFormatMetaDataProcedure As FileFormatMetaDataProcedure, ByVal ApplicationSettings As ApplicationSettings)
    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand

    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spFileFormatMetaDataProcedure_I_DataProcedureParameter"

    oCommand.Parameters.Add("@FileFormatCode", SqlDbType.VarChar, 10)
    oCommand.Parameters.Add("@FileFormatFieldID", SqlDbType.Int)
    oCommand.Parameters.Add("@MetaDatabaseCode", SqlDbType.VarChar, 10)
    oCommand.Parameters.Add("@DataProcedureID", SqlDbType.Int)

    oCommand.Parameters("@FileFormatCode").Value = FileFormatMetaDataProcedure._FileFormatCode.Value
    oCommand.Parameters("@FileFormatFieldID").Value = FileFormatMetaDataProcedure._FileFormatFieldID.Value
    oCommand.Parameters("@MetaDatabaseCode").Value = FileFormatMetaDataProcedure._MetaDatabaseCode.Value
    oCommand.Parameters("@DataProcedureID").Value = FileFormatMetaDataProcedure._DataProcedureID.Value

    ApplicationSettings.ActionQuery(oCommand)
  End Sub

  Public Shared Sub DeleteDataProcedureParameters(ByVal FileFormatMetaDataProcedure As FileFormatMetaDataProcedure, ByVal ApplicationSettings As ApplicationSettings)
    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand

    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spFileFormatMetaDataProcedure_D_DataProcedureParameter"

    oCommand.Parameters.Add("@FileFormatCode", SqlDbType.VarChar, 10)
    oCommand.Parameters.Add("@FileFormatFieldID", SqlDbType.Int)
    oCommand.Parameters.Add("@MetaDatabaseCode", SqlDbType.VarChar, 10)
    oCommand.Parameters.Add("@DataProcedureID", SqlDbType.Int)

    oCommand.Parameters("@FileFormatCode").Value = FileFormatMetaDataProcedure._FileFormatCode.Value
    oCommand.Parameters("@FileFormatFieldID").Value = FileFormatMetaDataProcedure._FileFormatFieldID.Value
    oCommand.Parameters("@MetaDatabaseCode").Value = FileFormatMetaDataProcedure._MetaDatabaseCode.Value
    oCommand.Parameters("@DataProcedureID").Value = FileFormatMetaDataProcedure._DataProcedureID.Value

    ApplicationSettings.ActionQuery(oCommand)
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class FileFormatMetaDataProcedureRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _FileFormatFieldID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _FileFormatCode As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _DataProcedureID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _MetaDatabaseCode As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New FileFormatMetaDataProcedure(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _FileFormatFieldID.ForeignElement = CType(Instance, FileFormatMetaDataProcedure)._FileFormatFieldID
    _FileFormatCode.ForeignElement = CType(Instance, FileFormatMetaDataProcedure)._FileFormatCode
    _DataProcedureID.ForeignElement = CType(Instance, FileFormatMetaDataProcedure)._DataProcedureID
    _MetaDatabaseCode.ForeignElement = CType(Instance, FileFormatMetaDataProcedure)._MetaDatabaseCode
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0FileFormatMetaDataProcedure")
    oList.Columns.Add(New ListColumn("FileFormatCode", "[A0FileFormatMetaDataProcedure].[FileFormatCode]", "FileFormatCode", DataType.String, Nothing, True, 0, False, 100, "File Format Code"))
    oList.Columns.Add(New ListColumn("FileFormatFieldID", "[A0FileFormatMetaDataProcedure].[FileFormatFieldID]", "FileFormatFieldID", DataType.Integer, Nothing, True, 0, False, 100, "File Format Field ID"))
    oList.Columns.Add(New ListColumn("DataProcedureID1", "[A2DataProcedure].[DataProcedureID]", "DataProcedureID", DataType.Long, Nothing, False, 0, False, 100, "Data Procedure ID"))
    oList.Columns.Add(New ListColumn("DataProcedureDesc", "[A2DataProcedure].[DataProcedureDesc]", "DataProcedureDesc", DataType.String, Nothing, False, 0, True, 200, "Data Procedure Description"))
    oList.Columns.Add(New ListColumn("DataProcedureTypeInd", "[A2DataProcedure].[DataProcedureTypeInd]", "DataProcedureTypeInd", DataType.String, Nothing, False, 0, False, 100, "Data Procedure Type"))
    oList.Columns.Add(New ListColumn("ParameterTypeInd", "[A2DataProcedure].[ParameterTypeInd]", "ParameterTypeInd", DataType.String, Nothing, False, 0, False, 100, "Parameter Type"))
    oList.Columns.Add(New ListColumn("MetaDatabaseCode", "[A0FileFormatMetaDataProcedure].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, True, 0, False, 100, "Meta Database Code"))
    oList.Columns.Add(New ListColumn("FileFormatFieldDesc", "[A1FileFormatField].[FileFormatFieldDesc]", "FileFormatFieldDesc", DataType.String, Nothing, False, 0, True, 200, "Field Description "))
    oList.Columns.Add(New ListColumn("SequenceNum", "[A0FileFormatMetaDataProcedure].[SequenceNum]", "SequenceNum", DataType.Integer, Nothing, False, 1, False, 100, "Sequence Number"))
    oList.Columns.Add(New ListColumn("DataProcedureID", "[A0FileFormatMetaDataProcedure].[DataProcedureID]", "DataProcedureID", DataType.Integer, Nothing, True, 0, False, 100, "Data Procedure ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "(([FileFormatMetaDataProcedure]  [A0FileFormatMetaDataProcedure]" + _
           "    join [FileFormatField]  [A1FileFormatField] on [A0FileFormatMetaDataProcedure].[FileFormatFieldID] = [A1FileFormatField].[FileFormatFieldID] and [A0FileFormatMetaDataProcedure].[FileFormatCode] = [A1FileFormatField].[FileFormatCode])" + _
           "    join [DataProcedure]  [A2DataProcedure] on [A0FileFormatMetaDataProcedure].[DataProcedureID] = [A2DataProcedure].[DataProcedureID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
