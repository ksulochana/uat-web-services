Imports sembleWare.Runtime
Imports System
Public Class Director 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly DirectorID As New IdentityElement("DirectorID", Me, True, Nothing, Nothing)
  Public ReadOnly FirstInitial As New StringElement("FirstInitial", Me, False, False, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SecondInitial As New StringElement("SecondInitial", Me, False, False, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FirstName As New StringElement("FirstName", Me, False, False, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SecondName As New StringElement("SecondName", Me, False, False, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Surname As New StringElement("Surname", Me, False, False, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SurnameParticular As New StringElement("SurnameParticular", Me, False, False, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SurnamePrevious As New StringElement("SurnamePrevious", Me, False, False, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly IDNo As New StringElement("IDNo", Me, False, False, True, True, False, 13, "ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly BirthDate As New DateTimeElement("BirthDate", Me, False, False, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly IsPossibleNameConflictYN As New BooleanElement("IsPossibleNameConflictYN", Me, False, False, True, True, True, "Is Possible Name Conflict?", Nothing, False, "Yes;No")
  Public ReadOnly IsPossibleDuplicateRecordYN As New BooleanElement("IsPossibleDuplicateRecordYN", Me, False, False, True, True, True, "Is Possible Duplicate Record?", Nothing, False, "Yes;No")
  Public ReadOnly RecordStatusInd As New IndicatorElement("RecordStatusInd", Me, False, False, False, True, True, True, "Record Status", Nothing, Director.RecordStatusIndOptions, "A")
  Public ReadOnly DeletedReason As New StringElement("DeletedReason", Me, False, True, True, True, False, 500, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LastUpdatedDate As New DateTimeElement("LastUpdatedDate", Me, False, False, True, True, False, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly DirectorLabelDescription As New StringElement("DirectorLabelDescription", Me, False, True, True, False, False, 250, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly DirectorAddress_OwnMany As Relationship = New DirectorAddressRelationship("DirectorAddress", Nothing, "Director", Me)
  Public ReadOnly DirectorName_OwnMany As Relationship = New DirectorNameRelationship("DirectorName", Nothing, "Director", Me)
  Public ReadOnly DirectorNameSoundEx_OwnMany As Relationship = New DirectorNameSoundExRelationship("DirectorNameSoundEx", Nothing, "Director", Me)
  Public ReadOnly DirectorConflict_OwnMany As Relationship = New DirectorConflictRelationship("DirectorConflict", Nothing, "Director", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  Public ReadOnly _DirectorAddress_OwnMany As DirectorAddressRelationship = DirectorAddress_OwnMany
  Public ReadOnly _DirectorName_OwnMany As DirectorNameRelationship = DirectorName_OwnMany
  Public ReadOnly _DirectorNameSoundEx_OwnMany As DirectorNameSoundExRelationship = DirectorNameSoundEx_OwnMany
  Public ReadOnly _DirectorConflict_OwnMany As DirectorConflictRelationship = DirectorConflict_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moRecordStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property RecordStatusIndOptions() As IndicatorOptions
    Get
      If moRecordStatusIndOptions Is Nothing Then
        moRecordStatusIndOptions = New IndicatorOptions
        moRecordStatusIndOptions.Add("A", "Active")
        moRecordStatusIndOptions.Add("D", "Deleted")
      End If
      Return moRecordStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("Director", ApplicationSettings)
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreate()
  End Sub

  Public Overrides Sub Load()
    MyBase.Load()
    BRLoad()
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreate()
    Me.LastUpdatedDate.Value = System.DateTime.Now
    BRDirectorLabelDescription()
  End Sub

  Private Sub BRLoad()
    BRDirectorLabelDescription()
  End Sub

  Private Sub BRDirectorLabelDescription()
    If Me.IsNew Then
      Me.DirectorLabelDescription.Value = "Director: (New)"
    Else
      Dim sDesc As String = Trim(Me.FirstName.Value & " " & Me.Surname.Value)
      Me.DirectorLabelDescription.Value = "Director: " & Me.DirectorID.Value & " (" & sDesc & ")"
    End If
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class DirectorRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _DirectorID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New Director(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _DirectorID.ForeignElement = CType(Instance, Director).DirectorID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0Director")
    oList.Columns.Add(New ListColumn("DirectorID", "[A0Director].[DirectorID]", "DirectorID", DataType.Long, Nothing, True, 1, True, 100, "Director ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A0Director].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("Surname", "[A0Director].[Surname]", "Surname", DataType.String, Nothing, False, 3, True, 100, "Surname"))
    oList.Columns.Add(New ListColumn("FirstName", "[A0Director].[FirstName]", "FirstName", DataType.String, Nothing, False, 2, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("FirstInitial", "[A0Director].[FirstInitial]", "FirstInitial", DataType.String, Nothing, False, 0, True, 100, "First Initial"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A0Director].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Birth Date"))
    oList.SelectStatement = "select"
    oList.FromClause = "[Director]  [A0Director]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0Director")
    oList.Columns.Add(New ListColumn("DirectorID", "[A0Director].[DirectorID]", "DirectorID", DataType.Long, Nothing, True, 1, False, 100, "Director ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A0Director].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("Surname", "[A0Director].[Surname]", "Surname", DataType.String, Nothing, False, 3, True, 100, "Surname"))
    oList.Columns.Add(New ListColumn("FirstName", "[A0Director].[FirstName]", "FirstName", DataType.String, Nothing, False, 2, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("FirstInitial", "[A0Director].[FirstInitial]", "FirstInitial", DataType.String, Nothing, False, 0, True, 100, "First Initial"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A0Director].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Birth Date"))
    oList.SelectStatement = "select"
    oList.FromClause = "[Director]  [A0Director]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ConflictForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0Director")
    oList.Columns.Add(New ListColumn("DirectorID", "[A0Director].[DirectorID]", "DirectorID", DataType.Long, Nothing, True, 1, True, 100, "Director ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A0Director].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("Surname", "[A0Director].[Surname]", "Surname", DataType.String, Nothing, False, 3, True, 100, "Surname"))
    oList.Columns.Add(New ListColumn("FirstName", "[A0Director].[FirstName]", "FirstName", DataType.String, Nothing, False, 2, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("FirstInitial", "[A0Director].[FirstInitial]", "FirstInitial", DataType.String, Nothing, False, 0, True, 100, "First Initial"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A0Director].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Birth Date"))
    oList.SelectStatement = "select"
    oList.FromClause = "[Director]  [A0Director]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ConflictGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0Director")
    oList.Columns.Add(New ListColumn("DirectorID", "[A0Director].[DirectorID]", "DirectorID", DataType.Long, Nothing, True, 1, True, 100, "Director ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A0Director].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("Surname", "[A0Director].[Surname]", "Surname", DataType.String, Nothing, False, 0, True, 100, "Surname"))
    oList.Columns.Add(New ListColumn("FirstName", "[A0Director].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("FirstInitial", "[A0Director].[FirstInitial]", "FirstInitial", DataType.String, Nothing, False, 0, True, 100, "First Initial"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A0Director].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Birth Date"))
    oList.Columns.Add(New ListColumn("IsPossibleNameConflictYN", "[A0Director].[IsPossibleNameConflictYN]", "IsPossibleNameConflictYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Is Possible Name Conflict?"))
    oList.Columns.Add(New ListColumn("IsPossibleDuplicateRecordYN", "[A0Director].[IsPossibleDuplicateRecordYN]", "IsPossibleDuplicateRecordYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Is Possible Duplicate Record?"))
    oList.SelectStatement = "select"
    oList.FromClause = "[Director]  [A0Director]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
