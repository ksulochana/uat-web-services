Imports sembleWare.Runtime
Imports System
Public Class Commercial 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly CommercialID As New IdentityElement("CommercialID", Me, True, Nothing, Nothing)
  Public ReadOnly RegistrationNo As New StringElement("RegistrationNo", Me, False, False, True, True, False, 14, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RegistrationNoOld As New StringElement("RegistrationNoOld", Me, False, False, True, True, False, 14, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RegistrationNoConverted As New StringElement("RegistrationNoConverted", Me, False, False, True, True, False, 14, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CommercialTypeDate As New DateTimeElement("CommercialTypeDate", Me, False, False, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly CommercialName As New StringElement("CommercialName", Me, False, False, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CommercialShortName As New StringElement("CommercialShortName", Me, False, False, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CommercialTranslatedName As New StringElement("CommercialTranslatedName", Me, False, False, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RegistrationDate As New DateTimeElement("RegistrationDate", Me, False, False, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly BusinessStartDate As New DateTimeElement("BusinessStartDate", Me, False, False, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly IsWithdrawnFromPublicYN As New BooleanElement("IsWithdrawnFromPublicYN", Me, False, False, True, True, True, "Is Withdrawn From Public?", Nothing, False, "Yes;No")
  Public ReadOnly CommercialStatusDate As New DateTimeElement("CommercialStatusDate", Me, False, False, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly FinancialYearEnd As New IndicatorElement("FinancialYearEnd", Me, False, True, False, True, True, False, Nothing, Nothing, Commercial.FinancialYearEndOptions, Nothing)
  Public ReadOnly FinancialEffectiveDate As New DateTimeElement("FinancialEffectiveDate", Me, False, False, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly AuthorisedCapitalAmt As New DecimalElement("AuthorisedCapitalAmt", Me, False, False, True, True, False, "Authorised Capital Amount", Nothing, "c", Nothing, Nothing, Nothing)
  Public ReadOnly AuthorisedNoOfShares As New DecimalElement("AuthorisedNoOfShares", Me, False, False, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly IssuedCapitalAmt As New DecimalElement("IssuedCapitalAmt", Me, False, False, True, True, False, Nothing, Nothing, "c", Nothing, Nothing, Nothing)
  Public ReadOnly IssuedNoOfShares As New DecimalElement("IssuedNoOfShares", Me, False, False, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CKReceivedDate As New DateTimeElement("CKReceivedDate", Me, False, False, True, True, False, "CK Received Date", Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly CKOnFormDate As New DateTimeElement("CKOnFormDate", Me, False, False, True, True, False, "CK On Form Date", Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly TaxNo As New StringElement("TaxNo", Me, False, False, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CountryOfOrigin As New StringElement("CountryOfOrigin", Me, False, False, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly IsPossibleNameConflictYN As New BooleanElement("IsPossibleNameConflictYN", Me, False, False, True, True, True, "Is Possible Name Conflict?", Nothing, False, "Yes;No")
  Public ReadOnly IsPossibleDuplicateRecordYN As New BooleanElement("IsPossibleDuplicateRecordYN", Me, False, False, True, True, True, "Is Possible Duplicate Record?", Nothing, False, "Yes;No")
  Public ReadOnly RecordStatusInd As New IndicatorElement("RecordStatusInd", Me, False, False, False, True, True, True, "Record Status", Nothing, Commercial.RecordStatusIndOptions, "A")
  Public ReadOnly DeletedReason As New StringElement("DeletedReason", Me, False, True, True, True, False, 500, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LastUpdatedDate As New DateTimeElement("LastUpdatedDate", Me, False, False, True, True, True, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly CommercialLabelDescription As New StringElement("CommercialLabelDescription", Me, False, False, True, False, False, 250, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SIC As Relationship = New SICRelationship("SIC", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly CommercialStatus As Relationship = New CommercialStatusRelationship("CommercialStatus", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly CommercialType As Relationship = New CommercialTypeRelationship("CommercialType", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, False, True, Me)
  Public ReadOnly CommercialAddress_OwnMany As Relationship = New CommercialAddressRelationship("CommercialAddress", Nothing, "Commercial", Me)
  Public ReadOnly CommercialTelephone_OwnMany As Relationship = New CommercialTelephoneRelationship("CommercialTelephone", Nothing, "Commercial", Me)
  Public ReadOnly CommercialDirector_OwnMany As Relationship = New CommercialDirectorRelationship("CommercialDirector", Nothing, "Commercial", Me)
  Public ReadOnly CommercialAuditor_OwnMany As Relationship = New CommercialAuditorRelationship("CommercialAuditor", Nothing, "Commercial", Me)
  Public ReadOnly CommercialHistory_OwnMany As Relationship = New CommercialHistoryRelationship("CommercialHistory", Nothing, "Commercial", Me)
  Public ReadOnly CommercialNames_OwnMany As Relationship = New CommercialNameRelationship("CommercialNames", Nothing, "Commercial", Me)
  Public ReadOnly CommercialNameSoundEx_OwnMany As Relationship = New CommercialNameSoundExRelationship("CommercialNameSoundEx", Nothing, "Commercial", Me)
  Public ReadOnly CommercialConflict_OwnMany As Relationship = New CommercialConflictRelationship("CommercialConflict", Nothing, "Commercial", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SICCode As New StringElement("SICCode", Me, False)
  Public ReadOnly _CommercialStatusCode As New StringElement("CommercialStatusCode", Me, False)
  Public ReadOnly _CommercialTypeCode As New StringElement("CommercialTypeCode", Me, False)
  Public ReadOnly _SIC As SICRelationship = SIC
  Public ReadOnly _CommercialStatus As CommercialStatusRelationship = CommercialStatus
  Public ReadOnly _CommercialType As CommercialTypeRelationship = CommercialType
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  Public ReadOnly _CommercialAddress_OwnMany As CommercialAddressRelationship = CommercialAddress_OwnMany
  Public ReadOnly _CommercialTelephone_OwnMany As CommercialTelephoneRelationship = CommercialTelephone_OwnMany
  Public ReadOnly _CommercialDirector_OwnMany As CommercialDirectorRelationship = CommercialDirector_OwnMany
  Public ReadOnly _CommercialAuditor_OwnMany As CommercialAuditorRelationship = CommercialAuditor_OwnMany
  Public ReadOnly _CommercialHistory_OwnMany As CommercialHistoryRelationship = CommercialHistory_OwnMany
  Public ReadOnly _CommercialNames_OwnMany As CommercialNameRelationship = CommercialNames_OwnMany
  Public ReadOnly _CommercialNameSoundEx_OwnMany As CommercialNameSoundExRelationship = CommercialNameSoundEx_OwnMany
  Public ReadOnly _CommercialConflict_OwnMany As CommercialConflictRelationship = CommercialConflict_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moFinancialYearEndOptions As IndicatorOptions
  Public Shared ReadOnly Property FinancialYearEndOptions() As IndicatorOptions
    Get
      If moFinancialYearEndOptions Is Nothing Then
        moFinancialYearEndOptions = New IndicatorOptions
        moFinancialYearEndOptions.Add(1, "January")
        moFinancialYearEndOptions.Add(2, "February")
        moFinancialYearEndOptions.Add(3, "March")
        moFinancialYearEndOptions.Add(4, "April")
        moFinancialYearEndOptions.Add(5, "May")
        moFinancialYearEndOptions.Add(6, "June")
        moFinancialYearEndOptions.Add(7, "July")
        moFinancialYearEndOptions.Add(8, "August")
        moFinancialYearEndOptions.Add(9, "September")
        moFinancialYearEndOptions.Add(10, "October")
        moFinancialYearEndOptions.Add(11, "November")
        moFinancialYearEndOptions.Add(12, "December")
      End If
      Return moFinancialYearEndOptions
    End Get
  End Property
  Private Shared moRecordStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property RecordStatusIndOptions() As IndicatorOptions
    Get
      If moRecordStatusIndOptions Is Nothing Then
        moRecordStatusIndOptions = New IndicatorOptions
        moRecordStatusIndOptions.Add("A", "Active")
        moRecordStatusIndOptions.Add("D", "Deleted")
      End If
      Return moRecordStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("Commercial", ApplicationSettings)
    _SIC._SICCode.LocalElement = _SICCode
    _CommercialStatus._CommercialStatusCode.LocalElement = _CommercialStatusCode
    _CommercialType._CommercialTypeCode.LocalElement = _CommercialTypeCode
    'sembleWare: Constructor End - Do Not Modify
    Me.DeleteType = CustomDBPart.enumDeleteType.ChangeRecordStatus
  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"

#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreate()
  End Sub

  Public Overrides Sub Load()
    MyBase.Load()
    BRLoad()
  End Sub

  Public Overrides Sub Save()
    MyBase.Save()
    BRSave()
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreate()
    Me.LastUpdatedDate.Value = System.DateTime.Now
    BRCommercialLabelDescription()
  End Sub

  Private Sub BRLoad()
    BRCommercialLabelDescription()
  End Sub

  Private Sub BRSave()
    BRCommercialLabelDescription()
  End Sub

  Public Sub BRCommercialLabelDescription()
    Dim sDesc As String = Trim(Me.CommercialName.Value)
    Me.CommercialLabelDescription.Value = "Commercial: " & Me.CommercialID.Value & " (" & sDesc & ")"
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class CommercialRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _CommercialID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New Commercial(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _CommercialID.ForeignElement = CType(Instance, Commercial).CommercialID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0Commercial")
    oList.Columns.Add(New ListColumn("CommercialID", "[A0Commercial].[CommercialID]", "CommercialID", DataType.Long, Nothing, True, 1, True, 100, "Commercial ID"))
    oList.Columns.Add(New ListColumn("RegistrationNo", "[A0Commercial].[RegistrationNo]", "RegistrationNo", DataType.String, Nothing, False, 0, True, 100, "Registration No"))
    oList.Columns.Add(New ListColumn("CommercialName", "[A0Commercial].[CommercialName]", "CommercialName", DataType.String, Nothing, False, 0, True, 200, "Commercial Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "[Commercial]  [A0Commercial]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function DropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0Commercial")
    oList.Columns.Add(New ListColumn("CommercialID", "[A0Commercial].[CommercialID]", "CommercialID", DataType.Long, Nothing, True, 1, True, 100, "Company ID"))
    oList.Columns.Add(New ListColumn("CommercialName", "[A0Commercial].[CommercialName]", "CommercialName", DataType.String, Nothing, False, 0, True, 200, "Company Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "[Commercial]  [A0Commercial]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0Commercial")
    oList.Columns.Add(New ListColumn("CommercialID", "[A0Commercial].[CommercialID]", "CommercialID", DataType.Long, Nothing, True, 1, False, 100, "Commercial ID"))
    oList.Columns.Add(New ListColumn("RegistrationNo", "[A0Commercial].[RegistrationNo]", "RegistrationNo", DataType.String, Nothing, False, 0, True, 100, "Registration No"))
    oList.Columns.Add(New ListColumn("CommercialName", "[A0Commercial].[CommercialName]", "CommercialName", DataType.String, Nothing, False, 0, True, 200, "Commercial Name"))
    oList.Columns.Add(New ListColumn("CommercialStatusCode", "[A1CommercialStatus].[CommercialStatusCode]", "CommercialStatusCode", DataType.String, Nothing, False, 0, False, 100, "Commercial Status Code"))
    oList.Columns.Add(New ListColumn("CommercialStatusDesc", "[A1CommercialStatus].[CommercialStatusDesc]", "CommercialStatusDesc", DataType.String, Nothing, False, 0, True, 200, "Commercial Status Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "([Commercial]  [A0Commercial]" + _
           "    left join [CommercialStatus]  [A1CommercialStatus] on [A0Commercial].[CommercialStatusCode] = [A1CommercialStatus].[CommercialStatusCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ConflictForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0Commercial")
    oList.Columns.Add(New ListColumn("CommercialID", "[A0Commercial].[CommercialID]", "CommercialID", DataType.Long, Nothing, True, 1, True, 100, "Commercial ID"))
    oList.Columns.Add(New ListColumn("RegistrationNo", "[A0Commercial].[RegistrationNo]", "RegistrationNo", DataType.String, Nothing, False, 0, True, 100, "Registration No"))
    oList.Columns.Add(New ListColumn("CommercialName", "[A0Commercial].[CommercialName]", "CommercialName", DataType.String, Nothing, False, 0, True, 200, "Commercial Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "[Commercial]  [A0Commercial]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ConflictGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0Commercial")
    oList.Columns.Add(New ListColumn("CommercialID", "[A0Commercial].[CommercialID]", "CommercialID", DataType.Long, Nothing, True, 1, True, 100, "Commercial ID"))
    oList.Columns.Add(New ListColumn("RegistrationNo", "[A0Commercial].[RegistrationNo]", "RegistrationNo", DataType.String, Nothing, False, 0, True, 100, "Registration No"))
    oList.Columns.Add(New ListColumn("CommercialName", "[A0Commercial].[CommercialName]", "CommercialName", DataType.String, Nothing, False, 0, True, 200, "Commercial Name"))
    oList.Columns.Add(New ListColumn("IsPossibleNameConflictYN", "[A0Commercial].[IsPossibleNameConflictYN]", "IsPossibleNameConflictYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Is Possible Name Conflict?"))
    oList.Columns.Add(New ListColumn("IsPossibleDuplicateRecordYN", "[A0Commercial].[IsPossibleDuplicateRecordYN]", "IsPossibleDuplicateRecordYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Is Possible Duplicate Record?"))
    oList.SelectStatement = "select"
    oList.FromClause = "[Commercial]  [A0Commercial]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
