Imports sembleWare.Runtime
Imports System
Public Class PrioritizationRating 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly PrioritizationRatingID As New IdentityElement("PrioritizationRatingID", Me, True, Nothing, Nothing)
  Public ReadOnly PrioritizationRatingDesc As New StringElement("PrioritizationRatingDesc", Me, False, True, True, True, True, 50, "Prioritization Rating Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly DurationTypeInd As New IndicatorElement("DurationTypeInd", Me, False, False, True, True, True, True, "Duration Type", Nothing, PrioritizationRating.DurationTypeIndOptions, "M")
  Public ReadOnly PrioritizationRatingLabelDescription As New StringElement("PrioritizationRatingLabelDescription", Me, False, True, True, False, False, 250, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly MetaDatabase As Relationship = New MetaDatabaseRelationship("MetaDatabase", "Database", RelationshipType.Include, True, True, True, Me)
  Public ReadOnly MetaField As Relationship = New MetaFieldRelationship("MetaField", "Field", RelationshipType.Include, True, True, True, Me)
  Public ReadOnly MetaTable As Relationship = New MetaTableRelationship("MetaTable", "Table", RelationshipType.Include, True, True, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly PrioritizationRatingCriteria_OwnMany As Relationship = New PrioritizationRatingCriteriaRelationship("PrioritizationRatingCriteria", Nothing, "PrioritizationRating", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _MetaDatabaseCode As New StringElement("MetaDatabaseCode", Me, False)
  Public ReadOnly _MetaFieldName As New StringElement("MetaFieldName", Me, False)
  Public ReadOnly _MetaTableName As New StringElement("MetaTableName", Me, False)
  Public ReadOnly _MetaDatabase As MetaDatabaseRelationship = MetaDatabase
  Public ReadOnly _MetaField As MetaFieldRelationship = MetaField
  Public ReadOnly _MetaTable As MetaTableRelationship = MetaTable
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  Public ReadOnly _PrioritizationRatingCriteria_OwnMany As PrioritizationRatingCriteriaRelationship = PrioritizationRatingCriteria_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moDurationTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property DurationTypeIndOptions() As IndicatorOptions
    Get
      If moDurationTypeIndOptions Is Nothing Then
        moDurationTypeIndOptions = New IndicatorOptions
        moDurationTypeIndOptions.Add("W", "Week")
        moDurationTypeIndOptions.Add("M", "Months")
      End If
      Return moDurationTypeIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("PrioritizationRating", ApplicationSettings)
    _MetaDatabase._MetaDatabaseCode.LocalElement = _MetaDatabaseCode
    _MetaField._MetaFieldName.LocalElement = _MetaFieldName
    _MetaField._MetaTableName.LocalElement = _MetaTableName
    _MetaField._MetaDatabaseCode.LocalElement = _MetaDatabaseCode
    _MetaTable._MetaTableName.LocalElement = _MetaTableName
    _MetaTable._MetaDatabaseCode.LocalElement = _MetaDatabaseCode
    'sembleWare: Constructor End - Do Not Modify
    AddHandler MetaTable.LimitRelatedList, AddressOf MetaTable_LimitRelatedList
    AddHandler MetaField.LimitRelatedList, AddressOf MetaField_LimitRelatedList
  End Sub

#End Region 'sembleWare: Constructor

#Region " LimitRelatedList Events"
  Private Sub MetaTable_LimitRelatedList(ByVal List As List)
    List.LimitByRelatedPart(Me.MetaDatabase.Instance, "MetaDatabase", True)
  End Sub

  Private Sub MetaField_LimitRelatedList(ByVal List As List)
    List.LimitByRelatedPart(Me.MetaTable.Instance, "MetaTable", True)
  End Sub
#End Region

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreate()
  End Sub

  Public Overrides Sub Load()
    MyBase.Load()
    BRLoad()
  End Sub

  Public Overrides Sub Save()
    With ApplicationSettings
      Try
        Dim bIsNewYN As Boolean = Me.IsNew

        .BeginTransaction()
        MyBase.Save()
        BRAfterSave(bIsNewYN)
        .CommitTransaction()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
    End With
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreate()
    BRPrioritizationRatingLabelDescription()
  End Sub

  Private Sub BRLoad()
    BRPrioritizationRatingLabelDescription()
  End Sub

  Private Sub BRAfterSave(ByVal IsNewYN As Boolean)
    BRPrioritizationRatingLabelDescription()
  End Sub

  Private Sub BRPrioritizationRatingLabelDescription()
    If Me.IsNew Then
      Me.PrioritizationRatingLabelDescription.Value = " Rating Prioritization: (New)"
    Else
      Me.PrioritizationRatingLabelDescription.Value = " Rating Prioritization: " & Me.PrioritizationRatingID.Value & " (" & Me.PrioritizationRatingDesc.Value & ")"
    End If
  End Sub
#End Region

#Region " Public Methods"
  Public Sub ExecuteRatingCalculation()
    Dim sQuery As String = GetRatingCalculationUpdateQuery()
    ApplicationSettings.ActionQuery(sQuery)

    sQuery = GetRatingCalculationInsertQuery()
    ApplicationSettings.ActionQuery(sQuery)
  End Sub
#End Region

#Region " Helper Functions"
  Private Function GetRatingCalculationInsertQuery() As String
    Dim oQuery As Text.StringBuilder = New Text.StringBuilder
    Dim oRatingCodeQuery As Text.StringBuilder = New Text.StringBuilder
    Dim oRatingDescQuery As Text.StringBuilder = New Text.StringBuilder

    GetRatingCalculationCriteriaQuery(True, oRatingCodeQuery, oRatingDescQuery)

    oQuery.Append("insert into ConsumerPrioritizationRating(ConsumerID, PrioritizationRatingID, RatingCode, RatingDesc, LastUpdatedDate, CreatedByUser, CreatedOnDate)" & vbNewLine)
    oQuery.Append("select ConsumerID, " & ApplicationSettings.QueryBuilder.ToSQL(Me.PrioritizationRatingID.Value) & ", " & vbNewLine)
    oQuery.Append("       " & oRatingCodeQuery.ToString() & ", " & vbNewLine)
    oQuery.Append("       " & oRatingDescQuery.ToString() & ", " & vbNewLine)
    oQuery.Append("       getdate(), " & ApplicationSettings.QueryBuilder.ToSQL(Constants.XDSAdministrationServicesUser) & ", getdate()" & vbNewLine)
    oQuery.Append("  from " & Me._MetaTableName.Value & vbNewLine)
    oQuery.Append(" where not exists (select PrioritizationRatingID" & vbNewLine)
    oQuery.Append("                     from ConsumerPrioritizationRating cpr" & vbNewLine)
    oQuery.Append("                    where cpr.ConsumerID = " & Me._MetaTableName.Value & ".ConsumerID" & vbNewLine)
    oQuery.Append("                      and cpr.PrioritizationRatingID = " & ApplicationSettings.QueryBuilder.ToSQL(Me.PrioritizationRatingID.Value) & ")" & vbNewLine)
    oQuery.Append(" group by ConsumerID")

    Return oQuery.ToString()
  End Function

  Private Function GetRatingCalculationUpdateQuery() As String
    Dim oQuery As Text.StringBuilder = New Text.StringBuilder
    Dim oRatingCodeQuery As Text.StringBuilder = New Text.StringBuilder
    Dim oRatingDescQuery As Text.StringBuilder = New Text.StringBuilder
    
    GetRatingCalculationCriteriaQuery(False, oRatingCodeQuery, oRatingDescQuery)

    oQuery.Append("update ConsumerPrioritizationRating" & vbNewLine)
    oQuery.Append("   set RatingCode = " & oRatingCodeQuery.ToString() & ", " & vbNewLine)
    oQuery.Append("       RatingDesc = " & oRatingDescQuery.ToString() & ", " & vbNewLine)
    oQuery.Append("       LastUpdatedDate = getdate()" & ", " & vbNewLine)
    oQuery.Append("       ChangedByUser = " & ApplicationSettings.QueryBuilder.ToSQL(Constants.XDSAdministrationServicesUser) & ", " & vbNewLine)
    oQuery.Append("       CreatedOnDate = getdate()" & vbNewLine)
    oQuery.Append("  from " & Me._MetaTableName.Value & vbNewLine)
    oQuery.Append(" where ConsumerPrioritizationRating.PrioritizationRatingID = " & ApplicationSettings.QueryBuilder.ToSQL(Me.PrioritizationRatingID.Value))
    oQuery.Append("   and " & Me._MetaTableName.Value & ".ConsumerID = ConsumerPrioritizationRating.ConsumerID" & vbNewLine)
    oQuery.Append("   and " & Me._MetaTableName.Value & "." & Me._MetaFieldName.Value & " = (select max(tn." & Me._MetaFieldName.Value & ")" & vbNewLine)
    oQuery.Append("                                                                            from " & Me._MetaTableName.Value & " tn" & vbNewLine)
    oQuery.Append("                                                                           where tn.ConsumerID = ConsumerPrioritizationRating.ConsumerID)" & vbNewLine)

    Return oQuery.ToString()
  End Function

  Private Sub GetRatingCalculationCriteriaQuery(ByVal IncludeMaxYN As Boolean, ByRef RatingCodeQuery As Text.StringBuilder, ByRef RatingDescQuery As Text.StringBuilder)
    Dim oRatingCodeQuery As Text.StringBuilder = New Text.StringBuilder
    Dim oRatingDescQuery As Text.StringBuilder = New Text.StringBuilder
    Dim oList As List = Me._PrioritizationRatingCriteria_OwnMany.GridList
    Dim oDataRow As DataRow
    Dim sMetaFieldName As String = Me._MetaFieldName.Value

    If IncludeMaxYN Then
      sMetaFieldName = "max(" & sMetaFieldName & ")"
    End If
    oRatingCodeQuery.Append("case" & vbNewLine)
    oRatingDescQuery.Append("case" & vbNewLine)
    For Each oDataRow In oList.DataTable.Rows
      Dim sString As String = ""

      Select Case oDataRow("OperatorTypeInd")
        Case Constants.PrioritizationRatingCriteria.OperatorTypes.Between
          sString = " between " & oDataRow("StartDuration") & " and " & oDataRow("EndDuration")
        Case Constants.PrioritizationRatingCriteria.OperatorTypes.EqualsTo
          sString = " = " & oDataRow("StartDuration")
        Case Constants.PrioritizationRatingCriteria.OperatorTypes.GreaterThan
          sString = " > " & oDataRow("StartDuration")
        Case Constants.PrioritizationRatingCriteria.OperatorTypes.LessThan
          sString = " < " & oDataRow("StartDuration")
      End Select
      oRatingCodeQuery.Append("  when datediff(" & Me.DurationTypeInd.Value & ", " & sMetaFieldName & ", getdate())" & sString & " then " & ApplicationSettings.QueryBuilder.ToSQL(oDataRow("RatingCode")) & vbNewLine)
      oRatingDescQuery.Append("  when datediff(" & Me.DurationTypeInd.Value & ", " & sMetaFieldName & ", getdate())" & sString & " then " & ApplicationSettings.QueryBuilder.ToSQL(oDataRow("RatingDesc")) & vbNewLine)
    Next
    oRatingCodeQuery.Append("  when " & sMetaFieldName & " is null then 'N/A'" & vbNewLine)
    oRatingDescQuery.Append("  when " & sMetaFieldName & " is null then 'Not Applicable'" & vbNewLine)
    oRatingCodeQuery.Append("end")
    oRatingDescQuery.Append("end")

    RatingCodeQuery = oRatingCodeQuery
    RatingDescQuery = oRatingDescQuery
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class PrioritizationRatingRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _PrioritizationRatingID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New PrioritizationRating(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _PrioritizationRatingID.ForeignElement = CType(Instance, PrioritizationRating).PrioritizationRatingID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0PrioritizationRating")
    oList.Columns.Add(New ListColumn("PrioritizationRatingID", "[A0PrioritizationRating].[PrioritizationRatingID]", "PrioritizationRatingID", DataType.Long, Nothing, True, 0, False, 100, "Prioritization Rating ID"))
    oList.Columns.Add(New ListColumn("PrioritizationRatingDesc", "[A0PrioritizationRating].[PrioritizationRatingDesc]", "PrioritizationRatingDesc", DataType.String, Nothing, False, 1, True, 200, "Prioritization Rating Description"))
    oList.Columns.Add(New ListColumn("MetaDatabaseCode", "[A2MetaDatabase].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, False, 0, False, 100, "Database Code"))
    oList.Columns.Add(New ListColumn("MetaDatabaseDesc", "[A2MetaDatabase].[MetaDatabaseDesc]", "MetaDatabaseDesc", DataType.String, Nothing, False, 0, True, 100, "Database Description"))
    oList.Columns.Add(New ListColumn("MetaFieldName", "[A1MetaField].[MetaFieldName]", "MetaFieldName", DataType.String, Nothing, False, 0, False, 100, "Field Name"))
    oList.Columns.Add(New ListColumn("MetaFieldDesc", "[A1MetaField].[MetaFieldDesc]", "MetaFieldDesc", DataType.String, Nothing, False, 0, True, 100, "Field Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "(([PrioritizationRating]  [A0PrioritizationRating]" + _
           "    join [MetaField]  [A1MetaField] on [A0PrioritizationRating].[MetaFieldName] = [A1MetaField].[MetaFieldName] and [A0PrioritizationRating].[MetaTableName] = [A1MetaField].[MetaTableName] and [A0PrioritizationRating].[MetaDatabaseCode] = [A1MetaField].[MetaDatabaseCode])" + _
           "    join [MetaDatabase]  [A2MetaDatabase] on [A0PrioritizationRating].[MetaDatabaseCode] = [A2MetaDatabase].[MetaDatabaseCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0PrioritizationRating")
    oList.Columns.Add(New ListColumn("PrioritizationRatingID", "[A0PrioritizationRating].[PrioritizationRatingID]", "PrioritizationRatingID", DataType.Long, Nothing, True, 0, False, 100, "Prioritization Rating ID"))
    oList.Columns.Add(New ListColumn("PrioritizationRatingDesc", "[A0PrioritizationRating].[PrioritizationRatingDesc]", "PrioritizationRatingDesc", DataType.String, Nothing, False, 1, True, 200, "Prioritization Rating Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "[PrioritizationRating]  [A0PrioritizationRating]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship