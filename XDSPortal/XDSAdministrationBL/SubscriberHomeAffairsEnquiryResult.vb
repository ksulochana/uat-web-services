Imports sembleWare.Runtime
Imports System
Public Class SubscriberHomeAffairsEnquiryResult 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly IDNo As New StringElement("IDNo", Me, False, False, True, True, False, 13, "ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Surname As New StringElement("Surname", Me, False, False, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FirstName As New StringElement("FirstName", Me, False, False, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SecondName As New StringElement("SecondName", Me, False, False, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly BirthDate As New DateTimeElement("BirthDate", Me, False, False, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly ThirdName As New StringElement("ThirdName", Me, False, False, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly DeceasedStatus As New StringElement("DeceasedStatus", Me, False, False, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly DeceasedDate As New DateTimeElement("DeceasedDate", Me, False, False, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly CauseOfDeath As New StringElement("CauseOfDeath", Me, False, False, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly DetailsViewedYN As New BooleanElement("DetailsViewedYN", Me, False, False, True, True, True, "Details Viewed?", Nothing, False, "Yes;No")
  Public ReadOnly DetailsViewedDate As New DateTimeElement("DetailsViewedDate", Me, False, False, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly HomeAffairsSelectedYN As New BooleanElement("HomeAffairsSelectedYN", Me, False, True, True, True, True, "Home Affairs Selected?", Nothing, False, "Yes;No")
  Public ReadOnly SubscriberHomeAffairsEnquiry As Relationship = New SubscriberHomeAffairsEnquiryRelationship("SubscriberHomeAffairsEnquiry", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly HomeAffairs As Relationship = New HomeAffairsRelationship("HomeAffairs", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _SubscriberHomeAffairsEnquiryID As New IntegerElement("SubscriberHomeAffairsEnquiryID", Me, True)
  Public ReadOnly _HomeAffairsID As New IntegerElement("HomeAffairsID", Me, True)
  Public ReadOnly _SubscriberHomeAffairsEnquiry As SubscriberHomeAffairsEnquiryRelationship = SubscriberHomeAffairsEnquiry
  Public ReadOnly _HomeAffairs As HomeAffairsRelationship = HomeAffairs
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberHomeAffairsEnquiryResult", ApplicationSettings)
    _SubscriberHomeAffairsEnquiry._SubscriberID.LocalElement = _SubscriberID
    _SubscriberHomeAffairsEnquiry._SubscriberHomeAffairsEnquiryID.LocalElement = _SubscriberHomeAffairsEnquiryID
    _HomeAffairs._HomeAffairsID.LocalElement = _HomeAffairsID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"

  Public Sub SelectRecord()
    With ApplicationSettings
      Try
        .BeginTransaction()
        Me.HomeAffairsSelectedYN.Value = True
        Me.DetailsViewedYN.Value = True
        Me.DetailsViewedDate.Value = System.DateTime.Now
        Me.Save()

        Dim oHomeAffairs As HomeAffairs = Me.HomeAffairs.Instance
        Dim oSubscriberHomeAffairsEnquiry As SubscriberHomeAffairsEnquiry = Me.SubscriberHomeAffairsEnquiry.Instance
        oSubscriberHomeAffairsEnquiry.EnquiryResultInd.Value = Constants.SubscriberHomeAffairsEnquiry.Results.RecordSelected

        oSubscriberHomeAffairsEnquiry.ResultIDNo.Value = oHomeAffairs.IDNo.Value
        oSubscriberHomeAffairsEnquiry.ResultFirstName.Value = oHomeAffairs.FirstName.Value
        oSubscriberHomeAffairsEnquiry.ResultSecondName.Value = oHomeAffairs.SecondName.Value
        oSubscriberHomeAffairsEnquiry.ResultSurname.Value = oHomeAffairs.Surname.Value
        oSubscriberHomeAffairsEnquiry.ResultBirthDate.ValueAsObject = oHomeAffairs.BirthDate.ValueAsObject

        oSubscriberHomeAffairsEnquiry.Save()
        .CommitTransaction()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
    End With
  End Sub

#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class SubscriberHomeAffairsEnquiryResultRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SubscriberHomeAffairsEnquiryID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _HomeAffairsID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberHomeAffairsEnquiryResult(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberID.ForeignElement = CType(Instance, SubscriberHomeAffairsEnquiryResult)._SubscriberID
    _SubscriberHomeAffairsEnquiryID.ForeignElement = CType(Instance, SubscriberHomeAffairsEnquiryResult)._SubscriberHomeAffairsEnquiryID
    _HomeAffairsID.ForeignElement = CType(Instance, SubscriberHomeAffairsEnquiryResult)._HomeAffairsID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberHomeAffairsEnquiryResult")
    oList.Columns.Add(New ListColumn("SubscriberHomeAffairsEnquiryID", "[A0SubscriberHomeAffairsEnquiryResult].[SubscriberHomeAffairsEnquiryID]", "SubscriberHomeAffairsEnquiryID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber Home Affairs Enquiry ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberHomeAffairsEnquiryResult].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("HomeAffairsID", "[A0SubscriberHomeAffairsEnquiryResult].[HomeAffairsID]", "HomeAffairsID", DataType.Integer, Nothing, True, 0, False, 100, "Home Affairs ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A0SubscriberHomeAffairsEnquiryResult].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("FirstName", "[A0SubscriberHomeAffairsEnquiryResult].[FirstName]", "FirstName", DataType.String, Nothing, False, 2, True, 150, "First Name"))
    oList.Columns.Add(New ListColumn("SecondName", "[A0SubscriberHomeAffairsEnquiryResult].[SecondName]", "SecondName", DataType.String, Nothing, False, 0, True, 150, "Second Name"))
    oList.Columns.Add(New ListColumn("ThirdName", "[A0SubscriberHomeAffairsEnquiryResult].[ThirdName]", "ThirdName", DataType.String, Nothing, False, 0, True, 150, "Third Name"))
    oList.Columns.Add(New ListColumn("Surname", "[A0SubscriberHomeAffairsEnquiryResult].[Surname]", "Surname", DataType.String, Nothing, False, 1, True, 150, "Surname"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A0SubscriberHomeAffairsEnquiryResult].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Birth Date"))
    oList.Columns.Add(New ListColumn("DetailsViewedYN", "[A0SubscriberHomeAffairsEnquiryResult].[DetailsViewedYN]", "DetailsViewedYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Details Viewed?"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberHomeAffairsEnquiryResult]  [A0SubscriberHomeAffairsEnquiryResult]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function MultipleMatchGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberHomeAffairsEnquiryResult")
    oList.Columns.Add(New ListColumn("SubscriberHomeAffairsEnquiryID", "[A0SubscriberHomeAffairsEnquiryResult].[SubscriberHomeAffairsEnquiryID]", "SubscriberHomeAffairsEnquiryID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber Home Affairs Enquiry ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberHomeAffairsEnquiryResult].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("HomeAffairsID", "[A0SubscriberHomeAffairsEnquiryResult].[HomeAffairsID]", "HomeAffairsID", DataType.Integer, Nothing, True, 0, False, 100, "Home Affairs ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A0SubscriberHomeAffairsEnquiryResult].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("FirstName", "[A0SubscriberHomeAffairsEnquiryResult].[FirstName]", "FirstName", DataType.String, Nothing, False, 2, True, 150, "First Name"))
    oList.Columns.Add(New ListColumn("SecondName", "[A0SubscriberHomeAffairsEnquiryResult].[SecondName]", "SecondName", DataType.String, Nothing, False, 0, True, 150, "Second Name"))
    oList.Columns.Add(New ListColumn("ThirdName", "[A0SubscriberHomeAffairsEnquiryResult].[ThirdName]", "ThirdName", DataType.String, Nothing, False, 0, True, 150, "Third Name"))
    oList.Columns.Add(New ListColumn("Surname", "[A0SubscriberHomeAffairsEnquiryResult].[Surname]", "Surname", DataType.String, Nothing, False, 1, True, 150, "Surname"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A0SubscriberHomeAffairsEnquiryResult].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Birth Date"))
    oList.Columns.Add(New ListColumn("DetailsViewedYN", "[A0SubscriberHomeAffairsEnquiryResult].[DetailsViewedYN]", "DetailsViewedYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Details Viewed?"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberHomeAffairsEnquiryResult]  [A0SubscriberHomeAffairsEnquiryResult]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
