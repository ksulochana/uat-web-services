Imports sembleWare.Runtime
Imports System
Public Class EnquiryMatchingEngineSAFPSSubject 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly EnquiryMatchingEngineSAFPSSubjectID As New IdentityElement("EnquiryMatchingEngineSAFPSSubjectID", Me, True, Nothing, Nothing)
  Public ReadOnly IDNo As New StringElement("IDNo", Me, False, True, True, True, False, 13, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PassportNo As New StringElement("PassportNo", Me, False, True, True, True, False, 16, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Surname As New StringElement("Surname", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FirstName As New StringElement("FirstName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SecondName As New StringElement("SecondName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly BirthDate As New DateTimeElement("BirthDate", Me, False, True, True, True, False, "Date of Birth", Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly GenderInd As New IndicatorElement("GenderInd", Me, False, False, True, True, True, False, "Gender", Nothing, EnquiryMatchingEngineSAFPSSubject.GenderIndOptions, Nothing)
  Public ReadOnly SAFPSSubjectID As New IntegerElement("SAFPSSubjectID", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly EnquiryMatchingEngine As Relationship = New EnquiryMatchingEngineRelationship("EnquiryMatchingEngine", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly EnquiryMatchingEngineSAFPSSubjectResult_OwnMany As Relationship = New EnquiryMatchingEngineSAFPSSubjectResultRelationship("EnquiryMatchingEngineSAFPSSubjectResult", Nothing, "EnquiryMatchingEngineSAFPSSubject", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _EnquiryMatchingEngineID As New IntegerElement("EnquiryMatchingEngineID", Me, True)
  Public ReadOnly _EnquiryMatchingEngine As EnquiryMatchingEngineRelationship = EnquiryMatchingEngine
  Public ReadOnly _EnquiryMatchingEngineSAFPSSubjectResult_OwnMany As EnquiryMatchingEngineSAFPSSubjectResultRelationship = EnquiryMatchingEngineSAFPSSubjectResult_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moGenderIndOptions As IndicatorOptions
  Public Shared ReadOnly Property GenderIndOptions() As IndicatorOptions
    Get
      If moGenderIndOptions Is Nothing Then
        moGenderIndOptions = New IndicatorOptions
        moGenderIndOptions.Add("M", "Male")
        moGenderIndOptions.Add("F", "Female")
      End If
      Return moGenderIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("EnquiryMatchingEngineSAFPSSubject", ApplicationSettings)
    _EnquiryMatchingEngine._EnquiryMatchingEngineID.LocalElement = _EnquiryMatchingEngineID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class EnquiryMatchingEngineSAFPSSubjectRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _EnquiryMatchingEngineSAFPSSubjectID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _EnquiryMatchingEngineID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New EnquiryMatchingEngineSAFPSSubject(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _EnquiryMatchingEngineSAFPSSubjectID.ForeignElement = CType(Instance, EnquiryMatchingEngineSAFPSSubject).EnquiryMatchingEngineSAFPSSubjectID
    _EnquiryMatchingEngineID.ForeignElement = CType(Instance, EnquiryMatchingEngineSAFPSSubject)._EnquiryMatchingEngineID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
