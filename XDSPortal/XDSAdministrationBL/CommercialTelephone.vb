Imports sembleWare.Runtime
Imports System
Public Class CommercialTelephone 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly CommercialTelephoneID As New IdentityElement("CommercialTelephoneID", Me, True, Nothing, Nothing)
  Public ReadOnly TelephoneTypeInd As New IndicatorElement("TelephoneTypeInd", Me, False, False, True, True, True, True, "Telephone Type", Nothing, CommercialTelephone.TelephoneTypeIndOptions, Nothing)
  Public ReadOnly TelephoneCode As New StringElement("TelephoneCode", Me, False, True, True, True, False, 5, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly TelephoneNo As New StringElement("TelephoneNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RecordStatusInd As New IndicatorElement("RecordStatusInd", Me, False, False, False, True, True, True, "Record Status", Nothing, CommercialTelephone.RecordStatusIndOptions, "A")
  Public ReadOnly DeletedReason As New StringElement("DeletedReason", Me, False, True, True, True, False, 500, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LastUpdatedDate As New DateTimeElement("LastUpdatedDate", Me, False, False, True, True, True, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly Loader As Relationship = New LoaderRelationship("Loader", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Commercial As Relationship = New CommercialRelationship("Commercial", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _LoaderID As New IntegerElement("LoaderID", Me, False)
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, False)
  Public ReadOnly _CommercialID As New IntegerElement("CommercialID", Me, True)
  Public ReadOnly _Loader As LoaderRelationship = Loader
  Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
  Public ReadOnly _Commercial As CommercialRelationship = Commercial
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moTelephoneTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property TelephoneTypeIndOptions() As IndicatorOptions
    Get
      If moTelephoneTypeIndOptions Is Nothing Then
        moTelephoneTypeIndOptions = New IndicatorOptions
        moTelephoneTypeIndOptions.Add("M", "Telephone (Main Line)")
        moTelephoneTypeIndOptions.Add("F", "Telephone (Fax)")
      End If
      Return moTelephoneTypeIndOptions
    End Get
  End Property
  Private Shared moRecordStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property RecordStatusIndOptions() As IndicatorOptions
    Get
      If moRecordStatusIndOptions Is Nothing Then
        moRecordStatusIndOptions = New IndicatorOptions
        moRecordStatusIndOptions.Add("A", "Active")
        moRecordStatusIndOptions.Add("D", "Deleted")
      End If
      Return moRecordStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("CommercialTelephone", ApplicationSettings)
    _Loader._LoaderID.LocalElement = _LoaderID
    _Subscriber._SubscriberID.LocalElement = _SubscriberID
    _Commercial._CommercialID.LocalElement = _CommercialID
    'sembleWare: Constructor End - Do Not Modify
    Me.DeleteType = CustomDBPart.enumDeleteType.ChangeRecordStatus
  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class CommercialTelephoneRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _CommercialTelephoneID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _CommercialID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New CommercialTelephone(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _CommercialTelephoneID.ForeignElement = CType(Instance, CommercialTelephone).CommercialTelephoneID
    _CommercialID.ForeignElement = CType(Instance, CommercialTelephone)._CommercialID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0CommercialTelephone")
    oList.Columns.Add(New ListColumn("CommercialTelephoneID", "[A0CommercialTelephone].[CommercialTelephoneID]", "CommercialTelephoneID", DataType.Long, Nothing, True, 0, False, 100, "Commercial Telephone ID"))
    oList.Columns.Add(New ListColumn("CommercialID", "[A0CommercialTelephone].[CommercialID]", "CommercialID", DataType.Integer, Nothing, True, 0, False, 100, "Commercial ID"))
    oList.Columns.Add(New ListColumn("TelephoneTypeInd", "[A0CommercialTelephone].[TelephoneTypeInd]", "TelephoneTypeInd", DataType.String, Nothing, False, 0, True, 100, "Telephone Type"))
    oList.Columns.Add(New ListColumn("TelephoneCode", "[A0CommercialTelephone].[TelephoneCode]", "TelephoneCode", DataType.String, Nothing, False, 0, True, 100, "Telephone Code"))
    oList.Columns.Add(New ListColumn("TelephoneNo", "[A0CommercialTelephone].[TelephoneNo]", "TelephoneNo", DataType.String, Nothing, False, 0, True, 100, "Telephone No"))
    oList.Columns.Add(New ListColumn("LastUpdatedDate", "[A0CommercialTelephone].[LastUpdatedDate]", "LastUpdatedDate", DataType.DateTime, "g", False, -1, True, 100, "Last Updated Date"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A1Subscriber].[SubscriberID]", "SubscriberID", DataType.Long, Nothing, False, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberName", "[A1Subscriber].[SubscriberName]", "SubscriberName", DataType.String, Nothing, False, 0, True, 200, "Subscriber Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "([CommercialTelephone]  [A0CommercialTelephone]" + _
           "    left join [Subscriber]  [A1Subscriber] on [A0CommercialTelephone].[SubscriberID] = [A1Subscriber].[SubscriberID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
