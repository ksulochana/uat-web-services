Imports sembleWare.Runtime
Imports System
Public Class LoaderProcessMetaDataProcedure 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly ValidRecordCount As New DecimalElement("ValidRecordCount", Me, False, False, True, True, True, Nothing, Nothing, "##0", Nothing, Nothing, Nothing)
  Public ReadOnly InvalidRecordCount As New DecimalElement("InvalidRecordCount", Me, False, False, True, True, True, Nothing, Nothing, "##0", Nothing, Nothing, Nothing)
  Public ReadOnly FileFormatField As Relationship = New FileFormatFieldRelationship("FileFormatField", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly DataProcedure As Relationship = New DataProcedureRelationship("DataProcedure", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly MetaDatabase As Relationship = New MetaDatabaseRelationship("MetaDatabase", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly LoaderProcessMetaDatabase As Relationship = New LoaderProcessMetaDatabaseRelationship("LoaderProcessMetaDatabase", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly LoaderProcess As Relationship = New LoaderProcessRelationship("LoaderProcess", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _FileFormatFieldID As New IntegerElement("FileFormatFieldID", Me, True)
  Public ReadOnly _FileFormatCode As New StringElement("FileFormatCode", Me, True)
  Public ReadOnly _DataProcedureID As New IntegerElement("DataProcedureID", Me, True)
  Public ReadOnly _MetaDatabaseCode As New StringElement("MetaDatabaseCode", Me, True)
  Public ReadOnly _LoaderID As New IntegerElement("LoaderID", Me, True)
  Public ReadOnly _LoaderProcessTypeCode As New StringElement("LoaderProcessTypeCode", Me, True)
  Public ReadOnly _FileFormatField As FileFormatFieldRelationship = FileFormatField
  Public ReadOnly _DataProcedure As DataProcedureRelationship = DataProcedure
  Public ReadOnly _MetaDatabase As MetaDatabaseRelationship = MetaDatabase
  Public ReadOnly _LoaderProcessMetaDatabase As LoaderProcessMetaDatabaseRelationship = LoaderProcessMetaDatabase
  Public ReadOnly _LoaderProcess As LoaderProcessRelationship = LoaderProcess
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("LoaderProcessMetaDataProcedure", ApplicationSettings)
    _FileFormatField._FileFormatFieldID.LocalElement = _FileFormatFieldID
    _FileFormatField._FileFormatCode.LocalElement = _FileFormatCode
    _DataProcedure._DataProcedureID.LocalElement = _DataProcedureID
    _MetaDatabase._MetaDatabaseCode.LocalElement = _MetaDatabaseCode
    _LoaderProcessMetaDatabase._LoaderID.LocalElement = _LoaderID
    _LoaderProcessMetaDatabase._LoaderProcessTypeCode.LocalElement = _LoaderProcessTypeCode
    _LoaderProcessMetaDatabase._MetaDatabaseCode.LocalElement = _MetaDatabaseCode
    _LoaderProcess._LoaderID.LocalElement = _LoaderID
    _LoaderProcess._LoaderProcessTypeCode.LocalElement = _LoaderProcessTypeCode
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: ConstructorV

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class LoaderProcessMetaDataProcedureRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _FileFormatFieldID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _FileFormatCode As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _DataProcedureID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _MetaDatabaseCode As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _LoaderID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _LoaderProcessTypeCode As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New LoaderProcessMetaDataProcedure(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _FileFormatFieldID.ForeignElement = CType(Instance, LoaderProcessMetaDataProcedure)._FileFormatFieldID
    _FileFormatCode.ForeignElement = CType(Instance, LoaderProcessMetaDataProcedure)._FileFormatCode
    _DataProcedureID.ForeignElement = CType(Instance, LoaderProcessMetaDataProcedure)._DataProcedureID
    _MetaDatabaseCode.ForeignElement = CType(Instance, LoaderProcessMetaDataProcedure)._MetaDatabaseCode
    _LoaderID.ForeignElement = CType(Instance, LoaderProcessMetaDataProcedure)._LoaderID
    _LoaderProcessTypeCode.ForeignElement = CType(Instance, LoaderProcessMetaDataProcedure)._LoaderProcessTypeCode
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code   


#Region " List Definitions"

  Public Function ValidationGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0LoaderProcessMetaDataProcedure")
    oList.Columns.Add(New ListColumn("MetaDatabaseCode", "[A0LoaderProcessMetaDataProcedure].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, True, 0, False, 100, "Meta Database Code"))
    oList.Columns.Add(New ListColumn("FileFormatCode", "[A0LoaderProcessMetaDataProcedure].[FileFormatCode]", "FileFormatCode", DataType.String, Nothing, True, 0, False, 100, "File Format Code"))
    oList.Columns.Add(New ListColumn("FileFormatFieldID", "[A0LoaderProcessMetaDataProcedure].[FileFormatFieldID]", "FileFormatFieldID", DataType.Integer, Nothing, True, 0, False, 100, "File Format Field ID"))
    oList.Columns.Add(New ListColumn("MetaDatabaseCode1", "[A3MetaDatabase].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, False, 0, True, 100, "Database Code"))
    oList.Columns.Add(New ListColumn("MetaDatabaseDesc", "[A3MetaDatabase].[MetaDatabaseDesc]", "MetaDatabaseDesc", DataType.String, Nothing, False, 0, True, 200, "Database Description"))
    oList.Columns.Add(New ListColumn("FileFormatFieldDesc", "[A1FileFormatField].[FileFormatFieldDesc]", "FileFormatFieldDesc", DataType.String, Nothing, False, 0, True, 200, "Field Description "))
    oList.Columns.Add(New ListColumn("SequenceNum", "[A1FileFormatField].[SequenceNum]", "SequenceNum", DataType.Integer, Nothing, False, 1, False, 100, "Sequence Number"))
    oList.Columns.Add(New ListColumn("DataProcedureID1", "[A2DataProcedure].[DataProcedureID]", "DataProcedureID", DataType.Long, Nothing, False, 0, False, 100, "Data Procedure ID"))
    oList.Columns.Add(New ListColumn("DataProcedureDesc", "[A2DataProcedure].[DataProcedureDesc]", "DataProcedureDesc", DataType.String, Nothing, False, 0, True, 200, "Data Procedure Description"))
    oList.Columns.Add(New ListColumn("DataProcedureTypeInd", "[A2DataProcedure].[DataProcedureTypeInd]", "DataProcedureTypeInd", DataType.String, Nothing, False, 0, False, 100, "Data Procedure Type"))
    oList.Columns.Add(New ListColumn("ValidRecordCount", "[A0LoaderProcessMetaDataProcedure].[ValidRecordCount]", "ValidRecordCount", DataType.Decimal, "##0", False, 0, True, 100, "Valid Record Count"))
    oList.Columns.Add(New ListColumn("InvalidRecordCount", "[A0LoaderProcessMetaDataProcedure].[InvalidRecordCount]", "InvalidRecordCount", DataType.Decimal, "##0", False, 0, True, 100, "Invalid Record Count"))
    oList.Columns.Add(New ListColumn("LoaderID", "[A0LoaderProcessMetaDataProcedure].[LoaderID]", "LoaderID", DataType.Integer, Nothing, True, 0, False, 100, "Loader ID"))
    oList.Columns.Add(New ListColumn("LoaderProcessTypeCode", "[A0LoaderProcessMetaDataProcedure].[LoaderProcessTypeCode]", "LoaderProcessTypeCode", DataType.String, Nothing, True, 0, False, 100, "Loader Process Type Code"))
    oList.Columns.Add(New ListColumn("DataProcedureID", "[A0LoaderProcessMetaDataProcedure].[DataProcedureID]", "DataProcedureID", DataType.Integer, Nothing, True, 0, False, 100, "Data Procedure ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "((([LoaderProcessMetaDataProcedure]  [A0LoaderProcessMetaDataProcedure]" + _
           "    join [FileFormatField]  [A1FileFormatField] on [A0LoaderProcessMetaDataProcedure].[FileFormatFieldID] = [A1FileFormatField].[FileFormatFieldID] and [A0LoaderProcessMetaDataProcedure].[FileFormatCode] = [A1FileFormatField].[FileFormatCode])" + _
           "    join [DataProcedure]  [A2DataProcedure] on [A0LoaderProcessMetaDataProcedure].[DataProcedureID] = [A2DataProcedure].[DataProcedureID])" + _
           "    join [MetaDatabase]  [A3MetaDatabase] on [A0LoaderProcessMetaDataProcedure].[MetaDatabaseCode] = [A3MetaDatabase].[MetaDatabaseCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function CalculationConversionGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0LoaderProcessMetaDataProcedure")
    oList.Columns.Add(New ListColumn("MetaDatabaseCode", "[A0LoaderProcessMetaDataProcedure].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, True, 0, False, 100, "Meta Database Code"))
    oList.Columns.Add(New ListColumn("FileFormatCode", "[A0LoaderProcessMetaDataProcedure].[FileFormatCode]", "FileFormatCode", DataType.String, Nothing, True, 0, False, 100, "File Format Code"))
    oList.Columns.Add(New ListColumn("FileFormatFieldID", "[A0LoaderProcessMetaDataProcedure].[FileFormatFieldID]", "FileFormatFieldID", DataType.Integer, Nothing, True, 0, False, 100, "File Format Field ID"))
    oList.Columns.Add(New ListColumn("MetaDatabaseCode1", "[A3MetaDatabase].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, False, 1, True, 100, "Database Name"))
    oList.Columns.Add(New ListColumn("MetaDatabaseDesc", "[A3MetaDatabase].[MetaDatabaseDesc]", "MetaDatabaseDesc", DataType.String, Nothing, False, 0, True, 200, "Database Description"))
    oList.Columns.Add(New ListColumn("FileFormatFieldDesc", "[A1FileFormatField].[FileFormatFieldDesc]", "FileFormatFieldDesc", DataType.String, Nothing, False, 0, True, 200, "Field Description "))
    oList.Columns.Add(New ListColumn("SequenceNum", "[A1FileFormatField].[FieldLength]", "FieldLength", DataType.Integer, Nothing, False, 2, False, 100, "Sequence Number"))
    oList.Columns.Add(New ListColumn("DataProcedureID1", "[A2DataProcedure].[DataProcedureID]", "DataProcedureID", DataType.Long, Nothing, False, 0, False, 100, "Data Procedure ID"))
    oList.Columns.Add(New ListColumn("DataProcedureDesc", "[A2DataProcedure].[DataProcedureDesc]", "DataProcedureDesc", DataType.String, Nothing, False, 0, True, 200, "Data Procedure Description"))
    oList.Columns.Add(New ListColumn("DataProcedureTypeInd", "[A2DataProcedure].[DataProcedureTypeInd]", "DataProcedureTypeInd", DataType.String, Nothing, False, 0, False, 100, "Data Procedure Type"))
    oList.Columns.Add(New ListColumn("ValidRecordCount", "[A0LoaderProcessMetaDataProcedure].[ValidRecordCount]", "ValidRecordCount", DataType.Decimal, "##0", False, 0, True, 100, "Affected Record Count"))
    oList.Columns.Add(New ListColumn("InvalidRecordCount", "[A0LoaderProcessMetaDataProcedure].[InvalidRecordCount]", "InvalidRecordCount", DataType.Decimal, "##0", False, 0, True, 100, "Unaffected Record Count"))
    oList.Columns.Add(New ListColumn("LoaderID", "[A0LoaderProcessMetaDataProcedure].[LoaderID]", "LoaderID", DataType.Integer, Nothing, True, 0, False, 100, "Loader ID"))
    oList.Columns.Add(New ListColumn("LoaderProcessTypeCode", "[A0LoaderProcessMetaDataProcedure].[LoaderProcessTypeCode]", "LoaderProcessTypeCode", DataType.String, Nothing, True, 0, False, 100, "Loader Process Type Code"))
    oList.Columns.Add(New ListColumn("DataProcedureID", "[A0LoaderProcessMetaDataProcedure].[DataProcedureID]", "DataProcedureID", DataType.Integer, Nothing, True, 0, False, 100, "Data Procedure ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "((([LoaderProcessMetaDataProcedure]  [A0LoaderProcessMetaDataProcedure]" + _
           "    join [FileFormatField]  [A1FileFormatField] on [A0LoaderProcessMetaDataProcedure].[FileFormatFieldID] = [A1FileFormatField].[FileFormatFieldID] and [A0LoaderProcessMetaDataProcedure].[FileFormatCode] = [A1FileFormatField].[FileFormatCode])" + _
           "    join [DataProcedure]  [A2DataProcedure] on [A0LoaderProcessMetaDataProcedure].[DataProcedureID] = [A2DataProcedure].[DataProcedureID])" + _
           "    join [MetaDatabase]  [A3MetaDatabase] on [A0LoaderProcessMetaDataProcedure].[MetaDatabaseCode] = [A3MetaDatabase].[MetaDatabaseCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function MatchingEvaluationGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0LoaderProcessMetaDataProcedure")
    oList.Columns.Add(New ListColumn("MetaDatabaseCode", "[A0LoaderProcessMetaDataProcedure].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, True, 0, False, 100, "Meta Database Code"))
    oList.Columns.Add(New ListColumn("FileFormatCode", "[A0LoaderProcessMetaDataProcedure].[FileFormatCode]", "FileFormatCode", DataType.String, Nothing, True, 0, False, 100, "File Format Code"))
    oList.Columns.Add(New ListColumn("FileFormatFieldID", "[A0LoaderProcessMetaDataProcedure].[FileFormatFieldID]", "FileFormatFieldID", DataType.Integer, Nothing, True, 0, False, 100, "File Format Field ID"))
    oList.Columns.Add(New ListColumn("MetaDatabaseCode1", "[A3MetaDatabase].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, False, 1, True, 100, "Database Name"))
    oList.Columns.Add(New ListColumn("MetaDatabaseDesc", "[A3MetaDatabase].[MetaDatabaseDesc]", "MetaDatabaseDesc", DataType.String, Nothing, False, 0, True, 200, "Database Description"))
    oList.Columns.Add(New ListColumn("FileFormatFieldDesc", "[A1FileFormatField].[FileFormatFieldDesc]", "FileFormatFieldDesc", DataType.String, Nothing, False, 0, True, 200, "Field Description "))
    oList.Columns.Add(New ListColumn("DataProcedureID1", "[A2DataProcedure].[DataProcedureID]", "DataProcedureID", DataType.Long, Nothing, False, 0, False, 100, "Data Procedure ID"))
    oList.Columns.Add(New ListColumn("DataProcedureDesc", "[A2DataProcedure].[DataProcedureDesc]", "DataProcedureDesc", DataType.String, Nothing, False, 0, True, 200, "Data Procedure Description"))
    oList.Columns.Add(New ListColumn("DataProcedureTypeInd", "[A2DataProcedure].[DataProcedureTypeInd]", "DataProcedureTypeInd", DataType.String, Nothing, False, 0, False, 100, "Data Procedure Type"))
    oList.Columns.Add(New ListColumn("ValidRecordCount", "[A0LoaderProcessMetaDataProcedure].[ValidRecordCount]", "ValidRecordCount", DataType.Decimal, "##0", False, 0, True, 100, "New Record Count"))
    oList.Columns.Add(New ListColumn("InvalidRecordCount", "[A0LoaderProcessMetaDataProcedure].[InvalidRecordCount]", "InvalidRecordCount", DataType.Decimal, "##0", False, 0, True, 100, "Existing Record Count"))
    oList.Columns.Add(New ListColumn("SequenceNum", "[A1FileFormatField].[FieldLength]", "FieldLength", DataType.Integer, Nothing, False, 2, False, 100, "Sequence Number"))
    oList.Columns.Add(New ListColumn("LoaderID", "[A0LoaderProcessMetaDataProcedure].[LoaderID]", "LoaderID", DataType.Integer, Nothing, True, 0, False, 100, "Loader ID"))
    oList.Columns.Add(New ListColumn("LoaderProcessTypeCode", "[A0LoaderProcessMetaDataProcedure].[LoaderProcessTypeCode]", "LoaderProcessTypeCode", DataType.String, Nothing, True, 0, False, 100, "Loader Process Type Code"))
    oList.Columns.Add(New ListColumn("DataProcedureID", "[A0LoaderProcessMetaDataProcedure].[DataProcedureID]", "DataProcedureID", DataType.Integer, Nothing, True, 0, False, 100, "Data Procedure ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "((([LoaderProcessMetaDataProcedure]  [A0LoaderProcessMetaDataProcedure]" + _
           "    join [FileFormatField]  [A1FileFormatField] on [A0LoaderProcessMetaDataProcedure].[FileFormatFieldID] = [A1FileFormatField].[FileFormatFieldID] and [A0LoaderProcessMetaDataProcedure].[FileFormatCode] = [A1FileFormatField].[FileFormatCode])" + _
           "    join [DataProcedure]  [A2DataProcedure] on [A0LoaderProcessMetaDataProcedure].[DataProcedureID] = [A2DataProcedure].[DataProcedureID])" + _
           "    join [MetaDatabase]  [A3MetaDatabase] on [A0LoaderProcessMetaDataProcedure].[MetaDatabaseCode] = [A3MetaDatabase].[MetaDatabaseCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function DataStatisticsGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0LoaderProcessMetaDataProcedure")
    oList.Columns.Add(New ListColumn("MetaDatabaseCode", "[A0LoaderProcessMetaDataProcedure].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, True, 0, False, 100, "Meta Database Code"))
    oList.Columns.Add(New ListColumn("FileFormatCode", "[A0LoaderProcessMetaDataProcedure].[FileFormatCode]", "FileFormatCode", DataType.String, Nothing, True, 0, False, 100, "File Format Code"))
    oList.Columns.Add(New ListColumn("FileFormatFieldID", "[A0LoaderProcessMetaDataProcedure].[FileFormatFieldID]", "FileFormatFieldID", DataType.Integer, Nothing, True, 0, False, 100, "File Format Field ID"))
    oList.Columns.Add(New ListColumn("MetaDatabaseCode1", "[A3MetaDatabase].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, False, 0, True, 100, "Database Code"))
    oList.Columns.Add(New ListColumn("MetaDatabaseDesc", "[A3MetaDatabase].[MetaDatabaseDesc]", "MetaDatabaseDesc", DataType.String, Nothing, False, 0, True, 200, "Database Description"))
    oList.Columns.Add(New ListColumn("FileFormatFieldDesc", "[A1FileFormatField].[FileFormatFieldDesc]", "FileFormatFieldDesc", DataType.String, Nothing, False, 0, True, 200, "Field Description "))
    oList.Columns.Add(New ListColumn("SequenceNum1", "[A1FileFormatField].[FieldLength]", "FieldLength", DataType.Integer, Nothing, False, 1, False, 100, "Fixed Field Start"))
    oList.Columns.Add(New ListColumn("DataProcedureDesc", "[A2DataProcedure].[DataProcedureDesc]", "DataProcedureDesc", DataType.String, Nothing, False, 0, True, 200, "Data Procedure Description"))
    oList.Columns.Add(New ListColumn("DataProcedureTypeInd", "[A2DataProcedure].[DataProcedureTypeInd]", "DataProcedureTypeInd", DataType.String, Nothing, False, 0, False, 100, "Data Procedure Type"))
    oList.Columns.Add(New ListColumn("ValidRecordCount", "[A0LoaderProcessMetaDataProcedure].[ValidRecordCount]", "ValidRecordCount", DataType.Decimal, "##0", False, 0, True, 100, "Total Sum / Count"))
    oList.Columns.Add(New ListColumn("LoaderID", "[A0LoaderProcessMetaDataProcedure].[LoaderID]", "LoaderID", DataType.Integer, Nothing, True, 0, False, 100, "Loader ID"))
    oList.Columns.Add(New ListColumn("LoaderProcessTypeCode", "[A0LoaderProcessMetaDataProcedure].[LoaderProcessTypeCode]", "LoaderProcessTypeCode", DataType.String, Nothing, True, 0, False, 100, "Loader Process Type Code"))
    oList.Columns.Add(New ListColumn("DataProcedureID", "[A0LoaderProcessMetaDataProcedure].[DataProcedureID]", "DataProcedureID", DataType.Integer, Nothing, True, 0, False, 100, "Data Procedure ID"))
    oList.Columns.Add(New ListColumn("SequenceNum", "[A1FileFormatField].[SequenceNum]", "SequenceNum", DataType.Integer, Nothing, False, 0, False, 100, "Sequence Number"))
    oList.Columns.Add(New ListColumn("FileFormatFieldTypeInd", "[A1FileFormatField].[FileFormatFieldTypeInd]", "FileFormatFieldTypeInd", DataType.String, Nothing, False, 0, False, 100, "File Format Field Type"))
    oList.Columns.Add(New ListColumn("MetaDatabaseTypeInd", "[A3MetaDatabase].[MetaDatabaseTypeInd]", "MetaDatabaseTypeInd", DataType.String, Nothing, False, 0, False, 100, "Database Type"))
    oList.SelectStatement = "select"
    oList.FromClause = "((([LoaderProcessMetaDataProcedure]  [A0LoaderProcessMetaDataProcedure]" + _
           "    join [FileFormatField]  [A1FileFormatField] on [A0LoaderProcessMetaDataProcedure].[FileFormatFieldID] = [A1FileFormatField].[FileFormatFieldID] and [A0LoaderProcessMetaDataProcedure].[FileFormatCode] = [A1FileFormatField].[FileFormatCode])" + _
           "    join [DataProcedure]  [A2DataProcedure] on [A0LoaderProcessMetaDataProcedure].[DataProcedureID] = [A2DataProcedure].[DataProcedureID])" + _
           "    join [MetaDatabase]  [A3MetaDatabase] on [A0LoaderProcessMetaDataProcedure].[MetaDatabaseCode] = [A3MetaDatabase].[MetaDatabaseCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
