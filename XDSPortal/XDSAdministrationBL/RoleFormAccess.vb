Imports sembleWare.Runtime
Imports System
Public Class RoleFormAccess 'sembleWare: Part
  Inherits CustomDBPart

#Region "Indicator Options"
  Public Const Access_None As Integer = 0
  Public Const Access_CanView As Integer = 1
  Public Const Access_CanChange As Integer = 2
#End Region

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly AccessInd As New IndicatorElement("AccessInd", Me, False, True, True, True, True, False, "Access", Nothing, RoleFormAccess.AccessIndOptions, Nothing)
  Public ReadOnly RoleFormAccessLabelDescription As New StringElement("RoleFormAccessLabelDescription", Me, False, True, True, False, False, 250, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Role As Relationship = New RoleRelationship("Role", Nothing, RelationshipType.OwnedBy, True, False, True, Me)
  Public ReadOnly SecurityForm As Relationship = New SecurityFormRelationship("SecurityForm", Nothing, RelationshipType.OwnedBy, True, False, True, Me)
  Public ReadOnly RoleControlAccess_OwnMany As Relationship = New RoleControlAccessRelationship("RoleControlAccess", Nothing, "RoleFormAccess", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _FormName As New StringElement("FormName", Me, True)
  Public ReadOnly _AssemblyName As New StringElement("AssemblyName", Me, True)
  Public ReadOnly _RoleCode As New StringElement("RoleCode", Me, True)
  Public ReadOnly _Role As RoleRelationship = Role
  Public ReadOnly _SecurityForm As SecurityFormRelationship = SecurityForm
  Public ReadOnly _RoleControlAccess_OwnMany As RoleControlAccessRelationship = RoleControlAccess_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moAccessIndOptions As IndicatorOptions
  Public Shared ReadOnly Property AccessIndOptions() As IndicatorOptions
    Get
      If moAccessIndOptions Is Nothing Then
        moAccessIndOptions = New IndicatorOptions
        moAccessIndOptions.Add(0, "None")
        moAccessIndOptions.Add(1, "Can View")
        moAccessIndOptions.Add(2, "Can Change")
      End If
      Return moAccessIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("RoleFormAccess", ApplicationSettings)
    _Role._RoleCode.LocalElement = _RoleCode
    _SecurityForm._FormName.LocalElement = _FormName
    _SecurityForm._AssemblyName.LocalElement = _AssemblyName
    'sembleWare: Constructor End - Do Not Modify
    Me.ForceUpperCaseKeys = False
  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreate()
  End Sub

  Public Overrides Sub Load()
    MyBase.Load()
    BRLoad()
  End Sub

  Public Overrides Sub Save()
    MyBase.Save()
    BRSave()
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreate()
    BRRoleFormAccessLabelDescription()
  End Sub

  Private Sub BRLoad()
    BRRoleFormAccessLabelDescription()
  End Sub

  Private Sub BRSave()
    BRRoleFormAccessLabelDescription()
  End Sub

  Public Sub BRRoleFormAccessLabelDescription()
    Dim oRole As Role = Me.Role.Instance
    Dim oSecurityForm As SecurityForm = Me.SecurityForm.Instance

    If Me.IsNew Then
      Me.RoleFormAccessLabelDescription.Value = oRole.RoleLabelDescription.Value & " - Security Form: (New)"
    Else
      Dim sDesc As String = Trim(oSecurityForm.FormCaption.Value)
      Me.RoleFormAccessLabelDescription.Value = oRole.RoleLabelDescription.Value & " - Security Form: " & oSecurityForm.FormName.Value & " (" & sDesc & ")"
    End If
  End Sub
#End Region

#Region " Shared Procedures"
  Public Shared Function UpdateRoleFormControlAccess(ByVal AssemblyName As String, ByVal ApplicationSettings As ApplicationSettings) As Boolean
    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand

    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spRoleFormAccess_U_Access"

    oCommand.Parameters.Add("@AssemblyName", SqlDbType.VarChar, 100)

    oCommand.Parameters("@AssemblyName").Value = AssemblyName

    ApplicationSettings.ActionQuery(oCommand)
  End Function
#End Region
End Class 'sembleWare: Part

Public Class RoleFormAccessRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _FormName As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _AssemblyName As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _RoleCode As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New RoleFormAccess(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _FormName.ForeignElement = CType(Instance, RoleFormAccess)._FormName
    _AssemblyName.ForeignElement = CType(Instance, RoleFormAccess)._AssemblyName
    _RoleCode.ForeignElement = CType(Instance, RoleFormAccess)._RoleCode
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function CurrentUserAccessList() As sembleWare.Runtime.List
    Dim nSystemUserID = GlobalSettings.SystemUser.LoggedInSystemUserID
    'Custom List
    Dim oList As DBList = New DBList(Me, "A0RoleFormAccess")
    oList.Columns.Add(New ListColumn("FormName", "[A0RoleFormAccess].[FormName]", "FormName", DataType.String, Nothing, True, 0, False, 100, "Form Name"))
    oList.Columns.Add(New ListColumn("AccessInd", "Max([A0RoleFormAccess].[AccessInd])", "AccessInd", DataType.Integer, Nothing, False, 0, True, 100, "Access"))
    oList.SelectStatement = "select"
    oList.FromClause = "[RoleFormAccess]  [A0RoleFormAccess]" + _
        "   join [SystemUserRole] [A1SystemUserRole] on [A0RoleFormAccess].[RoleCode] = [A1SystemUserRole].[RoleCode] and [A1SystemUserRole].[SystemUserID] = " + ApplicationSettings.QueryBuilder.ToSQL(nSystemUserID)
    oList.WhereClause = ""
    oList.GroupByClause = "[A0RoleFormAccess].[FormName]"
    Return oList
  End Function

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0RoleFormAccess")
    oList.Columns.Add(New ListColumn("FormName", "[A0RoleFormAccess].[FormName]", "FormName", DataType.String, Nothing, True, 0, False, 100, "Form Name"))
    oList.Columns.Add(New ListColumn("ModuleCode", "[A2SecurityModule].[ModuleCode]", "ModuleCode", DataType.String, Nothing, False, 1, True, 100, "Module Code"))
    oList.Columns.Add(New ListColumn("ModuleDesc", "[A2SecurityModule].[ModuleDesc]", "ModuleDesc", DataType.String, Nothing, False, 0, True, 200, "Module Description"))
    oList.Columns.Add(New ListColumn("FormName1", "[A1SecurityForm].[FormName]", "FormName", DataType.String, Nothing, False, 2, True, 200, "Form Name"))
    oList.Columns.Add(New ListColumn("FormCaption", "[A1SecurityForm].[FormCaption]", "FormCaption", DataType.String, Nothing, False, 0, True, 200, "Form Caption"))
    oList.Columns.Add(New ListColumn("AccessInd", "[A0RoleFormAccess].[AccessInd]", "AccessInd", DataType.Integer, Nothing, False, 0, True, 75, "Access"))
    oList.Columns.Add(New ListColumn("AssemblyName", "[A0RoleFormAccess].[AssemblyName]", "AssemblyName", DataType.String, Nothing, True, 0, False, 100, "Assembly Name"))
    oList.Columns.Add(New ListColumn("RoleCode", "[A0RoleFormAccess].[RoleCode]", "RoleCode", DataType.String, Nothing, True, 0, False, 100, "Role Code"))
    oList.SelectStatement = "select"
    oList.FromClause = "([RoleFormAccess]  [A0RoleFormAccess]" + _
           "    join ([SecurityForm]  [A1SecurityForm]" + _
           "    left join [SecurityModule]  [A2SecurityModule] on [A1SecurityForm].[ModuleCode] = [A2SecurityModule].[ModuleCode]) on [A0RoleFormAccess].[FormName] = [A1SecurityForm].[FormName] and [A0RoleFormAccess].[AssemblyName] = [A1SecurityForm].[AssemblyName])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
