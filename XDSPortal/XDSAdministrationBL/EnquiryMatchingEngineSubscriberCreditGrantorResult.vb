Imports sembleWare.Runtime
Imports System
Public Class EnquiryMatchingEngineSubscriberCreditGrantorResult 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly IgnoreRecordYN As New BooleanElement("IgnoreRecordYN", Me, False, True, True, True, True, "Ignore Record?", Nothing, False, "Yes;No")
  Public ReadOnly EnquiryMatchingEngineSubscriberCreditGrantor As Relationship = New EnquiryMatchingEngineSubscriberCreditGrantorRelationship("EnquiryMatchingEngineSubscriberCreditGrantor", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly SubscriberCreditGrantor As Relationship = New SubscriberCreditGrantorRelationship("SubscriberCreditGrantor", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _EnquiryMatchingEngineID As New IntegerElement("EnquiryMatchingEngineID", Me, True)
  Public ReadOnly _EnquiryMatchingEngineSubscriberCreditGrantorID As New IntegerElement("EnquiryMatchingEngineSubscriberCreditGrantorID", Me, True)
  Public ReadOnly _SubscriberCreditGrantorID As New IntegerElement("SubscriberCreditGrantorID", Me, True)
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _EnquiryMatchingEngineSubscriberCreditGrantor As EnquiryMatchingEngineSubscriberCreditGrantorRelationship = EnquiryMatchingEngineSubscriberCreditGrantor
  Public ReadOnly _SubscriberCreditGrantor As SubscriberCreditGrantorRelationship = SubscriberCreditGrantor
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("EnquiryMatchingEngineSubscriberCreditGrantorResult", ApplicationSettings)
    _EnquiryMatchingEngineSubscriberCreditGrantor._EnquiryMatchingEngineID.LocalElement = _EnquiryMatchingEngineID
    _EnquiryMatchingEngineSubscriberCreditGrantor._EnquiryMatchingEngineSubscriberCreditGrantorID.LocalElement = _EnquiryMatchingEngineSubscriberCreditGrantorID
    _SubscriberCreditGrantor._SubscriberCreditGrantorID.LocalElement = _SubscriberCreditGrantorID
    _SubscriberCreditGrantor._SubscriberID.LocalElement = _SubscriberID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class EnquiryMatchingEngineSubscriberCreditGrantorResultRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _EnquiryMatchingEngineID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _EnquiryMatchingEngineSubscriberCreditGrantorID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SubscriberCreditGrantorID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New EnquiryMatchingEngineSubscriberCreditGrantorResult(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _EnquiryMatchingEngineID.ForeignElement = CType(Instance, EnquiryMatchingEngineSubscriberCreditGrantorResult)._EnquiryMatchingEngineID
    _EnquiryMatchingEngineSubscriberCreditGrantorID.ForeignElement = CType(Instance, EnquiryMatchingEngineSubscriberCreditGrantorResult)._EnquiryMatchingEngineSubscriberCreditGrantorID
    _SubscriberCreditGrantorID.ForeignElement = CType(Instance, EnquiryMatchingEngineSubscriberCreditGrantorResult)._SubscriberCreditGrantorID
    _SubscriberID.ForeignElement = CType(Instance, EnquiryMatchingEngineSubscriberCreditGrantorResult)._SubscriberID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
