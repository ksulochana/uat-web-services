Imports sembleWare.Runtime
Imports System
Public Class EnquiryMatchingEngineAuditor 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly EnquiryMatchingEngineAuditorID As New IdentityElement("EnquiryMatchingEngineAuditorID", Me, True, Nothing, Nothing)
  Public ReadOnly ProfessionNo As New StringElement("ProfessionNo", Me, False, True, True, True, False, 10, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AuditorName As New StringElement("AuditorName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AuditorID As New IntegerElement("AuditorID", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly EnquiryMatchingEngine As Relationship = New EnquiryMatchingEngineRelationship("EnquiryMatchingEngine", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly EnquiryMatchingEngineAuditorResult_OwnMany As Relationship = New EnquiryMatchingEngineAuditorResultRelationship("EnquiryMatchingEngineAuditorResult", Nothing, "EnquiryMatchingEngineAuditor", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _EnquiryMatchingEngineID As New IntegerElement("EnquiryMatchingEngineID", Me, True)
  Public ReadOnly _EnquiryMatchingEngine As EnquiryMatchingEngineRelationship = EnquiryMatchingEngine
  Public ReadOnly _EnquiryMatchingEngineAuditorResult_OwnMany As EnquiryMatchingEngineAuditorResultRelationship = EnquiryMatchingEngineAuditorResult_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("EnquiryMatchingEngineAuditor", ApplicationSettings)
    _EnquiryMatchingEngine._EnquiryMatchingEngineID.LocalElement = _EnquiryMatchingEngineID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class EnquiryMatchingEngineAuditorRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _EnquiryMatchingEngineID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _EnquiryMatchingEngineAuditorID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New EnquiryMatchingEngineAuditor(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _EnquiryMatchingEngineID.ForeignElement = CType(Instance, EnquiryMatchingEngineAuditor)._EnquiryMatchingEngineID
    _EnquiryMatchingEngineAuditorID.ForeignElement = CType(Instance, EnquiryMatchingEngineAuditor).EnquiryMatchingEngineAuditorID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
