Imports sembleWare.Runtime
Imports System
Public Class SAFPSSubjectConflict 'sembleWare: Part
    Inherits CustomDBPart

#Region " Elements & Relationships"

    'sembleWare: Elements Start - Do Not Modify
    Public ReadOnly SAFPSSubject As Relationship = New SAFPSSubjectRelationship("SAFPSSubject", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
    Public ReadOnly ConflictSAFPSSubject As Relationship = New SAFPSSubjectRelationship("ConflictSAFPSSubject", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
    'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

    'sembleWare: Foreign Items Start - Do Not Modify
    Public ReadOnly _SAFPSSubjectID As New IntegerElement("SAFPSSubjectID", Me, True)
    Public ReadOnly _ConflictSAFPSSubjectID As New IntegerElement("ConflictSAFPSSubjectID", Me, True)
    Public ReadOnly _SAFPSSubject As SAFPSSubjectRelationship = SAFPSSubject
    Public ReadOnly _ConflictSAFPSSubject As SAFPSSubjectRelationship = ConflictSAFPSSubject
    'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

    'sembleWare: Indicator Options Start - Do Not Modify
    'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

    Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

        'sembleWare: Constructor Start - Do Not Modify
        MyBase.New("SAFPSSubjectConflict", ApplicationSettings)
        _SAFPSSubject._SAFPSSubjectID.LocalElement = _SAFPSSubjectID
        _ConflictSAFPSSubject._SAFPSSubjectID.LocalElement = _ConflictSAFPSSubjectID
        'sembleWare: Constructor End - Do Not Modify
    End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"

    Public Sub Merge()
        SAFPSSubjectConflict.SAFPSSubjectMerge(Me._SAFPSSubjectID.Value, Me._ConflictSAFPSSubjectID.Value, GlobalSettings.SystemUser.LoggedInSystemUserID, ApplicationSettings)
    End Sub

    Public Sub MergeAll()
        SAFPSSubjectConflict.SAFPSSubjectMergeAll(Me._SAFPSSubjectID.Value, GlobalSettings.SystemUser.LoggedInSystemUserID, ApplicationSettings)
    End Sub

#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Shared Procedures"
    Public Shared Sub SAFPSSubjectMerge(ByVal SAFPSSubjectID As Integer, ByVal MergeToSAFPSSubjectID As Integer, ByVal ActionedBySystemUserID As Integer, ByVal ApplicationSettings As ApplicationSettings)
        Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand

        oCommand.CommandType = CommandType.StoredProcedure
        oCommand.CommandText = "spSAFPSSubject_U_Merge"

        oCommand.Parameters.Add("@SAFPSSubjectID", SqlDbType.Int)
        oCommand.Parameters.Add("@MergeToSAFPSSubjectID", SqlDbType.Int)
        oCommand.Parameters.Add("@ActionedBySystemUserID", SqlDbType.Int)

        oCommand.Parameters("@SAFPSSubjectID").Value = SAFPSSubjectID
        oCommand.Parameters("@MergeToSAFPSSubjectID").Value = MergeToSAFPSSubjectID
        oCommand.Parameters("@ActionedBySystemUserID").Value = ActionedBySystemUserID

        ApplicationSettings.ActionQuery(oCommand)
    End Sub

    Public Shared Sub SAFPSSubjectMergeAll(ByVal SAFPSSubjectID As Integer, ByVal ActionedBySystemUserID As Integer, ByVal ApplicationSettings As ApplicationSettings)
        Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand

        oCommand.CommandType = CommandType.StoredProcedure
        oCommand.CommandText = "spSAFPSSubject_U_MergeAll"

        oCommand.Parameters.Add("@SAFPSSubjectID", SqlDbType.Int)
        oCommand.Parameters.Add("@ActionedBySystemUserID", SqlDbType.Int)

        oCommand.Parameters("@SAFPSSubjectID").Value = SAFPSSubjectID
        oCommand.Parameters("@ActionedBySystemUserID").Value = ActionedBySystemUserID

        ApplicationSettings.ActionQuery(oCommand)

    End Sub
#End Region

End Class 'sembleWare: Part

Public Class SAFPSSubjectConflictRelationship 'sembleWare: Relationship
    Inherits Relationship

#Region " Relationship Code"

    'sembleWare: Relationship Start - Do Not Modify
    Public ReadOnly _SAFPSSubjectID As ForeignKey = New ForeignKey(Me)
    Public ReadOnly _ConflictSAFPSSubjectID As ForeignKey = New ForeignKey(Me)

    Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
        MyBase.New(ApplicationSettings)
    End Sub

    Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
        MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
    End Sub

    Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
        MyBase.New(Name, Caption, OwnedByName, Owner)
    End Sub

    Public Overrides Function CreateObject() As sembleWare.Runtime.Part
        Return New SAFPSSubjectConflict(ApplicationSettings)
    End Function

    Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
        _SAFPSSubjectID.ForeignElement = CType(Instance, SAFPSSubjectConflict)._SAFPSSubjectID
        _ConflictSAFPSSubjectID.ForeignElement = CType(Instance, SAFPSSubjectConflict)._ConflictSAFPSSubjectID
    End Sub

    'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

    Public Function GridList() As sembleWare.Runtime.List
        'sembleWare: List Declaration Start - Do Not Modify
        Dim oList As DBList = New DBList(Me, "A0SAFPSSubjectConflict")
        oList.Columns.Add(New ListColumn("SAFPSSubjectID", "[A0SAFPSSubjectConflict].[SAFPSSubjectID]", "SAFPSSubjectID", DataType.Integer, Nothing, True, 0, False, 100, "SAFPSSubject ID"))
        oList.Columns.Add(New ListColumn("ConflictSAFPSSubjectID", "[A0SAFPSSubjectConflict].[ConflictSAFPSSubjectID]", "ConflictSAFPSSubjectID", DataType.Integer, Nothing, True, 0, False, 100, "Conflict SAFPSSubject ID"))
        oList.Columns.Add(New ListColumn("SAFPSSubjectID1", "[A1ConflictSAFPSSubject].[SAFPSSubjectID]", "SAFPSSubjectID", DataType.Long, Nothing, False, 1, True, 100, "SAFPS Subject ID"))
        oList.Columns.Add(New ListColumn("IDNo", "[A1ConflictSAFPSSubject].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
        oList.Columns.Add(New ListColumn("PassportNo", "[A1ConflictSAFPSSubject].[PassportNo]", "PassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No"))
        oList.Columns.Add(New ListColumn("Surname", "[A1ConflictSAFPSSubject].[Surname]", "Surname", DataType.String, Nothing, False, 0, True, 100, "Surname"))
        oList.Columns.Add(New ListColumn("FirstName", "[A1ConflictSAFPSSubject].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
        oList.Columns.Add(New ListColumn("SecondName", "[A1ConflictSAFPSSubject].[SecondName]", "SecondName", DataType.String, Nothing, False, 0, True, 100, "Second Name"))
        oList.Columns.Add(New ListColumn("BirthDate", "[A1ConflictSAFPSSubject].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Birth Date"))
        oList.Columns.Add(New ListColumn("GenderInd", "[A1ConflictSAFPSSubject].[GenderInd]", "GenderInd", DataType.String, Nothing, False, 0, True, 100, "Gender"))
        oList.SelectStatement = "select"
        oList.FromClause = "([SAFPSSubjectConflict]  [A0SAFPSSubjectConflict]" + _
               "    join [SAFPSSubject]  [A1ConflictSAFPSSubject] on [A0SAFPSSubjectConflict].[ConflictSAFPSSubjectID] = [A1ConflictSAFPSSubject].[SAFPSSubjectID])"
        oList.WhereClause = ""
        oList.ApplyRelationshipLimits()
        'sembleWare: List Declaration End - Do Not Modify
        oList.AndFilters.Add(New ListFilter("RecordStatusInd", "[A1ConflictSAFPSSubject].[RecordStatusInd]", Constants.RecordStatus.Active, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
        Return oList
    End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
