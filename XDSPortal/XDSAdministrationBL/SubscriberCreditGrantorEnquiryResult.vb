Imports sembleWare.Runtime
Imports System
Public Class SubscriberCreditGrantorEnquiryResult 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly IDNo As New StringElement("IDNo", Me, False, False, True, True, False, 13, "ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PassportNo As New StringElement("PassportNo", Me, False, False, True, True, False, 16, "Passport No / Other ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ReferenceNo As New StringElement("ReferenceNo", Me, False, False, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly EnquiryDate As New DateTimeElement("EnquiryDate", Me, False, False, True, True, False, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly DetailsViewedYN As New BooleanElement("DetailsViewedYN", Me, False, False, True, True, True, Nothing, Nothing, False, "Yes;No")
  Public ReadOnly DetailsViewedDate As New DateTimeElement("DetailsViewedDate", Me, False, False, True, True, False, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly CreditGrantorSelectedYN As New BooleanElement("CreditGrantorSelectedYN", Me, False, False, True, True, True, Nothing, Nothing, False, "Yes;No")
  Public ReadOnly SubscriberCreditGrantorEnquiry As Relationship = New SubscriberCreditGrantorEnquiryRelationship("SubscriberCreditGrantorEnquiry", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly SubscriberCreditGrantor As Relationship = New SubscriberCreditGrantorRelationship("SubscriberCreditGrantor", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _SubscriberCreditGrantorEnquiryID As New IntegerElement("SubscriberCreditGrantorEnquiryID", Me, True)
  Public ReadOnly _SubscriberCreditGrantorID As New IntegerElement("SubscriberCreditGrantorID", Me, True)
  Public ReadOnly _SubscriberCreditGrantorEnquiry As SubscriberCreditGrantorEnquiryRelationship = SubscriberCreditGrantorEnquiry
  Public ReadOnly _SubscriberCreditGrantor As SubscriberCreditGrantorRelationship = SubscriberCreditGrantor
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberCreditGrantorEnquiryResult", ApplicationSettings)
    _SubscriberCreditGrantorEnquiry._SubscriberID.LocalElement = _SubscriberID
    _SubscriberCreditGrantorEnquiry._SubscriberCreditGrantorEnquiryID.LocalElement = _SubscriberCreditGrantorEnquiryID
    _SubscriberCreditGrantor._SubscriberCreditGrantorID.LocalElement = _SubscriberCreditGrantorID
    _SubscriberCreditGrantor._SubscriberID.LocalElement = _SubscriberID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"

#End Region 'sembleWare: Change Events

#Region " Actions"

  Public Sub SelectRecord()
    With ApplicationSettings
      Try
        .BeginTransaction()
        Me.CreditGrantorSelectedYN.Value = True
        Me.DetailsViewedYN.Value = True
        Me.DetailsViewedDate.Value = System.DateTime.Now
        Me.Save()

        Dim oSubscriberCreditGrantor As SubscriberCreditGrantor = Me.SubscriberCreditGrantor.Instance
        Dim oSubscriberCreditGrantorEnquiry As SubscriberCreditGrantorEnquiry = Me.SubscriberCreditGrantorEnquiry.Instance
        oSubscriberCreditGrantorEnquiry.EnquiryResultInd.Value = Constants.SubscriberCreditGrantorEnquiry.Results.RecordSelected

        oSubscriberCreditGrantorEnquiry.ResultReferenceNo.Value = oSubscriberCreditGrantor.ReferenceNo.Value
        oSubscriberCreditGrantorEnquiry.ResultEnquiryDate.Value = oSubscriberCreditGrantor.EnquiryDate.Value

        oSubscriberCreditGrantorEnquiry.Save()
        .CommitTransaction()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
    End With
  End Sub

#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class SubscriberCreditGrantorEnquiryResultRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SubscriberCreditGrantorEnquiryID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SubscriberCreditGrantorID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberCreditGrantorEnquiryResult(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberID.ForeignElement = CType(Instance, SubscriberCreditGrantorEnquiryResult)._SubscriberID
    _SubscriberCreditGrantorEnquiryID.ForeignElement = CType(Instance, SubscriberCreditGrantorEnquiryResult)._SubscriberCreditGrantorEnquiryID
    _SubscriberCreditGrantorID.ForeignElement = CType(Instance, SubscriberCreditGrantorEnquiryResult)._SubscriberCreditGrantorID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberCreditGrantorEnquiryResult")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberCreditGrantorEnquiryResult].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberCreditGrantorEnquiryID", "[A0SubscriberCreditGrantorEnquiryResult].[SubscriberCreditGrantorEnquiryID]", "SubscriberCreditGrantorEnquiryID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber Credit Grantor Enquiry ID"))
    oList.Columns.Add(New ListColumn("SubscriberCreditGrantorID", "[A0SubscriberCreditGrantorEnquiryResult].[SubscriberCreditGrantorID]", "SubscriberCreditGrantorID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber Credit Grantor ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A0SubscriberCreditGrantorEnquiryResult].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("PassportNo", "[A0SubscriberCreditGrantorEnquiryResult].[PassportNo]", "PassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No / Other ID No"))
    oList.Columns.Add(New ListColumn("ReferenceNo", "[A0SubscriberCreditGrantorEnquiryResult].[ReferenceNo]", "ReferenceNo", DataType.String, Nothing, False, 0, True, 100, "Reference No"))
    oList.Columns.Add(New ListColumn("EnquiryDate", "[A0SubscriberCreditGrantorEnquiryResult].[EnquiryDate]", "EnquiryDate", DataType.DateTime, "g", False, 0, True, 100, "Enquiry Date"))
    oList.Columns.Add(New ListColumn("DetailsViewedYN", "[A0SubscriberCreditGrantorEnquiryResult].[DetailsViewedYN]", "DetailsViewedYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Details Viewed YN"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberCreditGrantorEnquiryResult]  [A0SubscriberCreditGrantorEnquiryResult]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function MultipleMatchGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberCreditGrantorEnquiryResult")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberCreditGrantorEnquiryResult].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberCreditGrantorEnquiryID", "[A0SubscriberCreditGrantorEnquiryResult].[SubscriberCreditGrantorEnquiryID]", "SubscriberCreditGrantorEnquiryID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber Credit Grantor Enquiry ID"))
    oList.Columns.Add(New ListColumn("SubscriberCreditGrantorID", "[A0SubscriberCreditGrantorEnquiryResult].[SubscriberCreditGrantorID]", "SubscriberCreditGrantorID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber Credit Grantor ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A0SubscriberCreditGrantorEnquiryResult].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("PassportNo", "[A0SubscriberCreditGrantorEnquiryResult].[PassportNo]", "PassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No / Other ID No"))
    oList.Columns.Add(New ListColumn("ReferenceNo", "[A0SubscriberCreditGrantorEnquiryResult].[ReferenceNo]", "ReferenceNo", DataType.String, Nothing, False, 0, True, 100, "Reference No"))
    oList.Columns.Add(New ListColumn("EnquiryDate", "[A0SubscriberCreditGrantorEnquiryResult].[EnquiryDate]", "EnquiryDate", DataType.DateTime, "g", False, 0, True, 100, "Enquiry Date"))
    oList.Columns.Add(New ListColumn("DetailsViewedYN", "[A0SubscriberCreditGrantorEnquiryResult].[DetailsViewedYN]", "DetailsViewedYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Details Viewed YN"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberCreditGrantorEnquiryResult]  [A0SubscriberCreditGrantorEnquiryResult]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberCreditGrantorEnquiryResult")
    oList.Columns.Add(New ListColumn("SubscriberCreditGrantorID", "[A0SubscriberCreditGrantorEnquiryResult].[SubscriberCreditGrantorID]", "SubscriberCreditGrantorID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber Credit Grantor ID"))
    oList.Columns.Add(New ListColumn("SubscriberCreditGrantorEnquiryID", "[A0SubscriberCreditGrantorEnquiryResult].[SubscriberCreditGrantorEnquiryID]", "SubscriberCreditGrantorEnquiryID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber Credit Grantor Enquiry ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberCreditGrantorEnquiryResult].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A0SubscriberCreditGrantorEnquiryResult].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("PassportNo", "[A0SubscriberCreditGrantorEnquiryResult].[PassportNo]", "PassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No / Other ID No"))
    oList.Columns.Add(New ListColumn("ReferenceNo", "[A0SubscriberCreditGrantorEnquiryResult].[ReferenceNo]", "ReferenceNo", DataType.String, Nothing, False, 0, True, 100, "Reference No"))
    oList.Columns.Add(New ListColumn("EnquiryDate", "[A0SubscriberCreditGrantorEnquiryResult].[EnquiryDate]", "EnquiryDate", DataType.DateTime, "g", False, 0, True, 100, "Enquiry Date"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberCreditGrantorEnquiryResult]  [A0SubscriberCreditGrantorEnquiryResult]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
