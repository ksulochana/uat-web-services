Imports sembleWare.Runtime
Imports System
Public Class EnquiryMatchingEngine 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly EnquiryMatchingEngineID As New IdentityElement("EnquiryMatchingEngineID", Me, True, Nothing, Nothing)
  Public ReadOnly MatchingEngineDate As New DateTimeElement("MatchingEngineDate", Me, False, True, True, True, True, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly EnquiryMatchingEngineConsumer_OwnMany As Relationship = New EnquiryMatchingEngineConsumerRelationship("EnquiryMatchingEngineConsumer", Nothing, "EnquiryMatchingEngine", Me)
  Public ReadOnly EnquiryMatchingEngineHomeAffairs_OwnMany As Relationship = New EnquiryMatchingEngineHomeAffairsRelationship("EnquiryMatchingEngineHomeAffairs", Nothing, "EnquiryMatchingEngine", Me)
  Public ReadOnly EnquiryMatchingEngineCommercial_OwnMany As Relationship = New EnquiryMatchingEngineCommercialRelationship("EnquiryMatchingEngineCommercial", Nothing, "EnquiryMatchingEngine", Me)
  Public ReadOnly EnquiryMatchingEngineAuditor_OwnMany As Relationship = New EnquiryMatchingEngineAuditorRelationship("EnquiryMatchingEngineAuditor", Nothing, "EnquiryMatchingEngine", Me)
  Public ReadOnly EnquiryMatchingEngineDirector_OwnMany As Relationship = New EnquiryMatchingEngineDirectorRelationship("EnquiryMatchingEngineDirector", Nothing, "EnquiryMatchingEngine", Me)
  Public ReadOnly EnquiryMatchingEngineSubscriberCreditGrantor_OwnMany As Relationship = New EnquiryMatchingEngineSubscriberCreditGrantorRelationship("EnquiryMatchingEngineSubscriberCreditGrantor", Nothing, "EnquiryMatchingEngine", Me)
  Public ReadOnly EnquiryMatchingEngineSAFPSSubject_OwnMany As Relationship = New EnquiryMatchingEngineSAFPSSubjectRelationship("EnquiryMatchingEngineSAFPSSubject", Nothing, "EnquiryMatchingEngine", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  Public ReadOnly _EnquiryMatchingEngineConsumer_OwnMany As EnquiryMatchingEngineConsumerRelationship = EnquiryMatchingEngineConsumer_OwnMany
  Public ReadOnly _EnquiryMatchingEngineHomeAffairs_OwnMany As EnquiryMatchingEngineHomeAffairsRelationship = EnquiryMatchingEngineHomeAffairs_OwnMany
  Public ReadOnly _EnquiryMatchingEngineCommercial_OwnMany As EnquiryMatchingEngineCommercialRelationship = EnquiryMatchingEngineCommercial_OwnMany
  Public ReadOnly _EnquiryMatchingEngineAuditor_OwnMany As EnquiryMatchingEngineAuditorRelationship = EnquiryMatchingEngineAuditor_OwnMany
  Public ReadOnly _EnquiryMatchingEngineDirector_OwnMany As EnquiryMatchingEngineDirectorRelationship = EnquiryMatchingEngineDirector_OwnMany
  Public ReadOnly _EnquiryMatchingEngineSubscriberCreditGrantor_OwnMany As EnquiryMatchingEngineSubscriberCreditGrantorRelationship = EnquiryMatchingEngineSubscriberCreditGrantor_OwnMany
  Public ReadOnly _EnquiryMatchingEngineSAFPSSubject_OwnMany As EnquiryMatchingEngineSAFPSSubjectRelationship = EnquiryMatchingEngineSAFPSSubject_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("EnquiryMatchingEngine", ApplicationSettings)
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreate()
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreate()
    MatchingEngineDate.Value = System.DateTime.Now
  End Sub
#End Region

#Region " Shared Procedures"
  Public Shared Function ConsumerMatch(ByVal IDNo As Element, ByVal PassportNo As Element, ByVal Surname As Element, ByVal MaidenName As Element, _
                                       ByVal FirstInitial As Element, ByVal FirstName As Element, ByVal SecondInitial As Element, ByVal SecondName As Element, _
                                       ByVal GenderInd As Element, ByVal BirthDate As Element, ByVal AccountNo As Element, ByVal SubAccountNo As Element, _
                                       ByVal ProductID As Integer, ByVal ApplicationSettings As ApplicationSettings) As Long
    Return EnquiryMatchingEngine.ConsumerMatch(IDNo, PassportNo, Surname, MaidenName, FirstInitial, FirstName, SecondInitial, SecondName, GenderInd, BirthDate, AccountNo, SubAccountNo, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, ProductID, ApplicationSettings)
  End Function

  Public Shared Function ConsumerMatch(ByVal IDNo As Element, ByVal PassportNo As Element, ByVal Surname As Element, ByVal MaidenName As Element, _
                                       ByVal FirstInitial As Element, ByVal FirstName As Element, ByVal SecondInitial As Element, ByVal SecondName As Element, _
                                       ByVal GenderInd As Element, ByVal BirthDate As Element, ByVal AccountNo As Element, ByVal SubAccountNo As Element, _
                                       ByVal Address1 As Element, ByVal Address2 As Element, ByVal Address3 As Element, ByVal Address4 As Element, _
                                       ByVal PostalCode As Element, ByVal TelephoneCode As Element, ByVal TelephoneNo As Element, _
                                       ByVal AgeSearchTypeInd As Element, ByVal Age As Element, ByVal AgeBirthYear As Element, ByVal AgeDeviation As Element, _
                                       ByVal ProductID As Integer, ByVal ApplicationSettings As ApplicationSettings) As Long
    Dim oEnquiryMatchingEngine As EnquiryMatchingEngine = New EnquiryMatchingEngineRelationship(ApplicationSettings).NewInstance
    oEnquiryMatchingEngine.Save()

    Dim oEnquiryMatchingEngineConsumer As EnquiryMatchingEngineConsumer = oEnquiryMatchingEngine.EnquiryMatchingEngineConsumer_OwnMany.NewInstance
    If Not IDNo Is Nothing AndAlso Not IDNo.IsEmpty Then
      oEnquiryMatchingEngineConsumer.IDNo.Value = IDNo.ValueAsObject
    End If
    If Not PassportNo Is Nothing AndAlso Not PassportNo.IsEmpty Then
      oEnquiryMatchingEngineConsumer.PassportNo.Value = PassportNo.ValueAsObject
    End If
    If Not Surname Is Nothing AndAlso Not Surname.IsEmpty Then
      oEnquiryMatchingEngineConsumer.Surname.Value = Surname.ValueAsObject
    End If
    If Not MaidenName Is Nothing AndAlso Not MaidenName.IsEmpty Then
      oEnquiryMatchingEngineConsumer.MaidenName.Value = MaidenName.ValueAsObject
    End If
    If Not FirstInitial Is Nothing AndAlso Not FirstInitial.IsEmpty Then
      oEnquiryMatchingEngineConsumer.FirstInitial.Value = FirstInitial.ValueAsObject
    End If
    If Not FirstName Is Nothing AndAlso Not FirstName.IsEmpty Then
      oEnquiryMatchingEngineConsumer.FirstName.Value = FirstName.ValueAsObject
    End If
    If Not SecondInitial Is Nothing AndAlso Not SecondInitial.IsEmpty Then
      oEnquiryMatchingEngineConsumer.SecondInitial.Value = SecondInitial.ValueAsObject
    End If
    If Not SecondName Is Nothing AndAlso Not SecondName.IsEmpty Then
      oEnquiryMatchingEngineConsumer.SecondName.Value = SecondName.ValueAsObject
    End If
    If Not GenderInd Is Nothing AndAlso Not GenderInd.IsEmpty Then
      oEnquiryMatchingEngineConsumer.GenderInd.Value = GenderInd.ValueAsObject
    End If
    If Not BirthDate Is Nothing AndAlso Not BirthDate.IsEmpty Then
      oEnquiryMatchingEngineConsumer.BirthDate.Value = BirthDate.ValueAsObject
    End If
    If Not AccountNo Is Nothing AndAlso Not AccountNo.IsEmpty Then
      oEnquiryMatchingEngineConsumer.AccountNo.Value = AccountNo.ValueAsObject
    End If
    If Not SubAccountNo Is Nothing AndAlso Not SubAccountNo.IsEmpty Then
      oEnquiryMatchingEngineConsumer.SubAccountNo.Value = SubAccountNo.ValueAsObject
    End If
    If Not Address1 Is Nothing AndAlso Not Address1.IsEmpty Then
      oEnquiryMatchingEngineConsumer.Address1.Value = Address1.ValueAsObject
    End If
    If Not Address2 Is Nothing AndAlso Not Address2.IsEmpty Then
      oEnquiryMatchingEngineConsumer.Address2.Value = Address2.ValueAsObject
    End If
    If Not Address3 Is Nothing AndAlso Not Address3.IsEmpty Then
      oEnquiryMatchingEngineConsumer.Address3.Value = Address3.ValueAsObject
    End If
    If Not Address4 Is Nothing AndAlso Not Address4.IsEmpty Then
      oEnquiryMatchingEngineConsumer.Address4.Value = Address4.ValueAsObject
    End If
    If Not PostalCode Is Nothing AndAlso Not PostalCode.IsEmpty Then
      oEnquiryMatchingEngineConsumer.PostalCode.Value = PostalCode.ValueAsObject
    End If
    If Not TelephoneCode Is Nothing AndAlso Not TelephoneCode.IsEmpty Then
      oEnquiryMatchingEngineConsumer.TelephoneCode.Value = TelephoneCode.ValueAsObject
    End If
    If Not TelephoneNo Is Nothing AndAlso Not TelephoneNo.IsEmpty Then
      oEnquiryMatchingEngineConsumer.TelephoneNo.Value = TelephoneNo.ValueAsObject
    End If
    If Not AgeSearchTypeInd Is Nothing AndAlso Not AgeSearchTypeInd.IsEmpty Then
      oEnquiryMatchingEngineConsumer.AgeSearchTypeInd.Value = AgeSearchTypeInd.ValueAsObject
    End If
    If Not Age Is Nothing AndAlso Not Age.IsEmpty Then
      oEnquiryMatchingEngineConsumer.Age.Value = Age.ValueAsObject
    End If
    If Not AgeBirthYear Is Nothing AndAlso Not AgeBirthYear.IsEmpty Then
      oEnquiryMatchingEngineConsumer.AgeBirthYear.Value = AgeBirthYear.ValueAsObject
    End If
    If Not AgeDeviation Is Nothing AndAlso Not AgeDeviation.IsEmpty Then
      oEnquiryMatchingEngineConsumer.AgeDeviation.Value = AgeDeviation.ValueAsObject
    End If
    If Not Address1 Is Nothing AndAlso Not Address1.IsEmpty AndAlso Not Address2 Is Nothing AndAlso Not Address2.IsEmpty AndAlso Not PostalCode Is Nothing AndAlso Not PostalCode.IsEmpty Then
      oEnquiryMatchingEngineConsumer.AddressDupCode.Value = ConsumerAddress.CalculateDupCode(Address1.ValueAsObject, Address2.ValueAsObject, PostalCode.ValueAsObject)
      ' NEW ETL REPLACEMENT METHODS
      'oEnquiryMatchingEngineConsumer.AddressDupCode.Value = ConsumerAddress.CalculateDupCode(Address1.ValueAsObject, Address2.ValueAsObject, PostalCode.ValueAsObject, ApplicationSettings)
    End If

    oEnquiryMatchingEngineConsumer.Save()

    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand
    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spConsumer_S_Match"
    oCommand.CommandTimeout = 0

    oCommand.Parameters.Add("@EnquiryMatchingEngineID", SqlDbType.Int)
    oCommand.Parameters.Add("@ProductID", SqlDbType.Int)

    oCommand.Parameters("@EnquiryMatchingEngineID").Value = oEnquiryMatchingEngine.EnquiryMatchingEngineID.Value
    oCommand.Parameters("@ProductID").Value = ProductID

    ApplicationSettings.ActionQuery(oCommand)

    Return oEnquiryMatchingEngine.EnquiryMatchingEngineID.Value
  End Function

  Public Shared Function HomeAffairsMatch(ByVal IDNo As Element, ByVal Surname As Element, ByVal FirstName As Element, ByVal SecondName As Element, _
                                          ByVal BirthDate As Element, ByVal ProductID As Integer, ByVal ApplicationSettings As ApplicationSettings) As Long
    Dim oEnquiryMatchingEngine As EnquiryMatchingEngine = New EnquiryMatchingEngineRelationship(ApplicationSettings).NewInstance
    oEnquiryMatchingEngine.Save()

    Dim oEnquiryMatchingEngineHomeAffairs As EnquiryMatchingEngineHomeAffairs = oEnquiryMatchingEngine.EnquiryMatchingEngineHomeAffairs_OwnMany.NewInstance
    If Not IDNo Is Nothing AndAlso Not IDNo.IsEmpty Then
      oEnquiryMatchingEngineHomeAffairs.IDNo.Value = IDNo.ValueAsObject
    End If
    If Not Surname Is Nothing AndAlso Not Surname.IsEmpty Then
      oEnquiryMatchingEngineHomeAffairs.Surname.Value = Surname.ValueAsObject
    End If
    If Not FirstName Is Nothing AndAlso Not FirstName.IsEmpty Then
      oEnquiryMatchingEngineHomeAffairs.FirstName.Value = FirstName.ValueAsObject
    End If
    If Not SecondName Is Nothing AndAlso Not SecondName.IsEmpty Then
      oEnquiryMatchingEngineHomeAffairs.SecondName.Value = SecondName.ValueAsObject
    End If
    If Not BirthDate Is Nothing AndAlso Not BirthDate.IsEmpty Then
      oEnquiryMatchingEngineHomeAffairs.BirthDate.Value = BirthDate.ValueAsObject
    End If

    oEnquiryMatchingEngineHomeAffairs.Save()

    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand
    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spHomeAffairs_S_Match"
    oCommand.CommandTimeout = 0

    oCommand.Parameters.Add("@EnquiryMatchingEngineID", SqlDbType.Int)
    oCommand.Parameters.Add("@ProductID", SqlDbType.Int)

    oCommand.Parameters("@EnquiryMatchingEngineID").Value = oEnquiryMatchingEngine.EnquiryMatchingEngineID.Value
    oCommand.Parameters("@ProductID").Value = ProductID

    ApplicationSettings.ActionQuery(oCommand)

    Return oEnquiryMatchingEngine.EnquiryMatchingEngineID.Value
  End Function

  Public Shared Function CommercialMatch(ByVal RegistrationNo As Element, ByVal CommercialName As Element, ByVal TaxNo As Element, _
                                         ByVal SICCode As Element, ByVal ProductID As Integer, ByVal ApplicationSettings As ApplicationSettings) As Long
    Dim oEnquiryMatchingEngine As EnquiryMatchingEngine = New EnquiryMatchingEngineRelationship(ApplicationSettings).NewInstance
    oEnquiryMatchingEngine.Save()

    Dim oEnquiryMatchingEngineCommercial As EnquiryMatchingEngineCommercial = oEnquiryMatchingEngine.EnquiryMatchingEngineCommercial_OwnMany.NewInstance
    If Not RegistrationNo Is Nothing AndAlso Not RegistrationNo.IsEmpty Then
      oEnquiryMatchingEngineCommercial.RegistrationNo.Value = RegistrationNo.ValueAsObject
    End If
    If Not CommercialName Is Nothing AndAlso Not CommercialName.IsEmpty Then
      oEnquiryMatchingEngineCommercial.CommercialName.Value = CommercialName.ValueAsObject
    End If
    If Not TaxNo Is Nothing AndAlso Not TaxNo.IsEmpty Then
      oEnquiryMatchingEngineCommercial.TaxNo.Value = TaxNo.ValueAsObject
    End If
    If Not SICCode Is Nothing AndAlso Not SICCode.IsEmpty Then
      oEnquiryMatchingEngineCommercial.SICCode.Value = SICCode.ValueAsObject
    End If

    oEnquiryMatchingEngineCommercial.Save()

    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand
    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spCommercial_S_Match"
    oCommand.CommandTimeout = 0

    oCommand.Parameters.Add("@EnquiryMatchingEngineID", SqlDbType.Int)
    oCommand.Parameters.Add("@ProductID", SqlDbType.Int)

    oCommand.Parameters("@EnquiryMatchingEngineID").Value = oEnquiryMatchingEngine.EnquiryMatchingEngineID.Value
    oCommand.Parameters("@ProductID").Value = ProductID

    ApplicationSettings.ActionQuery(oCommand)

    Return oEnquiryMatchingEngine.EnquiryMatchingEngineID.Value
  End Function

  Public Shared Function AuditorMatch(ByVal ProfessionNo As Element, ByVal AuditorName As Element, _
                                      ByVal ProductID As Integer, ByVal ApplicationSettings As ApplicationSettings) As Long
    Dim oEnquiryMatchingEngine As EnquiryMatchingEngine = New EnquiryMatchingEngineRelationship(ApplicationSettings).NewInstance
    oEnquiryMatchingEngine.Save()

    Dim oEnquiryMatchingEngineAuditor As EnquiryMatchingEngineAuditor = oEnquiryMatchingEngine.EnquiryMatchingEngineAuditor_OwnMany.NewInstance
    If Not ProfessionNo Is Nothing AndAlso Not ProfessionNo.IsEmpty Then
      oEnquiryMatchingEngineAuditor.ProfessionNo.Value = ProfessionNo.ValueAsObject
    End If
    If Not AuditorName Is Nothing AndAlso Not AuditorName.IsEmpty Then
      oEnquiryMatchingEngineAuditor.AuditorName.Value = AuditorName.ValueAsObject
    End If

    oEnquiryMatchingEngineAuditor.Save()

    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand
    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spAuditor_S_Match"
    oCommand.CommandTimeout = 0

    oCommand.Parameters.Add("@EnquiryMatchingEngineID", SqlDbType.Int)
    oCommand.Parameters.Add("@ProductID", SqlDbType.Int)

    oCommand.Parameters("@EnquiryMatchingEngineID").Value = oEnquiryMatchingEngine.EnquiryMatchingEngineID.Value
    oCommand.Parameters("@ProductID").Value = ProductID

    ApplicationSettings.ActionQuery(oCommand)

    Return oEnquiryMatchingEngine.EnquiryMatchingEngineID.Value
  End Function

  Public Shared Function DirectorMatch(ByVal IDNo As Element, ByVal Surname As Element, ByVal FirstInitial As Element, ByVal FirstName As Element, _
                                       ByVal SecondInitial As Element, ByVal SecondName As Element, ByVal BirthDate As Element, _
                                       ByVal ProductID As Integer, ByVal ApplicationSettings As ApplicationSettings) As Long
    Dim oEnquiryMatchingEngine As EnquiryMatchingEngine = New EnquiryMatchingEngineRelationship(ApplicationSettings).NewInstance
    oEnquiryMatchingEngine.Save()

    Dim oEnquiryMatchingEngineDirector As EnquiryMatchingEngineDirector = oEnquiryMatchingEngine.EnquiryMatchingEngineDirector_OwnMany.NewInstance
    If Not IDNo Is Nothing AndAlso Not IDNo.IsEmpty Then
      oEnquiryMatchingEngineDirector.IDNo.Value = IDNo.ValueAsObject
    End If
    If Not Surname Is Nothing AndAlso Not Surname.IsEmpty Then
      oEnquiryMatchingEngineDirector.Surname.Value = Surname.ValueAsObject
    End If
    If Not FirstInitial Is Nothing AndAlso Not FirstInitial.IsEmpty Then
      oEnquiryMatchingEngineDirector.FirstInitial.Value = FirstInitial.ValueAsObject
    End If
    If Not FirstName Is Nothing AndAlso Not FirstName.IsEmpty Then
      oEnquiryMatchingEngineDirector.FirstName.Value = FirstName.ValueAsObject
    End If
    If Not SecondInitial Is Nothing AndAlso Not SecondInitial.IsEmpty Then
      oEnquiryMatchingEngineDirector.SecondInitial.Value = SecondInitial.ValueAsObject
    End If
    If Not SecondName Is Nothing AndAlso Not SecondName.IsEmpty Then
      oEnquiryMatchingEngineDirector.SecondName.Value = SecondName.ValueAsObject
    End If
    If Not BirthDate Is Nothing AndAlso Not BirthDate.IsEmpty Then
      oEnquiryMatchingEngineDirector.BirthDate.Value = BirthDate.ValueAsObject
    End If

    oEnquiryMatchingEngineDirector.Save()

    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand
    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spDirector_S_Match"
    oCommand.CommandTimeout = 0

    oCommand.Parameters.Add("@EnquiryMatchingEngineID", SqlDbType.Int)
    oCommand.Parameters.Add("@ProductID", SqlDbType.Int)

    oCommand.Parameters("@EnquiryMatchingEngineID").Value = oEnquiryMatchingEngine.EnquiryMatchingEngineID.Value
    oCommand.Parameters("@ProductID").Value = ProductID

    ApplicationSettings.ActionQuery(oCommand)

    Return oEnquiryMatchingEngine.EnquiryMatchingEngineID.Value
  End Function

  Public Shared Function SubscriberCreditGrantorMatch(ByVal ReferenceNo As Element, ByVal IDNo As Element, ByVal PassportNo As Element, _
                                                      ByVal EnquiryDate As Element, ByVal SubscriberID As Element, _
                                                      ByVal ProductID As Integer, ByVal ApplicationSettings As ApplicationSettings) As Long
    Dim oEnquiryMatchingEngine As EnquiryMatchingEngine = New EnquiryMatchingEngineRelationship(ApplicationSettings).NewInstance
    oEnquiryMatchingEngine.Save()

    Dim oEnquiryMatchingEngineSubscriberCreditGrantor As EnquiryMatchingEngineSubscriberCreditGrantor = oEnquiryMatchingEngine.EnquiryMatchingEngineSubscriberCreditGrantor_OwnMany.NewInstance
    If Not ReferenceNo Is Nothing AndAlso Not ReferenceNo.IsEmpty Then
      oEnquiryMatchingEngineSubscriberCreditGrantor.ReferenceNo.Value = ReferenceNo.ValueAsObject
    End If
    If Not IDNo Is Nothing AndAlso Not IDNo.IsEmpty Then
      oEnquiryMatchingEngineSubscriberCreditGrantor.IDNo.Value = IDNo.ValueAsObject
    End If
    If Not PassportNo Is Nothing AndAlso Not PassportNo.IsEmpty Then
      oEnquiryMatchingEngineSubscriberCreditGrantor.PassportNo.Value = PassportNo.ValueAsObject
    End If
    If Not EnquiryDate Is Nothing AndAlso Not EnquiryDate.IsEmpty Then
      oEnquiryMatchingEngineSubscriberCreditGrantor.EnquiryDate.Value = EnquiryDate.ValueAsObject
    End If
    If Not SubscriberID Is Nothing AndAlso Not SubscriberID.IsEmpty Then
      oEnquiryMatchingEngineSubscriberCreditGrantor.SubscriberID.Value = SubscriberID.ValueAsObject
    End If

    oEnquiryMatchingEngineSubscriberCreditGrantor.Save()

    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand
    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spCreditGrantorEnquiry_S_Match"
    oCommand.CommandTimeout = 0

    oCommand.Parameters.Add("@EnquiryMatchingEngineID", SqlDbType.Int)
    oCommand.Parameters.Add("@ProductID", SqlDbType.Int)

    oCommand.Parameters("@EnquiryMatchingEngineID").Value = oEnquiryMatchingEngine.EnquiryMatchingEngineID.Value
    oCommand.Parameters("@ProductID").Value = ProductID

    ApplicationSettings.ActionQuery(oCommand)

    Return oEnquiryMatchingEngine.EnquiryMatchingEngineID.Value
  End Function

  Public Shared Function SAFPSSubjectMatch(ByVal IDNo As Element, ByVal PassportNo As Element, ByVal Surname As Element, _
                                           ByVal FirstName As Element, ByVal SecondName As Element, _
                                           ByVal GenderInd As Element, ByVal BirthDate As Element, _
                                           ByVal ProductID As Integer, ByVal ApplicationSettings As ApplicationSettings) As Long
    Dim oEnquiryMatchingEngine As EnquiryMatchingEngine = New EnquiryMatchingEngineRelationship(ApplicationSettings).NewInstance
    oEnquiryMatchingEngine.Save()

    Dim oEnquiryMatchingEngineSAFPSSubject As EnquiryMatchingEngineSAFPSSubject = oEnquiryMatchingEngine.EnquiryMatchingEngineSAFPSSubject_OwnMany.NewInstance
    If Not IDNo Is Nothing AndAlso Not IDNo.IsEmpty Then
      oEnquiryMatchingEngineSAFPSSubject.IDNo.Value = IDNo.ValueAsObject
    End If
    If Not PassportNo Is Nothing AndAlso Not PassportNo.IsEmpty Then
      oEnquiryMatchingEngineSAFPSSubject.PassportNo.Value = PassportNo.ValueAsObject
    End If
    If Not Surname Is Nothing AndAlso Not Surname.IsEmpty Then
      oEnquiryMatchingEngineSAFPSSubject.Surname.Value = Surname.ValueAsObject
    End If
    If Not FirstName Is Nothing AndAlso Not FirstName.IsEmpty Then
      oEnquiryMatchingEngineSAFPSSubject.FirstName.Value = FirstName.ValueAsObject
    End If
    If Not SecondName Is Nothing AndAlso Not SecondName.IsEmpty Then
      oEnquiryMatchingEngineSAFPSSubject.SecondName.Value = SecondName.ValueAsObject
    End If
    If Not GenderInd Is Nothing AndAlso Not GenderInd.IsEmpty Then
      oEnquiryMatchingEngineSAFPSSubject.GenderInd.Value = GenderInd.ValueAsObject
    End If
    If Not BirthDate Is Nothing AndAlso Not BirthDate.IsEmpty Then
      oEnquiryMatchingEngineSAFPSSubject.BirthDate.Value = BirthDate.ValueAsObject
    End If
    oEnquiryMatchingEngineSAFPSSubject.Save()

    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand
    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spSAFPSSubject_S_Match"
    oCommand.CommandTimeout = 0

    oCommand.Parameters.Add("@EnquiryMatchingEngineID", SqlDbType.Int)
    oCommand.Parameters.Add("@ProductID", SqlDbType.Int)

    oCommand.Parameters("@EnquiryMatchingEngineID").Value = oEnquiryMatchingEngine.EnquiryMatchingEngineID.Value
    oCommand.Parameters("@ProductID").Value = ProductID

    ApplicationSettings.ActionQuery(oCommand)

    Return oEnquiryMatchingEngine.EnquiryMatchingEngineID.Value
  End Function
#End Region
End Class 'sembleWare: Part

Public Class EnquiryMatchingEngineRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _EnquiryMatchingEngineID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New EnquiryMatchingEngine(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _EnquiryMatchingEngineID.ForeignElement = CType(Instance, EnquiryMatchingEngine).EnquiryMatchingEngineID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
