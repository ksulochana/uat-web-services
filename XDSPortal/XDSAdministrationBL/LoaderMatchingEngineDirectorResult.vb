Imports sembleWare.Runtime
Imports System
Public Class LoaderMatchingEngineDirectorResult 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly IgnoreSurnameSoundExYN As New BooleanElement("IgnoreSurnameSoundExYN", Me, False, False, True, True, True, "Ignore Surname Sound Ex?", Nothing, False, "Yes;No")
  Public ReadOnly IgnoreSurnameExactYN As New BooleanElement("IgnoreSurnameExactYN", Me, False, False, True, True, True, "Ignore Surname Exact?", Nothing, False, "Yes;No")
  Public ReadOnly IgnoreSurnameVariationYN As New BooleanElement("IgnoreSurnameVariationYN", Me, False, False, True, True, True, "Ignore Surname Variation?", Nothing, False, "Yes;No")
  Public ReadOnly IgnoreOtherYN As New BooleanElement("IgnoreOtherYN", Me, False, False, True, True, True, "Ignore Other?", Nothing, False, "Yes;No")
  Public ReadOnly LoaderMatchingEngineCommercialDirector As Relationship = New LoaderMatchingEngineDirectorRelationship("LoaderMatchingEngineCommercialDirector", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly Director As Relationship = New DirectorRelationship("Director", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _RecordID As New IntegerElement("RecordID", Me, True)
  Public ReadOnly _LoaderMatchingEngineID As New IntegerElement("LoaderMatchingEngineID", Me, True)
  Public ReadOnly _DirectorID As New IntegerElement("DirectorID", Me, True)
  Public ReadOnly _LoaderMatchingEngineCommercialDirector As LoaderMatchingEngineDirectorRelationship = LoaderMatchingEngineCommercialDirector
  Public ReadOnly _Director As DirectorRelationship = Director
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("LoaderMatchingEngineDirectorResult", ApplicationSettings)
    _LoaderMatchingEngineCommercialDirector._RecordID.LocalElement = _RecordID
    _LoaderMatchingEngineCommercialDirector._LoaderMatchingEngineID.LocalElement = _LoaderMatchingEngineID
    _Director._DirectorID.LocalElement = _DirectorID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class LoaderMatchingEngineDirectorResultRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _RecordID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _LoaderMatchingEngineID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _DirectorID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New LoaderMatchingEngineDirectorResult(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _RecordID.ForeignElement = CType(Instance, LoaderMatchingEngineDirectorResult)._RecordID
    _LoaderMatchingEngineID.ForeignElement = CType(Instance, LoaderMatchingEngineDirectorResult)._LoaderMatchingEngineID
    _DirectorID.ForeignElement = CType(Instance, LoaderMatchingEngineDirectorResult)._DirectorID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
