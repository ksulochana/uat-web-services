Imports sembleWare.Runtime
Imports System
Public Class SecurityControl 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly ControlName As New StringElement("ControlName", Me, True, False, True, True, True, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ControlCaption As New StringElement("ControlCaption", Me, False, False, True, True, False, 250, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly IsActiveYN As New BooleanElement("IsActiveYN", Me, False, True, True, True, True, Nothing, Nothing, True, "Yes;No")
  Public ReadOnly SecurityForm As Relationship = New SecurityFormRelationship("SecurityForm", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly SecurityControlAccess_OwnMany As Relationship = New RoleControlAccessRelationship("SecurityControlAccess", Nothing, "SecurityControl", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _FormName As New StringElement("FormName", Me, True)
  Public ReadOnly _AssemblyName As New StringElement("AssemblyName", Me, True)
  Public ReadOnly _SecurityForm As SecurityFormRelationship = SecurityForm
  Public ReadOnly _SecurityControlAccess_OwnMany As RoleControlAccessRelationship = SecurityControlAccess_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SecurityControl", ApplicationSettings)
    _SecurityForm._FormName.LocalElement = _FormName
    _SecurityForm._AssemblyName.LocalElement = _AssemblyName
    'sembleWare: Constructor End - Do Not Modify
    Me.ForceUpperCaseKeys = False
  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class SecurityControlRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _ControlName As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _FormName As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _AssemblyName As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SecurityControl(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _ControlName.ForeignElement = CType(Instance, SecurityControl).ControlName
    _FormName.ForeignElement = CType(Instance, SecurityControl)._FormName
    _AssemblyName.ForeignElement = CType(Instance, SecurityControl)._AssemblyName
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"




  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SecurityControl")
    oList.Columns.Add(New ListColumn("ControlName", "[A0SecurityControl].[ControlName]", "ControlName", DataType.String, Nothing, True, 1, False, 200, "Control Name"))
    oList.Columns.Add(New ListColumn("FormName", "[A0SecurityControl].[FormName]", "FormName", DataType.String, Nothing, True, 0, False, 100, "Form Name"))
    oList.Columns.Add(New ListColumn("ControlCaption", "[A0SecurityControl].[ControlCaption]", "ControlCaption", DataType.String, Nothing, False, 0, True, 200, "Control Caption"))
    oList.Columns.Add(New ListColumn("AssemblyName", "[A0SecurityControl].[AssemblyName]", "AssemblyName", DataType.String, Nothing, True, 0, False, 100, "Assembly Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SecurityControl]  [A0SecurityControl]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function DropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SecurityControl")
    oList.Columns.Add(New ListColumn("ControlName", "[A0SecurityControl].[ControlName]", "ControlName", DataType.String, Nothing, True, 1, True, 200, "Control Name"))
    oList.Columns.Add(New ListColumn("FormName", "[A0SecurityControl].[FormName]", "FormName", DataType.String, Nothing, True, 0, False, 100, "Form Name"))
    oList.Columns.Add(New ListColumn("ControlCaption", "[A0SecurityControl].[ControlCaption]", "ControlCaption", DataType.String, Nothing, False, 0, True, 200, "Control Caption"))
    oList.Columns.Add(New ListColumn("AssemblyName", "[A0SecurityControl].[AssemblyName]", "AssemblyName", DataType.String, Nothing, True, 0, False, 100, "Assembly Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SecurityControl]  [A0SecurityControl]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SecurityControl")
    oList.Columns.Add(New ListColumn("ControlName", "[A0SecurityControl].[ControlName]", "ControlName", DataType.String, Nothing, True, 1, True, 200, "Control Name"))
    oList.Columns.Add(New ListColumn("FormName", "[A0SecurityControl].[FormName]", "FormName", DataType.String, Nothing, True, 0, False, 100, "Form Name"))
    oList.Columns.Add(New ListColumn("ControlCaption", "[A0SecurityControl].[ControlCaption]", "ControlCaption", DataType.String, Nothing, False, 0, True, 200, "Control Caption"))
    oList.Columns.Add(New ListColumn("AssemblyName", "[A0SecurityControl].[AssemblyName]", "AssemblyName", DataType.String, Nothing, True, 0, False, 100, "Assembly Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SecurityControl]  [A0SecurityControl]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
