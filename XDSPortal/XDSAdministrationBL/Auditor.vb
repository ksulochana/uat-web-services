Imports sembleWare.Runtime
Imports System
Public Class Auditor 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly AuditorID As New IdentityElement("AuditorID", Me, True, Nothing, Nothing)
  Public ReadOnly ProfessionNo As New StringElement("ProfessionNo", Me, False, False, True, True, False, 10, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AuditorName As New StringElement("AuditorName", Me, False, False, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly IsPossibleNameConflictYN As New BooleanElement("IsPossibleNameConflictYN", Me, False, False, True, True, True, "Is Possible Name Conflict?", Nothing, False, "Yes;No")
  Public ReadOnly IsPossibleDuplicateRecordYN As New BooleanElement("IsPossibleDuplicateRecordYN", Me, False, False, True, True, True, "Is Possible Duplicate Record?", Nothing, False, "Yes;No")
  Public ReadOnly RecordStatusInd As New IndicatorElement("RecordStatusInd", Me, False, False, False, True, True, True, "Record Status", Nothing, Auditor.RecordStatusIndOptions, "A")
  Public ReadOnly DeletedReason As New StringElement("DeletedReason", Me, False, True, True, True, False, 500, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LastUpdatedDate As New DateTimeElement("LastUpdatedDate", Me, False, False, True, True, False, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly AuditorLabelDescription As New StringElement("AuditorLabelDescription", Me, False, True, True, False, False, 250, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly AuditorAddress_OwnMany As Relationship = New AuditorAddressRelationship("AuditorAddress", Nothing, "Auditor", Me)
  Public ReadOnly AuditorNames_OwnMany As Relationship = New AuditorNameRelationship("AuditorNames", Nothing, "Auditor", Me)
  Public ReadOnly AuditorNameSoundEx_OwnMany As Relationship = New AuditorNameSoundExRelationship("AuditorNameSoundEx", Nothing, "Auditor", Me)
  Public ReadOnly AuditorConflict_OwnMany As Relationship = New AuditorConflictRelationship("AuditorConflict", Nothing, "Auditor", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  Public ReadOnly _AuditorAddress_OwnMany As AuditorAddressRelationship = AuditorAddress_OwnMany
  Public ReadOnly _AuditorNames_OwnMany As AuditorNameRelationship = AuditorNames_OwnMany
  Public ReadOnly _AuditorNameSoundEx_OwnMany As AuditorNameSoundExRelationship = AuditorNameSoundEx_OwnMany
  Public ReadOnly _AuditorConflict_OwnMany As AuditorConflictRelationship = AuditorConflict_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moRecordStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property RecordStatusIndOptions() As IndicatorOptions
    Get
      If moRecordStatusIndOptions Is Nothing Then
        moRecordStatusIndOptions = New IndicatorOptions
        moRecordStatusIndOptions.Add("A", "Active")
        moRecordStatusIndOptions.Add("D", "Deleted")
      End If
      Return moRecordStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("Auditor", ApplicationSettings)
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreate()
  End Sub

  Public Overrides Sub Load()
    MyBase.Load()
    BRLoad()
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreate()
    Me.LastUpdatedDate.Value = System.DateTime.Now
    BRAuditorLabelDescription()
  End Sub

  Private Sub BRLoad()
    BRAuditorLabelDescription()
  End Sub

  Private Sub BRAuditorLabelDescription()
    If Me.IsNew Then
      Me.AuditorLabelDescription.Value = "Auditor: (New)"
    Else
      Dim sDesc As String = Trim(Me.AuditorName.Value)
      Me.AuditorLabelDescription.Value = "Auditor: " & Me.AuditorID.Value & " (" & sDesc & ")"
    End If
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class AuditorRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _AuditorID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New Auditor(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _AuditorID.ForeignElement = CType(Instance, Auditor).AuditorID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0Auditor")
    oList.Columns.Add(New ListColumn("AuditorID", "[A0Auditor].[AuditorID]", "AuditorID", DataType.Long, Nothing, True, 1, True, 100, "Auditor ID"))
    oList.Columns.Add(New ListColumn("ProfessionNo", "[A0Auditor].[ProfessionNo]", "ProfessionNo", DataType.String, Nothing, False, 0, True, 100, "Profession No"))
    oList.Columns.Add(New ListColumn("AuditorName", "[A0Auditor].[AuditorName]", "AuditorName", DataType.String, Nothing, False, 0, True, 200, "Auditor Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "[Auditor]  [A0Auditor]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0Auditor")
    oList.Columns.Add(New ListColumn("AuditorID", "[A0Auditor].[AuditorID]", "AuditorID", DataType.Long, Nothing, True, 0, False, 100, "Auditor ID"))
    oList.Columns.Add(New ListColumn("ProfessionNo", "[A0Auditor].[ProfessionNo]", "ProfessionNo", DataType.String, Nothing, False, 0, True, 100, "Profession No"))
    oList.Columns.Add(New ListColumn("AuditorName", "[A0Auditor].[AuditorName]", "AuditorName", DataType.String, Nothing, False, 0, True, 200, "Auditor Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "[Auditor]  [A0Auditor]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ConflictForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0Auditor")
    oList.Columns.Add(New ListColumn("AuditorID", "[A0Auditor].[AuditorID]", "AuditorID", DataType.Long, Nothing, True, 0, True, 100, "Auditor ID"))
    oList.Columns.Add(New ListColumn("ProfessionNo", "[A0Auditor].[ProfessionNo]", "ProfessionNo", DataType.String, Nothing, False, 0, True, 100, "Profession No"))
    oList.Columns.Add(New ListColumn("AuditorName", "[A0Auditor].[AuditorName]", "AuditorName", DataType.String, Nothing, False, 0, True, 200, "Auditor Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "[Auditor]  [A0Auditor]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ConflictGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0Auditor")
    oList.Columns.Add(New ListColumn("AuditorID", "[A0Auditor].[AuditorID]", "AuditorID", DataType.Long, Nothing, True, 1, True, 100, "Auditor ID"))
    oList.Columns.Add(New ListColumn("ProfessionNo", "[A0Auditor].[ProfessionNo]", "ProfessionNo", DataType.String, Nothing, False, 0, True, 100, "Profession No"))
    oList.Columns.Add(New ListColumn("AuditorName", "[A0Auditor].[AuditorName]", "AuditorName", DataType.String, Nothing, False, 0, True, 200, "Auditor Name"))
    oList.Columns.Add(New ListColumn("IsPossibleNameConflictYN", "[A0Auditor].[IsPossibleNameConflictYN]", "IsPossibleNameConflictYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Is Possible Name Conflict?"))
    oList.Columns.Add(New ListColumn("IsPossibleDuplicateRecordYN", "[A0Auditor].[IsPossibleDuplicateRecordYN]", "IsPossibleDuplicateRecordYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Is Possible Duplicate Record?"))
    oList.SelectStatement = "select"
    oList.FromClause = "[Auditor]  [A0Auditor]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
