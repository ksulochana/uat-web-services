Imports sembleWare.Runtime
Imports System
Public Class SPSubscriberProfileProductAuthenticationQuestion 'sembleWare: Part
  Inherits CustomMemoryPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly Question_SP As New StringElement("Question_SP", Me, False, True, True, True, False, 250, "Question", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly QuestionTypeInd_SP As New IndicatorElement("QuestionTypeInd_SP", Me, False, False, True, True, True, False, "Question Type", Nothing, SPSubscriberProfileProductAuthenticationQuestion.QuestionTypeInd_SPOptions, "All")
  Public ReadOnly PriorityTypeInd_SP As New IndicatorElement("PriorityTypeInd_SP", Me, False, False, True, True, True, False, "Priority Type", Nothing, SPSubscriberProfileProductAuthenticationQuestion.PriorityTypeInd_SPOptions, Nothing)
  Public ReadOnly ProductAuthenticationQuestionGroup_SP As Relationship = New ProductAuthenticationQuestionGroupRelationship("ProductAuthenticationQuestionGroup_SP", "Question Group", RelationshipType.Include, False, True, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _ProductAuthenticationQuestionGroupCode As New StringElement("ProductAuthenticationQuestionGroupCode", Me, False)
  Public ReadOnly _ProductAuthenticationQuestionGroup_SP As ProductAuthenticationQuestionGroupRelationship = ProductAuthenticationQuestionGroup_SP
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moQuestionTypeInd_SPOptions As IndicatorOptions
  Public Shared ReadOnly Property QuestionTypeInd_SPOptions() As IndicatorOptions
    Get
      If moQuestionTypeInd_SPOptions Is Nothing Then
        moQuestionTypeInd_SPOptions = New IndicatorOptions
        moQuestionTypeInd_SPOptions.Add("P", "Positive")
        moQuestionTypeInd_SPOptions.Add("N", "Negative")
        moQuestionTypeInd_SPOptions.Add("All", "All")
      End If
      Return moQuestionTypeInd_SPOptions
    End Get
  End Property
  Private Shared moPriorityTypeInd_SPOptions As IndicatorOptions
  Public Shared ReadOnly Property PriorityTypeInd_SPOptions() As IndicatorOptions
    Get
      If moPriorityTypeInd_SPOptions Is Nothing Then
        moPriorityTypeInd_SPOptions = New IndicatorOptions
        moPriorityTypeInd_SPOptions.Add("H", "High")
        moPriorityTypeInd_SPOptions.Add("M", "Medium")
        moPriorityTypeInd_SPOptions.Add("L", "Low")
        moPriorityTypeInd_SPOptions.Add("All", "All")
      End If
      Return moPriorityTypeInd_SPOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SPSubscriberProfileProductAuthenticationQuestion", ApplicationSettings)
    _ProductAuthenticationQuestionGroup_SP._ProductAuthenticationQuestionGroupCode.LocalElement = _ProductAuthenticationQuestionGroupCode
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class SPSubscriberProfileProductAuthenticationQuestionRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SPSubscriberProfileProductAuthenticationQuestion(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
