Imports sembleWare.Runtime
Imports System
Public Class SubscriberCreditGrantor 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly SubscriberCreditGrantorID As New IdentityElement("SubscriberCreditGrantorID", Me, True, Nothing, Nothing)
  Public ReadOnly EnquiryDate As New DateTimeElement("EnquiryDate", Me, False, False, True, True, False, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly ReferenceNo As New StringElement("ReferenceNo", Me, False, False, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ExternalReference As New StringElement("ExternalReference", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ReportFile As New BinaryElement("ReportFile", Me, False, True, True, False, Nothing, Nothing)
  Public ReadOnly ReportFileMimeType As New StringElement("ReportFileMimeType", Me, False, False, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ExcludeFromCreditEnquiryYN As New BooleanElement("ExcludeFromCreditEnquiryYN", Me, False, True, True, True, False, Nothing, Nothing, Nothing)
  Public ReadOnly Product As Relationship = New ProductRelationship("Product", Nothing, RelationshipType.Include, True, False, True, Me)
  Public ReadOnly ActionedBySystemUser As Relationship = New SystemUserRelationship("ActionedBySystemUser", "Actioned By", RelationshipType.Include, True, False, True, Me)
  Public ReadOnly SubscriberConsumerEnquiry As Relationship = New SubscriberConsumerEnquiryRelationship("SubscriberConsumerEnquiry", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly ResultSubscriberCreditGrantor As Relationship = New SubscriberCreditGrantorRelationship("ResultSubscriberCreditGrantor", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly CreditGrantorEnquiryReason As Relationship = New CreditGrantorEnquiryReasonRelationship("CreditGrantorEnquiryReason", Nothing, RelationshipType.Include, True, True, True, Me)
  Public ReadOnly SubscriberBillingGroup As Relationship = New SubscriberBillingGroupRelationship("SubscriberBillingGroup", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly Consumer As Relationship = New ConsumerRelationship("Consumer", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly HomeAffairs As Relationship = New HomeAffairsRelationship("HomeAffairs", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly SubscriberHomeAffairsEnquiry As Relationship = New SubscriberHomeAffairsEnquiryRelationship("SubscriberHomeAffairsEnquiry", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly SubscriberCreditGrantorEnquiry As Relationship = New SubscriberCreditGrantorEnquiryRelationship("SubscriberCreditGrantorEnquiry", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Director As Relationship = New DirectorRelationship("Director", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly SubscriberDirectorEnquiry As Relationship = New SubscriberDirectorEnquiryRelationship("SubscriberDirectorEnquiry", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly SubscriberSAFPSSubjectEnquiry As Relationship = New SubscriberSAFPSSubjectEnquiryRelationship("SubscriberSAFPSSubjectEnquiry", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly SubscriberCreditGrantorInfo_OwnMany As Relationship = New SubscriberCreditGrantorInfoRelationship("SubscriberCreditGrantorInfo", Nothing, "SubscriberCreditGrantor", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _ProductID As New IntegerElement("ProductID", Me, False)
  Public ReadOnly _ActionedBySystemUserID As New IntegerElement("ActionedBySystemUserID", Me, False)
  Public ReadOnly _SubscriberConsumerEnquiryID As New IntegerElement("SubscriberConsumerEnquiryID", Me, False)
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _ResultSubscriberCreditGrantorID As New IntegerElement("ResultSubscriberCreditGrantorID", Me, False)
  Public ReadOnly _ResultSubscriberID As New IntegerElement("ResultSubscriberID", Me, False)
  Public ReadOnly _CreditGrantorEnquiryReasonCode As New StringElement("CreditGrantorEnquiryReasonCode", Me, False)
  Public ReadOnly _SubscriberBillingGroupCode As New StringElement("SubscriberBillingGroupCode", Me, False)
  Public ReadOnly _ConsumerID As New IntegerElement("ConsumerID", Me, False)
  Public ReadOnly _HomeAffairsID As New IntegerElement("HomeAffairsID", Me, False)
  Public ReadOnly _SubscriberHomeAffairsEnquiryID As New IntegerElement("SubscriberHomeAffairsEnquiryID", Me, False)
  Public ReadOnly _SubscriberCreditGrantorEnquiryID As New IntegerElement("SubscriberCreditGrantorEnquiryID", Me, False)
  Public ReadOnly _DirectorID As New IntegerElement("DirectorID", Me, False)
  Public ReadOnly _SubscriberDirectorEnquiryID As New IntegerElement("SubscriberDirectorEnquiryID", Me, False)
  Public ReadOnly _SubscriberSAFPSSubjectEnquiryID As New IntegerElement("SubscriberSAFPSSubjectEnquiryID", Me, False)
  Public ReadOnly _Product As ProductRelationship = Product
  Public ReadOnly _ActionedBySystemUser As SystemUserRelationship = ActionedBySystemUser
  Public ReadOnly _SubscriberConsumerEnquiry As SubscriberConsumerEnquiryRelationship = SubscriberConsumerEnquiry
  Public ReadOnly _ResultSubscriberCreditGrantor As SubscriberCreditGrantorRelationship = ResultSubscriberCreditGrantor
  Public ReadOnly _CreditGrantorEnquiryReason As CreditGrantorEnquiryReasonRelationship = CreditGrantorEnquiryReason
  Public ReadOnly _SubscriberBillingGroup As SubscriberBillingGroupRelationship = SubscriberBillingGroup
  Public ReadOnly _Consumer As ConsumerRelationship = Consumer
  Public ReadOnly _HomeAffairs As HomeAffairsRelationship = HomeAffairs
  Public ReadOnly _SubscriberHomeAffairsEnquiry As SubscriberHomeAffairsEnquiryRelationship = SubscriberHomeAffairsEnquiry
  Public ReadOnly _SubscriberCreditGrantorEnquiry As SubscriberCreditGrantorEnquiryRelationship = SubscriberCreditGrantorEnquiry
  Public ReadOnly _Director As DirectorRelationship = Director
  Public ReadOnly _SubscriberDirectorEnquiry As SubscriberDirectorEnquiryRelationship = SubscriberDirectorEnquiry
  Public ReadOnly _SubscriberSAFPSSubjectEnquiry As SubscriberSAFPSSubjectEnquiryRelationship = SubscriberSAFPSSubjectEnquiry
  Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
  Public ReadOnly _SubscriberCreditGrantorInfo_OwnMany As SubscriberCreditGrantorInfoRelationship = SubscriberCreditGrantorInfo_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberCreditGrantor", ApplicationSettings)
    _Product._ProductID.LocalElement = _ProductID
    _ActionedBySystemUser._SystemUserID.LocalElement = _ActionedBySystemUserID
    _SubscriberConsumerEnquiry._SubscriberConsumerEnquiryID.LocalElement = _SubscriberConsumerEnquiryID
    _SubscriberConsumerEnquiry._SubscriberID.LocalElement = _SubscriberID
    _ResultSubscriberCreditGrantor._SubscriberCreditGrantorID.LocalElement = _ResultSubscriberCreditGrantorID
    _ResultSubscriberCreditGrantor._SubscriberID.LocalElement = _ResultSubscriberID
    _CreditGrantorEnquiryReason._CreditGrantorEnquiryReasonCode.LocalElement = _CreditGrantorEnquiryReasonCode
    _SubscriberBillingGroup._SubscriberBillingGroupCode.LocalElement = _SubscriberBillingGroupCode
    _Consumer._ConsumerID.LocalElement = _ConsumerID
    _HomeAffairs._HomeAffairsID.LocalElement = _HomeAffairsID
    _SubscriberHomeAffairsEnquiry._SubscriberHomeAffairsEnquiryID.LocalElement = _SubscriberHomeAffairsEnquiryID
    _SubscriberHomeAffairsEnquiry._SubscriberID.LocalElement = _SubscriberID
    _SubscriberCreditGrantorEnquiry._SubscriberCreditGrantorEnquiryID.LocalElement = _SubscriberCreditGrantorEnquiryID
    _SubscriberCreditGrantorEnquiry._SubscriberID.LocalElement = _SubscriberID
    _Director._DirectorID.LocalElement = _DirectorID
    _SubscriberDirectorEnquiry._SubscriberDirectorEnquiryID.LocalElement = _SubscriberDirectorEnquiryID
    _SubscriberDirectorEnquiry._SubscriberID.LocalElement = _SubscriberID
    _SubscriberSAFPSSubjectEnquiry._SubscriberSAFPSSubjectEnquiryID.LocalElement = _SubscriberSAFPSSubjectEnquiryID
    _SubscriberSAFPSSubjectEnquiry._SubscriberID.LocalElement = _SubscriberID
    _Subscriber._SubscriberID.LocalElement = _SubscriberID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Save()
    With ApplicationSettings
      Try
        .BeginTransaction()
        Dim bIsNewYN As Boolean = Me.IsNew

        BRBeforeSave(bIsNewYN)
        MyBase.Save()
        BRAfterSave(bIsNewYN)
        .CommitTransaction()
      Catch Exception As Exception
        .RollbackTransaction()
        Throw Exception
      End Try
    End With
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRBeforeSave(ByVal IsNewYN As Boolean)
    If IsNewYN Then
      Me.EnquiryDate.Value = System.DateTime.Now

      Dim oSubscriberProfile As SubscriberProfile = SubscriberProfile.GetSubscriberProfile(Me._SubscriberID.Value, ApplicationSettings)
      Me.ExcludeFromCreditEnquiryYN.Value = oSubscriberProfile.ExcludeFromCreditEnquiryYN.Value

      ' If ActionedBySystemUser is not empty, then the XDSAdministration Services has populated this value.
      If ActionedBySystemUser.IsEmpty Then
        Me.ActionedBySystemUser.Instance = SystemUser.GetLoggedInSystemUser(ApplicationSettings)
      End If
    Else
      BRGetReferenceNo()
    End If
  End Sub

  Private Sub BRAfterSave(ByVal IsNewYN As Boolean)
    If IsNewYN Then
      BRGetReferenceNo()
      Me.Save()
    End If
  End Sub

  Private Sub BRGetReferenceNo()
    Select Case Me._ProductID.Value
      Case Constants.Product.Records.CreditGrantorConsumerCreditEnquiry, Constants.Product.Records.CreditGrantorConsumerApplicationCreditEnquiry, Constants.Product.Records.CreditGrantorConsumerCreditEnquiryWebService
        Me.ReferenceNo.Value = "CG" & Me.SubscriberCreditGrantorID.Value.ToString() & "-" & Me._ConsumerID.Value.ToString()
      Case Constants.Product.Records.CreditGrantorReEnquiry, Constants.Product.Records.CreditGrantorDebtCollectorsEnquiry, Constants.Product.Records.CreditGrantorReEnquiryWebService, Constants.Product.Records.CreditGrantorDebtCollectorsEnquiryWebService
        Dim oSubscriberCreditGrantor As SubscriberCreditGrantor = Me.ResultSubscriberCreditGrantor.Instance

        Me.ReferenceNo.Value = "CG" & Me.SubscriberCreditGrantorID.Value.ToString() & "-" & oSubscriberCreditGrantor.SubscriberCreditGrantorID.Value.ToString()
    End Select
  End Sub

  Private Sub BRPerformHomeAffairsMatch()
    If Not Me.Consumer.IsEmpty And Me.HomeAffairs.IsEmpty Then
      Dim oConsumer As Consumer = Me.Consumer.Instance

      If Not oConsumer.IDNo.ValueAsObject = "" Then
        Dim oSubscriber As Subscriber = Me.Subscriber.Instance
        Dim oSubscriberHomeAffairsEnquiry As SubscriberHomeAffairsEnquiry = oSubscriber.SubscriberHomeAffairsEnquiry_OwnMany.NewInstance

        oSubscriberHomeAffairsEnquiry.SystemUser.Instance = Me.ActionedBySystemUser.Instance
        oSubscriberHomeAffairsEnquiry.Product.Instance = XDSAdministrationBL.Product.GetProduct(Constants.Product.Records.TraceConsumerIdentityVerification, ApplicationSettings)
        oSubscriberHomeAffairsEnquiry.IDNo.ValueAsObject = oConsumer.IDNo.ValueAsObject
        oSubscriberHomeAffairsEnquiry.Surname.ValueAsObject = oConsumer.Surname.ValueAsObject
        oSubscriberHomeAffairsEnquiry.FirstName.ValueAsObject = oConsumer.FirstName.ValueAsObject
        oSubscriberHomeAffairsEnquiry.SecondName.ValueAsObject = oConsumer.SecondName.ValueAsObject
        oSubscriberHomeAffairsEnquiry.BirthDate.ValueAsObject = oConsumer.BirthDate.ValueAsObject
        oSubscriberHomeAffairsEnquiry.Match()

        Me.SubscriberHomeAffairsEnquiry.Instance = oSubscriberHomeAffairsEnquiry
        Select Case oSubscriberHomeAffairsEnquiry.EnquiryResultInd.Value
          Case Constants.SubscriberHomeAffairsEnquiry.Results.RecordFound
            Me.HomeAffairs.Instance = oSubscriberHomeAffairsEnquiry.GetSubscriberHomeAffairsEnquiryResult().HomeAffairs.Instance
        End Select
      End If
    End If
  End Sub


  Private Sub BRPerformDirectorMatch()
    If Not Me.Consumer.IsEmpty Then
      Dim oConsumer As Consumer = Me.Consumer.Instance
      Dim oSubscriber As Subscriber = Me.Subscriber.Instance
      Dim oSubscriberDirectorEnquiry As SubscriberDirectorEnquiry = oSubscriber.SubscriberDirectorEnquiry_OwnMany.NewInstance

      oSubscriberDirectorEnquiry.Product.Instance = XDSAdministrationBL.Product.GetProduct(Constants.Product.Records.EnquiryCommercialDirector, ApplicationSettings)
      oSubscriberDirectorEnquiry.IDNo.ValueAsObject = oConsumer.IDNo.ValueAsObject
      oSubscriberDirectorEnquiry.Surname.ValueAsObject = oConsumer.Surname.ValueAsObject
      oSubscriberDirectorEnquiry.FirstName.ValueAsObject = oConsumer.FirstName.ValueAsObject
      oSubscriberDirectorEnquiry.SecondName.ValueAsObject = oConsumer.SecondName.ValueAsObject
      oSubscriberDirectorEnquiry.BirthDate.ValueAsObject = oConsumer.BirthDate.ValueAsObject
      oSubscriberDirectorEnquiry.Match()

      Me.SubscriberDirectorEnquiry.Instance = oSubscriberDirectorEnquiry
      Select Case oSubscriberDirectorEnquiry.EnquiryResultInd.Value
        Case Constants.SubscriberDirectorEnquiry.Results.RecordFound
          Me.Director.Instance = oSubscriberDirectorEnquiry.GetSubscriberDirectorEnquiryResult().Director.Instance
      End Select
    End If
  End Sub
#End Region

#Region " Public Methods"
  Public Sub UpdateSubscriberConsumerEnquiry(ByVal SubscriberConsumerEnquiry As SubscriberConsumerEnquiry)
    Select Case SubscriberConsumerEnquiry.EnquiryResultInd.Value
      Case Constants.SubscriberConsumerEnquiry.Results.RecordFound
        Dim oSystemUserProfile As SystemUserProfile = XDSAdministrationBL.SystemUserProfile.GetLoggedInSystemUserProfile(ApplicationSettings)

        If Not oSystemUserProfile.ConfirmPurchaseYN.Value Then
          Dim oSubscriberConsumerEnquiryResult As SubscriberConsumerEnquiryResult = SubscriberConsumerEnquiry.GetSubscriberConsumerEnquiryResult()
                    oSubscriberConsumerEnquiryResult.SelectRecord()

                    Me.Consumer.Instance = oSubscriberConsumerEnquiryResult.Consumer.Instance

          BRPerformHomeAffairsMatch()
          BRPerformDirectorMatch()
        End If
    End Select

    Me.Product.Instance = SubscriberConsumerEnquiry.Product.Instance
    Me.SubscriberConsumerEnquiry.Instance = SubscriberConsumerEnquiry
    Me.Save()
  End Sub

  Public Sub UpdateSubscriberConsumerEnquiry(ByVal SubscriberConsumerEnquiryResult As SubscriberConsumerEnquiryResult)
    Dim oSubscriberConsumerEnquiry As SubscriberConsumerEnquiry = SubscriberConsumerEnquiryResult.SubscriberConsumerEnquiry.Instance
    UpdateSubscriberConsumerEnquiry(oSubscriberConsumerEnquiry)
    Me.Consumer.Instance = SubscriberConsumerEnquiryResult.Consumer.Instance
    BRPerformHomeAffairsMatch()
    BRPerformDirectorMatch()
    Me.Save()
  End Sub

  Public Sub UpdateSubscriberHomeAffairsEnquiry(ByVal SubscriberHomeAffairsEnquiry As SubscriberHomeAffairsEnquiry)
    Select Case SubscriberHomeAffairsEnquiry.EnquiryResultInd.Value
      Case Constants.SubscriberHomeAffairsEnquiry.Results.RecordFound
        Dim oSystemUserProfile As SystemUserProfile = XDSAdministrationBL.SystemUserProfile.GetLoggedInSystemUserProfile(ApplicationSettings)

        If Not oSystemUserProfile.ConfirmPurchaseYN.Value Then
          Dim oSubscriberHomeAffairsEnquiryResult As SubscriberHomeAffairsEnquiryResult = SubscriberHomeAffairsEnquiry.GetSubscriberHomeAffairsEnquiryResult()
          oSubscriberHomeAffairsEnquiryResult.SelectRecord()
          Me.HomeAffairs.Instance = oSubscriberHomeAffairsEnquiryResult.HomeAffairs.Instance
        End If
    End Select

    Me.Product.Instance = SubscriberHomeAffairsEnquiry.Product.Instance
    Me.SubscriberHomeAffairsEnquiry.Instance = SubscriberHomeAffairsEnquiry
    Me.Save()
  End Sub

  Public Sub UpdateSubscriberHomeAffairsEnquiry(ByVal SubscriberHomeAffairsEnquiryResult As SubscriberHomeAffairsEnquiryResult)
    Dim oSubscriberHomeAffairsEnquiry As SubscriberHomeAffairsEnquiry = SubscriberHomeAffairsEnquiryResult.SubscriberHomeAffairsEnquiry.Instance
    UpdateSubscriberHomeAffairsEnquiry(oSubscriberHomeAffairsEnquiry)
    Me.HomeAffairs.Instance = SubscriberHomeAffairsEnquiryResult.HomeAffairs.Instance
    Me.Save()
  End Sub

  Public Sub UpdateSubscriberDirectorEnquiry(ByVal SubscriberDirectorEnquiry As SubscriberDirectorEnquiry)
    Select Case SubscriberDirectorEnquiry.EnquiryResultInd.Value
      Case Constants.SubscriberDirectorEnquiry.Results.RecordFound
        Dim oSystemUserProfile As SystemUserProfile = XDSAdministrationBL.SystemUserProfile.GetLoggedInSystemUserProfile(ApplicationSettings)

        If Not oSystemUserProfile.ConfirmPurchaseYN.Value Then
          Dim oSubscriberDirectorEnquiryResult As SubscriberDirectorEnquiryResult = SubscriberDirectorEnquiry.GetSubscriberDirectorEnquiryResult()
          oSubscriberDirectorEnquiryResult.SelectRecord()
          Me.Director.Instance = oSubscriberDirectorEnquiryResult.Director.Instance
        End If
    End Select

    Me.Product.Instance = SubscriberDirectorEnquiry.Product.Instance
    Me.SubscriberDirectorEnquiry.Instance = SubscriberDirectorEnquiry
    Me.Save()
  End Sub

  Public Sub UpdateSubscriberDirectorEnquiry(ByVal SubscriberDirectorEnquiryResult As SubscriberDirectorEnquiryResult)
    Dim oSubscriberDirectorEnquiry As SubscriberDirectorEnquiry = SubscriberDirectorEnquiryResult.SubscriberDirectorEnquiry.Instance
    UpdateSubscriberDirectorEnquiry(oSubscriberDirectorEnquiry)
    Me.Director.Instance = SubscriberDirectorEnquiryResult.Director.Instance
    Me.Save()
  End Sub

  Public Sub UpdateSubscriberCreditGrantorEnquiry(ByVal SubscriberCreditGrantorEnquiry As SubscriberCreditGrantorEnquiry)
    Select Case SubscriberCreditGrantorEnquiry.EnquiryResultInd.Value
      Case Constants.SubscriberCreditGrantorEnquiry.Results.RecordFound
        Dim oSystemUserProfile As SystemUserProfile = XDSAdministrationBL.SystemUserProfile.GetLoggedInSystemUserProfile(ApplicationSettings)

        If Not oSystemUserProfile.ConfirmPurchaseYN.Value Then
          Dim oSubscriberCreditGrantorEnquiryResult As SubscriberCreditGrantorEnquiryResult = SubscriberCreditGrantorEnquiry.GetSubscriberCreditGrantorEnquiryResult()
          oSubscriberCreditGrantorEnquiryResult.SelectRecord()
          Me.ResultSubscriberCreditGrantor.Instance = oSubscriberCreditGrantorEnquiryResult.SubscriberCreditGrantor.Instance
        End If
    End Select

    Me.Product.Instance = SubscriberCreditGrantorEnquiry.Product.Instance
    Me.SubscriberCreditGrantorEnquiry.Instance = SubscriberCreditGrantorEnquiry
    Me.Save()
  End Sub

  Public Sub UpdateSubscriberCreditGrantorEnquiry(ByVal SubscriberCreditGrantorEnquiryResult As SubscriberCreditGrantorEnquiryResult)
    Dim oSubscriberCreditGrantorEnquiry As SubscriberCreditGrantorEnquiry = SubscriberCreditGrantorEnquiryResult.SubscriberCreditGrantorEnquiry.Instance
    UpdateSubscriberCreditGrantorEnquiry(oSubscriberCreditGrantorEnquiry)
    Me.ResultSubscriberCreditGrantor.Instance = SubscriberCreditGrantorEnquiryResult.SubscriberCreditGrantor.Instance
    Me.Save()
  End Sub

  Public Sub GenerateReport(ByVal ReportImageUrl As String, ByVal ReportImagePath As String)
    Me.Save()

    Dim sMimeType As String
    Dim oProduct As Product = XDSAdministrationBL.Product.GetProduct(Me._ProductID.Value, ApplicationSettings)

    If Not oProduct Is Nothing Then
      Utility.BRCheckProductReport(oProduct)

      Dim oReportingServicesReport As ReportingServicesReport = Utility.BRGetProductReportingServicesReport(SubscriberProfile.GetSubscriberProfile(Me._SubscriberID.Value, ApplicationSettings), oProduct)
      Dim oReportCreditGrantorDetail As ReportCreditGrantorDetail = New ReportCreditGrantorDetailRelationship(ApplicationSettings).NewInstance()

      oReportCreditGrantorDetail.ReportingServicesReportFormat.Value = SystemUserProfile.GetSystemUserProfile(Me._ActionedBySystemUserID.Value, ApplicationSettings).DefaultTraceEnquiryReportTypeInd.Value
      oReportCreditGrantorDetail.ReportingServicesReport.Instance = oReportingServicesReport
      oReportCreditGrantorDetail.ReportImageUrl.Value = ReportImageUrl
      oReportCreditGrantorDetail.ReportImagePath.Value = ReportImagePath
      oReportCreditGrantorDetail.SubscriberID.Value = Me._SubscriberID.Value
      oReportCreditGrantorDetail.SubscriberCreditGrantorID.Value = Me.SubscriberCreditGrantorID.Value
      oReportCreditGrantorDetail.ConsumerID.Value = Me._ConsumerID.Value
      With oReportCreditGrantorDetail.ReportParameterCollection
        .Add("SubscriberID", oReportCreditGrantorDetail.SubscriberID.Value, oReportCreditGrantorDetail.SubscriberID.ElementType)
        .Add("SubscriberCreditGrantorID", oReportCreditGrantorDetail.SubscriberCreditGrantorID.Value, oReportCreditGrantorDetail.SubscriberCreditGrantorID.ElementType)
      End With
      Me.ReportFile.Value = oReportCreditGrantorDetail.RunReport(sMimeType)
      Me.ReportFileMimeType.Value = sMimeType
    End If
    Me.Save()
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class SubscriberCreditGrantorRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberCreditGrantorID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberCreditGrantor(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberCreditGrantorID.ForeignElement = CType(Instance, SubscriberCreditGrantor).SubscriberCreditGrantorID
    _SubscriberID.ForeignElement = CType(Instance, SubscriberCreditGrantor)._SubscriberID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberCreditGrantor")
    oList.Columns.Add(New ListColumn("SubscriberCreditGrantorID", "[A0SubscriberCreditGrantor].[SubscriberCreditGrantorID]", "SubscriberCreditGrantorID", DataType.Long, Nothing, True, 0, False, 100, "Subscriber Credit Grantor ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberCreditGrantor].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("ReferenceNo", "[A0SubscriberCreditGrantor].[ReferenceNo]", "ReferenceNo", DataType.String, Nothing, False, 0, True, 100, "Reference No"))
    oList.Columns.Add(New ListColumn("EnquiryDate", "[A0SubscriberCreditGrantor].[EnquiryDate]", "EnquiryDate", DataType.DateTime, "g", False, -1, True, 100, "Enquiry Date"))
    oList.Columns.Add(New ListColumn("ProductTypeDesc", "[A2ProductType].[ProductTypeDesc]", "ProductTypeDesc", DataType.String, Nothing, False, 0, True, 200, "Product Type Description"))
    oList.Columns.Add(New ListColumn("ProductDesc", "[A1Product].[ProductDesc]", "ProductDesc", DataType.String, Nothing, False, 0, True, 200, "Product Description"))
    oList.Columns.Add(New ListColumn("DefaultPointValue", "[A1Product].[DefaultPointValue]", "DefaultPointValue", DataType.Decimal, Nothing, False, 0, False, 100, "Default Point Value"))
    oList.Columns.Add(New ListColumn("ProductDetails", "[A1Product].[ProductDetails]", "ProductDetails", DataType.Text, Nothing, False, 0, False, 200, "Product Details"))
    oList.Columns.Add(New ListColumn("FullName", "isnull([A3ActionedBySystemUser].[FirstName], '') + ' ' + isnull([A3ActionedBySystemUser].[Surname], '')", "FullName", DataType.String, Nothing, False, 0, True, 200, "Full Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "(([SubscriberCreditGrantor]  [A0SubscriberCreditGrantor]" + _
           "    join ([Product]  [A1Product]" + _
           "    join [ProductType]  [A2ProductType] on [A1Product].[ProductTypeID] = [A2ProductType].[ProductTypeID]) on [A0SubscriberCreditGrantor].[ProductID] = [A1Product].[ProductID])" + _
           "    join [SystemUser]  [A3ActionedBySystemUser] on [A0SubscriberCreditGrantor].[ActionedBySystemUserID] = [A3ActionedBySystemUser].[SystemUserID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
