Imports sembleWare.Runtime
Imports System
Public Class SubscriberTraceConsumerAlert 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly SubscriberTraceConsumerAlertID As New IdentityElement("SubscriberTraceConsumerAlertID", Me, True, Nothing, Nothing)
  Public ReadOnly TraceAlertDate As New DateTimeElement("TraceAlertDate", Me, False, False, True, True, True, "Trace Alert Date", Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly TraceAlertStartDate As New DateTimeElement("TraceAlertStartDate", Me, False, False, True, True, False, "Start Date", Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly TraceAlertEndDate As New DateTimeElement("TraceAlertEndDate", Me, False, False, True, True, True, "End Date", Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly ReferenceNo As New StringElement("ReferenceNo", Me, False, False, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly StatusInd As New IndicatorElement("StatusInd", Me, False, False, False, True, True, True, "Status", Nothing, SubscriberTraceConsumerAlert.StatusIndOptions, "A")
  Public ReadOnly ProcessedDate As New DateTimeElement("ProcessedDate", Me, False, False, True, True, False, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly NotificationStatusInd As New IndicatorElement("NotificationStatusInd", Me, False, False, False, True, True, True, "Notification Status", Nothing, SubscriberTraceConsumerAlert.NotificationStatusIndOptions, "N")
  Public ReadOnly NotificationProcessedDate As New DateTimeElement("NotificationProcessedDate", Me, False, False, True, True, False, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly ErrorDetails As New TextElement("ErrorDetails", Me, False, True, True, False, Nothing, Nothing, Nothing)
  Public ReadOnly ActionedBySystemUser As Relationship = New SystemUserRelationship("ActionedBySystemUser", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly TraceAlertDuration As Relationship = New TraceAlertDurationRelationship("TraceAlertDuration", Nothing, RelationshipType.Include, True, False, True, Me)
  Public ReadOnly SubscriberTraceConsumer As Relationship = New SubscriberTraceConsumerRelationship("SubscriberTraceConsumer", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _ActionedBySystemUserID As New IntegerElement("ActionedBySystemUserID", Me, False)
  Public ReadOnly _TraceAlertDurationID As New IntegerElement("TraceAlertDurationID", Me, False)
  Public ReadOnly _SubscriberTraceConsumerID As New IntegerElement("SubscriberTraceConsumerID", Me, False)
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _ActionedBySystemUser As SystemUserRelationship = ActionedBySystemUser
  Public ReadOnly _TraceAlertDuration As TraceAlertDurationRelationship = TraceAlertDuration
  Public ReadOnly _SubscriberTraceConsumer As SubscriberTraceConsumerRelationship = SubscriberTraceConsumer
  Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property StatusIndOptions() As IndicatorOptions
    Get
      If moStatusIndOptions Is Nothing Then
        moStatusIndOptions = New IndicatorOptions
        moStatusIndOptions.Add("A", "Active")
        moStatusIndOptions.Add("C", "Completed")
        moStatusIndOptions.Add("P", "Processing")
        moStatusIndOptions.Add("E", "Expired")
        moStatusIndOptions.Add("F", "Failed")
        moStatusIndOptions.Add("X", "Cancelled")
      End If
      Return moStatusIndOptions
    End Get
  End Property
  Private Shared moNotificationStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property NotificationStatusIndOptions() As IndicatorOptions
    Get
      If moNotificationStatusIndOptions Is Nothing Then
        moNotificationStatusIndOptions = New IndicatorOptions
        moNotificationStatusIndOptions.Add("N", "Not Applicable")
        moNotificationStatusIndOptions.Add("Q", "Queued")
        moNotificationStatusIndOptions.Add("P", "Processing")
        moNotificationStatusIndOptions.Add("C", "Completed")
        moNotificationStatusIndOptions.Add("F", "Failed")
      End If
      Return moNotificationStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberTraceConsumerAlert", ApplicationSettings)
    _ActionedBySystemUser._SystemUserID.LocalElement = _ActionedBySystemUserID
    _TraceAlertDuration._TraceAlertDurationID.LocalElement = _TraceAlertDurationID
    _SubscriberTraceConsumer._SubscriberTraceConsumerID.LocalElement = _SubscriberTraceConsumerID
    _SubscriberTraceConsumer._SubscriberID.LocalElement = _SubscriberID
    _Subscriber._SubscriberID.LocalElement = _SubscriberID
    AddHandler TraceAlertDuration.Changed, AddressOf TraceAlertDuration_Changed
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub TraceAlertDuration_Changed(ByVal Relationship As Relationship)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"

  Public Sub Cancel()
    Select Case Me.StatusInd.Value
      Case Constants.SubscriberTraceConsumerAlert.Statuses.Active
        Me.StatusInd.Value = Constants.SubscriberTraceConsumerAlert.Statuses.Cancelled
        Me.Save()
      Case Else
        Throw New BusinessRuleException("Only Active Trace Alerts can be Cancelled!")
    End Select
  End Sub

#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreateLoad()
  End Sub

  Public Overrides Sub Load()
    MyBase.Load()
    BRCreateLoad()
  End Sub

  Public Overrides Sub Save()
    With ApplicationSettings
      Try
        .BeginTransaction()
        Dim bIsNewYN As Boolean = Me.IsNew

        BRBeforeSave(bIsNewYN)
        MyBase.Save()
        BRAfterSave(bIsNewYN)
        .CommitTransaction()
      Catch Exception As Exception
        .RollbackTransaction()
        Throw Exception
      End Try
    End With
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreateLoad()
    Me.TraceAlertDuration.Enabled = False
    Select Case StatusInd.Value
      Case Constants.SubscriberTraceAlert.Statuses.Active
        Me.TraceAlertDuration.Enabled = True
    End Select
  End Sub

  Private Sub BRBeforeSave(ByVal IsNewYN As Boolean)
    Dim oTraceAlertDuration As TraceAlertDuration = Me.TraceAlertDuration.Instance

    If IsNewYN Then
      Me.TraceAlertDate.Value = System.DateTime.Now
      Me.TraceAlertStartDate.Value = System.DateTime.Now
      Me.ActionedBySystemUser.Instance = SystemUser.GetLoggedInSystemUser(ApplicationSettings)
    End If
    Me.TraceAlertEndDate.Value = oTraceAlertDuration.AddDuration(Me.TraceAlertStartDate.Value)
  End Sub

  Private Sub BRAfterSave(ByVal IsNewYN As Boolean)
    If IsNewYN Then
      Me.ReferenceNo.Value = "TA" & Me.SubscriberTraceConsumerAlertID.Value.ToString() & "-" & Me._SubscriberTraceConsumerID.Value.ToString()
      Me.Save()
    End If
  End Sub

  Private Sub BRTraceAlertDuration(ByVal Relationship As Relationship, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If PerformRulesYN Then
      If Not Relationship.IsEmpty Then
        Dim oTraceAlertDuration As TraceAlertDuration = Relationship.Instance

        Me.TraceAlertEndDate.Value = oTraceAlertDuration.AddDuration(Me.TraceAlertStartDate.Value)
      End If
    End If
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class SubscriberTraceConsumerAlertRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberTraceConsumerAlertID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberTraceConsumerAlert(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberTraceConsumerAlertID.ForeignElement = CType(Instance, SubscriberTraceConsumerAlert).SubscriberTraceConsumerAlertID
    _SubscriberID.ForeignElement = CType(Instance, SubscriberTraceConsumerAlert)._SubscriberID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberTraceConsumerAlert")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberTraceConsumerAlert].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberTraceConsumerAlertID", "[A0SubscriberTraceConsumerAlert].[SubscriberTraceConsumerAlertID]", "SubscriberTraceConsumerAlertID", DataType.Long, Nothing, True, 0, False, 100, "Subscriber Trace Consumer Alert ID"))
    oList.Columns.Add(New ListColumn("ReferenceNo", "[A0SubscriberTraceConsumerAlert].[ReferenceNo]", "ReferenceNo", DataType.String, Nothing, False, 0, True, 100, "Reference No"))
    oList.Columns.Add(New ListColumn("TraceAlertDate", "[A0SubscriberTraceConsumerAlert].[TraceAlertDate]", "TraceAlertDate", DataType.DateTime, "g", False, -1, True, 100, "Trace Alert Date"))
    oList.Columns.Add(New ListColumn("TraceAlertStartDate", "[A0SubscriberTraceConsumerAlert].[TraceAlertStartDate]", "TraceAlertStartDate", DataType.DateTime, "d", False, 0, True, 100, "Start Date"))
    oList.Columns.Add(New ListColumn("TraceAlertEndDate", "[A0SubscriberTraceConsumerAlert].[TraceAlertEndDate]", "TraceAlertEndDate", DataType.DateTime, "d", False, 0, True, 100, "End Date"))
    oList.Columns.Add(New ListColumn("StatusInd", "[A0SubscriberTraceConsumerAlert].[StatusInd]", "StatusInd", DataType.String, Nothing, False, 0, True, 100, "Status"))
    oList.Columns.Add(New ListColumn("ProcessedDate", "[A0SubscriberTraceConsumerAlert].[ProcessedDate]", "ProcessedDate", DataType.DateTime, "g", False, 0, True, 100, "Processed Date"))
    oList.Columns.Add(New ListColumn("IDNo", "[A3SubscriberConsumerEnquiry].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("PassportNo", "[A3SubscriberConsumerEnquiry].[PassportNo]", "PassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No / Other ID No"))
    oList.Columns.Add(New ListColumn("Surname", "[A3SubscriberConsumerEnquiry].[Surname]", "Surname", DataType.String, Nothing, False, 0, True, 100, "Surname"))
    oList.Columns.Add(New ListColumn("FirstName", "[A3SubscriberConsumerEnquiry].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A3SubscriberConsumerEnquiry].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Date of Birth"))
    oList.Columns.Add(New ListColumn("FullName", "isnull([A1ActionedBySystemUser].[FirstName], '') + ' ' + isnull([A1ActionedBySystemUser].[Surname], '')", "FullName", DataType.String, Nothing, False, 0, True, 200, "Actioned By"))
    oList.Columns.Add(New ListColumn("SystemUserID", "[A3SubscriberConsumerEnquiry].[SystemUserID]", "SystemUserID", DataType.Integer, Nothing, False, 0, False, 100, "System User ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "(([SubscriberTraceConsumerAlert]  [A0SubscriberTraceConsumerAlert]" + _
           "    left join [SystemUser]  [A1ActionedBySystemUser] on [A0SubscriberTraceConsumerAlert].[ActionedBySystemUserID] = [A1ActionedBySystemUser].[SystemUserID])" + _
           "    left join ([SubscriberTraceConsumer]  [A2SubscriberTraceConsumer]" + _
           "    left join [SubscriberConsumerEnquiry]  [A3SubscriberConsumerEnquiry] on [A2SubscriberTraceConsumer].[SubscriberConsumerEnquiryID] = [A3SubscriberConsumerEnquiry].[SubscriberConsumerEnquiryID] and [A2SubscriberTraceConsumer].[SubscriberID] = [A3SubscriberConsumerEnquiry].[SubscriberID]) on [A0SubscriberTraceConsumerAlert].[SubscriberTraceConsumerID] = [A2SubscriberTraceConsumer].[SubscriberTraceConsumerID] and [A0SubscriberTraceConsumerAlert].[SubscriberID] = [A2SubscriberTraceConsumer].[SubscriberID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function SubscriberActiveGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberTraceConsumerAlert")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberTraceConsumerAlert].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberTraceConsumerAlertID", "[A0SubscriberTraceConsumerAlert].[SubscriberTraceConsumerAlertID]", "SubscriberTraceConsumerAlertID", DataType.Long, Nothing, True, 0, False, 100, "Subscriber Trace Alert ID"))
    oList.Columns.Add(New ListColumn("ReferenceNo", "[A0SubscriberTraceConsumerAlert].[ReferenceNo]", "ReferenceNo", DataType.String, Nothing, False, 0, True, 100, "Reference No"))
    oList.Columns.Add(New ListColumn("TraceAlertDate", "[A0SubscriberTraceConsumerAlert].[TraceAlertDate]", "TraceAlertDate", DataType.DateTime, "g", False, -1, False, 100, "Trace Alert Date"))
    oList.Columns.Add(New ListColumn("TraceAlertStartDate", "[A0SubscriberTraceConsumerAlert].[TraceAlertStartDate]", "TraceAlertStartDate", DataType.DateTime, "d", False, 0, True, 100, "Start Date"))
    oList.Columns.Add(New ListColumn("TraceAlertEndDate", "[A0SubscriberTraceConsumerAlert].[TraceAlertEndDate]", "TraceAlertEndDate", DataType.DateTime, "d", False, 0, True, 100, "End Date"))
    oList.Columns.Add(New ListColumn("StatusInd", "[A0SubscriberTraceConsumerAlert].[StatusInd]", "StatusInd", DataType.String, Nothing, False, 0, False, 100, "Status"))
    oList.Columns.Add(New ListColumn("IDNo", "[A2SubscriberConsumerEnquiry].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("PassportNo", "[A2SubscriberConsumerEnquiry].[PassportNo]", "PassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No / Other ID No"))
    oList.Columns.Add(New ListColumn("Surname", "[A2SubscriberConsumerEnquiry].[Surname]", "Surname", DataType.String, Nothing, False, 0, True, 100, "Surname"))
    oList.Columns.Add(New ListColumn("FirstName", "[A2SubscriberConsumerEnquiry].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A2SubscriberConsumerEnquiry].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Date of Birth"))
    oList.Columns.Add(New ListColumn("SystemUserID", "[A2SubscriberConsumerEnquiry].[SystemUserID]", "SystemUserID", DataType.Integer, Nothing, False, 0, True, 100, "System User ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "([SubscriberTraceConsumerAlert]  [A0SubscriberTraceConsumerAlert]" + _
           "    left join ([SubscriberTraceConsumer]  [A1SubscriberTraceConsumer]" + _
           "    left join [SubscriberConsumerEnquiry]  [A2SubscriberConsumerEnquiry] on [A1SubscriberTraceConsumer].[SubscriberConsumerEnquiryID] = [A2SubscriberConsumerEnquiry].[SubscriberConsumerEnquiryID] and [A1SubscriberTraceConsumer].[SubscriberID] = [A2SubscriberConsumerEnquiry].[SubscriberID]) on [A0SubscriberTraceConsumerAlert].[SubscriberTraceConsumerID] = [A1SubscriberTraceConsumer].[SubscriberTraceConsumerID] and [A0SubscriberTraceConsumerAlert].[SubscriberID] = [A1SubscriberTraceConsumer].[SubscriberID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function SubscriberCompletedGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberTraceConsumerAlert")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberTraceConsumerAlert].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberTraceConsumerAlertID", "[A0SubscriberTraceConsumerAlert].[SubscriberTraceConsumerAlertID]", "SubscriberTraceConsumerAlertID", DataType.Long, Nothing, True, 0, False, 100, "Subscriber Trace Alert ID"))
    oList.Columns.Add(New ListColumn("ReferenceNo", "[A0SubscriberTraceConsumerAlert].[ReferenceNo]", "ReferenceNo", DataType.String, Nothing, False, 0, True, 100, "Reference No"))
    oList.Columns.Add(New ListColumn("TraceAlertDate", "[A0SubscriberTraceConsumerAlert].[TraceAlertDate]", "TraceAlertDate", DataType.DateTime, "g", False, -1, False, 100, "Trace Alert Date"))
    oList.Columns.Add(New ListColumn("TraceAlertStartDate", "[A0SubscriberTraceConsumerAlert].[TraceAlertStartDate]", "TraceAlertStartDate", DataType.DateTime, "d", False, 0, True, 100, "Start Date"))
    oList.Columns.Add(New ListColumn("TraceAlertEndDate", "[A0SubscriberTraceConsumerAlert].[TraceAlertEndDate]", "TraceAlertEndDate", DataType.DateTime, "d", False, 0, True, 100, "End Date"))
    oList.Columns.Add(New ListColumn("StatusInd", "[A0SubscriberTraceConsumerAlert].[StatusInd]", "StatusInd", DataType.String, Nothing, False, 0, True, 100, "Status"))
    oList.Columns.Add(New ListColumn("ProcessedDate", "[A0SubscriberTraceConsumerAlert].[ProcessedDate]", "ProcessedDate", DataType.DateTime, "g", False, 0, True, 100, "Processed Date"))
    oList.Columns.Add(New ListColumn("IDNo", "[A2SubscriberConsumerEnquiry].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("PassportNo", "[A2SubscriberConsumerEnquiry].[PassportNo]", "PassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No / Other ID No"))
    oList.Columns.Add(New ListColumn("Surname", "[A2SubscriberConsumerEnquiry].[Surname]", "Surname", DataType.String, Nothing, False, 0, True, 100, "Surname"))
    oList.Columns.Add(New ListColumn("FirstName", "[A2SubscriberConsumerEnquiry].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A2SubscriberConsumerEnquiry].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Date of Birth"))
    oList.Columns.Add(New ListColumn("SystemUserID", "[A2SubscriberConsumerEnquiry].[SystemUserID]", "SystemUserID", DataType.Integer, Nothing, False, 0, True, 100, "System User ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "([SubscriberTraceConsumerAlert]  [A0SubscriberTraceConsumerAlert]" + _
           "    left join ([SubscriberTraceConsumer]  [A1SubscriberTraceConsumer]" + _
           "    left join [SubscriberConsumerEnquiry]  [A2SubscriberConsumerEnquiry] on [A1SubscriberTraceConsumer].[SubscriberConsumerEnquiryID] = [A2SubscriberConsumerEnquiry].[SubscriberConsumerEnquiryID] and [A1SubscriberTraceConsumer].[SubscriberID] = [A2SubscriberConsumerEnquiry].[SubscriberID]) on [A0SubscriberTraceConsumerAlert].[SubscriberTraceConsumerID] = [A1SubscriberTraceConsumer].[SubscriberTraceConsumerID] and [A0SubscriberTraceConsumerAlert].[SubscriberID] = [A1SubscriberTraceConsumer].[SubscriberID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function SubscriberGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberTraceConsumerAlert")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberTraceConsumerAlert].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberTraceConsumerAlertID", "[A0SubscriberTraceConsumerAlert].[SubscriberTraceConsumerAlertID]", "SubscriberTraceConsumerAlertID", DataType.Long, Nothing, True, 0, False, 100, "Subscriber Trace Consumer Alert ID"))
    oList.Columns.Add(New ListColumn("ReferenceNo", "[A0SubscriberTraceConsumerAlert].[ReferenceNo]", "ReferenceNo", DataType.String, Nothing, False, 0, True, 100, "Reference No"))
    oList.Columns.Add(New ListColumn("TraceAlertDate", "[A0SubscriberTraceConsumerAlert].[TraceAlertDate]", "TraceAlertDate", DataType.DateTime, "g", False, -1, True, 100, "Trace Alert Date"))
    oList.Columns.Add(New ListColumn("TraceAlertStartDate", "[A0SubscriberTraceConsumerAlert].[TraceAlertStartDate]", "TraceAlertStartDate", DataType.DateTime, "d", False, 0, True, 100, "Start Date"))
    oList.Columns.Add(New ListColumn("TraceAlertEndDate", "[A0SubscriberTraceConsumerAlert].[TraceAlertEndDate]", "TraceAlertEndDate", DataType.DateTime, "d", False, 0, True, 100, "End Date"))
    oList.Columns.Add(New ListColumn("StatusInd", "[A0SubscriberTraceConsumerAlert].[StatusInd]", "StatusInd", DataType.String, Nothing, False, 0, True, 100, "Status"))
    oList.Columns.Add(New ListColumn("ProcessedDate", "[A0SubscriberTraceConsumerAlert].[ProcessedDate]", "ProcessedDate", DataType.DateTime, "g", False, 0, True, 100, "Processed Date"))
    oList.Columns.Add(New ListColumn("IDNo", "[A3SubscriberConsumerEnquiry].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("PassportNo", "[A3SubscriberConsumerEnquiry].[PassportNo]", "PassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No / Other ID No"))
    oList.Columns.Add(New ListColumn("Surname", "[A3SubscriberConsumerEnquiry].[Surname]", "Surname", DataType.String, Nothing, False, 0, True, 100, "Surname"))
    oList.Columns.Add(New ListColumn("FirstName", "[A3SubscriberConsumerEnquiry].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A3SubscriberConsumerEnquiry].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Date of Birth"))
    oList.Columns.Add(New ListColumn("FullName", "isnull([A1ActionedBySystemUser].[FirstName], '') + ' ' + isnull([A1ActionedBySystemUser].[Surname], '')", "FullName", DataType.String, Nothing, False, 0, True, 200, "Actioned By"))
    oList.Columns.Add(New ListColumn("SystemUserID", "[A3SubscriberConsumerEnquiry].[SystemUserID]", "SystemUserID", DataType.Integer, Nothing, False, 0, True, 100, "System User ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "(([SubscriberTraceConsumerAlert]  [A0SubscriberTraceConsumerAlert]" + _
           "    left join [SystemUser]  [A1ActionedBySystemUser] on [A0SubscriberTraceConsumerAlert].[ActionedBySystemUserID] = [A1ActionedBySystemUser].[SystemUserID])" + _
           "    left join ([SubscriberTraceConsumer]  [A2SubscriberTraceConsumer]" + _
           "    left join [SubscriberConsumerEnquiry]  [A3SubscriberConsumerEnquiry] on [A2SubscriberTraceConsumer].[SubscriberConsumerEnquiryID] = [A3SubscriberConsumerEnquiry].[SubscriberConsumerEnquiryID] and [A2SubscriberTraceConsumer].[SubscriberID] = [A3SubscriberConsumerEnquiry].[SubscriberID]) on [A0SubscriberTraceConsumerAlert].[SubscriberTraceConsumerID] = [A2SubscriberTraceConsumer].[SubscriberTraceConsumerID] and [A0SubscriberTraceConsumerAlert].[SubscriberID] = [A2SubscriberTraceConsumer].[SubscriberID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
