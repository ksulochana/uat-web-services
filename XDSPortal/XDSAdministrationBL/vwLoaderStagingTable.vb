Imports sembleWare.Runtime
Imports System
Public Class vwLoaderStagingTable 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly RecordID As New IdentityElement("RecordID", Me, True, Nothing, Nothing)
  Public ReadOnly Loader As Relationship = New LoaderRelationship("Loader", Nothing, RelationshipType.Include, True, False, True, Me)
  Public ReadOnly Consumer As Relationship = New ConsumerRelationship("Consumer", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly MetaDatabase As Relationship = New MetaDatabaseRelationship("MetaDatabase", Nothing, RelationshipType.Include, False, False, False, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly vwLoaderStagingConsumerTable_OwnMany As Relationship = New vwLoaderStagingConsumerTableRelationship("vwLoaderStagingConsumerTable", Nothing, "vwLoaderStagingTable", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _LoaderID As New IntegerElement("LoaderID", Me, False)
  Public ReadOnly _ConsumerID As New IntegerElement("ConsumerID", Me, False)
  Public ReadOnly _MetaDatabaseCode As New StringElement("MetaDatabaseCode", Me, False)
  Public ReadOnly _Loader As LoaderRelationship = Loader
  Public ReadOnly _Consumer As ConsumerRelationship = Consumer
  Public ReadOnly _MetaDatabase As MetaDatabaseRelationship = MetaDatabase
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  Public ReadOnly _vwLoaderStagingConsumerTable_OwnMany As vwLoaderStagingConsumerTableRelationship = vwLoaderStagingConsumerTable_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("vwLoaderStagingTable", ApplicationSettings)
    _Loader._LoaderID.LocalElement = _LoaderID
    _Consumer._ConsumerID.LocalElement = _ConsumerID
    _MetaDatabase._MetaDatabaseCode.LocalElement = _MetaDatabaseCode
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Load()
    Me.CreatedByUser.Persist = False
    Me.CreatedOnDate.Persist = False
    Me.ChangedByUser.Persist = False
    Me.ChangedOnDate.Persist = False

    Dim oSubscriber As Subscriber = New SubscriberRelationship(ApplicationSettings).NewInstance()
    If ApplicationSettings.Settings.Contains(Constants.vwLoaderStagingTable.LoaderStagingTable_SubscriberID) Then
      oSubscriber.SubscriberID.Load(ApplicationSettings.Settings.Item(Constants.vwLoaderStagingTable.LoaderStagingTable_SubscriberID))
      oSubscriber.Load()
    End If
    Dim oMetaDatabase As MetaDatabase = New MetaDatabaseRelationship(ApplicationSettings).NewInstance()
    If ApplicationSettings.Settings.Contains(Constants.vwLoaderStagingTable.LoaderStagingTable_MetaDatabaseCode) Then
      oMetaDatabase.MetaDatabaseCode.Load(ApplicationSettings.Settings.Item(Constants.vwLoaderStagingTable.LoaderStagingTable_MetaDatabaseCode))
      oMetaDatabase.Load()
      Me.MetaDatabase.Instance = oMetaDatabase
    End If
    Dim oFileFormatDeployment As FileFormatDeployment = New FileFormatDeploymentRelationship(ApplicationSettings).NewInstance()
    If ApplicationSettings.Settings.Contains(Constants.vwLoaderStagingTable.LoaderStagingTable_FileFormatDeploymentID) Then
      oFileFormatDeployment.FileFormatDeploymentID.Load(System.Convert.ToInt64(ApplicationSettings.Settings.Item(Constants.vwLoaderStagingTable.LoaderStagingTable_FileFormatDeploymentID)))
      oFileFormatDeployment.Load()
    End If

    Dim oLoader As Loader = Me.Loader.Instance
    Dim oFileFormat As FileFormat = oLoader.FileFormat.Instance
    Dim oLoaderEngineManager As LoaderEngineManager.Manager = New LoaderEngineManager.Manager(oFileFormatDeployment, oFileFormat, ApplicationSettings)

    Me.TableName = oLoaderEngineManager.StagingTableName(Me.MetaDatabase.Instance)

    MyBase.Load()
  End Sub
#End Region

#Region " Methods"
  Public Function DefineLoaderStagingConsumerGridList(ByVal MetaDatabase As MetaDatabase) As sembleWare.Runtime.List
    Dim oXDSAdministrationRoot As XDSAdministrationRoot = Me.XDSAdministrationRoot.Instance
    Dim oDBList As DBList = Me._vwLoaderStagingConsumerTable_OwnMany.GridList
    Dim oLoader As Loader = Me.Loader.Instance
    Dim oSubscriber As Subscriber = oLoader.Subscriber.Instance
    Dim oFileFormat As FileFormat = oLoader.FileFormat.Instance
    Dim oLoaderEngineManager As LoaderEngineManager.Manager = New LoaderEngineManager.Manager(oLoader.FileFormatDeployment.Instance, oFileFormat, ApplicationSettings)

    oDBList.FromClause = oDBList.FromClause.Replace("[vwLoaderStagingConsumerTable]", "[" & oLoaderEngineManager.StagingMultipleMatchTableName(Me.MetaDatabase.Instance) & "]")

    Return oDBList
  End Function
#End Region

End Class 'sembleWare: Part

Public Class vwLoaderStagingTableRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _RecordID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New vwLoaderStagingTable(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _RecordID.ForeignElement = CType(Instance, vwLoaderStagingTable).RecordID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0vwLoaderStagingTable")
    oList.Columns.Add(New ListColumn("RecordID", "[A0vwLoaderStagingTable].[RecordID]", "RecordID", DataType.Long, Nothing, True, 2, True, 100, "Record ID"))
    oList.Columns.Add(New ListColumn("LoaderID", "[A0vwLoaderStagingTable].[LoaderID]", "LoaderID", DataType.Integer, Nothing, False, 1, False, 100, "Loader ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "[vwLoaderStagingTable]  [A0vwLoaderStagingTable]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
