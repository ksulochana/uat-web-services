Imports sembleWare.Runtime
Imports System
Public Class LoaderProcessHistory 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly LoaderProcessHistoryID As New IdentityElement("LoaderProcessHistoryID", Me, True, Nothing, Nothing)
  Public ReadOnly ActionedByUser As New StringElement("ActionedByUser", Me, False, False, True, True, True, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ActionedOnDate As New DateTimeElement("ActionedOnDate", Me, False, False, True, True, True, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly ActionedDetail As New StringElement("ActionedDetail", Me, False, False, True, True, True, 250, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ErrorMessage As New TextElement("ErrorMessage", Me, True, True, True, False, Nothing, Nothing, Nothing)
  Public ReadOnly Loader As Relationship = New LoaderRelationship("Loader", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly LoaderProcess As Relationship = New LoaderProcessRelationship("LoaderProcess", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly LoaderProcessType As Relationship = New LoaderProcessTypeRelationship("LoaderProcessType", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _LoaderID As New IntegerElement("LoaderID", Me, True)
  Public ReadOnly _LoaderProcessTypeCode As New StringElement("LoaderProcessTypeCode", Me, True)
  Public ReadOnly _Loader As LoaderRelationship = Loader
  Public ReadOnly _LoaderProcess As LoaderProcessRelationship = LoaderProcess
  Public ReadOnly _LoaderProcessType As LoaderProcessTypeRelationship = LoaderProcessType
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("LoaderProcessHistory", ApplicationSettings)
    _Loader._LoaderID.LocalElement = _LoaderID
    _LoaderProcess._LoaderID.LocalElement = _LoaderID
    _LoaderProcess._LoaderProcessTypeCode.LocalElement = _LoaderProcessTypeCode
    _LoaderProcessType._LoaderProcessTypeCode.LocalElement = _LoaderProcessTypeCode
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Shared Procedures"
  Public Shared Sub AddLoaderProcessHistory(ByVal LoaderID As Long, ByVal LoaderProcessTypeCode As String, ByVal StatusInd As String, ByVal MetaDatabase As MetaDatabase, ByVal Exception As Exception, ByVal ActionedByUser As String, ByVal ApplicationSettings As ApplicationSettings)
    Dim sActionDetail As String = ""
    Dim sMetaDatabaseDetails As String = ""

    If Not MetaDatabase Is Nothing Then
      sMetaDatabaseDetails = "for " & MetaDatabase.MetaDatabaseCode.Value & " - " & MetaDatabase.MetaDatabaseDesc.Value
    End If

    Select Case StatusInd
      Case Constants.LoaderProcess.Statuses.Processing
        If MetaDatabase Is Nothing Then
          sActionDetail += "Processing Started"
        Else
          sActionDetail += "Processing Started " & sMetaDatabaseDetails
        End If
      Case Constants.LoaderProcess.Statuses.Completed
        If MetaDatabase Is Nothing Then
          sActionDetail += "Processing Completed"
        Else
          sActionDetail += "Processing Completed " & sMetaDatabaseDetails
        End If
      Case Constants.LoaderProcess.Statuses.Failed
        If MetaDatabase Is Nothing Then
          sActionDetail += "Processing Failed"
        Else
          sActionDetail += "Processing Failed " & sMetaDatabaseDetails
        End If
    End Select

    Dim oLoaderProcessHistory As LoaderProcessHistory = New LoaderProcessHistoryRelationship(ApplicationSettings).NewInstance()
    oLoaderProcessHistory._LoaderID.Load(LoaderID)
    oLoaderProcessHistory._LoaderProcessTypeCode.Load(LoaderProcessTypeCode)
    oLoaderProcessHistory.ActionedByUser.Value = IIf(ActionedByUser = "", Constants.XDSAdministrationServicesUser, ActionedByUser)
    oLoaderProcessHistory.ActionedOnDate.Value = System.DateTime.Now
    oLoaderProcessHistory.ActionedDetail.Value = sActionDetail
    If StatusInd = Constants.LoaderProcess.Statuses.Failed AndAlso Not Exception Is Nothing Then
      oLoaderProcessHistory.ErrorMessage.Value = Exception.ToString()
    End If
    oLoaderProcessHistory.Save()
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class LoaderProcessHistoryRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _LoaderProcessHistoryID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _LoaderID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _LoaderProcessTypeCode As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New LoaderProcessHistory(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _LoaderProcessHistoryID.ForeignElement = CType(Instance, LoaderProcessHistory).LoaderProcessHistoryID
    _LoaderID.ForeignElement = CType(Instance, LoaderProcessHistory)._LoaderID
    _LoaderProcessTypeCode.ForeignElement = CType(Instance, LoaderProcessHistory)._LoaderProcessTypeCode
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0LoaderProcessHistory")
    oList.Columns.Add(New ListColumn("LoaderID", "[A0LoaderProcessHistory].[LoaderID]", "LoaderID", DataType.Integer, Nothing, True, 0, False, 100, "Loader ID"))
    oList.Columns.Add(New ListColumn("LoaderProcessHistoryID", "[A0LoaderProcessHistory].[LoaderProcessHistoryID]", "LoaderProcessHistoryID", DataType.Long, Nothing, True, -1, False, 100, "Loader Process History ID"))
    oList.Columns.Add(New ListColumn("LoaderProcessTypeCode", "[A0LoaderProcessHistory].[LoaderProcessTypeCode]", "LoaderProcessTypeCode", DataType.String, Nothing, True, 0, False, 150, "Loader Process Type Code"))
    oList.Columns.Add(New ListColumn("LoaderProcessTypeCode1", "[A1LoaderProcessType].[LoaderProcessTypeCode]", "LoaderProcessTypeCode", DataType.String, Nothing, False, 0, True, 100, "Loader Process Type Code"))
    oList.Columns.Add(New ListColumn("LoaderProcessTypeDesc", "[A1LoaderProcessType].[LoaderProcessTypeDesc]", "LoaderProcessTypeDesc", DataType.String, Nothing, False, 0, True, 200, "Loader Process Type Description"))
    oList.Columns.Add(New ListColumn("ActionedOnDate", "[A0LoaderProcessHistory].[ActionedOnDate]", "ActionedOnDate", DataType.DateTime, "g", False, 0, True, 150, "Actioned On Date"))
    oList.Columns.Add(New ListColumn("ActionedByUser", "[A0LoaderProcessHistory].[ActionedByUser]", "ActionedByUser", DataType.String, Nothing, False, 0, True, 100, "Actioned By User"))
    oList.Columns.Add(New ListColumn("ActionedDetail", "[A0LoaderProcessHistory].[ActionedDetail]", "ActionedDetail", DataType.String, Nothing, False, 0, True, 300, "Actioned Detail"))
    oList.SelectStatement = "select"
    oList.FromClause = "([LoaderProcessHistory]  [A0LoaderProcessHistory]" + _
           "    join [LoaderProcessType]  [A1LoaderProcessType] on [A0LoaderProcessHistory].[LoaderProcessTypeCode] = [A1LoaderProcessType].[LoaderProcessTypeCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship