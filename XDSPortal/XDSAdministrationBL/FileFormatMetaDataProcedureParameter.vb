Imports sembleWare.Runtime
Imports System
Public Class FileFormatMetaDataProcedureParameter 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly ParameterFileFormatField As Relationship = New FileFormatFieldRelationship("ParameterFileFormatField", "Parameter File Format Field", RelationshipType.Include, False, True, True, Me)
  Public ReadOnly FileFormatMetaDataProcedure As Relationship = New FileFormatMetaDataProcedureRelationship("FileFormatMetaDataProcedure", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly DataProcedureParameter As Relationship = New DataProcedureParameterRelationship("DataProcedureParameter", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _ParameterFileFormatFieldID As New IntegerElement("ParameterFileFormatFieldID", Me, False)
  Public ReadOnly _FileFormatCode As New StringElement("FileFormatCode", Me, True)
  Public ReadOnly _FileFormatFieldID As New IntegerElement("FileFormatFieldID", Me, True)
  Public ReadOnly _MetaDatabaseCode As New StringElement("MetaDatabaseCode", Me, True)
  Public ReadOnly _ParameterID As New IntegerElement("ParameterID", Me, True)
  Public ReadOnly _DataProcedureID As New IntegerElement("DataProcedureID", Me, True)
  Public ReadOnly _ParameterFileFormatField As FileFormatFieldRelationship = ParameterFileFormatField
  Public ReadOnly _FileFormatMetaDataProcedure As FileFormatMetaDataProcedureRelationship = FileFormatMetaDataProcedure
  Public ReadOnly _DataProcedureParameter As DataProcedureParameterRelationship = DataProcedureParameter
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("FileFormatMetaDataProcedureParameter", ApplicationSettings)
    _ParameterFileFormatField._FileFormatFieldID.LocalElement = _ParameterFileFormatFieldID
    _ParameterFileFormatField._FileFormatCode.LocalElement = _FileFormatCode
    _FileFormatMetaDataProcedure._FileFormatCode.LocalElement = _FileFormatCode
    _FileFormatMetaDataProcedure._FileFormatFieldID.LocalElement = _FileFormatFieldID
    _FileFormatMetaDataProcedure._MetaDatabaseCode.LocalElement = _MetaDatabaseCode
    _FileFormatMetaDataProcedure._DataProcedureID.LocalElement = _DataProcedureID
    _DataProcedureParameter._ParameterID.LocalElement = _ParameterID
    _DataProcedureParameter._DataProcedureID.LocalElement = _DataProcedureID
    'sembleWare: Constructor End - Do Not Modify
    AddHandler ParameterFileFormatField.LimitRelatedList, AddressOf ParameterFileFormatField_LimitRelatedList
  End Sub

#End Region 'sembleWare: Constructor

#Region " LimitRelatedList Events"
  Private Sub ParameterFileFormatField_LimitRelatedList(ByVal List As List)
    Dim oFileFormatMetaDataProcedure As FileFormatMetaDataProcedure = Me.FileFormatMetaDataProcedure.Instance
    List.LimitByRelatedPart(oFileFormatMetaDataProcedure.FileFormat.Instance, "FileFormat", True)
  End Sub
#End Region

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Load()
    MyBase.Load()
    BRLoad()
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRLoad()
    Me.ParameterFileFormatField.Mandatory = True
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class FileFormatMetaDataProcedureParameterRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _FileFormatCode As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _FileFormatFieldID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _MetaDatabaseCode As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _ParameterID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _DataProcedureID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New FileFormatMetaDataProcedureParameter(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _FileFormatCode.ForeignElement = CType(Instance, FileFormatMetaDataProcedureParameter)._FileFormatCode
    _FileFormatFieldID.ForeignElement = CType(Instance, FileFormatMetaDataProcedureParameter)._FileFormatFieldID
    _MetaDatabaseCode.ForeignElement = CType(Instance, FileFormatMetaDataProcedureParameter)._MetaDatabaseCode
    _ParameterID.ForeignElement = CType(Instance, FileFormatMetaDataProcedureParameter)._ParameterID
    _DataProcedureID.ForeignElement = CType(Instance, FileFormatMetaDataProcedureParameter)._DataProcedureID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0FileFormatMetaDataProcedureParameter")
    oList.Columns.Add(New ListColumn("FileFormatFieldID", "[A0FileFormatMetaDataProcedureParameter].[FileFormatFieldID]", "FileFormatFieldID", DataType.Integer, Nothing, True, 0, False, 100, "File Format Field ID"))
    oList.Columns.Add(New ListColumn("FileFormatCode", "[A0FileFormatMetaDataProcedureParameter].[FileFormatCode]", "FileFormatCode", DataType.String, Nothing, True, 0, False, 100, "File Format Code"))
    oList.Columns.Add(New ListColumn("MetaDatabaseCode", "[A0FileFormatMetaDataProcedureParameter].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, True, 0, False, 100, "Meta Database Code"))
    oList.Columns.Add(New ListColumn("ParameterDesc", "[A1DataProcedureParameter].[ParameterDesc]", "ParameterDesc", DataType.String, Nothing, False, 0, True, 200, "Parameter Description"))
    oList.Columns.Add(New ListColumn("SequenceNum", "[A1DataProcedureParameter].[SequenceNum]", "SequenceNum", DataType.Integer, Nothing, False, 1, False, 100, "Sequence Number"))
    oList.Columns.Add(New ListColumn("FileFormatFieldDesc", "[A2ParameterFileFormatField].[FileFormatFieldDesc]", "FileFormatFieldDesc", DataType.String, Nothing, False, 0, True, 200, "Field Description "))
    oList.Columns.Add(New ListColumn("DataProcedureID", "[A0FileFormatMetaDataProcedureParameter].[DataProcedureID]", "DataProcedureID", DataType.Integer, Nothing, True, 0, False, 100, "Data Procedure ID"))
    oList.Columns.Add(New ListColumn("ParameterID", "[A0FileFormatMetaDataProcedureParameter].[ParameterID]", "ParameterID", DataType.Integer, Nothing, True, 0, False, 100, "Parameter ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "(([FileFormatMetaDataProcedureParameter]  [A0FileFormatMetaDataProcedureParameter]" + _
           "    join [DataProcedureParameter]  [A1DataProcedureParameter] on [A0FileFormatMetaDataProcedureParameter].[ParameterID] = [A1DataProcedureParameter].[ParameterID] and [A0FileFormatMetaDataProcedureParameter].[DataProcedureID] = [A1DataProcedureParameter].[DataProcedureID])" + _
           "    left join [FileFormatField]  [A2ParameterFileFormatField] on [A0FileFormatMetaDataProcedureParameter].[ParameterFileFormatFieldID] = [A2ParameterFileFormatField].[FileFormatFieldID] and [A0FileFormatMetaDataProcedureParameter].[FileFormatCode] = [A2ParameterFileFormatField].[FileFormatCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
