Imports sembleWare.Runtime
Imports System
Public Class ReportingServicesFolder 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly ReportingServicesFolderPath As New StringElement("ReportingServicesFolderPath", Me, True, True, True, True, True, 100, "Folder Path", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ReportingServicesFolderDesc As New StringElement("ReportingServicesFolderDesc", Me, False, True, True, True, False, 100, "Folder Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ReportingServicesFolderLabelDescription As New StringElement("ReportingServicesFolderLabelDescription", Me, False, True, True, False, False, 500, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly DataSourceName As New StringElement("DataSourceName", Me, False, True, True, True, True, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly DataSourceDesc As New StringElement("DataSourceDesc", Me, False, True, True, True, False, 100, "Data Source Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly DataSourceServerName As New StringElement("DataSourceServerName", Me, False, True, True, True, True, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly DataSourceDatabaseName As New StringElement("DataSourceDatabaseName", Me, False, True, True, True, True, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly DataSourceUsername As New StringElement("DataSourceUsername", Me, False, True, True, True, True, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly DataSourcePassword As New StringElement("DataSourcePassword", Me, False, True, True, True, True, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly DataSourceResetPasswordYN As New BooleanElement("DataSourceResetPasswordYN", Me, False, True, True, False, False, "Reset Password?", Nothing, False, "Yes;No")
  Public ReadOnly DataSourceConfirmPassword As New StringElement("DataSourceConfirmPassword", Me, False, True, True, False, False, 30, "Confirm Password", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly DataSourceNewPassword As New StringElement("DataSourceNewPassword", Me, False, True, True, False, False, 30, "New Password", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ReportingServicesServer As Relationship = New ReportingServicesServerRelationship("ReportingServicesServer", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _ReportingServicesServerID As New IntegerElement("ReportingServicesServerID", Me, True)
  Public ReadOnly _ReportingServicesServer As ReportingServicesServerRelationship = ReportingServicesServer
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("ReportingServicesFolder", ApplicationSettings)
    _ReportingServicesServer._ReportingServicesServerID.LocalElement = _ReportingServicesServerID
    AddHandler DataSourceResetPasswordYN.Changed, AddressOf DataSourceResetPasswordYN_Changed
    'sembleWare: Constructor End - Do Not Modify
    Me.ForceUpperCaseKeys = False
  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub DataSourceResetPasswordYN_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BRDataSourceResetPasswordYN(Element, True, True)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"
  Public Sub PublishToReportingServices()
    Me.MessageToDisplay.SetToNullValue()

    Dim oReportingServicesServer As ReportingServicesServer = Me.ReportingServicesServer.Instance
    Dim sFullFolderPath As String = oReportingServicesServer.RootFolderPath.Value & "/" & Me.ReportingServicesFolderPath.Value
    Dim oEncryption As sembleWare.Security.Encryption = New sembleWare.Security.Encryption(Constants.PasswordHashString)
    
    SHBS.ReportingServices.ReportManager.CreateFolder(oReportingServicesServer.ServerUrl.Value, oReportingServicesServer.RootFolderPath.Value, Me.ReportingServicesFolderPath.Value, Me.ReportingServicesFolderDesc.Value & " Root Folder")
    SHBS.ReportingServices.ReportManager.CreateDataSource(oReportingServicesServer.ServerUrl.Value, sFullFolderPath, Me.DataSourceName.Value, Me.DataSourceDesc.Value, Me.DataSourceServerName.Value, Me.DataSourceDatabaseName.Value, Me.DataSourceUsername.Value, oEncryption.Decrypt(Me.DataSourcePassword.Value))
  End Sub

  Public Sub RemoveFromReportingServices()
    Me.MessageToDisplay.SetToNullValue()

    Dim oReportingServicesServer As ReportingServicesServer = Me.ReportingServicesServer.Instance

    SHBS.ReportingServices.ReportManager.DeleteItem(oReportingServicesServer.ServerUrl.Value, oReportingServicesServer.RootFolderPath.Value, Me.ReportingServicesFolderPath.Value, "")
    Me.MessageToDisplay.Value = "Reporting Services Folder Successfully Removed from: " & oReportingServicesServer.ReportingServicesServerDesc.Value
  End Sub

#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreate()
  End Sub

  Public Overrides Sub Load()
    MyBase.Load()
    BRLoad()
  End Sub

  Public Overrides Sub Save()
    With ApplicationSettings
      Try
        .BeginTransaction()
        Dim bIsNewYN As Boolean = Me.IsNew
        BRBeforeSave(bIsNewYN)
        MyBase.Save()
        BRAfterSave(bIsNewYN)
        .CommitTransaction()
      Catch Exception As Exception
        .RollbackTransaction()
        Throw Exception
      End Try
    End With
  End Sub

  Public Overrides Sub Delete()
    With ApplicationSettings
      Try
        .BeginTransaction()
        MyBase.Delete()
        BRDelete()
        .CommitTransaction()
      Catch Exception As Exception
        .RollbackTransaction()
        Throw Exception
      End Try
    End With
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreate()
    Me.DataSourceResetPasswordYN.Value = True
    BRDataSourceResetPasswordYN(Me.DataSourceResetPasswordYN, True, False)
    Me.DataSourceResetPasswordYN.Enabled = False
    BRReportingServicesFolderLabelDescription()
  End Sub

  Private Sub BRLoad()
    BRReportingServicesFolderLabelDescription()
  End Sub

  Private Sub BRBeforeSave(ByVal IsNewYN As Boolean)
    Me.MessageToDisplay.SetToNullValue()
    If Me.DataSourceResetPasswordYN.Value Then
      Dim oEncryption As sembleWare.Security.Encryption = New sembleWare.Security.Encryption(Constants.PasswordHashString)
      Dim sNewPassword As String = oEncryption.Encrypt(Me.DataSourceNewPassword.Value)
      Dim sConfirmPassword As String = oEncryption.Encrypt(Me.DataSourceConfirmPassword.Value)

      If Not sNewPassword = sConfirmPassword Then
        Throw New BusinessRuleException("The Password does match the Confirm Password. Please re-enter the password!")
      End If
      Me.DataSourcePassword.Value = sNewPassword
    End If
  End Sub

  Private Sub BRAfterSave(ByVal IsNewYN As Boolean)
    If IsNewYN Then
      PublishToReportingServices()
    End If
    Me.DataSourceResetPasswordYN.Enabled = True
    Me.DataSourceResetPasswordYN.Value = False
    BRDataSourceResetPasswordYN(Me.DataSourceResetPasswordYN, True, True)
    BRReportingServicesFolderLabelDescription()
  End Sub

  Private Sub BRDelete()
    RemoveFromReportingServices()
  End Sub

  Private Sub BRReportingServicesFolderLabelDescription()
    Dim oReportingServicesServer As ReportingServicesServer = Me.ReportingServicesServer.Instance

    Me.ReportingServicesFolderLabelDescription.Value = CreateLabelDescription(oReportingServicesServer.ReportingServicesServerLabelDescription.Value + " - Folder", Me.ReportingServicesFolderDesc)
  End Sub

  Private Sub BRDataSourceResetPasswordYN(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If SetEnableDisableYN Then
      Me.DataSourceNewPassword.Enabled = Element.ValueAsObject
      Me.DataSourceNewPassword.Mandatory = Element.ValueAsObject
      Me.DataSourceConfirmPassword.Enabled = Element.ValueAsObject
      Me.DataSourceConfirmPassword.Mandatory = Element.ValueAsObject
    End If

    If PerformRulesYN Then
      If Not Element.ValueAsObject Then
        Me.DataSourceNewPassword.SetToNullValue()
        Me.DataSourceConfirmPassword.SetToNullValue()
      End If
    End If
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class ReportingServicesFolderRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _ReportingServicesServerID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _ReportingServicesFolderPath As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New ReportingServicesFolder(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _ReportingServicesServerID.ForeignElement = CType(Instance, ReportingServicesFolder)._ReportingServicesServerID
    _ReportingServicesFolderPath.ForeignElement = CType(Instance, ReportingServicesFolder).ReportingServicesFolderPath
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0ReportingServicesFolder")
    oList.Columns.Add(New ListColumn("ReportingServicesServerID", "[A0ReportingServicesFolder].[ReportingServicesServerID]", "ReportingServicesServerID", DataType.Integer, Nothing, True, 0, False, 100, "Reporting Services Server ID"))
    oList.Columns.Add(New ListColumn("ReportingServicesFolderPath", "[A0ReportingServicesFolder].[ReportingServicesFolderPath]", "ReportingServicesFolderPath", DataType.String, Nothing, True, 1, True, 200, "Folder Path"))
    oList.Columns.Add(New ListColumn("ReportingServicesFolderDesc", "[A0ReportingServicesFolder].[ReportingServicesFolderDesc]", "ReportingServicesFolderDesc", DataType.String, Nothing, False, 0, True, 200, "Folder Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "[ReportingServicesFolder]  [A0ReportingServicesFolder]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function DropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0ReportingServicesFolder")
    oList.Columns.Add(New ListColumn("ReportingServicesServerID", "[A0ReportingServicesFolder].[ReportingServicesServerID]", "ReportingServicesServerID", DataType.Integer, Nothing, True, 0, False, 100, "Reporting Services Server ID"))
    oList.Columns.Add(New ListColumn("ReportingServicesFolderPath", "[A0ReportingServicesFolder].[ReportingServicesFolderPath]", "ReportingServicesFolderPath", DataType.String, Nothing, True, 1, True, 200, "Folder Path"))
    oList.SelectStatement = "select"
    oList.FromClause = "[ReportingServicesFolder]  [A0ReportingServicesFolder]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0ReportingServicesFolder")
    oList.Columns.Add(New ListColumn("ReportingServicesServerID", "[A0ReportingServicesFolder].[ReportingServicesServerID]", "ReportingServicesServerID", DataType.Integer, Nothing, True, 0, False, 100, "Reporting Services Server ID"))
    oList.Columns.Add(New ListColumn("ReportingServicesFolderPath", "[A0ReportingServicesFolder].[ReportingServicesFolderPath]", "ReportingServicesFolderPath", DataType.String, Nothing, True, 1, True, 200, "Folder Path"))
    oList.SelectStatement = "select"
    oList.FromClause = "[ReportingServicesFolder]  [A0ReportingServicesFolder]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
