Imports sembleWare.Runtime
Imports System
Public Class DirectorAddress 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly DirectorAddressID As New IdentityElement("DirectorAddressID", Me, True, Nothing, Nothing)
  Public ReadOnly AddressTypeInd As New IndicatorElement("AddressTypeInd", Me, False, False, True, True, True, True, "Address Type", Nothing, DirectorAddress.AddressTypeIndOptions, Nothing)
  Public ReadOnly OriginalAddress1 As New StringElement("OriginalAddress1", Me, False, True, True, True, False, 100, "Line 1", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly OriginalAddress2 As New StringElement("OriginalAddress2", Me, False, True, True, True, False, 100, "Line 2", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly OriginalAddress3 As New StringElement("OriginalAddress3", Me, False, True, True, True, False, 100, "Line 3", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly OriginalAddress4 As New StringElement("OriginalAddress4", Me, False, True, True, True, False, 100, "Line 4", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly OriginalPostalCode As New StringElement("OriginalPostalCode", Me, False, True, True, True, False, 10, "Postal Code", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CleanAddressValidTypeInd As New IndicatorElement("CleanAddressValidTypeInd", Me, False, False, True, True, True, False, "Clean Address Valid Type", Nothing, DirectorAddress.CleanAddressValidTypeIndOptions, Nothing)
  Public ReadOnly CleanAddress1 As New StringElement("CleanAddress1", Me, False, False, True, True, False, 100, "Line 1", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CleanAddress2 As New StringElement("CleanAddress2", Me, False, False, True, True, False, 100, "Line 2", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CleanAddress3 As New StringElement("CleanAddress3", Me, False, False, True, True, False, 100, "Line 3", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CleanAddress4 As New StringElement("CleanAddress4", Me, False, False, True, True, False, 100, "Line 4", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CleanPostalCode As New StringElement("CleanPostalCode", Me, False, False, True, True, False, 10, "Postal Code", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CleanAddressDupCode As New StringElement("CleanAddressDupCode", Me, False, True, True, True, False, 200, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RecordStatusInd As New IndicatorElement("RecordStatusInd", Me, False, False, False, True, True, True, "Record Status", Nothing, DirectorAddress.RecordStatusIndOptions, "A")
  Public ReadOnly DeletedReason As New StringElement("DeletedReason", Me, False, True, True, True, False, 500, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LastUpdatedDate As New DateTimeElement("LastUpdatedDate", Me, False, True, True, True, False, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly Loader As Relationship = New LoaderRelationship("Loader", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Country As Relationship = New CountryRelationship("Country", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly CountryRegion As Relationship = New CountryRegionRelationship("CountryRegion", "Region", RelationshipType.Include, False, True, True, Me)
  Public ReadOnly Director As Relationship = New DirectorRelationship("Director", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _LoaderID As New IntegerElement("LoaderID", Me, False)
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, False)
  Public ReadOnly _CountryCode As New StringElement("CountryCode", Me, False)
  Public ReadOnly _CountryRegionCode As New StringElement("CountryRegionCode", Me, False)
  Public ReadOnly _DirectorID As New IntegerElement("DirectorID", Me, True)
  Public ReadOnly _Loader As LoaderRelationship = Loader
  Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
  Public ReadOnly _Country As CountryRelationship = Country
  Public ReadOnly _CountryRegion As CountryRegionRelationship = CountryRegion
  Public ReadOnly _Director As DirectorRelationship = Director
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moAddressTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property AddressTypeIndOptions() As IndicatorOptions
    Get
      If moAddressTypeIndOptions Is Nothing Then
        moAddressTypeIndOptions = New IndicatorOptions
        moAddressTypeIndOptions.Add("R", "Residential")
        moAddressTypeIndOptions.Add("P", "Postal")
        moAddressTypeIndOptions.Add("G", "Registered")
        moAddressTypeIndOptions.Add("B", "Business")
      End If
      Return moAddressTypeIndOptions
    End Get
  End Property
  Private Shared moCleanAddressValidTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property CleanAddressValidTypeIndOptions() As IndicatorOptions
    Get
      If moCleanAddressValidTypeIndOptions Is Nothing Then
        moCleanAddressValidTypeIndOptions = New IndicatorOptions
        moCleanAddressValidTypeIndOptions.Add("S", "Suspect")
        moCleanAddressValidTypeIndOptions.Add("Y", "Valid")
        moCleanAddressValidTypeIndOptions.Add("N", "Invalid")
      End If
      Return moCleanAddressValidTypeIndOptions
    End Get
  End Property
  Private Shared moRecordStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property RecordStatusIndOptions() As IndicatorOptions
    Get
      If moRecordStatusIndOptions Is Nothing Then
        moRecordStatusIndOptions = New IndicatorOptions
        moRecordStatusIndOptions.Add("A", "Active")
        moRecordStatusIndOptions.Add("D", "Deleted")
      End If
      Return moRecordStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("DirectorAddress", ApplicationSettings)
    _Loader._LoaderID.LocalElement = _LoaderID
    _Subscriber._SubscriberID.LocalElement = _SubscriberID
    _Country._CountryCode.LocalElement = _CountryCode
    _CountryRegion._CountryRegionCode.LocalElement = _CountryRegionCode
    _CountryRegion._CountryCode.LocalElement = _CountryCode
    _Director._DirectorID.LocalElement = _DirectorID
    'sembleWare: Constructor End - Do Not Modify
    Me.DeleteType = CustomDBPart.enumDeleteType.ChangeRecordStatus
  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class DirectorAddressRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _DirectorAddressID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _DirectorID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New DirectorAddress(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _DirectorAddressID.ForeignElement = CType(Instance, DirectorAddress).DirectorAddressID
    _DirectorID.ForeignElement = CType(Instance, DirectorAddress)._DirectorID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0DirectorAddress")
    oList.Columns.Add(New ListColumn("DirectorID", "[A0DirectorAddress].[DirectorID]", "DirectorID", DataType.Integer, Nothing, True, 0, False, 100, "Director ID"))
    oList.Columns.Add(New ListColumn("DirectorAddressID", "[A0DirectorAddress].[DirectorAddressID]", "DirectorAddressID", DataType.Long, Nothing, True, 0, False, 100, "Director Address ID"))
    oList.Columns.Add(New ListColumn("AddressTypeInd", "[A0DirectorAddress].[AddressTypeInd]", "AddressTypeInd", DataType.String, Nothing, False, 2, True, 100, "Address Type"))
    oList.Columns.Add(New ListColumn("CleanAddress1", "[A0DirectorAddress].[CleanAddress1]", "CleanAddress1", DataType.String, Nothing, False, 0, True, 100, "Line 1"))
    oList.Columns.Add(New ListColumn("CleanAddress2", "[A0DirectorAddress].[CleanAddress2]", "CleanAddress2", DataType.String, Nothing, False, 0, True, 100, "Line 2"))
    oList.Columns.Add(New ListColumn("CleanAddress3", "[A0DirectorAddress].[CleanAddress3]", "CleanAddress3", DataType.String, Nothing, False, 0, True, 100, "Line 3"))
    oList.Columns.Add(New ListColumn("CleanAddress4", "[A0DirectorAddress].[CleanAddress4]", "CleanAddress4", DataType.String, Nothing, False, 0, True, 100, "Line 4"))
    oList.Columns.Add(New ListColumn("CleanPostalCode", "[A0DirectorAddress].[CleanPostalCode]", "CleanPostalCode", DataType.String, Nothing, False, 0, True, 100, "Postal Code"))
    oList.Columns.Add(New ListColumn("LastUpdatedDate", "[A0DirectorAddress].[LastUpdatedDate]", "LastUpdatedDate", DataType.DateTime, "g", False, -1, True, 100, "Last Updated Date"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A1Subscriber].[SubscriberID]", "SubscriberID", DataType.Long, Nothing, False, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberName", "[A1Subscriber].[SubscriberName]", "SubscriberName", DataType.String, Nothing, False, 0, True, 200, "Subscriber Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "([DirectorAddress]  [A0DirectorAddress]" + _
           "    left join [Subscriber]  [A1Subscriber] on [A0DirectorAddress].[SubscriberID] = [A1Subscriber].[SubscriberID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
