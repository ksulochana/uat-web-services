Imports sembleWare.Runtime
Imports System
Public Class SAFPSProtectiveRegistration 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly SAFPSProtectiveRegistrationID As New IdentityElement("SAFPSProtectiveRegistrationID", Me, True, "SAFPS Protective Registration ID", Nothing)
  Public ReadOnly PRReference As New StringElement("PRReference", Me, False, True, True, True, False, 10, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Surname As New StringElement("Surname", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FirstName As New StringElement("FirstName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SecondName As New StringElement("SecondName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly IDNo As New StringElement("IDNo", Me, False, True, True, True, False, 64, "ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PassportNo As New StringElement("PassportNo", Me, False, True, True, True, False, 64, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly BirthDate As New DateTimeElement("BirthDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly EmailAddress As New StringElement("EmailAddress", Me, False, True, True, True, False, 255, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Employer As New StringElement("Employer", Me, False, True, True, True, False, 4000, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly JobTitle As New StringElement("JobTitle", Me, False, True, True, True, False, 255, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly TaxNo As New StringElement("TaxNo", Me, False, True, True, True, False, 255, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SocialSecurity As New StringElement("SocialSecurity", Me, False, True, True, True, False, 255, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly DriversLicense As New StringElement("DriversLicense", Me, False, True, True, True, False, 255, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PhysicalAddress As New StringElement("PhysicalAddress", Me, False, True, True, True, False, 4000, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PostalCode As New StringElement("PostalCode", Me, False, True, True, True, False, 255, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Country As New StringElement("Country", Me, False, True, True, True, False, 255, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly YearsAtAddress As New IntegerElement("YearsAtAddress", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly MonthsAtAddress As New IntegerElement("MonthsAtAddress", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly TelephoneHome As New StringElement("TelephoneHome", Me, False, True, True, True, False, 255, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly TelephoneWork As New StringElement("TelephoneWork", Me, False, True, True, True, False, 255, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly TelephoneCellular As New StringElement("TelephoneCellular", Me, False, True, True, True, False, 255, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Reason As New TextElement("Reason", Me, True, True, True, False, Nothing, Nothing, Nothing)
  Public ReadOnly DocumentLost As New StringElement("DocumentLost", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly DocumentLostDate As New DateTimeElement("DocumentLostDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly PoliceDate As New DateTimeElement("PoliceDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly CaseNo As New StringElement("CaseNo", Me, False, True, True, True, False, 255, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PoliceAddress As New StringElement("PoliceAddress", Me, False, True, True, True, False, 4000, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PoliceTelephoneNo As New StringElement("PoliceTelephoneNo", Me, False, True, True, True, False, 255, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly OtherInfo As New TextElement("OtherInfo", Me, True, True, True, False, Nothing, Nothing, Nothing)
  Public ReadOnly AlternateNo As New StringElement("AlternateNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Status As New StringElement("Status", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly IsPossibleNameConflictYN As New BooleanElement("IsPossibleNameConflictYN", Me, False, False, True, True, True, "Is Possible Name Conflict?", Nothing, False, "Yes;No")
  Public ReadOnly IsPossibleDuplicateRecordYN As New BooleanElement("IsPossibleDuplicateRecordYN", Me, False, False, True, True, True, "Is Possible Duplicate Record?", Nothing, False, "Yes;No")
  Public ReadOnly RecordStatusInd As New IndicatorElement("RecordStatusInd", Me, False, False, False, True, True, True, "Record Status", Nothing, SAFPSProtectiveRegistration.RecordStatusIndOptions, "A")
  Public ReadOnly DeletedReason As New StringElement("DeletedReason", Me, False, True, True, True, False, 500, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LastUpdatedDate As New DateTimeElement("LastUpdatedDate", Me, False, False, True, True, True, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly Title As Relationship = New TitleRelationship("Title", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Loader As Relationship = New LoaderRelationship("Loader", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly SAFPSProtectiveRegistrationConflict_OwnMany As Relationship = New SAFPSProtectiveRegistrationConflictRelationship("SAFPSProtectiveRegistrationConflict", Nothing, "SAFPSProtectiveRegistration", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _TitleCode As New StringElement("TitleCode", Me, False)
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, False)
  Public ReadOnly _LoaderID As New IntegerElement("LoaderID", Me, False)
  Public ReadOnly _Title As TitleRelationship = Title
  Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
  Public ReadOnly _Loader As LoaderRelationship = Loader
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  Public ReadOnly _SAFPSProtectiveRegistrationConflict_OwnMany As SAFPSProtectiveRegistrationConflictRelationship = SAFPSProtectiveRegistrationConflict_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moRecordStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property RecordStatusIndOptions() As IndicatorOptions
    Get
      If moRecordStatusIndOptions Is Nothing Then
        moRecordStatusIndOptions = New IndicatorOptions
        moRecordStatusIndOptions.Add("A", "Active")
        moRecordStatusIndOptions.Add("D", "Deleted")
      End If
      Return moRecordStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SAFPSProtectiveRegistration", ApplicationSettings)
    _Title._TitleCode.LocalElement = _TitleCode
    _Subscriber._SubscriberID.LocalElement = _SubscriberID
    _Loader._LoaderID.LocalElement = _LoaderID
    'sembleWare: Constructor End - Do Not Modify
    Me.DeleteType = CustomDBPart.enumDeleteType.ChangeRecordStatus
  End Sub

#End Region 'sembleWare: Constructor


#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes


#Region "base Overrides"
  Public Overrides Sub Save()
    With ApplicationSettings
      Try
        .BeginTransaction()
        Dim bIsNewYN As Boolean = Me.IsNew
        BRBeforeSave(bIsNewYN)
        MyBase.Save()
        .CommitTransaction()
      Catch Exception As Exception
        .RollbackTransaction()
        Throw Exception
      End Try
    End With
  End Sub

#End Region

#Region "Business Rules"
  Private Sub BRBeforeSave(ByVal IsNewYN As Boolean)
    Me.LastUpdatedDate.Value = System.DateTime.Now
  End Sub
#End Region




End Class 'sembleWare: Part

Public Class SAFPSProtectiveRegistrationRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SAFPSProtectiveRegistrationID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SAFPSProtectiveRegistration(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SAFPSProtectiveRegistrationID.ForeignElement = CType(Instance, SAFPSProtectiveRegistration).SAFPSProtectiveRegistrationID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SAFPSProtectiveRegistration")
    oList.Columns.Add(New ListColumn("SAFPSProtectiveRegistrationID", "[A0SAFPSProtectiveRegistration].[SAFPSProtectiveRegistrationID]", "SAFPSProtectiveRegistrationID", DataType.Long, Nothing, True, 0, True, 100, "SAFPS Protective Registration ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A0SAFPSProtectiveRegistration].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("PassportNo", "[A0SAFPSProtectiveRegistration].[PassportNo]", "PassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No"))
    oList.Columns.Add(New ListColumn("Surname", "[A0SAFPSProtectiveRegistration].[Surname]", "Surname", DataType.String, Nothing, False, 0, True, 200, "Surname"))
    oList.Columns.Add(New ListColumn("FirstName", "[A0SAFPSProtectiveRegistration].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("SecondName", "[A0SAFPSProtectiveRegistration].[SecondName]", "SecondName", DataType.String, Nothing, False, 0, True, 100, "Second Name"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A0SAFPSProtectiveRegistration].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Birth Date"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SAFPSProtectiveRegistration]  [A0SAFPSProtectiveRegistration]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    oList.AndFilters.Add(New ListFilter("RecordStatusInd", "[A0SAFPSProtectiveRegistration].[RecordStatusInd]", Constants.RecordStatus.Active, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SAFPSProtectiveRegistration")
    oList.Columns.Add(New ListColumn("SAFPSProtectiveRegistrationID", "[A0SAFPSProtectiveRegistration].[SAFPSProtectiveRegistrationID]", "SAFPSProtectiveRegistrationID", DataType.Long, Nothing, True, 0, True, 100, "SAFPS Protective Registration ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A0SAFPSProtectiveRegistration].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("PassportNo", "[A0SAFPSProtectiveRegistration].[PassportNo]", "PassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No"))
    oList.Columns.Add(New ListColumn("Surname", "[A0SAFPSProtectiveRegistration].[Surname]", "Surname", DataType.String, Nothing, False, 0, True, 200, "Surname"))
    oList.Columns.Add(New ListColumn("FirstName", "[A0SAFPSProtectiveRegistration].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("SecondName", "[A0SAFPSProtectiveRegistration].[SecondName]", "SecondName", DataType.String, Nothing, False, 0, True, 100, "Second Name"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A0SAFPSProtectiveRegistration].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Birth Date"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SAFPSProtectiveRegistration]  [A0SAFPSProtectiveRegistration]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ConflictGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SAFPSProtectiveRegistration")
    oList.Columns.Add(New ListColumn("SAFPSProtectiveRegistrationID", "[A0SAFPSProtectiveRegistration].[SAFPSProtectiveRegistrationID]", "SAFPSProtectiveRegistrationID", DataType.Long, Nothing, True, 0, True, 100, "SAFPS Protective Registration ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A0SAFPSProtectiveRegistration].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("PassportNo", "[A0SAFPSProtectiveRegistration].[PassportNo]", "PassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No"))
    oList.Columns.Add(New ListColumn("Surname", "[A0SAFPSProtectiveRegistration].[Surname]", "Surname", DataType.String, Nothing, False, 0, True, 200, "Surname"))
    oList.Columns.Add(New ListColumn("FirstName", "[A0SAFPSProtectiveRegistration].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("SecondName", "[A0SAFPSProtectiveRegistration].[SecondName]", "SecondName", DataType.String, Nothing, False, 0, True, 100, "Second Name"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A0SAFPSProtectiveRegistration].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Birth Date"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SAFPSProtectiveRegistration]  [A0SAFPSProtectiveRegistration]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    oList.OrFilters.Add(New ListFilter("IsPossibleNameConflictYN", "[A0SAFPSProtectiveRegistration].[IsPossibleNameConflictYN]", True, DataType.Boolean, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
    oList.OrFilters.Add(New ListFilter("IsPossibleDuplicateRecordYN", "[A0SAFPSProtectiveRegistration].[IsPossibleDuplicateRecordYN]", True, DataType.Boolean, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))

    Return oList
  End Function


  Public Function ConflictForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SAFPSProtectiveRegistration")
    oList.Columns.Add(New ListColumn("SAFPSProtectiveRegistrationID", "[A0SAFPSProtectiveRegistration].[SAFPSProtectiveRegistrationID]", "SAFPSProtectiveRegistrationID", DataType.Long, Nothing, True, 0, True, 100, "SAFPS Protective Registration ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A0SAFPSProtectiveRegistration].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("PassportNo", "[A0SAFPSProtectiveRegistration].[PassportNo]", "PassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No"))
    oList.Columns.Add(New ListColumn("Surname", "[A0SAFPSProtectiveRegistration].[Surname]", "Surname", DataType.String, Nothing, False, 0, True, 200, "Surname"))
    oList.Columns.Add(New ListColumn("FirstName", "[A0SAFPSProtectiveRegistration].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("SecondName", "[A0SAFPSProtectiveRegistration].[SecondName]", "SecondName", DataType.String, Nothing, False, 0, True, 100, "Second Name"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A0SAFPSProtectiveRegistration].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Birth Date"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SAFPSProtectiveRegistration]  [A0SAFPSProtectiveRegistration]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
