Imports sembleWare.Runtime
Imports System
Public Class MetaTable 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly MetaTableName As New StringElement("MetaTableName", Me, True, True, True, True, True, 250, "Table Name", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly MetaTableDesc As New StringElement("MetaTableDesc", Me, False, True, True, True, True, 250, "Table Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly MetaTableLabelDescription As New StringElement("MetaTableLabelDescription", Me, False, True, True, False, False, 500, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly MetaDatabase As Relationship = New MetaDatabaseRelationship("MetaDatabase", "Database", RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly MetaField_OwnMany As Relationship = New MetaFieldRelationship("MetaField", Nothing, "MetaTable", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _MetaDatabaseCode As New StringElement("MetaDatabaseCode", Me, True)
  Public ReadOnly _MetaDatabase As MetaDatabaseRelationship = MetaDatabase
  Public ReadOnly _MetaField_OwnMany As MetaFieldRelationship = MetaField_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("MetaTable", ApplicationSettings)
    _MetaDatabase._MetaDatabaseCode.LocalElement = _MetaDatabaseCode
    'sembleWare: Constructor End - Do Not Modify
    Me.ForceUpperCaseKeys = False
  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"

#End Region 'sembleWare: Change Events

#Region " Actions"

  Public Sub CheckMetaDatabaseType()
    Dim oMetaDatabase As MetaDatabase = Me.MetaDatabase.Instance

    Select Case oMetaDatabase.MetaDatabaseTypeInd.Value
      Case Constants.MetaDatabase.Types.Repository
        Throw New BusinessRuleException("You may not add Tables as this Meta Database Type is not ""Credit Bureau""!")
    End Select
  End Sub

#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreate()
  End Sub

  Public Overrides Sub Load()
    MyBase.Load()
    BRLoad()
  End Sub

  Public Overrides Sub Save()
    MyBase.Save()
    BRSave()
  End Sub

  Public Overrides Sub Delete()
    BRDelete()
    MyBase.Delete()
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreate()
    BRMetaDatabaseTypeInd(True, True)
    BRMetaTableLabelDescription()
  End Sub

  Private Sub BRLoad()
    BRMetaDatabaseTypeInd(True, False)
    BRMetaTableLabelDescription()
  End Sub

  Private Sub BRSave()
    BRMetaTableLabelDescription()
  End Sub

  Private Sub BRDelete()
    Dim oMetaDatabase As MetaDatabase = Me.MetaDatabase.Instance

    Select Case oMetaDatabase.MetaDatabaseTypeInd.Value
      Case Constants.MetaDatabase.Types.Repository
        Throw New BusinessRuleException("You cannot delete this Meta Table as this Meta Database Type is not of ""Credit Bureau""!")
    End Select
  End Sub

  Private Sub BRMetaDatabaseTypeInd(ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    Dim oMetaDatabase As MetaDatabase = Me.MetaDatabase.Instance

    If SetEnableDisableYN Then
      Select Case oMetaDatabase.MetaDatabaseTypeInd.Value
        Case Constants.MetaDatabase.Types.Repository
          Me.MetaTableName.Enabled = False
          Me.MetaTableDesc.Enabled = False
        Case Constants.MetaDatabase.Types.System
          Me.MetaTableName.Enabled = True
          Me.MetaTableDesc.Enabled = True
      End Select
    End If
  End Sub

  Private Sub BRMetaTableLabelDescription()
    Dim oMetaDatabase As MetaDatabase = Me.MetaDatabase.Instance

    If Me.IsNew Then
      Me.MetaTableLabelDescription.Value = oMetaDatabase.MetaDatabaseLabelDescription.Value & " - Meta Table: (New)"
    Else
      Dim sDesc As String = Trim(Me.MetaTableDesc.Value)
      Me.MetaTableLabelDescription.Value = oMetaDatabase.MetaDatabaseLabelDescription.Value & " - Meta Table: " & Me.MetaTableName.Value & " (" & sDesc & ")"
    End If
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class MetaTableRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _MetaTableName As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _MetaDatabaseCode As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New MetaTable(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _MetaTableName.ForeignElement = CType(Instance, MetaTable).MetaTableName
    _MetaDatabaseCode.ForeignElement = CType(Instance, MetaTable)._MetaDatabaseCode
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0MetaTable")
    oList.Columns.Add(New ListColumn("MetaDatabaseCode", "[A0MetaTable].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, True, 0, False, 100, "Meta Database Code"))
    oList.Columns.Add(New ListColumn("MetaTableName", "[A0MetaTable].[MetaTableName]", "MetaTableName", DataType.String, Nothing, True, 1, True, 200, "Table Name"))
    oList.Columns.Add(New ListColumn("MetaTableDesc", "[A0MetaTable].[MetaTableDesc]", "MetaTableDesc", DataType.String, Nothing, False, 0, True, 200, "Table Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "[MetaTable]  [A0MetaTable]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0MetaTable")
    oList.Columns.Add(New ListColumn("MetaDatabaseCode", "[A0MetaTable].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, True, 0, False, 100, "Meta Database Code"))
    oList.Columns.Add(New ListColumn("MetaTableName", "[A0MetaTable].[MetaTableName]", "MetaTableName", DataType.String, Nothing, True, 1, True, 100, "Table Name"))
    oList.Columns.Add(New ListColumn("MetaTableDesc", "[A0MetaTable].[MetaTableDesc]", "MetaTableDesc", DataType.String, Nothing, False, 0, True, 200, "Table Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "[MetaTable]  [A0MetaTable]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function DropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0MetaTable")
    oList.Columns.Add(New ListColumn("MetaDatabaseCode", "[A0MetaTable].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, True, 0, False, 100, "Meta Database Code"))
    oList.Columns.Add(New ListColumn("MetaTableName", "[A0MetaTable].[MetaTableName]", "MetaTableName", DataType.String, Nothing, True, 1, True, 100, "Table Name"))
    oList.Columns.Add(New ListColumn("MetaTableDesc", "[A0MetaTable].[MetaTableDesc]", "MetaTableDesc", DataType.String, Nothing, False, 0, True, 200, "Table Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "[MetaTable]  [A0MetaTable]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
