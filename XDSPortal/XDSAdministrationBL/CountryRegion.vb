Imports sembleWare.Runtime
Imports System
Public Class CountryRegion 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly CountryRegionCode As New StringElement("CountryRegionCode", Me, True, True, True, True, True, 10, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CountryRegionDesc As New StringElement("CountryRegionDesc", Me, False, True, True, True, True, 50, "Country Region Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Country As Relationship = New CountryRelationship("Country", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _CountryCode As New StringElement("CountryCode", Me, True)
  Public ReadOnly _Country As CountryRelationship = Country
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("CountryRegion", ApplicationSettings)
    _Country._CountryCode.LocalElement = _CountryCode
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class CountryRegionRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _CountryRegionCode As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _CountryCode As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New CountryRegion(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _CountryRegionCode.ForeignElement = CType(Instance, CountryRegion).CountryRegionCode
    _CountryCode.ForeignElement = CType(Instance, CountryRegion)._CountryCode
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0CountryRegion")
    oList.Columns.Add(New ListColumn("CountryCode", "[A0CountryRegion].[CountryCode]", "CountryCode", DataType.String, Nothing, True, 0, False, 100, "Country Code"))
    oList.Columns.Add(New ListColumn("CountryRegionCode", "[A0CountryRegion].[CountryRegionCode]", "CountryRegionCode", DataType.String, Nothing, True, 1, True, 100, "Country Region Code"))
    oList.Columns.Add(New ListColumn("CountryRegionDesc", "[A0CountryRegion].[CountryRegionDesc]", "CountryRegionDesc", DataType.String, Nothing, False, 0, True, 200, "Country Region Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "[CountryRegion]  [A0CountryRegion]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function DropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0CountryRegion")
    oList.Columns.Add(New ListColumn("CountryCode", "[A0CountryRegion].[CountryCode]", "CountryCode", DataType.String, Nothing, True, 0, False, 100, "Country Code"))
    oList.Columns.Add(New ListColumn("CountryRegionCode", "[A0CountryRegion].[CountryRegionCode]", "CountryRegionCode", DataType.String, Nothing, True, 1, True, 100, "Country Region Code"))
    oList.Columns.Add(New ListColumn("CountryRegionDesc", "[A0CountryRegion].[CountryRegionDesc]", "CountryRegionDesc", DataType.String, Nothing, False, 0, True, 200, "Country Region Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "[CountryRegion]  [A0CountryRegion]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
