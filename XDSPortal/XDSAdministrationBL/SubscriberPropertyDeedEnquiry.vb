Imports sembleWare.Runtime
Imports System
Public Class SubscriberPropertyDeedEnquiry 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly SubscriberPropertyDeedEnquiryID As New IdentityElement("SubscriberPropertyDeedEnquiryID", Me, True, Nothing, Nothing)
  Public ReadOnly EnquiryDate As New DateTimeElement("EnquiryDate", Me, False, False, True, True, True, "Enquiry Date", Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly EnquiryStatusInd As New IndicatorElement("EnquiryStatusInd", Me, False, False, False, True, True, True, "Enquiry Status", Nothing, SubscriberPropertyDeedEnquiry.EnquiryStatusIndOptions, "Q")
  Public ReadOnly EnquiryResultInd As New IndicatorElement("EnquiryResultInd", Me, False, False, False, True, True, True, "Enquiry Result", Nothing, SubscriberPropertyDeedEnquiry.EnquiryResultIndOptions, "P")
  Public ReadOnly ProductPointValue As New DecimalElement("ProductPointValue", Me, False, False, True, True, True, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ExternalReference As New StringElement("ExternalReference", Me, False, True, True, True, False, 50, "External Reference", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly TitleDeedNo As New StringElement("TitleDeedNo", Me, False, True, True, True, False, 13, "ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResultTitleDeedNo As New StringElement("ResultTitleDeedNo", Me, False, False, True, True, False, 13, "ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Product As Relationship = New ProductRelationship("Product", Nothing, RelationshipType.Include, True, True, True, Me)
  Public ReadOnly SystemUser As Relationship = New SystemUserRelationship("SystemUser", Nothing, RelationshipType.Include, True, False, True, Me)
  Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly SubscriberPropertyDeedEnquiryResult_OwnMany As Relationship = New SubscriberPropertyDeedEnquiryResultRelationship("SubscriberPropertyDeedEnquiryResult", Nothing, "SubscriberPropertyDeedEnquiry", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _ProductID As New IntegerElement("ProductID", Me, False)
  Public ReadOnly _SystemUserID As New IntegerElement("SystemUserID", Me, False)
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _Product As ProductRelationship = Product
  Public ReadOnly _SystemUser As SystemUserRelationship = SystemUser
  Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
  Public ReadOnly _SubscriberPropertyDeedEnquiryResult_OwnMany As SubscriberPropertyDeedEnquiryResultRelationship = SubscriberPropertyDeedEnquiryResult_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moEnquiryStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property EnquiryStatusIndOptions() As IndicatorOptions
    Get
      If moEnquiryStatusIndOptions Is Nothing Then
        moEnquiryStatusIndOptions = New IndicatorOptions
        moEnquiryStatusIndOptions.Add("Q", "Queued")
        moEnquiryStatusIndOptions.Add("P", "Processing")
        moEnquiryStatusIndOptions.Add("C", "Completed")
      End If
      Return moEnquiryStatusIndOptions
    End Get
  End Property
  Private Shared moEnquiryResultIndOptions As IndicatorOptions
  Public Shared ReadOnly Property EnquiryResultIndOptions() As IndicatorOptions
    Get
      If moEnquiryResultIndOptions Is Nothing Then
        moEnquiryResultIndOptions = New IndicatorOptions
        moEnquiryResultIndOptions.Add("P", "Pending")
        moEnquiryResultIndOptions.Add("N", "No Record Found")
        moEnquiryResultIndOptions.Add("F", "Record Found")
        moEnquiryResultIndOptions.Add("M", "Multiple Records Found")
        moEnquiryResultIndOptions.Add("S", "Record Selected")
      End If
      Return moEnquiryResultIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberPropertyDeedEnquiry", ApplicationSettings)
    _Product._ProductID.LocalElement = _ProductID
    _SystemUser._SystemUserID.LocalElement = _SystemUserID
    _Subscriber._SubscriberID.LocalElement = _SubscriberID
    AddHandler Product.Changed, AddressOf Product_Changed
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub Product_Changed(ByVal Relationship As Relationship)
    BRProduct(Relationship, True, True)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"

  Public Sub Validate()
    Dim bAllowYN As Boolean = False
    Dim oException As Exception

    Select Case Me._ProductID.Value
      Case Constants.Product.Records.EnquiryPropertyDeed
        oException = New MissingValuesException("(ID No) or (Passport No / Other ID No) or (Surname and First Initial and Date of Birth)", MyBase.Caption, "")
        If Not Me.TitleDeedNo.IsEmpty Then
          bAllowYN = True
        End If
      Case Else
        bAllowYN = True
    End Select

    If Not bAllowYN Then
      Throw New BusinessRuleException(oException.Message)
    End If
  End Sub

  Public Sub QueueMatch()
    Validate()
    Me.EnquiryStatusInd.Value = Constants.SubscriberPropertyDeedEnquiry.Statuses.Queued
    Me.Save()
  End Sub

  Public Sub Match()
    Validate()
    Me.EnquiryStatusInd.Value = Constants.SubscriberPropertyDeedEnquiry.Statuses.Processing
    Me.Save()

    Dim oDataRow As DataRow
    Dim nEnquiryMatchingEngineID As Integer = EnquiryMatchingEngine.PropertyDeedMatch(Me.TitleDeedNo, Me._ProductID.Value, ApplicationSettings)
    Dim oString As Text.StringBuilder = New Text.StringBuilder

    oString.Append("insert into SubscriberPropertyDeedEnquiryResult(SubscriberID, SubscriberPropertyDeedEnquiryID, PropertyDeedID, DetailsViewedYN, PropertyDeedSelectedYN, TitleDeedNo, FirstInitial, SecondInitial, FirstName, Surname, BirthDate)" & vbNewLine)
    oString.Append("select " & Me._SubscriberID.Value & ", " & Me.SubscriberPropertyDeedEnquiryID.Value & ", edr.PropertyDeedID, 0, 0, d.TitleDeedNo, d.FirstInitial, d.SecondInitial, d.FirstName, d.Surname, d.BirthDate" & vbNewLine)
    oString.Append("  from EnquiryMatchingEnginePropertyDeedResult edr inner join EnquiryMatchingEnginePropertyDeed med")
    oString.Append("                                                  on edr.EnquiryMatchingEngineID = med.EnquiryMatchingEngineID" & vbNewLine)
    oString.Append("                                                 and edr.EnquiryMatchingEnginePropertyDeedID = med.EnquiryMatchingEnginePropertyDeedID" & vbNewLine)
    oString.Append("                                               inner join PropertyDeed d" & vbNewLine)
    oString.Append("                                                  on edr.PropertyDeedID = d.PropertyDeedID" & vbNewLine)
    oString.Append(" where med.EnquiryMatchingEngineID = " & ApplicationSettings.QueryBuilder.ToSQL(nEnquiryMatchingEngineID) & vbNewLine)
    oString.Append("   and edr.IgnoreRecordYN = 0")
    ApplicationSettings.ActionQuery(oString.ToString())

    oString = New Text.StringBuilder
    oString.Append("select count(edr.PropertyDeedID)" & vbNewLine)
    oString.Append("  from EnquiryMatchingEnginePropertyDeedResult edr inner join EnquiryMatchingEnginePropertyDeed med" & vbNewLine)
    oString.Append("                                                  on edr.EnquiryMatchingEngineID = med.EnquiryMatchingEngineID" & vbNewLine)
    oString.Append("                                                 and edr.EnquiryMatchingEnginePropertyDeedID = med.EnquiryMatchingEnginePropertyDeedID" & vbNewLine)
    oString.Append(" where med.EnquiryMatchingEngineID = " & ApplicationSettings.QueryBuilder.ToSQL(nEnquiryMatchingEngineID) & vbNewLine)
    oString.Append("   and edr.IgnoreRecordYN = 0")
    For Each oDataRow In ApplicationSettings.ResultQuery(oString.ToString()).Rows
      Select Case System.Convert.ToInt32(oDataRow(0))
        Case 0
          Me.EnquiryResultInd.Value = Constants.SubscriberPropertyDeedEnquiry.Results.NoRecordFound
        Case 1
          Me.EnquiryResultInd.Value = Constants.SubscriberPropertyDeedEnquiry.Results.RecordFound
        Case Else
          Me.EnquiryResultInd.Value = Constants.SubscriberPropertyDeedEnquiry.Results.MultipleRecordsFound
      End Select
    Next

    Me.EnquiryStatusInd.Value = Constants.SubscriberPropertyDeedEnquiry.Statuses.Completed
    Me.Save()
  End Sub
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Load()
    MyBase.Load()
    BRLoad()
  End Sub

  Public Overrides Sub Save()
    With ApplicationSettings
      Try
        Dim bIsNewYN As Boolean = Me.IsNew
        .BeginTransaction()
        BRBeforeSave(bIsNewYN)
        MyBase.Save()
        .CommitTransaction()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
    End With
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRLoad()
    Me.DisableElementsAndRelationship()
  End Sub

  Private Sub BRBeforeSave(ByVal IsNewYN As Boolean)
    If IsNewYN Then
      Me.EnquiryDate.Value = System.DateTime.Now
      Me.SystemUser.Instance = XDSAdministrationBL.SystemUser.GetLoggedInSystemUser(ApplicationSettings)

      Me.ResultTitleDeedNo.Value = Me.TitleDeedNo.Value
    End If
  End Sub

  Private Sub BRProduct(ByVal Relationship As Relationship, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    Dim oProduct As Product = Nothing

    If Not Relationship.IsEmpty Then
      oProduct = Relationship.Instance
    End If
    Utility.BRProductEnquiryPropertyDeed(oProduct, Me.TitleDeedNo, SetEnableDisableYN, PerformRulesYN)

    If PerformRulesYN Then
      If Not oProduct Is Nothing Then
        Dim oSubscriberProfile As SubscriberProfile = SubscriberProfile.GetSubscriberProfile(Me._SubscriberID.Value, ApplicationSettings)
        Dim oSubscriberProfileProduct As SubscriberProfileProduct = SubscriberProfileProduct.GetSubscriberProfileProduct(Me._SubscriberID.Value, oProduct.ProductID.Value, ApplicationSettings)

        If oSubscriberProfileProduct.OverrideDefaultPointValueYN.Value Then
          Me.ProductPointValue.Value = oSubscriberProfileProduct.OverridePointValue.Value
        Else
          Me.ProductPointValue.Value = oProduct.DefaultPointValue.Value
        End If
      End If
    End If
  End Sub
#End Region

#Region " Public Methods"
  Public Function GetSubscriberPropertyDeedEnquiryResult() As SubscriberPropertyDeedEnquiryResult
    If Me.EnquiryResultInd.Value = Constants.SubscriberPropertyDeedEnquiry.Results.RecordFound Or Me.EnquiryResultInd.Value = Constants.SubscriberPropertyDeedEnquiry.Results.RecordSelected Then
      Dim oList As List = Me._SubscriberPropertyDeedEnquiryResult_OwnMany.GridList
      Dim oDataTable As DataTable = oList.DataTable
      Dim oDataRow As DataRow = oDataTable.Rows(0)
      Dim oSubscriberPropertyDeedEnquiryResult As SubscriberPropertyDeedEnquiryResult = Me.SubscriberPropertyDeedEnquiryResult_OwnMany.NewInstance()

      oSubscriberPropertyDeedEnquiryResult._PropertyDeedID.Load(oDataRow("PropertyDeedID"))
      oSubscriberPropertyDeedEnquiryResult.Load()
      Return oSubscriberPropertyDeedEnquiryResult
    End If
    Return Nothing
  End Function
#End Region
End Class 'sembleWare: Part

Public Class SubscriberPropertyDeedEnquiryRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberPropertyDeedEnquiryID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberPropertyDeedEnquiry(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberPropertyDeedEnquiryID.ForeignElement = CType(Instance, SubscriberPropertyDeedEnquiry).SubscriberPropertyDeedEnquiryID
    _SubscriberID.ForeignElement = CType(Instance, SubscriberPropertyDeedEnquiry)._SubscriberID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function MultiplePropertyDeedGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberPropertyDeedEnquiry")
    oList.Columns.Add(New ListColumn("SubscriberPropertyDeedEnquiryID", "[A0SubscriberPropertyDeedEnquiry].[SubscriberPropertyDeedEnquiryID]", "SubscriberPropertyDeedEnquiryID", DataType.Long, Nothing, True, 0, False, 100, "Subscriber Property Deed Enquiry ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberPropertyDeedEnquiry].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("EnquiryDate", "[A0SubscriberPropertyDeedEnquiry].[EnquiryDate]", "EnquiryDate", DataType.DateTime, "g", False, -1, True, 100, "Enquiry Date"))
    oList.Columns.Add(New ListColumn("ResultTitleDeedNo", "[A0SubscriberPropertyDeedEnquiry].[ResultTitleDeedNo]", "ResultTitleDeedNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("EnquiryStatusInd", "[A0SubscriberPropertyDeedEnquiry].[EnquiryStatusInd]", "EnquiryStatusInd", DataType.String, Nothing, False, 0, True, 100, "Enquiry Status"))
    oList.Columns.Add(New ListColumn("EnquiryResultInd", "[A0SubscriberPropertyDeedEnquiry].[EnquiryResultInd]", "EnquiryResultInd", DataType.String, Nothing, False, 0, True, 100, "Enquiry Result"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberPropertyDeedEnquiry]  [A0SubscriberPropertyDeedEnquiry]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
