Imports System.Text
Imports sembleWare.Runtime

Namespace LoaderEngineManager
  Public Class Manager
#Region " Variables"
    Private moFileFormatDeployment As FileFormatDeployment
    Private moFileFormat As FileFormat
    Private msServerName As String
    Private msDatabaseName As String
    Private msUsername As String
    Private msPassword As String
    Private mApplicationSettings As ApplicationSettings
#End Region

#Region " Properties"
    Public ReadOnly Property FileFormatDeploymentID() As Long
      Get
        Return moFileFormatDeployment.FileFormatDeploymentID.Value
      End Get
    End Property

    Public Property FileFormat() As FileFormat
      Set(ByVal Value As FileFormat)
        moFileFormat = Value
      End Set
      Get
        Return moFileFormat
      End Get
    End Property

    Public Property FileFormatDeployment() As FileFormatDeployment
      Set(ByVal Value As FileFormatDeployment)
        moFileFormatDeployment = Value
      End Set
      Get
        Return moFileFormatDeployment
      End Get
    End Property

    Public Property ServerName() As String
      Get
        Return msServerName
      End Get
      Set(ByVal Value As String)
        msServerName = Value
      End Set
    End Property

    Public Property DatabaseName() As String
      Get
        Return msDatabaseName
      End Get
      Set(ByVal Value As String)
        msDatabaseName = Value
      End Set
    End Property

    Public Property Username() As String
      Get
        Return msUsername
      End Get
      Set(ByVal Value As String)
        msUsername = Value
      End Set
    End Property

    Public Property Password() As String
      Get
        Return msPassword
      End Get
      Set(ByVal Value As String)
        msPassword = Value
      End Set
    End Property

    Public Property ApplicationSettings() As ApplicationSettings
      Get
        Return mApplicationSettings
      End Get
      Set(ByVal Value As ApplicationSettings)
        mApplicationSettings = Value
      End Set
    End Property

    Public ReadOnly Property StagingTableName(ByVal MetaDatabase As MetaDatabase) As String
      Get
        Dim oStagingTable As SQLManager.StagingTable = New SQLManager.StagingTable(MetaDatabase, Me)

        Return oStagingTable.TableName
      End Get
    End Property

    Public ReadOnly Property StagingMultipleMatchTableName(ByVal MetaDatabase As MetaDatabase) As String
      Get
        Dim oStagingMultipleMatchTable As SQLManager.StagingMultipleMatchTable = New SQLManager.StagingMultipleMatchTable(MetaDatabase, Me)

        Return oStagingMultipleMatchTable.TableName
      End Get
    End Property

    Public ReadOnly Property MetaDatabaseList(ByVal MetaDatabaseTypeInd As String) As List
      Get
        Dim oList As List = moFileFormat._FileFormatMetaDatabase_OwnMany.GridList

        If Not MetaDatabaseTypeInd Is Nothing AndAlso Not MetaDatabaseTypeInd = "" Then
          oList.AndFilters.Add(New ListFilter("MetaDatabaseTypeInd", "MetaDatabaseTypeInd", MetaDatabaseTypeInd, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
        End If

        Return oList
      End Get
    End Property
#End Region

#Region " Constructor"
    Public Sub New(ByVal FileFormatDeployment As FileFormatDeployment, ByVal FileFormat As FileFormat, ByVal ApplicationSettings As ApplicationSettings)
      moFileFormatDeployment = FileFormatDeployment
      moFileFormat = FileFormat
      mApplicationSettings = ApplicationSettings
    End Sub

    Public Sub New(ByVal FileFormatDeployment As FileFormatDeployment, ByVal ServerName As String, ByVal DatabaseName As String, ByVal Username As String, ByVal Password As String, ByVal ApplicationSettings As ApplicationSettings)
      Me.New(FileFormatDeployment, Nothing, ServerName, DatabaseName, Username, Password, ApplicationSettings)
    End Sub

    Public Sub New(ByVal FileFormatDeployment As FileFormatDeployment, ByVal FileFormat As FileFormat, ByVal ServerName As String, ByVal DatabaseName As String, ByVal Username As String, ByVal Password As String, ByVal ApplicationSettings As ApplicationSettings)
      moFileFormatDeployment = FileFormatDeployment
      moFileFormat = FileFormat
      msServerName = ServerName
      msDatabaseName = DatabaseName
      msUsername = Username
      msPassword = Password
      mApplicationSettings = ApplicationSettings
    End Sub
#End Region

#Region " Methods"
    Public Sub RemovePreviousDeployments()
      Dim oXDSAdministrationRoot As XDSAdministrationRoot = New XDSAdministrationRoot(ApplicationSettings)
      Dim oList As List = oXDSAdministrationRoot._FileFormatDeployment_OwnMany.GridList
      Dim sDeploymentStatusInd As String = ApplicationSettings.QueryBuilder.ToSQL(Constants.FileFormatDeployment.Statuses.Completed) & ", " & ApplicationSettings.QueryBuilder.ToSQL(Constants.FileFormatDeployment.Statuses.Failed)
      Dim oDataRow As DataRow

      oList.AndFilters.Add(New ListFilter("DeploymentStatusInd", "DeploymentStatusInd", sDeploymentStatusInd, DataType.String, FilterType.In, FilterBlankBehavior.DisplayEmptyList, True))
      oList.AndFilters.Add(New ListFilter("FileFormatDeploymentID", "FileFormatDeploymentID", Me.FileFormatDeploymentID, DataType.Long, FilterType.NotEquals, FilterBlankBehavior.DisplayEmptyList, True))
      For Each oDataRow In oList.DataTable.Rows
        Dim oFileFormatDeployment As FileFormatDeployment = New FileFormatDeploymentRelationship(ApplicationSettings).NewInstance()
        oFileFormatDeployment.FileFormatDeploymentID.Load(oDataRow("FileFormatDeploymentID"))
        oFileFormatDeployment.Load()

        Me.FileFormatDeployment = oFileFormatDeployment
        If RemoveFileFormat() Then
          oFileFormatDeployment.DeploymentStatusInd.Value = Constants.FileFormatDeployment.Statuses.Removed
          oFileFormatDeployment.RemovedDate.Value = System.DateTime.Now
          oFileFormatDeployment.Save()
        End If
      Next
    End Sub

    Public Sub ValidateFileFormats()
      Dim oDataRow As DataRow
      Dim oQuery As StringBuilder = New StringBuilder
      Dim oMessage As StringBuilder = New StringBuilder
      Dim bIsValidYN As Boolean = True
      Dim bAddedCaptionYN As Boolean = False

      oQuery.Append("select mlp.FileFormatCode, ff.FileFormatDesc, mlp.MetaDatabaseCode, md.MetaDatabaseDesc, lpt.LoaderProcessTypeDesc" & vbNewLine)
      oQuery.Append("  from FileFormatMetaLoaderProcedure mlp inner join FileFormat ff" & vbNewLine)
      oQuery.Append("                                            on mlp.FileFormatCode = ff.FileFormatCode" & vbNewLine)
      oQuery.Append("                                         inner join MetaDatabase md" & vbNewLine)
      oQuery.Append("                                            on mlp.MetaDatabaseCode = md.MetaDatabaseCode" & vbNewLine)
      oQuery.Append("                                         inner join LoaderProcessType lpt" & vbNewLine)
      oQuery.Append("                                            on mlp.LoaderProcessTypeCode = lpt.LoaderProcessTypeCode" & vbNewLine)
      oQuery.Append(" where mlp.TemplateDetails is null" & vbNewLine)
      oQuery.Append(" group by mlp.FileFormatCode, mlp.MetaDatabaseCode, lpt.LoaderProcessTypeCode, md.MetaDatabaseDesc, ff.FileFormatDesc, lpt.LoaderProcessTypeDesc" & vbNewLine)
      oQuery.Append(" order by mlp.FileFormatCode, mlp.MetaDatabaseCode, lpt.LoaderProcessTypeCode" & vbNewLine)

      For Each oDataRow In moFileFormatDeployment.ApplicationSettings.ResultQuery(oQuery.ToString()).Rows
        If Not bAddedCaptionYN Then
          oMessage.Append("<b>The following loader procedures have not been specified:</b>" & vbNewLine)
          bAddedCaptionYN = True
        End If
        oMessage.Append("File Format: " & oDataRow("FileFormatCode") & " (" & oDataRow("FileFormatDesc") & ") - ")
        oMessage.Append("Meta Database: " & oDataRow("MetaDatabaseCode") & " (" & oDataRow("MetaDatabaseDesc") & ") - ")
        oMessage.Append("Loader Procedure: " & oDataRow("LoaderProcessTypeDesc") & vbNewLine)
        bIsValidYN = False
      Next

      bAddedCaptionYN = False
      oQuery = New StringBuilder
      oQuery.Append("select mdpp.FileFormatCode, ff.FileFormatDesc, mdpp.MetaDatabaseCode, md.MetaDatabaseDesc, dp.DataProcedureDesc" & vbNewLine)
      oQuery.Append("  from FileFormatMetaDataProcedureParameter mdpp inner join FileFormat ff" & vbNewLine)
      oQuery.Append("                                                    on mdpp.FileFormatCode = ff.FileFormatCode" & vbNewLine)
      oQuery.Append("                                                 inner join MetaDatabase md" & vbNewLine)
      oQuery.Append("                                                    on mdpp.MetaDatabaseCode = md.MetaDatabaseCode" & vbNewLine)
      oQuery.Append("                                                 inner join DataProcedure dp" & vbNewLine)
      oQuery.Append("                                                    on mdpp.DataProcedureID = dp.DataProcedureID" & vbNewLine)
      oQuery.Append("                                                 left join FileFormatField fff" & vbNewLine)
      oQuery.Append("                                                   on mdpp.ParameterFileFormatFieldID = fff.FileFormatFieldID" & vbNewLine)
      oQuery.Append("                                                 and mdpp.FileFormatCode = fff.FileFormatCode" & vbNewLine)
      oQuery.Append(" where mdpp.ParameterFileFormatFieldID is null" & vbNewLine)
      oQuery.Append(" group by mdpp.FileFormatCode, mdpp.MetaDatabaseCode, mdpp.FileFormatFieldID, ff.FileFormatDesc, mdpp.MetaDatabaseCode," & vbNewLine)
      oQuery.Append("          md.MetaDatabaseDesc, dp.DataProcedureDesc" & vbNewLine)
      oQuery.Append(" order by mdpp.FileFormatCode, mdpp.MetaDatabaseCode, dp.DataProcedureDesc, mdpp.FileFormatFieldID" & vbNewLine)

      For Each oDataRow In moFileFormatDeployment.ApplicationSettings.ResultQuery(oQuery.ToString()).Rows
        If Not bAddedCaptionYN Then
          oMessage.Append("<b>The following data procedures parameters have not been specified:</b>" & vbNewLine)
          bAddedCaptionYN = True
        End If
        oMessage.Append("File Format: " & oDataRow("FileFormatCode") & " (" & oDataRow("FileFormatDesc") & ") - ")
        oMessage.Append("Meta Database: " & oDataRow("MetaDatabaseCode") & " (" & oDataRow("MetaDatabaseDesc") & ") - ")
        oMessage.Append("Data Procedure: " & oDataRow("DataProcedureDesc") & vbNewLine)
        bIsValidYN = False
      Next

      If Not bIsValidYN Then
        Throw New BusinessRuleException(oMessage.ToString())
      End If
    End Sub

    Public Sub DeployFileFormats()
      Dim oXDSAdministrationRoot As XDSAdministrationRoot = New XDSAdministrationRoot(ApplicationSettings)
      Dim oList As List = oXDSAdministrationRoot._FileFormat_OwnMany.GridList()
      Dim oDataRow As DataRow
      Dim oSQLManager As SQLManager.Manager = New SQLManager.Manager(Nothing, Nothing, Me)

      ValidateFileFormats()

      oSQLManager.DeployNonDatabaseSpecificFunctions()

      oList.AndFilters.Add(New ListFilter("FileFormatTypeCode", "FileFormatTypeCode", Constants.FileFormat.Types.Loader, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      For Each oDataRow In oList.DataTable.Rows
        Dim oFileFormat As FileFormat = New FileFormatRelationship(ApplicationSettings).NewInstance()
        oFileFormat.FileFormatCode.Load(oDataRow("FileFormatCode"))
        oFileFormat.Load()

        moFileFormat = oFileFormat
        DeployFileFormat()
      Next
    End Sub

    Public Sub DeployLoaderFileFormats(ByVal LoaderID As Long)
      Dim oLoader As Loader = New LoaderRelationship(ApplicationSettings).NewInstance()
      oLoader.LoaderID.Load(LoaderID)
      oLoader.Load()

      Dim oFileFormat As FileFormat = oLoader.FileFormat.Instance
      Dim oList As List = oFileFormat._FileFormatMetaDatabase_OwnMany.GridList()
      Dim oDataRow As DataRow

      For Each oDataRow In oList.DataTable.Rows
        Dim oMetaDatabase As MetaDatabase = New MetaDatabaseRelationship(Me.ApplicationSettings).NewInstance()
        oMetaDatabase.MetaDatabaseCode.Load(oDataRow("MetaDatabaseCode"))
        oMetaDatabase.Load()

        Dim oSQLManager As SQLManager.Manager = New SQLManager.Manager(oMetaDatabase, oLoader.FileFormatDeployment.Instance, Me)
        oSQLManager.DeployLoaderStagingTable(LoaderID)
        oSQLManager.DeployLoaderStoredProcedures(LoaderID)
      Next
    End Sub

    Public Sub ExecuteDataLoad(ByVal LoaderID As Long, ByVal LoaderThreadCode As String)
      Try
        LoaderProcess.UpdateLoaderProcessStatus(LoaderID, Constants.LoaderProcess.Types.DataLoad, Constants.LoaderProcess.Statuses.Processing, ApplicationSettings)
        LoaderThread.UpdateLoaderThread(LoaderThreadCode, True, LoaderID, Constants.LoaderProcess.Types.DataLoad, ApplicationSettings)
        LoaderProcessHistory.AddLoaderProcessHistory(LoaderID, Constants.LoaderProcess.Types.DataLoad, Constants.LoaderProcess.Statuses.Processing, Nothing, Nothing, "", mApplicationSettings)

        Dim oDataTable As DataTable = MetaDatabaseList("").DataTable
        Dim oDataRow As DataRow
        Dim oLoader As Loader = New LoaderRelationship(ApplicationSettings).NewInstance
        oLoader.LoaderID.Load(LoaderID)
        oLoader.Load()

        Select Case moFileFormat.FileTypeInd.Value
          Case Constants.FileFormat.FileTypes.TextFile
            Dim oDTSManager As DTSManager.Manager = New DTSManager.Manager(Me)
            oDTSManager.ExecuteDTSImportPackage(oLoader)
          Case Constants.FileFormat.FileTypes.XMLFile
            Dim oXMLManager As XMLManager.Manager = New XMLManager.Manager(Me)
            oXMLManager.ExecuteImport(oLoader)
        End Select

        For Each oDataRow In oDataTable.Rows
          Dim oMetaDatabase As MetaDatabase = New MetaDatabaseRelationship(Me.ApplicationSettings).NewInstance()
          oMetaDatabase.MetaDatabaseCode.Load(oDataRow("MetaDatabaseCode"))
          oMetaDatabase.Load()

          Dim lTotalRecordCount As Long = 0
          Select Case oMetaDatabase.MetaDatabaseTypeInd.Value
            Case Constants.MetaDatabase.Types.System
              Dim oStagingTable As SQLManager.StagingTable = New SQLManager.StagingTable(oMetaDatabase, Me)
              lTotalRecordCount = oStagingTable.RecordCount(LoaderID)
            Case Constants.MetaDatabase.Types.Repository
              Dim oRepositoryTable As SQLManager.RepositoryTable = New SQLManager.RepositoryTable(oMetaDatabase, Me)
              lTotalRecordCount = oRepositoryTable.RecordCount(LoaderID)
          End Select
          LoaderProcessMetaDatabase.AddLoaderProcessMetaDatabase(LoaderID, Constants.LoaderProcess.Types.DataLoad, oMetaDatabase, 100, lTotalRecordCount, ApplicationSettings)
        Next

        LoaderProcess.UpdateLoaderProcessStatus(LoaderID, Constants.LoaderProcess.Types.DataLoad, Constants.LoaderProcess.Statuses.Completed, ApplicationSettings)
        LoaderThread.UpdateLoaderThread(LoaderThreadCode, False, LoaderID, Constants.LoaderProcess.Types.DataLoad, ApplicationSettings)
        LoaderProcessHistory.AddLoaderProcessHistory(LoaderID, Constants.LoaderProcess.Types.DataLoad, Constants.LoaderProcess.Statuses.Completed, Nothing, Nothing, "", mApplicationSettings)
      Catch oException As Exception
        LoaderProcess.UpdateLoaderProcessStatus(LoaderID, Constants.LoaderProcess.Types.DataLoad, Constants.LoaderProcess.Statuses.Failed, ApplicationSettings)
        LoaderThread.UpdateLoaderThread(LoaderThreadCode, False, LoaderID, Constants.LoaderProcess.Types.DataLoad, ApplicationSettings)
        LoaderProcessHistory.AddLoaderProcessHistory(LoaderID, Constants.LoaderProcess.Types.DataLoad, Constants.LoaderProcess.Statuses.Failed, Nothing, oException, "", mApplicationSettings)
      End Try
    End Sub

    Public Sub ExecuteDataValidation(ByVal LoaderID As Long, ByVal LoaderThreadCode As String)
      ExecuteLoaderProcess(LoaderID, XDSAdministrationBL.Constants.LoaderProcess.Types.DataValid, LoaderThreadCode)
    End Sub

    Public Sub ExecuteDataMatching(ByVal LoaderID As Long, ByVal LoaderThreadCode As String)
      ExecuteLoaderProcess(LoaderID, XDSAdministrationBL.Constants.LoaderProcess.Types.DataMatch, LoaderThreadCode)
    End Sub

    Public Sub ExecuteDataUpdate(ByVal LoaderID As Long, ByVal LoaderThreadCode As String)
      ExecuteLoaderProcess(LoaderID, XDSAdministrationBL.Constants.LoaderProcess.Types.DataUpdate, LoaderThreadCode)
    End Sub

    Public Sub ExecuteDataUndo(ByVal LoaderID As Long, ByVal LoaderThreadCode As String)
      ExecuteLoaderProcess(LoaderID, XDSAdministrationBL.Constants.LoaderProcess.Types.DataUndo, LoaderThreadCode)
    End Sub

    Public Function ParseFileFormatFieldDesc(ByVal FileFormatFieldDesc As String) As String
      Dim nCounter As Integer
      Dim sFileFormatFieldDesc As String = ""

      For nCounter = 0 To FileFormatFieldDesc.Length - 1
        If Char.IsLetterOrDigit(FileFormatFieldDesc.Chars(nCounter)) Then
          sFileFormatFieldDesc &= FileFormatFieldDesc.Chars(nCounter)
        End If
      Next

      Return sFileFormatFieldDesc
    End Function
#End Region

#Region " Helper Functions"
    Private Function RemoveFileFormat() As Boolean
      Dim oDataRow As DataRow
      Dim oDTSManager As DTSManager.Manager = New DTSManager.Manager(Me)
      Dim oSQLManager As SQLManager.Manager
      Dim bRemoveYN As Boolean = False
      Dim oQuery As StringBuilder = New StringBuilder
      Dim nNoOfLoaderFileFormatDeployments As Integer = 0
      Dim nNoOfCompletedDataUpdateLoaderProcesses As Integer = 0

      oQuery.Append("select count(LoaderID)")
      oQuery.Append("  from Loader")
      oQuery.Append(" where FileFormatDeploymentID = " & Me.ApplicationSettings.QueryBuilder.ToSQL(FileFormatDeployment.FileFormatDeploymentID.Value))
      For Each oDataRow In Me.ApplicationSettings.ResultQuery(oQuery.ToString()).Rows
        nNoOfLoaderFileFormatDeployments = System.Convert.ToInt32(oDataRow(0))
      Next

      oQuery = New StringBuilder
      oQuery.Append("select count(l.LoaderID)")
      oQuery.Append("  from LoaderProcess lp inner join Loader l")
      oQuery.Append("                           on lp.LoaderID = l.LoaderID")
      oQuery.Append(" where l.FileFormatDeploymentID = " & Me.ApplicationSettings.QueryBuilder.ToSQL(FileFormatDeployment.FileFormatDeploymentID.Value))
      oQuery.Append("   and lp.LoaderProcessTypeCode = " & Me.ApplicationSettings.QueryBuilder.ToSQL(Constants.LoaderProcess.Types.DataUpdate))
      oQuery.Append("   and lp.LoaderProcessStatusCode = " & Me.ApplicationSettings.QueryBuilder.ToSQL(Constants.LoaderProcess.Statuses.Completed))
      For Each oDataRow In Me.ApplicationSettings.ResultQuery(oQuery.ToString()).Rows
        nNoOfCompletedDataUpdateLoaderProcesses = System.Convert.ToInt32(oDataRow(0))
      Next

      If nNoOfLoaderFileFormatDeployments = nNoOfCompletedDataUpdateLoaderProcesses Then
        For Each oDataRow In MetaDatabaseList(Constants.MetaDatabase.Types.System).DataTable.Rows
          Dim oMetaDatabase As MetaDatabase = New MetaDatabaseRelationship(Me.ApplicationSettings).NewInstance()
          oMetaDatabase.MetaDatabaseCode.Load(oDataRow("MetaDatabaseCode"))
          oMetaDatabase.Load()

          oSQLManager = New SQLManager.Manager(oMetaDatabase, FileFormatDeployment, Me)
          If Not oSQLManager.IsDataInStagingTable Then
            bRemoveYN = True
            oSQLManager.RemoveDatabaseSpecificFunctions()
            oSQLManager.RemoveStagingTable()
            oSQLManager.RemoveStoredProcedures()

            Select Case Me.FileFormat.FileTypeInd.Value
              Case Constants.FileFormat.FileTypes.TextFile
                oDTSManager.RemoveExportPackage(oMetaDatabase)
            End Select
          End If
        Next

        If bRemoveYN Then
          oSQLManager = New SQLManager.Manager(Nothing, FileFormatDeployment, Me)
          oSQLManager.RemoveNonDatabaseSpecificFunctions()
          Select Case Me.FileFormat.FileTypeInd.Value
            Case Constants.FileFormat.FileTypes.TextFile
              oDTSManager.RemoveImportPackages()
          End Select
        End If
      End If

      Return bRemoveYN
    End Function

    Private Sub DeployFileFormat()
      Dim oDataRow As DataRow
      Dim oDTSManager As DTSManager.Manager = New DTSManager.Manager(Me)

      Select Case Me.FileFormat.FileTypeInd.Value
        Case Constants.FileFormat.FileTypes.TextFile
          oDTSManager.DeployImportPackages()
      End Select

      For Each oDataRow In MetaDatabaseList(Nothing).DataTable.Rows
        Dim oMetaDatabase As MetaDatabase = New MetaDatabaseRelationship(Me.ApplicationSettings).NewInstance()
        oMetaDatabase.MetaDatabaseCode.Load(oDataRow("MetaDatabaseCode"))
        oMetaDatabase.Load()

        Dim oSQLManager As SQLManager.Manager = New SQLManager.Manager(oMetaDatabase, moFileFormatDeployment, Me)
        oSQLManager.DeployDatabaseSpecificFunctions()
        oSQLManager.DeployRepositoryTable()
        oSQLManager.DeployStoredProcedures()

        Select Case Me.FileFormat.FileTypeInd.Value
          Case Constants.FileFormat.FileTypes.TextFile
            oDTSManager.DeployExportPackage(oMetaDatabase)
        End Select
      Next
    End Sub

    Private Sub ExecuteLoaderProcess(ByVal LoaderID As Long, ByVal LoaderProcessTypeCode As String, ByVal LoaderThreadCode As String)
      Try
        LoaderProcess.UpdateLoaderProcessStatus(LoaderID, LoaderProcessTypeCode, Constants.LoaderProcess.Statuses.Processing, ApplicationSettings)
        LoaderThread.UpdateLoaderThread(LoaderThreadCode, True, LoaderID, LoaderProcessTypeCode, ApplicationSettings)
        LoaderProcessHistory.AddLoaderProcessHistory(LoaderID, LoaderProcessTypeCode, Constants.LoaderProcess.Statuses.Processing, Nothing, Nothing, "", mApplicationSettings)

        Dim oDataRow As DataRow

        For Each oDataRow In MetaDatabaseList(Constants.MetaDatabase.Types.System).DataTable.Rows
          Dim oMetaDatabase As MetaDatabase = New MetaDatabaseRelationship(Me.ApplicationSettings).NewInstance()
          oMetaDatabase.MetaDatabaseCode.Load(oDataRow("MetaDatabaseCode"))
          oMetaDatabase.Load()

          Try
            Dim oSQLManager As SQLManager.Manager = New SQLManager.Manager(oMetaDatabase, moFileFormatDeployment, Me)

            LoaderProcessMetaDatabase.AddLoaderProcessMetaDatabase(LoaderID, LoaderProcessTypeCode, oMetaDatabase, ApplicationSettings)
            LoaderProcessHistory.AddLoaderProcessHistory(LoaderID, LoaderProcessTypeCode, Constants.LoaderProcess.Statuses.Processing, oMetaDatabase, Nothing, "", mApplicationSettings)

            Select Case LoaderProcessTypeCode
              Case XDSAdministrationBL.Constants.LoaderProcess.Types.DataValid
                oSQLManager.ExecuteDataValidation(LoaderID)

                Dim oLoader As Loader = New LoaderRelationship(ApplicationSettings).NewInstance
                oLoader.LoaderID.Load(LoaderID)
                oLoader.Load()
                
                Select Case moFileFormat.FileTypeInd.Value
                  Case Constants.FileFormat.FileTypes.TextFile
                    Dim oDTSManager As DTSManager.Manager = New DTSManager.Manager(Me)
                    oDTSManager.ExecuteDTSExportPackage(oLoader, oMetaDatabase)
                  Case Constants.FileFormat.FileTypes.XMLFile
                    Dim oXMLManager As XMLManager.Manager = New XMLManager.Manager(Me)
                    oXMLManager.ExecuteExport(oLoader, oMetaDatabase)
                End Select
              Case XDSAdministrationBL.Constants.LoaderProcess.Types.DataMatch
                oSQLManager.ExecuteDataMatching(LoaderID)
              Case XDSAdministrationBL.Constants.LoaderProcess.Types.DataUpdate
                Dim oStagingTable As SQLManager.StagingTable = New SQLManager.StagingTable(oMetaDatabase, Me)
                oSQLManager.ExecuteDataUpdate(LoaderID)
                oSQLManager.RemoveLoaderStoredProcedures(LoaderID)
                oStagingTable.DropTable(LoaderID)

                Dim oStagingMultipleMatchTable As SQLManager.StagingMultipleMatchTable = New SQLManager.StagingMultipleMatchTable(oMetaDatabase, Me)
                oStagingMultipleMatchTable.DropTable(LoaderID)
              Case XDSAdministrationBL.Constants.LoaderProcess.Types.DataUndo
                oSQLManager.ExecuteDataUndo(LoaderID)
            End Select

            LoaderProcessMetaDatabase.UpdateLoaderProcessMetaDatabaseCompletion(LoaderID, LoaderProcessTypeCode, oMetaDatabase, 100, ApplicationSettings)
            LoaderProcessHistory.AddLoaderProcessHistory(LoaderID, LoaderProcessTypeCode, Constants.LoaderProcess.Statuses.Completed, oMetaDatabase, Nothing, "", mApplicationSettings)
          Catch oException As Exception
            LoaderProcessHistory.AddLoaderProcessHistory(LoaderID, LoaderProcessTypeCode, Constants.LoaderProcess.Statuses.Failed, oMetaDatabase, oException, "", mApplicationSettings)
            Throw oException
          End Try
        Next

        LoaderProcess.UpdateLoaderProcessStatus(LoaderID, LoaderProcessTypeCode, Constants.LoaderProcess.Statuses.Completed, ApplicationSettings)
        LoaderThread.UpdateLoaderThread(LoaderThreadCode, False, LoaderID, LoaderProcessTypeCode, ApplicationSettings)
        LoaderProcessHistory.AddLoaderProcessHistory(LoaderID, LoaderProcessTypeCode, Constants.LoaderProcess.Statuses.Completed, Nothing, Nothing, "", mApplicationSettings)
      Catch oException As Exception
        LoaderProcess.UpdateLoaderProcessStatus(LoaderID, LoaderProcessTypeCode, Constants.LoaderProcess.Statuses.Failed, ApplicationSettings)
        LoaderThread.UpdateLoaderThread(LoaderThreadCode, False, LoaderID, LoaderProcessTypeCode, ApplicationSettings)
        LoaderProcessHistory.AddLoaderProcessHistory(LoaderID, LoaderProcessTypeCode, Constants.LoaderProcess.Statuses.Failed, Nothing, oException, "", mApplicationSettings)
      End Try
    End Sub
#End Region
  End Class
End Namespace