Imports sembleWare.Runtime
Imports System
Public Class SAFPSProtectiveRegistrationConflict 'sembleWare: Part
    Inherits CustomDBPart

#Region " Elements & Relationships"

    'sembleWare: Elements Start - Do Not Modify
    Public ReadOnly SAFPSProtectiveRegistration As Relationship = New SAFPSProtectiveRegistrationRelationship("SAFPSProtectiveRegistration", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
    Public ReadOnly ConflictSAFPSProtectiveRegistration As Relationship = New SAFPSProtectiveRegistrationRelationship("ConflictSAFPSProtectiveRegistration", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
    'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

    'sembleWare: Foreign Items Start - Do Not Modify
    Public ReadOnly _SAFPSProtectiveRegistrationID As New IntegerElement("SAFPSProtectiveRegistrationID", Me, True)
    Public ReadOnly _ConflictSAFPSProtectiveRegistrationID As New IntegerElement("ConflictSAFPSProtectiveRegistrationID", Me, True)
    Public ReadOnly _SAFPSProtectiveRegistration As SAFPSProtectiveRegistrationRelationship = SAFPSProtectiveRegistration
    Public ReadOnly _ConflictSAFPSProtectiveRegistration As SAFPSProtectiveRegistrationRelationship = ConflictSAFPSProtectiveRegistration
    'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

    'sembleWare: Indicator Options Start - Do Not Modify
    'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

    Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

        'sembleWare: Constructor Start - Do Not Modify
        MyBase.New("SAFPSProtectiveRegistrationConflict", ApplicationSettings)
        _SAFPSProtectiveRegistration._SAFPSProtectiveRegistrationID.LocalElement = _SAFPSProtectiveRegistrationID
        _ConflictSAFPSProtectiveRegistration._SAFPSProtectiveRegistrationID.LocalElement = _ConflictSAFPSProtectiveRegistrationID
        'sembleWare: Constructor End - Do Not Modify

    End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
    Public Sub Merge()
        SAFPSProtectiveRegistrationConflict.SAFPSProtectiveRegistrationMerge(Me._SAFPSProtectiveRegistrationID.Value, Me._ConflictSAFPSProtectiveRegistrationID.Value, GlobalSettings.SystemUser.LoggedInSystemUserID, ApplicationSettings)
    End Sub

    Public Sub MergeAll()
        SAFPSProtectiveRegistrationConflict.SAFPSProtectiveRegistrationMergeAll(Me._SAFPSProtectiveRegistrationID.Value, GlobalSettings.SystemUser.LoggedInSystemUserID, ApplicationSettings)
    End Sub
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes
#Region " Shared Procedures"
    Public Shared Sub SAFPSProtectiveRegistrationMerge(ByVal SAFPSProtectiveRegistrationID As Integer, ByVal MergeToSAFPSProtectiveRegistrationID As Integer, ByVal ActionedBySystemUserID As Integer, ByVal ApplicationSettings As ApplicationSettings)
        Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand

        oCommand.CommandType = CommandType.StoredProcedure
        oCommand.CommandText = "spSAFPSProtectiveRegistration_U_Merge"

        oCommand.Parameters.Add("@SAFPSProtectiveRegistrationID", SqlDbType.Int)
        oCommand.Parameters.Add("@MergeToSAFPSProtectiveRegistrationID", SqlDbType.Int)
        oCommand.Parameters.Add("@ActionedBySystemUserID", SqlDbType.Int)

        oCommand.Parameters("@SAFPSProtectiveRegistrationID").Value = SAFPSProtectiveRegistrationID
        oCommand.Parameters("@MergeToSAFPSProtectiveRegistrationID").Value = MergeToSAFPSProtectiveRegistrationID
        oCommand.Parameters("@ActionedBySystemUserID").Value = ActionedBySystemUserID

        ApplicationSettings.ActionQuery(oCommand)
    End Sub

    Public Shared Sub SAFPSProtectiveRegistrationMergeAll(ByVal SAFPSProtectiveRegistrationID As Integer, ByVal ActionedBySystemUserID As Integer, ByVal ApplicationSettings As ApplicationSettings)
        Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand

        oCommand.CommandType = CommandType.StoredProcedure
        oCommand.CommandText = "spSAFPSProtectiveRegistration_U_MergeAll"

        oCommand.Parameters.Add("@SAFPSProtectiveRegistrationID", SqlDbType.Int)
        oCommand.Parameters.Add("@ActionedBySystemUserID", SqlDbType.Int)

        oCommand.Parameters("@SAFPSProtectiveRegistrationID").Value = SAFPSProtectiveRegistrationID
        oCommand.Parameters("@ActionedBySystemUserID").Value = ActionedBySystemUserID

        ApplicationSettings.ActionQuery(oCommand)

    End Sub
#End Region

End Class 'sembleWare: Part

Public Class SAFPSProtectiveRegistrationConflictRelationship 'sembleWare: Relationship
    Inherits Relationship

#Region " Relationship Code"

    'sembleWare: Relationship Start - Do Not Modify
    Public ReadOnly _SAFPSProtectiveRegistrationID As ForeignKey = New ForeignKey(Me)
    Public ReadOnly _ConflictSAFPSProtectiveRegistrationID As ForeignKey = New ForeignKey(Me)

    Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
        MyBase.New(ApplicationSettings)
    End Sub

    Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
        MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
    End Sub

    Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
        MyBase.New(Name, Caption, OwnedByName, Owner)
    End Sub

    Public Overrides Function CreateObject() As sembleWare.Runtime.Part
        Return New SAFPSProtectiveRegistrationConflict(ApplicationSettings)
    End Function

    Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
        _SAFPSProtectiveRegistrationID.ForeignElement = CType(Instance, SAFPSProtectiveRegistrationConflict)._SAFPSProtectiveRegistrationID
        _ConflictSAFPSProtectiveRegistrationID.ForeignElement = CType(Instance, SAFPSProtectiveRegistrationConflict)._ConflictSAFPSProtectiveRegistrationID
    End Sub

    'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

    Public Function GridList() As sembleWare.Runtime.List
        'sembleWare: List Declaration Start - Do Not Modify
        Dim oList As DBList = New DBList(Me, "A0SAFPSProtectiveRegistrationConflict")
        oList.Columns.Add(New ListColumn("SAFPSProtectiveRegistrationID", "[A0SAFPSProtectiveRegistrationConflict].[SAFPSProtectiveRegistrationID]", "SAFPSProtectiveRegistrationID", DataType.Integer, Nothing, True, 0, False, 100, "SAFPSProtective Registration ID"))
        oList.Columns.Add(New ListColumn("ConflictSAFPSProtectiveRegistrationID", "[A0SAFPSProtectiveRegistrationConflict].[ConflictSAFPSProtectiveRegistrationID]", "ConflictSAFPSProtectiveRegistrationID", DataType.Integer, Nothing, True, 0, False, 100, "Conflict SAFPSProtective Registration ID"))
        oList.Columns.Add(New ListColumn("SAFPSProtectiveRegistrationID1", "[A1ConflictSAFPSProtectiveRegistration].[SAFPSProtectiveRegistrationID]", "SAFPSProtectiveRegistrationID", DataType.Long, Nothing, False, 1, True, 100, "SAFPS Protective Registration ID"))
        oList.Columns.Add(New ListColumn("IDNo", "[A1ConflictSAFPSProtectiveRegistration].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
        oList.Columns.Add(New ListColumn("PassportNo", "[A1ConflictSAFPSProtectiveRegistration].[PassportNo]", "PassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No"))
        oList.Columns.Add(New ListColumn("Surname", "[A1ConflictSAFPSProtectiveRegistration].[Surname]", "Surname", DataType.String, Nothing, False, 0, True, 200, "Surname"))
        oList.Columns.Add(New ListColumn("FirstName", "[A1ConflictSAFPSProtectiveRegistration].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
        oList.Columns.Add(New ListColumn("SecondName", "[A1ConflictSAFPSProtectiveRegistration].[SecondName]", "SecondName", DataType.String, Nothing, False, 0, True, 100, "Second Name"))
        oList.Columns.Add(New ListColumn("BirthDate", "[A1ConflictSAFPSProtectiveRegistration].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Birth Date"))
        oList.SelectStatement = "select"
        oList.FromClause = "([SAFPSProtectiveRegistrationConflict]  [A0SAFPSProtectiveRegistrationConflict]" + _
               "    join [SAFPSProtectiveRegistration]  [A1ConflictSAFPSProtectiveRegistration] on [A0SAFPSProtectiveRegistrationConflict].[ConflictSAFPSProtectiveRegistrationID] = [A1ConflictSAFPSProtectiveRegistration].[SAFPSProtectiveRegistrationID])"
        oList.WhereClause = ""
        oList.ApplyRelationshipLimits()
        'sembleWare: List Declaration End - Do Not Modify
        Return oList
    End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
