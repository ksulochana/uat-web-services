Imports sembleWare.Runtime
Imports System
Public Class HomeAffairsConflict 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly HomeAffairs As Relationship = New HomeAffairsRelationship("HomeAffairs", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly ConflictHomeAffairs As Relationship = New HomeAffairsRelationship("ConflictHomeAffairs", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _HomeAffairsID As New IntegerElement("HomeAffairsID", Me, True)
  Public ReadOnly _ConflictHomeAffairsID As New IntegerElement("ConflictHomeAffairsID", Me, True)
  Public ReadOnly _HomeAffairs As HomeAffairsRelationship = HomeAffairs
  Public ReadOnly _ConflictHomeAffairs As HomeAffairsRelationship = ConflictHomeAffairs
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("HomeAffairsConflict", ApplicationSettings)
    _HomeAffairs._HomeAffairsID.LocalElement = _HomeAffairsID
    _ConflictHomeAffairs._HomeAffairsID.LocalElement = _ConflictHomeAffairsID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"

  Public Sub Merge()
    Throw New BusinessRuleException("This feature has not been requested by XDS!")
    HomeAffairsConflict.HomeAffairsMerge(Me._HomeAffairsID.Value, Me._ConflictHomeAffairsID.Value, GlobalSettings.SystemUser.LoggedInSystemUserID, ApplicationSettings)
  End Sub

  Public Sub MergeAll()
    Throw New BusinessRuleException("This feature has not been requested by XDS!")
    HomeAffairsConflict.HomeAffairsMergeAll(Me._HomeAffairsID.Value, GlobalSettings.SystemUser.LoggedInSystemUserID, ApplicationSettings)
  End Sub

#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Shared Procedures"
#Region " Shared Procedures"
  Public Shared Sub HomeAffairsMerge(ByVal HomeAffairsID As Integer, ByVal MergeToHomeAffairsID As Integer, ByVal ActionedBySystemUserID As Integer, ByVal ApplicationSettings As ApplicationSettings)
    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand

    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spHomeAffairs_U_Merge"

    oCommand.Parameters.Add("@HomeAffairsID", SqlDbType.Int)
    oCommand.Parameters.Add("@MergeToHomeAffairsID", SqlDbType.Int)
    oCommand.Parameters.Add("@ActionedBySystemUserID", SqlDbType.Int)

    oCommand.Parameters("@HomeAffairsID").Value = HomeAffairsID
    oCommand.Parameters("@MergeToHomeAffairsID").Value = MergeToHomeAffairsID
    oCommand.Parameters("@ActionedBySystemUserID").Value = ActionedBySystemUserID

    ApplicationSettings.ActionQuery(oCommand)
  End Sub

  Public Shared Sub HomeAffairsMergeAll(ByVal HomeAffairsID As Integer, ByVal ActionedBySystemUserID As Integer, ByVal ApplicationSettings As ApplicationSettings)
    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand

    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spHomeAffairsConflict_U_MergeAll"

    oCommand.Parameters.Add("@HomeAffairsID", SqlDbType.Int)
    oCommand.Parameters.Add("@ActionedBySystemUserID", SqlDbType.Int)

    oCommand.Parameters("@HomeAffairsID").Value = HomeAffairsID
    oCommand.Parameters("@ActionedBySystemUserID").Value = ActionedBySystemUserID

    ApplicationSettings.ActionQuery(oCommand)
  End Sub
#End Region
#End Region
End Class 'sembleWare: Part

Public Class HomeAffairsConflictRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _HomeAffairsID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _ConflictHomeAffairsID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New HomeAffairsConflict(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _HomeAffairsID.ForeignElement = CType(Instance, HomeAffairsConflict)._HomeAffairsID
    _ConflictHomeAffairsID.ForeignElement = CType(Instance, HomeAffairsConflict)._ConflictHomeAffairsID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0HomeAffairsConflict")
    oList.Columns.Add(New ListColumn("HomeAffairsID", "[A0HomeAffairsConflict].[HomeAffairsID]", "HomeAffairsID", DataType.Integer, Nothing, True, 0, False, 100, "Home Affairs ID"))
    oList.Columns.Add(New ListColumn("ConflictHomeAffairsID", "[A0HomeAffairsConflict].[ConflictHomeAffairsID]", "ConflictHomeAffairsID", DataType.Integer, Nothing, True, 0, False, 100, "Conflict Home Affairs ID"))
    oList.Columns.Add(New ListColumn("HomeAffairsID1", "[A1ConflictHomeAffairs].[HomeAffairsID]", "HomeAffairsID", DataType.Long, Nothing, False, 1, True, 100, "Home Affairs ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A1ConflictHomeAffairs].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("FirstName", "[A1ConflictHomeAffairs].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("SecondName", "[A1ConflictHomeAffairs].[SecondName]", "SecondName", DataType.String, Nothing, False, 0, True, 100, "Second Name"))
    oList.Columns.Add(New ListColumn("ThirdName", "[A1ConflictHomeAffairs].[ThirdName]", "ThirdName", DataType.String, Nothing, False, 0, True, 100, "Third Name"))
    oList.Columns.Add(New ListColumn("Surname", "[A1ConflictHomeAffairs].[Surname]", "Surname", DataType.String, Nothing, False, 0, True, 100, "Surname"))
    oList.Columns.Add(New ListColumn("HomeAffairsRunYN", "[A1ConflictHomeAffairs].[HomeAffairsRunYN]", "HomeAffairsRunYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Home Affairs Run?"))
    oList.SelectStatement = "select"
    oList.FromClause = "([HomeAffairsConflict]  [A0HomeAffairsConflict]" + _
           "    join [HomeAffairs]  [A1ConflictHomeAffairs] on [A0HomeAffairsConflict].[ConflictHomeAffairsID] = [A1ConflictHomeAffairs].[HomeAffairsID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
