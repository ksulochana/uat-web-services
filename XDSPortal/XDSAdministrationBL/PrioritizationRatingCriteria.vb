Imports sembleWare.Runtime
Imports System
Public Class PrioritizationRatingCriteria 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly PrioritizationRatingCriteriaID As New IdentityElement("PrioritizationRatingCriteriaID", Me, True, Nothing, Nothing)
  Public ReadOnly RatingCode As New StringElement("RatingCode", Me, False, True, True, True, True, 10, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RatingDesc As New StringElement("RatingDesc", Me, False, True, True, True, True, 50, "Rating Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly OperatorTypeInd As New IndicatorElement("OperatorTypeInd", Me, False, False, True, True, True, True, "Operator Type", Nothing, PrioritizationRatingCriteria.OperatorTypeIndOptions, Nothing)
  Public ReadOnly StartDuration As New IntegerElement("StartDuration", Me, False, True, True, True, True, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly EndDuration As New IntegerElement("EndDuration", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PrioritizationRating As Relationship = New PrioritizationRatingRelationship("PrioritizationRating", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _PrioritizationRatingID As New IntegerElement("PrioritizationRatingID", Me, True)
  Public ReadOnly _PrioritizationRating As PrioritizationRatingRelationship = PrioritizationRating
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moOperatorTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property OperatorTypeIndOptions() As IndicatorOptions
    Get
      If moOperatorTypeIndOptions Is Nothing Then
        moOperatorTypeIndOptions = New IndicatorOptions
        moOperatorTypeIndOptions.Add("L", "Less than")
        moOperatorTypeIndOptions.Add("E", "Equals")
        moOperatorTypeIndOptions.Add("G", "Greater than")
        moOperatorTypeIndOptions.Add("B", "Between")
      End If
      Return moOperatorTypeIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("PrioritizationRatingCriteria", ApplicationSettings)
    _PrioritizationRating._PrioritizationRatingID.LocalElement = _PrioritizationRatingID
    AddHandler OperatorTypeInd.Changed, AddressOf OperatorTypeInd_Changed
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub OperatorTypeInd_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BROperatorTypeInd(Element, True, True)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreateLoad()
  End Sub

  Public Overrides Sub Load()
    MyBase.Load()
    BRCreateLoad()
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreateLoad()
    BROperatorTypeInd(Me.OperatorTypeInd, True, False)
  End Sub

  Private Sub BROperatorTypeInd(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If SetEnableDisableYN Then
      Me.EndDuration.Enabled = False
      Me.EndDuration.Mandatory = False
      Select Case Element.ValueAsObject
        Case Constants.PrioritizationRatingCriteria.OperatorTypes.Between
          Me.EndDuration.Enabled = True
          Me.EndDuration.Mandatory = True
      End Select
    End If

    If PerformRulesYN Then
      Select Case Element.ValueAsObject
        Case Constants.PrioritizationRatingCriteria.OperatorTypes.EqualsTo, _
             Constants.PrioritizationRatingCriteria.OperatorTypes.GreaterThan, _
             Constants.PrioritizationRatingCriteria.OperatorTypes.LessThan
          Me.EndDuration.SetToNullValue()
      End Select
    End If
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class PrioritizationRatingCriteriaRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _PrioritizationRatingCriteriaID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _PrioritizationRatingID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New PrioritizationRatingCriteria(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _PrioritizationRatingCriteriaID.ForeignElement = CType(Instance, PrioritizationRatingCriteria).PrioritizationRatingCriteriaID
    _PrioritizationRatingID.ForeignElement = CType(Instance, PrioritizationRatingCriteria)._PrioritizationRatingID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0PrioritizationRatingCriteria")
    oList.Columns.Add(New ListColumn("RatingCode", "[A0PrioritizationRatingCriteria].[RatingCode]", "RatingCode", DataType.String, Nothing, False, 1, True, 100, "Rating Code"))
    oList.Columns.Add(New ListColumn("RatingDesc", "[A0PrioritizationRatingCriteria].[RatingDesc]", "RatingDesc", DataType.String, Nothing, False, 0, True, 200, "Rating Description"))
    oList.Columns.Add(New ListColumn("OperatorTypeInd", "[A0PrioritizationRatingCriteria].[OperatorTypeInd]", "OperatorTypeInd", DataType.String, Nothing, False, 0, True, 100, "Operator Type"))
    oList.Columns.Add(New ListColumn("StartDuration", "[A0PrioritizationRatingCriteria].[StartDuration]", "StartDuration", DataType.Integer, Nothing, False, 0, True, 100, "Start Duration"))
    oList.Columns.Add(New ListColumn("EndDuration", "[A0PrioritizationRatingCriteria].[EndDuration]", "EndDuration", DataType.Integer, Nothing, False, 0, True, 100, "End Duration"))
    oList.Columns.Add(New ListColumn("PrioritizationRatingID", "[A0PrioritizationRatingCriteria].[PrioritizationRatingID]", "PrioritizationRatingID", DataType.Integer, Nothing, True, 0, False, 100, "Prioritization Rating ID"))
    oList.Columns.Add(New ListColumn("PrioritizationRatingCriteriaID", "[A0PrioritizationRatingCriteria].[PrioritizationRatingCriteriaID]", "PrioritizationRatingCriteriaID", DataType.Long, Nothing, True, 0, False, 100, "Prioritization Rating Criteria ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "[PrioritizationRatingCriteria]  [A0PrioritizationRatingCriteria]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
