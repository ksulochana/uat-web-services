Imports sembleWare.Runtime
Imports System
Public Class SubscriberProfileReciprocity 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly StatusInd As New IndicatorElement("StatusInd", Me, False, False, True, True, True, True, "Status", Nothing, SubscriberProfileReciprocity.StatusIndOptions, Nothing)
  Public ReadOnly SubscriberProfile As Relationship = New SubscriberProfileRelationship("SubscriberProfile", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly DataStatus As Relationship = New DataStatusRelationship("DataStatus", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _DataStatusCode As New StringElement("DataStatusCode", Me, True)
  Public ReadOnly _SubscriberProfile As SubscriberProfileRelationship = SubscriberProfile
  Public ReadOnly _DataStatus As DataStatusRelationship = DataStatus
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property StatusIndOptions() As IndicatorOptions
    Get
      If moStatusIndOptions Is Nothing Then
        moStatusIndOptions = New IndicatorOptions
        moStatusIndOptions.Add("A", "Allowed")
        moStatusIndOptions.Add("D", "Denied")
      End If
      Return moStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberProfileReciprocity", ApplicationSettings)
    _SubscriberProfile._SubscriberID.LocalElement = _SubscriberID
    _DataStatus._DataStatusCode.LocalElement = _DataStatusCode
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Save()
    With ApplicationSettings
      Try
        .BeginTransaction()
        Dim bIsNewYN As Boolean = Me.IsNew
        Dim bMustPersistYN As Boolean = Me.MustPersist
        Dim sOldStatusInd As String = IIf(bIsNewYN, Me.StatusInd.Value, Me.StatusInd.LoadValueAsObject)

        MyBase.Save()
        BRAddToHistory(bMustPersistYN, IIf(bIsNewYN, "Added", "Chanaged"), sOldStatusInd, Me.StatusInd.Value)
        .CommitTransaction()
      Catch Exception As Exception
        .RollbackTransaction()
        Throw Exception
      End Try
    End With
  End Sub

  Public Overrides Sub Delete()
    With ApplicationSettings
      Try
        .BeginTransaction()
        BRAddToHistory(True, "Deleted", Me.StatusInd.Value, Me.StatusInd.Value)
        MyBase.Delete()
        .CommitTransaction()
      Catch Exception As Exception
        .RollbackTransaction()
        Throw Exception
      End Try
    End With
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRAddToHistory(ByVal MustPersistYN As Boolean, ByVal Action As String, ByVal PreviousStatusInd As String, ByVal StatusInd As String)
    If MustPersistYN Then
      Dim oSubscriberProfile As SubscriberProfile = Me.SubscriberProfile.Instance
      Dim oSubscriberProfileHistory As SubscriberProfileHistory = oSubscriberProfile.SubscriberProfileHistory_OwnMany.NewInstance()

      oSubscriberProfileHistory.ActionDesc.Value = "Reciprocity " & Action
      oSubscriberProfileHistory.ActionedOnDate.Value = System.DateTime.Now
      oSubscriberProfileHistory.ActionedByUser.Value = XDSAdministrationBL.GlobalSettings.SystemUser.LoggedInSystemUsername()
      oSubscriberProfileHistory.PreviousStatusInd.Value = PreviousStatusInd
      oSubscriberProfileHistory.ChangedStatusInd.Value = StatusInd

      oSubscriberProfileHistory.Save()
    End If
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class SubscriberProfileReciprocityRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _DataStatusCode As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberProfileReciprocity(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberID.ForeignElement = CType(Instance, SubscriberProfileReciprocity)._SubscriberID
    _DataStatusCode.ForeignElement = CType(Instance, SubscriberProfileReciprocity)._DataStatusCode
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberProfileReciprocity")
    oList.Columns.Add(New ListColumn("DataStatusCode", "[A0SubscriberProfileReciprocity].[DataStatusCode]", "DataStatusCode", DataType.String, Nothing, True, 0, False, 100, "Data Status Code"))
    oList.Columns.Add(New ListColumn("DataStatusCode1", "[A1DataStatus].[DataStatusCode]", "DataStatusCode", DataType.String, Nothing, False, 1, True, 100, "Data Status Code"))
    oList.Columns.Add(New ListColumn("DataStatusDesc", "[A1DataStatus].[DataStatusDesc]", "DataStatusDesc", DataType.String, Nothing, False, 0, True, 200, "Data Status Description"))
    oList.Columns.Add(New ListColumn("StatusInd", "[A0SubscriberProfileReciprocity].[StatusInd]", "StatusInd", DataType.String, Nothing, False, 0, True, 100, "Status"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberProfileReciprocity].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "([SubscriberProfileReciprocity]  [A0SubscriberProfileReciprocity]" + _
           "    join [DataStatus]  [A1DataStatus] on [A0SubscriberProfileReciprocity].[DataStatusCode] = [A1DataStatus].[DataStatusCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberProfileReciprocity")
    oList.Columns.Add(New ListColumn("DataStatusCode", "[A0SubscriberProfileReciprocity].[DataStatusCode]", "DataStatusCode", DataType.String, Nothing, True, 0, False, 100, "Data Status Code"))
    oList.Columns.Add(New ListColumn("DataStatusCode1", "[A1DataStatus].[DataStatusCode]", "DataStatusCode", DataType.String, Nothing, False, 1, True, 100, "Data Status Code"))
    oList.Columns.Add(New ListColumn("DataStatusDesc", "[A1DataStatus].[DataStatusDesc]", "DataStatusDesc", DataType.String, Nothing, False, 0, True, 200, "Data Status Description"))
    oList.Columns.Add(New ListColumn("StatusInd", "[A0SubscriberProfileReciprocity].[StatusInd]", "StatusInd", DataType.String, Nothing, False, 0, True, 100, "Reciprocity Type"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberProfileReciprocity].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "([SubscriberProfileReciprocity]  [A0SubscriberProfileReciprocity]" + _
           "    join [DataStatus]  [A1DataStatus] on [A0SubscriberProfileReciprocity].[DataStatusCode] = [A1DataStatus].[DataStatusCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function DropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberProfileReciprocity")
    oList.Columns.Add(New ListColumn("DataStatusCode", "[A0SubscriberProfileReciprocity].[DataStatusCode]", "DataStatusCode", DataType.String, Nothing, True, 0, False, 100, "Data Status Code"))
    oList.Columns.Add(New ListColumn("DataStatusCode1", "[A1DataStatus].[DataStatusCode]", "DataStatusCode", DataType.String, Nothing, False, 1, True, 100, "Data Status Code"))
    oList.Columns.Add(New ListColumn("DataStatusDesc", "[A1DataStatus].[DataStatusDesc]", "DataStatusDesc", DataType.String, Nothing, False, 0, True, 200, "Data Status Description"))
    oList.Columns.Add(New ListColumn("StatusInd", "[A0SubscriberProfileReciprocity].[StatusInd]", "StatusInd", DataType.String, Nothing, False, 0, True, 100, "Reciprocity Type"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberProfileReciprocity].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "([SubscriberProfileReciprocity]  [A0SubscriberProfileReciprocity]" + _
           "    join [DataStatus]  [A1DataStatus] on [A0SubscriberProfileReciprocity].[DataStatusCode] = [A1DataStatus].[DataStatusCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
