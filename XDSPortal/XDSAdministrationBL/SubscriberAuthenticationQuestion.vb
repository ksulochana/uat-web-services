Imports sembleWare.Runtime
Imports System
Public Class SubscriberAuthenticationQuestion 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly Question As New StringElement("Question", Me, False, False, True, True, True, 250, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PriorityTypeInd As New IndicatorElement("PriorityTypeInd", Me, False, False, False, True, True, True, "Priority Type", Nothing, SubscriberAuthenticationQuestion.PriorityTypeIndOptions, Nothing)
  Public ReadOnly QuestionTypeInd As New IndicatorElement("QuestionTypeInd", Me, False, False, False, True, True, True, "Question Type", Nothing, SubscriberAuthenticationQuestion.QuestionTypeIndOptions, Nothing)
  Public ReadOnly AnswerStatusInd As New IndicatorElement("AnswerStatusInd", Me, False, False, False, True, True, True, "Answer Status", Nothing, SubscriberAuthenticationQuestion.AnswerStatusIndOptions, "P")
  Public ReadOnly QuestionPointValue As New IntegerElement("QuestionPointValue", Me, False, False, True, True, True, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RequiredNoOfAnswers As New IntegerElement("RequiredNoOfAnswers", Me, False, False, True, True, True, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SystemPreparationStatusInd As New IndicatorElement("SystemPreparationStatusInd", Me, False, False, True, True, True, True, Nothing, Nothing, SubscriberAuthenticationQuestion.SystemPreparationStatusIndOptions, "P")
  Public ReadOnly MaxNoOfCorrectAnswers As New IntegerElement("MaxNoOfCorrectAnswers", Me, False, True, True, True, True, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SubscriberAuthentication As Relationship = New SubscriberAuthenticationRelationship("SubscriberAuthentication", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly ProductAuthenticationQuestion As Relationship = New ProductAuthenticationQuestionRelationship("ProductAuthenticationQuestion", "Question", RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly SubscriberAuthenticationQuestionAnswer_OwnMany As Relationship = New SubscriberAuthenticationQuestionAnswerRelationship("SubscriberAuthenticationQuestionAnswer", Nothing, "SubscriberAuthenticationQuestion", Me)
  Public ReadOnly SubscriberAuthenticationQuestionAnswerRandomSetup_OwnMany As Relationship = New SubscriberAuthenticationQuestionAnswerRandomSetupRelationship("SubscriberAuthenticationQuestionAnswerRandomSetup", Nothing, "SubscriberAuthenticationQuestion", Me)
  Public ReadOnly SubscriberAuthenticationQuestionAnswerSetup_OwnMany As Relationship = New SubscriberAuthenticationQuestionAnswerSetupRelationship("SubscriberAuthenticationQuestionAnswerSetup", Nothing, "SubscriberAuthenticationQuestion", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _SubscriberAuthenticationID As New IntegerElement("SubscriberAuthenticationID", Me, True)
  Public ReadOnly _ProductAuthenticationQuestionID As New IntegerElement("ProductAuthenticationQuestionID", Me, True)
  Public ReadOnly _SubscriberAuthentication As SubscriberAuthenticationRelationship = SubscriberAuthentication
  Public ReadOnly _ProductAuthenticationQuestion As ProductAuthenticationQuestionRelationship = ProductAuthenticationQuestion
  Public ReadOnly _SubscriberAuthenticationQuestionAnswer_OwnMany As SubscriberAuthenticationQuestionAnswerRelationship = SubscriberAuthenticationQuestionAnswer_OwnMany
  Public ReadOnly _SubscriberAuthenticationQuestionAnswerRandomSetup_OwnMany As SubscriberAuthenticationQuestionAnswerRandomSetupRelationship = SubscriberAuthenticationQuestionAnswerRandomSetup_OwnMany
  Public ReadOnly _SubscriberAuthenticationQuestionAnswerSetup_OwnMany As SubscriberAuthenticationQuestionAnswerSetupRelationship = SubscriberAuthenticationQuestionAnswerSetup_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moPriorityTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property PriorityTypeIndOptions() As IndicatorOptions
    Get
      If moPriorityTypeIndOptions Is Nothing Then
        moPriorityTypeIndOptions = New IndicatorOptions
        moPriorityTypeIndOptions.Add("H", "High")
        moPriorityTypeIndOptions.Add("M", "Medium")
        moPriorityTypeIndOptions.Add("L", "Low")
      End If
      Return moPriorityTypeIndOptions
    End Get
  End Property
  Private Shared moQuestionTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property QuestionTypeIndOptions() As IndicatorOptions
    Get
      If moQuestionTypeIndOptions Is Nothing Then
        moQuestionTypeIndOptions = New IndicatorOptions
        moQuestionTypeIndOptions.Add("P", "Positive")
        moQuestionTypeIndOptions.Add("N", "Negative")
      End If
      Return moQuestionTypeIndOptions
    End Get
  End Property
  Private Shared moAnswerStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property AnswerStatusIndOptions() As IndicatorOptions
    Get
      If moAnswerStatusIndOptions Is Nothing Then
        moAnswerStatusIndOptions = New IndicatorOptions
        moAnswerStatusIndOptions.Add("P", "Pending")
        moAnswerStatusIndOptions.Add("C", "Correct")
        moAnswerStatusIndOptions.Add("I", "Incorrect")
      End If
      Return moAnswerStatusIndOptions
    End Get
  End Property
  Private Shared moSystemPreparationStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property SystemPreparationStatusIndOptions() As IndicatorOptions
    Get
      If moSystemPreparationStatusIndOptions Is Nothing Then
        moSystemPreparationStatusIndOptions = New IndicatorOptions
        moSystemPreparationStatusIndOptions.Add("P", "Pending")
        moSystemPreparationStatusIndOptions.Add("C", "Completed")
        moSystemPreparationStatusIndOptions.Add("F", "Failed")
        moSystemPreparationStatusIndOptions.Add("R", "Replaced")
      End If
      Return moSystemPreparationStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberAuthenticationQuestion", ApplicationSettings)
    _SubscriberAuthentication._SubscriberID.LocalElement = _SubscriberID
    _SubscriberAuthentication._SubscriberAuthenticationID.LocalElement = _SubscriberAuthenticationID
    _ProductAuthenticationQuestion._ProductAuthenticationQuestionID.LocalElement = _ProductAuthenticationQuestionID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class SubscriberAuthenticationQuestionRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SubscriberAuthenticationID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _ProductAuthenticationQuestionID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberAuthenticationQuestion(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberID.ForeignElement = CType(Instance, SubscriberAuthenticationQuestion)._SubscriberID
    _SubscriberAuthenticationID.ForeignElement = CType(Instance, SubscriberAuthenticationQuestion)._SubscriberAuthenticationID
    _ProductAuthenticationQuestionID.ForeignElement = CType(Instance, SubscriberAuthenticationQuestion)._ProductAuthenticationQuestionID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function SubscriberQuestionaireGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberAuthenticationQuestion")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberAuthenticationQuestion].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberAuthenticationID", "[A0SubscriberAuthenticationQuestion].[SubscriberAuthenticationID]", "SubscriberAuthenticationID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber Authentication ID"))
    oList.Columns.Add(New ListColumn("ProductAuthenticationQuestionID", "[A0SubscriberAuthenticationQuestion].[ProductAuthenticationQuestionID]", "ProductAuthenticationQuestionID", DataType.Integer, Nothing, True, 0, False, 100, "Product Authentication Question ID"))
    oList.Columns.Add(New ListColumn("Question", "[A0SubscriberAuthenticationQuestion].[Question]", "Question", DataType.String, Nothing, False, 1, True, 200, "Question"))
    oList.Columns.Add(New ListColumn("RequiredNoOfAnswers", "[A0SubscriberAuthenticationQuestion].[RequiredNoOfAnswers]", "RequiredNoOfAnswers", DataType.Integer, Nothing, False, 0, True, 100, "Required No Of Answers"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberAuthenticationQuestion]  [A0SubscriberAuthenticationQuestion]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberAuthenticationQuestion")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberAuthenticationQuestion].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberAuthenticationID", "[A0SubscriberAuthenticationQuestion].[SubscriberAuthenticationID]", "SubscriberAuthenticationID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber Authentication ID"))
    oList.Columns.Add(New ListColumn("Question", "[A0SubscriberAuthenticationQuestion].[Question]", "Question", DataType.String, Nothing, False, 1, True, 200, "Question"))
    oList.Columns.Add(New ListColumn("PriorityTypeInd", "[A0SubscriberAuthenticationQuestion].[PriorityTypeInd]", "PriorityTypeInd", DataType.String, Nothing, False, 0, True, 100, "Priority Type"))
    oList.Columns.Add(New ListColumn("QuestionTypeInd", "[A0SubscriberAuthenticationQuestion].[QuestionTypeInd]", "QuestionTypeInd", DataType.String, Nothing, False, 0, True, 100, "Question Type"))
    oList.Columns.Add(New ListColumn("AnswerStatusInd", "[A0SubscriberAuthenticationQuestion].[AnswerStatusInd]", "AnswerStatusInd", DataType.String, Nothing, False, 0, True, 100, "Answer Status"))
    oList.Columns.Add(New ListColumn("QuestionPointValue", "[A0SubscriberAuthenticationQuestion].[QuestionPointValue]", "QuestionPointValue", DataType.Integer, Nothing, False, 0, True, 100, "Question Point Value"))
    oList.Columns.Add(New ListColumn("RequiredNoOfAnswers", "[A0SubscriberAuthenticationQuestion].[RequiredNoOfAnswers]", "RequiredNoOfAnswers", DataType.Integer, Nothing, False, 0, True, 100, "Required No Of Answers"))
    oList.Columns.Add(New ListColumn("ProductAuthenticationQuestionID", "[A0SubscriberAuthenticationQuestion].[ProductAuthenticationQuestionID]", "ProductAuthenticationQuestionID", DataType.Integer, Nothing, True, 0, False, 100, "Product Authentication Question ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberAuthenticationQuestion]  [A0SubscriberAuthenticationQuestion]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function SubscriberGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberAuthenticationQuestion")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberAuthenticationQuestion].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberAuthenticationID", "[A0SubscriberAuthenticationQuestion].[SubscriberAuthenticationID]", "SubscriberAuthenticationID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber Authentication ID"))
    oList.Columns.Add(New ListColumn("Question", "[A0SubscriberAuthenticationQuestion].[Question]", "Question", DataType.String, Nothing, False, 1, True, 200, "Question"))
    oList.Columns.Add(New ListColumn("AnswerStatusInd", "[A0SubscriberAuthenticationQuestion].[AnswerStatusInd]", "AnswerStatusInd", DataType.String, Nothing, False, 0, True, 100, "Answer Status"))
    oList.Columns.Add(New ListColumn("ProductAuthenticationQuestionID", "[A0SubscriberAuthenticationQuestion].[ProductAuthenticationQuestionID]", "ProductAuthenticationQuestionID", DataType.Integer, Nothing, True, 0, False, 100, "Product Authentication Question ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberAuthenticationQuestion]  [A0SubscriberAuthenticationQuestion]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
