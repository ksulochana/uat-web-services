Imports sembleWare.Runtime
Imports System
Public Class LoaderProcessDebugLog 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly LoaderProcessDebugLogID As New IdentityElement("LoaderProcessDebugLogID", Me, True, Nothing, Nothing)
  Public ReadOnly DebugLogDate As New DateTimeElement("DebugLogDate", Me, False, False, True, True, True, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly DebugLogDesc As New StringElement("DebugLogDesc", Me, False, False, True, True, True, 250, "Debug Log Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly DebugLogDetails As New TextElement("DebugLogDetails", Me, False, True, True, False, Nothing, Nothing, Nothing)
  Public ReadOnly StagingRecordCount As New DecimalElement("StagingRecordCount", Me, False, False, True, True, True, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly TotalRecordCount As New DecimalElement("TotalRecordCount", Me, False, False, True, True, True, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AffectedRecordCount As New DecimalElement("AffectedRecordCount", Me, False, False, True, True, True, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LoaderProcess As Relationship = New LoaderProcessRelationship("LoaderProcess", Nothing, RelationshipType.Include, True, False, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _LoaderID As New IntegerElement("LoaderID", Me, False)
  Public ReadOnly _LoaderProcessTypeCode As New StringElement("LoaderProcessTypeCode", Me, False)
  Public ReadOnly _LoaderProcess As LoaderProcessRelationship = LoaderProcess
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("LoaderProcessDebugLog", ApplicationSettings)
    _LoaderProcess._LoaderID.LocalElement = _LoaderID
    _LoaderProcess._LoaderProcessTypeCode.LocalElement = _LoaderProcessTypeCode
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class LoaderProcessDebugLogRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _LoaderProcessDebugLogID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New LoaderProcessDebugLog(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _LoaderProcessDebugLogID.ForeignElement = CType(Instance, LoaderProcessDebugLog).LoaderProcessDebugLogID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
