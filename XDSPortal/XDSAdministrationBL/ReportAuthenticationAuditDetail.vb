Imports sembleWare.Runtime
Imports System
Public Class ReportAuthenticationAuditDetail 'sembleWare: Part
  Inherits CustomMemoryPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly Subscriber_SP As Relationship = New SubscriberRelationship("Subscriber_SP", "Subscriber", RelationshipType.Include, False, True, True, Me)
  Public ReadOnly SubscriberAuthentication_SP As Relationship = New SubscriberAuthenticationRelationship("SubscriberAuthentication_SP", "Authentication", RelationshipType.Include, False, True, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, False)
  Public ReadOnly _SubscriberAuthenticationID As New IntegerElement("SubscriberAuthenticationID", Me, False)
  Public ReadOnly _Subscriber_SP As SubscriberRelationship = Subscriber_SP
  Public ReadOnly _SubscriberAuthentication_SP As SubscriberAuthenticationRelationship = SubscriberAuthentication_SP
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("ReportAuthenticationAuditDetail", ApplicationSettings)
    _Subscriber_SP._SubscriberID.LocalElement = _SubscriberID
    _SubscriberAuthentication_SP._SubscriberAuthenticationID.LocalElement = _SubscriberAuthenticationID
    _SubscriberAuthentication_SP._SubscriberID.LocalElement = _SubscriberID
    'sembleWare: Constructor End - Do Not Modify
    AddHandler SubscriberAuthentication_SP.LimitRelatedList, AddressOf SubscriberAuthentication_SP_LimitRelatedList
  End Sub

#End Region 'sembleWare: Constructor

#Region " LimitRelatedList Events"
  Private Sub SubscriberAuthentication_SP_LimitRelatedList(ByVal List As List)
    List.LimitByRelatedPart(Me.Subscriber_SP.Instance, "Subscriber")
  End Sub
#End Region

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class ReportAuthenticationAuditDetailRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New ReportAuthenticationAuditDetail(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
