Imports sembleWare.Runtime
Imports System
Public Class SubscriberDirectorEnquiryResult 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly IDNo As New StringElement("IDNo", Me, False, False, True, True, False, 13, "ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PassportNo As New StringElement("PassportNo", Me, False, False, True, True, False, 16, "Passport No / Other ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FirstInitial As New StringElement("FirstInitial", Me, False, False, True, True, False, 1, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SecondInitial As New StringElement("SecondInitial", Me, False, False, True, True, False, 1, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FirstName As New StringElement("FirstName", Me, False, False, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SecondName As New StringElement("SecondName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Surname As New StringElement("Surname", Me, False, False, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly BirthDate As New DateTimeElement("BirthDate", Me, False, False, True, True, False, "Date of Birth", Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly GenderInd As New IndicatorElement("GenderInd", Me, False, False, False, True, True, False, "Gender", Nothing, SubscriberDirectorEnquiryResult.GenderIndOptions, Nothing)
  Public ReadOnly DetailsViewedYN As New BooleanElement("DetailsViewedYN", Me, False, False, True, True, True, "Details Viewed?", Nothing, False, "Yes;No")
  Public ReadOnly DetailsViewedDate As New DateTimeElement("DetailsViewedDate", Me, False, False, True, True, False, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly DirectorSelectedYN As New BooleanElement("DirectorSelectedYN", Me, False, True, True, True, True, "Director Selected?", Nothing, False, "Yes;No")
  Public ReadOnly SubscriberDirectorEnquiry As Relationship = New SubscriberDirectorEnquiryRelationship("SubscriberDirectorEnquiry", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly Director As Relationship = New DirectorRelationship("Director", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _SubscriberDirectorEnquiryID As New IntegerElement("SubscriberDirectorEnquiryID", Me, True)
  Public ReadOnly _DirectorID As New IntegerElement("DirectorID", Me, True)
  Public ReadOnly _SubscriberDirectorEnquiry As SubscriberDirectorEnquiryRelationship = SubscriberDirectorEnquiry
  Public ReadOnly _Director As DirectorRelationship = Director
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moGenderIndOptions As IndicatorOptions
  Public Shared ReadOnly Property GenderIndOptions() As IndicatorOptions
    Get
      If moGenderIndOptions Is Nothing Then
        moGenderIndOptions = New IndicatorOptions
        moGenderIndOptions.Add("M", "Male")
        moGenderIndOptions.Add("F", "Female")
      End If
      Return moGenderIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberDirectorEnquiryResult", ApplicationSettings)
    _SubscriberDirectorEnquiry._SubscriberID.LocalElement = _SubscriberID
    _SubscriberDirectorEnquiry._SubscriberDirectorEnquiryID.LocalElement = _SubscriberDirectorEnquiryID
    _Director._DirectorID.LocalElement = _DirectorID
    AddHandler DetailsViewedYN.Changed, AddressOf DetailsViewedYN_Changed
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub DetailsViewedYN_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"

  Public Sub SelectRecord()
    With ApplicationSettings
      Try
        .BeginTransaction()
        Me.DirectorSelectedYN.Value = True
        Me.DetailsViewedYN.Value = True
        Me.DetailsViewedDate.Value = System.DateTime.Now
        Me.Save()

        Dim oDirector As Director = Me.Director.Instance
        Dim OSubscriberDirectorEnquiry As SubscriberDirectorEnquiry = Me.SubscriberDirectorEnquiry.Instance
        OSubscriberDirectorEnquiry.EnquiryResultInd.Value = Constants.SubscriberDirectorEnquiry.Results.RecordSelected

        OSubscriberDirectorEnquiry.ResultIDNo.Value = oDirector.IDNo.Value
        OSubscriberDirectorEnquiry.ResultFirstInitial.Value = oDirector.FirstInitial.Value
        OSubscriberDirectorEnquiry.ResultSecondInitial.Value = oDirector.SecondInitial.Value
        OSubscriberDirectorEnquiry.ResultSurname.Value = oDirector.Surname.Value
        OSubscriberDirectorEnquiry.ResultBirthDate.ValueAsObject = oDirector.BirthDate.ValueAsObject

        OSubscriberDirectorEnquiry.Save()
        .CommitTransaction()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
    End With
  End Sub

#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class SubscriberDirectorEnquiryResultRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SubscriberDirectorEnquiryID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _DirectorID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberDirectorEnquiryResult(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberID.ForeignElement = CType(Instance, SubscriberDirectorEnquiryResult)._SubscriberID
    _SubscriberDirectorEnquiryID.ForeignElement = CType(Instance, SubscriberDirectorEnquiryResult)._SubscriberDirectorEnquiryID
    _DirectorID.ForeignElement = CType(Instance, SubscriberDirectorEnquiryResult)._DirectorID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberDirectorEnquiryResult")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberDirectorEnquiryResult].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberDirectorEnquiryID", "[A0SubscriberDirectorEnquiryResult].[SubscriberDirectorEnquiryID]", "SubscriberDirectorEnquiryID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber Director Enquiry ID"))
    oList.Columns.Add(New ListColumn("DirectorID", "[A0SubscriberDirectorEnquiryResult].[DirectorID]", "DirectorID", DataType.Integer, Nothing, True, 0, False, 100, "Director ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A0SubscriberDirectorEnquiryResult].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("PassportNo", "[A0SubscriberDirectorEnquiryResult].[PassportNo]", "PassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No / Other ID No"))
    oList.Columns.Add(New ListColumn("FirstInitial", "[A0SubscriberDirectorEnquiryResult].[FirstInitial]", "FirstInitial", DataType.String, Nothing, False, 0, True, 100, "First Initial"))
    oList.Columns.Add(New ListColumn("FirstName", "[A0SubscriberDirectorEnquiryResult].[FirstName]", "FirstName", DataType.String, Nothing, False, 2, True, 150, "First Name"))
    oList.Columns.Add(New ListColumn("Surname", "[A0SubscriberDirectorEnquiryResult].[Surname]", "Surname", DataType.String, Nothing, False, 1, True, 150, "Surname"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A0SubscriberDirectorEnquiryResult].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Date of Birth"))
    oList.Columns.Add(New ListColumn("GenderInd", "[A0SubscriberDirectorEnquiryResult].[GenderInd]", "GenderInd", DataType.String, Nothing, False, 0, True, 100, "Gender"))
    oList.Columns.Add(New ListColumn("DetailsViewedYN", "[A0SubscriberDirectorEnquiryResult].[DetailsViewedYN]", "DetailsViewedYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Details Viewed?"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberDirectorEnquiryResult]  [A0SubscriberDirectorEnquiryResult]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function MultipleMatchGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberDirectorEnquiryResult")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberDirectorEnquiryResult].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberDirectorEnquiryID", "[A0SubscriberDirectorEnquiryResult].[SubscriberDirectorEnquiryID]", "SubscriberDirectorEnquiryID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber Director Enquiry ID"))
    oList.Columns.Add(New ListColumn("DirectorID", "[A0SubscriberDirectorEnquiryResult].[DirectorID]", "DirectorID", DataType.Integer, Nothing, True, 0, False, 100, "Director ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A0SubscriberDirectorEnquiryResult].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("PassportNo", "[A0SubscriberDirectorEnquiryResult].[PassportNo]", "PassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No / Other ID No"))
    oList.Columns.Add(New ListColumn("FirstInitial", "[A0SubscriberDirectorEnquiryResult].[FirstInitial]", "FirstInitial", DataType.String, Nothing, False, 0, True, 100, "First Initial"))
    oList.Columns.Add(New ListColumn("FirstName", "[A0SubscriberDirectorEnquiryResult].[FirstName]", "FirstName", DataType.String, Nothing, False, 2, True, 150, "First Name"))
    oList.Columns.Add(New ListColumn("Surname", "[A0SubscriberDirectorEnquiryResult].[Surname]", "Surname", DataType.String, Nothing, False, 1, True, 150, "Surname"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A0SubscriberDirectorEnquiryResult].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Date of Birth"))
    oList.Columns.Add(New ListColumn("GenderInd", "[A0SubscriberDirectorEnquiryResult].[GenderInd]", "GenderInd", DataType.String, Nothing, False, 0, True, 100, "Gender"))
    oList.Columns.Add(New ListColumn("DetailsViewedYN", "[A0SubscriberDirectorEnquiryResult].[DetailsViewedYN]", "DetailsViewedYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Details Viewed?"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberDirectorEnquiryResult]  [A0SubscriberDirectorEnquiryResult]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

Public Function MultipleMatchGridList1() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim strDisbQuery = "(select count(IDNo) from ConsumerNameSoundEx where (IDNo = substring([A0SubscriberDirectorEnquiryResult].[IDNo],0,11) and Surname = [A0SubscriberDirectorEnquiryResult].[Surname]) " + _
                        "or (IDNo = substring([A0SubscriberDirectorEnquiryResult].[IDNo],0,11) and FirstInitial = [A0SubscriberDirectorEnquiryResult].[FirstInitial]) " + _
                        "or (Surname = [A0SubscriberDirectorEnquiryResult].[Surname] and FirstInitial = [A0SubscriberDirectorEnquiryResult].[FirstInitial] and BirthDate = [A0SubscriberDirectorEnquiryResult].[BirthDate]) )"
    Dim oList As DBList = New DBList(Me, "A0SubscriberDirectorEnquiryResult")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberDirectorEnquiryResult].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberDirectorEnquiryID", "[A0SubscriberDirectorEnquiryResult].[SubscriberDirectorEnquiryID]", "SubscriberDirectorEnquiryID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber Director Enquiry ID"))
    oList.Columns.Add(New ListColumn("DirectorID", "[A0SubscriberDirectorEnquiryResult].[DirectorID]", "DirectorID", DataType.Integer, Nothing, True, 0, False, 100, "Director ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A0SubscriberDirectorEnquiryResult].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("PassportNo", "[A0SubscriberDirectorEnquiryResult].[PassportNo]", "PassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No / Other ID No"))
    oList.Columns.Add(New ListColumn("FirstInitial", "[A0SubscriberDirectorEnquiryResult].[FirstInitial]", "FirstInitial", DataType.String, Nothing, False, 0, True, 100, "First Initial"))
    oList.Columns.Add(New ListColumn("FirstName", "[A0SubscriberDirectorEnquiryResult].[FirstName]", "FirstName", DataType.String, Nothing, False, 2, True, 150, "First Name"))
    oList.Columns.Add(New ListColumn("Surname", "[A0SubscriberDirectorEnquiryResult].[Surname]", "Surname", DataType.String, Nothing, False, 1, True, 150, "Surname"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A0SubscriberDirectorEnquiryResult].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Date of Birth"))
    oList.Columns.Add(New ListColumn("GenderInd", "[A0SubscriberDirectorEnquiryResult].[GenderInd]", "GenderInd", DataType.String, Nothing, False, 0, True, 100, "Gender"))
    oList.Columns.Add(New ListColumn("DetailsViewedYN", "[A0SubscriberDirectorEnquiryResult].[DetailsViewedYN]", "DetailsViewedYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Details Viewed?"))
    oList.Columns.Add(New ListColumn("DisbFlag", strDisbQuery, "DisbFlag", DataType.Integer, Nothing, False, 0, True, 100, "DisbFlag"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberDirectorEnquiryResult]  [A0SubscriberDirectorEnquiryResult]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberDirectorEnquiryResult")
    oList.Columns.Add(New ListColumn("DirectorID", "[A0SubscriberDirectorEnquiryResult].[DirectorID]", "DirectorID", DataType.Integer, Nothing, True, 0, False, 100, "Director ID"))
    oList.Columns.Add(New ListColumn("SubscriberDirectorEnquiryID", "[A0SubscriberDirectorEnquiryResult].[SubscriberDirectorEnquiryID]", "SubscriberDirectorEnquiryID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber Director Enquiry ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberDirectorEnquiryResult].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A0SubscriberDirectorEnquiryResult].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("PassportNo", "[A0SubscriberDirectorEnquiryResult].[PassportNo]", "PassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No / Other ID No"))
    oList.Columns.Add(New ListColumn("Surname", "[A0SubscriberDirectorEnquiryResult].[Surname]", "Surname", DataType.String, Nothing, False, 0, True, 100, "Surname"))
    oList.Columns.Add(New ListColumn("FirstName", "[A0SubscriberDirectorEnquiryResult].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A0SubscriberDirectorEnquiryResult].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Date of Birth"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberDirectorEnquiryResult]  [A0SubscriberDirectorEnquiryResult]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
