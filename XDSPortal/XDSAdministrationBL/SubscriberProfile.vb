Imports sembleWare.Runtime
Imports System
Public Class SubscriberProfile 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly AuthenticationSearchOnIDNoYN As New BooleanElement("AuthenticationSearchOnIDNoYN", Me, False, True, True, True, True, "Search On ID No & Passport No?", Nothing, False, "Yes;No")
  Public ReadOnly AuthenticationSearchOnAccountNoYN As New BooleanElement("AuthenticationSearchOnAccountNoYN", Me, False, True, True, True, True, "Search On Account No?", Nothing, False, "Yes;No")
  Public ReadOnly AuthenticationSearchOnCellPhoneNoYN As New BooleanElement("AuthenticationSearchOnCellPhoneNoYN", Me, False, True, True, True, True, "Search On Cell Phone No?", Nothing, False, "Yes;No")
  Public ReadOnly AuthenticationDefaultIDVerificationSelectionInd As New IndicatorElement("AuthenticationDefaultIDVerificationSelectionInd", Me, False, False, True, True, True, True, "Default ID Verification Selection", Nothing, SubscriberProfile.AuthenticationDefaultIDVerificationSelectionIndOptions, "I")
  Public ReadOnly AuthenticationPrimaryNoOfPositiveHighPriorityQuestions As New IntegerElement("AuthenticationPrimaryNoOfPositiveHighPriorityQuestions", Me, False, True, True, True, True, "No of High Priority (+)", Nothing, Nothing, 1, Nothing, Nothing)
  Public ReadOnly AuthenticationPrimaryNoOfPositiveMediumPriorityQuestions As New IntegerElement("AuthenticationPrimaryNoOfPositiveMediumPriorityQuestions", Me, False, True, True, True, True, "No of Medium Priority (+)", Nothing, Nothing, 1, Nothing, Nothing)
  Public ReadOnly AuthenticationPrimaryNoOfPositiveLowPriorityQuestions As New IntegerElement("AuthenticationPrimaryNoOfPositiveLowPriorityQuestions", Me, False, True, True, True, True, "No of Low Priority (+)", Nothing, Nothing, 1, Nothing, Nothing)
  Public ReadOnly AuthenticationPrimaryNoOfNegativeHighPriorityQuestions As New IntegerElement("AuthenticationPrimaryNoOfNegativeHighPriorityQuestions", Me, False, True, True, True, True, "No of High Priority (-)", Nothing, Nothing, 1, Nothing, Nothing)
  Public ReadOnly AuthenticationPrimaryNoOfNegativeMediumPriorityQuestions As New IntegerElement("AuthenticationPrimaryNoOfNegativeMediumPriorityQuestions", Me, False, True, True, True, True, "No of Medium Priority (-)", Nothing, Nothing, 1, Nothing, Nothing)
  Public ReadOnly AuthenticationPrimaryNoOfNegativeLowPriorityQuestions As New IntegerElement("AuthenticationPrimaryNoOfNegativeLowPriorityQuestions", Me, False, True, True, True, True, "No of Low Priority (-)", Nothing, Nothing, 0, Nothing, Nothing)
  Public ReadOnly AuthenticationPrimaryNoOfYesNoQuestions As New IntegerElement("AuthenticationPrimaryNoOfYesNoQuestions", Me, False, True, True, True, True, "No Of Yes/No", Nothing, Nothing, 3, Nothing, Nothing)
  Public ReadOnly AuthenticationPrimaryNoOfAnswersPerQuestion As New IntegerElement("AuthenticationPrimaryNoOfAnswersPerQuestion", Me, False, True, True, True, True, "No of Answers per Question", Nothing, Nothing, 5, Nothing, Nothing)
  Public ReadOnly AuthenticationPrimaryRequiredAuthenticatedPerc As New DecimalElement("AuthenticationPrimaryRequiredAuthenticatedPerc", Me, False, True, True, True, True, "Required Authenticated %", Nothing, "##0.00", 80, Nothing, Nothing)
  Public ReadOnly AuthenticationPrimaryEnsureRequiredNoOfQuestionsYN As New BooleanElement("AuthenticationPrimaryEnsureRequiredNoOfQuestionsYN", Me, False, True, True, True, True, "Ensure Required No Of Questions (Populate with Negative Questions)?", Nothing, False, "Yes;No")
  Public ReadOnly AuthenticationSecondaryNoOfPositiveHighPriorityQuestions As New IntegerElement("AuthenticationSecondaryNoOfPositiveHighPriorityQuestions", Me, False, True, True, True, True, "No of High Priority (+)", Nothing, Nothing, 2, Nothing, Nothing)
  Public ReadOnly AuthenticationSecondaryNoOfPositiveMediumPriorityQuestions As New IntegerElement("AuthenticationSecondaryNoOfPositiveMediumPriorityQuestions", Me, False, True, True, True, True, "No of Medium Priority (+)", Nothing, Nothing, 0, Nothing, Nothing)
  Public ReadOnly AuthenticationSecondaryNoOfPositiveLowPriorityQuestions As New IntegerElement("AuthenticationSecondaryNoOfPositiveLowPriorityQuestions", Me, False, True, True, True, True, "No of Low Priority (+)", Nothing, Nothing, 0, Nothing, Nothing)
  Public ReadOnly AuthenticationSecondaryNoOfNegativeHighPriorityQuestions As New IntegerElement("AuthenticationSecondaryNoOfNegativeHighPriorityQuestions", Me, False, True, True, True, True, "No of High Priority (-)", Nothing, Nothing, 3, Nothing, Nothing)
  Public ReadOnly AuthenticationSecondaryNoOfNegativeMediumPriorityQuestions As New IntegerElement("AuthenticationSecondaryNoOfNegativeMediumPriorityQuestions", Me, False, True, True, True, True, "No of Medium Priority (-)", Nothing, Nothing, 0, Nothing, Nothing)
  Public ReadOnly AuthenticationSecondaryNoOfNegativeLowPriorityQuestions As New IntegerElement("AuthenticationSecondaryNoOfNegativeLowPriorityQuestions", Me, False, True, True, True, True, "No of Low Priority (-)", Nothing, Nothing, 0, Nothing, Nothing)
  Public ReadOnly AuthenticationSecondaryNoOfYesNoQuestions As New IntegerElement("AuthenticationSecondaryNoOfYesNoQuestions", Me, False, True, True, True, True, "No Of Yes/No", Nothing, Nothing, 2, Nothing, Nothing)
  Public ReadOnly AuthenticationSecondaryNoOfAnswersPerQuestion As New IntegerElement("AuthenticationSecondaryNoOfAnswersPerQuestion", Me, False, True, True, True, True, "No of Answers per Question", Nothing, Nothing, 5, Nothing, Nothing)
  Public ReadOnly AuthenticationSecondaryRequiredAuthenticatedPerc As New DecimalElement("AuthenticationSecondaryRequiredAuthenticatedPerc", Me, False, True, True, True, True, "Required Authenticated %", Nothing, "##0.00", 80, Nothing, Nothing)
  Public ReadOnly AuthenticationSecondaryEnsureRequiredNoOfQuestionsYN As New BooleanElement("AuthenticationSecondaryEnsureRequiredNoOfQuestionsYN", Me, False, True, True, True, True, "Ensure Required No Of Questions (Populate with Negative Questions)?", Nothing, False, "Yes;No")
  Public ReadOnly SubscriberProfileLabelDescription As New StringElement("SubscriberProfileLabelDescription", Me, False, True, True, False, False, 250, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ExcludeFromCreditEnquiryYN As New BooleanElement("ExcludeFromCreditEnquiryYN", Me, False, True, True, True, False, "Exclude From Credit Enquiry Report", Nothing, False)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly SubscriberProfileReciprocity_OwnMany As Relationship = New SubscriberProfileReciprocityRelationship("SubscriberProfileReciprocity", Nothing, "SubscriberProfile", Me)
  Public ReadOnly SubscriberProfileProduct_OwnMany As Relationship = New SubscriberProfileProductRelationship("SubscriberProfileProduct", Nothing, "SubscriberProfile", Me)
  Public ReadOnly SubscriberProfileHistory_OwnMany As Relationship = New SubscriberProfileHistoryRelationship("SubscriberProfileHistory", Nothing, "SubscriberProfile", Me)
  Public ReadOnly SubscriberProfileProductAuthenticationQuestion_OwnMany As Relationship = New SubscriberProfileProductAuthenticationQuestionRelationship("SubscriberProfileProductAuthenticationQuestion", Nothing, "SubscriberProfile", Me)
  Public ReadOnly SubscriberProfileProductAuthenticationBranch_OwnMany As Relationship = New SubscriberProfileProductAuthenticationBranchRelationship("SubscriberProfileProductAuthenticationBranch", Nothing, "SubscriberProfile", Me)
  Public ReadOnly SubscriberProfileProductAuthenticationPurpose_OwnMany As Relationship = New SubscriberProfileProductAuthenticationPurposeRelationship("SubscriberProfileProductAuthenticationPurpose", Nothing, "SubscriberProfile", Me)
  Public ReadOnly SubscriberProfileProductAuthenticationSubscriber_OwnMany As Relationship = New SubscriberProfileProductAuthenticationSubscriberRelationship("SubscriberProfileProductAuthenticationSubscriber", Nothing, "SubscriberProfile", Me)
  Public ReadOnly SubscriberProfileConsumerTraceReport_OwnMany As Relationship = New SubscriberProfileConsumerTraceReportRelationship("SubscriberProfileConsumerTraceReport", Nothing, "SubscriberProfile", Me)
  Public ReadOnly SubscriberProfileConsumerCreditGrantorReport_OwnMany As Relationship = New SubscriberProfileConsumerCreditGrantorReportRelationship("SubscriberProfileConsumerCreditGrantorReport", Nothing, "SubscriberProfile", Me)
  Public ReadOnly SubscriberProfileCommercialEnquiryReport_OwnMany As Relationship = New SubscriberProfileCommercialEnquiryReportRelationship("SubscriberProfileCommercialEnquiryReport", Nothing, "SubscriberProfile", Me)
  Public ReadOnly SubscriberProfileFileFormat_OwnMany As Relationship = New SubscriberProfileFileFormatRelationship("SubscriberProfileFileFormat", Nothing, "SubscriberProfile", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
  Public ReadOnly _SubscriberProfileReciprocity_OwnMany As SubscriberProfileReciprocityRelationship = SubscriberProfileReciprocity_OwnMany
  Public ReadOnly _SubscriberProfileProduct_OwnMany As SubscriberProfileProductRelationship = SubscriberProfileProduct_OwnMany
  Public ReadOnly _SubscriberProfileHistory_OwnMany As SubscriberProfileHistoryRelationship = SubscriberProfileHistory_OwnMany
  Public ReadOnly _SubscriberProfileProductAuthenticationQuestion_OwnMany As SubscriberProfileProductAuthenticationQuestionRelationship = SubscriberProfileProductAuthenticationQuestion_OwnMany
  Public ReadOnly _SubscriberProfileProductAuthenticationBranch_OwnMany As SubscriberProfileProductAuthenticationBranchRelationship = SubscriberProfileProductAuthenticationBranch_OwnMany
  Public ReadOnly _SubscriberProfileProductAuthenticationPurpose_OwnMany As SubscriberProfileProductAuthenticationPurposeRelationship = SubscriberProfileProductAuthenticationPurpose_OwnMany
  Public ReadOnly _SubscriberProfileProductAuthenticationSubscriber_OwnMany As SubscriberProfileProductAuthenticationSubscriberRelationship = SubscriberProfileProductAuthenticationSubscriber_OwnMany
  Public ReadOnly _SubscriberProfileConsumerTraceReport_OwnMany As SubscriberProfileConsumerTraceReportRelationship = SubscriberProfileConsumerTraceReport_OwnMany
  Public ReadOnly _SubscriberProfileConsumerCreditGrantorReport_OwnMany As SubscriberProfileConsumerCreditGrantorReportRelationship = SubscriberProfileConsumerCreditGrantorReport_OwnMany
  Public ReadOnly _SubscriberProfileCommercialEnquiryReport_OwnMany As SubscriberProfileCommercialEnquiryReportRelationship = SubscriberProfileCommercialEnquiryReport_OwnMany
  Public ReadOnly _SubscriberProfileFileFormat_OwnMany As SubscriberProfileFileFormatRelationship = SubscriberProfileFileFormat_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moAuthenticationDefaultIDVerificationSelectionIndOptions As IndicatorOptions
  Public Shared ReadOnly Property AuthenticationDefaultIDVerificationSelectionIndOptions() As IndicatorOptions
    Get
      If moAuthenticationDefaultIDVerificationSelectionIndOptions Is Nothing Then
        moAuthenticationDefaultIDVerificationSelectionIndOptions = New IndicatorOptions
        moAuthenticationDefaultIDVerificationSelectionIndOptions.Add("I", "Include")
        moAuthenticationDefaultIDVerificationSelectionIndOptions.Add("G", "Ignore")
      End If
      Return moAuthenticationDefaultIDVerificationSelectionIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberProfile", ApplicationSettings)
    _Subscriber._SubscriberID.LocalElement = _SubscriberID
    'sembleWare: Constructor End - Do Not Modify
  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub ProductAuthenticationNoOfAnswersPerQuestion_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BRProductAuthenticationNoOfAnswersPerQuestion(Element, True, True)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Business Rules"
  Private Sub BRProductAuthenticationNoOfAnswersPerQuestion(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If PerformRulesYN Then
      If Element.ValueAsObject < 1 Then
        Throw New BusinessRuleException("The No of Answers per Question must be greater than 0!")
      End If
    End If
  End Sub
#End Region

#Region " Shared Procedures"
  Public Shared Function GetSubscriberProfile(ByVal SubscriberID As Long, ByVal ApplicationSettings As ApplicationSettings) As SubscriberProfile
    Dim oSubscriberProfile As SubscriberProfile = New SubscriberProfileRelationship(ApplicationSettings).NewInstance
    Try
      oSubscriberProfile._SubscriberID.Load(SubscriberID)
      oSubscriberProfile.Load()
    Catch oException As sembleWare.Runtime.RecordNotFoundException
      oSubscriberProfile = New SubscriberProfileRelationship(ApplicationSettings).NewInstance
      oSubscriberProfile._SubscriberID.Load(SubscriberID)
    Catch oException As Exception
      Throw oException
    End Try
    Return oSubscriberProfile
  End Function
#End Region
End Class 'sembleWare: Part

Public Class SubscriberProfileRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberProfile(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberID.ForeignElement = CType(Instance, SubscriberProfile)._SubscriberID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberProfile")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberProfile].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, True, 100, "Subscriber ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberProfile]  [A0SubscriberProfile]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function DropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberProfile")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberProfile].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, True, 100, "Subscriber ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberProfile]  [A0SubscriberProfile]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberProfile")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberProfile].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, True, 100, "Subscriber ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberProfile]  [A0SubscriberProfile]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
