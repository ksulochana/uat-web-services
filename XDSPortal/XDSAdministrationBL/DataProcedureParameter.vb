Imports sembleWare.Runtime
Imports System
Public Class DataProcedureParameter 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly ParameterID As New IdentityElement("ParameterID", Me, True, Nothing, Nothing)
  Public ReadOnly ParameterDesc As New StringElement("ParameterDesc", Me, False, True, True, True, True, 50, "Parameter Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SequenceNum As New IntegerElement("SequenceNum", Me, False, False, True, True, True, "Sequence Number", Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly DataProcedure As Relationship = New DataProcedureRelationship("DataProcedure", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _DataProcedureID As New IntegerElement("DataProcedureID", Me, True)
  Public ReadOnly _DataProcedure As DataProcedureRelationship = DataProcedure
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("DataProcedureParameter", ApplicationSettings)
    _DataProcedure._DataProcedureID.LocalElement = _DataProcedureID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"

  Public Sub CheckParameterType()
    Dim oDataProcedure As DataProcedure = Me.DataProcedure.Instance

    Select Case oDataProcedure.ParameterTypeInd.Value
      Case "N", "S"
        Throw New BusinessRuleException("You may not add Parameters as this Data Procedure Parameter Type is not ""Multiple Specified Fields""!")
    End Select
  End Sub

  Public Sub MoveUp()
    If Me.SequenceNum.Value = 0 Then
      Return
    End If
    Me.SequenceNum.Value = Me.SequenceNum.Value - 1
    Save()
  End Sub

  Public Sub MoveDown()
    Dim nSequenceNum = Utility.GetNextSequenceNumber(Me.TableName, Me.DataProcedure.Instance, ApplicationSettings) - 1
    If Me.SequenceNum.Value = nSequenceNum Then
      Return
    End If
    Me.SequenceNum.Value = Me.SequenceNum.Value + 1
    Save()
  End Sub

#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreate()
  End Sub

  Public Overrides Sub Save()
    With ApplicationSettings
      .BeginTransaction()
      Try
        Dim bIsNewYN As Boolean = Me.IsNew
        BRBeforeSave(bIsNewYN)
        MyBase.Save()
        BRAfterSave(bIsNewYN)
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
      .CommitTransaction()
    End With
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreate()
    Me.SequenceNum.Value = Utility.GetNextSequenceNumber(Me.TableName, Me.DataProcedure.Instance, ApplicationSettings)
  End Sub

  Private Sub BRBeforeSave(ByVal IsNewYN As Boolean)
    If IsNewYN Then
      If Me.SequenceNum.IsEmpty Then
        Me.SequenceNum.Value = Utility.GetNextSequenceNumber(Me.TableName, Me.DataProcedure.Instance, ApplicationSettings)
        InsertFileFormatMetaDataProcedureParameters(Me, ApplicationSettings)
      End If
    End If
    Utility.UpdateSequenceNumber(Me, Me.DataProcedure.Instance, False, ApplicationSettings)
  End Sub

  Private Sub BRAfterSave(ByVal IsNewYN As Boolean)
    InsertFileFormatMetaDataProcedureParameters(Me, ApplicationSettings)
  End Sub
#End Region

#Region " Shared Procedures"
  Public Shared Sub InsertFileFormatMetaDataProcedureParameters(ByVal DataProcedureParameter As DataProcedureParameter, ByVal ApplicationSettings As ApplicationSettings)
    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand

    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spDataProcedureParameter_I_FileFormatMetaDataProcedure"

    oCommand.Parameters.Add("@DataProcedureID", SqlDbType.Int)
    oCommand.Parameters.Add("@ParameterID", SqlDbType.Int)

    oCommand.Parameters("@DataProcedureID").Value = DataProcedureParameter._DataProcedureID.Value
    oCommand.Parameters("@ParameterID").Value = DataProcedureParameter.ParameterID.Value

    ApplicationSettings.ActionQuery(oCommand)
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class DataProcedureParameterRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _ParameterID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _DataProcedureID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New DataProcedureParameter(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _ParameterID.ForeignElement = CType(Instance, DataProcedureParameter).ParameterID
    _DataProcedureID.ForeignElement = CType(Instance, DataProcedureParameter)._DataProcedureID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0DataProcedureParameter")
    oList.Columns.Add(New ListColumn("ParameterID", "[A0DataProcedureParameter].[ParameterID]", "ParameterID", DataType.Long, Nothing, True, 0, False, 100, "Parameter ID"))
    oList.Columns.Add(New ListColumn("ParameterDesc", "[A0DataProcedureParameter].[ParameterDesc]", "ParameterDesc", DataType.String, Nothing, False, 0, True, 200, "Parameter Description"))
    oList.Columns.Add(New ListColumn("SequenceNum", "[A0DataProcedureParameter].[SequenceNum]", "SequenceNum", DataType.Integer, Nothing, False, 1, False, 100, "Sequence Number"))
    oList.Columns.Add(New ListColumn("DataProcedureID", "[A0DataProcedureParameter].[DataProcedureID]", "DataProcedureID", DataType.Integer, Nothing, True, 0, False, 100, "Data Procedure ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "[DataProcedureParameter]  [A0DataProcedureParameter]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0DataProcedureParameter")
    oList.Columns.Add(New ListColumn("ParameterID", "[A0DataProcedureParameter].[ParameterID]", "ParameterID", DataType.Long, Nothing, True, 1, False, 100, "Parameter ID"))
    oList.Columns.Add(New ListColumn("ParameterDesc", "[A0DataProcedureParameter].[ParameterDesc]", "ParameterDesc", DataType.String, Nothing, False, 0, True, 200, "Parameter Description"))
    oList.Columns.Add(New ListColumn("SequenceNum", "[A0DataProcedureParameter].[SequenceNum]", "SequenceNum", DataType.Integer, Nothing, False, 0, True, 100, "Sequence Number"))
    oList.Columns.Add(New ListColumn("DataProcedureID", "[A0DataProcedureParameter].[DataProcedureID]", "DataProcedureID", DataType.Integer, Nothing, True, 0, False, 100, "Data Procedure ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "[DataProcedureParameter]  [A0DataProcedureParameter]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function DropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0DataProcedureParameter")
    oList.Columns.Add(New ListColumn("ParameterID", "[A0DataProcedureParameter].[ParameterID]", "ParameterID", DataType.Long, Nothing, True, 0, False, 100, "Parameter ID"))
    oList.Columns.Add(New ListColumn("ParameterDesc", "[A0DataProcedureParameter].[ParameterDesc]", "ParameterDesc", DataType.String, Nothing, False, 0, True, 200, "Parameter Description"))
    oList.Columns.Add(New ListColumn("SequenceNum", "[A0DataProcedureParameter].[SequenceNum]", "SequenceNum", DataType.Integer, Nothing, False, 1, False, 100, "Sequence Number"))
    oList.Columns.Add(New ListColumn("DataProcedureID", "[A0DataProcedureParameter].[DataProcedureID]", "DataProcedureID", DataType.Integer, Nothing, True, 0, False, 100, "Data Procedure ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "[DataProcedureParameter]  [A0DataProcedureParameter]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
