Imports sembleWare.Runtime
Imports System
Public Class SubscriberAuthenticationQuestionAnswerSetup 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly Answer As New StringElement("Answer", Me, True, True, True, True, True, 500, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly IsCorrectAnswerYN As New BooleanElement("IsCorrectAnswerYN", Me, False, True, True, True, True, "Is Correct Answer?", Nothing, False, "Yes;No")
  Public ReadOnly IgnoreSubscriberID As New IntegerElement("IgnoreSubscriberID", Me, False, False, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SubscriberAuthenticationQuestion As Relationship = New SubscriberAuthenticationQuestionRelationship("SubscriberAuthenticationQuestion", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _SubscriberAuthenticationID As New IntegerElement("SubscriberAuthenticationID", Me, True)
  Public ReadOnly _ProductAuthenticationQuestionID As New IntegerElement("ProductAuthenticationQuestionID", Me, True)
  Public ReadOnly _SubscriberAuthenticationQuestion As SubscriberAuthenticationQuestionRelationship = SubscriberAuthenticationQuestion
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberAuthenticationQuestionAnswerSetup", ApplicationSettings)
    _SubscriberAuthenticationQuestion._SubscriberID.LocalElement = _SubscriberID
    _SubscriberAuthenticationQuestion._SubscriberAuthenticationID.LocalElement = _SubscriberAuthenticationID
    _SubscriberAuthenticationQuestion._ProductAuthenticationQuestionID.LocalElement = _ProductAuthenticationQuestionID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class SubscriberAuthenticationQuestionAnswerSetupRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _Answer As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SubscriberAuthenticationID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _ProductAuthenticationQuestionID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberAuthenticationQuestionAnswerSetup(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _Answer.ForeignElement = CType(Instance, SubscriberAuthenticationQuestionAnswerSetup).Answer
    _SubscriberID.ForeignElement = CType(Instance, SubscriberAuthenticationQuestionAnswerSetup)._SubscriberID
    _SubscriberAuthenticationID.ForeignElement = CType(Instance, SubscriberAuthenticationQuestionAnswerSetup)._SubscriberAuthenticationID
    _ProductAuthenticationQuestionID.ForeignElement = CType(Instance, SubscriberAuthenticationQuestionAnswerSetup)._ProductAuthenticationQuestionID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
