Imports sembleWare.Runtime
Imports System
Public Class SubscriberCreditGrantorInfoAffordabilityExpense 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly SubscriberCreditGrantorInfoAffordabilityExpenseID As New IdentityElement("SubscriberCreditGrantorInfoAffordabilityExpenseID", Me, True, Nothing, Nothing)
  Public ReadOnly CompanyName As New StringElement("CompanyName", Me, False, True, True, True, True, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly InstallmentAmt As New DecimalElement("InstallmentAmt", Me, False, True, True, True, True, "Installment Amount", Nothing, "c", Nothing, Nothing, Nothing)
  Public ReadOnly ExpenseTypeInd As New IndicatorElement("ExpenseTypeInd", Me, False, False, True, True, True, True, "Expense Type", Nothing, SubscriberCreditGrantorInfoAffordabilityExpense.ExpenseTypeIndOptions, Nothing)
  Public ReadOnly SubscriberCreditGrantorInfo As Relationship = New SubscriberCreditGrantorInfoRelationship("SubscriberCreditGrantorInfo", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberCreditGrantorID As New IntegerElement("SubscriberCreditGrantorID", Me, True)
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _SubscriberCreditGrantorInfo As SubscriberCreditGrantorInfoRelationship = SubscriberCreditGrantorInfo
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moExpenseTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property ExpenseTypeIndOptions() As IndicatorOptions
    Get
      If moExpenseTypeIndOptions Is Nothing Then
        moExpenseTypeIndOptions = New IndicatorOptions
        moExpenseTypeIndOptions.Add("P", "Property")
        moExpenseTypeIndOptions.Add("V", "Vehicle")
        moExpenseTypeIndOptions.Add("CC", "Credit Card")
        moExpenseTypeIndOptions.Add("CA", "Existing Credit Account")
      End If
      Return moExpenseTypeIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberCreditGrantorInfoAffordabilityExpense", ApplicationSettings)
    _SubscriberCreditGrantorInfo._SubscriberCreditGrantorID.LocalElement = _SubscriberCreditGrantorID
    _SubscriberCreditGrantorInfo._SubscriberID.LocalElement = _SubscriberID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class SubscriberCreditGrantorInfoAffordabilityExpenseRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberCreditGrantorInfoAffordabilityExpenseID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SubscriberCreditGrantorID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberCreditGrantorInfoAffordabilityExpense(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberCreditGrantorInfoAffordabilityExpenseID.ForeignElement = CType(Instance, SubscriberCreditGrantorInfoAffordabilityExpense).SubscriberCreditGrantorInfoAffordabilityExpenseID
    _SubscriberCreditGrantorID.ForeignElement = CType(Instance, SubscriberCreditGrantorInfoAffordabilityExpense)._SubscriberCreditGrantorID
    _SubscriberID.ForeignElement = CType(Instance, SubscriberCreditGrantorInfoAffordabilityExpense)._SubscriberID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberCreditGrantorInfoAffordabilityExpense")
    oList.Columns.Add(New ListColumn("SubscriberCreditGrantorInfoAffordabilityExpenseID", "[A0SubscriberCreditGrantorInfoAffordabilityExpense].[SubscriberCreditGrantorInfoAffordabilityExpenseID]", "SubscriberCreditGrantorInfoAffordabilityExpenseID", DataType.Long, Nothing, True, 0, False, 100, "Subscriber Credit Grantor Info Affordability Expense ID"))
    oList.Columns.Add(New ListColumn("SubscriberCreditGrantorID", "[A0SubscriberCreditGrantorInfoAffordabilityExpense].[SubscriberCreditGrantorID]", "SubscriberCreditGrantorID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber Credit Grantor ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberCreditGrantorInfoAffordabilityExpense].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("CompanyName", "[A0SubscriberCreditGrantorInfoAffordabilityExpense].[CompanyName]", "CompanyName", DataType.String, Nothing, False, 1, True, 200, "Company Name"))
    oList.Columns.Add(New ListColumn("InstallmentAmt", "[A0SubscriberCreditGrantorInfoAffordabilityExpense].[InstallmentAmt]", "InstallmentAmt", DataType.Decimal, "c", False, 0, True, 100, "Installment Amount"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberCreditGrantorInfoAffordabilityExpense]  [A0SubscriberCreditGrantorInfoAffordabilityExpense]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function SubscriberGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberCreditGrantorInfoAffordabilityExpense")
    oList.Columns.Add(New ListColumn("SubscriberCreditGrantorInfoAffordabilityExpenseID", "[A0SubscriberCreditGrantorInfoAffordabilityExpense].[SubscriberCreditGrantorInfoAffordabilityExpenseID]", "SubscriberCreditGrantorInfoAffordabilityExpenseID", DataType.Long, Nothing, True, 0, False, 100, "Subscriber Credit Grantor Enquiry Info Affordability Expense ID"))
    oList.Columns.Add(New ListColumn("SubscriberCreditGrantorID", "[A0SubscriberCreditGrantorInfoAffordabilityExpense].[SubscriberCreditGrantorID]", "SubscriberCreditGrantorID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber Credit Grantor Enquiry ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberCreditGrantorInfoAffordabilityExpense].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("ExpenseTypeInd", "[A0SubscriberCreditGrantorInfoAffordabilityExpense].[ExpenseTypeInd]", "ExpenseTypeInd", DataType.String, Nothing, False, 1, True, 100, "Expense Type"))
    oList.Columns.Add(New ListColumn("CompanyName", "[A0SubscriberCreditGrantorInfoAffordabilityExpense].[CompanyName]", "CompanyName", DataType.String, Nothing, False, 2, True, 200, "Company Name"))
    oList.Columns.Add(New ListColumn("InstallmentAmt", "[A0SubscriberCreditGrantorInfoAffordabilityExpense].[InstallmentAmt]", "InstallmentAmt", DataType.Decimal, "c", False, 0, True, 100, "Installment Amount"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberCreditGrantorInfoAffordabilityExpense]  [A0SubscriberCreditGrantorInfoAffordabilityExpense]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
