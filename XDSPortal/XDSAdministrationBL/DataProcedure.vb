Imports sembleWare.Runtime
Imports System
Public Class DataProcedure 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly DataProcedureID As New IdentityElement("DataProcedureID", Me, True, Nothing, Nothing)
  Public ReadOnly DataProcedureDesc As New StringElement("DataProcedureDesc", Me, False, True, True, True, True, 100, "Data Procedure Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly DataProcedureTypeInd As New IndicatorElement("DataProcedureTypeInd", Me, False, False, True, True, True, True, "Data Procedure Type", Nothing, DataProcedure.DataProcedureTypeIndOptions, Nothing)
  Public ReadOnly ValidationReportInInvalidFileYN As New BooleanElement("ValidationReportInInvalidFileYN", Me, False, True, True, True, True, "Report In Invalid File?", Nothing, False, "Yes;No")
  Public ReadOnly ParameterTypeInd As New IndicatorElement("ParameterTypeInd", Me, False, False, True, True, True, True, "Parameter Type", Nothing, DataProcedure.ParameterTypeIndOptions, Nothing)
  Public ReadOnly AppliesToInd As New IndicatorElement("AppliesToInd", Me, False, False, True, True, True, True, "Applies To", Nothing, DataProcedure.AppliesToIndOptions, Nothing)
  Public ReadOnly TemplateDetails As New TextElement("TemplateDetails", Me, True, True, True, True, Nothing, Nothing, Nothing)
  Public ReadOnly DataProcedureLabelDescription As New StringElement("DataProcedureLabelDescription", Me, False, True, True, False, False, 250, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly MetaDatabase As Relationship = New MetaDatabaseRelationship("MetaDatabase", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly DataProcedureParameter_OwnMany As Relationship = New DataProcedureParameterRelationship("DataProcedureParameter", Nothing, "DataProcedure", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _MetaDatabaseCode As New StringElement("MetaDatabaseCode", Me, False)
  Public ReadOnly _MetaDatabase As MetaDatabaseRelationship = MetaDatabase
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  Public ReadOnly _DataProcedureParameter_OwnMany As DataProcedureParameterRelationship = DataProcedureParameter_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moDataProcedureTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property DataProcedureTypeIndOptions() As IndicatorOptions
    Get
      If moDataProcedureTypeIndOptions Is Nothing Then
        moDataProcedureTypeIndOptions = New IndicatorOptions
        moDataProcedureTypeIndOptions.Add("DE", "Deletion")
        moDataProcedureTypeIndOptions.Add("CO", "Conversion")
        moDataProcedureTypeIndOptions.Add("V", "Required Validation")
        moDataProcedureTypeIndOptions.Add("NV", "Non-Required Validation")
        moDataProcedureTypeIndOptions.Add("DS", "Data Statistics")
        moDataProcedureTypeIndOptions.Add("CA", "Calculation")
        moDataProcedureTypeIndOptions.Add("ME", "Matching Evaluation")
      End If
      Return moDataProcedureTypeIndOptions
    End Get
  End Property
  Private Shared moParameterTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property ParameterTypeIndOptions() As IndicatorOptions
    Get
      If moParameterTypeIndOptions Is Nothing Then
        moParameterTypeIndOptions = New IndicatorOptions
        moParameterTypeIndOptions.Add("N", "None")
        moParameterTypeIndOptions.Add("S", "Default to Selected Field")
        moParameterTypeIndOptions.Add("M", "Multiple Specified Fields")
      End If
      Return moParameterTypeIndOptions
    End Get
  End Property
  Private Shared moAppliesToIndOptions As IndicatorOptions
  Public Shared ReadOnly Property AppliesToIndOptions() As IndicatorOptions
    Get
      If moAppliesToIndOptions Is Nothing Then
        moAppliesToIndOptions = New IndicatorOptions
        moAppliesToIndOptions.Add("A", "All Databases")
        moAppliesToIndOptions.Add("M", "Specific Database")
      End If
      Return moAppliesToIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("DataProcedure", ApplicationSettings)
    _MetaDatabase._MetaDatabaseCode.LocalElement = _MetaDatabaseCode
    AddHandler DataProcedureTypeInd.Changed, AddressOf DataProcedureTypeInd_Changed
    AddHandler AppliesToInd.Changed, AddressOf AppliesToInd_Changed
    'sembleWare: Constructor End - Do Not Modify
    AddHandler MetaDatabase.LimitRelatedList, AddressOf MetaDatabase_LimitRelatedList
  End Sub

#End Region 'sembleWare: Constructor

#Region " LimitRelatedList Events"
  Private Sub MetaDatabase_LimitRelatedList(ByVal List As List)
    List.AndFilters.Add(New ListFilter("MetaDatabaseTypeInd", "MetaDatabaseTypeInd", Constants.MetaDatabase.Types.System, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
  End Sub
#End Region

#Region " Change Events"
  Private Sub AppliesToInd_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BRAppliesToInd(Element, True, True)
  End Sub

  Private Sub DataProcedureTypeInd_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BRDataProcedureTypeInd(Element, True, True)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"

  Public Sub CheckMetaDatabaseType()
    Dim oMetaDatabase As MetaDatabase = Me.MetaDatabase.Instance

    Select Case oMetaDatabase.MetaDatabaseTypeInd.Value
      Case Constants.MetaDatabase.Types.Repository, Constants.MetaDatabase.Types.Enquiry
        Throw New BusinessRuleException("You may not add Data Procedures as this Meta Database Type is not ""Credit Bureau""!")
    End Select
  End Sub

#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreate()
  End Sub

  Public Overrides Sub Load()
    MyBase.Load()
    BRLoad()
  End Sub

  Public Overrides Sub Delete()
    BRDelete()
    MyBase.Delete()
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreate()
    BRAppliesToInd(Me.AppliesToInd, True, False)
    BRDataProcedureTypeInd(Me.DataProcedureTypeInd, True, False)
    BRDataProcedureLabelDescription()
  End Sub

  Private Sub BRLoad()
    BRAppliesToInd(Me.AppliesToInd, True, False)
    BRDataProcedureTypeInd(Me.DataProcedureTypeInd, True, False)
    BRDataProcedureLabelDescription()
  End Sub

  Private Sub BRDelete()
    Dim oMetaDatabase As MetaDatabase = Me.MetaDatabase.Instance

    Select Case oMetaDatabase.MetaDatabaseTypeInd.Value
      Case Constants.MetaDatabase.Types.Repository
        Throw New BusinessRuleException("You cannot delete this Data Procedure as this Meta Database Type is not of ""Credit Bureau""!")
    End Select
  End Sub

  Private Sub BRAppliesToInd(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If SetEnableDisableYN Then
      Me.MetaDatabase.Enabled = False
      Me.MetaDatabase.Mandatory = False
      Select Case Element.ValueAsObject
        Case Constants.DataProcedure.AppliesToTypes.MetaDatabase
          Me.MetaDatabase.Enabled = True
          Me.MetaDatabase.Mandatory = True
      End Select
    End If

    If PerformRulesYN Then
      Me.MetaDatabase.Instance = Nothing
    End If
  End Sub

  Private Sub BRDataProcedureTypeInd(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If SetEnableDisableYN Then
      Me.ValidationReportInInvalidFileYN.Enabled = False
      Me.ValidationReportInInvalidFileYN.Mandatory = False
      Select Case Element.ValueAsObject
        Case Constants.DataProcedure.Types.RequiredValidation, Constants.DataProcedure.Types.NonRequiredValidation
          Me.ValidationReportInInvalidFileYN.Enabled = True
          Me.ValidationReportInInvalidFileYN.Mandatory = True
      End Select
    End If

    If PerformRulesYN Then
      Select Case Element.ValueAsObject
        Case Constants.DataProcedure.Types.RequiredValidation, Constants.DataProcedure.Types.NonRequiredValidation
          ' Do nothing
        Case Else
          Me.ValidationReportInInvalidFileYN.Value = False
      End Select
    End If
  End Sub

  Private Sub BRDataProcedureLabelDescription()
    If Me.IsNew Then
      Me.DataProcedureLabelDescription.Value = "Data Procedure: (New)"
    Else
      Dim sDesc As String = Trim(Me.DataProcedureDesc.Value)
      Me.DataProcedureLabelDescription.Value = "Data Procedure: " & Me.DataProcedureID.Value & " (" & sDesc & ")"
    End If
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class DataProcedureRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _DataProcedureID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New DataProcedure(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _DataProcedureID.ForeignElement = CType(Instance, DataProcedure).DataProcedureID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0DataProcedure")
    oList.Columns.Add(New ListColumn("DataProcedureID", "[A0DataProcedure].[DataProcedureID]", "DataProcedureID", DataType.Long, Nothing, True, 0, True, 100, "Data Procedure ID"))
    oList.Columns.Add(New ListColumn("DataProcedureDesc", "[A0DataProcedure].[DataProcedureDesc]", "DataProcedureDesc", DataType.String, Nothing, False, 1, True, 200, "Data Procedure Description"))
    oList.Columns.Add(New ListColumn("DataProcedureTypeInd", "[A0DataProcedure].[DataProcedureTypeInd]", "DataProcedureTypeInd", DataType.String, Nothing, False, 0, True, 125, "Data Procedure Type"))
    oList.Columns.Add(New ListColumn("ParameterTypeInd", "[A0DataProcedure].[ParameterTypeInd]", "ParameterTypeInd", DataType.String, Nothing, False, 0, True, 125, "Parameter Type"))
    oList.Columns.Add(New ListColumn("AppliesToInd", "[A0DataProcedure].[AppliesToInd]", "AppliesToInd", DataType.String, Nothing, False, 0, True, 100, "Applies To"))
    oList.Columns.Add(New ListColumn("TemplateDetails", "[A0DataProcedure].[TemplateDetails]", "TemplateDetails", DataType.Text, Nothing, False, 0, False, 100, "Template Details"))
    oList.SelectStatement = "select"
    oList.FromClause = "[DataProcedure]  [A0DataProcedure]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function DropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0DataProcedure")
    oList.Columns.Add(New ListColumn("DataProcedureID", "[A0DataProcedure].[DataProcedureID]", "DataProcedureID", DataType.Long, Nothing, True, 1, False, 100, "Data Procedure ID"))
    oList.Columns.Add(New ListColumn("DataProcedureDesc", "[A0DataProcedure].[DataProcedureDesc]", "DataProcedureDesc", DataType.String, Nothing, False, 0, True, 200, "Data Procedure Description"))
    oList.Columns.Add(New ListColumn("DataProcedureTypeInd", "[A0DataProcedure].[DataProcedureTypeInd]", "DataProcedureTypeInd", DataType.String, Nothing, False, 0, True, 125, "Data Procedure Type"))
    oList.Columns.Add(New ListColumn("ParameterTypeInd", "[A0DataProcedure].[ParameterTypeInd]", "ParameterTypeInd", DataType.String, Nothing, False, 0, True, 125, "Parameter Type"))
    oList.SelectStatement = "select"
    oList.FromClause = "[DataProcedure]  [A0DataProcedure]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0DataProcedure")
    oList.Columns.Add(New ListColumn("DataProcedureID", "[A0DataProcedure].[DataProcedureID]", "DataProcedureID", DataType.Long, Nothing, True, 1, False, 100, "Data Procedure ID"))
    oList.Columns.Add(New ListColumn("DataProcedureDesc", "[A0DataProcedure].[DataProcedureDesc]", "DataProcedureDesc", DataType.String, Nothing, False, 0, True, 200, "Data Procedure Description"))
    oList.Columns.Add(New ListColumn("DataProcedureTypeInd", "[A0DataProcedure].[DataProcedureTypeInd]", "DataProcedureTypeInd", DataType.String, Nothing, False, 0, True, 125, "Data Procedure Type"))
    oList.Columns.Add(New ListColumn("ParameterTypeInd", "[A0DataProcedure].[ParameterTypeInd]", "ParameterTypeInd", DataType.String, Nothing, False, 0, True, 125, "Parameter Type"))
    oList.SelectStatement = "select"
    oList.FromClause = "[DataProcedure]  [A0DataProcedure]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function MetaDatabaseGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0DataProcedure")
    oList.Columns.Add(New ListColumn("DataProcedureID", "[A0DataProcedure].[DataProcedureID]", "DataProcedureID", DataType.Long, Nothing, True, 1, False, 100, "Data Procedure ID"))
    oList.Columns.Add(New ListColumn("DataProcedureDesc", "[A0DataProcedure].[DataProcedureDesc]", "DataProcedureDesc", DataType.String, Nothing, False, 0, True, 200, "Data Procedure Description"))
    oList.Columns.Add(New ListColumn("DataProcedureTypeInd", "[A0DataProcedure].[DataProcedureTypeInd]", "DataProcedureTypeInd", DataType.String, Nothing, False, 0, True, 150, "Data Procedure Type"))
    oList.Columns.Add(New ListColumn("ParameterTypeInd", "[A0DataProcedure].[ParameterTypeInd]", "ParameterTypeInd", DataType.String, Nothing, False, 0, True, 100, "Parameter Type"))
    oList.Columns.Add(New ListColumn("TemplateDetails", "[A0DataProcedure].[TemplateDetails]", "TemplateDetails", DataType.Text, Nothing, False, 0, False, 100, "Template Details"))
    oList.SelectStatement = "select"
    oList.FromClause = "[DataProcedure]  [A0DataProcedure]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function FileFormatDeploymentGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0DataProcedure")
    oList.Columns.Add(New ListColumn("DataProcedureID", "[A0DataProcedure].[DataProcedureID]", "DataProcedureID", DataType.Long, Nothing, True, 1, False, 100, "Data Procedure Code"))
    oList.Columns.Add(New ListColumn("DataProcedureDesc", "[A0DataProcedure].[DataProcedureDesc]", "DataProcedureDesc", DataType.String, Nothing, False, 0, True, 200, "Data Procedure Description"))
    oList.Columns.Add(New ListColumn("DataProcedureTypeInd", "[A0DataProcedure].[DataProcedureTypeInd]", "DataProcedureTypeInd", DataType.String, Nothing, False, 0, True, 150, "Data Procedure Type"))
    oList.Columns.Add(New ListColumn("AppliesToInd", "[A0DataProcedure].[AppliesToInd]", "AppliesToInd", DataType.String, Nothing, False, 0, True, 100, "Applies To"))
    oList.SelectStatement = "select"
    oList.FromClause = "[DataProcedure]  [A0DataProcedure]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
