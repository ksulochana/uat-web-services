Imports sembleWare.Runtime
Imports System
Public Class LoaderProcess 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly LoaderQueuedDate As New DateTimeElement("LoaderQueuedDate", Me, False, False, True, True, False, "Queued Date", Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly CompletionPerc As New DecimalElement("CompletionPerc", Me, False, False, True, True, True, "Completion %", Nothing, "##0.00", 0, Nothing, Nothing)
  Public ReadOnly LoaderThread As Relationship = New LoaderThreadRelationship("LoaderThread", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly LoaderProcessStatus As Relationship = New LoaderProcessStatusRelationship("LoaderProcessStatus", Nothing, RelationshipType.Include, True, False, True, Me)
  Public ReadOnly Loader As Relationship = New LoaderRelationship("Loader", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly LoaderProcessType As Relationship = New LoaderProcessTypeRelationship("LoaderProcessType", Nothing, RelationshipType.OwnedBy, True, False, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly LoaderProcessHistory_OwnMany As Relationship = New LoaderProcessHistoryRelationship("LoaderProcessHistory", Nothing, "LoaderProcess", Me)
  Public ReadOnly LoaderProcessMetaDatabase_OwnMany As Relationship = New LoaderProcessMetaDatabaseRelationship("LoaderProcessMetaDatabase", Nothing, "LoaderProcess", Me)
  Public ReadOnly LoaderProcessMetaDataProcedure_OwnMany As Relationship = New LoaderProcessMetaDataProcedureRelationship("LoaderProcessMetaDataProcedure", Nothing, "LoaderProcess", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _LoaderThreadCode As New StringElement("LoaderThreadCode", Me, False)
  Public ReadOnly _LoaderProcessStatusCode As New StringElement("LoaderProcessStatusCode", Me, False)
  Public ReadOnly _LoaderID As New IntegerElement("LoaderID", Me, True)
  Public ReadOnly _LoaderProcessTypeCode As New StringElement("LoaderProcessTypeCode", Me, True)
  Public ReadOnly _LoaderThread As LoaderThreadRelationship = LoaderThread
  Public ReadOnly _LoaderProcessStatus As LoaderProcessStatusRelationship = LoaderProcessStatus
  Public ReadOnly _Loader As LoaderRelationship = Loader
  Public ReadOnly _LoaderProcessType As LoaderProcessTypeRelationship = LoaderProcessType
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  Public ReadOnly _LoaderProcessHistory_OwnMany As LoaderProcessHistoryRelationship = LoaderProcessHistory_OwnMany
  Public ReadOnly _LoaderProcessMetaDatabase_OwnMany As LoaderProcessMetaDatabaseRelationship = LoaderProcessMetaDatabase_OwnMany
  Public ReadOnly _LoaderProcessMetaDataProcedure_OwnMany As LoaderProcessMetaDataProcedureRelationship = LoaderProcessMetaDataProcedure_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("LoaderProcess", ApplicationSettings)
    _LoaderThread._LoaderThreadCode.LocalElement = _LoaderThreadCode
    _LoaderProcessStatus._LoaderProcessStatusCode.LocalElement = _LoaderProcessStatusCode
    _Loader._LoaderID.LocalElement = _LoaderID
    _LoaderProcessType._LoaderProcessTypeCode.LocalElement = _LoaderProcessTypeCode
    'sembleWare: Constructor End - Do Not Modify
    AddHandler LoaderThread.LimitRelatedList, AddressOf LoaderThread_LimitRelatedList
  End Sub

#End Region 'sembleWare: Constructor

#Region " LimitRelatedList Events"
  Private Sub LoaderThread_LimitRelatedList(ByVal List As List)
    Dim oDBList As DBList = List

    oDBList.AndFilters.Add(New ListFilter("LoaderThreadStatusInd", "LoaderThreadStatusInd", Constants.RecordStatus.Active, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
    oDBList.AppendToWhereClause("(LoaderProcessTypeCode = " & ApplicationSettings.QueryBuilder.ToSQL(Me._LoaderProcessTypeCode.Value) & " or LoaderProcessTypeCode is null)")
  End Sub
#End Region

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"

  Public Sub DataLoadSubmit()
    If Not Me._LoaderProcessStatusCode.Value = Constants.LoaderProcess.Statuses.RequireSubmit Then
      Throw New BusinessRuleException("Data Load has already been submitted!")
    End If

    Dim oLoader As Loader = Me.Loader.Instance
    oLoader.CheckMandatoryLoaderParameters()

    With ApplicationSettings
      Try
        .BeginTransaction()
        Me.LoaderQueuedDate.Value = System.DateTime.Now
        Me.LoaderProcessStatus.Instance = XDSAdministrationBL.LoaderProcessStatus.GetQueued(ApplicationSettings)
        Save()
        BRAddLoaderProcessHistory(Me, ApplicationSettings)
        .CommitTransaction()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
    End With
  End Sub

  Public Sub DataValidationSubmit()
    If Not Me.GetDataLoad(Me.Loader.Instance, ApplicationSettings)._LoaderProcessStatusCode.Value = Constants.LoaderProcess.Statuses.Completed Then
      Throw New BusinessRuleException("Cannot Submit for Data Validation as the Data Load has not completed successfully!")
    End If

    If Not Me._LoaderProcessStatusCode.Value = Constants.LoaderProcess.Statuses.RequireSubmit Then
      Throw New BusinessRuleException("Data Validation has already been submitted!")
    End If

    With ApplicationSettings
      Try
        .BeginTransaction()
        Me.LoaderQueuedDate.Value = System.DateTime.Now
        Me.LoaderProcessStatus.Instance = XDSAdministrationBL.LoaderProcessStatus.GetQueued(ApplicationSettings)
        Save()
        BRAddLoaderProcessHistory(Me, ApplicationSettings)
        .CommitTransaction()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
    End With
  End Sub

  Public Sub DataMatchingSubmit()
    If Not Me.GetDataValidation(Me.Loader.Instance, ApplicationSettings)._LoaderProcessStatusCode.Value = Constants.LoaderProcess.Statuses.Completed Then
      Throw New BusinessRuleException("Cannot Submit for Data Matching as the Data Validation has not completed successfully!")
    End If

    If Not Me._LoaderProcessStatusCode.Value = Constants.LoaderProcess.Statuses.RequireSubmit Then
      Throw New BusinessRuleException("Data Matching has already been submitted!")
    End If

    With ApplicationSettings
      Try
        .BeginTransaction()
        Me.LoaderQueuedDate.Value = System.DateTime.Now
        Me.LoaderProcessStatus.Instance = XDSAdministrationBL.LoaderProcessStatus.GetQueued(ApplicationSettings)
        Save()
        BRAddLoaderProcessHistory(Me, ApplicationSettings)
        .CommitTransaction()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
    End With
  End Sub

  Public Sub DataUpdateSubmit()
    If Not Me.GetDataMatching(Me.Loader.Instance, ApplicationSettings)._LoaderProcessStatusCode.Value = Constants.LoaderProcess.Statuses.Completed Then
      Throw New BusinessRuleException("Cannot Submit for Data Update as the Data Matching has not completed successfully!")
    End If

    If Not Me._LoaderProcessStatusCode.Value = Constants.LoaderProcess.Statuses.RequireSubmit Then
      Throw New BusinessRuleException("Data Update has already been submitted!")
    End If

    With ApplicationSettings
      Try
        .BeginTransaction()
        Me.LoaderQueuedDate.Value = System.DateTime.Now
        Me.LoaderProcessStatus.Instance = XDSAdministrationBL.LoaderProcessStatus.GetQueued(ApplicationSettings)
        Save()
        BRAddLoaderProcessHistory(Me, ApplicationSettings)
        .CommitTransaction()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
    End With
  End Sub

  Public Sub DataUndoSubmit()
    If Not Me.GetDataUpdate(Me.Loader.Instance, ApplicationSettings)._LoaderProcessStatusCode.Value = Constants.LoaderProcess.Statuses.Completed Then
      Throw New BusinessRuleException("Cannot Submit for Data Undo as the Data Update has not completed successfully!")
    End If

    If Not Me._LoaderProcessStatusCode.Value = Constants.LoaderProcess.Statuses.RequireSubmit Then
      Throw New BusinessRuleException("Data Undo has already been submitted!")
    End If

    With ApplicationSettings
      Try
        .BeginTransaction()
        Me.LoaderQueuedDate.Value = System.DateTime.Now
        Me.LoaderProcessStatus.Instance = XDSAdministrationBL.LoaderProcessStatus.GetQueued(ApplicationSettings)
        Save()
        BRAddLoaderProcessHistory(Me, ApplicationSettings)
        .CommitTransaction()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
    End With
  End Sub

  Public Sub SetToCompleted()
    Me.LoaderProcessStatus.Instance = XDSAdministrationBL.LoaderProcessStatus.GetCompleted(ApplicationSettings)
    Me.CompletionPerc.Value = 100
    Me.Save()

    Dim oLoaderProcess As LoaderProcess = Nothing

    Select Case Me._LoaderProcessTypeCode.Value
      Case Constants.LoaderProcess.Types.DataLoad
        oLoaderProcess = LoaderProcess.GetDataValidation(Me.Loader.Instance, ApplicationSettings)
      Case Constants.LoaderProcess.Types.DataValid
        oLoaderProcess = LoaderProcess.GetDataMatching(Me.Loader.Instance, ApplicationSettings)
      Case Constants.LoaderProcess.Types.DataMatch
        oLoaderProcess = LoaderProcess.GetDataUpdate(Me.Loader.Instance, ApplicationSettings)
      Case Constants.LoaderProcess.Types.DataUpdate
        oLoaderProcess = LoaderProcess.GetDataUndo(Me.Loader.Instance, ApplicationSettings)
    End Select
    If Not oLoaderProcess Is Nothing Then
      oLoaderProcess.Save()
      oLoaderProcess.CheckForQuickLoad()
    End If
  End Sub
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreateLoad()
  End Sub

  Public Overrides Sub Load()
    MyBase.Load()
    BRCreateLoad()
  End Sub

  Public Overrides Sub Save()
    MyBase.Save()
    BRSave()
  End Sub
#End Region

#Region " Methods"
  Public Sub CheckForQuickLoad()
    Dim oLoader As Loader = Me.Loader.Instance

    If oLoader.EnableQuickLoadYN.Value Then
      Select Case Me._LoaderProcessTypeCode.Value
        Case Constants.LoaderProcess.Types.DataLoad, Constants.LoaderProcess.Types.DataMatch, Constants.LoaderProcess.Types.DataUpdate
          Me.LoaderThread.Instance = XDSAdministrationBL.LoaderThread.GetLoaderThread(Me._LoaderProcessTypeCode.Value, ApplicationSettings)
          Me.Save()
        Case Constants.LoaderProcess.Types.DataValid
          Me.LoaderThread.Instance = XDSAdministrationBL.LoaderThread.GetLoaderThread(Me._LoaderProcessTypeCode.Value, ApplicationSettings)
          Me.DataValidationSubmit()
      End Select
    End If
  End Sub

  Public Sub Execute(ByVal ConnectString As String)
    Dim sServerName As String = ""
    Dim sDatabaseName As String = ""
    Dim sUsername As String = ""
    Dim sPassword As String = ""
    Dim sItems() As String = ConnectString.Split(";")
    Dim sItem As String

    For Each sItem In sItems
      Dim sSubItems() As String = sItem.Split("=")
      Select Case sSubItems(0).ToLower()
        Case "data source"
          sServerName = sSubItems(1)
        Case "initial catalog"
          sDatabaseName = sSubItems(1)
        Case "user id"
          sUsername = sSubItems(1)
        Case "pwd"
          sPassword = sSubItems(1)
      End Select
    Next

    Dim oLoader As Loader = Me.Loader.Instance
    Dim oSubscriber As Subscriber = oLoader.Subscriber.Instance
    Dim oLoaderEngineManager As LoaderEngineManager.Manager = New LoaderEngineManager.Manager(oLoader.FileFormatDeployment.Instance, oLoader.FileFormat.Instance, sServerName, sDatabaseName, sUsername, sPassword, ApplicationSettings)

    Select Case Me._LoaderProcessTypeCode.Value
      Case Constants.LoaderProcess.Types.DataLoad
        oLoaderEngineManager.ExecuteDataLoad(Me._LoaderID.Value, Me._LoaderThreadCode.Value)
      Case Constants.LoaderProcess.Types.DataValid
        oLoaderEngineManager.ExecuteDataValidation(Me._LoaderID.Value, Me._LoaderThreadCode.Value)
      Case Constants.LoaderProcess.Types.DataMatch
        oLoaderEngineManager.ExecuteDataMatching(Me._LoaderID.Value, Me._LoaderThreadCode.Value)
      Case Constants.LoaderProcess.Types.DataUpdate
        oLoaderEngineManager.ExecuteDataUpdate(Me._LoaderID.Value, Me._LoaderThreadCode.Value)
      Case Constants.LoaderProcess.Types.DataUndo
        oLoaderEngineManager.ExecuteDataUndo(Me._LoaderID.Value, Me._LoaderThreadCode.Value)
    End Select
    Me.IgnoreCacheOnce = True
    Me.Load()
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreateLoad()
    Me.LoaderThread.Enabled = False
    Me.LoaderThread.Mandatory = False
    Select Case Me._LoaderProcessStatusCode.Value
      Case Constants.LoaderProcess.Statuses.RequireSubmit
        Me.LoaderThread.Enabled = True
        Me.LoaderThread.Mandatory = True
    End Select
  End Sub

  Private Sub BRSave()
    If Not Me._LoaderProcessStatusCode.Value = Constants.LoaderProcess.Statuses.RequireSubmit Then
      Me.LoaderThread.Enabled = False
    End If
  End Sub

  Private Sub BRAddLoaderProcessHistory(ByVal LoaderProcess As LoaderProcess, ByVal ApplicationSettings As ApplicationSettings)
    Dim oLoader As Loader = LoaderProcess.Loader.Instance
    Dim oLoaderProcessHistory As LoaderProcessHistory = oLoader.LoaderProcessHistory_OwnMany.NewInstance()
    Dim sUsername = Constants.XDSAdministrationServicesUser

    If Not GlobalSettings.SystemUser.LoggedInSystemUserID = 0 Then
      sUsername = XDSAdministrationBL.GlobalSettings.SystemUser.LoggedInSystemUsername()
    End If
    oLoaderProcessHistory.ActionedByUser.Value = sUsername
    oLoaderProcessHistory.ActionedOnDate.Value = System.DateTime.Now
    oLoaderProcessHistory.LoaderProcess.Instance = LoaderProcess

    Select Case LoaderProcess._LoaderProcessTypeCode.Value
      Case Constants.LoaderProcess.Types.DataLoad
        oLoaderProcessHistory.ActionedDetail.Value = "Submitted"
      Case Constants.LoaderProcess.Types.DataValid
        oLoaderProcessHistory.ActionedDetail.Value = "Submitted"
      Case Constants.LoaderProcess.Types.DataMatch
        oLoaderProcessHistory.ActionedDetail.Value = "Submitted"
      Case Constants.LoaderProcess.Types.DataUpdate
        oLoaderProcessHistory.ActionedDetail.Value = "Submitted"
      Case Constants.LoaderProcess.Types.DataUndo
        oLoaderProcessHistory.ActionedDetail.Value = "Submitted"
    End Select

    oLoaderProcessHistory.Save()
  End Sub
#End Region

#Region " Shared Procedures"
  Private Shared Function GetLoaderProcess(ByVal Loader As Loader, ByVal LoaderProcessType As LoaderProcessType, ByVal ApplicationSettings As ApplicationSettings) As LoaderProcess
    Dim oLoaderProcess As LoaderProcess = Loader.LoaderProcess_OwnMany.NewInstance()

    Try
      oLoaderProcess.IgnoreCacheOnce = True
      oLoaderProcess.LoaderProcessType.Instance = LoaderProcessType
      oLoaderProcess.Load()
    Catch oException As RecordNotFoundException
      oLoaderProcess = Loader.LoaderProcess_OwnMany.NewInstance()
      oLoaderProcess.LoaderProcessType.Instance = LoaderProcessType
      oLoaderProcess._LoaderProcessStatusCode.Value = Constants.LoaderProcess.Statuses.RequireSubmit
    Catch oException As Exception
      Throw oException
    End Try
    Return oLoaderProcess
  End Function

  Public Shared Function GetDataLoad(ByVal Loader As Loader, ByVal ApplicationSettings As ApplicationSettings) As LoaderProcess
    Return GetLoaderProcess(Loader, XDSAdministrationBL.LoaderProcessType.GetDataLoad(ApplicationSettings), ApplicationSettings)
  End Function

  Public Shared Function GetDataValidation(ByVal Loader As Loader, ByVal ApplicationSettings As ApplicationSettings) As LoaderProcess
    Return GetLoaderProcess(Loader, XDSAdministrationBL.LoaderProcessType.GetDataValidation(ApplicationSettings), ApplicationSettings)
  End Function

  Public Shared Function GetDataMatching(ByVal Loader As Loader, ByVal ApplicationSettings As ApplicationSettings) As LoaderProcess
    Return GetLoaderProcess(Loader, XDSAdministrationBL.LoaderProcessType.GetDataMatching(ApplicationSettings), ApplicationSettings)
  End Function

  Public Shared Function GetDataUpdate(ByVal Loader As Loader, ByVal ApplicationSettings As ApplicationSettings) As LoaderProcess
    Return GetLoaderProcess(Loader, XDSAdministrationBL.LoaderProcessType.GetDataUpdate(ApplicationSettings), ApplicationSettings)
  End Function

  Public Shared Function GetDataUndo(ByVal Loader As Loader, ByVal ApplicationSettings As ApplicationSettings) As LoaderProcess
    Return GetLoaderProcess(Loader, XDSAdministrationBL.LoaderProcessType.GetDataUndo(ApplicationSettings), ApplicationSettings)
  End Function

  Public Shared Sub UpdateLoaderProcessStatus(ByVal LoaderID As Long, ByVal LoaderProcessTypeCode As String, ByVal StatusInd As String, ByVal ApplicationSettings As ApplicationSettings)
    Dim oLoaderProcess As LoaderProcess = New LoaderProcessRelationship(ApplicationSettings).NewInstance()
    oLoaderProcess._LoaderID.Load(LoaderID)
    oLoaderProcess._LoaderProcessTypeCode.Load(LoaderProcessTypeCode)
    oLoaderProcess.IgnoreCacheOnce = True
    oLoaderProcess.Load()

    If StatusInd = Constants.LoaderProcess.Statuses.Completed Then
      oLoaderProcess.SetToCompleted()
    Else
      oLoaderProcess._LoaderProcessStatusCode.Value = StatusInd
      oLoaderProcess.Save()
    End If
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class LoaderProcessRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _LoaderID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _LoaderProcessTypeCode As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New LoaderProcess(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _LoaderID.ForeignElement = CType(Instance, LoaderProcess)._LoaderID
    _LoaderProcessTypeCode.ForeignElement = CType(Instance, LoaderProcess)._LoaderProcessTypeCode
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0LoaderProcess")
    oList.Columns.Add(New ListColumn("LoaderID", "[A0LoaderProcess].[LoaderID]", "LoaderID", DataType.Integer, Nothing, True, 0, False, 100, "Loader ID"))
    oList.Columns.Add(New ListColumn("LoaderProcessTypeCode", "[A0LoaderProcess].[LoaderProcessTypeCode]", "LoaderProcessTypeCode", DataType.String, Nothing, True, 0, False, 100, "Loader Process Type Code"))
    oList.Columns.Add(New ListColumn("LoaderProcessTypeCode1", "[A1LoaderProcessType].[LoaderProcessTypeCode]", "LoaderProcessTypeCode", DataType.String, Nothing, False, 1, True, 100, "Loader Process Type Code"))
    oList.Columns.Add(New ListColumn("LoaderProcessTypeDesc", "[A1LoaderProcessType].[LoaderProcessTypeDesc]", "LoaderProcessTypeDesc", DataType.String, Nothing, False, 0, True, 200, "Loader Process Type Description"))
    oList.Columns.Add(New ListColumn("LoaderThreadCode", "[A2LoaderThread].[LoaderThreadCode]", "LoaderThreadCode", DataType.String, Nothing, False, 0, True, 100, "Loader Thread Code"))
    oList.Columns.Add(New ListColumn("LoaderThreadDesc", "[A2LoaderThread].[LoaderThreadDesc]", "LoaderThreadDesc", DataType.String, Nothing, False, 0, True, 200, "Loader Thread Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "(([LoaderProcess]  [A0LoaderProcess]" + _
           "    join [LoaderProcessType]  [A1LoaderProcessType] on [A0LoaderProcess].[LoaderProcessTypeCode] = [A1LoaderProcessType].[LoaderProcessTypeCode])" + _
           "    left join [LoaderThread]  [A2LoaderThread] on [A0LoaderProcess].[LoaderThreadCode] = [A2LoaderThread].[LoaderThreadCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function





  Public Function DropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0LoaderProcess")
    oList.Columns.Add(New ListColumn("LoaderID", "[A0LoaderProcess].[LoaderID]", "LoaderID", DataType.Integer, Nothing, True, 0, False, 100, "Loader ID"))
    oList.Columns.Add(New ListColumn("LoaderProcessTypeCode", "[A0LoaderProcess].[LoaderProcessTypeCode]", "LoaderProcessTypeCode", DataType.String, Nothing, True, 1, True, 100, "Loader Process Type Code"))
    oList.SelectStatement = "select"
    oList.FromClause = "[LoaderProcess]  [A0LoaderProcess]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function InProcessGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0LoaderProcess")
    oList.Columns.Add(New ListColumn("LoaderID", "[A0LoaderProcess].[LoaderID]", "LoaderID", DataType.Integer, Nothing, True, -2, True, 100, "Loader ID"))
    oList.Columns.Add(New ListColumn("LoaderProcessTypeCode", "[A0LoaderProcess].[LoaderProcessTypeCode]", "LoaderProcessTypeCode", DataType.String, Nothing, True, 0, False, 100, "Loader Process Type Code"))
    oList.Columns.Add(New ListColumn("LoaderDate", "[A3Loader].[LoaderDate]", "LoaderDate", DataType.DateTime, "d", False, 0, False, 100, "Loader Date"))
    oList.Columns.Add(New ListColumn("SubscriberName", "[A4Subscriber].[SubscriberName]", "SubscriberName", DataType.String, Nothing, False, 0, True, 200, "Subscriber Name"))
    oList.Columns.Add(New ListColumn("OriginalFileName", "[A3Loader].[OriginalFileName]", "OriginalFileName", DataType.String, Nothing, False, 0, True, 150, "Original File Name"))
    oList.Columns.Add(New ListColumn("LoaderQueuedDate", "[A0LoaderProcess].[LoaderQueuedDate]", "LoaderQueuedDate", DataType.DateTime, "g", False, -1, True, 150, "Queued Date"))
    oList.Columns.Add(New ListColumn("LoaderThreadCode", "[A2LoaderThread].[LoaderThreadCode]", "LoaderThreadCode", DataType.String, Nothing, False, 0, False, 100, "Loader Thread Code"))
    oList.Columns.Add(New ListColumn("LoaderThreadDesc", "[A2LoaderThread].[LoaderThreadDesc]", "LoaderThreadDesc", DataType.String, Nothing, False, 0, True, 150, "Loader Thread"))
    oList.Columns.Add(New ListColumn("LoaderProcessTypeCode1", "[A1LoaderProcessType].[LoaderProcessTypeCode]", "LoaderProcessTypeCode", DataType.String, Nothing, False, 0, False, 100, "Loader Process Type Code"))
    oList.Columns.Add(New ListColumn("LoaderProcessTypeDesc", "[A1LoaderProcessType].[LoaderProcessTypeDesc]", "LoaderProcessTypeDesc", DataType.String, Nothing, False, 0, True, 150, "Loader Process Type"))
    oList.Columns.Add(New ListColumn("LoaderProcessStatusCode", "[A5LoaderProcessStatus].[LoaderProcessStatusCode]", "LoaderProcessStatusCode", DataType.String, Nothing, False, 0, False, 100, "Loader Process Status Code"))
    oList.Columns.Add(New ListColumn("LoaderProcessStatusDesc", "[A5LoaderProcessStatus].[LoaderProcessStatusDesc]", "LoaderProcessStatusDesc", DataType.String, Nothing, False, 0, True, 150, "Loader Process Status"))
    oList.Columns.Add(New ListColumn("CompletionPerc", "[A0LoaderProcess].[CompletionPerc]", "CompletionPerc", DataType.Decimal, "##0.00", False, 0, True, 100, "Completion %"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A4Subscriber].[SubscriberID]", "SubscriberID", DataType.Long, Nothing, False, 0, False, 100, "Subscriber ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "(((([LoaderProcess]  [A0LoaderProcess]" + _
           "    join [LoaderProcessType]  [A1LoaderProcessType] on [A0LoaderProcess].[LoaderProcessTypeCode] = [A1LoaderProcessType].[LoaderProcessTypeCode])" + _
           "    left join [LoaderThread]  [A2LoaderThread] on [A0LoaderProcess].[LoaderThreadCode] = [A2LoaderThread].[LoaderThreadCode])" + _
           "    join ([Loader]  [A3Loader]" + _
           "    join [Subscriber]  [A4Subscriber] on [A3Loader].[SubscriberID] = [A4Subscriber].[SubscriberID]) on [A0LoaderProcess].[LoaderID] = [A3Loader].[LoaderID])" + _
           "    join [LoaderProcessStatus]  [A5LoaderProcessStatus] on [A0LoaderProcess].[LoaderProcessStatusCode] = [A5LoaderProcessStatus].[LoaderProcessStatusCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function CompletedGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0LoaderProcess")
    oList.Columns.Add(New ListColumn("LoaderID", "[A0LoaderProcess].[LoaderID]", "LoaderID", DataType.Integer, Nothing, True, -2, True, 100, "Loader ID"))
    oList.Columns.Add(New ListColumn("LoaderProcessTypeCode", "[A0LoaderProcess].[LoaderProcessTypeCode]", "LoaderProcessTypeCode", DataType.String, Nothing, True, 0, False, 100, "Loader Process Type Code"))
    oList.Columns.Add(New ListColumn("LoaderDate", "[A1Loader].[LoaderDate]", "LoaderDate", DataType.DateTime, "d", False, 0, False, 100, "Loader Date"))
    oList.Columns.Add(New ListColumn("SubscriberName", "[A2Subscriber].[SubscriberName]", "SubscriberName", DataType.String, Nothing, False, 0, True, 200, "Subscriber Name"))
    oList.Columns.Add(New ListColumn("OriginalFileName", "[A1Loader].[OriginalFileName]", "OriginalFileName", DataType.String, Nothing, False, 0, True, 150, "Original File Name"))
    oList.Columns.Add(New ListColumn("LoaderQueuedDate", "[A0LoaderProcess].[LoaderQueuedDate]", "LoaderQueuedDate", DataType.DateTime, "g", False, -1, True, 150, "Queued Date"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A2Subscriber].[SubscriberID]", "SubscriberID", DataType.Long, Nothing, False, 0, False, 100, "Subscriber ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "([LoaderProcess]  [A0LoaderProcess]" + _
           "    join ([Loader]  [A1Loader]" + _
           "    join [Subscriber]  [A2Subscriber] on [A1Loader].[SubscriberID] = [A2Subscriber].[SubscriberID]) on [A0LoaderProcess].[LoaderID] = [A1Loader].[LoaderID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
