Imports sembleWare.Runtime
Imports System
Public Class ConsumerConflict 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly Consumer As Relationship = New ConsumerRelationship("Consumer", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly ConflictConsumer As Relationship = New ConsumerRelationship("ConflictConsumer", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _ConsumerID As New IntegerElement("ConsumerID", Me, True)
  Public ReadOnly _ConflictConsumerID As New IntegerElement("ConflictConsumerID", Me, True)
  Public ReadOnly _Consumer As ConsumerRelationship = Consumer
  Public ReadOnly _ConflictConsumer As ConsumerRelationship = ConflictConsumer
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("ConsumerConflict", ApplicationSettings)
    _Consumer._ConsumerID.LocalElement = _ConsumerID
    _ConflictConsumer._ConsumerID.LocalElement = _ConflictConsumerID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"

  Public Sub Merge()
    ConsumerConflict.ConsumerMerge(Me._ConsumerID.Value, Me._ConflictConsumerID.Value, GlobalSettings.SystemUser.LoggedInSystemUserID, ApplicationSettings)
  End Sub

  Public Sub MergeAll()
    ConsumerConflict.ConsumerMergeAll(Me._ConsumerID.Value, GlobalSettings.SystemUser.LoggedInSystemUserID, ApplicationSettings)
  End Sub

#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Shared Procedures"
  Public Shared Sub ConsumerMerge(ByVal ConsumerID As Integer, ByVal MergeToConsumerID As Integer, ByVal ActionedBySystemUserID As Integer, ByVal ApplicationSettings As ApplicationSettings)
    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand

    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spConsumer_U_Merge"

    oCommand.Parameters.Add("@ConsumerID", SqlDbType.Int)
    oCommand.Parameters.Add("@MergeToConsumerID", SqlDbType.Int)
    oCommand.Parameters.Add("@ActionedBySystemUserID", SqlDbType.Int)

    oCommand.Parameters("@ConsumerID").Value = ConsumerID
    oCommand.Parameters("@MergeToConsumerID").Value = MergeToConsumerID
    oCommand.Parameters("@ActionedBySystemUserID").Value = ActionedBySystemUserID

    ApplicationSettings.ActionQuery(oCommand)
  End Sub

  Public Shared Sub ConsumerMergeAll(ByVal ConsumerID As Integer, ByVal ActionedBySystemUserID As Integer, ByVal ApplicationSettings As ApplicationSettings)
    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand

    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spConsumerConflict_U_MergeAll"

    oCommand.Parameters.Add("@ConsumerID", SqlDbType.Int)
    oCommand.Parameters.Add("@ActionedBySystemUserID", SqlDbType.Int)

    oCommand.Parameters("@ConsumerID").Value = ConsumerID
    oCommand.Parameters("@ActionedBySystemUserID").Value = ActionedBySystemUserID

    ApplicationSettings.ActionQuery(oCommand)
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class ConsumerConflictRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _ConsumerID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _ConflictConsumerID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New ConsumerConflict(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _ConsumerID.ForeignElement = CType(Instance, ConsumerConflict)._ConsumerID
    _ConflictConsumerID.ForeignElement = CType(Instance, ConsumerConflict)._ConflictConsumerID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0ConsumerConflict")
    oList.Columns.Add(New ListColumn("ConsumerID", "[A0ConsumerConflict].[ConsumerID]", "ConsumerID", DataType.Integer, Nothing, True, 0, False, 100, "Consumer ID"))
    oList.Columns.Add(New ListColumn("ConflictConsumerID", "[A0ConsumerConflict].[ConflictConsumerID]", "ConflictConsumerID", DataType.Integer, Nothing, True, 0, False, 100, "Conflict Consumer ID"))
    oList.Columns.Add(New ListColumn("ConsumerID1", "[A1ConflictConsumer].[ConsumerID]", "ConsumerID", DataType.Long, Nothing, False, 1, True, 100, "Consumer ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A1ConflictConsumer].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("PassportNo", "[A1ConflictConsumer].[PassportNo]", "PassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No / Other ID No"))
    oList.Columns.Add(New ListColumn("FirstName", "[A1ConflictConsumer].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 200, "First Name"))
    oList.Columns.Add(New ListColumn("FirstInitial", "[A1ConflictConsumer].[FirstInitial]", "FirstInitial", DataType.String, Nothing, False, 0, True, 100, "First Initial"))
    oList.Columns.Add(New ListColumn("Surname", "[A1ConflictConsumer].[Surname]", "Surname", DataType.String, Nothing, False, 0, True, 200, "Surname"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A1ConflictConsumer].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Date of Birth"))
    oList.Columns.Add(New ListColumn("GenderInd", "[A1ConflictConsumer].[GenderInd]", "GenderInd", DataType.String, Nothing, False, 0, True, 100, "Gender"))
    oList.Columns.Add(New ListColumn("SubscriberName", "null", "SubscriberName", DataType.String, Nothing, False, 0, True, 200, "Subscriber Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "([ConsumerConflict]  [A0ConsumerConflict]" + _
           "    join [Consumer]  [A1ConflictConsumer] on [A0ConsumerConflict].[ConflictConsumerID] = [A1ConflictConsumer].[ConsumerID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
