Imports sembleWare.Runtime
Imports System
Public Class ConsumerHistory 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly ConsumerHistoryID As New IdentityElement("ConsumerHistoryID", Me, True, Nothing, Nothing)
  Public ReadOnly ActionedOnDate As New DateTimeElement("ActionedOnDate", Me, False, False, True, True, True, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly ActionedBySystemUser As New StringElement("ActionedBySystemUser", Me, False, False, True, True, True, 50, "Actioned By", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ActionDesc As New StringElement("ActionDesc", Me, False, False, True, True, True, 100, "Action Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ActionDetails As New TextElement("ActionDetails", Me, False, True, True, True, Nothing, Nothing, Nothing)
  Public ReadOnly Consumer As Relationship = New ConsumerRelationship("Consumer", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _ConsumerID As New IntegerElement("ConsumerID", Me, True)
  Public ReadOnly _Consumer As ConsumerRelationship = Consumer
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("ConsumerHistory", ApplicationSettings)
    _Consumer._ConsumerID.LocalElement = _ConsumerID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class ConsumerHistoryRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _ConsumerHistoryID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _ConsumerID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New ConsumerHistory(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _ConsumerHistoryID.ForeignElement = CType(Instance, ConsumerHistory).ConsumerHistoryID
    _ConsumerID.ForeignElement = CType(Instance, ConsumerHistory)._ConsumerID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0ConsumerHistory")
    oList.Columns.Add(New ListColumn("ConsumerID", "[A0ConsumerHistory].[ConsumerID]", "ConsumerID", DataType.Integer, Nothing, True, 0, False, 100, "Consumer ID"))
    oList.Columns.Add(New ListColumn("ConsumerHistoryID", "[A0ConsumerHistory].[ConsumerHistoryID]", "ConsumerHistoryID", DataType.Long, Nothing, True, 0, False, 100, "Consumer History ID"))
    oList.Columns.Add(New ListColumn("ActionedOnDate", "[A0ConsumerHistory].[ActionedOnDate]", "ActionedOnDate", DataType.DateTime, "g", False, -1, True, 100, "Actioned On Date"))
    oList.Columns.Add(New ListColumn("ActionedBySystemUser", "[A0ConsumerHistory].[ActionedBySystemUser]", "ActionedBySystemUser", DataType.String, Nothing, False, 0, True, 100, "Actioned By"))
    oList.Columns.Add(New ListColumn("ActionDesc", "[A0ConsumerHistory].[ActionDesc]", "ActionDesc", DataType.String, Nothing, False, 0, True, 200, "Action Description"))
    oList.Columns.Add(New ListColumn("ActionDetails", "[A0ConsumerHistory].[ActionDetails]", "ActionDetails", DataType.Text, Nothing, False, 0, True, 300, "Action Details"))
    oList.SelectStatement = "select"
    oList.FromClause = "[ConsumerHistory]  [A0ConsumerHistory]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
