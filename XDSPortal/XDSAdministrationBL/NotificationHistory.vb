Imports sembleWare.Runtime
Imports System
Public Class NotificationHistory 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly NotificationHistoryID As New IdentityElement("NotificationHistoryID", Me, True, Nothing, Nothing)
  Public ReadOnly NotificationHistoryDate As New DateTimeElement("NotificationHistoryDate", Me, False, False, True, True, True, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly SMTPServerName As New StringElement("SMTPServerName", Me, False, False, True, True, True, 100, "SMTP Server Name", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SenderEmailAddress As New StringElement("SenderEmailAddress", Me, False, False, True, True, True, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RecipientEmailAddress As New StringElement("RecipientEmailAddress", Me, False, False, True, True, True, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly EmailSubject As New StringElement("EmailSubject", Me, False, False, True, True, True, 250, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly EmailBody As New TextElement("EmailBody", Me, False, True, True, True, Nothing, Nothing, Nothing)
  Public ReadOnly StatusInd As New IndicatorElement("StatusInd", Me, False, False, False, True, True, True, "Status", Nothing, NotificationHistory.StatusIndOptions, "Q")
  Public ReadOnly ProcessedDate As New DateTimeElement("ProcessedDate", Me, False, False, True, True, False, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly ErrorDetails As New TextElement("ErrorDetails", Me, False, True, True, False, Nothing, Nothing, Nothing)
  Public ReadOnly NotificationQueue As Relationship = New NotificationQueueRelationship("NotificationQueue", Nothing, RelationshipType.Include, True, True, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _NotificationQueueID As New IntegerElement("NotificationQueueID", Me, False)
  Public ReadOnly _NotificationQueue As NotificationQueueRelationship = NotificationQueue
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property StatusIndOptions() As IndicatorOptions
    Get
      If moStatusIndOptions Is Nothing Then
        moStatusIndOptions = New IndicatorOptions
        moStatusIndOptions.Add("Q", "Queued")
        moStatusIndOptions.Add("P", "Processing")
        moStatusIndOptions.Add("S", "Sent")
        moStatusIndOptions.Add("F", "Failed")
      End If
      Return moStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("NotificationHistory", ApplicationSettings)
    _NotificationQueue._NotificationQueueID.LocalElement = _NotificationQueueID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class NotificationHistoryRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _NotificationHistoryID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New NotificationHistory(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _NotificationHistoryID.ForeignElement = CType(Instance, NotificationHistory).NotificationHistoryID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0NotificationHistory")
    oList.Columns.Add(New ListColumn("NotificationHistoryID", "[A0NotificationHistory].[NotificationHistoryID]", "NotificationHistoryID", DataType.Long, Nothing, True, 0, True, 100, "Notification History ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "[NotificationHistory]  [A0NotificationHistory]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
