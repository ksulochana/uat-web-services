Imports sembleWare.Runtime
Imports System
Public Class CommercialAuditor 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly CommercialAuditorID As New IdentityElement("CommercialAuditorID", Me, True, Nothing, Nothing)
  Public ReadOnly ActStartDate As New DateTimeElement("ActStartDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly ActEndDate As New DateTimeElement("ActEndDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly ActExpiryDate As New DateTimeElement("ActExpiryDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly ReferenceNo As New StringElement("ReferenceNo", Me, False, True, True, True, False, 10, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly IsWithdrawnFromPublicYN As New BooleanElement("IsWithdrawnFromPublicYN", Me, False, True, True, True, True, "Is Withdrawn From Public?", Nothing, False, "Yes;No")
  Public ReadOnly ActIndMPYNo As New StringElement("ActIndMPYNo", Me, False, True, True, True, False, 10, "Act Ind MPY No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RegisteredEntryDate As New DateTimeElement("RegisteredEntryDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly CM31Date As New DateTimeElement("CM31Date", Me, False, True, True, True, False, "CM 31 Date", Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly ReceiveDate As New DateTimeElement("ReceiveDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly FineLetter As New IntegerElement("FineLetter", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RecordStatusInd As New IndicatorElement("RecordStatusInd", Me, False, False, False, True, True, True, "Record Status", Nothing, CommercialAuditor.RecordStatusIndOptions, "A")
  Public ReadOnly DeletedReason As New StringElement("DeletedReason", Me, False, True, True, True, False, 500, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LastUpdatedDate As New DateTimeElement("LastUpdatedDate", Me, False, False, True, True, False, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly Loader As Relationship = New LoaderRelationship("Loader", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Profession As Relationship = New ProfessionRelationship("Profession", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly AuditorType As Relationship = New AuditorTypeRelationship("AuditorType", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly AuditorStatus As Relationship = New AuditorStatusRelationship("AuditorStatus", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly Auditor As Relationship = New AuditorRelationship("Auditor", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly Commercial As Relationship = New CommercialRelationship("Commercial", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _LoaderID As New IntegerElement("LoaderID", Me, False)
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, False)
  Public ReadOnly _ProfessionCode As New StringElement("ProfessionCode", Me, False)
  Public ReadOnly _AuditorTypeCode As New StringElement("AuditorTypeCode", Me, False)
  Public ReadOnly _AuditorStatusCode As New StringElement("AuditorStatusCode", Me, False)
  Public ReadOnly _AuditorID As New IntegerElement("AuditorID", Me, False)
  Public ReadOnly _CommercialID As New IntegerElement("CommercialID", Me, True)
  Public ReadOnly _Loader As LoaderRelationship = Loader
  Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
  Public ReadOnly _Profession As ProfessionRelationship = Profession
  Public ReadOnly _AuditorType As AuditorTypeRelationship = AuditorType
  Public ReadOnly _AuditorStatus As AuditorStatusRelationship = AuditorStatus
  Public ReadOnly _Auditor As AuditorRelationship = Auditor
  Public ReadOnly _Commercial As CommercialRelationship = Commercial
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moRecordStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property RecordStatusIndOptions() As IndicatorOptions
    Get
      If moRecordStatusIndOptions Is Nothing Then
        moRecordStatusIndOptions = New IndicatorOptions
        moRecordStatusIndOptions.Add("A", "Active")
        moRecordStatusIndOptions.Add("D", "Deleted")
      End If
      Return moRecordStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("CommercialAuditor", ApplicationSettings)
    _Loader._LoaderID.LocalElement = _LoaderID
    _Subscriber._SubscriberID.LocalElement = _SubscriberID
    _Profession._ProfessionCode.LocalElement = _ProfessionCode
    _AuditorType._AuditorTypeCode.LocalElement = _AuditorTypeCode
    _AuditorStatus._AuditorStatusCode.LocalElement = _AuditorStatusCode
    _Auditor._AuditorID.LocalElement = _AuditorID
    _Commercial._CommercialID.LocalElement = _CommercialID
    'sembleWare: Constructor End - Do Not Modify
    Me.DeleteType = CustomDBPart.enumDeleteType.ChangeRecordStatus
  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class CommercialAuditorRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _CommercialAuditorID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _CommercialID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New CommercialAuditor(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _CommercialAuditorID.ForeignElement = CType(Instance, CommercialAuditor).CommercialAuditorID
    _CommercialID.ForeignElement = CType(Instance, CommercialAuditor)._CommercialID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0CommercialAuditor")
    oList.Columns.Add(New ListColumn("CommercialID", "[A0CommercialAuditor].[CommercialID]", "CommercialID", DataType.Integer, Nothing, True, 0, False, 100, "Commercial ID"))
    oList.Columns.Add(New ListColumn("CommercialAuditorID", "[A0CommercialAuditor].[CommercialAuditorID]", "CommercialAuditorID", DataType.Long, Nothing, True, 0, False, 100, "Commercial Auditor ID"))
    oList.Columns.Add(New ListColumn("LastUpdatedDate", "[A0CommercialAuditor].[LastUpdatedDate]", "LastUpdatedDate", DataType.DateTime, "g", False, -1, True, 100, "Last Updated Date"))
    oList.Columns.Add(New ListColumn("ProfessionNo", "[A1Auditor].[ProfessionNo]", "ProfessionNo", DataType.String, Nothing, False, 2, True, 100, "Profession No"))
    oList.Columns.Add(New ListColumn("AuditorName", "[A1Auditor].[AuditorName]", "AuditorName", DataType.String, Nothing, False, 0, True, 200, "Auditor Name"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A2Subscriber].[SubscriberID]", "SubscriberID", DataType.Long, Nothing, False, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberName", "[A2Subscriber].[SubscriberName]", "SubscriberName", DataType.String, Nothing, False, 0, True, 200, "Subscriber Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "(([CommercialAuditor]  [A0CommercialAuditor]" + _
           "    left join [Auditor]  [A1Auditor] on [A0CommercialAuditor].[AuditorID] = [A1Auditor].[AuditorID])" + _
           "    left join [Subscriber]  [A2Subscriber] on [A0CommercialAuditor].[SubscriberID] = [A2Subscriber].[SubscriberID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
