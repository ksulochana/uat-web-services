Imports sembleWare.Runtime
Imports System
Public Class EnquiryMatchingEngineHomeAffairs 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly EnquiryMatchingEngineHomeAffairsID As New IdentityElement("EnquiryMatchingEngineHomeAffairsID", Me, True, Nothing, Nothing)
  Public ReadOnly IDNo As New StringElement("IDNo", Me, False, True, True, True, False, 13, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Surname As New StringElement("Surname", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FirstName As New StringElement("FirstName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SecondName As New StringElement("SecondName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly BirthDate As New DateTimeElement("BirthDate", Me, False, True, True, True, False, "Date of Birth", Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FirstInitial As New StringElement("FirstInitial", Me, False, True, True, True, False, 1, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly HomeAffairsID As New IntegerElement("HomeAffairsID", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly EnquiryMatchingEngine As Relationship = New EnquiryMatchingEngineRelationship("EnquiryMatchingEngine", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly EnquiryMatchingEngineHomeAffairsResult_OwnMany As Relationship = New EnquiryMatchingEngineHomeAffairsResultRelationship("EnquiryMatchingEngineHomeAffairsResult", Nothing, "EnquiryMatchingEngineHomeAffairs", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _EnquiryMatchingEngineID As New IntegerElement("EnquiryMatchingEngineID", Me, True)
  Public ReadOnly _EnquiryMatchingEngine As EnquiryMatchingEngineRelationship = EnquiryMatchingEngine
  Public ReadOnly _EnquiryMatchingEngineHomeAffairsResult_OwnMany As EnquiryMatchingEngineHomeAffairsResultRelationship = EnquiryMatchingEngineHomeAffairsResult_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("EnquiryMatchingEngineHomeAffairs", ApplicationSettings)
    _EnquiryMatchingEngine._EnquiryMatchingEngineID.LocalElement = _EnquiryMatchingEngineID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class EnquiryMatchingEngineHomeAffairsRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _EnquiryMatchingEngineHomeAffairsID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _EnquiryMatchingEngineID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New EnquiryMatchingEngineHomeAffairs(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _EnquiryMatchingEngineHomeAffairsID.ForeignElement = CType(Instance, EnquiryMatchingEngineHomeAffairs).EnquiryMatchingEngineHomeAffairsID
    _EnquiryMatchingEngineID.ForeignElement = CType(Instance, EnquiryMatchingEngineHomeAffairs)._EnquiryMatchingEngineID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
