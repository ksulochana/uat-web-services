Imports sembleWare.Runtime
Imports System

Public Class SystemUser 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly SystemUserID As New IdentityElement("SystemUserID", Me, True, Nothing, Nothing)
  Public ReadOnly SystemUserTypeInd As New IndicatorElement("SystemUserTypeInd", Me, False, False, False, True, True, True, "System User Type", Nothing, SystemUser.SystemUserTypeIndOptions, "I")
  Public ReadOnly FirstName As New StringElement("FirstName", Me, False, True, True, True, True, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Surname As New StringElement("Surname", Me, False, True, True, True, True, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly IDNo As New StringElement("IDNo", Me, False, True, True, True, False, 13, "ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly JobPosition As New StringElement("JobPosition", Me, False, True, True, True, False, 50, "Job Position", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CellularCode As New StringElement("CellularCode", Me, False, True, True, True, False, 5, "Cellular Code", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CellularNo As New StringElement("CellularNo", Me, False, True, True, True, False, 50, "Cellular No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly EmailAddress As New StringElement("EmailAddress", Me, False, True, True, True, True, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ActiveYN As New BooleanElement("ActiveYN", Me, False, True, True, True, True, "Active?", Nothing, True, "Yes;No")
  Public ReadOnly Username As New StringElement("Username", Me, False, False, True, True, True, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Password As New StringElement("Password", Me, False, False, True, True, True, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FullName As New StringElement("FullName", Me, False, True, True, False, False, 250, "Full Name", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SystemUserLabelDescription As New StringElement("SystemUserLabelDescription", Me, False, True, True, False, False, 250, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResetPasswordYN As New BooleanElement("ResetPasswordYN", Me, False, True, True, False, True, "Reset Password?", Nothing, False)
  Public ReadOnly NewPassword As New StringElement("NewPassword", Me, False, True, True, False, False, 30, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ConfirmPassword As New StringElement("ConfirmPassword", Me, False, True, True, False, False, 30, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Title As Relationship = New TitleRelationship("Title", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly SystemUserRole_OwnMany As Relationship = New SystemUserRoleRelationship("SystemUserRole", Nothing, "SystemUser", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _TitleCode As New StringElement("TitleCode", Me, False)
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, False)
  Public ReadOnly _Title As TitleRelationship = Title
  Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  Public ReadOnly _SystemUserRole_OwnMany As SystemUserRoleRelationship = SystemUserRole_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moSystemUserTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property SystemUserTypeIndOptions() As IndicatorOptions
    Get
      If moSystemUserTypeIndOptions Is Nothing Then
        moSystemUserTypeIndOptions = New IndicatorOptions
        moSystemUserTypeIndOptions.Add("I", "Internal User")
        moSystemUserTypeIndOptions.Add("S", "Subscriber User")
      End If
      Return moSystemUserTypeIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SystemUser", ApplicationSettings)
    _Title._TitleCode.LocalElement = _TitleCode
    _Subscriber._SubscriberID.LocalElement = _SubscriberID
    AddHandler SystemUserTypeInd.Changed, AddressOf SystemUserTypeInd_Changed
    AddHandler ResetPasswordYN.Changed, AddressOf ResetPasswordYN_Changed
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub ResetPasswordYN_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BRResetPasswordYN(Element, True, True)
  End Sub

  Private Sub SystemUserTypeInd_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BRSystemUserTypeInd(Element, True, True)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreate()
  End Sub

  Public Overrides Sub Load()
    MyBase.Load()
    BRLoad()
  End Sub

  Public Overrides Sub Save()
    With ApplicationSettings
      Try
        Dim bIsNewYN As Boolean = Me.IsNew

        .BeginTransaction()
        BRBeforeSave(bIsNewYN)
        MyBase.Save()
        BRAfterSave(bIsNewYN)
        .CommitTransaction()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
    End With
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreate()
    Me.Username.Enabled = True
    Me.ResetPasswordYN.Value = True
    BRResetPasswordYN(Me.ResetPasswordYN, True, False)
    BRSystemUserTypeInd(Me.SystemUserTypeInd, True, False)
    Me.ResetPasswordYN.Enabled = False
    BRSystemUserLabelDescription()
  End Sub

  Private Sub BRLoad()
    BRResetPasswordYN(Me.ResetPasswordYN, True, False)
    BRSystemUserTypeInd(Me.SystemUserTypeInd, True, False)
    BRSystemUserLabelDescription()
  End Sub

  Private Sub BRBeforeSave(ByVal IsNewYN As Boolean)
    If Me.ResetPasswordYN.Value Then
      Me.Password.Value = Utility.ValidatePassword(Me.NewPassword.Value, Me.ConfirmPassword.Value)
    End If
  End Sub

  Private Sub BRAfterSave(ByVal IsNewYN As Boolean)
    If IsNewYN Then
      BRCreateSystemUserProfile()
      BRCreateSystemUserRoles()
      Select Case SystemUserTypeInd.Value
        Case Constants.SystemUser.Types.SubscriberUser
          InsertSystemUserProfileProducts(Me._SubscriberID.Value, Me.SystemUserID.Value, ApplicationSettings)
      End Select
    End If
    Me.Username.Enabled = False
    Me.ResetPasswordYN.Enabled = True
    Me.ResetPasswordYN.Value = False
    BRResetPasswordYN(Me.ResetPasswordYN, True, True)
    BRSystemUserLabelDescription()
  End Sub

  Private Sub BRSystemUserTypeInd(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If SetEnableDisableYN Then
      Me.Subscriber.Mandatory = False
      Select Case Element.ValueAsObject
        Case Constants.SystemUser.Types.SubscriberUser
          Me.Subscriber.Mandatory = True
      End Select
    End If

    If PerformRulesYN Then
      Select Case Element.ValueAsObject
        Case Constants.SystemUser.Types.InternalUser
          Me.Subscriber.Instance = Nothing
      End Select
    End If
  End Sub

  Private Sub BRResetPasswordYN(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If SetEnableDisableYN Then
      Me.NewPassword.Enabled = Element.ValueAsObject
      Me.NewPassword.Mandatory = Element.ValueAsObject
      Me.ConfirmPassword.Enabled = Element.ValueAsObject
      Me.ConfirmPassword.Mandatory = Element.ValueAsObject
    End If

    If PerformRulesYN Then
      If Not Element.ValueAsObject Then
        Me.NewPassword.SetToNullValue()
        Me.ConfirmPassword.SetToNullValue()
      End If
    End If
  End Sub

  Private Sub BRSystemUserLabelDescription()
    If Me.IsNew Then
      Me.SystemUserLabelDescription.Value = "System User: (New)"
    Else
      Dim sDesc As String = Trim(Me._TitleCode.Value & " " & Me.FirstName.Value & " " & Me.Surname.Value)
      Me.SystemUserLabelDescription.Value = "System User: " & Me.SystemUserID.Value & " (" & sDesc & ")"
    End If
  End Sub

  Private Sub BRCreateSystemUserProfile()
    Dim oSystemUserProfile As SystemUserProfile = New SystemUserProfileRelationship(ApplicationSettings).NewInstance

    oSystemUserProfile.SystemUser.Instance = Me
    oSystemUserProfile.Save()
  End Sub

  Private Sub BRCreateSystemUserRoles()
    Select Case Me.SystemUserTypeInd.Value
      Case Constants.SystemUser.Types.SubscriberUser
        Dim oSystemUserRole As SystemUserRole = Me.SystemUserRole_OwnMany.NewInstance()

        oSystemUserRole._RoleCode.Value = Constants.Role.Records.SubscriberUsers
        oSystemUserRole.Save()
    End Select
  End Sub
#End Region

#Region " Shared Procedures"
  Public Shared Function GetSystemUser(ByVal SystemUserID As Long, ByVal ApplicationSettings As ApplicationSettings) As SystemUser
    Dim oSystemUser As SystemUser = New SystemUser(ApplicationSettings)
    oSystemUser.SystemUserID.Load(SystemUserID)
    oSystemUser.Load()
    Return oSystemUser
  End Function

  Public Shared Function GetSystemUser(ByVal Username As String, ByVal ApplicationSettings As ApplicationSettings) As SystemUser
    Dim oXDSAdministrationRoot As XDSAdministrationRoot = New XDSAdministrationRootRelationship(ApplicationSettings).NewInstance()
    Dim oList As List = oXDSAdministrationRoot._SystemUser_OwnMany.GridList
    Dim oDataRow As DataRow

    oList.AndFilters.Add(New ListFilter("Username", "Username", Username, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
    For Each oDataRow In oList.DataTable.Rows
      Dim oSystemUser As SystemUser = New SystemUser(ApplicationSettings)

      oSystemUser.SystemUserID.Load(oDataRow("SystemUserID"))
      oSystemUser.Load()

      Return oSystemUser
    Next

    Return Nothing
  End Function

  Public Shared Function GetLoggedInSystemUser(ByVal ApplicationSettings As ApplicationSettings) As SystemUser
    If Not GlobalSettings.SystemUser.LoggedInSystemUserID = 0 Then
      Return SystemUser.GetSystemUser(GlobalSettings.SystemUser.LoggedInSystemUserID, ApplicationSettings)
    End If
    Return Nothing
  End Function

  Public Shared Function InsertSystemUserProfileProducts(ByVal SubscriberID As Long, ByVal SystemUserID As Long, ByVal ApplicationSettings As ApplicationSettings) As Boolean
    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand

    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spSystemUser_I_SystemUserProfileProduct"

    oCommand.Parameters.Add("@SubscriberID", SqlDbType.Int)
    oCommand.Parameters.Add("@SystemUserID", SqlDbType.Int)

    oCommand.Parameters("@SubscriberID").Value = SubscriberID
    oCommand.Parameters("@SystemUserID").Value = SystemUserID

    ApplicationSettings.ActionQuery(oCommand)
  End Function

  Public Shared Function ValidateLogin(ByVal SystemUserType As String, ByVal Username As String, ByVal Password As String, ByVal ApplicationSettings As ApplicationSettings) As Boolean
    Dim oEncryption As sembleWare.Security.Encryption = New sembleWare.Security.Encryption(Constants.PasswordHashString)
    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand

    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spSystemUser_S_Login"

    oCommand.Parameters.Add("@Username", SqlDbType.VarChar, 50)
    oCommand.Parameters.Add("@Password", SqlDbType.VarChar, 100)
    oCommand.Parameters.Add("@SystemUserTypeInd", SqlDbType.VarChar, 1)
    oCommand.Parameters.Add("@ConfirmedSystemUserID", SqlDbType.Int)
    oCommand.Parameters.Add("@ConfirmedUsername", SqlDbType.VarChar, 50)

    oCommand.Parameters("@ConfirmedSystemUserID").Direction = ParameterDirection.Output
    oCommand.Parameters("@ConfirmedUsername").Direction = ParameterDirection.Output

    oCommand.Parameters("@Username").Value = Username
    oCommand.Parameters("@Password").Value = oEncryption.Encrypt(Password)
    oCommand.Parameters("@SystemUserTypeInd").Value = SystemUserType

    ApplicationSettings.ActionQuery(oCommand)

    If oCommand.Parameters("@ConfirmedSystemUserID").Value Is System.DBNull.Value Then
      Dim oMessage As Text.StringBuilder = New Text.StringBuilder

      Select Case SystemUserType
        Case Constants.SystemUser.Types.InternalUser
          oMessage.Append("Unable to login user : '" & Username & "'. ")
          oMessage.Append("The User may not have access to the system or is inactive. Please contact your system administrator.")
        Case Constants.SystemUser.Types.SubscriberUser
          oMessage.Append("Unable to login user : '" & Username & "'. ")
          oMessage.Append("The User may not have access to the system or is inactive. Please contact your system administrator.")
      End Select
      Throw New BusinessRuleException(oMessage.ToString)
    End If
    GlobalSettings.SystemUser.SetLoggedInSystemUserID(oCommand.Parameters("@ConfirmedSystemUserID").Value)
    GlobalSettings.SystemUser.SetLoggedInSystemUsername(oCommand.Parameters("@ConfirmedUsername").Value)
  End Function

  Public Shared Function ForgotPassword(ByVal SystemUserType As String, ByVal Username As String, ByVal EmailAddress As String, ByVal ApplicationSettings As ApplicationSettings) As Boolean
    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand

    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spSystemUser_S_ForgotPassword"

    oCommand.Parameters.Add("@Username", SqlDbType.VarChar, 50)
    oCommand.Parameters.Add("@EmailAddress", SqlDbType.VarChar, 100)
    oCommand.Parameters.Add("@SystemUserTypeInd", SqlDbType.VarChar, 1)
    oCommand.Parameters.Add("@ConfirmedSystemUserID", SqlDbType.Int)

    oCommand.Parameters("@ConfirmedSystemUserID").Direction = ParameterDirection.Output

    oCommand.Parameters("@Username").Value = Username
    oCommand.Parameters("@EmailAddress").Value = EmailAddress
    oCommand.Parameters("@SystemUserTypeInd").Value = SystemUserType

    ApplicationSettings.ActionQuery(oCommand)

    If oCommand.Parameters("@ConfirmedSystemUserID").Value Is System.DBNull.Value Then
      Dim oMessage As Text.StringBuilder = New Text.StringBuilder

      Select Case SystemUserType
        Case Constants.SystemUser.Types.InternalUser
          oMessage.Append("Unable to find user : '" & Username & "'. ")
          oMessage.Append("The User may not exist or the email address does not match the username.")
        Case Constants.SystemUser.Types.SubscriberUser
          oMessage.Append("Unable to find user : '" & Username & "'. ")
          oMessage.Append("The User may not exist or the email address does not match the username.")
      End Select
      Throw New BusinessRuleException(oMessage.ToString)
    End If

    Dim oSystemUser As SystemUser = New SystemUserRelationship(ApplicationSettings).NewInstance
    oSystemUser.SystemUserID.Load(oCommand.Parameters("@ConfirmedSystemUserID").Value)
    oSystemUser.Load()

    ' Send Notification
    NotificationTemplate.CreateForgotPasswordNotification(oSystemUser, ApplicationSettings)
  End Function
#End Region
End Class 'sembleWare: Part

Public Class SystemUserRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SystemUserID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SystemUser(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SystemUserID.ForeignElement = CType(Instance, SystemUser).SystemUserID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function SystemUserDropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SystemUser")
    oList.Columns.Add(New ListColumn("SystemUserID", "[A0SystemUser].[SystemUserID]", "SystemUserID", DataType.Long, Nothing, True, 0, True, 100, "Internal User ID"))
    oList.Columns.Add(New ListColumn("FirstName", "[A0SystemUser].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("Surname", "[A0SystemUser].[Surname]", "Surname", DataType.String, Nothing, False, 0, True, 100, "Surname"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SystemUser]  [A0SystemUser]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function SystemUserRoleList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SystemUser")
    oList.Columns.Add(New ListColumn("SystemUserID", "[A0SystemUser].[SystemUserID]", "SystemUserID", DataType.Long, Nothing, True, 0, False, 100, "Internal User ID"))
    oList.Columns.Add(New ListColumn("FirstName", "[A0SystemUser].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("Surname", "[A0SystemUser].[Surname]", "Surname", DataType.String, Nothing, False, 0, True, 100, "Surname"))
    oList.Columns.Add(New ListColumn("Email", "[A0SystemUser].[Email]", "Email", DataType.String, Nothing, False, 0, True, 100, "Email"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SystemUser]  [A0SystemUser]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function SystemUserGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SystemUser")
    oList.Columns.Add(New ListColumn("SystemUserID", "[A0SystemUser].[SystemUserID]", "SystemUserID", DataType.Long, Nothing, True, 0, False, 100, "SystemUser ID"))
    oList.Columns.Add(New ListColumn("FirstName", "[A0SystemUser].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("Surname", "[A0SystemUser].[Surname]", "Surname", DataType.String, Nothing, False, 0, True, 100, "Surname"))
    oList.Columns.Add(New ListColumn("Email", "[A0SystemUser].[Email]", "Email", DataType.String, Nothing, False, 0, True, 100, "Email"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SystemUser]  [A0SystemUser]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function DropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SystemUser")
    oList.Columns.Add(New ListColumn("SystemUserID", "[A0SystemUser].[SystemUserID]", "SystemUserID", DataType.Long, Nothing, True, 0, False, 100, "System User ID"))
    oList.Columns.Add(New ListColumn("SystemUserTypeInd", "[A0SystemUser].[SystemUserTypeInd]", "SystemUserTypeInd", DataType.String, Nothing, False, 0, False, 100, "System User Type"))
    oList.Columns.Add(New ListColumn("FullName", "isnull([A0SystemUser].FirstName, '') + ' ' + isnull([A0SystemUser].[Surname], '')", "FullName", DataType.String, Nothing, False, 1, True, 200, "Full Name"))
    oList.Columns.Add(New ListColumn("EmailAddress", "[A0SystemUser].[EmailAddress]", "EmailAddress", DataType.String, Nothing, False, 0, True, 100, "Email Address"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A1Subscriber].[SubscriberID]", "SubscriberID", DataType.Long, Nothing, False, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberName", "[A1Subscriber].[SubscriberName]", "SubscriberName", DataType.String, Nothing, False, 0, True, 200, "Subscriber Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "([SystemUser]  [A0SystemUser]" + _
           "    left join [Subscriber]  [A1Subscriber] on [A0SystemUser].[SubscriberID] = [A1Subscriber].[SubscriberID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SystemUser")
    oList.Columns.Add(New ListColumn("SystemUserID", "[A0SystemUser].[SystemUserID]", "SystemUserID", DataType.Long, Nothing, True, 0, False, 100, "Person ID"))
    oList.Columns.Add(New ListColumn("SystemUserTypeInd", "[A0SystemUser].[SystemUserTypeInd]", "SystemUserTypeInd", DataType.String, Nothing, False, 0, True, 100, "System User Type"))
    oList.Columns.Add(New ListColumn("FirstName", "[A0SystemUser].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("Surname", "[A0SystemUser].[Surname]", "Surname", DataType.String, Nothing, False, 0, True, 100, "Surname"))
    oList.Columns.Add(New ListColumn("Username", "[A0SystemUser].[Username]", "Username", DataType.String, Nothing, False, 0, True, 100, "Username"))
    oList.Columns.Add(New ListColumn("EmailAddress", "[A0SystemUser].[EmailAddress]", "EmailAddress", DataType.String, Nothing, False, 0, True, 100, "Email Address"))
    oList.Columns.Add(New ListColumn("ActiveYN", "[A0SystemUser].[ActiveYN]", "ActiveYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Active?"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A1Subscriber].[SubscriberID]", "SubscriberID", DataType.Long, Nothing, False, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberName", "[A1Subscriber].[SubscriberName]", "SubscriberName", DataType.String, Nothing, False, 0, True, 200, "Subscriber Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "([SystemUser]  [A0SystemUser]" + _
           "    left join [Subscriber]  [A1Subscriber] on [A0SystemUser].[SubscriberID] = [A1Subscriber].[SubscriberID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SystemUser")
    oList.Columns.Add(New ListColumn("SystemUserID", "[A0SystemUser].[SystemUserID]", "SystemUserID", DataType.Long, Nothing, True, 0, False, 100, "Person ID"))
    oList.Columns.Add(New ListColumn("FullName", "isnull([A0SystemUser].FirstName, '') + ' ' + isnull([A0SystemUser].[Surname], '')", "FullName", DataType.String, Nothing, False, 0, True, 200, "Full Name"))
    oList.Columns.Add(New ListColumn("EmailAddress", "[A0SystemUser].[EmailAddress]", "EmailAddress", DataType.String, Nothing, False, 0, True, 100, "Email Address"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SystemUser]  [A0SystemUser]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship