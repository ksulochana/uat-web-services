Imports sembleWare.Runtime
Imports System
Public Class FileFormatMetaDatabase 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly FileFormatMetaDatabaseLabelDescription As New StringElement("FileFormatMetaDatabaseLabelDescription", Me, False, True, True, False, False, 500, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FileFormat As Relationship = New FileFormatRelationship("FileFormat", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly MetaDatabase As Relationship = New MetaDatabaseRelationship("MetaDatabase", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly FileFormatMetaGroup_OwnMany As Relationship = New FileFormatMetaGroupRelationship("FileFormatMetaGroup", Nothing, "FileFormatMetaDatabase", Me)
  Public ReadOnly FileFormatMetaDataProcedure_OwnMany As Relationship = New FileFormatMetaDataProcedureRelationship("FileFormatMetaDataProcedure", Nothing, "FileFormatMetaDatabase", Me)
  Public ReadOnly FileFormatMetaLoaderProcedure_OwnMany As Relationship = New FileFormatMetaLoaderProcedureRelationship("FileFormatMetaLoaderProcedure", Nothing, "FileFormatMetaDatabase", Me)
  Public ReadOnly FileFormatMatchingEngineField_OwnMany As Relationship = New FileFormatMatchingEngineFieldRelationship("FileFormatMatchingEngineField", Nothing, "FileFormatMetaDatabase", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _FileFormatCode As New StringElement("FileFormatCode", Me, True)
  Public ReadOnly _MetaDatabaseCode As New StringElement("MetaDatabaseCode", Me, True)
  Public ReadOnly _FileFormat As FileFormatRelationship = FileFormat
  Public ReadOnly _MetaDatabase As MetaDatabaseRelationship = MetaDatabase
  Public ReadOnly _FileFormatMetaGroup_OwnMany As FileFormatMetaGroupRelationship = FileFormatMetaGroup_OwnMany
  Public ReadOnly _FileFormatMetaDataProcedure_OwnMany As FileFormatMetaDataProcedureRelationship = FileFormatMetaDataProcedure_OwnMany
  Public ReadOnly _FileFormatMetaLoaderProcedure_OwnMany As FileFormatMetaLoaderProcedureRelationship = FileFormatMetaLoaderProcedure_OwnMany
  Public ReadOnly _FileFormatMatchingEngineField_OwnMany As FileFormatMatchingEngineFieldRelationship = FileFormatMatchingEngineField_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("FileFormatMetaDatabase", ApplicationSettings)
    _FileFormat._FileFormatCode.LocalElement = _FileFormatCode
    _MetaDatabase._MetaDatabaseCode.LocalElement = _MetaDatabaseCode
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreate()
  End Sub

  Public Overrides Sub Load()
    MyBase.Load()
    BRLoad()
  End Sub

  Public Overrides Sub Save()
    With ApplicationSettings
      .BeginTransaction()
      Try
        Dim bIsNewYN As Boolean = Me.IsNew
        MyBase.Save()
        BRAfterSave(bIsNewYN)
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
      .CommitTransaction()
    End With
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreate()
    BRFileFormatMetaDatabaseLabelDescription()
  End Sub

  Private Sub BRLoad()
    BRFileFormatMetaDatabaseLabelDescription()
  End Sub

  Public Sub BRAfterSave(ByVal IsNewYN As Boolean)
    If IsNewYN Then
      InsertDefaultLoaderProcedures(Me, ApplicationSettings)
    End If
  End Sub

  Private Sub BRFileFormatMetaDatabaseLabelDescription()
    Dim oFileFormat As FileFormat = Me.FileFormat.Instance
    Dim oMetaDatabase As MetaDatabase = Me.MetaDatabase.Instance
    If Me.IsNew Then
      Me.FileFormatMetaDatabaseLabelDescription.Value = oFileFormat.FileFormatLabelDescription.Value & " - Database: (New)"
    Else
      Dim sDesc As String = Trim(oMetaDatabase.MetaDatabaseDesc.Value)
      Me.FileFormatMetaDatabaseLabelDescription.Value = oFileFormat.FileFormatLabelDescription.Value & " - Database: " & Me._MetaDatabaseCode.Value & " (" & sDesc & ")"
    End If
  End Sub
#End Region

#Region " Shared Procedures"
  Public Shared Sub InsertDefaultLoaderProcedures(ByVal FileFormatMetaDatabase As FileFormatMetaDatabase, ByVal ApplicationSettings As ApplicationSettings)
    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand

    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spFileFormatMetaLoaderProcedure_I_LoaderProcessType"

    oCommand.Parameters.Add("@MetaDatabaseCode", SqlDbType.VarChar, 10)
    oCommand.Parameters.Add("@FileFormatCode", SqlDbType.VarChar, 10)
    oCommand.Parameters.Add("@Username", SqlDbType.VarChar, 50)

    oCommand.Parameters("@MetaDatabaseCode").Value = FileFormatMetaDatabase._MetaDatabaseCode.Value
    oCommand.Parameters("@FileFormatCode").Value = FileFormatMetaDatabase._FileFormatCode.Value
    oCommand.Parameters("@Username").Value = SystemUser.GetLoggedInSystemUser(ApplicationSettings).Username.Value

    ApplicationSettings.ActionQuery(oCommand)
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class FileFormatMetaDatabaseRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _FileFormatCode As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _MetaDatabaseCode As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New FileFormatMetaDatabase(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _FileFormatCode.ForeignElement = CType(Instance, FileFormatMetaDatabase)._FileFormatCode
    _MetaDatabaseCode.ForeignElement = CType(Instance, FileFormatMetaDatabase)._MetaDatabaseCode
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0FileFormatMetaDatabase")
    oList.Columns.Add(New ListColumn("FileFormatCode", "[A0FileFormatMetaDatabase].[FileFormatCode]", "FileFormatCode", DataType.String, Nothing, True, 0, False, 100, "File Format Code"))
    oList.Columns.Add(New ListColumn("MetaDatabaseCode", "[A0FileFormatMetaDatabase].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, True, 0, False, 100, "Meta Database Code"))
    oList.Columns.Add(New ListColumn("MetaDatabaseCode1", "[A1MetaDatabase].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, False, 1, True, 100, "Database Code"))
    oList.Columns.Add(New ListColumn("MetaDatabaseDesc", "[A1MetaDatabase].[MetaDatabaseDesc]", "MetaDatabaseDesc", DataType.String, Nothing, False, 0, True, 200, "Database Description"))
    oList.Columns.Add(New ListColumn("MetaDatabaseTypeInd", "[A1MetaDatabase].[MetaDatabaseTypeInd]", "MetaDatabaseTypeInd", DataType.String, Nothing, False, 0, True, 100, "Database Type"))
    oList.SelectStatement = "select"
    oList.FromClause = "([FileFormatMetaDatabase]  [A0FileFormatMetaDatabase]" + _
           "    join [MetaDatabase]  [A1MetaDatabase] on [A0FileFormatMetaDatabase].[MetaDatabaseCode] = [A1MetaDatabase].[MetaDatabaseCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
