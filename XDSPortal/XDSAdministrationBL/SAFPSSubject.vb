Imports sembleWare.Runtime
Imports System
Public Class SAFPSSubject 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly SAFPSSubjectID As New IdentityElement("SAFPSSubjectID", Me, True, "SAFPS Subject ID", Nothing)
  Public ReadOnly IDNo As New StringElement("IDNo", Me, False, True, True, True, False, 64, "ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PassportNo As New StringElement("PassportNo", Me, False, True, True, True, False, 64, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FirstName As New StringElement("FirstName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SecondName As New StringElement("SecondName", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly Surname As New StringElement("Surname", Me, False, True, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly BirthDate As New DateTimeElement("BirthDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly GenderInd As New IndicatorElement("GenderInd", Me, False, False, True, True, True, False, "Gender", Nothing, SAFPSSubject.GenderIndOptions, Nothing)
  Public ReadOnly EmailAddress As New StringElement("EmailAddress", Me, False, True, True, True, False, 255, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly IsVictimYN As New BooleanElement("IsVictimYN", Me, False, True, True, True, True, "Is Victim?", Nothing, Nothing)
  Public ReadOnly ExternalSourceID As New IntegerElement("ExternalSourceID", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly IsPossibleNameConflictYN As New BooleanElement("IsPossibleNameConflictYN", Me, False, False, True, True, True, "Is Possible Name Conflict?", Nothing, False, "Yes;No")
  Public ReadOnly IsPossibleDuplicateRecordYN As New BooleanElement("IsPossibleDuplicateRecordYN", Me, False, False, True, True, True, "Is Possible Duplicate Record?", Nothing, False, "Yes;No")
  Public ReadOnly RecordStatusInd As New IndicatorElement("RecordStatusInd", Me, False, False, False, True, True, True, "Record Status", Nothing, SAFPSSubject.RecordStatusIndOptions, "A")
  Public ReadOnly DeletedReason As New StringElement("DeletedReason", Me, False, True, True, True, False, 500, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LastUpdatedDate As New DateTimeElement("LastUpdatedDate", Me, False, False, True, True, True, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly Title As Relationship = New TitleRelationship("Title", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Loader As Relationship = New LoaderRelationship("Loader", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly SAFPSSubjectAddress_OwnMany As Relationship = New SAFPSSubjectAddressRelationship("SAFPSSubjectAddress", Nothing, "SAFPSSubject", Me)
  Public ReadOnly SAFPSSubjectBankAccount_OwnMany As Relationship = New SAFPSSubjectBankAccountRelationship("SAFPSSubjectBankAccount", Nothing, "SAFPSSubject", Me)
  Public ReadOnly SAFPSSubjectAlias_OwnMany As Relationship = New SAFPSSubjectAliasRelationship("SAFPSSubjectAlias", Nothing, "SAFPSSubject", Me)
  Public ReadOnly SAFPSSubjectEmployment_OwnMany As Relationship = New SAFPSSubjectEmploymentRelationship("SAFPSSubjectEmployment", Nothing, "SAFPSSubject", Me)
  Public ReadOnly SAFPSSubjectCase_OwnMany As Relationship = New SAFPSSubjectCaseRelationship("SAFPSSubjectCase", Nothing, "SAFPSSubject", Me)
  Public ReadOnly SAFPSSubjectIncident_OwnMany As Relationship = New SAFPSSubjectIncidentRelationship("SAFPSSubjectIncident", Nothing, "SAFPSSubject", Me)
  Public ReadOnly SAFPSSubjectOtherID_OwnMany As Relationship = New SAFPSSubjectOtherIDRelationship("SAFPSSubjectOtherID", Nothing, "SAFPSSubject", Me)
  Public ReadOnly SAFPSSubjectOtherNumber_OwnMany As Relationship = New SAFPSSubjectOtherNumberRelationship("SAFPSSubjectOtherNumber", Nothing, "SAFPSSubject", Me)
  Public ReadOnly SAFPSSubjectConflict_OwnMany As Relationship = New SAFPSSubjectConflictRelationship("SAFPSSubjectConflict", Nothing, "SAFPSSubject", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _TitleCode As New StringElement("TitleCode", Me, False)
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, False)
  Public ReadOnly _LoaderID As New IntegerElement("LoaderID", Me, False)
  Public ReadOnly _Title As TitleRelationship = Title
  Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
  Public ReadOnly _Loader As LoaderRelationship = Loader
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  Public ReadOnly _SAFPSSubjectAddress_OwnMany As SAFPSSubjectAddressRelationship = SAFPSSubjectAddress_OwnMany
  Public ReadOnly _SAFPSSubjectBankAccount_OwnMany As SAFPSSubjectBankAccountRelationship = SAFPSSubjectBankAccount_OwnMany
  Public ReadOnly _SAFPSSubjectAlias_OwnMany As SAFPSSubjectAliasRelationship = SAFPSSubjectAlias_OwnMany
  Public ReadOnly _SAFPSSubjectEmployment_OwnMany As SAFPSSubjectEmploymentRelationship = SAFPSSubjectEmployment_OwnMany
  Public ReadOnly _SAFPSSubjectCase_OwnMany As SAFPSSubjectCaseRelationship = SAFPSSubjectCase_OwnMany
  Public ReadOnly _SAFPSSubjectIncident_OwnMany As SAFPSSubjectIncidentRelationship = SAFPSSubjectIncident_OwnMany
  Public ReadOnly _SAFPSSubjectOtherID_OwnMany As SAFPSSubjectOtherIDRelationship = SAFPSSubjectOtherID_OwnMany
  Public ReadOnly _SAFPSSubjectOtherNumber_OwnMany As SAFPSSubjectOtherNumberRelationship = SAFPSSubjectOtherNumber_OwnMany
  Public ReadOnly _SAFPSSubjectConflict_OwnMany As SAFPSSubjectConflictRelationship = SAFPSSubjectConflict_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moGenderIndOptions As IndicatorOptions
  Public Shared ReadOnly Property GenderIndOptions() As IndicatorOptions
    Get
      If moGenderIndOptions Is Nothing Then
        moGenderIndOptions = New IndicatorOptions
        moGenderIndOptions.Add("M", "Male")
        moGenderIndOptions.Add("F", "Female")
      End If
      Return moGenderIndOptions
    End Get
  End Property
  Private Shared moRecordStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property RecordStatusIndOptions() As IndicatorOptions
    Get
      If moRecordStatusIndOptions Is Nothing Then
        moRecordStatusIndOptions = New IndicatorOptions
        moRecordStatusIndOptions.Add("A", "Active")
        moRecordStatusIndOptions.Add("D", "Deleted")
      End If
      Return moRecordStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SAFPSSubject", ApplicationSettings)
    _Title._TitleCode.LocalElement = _TitleCode
    _Subscriber._SubscriberID.LocalElement = _SubscriberID
    _Loader._LoaderID.LocalElement = _LoaderID
    'sembleWare: Constructor End - Do Not Modify
    Me.DeleteType = CustomDBPart.enumDeleteType.ChangeRecordStatus
  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region "base Overrides"
  Public Overrides Sub Save()
    With ApplicationSettings
      Try
        .BeginTransaction()
        Dim bIsNewYN As Boolean = Me.IsNew
        BRBeforeSave(bIsNewYN)
        MyBase.Save()
        .CommitTransaction()
      Catch Exception As Exception
        .RollbackTransaction()
        Throw Exception
      End Try
    End With
  End Sub

#End Region

#Region "Business Rules"
  Private Sub BRBeforeSave(ByVal IsNewYN As Boolean)
    Me.LastUpdatedDate.Value = System.DateTime.Now
  End Sub
#End Region

End Class 'sembleWare: Part

Public Class SAFPSSubjectRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SAFPSSubjectID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SAFPSSubject(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SAFPSSubjectID.ForeignElement = CType(Instance, SAFPSSubject).SAFPSSubjectID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SAFPSSubject")
    oList.Columns.Add(New ListColumn("SAFPSSubjectID", "[A0SAFPSSubject].[SAFPSSubjectID]", "SAFPSSubjectID", DataType.Long, Nothing, True, 1, True, 100, "SAFPS Subject ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A0SAFPSSubject].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("PassportNo", "[A0SAFPSSubject].[PassportNo]", "PassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No"))
    oList.Columns.Add(New ListColumn("Surname", "[A0SAFPSSubject].[Surname]", "Surname", DataType.String, Nothing, False, 2, True, 100, "Surname"))
    oList.Columns.Add(New ListColumn("FirstName", "[A0SAFPSSubject].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("SecondName", "[A0SAFPSSubject].[SecondName]", "SecondName", DataType.String, Nothing, False, 0, True, 100, "Second Name"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A0SAFPSSubject].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Birth Date"))
    oList.Columns.Add(New ListColumn("GenderInd", "[A0SAFPSSubject].[GenderInd]", "GenderInd", DataType.String, Nothing, False, 0, True, 100, "Gender"))
    oList.Columns.Add(New ListColumn("ExternalSourceID", "[A0SAFPSSubject].[ExternalSourceID]", "ExternalSourceID", DataType.Integer, Nothing, False, 0, True, 100, "External Source ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SAFPSSubject]  [A0SAFPSSubject]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    oList.AndFilters.Add(New ListFilter("RecordStatusInd", "[A0SAFPSSubject].[RecordStatusInd]", Constants.RecordStatus.Active, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SAFPSSubject")
    oList.Columns.Add(New ListColumn("SAFPSSubjectID", "[A0SAFPSSubject].[SAFPSSubjectID]", "SAFPSSubjectID", DataType.Long, Nothing, True, 1, True, 100, "SAFPS Subject ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A0SAFPSSubject].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("PassportNo", "[A0SAFPSSubject].[PassportNo]", "PassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No"))
    oList.Columns.Add(New ListColumn("Surname", "[A0SAFPSSubject].[Surname]", "Surname", DataType.String, Nothing, False, 2, True, 100, "Surname"))
    oList.Columns.Add(New ListColumn("FirstName", "[A0SAFPSSubject].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("SecondName", "[A0SAFPSSubject].[SecondName]", "SecondName", DataType.String, Nothing, False, 0, True, 100, "Second Name"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A0SAFPSSubject].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Birth Date"))
    oList.Columns.Add(New ListColumn("GenderInd", "[A0SAFPSSubject].[GenderInd]", "GenderInd", DataType.String, Nothing, False, 0, True, 100, "Gender"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SAFPSSubject]  [A0SAFPSSubject]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ConflictGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SAFPSSubject")
    oList.Columns.Add(New ListColumn("SAFPSSubjectID", "[A0SAFPSSubject].[SAFPSSubjectID]", "SAFPSSubjectID", DataType.Long, Nothing, True, 1, True, 100, "SAFPS Subject ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A0SAFPSSubject].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("PassportNo", "[A0SAFPSSubject].[PassportNo]", "PassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No"))
    oList.Columns.Add(New ListColumn("Surname", "[A0SAFPSSubject].[Surname]", "Surname", DataType.String, Nothing, False, 2, True, 100, "Surname"))
    oList.Columns.Add(New ListColumn("FirstName", "[A0SAFPSSubject].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("SecondName", "[A0SAFPSSubject].[SecondName]", "SecondName", DataType.String, Nothing, False, 0, True, 100, "Second Name"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A0SAFPSSubject].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Birth Date"))
    oList.Columns.Add(New ListColumn("GenderInd", "[A0SAFPSSubject].[GenderInd]", "GenderInd", DataType.String, Nothing, False, 0, True, 100, "Gender"))
    oList.Columns.Add(New ListColumn("IsPossibleNameConflictYN", "[A0SAFPSSubject].[IsPossibleNameConflictYN]", "IsPossibleNameConflictYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Is Possible Name Conflict?"))
    oList.Columns.Add(New ListColumn("IsPossibleDuplicateRecordYN", "[A0SAFPSSubject].[IsPossibleDuplicateRecordYN]", "IsPossibleDuplicateRecordYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Is Possible Duplicate Record?"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SAFPSSubject]  [A0SAFPSSubject]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    oList.OrFilters.Add(New ListFilter("IsPossibleNameConflictYN", "[A0SAFPSSubject].[IsPossibleNameConflictYN]", True, DataType.Boolean, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
    oList.OrFilters.Add(New ListFilter("IsPossibleDuplicateRecordYN", "[A0SAFPSSubject].[IsPossibleDuplicateRecordYN]", True, DataType.Boolean, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
    Return oList
  End Function


  Public Function ConflictForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SAFPSSubject")
    oList.Columns.Add(New ListColumn("SAFPSSubjectID", "[A0SAFPSSubject].[SAFPSSubjectID]", "SAFPSSubjectID", DataType.Long, Nothing, True, 1, True, 100, "SAFPS Subject ID"))
    oList.Columns.Add(New ListColumn("IDNo", "[A0SAFPSSubject].[IDNo]", "IDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("PassportNo", "[A0SAFPSSubject].[PassportNo]", "PassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No"))
    oList.Columns.Add(New ListColumn("Surname", "[A0SAFPSSubject].[Surname]", "Surname", DataType.String, Nothing, False, 2, True, 100, "Surname"))
    oList.Columns.Add(New ListColumn("FirstName", "[A0SAFPSSubject].[FirstName]", "FirstName", DataType.String, Nothing, False, 0, True, 100, "First Name"))
    oList.Columns.Add(New ListColumn("SecondName", "[A0SAFPSSubject].[SecondName]", "SecondName", DataType.String, Nothing, False, 0, True, 100, "Second Name"))
    oList.Columns.Add(New ListColumn("BirthDate", "[A0SAFPSSubject].[BirthDate]", "BirthDate", DataType.DateTime, "d", False, 0, True, 100, "Birth Date"))
    oList.Columns.Add(New ListColumn("GenderInd", "[A0SAFPSSubject].[GenderInd]", "GenderInd", DataType.String, Nothing, False, 0, True, 100, "Gender"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SAFPSSubject]  [A0SAFPSSubject]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
