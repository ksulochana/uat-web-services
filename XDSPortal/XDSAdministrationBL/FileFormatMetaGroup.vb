Imports sembleWare.Runtime
Imports System
Public Class FileFormatMetaGroup 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly FileFormatMetaGroupID As New IdentityElement("FileFormatMetaGroupID", Me, True, Nothing, Nothing)
  Public ReadOnly FileFormatMetaGroupDesc As New StringElement("FileFormatMetaGroupDesc", Me, False, True, True, True, True, 50, "File Format Group Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FileFormatMetaGroupLabelDescription As New StringElement("FileFormatMetaGroupLabelDescription", Me, False, True, True, False, False, 750, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly FileFormatMetaDatabase As Relationship = New FileFormatMetaDatabaseRelationship("FileFormatMetaDatabase", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly MetaGroup As Relationship = New MetaGroupRelationship("MetaGroup", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly MetaDatabase As Relationship = New MetaDatabaseRelationship("MetaDatabase", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly FileFormat As Relationship = New FileFormatRelationship("FileFormat", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly FileFormatMetaGroupField_OwnMany As Relationship = New FileFormatMetaGroupFieldRelationship("FileFormatMetaGroupField", Nothing, "FileFormatMetaGroup", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _FileFormatCode As New StringElement("FileFormatCode", Me, True)
  Public ReadOnly _MetaDatabaseCode As New StringElement("MetaDatabaseCode", Me, True)
  Public ReadOnly _MetaGroupCode As New StringElement("MetaGroupCode", Me, True)
  Public ReadOnly _FileFormatMetaDatabase As FileFormatMetaDatabaseRelationship = FileFormatMetaDatabase
  Public ReadOnly _MetaGroup As MetaGroupRelationship = MetaGroup
  Public ReadOnly _MetaDatabase As MetaDatabaseRelationship = MetaDatabase
  Public ReadOnly _FileFormat As FileFormatRelationship = FileFormat
  Public ReadOnly _FileFormatMetaGroupField_OwnMany As FileFormatMetaGroupFieldRelationship = FileFormatMetaGroupField_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("FileFormatMetaGroup", ApplicationSettings)
    _FileFormatMetaDatabase._FileFormatCode.LocalElement = _FileFormatCode
    _FileFormatMetaDatabase._MetaDatabaseCode.LocalElement = _MetaDatabaseCode
    _MetaGroup._MetaGroupCode.LocalElement = _MetaGroupCode
    _MetaGroup._MetaDatabaseCode.LocalElement = _MetaDatabaseCode
    _MetaDatabase._MetaDatabaseCode.LocalElement = _MetaDatabaseCode
    _FileFormat._FileFormatCode.LocalElement = _FileFormatCode
    AddHandler MetaGroup.Changed, AddressOf MetaGroup_Changed
    'sembleWare: Constructor End - Do Not Modify
    AddHandler MetaGroup.LimitRelatedList, AddressOf MetaGroup_LimitRelatedList
  End Sub

#End Region 'sembleWare: Constructor

#Region " LimitRelatedList Events"
  Private Sub MetaGroup_LimitRelatedList(ByVal List As List)
    List.LimitByRelatedPart(Me.MetaDatabase.Instance, "MetaDatabase", True)
  End Sub
#End Region

#Region " Change Events"
  Private Sub MetaGroup_Changed(ByVal Relationship As Relationship)
    BRMetaGroup(Relationship, True, True)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreate()
  End Sub

  Public Overrides Sub Load()
    MyBase.Load()
    BRFileFormatMapLabelDescription()
  End Sub

  Public Overrides Sub Save()
    With ApplicationSettings
      .BeginTransaction()
      Try
        Dim bIsNewYN As Boolean = Me.IsNew
        MyBase.Save()
        BRAfterSave(bIsNewYN)
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
      .CommitTransaction()
    End With
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreate()
    BRFileFormatMapLabelDescription()
  End Sub

  Private Sub BRLoad()
    BRFileFormatMapLabelDescription()
  End Sub

  Private Sub BRAfterSave(ByVal IsNewYN As Boolean)
    If IsNewYN Then
      InsertDefaultMetaGroupFields(Me, ApplicationSettings)
    End If
    BRFileFormatMapLabelDescription()
  End Sub

  Private Sub BRMetaGroup(ByVal Relationship As Relationship, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If PerformRulesYN Then
      Me.FileFormatMetaGroupDesc.SetToNullValue()
      If Not Relationship.IsEmpty Then
        Dim oMetaGroup As MetaGroup = Relationship.Instance
        Me.FileFormatMetaGroupDesc.Value = oMetaGroup.MetaGroupDesc.Value
      End If
    End If
  End Sub

  Private Sub BRFileFormatMapLabelDescription()
    Dim oFileFormatMetaDatabase As FileFormatMetaDatabase = Me.FileFormatMetaDatabase.Instance
    Dim oMetaGroup As MetaGroup = Me.MetaGroup.Instance

    If Me.IsNew Then
      Me.FileFormatMetaGroupLabelDescription.Value = oFileFormatMetaDatabase.FileFormatMetaDatabaseLabelDescription.Value & " - Group: (New)"
    Else
      Dim sDesc As String = Trim(oMetaGroup.MetaGroupDesc.Value)
      Me.FileFormatMetaGroupLabelDescription.Value = oFileFormatMetaDatabase.FileFormatMetaDatabaseLabelDescription.Value & " - Group: " & Me._MetaGroupCode.Value & " (" & sDesc & ")"
    End If
  End Sub
#End Region

#Region " Shared Procedures"
  Public Shared Sub InsertDefaultMetaGroupFields(ByVal FileFormatMetaGroup As FileFormatMetaGroup, ByVal ApplicationSettings As ApplicationSettings)
    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand

    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spFileFormatMetaGroup_I_MetaGroupFields"

    oCommand.Parameters.Add("@FileFormatCode", SqlDbType.VarChar, 10)
    oCommand.Parameters.Add("@MetaDatabaseCode", SqlDbType.VarChar, 10)
    oCommand.Parameters.Add("@MetaGroupCode", SqlDbType.VarChar, 10)
    oCommand.Parameters.Add("@FileFormatMetaGroupID", SqlDbType.Int)

    oCommand.Parameters("@FileFormatCode").Value = FileFormatMetaGroup._FileFormatCode.Value
    oCommand.Parameters("@MetaDatabaseCode").Value = FileFormatMetaGroup._MetaDatabaseCode.Value
    oCommand.Parameters("@MetaGroupCode").Value = FileFormatMetaGroup._MetaGroupCode.Value
    oCommand.Parameters("@FileFormatMetaGroupID").Value = FileFormatMetaGroup.FileFormatMetaGroupID.Value

    ApplicationSettings.ActionQuery(oCommand)
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class FileFormatMetaGroupRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _FileFormatMetaGroupID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _FileFormatCode As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _MetaDatabaseCode As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _MetaGroupCode As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New FileFormatMetaGroup(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _FileFormatMetaGroupID.ForeignElement = CType(Instance, FileFormatMetaGroup).FileFormatMetaGroupID
    _FileFormatCode.ForeignElement = CType(Instance, FileFormatMetaGroup)._FileFormatCode
    _MetaDatabaseCode.ForeignElement = CType(Instance, FileFormatMetaGroup)._MetaDatabaseCode
    _MetaGroupCode.ForeignElement = CType(Instance, FileFormatMetaGroup)._MetaGroupCode
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0FileFormatMetaGroup")
    oList.Columns.Add(New ListColumn("FileFormatCode", "[A0FileFormatMetaGroup].[FileFormatCode]", "FileFormatCode", DataType.String, Nothing, True, 0, False, 100, "File Format Code"))
    oList.Columns.Add(New ListColumn("MetaDatabaseCode", "[A0FileFormatMetaGroup].[MetaDatabaseCode]", "MetaDatabaseCode", DataType.String, Nothing, True, 0, False, 100, "Meta Database Code"))
    oList.Columns.Add(New ListColumn("MetaGroupCode", "[A0FileFormatMetaGroup].[MetaGroupCode]", "MetaGroupCode", DataType.String, Nothing, True, 0, False, 100, "Meta Group Code"))
    oList.Columns.Add(New ListColumn("FileFormatMetaGroupID", "[A0FileFormatMetaGroup].[FileFormatMetaGroupID]", "FileFormatMetaGroupID", DataType.Long, Nothing, True, 0, False, 100, "File Format Meta Group ID"))
    oList.Columns.Add(New ListColumn("FileFormatMetaGroupDesc", "[A0FileFormatMetaGroup].[FileFormatMetaGroupDesc]", "FileFormatMetaGroupDesc", DataType.String, Nothing, False, 2, True, 200, "File Format Group Description"))
    oList.Columns.Add(New ListColumn("MetaGroupCode1", "[A1MetaGroup].[MetaGroupCode]", "MetaGroupCode", DataType.String, Nothing, False, 0, True, 100, "Group Code"))
    oList.Columns.Add(New ListColumn("MetaGroupDesc", "[A1MetaGroup].[MetaGroupDesc]", "MetaGroupDesc", DataType.String, Nothing, False, 0, True, 200, "Group Description"))
    oList.Columns.Add(New ListColumn("SequenceNum", "[A1MetaGroup].[SequenceNum]", "SequenceNum", DataType.Integer, Nothing, False, 1, False, 100, "Sequence Number"))
    oList.SelectStatement = "select"
    oList.FromClause = "([FileFormatMetaGroup]  [A0FileFormatMetaGroup]" + _
           "    join [MetaGroup]  [A1MetaGroup] on [A0FileFormatMetaGroup].[MetaGroupCode] = [A1MetaGroup].[MetaGroupCode] and [A0FileFormatMetaGroup].[MetaDatabaseCode] = [A1MetaGroup].[MetaDatabaseCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
