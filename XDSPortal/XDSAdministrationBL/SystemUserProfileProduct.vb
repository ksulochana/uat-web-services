Imports sembleWare.Runtime
Imports System
Public Class SystemUserProfileProduct 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly StatusInd As New IndicatorElement("StatusInd", Me, False, False, True, True, True, True, "Status", Nothing, SystemUserProfileProduct.StatusIndOptions, "A")
  Public ReadOnly SystemUserProfile As Relationship = New SystemUserProfileRelationship("SystemUserProfile", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly SubscriberProfileProduct As Relationship = New SubscriberProfileProductRelationship("SubscriberProfileProduct", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly Product As Relationship = New ProductRelationship("Product", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SystemUserID As New IntegerElement("SystemUserID", Me, True)
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _ProductID As New IntegerElement("ProductID", Me, True)
  Public ReadOnly _SystemUserProfile As SystemUserProfileRelationship = SystemUserProfile
  Public ReadOnly _SubscriberProfileProduct As SubscriberProfileProductRelationship = SubscriberProfileProduct
  Public ReadOnly _Product As ProductRelationship = Product
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property StatusIndOptions() As IndicatorOptions
    Get
      If moStatusIndOptions Is Nothing Then
        moStatusIndOptions = New IndicatorOptions
        moStatusIndOptions.Add("A", "Allowed")
        moStatusIndOptions.Add("D", "Denied")
      End If
      Return moStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SystemUserProfileProduct", ApplicationSettings)
    _SystemUserProfile._SystemUserID.LocalElement = _SystemUserID
    _SubscriberProfileProduct._SubscriberID.LocalElement = _SubscriberID
    _SubscriberProfileProduct._ProductID.LocalElement = _ProductID
    _Product._ProductID.LocalElement = _ProductID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class SystemUserProfileProductRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SystemUserID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _ProductID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SystemUserProfileProduct(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SystemUserID.ForeignElement = CType(Instance, SystemUserProfileProduct)._SystemUserID
    _SubscriberID.ForeignElement = CType(Instance, SystemUserProfileProduct)._SubscriberID
    _ProductID.ForeignElement = CType(Instance, SystemUserProfileProduct)._ProductID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SystemUserProfileProduct")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SystemUserProfileProduct].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SystemUserID", "[A0SystemUserProfileProduct].[SystemUserID]", "SystemUserID", DataType.Integer, Nothing, True, 0, False, 100, "System User ID"))
    oList.Columns.Add(New ListColumn("ProductTypeDesc", "[A2ProductType].[ProductTypeDesc]", "ProductTypeDesc", DataType.String, Nothing, False, 1, True, 200, "Product Type Description"))
    oList.Columns.Add(New ListColumn("ProductDesc", "[A1Product].[ProductDesc]", "ProductDesc", DataType.String, Nothing, False, 2, True, 200, "Product Description"))
    oList.Columns.Add(New ListColumn("DefaultPointValue", "[A1Product].[DefaultPointValue]", "DefaultPointValue", DataType.Decimal, Nothing, False, 0, False, 100, "Default Point Value"))
    oList.Columns.Add(New ListColumn("ProductID", "[A0SystemUserProfileProduct].[ProductID]", "ProductID", DataType.Integer, Nothing, True, 0, False, 100, "Product ID"))
    oList.Columns.Add(New ListColumn("StatusInd", "[A0SystemUserProfileProduct].[StatusInd]", "StatusInd", DataType.String, Nothing, False, 0, True, 100, "Status"))
    oList.SelectStatement = "select"
    oList.FromClause = "([SystemUserProfileProduct]  [A0SystemUserProfileProduct]" + _
           "    join ([Product]  [A1Product]" + _
           "    join [ProductType]  [A2ProductType] on [A1Product].[ProductTypeID] = [A2ProductType].[ProductTypeID]) on [A0SystemUserProfileProduct].[ProductID] = [A1Product].[ProductID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function SubscriberGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SystemUserProfileProduct")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SystemUserProfileProduct].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SystemUserID", "[A0SystemUserProfileProduct].[SystemUserID]", "SystemUserID", DataType.Integer, Nothing, True, 0, False, 100, "System User ID"))
    oList.Columns.Add(New ListColumn("ProductTypeDesc", "[A2ProductType].[ProductTypeDesc]", "ProductTypeDesc", DataType.String, Nothing, False, 1, True, 200, "Product Type Description"))
    oList.Columns.Add(New ListColumn("ProductDesc", "[A1Product].[ProductDesc]", "ProductDesc", DataType.String, Nothing, False, 2, True, 200, "Product Description"))
    oList.Columns.Add(New ListColumn("DefaultPointValue", "[A1Product].[DefaultPointValue]", "DefaultPointValue", DataType.Decimal, Nothing, False, 0, True, 100, "Default Point Value"))
    oList.Columns.Add(New ListColumn("ProductDetails", "[A1Product].[ProductDetails]", "ProductDetails", DataType.Text, Nothing, False, 0, True, 200, "Product Details"))
    oList.Columns.Add(New ListColumn("ProductID", "[A0SystemUserProfileProduct].[ProductID]", "ProductID", DataType.Integer, Nothing, True, 0, False, 100, "Product ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "([SystemUserProfileProduct]  [A0SystemUserProfileProduct]" + _
           "    join ([Product]  [A1Product]" + _
           "    join [ProductType]  [A2ProductType] on [A1Product].[ProductTypeID] = [A2ProductType].[ProductTypeID]) on [A0SystemUserProfileProduct].[ProductID] = [A1Product].[ProductID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
