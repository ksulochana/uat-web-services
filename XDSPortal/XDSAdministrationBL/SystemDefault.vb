Imports sembleWare.Runtime
Imports System
Imports System.Web
Public Class SystemDefault 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly SystemDefaultID As New IntegerElement("SystemDefaultID", Me, True, True, True, True, True, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LoaderUploadFolderPath As New StringElement("LoaderUploadFolderPath", Me, False, True, True, True, True, 250, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LoaderStorageFolderPath As New StringElement("LoaderStorageFolderPath", Me, False, True, True, True, True, 250, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LoaderInvalidFolderPath As New StringElement("LoaderInvalidFolderPath", Me, False, True, True, True, True, 250, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CullingRuleProcessedDate As New DateTimeElement("CullingRuleProcessedDate", Me, False, True, True, True, True, "Date of Last Processing", Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly EnableLoaderProcessQuickLoadYN As New BooleanElement("EnableLoaderProcessQuickLoadYN", Me, False, True, True, True, True, "Enable Quick Load?", Nothing, False, "Yes;No")
  Public ReadOnly EnableLoaderProcessDebugLogYN As New BooleanElement("EnableLoaderProcessDebugLogYN", Me, False, True, True, True, True, "Enable Debug Log?", Nothing, False, "Yes;No")
  Public ReadOnly EnableCullingRuleDebugLogYN As New BooleanElement("EnableCullingRuleDebugLogYN", Me, False, True, True, True, True, "Enable Debug Log?", Nothing, False, "Yes;No")
  Public ReadOnly NotificationSMTPServer As New StringElement("NotificationSMTPServer", Me, False, True, True, True, True, 100, "Notification SMTP Server", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly NotificationFirstName As New StringElement("NotificationFirstName", Me, False, True, True, True, True, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly NotificationSurname As New StringElement("NotificationSurname", Me, False, True, True, True, True, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly NotificationEmailAddress As New StringElement("NotificationEmailAddress", Me, False, True, True, True, True, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly NotificationTitle As Relationship = New TitleRelationship("NotificationTitle", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _NotificationTitleCode As New StringElement("NotificationTitleCode", Me, False)
  Public ReadOnly _NotificationTitle As TitleRelationship = NotificationTitle
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SystemDefault", ApplicationSettings)
    _NotificationTitle._TitleCode.LocalElement = _NotificationTitleCode
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    Me.SystemDefaultID.Value = 1
  End Sub

  Public Overrides Sub Load()
    Me.SystemDefaultID.Load(1)
    Try
      MyBase.Load()
    Catch oException As RecordNotFoundException
      Throw New BusinessRuleException("System Defaults has not been setup.  Please contact your system administrator")
    End Try
  End Sub

  Public Overrides Sub Save()
    MyBase.Save()
    SystemDefault.Current(ApplicationSettings) = Me
  End Sub
#End Region

#Region " Shared Procedures"
  Private Const msCacheKey As String = "XDSAdministrationSystemDefault"

  Public Shared Property Current(ByVal ApplicationSettings As ApplicationSettings) As SystemDefault
    Get
      If Not HttpContext.Current Is Nothing Then
        If Not HttpContext.Current.Cache.Item(msCacheKey) Is Nothing Then
          Return CType(HttpContext.Current.Cache.Item(msCacheKey), SystemDefault)
        End If
      End If
      Dim oSystemDefault As SystemDefault = New SystemDefault(ApplicationSettings)
      oSystemDefault.Load()
      If Not HttpContext.Current Is Nothing Then
        HttpContext.Current.Cache.Insert(msCacheKey, oSystemDefault, Nothing, DateTime.Now.AddDays(1), TimeSpan.Zero)
      End If
      Return oSystemDefault
    End Get
    Set(ByVal Value As SystemDefault)
      HttpContext.Current.Cache.Remove(msCacheKey)
      HttpContext.Current.Cache.Insert(msCacheKey, Value, Nothing, DateTime.Now.AddDays(1), TimeSpan.Zero)
    End Set
  End Property

  Public Shared Function GetLoaderUploadFolderPath(ByVal ApplicationSettings As ApplicationSettings) As String
    Dim sFolderPath As String = SystemDefault.Current(ApplicationSettings).LoaderUploadFolderPath.Value

    SystemDefault.CreateFolderPath(sFolderPath)

    Return sFolderPath
  End Function

  Public Shared Function GetLoaderStorageFolderPath(ByVal ApplicationSettings As ApplicationSettings) As String
    Dim sFolderPath As String = SystemDefault.Current(ApplicationSettings).LoaderStorageFolderPath.Value

    SystemDefault.CreateFolderPath(sFolderPath)

    Return sFolderPath
  End Function

  Public Shared Function GetLoaderInvalidFolderPath(ByVal ApplicationSettings As ApplicationSettings) As String
    Dim sFolderPath As String = SystemDefault.Current(ApplicationSettings).LoaderInvalidFolderPath.Value

    SystemDefault.CreateFolderPath(sFolderPath)

    Return sFolderPath
  End Function

  Private Shared Sub CreateFolderPath(ByVal FolderPath As String)
    If Not System.IO.Directory.Exists(FolderPath) Then
      System.IO.Directory.CreateDirectory(FolderPath)
    End If
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class SystemDefaultRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SystemDefaultID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SystemDefault(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SystemDefaultID.ForeignElement = CType(Instance, SystemDefault).SystemDefaultID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
