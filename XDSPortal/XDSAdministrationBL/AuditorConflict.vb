Imports sembleWare.Runtime
Imports System
Public Class AuditorConflict 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly Auditor As Relationship = New AuditorRelationship("Auditor", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly ConflictAuditor As Relationship = New AuditorRelationship("ConflictAuditor", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _AuditorID As New IntegerElement("AuditorID", Me, True)
  Public ReadOnly _ConflictAuditorID As New IntegerElement("ConflictAuditorID", Me, True)
  Public ReadOnly _Auditor As AuditorRelationship = Auditor
  Public ReadOnly _ConflictAuditor As AuditorRelationship = ConflictAuditor
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("AuditorConflict", ApplicationSettings)
    _Auditor._AuditorID.LocalElement = _AuditorID
    _ConflictAuditor._AuditorID.LocalElement = _ConflictAuditorID
    'sembleWare: Constructor End - Do Not Modify
    Me.DeleteType = CustomDBPart.enumDeleteType.ChangeRecordStatus
  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class AuditorConflictRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _AuditorID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _ConflictAuditorID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New AuditorConflict(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _AuditorID.ForeignElement = CType(Instance, AuditorConflict)._AuditorID
    _ConflictAuditorID.ForeignElement = CType(Instance, AuditorConflict)._ConflictAuditorID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0AuditorConflict")
    oList.Columns.Add(New ListColumn("AuditorID", "[A0AuditorConflict].[AuditorID]", "AuditorID", DataType.Integer, Nothing, True, 0, False, 100, "Auditor ID"))
    oList.Columns.Add(New ListColumn("ConflictAuditorID", "[A0AuditorConflict].[ConflictAuditorID]", "ConflictAuditorID", DataType.Integer, Nothing, True, 0, False, 100, "Conflict Auditor ID"))
    oList.Columns.Add(New ListColumn("AuditorID1", "[A1ConflictAuditor].[AuditorID]", "AuditorID", DataType.Long, Nothing, False, 1, True, 100, "Auditor ID"))
    oList.Columns.Add(New ListColumn("ProfessionNo", "[A1ConflictAuditor].[ProfessionNo]", "ProfessionNo", DataType.String, Nothing, False, 0, True, 100, "Profession No"))
    oList.Columns.Add(New ListColumn("AuditorName", "[A1ConflictAuditor].[AuditorName]", "AuditorName", DataType.String, Nothing, False, 0, True, 200, "Auditor Name"))
    oList.SelectStatement = "select"
    oList.FromClause = "([AuditorConflict]  [A0AuditorConflict]" + _
           "    join [Auditor]  [A1ConflictAuditor] on [A0AuditorConflict].[ConflictAuditorID] = [A1ConflictAuditor].[AuditorID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
