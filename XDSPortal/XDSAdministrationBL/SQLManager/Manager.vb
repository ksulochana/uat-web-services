Imports System
Imports System.IO
Imports System.Text
Imports sembleWare.Runtime
Imports XDSAdministrationBL

Namespace SQLManager
#Region " Namespace Enumerators"
  Friend Enum TableTypes
    Consumer
    Staging
  End Enum
#End Region

  Public Class Manager
#Region " Variables"
    Private moMetaDatabase As MetaDatabase
    Private moFileFormatDeployment As FileFormatDeployment
    Private moLoaderEngineManager As LoaderEngineManager.Manager
#End Region

#Region " Constructor"
    Public Sub New(ByVal MetaDatabase As MetaDatabase, ByVal FileFormatDeployment As FileFormatDeployment, ByVal LoaderEngineManager As LoaderEngineManager.Manager)
      moMetaDatabase = MetaDatabase
      moFileFormatDeployment = FileFormatDeployment
      moLoaderEngineManager = LoaderEngineManager
    End Sub
#End Region

#Region " Methods"
#Region " Deploy"
    'Public Sub DeployStagingTable()
    '  If moMetaDatabase.MetaDatabaseTypeInd.Value = XDSAdministrationBL.Constants.MetaDatabase.Types.System Then
    '    Dim oStagingTable As SQLManager.StagingTable = New SQLManager.StagingTable(moMetaDatabase, moLoaderEngineManager)
    '    oStagingTable.DropTable()
    '    oStagingTable.CreateTable()

    '    Dim oStagingMultipleMatchTable As SQLManager.StagingMultipleMatchTable = New SQLManager.StagingMultipleMatchTable(moMetaDatabase, moLoaderEngineManager)
    '    oStagingMultipleMatchTable.DropTable()
    '    oStagingMultipleMatchTable.CreateTable()
    '  End If
    'End Sub

    Public Sub DeployLoaderStagingTable(ByVal LoaderID As Long)
      If moMetaDatabase.MetaDatabaseTypeInd.Value = XDSAdministrationBL.Constants.MetaDatabase.Types.System Then
        Dim oStagingTable As SQLManager.StagingTable = New SQLManager.StagingTable(moMetaDatabase, moLoaderEngineManager)
        oStagingTable.DropTable(LoaderID)
        oStagingTable.CreateTable(LoaderID)

        Dim oStagingMultipleMatchTable As SQLManager.StagingMultipleMatchTable = New SQLManager.StagingMultipleMatchTable(moMetaDatabase, moLoaderEngineManager)
        oStagingMultipleMatchTable.DropTable(LoaderID)
        oStagingMultipleMatchTable.CreateTable(LoaderID)
      End If
    End Sub

    Public Sub DeployRepositoryTable()
      If moMetaDatabase.MetaDatabaseTypeInd.Value = XDSAdministrationBL.Constants.MetaDatabase.Types.Repository Then
        Dim oRepositoryTable As SQLManager.RepositoryTable = New SQLManager.RepositoryTable(moMetaDatabase, moLoaderEngineManager)
        oRepositoryTable.DropTable()
        oRepositoryTable.CreateTable()
      End If
    End Sub

    Public Sub DeployDatabaseSpecificFunctions()
      If moMetaDatabase.MetaDatabaseTypeInd.Value = XDSAdministrationBL.Constants.MetaDatabase.Types.System Then
        Dim oDataProcedure As SQLManager.DataProcedure = New SQLManager.DataProcedure(moMetaDatabase, moLoaderEngineManager)
        oDataProcedure.CreateFunctions()
      End If
    End Sub

    Public Sub DeployNonDatabaseSpecificFunctions()
      Dim oDataProcedure As SQLManager.DataProcedure = New SQLManager.DataProcedure(Nothing, moLoaderEngineManager)
      oDataProcedure.CreateFunctions()
    End Sub

    Public Sub DeployStoredProcedures()
      DeployLoaderStoredProcedures(0)

      If moMetaDatabase.MetaDatabaseTypeInd.Value = XDSAdministrationBL.Constants.MetaDatabase.Types.System Then
        Dim oLoaderDataUndo As SQLManager.LoaderDataUndo = New SQLManager.LoaderDataUndo(moMetaDatabase, moFileFormatDeployment, moLoaderEngineManager)
        oLoaderDataUndo.DropStoredProcedure()
        oLoaderDataUndo.CreateStoredProcedure()
      End If

      RemoveLoaderStoredProcedures(0)
    End Sub

    Public Sub DeployLoaderStoredProcedures(ByVal LoaderID As Long)
      If moMetaDatabase.MetaDatabaseTypeInd.Value = XDSAdministrationBL.Constants.MetaDatabase.Types.System Then
        Dim oLoaderDataValidation As SQLManager.LoaderDataValidation = New SQLManager.LoaderDataValidation(moMetaDatabase, moFileFormatDeployment, moLoaderEngineManager, LoaderID)
        oLoaderDataValidation.DropStoredProcedure()
        oLoaderDataValidation.CreateStoredProcedure()

        Dim oLoaderDataMatching As SQLManager.LoaderDataMatching = New SQLManager.LoaderDataMatching(moMetaDatabase, moFileFormatDeployment, moLoaderEngineManager, LoaderID)
        oLoaderDataMatching.DropStoredProcedure()
        oLoaderDataMatching.CreateStoredProcedure()

        Dim oLoaderDataUpdate As SQLManager.LoaderDataUpdate = New SQLManager.LoaderDataUpdate(moMetaDatabase, moFileFormatDeployment, moLoaderEngineManager, LoaderID)
        oLoaderDataUpdate.DropStoredProcedure()
        oLoaderDataUpdate.CreateStoredProcedure()
      End If
    End Sub
#End Region

#Region " Remove"
    Public Function IsDataInStagingTable() As Boolean
      Dim oStagingTable As SQLManager.StagingTable = New SQLManager.StagingTable(moMetaDatabase, moLoaderEngineManager)

      If oStagingTable.RecordCount() > 0 Then
        Return True
      End If

      Return False
    End Function

    Public Sub RemoveStagingTable()
      If moMetaDatabase.MetaDatabaseTypeInd.Value = XDSAdministrationBL.Constants.MetaDatabase.Types.System Then
        Dim oStagingTable As SQLManager.StagingTable = New SQLManager.StagingTable(moMetaDatabase, moLoaderEngineManager)
        oStagingTable.DropTable()

        Dim oStagingMultipleMatchTable As SQLManager.StagingMultipleMatchTable = New SQLManager.StagingMultipleMatchTable(moMetaDatabase, moLoaderEngineManager)
        oStagingMultipleMatchTable.DropTable()
      End If
    End Sub

    Public Sub RemoveLoaderStagingTable(ByVal LoaderID As Long)
      If moMetaDatabase.MetaDatabaseTypeInd.Value = XDSAdministrationBL.Constants.MetaDatabase.Types.System Then
        Dim oStagingTable As SQLManager.StagingTable = New SQLManager.StagingTable(moMetaDatabase, moLoaderEngineManager)
        oStagingTable.DropTable(LoaderID)
      End If
    End Sub

    Public Sub RemoveDatabaseSpecificFunctions()
      If moMetaDatabase.MetaDatabaseTypeInd.Value = XDSAdministrationBL.Constants.MetaDatabase.Types.System Then
        Dim oDataProcedure As SQLManager.DataProcedure = New SQLManager.DataProcedure(moMetaDatabase, moLoaderEngineManager)
        oDataProcedure.DropFunctions()
      End If
    End Sub

    Public Sub RemoveNonDatabaseSpecificFunctions()
      Dim oDataProcedure As SQLManager.DataProcedure = New SQLManager.DataProcedure(Nothing, moLoaderEngineManager)
      oDataProcedure.DropFunctions()
    End Sub

    Public Sub RemoveLoaderStoredProcedures(ByVal LoaderID As Long)
      If moMetaDatabase.MetaDatabaseTypeInd.Value = XDSAdministrationBL.Constants.MetaDatabase.Types.System Then
        Dim oLoaderDataValidation As SQLManager.LoaderDataValidation = New SQLManager.LoaderDataValidation(moMetaDatabase, moFileFormatDeployment, moLoaderEngineManager, LoaderID)
        oLoaderDataValidation.DropStoredProcedure()

        Dim oLoaderDataMatching As SQLManager.LoaderDataMatching = New SQLManager.LoaderDataMatching(moMetaDatabase, moFileFormatDeployment, moLoaderEngineManager, LoaderID)
        oLoaderDataMatching.DropStoredProcedure()

        Dim oLoaderDataUpdate As SQLManager.LoaderDataUpdate = New SQLManager.LoaderDataUpdate(moMetaDatabase, moFileFormatDeployment, moLoaderEngineManager, LoaderID)
        oLoaderDataUpdate.DropStoredProcedure()
      End If
    End Sub

    Public Sub RemoveStoredProcedures()
      If moMetaDatabase.MetaDatabaseTypeInd.Value = XDSAdministrationBL.Constants.MetaDatabase.Types.System Then
        Dim oLoaderDataUndo As SQLManager.LoaderDataUndo = New SQLManager.LoaderDataUndo(moMetaDatabase, moFileFormatDeployment, moLoaderEngineManager)
        oLoaderDataUndo.DropStoredProcedure()
      End If
    End Sub
#End Region

    Public Sub ExecuteDataValidation(ByVal LoaderID As Long)
      Dim oLoaderDataValidation As LoaderDataValidation = New LoaderDataValidation(moMetaDatabase, moFileFormatDeployment, moLoaderEngineManager, LoaderID)
      Dim oCommand As SqlClient.SqlCommand = moLoaderEngineManager.ApplicationSettings.CreateCommand

      oCommand.CommandType = CommandType.StoredProcedure
      oCommand.CommandText = oLoaderDataValidation.StoredProcedureName
      oCommand.CommandTimeout = 0

      oCommand.Parameters.Add("@LoaderID", SqlDbType.Int)

      oCommand.Parameters("@LoaderID").Value = LoaderID

      moLoaderEngineManager.ApplicationSettings.ActionQuery(oCommand)
    End Sub

    Public Sub ExecuteDataMatching(ByVal LoaderID As Long)
      Dim oLoaderDataMatching As LoaderDataMatching = New LoaderDataMatching(moMetaDatabase, moFileFormatDeployment, moLoaderEngineManager, LoaderID)
      Dim oCommand As SqlClient.SqlCommand = moLoaderEngineManager.ApplicationSettings.CreateCommand

      oCommand.CommandType = CommandType.StoredProcedure
      oCommand.CommandText = oLoaderDataMatching.StoredProcedureName
      oCommand.CommandTimeout = 0

      oCommand.Parameters.Add("@LoaderID", SqlDbType.Int)

      oCommand.Parameters("@LoaderID").Value = LoaderID

      moLoaderEngineManager.ApplicationSettings.ActionQuery(oCommand)
    End Sub

    Public Sub ExecuteDataUpdate(ByVal LoaderID As Long)
      Dim oLoaderDataUpdate As LoaderDataUpdate = New LoaderDataUpdate(moMetaDatabase, moFileFormatDeployment, moLoaderEngineManager, LoaderID)
      Dim oCommand As SqlClient.SqlCommand = moLoaderEngineManager.ApplicationSettings.CreateCommand

      oCommand.CommandType = CommandType.StoredProcedure
      oCommand.CommandText = oLoaderDataUpdate.StoredProcedureName
      oCommand.CommandTimeout = 0

      oCommand.Parameters.Add("@LoaderID", SqlDbType.Int)

      oCommand.Parameters("@LoaderID").Value = LoaderID

      moLoaderEngineManager.ApplicationSettings.ActionQuery(oCommand)
    End Sub

    Public Sub ExecuteDataUndo(ByVal LoaderID As Long)
      Dim oLoaderDataUndo As LoaderDataUndo = New LoaderDataUndo(moMetaDatabase, moFileFormatDeployment, moLoaderEngineManager)
      Dim oCommand As SqlClient.SqlCommand = moLoaderEngineManager.ApplicationSettings.CreateCommand

      oCommand.CommandType = CommandType.StoredProcedure
      oCommand.CommandText = oLoaderDataUndo.StoredProcedureName
      oCommand.CommandTimeout = 0

      oCommand.Parameters.Add("@LoaderID", SqlDbType.Int)

      oCommand.Parameters("@LoaderID").Value = LoaderID

      moLoaderEngineManager.ApplicationSettings.ActionQuery(oCommand)
    End Sub
#End Region
  End Class
End Namespace