Imports System
Imports System.IO
Imports System.Text
Imports sembleWare.Runtime
Imports XDSAdministrationBL

Namespace SQLManager
  Friend Class LoaderDataUpdate
#Region " Variables"
    Private moMetaDatabase As MetaDatabase
    Private moLoaderEngineManager As LoaderEngineManager.Manager
    Private moFileFormatDeployment As FileFormatDeployment
    Private mlLoaderID As Long
#End Region

#Region " Properties"
    Public ReadOnly Property StoredProcedureName() As String
      Get
        Return "sp" & Me.StagingTableName & "_DataUpdate"
      End Get
    End Property

    Public ReadOnly Property DatabaseName() As String
      Get
        Return moMetaDatabase.DatabaseName.Value
      End Get
    End Property

    Private ReadOnly Property BaseStagingTableName() As String
      Get
        Return SQLManager.StagingTable.TableName(moMetaDatabase, moLoaderEngineManager.FileFormat, moLoaderEngineManager.FileFormatDeploymentID, 0)
      End Get
    End Property

    Private ReadOnly Property BaseStagingMultipleMatchTableName() As String
      Get
        Return SQLManager.StagingMultipleMatchTable.TableName(moMetaDatabase, moLoaderEngineManager.FileFormat, moLoaderEngineManager.FileFormatDeploymentID, 0)
      End Get
    End Property

    Private ReadOnly Property StagingTableName() As String
      Get
        Return SQLManager.StagingTable.TableName(moMetaDatabase, moLoaderEngineManager.FileFormat, moLoaderEngineManager.FileFormatDeploymentID, mlLoaderID)
      End Get
    End Property

    Public ReadOnly Property StagingMultipleMatchTableName() As String
      Get
        Return SQLManager.StagingMultipleMatchTable.TableName(moMetaDatabase, moLoaderEngineManager.FileFormat, moLoaderEngineManager.FileFormatDeploymentID, mlLoaderID)
      End Get
    End Property

    Private ReadOnly Property StagingTableIndexName() As String
      Get
        Return SQLManager.StagingTable.TableIndexName(moMetaDatabase, moLoaderEngineManager.FileFormat, moLoaderEngineManager.FileFormatDeploymentID, mlLoaderID)
      End Get

    End Property
#End Region

#Region " Constructors"
    Public Sub New(ByVal MetaDatabase As MetaDatabase, ByVal FileFormatDeployment As FileFormatDeployment, ByVal LoaderEngineManager As LoaderEngineManager.Manager, ByVal LoaderID As Long)
      moMetaDatabase = MetaDatabase
      moLoaderEngineManager = LoaderEngineManager
      moFileFormatDeployment = FileFormatDeployment
      mlLoaderID = LoaderID
    End Sub
#End Region

#Region " Methods"
    Public Sub DropStoredProcedure()
      Dim oString As StringBuilder = New StringBuilder

      oString.Append("if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[" & Me.StoredProcedureName & "]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbNewLine)
      oString.Append("drop procedure [dbo].[" & Me.StoredProcedureName & "]" & vbNewLine)

      moLoaderEngineManager.ApplicationSettings.ActionQuery(oString.ToString())
    End Sub

    Public Sub CreateStoredProcedure()
      Dim oStringBuilder As StringBuilder = New StringBuilder
      Dim oFileFormatDeploymentMetaLoaderProcedure As FileFormatDeploymentMetaLoaderProcedure = FileFormatDeploymentMetaLoaderProcedure.GetDataUpdate(moFileFormatDeployment, moLoaderEngineManager.FileFormat, moMetaDatabase, moLoaderEngineManager.ApplicationSettings)
      Dim oStringReader As StringReader = New StringReader(oFileFormatDeploymentMetaLoaderProcedure.TemplateDetails.Value)

      Try
        Dim sLine As String = oStringReader.ReadLine()

        While Not sLine Is Nothing
          If Not sLine.IndexOf(Constants.StoredProcedureName) = -1 Then
            sLine = sLine.Replace(Constants.StoredProcedureName, Me.StoredProcedureName)
          End If

          If Not sLine.IndexOf(Constants.DatabaseName) = -1 Then
            sLine = sLine.Replace(Constants.DatabaseName, Me.DatabaseName)
          End If

          If Not sLine.IndexOf(Constants.BaseStaging_TableName) = -1 Then
            sLine = sLine.Replace(Constants.BaseStaging_TableName, Me.BaseStagingTableName)
          End If

          If Not sLine.IndexOf(Constants.BaseStaging_MultipleMatch_TableName) = -1 Then
            sLine = sLine.Replace(Constants.BaseStaging_MultipleMatch_TableName, Me.BaseStagingMultipleMatchTableName)
          End If

          If Not sLine.IndexOf(Constants.Staging_Fields) = -1 Then
            Dim oStagingTable As StagingTable = New StagingTable(moMetaDatabase, moLoaderEngineManager)

            sLine = sLine.Replace(Constants.Staging_Fields, oStagingTable.GetFields())
          End If

          If Not sLine.IndexOf(Constants.Staging_MultipleMatch_Fields) = -1 Then
            Dim oStagingMultipleMatchTable As StagingMultipleMatchTable = New StagingMultipleMatchTable(moMetaDatabase, moLoaderEngineManager)

            sLine = sLine.Replace(Constants.Staging_MultipleMatch_Fields, oStagingMultipleMatchTable.GetFields())
          End If

          If Not sLine.IndexOf(Constants.Staging_TableName) = -1 Then
            sLine = sLine.Replace(Constants.Staging_TableName, Me.StagingTableName)
          End If

          If Not sLine.IndexOf(Constants.Staging_MultipleMatch_TableName) = -1 Then
            sLine = sLine.Replace(Constants.Staging_MultipleMatch_TableName, Me.StagingMultipleMatchTableName)
          End If

          If Not sLine.IndexOf(Constants.FileFormat_MetaDatabaseCode) = -1 Then
            sLine = sLine.Replace(Constants.FileFormat_MetaDatabaseCode, oFileFormatDeploymentMetaLoaderProcedure._MetaDatabaseCode.Value)
          End If

          oStringBuilder.Append(sLine & vbNewLine)

          If Not sLine.IndexOf(Constants.MetaGroupRepeater_Start) = -1 Then
            ReplaceMetaGroupRepeaterTag(oStringReader, Constants.MetaGroupRepeater_End, "", False, oStringBuilder)
          End If

          If Not sLine.IndexOf(Constants.MetaGroupInsertRepeater_Start) = -1 Then
            ReplaceMetaGroupRepeaterTag(oStringReader, Constants.MetaGroupInsertRepeater_End, "", True, oStringBuilder)
          End If

          If Not sLine.IndexOf(Constants.MetaGroupLastUpdatedRepeater_Start) = -1 Then
            ReplaceMetaGroupRepeaterTag(oStringReader, Constants.MetaGroupLastUpdatedRepeater_End, XDSAdministrationBL.Constants.LoaderDataUpdateMode.LastUpdatedDate, False, oStringBuilder)
          End If

          If Not sLine.IndexOf(Constants.MetaGroupAllFieldsRepeater_Start) = -1 Then
            ReplaceMetaGroupRepeaterTag(oStringReader, Constants.MetaGroupAllFieldsRepeater_End, XDSAdministrationBL.Constants.LoaderDataUpdateMode.AllFields, False, oStringBuilder)
          End If

          sLine = oStringReader.ReadLine()
        End While
        oStringReader.Close()

        moLoaderEngineManager.ApplicationSettings.ActionQuery(oStringBuilder.ToString())
      Catch oException As Exception
        Dim sErrorString As String = "Error deploying Loader Procedure Data Update of File Format: " & moLoaderEngineManager.FileFormat.FileFormatCode.Value & " - " & moLoaderEngineManager.FileFormat.FileFormatDesc.Value
        Throw New Exception(sErrorString & vbNewLine & oException.Message, New Exception(oStringBuilder.ToString(), oException))
      End Try
    End Sub
#End Region

#Region " Helper Functions"
    Private Sub ReplaceMetaGroupRepeaterTag(ByVal StringReader As StringReader, ByVal EndRepeaterTag As String, ByVal LoaderDataUpdateModeInd As String, ByVal InsertYN As Boolean, ByRef StringBuilder As StringBuilder)
      Dim oXDSAdministrationRoot As XDSAdministrationRoot = moLoaderEngineManager.FileFormat.XDSAdministrationRoot.Instance
      Dim oList As List = oXDSAdministrationRoot._vwDeploymentFileFormatMetaGroup_OwnMany.GridList
      Dim oDataRow As DataRow
      Dim oRepeaterArray As ArrayList = New ArrayList
      Dim lFileFormatMapGroupID As Long = 0

      Dim sLine As String = StringReader.ReadLine()
      While Not sLine Is Nothing
        If Not sLine.IndexOf(EndRepeaterTag) = -1 Then
          Exit While
        End If
        oRepeaterArray.Add(sLine)
        sLine = StringReader.ReadLine()
      End While

      oList.LimitByRelatedPart(moMetaDatabase, "MetaDatabase", True)
      oList.AndFilters.Add(New ListFilter("FileFormatCode", "FileFormatCode", moLoaderEngineManager.FileFormat.FileFormatCode.Value, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      oList.AndFilters.Add(New ListFilter("LoaderDataUpdateModeInd", "LoaderDataUpdateModeInd", LoaderDataUpdateModeInd, DataType.String, FilterType.Equals, FilterBlankBehavior.IgnoreFilter, True))
      For Each oDataRow In oList.DataTable.Rows
        AddMetaGroup(StringReader, oDataRow, oRepeaterArray, InsertYN, StringBuilder)
      Next
    End Sub

    Private Sub ReplaceSystemStagingFieldRepeaterTag(ByVal RepeaterArray As ArrayList, ByVal StartRepeaterTag As String, ByVal EndRepeaterTag As String, ByRef StringBuilder As StringBuilder, ByVal DataRow As DataRow)
      Dim oXDSAdministrationRoot As XDSAdministrationRoot = moLoaderEngineManager.FileFormat.XDSAdministrationRoot.Instance
      Dim oDBList As DBList = oXDSAdministrationRoot._vwDeploymentFileFormatMetaGroupField_OwnMany.GridList
      Dim oDataRow As DataRow
      Dim oRepeaterArray As ArrayList = New ArrayList
      Dim sString As String

      Dim bStartYN As Boolean = False
      For Each sString In RepeaterArray
        If bStartYN Then
          If Not sString.IndexOf(EndRepeaterTag) = -1 Then
            Exit For
          End If
          oRepeaterArray.Add(sString)
        End If
        If Not sString.IndexOf(StartRepeaterTag) = -1 Then
          bStartYN = True
        End If
      Next

      oDBList.LimitByRelatedPart(moMetaDatabase, "MetaDatabase", True)
      oDBList.AndFilters.Add(New ListFilter("FileFormatCode", "FileFormatCode", moLoaderEngineManager.FileFormat.FileFormatCode.Value, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      oDBList.AndFilters.Add(New ListFilter("MetaDatabaseCode", "MetaDatabaseCode", DataRow("MetaDatabaseCode"), DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      oDBList.AndFilters.Add(New ListFilter("MetaTableName", "MetaTableName", DataRow("MetaTableName"), DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      oDBList.AndFilters.Add(New ListFilter("FileFormatMetaGroupID", "FileFormatMetaGroupID", DataRow("FileFormatMetaGroupID"), DataType.Integer, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      oDBList.AndFilters.Add(New ListFilter("MetaGroupFieldTypeInd", "MetaGroupFieldTypeInd", XDSAdministrationBL.Constants.MetaGroupField.Types.FileFormatField, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      For Each oDataRow In oDBList.DataTable.Rows
        AddMetaGroupField(oDataRow, oRepeaterArray, StringBuilder)
      Next
    End Sub

    Private Sub ReplaceMetaGroupFieldRepeaterTag(ByVal RepeaterArray As ArrayList, ByVal StartRepeaterTag As String, ByVal EndRepeaterTag As String, ByRef StringBuilder As StringBuilder, ByVal DataRow As DataRow, ByVal MetaGroupFieldTypeInd As String, ByVal bInsertYN As Boolean)
      Dim oXDSAdministrationRoot As XDSAdministrationRoot = moLoaderEngineManager.FileFormat.XDSAdministrationRoot.Instance
      Dim oDBList As DBList = oXDSAdministrationRoot._vwDeploymentFileFormatMetaGroupField_OwnMany.GridList
      Dim oDataRow As DataRow
      Dim oRepeaterArray As ArrayList = New ArrayList
      Dim lFileFormatMapGroupID As Long = 0
      Dim sString As String

      Dim bStartYN As Boolean = False
      For Each sString In RepeaterArray
        If bStartYN Then
          If Not sString.IndexOf(EndRepeaterTag) = -1 Then
            Exit For
          End If
          oRepeaterArray.Add(sString)
        End If
        If Not sString.IndexOf(StartRepeaterTag) = -1 Then
          bStartYN = True
        End If
      Next

      oDBList.LimitByRelatedPart(moMetaDatabase, "MetaDatabase", True)
      oDBList.AndFilters.Add(New ListFilter("FileFormatCode", "FileFormatCode", moLoaderEngineManager.FileFormat.FileFormatCode.Value, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      oDBList.AndFilters.Add(New ListFilter("MetaDatabaseCode", "MetaDatabaseCode", DataRow("MetaDatabaseCode"), DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      oDBList.AndFilters.Add(New ListFilter("MetaTableName", "MetaTableName", DataRow("MetaTableName"), DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      oDBList.AndFilters.Add(New ListFilter("FileFormatMetaGroupID", "FileFormatMetaGroupID", DataRow("FileFormatMetaGroupID"), DataType.Integer, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      oDBList.AndFilters.Add(New ListFilter("MetaGroupFieldTypeInd", "MetaGroupFieldTypeInd", MetaGroupFieldTypeInd, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      If bInsertYN Then
        oDBList.AndFilters.Add(New ListFilter("LoaderDataUpdateUseForInsertYN", "LoaderDataUpdateUseForInsertYN", True, DataType.Boolean, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      Else
        oDBList.AndFilters.Add(New ListFilter("LoaderDataUpdateUseForUpdateYN", "LoaderDataUpdateUseForUpdateYN", True, DataType.Boolean, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      End If

      Select Case MetaGroupFieldTypeInd
        Case "F"
          oDBList.AppendToWhereClause("FileFormatFieldID is not null")
      End Select
      For Each oDataRow In oDBList.DataTable.Rows
        AddMetaGroupField(oDataRow, oRepeaterArray, StringBuilder)
      Next
    End Sub

    Private Sub AddMetaGroup(ByVal StringReader As StringReader, ByVal DataRow As DataRow, ByVal RepeaterArray As ArrayList, ByVal InsertYN As Boolean, ByRef StringBuilder As StringBuilder)
      Dim sString As String
      Dim bSkipLineYN As Boolean = False

      For Each sString In RepeaterArray
        If bSkipLineYN Then
          If Not sString.IndexOf(Constants.SystemStagingFieldRepeater_End) = -1 Then
            bSkipLineYN = False
          End If

          If Not sString.IndexOf(Constants.MetaGroupFieldValueRepeater_End) = -1 Then
            bSkipLineYN = False
          End If
        Else
          If Not sString.IndexOf(Constants.System_TableName) = -1 Then
            Dim sTableName As String = DataRow("DatabaseName") & ".." & DataRow("MetaTableName")
            sString = sString.Replace(Constants.System_TableName, sTableName)
          End If

          If Not sString.IndexOf(Constants.Staging_TableName) = -1 Then
            sString = sString.Replace(Constants.Staging_TableName, Me.StagingTableName)
          End If

          If Not sString.IndexOf(Constants.Staging_Table_IndexName) = -1 Then
            sString = sString.Replace(Constants.Staging_Table_IndexName, Me.StagingTableIndexName & "_" & DTSManager.Constants.Field_RecordChecksumMetaGroup & DataRow("FileFormatMetaGroupID"))
          End If

          If Not sString.IndexOf(Constants.System_Fields) = -1 Then
            sString = sString.Replace(Constants.System_Fields, GetTableFields(TableTypes.Consumer, DataRow))
          End If

          If Not sString.IndexOf(Constants.Staging_Fields) = -1 Then
            sString = sString.Replace(Constants.Staging_Fields, GetTableFields(TableTypes.Staging, DataRow))
          End If

          If Not sString.IndexOf(Constants.MetaGroup_MetaGroupCode) = -1 Then
            sString = sString.Replace(Constants.MetaGroup_MetaGroupCode, DataRow("MetaGroupCode"))
          End If

          If Not sString.IndexOf(Constants.FileFormatMetaGroup_FileFormatMetaGroupID) = -1 Then
            sString = sString.Replace(Constants.FileFormatMetaGroup_FileFormatMetaGroupID, DataRow("FileFormatMetaGroupID"))
          End If

          If Not sString.IndexOf(Constants.Staging_RecordChecksumField) = -1 Then
            sString = sString.Replace(Constants.Staging_RecordChecksumField, DTSManager.Constants.Field_RecordChecksumMetaGroup & DataRow("FileFormatMetaGroupID"))
          End If

          If Not sString.IndexOf(Constants.Staging_MetaGroupTotalLengthField) = -1 Then
            sString = sString.Replace(Constants.Staging_MetaGroupTotalLengthField, DTSManager.Constants.Field_TotalLengthMetaGroup & DataRow("FileFormatMetaGroupID"))
          End If

          If Not sString.IndexOf(Constants.SystemStagingFieldRepeater_Start) = -1 Then
            ReplaceSystemStagingFieldRepeaterTag(RepeaterArray, Constants.SystemStagingFieldRepeater_Start, Constants.SystemStagingFieldRepeater_End, StringBuilder, DataRow)
            bSkipLineYN = True
          End If

          If Not sString.IndexOf(Constants.MetaGroupFieldValueRepeater_Start) = -1 Then
            ReplaceMetaGroupFieldRepeaterTag(RepeaterArray, Constants.MetaGroupFieldValueRepeater_Start, Constants.MetaGroupFieldValueRepeater_End, StringBuilder, DataRow, "V", InsertYN)
            bSkipLineYN = True
          End If

          If Not bSkipLineYN Then
            StringBuilder.Append(sString & vbNewLine)
          End If
        End If
      Next
    End Sub

    Private Sub AddMetaGroupField(ByVal DataRow As DataRow, ByVal RepeaterArray As ArrayList, ByRef StringBuilder As StringBuilder)
      Dim sString As String

      For Each sString In RepeaterArray
        If Not sString.IndexOf(Constants.System_TableName) = -1 Then
          Dim sTableName As String = DataRow("DatabaseName") & ".." & DataRow("MetaTableName")
          sString = sString.Replace(Constants.System_TableName, sTableName)
        End If

        If Not sString.IndexOf(Constants.Staging_TableName) = -1 Then
          sString = sString.Replace(Constants.Staging_TableName, Me.StagingTableName)
        End If

        If Not sString.IndexOf(Constants.System_Field) = -1 Then
          sString = sString.Replace(Constants.System_Field, GetTableField(TableTypes.Consumer, DataRow))
        End If

        If Not sString.IndexOf(Constants.Staging_Field) = -1 Then
          sString = sString.Replace(Constants.Staging_Field, GetTableField(TableTypes.Staging, DataRow))
        End If

        If Not sString.IndexOf(Constants.MetaGroupField_Value) = -1 Then
          sString = sString.Replace(Constants.MetaGroupField_Value, GetTableField(TableTypes.Staging, DataRow))
        End If

        StringBuilder.Append(sString & vbNewLine)
      Next
    End Sub

    Private Function GetTableFields(ByVal TableType As TableTypes) As String
      Dim sFields As String = ""

      Select Case TableType
        Case TableTypes.Staging
          Dim oList As List = moLoaderEngineManager.FileFormat._FileFormatField_OwnMany.GridList
          Dim oDataRow As DataRow

          sFields &= DTSManager.Constants.Field_RecordID & ", "
          sFields &= DTSManager.Constants.Field_LoaderID & ", "
          sFields &= DTSManager.Constants.Field_IsValidRecordYN & ", "
          sFields &= DTSManager.Constants.Field_IsPossibleNameConflictYN & ", "
          sFields &= DTSManager.Constants.Field_IsPossibleDuplicateRecordYN & ", "
          sFields &= DTSManager.Constants.Field_RequiredValidationDataProcedureDetails & ", "
          sFields &= DTSManager.Constants.Field_NonRequiredValidationDataProcedureDetails & ", "

          For Each oDataRow In oList.DataTable.Rows
            sFields &= moLoaderEngineManager.ParseFileFormatFieldDesc(oDataRow("FileFormatFieldDesc")) & ", "
          Next
          If sFields.Length > 0 Then
            sFields = sFields.Substring(0, sFields.Length - 2)
          End If
      End Select

      Return sFields
    End Function

    Private Function GetTableFields(ByVal TableType As TableTypes, ByVal DataRow As DataRow) As String
      Dim oXDSAdministrationRoot As XDSAdministrationRoot = moLoaderEngineManager.FileFormat.XDSAdministrationRoot.Instance
      Dim oList As List = oXDSAdministrationRoot._vwDeploymentFileFormatMetaGroupField_OwnMany.GridList
      Dim oDataRow As DataRow
      Dim sFields As String = ""

      oList.LimitByRelatedPart(moMetaDatabase, "MetaDatabase", True)
      oList.AndFilters.Add(New ListFilter("FileFormatCode", "FileFormatCode", moLoaderEngineManager.FileFormat.FileFormatCode.Value, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      oList.AndFilters.Add(New ListFilter("MetaDatabaseCode", "MetaDatabaseCode", DataRow("MetaDatabaseCode"), DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      oList.AndFilters.Add(New ListFilter("MetaTableName", "MetaTableName", DataRow("MetaTableName"), DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      oList.AndFilters.Add(New ListFilter("FileFormatMetaGroupID", "FileFormatMetaGroupID", DataRow("FileFormatMetaGroupID"), DataType.Integer, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      For Each oDataRow In oList.DataTable.Rows
        Select Case TableType
          Case TableTypes.Consumer
            Select Case oDataRow("MetaGroupFieldTypeInd")
              Case "F"
                If Not IsDBNull(oDataRow("FileFormatFieldID")) Then
                  sFields &= oDataRow("MetaFieldName") & ", "
                End If
              Case "V"
                sFields &= oDataRow("MetaFieldName") & ", "
            End Select
          Case TableTypes.Staging
            Select Case oDataRow("MetaGroupFieldTypeInd")
              Case "F"
                If Not IsDBNull(oDataRow("FileFormatFieldID")) Then
                  sFields &= moLoaderEngineManager.ParseFileFormatFieldDesc(oDataRow("FileFormatFieldDesc")) & ", "
                End If
              Case "V"
                sFields &= GetAssignedValue(oDataRow("AssignedValue"), oDataRow("MetaFieldDataTypeInd")) & ", "
            End Select
        End Select
      Next
      If sFields.Length > 0 Then
        sFields = sFields.Substring(0, sFields.Length - 2)
      End If
      Return sFields
    End Function

    Private Function GetTableField(ByVal TableType As TableTypes, ByVal DataRow As DataRow) As String
      Dim sField As String = ""

      Select Case TableType
        Case TableTypes.Consumer
          Select Case DataRow("MetaGroupFieldTypeInd")
            Case "F"
              If Not IsDBNull(DataRow("FileFormatFieldID")) Then
                sField &= DataRow("MetaFieldName")
              End If
            Case "V"
              sField &= DataRow("MetaFieldName")
          End Select
        Case TableTypes.Staging
          Select Case DataRow("MetaGroupFieldTypeInd")
            Case "F"
              If Not IsDBNull(DataRow("FileFormatFieldID")) Then
                sField &= moLoaderEngineManager.ParseFileFormatFieldDesc(DataRow("FileFormatFieldDesc"))
              End If
            Case "V"
              sField &= GetAssignedValue(DataRow("AssignedValue"), DataRow("MetaFieldDataTypeInd"))
          End Select
      End Select

      Return sField
    End Function

    Private Function GetAssignedValue(ByVal AssignedValue As String, ByVal DataTypeInd As String) As String
      Select Case DataTypeInd
        Case "S", "D"
          Return "'" & AssignedValue & "'"
        Case "N", "B"
          Return AssignedValue
      End Select
    End Function
#End Region
  End Class
End Namespace