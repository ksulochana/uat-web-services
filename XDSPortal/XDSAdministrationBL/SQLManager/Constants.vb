Imports System
Imports System.IO
Imports System.Text
Imports sembleWare.Runtime
Imports XDSAdministrationBL

Namespace SQLManager
  Public Class Constants
    Public Const FunctionName As String = "<FunctionName>"
    Public Const StoredProcedureName As String = "<StoredProcedureName>"
    Public Const DatabaseName As String = "<DatabaseName>"

    Public Const BaseStaging_TableName As String = "<BaseStaging_TableName>"
    Public Const BaseStaging_MultipleMatch_TableName As String = "<BaseStaging_MultipleMatch_TableName>"
    Public Const Staging_TableName As String = "<Staging_TableName>"
    Public Const Staging_MultipleMatch_TableName As String = "<Staging_MultipleMatch_TableName>"
    Public Const Staging_Table_IndexName As String = "<Staging_Table_IndexName>"
    Public Const Staging_Fields As String = "<Staging_Fields>"
    Public Const Staging_Field As String = "<Staging_Field>"
    Public Const Staging_RecordChecksumField As String = "<Staging_RecordChecksumField>"
    Public Const Staging_MetaGroupTotalLengthField As String = "<Staging_MetaGroupTotalLengthField>"
    Public Const Staging_MultipleMatch_Fields As String = "<Staging_MultipleMatch_Fields>"
    Public Const DuplicateStaging_Fields As String = "<DuplicateStaging_Fields>"
    Public Const DuplicateStaging_Field As String = "<DuplicateStaging_Field>"
    Public Const DuplicateStaging_FieldDataType As String = "<DuplicateStaging_FieldDataType>"

    Public Const System_TableName As String = "<System_TableName>"
    Public Const System_Fields As String = "<System_Fields>"
    Public Const System_Field As String = "<System_Field>"

    Public Const DuplicateTableFieldRepeater_Start As String = "<DuplicateTableFieldRepeater>"
    Public Const DuplicateTableFieldRepeater_End As String = "</DuplicateTableFieldRepeater>"
    Public Const DeletionRepeater_Start As String = "<DeletionRepeater>"
    Public Const DeletionRepeater_End As String = "</DeletionRepeater>"
    Public Const ConversionRepeater_Start As String = "<ConversionRepeater>"
    Public Const ConversionRepeater_End As String = "</ConversionRepeater>"
    Public Const RequiredValidationInvalidFileRepeater_Start As String = "<RequiredValidationInvalidFileRepeater>"
    Public Const RequiredValidationInvalidFileRepeater_End As String = "</RequiredValidationInvalidFileRepeater>"
    Public Const RequiredValidationRepeater_Start As String = "<RequiredValidationRepeater>"
    Public Const RequiredValidationRepeater_End As String = "</RequiredValidationRepeater>"
    Public Const NonRequiredValidationRepeater_Start As String = "<NonRequiredValidationRepeater>"
    Public Const NonRequiredValidationRepeater_End As String = "</NonRequiredValidationRepeater>"
    Public Const NonRequiredValidationInvalidFileRepeater_Start As String = "<NonRequiredValidationInvalidFileRepeater>"
    Public Const NonRequiredValidationInvalidFileRepeater_End As String = "</NonRequiredValidationInvalidFileRepeater>"
    Public Const DataStatisticRepeater_Start As String = "<DataStatisticRepeater>"
    Public Const DataStatisticRepeater_End As String = "</DataStatisticRepeater>"
    Public Const CalculationRepeater_Start As String = "<CalculationRepeater>"
    Public Const CalculationRepeater_End As String = "</CalculationRepeater>"
    Public Const MatchingEvaluationRepeater_Start As String = "<MatchingEvaluationRepeater>"
    Public Const MatchingEvaluationRepeater_End As String = "</MatchingEvaluationRepeater>"

    Public Const MetaGroupRepeater_Start As String = "<MetaGroupRepeater>"
    Public Const MetaGroupRepeater_End As String = "</MetaGroupRepeater>"
    Public Const MetaGroupInsertRepeater_Start As String = "<MetaGroupInsertRepeater>"
    Public Const MetaGroupInsertRepeater_End As String = "</MetaGroupInsertRepeater>"
    Public Const MetaGroupLastUpdatedRepeater_Start As String = "<MetaGroupLastUpdatedRepeater>"
    Public Const MetaGroupLastUpdatedRepeater_End As String = "</MetaGroupLastUpdatedRepeater>"
    Public Const MetaGroupAllFieldsRepeater_Start As String = "<MetaGroupAllFieldsRepeater>"
    Public Const MetaGroupAllFieldsRepeater_End As String = "</MetaGroupAllFieldsRepeater>"
    Public Const MetaGroupFieldRepeater_Start As String = "<MetaGroupFieldRepeater>"
    Public Const MetaGroupFieldRepeater_End As String = "</MetaGroupFieldRepeater>"
    Public Const MetaGroupFieldValueRepeater_Start As String = "<MetaGroupFieldValueRepeater>"
    Public Const MetaGroupFieldValueRepeater_End As String = "</MetaGroupFieldValueRepeater>"

    Public Const SystemStagingFieldRepeater_Start As String = "<SystemStagingFieldRepeater>"
    Public Const SystemStagingFieldRepeater_End As String = "</SystemStagingFieldRepeater>"

    Public Const MetaGroupField_Value As String = "<MetaGroupField_Value>"

    Public Const MetaGroup_MetaGroupCode As String = "<MetaGroup_MetaGroupCode>"

    Public Const FileFormatMetaGroup_FileFormatMetaGroupID As String = "<FileFormatMetaGroup_FileFormatMetaGroupID>"

    Public Const FileFormat_MetaDatabaseCode As String = "<FileFormat_MetaDatabaseCode>"
    Public Const FileFormatField_FileFormatFieldID As String = "<FileFormatField_FileFormatFieldID>"
    Public Const FileFormatField_FileFormatFieldDesc As String = "<FileFormatField_FileFormatFieldDesc>"

    Public Const DataProcedure_SQLFunctionName As String = "<DataProcedure_SQLFunctionName>"
    Public Const DataProcedure_MetaDatabaseCode As String = "<DataProcedure_MetaDatabaseCode>"
    Public Const DataProcedure_DataProcedureID As String = "<DataProcedure_DataProcedureID>"
  End Class
End Namespace