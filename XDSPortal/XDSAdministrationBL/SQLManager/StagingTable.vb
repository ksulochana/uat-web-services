Imports System
Imports System.IO
Imports System.Text
Imports sembleWare.Runtime
Imports XDSAdministrationBL

Namespace SQLManager
  Friend Class StagingTable
#Region " Constants"
    Private Const msSTAGING As String = "Staging"
#End Region

#Region " Variables"
    Private moMetaDatabase As MetaDatabase
    Private moLoaderEngineManager As LoaderEngineManager.Manager
#End Region

#Region " Properties"
    Public ReadOnly Property TableName() As String
      Get
        Return StagingTable.TableName(moMetaDatabase, moLoaderEngineManager.FileFormat, moLoaderEngineManager.FileFormatDeploymentID)
      End Get
    End Property

    Public ReadOnly Property TableIndexName() As String
      Get
        Return StagingTable.TableIndexName(moMetaDatabase, moLoaderEngineManager.FileFormat, moLoaderEngineManager.FileFormatDeploymentID)
      End Get
    End Property

    Public ReadOnly Property TableName(ByVal LoaderID As Long) As String
      Get
        Return StagingTable.TableName(moMetaDatabase, moLoaderEngineManager.FileFormat, moLoaderEngineManager.FileFormatDeploymentID, LoaderID)
      End Get
    End Property

    Public ReadOnly Property TableIndexName(ByVal LoaderID As Long) As String
      Get
        Return StagingTable.TableIndexName(moMetaDatabase, moLoaderEngineManager.FileFormat, moLoaderEngineManager.FileFormatDeploymentID, LoaderID)
      End Get
    End Property
#End Region

#Region " Shared Properties"
    Public Shared ReadOnly Property TableName(ByVal MetaDatabase As MetaDatabase, ByVal FileFormat As FileFormat, ByVal FileFormatDeploymentID As Integer) As String
      Get
        Return StagingTable.TableName(MetaDatabase, FileFormat, FileFormatDeploymentID, 0)
      End Get
    End Property

    Public Shared ReadOnly Property TableIndexName(ByVal MetaDatabase As MetaDatabase, ByVal FileFormat As FileFormat, ByVal FileFormatDeploymentID As Integer) As String
      Get
        Return StagingTable.TableIndexName(MetaDatabase, FileFormat, FileFormatDeploymentID, 0)
      End Get
    End Property

    Public Shared ReadOnly Property TableName(ByVal MetaDatabase As MetaDatabase, ByVal FileFormat As FileFormat, ByVal FileFormatDeploymentID As Integer, ByVal LoaderID As Long) As String
      Get
        Dim sTableName As String = msSTAGING & MetaDatabase.MetaDatabaseCode.Value & FileFormat.FileFormatCode.Value & "_" & FileFormatDeploymentID.ToString()
        If Not LoaderID = 0 Then
          sTableName &= "_Loader" & LoaderID.ToString()
        End If
        Return sTableName
      End Get
    End Property

    Public Shared ReadOnly Property TableIndexName(ByVal MetaDatabase As MetaDatabase, ByVal FileFormat As FileFormat, ByVal FileFormatDeploymentID As Integer, ByVal LoaderID As Long) As String
      Get
        Dim sTableIndexName As String = "IX_" & StagingTable.TableName(MetaDatabase, FileFormat, FileFormatDeploymentID)
        If Not LoaderID = 0 Then
          sTableIndexName &= "_Loader" & LoaderID.ToString()
        End If
        Return sTableIndexName
      End Get
    End Property
#End Region

#Region " Constructors"
    Public Sub New(ByVal MetaDatabase As MetaDatabase, ByVal LoaderEngineManager As LoaderEngineManager.Manager)
      moMetaDatabase = MetaDatabase
      moLoaderEngineManager = LoaderEngineManager
    End Sub
#End Region

#Region " Methods"
    Public Sub DropTable()
      Me.DropTable(0)
    End Sub

    Public Sub DropTable(ByVal LoaderID As Long)
      Dim oString As StringBuilder = New StringBuilder
      Dim sTableName As String = Me.TableName(LoaderID)

      If LoaderID = 0 Then
        sTableName = Me.TableName()
      End If

      oString.Append("if exists (select * from dbo.sysobjects where id = object_id(N'[" & sTableName & "]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbNewLine)
      oString.Append("drop table [" & sTableName & "]" & vbNewLine)

      moLoaderEngineManager.ApplicationSettings.ActionQuery(oString.ToString())
    End Sub

    Public Sub CreateTable()
      Me.CreateTable(0)
    End Sub

    Public Sub CreateTable(ByVal LoaderID As Long)
      Dim oString As StringBuilder = New StringBuilder
      Dim sTableName As String = Me.TableName(LoaderID)
      Dim sTableIndexName As String = Me.TableIndexName(LoaderID)

      Try
        If LoaderID = 0 Then
          sTableName = Me.TableName()
          sTableIndexName = Me.TableIndexName()
        End If
        oString.Append("create table " & sTableName & " (" & vbNewLine)
        If LoaderID = 0 Then
          oString.Append(DTSManager.Constants.Field_RecordID & " int not null," & vbNewLine)
        Else
          oString.Append(DTSManager.Constants.Field_RecordID & " int identity (1, 1) not null," & vbNewLine)
        End If
        oString.Append(DTSManager.Constants.Field_LoaderID & " int not null," & vbNewLine)
        oString.Append(DTSManager.Constants.Field_IsValidRecordYN & " bit not null," & vbNewLine)
        oString.Append(DTSManager.Constants.Field_IsPossibleNameConflictYN & " bit not null," & vbNewLine)
        oString.Append(DTSManager.Constants.Field_IsPossibleDuplicateRecordYN & " bit not null," & vbNewLine)
        oString.Append(DTSManager.Constants.Field_RequiredValidationDataProcedureDetails & " varchar(100) not null," & vbNewLine)
        oString.Append(DTSManager.Constants.Field_NonRequiredValidationDataProcedureDetails & " varchar(100) not null," & vbNewLine)

        AddFileFormatFields(XDSAdministrationBL.Constants.FileFormatField.Types.File, True, oString)
        AddFileFormatFields(XDSAdministrationBL.Constants.FileFormatField.Types.Calculation, True, oString)
        AddFileFormatFields(XDSAdministrationBL.Constants.FileFormatField.Types.Loader, True, oString)
        AddFileFormatFields(XDSAdministrationBL.Constants.FileFormatField.Types.System, True, oString)
        AddFileFormatMetaGroups(True, oString)

        oString.Append(")" & vbNewLine)
        moLoaderEngineManager.ApplicationSettings.ActionQuery(oString.ToString())

        oString = New StringBuilder
        oString.Append("alter table [" & sTableName & "] add" & vbNewLine)
        oString.Append("constraint [PK_" & sTableName & "] primary key clustered" & vbNewLine)
        oString.Append("(" & vbNewLine)
        oString.Append(DTSManager.Constants.Field_RecordID & ", " & vbNewLine)
        oString.Append(DTSManager.Constants.Field_LoaderID & vbNewLine)
        oString.Append(")")
        moLoaderEngineManager.ApplicationSettings.ActionQuery(oString.ToString())
      Catch oException As Exception
        Dim sErrorString As String = "Error creating staging table for File Format: " & moLoaderEngineManager.FileFormat.FileFormatCode.Value & " - " & moLoaderEngineManager.FileFormat.FileFormatDesc.Value & "."
        Throw New Exception(sErrorString & vbNewLine & oException.Message, New Exception(oString.ToString(), oException))
      End Try
    End Sub

    Public Function RecordCount(ByVal LoaderID As Long) As Long
      Dim oDataRow As DataRow
      Dim oQuery As StringBuilder = New StringBuilder
      Dim sTableName As String = Me.TableName(LoaderID)

      If LoaderID = 0 Then
        sTableName = Me.TableName()
      End If
      Try
        oQuery.Append("select count(" & DTSManager.Constants.Field_RecordID & ") ")
        oQuery.Append("  from " & sTableName)
        oQuery.Append(" where " & DTSManager.Constants.Field_LoaderID & " = " & moLoaderEngineManager.ApplicationSettings.QueryBuilder.ToSQL(LoaderID))

        For Each oDataRow In moLoaderEngineManager.ApplicationSettings.ResultQuery(oQuery.ToString()).Rows
          Return SHBS.General.ZeroIfDBNull(oDataRow(0))
        Next
      Catch
      End Try

      Return 0
    End Function

    Public Function RecordCount() As Long
      Dim oDataRow As DataRow
      Dim oQuery As StringBuilder = New StringBuilder

      Try
        oQuery.Append("select count(" & DTSManager.Constants.Field_RecordID & ") ")
        oQuery.Append("  from " & Me.TableName)

        For Each oDataRow In moLoaderEngineManager.ApplicationSettings.ResultQuery(oQuery.ToString()).Rows
          Return SHBS.General.ZeroIfDBNull(oDataRow(0))
        Next
      Catch
      End Try

      Return 0
    End Function

    Public Function GetFields() As String
      Dim oString As StringBuilder = New StringBuilder

      oString.Append(DTSManager.Constants.Field_LoaderID & ", ")
      oString.Append(DTSManager.Constants.Field_IsValidRecordYN & ", ")
      oString.Append(DTSManager.Constants.Field_IsPossibleNameConflictYN & ", " & vbNewLine)
      oString.Append(DTSManager.Constants.Field_IsPossibleDuplicateRecordYN & ", " & vbNewLine)
      oString.Append(DTSManager.Constants.Field_RequiredValidationDataProcedureDetails & ", ")
      oString.Append(DTSManager.Constants.Field_NonRequiredValidationDataProcedureDetails & ", ")

      AddFileFormatFields(XDSAdministrationBL.Constants.FileFormatField.Types.File, False, oString)
      AddFileFormatFields(XDSAdministrationBL.Constants.FileFormatField.Types.Calculation, False, oString)
      AddFileFormatFields(XDSAdministrationBL.Constants.FileFormatField.Types.Loader, False, oString)
      AddFileFormatFields(XDSAdministrationBL.Constants.FileFormatField.Types.System, False, oString)
      AddFileFormatMetaGroups(False, oString)

      If oString.Length > 0 Then
        oString = oString.Remove(oString.Length - 2, 2)
      End If

      Return oString.ToString()
    End Function

    'Public Sub CreateIndexes(ByVal LoaderID As Long)
    '  Dim oFileFormatMetaDatabase As FileFormatMetaDatabase = moLoaderEngineManager.FileFormat.FileFormatMetaDatabase_OwnMany.NewInstance()
    '  oFileFormatMetaDatabase.MetaDatabase.Instance = moMetaDatabase
    '  Dim oList As List = oFileFormatMetaDatabase._FileFormatMetaGroup_OwnMany.GridList
    '  Dim oDataRow As DataRow
    '  Dim sTableName As String = Me.TableName(LoaderID)

    '  If LoaderID = 0 Then
    '    sTableName = Me.TableName()
    '  End If
    '  For Each oDataRow In oList.DataTable.Rows
    '    Dim sMetaGroupCode As String = oDataRow("MetaGroupCode")
    '    Dim nFileFormatMetaGroupID As Integer = System.Convert.ToInt32(SHBS.General.ZeroIfDBNull(oDataRow("FileFormatMetaGroupID")))
    '    Dim sIndexName As String = Me.TableIndexName & "_" & DTSManager.Constants.Field_RecordChecksumMetaGroup & nFileFormatMetaGroupID.ToString()

    '    Dim oString As StringBuilder = New StringBuilder
    '    oString.Append("create index [" & sIndexName & "] on [" & sTableName & "]" & vbNewLine)
    '    oString.Append("(" & vbNewLine)
    '    GetFileFormatIndexes(XDSAdministrationBL.Constants.FileFormatField.Types.System, oString)
    '    oString.Append(DTSManager.Constants.Field_RecordChecksumMetaGroup & nFileFormatMetaGroupID.ToString() & ", " & vbNewLine)
    '    oString.Append(DTSManager.Constants.Field_LoaderID & ", " & vbNewLine)
    '    oString.Append(DTSManager.Constants.Field_TotalLengthMetaGroup & nFileFormatMetaGroupID.ToString() & vbNewLine)
    '    oString.Append(")")
    '    moLoaderEngineManager.ApplicationSettings.ActionQuery(oString.ToString())

    '    oString = New StringBuilder
    '    oString.Append("dbcc dbreindex(N'[" & sTableName & "]', N'" & sIndexName & "', " & DTSManager.Constants.IndexFillFactor.ToString() & ")" & vbNewLine)
    '    moLoaderEngineManager.ApplicationSettings.ActionQuery(oString.ToString())
    '  Next
    'End Sub
#End Region

#Region " Helper Functions"
    Private Sub AddFileFormatFields(ByVal FileFormatFieldTypeInd As String, ByVal CreateTableYN As Boolean, ByRef StringBuilder As StringBuilder)
      Dim oList As List = moLoaderEngineManager.FileFormat._FileFormatField_OwnMany.GridList
      Dim oDataRow As DataRow

      oList.AndFilters.Add(New ListFilter("FileFormatFieldTypeInd", "FileFormatFieldTypeInd", FileFormatFieldTypeInd, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      For Each oDataRow In oList.DataTable.Rows
        Dim sFieldName As String = moLoaderEngineManager.ParseFileFormatFieldDesc(oDataRow("FileFormatFieldDesc"))

        If CreateTableYN Then
          Select Case FileFormatFieldTypeInd
            Case XDSAdministrationBL.Constants.FileFormatField.Types.System
              StringBuilder.Append(sFieldName & " int null, " & vbNewLine)
            Case Else
              Dim nFieldLength As Integer = System.Convert.ToInt32(SHBS.General.ZeroIfDBNull(oDataRow("FieldLength"))) + 10

              nFieldLength = IIf(nFieldLength = 0, 250, nFieldLength)
              StringBuilder.Append(sFieldName & " varchar (" & nFieldLength & ") null, " & vbNewLine)
          End Select
        Else
          StringBuilder.Append(sFieldName & ", ")
        End If
      Next
    End Sub

    Private Sub AddFileFormatMetaGroups(ByVal CreateTableYN As Boolean, ByRef StringBuilder As StringBuilder)
      Dim oFileFormatMetaDatabase As FileFormatMetaDatabase = moLoaderEngineManager.FileFormat.FileFormatMetaDatabase_OwnMany.NewInstance()
      oFileFormatMetaDatabase.MetaDatabase.Instance = moMetaDatabase
      Dim oList As List = oFileFormatMetaDatabase._FileFormatMetaGroup_OwnMany.GridList
      Dim oDataRow As DataRow

      For Each oDataRow In oList.DataTable.Rows
        Dim sMetaGroupCode As String = oDataRow("MetaGroupCode")
        Dim nFileFormatMetaGroupID As Integer = System.Convert.ToInt32(SHBS.General.ZeroIfDBNull(oDataRow("FileFormatMetaGroupID")))
        Dim oFileFormatMetaGroup As FileFormatMetaGroup = oFileFormatMetaDatabase.FileFormatMetaGroup_OwnMany.NewInstance

        oFileFormatMetaGroup._MetaGroupCode.Load(sMetaGroupCode)
        oFileFormatMetaGroup.FileFormatMetaGroupID.Load(nFileFormatMetaGroupID)

        Dim sFieldName As String = DTSManager.Constants.Field_TotalLengthMetaGroup & oFileFormatMetaGroup.FileFormatMetaGroupID.Value.ToString()

        If CreateTableYN Then
          AddFileFormatMetaGroupFields(oFileFormatMetaGroup, StringBuilder)

          StringBuilder.Append(sFieldName & " int, " & vbNewLine)
        Else
          StringBuilder.Append(sFieldName & ", ")
        End If
      Next
    End Sub

    Private Sub AddFileFormatMetaGroupFields(ByVal FileFormatMetaGroup As FileFormatMetaGroup, ByRef StringBuilder As StringBuilder)
      Dim sFieldName As String = DTSManager.Constants.Field_RecordChecksumMetaGroup & FileFormatMetaGroup.FileFormatMetaGroupID.Value.ToString()
      Dim oList As List = FileFormatMetaGroup._FileFormatMetaGroupField_OwnMany.GridList
      Dim oDataRow As DataRow
      Dim sChecksumFields As String = ""

      oList.AndFilters.Add(New ListFilter("IncludeInChecksumYN", "IncludeInChecksumYN", True, DataType.Boolean, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      oList.OrderBy("ChecksumSequenceNum")
      For Each oDataRow In oList.DataTable.Rows
        Dim sMetaGroupFieldTypeInd As String = oDataRow("MetaGroupFieldTypeInd")
        Dim sAssignedValue As String = SHBS.General.EmptyStringIfDBNull(oDataRow("AssignedValue"))

        Select Case sMetaGroupFieldTypeInd
          Case XDSAdministrationBL.Constants.MetaGroupField.Types.FileFormatField
            sChecksumFields &= moLoaderEngineManager.ParseFileFormatFieldDesc(oDataRow("FileFormatFieldDesc")) & ", "
          Case XDSAdministrationBL.Constants.MetaGroupField.Types.AssignedValue
            sChecksumFields &= "'" & sAssignedValue & "', "
        End Select
      Next
      If sChecksumFields.Length > 0 Then
        sChecksumFields = sChecksumFields.Substring(0, sChecksumFields.Length - 2)
      End If
      StringBuilder.Append(sFieldName & " as (checksum(" & sChecksumFields & ")), " & vbNewLine)
    End Sub

    Private Sub GetFileFormatIndexes(ByVal FileFormatFieldTypeInd As String, ByRef StringBuilder As StringBuilder)
      Dim oList As List = moLoaderEngineManager.FileFormat._FileFormatField_OwnMany.GridList
      Dim oDataRow As DataRow

      oList.AndFilters.Add(New ListFilter("FileFormatFieldTypeInd", "FileFormatFieldTypeInd", FileFormatFieldTypeInd, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      For Each oDataRow In oList.DataTable.Rows
        Dim sFieldName As String = moLoaderEngineManager.ParseFileFormatFieldDesc(oDataRow("FileFormatFieldDesc"))

        StringBuilder.Append(sFieldName & ", " & vbNewLine)
      Next
    End Sub
#End Region
  End Class
End Namespace
