Imports System
Imports System.IO
Imports System.Text
Imports sembleWare.Runtime
Imports XDSAdministrationBL

Namespace SQLManager
  Friend Class RepositoryTable
#Region " Constants"
    Private Const msREPOSITORY As String = "Repository"
#End Region

#Region " Variables"
    Private moMetaDatabase As MetaDatabase
    Private moLoaderEngineManager As LoaderEngineManager.Manager
    Private moApplicationSettings As ApplicationSettings
#End Region

#Region " Properties"
    Public ReadOnly Property ApplicationSettings() As ApplicationSettings
      Get
        Return moApplicationSettings
      End Get
    End Property

    Public ReadOnly Property TableName() As String
      Get
        Return RepositoryTable.TableName(moLoaderEngineManager.FileFormat, moLoaderEngineManager.FileFormatDeploymentID)
      End Get
    End Property

    Public ReadOnly Property TableIndexName() As String
      Get
        Return RepositoryTable.TableIndexName(moMetaDatabase, moLoaderEngineManager.FileFormat, moLoaderEngineManager.FileFormatDeploymentID)
      End Get
    End Property
#End Region

#Region " Shared Properties"
    Public Shared ReadOnly Property TableName(ByVal FileFormat As FileFormat, ByVal FileFormatDeploymentID As Integer) As String
      Get
        Return msREPOSITORY & FileFormat.FileFormatCode.Value & "_" & FileFormatDeploymentID.ToString()
      End Get
    End Property

    Public Shared ReadOnly Property TableIndexName(ByVal MetaDatabase As MetaDatabase, ByVal FileFormat As FileFormat, ByVal FileFormatDeploymentID As Integer) As String
      Get
        Return "IX_" & RepositoryTable.TableName(FileFormat, FileFormatDeploymentID) & "_" & FileFormatDeploymentID.ToString()
      End Get
    End Property

    Public Shared ReadOnly Property ConnectionString(ByVal MetaDatabase As MetaDatabase) As String
      Get
        Dim oEncryption As sembleWare.Security.Encryption = New sembleWare.Security.Encryption(XDSAdministrationBL.Constants.PasswordHashString)
        Dim sConnectionString As String = "data source=" & MetaDatabase.ServerName.Value
        sConnectionString &= ";initial catalog=" & MetaDatabase.DatabaseName.Value
        sConnectionString &= ";persist security info=False"
        sConnectionString &= ";user id = " & MetaDatabase.Username.Value
        sConnectionString &= ";pwd=" & oEncryption.Decrypt(MetaDatabase.Password.Value)
        sConnectionString &= ";packet size=4096"

        Return sConnectionString
      End Get
    End Property
#End Region

#Region " Constructors"
    Public Sub New(ByVal MetaDatabase As MetaDatabase, ByVal LoaderEngineManager As LoaderEngineManager.Manager)
      moMetaDatabase = MetaDatabase
      moLoaderEngineManager = LoaderEngineManager

      If Not moMetaDatabase Is Nothing Then
        Dim sConnectionString As String = RepositoryTable.ConnectionString(moMetaDatabase)
        moApplicationSettings = New ApplicationSettings(New System.Data.SqlClient.SqlConnection(sConnectionString), New sembleWare.Runtime.SQLData.SQLQueryBuilder, 1800)
      End If
    End Sub
#End Region

#Region " Methods"
    Public Sub DropTable()
      If Not moApplicationSettings Is Nothing Then
        Dim oString As StringBuilder = New StringBuilder

        oString.Append("if exists (select * from dbo.sysobjects where id = object_id(N'[" & Me.TableName & "]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbNewLine)
        oString.Append("drop table [" & Me.TableName & "]" & vbNewLine)

        moApplicationSettings.ActionQuery(oString.ToString())
      End If
    End Sub

    Public Sub CreateTable()
      If Not moApplicationSettings Is Nothing Then
        Dim oString As StringBuilder = New StringBuilder

        Try
          oString.Append("create table " & Me.TableName & " (" & vbNewLine)
          oString.Append(DTSManager.Constants.Field_RecordID & " int identity (1, 1) not null," & vbNewLine)
          oString.Append(DTSManager.Constants.Field_LoaderID & " int not null" & vbNewLine)

          AddFileFormatFields(XDSAdministrationBL.Constants.FileFormatField.Types.File, oString)

          oString.Append(")" & vbNewLine)
          moApplicationSettings.ActionQuery(oString.ToString())

          oString = New StringBuilder
          oString.Append("alter table [" & Me.TableName & "] add" & vbNewLine)
          oString.Append("constraint [PK_" & Me.TableName & "] primary key clustered" & vbNewLine)
          oString.Append("(" & vbNewLine)
          oString.Append(DTSManager.Constants.Field_RecordID & vbNewLine)
          oString.Append(")")
          moApplicationSettings.ActionQuery(oString.ToString())

          oString = New StringBuilder
          oString.Append("create index [" & Me.TableIndexName & "] on [" & TableName & "]" & vbNewLine)
          oString.Append("(" & vbNewLine)
          oString.Append(DTSManager.Constants.Field_LoaderID & vbNewLine)
          oString.Append(")")
          moApplicationSettings.ActionQuery(oString.ToString())
        Catch oException As Exception
          Dim sErrorString As String = "Error creating repository table for File Format: " & moLoaderEngineManager.FileFormat.FileFormatCode.Value & " - " & moLoaderEngineManager.FileFormat.FileFormatDesc.Value & "."
          Throw New Exception(sErrorString & vbNewLine & oException.Message, New Exception(oString.ToString(), oException))
        End Try
      End If
    End Sub

    Public Function RecordCount(ByVal LoaderID As Long) As Long
      Dim oDataRow As DataRow
      Dim oQuery As StringBuilder = New StringBuilder

      oQuery.Append("select count(" & DTSManager.Constants.Field_RecordID & ") ")
      oQuery.Append("  from " & Me.TableName)
      oQuery.Append(" where " & DTSManager.Constants.Field_LoaderID & " = " & moApplicationSettings.QueryBuilder.ToSQL(LoaderID))

      For Each oDataRow In moApplicationSettings.ResultQuery(oQuery.ToString()).Rows
        Return SHBS.General.ZeroIfDBNull(oDataRow(0))
      Next
      Return 0
    End Function
#End Region

#Region " Helper Functions"
    Private Sub AddFileFormatFields(ByVal FileFormatFieldTypeInd As String, ByRef StringBuilder As StringBuilder)
      Dim oList As List = moLoaderEngineManager.FileFormat._FileFormatField_OwnMany.GridList
      Dim oDataRow As DataRow

      oList.AndFilters.Add(New ListFilter("FileFormatFieldTypeInd", "FileFormatFieldTypeInd", FileFormatFieldTypeInd, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      For Each oDataRow In oList.DataTable.Rows
        Dim sFieldName As String = moLoaderEngineManager.ParseFileFormatFieldDesc(oDataRow("FileFormatFieldDesc"))
        Dim nFieldLength As Integer = System.Convert.ToInt32(SHBS.General.ZeroIfDBNull(oDataRow("FieldLength")))

        nFieldLength = IIf(nFieldLength = 0, 250, nFieldLength)
        StringBuilder.Append(", " & sFieldName & " varchar (" & nFieldLength & ") null" & vbNewLine)
      Next
    End Sub
#End Region
  End Class
End Namespace