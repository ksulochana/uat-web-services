Imports System
Imports System.IO
Imports System.Text
Imports sembleWare.Runtime
Imports XDSAdministrationBL

Namespace SQLManager
  Friend Class LoaderDataUndo
#Region " Variables"
    Private moMetaDatabase As MetaDatabase
    Private moLoaderEngineManager As LoaderEngineManager.Manager
    Private moFileFormatDeployment As FileFormatDeployment
#End Region

#Region " Properties"
    Public ReadOnly Property StoredProcedureName() As String
      Get
        Return "sp" & Me.StagingTableName & "_DataUndo"
      End Get
    End Property

    Public ReadOnly Property DatabaseName() As String
      Get
        Return moMetaDatabase.DatabaseName.Value
      End Get
    End Property

    Private ReadOnly Property StagingTableName() As String
      Get
        Return SQLManager.StagingTable.TableName(moMetaDatabase, moLoaderEngineManager.FileFormat, moLoaderEngineManager.FileFormatDeploymentID)
      End Get
    End Property
#End Region

#Region " Constructors"
    Public Sub New(ByVal MetaDatabase As MetaDatabase, ByVal FileFormatDeployment As FileFormatDeployment, ByVal LoaderEngineManager As LoaderEngineManager.Manager)
      moMetaDatabase = MetaDatabase
      moLoaderEngineManager = LoaderEngineManager
      moFileFormatDeployment = FileFormatDeployment
    End Sub
#End Region

#Region " Methods"
    Public Sub DropStoredProcedure()
      Dim oString As StringBuilder = New StringBuilder

      oString.Append("if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[" & Me.StoredProcedureName & "]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbNewLine)
      oString.Append("drop procedure [dbo].[" & Me.StoredProcedureName & "]" & vbNewLine)

      moLoaderEngineManager.ApplicationSettings.ActionQuery(oString.ToString())
    End Sub

    Public Sub CreateStoredProcedure()
      Dim oStringBuilder As StringBuilder = New StringBuilder
      Dim oFileFormatDeploymentMetaLoaderProcedure As FileFormatDeploymentMetaLoaderProcedure = FileFormatDeploymentMetaLoaderProcedure.GetDataUndo(moFileFormatDeployment, moLoaderEngineManager.FileFormat, moMetaDatabase, moLoaderEngineManager.ApplicationSettings)
      Dim oStringReader As StringReader = New StringReader(oFileFormatDeploymentMetaLoaderProcedure.TemplateDetails.Value)

      Try
        Dim sLine As String = oStringReader.ReadLine()

        While Not sLine Is Nothing
          If Not sLine.IndexOf(Constants.StoredProcedureName) = -1 Then
            sLine = sLine.Replace(Constants.StoredProcedureName, Me.StoredProcedureName)
          End If

          If Not sLine.IndexOf(Constants.DatabaseName) = -1 Then
            sLine = sLine.Replace(Constants.DatabaseName, Me.DatabaseName)
          End If

          If Not sLine.IndexOf(Constants.FileFormat_MetaDatabaseCode) = -1 Then
            sLine = sLine.Replace(Constants.FileFormat_MetaDatabaseCode, oFileFormatDeploymentMetaLoaderProcedure._MetaDatabaseCode.Value)
          End If

          oStringBuilder.Append(sLine & vbNewLine)

          sLine = oStringReader.ReadLine()
        End While
        oStringReader.Close()

        moLoaderEngineManager.ApplicationSettings.ActionQuery(oStringBuilder.ToString())
      Catch oException As Exception
        Dim sErrorString As String = "Error deploying Loader Procedure Data Undo of File Format: " & moLoaderEngineManager.FileFormat.FileFormatCode.Value & " - " & moLoaderEngineManager.FileFormat.FileFormatDesc.Value
        Throw New Exception(sErrorString & vbNewLine & oException.Message, New Exception(oStringBuilder.ToString(), oException))
      End Try
    End Sub
#End Region
  End Class
End Namespace