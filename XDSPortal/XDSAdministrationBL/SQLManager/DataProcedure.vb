Imports System
Imports System.IO
Imports System.Text
Imports sembleWare.Runtime
Imports XDSAdministrationBL

Namespace SQLManager
  Friend Class DataProcedure
#Region " Variables"
    Private moMetaDatabase As MetaDatabase
    Private moLoaderEngineManager As LoaderEngineManager.Manager
#End Region

#Region " Properties"
    Public ReadOnly Property FunctionName(ByVal DataProcedureID As String, ByVal DataProcedureTypeInd As String) As String
      Get
        Dim sFunctionName As String = "fn"
        Select Case DataProcedureTypeInd
          Case XDSAdministrationBL.Constants.DataProcedure.Types.Deletion
            sFunctionName &= "Deletion"
          Case XDSAdministrationBL.Constants.DataProcedure.Types.Conversion
            sFunctionName &= "Conversion"
          Case XDSAdministrationBL.Constants.DataProcedure.Types.RequiredValidation
            sFunctionName &= "RequiredValidation"
          Case XDSAdministrationBL.Constants.DataProcedure.Types.NonRequiredValidation
            sFunctionName &= "NonRequiredValidation"
          Case XDSAdministrationBL.Constants.DataProcedure.Types.Calculation
            sFunctionName &= "Calculation"
          Case XDSAdministrationBL.Constants.DataProcedure.Types.MatchingEvaluation
            sFunctionName &= "MatchingEvaluation"
        End Select
        If Not moMetaDatabase Is Nothing Then
          sFunctionName &= moMetaDatabase.MetaDatabaseCode.Value
        End If
        sFunctionName &= "_" & moLoaderEngineManager.FileFormatDeploymentID.ToString() & "_" & DataProcedureID
        Return sFunctionName
      End Get
    End Property
#End Region

#Region " Constructors"
    Public Sub New(ByVal MetaDatabase As MetaDatabase, ByVal LoaderEngineManager As LoaderEngineManager.Manager)
      moMetaDatabase = MetaDatabase
      moLoaderEngineManager = LoaderEngineManager
    End Sub
#End Region

#Region " Methods"
    Public Sub DropFunctions()
      Dim oXDSAdministrationRoot As XDSAdministrationRoot = New XDSAdministrationRoot(moLoaderEngineManager.ApplicationSettings)
      Dim oList As List = oXDSAdministrationRoot._DataProcedure_OwnMany.GridList
      Dim oDataRow As DataRow

      If moMetaDatabase Is Nothing Then
        oList.AndFilters.Add(New ListFilter("AppliesToInd", "AppliesToInd", XDSAdministrationBL.Constants.DataProcedure.AppliesToTypes.All, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      Else
        oList.AndFilters.Add(New ListFilter("AppliesToInd", "AppliesToInd", XDSAdministrationBL.Constants.DataProcedure.AppliesToTypes.MetaDatabase, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
        oList.LimitByRelatedPart(moMetaDatabase, "MetaDatabase")
      End If
      For Each oDataRow In oList.DataTable.Rows
        Dim sDataProcedureID As String = oDataRow("DataProcedureID")
        Dim sDataProcedureTypeInd As String = oDataRow("DataProcedureTypeInd")

        DropFunction(sDataProcedureID, sDataProcedureTypeInd)
      Next
    End Sub

    Public Sub CreateFunctions()
      Dim oXDSAdministrationRoot As XDSAdministrationRoot = New XDSAdministrationRoot(moLoaderEngineManager.ApplicationSettings)
      Dim oList As List = oXDSAdministrationRoot._DataProcedure_OwnMany.GridList
      Dim oDataRow As DataRow

      If moMetaDatabase Is Nothing Then
        oList.AndFilters.Add(New ListFilter("AppliesToInd", "AppliesToInd", XDSAdministrationBL.Constants.DataProcedure.AppliesToTypes.All, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      Else
        oList.AndFilters.Add(New ListFilter("AppliesToInd", "AppliesToInd", XDSAdministrationBL.Constants.DataProcedure.AppliesToTypes.MetaDatabase, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
        oList.LimitByRelatedPart(moMetaDatabase, "MetaDatabase")
      End If
      For Each oDataRow In oList.DataTable.Rows
        Dim sDataProcedureID As String = oDataRow("DataProcedureID")
        Dim sDataProcedureTypeInd As String = oDataRow("DataProcedureTypeInd")
        Dim sTemplateDetails As String = oDataRow("TemplateDetails")

        DropFunction(sDataProcedureID, sDataProcedureTypeInd)
        CreateFunction(sDataProcedureID, sDataProcedureTypeInd, sTemplateDetails)
      Next
    End Sub
#End Region

#Region " Helper Functions"
    Private Sub DropFunction(ByVal DataProcedureID As String, ByVal DataProcedureTypeInd As String)
      Dim oString As StringBuilder = New StringBuilder

      oString.Append("if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[" & Me.FunctionName(DataProcedureID, DataProcedureTypeInd) & "]') and xtype in (N'FN', N'IF', N'TF'))" & vbNewLine)
      oString.Append("drop function [dbo].[" & Me.FunctionName(DataProcedureID, DataProcedureTypeInd) & "]" & vbNewLine)

      moLoaderEngineManager.ApplicationSettings.ActionQuery(oString.ToString())
    End Sub

    Private Sub CreateFunction(ByVal DataProcedureID As String, ByVal DataProcedureTypeInd As String, ByVal TemplateDetails As String)
      Dim oStringBuilder As StringBuilder = New StringBuilder
      Dim oStringReader As StringReader = New StringReader(TemplateDetails)

      Try
        Dim sLine As String = oStringReader.ReadLine()

        While Not sLine Is Nothing
          Dim bAppendYN As Boolean = True

          If Not sLine.IndexOf(Constants.FunctionName) = -1 Then
            sLine = sLine.Replace(Constants.FunctionName, Me.FunctionName(DataProcedureID, DataProcedureTypeInd))
          End If

          oStringBuilder.Append(sLine & vbNewLine)

          sLine = oStringReader.ReadLine()
        End While
        oStringReader.Close()

        moLoaderEngineManager.ApplicationSettings.ActionQuery(oStringBuilder.ToString())
      Catch oException As Exception
        Dim sErrorString As String = "Error deploying Data Procedure: " & Me.FunctionName(DataProcedureID, DataProcedureTypeInd) & "."
        Throw New Exception(sErrorString & vbNewLine & oException.Message, New Exception(oStringBuilder.ToString(), oException))
      End Try
    End Sub
#End Region
  End Class

End Namespace