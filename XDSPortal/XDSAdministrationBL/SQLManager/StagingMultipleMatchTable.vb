Imports System
Imports System.IO
Imports System.Text
Imports sembleWare.Runtime
Imports XDSAdministrationBL

Namespace SQLManager
  Friend Class StagingMultipleMatchTable
#Region " Constants"
    Private Const msSTAGING As String = "Staging"
#End Region

#Region " Variables"
    Private moMetaDatabase As MetaDatabase
    Private moLoaderEngineManager As LoaderEngineManager.Manager
#End Region

#Region " Properties"
    Public ReadOnly Property TableName() As String
      Get
        Return StagingMultipleMatchTable.TableName(moMetaDatabase, moLoaderEngineManager.FileFormat, moLoaderEngineManager.FileFormatDeploymentID)
      End Get
    End Property

    Public ReadOnly Property TableName(ByVal LoaderID As Long) As String
      Get
        Return StagingMultipleMatchTable.TableName(moMetaDatabase, moLoaderEngineManager.FileFormat, moLoaderEngineManager.FileFormatDeploymentID, LoaderID)
      End Get
    End Property
#End Region

#Region " Shared Properties"
    Public Shared ReadOnly Property TableName(ByVal MetaDatabase As MetaDatabase, ByVal FileFormat As FileFormat, ByVal FileFormatDeploymentID As Integer) As String
      Get
        Return TableName(MetaDatabase, FileFormat, FileFormatDeploymentID, 0)
      End Get
    End Property

    Public Shared ReadOnly Property TableName(ByVal MetaDatabase As MetaDatabase, ByVal FileFormat As FileFormat, ByVal FileFormatDeploymentID As Integer, ByVal LoaderID As Long) As String
      Get
        Dim sTableName As String = msSTAGING & MetaDatabase.MetaDatabaseCode.Value & FileFormat.FileFormatCode.Value & "_" & FileFormatDeploymentID.ToString() & "MultipleMatch"
        If Not LoaderID = 0 Then
          sTableName &= "_Loader" & LoaderID.ToString()
        End If
        Return sTableName
      End Get
    End Property
#End Region

#Region " Constructors"
    Public Sub New(ByVal MetaDatabase As MetaDatabase, ByVal LoaderEngineManager As LoaderEngineManager.Manager)
      moMetaDatabase = MetaDatabase
      moLoaderEngineManager = LoaderEngineManager
    End Sub
#End Region

#Region " Methods"
    Public Sub DropTable()
      DropTable(0)
    End Sub

    Public Sub DropTable(ByVal LoaderID As Long)
      Dim oDataRow As DataRow
      Dim oString As StringBuilder = New StringBuilder
      Dim sTableName As String = Me.TableName(LoaderID)

      If LoaderID = 0 Then
        sTableName = Me.TableName()
      End If
      oString.Append("if exists (select * from dbo.sysobjects where id = object_id(N'[" & sTableName & "]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbNewLine)
      oString.Append("drop table [" & sTableName & "]" & vbNewLine)

      moLoaderEngineManager.ApplicationSettings.ActionQuery(oString.ToString())
    End Sub

    Public Sub CreateTable()
      Me.CreateTable(0)
    End Sub

    Public Sub CreateTable(ByVal LoaderID As Long)
      Dim oString As StringBuilder = New StringBuilder
      Dim sTableName As String = Me.TableName(LoaderID)

      Try
        If LoaderID = 0 Then
          sTableName = Me.TableName()
        End If
        oString.Append("create table " & sTableName & " (" & vbNewLine)
        oString.Append(DTSManager.Constants.Field_RecordID & " int not null," & vbNewLine)

        AddFileFormatFields(XDSAdministrationBL.Constants.FileFormatField.Types.System, True, oString)

        oString.Append(")")
        moLoaderEngineManager.ApplicationSettings.ActionQuery(oString.ToString())

        oString = New StringBuilder
        oString.Append("alter table [" & sTableName & "] add" & vbNewLine)
        oString.Append("constraint [PK_" & sTableName & "] primary key clustered" & vbNewLine)
        oString.Append("(" & vbNewLine)
        oString.Append(DTSManager.Constants.Field_RecordID & ", " & vbNewLine)
        AddFileFormatFields(XDSAdministrationBL.Constants.FileFormatField.Types.System, False, oString)

        If oString.Length > 0 Then
          oString = oString.Remove(oString.Length - 2, 2)
        End If
        oString.Append(")")

        moLoaderEngineManager.ApplicationSettings.ActionQuery(oString.ToString())
      Catch oException As Exception
        Dim sErrorString As String = "Error creating staging multiple match table for File Format: " & moLoaderEngineManager.FileFormat.FileFormatCode.Value & " - " & moLoaderEngineManager.FileFormat.FileFormatDesc.Value & "."
        Throw New Exception(sErrorString & vbNewLine & oException.Message, New Exception(oString.ToString(), oException))
      End Try
    End Sub

    Public Function GetFields() As String
      Dim oString As StringBuilder = New StringBuilder

      AddFileFormatFields(XDSAdministrationBL.Constants.FileFormatField.Types.System, False, oString)
      
      If oString.Length > 0 Then
        oString = oString.Remove(oString.Length - 2, 2)
      End If

      Return oString.ToString()
    End Function
#End Region

#Region " Helper Functions"
    Private Sub AddFileFormatFields(ByVal FileFormatFieldTypeInd As String, ByVal CreateTableYN As Boolean, ByRef StringBuilder As StringBuilder)
      Dim oList As List = moLoaderEngineManager.FileFormat._FileFormatField_OwnMany.GridList
      Dim oDataRow As DataRow

      oList.AndFilters.Add(New ListFilter("FileFormatFieldTypeInd", "FileFormatFieldTypeInd", FileFormatFieldTypeInd, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      For Each oDataRow In oList.DataTable.Rows
        Dim sFieldName As String = moLoaderEngineManager.ParseFileFormatFieldDesc(oDataRow("FileFormatFieldDesc"))

        If CreateTableYN Then
          Dim nFieldLength As Integer = System.Convert.ToInt32(SHBS.General.ZeroIfDBNull(oDataRow("FieldLength")))

          nFieldLength = IIf(nFieldLength = 0, 250, nFieldLength)
          StringBuilder.Append(sFieldName & " varchar (" & nFieldLength & ") not null, " & vbNewLine)
        Else
          StringBuilder.Append(sFieldName & ", ")
        End If
      Next
    End Sub
#End Region
  End Class
End Namespace