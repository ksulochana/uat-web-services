Imports System
Imports System.IO
Imports System.Text
Imports sembleWare.Runtime
Imports XDSAdministrationBL

Namespace SQLManager
  Friend Class LoaderDataValidation
#Region " Variables"
    Private moMetaDatabase As MetaDatabase
    Private moLoaderEngineManager As LoaderEngineManager.Manager
    Private moFileFormatDeployment As FileFormatDeployment
    Private mlLoaderID As Long
#End Region

#Region " Properties"
    Public ReadOnly Property StoredProcedureName() As String
      Get
        Return "sp" & Me.StagingTableName & "_DataValidation"
      End Get
    End Property

    Public ReadOnly Property DatabaseName() As String
      Get
        Return moMetaDatabase.DatabaseName.Value
      End Get
    End Property

    Private ReadOnly Property StagingTableName() As String
      Get
        Return SQLManager.StagingTable.TableName(moMetaDatabase, moLoaderEngineManager.FileFormat, moLoaderEngineManager.FileFormatDeploymentID, mlLoaderID)
      End Get
    End Property
#End Region

#Region " Constructors"
    Public Sub New(ByVal MetaDatabase As MetaDatabase, ByVal FileFormatDeployment As FileFormatDeployment, ByVal LoaderEngineManager As LoaderEngineManager.Manager, ByVal LoaderID As Long)
      moMetaDatabase = MetaDatabase
      moLoaderEngineManager = LoaderEngineManager
      moFileFormatDeployment = FileFormatDeployment
      mlLoaderID = LoaderID
    End Sub
#End Region

#Region " Methods"
    Public Sub DropStoredProcedure()
      Dim oString As StringBuilder = New StringBuilder

      oString.Append("if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[" & Me.StoredProcedureName & "]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbNewLine)
      oString.Append("drop procedure [dbo].[" & Me.StoredProcedureName & "]" & vbNewLine)

      moLoaderEngineManager.ApplicationSettings.ActionQuery(oString.ToString())
    End Sub

    Public Sub CreateStoredProcedure()
      Dim oStringBuilder As StringBuilder = New StringBuilder
      Dim oFileFormatDeploymentMetaLoaderProcedure As FileFormatDeploymentMetaLoaderProcedure = FileFormatDeploymentMetaLoaderProcedure.GetDataValidation(moFileFormatDeployment, moLoaderEngineManager.FileFormat, moMetaDatabase, moLoaderEngineManager.ApplicationSettings)
      Dim oStringReader As StringReader = New StringReader(oFileFormatDeploymentMetaLoaderProcedure.TemplateDetails.Value)

      Try
        Dim sLine As String = oStringReader.ReadLine()

        While Not sLine Is Nothing
          Dim bAppendYN As Boolean = True

          If Not sLine.IndexOf(Constants.StoredProcedureName) = -1 Then
            sLine = sLine.Replace(Constants.StoredProcedureName, Me.StoredProcedureName)
          End If

          If Not sLine.IndexOf(Constants.DatabaseName) = -1 Then
            sLine = sLine.Replace(Constants.DatabaseName, Me.DatabaseName)
          End If

          If Not sLine.IndexOf(Constants.Staging_TableName) = -1 Then
            sLine = sLine.Replace(Constants.Staging_TableName, Me.StagingTableName)
          End If

          If Not sLine.IndexOf(Constants.DuplicateStaging_Fields) = -1 Then
            sLine = sLine.Replace(Constants.DuplicateStaging_Fields, GetDuplicateTableFields(TableTypes.Staging))
          End If

          If Not sLine.IndexOf(Constants.FileFormat_MetaDatabaseCode) = -1 Then
            sLine = sLine.Replace(Constants.FileFormat_MetaDatabaseCode, oFileFormatDeploymentMetaLoaderProcedure._MetaDatabaseCode.Value)
          End If

          If Not sLine.IndexOf(Constants.DuplicateTableFieldRepeater_Start) = -1 Then
            ReplaceDuplicateRepeaterTag(oStringReader, Constants.DuplicateTableFieldRepeater_End, oStringBuilder)
            bAppendYN = False
          End If

          If Not sLine.IndexOf(Constants.DeletionRepeater_Start) = -1 Then
            ReplaceDataProcedureRepeaterTag(oStringReader, Constants.DeletionRepeater_End, XDSAdministrationBL.Constants.DataProcedure.Types.Deletion, oStringBuilder)
            bAppendYN = False
          End If

          If Not sLine.IndexOf(Constants.ConversionRepeater_Start) = -1 Then
            ReplaceDataProcedureRepeaterTag(oStringReader, Constants.ConversionRepeater_End, XDSAdministrationBL.Constants.DataProcedure.Types.Conversion, oStringBuilder)
            bAppendYN = False
          End If

          If Not sLine.IndexOf(Constants.RequiredValidationInvalidFileRepeater_Start) = -1 Then
            ReplaceDataProcedureRepeaterTag(oStringReader, Constants.RequiredValidationInvalidFileRepeater_End, XDSAdministrationBL.Constants.DataProcedure.Types.RequiredValidation, oStringBuilder)
            bAppendYN = False
          End If

          If Not sLine.IndexOf(Constants.RequiredValidationRepeater_Start) = -1 Then
            ReplaceDataProcedureRepeaterTag(oStringReader, Constants.RequiredValidationRepeater_End, XDSAdministrationBL.Constants.DataProcedure.Types.RequiredValidation, oStringBuilder)
            bAppendYN = False
          End If

          If Not sLine.IndexOf(Constants.NonRequiredValidationInvalidFileRepeater_Start) = -1 Then
            ReplaceDataProcedureRepeaterTag(oStringReader, Constants.NonRequiredValidationInvalidFileRepeater_End, XDSAdministrationBL.Constants.DataProcedure.Types.NonRequiredValidation, oStringBuilder)
            bAppendYN = False
          End If

          If Not sLine.IndexOf(Constants.NonRequiredValidationRepeater_Start) = -1 Then
            ReplaceDataProcedureRepeaterTag(oStringReader, Constants.NonRequiredValidationRepeater_End, XDSAdministrationBL.Constants.DataProcedure.Types.NonRequiredValidation, oStringBuilder)
            bAppendYN = False
          End If

          If Not sLine.IndexOf(Constants.DataStatisticRepeater_Start) = -1 Then
            ReplaceDataProcedureRepeaterTag(oStringReader, Constants.DataStatisticRepeater_End, XDSAdministrationBL.Constants.DataProcedure.Types.DataStatistic, oStringBuilder)
            bAppendYN = False
          End If

          If Not sLine.IndexOf(Constants.CalculationRepeater_Start) = -1 Then
            ReplaceDataProcedureRepeaterTag(oStringReader, Constants.CalculationRepeater_End, XDSAdministrationBL.Constants.DataProcedure.Types.Calculation, oStringBuilder)
            bAppendYN = False
          End If

          If Not sLine.IndexOf(Constants.MetaGroupRepeater_Start) = -1 Then
            ReplaceMetaGroupRepeaterTag(oStringReader, Constants.MetaGroupRepeater_End, oStringBuilder)
            bAppendYN = False
          End If

          If bAppendYN Then
            oStringBuilder.Append(sLine & vbNewLine)
          End If

          sLine = oStringReader.ReadLine()
        End While
        oStringReader.Close()
      
        moLoaderEngineManager.ApplicationSettings.ActionQuery(oStringBuilder.ToString())
      Catch oException As Exception
        Dim sErrorString As String = "Error deploying Loader Procedure Data Validation of File Format: " & moLoaderEngineManager.FileFormat.FileFormatCode.Value & " - " & moLoaderEngineManager.FileFormat.FileFormatDesc.Value
        Throw New Exception(sErrorString & vbNewLine & oException.Message, New Exception(oStringBuilder.ToString(), oException))
      End Try
    End Sub
#End Region

#Region " Helper Functions"
    Private Sub ReplaceDataProcedureRepeaterTag(ByVal StringReader As StringReader, ByVal EndRepeaterTag As String, ByVal DataProcedureTypeInd As String, ByRef StringBuilder As StringBuilder)
      Dim oList As List = moLoaderEngineManager.FileFormat._FileFormatMetaDataProcedure_OwnMany.GridList
      Dim oDataRow
      Dim oRepeaterArray As ArrayList = New ArrayList

      Dim sLine As String = StringReader.ReadLine()
      While Not sLine Is Nothing
        If Not sLine.IndexOf(EndRepeaterTag) = -1 Then
          Exit While
        End If
        oRepeaterArray.Add(sLine)
        sLine = StringReader.ReadLine()
      End While

      Dim oDataProcedure As DataProcedure = New DataProcedure(Nothing, moLoaderEngineManager)
      oList.AndFilters.Add(New ListFilter("DataProcedureTypeInd", "DataProcedureTypeInd", DataProcedureTypeInd, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      oList.AndFilters.Add(New ListFilter("AppliesToInd", "AppliesToInd", XDSAdministrationBL.Constants.DataProcedure.AppliesToTypes.All, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      Select Case EndRepeaterTag
        Case Constants.RequiredValidationInvalidFileRepeater_End, Constants.NonRequiredValidationInvalidFileRepeater_End
          oList.AndFilters.Add(New ListFilter("ValidationReportInInvalidFileYN", "ValidationReportInInvalidFileYN", True, DataType.Boolean, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      End Select
      For Each oDataRow In oList.DataTable.Rows
        AddDataProcedure(oDataProcedure, DataProcedureTypeInd, oDataRow, oRepeaterArray, StringBuilder)
      Next

      oDataProcedure = New DataProcedure(moMetaDatabase, moLoaderEngineManager)
      oList = moLoaderEngineManager.FileFormat._FileFormatMetaDataProcedure_OwnMany.GridList
      oList.AndFilters.Add(New ListFilter("DataProcedureTypeInd", "DataProcedureTypeInd", DataProcedureTypeInd, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      oList.AndFilters.Add(New ListFilter("AppliesToInd", "AppliesToInd", XDSAdministrationBL.Constants.DataProcedure.AppliesToTypes.MetaDatabase, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      oList.LimitByRelatedPart(moMetaDatabase, "MetaDatabase")
      Select Case EndRepeaterTag
        Case Constants.RequiredValidationInvalidFileRepeater_End, Constants.NonRequiredValidationInvalidFileRepeater_End
          oList.AndFilters.Add(New ListFilter("ValidationReportInInvalidFileYN", "ValidationReportInInvalidFileYN", True, DataType.Boolean, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      End Select
      For Each oDataRow In oList.DataTable.Rows
        AddDataProcedure(oDataProcedure, DataProcedureTypeInd, oDataRow, oRepeaterArray, StringBuilder)
      Next
    End Sub

    Private Sub AddDataProcedure(ByVal DataProcedure As DataProcedure, ByVal DataProcedureTypeInd As String, ByVal DataRow As DataRow, ByVal RepeaterArray As ArrayList, ByRef StringBuilder As StringBuilder)
      Dim sString As String

      For Each sString In RepeaterArray
        If Not sString.IndexOf(Constants.Staging_TableName) = -1 Then
          sString = sString.Replace(Constants.Staging_TableName, Me.StagingTableName)
        End If

        If Not sString.IndexOf(Constants.FileFormatField_FileFormatFieldID) = -1 Then
          sString = sString.Replace(Constants.FileFormatField_FileFormatFieldID, System.Convert.ToInt32(DataRow("FileFormatFieldID")))
        End If

        If Not sString.IndexOf(Constants.FileFormatField_FileFormatFieldDesc) = -1 Then
          sString = sString.Replace(Constants.FileFormatField_FileFormatFieldDesc, moLoaderEngineManager.ParseFileFormatFieldDesc(DataRow("FileFormatFieldDesc")))
        End If

        If Not sString.IndexOf(Constants.DataProcedure_MetaDatabaseCode) = -1 Then
          sString = sString.Replace(Constants.DataProcedure_MetaDatabaseCode, DataRow("MetaDatabaseCode"))
        End If

        If Not sString.IndexOf(Constants.DataProcedure_DataProcedureID) = -1 Then
          sString = sString.Replace(Constants.DataProcedure_DataProcedureID, DataRow("DataProcedureID"))
        End If

        If Not sString.IndexOf(Constants.DataProcedure_SQLFunctionName) = -1 Then
          Dim sSQLFunctionName As String = ""

          Select Case DataRow("ParameterTypeInd")
            Case "N"
              sSQLFunctionName = "()"
            Case "S"
              sSQLFunctionName = "(" & moLoaderEngineManager.ParseFileFormatFieldDesc(DataRow("FileFormatFieldDesc")) & ")"
            Case "M"
              Dim oFileFormatMetaDataProcedure As FileFormatMetaDataProcedure = New FileFormatMetaDataProcedureRelationship(moLoaderEngineManager.ApplicationSettings).NewInstance()
              oFileFormatMetaDataProcedure._DataProcedureID.Load(DataRow("DataProcedureID"))
              oFileFormatMetaDataProcedure._FileFormatCode.Load(DataRow("FileFormatCode"))
              oFileFormatMetaDataProcedure._FileFormatFieldID.Load(DataRow("FileFormatFieldID"))
              oFileFormatMetaDataProcedure._MetaDatabaseCode.Load(moMetaDatabase.MetaDatabaseCode.Value)

              Dim oList As List = oFileFormatMetaDataProcedure._FileFormatMetaDataProcedureParameter_OwnMany.GridList
              Dim oDataRow As DataRow

              For Each oDataRow In oList.DataTable.Rows
                sSQLFunctionName &= moLoaderEngineManager.ParseFileFormatFieldDesc(oDataRow("FileFormatFieldDesc")) & ", "
              Next
              If sSQLFunctionName.Length > 0 Then
                sSQLFunctionName = "(" & sSQLFunctionName.Substring(0, sSQLFunctionName.Length - 2) & ")"
              End If
          End Select
          sSQLFunctionName = DataProcedure.FunctionName(DataRow("DataProcedureID"), DataRow("DataProcedureTypeInd")) & sSQLFunctionName
          sString = sString.Replace(Constants.DataProcedure_SQLFunctionName, sSQLFunctionName)
        End If
        StringBuilder.Append(sString & vbNewLine)
      Next
    End Sub

    Private Sub ReplaceDuplicateRepeaterTag(ByVal StringReader As StringReader, ByVal EndRepeaterTag As String, ByRef StringBuilder As StringBuilder)
      Dim oList As List = moLoaderEngineManager.FileFormat._FileFormatField_OwnMany.GridList
      Dim oDataRow
      Dim oRepeaterArray As ArrayList = New ArrayList

      Dim sLine As String = StringReader.ReadLine()
      While Not sLine Is Nothing
        If Not sLine.IndexOf(EndRepeaterTag) = -1 Then
          Exit While
        End If
        oRepeaterArray.Add(sLine)
        sLine = StringReader.ReadLine()
      End While

      oList.AndFilters.Add(New ListFilter("LoaderDataValidationUseForDuplicateYN", "LoaderDataValidationUseForDuplicateYN", True, DataType.Boolean, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      For Each oDataRow In oList.DataTable.Rows
        AddDuplicateField(oDataRow, oRepeaterArray, StringBuilder)
      Next
    End Sub

    Private Sub AddDuplicateField(ByVal DataRow As DataRow, ByVal RepeaterArray As ArrayList, ByRef StringBuilder As StringBuilder)
      Dim sString As String

      For Each sString In RepeaterArray
        If Not sString.IndexOf(Constants.Staging_TableName) = -1 Then
          sString = sString.Replace(Constants.Staging_TableName, Me.StagingTableName)
        End If

        If Not sString.IndexOf(Constants.DuplicateStaging_Field) = -1 Then
          sString = sString.Replace(Constants.DuplicateStaging_Field, moLoaderEngineManager.ParseFileFormatFieldDesc(DataRow("FileFormatFieldDesc")))
        End If

        If Not sString.IndexOf(Constants.DuplicateStaging_FieldDataType) = -1 Then
          Dim nFieldLength As Integer = System.Convert.ToInt32(SHBS.General.ZeroIfDBNull(DataRow("FieldLength")))

          nFieldLength = IIf(nFieldLength = 0, 250, nFieldLength)
          sString = sString.Replace(Constants.DuplicateStaging_FieldDataType, "varchar (" & nFieldLength & ") null")
        End If

        StringBuilder.Append(sString & vbNewLine)
      Next
    End Sub

    Private Function GetDuplicateTableFields(ByVal TableType As TableTypes) As String
      Dim sFields As String = ""

      Select Case TableType
        Case TableTypes.Staging
          Dim oList As List = moLoaderEngineManager.FileFormat._FileFormatField_OwnMany.GridList
          Dim oDataRow As DataRow

          oList.AndFilters.Add(New ListFilter("LoaderDataValidationUseForDuplicateYN", "LoaderDataValidationUseForDuplicateYN", True, DataType.Boolean, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
          For Each oDataRow In oList.DataTable.Rows
            sFields &= moLoaderEngineManager.ParseFileFormatFieldDesc(oDataRow("FileFormatFieldDesc")) & ", "
          Next
          If sFields.Length > 0 Then
            sFields = sFields.Substring(0, sFields.Length - 2)
          End If
      End Select

      Return sFields
    End Function

    Private Sub ReplaceMetaGroupRepeaterTag(ByVal StringReader As StringReader, ByVal EndRepeaterTag As String, ByRef StringBuilder As StringBuilder)
      Dim oXDSAdministrationRoot As XDSAdministrationRoot = moLoaderEngineManager.FileFormat.XDSAdministrationRoot.Instance
      Dim oList As List = oXDSAdministrationRoot._vwDeploymentFileFormatMetaGroup_OwnMany.GridList
      Dim oDataRow As DataRow
      Dim oRepeaterArray As ArrayList = New ArrayList
      Dim lFileFormatMapGroupID As Long = 0

      Dim sLine As String = StringReader.ReadLine()
      While Not sLine Is Nothing
        If Not sLine.IndexOf(EndRepeaterTag) = -1 Then
          Exit While
        End If
        oRepeaterArray.Add(sLine)
        sLine = StringReader.ReadLine()
      End While

      oList.LimitByRelatedPart(moMetaDatabase, "MetaDatabase", True)
      oList.AndFilters.Add(New ListFilter("FileFormatCode", "FileFormatCode", moLoaderEngineManager.FileFormat.FileFormatCode.Value, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      For Each oDataRow In oList.DataTable.Rows
        AddMetaGroup(StringReader, oDataRow, oRepeaterArray, StringBuilder)
      Next
    End Sub

    Private Sub AddMetaGroup(ByVal StringReader As StringReader, ByVal DataRow As DataRow, ByVal RepeaterArray As ArrayList, ByRef StringBuilder As StringBuilder)
      Dim sString As String
      Dim bSkipLineYN As Boolean = False

      For Each sString In RepeaterArray
        If bSkipLineYN Then
          If Not sString.IndexOf(Constants.MetaGroupFieldRepeater_End) = -1 Then
            bSkipLineYN = False
          End If
        Else
          If Not sString.IndexOf(Constants.System_TableName) = -1 Then
            Dim sTableName As String = DataRow("DatabaseName") & ".." & DataRow("MetaTableName")
            sString = sString.Replace(Constants.System_TableName, sTableName)
          End If

          If Not sString.IndexOf(Constants.Staging_TableName) = -1 Then
            sString = sString.Replace(Constants.Staging_TableName, Me.StagingTableName)
          End If

          If Not sString.IndexOf(Constants.MetaGroup_MetaGroupCode) = -1 Then
            sString = sString.Replace(Constants.MetaGroup_MetaGroupCode, DataRow("MetaGroupCode"))
          End If

          If Not sString.IndexOf(Constants.Staging_MetaGroupTotalLengthField) = -1 Then
            sString = sString.Replace(Constants.Staging_MetaGroupTotalLengthField, DTSManager.Constants.Field_TotalLengthMetaGroup & DataRow("FileFormatMetaGroupID"))
          End If

          If Not sString.IndexOf(Constants.MetaGroupFieldRepeater_Start) = -1 Then
            ReplaceMetaGroupFieldRepeaterTag(RepeaterArray, Constants.MetaGroupFieldRepeater_Start, Constants.MetaGroupFieldRepeater_End, StringBuilder, DataRow, "F")
            bSkipLineYN = True
          End If

          If Not bSkipLineYN Then
            StringBuilder.Append(sString & vbNewLine)
          End If
        End If
      Next
    End Sub

    Private Sub ReplaceMetaGroupFieldRepeaterTag(ByVal RepeaterArray As ArrayList, ByVal StartRepeaterTag As String, ByVal EndRepeaterTag As String, ByRef StringBuilder As StringBuilder, ByVal DataRow As DataRow, ByVal MetaGroupFieldTypeInd As String)
      Dim oXDSAdministrationRoot As XDSAdministrationRoot = moLoaderEngineManager.FileFormat.XDSAdministrationRoot.Instance
      Dim oDBList As DBList = oXDSAdministrationRoot._vwDeploymentFileFormatMetaGroupField_OwnMany.GridList
      Dim oDataRow As DataRow
      Dim oRepeaterArray As ArrayList = New ArrayList
      Dim lFileFormatMapGroupID As Long = 0
      Dim sString As String

      Dim bStartYN As Boolean = False
      For Each sString In RepeaterArray
        If bStartYN Then
          If Not sString.IndexOf(EndRepeaterTag) = -1 Then
            Exit For
          End If
          oRepeaterArray.Add(sString)
        End If
        If Not sString.IndexOf(StartRepeaterTag) = -1 Then
          bStartYN = True
        End If
      Next

      oDBList.LimitByRelatedPart(moMetaDatabase, "MetaDatabase", True)
      oDBList.AndFilters.Add(New ListFilter("FileFormatCode", "FileFormatCode", moLoaderEngineManager.FileFormat.FileFormatCode.Value, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      oDBList.AndFilters.Add(New ListFilter("MetaDatabaseCode", "MetaDatabaseCode", DataRow("MetaDatabaseCode"), DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      oDBList.AndFilters.Add(New ListFilter("MetaTableName", "MetaTableName", DataRow("MetaTableName"), DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      oDBList.AndFilters.Add(New ListFilter("FileFormatMetaGroupID", "FileFormatMetaGroupID", DataRow("FileFormatMetaGroupID"), DataType.Integer, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      oDBList.AndFilters.Add(New ListFilter("MetaGroupFieldTypeInd", "MetaGroupFieldTypeInd", MetaGroupFieldTypeInd, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      oDBList.AndFilters.Add(New ListFilter("LoaderDataUpdateUseForInsertYN", "LoaderDataUpdateUseForInsertYN", True, DataType.Boolean, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))

      Select Case MetaGroupFieldTypeInd
        Case "F"
          oDBList.AppendToWhereClause("FileFormatFieldID is not null")
      End Select
      For Each oDataRow In oDBList.DataTable.Rows
        AddMetaGroupField(oDataRow, oRepeaterArray, StringBuilder)
      Next
    End Sub

    Private Sub AddMetaGroupField(ByVal DataRow As DataRow, ByVal RepeaterArray As ArrayList, ByRef StringBuilder As StringBuilder)
      Dim sString As String

      For Each sString In RepeaterArray
        If Not sString.IndexOf(Constants.System_TableName) = -1 Then
          Dim sTableName As String = DataRow("DatabaseName") & ".." & DataRow("MetaTableName")
          sString = sString.Replace(Constants.System_TableName, sTableName)
        End If

        If Not sString.IndexOf(Constants.Staging_TableName) = -1 Then
          sString = sString.Replace(Constants.Staging_TableName, Me.StagingTableName)
        End If

        If Not sString.IndexOf(Constants.Staging_Field) = -1 Then
          sString = sString.Replace(Constants.Staging_Field, GetTableField(DataRow))
        End If

        StringBuilder.Append(sString & vbNewLine)
      Next
    End Sub

    Private Function GetTableField(ByVal DataRow As DataRow) As String
      Dim sField As String = ""

      Select Case DataRow("MetaGroupFieldTypeInd")
        Case "F"
          If Not IsDBNull(DataRow("FileFormatFieldID")) Then
            sField &= moLoaderEngineManager.ParseFileFormatFieldDesc(DataRow("FileFormatFieldDesc"))
          End If
      End Select

      Return sField
    End Function
#End Region
  End Class
End Namespace