Imports System
Imports System.IO
Imports System.Text
Imports sembleWare.Runtime
Imports XDSAdministrationBL

Namespace SQLManager
  Friend Class LoaderDataMatching
#Region " Variables"
    Private moMetaDatabase As MetaDatabase
    Private moLoaderEngineManager As LoaderEngineManager.Manager
    Private moFileFormatDeployment As FileFormatDeployment
    Private mlLoaderID As Long
#End Region

#Region " Properties"
    Public ReadOnly Property StoredProcedureName() As String
      Get
        Return "sp" & Me.StagingTableName & "_DataMatching"
      End Get
    End Property

    Public ReadOnly Property DatabaseName() As String
      Get
        Return moMetaDatabase.DatabaseName.Value
      End Get
    End Property

    Private ReadOnly Property StagingTableName() As String
      Get
        Return SQLManager.StagingTable.TableName(moMetaDatabase, moLoaderEngineManager.FileFormat, moLoaderEngineManager.FileFormatDeploymentID, mlLoaderID)
      End Get
    End Property

    Public ReadOnly Property StagingMultipleMatchTableName() As String
      Get
        Return SQLManager.StagingMultipleMatchTable.TableName(moMetaDatabase, moLoaderEngineManager.FileFormat, moLoaderEngineManager.FileFormatDeploymentID, mlLoaderID)
      End Get
    End Property
#End Region

#Region " Constructors"
    Public Sub New(ByVal MetaDatabase As MetaDatabase, ByVal FileFormatDeployment As FileFormatDeployment, ByVal LoaderEngineManager As LoaderEngineManager.Manager, ByVal LoaderID As Long)
      moMetaDatabase = MetaDatabase
      moLoaderEngineManager = LoaderEngineManager
      moFileFormatDeployment = FileFormatDeployment
      mlLoaderID = LoaderID
    End Sub
#End Region

#Region " Methods"
    Public Sub DropStoredProcedure()
      Dim oString As StringBuilder = New StringBuilder

      oString.Append("if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[" & Me.StoredProcedureName & "]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbNewLine)
      oString.Append("drop procedure [dbo].[" & Me.StoredProcedureName & "]" & vbNewLine)

      moLoaderEngineManager.ApplicationSettings.ActionQuery(oString.ToString())
    End Sub

    Public Sub CreateStoredProcedure()
      Dim oStringBuilder As StringBuilder = New StringBuilder
      Dim oFileFormatDeploymentMetaLoaderProcedure As FileFormatDeploymentMetaLoaderProcedure = FileFormatDeploymentMetaLoaderProcedure.GetDataMatching(moFileFormatDeployment, moLoaderEngineManager.FileFormat, moMetaDatabase, moLoaderEngineManager.ApplicationSettings)
      Dim oStringReader As StringReader = New StringReader(oFileFormatDeploymentMetaLoaderProcedure.TemplateDetails.Value)

      Try
        Dim sLine As String = oStringReader.ReadLine()

        While Not sLine Is Nothing
          Dim bAppendYN As Boolean = True

          If Not sLine.IndexOf(Constants.StoredProcedureName) = -1 Then
            sLine = sLine.Replace(Constants.StoredProcedureName, Me.StoredProcedureName)
          End If

          If Not sLine.IndexOf(Constants.DatabaseName) = -1 Then
            sLine = sLine.Replace(Constants.DatabaseName, Me.DatabaseName)
          End If

          If Not sLine.IndexOf(Constants.Staging_MultipleMatch_TableName) = -1 Then
            sLine = sLine.Replace(Constants.Staging_MultipleMatch_TableName, Me.StagingMultipleMatchTableName)
          End If

          If Not sLine.IndexOf(Constants.Staging_TableName) = -1 Then
            sLine = sLine.Replace(Constants.Staging_TableName, Me.StagingTableName)
          End If

          If Not sLine.IndexOf(Constants.FileFormat_MetaDatabaseCode) = -1 Then
            sLine = sLine.Replace(Constants.FileFormat_MetaDatabaseCode, oFileFormatDeploymentMetaLoaderProcedure._MetaDatabaseCode.Value)
          End If

          If Not sLine.IndexOf(Constants.Staging_Fields) = -1 Then
            sLine = sLine.Replace(Constants.Staging_Fields, GetTableFields(TableTypes.Staging))
          End If

          If Not sLine.IndexOf(Constants.MatchingEvaluationRepeater_Start) = -1 Then
            ReplaceRepeaterTag(oStringReader, Constants.MatchingEvaluationRepeater_End, XDSAdministrationBL.Constants.DataProcedure.Types.MatchingEvaluation, oStringBuilder)
            bAppendYN = False
          End If

          If bAppendYN Then
            oStringBuilder.Append(sLine & vbNewLine)
          End If

          sLine = oStringReader.ReadLine()
        End While
        oStringReader.Close()

        moLoaderEngineManager.ApplicationSettings.ActionQuery(oStringBuilder.ToString())
      Catch oException As Exception
        Dim sErrorString As String = "Error deploying Loader Procedure Data Matching of File Format: " & moLoaderEngineManager.FileFormat.FileFormatCode.Value & " - " & moLoaderEngineManager.FileFormat.FileFormatDesc.Value
        Throw New Exception(sErrorString & vbNewLine & oException.Message, New Exception(oStringBuilder.ToString(), oException))
      End Try
    End Sub
#End Region

#Region " Helper Functions"
    Private Function GetTableFields(ByVal TableType As TableTypes) As String
      Dim sFields As String = ""

      Select Case TableType
        Case TableTypes.Staging
          Dim oFileFormatMetaDatabase As FileFormatMetaDatabase = moLoaderEngineManager.FileFormat.FileFormatMetaDatabase_OwnMany.NewInstance()

          oFileFormatMetaDatabase.MetaDatabase.Instance = moMetaDatabase
          oFileFormatMetaDatabase.Load()
          Dim oList As List = oFileFormatMetaDatabase._FileFormatMatchingEngineField_OwnMany.GridList
          Dim oDataRow As DataRow

          For Each oDataRow In oList.DataTable.Rows
            sFields &= moLoaderEngineManager.ParseFileFormatFieldDesc(oDataRow("FileFormatFieldDesc")) & ", "
          Next
          If sFields.Length > 0 Then
            sFields = sFields.Substring(0, sFields.Length - 2)
          End If
      End Select

      Return sFields
    End Function

    Private Sub ReplaceRepeaterTag(ByVal StringReader As StringReader, ByVal EndRepeaterTag As String, ByVal DataProcedureTypeInd As String, ByRef StringBuilder As StringBuilder)
      Dim oList As List = moLoaderEngineManager.FileFormat._FileFormatMetaDataProcedure_OwnMany.GridList
      Dim oDataRow
      Dim oRepeaterArray As ArrayList = New ArrayList

      Dim sLine As String = StringReader.ReadLine()
      While Not sLine Is Nothing
        If Not sLine.IndexOf(EndRepeaterTag) = -1 Then
          Exit While
        End If
        oRepeaterArray.Add(sLine)
        sLine = StringReader.ReadLine()
      End While

      Dim oDataProcedure As DataProcedure = New DataProcedure(moMetaDatabase, moLoaderEngineManager)
      oList.LimitByRelatedPart(moMetaDatabase, "MetaDatabase", True)
      oList.AndFilters.Add(New ListFilter("DataProcedureTypeInd", "DataProcedureTypeInd", DataProcedureTypeInd, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
      For Each oDataRow In oList.DataTable.Rows
        AddDataProcedure(oDataProcedure, DataProcedureTypeInd, oDataRow, oRepeaterArray, StringBuilder)
      Next
    End Sub

    Private Sub AddDataProcedure(ByVal DataProcedure As DataProcedure, ByVal DataProcedureTypeInd As String, ByVal DataRow As DataRow, ByVal RepeaterArray As ArrayList, ByRef StringBuilder As StringBuilder)
      Dim sString As String

      For Each sString In RepeaterArray
        If Not sString.IndexOf(Constants.Staging_TableName) = -1 Then
          sString = sString.Replace(Constants.Staging_TableName, Me.StagingTableName)
        End If

        If Not sString.IndexOf(Constants.FileFormatField_FileFormatFieldID) = -1 Then
          sString = sString.Replace(Constants.FileFormatField_FileFormatFieldID, System.Convert.ToInt32(DataRow("FileFormatFieldID")))
        End If

        If Not sString.IndexOf(Constants.FileFormatField_FileFormatFieldDesc) = -1 Then
          sString = sString.Replace(Constants.FileFormatField_FileFormatFieldDesc, moLoaderEngineManager.ParseFileFormatFieldDesc(DataRow("FileFormatFieldDesc")))
        End If

        If Not sString.IndexOf(Constants.DataProcedure_MetaDatabaseCode) = -1 Then
          sString = sString.Replace(Constants.DataProcedure_MetaDatabaseCode, DataRow("MetaDatabaseCode"))
        End If

        If Not sString.IndexOf(Constants.DataProcedure_DataProcedureID) = -1 Then
          sString = sString.Replace(Constants.DataProcedure_DataProcedureID, DataRow("DataProcedureID"))
        End If

        If Not sString.IndexOf(Constants.DataProcedure_SQLFunctionName) = -1 Then
          Dim sSQLFunctionName As String = ""

          Select Case DataRow("ParameterTypeInd")
            Case XDSAdministrationBL.Constants.DataProcedure.ParameterTypes.None
              sSQLFunctionName = "()"
            Case XDSAdministrationBL.Constants.DataProcedure.ParameterTypes.DefaultToSelectedField
              sSQLFunctionName = "(" & moLoaderEngineManager.ParseFileFormatFieldDesc(DataRow("FileFormatFieldDesc")) & ")"
            Case XDSAdministrationBL.Constants.DataProcedure.ParameterTypes.MultipleSpecifiedFields
              Dim oFileFormatMetaDataProcedure As FileFormatMetaDataProcedure = New FileFormatMetaDataProcedureRelationship(moLoaderEngineManager.ApplicationSettings).NewInstance()
              oFileFormatMetaDataProcedure._DataProcedureID.Load(DataRow("DataProcedureID"))
              oFileFormatMetaDataProcedure._FileFormatCode.Load(DataRow("FileFormatCode"))
              oFileFormatMetaDataProcedure._FileFormatFieldID.Load(DataRow("FileFormatFieldID"))
              oFileFormatMetaDataProcedure._MetaDatabaseCode.Load(moMetaDatabase.MetaDatabaseCode.Value)

              Dim oList As List = oFileFormatMetaDataProcedure._FileFormatMetaDataProcedureParameter_OwnMany.GridList
              Dim oDataRow As DataRow

              For Each oDataRow In oList.DataTable.Rows
                sSQLFunctionName &= moLoaderEngineManager.ParseFileFormatFieldDesc(oDataRow("FileFormatFieldDesc")) & ", "
              Next
              If sSQLFunctionName.Length > 0 Then
                sSQLFunctionName = "(" & sSQLFunctionName.Substring(0, sSQLFunctionName.Length - 2) & ")"
              End If
          End Select
          sSQLFunctionName = DataProcedure.FunctionName(DataRow("DataProcedureID"), DataRow("DataProcedureTypeInd")) & sSQLFunctionName
          sString = sString.Replace(Constants.DataProcedure_SQLFunctionName, sSQLFunctionName)
        End If
        StringBuilder.Append(sString & vbNewLine)
      Next
    End Sub
#End Region
  End Class
End Namespace