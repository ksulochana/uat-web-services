Imports sembleWare.Runtime
Imports System
Public Class CullingRule 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly CullingRuleCode As New StringElement("CullingRuleCode", Me, True, True, True, True, True, 10, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CullingRuleDesc As New StringElement("CullingRuleDesc", Me, False, False, True, True, True, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly StatusInd As New IndicatorElement("StatusInd", Me, False, False, True, True, True, True, "Status", Nothing, CullingRule.StatusIndOptions, "I")
  Public ReadOnly MaxNoOfMonths As New IntegerElement("MaxNoOfMonths", Me, False, True, True, True, True, "Maximum No Of Months from List Date", Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property StatusIndOptions() As IndicatorOptions
    Get
      If moStatusIndOptions Is Nothing Then
        moStatusIndOptions = New IndicatorOptions
        moStatusIndOptions.Add("A", "Active")
        moStatusIndOptions.Add("I", "Inactive")
      End If
      Return moStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("CullingRule", ApplicationSettings)
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class CullingRuleRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _CullingRuleCode As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New CullingRule(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _CullingRuleCode.ForeignElement = CType(Instance, CullingRule).CullingRuleCode
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0CullingRule")
    oList.Columns.Add(New ListColumn("CullingRuleCode", "[A0CullingRule].[CullingRuleCode]", "CullingRuleCode", DataType.String, Nothing, True, 1, True, 100, "Culling Rule Code"))
    oList.Columns.Add(New ListColumn("CullingRuleDesc", "[A0CullingRule].[CullingRuleDesc]", "CullingRuleDesc", DataType.String, Nothing, False, 0, True, 200, "Culling Rule Desc"))
    oList.Columns.Add(New ListColumn("StatusInd", "[A0CullingRule].[StatusInd]", "StatusInd", DataType.String, Nothing, False, 0, True, 100, "Status"))
    oList.Columns.Add(New ListColumn("MaxNoOfMonths", "[A0CullingRule].[MaxNoOfMonths]", "MaxNoOfMonths", DataType.Integer, Nothing, False, 0, True, 100, "Maximum No Of Months from List Date"))
    oList.SelectStatement = "select"
    oList.FromClause = "[CullingRule]  [A0CullingRule]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
