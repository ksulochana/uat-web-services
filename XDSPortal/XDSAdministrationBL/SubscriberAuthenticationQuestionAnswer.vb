Imports sembleWare.Runtime
Imports System
Public Class SubscriberAuthenticationQuestionAnswer 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly AnswerID As New IdentityElement("AnswerID", Me, True, Nothing, Nothing)
  Public ReadOnly Answer As New StringElement("Answer", Me, False, True, True, True, False, 500, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly IsCorrectAnswerYN As New BooleanElement("IsCorrectAnswerYN", Me, False, True, True, True, True, "Is Correct Answer?", Nothing, False, "Yes;No")
  Public ReadOnly IsEnteredAnswerYN As New BooleanElement("IsEnteredAnswerYN", Me, False, True, True, True, True, "Is Entered Answer?", Nothing, False, "Yes;No")
  Public ReadOnly IgnoreSubscriberID As New IntegerElement("IgnoreSubscriberID", Me, False, False, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SubscriberAuthenticationQuestion As Relationship = New SubscriberAuthenticationQuestionRelationship("SubscriberAuthenticationQuestion", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _SubscriberAuthenticationID As New IntegerElement("SubscriberAuthenticationID", Me, True)
  Public ReadOnly _ProductAuthenticationQuestionID As New IntegerElement("ProductAuthenticationQuestionID", Me, True)
  Public ReadOnly _SubscriberAuthenticationQuestion As SubscriberAuthenticationQuestionRelationship = SubscriberAuthenticationQuestion
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberAuthenticationQuestionAnswer", ApplicationSettings)
    _SubscriberAuthenticationQuestion._SubscriberID.LocalElement = _SubscriberID
    _SubscriberAuthenticationQuestion._SubscriberAuthenticationID.LocalElement = _SubscriberAuthenticationID
    _SubscriberAuthenticationQuestion._ProductAuthenticationQuestionID.LocalElement = _ProductAuthenticationQuestionID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class SubscriberAuthenticationQuestionAnswerRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _AnswerID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SubscriberAuthenticationID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _ProductAuthenticationQuestionID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberAuthenticationQuestionAnswer(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _AnswerID.ForeignElement = CType(Instance, SubscriberAuthenticationQuestionAnswer).AnswerID
    _SubscriberID.ForeignElement = CType(Instance, SubscriberAuthenticationQuestionAnswer)._SubscriberID
    _SubscriberAuthenticationID.ForeignElement = CType(Instance, SubscriberAuthenticationQuestionAnswer)._SubscriberAuthenticationID
    _ProductAuthenticationQuestionID.ForeignElement = CType(Instance, SubscriberAuthenticationQuestionAnswer)._ProductAuthenticationQuestionID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function AvailableAnswerGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberAuthenticationQuestionAnswer")
    oList.Columns.Add(New ListColumn("AnswerID", "[A0SubscriberAuthenticationQuestionAnswer].[AnswerID]", "AnswerID", DataType.Long, Nothing, True, 0, False, 100, "Answer ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberAuthenticationQuestionAnswer].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberAuthenticationID", "[A0SubscriberAuthenticationQuestionAnswer].[SubscriberAuthenticationID]", "SubscriberAuthenticationID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber Authentication ID"))
    oList.Columns.Add(New ListColumn("ProductAuthenticationQuestionID", "[A0SubscriberAuthenticationQuestionAnswer].[ProductAuthenticationQuestionID]", "ProductAuthenticationQuestionID", DataType.Integer, Nothing, True, 0, False, 100, "Product Authentication Question ID"))
    oList.Columns.Add(New ListColumn("Answer", "[A0SubscriberAuthenticationQuestionAnswer].[Answer]", "Answer", DataType.String, Nothing, False, 1, True, 100, "Answer"))
    oList.Columns.Add(New ListColumn("IsCorrectAnswerYN", "[A0SubscriberAuthenticationQuestionAnswer].[IsCorrectAnswerYN]", "IsCorrectAnswerYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Is Correct Answer?"))
    oList.Columns.Add(New ListColumn("IsEnteredAnswerYN", "[A0SubscriberAuthenticationQuestionAnswer].[IsEnteredAnswerYN]", "IsEnteredAnswerYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Is Entered Answer?"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberAuthenticationQuestionAnswer]  [A0SubscriberAuthenticationQuestionAnswer]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function CorrectAnswerGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberAuthenticationQuestionAnswer")
    oList.Columns.Add(New ListColumn("AnswerID", "[A0SubscriberAuthenticationQuestionAnswer].[AnswerID]", "AnswerID", DataType.Long, Nothing, True, 0, False, 100, "Answer ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberAuthenticationQuestionAnswer].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberAuthenticationID", "[A0SubscriberAuthenticationQuestionAnswer].[SubscriberAuthenticationID]", "SubscriberAuthenticationID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber Authentication ID"))
    oList.Columns.Add(New ListColumn("ProductAuthenticationQuestionID", "[A0SubscriberAuthenticationQuestionAnswer].[ProductAuthenticationQuestionID]", "ProductAuthenticationQuestionID", DataType.Integer, Nothing, True, 0, False, 100, "Product Authentication Question ID"))
    oList.Columns.Add(New ListColumn("Answer", "[A0SubscriberAuthenticationQuestionAnswer].[Answer]", "Answer", DataType.String, Nothing, False, 1, True, 100, "Answer"))
    oList.Columns.Add(New ListColumn("IsCorrectAnswerYN", "[A0SubscriberAuthenticationQuestionAnswer].[IsCorrectAnswerYN]", "IsCorrectAnswerYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Is Correct Answer?"))
    oList.Columns.Add(New ListColumn("IsEnteredAnswerYN", "[A0SubscriberAuthenticationQuestionAnswer].[IsEnteredAnswerYN]", "IsEnteredAnswerYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Is Entered Answer?"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberAuthenticationQuestionAnswer]  [A0SubscriberAuthenticationQuestionAnswer]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    oList.AndFilters.Add(New ListFilter("IsCorrectAnswerYN", "IsCorrectAnswerYN", True, DataType.Boolean, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
    Return oList
  End Function


  Public Function EnteredAnswerGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberAuthenticationQuestionAnswer")
    oList.Columns.Add(New ListColumn("Answer", "[A0SubscriberAuthenticationQuestionAnswer].[Answer]", "Answer", DataType.String, Nothing, False, 2, True, 100, "Answer"))
    oList.Columns.Add(New ListColumn("AnswerID", "[A0SubscriberAuthenticationQuestionAnswer].[AnswerID]", "AnswerID", DataType.Long, Nothing, True, 0, False, 100, "Answer ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberAuthenticationQuestionAnswer].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberAuthenticationID", "[A0SubscriberAuthenticationQuestionAnswer].[SubscriberAuthenticationID]", "SubscriberAuthenticationID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber Authentication ID"))
    oList.Columns.Add(New ListColumn("ProductAuthenticationQuestionID", "[A0SubscriberAuthenticationQuestionAnswer].[ProductAuthenticationQuestionID]", "ProductAuthenticationQuestionID", DataType.Integer, Nothing, True, 0, False, 100, "Product Authentication Question ID"))
    oList.Columns.Add(New ListColumn("IsCorrectAnswerYN", "[A0SubscriberAuthenticationQuestionAnswer].[IsCorrectAnswerYN]", "IsCorrectAnswerYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Is Correct Answer?"))
    oList.Columns.Add(New ListColumn("IsEnteredAnswerYN", "[A0SubscriberAuthenticationQuestionAnswer].[IsEnteredAnswerYN]", "IsEnteredAnswerYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Is Entered Answer?"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberAuthenticationQuestionAnswer]  [A0SubscriberAuthenticationQuestionAnswer]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    oList.AndFilters.Add(New ListFilter("IsEnteredAnswerYN", "IsEnteredAnswerYN", True, DataType.Boolean, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
    Return oList
  End Function


  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberAuthenticationQuestionAnswer")
    oList.Columns.Add(New ListColumn("AnswerID", "[A0SubscriberAuthenticationQuestionAnswer].[AnswerID]", "AnswerID", DataType.Long, Nothing, True, 0, False, 100, "Answer ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberAuthenticationQuestionAnswer].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberAuthenticationID", "[A0SubscriberAuthenticationQuestionAnswer].[SubscriberAuthenticationID]", "SubscriberAuthenticationID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber Authentication ID"))
    oList.Columns.Add(New ListColumn("ProductAuthenticationQuestionID", "[A0SubscriberAuthenticationQuestionAnswer].[ProductAuthenticationQuestionID]", "ProductAuthenticationQuestionID", DataType.Integer, Nothing, True, 0, False, 100, "Product Authentication Question ID"))
    oList.Columns.Add(New ListColumn("Answer", "[A0SubscriberAuthenticationQuestionAnswer].[Answer]", "Answer", DataType.String, Nothing, False, 1, True, 300, "Answer"))
    oList.Columns.Add(New ListColumn("IsCorrectAnswerYN", "[A0SubscriberAuthenticationQuestionAnswer].[IsCorrectAnswerYN]", "IsCorrectAnswerYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Is Correct Answer?"))
    oList.Columns.Add(New ListColumn("IsEnteredAnswerYN", "[A0SubscriberAuthenticationQuestionAnswer].[IsEnteredAnswerYN]", "IsEnteredAnswerYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Is Entered Answer?"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberAuthenticationQuestionAnswer]  [A0SubscriberAuthenticationQuestionAnswer]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
