Imports sembleWare.Runtime
Imports System
Public Class NotificationQueue 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly NotificationQueueID As New IdentityElement("NotificationQueueID", Me, True, Nothing, Nothing)
  Public ReadOnly NotificationQueueDate As New DateTimeElement("NotificationQueueDate", Me, False, False, True, True, True, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly StatusInd As New IndicatorElement("StatusInd", Me, False, False, False, True, True, True, Nothing, Nothing, NotificationQueue.StatusIndOptions, "Q")
  Public ReadOnly SMTPServerName As New StringElement("SMTPServerName", Me, False, False, True, True, True, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SenderTypeInd As New IndicatorElement("SenderTypeInd", Me, False, False, False, True, True, True, Nothing, Nothing, NotificationQueue.SenderTypeIndOptions, Nothing)
  Public ReadOnly SenderFirstName As New StringElement("SenderFirstName", Me, False, False, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SenderSurname As New StringElement("SenderSurname", Me, False, False, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SenderEmailAddress As New StringElement("SenderEmailAddress", Me, False, False, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ProcessedDate As New DateTimeElement("ProcessedDate", Me, False, False, True, True, False, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly ErrorDetails As New TextElement("ErrorDetails", Me, False, True, True, False, Nothing, Nothing, Nothing)
  Public ReadOnly NotificationTemplate As Relationship = New NotificationTemplateRelationship("NotificationTemplate", Nothing, RelationshipType.Include, True, True, True, Me)
  Public ReadOnly SenderSystemUser As Relationship = New SystemUserRelationship("SenderSystemUser", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly SenderTitle As Relationship = New TitleRelationship("SenderTitle", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly NotificationQueueRecipient_OwnMany As Relationship = New NotificationQueueRecipientRelationship("NotificationQueueRecipient", Nothing, "NotificationQueue", Me)
  Public ReadOnly NotificationQueueParameter_OwnMany As Relationship = New NotificationQueueParameterRelationship("NotificationQueueParameter", Nothing, "NotificationQueue", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _NotificationTemplateID As New IntegerElement("NotificationTemplateID", Me, False)
  Public ReadOnly _SenderSystemUserID As New IntegerElement("SenderSystemUserID", Me, False)
  Public ReadOnly _SenderTitleCode As New StringElement("SenderTitleCode", Me, False)
  Public ReadOnly _NotificationTemplate As NotificationTemplateRelationship = NotificationTemplate
  Public ReadOnly _SenderSystemUser As SystemUserRelationship = SenderSystemUser
  Public ReadOnly _SenderTitle As TitleRelationship = SenderTitle
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  Public ReadOnly _NotificationQueueRecipient_OwnMany As NotificationQueueRecipientRelationship = NotificationQueueRecipient_OwnMany
  Public ReadOnly _NotificationQueueParameter_OwnMany As NotificationQueueParameterRelationship = NotificationQueueParameter_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property StatusIndOptions() As IndicatorOptions
    Get
      If moStatusIndOptions Is Nothing Then
        moStatusIndOptions = New IndicatorOptions
        moStatusIndOptions.Add("Q", "Queued")
        moStatusIndOptions.Add("P", "Processing")
        moStatusIndOptions.Add("C", "Completed")
        moStatusIndOptions.Add("F", "Failed")
      End If
      Return moStatusIndOptions
    End Get
  End Property
  Private Shared moSenderTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property SenderTypeIndOptions() As IndicatorOptions
    Get
      If moSenderTypeIndOptions Is Nothing Then
        moSenderTypeIndOptions = New IndicatorOptions
        moSenderTypeIndOptions.Add("S", "System User")
        moSenderTypeIndOptions.Add("E", "Email Address")
      End If
      Return moSenderTypeIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("NotificationQueue", ApplicationSettings)
    _NotificationTemplate._NotificationTemplateID.LocalElement = _NotificationTemplateID
    _SenderSystemUser._SystemUserID.LocalElement = _SenderSystemUserID
    _SenderTitle._TitleCode.LocalElement = _SenderTitleCode
    AddHandler SenderTypeInd.Changed, AddressOf SenderTypeInd_Changed
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub SenderTypeInd_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BRSenderTypeInd(Element, True, True)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Shared Procedures"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreate()
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreate()
    Dim oSystemDefault As SystemDefault = SystemDefault.Current(ApplicationSettings)

    Me.NotificationQueueDate.Value = System.DateTime.Now
    Me.SenderTypeInd.Value = Constants.NotificationQueue.SenderTypes.EmailAddress
    Me.SMTPServerName.Value = oSystemDefault.NotificationSMTPServer.Value
    Me.SenderTitle.Instance = oSystemDefault.NotificationTitle.Instance
    Me.SenderFirstName.Value = oSystemDefault.NotificationFirstName.Value
    Me.SenderSurname.Value = oSystemDefault.NotificationSurname.Value
    Me.SenderEmailAddress.Value = oSystemDefault.NotificationEmailAddress.Value
  End Sub

  Private Sub BRSenderTypeInd(ByVal Element As Element, ByVal SetEnableMandatory As Boolean, ByVal PerformRules As Boolean)
    If SetEnableMandatory Then
      Me.SenderSystemUser.Enabled = False
      Me.SenderSystemUser.Mandatory = False
      Me.SenderEmailAddress.Enabled = False
      Me.SenderEmailAddress.Mandatory = False

      Select Case Element.ValueAsObject
        Case Constants.NotificationQueue.SenderTypes.SystemUser
          Me.SenderSystemUser.Enabled = True
          Me.SenderSystemUser.Mandatory = True
        Case Constants.NotificationQueue.SenderTypes.EmailAddress
          Me.SenderEmailAddress.Enabled = True
          Me.SenderEmailAddress.Mandatory = True
      End Select
    End If

    If PerformRules Then
      Me.SenderEmailAddress.SetToNullValue()
      Me.SenderSystemUser.Instance = Nothing
    End If
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class NotificationQueueRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _NotificationQueueID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New NotificationQueue(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _NotificationQueueID.ForeignElement = CType(Instance, NotificationQueue).NotificationQueueID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0NotificationQueue")
    oList.Columns.Add(New ListColumn("NotificationQueueID", "[A0NotificationQueue].[NotificationQueueID]", "NotificationQueueID", DataType.Long, Nothing, True, 0, True, 100, "Notification Queue ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "[NotificationQueue]  [A0NotificationQueue]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
