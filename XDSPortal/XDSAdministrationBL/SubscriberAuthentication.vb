Imports sembleWare.Runtime
Imports System
Imports System.Data
Imports System.Data.SqlClient

Public Class SubscriberAuthentication 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly SubscriberAuthenticationID As New IdentityElement("SubscriberAuthenticationID", Me, True, Nothing, Nothing)
  Public ReadOnly AuthenticationTypeInd As New IndicatorElement("AuthenticationTypeInd", Me, False, False, False, True, True, False, "Authentication Type", Nothing, SubscriberAuthentication.AuthenticationTypeIndOptions, Nothing)
  Public ReadOnly AuthenticationDate As New DateTimeElement("AuthenticationDate", Me, False, False, True, True, True, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly ReferenceNo As New StringElement("ReferenceNo", Me, False, False, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly EncryptedReferenceNo As New StringElement("EncryptedReferenceNo", Me, False, False, True, True, False, 200, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AuthenticationStatusInd As New IndicatorElement("AuthenticationStatusInd", Me, False, False, False, True, True, True, "Authentication Status", Nothing, SubscriberAuthentication.AuthenticationStatusIndOptions, "P")
  Public ReadOnly RequiredAuthenticatedPerc As New DecimalElement("RequiredAuthenticatedPerc", Me, False, False, True, True, True, "Required Authenticated %", Nothing, "##0.00", 0, Nothing, Nothing)
  Public ReadOnly AuthenticatedPerc As New DecimalElement("AuthenticatedPerc", Me, False, False, True, True, True, "Authenticated %", Nothing, "##0.00", 0, Nothing, Nothing)
  Public ReadOnly TotalQuestionPointValue As New IntegerElement("TotalQuestionPointValue", Me, False, False, True, True, True, Nothing, Nothing, Nothing, 0, Nothing, Nothing)
  Public ReadOnly IDNoVerificationYN As New BooleanElement("IDNoVerificationYN", Me, False, True, True, True, True, "ID No Verification?", Nothing, False)
  Public ReadOnly ConsumerAccountAgeMessage As New TextElement("ConsumerAccountAgeMessage", Me, False, True, True, False, Nothing, Nothing, Nothing)
  Public ReadOnly RepeatAuthenticationMessage As New TextElement("RepeatAuthenticationMessage", Me, False, True, True, False, Nothing, Nothing, Nothing)
  Public ReadOnly AuthenticationComment As New TextElement("AuthenticationComment", Me, True, True, True, False, "Comments", Nothing, Nothing)
  Public ReadOnly ReportFile As New BinaryElement("ReportFile", Me, False, True, True, False, Nothing, Nothing)
  Public ReadOnly ReportFileMimeType As New StringElement("ReportFileMimeType", Me, False, False, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ActionedBySystemUser As Relationship = New SystemUserRelationship("ActionedBySystemUser", "Actioned By", RelationshipType.Include, True, False, True, Me)
  Public ReadOnly SubscriberProfileProductAuthenticationBranch As Relationship = New SubscriberProfileProductAuthenticationBranchRelationship("SubscriberProfileProductAuthenticationBranch", "Branch", RelationshipType.Include, True, False, True, Me)
  Public ReadOnly SubscriberProfileProductAuthenticationPurpose As Relationship = New SubscriberProfileProductAuthenticationPurposeRelationship("SubscriberProfileProductAuthenticationPurpose", "Purpose", RelationshipType.Include, True, False, True, Me)
  Public ReadOnly PreviousSubscriberAuthentication As Relationship = New SubscriberAuthenticationRelationship("PreviousSubscriberAuthentication", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly SubscriberBillingGroup As Relationship = New SubscriberBillingGroupRelationship("SubscriberBillingGroup", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly SubscriberConsumerEnquiry As Relationship = New SubscriberConsumerEnquiryRelationship("SubscriberConsumerEnquiry", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly SubscriberHomeAffairsEnquiry As Relationship = New SubscriberHomeAffairsEnquiryRelationship("SubscriberHomeAffairsEnquiry", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Consumer As Relationship = New ConsumerRelationship("Consumer", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly HomeAffairs As Relationship = New HomeAffairsRelationship("HomeAffairs", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly Product As Relationship = New ProductRelationship("Product", Nothing, RelationshipType.Include, True, False, True, Me)
  Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
    Public ReadOnly SubscriberAuthenticationQuestion_OwnMany As Relationship = New SubscriberAuthenticationQuestionRelationship("SubscriberAuthenticationQuestion", Nothing, "SubscriberAuthentication", Me)
    Public ReadOnly AdminDBConnectionstring As New TextElement("AdminDBConnectionstring", Nothing, True, False, True, False, "AdminDBConnectionstring", Nothing, Nothing)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _ConsumerID As New IntegerElement("ConsumerID", Me, False)
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _ActionedBySystemUserID As New IntegerElement("ActionedBySystemUserID", Me, False)
  Public ReadOnly _SubscriberBranchCode As New StringElement("SubscriberBranchCode", Me, False)
  Public ReadOnly _AuthenticationPurposeID As New IntegerElement("AuthenticationPurposeID", Me, False)
  Public ReadOnly _PreviousSubscriberAuthenticationID As New IntegerElement("PreviousSubscriberAuthenticationID", Me, False)
  Public ReadOnly _PreviousSubscriberID As New IntegerElement("PreviousSubscriberID", Me, False)
  Public ReadOnly _HomeAffairsID As New IntegerElement("HomeAffairsID", Me, False)
  Public ReadOnly _SubscriberBillingGroupCode As New StringElement("SubscriberBillingGroupCode", Me, False)
  Public ReadOnly _SubscriberConsumerEnquiryID As New IntegerElement("SubscriberConsumerEnquiryID", Me, False)
  Public ReadOnly _SubscriberHomeAffairsEnquiryID As New IntegerElement("SubscriberHomeAffairsEnquiryID", Me, False)
  Public ReadOnly _ProductID As New IntegerElement("ProductID", Me, False)
  Public ReadOnly _ActionedBySystemUser As SystemUserRelationship = ActionedBySystemUser
  Public ReadOnly _SubscriberProfileProductAuthenticationBranch As SubscriberProfileProductAuthenticationBranchRelationship = SubscriberProfileProductAuthenticationBranch
  Public ReadOnly _SubscriberProfileProductAuthenticationPurpose As SubscriberProfileProductAuthenticationPurposeRelationship = SubscriberProfileProductAuthenticationPurpose
  Public ReadOnly _PreviousSubscriberAuthentication As SubscriberAuthenticationRelationship = PreviousSubscriberAuthentication
  Public ReadOnly _SubscriberBillingGroup As SubscriberBillingGroupRelationship = SubscriberBillingGroup
  Public ReadOnly _SubscriberConsumerEnquiry As SubscriberConsumerEnquiryRelationship = SubscriberConsumerEnquiry
  Public ReadOnly _SubscriberHomeAffairsEnquiry As SubscriberHomeAffairsEnquiryRelationship = SubscriberHomeAffairsEnquiry
  Public ReadOnly _Consumer As ConsumerRelationship = Consumer
  Public ReadOnly _HomeAffairs As HomeAffairsRelationship = HomeAffairs
  Public ReadOnly _Product As ProductRelationship = Product
    Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
    'Public ReadOnly _AdminDBConnectionstring As New StringElement("AdminDBConnectionstring", Me, False)
  Public ReadOnly _SubscriberAuthenticationQuestion_OwnMany As SubscriberAuthenticationQuestionRelationship = SubscriberAuthenticationQuestion_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moAuthenticationTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property AuthenticationTypeIndOptions() As IndicatorOptions
    Get
      If moAuthenticationTypeIndOptions Is Nothing Then
        moAuthenticationTypeIndOptions = New IndicatorOptions
        moAuthenticationTypeIndOptions.Add("P", "Primary")
        moAuthenticationTypeIndOptions.Add("S", "Secondary")
      End If
      Return moAuthenticationTypeIndOptions
    End Get
  End Property
  Private Shared moAuthenticationStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property AuthenticationStatusIndOptions() As IndicatorOptions
    Get
      If moAuthenticationStatusIndOptions Is Nothing Then
        moAuthenticationStatusIndOptions = New IndicatorOptions
        moAuthenticationStatusIndOptions.Add("P", "Pending")
        moAuthenticationStatusIndOptions.Add("A", "Authenticated")
        moAuthenticationStatusIndOptions.Add("N", "Not Authenticated")
        moAuthenticationStatusIndOptions.Add("V", "Voided")
        moAuthenticationStatusIndOptions.Add("NC", "No Consumer Found")
      End If
      Return moAuthenticationStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberAuthentication", ApplicationSettings)
    _ActionedBySystemUser._SystemUserID.LocalElement = _ActionedBySystemUserID
    _SubscriberProfileProductAuthenticationBranch._SubscriberBranchCode.LocalElement = _SubscriberBranchCode
    _SubscriberProfileProductAuthenticationBranch._SubscriberID.LocalElement = _SubscriberID
    _SubscriberProfileProductAuthenticationPurpose._AuthenticationPurposeID.LocalElement = _AuthenticationPurposeID
    _SubscriberProfileProductAuthenticationPurpose._SubscriberID.LocalElement = _SubscriberID
    _PreviousSubscriberAuthentication._SubscriberAuthenticationID.LocalElement = _PreviousSubscriberAuthenticationID
    _PreviousSubscriberAuthentication._SubscriberID.LocalElement = _PreviousSubscriberID
    _SubscriberBillingGroup._SubscriberBillingGroupCode.LocalElement = _SubscriberBillingGroupCode
    _SubscriberConsumerEnquiry._SubscriberConsumerEnquiryID.LocalElement = _SubscriberConsumerEnquiryID
    _SubscriberConsumerEnquiry._SubscriberID.LocalElement = _SubscriberID
    _SubscriberHomeAffairsEnquiry._SubscriberHomeAffairsEnquiryID.LocalElement = _SubscriberHomeAffairsEnquiryID
    _SubscriberHomeAffairsEnquiry._SubscriberID.LocalElement = _SubscriberID
    _Consumer._ConsumerID.LocalElement = _ConsumerID
        _HomeAffairs._HomeAffairsID.LocalElement = _HomeAffairsID
    _Product._ProductID.LocalElement = _ProductID
        _Subscriber._SubscriberID.LocalElement = _SubscriberID

    AddHandler AuthenticationTypeInd.Changed, AddressOf AuthenticationTypeInd_Changed
    'sembleWare: Constructor End - Do Not Modify
    AddHandler SubscriberProfileProductAuthenticationBranch.LimitRelatedList, AddressOf SubscriberProfileProductAuthenticationBranch_LimitRelatedList
    AddHandler SubscriberProfileProductAuthenticationPurpose.LimitRelatedList, AddressOf SubscriberProfileProductAuthenticationPurpose_LimitRelatedList
  End Sub

#End Region 'sembleWare: Constructor

#Region " LimitRelatedList Events"
  Private Sub SubscriberProfileProductAuthenticationBranch_LimitRelatedList(ByVal List As List)
    Dim oSubscriberProfile As SubscriberProfile = SubscriberProfile.GetSubscriberProfile(Me._SubscriberID.Value, ApplicationSettings)
    List.LimitByRelatedPart(oSubscriberProfile, "SubscriberProfile", True)
  End Sub

  Private Sub SubscriberProfileProductAuthenticationPurpose_LimitRelatedList(ByVal List As List)
    Dim oSubscriberProfile As SubscriberProfile = SubscriberProfile.GetSubscriberProfile(Me._SubscriberID.Value, ApplicationSettings)
    List.LimitByRelatedPart(oSubscriberProfile, "SubscriberProfile", True)
  End Sub
#End Region

#Region " Change Events"
  Private Sub AuthenticationTypeInd_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BRAuthenticationTypeInd(Element, True, True)
  End Sub

  Private Sub TempSubscriberProfileProductAuthenticationBranch_Changed(ByVal Relationship As Relationship)
    ' Issue with VW on the screen when using DropDownList
    Me.SubscriberProfileProductAuthenticationBranch.Instance = Relationship.Instance
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"

  Public Sub AuthenticateQuestions()
    SubscriberAuthentication.ValidateAnswers(Me._SubscriberID.Value, Me.SubscriberAuthenticationID.Value, Me._ActionedBySystemUserID.Value, ApplicationSettings)
    Me.IgnoreCacheOnce = True
    Me.Load()
    BRGetEncryptedReferenceNo()
    Me.Save()
  End Sub

  Public Sub SaveValidate()
    Dim bAllowYN As Boolean = True
    Dim sMissingFields As String = ""

    If Me.SubscriberProfileProductAuthenticationBranch.IsEmpty Then
      bAllowYN = False
      sMissingFields &= Me.SubscriberProfileProductAuthenticationBranch.Caption & ", "
    End If

    If Me.SubscriberProfileProductAuthenticationPurpose.IsEmpty Then
      bAllowYN = False
      sMissingFields &= Me.SubscriberProfileProductAuthenticationPurpose.Caption & ", "
    End If

    If Not sMissingFields = "" Then
      sMissingFields = sMissingFields.Substring(0, sMissingFields.Length - 2)
    End If

    If Not bAllowYN Then
      Throw New BusinessRuleException(New MissingValuesException(sMissingFields, MyBase.Caption, "").Message)
    End If
  End Sub

  Public Sub Void()
    Me.AuthenticationStatusInd.Value = Constants.SubscriberAuthentication.AuthenticationStatuses.Voided
    Me.Save()
    BRGetReferenceNo()
    Me.Save()
  End Sub

  Public Sub SetupConsumerNotFound()
    Me.AuthenticationStatusInd.Value = Constants.SubscriberAuthentication.AuthenticationStatuses.NoConsumerFound
    Me.Save()
    BRGetReferenceNo()
    Me.Save()
  End Sub

  Public Sub SetupAuthentication()
    Me.Save()
    BRConsumerAccountAgeMessage()
    BRCheckTypeOfAuthentication()
  End Sub
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreate()
  End Sub

  Public Overrides Sub Load()
    MyBase.Load()
    BRLoad()
  End Sub

  Public Overrides Sub Save()
    With ApplicationSettings
      Try
        .BeginTransaction()
        Dim bIsNewYN As Boolean = Me.IsNew

        BRBeforeSave(bIsNewYN)
        MyBase.Save()
        BRAfterSave(bIsNewYN)
        .CommitTransaction()
      Catch Exception As Exception
        .RollbackTransaction()
        Throw Exception
      End Try
    End With
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreate()
    BRAuthenticationTypeInd(Me.AuthenticationStatusInd, True, False)
    Me.SubscriberProfileProductAuthenticationBranch.Enabled = True
    Me.SubscriberProfileProductAuthenticationPurpose.Enabled = True
    BRSetDefaultSubscriberProfileProductAuthenticationBranch()
    BRSetDefaultSubscriberProfileProductAuthenticationPurpose()
    BRSetIDNoVerificationYN()
  End Sub

  Private Sub BRLoad()
    BRAuthenticationTypeInd(Me.AuthenticationStatusInd, True, False)
  End Sub

  Private Sub BRBeforeSave(ByVal IsNewYN As Boolean)
    If IsNewYN Then
      Me.AuthenticationDate.Value = System.DateTime.Now
      ' If ActionedBySystemUser is not empty, then the XDSAdministration Services has populated this value.
      If ActionedBySystemUser.IsEmpty Then
        Me.ActionedBySystemUser.Instance = SystemUser.GetLoggedInSystemUser(ApplicationSettings)
      End If
    End If
  End Sub

  Private Sub BRAfterSave(ByVal IsNewYN As Boolean)
    If IsNewYN Then
      Me.SubscriberProfileProductAuthenticationBranch.Enabled = False
      Me.SubscriberProfileProductAuthenticationPurpose.Enabled = False
    End If
  End Sub

  Private Sub BRAuthenticationTypeInd(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If PerformRulesYN Then
      Dim oSubscriberProfile As SubscriberProfile = XDSAdministrationBL.SubscriberProfile.GetSubscriberProfile(Me._SubscriberID.Value, ApplicationSettings)

      Select Case Element.ValueAsObject
        Case Constants.SubscriberAuthentication.AuthenticationTypes.Primary
          Me.RequiredAuthenticatedPerc.Value = oSubscriberProfile.AuthenticationPrimaryRequiredAuthenticatedPerc.Value
        Case Constants.SubscriberAuthentication.AuthenticationTypes.Secondary
          Me.RequiredAuthenticatedPerc.Value = oSubscriberProfile.AuthenticationSecondaryRequiredAuthenticatedPerc.Value
      End Select
    End If
  End Sub

    'Private Sub BRConsumerAccountAgeMessage()
    '  Dim oSubscriber As Subscriber = Me.Subscriber.Instance
    '  Dim oXDSAdministrationRoot As XDSAdministrationRoot = New XDSAdministrationRootRelationship(ApplicationSettings).NewInstance
    '  Dim oDBList As DBList = oXDSAdministrationRoot._vwConsumerAccountAge_OwnMany.GridList
    '  Dim oDataTable As DataTable
    '  Dim oDataRow As DataRow
    '  Dim sMessage As String = ""
    '  Dim oQuery As Text.StringBuilder = New Text.StringBuilder

    '  oDBList.AndFilters.Add(New ListFilter("ConsumerID", "[A0vwConsumerAccountAge].[ConsumerID]", Me._ConsumerID.Value, DataType.Long, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
    '  oDBList.AndFilters.Add(New ListFilter("RecordStatusInd", "[A0vwConsumerAccountAge].[RecordStatusInd]", Constants.RecordStatus.Active, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
    '  oDBList.AndFilters.Add(New ListFilter("SubscriberGroupCode", "[A0vwConsumerAccountAge].[SubscriberGroupCode]", oSubscriber._SubscriberGroupCode.Value, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
    '      oDataTable = oDBList.DataTable

    '  For Each oDataRow In oDataTable.Rows
    '    If SHBS.General.ZeroIfDBNull(oDataRow("NoOfYears")) = 0 AndAlso SHBS.General.ZeroIfDBNull(oDataRow("NoOfMonths")) < 6 Then
    '      sMessage &= "Consumer's " & oDataRow("SubscriberName").ToString() & " account is less than 6 months old" & vbNewLine
    '    Else
    '              sMessage &= "Consumer's " & oDataRow("SubscriberName").ToString() & " account is " & oDataRow("NoOfYears").ToString() & " year(s) and " & oDataRow("NoOfMonths").ToString() & " month(s) old" & vbNewLine
    '    End If
    '  Next

    '  'oQuery.Append("select SubscriberName" & vbNewLine)
    '  'oQuery.Append("  from Subscriber" & vbNewLine)
    '  'oQuery.Append(" where SubscriberID not in (select SubscriberID" & vbNewLine)
    '  'oQuery.Append("                              from ConsumerAccount" & vbNewLine)
    '  'oQuery.Append("                             where ConsumerID = " & ApplicationSettings.QueryBuilder.ToSQL(Me._ConsumerID.Value) & vbNewLine)
    '  'oQuery.Append("                               and RecordStatusInd = " & ApplicationSettings.QueryBuilder.ToSQL(Constants.RecordStatus.Active) & ")" & vbNewLine)
    '  'oQuery.Append("   and SubscriberGroupCode = " & ApplicationSettings.QueryBuilder.ToSQL(oSubscriber._SubscriberGroupCode.Value))

    '  'oDataTable = ApplicationSettings.ResultQuery(oQuery.ToString())
    '  'For Each oDataRow In oDataTable.Rows
    '  '  sMessage &= "Consumer does not have any " & oDataRow("SubscriberName").ToString() & " accounts" & vbNewLine
    '  'Next

    '  Me.ConsumerAccountAgeMessage.Value = sMessage
    'End Sub



    Private Sub BRConsumerAccountAgeMessage()
        Dim cn As SqlClient.SqlConnection
        Dim oSubscriber As Subscriber = Me.Subscriber.Instance
        Dim oXDSAdministrationRoot As XDSAdministrationRoot = New XDSAdministrationRootRelationship(ApplicationSettings).NewInstance
        Dim oDBList As DBList = oXDSAdministrationRoot._vwConsumerAccountAge_OwnMany.GridList
        Dim oDataTable As DataTable
        Dim oDataRow As DataRow
        Dim sMessage As String = ""
        Dim oQuery As Text.StringBuilder = New Text.StringBuilder
        'cn = ApplicationSettings.Connection
        cn = New SqlConnection(AdminDBConnectionstring.Value())

        'oDBList.AndFilters.Add(New ListFilter("ConsumerID", "[A0vwConsumerAccountAge].[ConsumerID]", Me._ConsumerID.Value, DataType.Long, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
        'oDBList.AndFilters.Add(New ListFilter("RecordStatusInd", "[A0vwConsumerAccountAge].[RecordStatusInd]", Constants.RecordStatus.Active, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
        'oDBList.AndFilters.Add(New ListFilter("SubscriberGroupCode", "[A0vwConsumerAccountAge].[SubscriberGroupCode]", oSubscriber._SubscriberGroupCode.Value, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
        'oDataTable = oDBList.DataTable

        Dim strSQL As String
        strSQL = "Select * from vwConsumerAccountAge Where ConsumerID = " & Me._ConsumerID.Value & " AND [RecordStatusInd] = 'A' AND [SubscriberGroupCode] = '" & oSubscriber._SubscriberGroupCode.Value & "'"
        Dim sqlcmd As SqlCommand = New SqlCommand(strSQL, cn)
        Dim sqlda As SqlDataAdapter = New SqlDataAdapter(sqlcmd)
        Dim ds As DataSet = New DataSet()
        sqlda.Fill(ds)

        For Each oDataRow In ds.Tables(0).Rows
            If SHBS.General.ZeroIfDBNull(oDataRow("NoOfYears")) = 0 AndAlso SHBS.General.ZeroIfDBNull(oDataRow("NoOfMonths")) < 6 Then
                sMessage &= "Consumer's " & oDataRow("SubscriberName").ToString() & " account is less than 6 months old" & vbNewLine
            Else
                sMessage &= "Consumer's " & oDataRow("SubscriberName").ToString() & " account is " & oDataRow("NoOfYears").ToString() & " year(s) and " & oDataRow("NoOfMonths").ToString() & " month(s) old" & vbNewLine
            End If
        Next



        Me.ConsumerAccountAgeMessage.Value = sMessage
    End Sub


  Private Function BRRepeatAuthenticationMessage(ByVal SubscriberAuthentication As SubscriberAuthentication, ByVal SubscriberProfileProductAuthenticationBranch As SubscriberProfileProductAuthenticationBranch, ByVal FailedYN As Boolean) As String
    Dim oSubscriber As Subscriber = SubscriberAuthentication.Subscriber.Instance
    Dim oConsumer As Consumer = SubscriberAuthentication.Consumer.Instance

    If Not FailedYN Then
      Return " and has passed authentication on the " & SubscriberAuthentication.AuthenticationDate.FormattedValue & " at the " & oSubscriber.SubscriberName.Value & " branch: " & SubscriberProfileProductAuthenticationBranch.SubscriberBranchDescription.Value
    End If
    Return oConsumer._TitleCode.ValueAsObject & " " & oConsumer.FirstName.Value & " " & oConsumer.Surname.Value & " has failed authentication on the " & SubscriberAuthentication.AuthenticationDate.FormattedValue & " at the " & oSubscriber.SubscriberName.Value & " branch (" & SubscriberProfileProductAuthenticationBranch.SubscriberBranchDescription.Value & ")"
  End Function

  Private Sub BRCheckTypeOfAuthentication()
    Dim oSubscriber As Subscriber = Me.Subscriber.Instance
    Dim oSubscriberProfileProductAuthenticationBranch As SubscriberProfileProductAuthenticationBranch = Me.SubscriberProfileProductAuthenticationBranch.Instance
    Dim bSetupPrimaryAuthentication As Boolean = True
    Dim sRepeatAuthenticationMessage As String = ""

    ' Perform Branch Checks
    If oSubscriberProfileProductAuthenticationBranch.IsHighDisputeInd.Value = Constants.YesNo.Yes Or _
       oSubscriberProfileProductAuthenticationBranch.IsHighFraudInd.Value = Constants.YesNo.Yes Then
      bSetupPrimaryAuthentication = False
    End If

    ' Perform Consumer Checks
    Dim oSubscriberAuthentication As SubscriberAuthentication = XDSAdministrationBL.SubscriberAuthentication.GetPreviousSubscriberAuthentication(Me._SubscriberID.Value, Me.SubscriberAuthenticationID.Value, ApplicationSettings)
    If Not oSubscriberAuthentication Is Nothing Then
      Me.PreviousSubscriberAuthentication.Instance = oSubscriberAuthentication

      oSubscriberProfileProductAuthenticationBranch = oSubscriberAuthentication.SubscriberProfileProductAuthenticationBranch.Instance
      Select Case oSubscriberAuthentication.AuthenticationStatusInd.Value
        Case Constants.SubscriberAuthentication.AuthenticationStatuses.Authenticated
          sRepeatAuthenticationMessage = BRRepeatAuthenticationMessage(oSubscriberAuthentication, oSubscriberProfileProductAuthenticationBranch, False)
                    oSubscriberAuthentication = oSubscriberAuthentication.PreviousSubscriberAuthentication.Instance()
          If Not oSubscriberAuthentication Is Nothing Then
            Select Case oSubscriberAuthentication.AuthenticationStatusInd.Value
              Case Constants.SubscriberAuthentication.AuthenticationStatuses.NotAuthenticated
                oSubscriberProfileProductAuthenticationBranch = oSubscriberAuthentication.SubscriberProfileProductAuthenticationBranch.Instance
                sRepeatAuthenticationMessage = BRRepeatAuthenticationMessage(oSubscriberAuthentication, oSubscriberProfileProductAuthenticationBranch, True)
                bSetupPrimaryAuthentication = False
            End Select
          End If
        Case Constants.SubscriberAuthentication.AuthenticationStatuses.NotAuthenticated
          sRepeatAuthenticationMessage = BRRepeatAuthenticationMessage(oSubscriberAuthentication, oSubscriberProfileProductAuthenticationBranch, True)
          bSetupPrimaryAuthentication = False
      End Select
    End If

        'BRPerformIDNoVerification()

    If bSetupPrimaryAuthentication Then
      BRSetupPrimaryAuthentication()
    Else
      Me.RepeatAuthenticationMessage.Value = sRepeatAuthenticationMessage
      BRSetupSecondaryAuthentication()
    End If
    Me.IgnoreCacheOnce = True
    Me.Load()
  End Sub

  Private Sub BRGetReferenceNo()
    Dim sReferenceNo As String = "A"

    Select Case Me.AuthenticationStatusInd.Value
      Case Constants.SubscriberAuthentication.AuthenticationStatuses.NoConsumerFound
        sReferenceNo = "B"
    End Select

    Me.ReferenceNo.Value = sReferenceNo & Me.SubscriberAuthenticationID.Value.ToString()
  End Sub

  Private Sub BRGetEncryptedReferenceNo()
    Const sSEPARATOR As String = "|"
    Dim oSubscriberConsumerEnquiry As SubscriberConsumerEnquiry = Me.SubscriberConsumerEnquiry.Instance
    Dim sIDNo As String = oSubscriberConsumerEnquiry.ResultIDNo.Value
    Dim nChecksumEven As Integer = 0
    Dim nChecksumOdd As Integer = 0
    Dim nChecksumTotal As Integer = 0
    Dim sReferenceNo As String = sIDNo & sSEPARATOR & Me.AuthenticationStatusInd.Value & sSEPARATOR & Me.AuthenticatedPerc.Value.ToString()
    Dim sChar As Char
    Dim nCounter As Integer = 0

    For Each sChar In sIDNo.ToCharArray()
      If nCounter Mod 2 = 0 Then
        nChecksumEven += Convert.ToInt32(Char.GetNumericValue(sChar))
      Else
        nChecksumOdd += Convert.ToInt32(Char.GetNumericValue(sChar))
      End If
      nCounter = nCounter + 1
    Next
    nChecksumTotal = nChecksumEven + nChecksumOdd + 100

    sReferenceNo = sReferenceNo & sSEPARATOR & nChecksumEven.ToString() & nChecksumOdd.ToString() & nChecksumTotal.ToString()

    Dim oEncryption As sembleWare.Security.Encryption = New sembleWare.Security.Encryption(Constants.AuthenticationHashString)

    Me.EncryptedReferenceNo.Value = oEncryption.Encrypt(sReferenceNo)
  End Sub

  Private Sub BRSetDefaultSubscriberProfileProductAuthenticationBranch()
    Dim oSubscriberProfile As SubscriberProfile = XDSAdministrationBL.SubscriberProfile.GetSubscriberProfile(Me._SubscriberID.Value, ApplicationSettings)
    Dim oList As List = oSubscriberProfile._SubscriberProfileProductAuthenticationBranch_OwnMany.GridList
    Dim oDataTable As DataTable = oList.DataTable
    Dim oSubscriberProfileProductAuthenticationBranch As SubscriberProfileProductAuthenticationBranch = oSubscriberProfile.SubscriberProfileProductAuthenticationBranch_OwnMany.NewInstance()

    Me.SubscriberProfileProductAuthenticationBranch.Instance = Nothing
    If oDataTable.Rows.Count = 1 Then
      oSubscriberProfileProductAuthenticationBranch.SubscriberBranchCode.Load(oDataTable.Rows(0)("SubscriberBranchCode"))
      oSubscriberProfileProductAuthenticationBranch.Load()

      Me.SubscriberProfileProductAuthenticationBranch.Instance = oSubscriberProfileProductAuthenticationBranch
    End If
  End Sub

  Private Sub BRSetDefaultSubscriberProfileProductAuthenticationPurpose()
    Dim oSubscriberProfile As SubscriberProfile = XDSAdministrationBL.SubscriberProfile.GetSubscriberProfile(Me._SubscriberID.Value, ApplicationSettings)
    Dim oList As List = oSubscriberProfile._SubscriberProfileProductAuthenticationPurpose_OwnMany.GridList
    Dim oDataTable As DataTable = oList.DataTable
    Dim oSubscriberProfileProductAuthenticationPurpose As SubscriberProfileProductAuthenticationPurpose = oSubscriberProfile.SubscriberProfileProductAuthenticationPurpose_OwnMany.NewInstance()

    Me.SubscriberProfileProductAuthenticationPurpose.Instance = Nothing
    If oDataTable.Rows.Count = 1 Then
      oSubscriberProfileProductAuthenticationPurpose.AuthenticationPurposeID.Load(oDataTable.Rows(0)("AuthenticationPurposeID"))
      oSubscriberProfileProductAuthenticationPurpose.Load()

      Me.SubscriberProfileProductAuthenticationPurpose.Instance = oSubscriberProfileProductAuthenticationPurpose
    End If
  End Sub

  Private Sub BRSetIDNoVerificationYN()
    Dim oSubscriberProfile As SubscriberProfile = XDSAdministrationBL.SubscriberProfile.GetSubscriberProfile(Me._SubscriberID.Value, ApplicationSettings)

    Me.IDNoVerificationYN.Value = False
    Select Case oSubscriberProfile.AuthenticationDefaultIDVerificationSelectionInd.Value
      Case Constants.SubscriberProfile.DefaultIDVerificationSelectionTypes.Include
        Me.IDNoVerificationYN.Value = True
    End Select
  End Sub

  Private Sub BRSetupPrimaryAuthentication()
    Me.AuthenticationTypeInd.Value = Constants.SubscriberAuthentication.AuthenticationTypes.Primary
    Me.Save()
    BRGetReferenceNo()
    Me.Save()

    SubscriberAuthentication.InsertDefaultQuestions(Me._SubscriberID.Value, Me.SubscriberAuthenticationID.Value, Me._ActionedBySystemUserID.Value, ApplicationSettings)
  End Sub

  Private Sub BRSetupSecondaryAuthentication()
    Me.AuthenticationTypeInd.Value = Constants.SubscriberAuthentication.AuthenticationTypes.Secondary
    Me.Save()
    BRGetReferenceNo()
    Me.Save()

    SubscriberAuthentication.InsertDefaultQuestions(Me._SubscriberID.Value, Me.SubscriberAuthenticationID.Value, Me._ActionedBySystemUserID.Value, ApplicationSettings)
  End Sub

  Private Sub BRPerformIDNoVerification()
    If Me.IDNoVerificationYN.Value Then
      If Not Me.Consumer.IsEmpty Then
        Dim oConsumer As Consumer = Me.Consumer.Instance
        Dim oSubscriber As Subscriber = Me.Subscriber.Instance
        Dim oSubscriberHomeAffairsEnquiry As SubscriberHomeAffairsEnquiry = oSubscriber._SubscriberHomeAffairsEnquiry_OwnMany.NewInstance

        oSubscriberHomeAffairsEnquiry.Product.Instance = XDSAdministrationBL.Product.GetProduct(Constants.Product.Records.TraceConsumerIdentityVerification, ApplicationSettings)
        oSubscriberHomeAffairsEnquiry.IDNo.ValueAsObject = oConsumer.IDNo.ValueAsObject
        oSubscriberHomeAffairsEnquiry.Surname.ValueAsObject = oConsumer.Surname.ValueAsObject
        oSubscriberHomeAffairsEnquiry.FirstName.ValueAsObject = oConsumer.FirstName.ValueAsObject
        oSubscriberHomeAffairsEnquiry.SecondName.ValueAsObject = oConsumer.SecondName.ValueAsObject
        oSubscriberHomeAffairsEnquiry.BirthDate.ValueAsObject = oConsumer.BirthDate.ValueAsObject
        oSubscriberHomeAffairsEnquiry.Match()

        Me.UpdateSubscriberHomeAffairsEnquiry(oSubscriberHomeAffairsEnquiry)
        Select Case oSubscriberHomeAffairsEnquiry.EnquiryResultInd.Value
          Case Constants.SubscriberConsumerEnquiry.Results.RecordFound
            Me.HomeAffairs.Instance = oSubscriberHomeAffairsEnquiry.GetSubscriberHomeAffairsEnquiryResult().HomeAffairs.Instance
        End Select
        Me.Save()
      End If
    End If
  End Sub
#End Region

#Region " Public Methods"
  Public Sub UpdateSubscriberConsumerEnquiry(ByVal SubscriberConsumerEnquiry As SubscriberConsumerEnquiry)
    Select Case SubscriberConsumerEnquiry.EnquiryResultInd.Value
      Case Constants.SubscriberConsumerEnquiry.Results.RecordFound
        Dim oSystemUserProfile As SystemUserProfile = XDSAdministrationBL.SystemUserProfile.GetLoggedInSystemUserProfile(ApplicationSettings)

        If Not oSystemUserProfile.ConfirmPurchaseYN.Value Then
          Dim oSubscriberConsumerEnquiryResult As SubscriberConsumerEnquiryResult = SubscriberConsumerEnquiry.GetSubscriberConsumerEnquiryResult()
          oSubscriberConsumerEnquiryResult.SelectRecord()
          Me.Consumer.Instance = oSubscriberConsumerEnquiryResult.Consumer.Instance
        End If
    End Select

    Me.Product.Instance = SubscriberConsumerEnquiry.Product.Instance
    Me.SubscriberConsumerEnquiry.Instance = SubscriberConsumerEnquiry
    Me.Save()
  End Sub

  Public Sub UpdateSubscriberConsumerEnquiry(ByVal SubscriberConsumerEnquiryResult As SubscriberConsumerEnquiryResult)
    Dim oSubscriberConsumerEnquiry As SubscriberConsumerEnquiry = SubscriberConsumerEnquiryResult.SubscriberConsumerEnquiry.Instance
    UpdateSubscriberConsumerEnquiry(oSubscriberConsumerEnquiry)
    Me.Consumer.Instance = SubscriberConsumerEnquiryResult.Consumer.Instance
    Me.Save()
  End Sub

  Public Sub UpdateSubscriberHomeAffairsEnquiry(ByVal SubscriberHomeAffairsEnquiry As SubscriberHomeAffairsEnquiry)
    Select Case SubscriberHomeAffairsEnquiry.EnquiryResultInd.Value
      Case Constants.SubscriberHomeAffairsEnquiry.Results.RecordFound
        Dim oSystemUserProfile As SystemUserProfile = XDSAdministrationBL.SystemUserProfile.GetLoggedInSystemUserProfile(ApplicationSettings)

        If Not oSystemUserProfile.ConfirmPurchaseYN.Value Then
          Dim oSubscriberHomeAffairsEnquiryResult As SubscriberHomeAffairsEnquiryResult = SubscriberHomeAffairsEnquiry.GetSubscriberHomeAffairsEnquiryResult()
          oSubscriberHomeAffairsEnquiryResult.SelectRecord()
          Me.HomeAffairs.Instance = oSubscriberHomeAffairsEnquiryResult.HomeAffairs.Instance
        End If
    End Select

    Me.Product.Instance = SubscriberHomeAffairsEnquiry.Product.Instance
    Me.SubscriberHomeAffairsEnquiry.Instance = SubscriberHomeAffairsEnquiry
    Me.Save()
  End Sub

  Public Sub UpdateSubscriberHomeAffairsEnquiry(ByVal SubscriberHomeAffairsEnquiryResult As SubscriberHomeAffairsEnquiryResult)
    Dim oSubscriberHomeAffairsEnquiry As SubscriberHomeAffairsEnquiry = SubscriberHomeAffairsEnquiryResult.SubscriberHomeAffairsEnquiry.Instance
    UpdateSubscriberHomeAffairsEnquiry(oSubscriberHomeAffairsEnquiry)
    Me.HomeAffairs.Instance = SubscriberHomeAffairsEnquiryResult.HomeAffairs.Instance
    Me.Save()
  End Sub

  Public Sub GenerateReport(ByVal ReportImageUrl As String, ByVal ReportImagePath As String)
    Me.Save()

    Dim sMimeType As String
    Dim oProduct As Product = XDSAdministrationBL.Product.GetProduct(Constants.Product.Records.AuthenticationConsumer, ApplicationSettings)

    If Not oProduct Is Nothing Then
      Utility.BRCheckProductReport(oProduct)

      Dim oReportingServicesReport As ReportingServicesReport = Utility.BRGetProductReportingServicesReport(XDSAdministrationBL.SubscriberProfile.GetSubscriberProfile(Me._SubscriberID.Value, ApplicationSettings), oProduct)
      Dim oReportAuthenticationAuditDetail As ReportAuthenticationAuditDetail = New ReportAuthenticationAuditDetailRelationship(ApplicationSettings).NewInstance()

      oReportAuthenticationAuditDetail.ReportingServicesReportFormat.Value = SystemUserProfile.GetSystemUserProfile(Me._ActionedBySystemUserID.Value, ApplicationSettings).DefaultTraceEnquiryReportTypeInd.Value
      oReportAuthenticationAuditDetail.ReportingServicesReport.Instance = oReportingServicesReport
      oReportAuthenticationAuditDetail.ReportImageUrl.Value = ReportImageUrl
      oReportAuthenticationAuditDetail.ReportImagePath.Value = ReportImagePath
      oReportAuthenticationAuditDetail._SubscriberID.Value = Me._SubscriberID.Value
      oReportAuthenticationAuditDetail._SubscriberAuthenticationID.Value = Me.SubscriberAuthenticationID.Value
      With oReportAuthenticationAuditDetail.ReportParameterCollection
        .Add("SubscriberID", oReportAuthenticationAuditDetail._SubscriberID.Value, oReportAuthenticationAuditDetail._SubscriberID.ElementType)
        .Add("SubscriberAuthenticationID", oReportAuthenticationAuditDetail._SubscriberAuthenticationID.Value, oReportAuthenticationAuditDetail._SubscriberAuthenticationID.ElementType)
      End With
      Me.ReportFile.Value = oReportAuthenticationAuditDetail.RunReport(sMimeType)
      Me.ReportFileMimeType.Value = sMimeType
    End If
    Me.Save()
  End Sub
#End Region

#Region " Shared Procedures"
  Public Shared Function InsertDefaultQuestions(ByVal SubscriberID As Long, ByVal SubscriberAuthenticationID As Long, ByVal SystemUserID As Long, ByVal ApplicationSettings As ApplicationSettings) As Boolean
    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand

    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spSubscriberAuthenticationQuestion_I_DefaultQuestions"

    oCommand.Parameters.Add("@SubscriberID", SqlDbType.Int)
    oCommand.Parameters.Add("@SubscriberAuthenticationID", SqlDbType.Int)
    oCommand.Parameters.Add("@Username", SqlDbType.VarChar, 50)

    oCommand.Parameters("@SubscriberID").Value = SubscriberID
    oCommand.Parameters("@SubscriberAuthenticationID").Value = SubscriberAuthenticationID
    oCommand.Parameters("@Username").Value = SystemUser.GetSystemUser(SystemUserID, ApplicationSettings).Username.Value

    ApplicationSettings.ActionQuery(oCommand)
  End Function

  Public Shared Function ValidateAnswers(ByVal SubscriberID As Long, ByVal SubscriberAuthenticationID As Long, ByVal SystemUserID As Long, ByVal ApplicationSettings As ApplicationSettings) As Boolean
    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand

    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spSubscriberAuthenticationQuestion_U_ValidateAnswers"

    oCommand.Parameters.Add("@SubscriberID", SqlDbType.Int)
    oCommand.Parameters.Add("@SubscriberAuthenticationID", SqlDbType.Int)
    oCommand.Parameters.Add("@Username", SqlDbType.VarChar, 50)

    oCommand.Parameters("@SubscriberID").Value = SubscriberID
    oCommand.Parameters("@SubscriberAuthenticationID").Value = SubscriberAuthenticationID
    oCommand.Parameters("@Username").Value = SystemUser.GetSystemUser(SystemUserID, ApplicationSettings).Username.Value

    ApplicationSettings.ActionQuery(oCommand)
  End Function

  Public Shared Function GetPreviousSubscriberAuthentication(ByVal SubscriberID As Long, ByVal SubscriberAuthenticationID As Long, ByVal ApplicationSettings As ApplicationSettings) As SubscriberAuthentication
    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand

    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spSubscriberAuthentication_S_PreviousAuthentication"

    oCommand.Parameters.Add("@SubscriberID", SqlDbType.Int)
    oCommand.Parameters.Add("@SubscriberAuthenticationID", SqlDbType.Int)
    oCommand.Parameters.Add("@SystemUserID", SqlDbType.Int)
    oCommand.Parameters.Add("@PreviousSubscriberID", SqlDbType.Int)
    oCommand.Parameters.Add("@PreviousSubscriberAuthenticationID", SqlDbType.Int)

    oCommand.Parameters("@PreviousSubscriberID").Direction = ParameterDirection.Output
    oCommand.Parameters("@PreviousSubscriberAuthenticationID").Direction = ParameterDirection.Output

    oCommand.Parameters("@SubscriberID").Value = SubscriberID
    oCommand.Parameters("@SubscriberAuthenticationID").Value = SubscriberAuthenticationID
    oCommand.Parameters("@SystemUserID").Value = GlobalSettings.SystemUser.LoggedInSystemUserID

    ApplicationSettings.ActionQuery(oCommand)

    If Not oCommand.Parameters("@PreviousSubscriberID").Value = 0 AndAlso Not oCommand.Parameters("@PreviousSubscriberAuthenticationID").Value Then
      Dim oSubscriberAuthentication As SubscriberAuthentication = New SubscriberAuthenticationRelationship(ApplicationSettings).NewInstance
      oSubscriberAuthentication._SubscriberID.Load(oCommand.Parameters("@PreviousSubscriberID").Value)
      oSubscriberAuthentication.SubscriberAuthenticationID.Load(oCommand.Parameters("@PreviousSubscriberAuthenticationID").Value)

      oSubscriberAuthentication.Load()

      Return oSubscriberAuthentication
    End If

    Return Nothing
  End Function
#End Region
End Class 'sembleWare: Part

Public Class SubscriberAuthenticationRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberAuthenticationID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberAuthentication(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberAuthenticationID.ForeignElement = CType(Instance, SubscriberAuthentication).SubscriberAuthenticationID
    _SubscriberID.ForeignElement = CType(Instance, SubscriberAuthentication)._SubscriberID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberAuthentication")
    oList.Columns.Add(New ListColumn("SubscriberAuthenticationID", "[A0SubscriberAuthentication].[SubscriberAuthenticationID]", "SubscriberAuthenticationID", DataType.Long, Nothing, True, -1, False, 100, "Subscriber Authentication ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberAuthentication].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("ReferenceNo", "[A0SubscriberAuthentication].[ReferenceNo]", "ReferenceNo", DataType.String, Nothing, False, 0, True, 100, "Reference No"))
    oList.Columns.Add(New ListColumn("AuthenticationDate", "[A0SubscriberAuthentication].[AuthenticationDate]", "AuthenticationDate", DataType.DateTime, "g", False, -2, True, 100, "Authentication Date"))
    oList.Columns.Add(New ListColumn("AuthenticationTypeInd", "[A0SubscriberAuthentication].[AuthenticationTypeInd]", "AuthenticationTypeInd", DataType.String, Nothing, False, 0, True, 100, "Authentication Type"))
    oList.Columns.Add(New ListColumn("AuthenticationStatusInd", "[A0SubscriberAuthentication].[AuthenticationStatusInd]", "AuthenticationStatusInd", DataType.String, Nothing, False, 0, True, 100, "Authentication Status"))
    oList.Columns.Add(New ListColumn("RequiredAuthenticatedPerc", "[A0SubscriberAuthentication].[RequiredAuthenticatedPerc]", "RequiredAuthenticatedPerc", DataType.Decimal, "##0.00", False, 0, True, 100, "Required Authenticated %"))
    oList.Columns.Add(New ListColumn("AuthenticatedPerc", "[A0SubscriberAuthentication].[AuthenticatedPerc]", "AuthenticatedPerc", DataType.Decimal, "##0.00", False, 0, True, 100, "Authenticated %"))
    oList.Columns.Add(New ListColumn("FullName", "isnull([A1ActionedBySystemUser].[FirstName], '') + ' ' + isnull([A1ActionedBySystemUser].[Surname], '')", "FullName", DataType.String, Nothing, False, 0, True, 200, "Actioned By"))
    oList.SelectStatement = "select"
    oList.FromClause = "([SubscriberAuthentication]  [A0SubscriberAuthentication]" + _
           "    join [SystemUser]  [A1ActionedBySystemUser] on [A0SubscriberAuthentication].[ActionedBySystemUserID] = [A1ActionedBySystemUser].[SystemUserID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function





  Public Function DropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberAuthentication")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberAuthentication].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberAuthenticationID", "[A0SubscriberAuthentication].[SubscriberAuthenticationID]", "SubscriberAuthenticationID", DataType.Long, Nothing, True, -1, True, 100, "Subscriber Authentication ID"))
    oList.Columns.Add(New ListColumn("ReferenceNo", "[A0SubscriberAuthentication].[ReferenceNo]", "ReferenceNo", DataType.String, Nothing, False, 0, True, 200, "Reference No"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberAuthentication]  [A0SubscriberAuthentication]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
