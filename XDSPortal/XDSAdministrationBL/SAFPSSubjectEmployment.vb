Imports sembleWare.Runtime
Imports System
Public Class SAFPSSubjectEmployment 'sembleWare: Part
    Inherits CustomDBPart

#Region " Elements & Relationships"

    'sembleWare: Elements Start - Do Not Modify
    Public ReadOnly SAFPSSubjectEmploymentID As New IdentityElement("SAFPSSubjectEmploymentID", Me, True, Nothing, Nothing)
    Public ReadOnly EmployerName As New StringElement("EmployerName", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly EmployerTelNo As New StringElement("EmployerTelNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly CompanyName As New StringElement("CompanyName", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly CompanyNo As New StringElement("CompanyNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly Occupation As New StringElement("Occupation", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly FromDate As New DateTimeElement("FromDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
    Public ReadOnly ToDate As New DateTimeElement("ToDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
    Public ReadOnly ExternalSubjectSourceID As New IntegerElement("ExternalSubjectSourceID", Me, False, False, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly ExternalEmploymentSourceID As New IntegerElement("ExternalEmploymentSourceID", Me, False, False, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly RecordStatusInd As New IndicatorElement("RecordStatusInd", Me, False, False, False, True, True, True, "Record Status", Nothing, SAFPSSubjectEmployment.RecordStatusIndOptions, "A")
    Public ReadOnly DeletedReason As New StringElement("DeletedReason", Me, False, True, True, True, False, 500, Nothing, Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly LastUpdatedDate As New DateTimeElement("LastUpdatedDate", Me, False, False, True, True, True, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
    Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.Include, False, False, True, Me)
    Public ReadOnly Loader As Relationship = New LoaderRelationship("Loader", Nothing, RelationshipType.Include, False, False, True, Me)
    Public ReadOnly SAFPSSubject As Relationship = New SAFPSSubjectRelationship("SAFPSSubject", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
    'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

    'sembleWare: Foreign Items Start - Do Not Modify
    Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, False)
    Public ReadOnly _LoaderID As New IntegerElement("LoaderID", Me, False)
    Public ReadOnly _SAFPSSubjectID As New IntegerElement("SAFPSSubjectID", Me, True)
    Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
    Public ReadOnly _Loader As LoaderRelationship = Loader
    Public ReadOnly _SAFPSSubject As SAFPSSubjectRelationship = SAFPSSubject
    'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

    'sembleWare: Indicator Options Start - Do Not Modify
    Private Shared moRecordStatusIndOptions As IndicatorOptions
    Public Shared ReadOnly Property RecordStatusIndOptions() As IndicatorOptions
        Get
            If moRecordStatusIndOptions Is Nothing Then
                moRecordStatusIndOptions = New IndicatorOptions
                moRecordStatusIndOptions.Add("A", "Active")
                moRecordStatusIndOptions.Add("D", "Deleted")
            End If
            Return moRecordStatusIndOptions
        End Get
    End Property
    'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

    Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

        'sembleWare: Constructor Start - Do Not Modify
        MyBase.New("SAFPSSubjectEmployment", ApplicationSettings)
        _Subscriber._SubscriberID.LocalElement = _SubscriberID
        _Loader._LoaderID.LocalElement = _LoaderID
        _SAFPSSubject._SAFPSSubjectID.LocalElement = _SAFPSSubjectID
        'sembleWare: Constructor End - Do Not Modify
        Me.DeleteType = CustomDBPart.enumDeleteType.ChangeRecordStatus
    End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region "base Overrides"
    Public Overrides Sub Save()
        With ApplicationSettings
            Try
                .BeginTransaction()
                Dim bIsNewYN As Boolean = Me.IsNew
                BRBeforeSave(bIsNewYN)
                MyBase.Save()
                .CommitTransaction()
            Catch Exception As Exception
                .RollbackTransaction()
                Throw Exception
            End Try
        End With
    End Sub

#End Region

#Region "Business Rules"
    Private Sub BRBeforeSave(ByVal IsNewYN As Boolean)
        Me.LastUpdatedDate.Value = System.DateTime.Now
    End Sub
#End Region

End Class 'sembleWare: Part

Public Class SAFPSSubjectEmploymentRelationship 'sembleWare: Relationship
    Inherits Relationship

#Region " Relationship Code"

    'sembleWare: Relationship Start - Do Not Modify
    Public ReadOnly _SAFPSSubjectEmploymentID As ForeignKey = New ForeignKey(Me)
    Public ReadOnly _SAFPSSubjectID As ForeignKey = New ForeignKey(Me)

    Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
        MyBase.New(ApplicationSettings)
    End Sub

    Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
        MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
    End Sub

    Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
        MyBase.New(Name, Caption, OwnedByName, Owner)
    End Sub

    Public Overrides Function CreateObject() As sembleWare.Runtime.Part
        Return New SAFPSSubjectEmployment(ApplicationSettings)
    End Function

    Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
        _SAFPSSubjectEmploymentID.ForeignElement = CType(Instance, SAFPSSubjectEmployment).SAFPSSubjectEmploymentID
        _SAFPSSubjectID.ForeignElement = CType(Instance, SAFPSSubjectEmployment)._SAFPSSubjectID
    End Sub

    'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

    Public Function GridList() As sembleWare.Runtime.List
        'sembleWare: List Declaration Start - Do Not Modify
        Dim oList As DBList = New DBList(Me, "A0SAFPSSubjectEmployment")
        oList.Columns.Add(New ListColumn("SAFPSSubjectEmploymentID", "[A0SAFPSSubjectEmployment].[SAFPSSubjectEmploymentID]", "SAFPSSubjectEmploymentID", DataType.Long, Nothing, True, 0, False, 100, "SAFPSSubject Employment ID"))
        oList.Columns.Add(New ListColumn("SAFPSSubjectID", "[A0SAFPSSubjectEmployment].[SAFPSSubjectID]", "SAFPSSubjectID", DataType.Integer, Nothing, True, 0, False, 100, "SAFPSSubject ID"))
        oList.Columns.Add(New ListColumn("EmployerName", "[A0SAFPSSubjectEmployment].[EmployerName]", "EmployerName", DataType.String, Nothing, False, 0, True, 100, "Employer Name"))
        oList.Columns.Add(New ListColumn("EmployerTelNo", "[A0SAFPSSubjectEmployment].[EmployerTelNo]", "EmployerTelNo", DataType.String, Nothing, False, 0, True, 100, "Employer Tel No"))
        oList.Columns.Add(New ListColumn("CompanyName", "[A0SAFPSSubjectEmployment].[CompanyName]", "CompanyName", DataType.String, Nothing, False, 0, True, 100, "Company Name"))
        oList.Columns.Add(New ListColumn("CompanyNo", "[A0SAFPSSubjectEmployment].[CompanyNo]", "CompanyNo", DataType.String, Nothing, False, 0, True, 100, "Company No"))
        oList.Columns.Add(New ListColumn("Occupation", "[A0SAFPSSubjectEmployment].[Occupation]", "Occupation", DataType.String, Nothing, False, 0, True, 100, "Occupation"))
        oList.Columns.Add(New ListColumn("FromDate", "[A0SAFPSSubjectEmployment].[FromDate]", "FromDate", DataType.DateTime, "d", False, 0, True, 100, "From Date"))
        oList.Columns.Add(New ListColumn("ToDate", "[A0SAFPSSubjectEmployment].[ToDate]", "ToDate", DataType.DateTime, "d", False, 0, True, 100, "To Date"))
        oList.SelectStatement = "select"
        oList.FromClause = "[SAFPSSubjectEmployment]  [A0SAFPSSubjectEmployment]"
        oList.WhereClause = ""
        oList.ApplyRelationshipLimits()
        'sembleWare: List Declaration End - Do Not Modify
        oList.AndFilters.Add(New ListFilter("RecordStatusInd", "[A0SAFPSSubjectEmployment].[RecordStatusInd]", Constants.RecordStatus.Active, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
        Return oList
    End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
