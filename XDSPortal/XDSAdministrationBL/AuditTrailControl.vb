Imports sembleWare.Runtime
Imports System
Public Class AuditTrailControl 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly SQLTableName As New StringElement("SQLTableName", Me, True, True, True, True, True, 250, "Table Name", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ActiveYN As New BooleanElement("ActiveYN", Me, False, True, True, True, True, "Active?", Nothing, True, "Yes;No")
  Public ReadOnly AuditInsertYN As New BooleanElement("AuditInsertYN", Me, False, True, True, True, True, "Audit Insert?", Nothing, False, "Yes;No")
  Public ReadOnly AuditUpdateYN As New BooleanElement("AuditUpdateYN", Me, False, True, True, True, True, "Audit Update?", Nothing, True, "Yes;No")
  Public ReadOnly AuditDeleteYN As New BooleanElement("AuditDeleteYN", Me, False, True, True, True, True, "Audit Delete?", Nothing, True, "Yes;No")
  Public ReadOnly DatabaseUsername As New StringElement("DatabaseUsername", Me, False, True, True, False, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly DatabasePassword As New StringElement("DatabasePassword", Me, False, True, True, False, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("AuditTrailControl", ApplicationSettings)
    'sembleWare: Constructor End - Do Not Modify
    Me.ForceUpperCaseKeys = False
  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"

#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Shared Procedures"
  Public Shared Sub PopulateAuditTrailControl(ByVal ConnectionString As String)
    Dim oManager As SHBS.AuditTrail.Manager = New SHBS.AuditTrail.Manager(ConnectionString)
    oManager.Synchronize(SHBS.AuditTrail.eAuditFieldNullableType.Null)
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class AuditTrailControlRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SQLTableName As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New AuditTrailControl(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SQLTableName.ForeignElement = CType(Instance, AuditTrailControl).SQLTableName
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0AuditTrailControl")
    oList.Columns.Add(New ListColumn("SQLTableName", "[A0AuditTrailControl].[SQLTableName]", "SQLTableName", DataType.String, Nothing, True, 1, True, 200, "Table Name"))
    oList.Columns.Add(New ListColumn("ActiveYN", "[A0AuditTrailControl].[ActiveYN]", "ActiveYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Active?"))
    oList.Columns.Add(New ListColumn("AuditInsertYN", "[A0AuditTrailControl].[AuditInsertYN]", "AuditInsertYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Audit Insert?"))
    oList.Columns.Add(New ListColumn("AuditUpdateYN", "[A0AuditTrailControl].[AuditUpdateYN]", "AuditUpdateYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Audit Update?"))
    oList.Columns.Add(New ListColumn("AuditDeleteYN", "[A0AuditTrailControl].[AuditDeleteYN]", "AuditDeleteYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Audit Delete?"))
    oList.SelectStatement = "select"
    oList.FromClause = "[AuditTrailControl]  [A0AuditTrailControl]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
