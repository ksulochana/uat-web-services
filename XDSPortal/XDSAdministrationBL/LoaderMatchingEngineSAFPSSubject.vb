Imports sembleWare.Runtime
Imports System
Public Class LoaderMatchingEngineSAFPSSubject 'sembleWare: Part
    Inherits CustomDBPart

#Region " Elements & Relationships"

    'sembleWare: Elements Start - Do Not Modify
    Public ReadOnly RecordID As New IntegerElement("RecordID", Me, True, True, True, True, True, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly ExternalSourceID As New IntegerElement("ExternalSourceID", Me, False, True, True, True, False, "ID No", "ExternalSourceID", Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly SAFPSSubjectID As New IntegerElement("SAFPSSubjectID", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
    Public ReadOnly IsPossibleNameConflictYN As New BooleanElement("IsPossibleNameConflictYN", Me, False, True, True, True, False, "Is Possible Name Conflict?", Nothing, False, "Yes;No")
    Public ReadOnly IsPossibleDuplicateRecordYN As New BooleanElement("IsPossibleDuplicateRecordYN", Me, False, True, True, True, False, "Is Possible Duplicate Record?", Nothing, False, "Yes;No")
    Public ReadOnly LoaderMatchingEngine As Relationship = New LoaderMatchingEngineRelationship("LoaderMatchingEngine", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
    Public ReadOnly LoaderMatchingEngineSAFPSSubjectResult_OwnMany As Relationship = New LoaderMatchingEngineSAFPSSubjectResultRelationship("LoaderMatchingEngineSAFPSSubjectResult", Nothing, "LoaderMatchingEngineSAFPSSubject", Me)
    'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

    'sembleWare: Foreign Items Start - Do Not Modify
    Public ReadOnly _LoaderMatchingEngineID As New IntegerElement("LoaderMatchingEngineID", Me, True)
    Public ReadOnly _LoaderMatchingEngine As LoaderMatchingEngineRelationship = LoaderMatchingEngine
    Public ReadOnly _LoaderMatchingEngineSAFPSSubjectResult_OwnMany As LoaderMatchingEngineSAFPSSubjectResultRelationship = LoaderMatchingEngineSAFPSSubjectResult_OwnMany
    'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

    'sembleWare: Indicator Options Start - Do Not Modify
    'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

    Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

        'sembleWare: Constructor Start - Do Not Modify
        MyBase.New("LoaderMatchingEngineSAFPSSubject", ApplicationSettings)
        _LoaderMatchingEngine._LoaderMatchingEngineID.LocalElement = _LoaderMatchingEngineID
        'sembleWare: Constructor End - Do Not Modify

    End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class LoaderMatchingEngineSAFPSSubjectRelationship 'sembleWare: Relationship
    Inherits Relationship

#Region " Relationship Code"

    'sembleWare: Relationship Start - Do Not Modify
    Public ReadOnly _RecordID As ForeignKey = New ForeignKey(Me)
    Public ReadOnly _LoaderMatchingEngineID As ForeignKey = New ForeignKey(Me)

    Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
        MyBase.New(ApplicationSettings)
    End Sub

    Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
        MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
    End Sub

    Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
        MyBase.New(Name, Caption, OwnedByName, Owner)
    End Sub

    Public Overrides Function CreateObject() As sembleWare.Runtime.Part
        Return New LoaderMatchingEngineSAFPSSubject(ApplicationSettings)
    End Function

    Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
        _RecordID.ForeignElement = CType(Instance, LoaderMatchingEngineSAFPSSubject).RecordID
        _LoaderMatchingEngineID.ForeignElement = CType(Instance, LoaderMatchingEngineSAFPSSubject)._LoaderMatchingEngineID
    End Sub

    'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
