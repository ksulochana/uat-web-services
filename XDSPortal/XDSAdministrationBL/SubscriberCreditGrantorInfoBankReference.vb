Imports sembleWare.Runtime
Imports System
Public Class SubscriberCreditGrantorInfoBankReference 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly SubscriberCreditGrantorInfoBankReferenceID As New IdentityElement("SubscriberCreditGrantorInfoBankReferenceID", Me, True, Nothing, Nothing)
  Public ReadOnly BankName As New StringElement("BankName", Me, False, True, True, True, True, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly BranchName As New StringElement("BranchName", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly BranchNo As New StringElement("BranchNo", Me, False, True, True, True, False, 10, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AccountNo As New StringElement("AccountNo", Me, False, True, True, True, True, 30, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly AccountTypeInd As New IndicatorElement("AccountTypeInd", Me, False, False, True, True, True, True, "Account Type", Nothing, SubscriberCreditGrantorInfoBankReference.AccountTypeIndOptions, Nothing)
  Public ReadOnly SubscriberCreditGrantorAffordability As Relationship = New SubscriberCreditGrantorInfoRelationship("SubscriberCreditGrantorAffordability", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberCreditGrantorID As New IntegerElement("SubscriberCreditGrantorID", Me, True)
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _SubscriberCreditGrantorAffordability As SubscriberCreditGrantorInfoRelationship = SubscriberCreditGrantorAffordability
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moAccountTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property AccountTypeIndOptions() As IndicatorOptions
    Get
      If moAccountTypeIndOptions Is Nothing Then
        moAccountTypeIndOptions = New IndicatorOptions
        moAccountTypeIndOptions.Add("S", "Savings")
        moAccountTypeIndOptions.Add("H", "Cheque")
        moAccountTypeIndOptions.Add("C", "Credit Card")
        moAccountTypeIndOptions.Add("I", "Investment")
      End If
      Return moAccountTypeIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberCreditGrantorInfoBankReference", ApplicationSettings)
    _SubscriberCreditGrantorAffordability._SubscriberCreditGrantorID.LocalElement = _SubscriberCreditGrantorID
    _SubscriberCreditGrantorAffordability._SubscriberID.LocalElement = _SubscriberID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class SubscriberCreditGrantorInfoBankReferenceRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberCreditGrantorInfoBankReferenceID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SubscriberCreditGrantorID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberCreditGrantorInfoBankReference(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberCreditGrantorInfoBankReferenceID.ForeignElement = CType(Instance, SubscriberCreditGrantorInfoBankReference).SubscriberCreditGrantorInfoBankReferenceID
    _SubscriberCreditGrantorID.ForeignElement = CType(Instance, SubscriberCreditGrantorInfoBankReference)._SubscriberCreditGrantorID
    _SubscriberID.ForeignElement = CType(Instance, SubscriberCreditGrantorInfoBankReference)._SubscriberID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberCreditGrantorInfoBankReference")
    oList.Columns.Add(New ListColumn("SubscriberCreditGrantorInfoBankReferenceID", "[A0SubscriberCreditGrantorInfoBankReference].[SubscriberCreditGrantorInfoBankReferenceID]", "SubscriberCreditGrantorInfoBankReferenceID", DataType.Long, Nothing, True, 0, False, 100, "Subscriber Credit Grantor Info Bank Reference ID"))
    oList.Columns.Add(New ListColumn("SubscriberCreditGrantorID", "[A0SubscriberCreditGrantorInfoBankReference].[SubscriberCreditGrantorID]", "SubscriberCreditGrantorID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber Credit Grantor ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberCreditGrantorInfoBankReference].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("BankName", "[A0SubscriberCreditGrantorInfoBankReference].[BankName]", "BankName", DataType.String, Nothing, False, 1, True, 100, "Bank Name"))
    oList.Columns.Add(New ListColumn("BranchName", "[A0SubscriberCreditGrantorInfoBankReference].[BranchName]", "BranchName", DataType.String, Nothing, False, 0, True, 100, "Branch Name"))
    oList.Columns.Add(New ListColumn("BranchNo", "[A0SubscriberCreditGrantorInfoBankReference].[BranchNo]", "BranchNo", DataType.String, Nothing, False, 0, True, 100, "Branch No"))
    oList.Columns.Add(New ListColumn("AccountNo", "[A0SubscriberCreditGrantorInfoBankReference].[AccountNo]", "AccountNo", DataType.String, Nothing, False, 0, True, 100, "Account No"))
    oList.Columns.Add(New ListColumn("AccountTypeInd", "[A0SubscriberCreditGrantorInfoBankReference].[AccountTypeInd]", "AccountTypeInd", DataType.String, Nothing, False, 0, True, 100, "Account Type"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberCreditGrantorInfoBankReference]  [A0SubscriberCreditGrantorInfoBankReference]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
