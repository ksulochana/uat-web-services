Imports sembleWare.Runtime
Imports System
Public Class SubscriberProfileProduct 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly StatusInd As New IndicatorElement("StatusInd", Me, False, False, True, True, True, True, "Status", Nothing, SubscriberProfileProduct.StatusIndOptions, "A")
  Public ReadOnly OverrideDefaultPointValueYN As New BooleanElement("OverrideDefaultPointValueYN", Me, False, True, True, True, True, "Override Default Point Value?", Nothing, False, "Yes;No")
  Public ReadOnly OverridePointValue As New DecimalElement("OverridePointValue", Me, False, True, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly OverrideReportingServicesReportYN As New BooleanElement("OverrideReportingServicesReportYN", Me, False, True, True, True, True, "Override Report?", Nothing, False)
  Public ReadOnly OverrideReportingServicesReportDisplayInd As New IndicatorElement("OverrideReportingServicesReportDisplayInd", Me, False, False, True, True, True, False, "Report Display", Nothing, SubscriberProfileProduct.OverrideReportingServicesReportDisplayIndOptions, Nothing)
  Public ReadOnly OverrideReportingServicesReport As Relationship = New ReportingServicesReportRelationship("OverrideReportingServicesReport", "Override Report", RelationshipType.Include, False, True, True, Me)
  Public ReadOnly Product As Relationship = New ProductRelationship("Product", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly SubscriberProfile As Relationship = New SubscriberProfileRelationship("SubscriberProfile", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _ProductID As New IntegerElement("ProductID", Me, True)
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _OverrideReportingServicesReportID As New IntegerElement("OverrideReportingServicesReportID", Me, False)
  Public ReadOnly _OverrideReportingServicesReport As ReportingServicesReportRelationship = OverrideReportingServicesReport
  Public ReadOnly _Product As ProductRelationship = Product
  Public ReadOnly _SubscriberProfile As SubscriberProfileRelationship = SubscriberProfile
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property StatusIndOptions() As IndicatorOptions
    Get
      If moStatusIndOptions Is Nothing Then
        moStatusIndOptions = New IndicatorOptions
        moStatusIndOptions.Add("A", "Allowed")
        moStatusIndOptions.Add("D", "Denied")
      End If
      Return moStatusIndOptions
    End Get
  End Property
  Private Shared moOverrideReportingServicesReportDisplayIndOptions As IndicatorOptions
  Public Shared ReadOnly Property OverrideReportingServicesReportDisplayIndOptions() As IndicatorOptions
    Get
      If moOverrideReportingServicesReportDisplayIndOptions Is Nothing Then
        moOverrideReportingServicesReportDisplayIndOptions = New IndicatorOptions
        moOverrideReportingServicesReportDisplayIndOptions.Add("E", "Existing Window")
        moOverrideReportingServicesReportDisplayIndOptions.Add("N", "New Window")
      End If
      Return moOverrideReportingServicesReportDisplayIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberProfileProduct", ApplicationSettings)
    _OverrideReportingServicesReport._ReportingServicesReportID.LocalElement = _OverrideReportingServicesReportID
    _Product._ProductID.LocalElement = _ProductID
    _SubscriberProfile._SubscriberID.LocalElement = _SubscriberID
    AddHandler OverrideDefaultPointValueYN.Changed, AddressOf OverrideDefaultPointValueYN_Changed
    AddHandler OverrideReportingServicesReportYN.Changed, AddressOf OverrideReportingServicesReportYN_Changed
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub OverrideDefaultPointValueYN_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BROverrideDefaultPointValueYN(Element, True, True)
  End Sub

  Private Sub OverrideReportingServicesReportYN_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BROverrideReportingServicesReportYN(Element, True, True)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreateLoad()
  End Sub

  Public Overrides Sub Load()
    MyBase.Load()
    BRCreateLoad()
  End Sub

  Public Overrides Sub Save()
    With ApplicationSettings
      Try
        .BeginTransaction()
        Dim bIsNewYN As Boolean = Me.IsNew
        Dim bMustPersistYN As Boolean = Me.MustPersist
        Dim sOldStatusInd As String = IIf(bIsNewYN, Me.StatusInd.Value, Me.StatusInd.LoadValueAsObject)
        Dim bOldOverrideDefaultPointValueYN As Boolean = IIf(bIsNewYN, Me.OverrideDefaultPointValueYN.Value, Me.OverrideDefaultPointValueYN.LoadValueAsObject)
        Dim dOverridePointValue As Decimal = IIf(bIsNewYN, Me.OverridePointValue.Value, Me.OverridePointValue.LoadValueAsObject)

        MyBase.Save()
        BRAfterSave(bIsNewYN)
        BRAddToHistory(bMustPersistYN, IIf(bIsNewYN, "Added", "Chanaged"), sOldStatusInd, Me.StatusInd.Value, bOldOverrideDefaultPointValueYN, Me.OverrideDefaultPointValueYN.Value, dOverridePointValue, Me.OverridePointValue.Value)
        .CommitTransaction()
      Catch Exception As Exception
        .RollbackTransaction()
        Throw Exception
      End Try
    End With
  End Sub

  Public Overrides Sub Delete()
    With ApplicationSettings
      Try
        .BeginTransaction()
        BRAddToHistory(True, "Deleted", Me.StatusInd.Value, Me.StatusInd.Value, Me.OverrideDefaultPointValueYN.Value, Me.OverrideDefaultPointValueYN.Value, Me.OverridePointValue.Value, Me.OverridePointValue.Value)
        MyBase.Delete()
        .CommitTransaction()
      Catch Exception As Exception
        .RollbackTransaction()
        Throw Exception
      End Try
    End With
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRCreateLoad()
    BROverrideDefaultPointValueYN(Me.OverrideDefaultPointValueYN, True, False)
    BROverrideReportingServicesReportYN(Me.OverrideReportingServicesReportYN, True, False)
  End Sub

  Private Sub BRAfterSave(ByVal IsNewYN As Boolean)
    If IsNewYN Then
      InsertSystemUserProfileProduct(Me._SubscriberID.Value, Me._ProductID.Value, ApplicationSettings)
    Else
      UpdateSystemUserProfileProduct(Me._SubscriberID.Value, Me._ProductID.Value, ApplicationSettings)
    End If
  End Sub

  Private Sub BROverrideDefaultPointValueYN(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If SetEnableDisableYN Then
      Me.OverridePointValue.Enabled = Element.ValueAsObject
      Me.OverridePointValue.Mandatory = Element.ValueAsObject
    End If

    If PerformRulesYN Then
      Me.OverridePointValue.SetToNullValue()
    End If
  End Sub

  Private Sub BROverrideReportingServicesReportYN(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If SetEnableDisableYN Then
      Me.OverrideReportingServicesReport.Enabled = Element.ValueAsObject
      Me.OverrideReportingServicesReport.Mandatory = Element.ValueAsObject
      Me.OverrideReportingServicesReportDisplayInd.Enabled = Element.ValueAsObject
      Me.OverrideReportingServicesReportDisplayInd.Mandatory = Element.ValueAsObject
    End If

    If PerformRulesYN Then
      If Not Element.ValueAsObject Then
        Me.OverrideReportingServicesReport.Instance = Nothing
        Me.OverrideReportingServicesReportDisplayInd.SetToNullValue()
      End If
    End If
  End Sub

  Private Sub BRAddToHistory(ByVal MustPersistYN As Boolean, ByVal Action As String, ByVal PreviousStatusInd As String, ByVal StatusInd As String, _
                             ByVal PreviousOverrideDefaultPointValueYN As Boolean, ByVal ChangedOverrideDefaultPointValueYN As Boolean, _
                             ByVal PreviousOverridePointValue As Decimal, ByVal ChangedOverridePointValue As Decimal)
    If MustPersistYN Then
      Dim oSubscriberProfile As SubscriberProfile = Me.SubscriberProfile.Instance
      Dim oSubscriberProfileHistory As SubscriberProfileHistory = oSubscriberProfile.SubscriberProfileHistory_OwnMany.NewInstance()

      oSubscriberProfileHistory.ActionDesc.Value = "Product " & Action
      oSubscriberProfileHistory.ActionedOnDate.Value = System.DateTime.Now
      oSubscriberProfileHistory.ActionedByUser.Value = XDSAdministrationBL.GlobalSettings.SystemUser.LoggedInSystemUsername()
      oSubscriberProfileHistory.PreviousStatusInd.Value = PreviousStatusInd
      oSubscriberProfileHistory.ChangedStatusInd.Value = StatusInd
      oSubscriberProfileHistory.PreviousOverrideDefaultPointValueYN.Value = PreviousOverrideDefaultPointValueYN
      oSubscriberProfileHistory.ChangedOverrideDefaultPointValueYN.Value = ChangedOverrideDefaultPointValueYN
      oSubscriberProfileHistory.PreviousOverridePointValue.Value = PreviousOverridePointValue
      oSubscriberProfileHistory.ChangedOverridePointValue.Value = ChangedOverridePointValue

      oSubscriberProfileHistory.Save()
    End If
  End Sub
#End Region

#Region " Shared Procedures"
  Public Shared Function GetSubscriberProfileProduct(ByVal SubscriberID As Long, ByVal ProductID As Integer, ByVal ApplicationSettings As ApplicationSettings) As SubscriberProfileProduct
    Dim oSubscriberProfileProduct As SubscriberProfileProduct = New SubscriberProfileProductRelationship(ApplicationSettings).NewInstance

    oSubscriberProfileProduct._SubscriberID.Load(SubscriberID)
    oSubscriberProfileProduct._ProductID.Load(ProductID)
    oSubscriberProfileProduct.Load()

    Return oSubscriberProfileProduct
  End Function

  Public Shared Sub InsertSystemUserProfileProduct(ByVal SubscriberID As Long, ByVal ProductID As Integer, ByVal ApplicationSettings As ApplicationSettings)
    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand

    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spSubscriberProfileProduct_I_SystemUserProfileProduct"

    oCommand.Parameters.Add("@SubscriberID", SqlDbType.Int)
    oCommand.Parameters.Add("@ProductID", SqlDbType.Int)
    oCommand.Parameters.Add("@Username", SqlDbType.VarChar, 50)

    oCommand.Parameters("@SubscriberID").Value = SubscriberID
    oCommand.Parameters("@ProductID").Value = ProductID
    oCommand.Parameters("@Username").Value = SystemUser.GetLoggedInSystemUser(ApplicationSettings).Username.Value

    ApplicationSettings.ActionQuery(oCommand)
  End Sub

  Public Shared Sub UpdateSystemUserProfileProduct(ByVal SubscriberID As Long, ByVal ProductID As Integer, ByVal ApplicationSettings As ApplicationSettings)
    Dim oCommand As SqlClient.SqlCommand = ApplicationSettings.CreateCommand

    oCommand.CommandType = CommandType.StoredProcedure
    oCommand.CommandText = "spSubscriberProfileProduct_U_SystemUserProfileProduct"

    oCommand.Parameters.Add("@SubscriberID", SqlDbType.Int)
    oCommand.Parameters.Add("@ProductID", SqlDbType.Int)
    oCommand.Parameters.Add("@Username", SqlDbType.VarChar, 50)

    oCommand.Parameters("@SubscriberID").Value = SubscriberID
    oCommand.Parameters("@ProductID").Value = ProductID
    oCommand.Parameters("@Username").Value = SystemUser.GetLoggedInSystemUser(ApplicationSettings).Username.Value

    ApplicationSettings.ActionQuery(oCommand)
  End Sub
#End Region
End Class 'sembleWare: Part

Public Class SubscriberProfileProductRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _ProductID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberProfileProduct(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _ProductID.ForeignElement = CType(Instance, SubscriberProfileProduct)._ProductID
    _SubscriberID.ForeignElement = CType(Instance, SubscriberProfileProduct)._SubscriberID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberProfileProduct")
    oList.Columns.Add(New ListColumn("ProductTypeDesc", "[A2ProductType].[ProductTypeDesc]", "ProductTypeDesc", DataType.String, Nothing, False, 1, True, 100, "Product Type Description"))
    oList.Columns.Add(New ListColumn("ProductDesc", "[A1Product].[ProductDesc]", "ProductDesc", DataType.String, Nothing, False, 2, True, 200, "Product Description"))
    oList.Columns.Add(New ListColumn("DefaultPointValue", "[A1Product].[DefaultPointValue]", "DefaultPointValue", DataType.Decimal, Nothing, False, 0, True, 100, "Default Point Value"))
    oList.Columns.Add(New ListColumn("OverrideDefaultPointValueYN", "[A0SubscriberProfileProduct].[OverrideDefaultPointValueYN]", "OverrideDefaultPointValueYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Override Default Point Value?"))
    oList.Columns.Add(New ListColumn("OverridePointValue", "[A0SubscriberProfileProduct].[OverridePointValue]", "OverridePointValue", DataType.Decimal, Nothing, False, 0, True, 100, "Override Point Value"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberProfileProduct].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("ProductID", "[A0SubscriberProfileProduct].[ProductID]", "ProductID", DataType.Integer, Nothing, True, 0, False, 100, "Product ID"))
    oList.Columns.Add(New ListColumn("StatusInd", "[A0SubscriberProfileProduct].[StatusInd]", "StatusInd", DataType.String, Nothing, False, 0, True, 100, "Status"))
    oList.SelectStatement = "select"
    oList.FromClause = "([SubscriberProfileProduct]  [A0SubscriberProfileProduct]" + _
           "    join ([Product]  [A1Product]" + _
           "    join [ProductType]  [A2ProductType] on [A1Product].[ProductTypeID] = [A2ProductType].[ProductTypeID]) on [A0SubscriberProfileProduct].[ProductID] = [A1Product].[ProductID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function








  Public Function DropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberProfileProduct")
    oList.Columns.Add(New ListColumn("ProductTypeDesc", "[A2ProductType].[ProductTypeDesc]", "ProductTypeDesc", DataType.String, Nothing, False, 1, True, 200, "Product Type Description"))
    oList.Columns.Add(New ListColumn("ProductDesc", "[A1Product].[ProductDesc]", "ProductDesc", DataType.String, Nothing, False, 2, True, 200, "Product Description"))
    oList.Columns.Add(New ListColumn("DefaultPointValue", "[A1Product].[DefaultPointValue]", "DefaultPointValue", DataType.Decimal, Nothing, False, 0, False, 100, "Default Point Value"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberProfileProduct].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("ProductID", "[A0SubscriberProfileProduct].[ProductID]", "ProductID", DataType.Integer, Nothing, True, 0, False, 100, "Product ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "([SubscriberProfileProduct]  [A0SubscriberProfileProduct]" + _
           "    join ([Product]  [A1Product]" + _
           "    join [ProductType]  [A2ProductType] on [A1Product].[ProductTypeID] = [A2ProductType].[ProductTypeID]) on [A0SubscriberProfileProduct].[ProductID] = [A1Product].[ProductID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
