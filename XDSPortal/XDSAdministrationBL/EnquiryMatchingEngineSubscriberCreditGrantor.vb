Imports sembleWare.Runtime
Imports System
Public Class EnquiryMatchingEngineSubscriberCreditGrantor 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly EnquiryMatchingEngineSubscriberCreditGrantorID As New IdentityElement("EnquiryMatchingEngineSubscriberCreditGrantorID", Me, True, Nothing, Nothing)
  Public ReadOnly ReferenceNo As New StringElement("ReferenceNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly EnquiryDate As New DateTimeElement("EnquiryDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly IDNo As New StringElement("IDNo", Me, False, True, True, True, False, 13, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PassportNo As New StringElement("PassportNo", Me, False, True, True, True, False, 16, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ExternalReference As New StringElement("ExternalReference", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SubscriberID As New IntegerElement("SubscriberID", Me, False, False, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly SubscriberCreditGrantorID As New IntegerElement("SubscriberCreditGrantorID", Me, False, False, True, True, False, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly EnquiryMatchingEngine As Relationship = New EnquiryMatchingEngineRelationship("EnquiryMatchingEngine", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly EnquiryMatchingEngineSubscriberCreditGrantorResult_OwnMany As Relationship = New EnquiryMatchingEngineSubscriberCreditGrantorResultRelationship("EnquiryMatchingEngineSubscriberCreditGrantorResult", Nothing, "EnquiryMatchingEngineSubscriberCreditGrantor", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _EnquiryMatchingEngineID As New IntegerElement("EnquiryMatchingEngineID", Me, True)
  Public ReadOnly _EnquiryMatchingEngine As EnquiryMatchingEngineRelationship = EnquiryMatchingEngine
  Public ReadOnly _EnquiryMatchingEngineSubscriberCreditGrantorResult_OwnMany As EnquiryMatchingEngineSubscriberCreditGrantorResultRelationship = EnquiryMatchingEngineSubscriberCreditGrantorResult_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items`

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("EnquiryMatchingEngineSubscriberCreditGrantor", ApplicationSettings)
    _EnquiryMatchingEngine._EnquiryMatchingEngineID.LocalElement = _EnquiryMatchingEngineID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class EnquiryMatchingEngineSubscriberCreditGrantorRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _EnquiryMatchingEngineSubscriberCreditGrantorID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _EnquiryMatchingEngineID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New EnquiryMatchingEngineSubscriberCreditGrantor(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _EnquiryMatchingEngineSubscriberCreditGrantorID.ForeignElement = CType(Instance, EnquiryMatchingEngineSubscriberCreditGrantor).EnquiryMatchingEngineSubscriberCreditGrantorID
    _EnquiryMatchingEngineID.ForeignElement = CType(Instance, EnquiryMatchingEngineSubscriberCreditGrantor)._EnquiryMatchingEngineID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
