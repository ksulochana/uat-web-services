Imports sembleWare.Runtime
Imports System
Public Class CommercialNameSoundEx 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly RegistrationNo As New StringElement("RegistrationNo", Me, False, True, True, True, False, 14, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RegistrationNoOld As New StringElement("RegistrationNoOld", Me, False, True, True, True, False, 14, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RegistrationNoConverted As New StringElement("RegistrationNoConverted", Me, False, True, True, True, False, 14, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CommercialTypeDate As New DateTimeElement("CommercialTypeDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly CommercialName As New StringElement("CommercialName", Me, False, False, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CommercialShortName As New StringElement("CommercialShortName", Me, False, False, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CommercialTranslatedName As New StringElement("CommercialTranslatedName", Me, False, False, True, True, False, 150, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RegistrationDate As New DateTimeElement("RegistrationDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly BusinessStartDate As New DateTimeElement("BusinessStartDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly TaxNo As New StringElement("TaxNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CommercialNamePrefix1 As New StringElement("CommercialNamePrefix1", Me, False, False, True, True, False, 1, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CommercialNamePrefix2 As New StringElement("CommercialNamePrefix2", Me, False, False, True, True, False, 2, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CommercialNamePrefix3 As New StringElement("CommercialNamePrefix3", Me, False, False, True, True, False, 3, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CommercialNamePrefix4 As New StringElement("CommercialNamePrefix4", Me, False, False, True, True, False, 4, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CommercialNamePrefix5 As New StringElement("CommercialNamePrefix5", Me, False, False, True, True, False, 5, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CommercialNamePrefix6 As New StringElement("CommercialNamePrefix6", Me, False, False, True, True, False, 6, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CommercialNamePrefix7 As New StringElement("CommercialNamePrefix7", Me, False, False, True, True, False, 7, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CommercialNamePrefix8 As New StringElement("CommercialNamePrefix8", Me, False, False, True, True, False, 8, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CommercialNamePrefix9 As New StringElement("CommercialNamePrefix9", Me, False, False, True, True, False, 9, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly CommercialNamePrefix10 As New StringElement("CommercialNamePrefix10", Me, False, False, True, True, False, 10, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly RecordStatusInd As New IndicatorElement("RecordStatusInd", Me, False, False, False, True, True, True, "Record Status", Nothing, CommercialNameSoundEx.RecordStatusIndOptions, "A")
  Public ReadOnly SIC As Relationship = New SICRelationship("SIC", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly Commercial As Relationship = New CommercialRelationship("Commercial", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly CommercialNames As Relationship = New CommercialNameRelationship("CommercialNames", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SICCode As New StringElement("SICCode", Me, False)
  Public ReadOnly _CommercialID As New IntegerElement("CommercialID", Me, True)
  Public ReadOnly _CommercialNameID As New IntegerElement("CommercialNameID", Me, True)
  Public ReadOnly _SIC As SICRelationship = SIC
  Public ReadOnly _Commercial As CommercialRelationship = Commercial
  Public ReadOnly _CommercialNames As CommercialNameRelationship = CommercialNames
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moRecordStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property RecordStatusIndOptions() As IndicatorOptions
    Get
      If moRecordStatusIndOptions Is Nothing Then
        moRecordStatusIndOptions = New IndicatorOptions
        moRecordStatusIndOptions.Add("A", "Active")
        moRecordStatusIndOptions.Add("D", "Deleted")
      End If
      Return moRecordStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("CommercialNameSoundEx", ApplicationSettings)
    _SIC._SICCode.LocalElement = _SICCode
    _Commercial._CommercialID.LocalElement = _CommercialID
    _CommercialNames._CommercialNameID.LocalElement = _CommercialNameID
    _CommercialNames._CommercialID.LocalElement = _CommercialID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class CommercialNameSoundExRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _CommercialID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _CommercialNameID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New CommercialNameSoundEx(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _CommercialID.ForeignElement = CType(Instance, CommercialNameSoundEx)._CommercialID
    _CommercialNameID.ForeignElement = CType(Instance, CommercialNameSoundEx)._CommercialNameID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
