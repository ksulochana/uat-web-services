Imports sembleWare.Runtime
Imports System
Public Class SubscriberPropertyDeedEnquiryResult 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly TitleDeedNo As New StringElement("TitleDeedNo", Me, False, False, True, True, False, 13, "ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly DetailsViewedYN As New BooleanElement("DetailsViewedYN", Me, False, False, True, True, True, "Details Viewed?", Nothing, False, "Yes;No")
  Public ReadOnly DetailsViewedDate As New DateTimeElement("DetailsViewedDate", Me, False, False, True, True, False, Nothing, Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly PropertyDeedSelectedYN As New BooleanElement("PropertyDeedSelectedYN", Me, False, True, True, True, True, "PropertyDeed Selected?", Nothing, False, "Yes;No")
  Public ReadOnly SubscriberPropertyDeedEnquiry As Relationship = New SubscriberPropertyDeedEnquiryRelationship("SubscriberPropertyDeedEnquiry", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly PropertyDeed As Relationship = New PropertyDeedRelationship("PropertyDeed", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _SubscriberPropertyDeedEnquiryID As New IntegerElement("SubscriberPropertyDeedEnquiryID", Me, True)
  Public ReadOnly _PropertyDeedID As New IntegerElement("PropertyDeedID", Me, True)
  Public ReadOnly _SubscriberPropertyDeedEnquiry As SubscriberPropertyDeedEnquiryRelationship = SubscriberPropertyDeedEnquiry
  Public ReadOnly _PropertyDeed As PropertyDeedRelationship = PropertyDeed
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberPropertyDeedEnquiryResult", ApplicationSettings)
    _SubscriberPropertyDeedEnquiry._SubscriberID.LocalElement = _SubscriberID
    _SubscriberPropertyDeedEnquiry._SubscriberPropertyDeedEnquiryID.LocalElement = _SubscriberPropertyDeedEnquiryID
    _PropertyDeed._PropertyDeedID.LocalElement = _PropertyDeedID
    AddHandler DetailsViewedYN.Changed, AddressOf DetailsViewedYN_Changed
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub DetailsViewedYN_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"

  Public Sub SelectRecord()
    With ApplicationSettings
      Try
        .BeginTransaction()
        Me.PropertyDeedSelectedYN.Value = True
        Me.DetailsViewedYN.Value = True
        Me.DetailsViewedDate.Value = System.DateTime.Now
        Me.Save()

        Dim oPropertyDeed As PropertyDeed = Me.PropertyDeed.Instance
        Dim OSubscriberPropertyDeedEnquiry As SubscriberPropertyDeedEnquiry = Me.SubscriberPropertyDeedEnquiry.Instance
        OSubscriberPropertyDeedEnquiry.EnquiryResultInd.Value = Constants.SubscriberPropertyDeedEnquiry.Results.RecordSelected

        OSubscriberPropertyDeedEnquiry.ResultTitleDeedNo.Value = oPropertyDeed.TitleDeedNo.Value

        OSubscriberPropertyDeedEnquiry.Save()
        .CommitTransaction()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
    End With
  End Sub

#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class SubscriberPropertyDeedEnquiryResultRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SubscriberPropertyDeedEnquiryID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _PropertyDeedID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberPropertyDeedEnquiryResult(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberID.ForeignElement = CType(Instance, SubscriberPropertyDeedEnquiryResult)._SubscriberID
    _SubscriberPropertyDeedEnquiryID.ForeignElement = CType(Instance, SubscriberPropertyDeedEnquiryResult)._SubscriberPropertyDeedEnquiryID
    _PropertyDeedID.ForeignElement = CType(Instance, SubscriberPropertyDeedEnquiryResult)._PropertyDeedID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberPropertyDeedEnquiryResult")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberPropertyDeedEnquiryResult].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberPropertyDeedEnquiryID", "[A0SubscriberPropertyDeedEnquiryResult].[SubscriberPropertyDeedEnquiryID]", "SubscriberPropertyDeedEnquiryID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber Property Deed Enquiry ID"))
    oList.Columns.Add(New ListColumn("PropertyDeedID", "[A0SubscriberPropertyDeedEnquiryResult].[PropertyDeedID]", "PropertyDeedID", DataType.Integer, Nothing, True, 0, False, 100, "Property Deed ID"))
    oList.Columns.Add(New ListColumn("TitleDeedNo", "[A0SubscriberPropertyDeedEnquiryResult].[TitleDeedNo]", "TitleDeedNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberPropertyDeedEnquiryResult]  [A0SubscriberPropertyDeedEnquiryResult]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function MultipleMatchGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberPropertyDeedEnquiryResult")
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberPropertyDeedEnquiryResult].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("SubscriberPropertyDeedEnquiryID", "[A0SubscriberPropertyDeedEnquiryResult].[SubscriberPropertyDeedEnquiryID]", "SubscriberPropertyDeedEnquiryID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber Property Deed Enquiry ID"))
    oList.Columns.Add(New ListColumn("PropertyDeedID", "[A0SubscriberPropertyDeedEnquiryResult].[PropertyDeedID]", "PropertyDeedID", DataType.Integer, Nothing, True, 0, False, 100, "Property Deed ID"))
    oList.Columns.Add(New ListColumn("TitleDeedNo", "[A0SubscriberPropertyDeedEnquiryResult].[TitleDeedNo]", "TitleDeedNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("DetailsViewedYN", "[A0SubscriberPropertyDeedEnquiryResult].[DetailsViewedYN]", "DetailsViewedYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Details Viewed?"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberPropertyDeedEnquiryResult]  [A0SubscriberPropertyDeedEnquiryResult]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberPropertyDeedEnquiryResult")
    oList.Columns.Add(New ListColumn("PropertyDeedID", "[A0SubscriberPropertyDeedEnquiryResult].[PropertyDeedID]", "PropertyDeedID", DataType.Integer, Nothing, True, 0, False, 100, "Property Deed ID"))
    oList.Columns.Add(New ListColumn("SubscriberPropertyDeedEnquiryID", "[A0SubscriberPropertyDeedEnquiryResult].[SubscriberPropertyDeedEnquiryID]", "SubscriberPropertyDeedEnquiryID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber Property Deed Enquiry ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberPropertyDeedEnquiryResult].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("TitleDeedNo", "[A0SubscriberPropertyDeedEnquiryResult].[TitleDeedNo]", "TitleDeedNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberPropertyDeedEnquiryResult]  [A0SubscriberPropertyDeedEnquiryResult]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
