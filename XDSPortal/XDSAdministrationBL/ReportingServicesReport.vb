Imports SHBS.ReportingServices
Imports SHBS.ReportingServices.ReportEngine
Imports sembleWare.Runtime
Imports System
Imports System.Web.UI.WebControls
Public Class ReportingServicesReport 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly ReportingServicesReportID As New IdentityElement("ReportingServicesReportID", Me, True, "Report ID", Nothing)
  Public ReadOnly ReportName As New StringElement("ReportName", Me, False, False, True, True, True, 100, "Report Name", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ReportDesc As New StringElement("ReportDesc", Me, False, True, True, True, True, 100, "Report Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ReportDetails As New TextElement("ReportDetails", Me, True, True, True, False, Nothing, Nothing, Nothing)
  Public ReadOnly ReportStatusInd As New IndicatorElement("ReportStatusInd", Me, False, False, True, True, True, True, "Report Status", Nothing, ReportingServicesReport.ReportStatusIndOptions, Nothing)
  Public ReadOnly ReportScreenTypeInd As New IndicatorElement("ReportScreenTypeInd", Me, False, False, False, True, True, True, "Report Screen Type", Nothing, ReportingServicesReport.ReportScreenTypeIndOptions, "C")
  Public ReadOnly CustomScreenName As New StringElement("CustomScreenName", Me, False, True, True, True, False, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly IsVisibleYN As New BooleanElement("IsVisibleYN", Me, False, True, True, True, True, "Is Visible?", Nothing, False, "Yes;No")
  Public ReadOnly FileName As New StringElement("FileName", Me, False, False, True, True, True, 100, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly BinaryFile As New BinaryElement("BinaryFile", Me, True, True, True, True, Nothing, Nothing)
  Public ReadOnly UploadFileYN As New BooleanElement("UploadFileYN", Me, False, True, True, False, False, "Upload File?", Nothing, False, "Yes;No")
  Public ReadOnly ReportingServicesReportLabelDescription As New StringElement("ReportingServicesReportLabelDescription", Me, False, True, True, False, False, 250, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ReportingServicesServer As Relationship = New ReportingServicesServerRelationship("ReportingServicesServer", Nothing, RelationshipType.Include, True, True, True, Me)
  Public ReadOnly ReportingServicesFolder As Relationship = New ReportingServicesFolderRelationship("ReportingServicesFolder", Nothing, RelationshipType.Include, True, True, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly ReportingServicesSubReport_OwnMany As Relationship = New ReportingServicesSubReportRelationship("ReportingServicesSubReport", Nothing, "ReportingServicesReport", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _ReportingServicesServerID As New IntegerElement("ReportingServicesServerID", Me, False)
  Public ReadOnly _ReportingServicesFolderPath As New StringElement("ReportingServicesFolderPath", Me, False)
  Public ReadOnly _ReportingServicesServer As ReportingServicesServerRelationship = ReportingServicesServer
  Public ReadOnly _ReportingServicesFolder As ReportingServicesFolderRelationship = ReportingServicesFolder
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  Public ReadOnly _ReportingServicesSubReport_OwnMany As ReportingServicesSubReportRelationship = ReportingServicesSubReport_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moReportStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property ReportStatusIndOptions() As IndicatorOptions
    Get
      If moReportStatusIndOptions Is Nothing Then
        moReportStatusIndOptions = New IndicatorOptions
        moReportStatusIndOptions.Add("A", "Active")
        moReportStatusIndOptions.Add("I", "Inactive")
      End If
      Return moReportStatusIndOptions
    End Get
  End Property
  Private Shared moReportScreenTypeIndOptions As IndicatorOptions
  Public Shared ReadOnly Property ReportScreenTypeIndOptions() As IndicatorOptions
    Get
      If moReportScreenTypeIndOptions Is Nothing Then
        moReportScreenTypeIndOptions = New IndicatorOptions
        moReportScreenTypeIndOptions.Add("N", "None")
        moReportScreenTypeIndOptions.Add("S", "Standard")
        moReportScreenTypeIndOptions.Add("C", "Custom")
      End If
      Return moReportScreenTypeIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("ReportingServicesReport", ApplicationSettings)
    _ReportingServicesServer._ReportingServicesServerID.LocalElement = _ReportingServicesServerID
    _ReportingServicesFolder._ReportingServicesServerID.LocalElement = _ReportingServicesServerID
    _ReportingServicesFolder._ReportingServicesFolderPath.LocalElement = _ReportingServicesFolderPath
    AddHandler ReportScreenTypeInd.Changed, AddressOf ReportScreenTypeInd_Changed
    AddHandler UploadFileYN.Changed, AddressOf UploadFileYN_Changed
    AddHandler ReportingServicesServer.Changed, AddressOf ReportingServicesServer_Changed
    AddHandler ReportingServicesFolder.Changed, AddressOf ReportingServicesFolder_Changed
    'sembleWare: Constructor End - Do Not Modify
    AddHandler ReportingServicesFolder.LimitRelatedList, AddressOf ReportingServicesFolder_LimitRelatedList
    Me.ForceUpperCaseKeys = False
  End Sub

#End Region 'sembleWare: Constructor

#Region " LimitRelatedList Events"
  Private Sub ReportingServicesFolder_LimitRelatedList(ByVal List As List)
    List.LimitByRelatedPart(Me.ReportingServicesServer.Instance, "ReportingServicesServer", True)
  End Sub
#End Region

#Region " Change Events"
  Private Sub ReportScreenTypeInd_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BRReportScreenTypeInd(Element, True, True)
  End Sub

  Private Sub UploadFileYN_Changed(ByVal Element As Element, ByRef CancelChanges As Boolean)
    BRUploadFileYN(Element, True, True)
  End Sub

  Private Sub ReportingServicesServer_Changed(ByVal Relationship As Relationship)
    BRReportingServicesServer(Relationship, True, True)
  End Sub

  Private Sub ReportingServicesFolder_Changed(ByVal Relationship As Relationship)
    BRReportingServicesFolder(Relationship, True, True)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"

  Public Sub DeleteAll()
    With ApplicationSettings
      Try
        .BeginTransaction()
        Dim oXDSAdministrationRoot As XDSAdministrationRoot = Me.XDSAdministrationRoot.Instance
        Dim oList As List = oXDSAdministrationRoot._ReportingServicesReport_OwnMany.GridList
        Dim oDataRow As DataRow

        For Each oDataRow In oList.DataTable.Rows
          Dim oReportingServicesReport As ReportingServicesReport = oXDSAdministrationRoot.ReportingServicesReport_OwnMany.NewInstance()
          oReportingServicesReport.ReportingServicesReportID.Load(oDataRow("ReportingServicesReportID"))
          oReportingServicesReport.Load()

          Dim oList2 As List = oReportingServicesReport._ReportingServicesSubReport_OwnMany.GridList
          Dim oDataRow2 As DataRow

          For Each oDataRow2 In oList2.DataTable.Rows
            Dim oReportingServicesSubReport As ReportingServicesSubReport = oReportingServicesReport.ReportingServicesSubReport_OwnMany.NewInstance()
            oReportingServicesSubReport.ReportingServicesSubReportID.Load(oDataRow2("ReportingServicesSubReportID"))
            oReportingServicesSubReport.Load()
            oReportingServicesSubReport.Delete()
          Next

          oReportingServicesReport.Delete()
        Next
        .CommitTransaction()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
    End With
  End Sub

  Public Sub PublishToReportingServices()
    Me.MessageToDisplay.SetToNullValue()

    Dim oReportingServicesFolder As ReportingServicesFolder = Me.ReportingServicesFolder.Instance
    Dim oReportingServicesServer As ReportingServicesServer = Me.ReportingServicesServer.Instance

    SHBS.ReportingServices.ReportManager.CreateReport(oReportingServicesServer.ServerUrl.Value, oReportingServicesServer.RootFolderPath.Value, oReportingServicesFolder.ReportingServicesFolderPath.Value, Me.ReportName.Value, Me.ReportDesc.Value, Me.BinaryFile.Value, oReportingServicesFolder.DataSourceName.Value)
    Me.MessageToDisplay.Value = "Reporting Services Report Successfully Published to: " & oReportingServicesServer.ReportingServicesServerDesc.Value
  End Sub

  Public Sub RemoveFromReportingServices()
    Me.MessageToDisplay.SetToNullValue()

    Dim oReportingServicesFolder As ReportingServicesFolder = Me.ReportingServicesFolder.Instance
    Dim oReportingServicesServer As ReportingServicesServer = Me.ReportingServicesServer.Instance

    SHBS.ReportingServices.ReportManager.DeleteItem(oReportingServicesServer.ServerUrl.Value, oReportingServicesServer.RootFolderPath.Value, oReportingServicesFolder.ReportingServicesFolderPath.Value, Me.ReportName.Value)
    Me.MessageToDisplay.Value = "Reporting Services Report Successfully Removed from: " & oReportingServicesServer.ReportingServicesServerDesc.Value
  End Sub

#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Create()
    MyBase.Create()
    BRCreate()
  End Sub

  Public Overrides Sub Load()
    MyBase.Load()
    BRLoad()
  End Sub

  Public Overrides Sub Save()
    With ApplicationSettings
      Try
        .BeginTransaction()
        Dim bIsNewYN As Boolean = Me.IsNew
        BRBeforeSave(bIsNewYN)
        MyBase.Save()
        BRAfterSave(bIsNewYN)
        .CommitTransaction()
      Catch Exception As Exception
        .RollbackTransaction()
        Throw Exception
      End Try
    End With
  End Sub

  Public Overrides Sub Delete()
    With ApplicationSettings
      Try
        .BeginTransaction()
        MyBase.Delete()
        BRDelete()
        .CommitTransaction()
      Catch Exception As Exception
        .RollbackTransaction()
        Throw Exception
      End Try
    End With
  End Sub
#End Region

#Region " Methods"
  Public Sub LoadOnReportName(ByVal ReportName As String)
    Dim oXDSAdministrationRoot As XDSAdministrationRoot = New XDSAdministrationRootRelationship(ApplicationSettings).NewInstance()
    Dim oList As List = oXDSAdministrationRoot._ReportingServicesReport_OwnMany.RunReportGridList
    Dim oDataRow As DataRow

    oList.AndFilters.Add(New ListFilter("ReportStatusInd", "ReportStatusInd", Constants.ReportingServicesReport.Statuses.Active, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
    oList.AndFilters.Add(New ListFilter("CustomScreenName", "CustomScreenName", CustomScreenName, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
    For Each oDataRow In oList.DataTable.Rows
      Me.ReportingServicesReportID.Load(oDataRow("ReportingServicesReportID"))
      Me.Load()
      Exit For
    Next
  End Sub

  Public Sub LoadOnCustomScreenName(ByVal CustomScreenName As String)
    Dim oXDSAdministrationRoot As XDSAdministrationRoot = New XDSAdministrationRootRelationship(ApplicationSettings).NewInstance()
    Dim oList As List = oXDSAdministrationRoot._ReportingServicesReport_OwnMany.RunReportGridList
    Dim oDataRow As DataRow

    ' need to cater due to multiple reporting services
    oList.AndFilters.Add(New ListFilter("ReportStatusInd", "ReportStatusInd", Constants.ReportingServicesReport.Statuses.Active, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
    oList.AndFilters.Add(New ListFilter("CustomScreenName", "CustomScreenName", CustomScreenName, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, True))
    For Each oDataRow In oList.DataTable.Rows
      Me.ReportingServicesReportID.Load(oDataRow("ReportingServicesReportID"))
      Me.Load()
      Exit For
    Next
  End Sub

  Public Sub PopulateReportTypeDropDownList(ByVal DropDownList As System.Web.UI.WebControls.DropDownList, ByVal ReportingServicesReportFormat As String)
    Dim oReportingServicesServer As ReportingServicesServer = Me.ReportingServicesServer.Instance
    Dim oExtension As SHBS.ReportingServices.ReportService.Extension
    Dim sSelected As String = ReportingServicesReportFormat
    Dim oReportFormat As ReportFormat

    If DropDownList.SelectedIndex >= 0 AndAlso IsNumeric(DropDownList.SelectedItem.Value) Then
      sSelected = DropDownList.SelectedItem.Value
    End If
    If sSelected = "" Then
      sSelected = ReportEngine.GetReportFormats().ReportFormatPDF.Value
    End If
    DropDownList.Items.Clear()

    For Each oReportFormat In ReportEngine.GetReportFormats().Items
      Dim oListItem As ListItem = New ListItem("Report into " & oReportFormat.Name, oReportFormat.Value)

      DropDownList.Items.Add(oListItem)
      If oListItem.Value = sSelected Then
        oListItem.Selected = True
      End If
    Next
  End Sub

  Public Function GetReportBytes(ByVal ReportParameterCollection As SHBS.ReportingServices.ReportParameterCollection, ByVal ReportFormat As String, ByVal ReportImageUrl As String, ByVal ReportImagePath As String, ByRef MimeType As String) As Byte()
    Dim sMimeType As String = ""
    Dim oReportingServicesServer As ReportingServicesServer = Me.ReportingServicesServer.Instance
    Dim oReportingServicesFolder As ReportingServicesFolder = Me.ReportingServicesFolder.Instance
    Dim oReportEngine As ReportEngine = New ReportEngine(oReportingServicesServer.ServerUrl.Value, _
                                                         oReportingServicesServer.RootFolderPath.Value, _
                                                         oReportingServicesFolder.ReportingServicesFolderPath.Value, _
                                                         Me.ReportName.Value, _
                                                         ReportParameterCollection)

    Return oReportEngine.ExecuteReport(ReportFormat, ReportImageUrl, ReportImagePath, MimeType)
  End Function
#End Region

#Region " Business Rules"
  Private Sub BRCreate()
    Me.UploadFileYN.Enabled = False
    Me.UploadFileYN.Value = True
    BRUploadFileYN(Me.UploadFileYN, True, False)
    BRReportScreenTypeInd(Me.ReportScreenTypeInd, True, False)
    BRReportingServicesReportLabelDescription()
  End Sub

  Private Sub BRLoad()
    BRUploadFileYN(Me.UploadFileYN, True, False)
    BRReportScreenTypeInd(Me.ReportScreenTypeInd, True, False)
    BRReportingServicesReportLabelDescription()
  End Sub

  Private Sub BRBeforeSave(ByVal IsNewYN As Boolean)
    Me.MessageToDisplay.SetToNullValue()
    If IsNewYN Then
      Me.ReportName.Value = System.IO.Path.GetFileNameWithoutExtension(Me.FileName.Value)
    End If
  End Sub

  Private Sub BRAfterSave(ByVal IsNewYN As Boolean)
    If IsNewYN Or Me.UploadFileYN.Value Then
      PublishToReportingServices()
      Me.UploadFileYN.Value = False
      BRUploadFileYN(Me.UploadFileYN, True, False)
    End If
    BRReportingServicesReportLabelDescription()
  End Sub

  Private Sub BRDelete()
    RemoveFromReportingServices()
  End Sub

  Private Sub BRReportingServicesReportLabelDescription()
    Dim sDescription As String = ""

    If Not Me.ReportingServicesFolder.IsEmpty Then
      Dim oReportingServicesFolder As ReportingServicesFolder = Me.ReportingServicesFolder.Instance

      sDescription = oReportingServicesFolder.ReportingServicesFolderLabelDescription.Value & " - "
    Else
      If Not Me.ReportingServicesServer.IsEmpty Then
        Dim oReportingServicesServer As ReportingServicesServer = Me.ReportingServicesServer.Instance

        sDescription = oReportingServicesServer.ReportingServicesServerLabelDescription.Value & " - "
      End If
    End If

    Me.ReportingServicesReportLabelDescription.Value = CreateLabelDescription(sDescription & "Report", Me.ReportDesc)
  End Sub

  Private Sub BRReportScreenTypeInd(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If SetEnableDisableYN Then
      Me.CustomScreenName.Enabled = False
      Me.CustomScreenName.Mandatory = False
      Select Case Element.ValueAsObject
        Case "C"
          Me.CustomScreenName.Enabled = True
          Me.CustomScreenName.Mandatory = True
      End Select
    End If

    If PerformRulesYN Then
      Me.CustomScreenName.SetToNullValue()
    End If
  End Sub

  Private Sub BRUploadFileYN(ByVal Element As Element, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If SetEnableDisableYN Then
      Me.BinaryFile.Mandatory = Element.ValueAsObject
    End If
  End Sub

  Private Sub BRReportingServicesServer(ByVal Relationship As Relationship, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If SetEnableDisableYN Then
      BRReportingServicesReportLabelDescription()
    End If
  End Sub

  Private Sub BRReportingServicesFolder(ByVal Relationship As Relationship, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    If SetEnableDisableYN Then
      BRReportingServicesReportLabelDescription()
    End If
  End Sub
#End Region

#Region " Class ReportParameterCollection"
  Public Class ReportParameterCollection
    Inherits SHBS.ReportingServices.ReportParameterCollection

    Public Shadows Sub Add(ByVal ParameterName As String, ByVal ParameterValue As Object, ByVal ParameterDataType As DataType)
      Dim oValue As Object = System.DBNull.Value

      Select Case ParameterDataType
        Case DataType.DateTime
          If IsDate(ParameterValue) Then
            If Not ParameterValue = New Date Then
              oValue = CType(ParameterValue, DateTime).ToString("MM/dd/yyyy")
            End If
          End If
        Case DataType.String, DataType.Text
          If Not ParameterValue = "" Then
            oValue = ParameterValue
          End If
        Case Else
          oValue = ParameterValue
      End Select
      MyBase.Add(ParameterName, oValue)
    End Sub
  End Class
#End Region
End Class 'sembleWare: Part

Public Class ReportingServicesReportRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _ReportingServicesReportID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New ReportingServicesReport(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _ReportingServicesReportID.ForeignElement = CType(Instance, ReportingServicesReport).ReportingServicesReportID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0ReportingServicesReport")
    oList.Columns.Add(New ListColumn("ReportingServicesReportID", "[A0ReportingServicesReport].[ReportingServicesReportID]", "ReportingServicesReportID", DataType.Long, Nothing, True, 0, False, 100, "Report ID"))
    oList.Columns.Add(New ListColumn("ReportName", "[A0ReportingServicesReport].[ReportName]", "ReportName", DataType.String, Nothing, False, 1, True, 200, "Report Name"))
    oList.Columns.Add(New ListColumn("ReportDesc", "[A0ReportingServicesReport].[ReportDesc]", "ReportDesc", DataType.String, Nothing, False, 0, True, 200, "Report Description"))
    oList.Columns.Add(New ListColumn("ReportStatusInd", "[A0ReportingServicesReport].[ReportStatusInd]", "ReportStatusInd", DataType.String, Nothing, False, 0, True, 100, "Report Status"))
    oList.Columns.Add(New ListColumn("IsVisibleYN", "[A0ReportingServicesReport].[IsVisibleYN]", "IsVisibleYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Is Visible?"))
    oList.SelectStatement = "select"
    oList.FromClause = "[ReportingServicesReport]  [A0ReportingServicesReport]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0ReportingServicesReport")
    oList.Columns.Add(New ListColumn("ReportName", "[A0ReportingServicesReport].[ReportName]", "ReportName", DataType.String, Nothing, False, 1, True, 200, "Report Name"))
    oList.Columns.Add(New ListColumn("ReportDesc", "[A0ReportingServicesReport].[ReportDesc]", "ReportDesc", DataType.String, Nothing, False, 0, True, 200, "Report Description"))
    oList.Columns.Add(New ListColumn("ReportingServicesReportID", "[A0ReportingServicesReport].[ReportingServicesReportID]", "ReportingServicesReportID", DataType.Long, Nothing, True, 0, False, 100, "Report ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "[ReportingServicesReport]  [A0ReportingServicesReport]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function DropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0ReportingServicesReport")
    oList.Columns.Add(New ListColumn("ReportName", "[A0ReportingServicesReport].[ReportName]", "ReportName", DataType.String, Nothing, False, 1, True, 200, "Report Name"))
    oList.Columns.Add(New ListColumn("ReportDesc", "[A0ReportingServicesReport].[ReportDesc]", "ReportDesc", DataType.String, Nothing, False, 0, True, 200, "Report Description"))
    oList.Columns.Add(New ListColumn("ReportingServicesReportID", "[A0ReportingServicesReport].[ReportingServicesReportID]", "ReportingServicesReportID", DataType.Long, Nothing, True, 0, False, 100, "Report ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "[ReportingServicesReport]  [A0ReportingServicesReport]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function RunReportGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0ReportingServicesReport")
    oList.Columns.Add(New ListColumn("ReportingServicesReportID", "[A0ReportingServicesReport].[ReportingServicesReportID]", "ReportingServicesReportID", DataType.Long, Nothing, True, 0, False, 100, "Report ID"))
    oList.Columns.Add(New ListColumn("ReportingServicesServerDesc", "[A1ReportingServicesServer].[ReportingServicesServerDesc]", "ReportingServicesServerDesc", DataType.String, Nothing, False, 1, False, 100, "Server Description"))
    oList.Columns.Add(New ListColumn("ReportingServicesFolderPath", "[A2ReportingServicesFolder].[ReportingServicesFolderPath]", "ReportingServicesFolderPath", DataType.String, Nothing, False, 2, True, 100, "Folder Path"))
    oList.Columns.Add(New ListColumn("ReportName", "[A0ReportingServicesReport].[ReportName]", "ReportName", DataType.String, Nothing, False, 3, True, 200, "Report Name"))
    oList.Columns.Add(New ListColumn("ReportDesc", "[A0ReportingServicesReport].[ReportDesc]", "ReportDesc", DataType.String, Nothing, False, 0, True, 200, "Report Description"))
    oList.Columns.Add(New ListColumn("ReportStatusInd", "[A0ReportingServicesReport].[ReportStatusInd]", "ReportStatusInd", DataType.String, Nothing, False, 0, False, 100, "Report Status"))
    oList.Columns.Add(New ListColumn("ReportDetails", "[A0ReportingServicesReport].[ReportDetails]", "ReportDetails", DataType.Text, Nothing, False, 0, True, 300, "Report Details"))
    oList.Columns.Add(New ListColumn("ReportScreenTypeInd", "[A0ReportingServicesReport].[ReportScreenTypeInd]", "ReportScreenTypeInd", DataType.String, Nothing, False, 0, False, 100, "Report Screen Type"))
    oList.Columns.Add(New ListColumn("CustomScreenName", "[A0ReportingServicesReport].[CustomScreenName]", "CustomScreenName", DataType.String, Nothing, False, 0, False, 100, "Custom Screen Name"))
    oList.Columns.Add(New ListColumn("RootFolderPath", "[A1ReportingServicesServer].[RootFolderPath]", "RootFolderPath", DataType.String, Nothing, False, 0, False, 100, "Root Folder Path"))
    oList.Columns.Add(New ListColumn("IsVisibleYN", "[A0ReportingServicesReport].[IsVisibleYN]", "IsVisibleYN", DataType.Boolean, "Yes;No", False, 0, False, 100, "Is Visible?"))
    oList.SelectStatement = "select"
    oList.FromClause = "(([ReportingServicesReport]  [A0ReportingServicesReport]" + _
           "    join [ReportingServicesServer]  [A1ReportingServicesServer] on [A0ReportingServicesReport].[ReportingServicesServerID] = [A1ReportingServicesServer].[ReportingServicesServerID])" + _
           "    join [ReportingServicesFolder]  [A2ReportingServicesFolder] on [A0ReportingServicesReport].[ReportingServicesServerID] = [A2ReportingServicesFolder].[ReportingServicesServerID] and [A0ReportingServicesReport].[ReportingServicesFolderPath] = [A2ReportingServicesFolder].[ReportingServicesFolderPath])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship