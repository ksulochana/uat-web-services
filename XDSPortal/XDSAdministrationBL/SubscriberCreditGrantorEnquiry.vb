Imports sembleWare.Runtime
Imports System
Public Class SubscriberCreditGrantorEnquiry 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly SubscriberCreditGrantorEnquiryID As New IdentityElement("SubscriberCreditGrantorEnquiryID", Me, True, Nothing, Nothing)
  Public ReadOnly CreditGrantorEnquiryDate As New DateTimeElement("CreditGrantorEnquiryDate", Me, False, False, True, True, True, "Enquiry Date", Nothing, "g", Nothing, Nothing, Nothing)
  Public ReadOnly EnquiryStatusInd As New IndicatorElement("EnquiryStatusInd", Me, False, False, False, True, True, True, "Enquiry Status", Nothing, SubscriberCreditGrantorEnquiry.EnquiryStatusIndOptions, "Q")
  Public ReadOnly EnquiryResultInd As New IndicatorElement("EnquiryResultInd", Me, False, False, False, True, True, True, "Enquiry Result", Nothing, SubscriberCreditGrantorEnquiry.EnquiryResultIndOptions, "P")
  Public ReadOnly ProductPointValue As New DecimalElement("ProductPointValue", Me, False, False, True, True, True, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ExternalReference As New StringElement("ExternalReference", Me, False, True, True, True, False, 50, "External Reference", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly IDNo As New StringElement("IDNo", Me, False, True, True, True, False, 13, "ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly PassportNo As New StringElement("PassportNo", Me, False, True, True, True, False, 16, "Passport No / Other ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ReferenceNo As New StringElement("ReferenceNo", Me, False, True, True, True, False, 50, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly EnquiryDate As New DateTimeElement("EnquiryDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly ResultIDNo As New StringElement("ResultIDNo", Me, False, False, True, True, False, 13, "ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResultPassportNo As New StringElement("ResultPassportNo", Me, False, False, True, True, False, 16, "Passport No / Other ID No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResultReferenceNo As New StringElement("ResultReferenceNo", Me, False, True, True, True, False, 50, "Reference No", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly ResultEnquiryDate As New DateTimeElement("ResultEnquiryDate", Me, False, True, True, True, False, Nothing, Nothing, "d", Nothing, Nothing, Nothing)
  Public ReadOnly Product As Relationship = New ProductRelationship("Product", Nothing, RelationshipType.Include, True, True, True, Me)
  Public ReadOnly SystemUser As Relationship = New SystemUserRelationship("SystemUser", Nothing, RelationshipType.Include, True, False, True, Me)
  Public ReadOnly Subscriber As Relationship = New SubscriberRelationship("Subscriber", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly SubscriberCreditGrantorEnquiryResult_OwnMany As Relationship = New SubscriberCreditGrantorEnquiryResultRelationship("SubscriberCreditGrantorEnquiryResult", Nothing, "SubscriberCreditGrantorEnquiry", Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _ProductID As New IntegerElement("ProductID", Me, False)
  Public ReadOnly _SubscriberID As New IntegerElement("SubscriberID", Me, True)
  Public ReadOnly _SystemUserID As New IntegerElement("SystemUserID", Me, False)
  Public ReadOnly _Product As ProductRelationship = Product
  Public ReadOnly _SystemUser As SystemUserRelationship = SystemUser
  Public ReadOnly _Subscriber As SubscriberRelationship = Subscriber
  Public ReadOnly _SubscriberCreditGrantorEnquiryResult_OwnMany As SubscriberCreditGrantorEnquiryResultRelationship = SubscriberCreditGrantorEnquiryResult_OwnMany
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moEnquiryStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property EnquiryStatusIndOptions() As IndicatorOptions
    Get
      If moEnquiryStatusIndOptions Is Nothing Then
        moEnquiryStatusIndOptions = New IndicatorOptions
        moEnquiryStatusIndOptions.Add("Q", "Queued")
        moEnquiryStatusIndOptions.Add("P", "Processing")
        moEnquiryStatusIndOptions.Add("C", "Completed")
      End If
      Return moEnquiryStatusIndOptions
    End Get
  End Property
  Private Shared moEnquiryResultIndOptions As IndicatorOptions
  Public Shared ReadOnly Property EnquiryResultIndOptions() As IndicatorOptions
    Get
      If moEnquiryResultIndOptions Is Nothing Then
        moEnquiryResultIndOptions = New IndicatorOptions
        moEnquiryResultIndOptions.Add("P", "Pending")
        moEnquiryResultIndOptions.Add("N", "No Record Found")
        moEnquiryResultIndOptions.Add("F", "Record Found")
        moEnquiryResultIndOptions.Add("M", "Multiple Records Found")
        moEnquiryResultIndOptions.Add("S", "Record Selected")
      End If
      Return moEnquiryResultIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("SubscriberCreditGrantorEnquiry", ApplicationSettings)
    _Product._ProductID.LocalElement = _ProductID
    _SystemUser._SystemUserID.LocalElement = _SystemUserID
    _Subscriber._SubscriberID.LocalElement = _SubscriberID
    AddHandler Product.Changed, AddressOf Product_Changed
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
  Private Sub Product_Changed(ByVal Relationship As Relationship)
    BRProduct(Relationship, True, True)
  End Sub
#End Region 'sembleWare: Change Events

#Region " Actions"

  Public Sub Validate()
    Dim bAllowYN As Boolean = False
    Dim oException As Exception

    Select Case Me._ProductID.Value
      Case Constants.Product.Records.CreditGrantorReEnquiry, Constants.Product.Records.CreditGrantorReEnquiryWebService, _
           Constants.Product.Records.CreditGrantorDebtCollectorsEnquiry, Constants.Product.Records.CreditGrantorDebtCollectorsEnquiryWebService
        oException = New MissingValuesException("(Reference No) or (ID No and Date of Enquiry) or (Passport No / Other ID No and Date of Enquiry)", MyBase.Caption, "")
        If Not Me.ReferenceNo.IsEmpty Then
          bAllowYN = True
        End If

        If Not Me.IDNo.IsEmpty And Not Me.EnquiryDate.IsEmpty Then
          bAllowYN = True
        End If

        If Not Me.PassportNo.IsEmpty And Not Me.EnquiryDate.IsEmpty Then
          bAllowYN = True
        End If
      Case Else
        bAllowYN = True
    End Select

    If Not bAllowYN Then
      Throw New BusinessRuleException(oException.Message)
    End If
  End Sub

  Public Sub QueueMatch()
    Validate()
    Me.EnquiryStatusInd.Value = Constants.SubscriberCreditGrantorEnquiry.Statuses.Queued
    Me.Save()
  End Sub

  Public Sub Match()
    Validate()
    Me.EnquiryStatusInd.Value = Constants.SubscriberCreditGrantorEnquiry.Statuses.Processing
    Me.Save()

    Dim oDataRow As DataRow
    Dim nEnquiryMatchingEngineID As Integer = EnquiryMatchingEngine.SubscriberCreditGrantorMatch(Me.ReferenceNo, Me.IDNo, Me.PassportNo, Me.EnquiryDate, _
                                                                                                 Me._SubscriberID, Me._ProductID.Value, ApplicationSettings)
    Dim oString As Text.StringBuilder = New Text.StringBuilder

    oString.Append("insert into SubscriberCreditGrantorEnquiryResult(SubscriberID, SubscriberCreditGrantorEnquiryID, SubscriberCreditGrantorID, DetailsViewedYN, CreditGrantorSelectedYN, IDNo, PassportNo, ReferenceNo, EnquiryDate)" & vbNewLine)
    oString.Append("select " & Me._SubscriberID.Value & ", " & Me.SubscriberCreditGrantorEnquiryID.Value & ", cgr.SubscriberCreditGrantorID, 0, 0, sce.ResultIDNo, sce.ResultPassportNo, scg.ReferenceNo, scg.EnquiryDate" & vbNewLine)
    oString.Append("  from EnquiryMatchingEngineSubscriberCreditGrantorResult cgr inner join EnquiryMatchingEngineSubscriberCreditGrantor escg")
    oString.Append("                                                                 on cgr.EnquiryMatchingEngineID = escg.EnquiryMatchingEngineID" & vbNewLine)
    oString.Append("                                                                and cgr.EnquiryMatchingEngineSubscriberCreditGrantorID = escg.EnquiryMatchingEngineSubscriberCreditGrantorID" & vbNewLine)
    oString.Append("                                                              inner join SubscriberCreditGrantor scg inner join SubscriberConsumerEnquiry sce" & vbNewLine)
    oString.Append("                                                                                                        on scg.SubscriberID = sce.SubscriberID" & vbNewLine)
    oString.Append("                                                                                                       and scg.SubscriberConsumerEnquiryID = sce.SubscriberConsumerEnquiryID" & vbNewLine)
    oString.Append("                                                                 on cgr.SubscriberID = scg.SubscriberID" & vbNewLine)
    oString.Append("                                                                and cgr.SubscriberCreditGrantorID = scg.SubscriberCreditGrantorID" & vbNewLine)
    oString.Append(" where escg.EnquiryMatchingEngineID = " & ApplicationSettings.QueryBuilder.ToSQL(nEnquiryMatchingEngineID) & vbNewLine)
    oString.Append("   and cgr.IgnoreRecordYN = 0")
    ApplicationSettings.ActionQuery(oString.ToString())

    oString = New Text.StringBuilder
    oString.Append("select count(ecr.SubscriberCreditGrantorID)" & vbNewLine)
    oString.Append("  from EnquiryMatchingEngineSubscriberCreditGrantorResult ecr inner join EnquiryMatchingEngineSubscriberCreditGrantor escg" & vbNewLine)
    oString.Append("                                                                 on ecr.EnquiryMatchingEngineID = escg.EnquiryMatchingEngineID" & vbNewLine)
    oString.Append("                                                                and ecr.EnquiryMatchingEngineSubscriberCreditGrantorID = escg.EnquiryMatchingEngineSubscriberCreditGrantorID" & vbNewLine)
    oString.Append(" where escg.EnquiryMatchingEngineID = " & ApplicationSettings.QueryBuilder.ToSQL(nEnquiryMatchingEngineID) & vbNewLine)
    oString.Append("   and ecr.IgnoreRecordYN = 0")
    For Each oDataRow In ApplicationSettings.ResultQuery(oString.ToString()).Rows
      Select Case System.Convert.ToInt32(oDataRow(0))
        Case 0
          Me.EnquiryResultInd.Value = Constants.SubscriberCreditGrantorEnquiry.Results.NoRecordFound
        Case 1
          Me.EnquiryResultInd.Value = Constants.SubscriberCreditGrantorEnquiry.Results.RecordFound
        Case Else
          Me.EnquiryResultInd.Value = Constants.SubscriberCreditGrantorEnquiry.Results.MultipleRecordsFound
      End Select
    Next

    Me.EnquiryStatusInd.Value = Constants.SubscriberCreditGrantorEnquiry.Statuses.Completed
    Me.Save()
  End Sub
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Base Overrides"
  Public Overrides Sub Load()
    MyBase.Load()
    BRLoad()
  End Sub

  Public Overrides Sub Save()
    With ApplicationSettings
      Try
        Dim bIsNewYN As Boolean = Me.IsNew
        .BeginTransaction()
        BRBeforeSave(bIsNewYN)
        MyBase.Save()
        .CommitTransaction()
      Catch oException As Exception
        .RollbackTransaction()
        Throw oException
      End Try
    End With
  End Sub
#End Region

#Region " Business Rules"
  Private Sub BRLoad()
    Me.DisableElementsAndRelationship()
  End Sub

  Private Sub BRBeforeSave(ByVal IsNewYN As Boolean)
    If IsNewYN Then
      Me.CreditGrantorEnquiryDate.Value = System.DateTime.Now
      ' If SystemUser is not empty, then the XDSAdministration Services has populated this value.
      If SystemUser.IsEmpty Then
        Me.SystemUser.Instance = XDSAdministrationBL.SystemUser.GetLoggedInSystemUser(ApplicationSettings)
      End If

      Me.ResultReferenceNo.Value = Me.ReferenceNo.Value
      Me.ResultEnquiryDate.ValueAsObject = Me.EnquiryDate.ValueAsObject
    End If
  End Sub

  Private Sub BRProduct(ByVal Relationship As Relationship, ByVal SetEnableDisableYN As Boolean, ByVal PerformRulesYN As Boolean)
    Dim oProduct As Product = Nothing

    If Not Relationship.IsEmpty Then
      oProduct = Relationship.Instance
    End If
    Utility.BRProductCreditGrantor(oProduct, Me.ReferenceNo, Me.EnquiryDate, SetEnableDisableYN, PerformRulesYN)

    If PerformRulesYN Then
      If Not oProduct Is Nothing Then
        Dim oSubscriberProfile As SubscriberProfile = SubscriberProfile.GetSubscriberProfile(Me._SubscriberID.Value, ApplicationSettings)
        Dim oSubscriberProfileProduct As SubscriberProfileProduct = SubscriberProfileProduct.GetSubscriberProfileProduct(Me._SubscriberID.Value, oProduct.ProductID.Value, ApplicationSettings)

        If oSubscriberProfileProduct.OverrideDefaultPointValueYN.Value Then
          Me.ProductPointValue.Value = oSubscriberProfileProduct.OverridePointValue.Value
        Else
          Me.ProductPointValue.Value = oProduct.DefaultPointValue.Value
        End If
      End If
    End If
  End Sub
#End Region

#Region " Public Methods"
  Public Function GetSubscriberCreditGrantorEnquiryResult() As SubscriberCreditGrantorEnquiryResult
    If Me.EnquiryResultInd.Value = Constants.SubscriberCreditGrantorEnquiry.Results.RecordFound Or Me.EnquiryResultInd.Value = Constants.SubscriberCreditGrantorEnquiry.Results.RecordSelected Then
      Dim oList As List = Me._SubscriberCreditGrantorEnquiryResult_OwnMany.GridList
      Dim oDataTable As DataTable = oList.DataTable
      Dim oDataRow As DataRow = oDataTable.Rows(0)
      Dim oSubscriberCreditGrantorEnquiryResult As SubscriberCreditGrantorEnquiryResult = Me.SubscriberCreditGrantorEnquiryResult_OwnMany.NewInstance()

      oSubscriberCreditGrantorEnquiryResult._SubscriberCreditGrantorID.Load(oDataRow("SubscriberCreditGrantorID"))
      oSubscriberCreditGrantorEnquiryResult.Load()
      Return oSubscriberCreditGrantorEnquiryResult
    End If
    Return Nothing
  End Function
#End Region
End Class 'sembleWare: Part

Public Class SubscriberCreditGrantorEnquiryRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _SubscriberCreditGrantorEnquiryID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _SubscriberID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New SubscriberCreditGrantorEnquiry(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _SubscriberCreditGrantorEnquiryID.ForeignElement = CType(Instance, SubscriberCreditGrantorEnquiry).SubscriberCreditGrantorEnquiryID
    _SubscriberID.ForeignElement = CType(Instance, SubscriberCreditGrantorEnquiry)._SubscriberID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"
  Public Function MultipleSubscriberCreditGrantorGridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0SubscriberCreditGrantorEnquiry")
    oList.Columns.Add(New ListColumn("SubscriberCreditGrantorEnquiryID", "[A0SubscriberCreditGrantorEnquiry].[SubscriberCreditGrantorEnquiryID]", "SubscriberCreditGrantorEnquiryID", DataType.Long, Nothing, True, 0, False, 100, "Subscriber Credit Grantor Enquiry ID"))
    oList.Columns.Add(New ListColumn("SubscriberID", "[A0SubscriberCreditGrantorEnquiry].[SubscriberID]", "SubscriberID", DataType.Integer, Nothing, True, 0, False, 100, "Subscriber ID"))
    oList.Columns.Add(New ListColumn("CreditGrantorEnquiryDate", "[A0SubscriberCreditGrantorEnquiry].[CreditGrantorEnquiryDate]", "CreditGrantorEnquiryDate", DataType.DateTime, "g", False, -1, True, 100, "Enquiry Date"))
    oList.Columns.Add(New ListColumn("ResultIDNo", "[A0SubscriberCreditGrantorEnquiry].[ResultIDNo]", "ResultIDNo", DataType.String, Nothing, False, 0, True, 100, "ID No"))
    oList.Columns.Add(New ListColumn("ResultPassportNo", "[A0SubscriberCreditGrantorEnquiry].[ResultPassportNo]", "ResultPassportNo", DataType.String, Nothing, False, 0, True, 100, "Passport No / Other ID No"))
    oList.Columns.Add(New ListColumn("ResultReferenceNo", "[A0SubscriberCreditGrantorEnquiry].[ResultReferenceNo]", "ResultReferenceNo", DataType.String, Nothing, False, 0, True, 100, "Reference No"))
    oList.Columns.Add(New ListColumn("ResultEnquiryDate", "[A0SubscriberCreditGrantorEnquiry].[ResultEnquiryDate]", "ResultEnquiryDate", DataType.DateTime, "d", False, 0, True, 100, "Result Enquiry Date"))
    oList.Columns.Add(New ListColumn("EnquiryStatusInd", "[A0SubscriberCreditGrantorEnquiry].[EnquiryStatusInd]", "EnquiryStatusInd", DataType.String, Nothing, False, 0, True, 100, "Enquiry Status"))
    oList.Columns.Add(New ListColumn("EnquiryResultInd", "[A0SubscriberCreditGrantorEnquiry].[EnquiryResultInd]", "EnquiryResultInd", DataType.String, Nothing, False, 0, True, 100, "Enquiry Result"))
    oList.SelectStatement = "select"
    oList.FromClause = "[SubscriberCreditGrantorEnquiry]  [A0SubscriberCreditGrantorEnquiry]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
