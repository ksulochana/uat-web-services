Imports sembleWare.Runtime
Imports System
Public Class LoaderThread 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly LoaderThreadCode As New StringElement("LoaderThreadCode", Me, True, True, True, True, True, 10, Nothing, Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LoaderThreadDesc As New StringElement("LoaderThreadDesc", Me, False, True, True, True, True, 50, "Loader Thread Description", Nothing, Nothing, Nothing, Nothing)
  Public ReadOnly LoaderThreadStatusInd As New IndicatorElement("LoaderThreadStatusInd", Me, False, False, True, True, True, True, "Loader Thread Status", Nothing, LoaderThread.LoaderThreadStatusIndOptions, "A")
  Public ReadOnly IsProcessingYN As New BooleanElement("IsProcessingYN", Me, False, False, True, True, True, "Is Processing?", Nothing, False, "Yes;No")
  Public ReadOnly LoaderProcessType As Relationship = New LoaderProcessTypeRelationship("LoaderProcessType", Nothing, RelationshipType.Include, False, True, True, Me)
  Public ReadOnly ProcessingLoaderProcess As Relationship = New LoaderProcessRelationship("ProcessingLoaderProcess", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly ProcessingLoader As Relationship = New LoaderRelationship("ProcessingLoader", Nothing, RelationshipType.Include, False, False, True, Me)
  Public ReadOnly XDSAdministrationRoot As Relationship = New XDSAdministrationRootRelationship("XDSAdministrationRoot", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _LoaderProcessTypeCode As New StringElement("LoaderProcessTypeCode", Me, False)
  Public ReadOnly _ProcessingLoaderID As New IntegerElement("ProcessingLoaderID", Me, False)
  Public ReadOnly _ProcessingLoaderProcessTypeCode As New StringElement("ProcessingLoaderProcessTypeCode", Me, False)
  Public ReadOnly _LoaderProcessType As LoaderProcessTypeRelationship = LoaderProcessType
  Public ReadOnly _ProcessingLoaderProcess As LoaderProcessRelationship = ProcessingLoaderProcess
  Public ReadOnly _ProcessingLoader As LoaderRelationship = ProcessingLoader
  Public ReadOnly _XDSAdministrationRoot As XDSAdministrationRootRelationship = XDSAdministrationRoot
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  Private Shared moLoaderThreadStatusIndOptions As IndicatorOptions
  Public Shared ReadOnly Property LoaderThreadStatusIndOptions() As IndicatorOptions
    Get
      If moLoaderThreadStatusIndOptions Is Nothing Then
        moLoaderThreadStatusIndOptions = New IndicatorOptions
        moLoaderThreadStatusIndOptions.Add("A", "Active")
        moLoaderThreadStatusIndOptions.Add("I", "Inactive")
      End If
      Return moLoaderThreadStatusIndOptions
    End Get
  End Property
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("LoaderThread", ApplicationSettings)
    _LoaderProcessType._LoaderProcessTypeCode.LocalElement = _LoaderProcessTypeCode
    _ProcessingLoaderProcess._LoaderID.LocalElement = _ProcessingLoaderID
    _ProcessingLoaderProcess._LoaderProcessTypeCode.LocalElement = _ProcessingLoaderProcessTypeCode
    _ProcessingLoader._LoaderID.LocalElement = _ProcessingLoaderID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

#Region " Shared Procedures"
  Public Shared Sub UpdateLoaderThread(ByVal LoaderThreadCode As String, ByVal IsProcessingYN As Boolean, ByVal LoaderID As Long, ByVal LoaderProcessTypeCode As String, ByVal ApplicationSettings As ApplicationSettings)
    If Not LoaderThreadCode Is Nothing AndAlso Not LoaderThreadCode = "" Then
      Dim oLoaderThread As LoaderThread = New LoaderThreadRelationship(ApplicationSettings).NewInstance
      oLoaderThread.LoaderThreadCode.Load(LoaderThreadCode)
      oLoaderThread.Load()

      oLoaderThread.IsProcessingYN.Value = IsProcessingYN
      oLoaderThread.ProcessingLoader.Instance = Nothing
      If IsProcessingYN Then
        oLoaderThread._ProcessingLoaderID.Value = LoaderID
        oLoaderThread._ProcessingLoaderProcessTypeCode.Value = LoaderProcessTypeCode
      End If
      oLoaderThread.Save()
    End If
  End Sub

  Public Shared Function GetLoaderThread(ByVal LoaderProcessTypeCode As String, ByVal ApplicationSettings As ApplicationSettings) As LoaderThread
    Dim oXDSAdministrationRoot As XDSAdministrationRoot = New XDSAdministrationRootRelationship(ApplicationSettings).NewInstance
    Dim oDBList As DBList = oXDSAdministrationRoot._LoaderThread_OwnMany.GridList
    Dim oDataRow As DataRow

    oDBList.AndFilters.Add(New ListFilter("LoaderThreadStatusInd", "[A0LoaderThread].[LoaderThreadStatusInd]", Constants.LoaderThread.Statuses.Active, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, False))
    oDBList.AppendToWhereClause("[A1LoaderProcessType].[LoaderProcessTypeCode] = " & ApplicationSettings.QueryBuilder.ToSQL(LoaderProcessTypeCode))
    If oDBList.DataTable.Rows.Count = 0 Then
      oDBList = oXDSAdministrationRoot._LoaderThread_OwnMany.GridList
      oDBList.AndFilters.Add(New ListFilter("LoaderThreadStatusInd", "[A0LoaderThread].[LoaderThreadStatusInd]", Constants.LoaderThread.Statuses.Active, DataType.String, FilterType.Equals, FilterBlankBehavior.DisplayEmptyList, False))
      oDBList.AppendToWhereClause("[A1LoaderProcessType].[LoaderProcessTypeCode] is null")
    End If
    For Each oDataRow In oDBList.DataTable.Rows
      Dim oLoaderThread As LoaderThread = oDBList.NewInstance

      oLoaderThread.LoaderThreadCode.Load(oDataRow("LoaderThreadCode"))
      oLoaderThread.Load()
      Return oLoaderThread
    Next
    Return Nothing
  End Function
#End Region
End Class 'sembleWare: Part

Public Class LoaderThreadRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _LoaderThreadCode As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New LoaderThread(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _LoaderThreadCode.ForeignElement = CType(Instance, LoaderThread).LoaderThreadCode
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0LoaderThread")
    oList.Columns.Add(New ListColumn("LoaderThreadCode", "[A0LoaderThread].[LoaderThreadCode]", "LoaderThreadCode", DataType.String, Nothing, True, 1, True, 100, "Loader Thread Code"))
    oList.Columns.Add(New ListColumn("LoaderThreadDesc", "[A0LoaderThread].[LoaderThreadDesc]", "LoaderThreadDesc", DataType.String, Nothing, False, 0, True, 200, "Loader Thread Description"))
    oList.Columns.Add(New ListColumn("LoaderThreadStatusInd", "[A0LoaderThread].[LoaderThreadStatusInd]", "LoaderThreadStatusInd", DataType.String, Nothing, False, 0, True, 100, "Loader Thread Status"))
    oList.Columns.Add(New ListColumn("LoaderProcessTypeCode", "[A1LoaderProcessType].[LoaderProcessTypeCode]", "LoaderProcessTypeCode", DataType.String, Nothing, False, 0, True, 100, "Loader Process Type Code"))
    oList.Columns.Add(New ListColumn("LoaderProcessTypeDesc", "[A1LoaderProcessType].[LoaderProcessTypeDesc]", "LoaderProcessTypeDesc", DataType.String, Nothing, False, 0, True, 200, "Loader Process Type Description"))
    oList.Columns.Add(New ListColumn("IsProcessingYN", "[A0LoaderThread].[IsProcessingYN]", "IsProcessingYN", DataType.Boolean, "Yes;No", False, 0, True, 100, "Is Processing?"))
    oList.Columns.Add(New ListColumn("ProcessingLoaderID", "[A0LoaderThread].[ProcessingLoaderID]", "ProcessingLoaderID", DataType.Integer, Nothing, False, 0, True, 100, "Processing Loader ID"))
    oList.SelectStatement = "select"
    oList.FromClause = "([LoaderThread]  [A0LoaderThread]" + _
           "    left join [LoaderProcessType]  [A1LoaderProcessType] on [A0LoaderThread].[LoaderProcessTypeCode] = [A1LoaderProcessType].[LoaderProcessTypeCode])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function DropDownList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0LoaderThread")
    oList.Columns.Add(New ListColumn("LoaderThreadCode", "[A0LoaderThread].[LoaderThreadCode]", "LoaderThreadCode", DataType.String, Nothing, True, 1, True, 100, "Loader Thread Code"))
    oList.Columns.Add(New ListColumn("LoaderThreadDesc", "[A0LoaderThread].[LoaderThreadDesc]", "LoaderThreadDesc", DataType.String, Nothing, False, 0, True, 200, "Loader Thread Description"))
    oList.Columns.Add(New ListColumn("LoaderThreadStatusInd", "[A0LoaderThread].[LoaderThreadStatusInd]", "LoaderThreadStatusInd", DataType.String, Nothing, False, 0, True, 100, "Loader Thread Status"))
    oList.SelectStatement = "select"
    oList.FromClause = "[LoaderThread]  [A0LoaderThread]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function


  Public Function ForeignList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0LoaderThread")
    oList.Columns.Add(New ListColumn("LoaderThreadCode", "[A0LoaderThread].[LoaderThreadCode]", "LoaderThreadCode", DataType.String, Nothing, True, 1, True, 100, "Loader Thread Code"))
    oList.Columns.Add(New ListColumn("LoaderThreadDesc", "[A0LoaderThread].[LoaderThreadDesc]", "LoaderThreadDesc", DataType.String, Nothing, False, 0, True, 200, "Loader Thread Description"))
    oList.SelectStatement = "select"
    oList.FromClause = "[LoaderThread]  [A0LoaderThread]"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
