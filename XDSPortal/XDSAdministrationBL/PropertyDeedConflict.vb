Imports sembleWare.Runtime
Imports System
Public Class PropertyDeedConflict 'sembleWare: Part
  Inherits CustomDBPart

#Region " Elements & Relationships"

  'sembleWare: Elements Start - Do Not Modify
  Public ReadOnly PropertyDeed As Relationship = New PropertyDeedRelationship("PropertyDeed", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  Public ReadOnly ConflictPropertyDeed As Relationship = New PropertyDeedRelationship("ConflictPropertyDeed", Nothing, RelationshipType.OwnedBy, True, True, True, Me)
  'sembleWare: Elements End - Do Not Modify

#Region "  Foreign Items"

  'sembleWare: Foreign Items Start - Do Not Modify
  Public ReadOnly _PropertyDeedID As New IntegerElement("PropertyDeedID", Me, True)
  Public ReadOnly _ConflictPropertyDeedID As New IntegerElement("ConflictPropertyDeedID", Me, True)
  Public ReadOnly _PropertyDeed As PropertyDeedRelationship = PropertyDeed
  Public ReadOnly _ConflictPropertyDeed As PropertyDeedRelationship = ConflictPropertyDeed
  'sembleWare: Foreign Items End - Do Not Modify

#End Region 'sembleWare: Foreign Items

#Region "  Indicator Options"

  'sembleWare: Indicator Options Start - Do Not Modify
  'sembleWare: Indicator Options End - Do Not Modify

#End Region 'sembleWare: Indicator Options
#End Region 'sembleWare: Elements & Relationships

#Region " Constructor"

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)

    'sembleWare: Constructor Start - Do Not Modify
    MyBase.New("PropertyDeedConflict", ApplicationSettings)
    _PropertyDeed._PropertyDeedID.LocalElement = _PropertyDeedID
    _ConflictPropertyDeed._PropertyDeedID.LocalElement = _ConflictPropertyDeedID
    'sembleWare: Constructor End - Do Not Modify

  End Sub

#End Region 'sembleWare: Constructor

#Region " Change Events"
#End Region 'sembleWare: Change Events

#Region " Actions"
#End Region 'sembleWare: Actions

#Region " Wrapper Classes"
#End Region 'sembleWare: Wrapper Classes

End Class 'sembleWare: Part

Public Class PropertyDeedConflictRelationship 'sembleWare: Relationship
  Inherits Relationship

#Region " Relationship Code"

  'sembleWare: Relationship Start - Do Not Modify
  Public ReadOnly _PropertyDeedID As ForeignKey = New ForeignKey(Me)
  Public ReadOnly _ConflictPropertyDeedID As ForeignKey = New ForeignKey(Me)

  Public Sub New(ByVal ApplicationSettings As ApplicationSettings)
    MyBase.New(ApplicationSettings)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal Type As RelationshipType, ByVal Mandatory As Boolean, ByVal Enabled As Boolean, ByVal Persist As Boolean, ByVal Owner As Part)
    MyBase.New(Name, Caption, Type, Mandatory, Enabled, Persist, Owner)
  End Sub

  Public Sub New(ByVal Name As String, ByVal Caption As String, ByVal OwnedByName As String, ByVal Owner As Part)
    MyBase.New(Name, Caption, OwnedByName, Owner)
  End Sub

  Public Overrides Function CreateObject() As sembleWare.Runtime.Part
    Return New PropertyDeedConflict(ApplicationSettings)
  End Function

  Public Overrides Sub MapForeignKeys(ByVal Instance As sembleWare.Runtime.Part)
    _PropertyDeedID.ForeignElement = CType(Instance, PropertyDeedConflict)._PropertyDeedID
    _ConflictPropertyDeedID.ForeignElement = CType(Instance, PropertyDeedConflict)._ConflictPropertyDeedID
  End Sub

  'sembleWare: Relationship End - Do Not Modify
#End Region 'sembleWare: Relationship Code


#Region " List Definitions"

  Public Function GridList() As sembleWare.Runtime.List
    'sembleWare: List Declaration Start - Do Not Modify
    Dim oList As DBList = New DBList(Me, "A0PropertyDeedConflict")
    oList.Columns.Add(New ListColumn("PropertyDeedID", "[A0PropertyDeedConflict].[PropertyDeedID]", "PropertyDeedID", DataType.Integer, Nothing, True, 0, False, 100, "Property Deed ID"))
    oList.Columns.Add(New ListColumn("ConflictPropertyDeedID", "[A0PropertyDeedConflict].[ConflictPropertyDeedID]", "ConflictPropertyDeedID", DataType.Integer, Nothing, True, 0, False, 100, "Conflict Property Deed ID"))
    oList.Columns.Add(New ListColumn("PropertyDeedID1", "[A1ConflictPropertyDeed].[PropertyDeedID]", "PropertyDeedID", DataType.Long, Nothing, False, 1, True, 100, "Property Deed ID"))
    oList.Columns.Add(New ListColumn("TitleDeedNo", "[A1ConflictPropertyDeed].[TitleDeedNo]", "TitleDeedNo", DataType.String, Nothing, False, 0, True, 100, "Title Deed No"))
    oList.SelectStatement = "select"
    oList.FromClause = "([PropertyDeedConflict]  [A0PropertyDeedConflict]" + _
           "    join [PropertyDeed]  [A1ConflictPropertyDeed] on [A0PropertyDeedConflict].[ConflictPropertyDeedID] = [A1ConflictPropertyDeed].[PropertyDeedID])"
    oList.WhereClause = ""
    oList.ApplyRelationshipLimits()
    'sembleWare: List Declaration End - Do Not Modify
    Return oList
  End Function

#End Region 'sembleWare: List Definitions

End Class 'sembleWare: Relationship
